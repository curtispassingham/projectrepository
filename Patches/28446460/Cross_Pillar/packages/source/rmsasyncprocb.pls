CREATE OR REPLACE PACKAGE BODY RMS_ASYNC_PROCESS_SQL AS

P_Item   ITEM_MASTER.ITEM%TYPE;

------------------------------------------------------------------------------------------------
-- Function Name: INSERT_ASYNC_STATUS_TBLS
-- Purpose      : This function inserts a record into the RMS_ASYNC_STATUS and RMS_ASYNC_RETRY
--                tables. It is called by enqueue processes to track the start of an async job.
------------------------------------------------------------------------------------------------
FUNCTION INSERT_ASYNC_STATUS_TBLS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rms_async_id   IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                  I_job_type       IN     RMS_ASYNC_STATUS.JOB_TYPE%TYPE,
                                  I_start_state    IN     RMS_ASYNC_STATUS.START_STATE%TYPE,
                                  I_end_state      IN     RMS_ASYNC_STATUS.END_STATE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: WRITE_SUCCESS
-- Purpose      : This function updates a given async job to success status. It is called by
--                dequeue processes when an async job is successfully completed.
------------------------------------------------------------------------------------------------
FUNCTION WRITE_SUCCESS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_rms_async_id   IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Function Name: WRITE_ERROR
-- Purpose      : This function updates a given async job to error status and writes the failure
--                message to the RMS_ASYNC_RETRY table. It is called by dequeue processes
--                when an async job fails during processing. It issues a rollback to savepoint
--                'ASYNC', which is expected to be set at the start of the asynchronous process.
------------------------------------------------------------------------------------------------
FUNCTION WRITE_ERROR(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_error_message  IN     RTK_ERRORS.RTK_TEXT%TYPE,
                      I_rms_async_id   IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_RAF_NOTIFICATION(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_notification_id      IN       RAF_NOTIFICATION.RAF_NOTIFICATION_ID%TYPE,
                                 I_task_flow_url        IN       VARCHAR2,
                                 I_severity             IN       RAF_NOTIFICATION.SEVERITY%TYPE,
                                 I_application_code     IN       RAF_NOTIFICATION.APPLICATION_CODE%TYPE,
                                 I_launchable           IN       RAF_NOTIFICATION.LAUNCHABLE%TYPE,
                                 I_user                 IN       RAF_NOTIFICATION_RECIPIENTS.RECIPIENT_ID%TYPE,
                                 I_rms_async_id         IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                 I_process_id           IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE DEFAULT NULL,
                                 I_job_type             IN       RMS_ASYNC_STATUS.JOB_TYPE%TYPE DEFAULT NULL,
                                 I_notification_desc    IN       RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE DEFAULT NULL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION WRITE_RAF_NOTIFICATION(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_rms_async_id        IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN;                                 
------------------------------------------------------------------------------------------------
FUNCTION GET_NOTIFICATION_INFO(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_taskflow_url        IN OUT   RMS_ASYNC_JOB.TASKFLOW_URL%TYPE,
                               O_notification_type    OUT      RAF_NOTIFICATION.NOTIFICATION_TYPE%TYPE,
                               O_notification_desc    OUT      RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE,
                               I_job_type             IN       RMS_ASYNC_STATUS.JOB_TYPE%TYPE,
                               I_action_type          IN       SVC_PROCESS_TRACKER.ACTION_TYPE%TYPE,
                               I_async_status         IN       RMS_ASYNC_STATUS.STATUS%TYPE,
                               I_process_source       IN       SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                               I_template_key         IN       SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE,
                               I_process_id           IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION INSERT_ASYNC_STATUS_TBLS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rms_async_id   IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                  I_job_type       IN     RMS_ASYNC_STATUS.JOB_TYPE%TYPE,
                                  I_start_state    IN     RMS_ASYNC_STATUS.START_STATE%TYPE,
                                  I_end_state      IN     RMS_ASYNC_STATUS.END_STATE%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.INSERT_ASYNC_STATUS_TBLS';

BEGIN

   insert into rms_async_status(rms_async_id,
                                job_type,
                                status,
                                start_state,
                                end_state,
                                create_id,
                                create_datetime,
                                last_update_id,
                                last_update_datetime)
                        values (I_rms_async_id,
                                I_job_type,
                                RMS_CONSTANTS.ASYNC_STATUS_NEW,
                                I_start_state,
                                I_end_state,
                                get_user,
                                sysdate,
                                get_user,
                                sysdate);

   --We need to create this here so we can capture the correct user.
   --If wait until processing, it will be running under the SYS user.
   insert into rms_async_retry(rms_async_id,
                               retry_attempt_num,
                               error_message,
                               retry_user_id,
                               retry_datetime,
                               last_update_id,
                               last_update_datetime)
                       values (I_rms_async_id,
                               1,
                               null,
                               get_user,
                               sysdate,
                               get_user,
                               sysdate);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ASYNC_STATUS_TBLS;
------------------------------------------------------------------------------------------------
FUNCTION WRITE_SUCCESS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rms_async_id   IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS';

   L_table              VARCHAR2(30)         := 'RMS_ASYNC_STATUS';
   L_user_id            RMS_ASYNC_STATUS.CREATE_ID%TYPE;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select create_id
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;
         
BEGIN

   logger.log('Call RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS for rms_async_id: '||I_rms_async_id);

   open C_LOCK;
   fetch C_LOCK into L_user_id;
   close C_LOCK;

   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_SUCCESS,
          last_update_id       = L_user_id,
          last_update_datetime = SYSDATE
    where rms_async_id = I_rms_async_id;
    
   if WRITE_RAF_NOTIFICATION(O_error_message,
                             I_rms_async_id) = FALSE then
       raise PROGRAM_ERROR;
   end if;   
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      if WRITE_ERROR(O_error_message,
                     O_error_message,
                     I_rms_async_id) = FALSE then
         return FALSE;
      end if;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if WRITE_ERROR(O_error_message,
                     O_error_message,
                     I_rms_async_id) = FALSE then
         return FALSE;
      end if;
      return FALSE;

END WRITE_SUCCESS;
------------------------------------------------------------------------------------------------
FUNCTION WRITE_ERROR(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_error_message  IN     RTK_ERRORS.RTK_TEXT%TYPE,
                     I_rms_async_id   IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(61) := 'RMS_ASYNC_PROCESS_SQL.WRITE_ERROR';

   L_max_retry_attempt    RMS_ASYNC_RETRY.RETRY_ATTEMPT_NUM%TYPE;
   L_user                 RMS_ASYNC_RETRY.RETRY_USER_ID%TYPE;
  
   L_table                VARCHAR2(30);
   
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STATUS is
      select create_id
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_RETRY_ATTEMPT is
      select max(retry_attempt_num)
        from rms_async_retry
       where rms_async_id = I_rms_async_id;

   cursor C_LOCK_RETRY is
      select 'x'
        from rms_async_retry
       where rms_async_id = I_rms_async_id
         and retry_attempt_num = L_max_retry_attempt
         for update nowait;


BEGIN
   DECLARE
      ex_savepoint_missing EXCEPTION;
      PRAGMA EXCEPTION_INIT(ex_savepoint_missing, -01086);
   BEGIN
      -- 'ASYNC' should be the name of the savepoint set at the beginning of async dequeue process
      DBMS_STANDARD.ROLLBACK_SV('ASYNC');
   EXCEPTION
      WHEN ex_savepoint_missing THEN
         --Ignore savepoint missing error. This is because, if the called package commits or rollback
         -- savepoint will be erased. In that case the call to write_error fails at this step
         NULL;
      WHEN others THEN
	     raise;
   END;

   logger.log('Call RMS_ASYNC_PROCESS_SQL.WRITE_ERROR for rms_async_id: '||I_rms_async_id || ' with error: ' || I_error_message);

   L_table := 'RMS_ASYNC_STATUS';
   open C_LOCK_STATUS;
   fetch C_LOCK_STATUS into L_user;
   close C_LOCK_STATUS;

   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_ERROR,
          last_update_id       = L_user,
          last_update_datetime = sysdate
    where rms_async_id = I_rms_async_id;

   open C_RETRY_ATTEMPT;
   fetch C_RETRY_ATTEMPT into L_max_retry_attempt;
   close C_RETRY_ATTEMPT;

   L_table := 'RMS_ASYNC_RETRY';
   open C_LOCK_RETRY;
   close C_LOCK_RETRY;

   update rms_async_retry
      set error_message        = I_error_message,
          last_update_id       = L_user,
          last_update_datetime = sysdate
    where rms_async_id = I_rms_async_id
      and retry_attempt_num = L_max_retry_attempt;

   if WRITE_RAF_NOTIFICATION(O_error_message,
                             I_rms_async_id) = FALSE then
       raise PROGRAM_ERROR;
   end if; 
   
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      if WRITE_RAF_NOTIFICATION(O_error_message,
                                I_rms_async_id) = FALSE then
          return FALSE;
      end if;                                            
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      if WRITE_RAF_NOTIFICATION(O_error_message,
                                I_rms_async_id) = FALSE then
         return FALSE;
      end if; 
      return FALSE;
END WRITE_ERROR;
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_NEW_ITEM_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_NEW_ITEM_LOC';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_NEW_ITEM_LOC;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   if RMS_ASYNC_PROCESS_SQL.INSERT_ASYNC_STATUS_TBLS(O_error_message,
                                                     I_rms_async_id,
                                                     RMS_CONSTANTS.ASYNC_JOB_NEW_ITEM_LOC,
                                                     NULL,  --start_state
                                                     NULL) = FALSE then    --end_state
      return FALSE;
   end if;

   L_message := rms_async_msg(I_rms_async_id);
   --Set visibility to DBMS_AQ.ON_COMMIT to ensure that enqueue is only done on commit,
   --so that all the data required by the async process has been committed to the database
   --prior to async execution.
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_NEW_ITEM_LOC, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_NEW_ITEM_LOC;
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_NEW_ITEM_LOC_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_NEW_ITEM_LOC_RETRY';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_NEW_ITEM_LOC;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

   L_retry_attempt_num   RMS_ASYNC_RETRY.RETRY_ATTEMPT_NUM%TYPE;

   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select 'x'
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_SEQ is
      select max(retry_attempt_num) + 1
        from rms_async_retry
       where rms_async_id = I_rms_async_id;

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   --Set async job status to in-progress
   L_table := 'RMS_ASYNC_STATUS';
   open C_LOCK;
   close C_LOCK;

   update rms_async_status
      set status = RMS_CONSTANTS.ASYNC_STATUS_RETRY,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where rms_async_id = I_rms_async_id;

   open C_SEQ;
   fetch C_SEQ into L_retry_attempt_num;
   close C_SEQ;

   insert into rms_async_retry (rms_async_id,
                                retry_attempt_num,
                                error_message,
                                retry_user_id,
                                retry_datetime,
                                last_update_id,
                                last_update_datetime)
                        values (I_rms_async_id,
                                L_retry_attempt_num,
                                null,
                                get_user,
                                sysdate,
                                get_user,
                                sysdate);

   L_message := rms_async_msg(I_rms_async_id);
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_NEW_ITEM_LOC_RETRY, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_NEW_ITEM_LOC_RETRY;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_NEW_ITEM_LOC(context  raw,
                              reginfo  sys.aq$_reg_info,
                              descr    sys.aq$_descriptor,
                              payload  raw,
                              payloadl number)
AS
   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.NOTIFY_NEW_ITEM_LOC';

   PROGRAM_ERROR         EXCEPTION;
   ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error

   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_rms_async_id        RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;

   L_message             RMS_ASYNC_MSG;
   L_dequeue_options     DBMS_AQ.DEQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);
   L_user                RMS_ASYNC_STATUS.CREATE_ID%TYPE;
   L_table               VARCHAR2(30);

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select create_id
        from rms_async_status
       where rms_async_id = L_rms_async_id
         for update nowait;

   cursor C_GET_ITEM is
      select item
        from svc_item_loc_ranging
       where rms_async_id = L_rms_async_id;
         
BEGIN

   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE;
   L_dequeue_options.wait := DBMS_AQ.NO_WAIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_NEW_ITEM_LOC, before dbms_aq.dequeue with msgid: ' || L_dequeue_options.msgid ||
              ', consumer_name: '|| L_dequeue_options.consumer_name ||
              ', queue_name: '|| descr.queue_name);

   DBMS_AQ.DEQUEUE(QUEUE_NAME => descr.queue_name,
                   DEQUEUE_OPTIONS => L_dequeue_options,
                   MESSAGE_PROPERTIES => L_message_properties,
                   PAYLOAD => L_message,
                   MSGID => L_message_handle);

   if L_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('DEQUEUE_ERROR', descr.queue_name, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rms_async_id := L_message.rms_async_id;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_NEW_ITEM_LOC, after dbms_aq.dequeue with rms_async_id: ' || L_rms_async_id);

   --Set a savepoint before the core processing to provide a rollback point in case of error
   DBMS_STANDARD.SAVEPOINT('ASYNC');

   L_table := 'RMS_ASYNC_STATUS';

   --Set async job status to in-progress
   open C_LOCK;
   fetch C_LOCK into L_user;
   close C_LOCK;

   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_IN_PROGRESS,
          last_update_id       = L_user,
          last_update_datetime = SYSDATE
    where rms_async_id = L_rms_async_id;

   /*Set the user id in this session's client info.            */
   /*This will be viewed by the filter_policy_sql further down */
   /*the processing via SYS_CONTEXT.                           */
   DBMS_APPLICATION_INFO.SET_CLIENT_INFO(L_user);

   open C_GET_ITEM;
   fetch C_GET_ITEM into P_Item;
   close C_GET_ITEM;

   --Call core processing logic for new item loc ranging
   logger.log('Call core service CORESVC_ITEM_LOC_RANGING_SQL.CREATE_ITEM_LOC with rms_async_id: ' || L_rms_async_id);

   if CORESVC_ITEM_LOC_RANGING_SQL.CREATE_ITEM_LOC(L_error_message,
                                                   L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- update the status table
   if RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS(L_error_message,
                                          L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
      --Only raise_application_error if something is fatally wrong and we cannot even log error.
      --Otherwise, do NOT raise_application_error so that error will be updated on the staging table with status.
      --Any business logic DMLs will be rolled back through savepoint.
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;

   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_rms_async_id,
                                            NULL);

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
END NOTIFY_NEW_ITEM_LOC;
-------------------------------------------------------------------------------
FUNCTION ENQUEUE_UPDATE_ITEM_LOC(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_UPDATE_ITEM_LOC';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_UPDATE_ITEM_LOC;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   if RMS_ASYNC_PROCESS_SQL.INSERT_ASYNC_STATUS_TBLS(O_error_message,
                                                     I_rms_async_id,
                                                     RMS_CONSTANTS.ASYNC_JOB_UPDATE_ITEM_LOC,
                                                     NULL,  --start_state
                                                     NULL) = FALSE then    --end_state
      return FALSE;
   end if;

   L_message := rms_async_msg(I_rms_async_id);
   --Set visibility to DBMS_AQ.ON_COMMIT to ensure that enqueue is only done on commit,
   --so that all the data required by the async process has been committed to the database
   --prior to async execution.
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_UPDATE_ITEM_LOC, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_UPDATE_ITEM_LOC;
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_UPDATE_ITEM_LOC_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_UPDATE_ITEM_LOC_RETRY';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_UPDATE_ITEM_LOC;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

   L_retry_attempt_num   RMS_ASYNC_RETRY.RETRY_ATTEMPT_NUM%TYPE;

   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select 'x'
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_SEQ is
      select max(retry_attempt_num) + 1
        from rms_async_retry
       where rms_async_id = I_rms_async_id;

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   --Set async job status to in-progress
   L_table := 'RMS_ASYNC_STATUS';
   open C_LOCK;
   close C_LOCK;

   update rms_async_status
      set status = RMS_CONSTANTS.ASYNC_STATUS_RETRY,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where rms_async_id = I_rms_async_id;

   open C_SEQ;
   fetch C_SEQ into L_retry_attempt_num;
   close C_SEQ;

   insert into rms_async_retry (rms_async_id,
                                retry_attempt_num,
                                error_message,
                                retry_user_id,
                                retry_datetime,
                                last_update_id,
                                last_update_datetime)
                        values (I_rms_async_id,
                                L_retry_attempt_num,
                                null,
                                get_user,
                                sysdate,
                                get_user,
                                sysdate);

   L_message := rms_async_msg(I_rms_async_id);
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_UPDATE_ITEM_LOC_RETRY, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_UPDATE_ITEM_LOC_RETRY;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_UPDATE_ITEM_LOC(context  raw,
                                 reginfo  sys.aq$_reg_info,
                                 descr    sys.aq$_descriptor,
                                 payload  raw,
                                 payloadl number)
AS
   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.NOTIFY_UPDATE_ITEM_LOC';

   PROGRAM_ERROR         EXCEPTION;
   ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error

   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_rms_async_id        RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;

   L_message             RMS_ASYNC_MSG;
   L_dequeue_options     DBMS_AQ.DEQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);
   L_user                RMS_ASYNC_STATUS.CREATE_ID%TYPE;
   L_table               VARCHAR2(30);

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select create_id
        from rms_async_status
       where rms_async_id = L_rms_async_id
         for update nowait;

BEGIN

   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE;
   L_dequeue_options.wait := DBMS_AQ.NO_WAIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_UPDATE_ITEM_LOC, before dbms_aq.dequeue with msgid: ' || L_dequeue_options.msgid ||
              ', consumer_name: '|| L_dequeue_options.consumer_name ||
              ', queue_name: '|| descr.queue_name);

   DBMS_AQ.DEQUEUE(QUEUE_NAME => descr.queue_name,
                   DEQUEUE_OPTIONS => L_dequeue_options,
                   MESSAGE_PROPERTIES => L_message_properties,
                   PAYLOAD => L_message,
                   MSGID => L_message_handle);

   if L_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('DEQUEUE_ERROR', descr.queue_name, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rms_async_id := L_message.rms_async_id;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_UPDATE_ITEM_LOC, after dbms_aq.dequeue with rms_async_id: ' || L_rms_async_id);

   --Set a savepoint before the core processing to provide a rollback point in case of error
   DBMS_STANDARD.SAVEPOINT('ASYNC');

   L_table := 'RMS_ASYNC_STATUS';

   --Set async job status to in-progress
   open C_LOCK;
   fetch C_LOCK into L_user;
   close C_LOCK;

   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_IN_PROGRESS,
          last_update_id       = L_user,
          last_update_datetime = SYSDATE
    where rms_async_id = L_rms_async_id;

   /*Set the user id in this session's client info.            */
   /*This will be viewed by the filter_policy_sql further down */
   /*the processing via SYS_CONTEXT.                           */
   DBMS_APPLICATION_INFO.SET_CLIENT_INFO(L_user);

   --Call core processing logic for new item loc ranging
   logger.log('Call core service CORESVC_ITEM_LOC_RANGING_SQL.UPDATE_ITEM_LOC with rms_async_id: ' || L_rms_async_id);

   if CORESVC_ITEM_LOC_RANGING_SQL.UPDATE_ITEM_LOC(L_error_message,
                                                   L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- update the status table
   if RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS(L_error_message,
                                          L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
      --Only raise_application_error if something is fatally wrong and we cannot even log error.
      --Otherwise, do NOT raise_application_error so that error will be updated on the staging table with status.
      --Any business logic DMLs will be rolled back through savepoint.
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;

   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_rms_async_id,
                                            NULL);
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;

   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
END NOTIFY_UPDATE_ITEM_LOC;
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_STORE_ADD(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_STORE_ADD';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_STORE_ADD;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   if RMS_ASYNC_PROCESS_SQL.INSERT_ASYNC_STATUS_TBLS(O_error_message,
                                                     I_rms_async_id,
                                                     RMS_CONSTANTS.ASYNC_JOB_STORE_ADD,
                                                     NULL,  --start_state
                                                     NULL) = FALSE then    --end_state
      return FALSE;
   end if;

   L_message := rms_async_msg(I_rms_async_id);
   --Set visibility to DBMS_AQ.ON_COMMIT to ensure that enqueue is only done on commit,
   --so that all the data required by the async process has been committed to the database
   --prior to async execution.
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_STORE_ADD, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_STORE_ADD;
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_STORE_ADD_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_STORE_ADD_RETRY';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_STORE_ADD;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

   L_retry_attempt_num   RMS_ASYNC_RETRY.RETRY_ATTEMPT_NUM%TYPE;

   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select 'x'
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_SEQ is
      select max(retry_attempt_num) + 1
        from rms_async_retry
       where rms_async_id = I_rms_async_id;

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   --Set async job status to in-progress
   L_table := 'RMS_ASYNC_STATUS';
   open C_LOCK;
   close C_LOCK;

   update rms_async_status
      set status = RMS_CONSTANTS.ASYNC_STATUS_RETRY,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where rms_async_id = I_rms_async_id;

   open C_SEQ;
   fetch C_SEQ into L_retry_attempt_num;
   close C_SEQ;

   insert into rms_async_retry (rms_async_id,
                                retry_attempt_num,
                                error_message,
                                retry_user_id,
                                retry_datetime,
                                last_update_id,
                                last_update_datetime)
                        values (I_rms_async_id,
                                L_retry_attempt_num,
                                null,
                                get_user,
                                sysdate,
                                get_user,
                                sysdate);

   L_message := rms_async_msg(I_rms_async_id);
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_STORE_ADD_RETRY, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_STORE_ADD_RETRY;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_STORE_ADD(context  raw,
                           reginfo  sys.aq$_reg_info,
                           descr    sys.aq$_descriptor,
                           payload  raw,
                           payloadl number)
AS
   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.NOTIFY_STORE_ADD';

   PROGRAM_ERROR         EXCEPTION;
   ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error

   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_rms_async_id        RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;

   L_message             RMS_ASYNC_MSG;
   L_dequeue_options     DBMS_AQ.DEQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);
   L_user                RMS_ASYNC_STATUS.CREATE_ID%TYPE;
   L_table               VARCHAR2(30);

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select create_id
        from rms_async_status
       where rms_async_id = L_rms_async_id
         for update nowait;

BEGIN

   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE;
   L_dequeue_options.wait := DBMS_AQ.NO_WAIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_STORE_ADD, before dbms_aq.dequeue with msgid: ' || L_dequeue_options.msgid ||
              ', consumer_name: '|| L_dequeue_options.consumer_name ||
              ', queue_name: '|| descr.queue_name);

   DBMS_AQ.DEQUEUE(QUEUE_NAME => descr.queue_name,
                   DEQUEUE_OPTIONS => L_dequeue_options,
                   MESSAGE_PROPERTIES => L_message_properties,
                   PAYLOAD => L_message,
                   MSGID => L_message_handle);

   if L_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('DEQUEUE_ERROR', descr.queue_name, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rms_async_id := L_message.rms_async_id;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_STORE_ADD, after dbms_aq.dequeue with rms_async_id: ' || L_rms_async_id);

   --Set a savepoint before the core processing to provide a rollback point in case of error
   DBMS_STANDARD.SAVEPOINT('ASYNC');

   L_table := 'RMS_ASYNC_STATUS';

   --Set async job status to in-progress
   open C_LOCK;
   fetch C_LOCK into L_user;
   close C_LOCK;

   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_IN_PROGRESS,
          last_update_id       = L_user,
          last_update_datetime = SYSDATE
    where rms_async_id = L_rms_async_id;

   /*Set the user id in this session's client info.            */
   /*This will be viewed by the filter_policy_sql further down */
   /*the processing via SYS_CONTEXT.                           */
   DBMS_APPLICATION_INFO.SET_CLIENT_INFO(L_user);

   --Call core processing logic for new item loc ranging
   logger.log('Call core service CORESVC_STORE_ADD_SQL.ADD_STORE with rms_async_id: ' || L_rms_async_id);

   if CORESVC_STORE_ADD_SQL.ADD_STORE(L_error_message,
                                      L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- update the status table
   if RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS(L_error_message,
                                          L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
      --Only raise_application_error if something is fatally wrong and we cannot even log error.
      --Otherwise, do NOT raise_application_error so that error will be updated on the staging table with status.
      --Any business logic DMLs will be rolled back through savepoint.
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;

   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_rms_async_id,
                                            NULL);

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
END NOTIFY_STORE_ADD;
-------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_WH_ADD(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_WH_ADD';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_WH_ADD;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   if RMS_ASYNC_PROCESS_SQL.INSERT_ASYNC_STATUS_TBLS(O_error_message,
                                                     I_rms_async_id,
                                                     RMS_CONSTANTS.ASYNC_JOB_WH_ADD,
                                                     NULL,  --start_state
                                                     NULL) = FALSE then    --end_state
      return FALSE;
   end if;

   L_message := rms_async_msg(I_rms_async_id);
   --Set visibility to DBMS_AQ.ON_COMMIT to ensure that enqueue is only done on commit,
   --so that all the data required by the async process has been committed to the database
   --prior to async execution.
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_WH_ADD, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_WH_ADD;
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_WH_ADD_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_WH_ADD_RETRY';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_WH_ADD;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

   L_retry_attempt_num   RMS_ASYNC_RETRY.RETRY_ATTEMPT_NUM%TYPE;

   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select 'x'
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_SEQ is
      select max(retry_attempt_num) + 1
        from rms_async_retry
       where rms_async_id = I_rms_async_id;

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   --Set async job status to in-progress
   L_table := 'RMS_ASYNC_STATUS';
   open C_LOCK;
   close C_LOCK;

   update rms_async_status
      set status = RMS_CONSTANTS.ASYNC_STATUS_RETRY,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where rms_async_id = I_rms_async_id;

   open C_SEQ;
   fetch C_SEQ into L_retry_attempt_num;
   close C_SEQ;

   insert into rms_async_retry (rms_async_id,
                                retry_attempt_num,
                                error_message,
                                retry_user_id,
                                retry_datetime,
                                last_update_id,
                                last_update_datetime)
                        values (I_rms_async_id,
                                L_retry_attempt_num,
                                null,
                                get_user,
                                sysdate,
                                get_user,
                                sysdate);

   L_message := rms_async_msg(I_rms_async_id);
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_WH_ADD_RETRY, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_WH_ADD_RETRY;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_WH_ADD(context  raw,
                        reginfo  sys.aq$_reg_info,
                        descr    sys.aq$_descriptor,
                        payload  raw,
                        payloadl number)
AS
   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.NOTIFY_WH_ADD';

   PROGRAM_ERROR         EXCEPTION;
   ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error

   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_rms_async_id        RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;

   L_message             RMS_ASYNC_MSG;
   L_dequeue_options     DBMS_AQ.DEQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);
   L_user                RMS_ASYNC_STATUS.CREATE_ID%TYPE;
   L_table               VARCHAR2(30);

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select create_id
        from rms_async_status
       where rms_async_id = L_rms_async_id
         for update nowait;

BEGIN

   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE;
   L_dequeue_options.wait := DBMS_AQ.NO_WAIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_WH_ADD, before dbms_aq.dequeue with msgid: ' || L_dequeue_options.msgid ||
              ', consumer_name: '|| L_dequeue_options.consumer_name ||
              ', queue_name: '|| descr.queue_name);

   DBMS_AQ.DEQUEUE(QUEUE_NAME => descr.queue_name,
                   DEQUEUE_OPTIONS => L_dequeue_options,
                   MESSAGE_PROPERTIES => L_message_properties,
                   PAYLOAD => L_message,
                   MSGID => L_message_handle);

   if L_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('DEQUEUE_ERROR', descr.queue_name, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rms_async_id := L_message.rms_async_id;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_WH_ADD, after dbms_aq.dequeue with rms_async_id: ' || L_rms_async_id);

   --Set a savepoint before the core processing to provide a rollback point in case of error
   DBMS_STANDARD.SAVEPOINT('ASYNC');

   L_table := 'RMS_ASYNC_STATUS';

   --Set async job status to in-progress
   open C_LOCK;
   fetch C_LOCK into L_user;
   close C_LOCK;

   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_IN_PROGRESS,
          last_update_id       = L_user,
          last_update_datetime = SYSDATE
    where rms_async_id = L_rms_async_id;

   /*Set the user id in this session's client info.            */
   /*This will be viewed by the filter_policy_sql further down */
   /*the processing via SYS_CONTEXT.                           */
   DBMS_APPLICATION_INFO.SET_CLIENT_INFO(L_user);

   --Call core processing logic for new item loc ranging
   logger.log('Call core service CORESVC_WH_ADD_SQL.ADD_WH with rms_async_id: ' || L_rms_async_id);

   if CORESVC_WH_ADD_SQL.ADD_WH(L_error_message,
                                L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- update the status table
   if RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS(L_error_message,
                                          L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
      --Only raise_application_error if something is fatally wrong and we cannot even log error.
      --Otherwise, do NOT raise_application_error so that error will be updated on the staging table with status.
      --Any business logic DMLs will be rolled back through savepoint.
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;

   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_rms_async_id,
                                            NULL);

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
END NOTIFY_WH_ADD;
-------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_STKLGR_INSERT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_STKLGR_INSERT';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_STKLGR_INSERT;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   if RMS_ASYNC_PROCESS_SQL.INSERT_ASYNC_STATUS_TBLS(O_error_message,
                                                     I_rms_async_id,
                                                     RMS_CONSTANTS.ASYNC_JOB_STKLGR_INSERT,
                                                     NULL,  --start_state
                                                     NULL) = FALSE then    --end_state
      return FALSE;
   end if;

   L_message := rms_async_msg(I_rms_async_id);
   --Set visibility to DBMS_AQ.ON_COMMIT to ensure that enqueue is only done on commit,
   --so that all the data required by the async process has been committed to the database
   --prior to async execution.
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_STKLGR_INSERT, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_STKLGR_INSERT;
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_STKLGR_INSERT_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_STKLGR_INSERT_RETRY';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_STKLGR_INSERT;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

   L_retry_attempt_num   RMS_ASYNC_RETRY.RETRY_ATTEMPT_NUM%TYPE;

   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select 'x'
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_SEQ is
      select max(retry_attempt_num) + 1
        from rms_async_retry
       where rms_async_id = I_rms_async_id;

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   --Set async job status to in-progress
   L_table := 'RMS_ASYNC_STATUS';
   open C_LOCK;
   close C_LOCK;

   update rms_async_status
      set status = RMS_CONSTANTS.ASYNC_STATUS_RETRY,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where rms_async_id = I_rms_async_id;

   open C_SEQ;
   fetch C_SEQ into L_retry_attempt_num;
   close C_SEQ;

   insert into rms_async_retry (rms_async_id,
                                retry_attempt_num,
                                error_message,
                                retry_user_id,
                                retry_datetime,
                                last_update_id,
                                last_update_datetime)
                        values (I_rms_async_id,
                                L_retry_attempt_num,
                                null,
                                get_user,
                                sysdate,
                                get_user,
                                sysdate);

   L_message := rms_async_msg(I_rms_async_id);
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_STKLGR_INSERT_RETRY, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_STKLGR_INSERT_RETRY;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_STKLGR_INSERT(context  raw,
                               reginfo  sys.aq$_reg_info,
                               descr    sys.aq$_descriptor,
                               payload  raw,
                               payloadl number)
AS
   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.NOTIFY_STKLGR_INSERT';

   PROGRAM_ERROR         EXCEPTION;
   ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error

   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_rms_async_id        RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;

   L_message             RMS_ASYNC_MSG;
   L_dequeue_options     DBMS_AQ.DEQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);
   L_user                RMS_ASYNC_STATUS.CREATE_ID%TYPE;
   L_table               VARCHAR2(30);

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select create_id
        from rms_async_status
       where rms_async_id = L_rms_async_id
         for update nowait;

BEGIN

   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE;
   L_dequeue_options.wait := DBMS_AQ.NO_WAIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_STKLGR_INSERT, before dbms_aq.dequeue with msgid: ' || L_dequeue_options.msgid ||
              ', consumer_name: '|| L_dequeue_options.consumer_name ||
              ', queue_name: '|| descr.queue_name);

   DBMS_AQ.DEQUEUE(QUEUE_NAME => descr.queue_name,
                   DEQUEUE_OPTIONS => L_dequeue_options,
                   MESSAGE_PROPERTIES => L_message_properties,
                   PAYLOAD => L_message,
                   MSGID => L_message_handle);

   if L_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('DEQUEUE_ERROR', descr.queue_name, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rms_async_id := L_message.rms_async_id;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_STKLGR_INSERT, after dbms_aq.dequeue with rms_async_id: ' || L_rms_async_id);

   --Set a savepoint before the core processing to provide a rollback point in case of error
   DBMS_STANDARD.SAVEPOINT('ASYNC');

   L_table := 'RMS_ASYNC_STATUS';

   --Set async job status to in-progress
   open C_LOCK;
   fetch C_LOCK into L_user;
   close C_LOCK;

   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_IN_PROGRESS,
          last_update_id       = L_user,
          last_update_datetime = SYSDATE
    where rms_async_id = L_rms_async_id;

   /*Set the user id in this session's client info.            */
   /*This will be viewed by the filter_policy_sql further down */
   /*the processing via SYS_CONTEXT.                           */
   DBMS_APPLICATION_INFO.SET_CLIENT_INFO(L_user);

   --Call core processing logic for new item loc ranging
   logger.log('Call core service CORESVC_STKLEDGR_INSERTS_SQL.PROCESS_STKLEDGR_INSERTS with rms_async_id: ' || L_rms_async_id);

   if CORESVC_STKLEDGR_INSERTS_SQL.PROCESS_STKLEDGR_INSERTS(L_error_message,
                                                            L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- update the status table
   if RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS(L_error_message,
                                          L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
      --Only raise_application_error if something is fatally wrong and we cannot even log error.
      --Otherwise, do NOT raise_application_error so that error will be updated on the staging table with status.
      --Any business logic DMLs will be rolled back through savepoint.
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;

   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_rms_async_id,
                                            NULL);

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
END NOTIFY_STKLGR_INSERT;
-------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_ITEM_INDUCT(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_rms_async_id        IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_ITEM_INDUCT';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_ITEM_INDUCT;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   if RMS_ASYNC_PROCESS_SQL.INSERT_ASYNC_STATUS_TBLS(O_error_message,
                                                     I_rms_async_id,
                                                     RMS_CONSTANTS.ASYNC_JOB_ITEM_INDUCT,
                                                     NULL,  --start_state
                                                     NULL) = FALSE then    --end_state
      return FALSE;
   end if;

   L_message := rms_async_msg(I_rms_async_id);
   --Set visibility to DBMS_AQ.ON_COMMIT to ensure that enqueue is only done on commit,
   --so that all the data required by the async process has been committed to the database
   --prior to async execution.
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_ITEM_INDUCT, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_ITEM_INDUCT;
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_ITEM_INDUCT_RETRY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_rms_async_id  IN     RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_ITEM_INDUCT_RETRY';

   L_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_owner               SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_queue_name          RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_ITEM_INDUCT;

   L_message             RMS_ASYNC_MSG;
   L_enqueue_options     DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);

   L_retry_attempt_num   RMS_ASYNC_RETRY.RETRY_ATTEMPT_NUM%TYPE;

   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select 'x'
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_SEQ is
      select max(retry_attempt_num) + 1
        from rms_async_retry
       where rms_async_id = I_rms_async_id;

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                             'I_rms_async_id', L_program);
       return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   --Set async job status to in-progress
   L_table := 'RMS_ASYNC_STATUS';
   open C_LOCK;
   close C_LOCK;

   update rms_async_status
      set status = RMS_CONSTANTS.ASYNC_STATUS_RETRY,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where rms_async_id = I_rms_async_id;

   open C_SEQ;
   fetch C_SEQ into L_retry_attempt_num;
   close C_SEQ;

   insert into rms_async_retry (rms_async_id,
                                retry_attempt_num,
                                error_message,
                                retry_user_id,
                                retry_datetime,
                                last_update_id,
                                last_update_datetime)
                        values (I_rms_async_id,
                                L_retry_attempt_num,
                                null,
                                get_user,
                                sysdate,
                                get_user,
                                sysdate);

   L_message := rms_async_msg(I_rms_async_id);
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_ITEM_INDUCT_RETRY, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(
      queue_name          => L_owner||'.'||L_queue_name,
      enqueue_options     => L_enqueue_options,
      message_properties  => L_message_properties,
      payload             => L_message,
      msgid               => L_message_handle);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ENQUEUE_ITEM_INDUCT_RETRY;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_ITEM_INDUCT(context  raw,
                             reginfo  sys.aq$_reg_info,
                             descr    sys.aq$_descriptor,
                             payload  raw,
                             payloadl number)
AS
   L_program  varchar2(61) := 'RMS_ASYNC_PROCESS_SQL.NOTIFY_ITEM_INDUCT';

   PROGRAM_ERROR         EXCEPTION;
   ERRNUM_PACKAGE_CALL   NUMBER(6) := -20020;  -- Define error for raise_application_error

   L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
   L_rms_async_id        RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;

   L_message             RMS_ASYNC_MSG;
   L_dequeue_options     DBMS_AQ.DEQUEUE_OPTIONS_T;
   L_message_properties  DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle      RAW(16);
   L_user                RMS_ASYNC_STATUS.CREATE_ID%TYPE;
   L_table               VARCHAR2(30);

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select create_id
        from rms_async_status
       where rms_async_id = L_rms_async_id
         for update nowait;

BEGIN
   if DATES_SQL.RESET_GLOBALS(L_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_ITEM_INDUCT, after reset_globals with vdate: '||GET_VDATE());

   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE;
   L_dequeue_options.wait := DBMS_AQ.NO_WAIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_ITEM_INDUCT, before dbms_aq.dequeue with msgid: ' || L_dequeue_options.msgid ||
              ', consumer_name: '|| L_dequeue_options.consumer_name ||
              ', queue_name: '|| descr.queue_name);

   DBMS_AQ.DEQUEUE(QUEUE_NAME => descr.queue_name,
                   DEQUEUE_OPTIONS => L_dequeue_options,
                   MESSAGE_PROPERTIES => L_message_properties,
                   PAYLOAD => L_message,
                   MSGID => L_message_handle);

   if L_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('DEQUEUE_ERROR', descr.queue_name, NULL, NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rms_async_id := L_message.rms_async_id;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_ITEM_INDUCT, after dbms_aq.dequeue with rms_async_id: ' || L_rms_async_id);

   --Set a savepoint before the core processing to provide a rollback point in case of error
   DBMS_STANDARD.SAVEPOINT('ASYNC');

   L_table := 'RMS_ASYNC_STATUS';

   --Set async job status to in-progress
   open C_LOCK;
   fetch C_LOCK into L_user;
   close C_LOCK;

   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_IN_PROGRESS,
          last_update_id       = L_user,
          last_update_datetime = SYSDATE
    where rms_async_id = L_rms_async_id;

   /*Set the user id in this session's client info.            */
   /*This will be viewed by the filter_policy_sql further down */
   /*the processing via SYS_CONTEXT.                           */
   DBMS_APPLICATION_INFO.SET_CLIENT_INFO(L_user);

   --Call core processing logic for item induction processing
   logger.log('Call core service ITEM_INDUCT_SQL.EXEC_ASYNC with rms_async_id: ' || L_rms_async_id);

   if ITEM_INDUCT_SQL.EXEC_ASYNC(L_error_message,
                                 L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   -- update the status table
   if RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS(L_error_message,
                                          L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when PROGRAM_ERROR then
      --Only raise_application_error if something is fatally wrong and we cannot even log error.
      --Otherwise, do NOT raise_application_error so that error will be updated on the staging table with status.
      --Any business logic DMLs will be rolled back through savepoint.
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;

   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_rms_async_id,
                                            NULL);

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
END NOTIFY_ITEM_INDUCT;
-------------------------------------------------------------------------------
FUNCTION ENQUEUE_PO_INDUCT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_rms_async_id    IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(60)                  := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_PO_INDUCT';
   L_queue_name           RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_PO_INDUCT;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_owner                SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_message              RMS_ASYNC_MSG;
   L_enqueue_options      DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties   DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle       RAW(16);

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rms_async_id',
                                            L_program);
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   if RMS_ASYNC_PROCESS_SQL.INSERT_ASYNC_STATUS_TBLS(O_error_message,
                                                     I_rms_async_id,
                                                     RMS_CONSTANTS.ASYNC_JOB_PO_INDUCT,
                                                     NULL,                 --start_state
                                                     NULL) = FALSE then    --end_state
      return FALSE;
   end if;

   L_message := RMS_ASYNC_MSG(I_rms_async_id);
   --Set visibility to DBMS_AQ.ON_COMMIT to ensure that enqueue is only done on commit,
   --so that all the data required by the async process has been committed to the database
   --prior to async execution.  
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_PO_INDUCT, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(queue_name         => L_owner||'.'||L_queue_name,
                   enqueue_options    => L_enqueue_options,
                   message_properties => L_message_properties,
                   payload            => L_message,
                   msgid              => L_message_handle);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ENQUEUE_PO_INDUCT;
------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_PO_INDUCT_RETRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_rms_async_id    IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(60)                  := 'RMS_ASYNC_PROCESS_SQL.ENQUEUE_PO_INDUCT_RETRY';
   L_queue_name           RMS_ASYNC_JOB.QUEUE_NAME%TYPE := RMS_CONSTANTS.ASYNC_QUEUE_PO_INDUCT;
   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_owner                SYSTEM_CONFIG_OPTIONS.TABLE_OWNER%TYPE;
   L_message              RMS_ASYNC_MSG;
   L_enqueue_options      DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties   DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle       RAW(16);
   L_retry_attempt_num    RMS_ASYNC_RETRY.RETRY_ATTEMPT_NUM%TYPE;
   L_table                VARCHAR2(30);
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select 'x'
        from rms_async_status
       where rms_async_id = I_rms_async_id
         for update nowait;

   cursor C_SEQ is
      select MAX(retry_attempt_num) + 1
        from rms_async_retry
       where rms_async_id = I_rms_async_id;

BEGIN

   -- Check required parameters
   if I_rms_async_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_rms_async_id',
                                            L_program);
      return FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_owner := L_system_options_row.table_owner;

   --Set async job status to in-progress
   L_table := 'RMS_ASYNC_STATUS';
   open C_LOCK;
   close C_LOCK;

   update rms_async_status
      set status = RMS_CONSTANTS.ASYNC_STATUS_RETRY,
          last_update_id = get_user,
          last_update_datetime = SYSDATE
    where rms_async_id = I_rms_async_id;

   open C_SEQ;
   fetch C_SEQ into L_retry_attempt_num;
   close C_SEQ;

   insert into rms_async_retry (rms_async_id,
                                retry_attempt_num,
                                error_message,
                                retry_user_id,
                                retry_datetime,
                                last_update_id,
                                last_update_datetime)
                        values (I_rms_async_id,
                                L_retry_attempt_num,
                                NULL,
                                get_user,
                                SYSDATE,
                                get_user,
                                SYSDATE);

   L_message := RMS_ASYNC_MSG(I_rms_async_id);
   L_enqueue_options.visibility := DBMS_AQ.ON_COMMIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.ENQUEUE_PO_INDUCT_RETRY, before dbms_aq.enqueue with rms_async_id: '||I_rms_async_id ||
              ', queue_name: ' || L_owner||'.'||L_queue_name);

   DBMS_AQ.ENQUEUE(queue_name         => L_owner||'.'||L_queue_name,
                   enqueue_options    => L_enqueue_options,
                   message_properties => L_message_properties,
                   payload            => L_message,
                   msgid              => L_message_handle);

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_rms_async_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ENQUEUE_PO_INDUCT_RETRY;
------------------------------------------------------------------------------------------------
PROCEDURE NOTIFY_PO_INDUCT(context    raw,
                           reginfo    sys.aq$_reg_info,
                           descr      sys.aq$_descriptor,
                           payload    raw,
                           payloadl   number)
AS

   L_program              VARCHAR2(60) := 'RMS_ASYNC_PROCESS_SQL.NOTIFY_PO_INDUCT';
   PROGRAM_ERROR          EXCEPTION;
   ERRNUM_PACKAGE_CALL    NUMBER(6)    := -20020;  -- Define error for raise_application_error
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_rms_async_id         RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;
   L_message              RMS_ASYNC_MSG;
   L_dequeue_options      DBMS_AQ.DEQUEUE_OPTIONS_T;
   L_message_properties   DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle       RAW(16);
   L_user                 RMS_ASYNC_STATUS.CREATE_ID%TYPE;
   L_table                VARCHAR2(30);
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK is
      select create_id
        from rms_async_status
       where rms_async_id = L_rms_async_id
         for update nowait;
   
         
BEGIN

   if DATES_SQL.RESET_GLOBALS(L_error_message) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_PO_INDUCT, after reset_globals with vdate: '||GET_VDATE());

   L_dequeue_options.msgid := descr.msg_id;
   L_dequeue_options.consumer_name := descr.consumer_name;
   L_dequeue_options.visibility := DBMS_AQ.IMMEDIATE;
   L_dequeue_options.wait := DBMS_AQ.NO_WAIT;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_PO_INDUCT, before dbms_aq.dequeue with msgid: ' || L_dequeue_options.msgid ||
              ', consumer_name: '|| L_dequeue_options.consumer_name ||
              ', queue_name: '|| descr.queue_name);

   DBMS_AQ.DEQUEUE(queue_name         => descr.queue_name,
                   dequeue_options    => L_dequeue_options,
                   message_properties => L_message_properties,
                   payload            => L_message,
                   msgid              => L_message_handle);

   if L_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('DEQUEUE_ERROR',
                                            descr.queue_name,
                                            NULL,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;

   L_rms_async_id := L_message.rms_async_id;

   logger.log('In RMS_ASYNC_PROCESS_SQL.NOTIFY_PO_INDUCT, after dbms_aq.dequeue with rms_async_id: ' || L_rms_async_id);

   --Set a savepoint before the core processing to provide a rollback point in case of error
   DBMS_STANDARD.SAVEPOINT('ASYNC');

   L_table := 'RMS_ASYNC_STATUS';
   --Set async job status to in-progress
   open C_LOCK;
   fetch C_LOCK into L_user;
   close C_LOCK;
   
   update rms_async_status
      set status               = RMS_CONSTANTS.ASYNC_STATUS_IN_PROGRESS,
          last_update_id       = L_user,
          last_update_datetime = SYSDATE
    where rms_async_id = L_rms_async_id;

   /*Set the user id in this session's client info.            */
   /*This will be viewed by the filter_policy_sql further down */
   /*the processing via SYS_CONTEXT.                           */
   DBMS_APPLICATION_INFO.SET_CLIENT_INFO(L_user);

   --Call core processing logic for PO induction processing
   logger.log('Call core service PO_INDUCT_SQL.EXEC_ASYNC with rms_async_id: ' || L_rms_async_id);

   if PO_INDUCT_SQL.EXEC_ASYNC(L_error_message,
                               L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;
   
   -- update the status table
   if RMS_ASYNC_PROCESS_SQL.WRITE_SUCCESS(L_error_message,
                                          L_rms_async_id) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when PROGRAM_ERROR then      
      --Only raise_application_error if something is fatally wrong and we cannot even log error.
      --Otherwise, do NOT raise_application_error so that error will be updated on the staging table with status.
      --Any business logic DMLs will be rolled back through savepoint.
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;
   when RECORD_LOCKED then
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_rms_async_id,
                                            NULL);
      ---
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;

   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      if RMS_ASYNC_PROCESS_SQL.WRITE_ERROR(L_error_message,
                                           L_error_message,
                                           L_rms_async_id) = FALSE then
         NULL;
      end if;                                            

END NOTIFY_PO_INDUCT;
-------------------------------------------------------------------------------
FUNCTION INSERT_RAF_NOTIFICATION(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_notification_id      IN       RAF_NOTIFICATION.RAF_NOTIFICATION_ID%TYPE,
                                 I_task_flow_url        IN       VARCHAR2,
                                 I_severity             IN       RAF_NOTIFICATION.SEVERITY%TYPE,
                                 I_application_code     IN       RAF_NOTIFICATION.APPLICATION_CODE%TYPE,
                                 I_launchable           IN       RAF_NOTIFICATION.LAUNCHABLE%TYPE,
                                 I_user                 IN       RAF_NOTIFICATION_RECIPIENTS.RECIPIENT_ID%TYPE,
                                 I_rms_async_id         IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE,
                                 I_process_id           IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE DEFAULT NULL,
                                 I_job_type             IN       RMS_ASYNC_STATUS.JOB_TYPE%TYPE DEFAULT NULL,
                                 I_notification_desc    IN       RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
 
   L_program              VARCHAR2(60)                               := 'RMS_ASYNC_PROCESS_SQL.INSERT_RAF_NOTIFICATION';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_notify_desc          RAF_NOTIFICATION_TYPE_TL.DESCRIPTION%TYPE;
   L_title                RAF_NOTIFICATION_TYPE_TL.NAME%TYPE;
   L_notify_context       VARCHAR2 (4000);
   L_tab                  CODE_DETAIL.CODE%TYPE;

   cursor C_GET_DESC_TL is
      select description
        from raf_notification_type_tl
       where notification_type = I_notification_id
         and language = userenv('LANG'); 

   cursor C_GET_TAB_TITLE is
      select code_desc
        from v_code_detail_tl
       where code_type = 'TABT'
         and code = L_tab; 
BEGIN

   open C_GET_DESC_TL;
   fetch C_GET_DESC_TL into L_title;
   close C_GET_DESC_TL;

   L_notify_context := 'title=' || L_title;
   
   if I_process_id is NOT NULL then
      if I_notification_desc IS NOT NULL then
         L_notify_desc := SQL_LIB.GET_MESSAGE_TEXT(I_notification_desc,
                                                   I_process_id);
      else
         L_notify_desc := L_title || ' Process Id: ' || I_process_id;
      end if;
   
      if I_task_flow_url is NOT NULL and I_launchable = 'Y' then
         L_tab := 'LOAD';
         open C_GET_TAB_TITLE;
         fetch C_GET_TAB_TITLE into L_title;
         close C_GET_TAB_TITLE;
         L_notify_context := 'title=' || L_title;
         L_notify_context := L_notify_context || '|url=' || I_task_flow_url || '|pmMode=EDIT' || '|pmProcessId=' || I_process_id;
      end if;
   
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_NEW_ITEM_LOC then
      if I_notification_desc IS NOT NULL then
         L_notify_desc := SQL_LIB.GET_MESSAGE_TEXT(I_notification_desc,
                                                   P_Item);
      else
         L_notify_desc := L_title || ' Item: ' || P_Item;
      end if;
      
      if I_task_flow_url is NOT NULL and I_launchable = 'Y' then
         L_tab := 'ITEM';
         open C_GET_TAB_TITLE;
         fetch C_GET_TAB_TITLE into L_title;
         close C_GET_TAB_TITLE;
         L_notify_context := 'title=' || L_title;
         L_notify_context := L_notify_context || '|url=' || I_task_flow_url || '|pmMode=EDIT' || '|pmItem=' || P_Item;
      end if;
   else   
      if I_notification_desc IS NOT NULL then
         L_notify_desc := SQL_LIB.GET_MESSAGE_TEXT(I_notification_desc,
                                                   I_rms_async_id);
      else
         L_notify_desc := L_title || ' Async Id: ' || I_rms_async_id;
      end if;
      
      if I_task_flow_url is NOT NULL and I_launchable = 'Y' then
         L_tab := 'ASYNC';
         open C_GET_TAB_TITLE;
         fetch C_GET_TAB_TITLE into L_title;
         close C_GET_TAB_TITLE;
         L_notify_context := 'title=' || L_title;
         L_notify_context := L_notify_context || '|url=' || I_task_flow_url || '|pmMode=EDIT' || '|pmRmsAsyncId=' || I_rms_async_id;
      end if;
   end if;

   if RAF_NOTIFICATION_TASK_PKG.CREATE_NOTIFY_TASK_AUTOTRAN(L_error_message,
                                                            RAF_NOTIFICATION_SEQ.NEXTVAL,
                                                            L_notify_context,
                                                            I_notification_id,
                                                            I_severity,
                                                            I_application_code,
                                                            L_notify_desc,
                                                            'U',
                                                            null,
                                                            to_timestamp(to_char(sysdate,'MM/DD/RRRR HH24:MI:SS'), 'MM/DD/RRRR HH24:MI:SS'),
                                                            I_user,
                                                            I_user,
                                                            I_launchable,
                                                            1) = 0 then
      raise PROGRAM_ERROR;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_RAF_NOTIFICATION;
-------------------------------------------------------------------------------
FUNCTION WRITE_RAF_NOTIFICATION(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_rms_async_id        IN       RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE)

RETURN BOOLEAN IS

   L_program                  VARCHAR2(61) := 'RMS_ASYNC_PROCESS_SQL.WRITE_RAF_NOTIFICATION';
   L_process_id               SVC_PROCESS_TRACKER.PROCESS_ID%TYPE;
   L_action_type              SVC_PROCESS_TRACKER.ACTION_TYPE%TYPE;
   L_job_type                 RMS_ASYNC_STATUS.JOB_TYPE%TYPE;
   L_notification_type        RAF_NOTIFICATION.NOTIFICATION_TYPE%TYPE;
   L_notification_desc        RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE;
   L_user_id                  RMS_ASYNC_STATUS.CREATE_ID%TYPE;
   L_async_status             RMS_ASYNC_STATUS.STATUS%TYPE;
   L_process_source           SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE;
   L_template_key             SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE;
   L_taskflow_url             RMS_ASYNC_JOB.TASKFLOW_URL%TYPE;
   L_launch_ind               RMS_ASYNC_JOB.NOTIFICATION_LAUNCH_IND%TYPE;

   cursor C_GET_ASYNC_INFO is
      select spt.process_id,
             spt.action_type,
             ras.job_type,
             ras.create_id,
             ras.status,
             spt.process_source,
             spt.template_key,
             raj.taskflow_url,
             raj.notification_launch_ind
        from svc_process_tracker spt,
             rms_async_status ras,
             rms_async_job raj
       where spt.rms_async_id(+) = ras.rms_async_id
         and ras.rms_async_id = I_rms_async_id
         and ras.job_type = raj.job_type;
         
         
BEGIN
   logger.log('Call RMS_ASYNC_PROCESS_SQL.WRITE_RAF_NOTIFICATION for rms_async_id: '||I_rms_async_id);

   open C_GET_ASYNC_INFO;
   fetch C_GET_ASYNC_INFO into L_process_id,
                               L_action_type,
                               L_job_type,
                               L_user_id,
                               L_async_status,
                               L_process_source,
                               L_template_key,
                               L_taskflow_url,
                               L_launch_ind;
   close C_GET_ASYNC_INFO;
         
   if GET_NOTIFICATION_INFO(O_error_message,
                            L_taskflow_url,
                            L_notification_type,       
                            L_notification_desc,                    
                            L_job_type,
                            L_action_type,
                            L_async_status,
                            L_process_source,                            
                            L_template_key,
                            L_process_id) = FALSE then
       raise PROGRAM_ERROR;
   end if;

   if L_job_type != RMS_CONSTANTS.ASYNC_JOB_STKLGR_INSERT and
      L_job_type != RMS_CONSTANTS.ASYNC_JOB_WH_ADD and
      L_job_type != RMS_CONSTANTS.ASYNC_JOB_STORE_ADD and 
      L_job_type != RMS_CONSTANTS.ASYNC_JOB_UPDATE_ITEM_LOC then
      if INSERT_RAF_NOTIFICATION(O_error_message,
                                 L_notification_type,
                                 L_taskflow_url,
                                 RMS_CONSTANTS.NOTIFICATION_SEV_LOW,
                                 RMS_CONSTANTS.ASYNC_APPLICATION_CODE,
                                 L_launch_ind,
                                 L_user_id,                              
                                 I_rms_async_id,
                                 L_process_id,
                                 L_job_type,
                                 L_notification_desc) = FALSE then
         raise PROGRAM_ERROR;
      end if;
   end if;
  
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;   
END WRITE_RAF_NOTIFICATION;                                
-------------------------------------------------------------------------------
FUNCTION GET_NOTIFICATION_INFO(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_taskflow_url        IN OUT   RMS_ASYNC_JOB.TASKFLOW_URL%TYPE,
                               O_notification_type    OUT      RAF_NOTIFICATION.NOTIFICATION_TYPE%TYPE,
                               O_notification_desc    OUT      RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE,
                               I_job_type             IN       RMS_ASYNC_STATUS.JOB_TYPE%TYPE,
                               I_action_type          IN       SVC_PROCESS_TRACKER.ACTION_TYPE%TYPE,
                               I_async_status         IN       RMS_ASYNC_STATUS.STATUS%TYPE,
                               I_process_source       IN       SVC_PROCESS_TRACKER.PROCESS_SOURCE%TYPE,
                               I_template_key         IN       SVC_PROCESS_TRACKER.TEMPLATE_KEY%TYPE,
                               I_process_id           IN       SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(61) := 'RMS_ASYNC_PROCESS_SQL.WRITE_RAF_NOTIFICATION';
   L_template_type      S9T_TEMPLATE.TEMPLATE_TYPE%TYPE;
   L_status             SVC_PROCESS_TRACKER.STATUS%TYPE;
   
   cursor C_GET_TEMPLATE_TYPE is
      select template_type
        from s9t_template
       where template_key = I_template_key;
   
   cursor C_GET_SVC_STATUS is
      select status
        from svc_process_tracker
       where process_id = I_process_id;

BEGIN
open  C_GET_TEMPLATE_TYPE;
fetch C_GET_TEMPLATE_TYPE into L_template_type;
close C_GET_TEMPLATE_TYPE;

open  C_GET_SVC_STATUS;
fetch C_GET_SVC_STATUS into L_status;
close C_GET_SVC_STATUS;


   if I_job_type = RMS_CONSTANTS.ASYNC_JOB_PO_INDUCT then
      if I_action_type = 'U' and L_status = 'PS' then
         if I_async_status = RMS_CONSTANTS.ASYNC_STATUS_SUCCESS then
            if I_process_source = RMS_CONSTANTS.ASYNC_SOURCE_SPREADSHEET then
               O_notification_type := RMS_CONSTANTS.ASYNC_PO_FILE_UPLD_COMPLETE;
               O_notification_desc := 'NOTIFY_PO_FILEUPLD_OK';
            else 
               O_notification_type := RMS_CONSTANTS.ASYNC_PO_INDUCT_UPLD_COMPLETE;
               O_notification_desc := 'NOTIFY_PO_STGUPLD_OK';
            end if;
         end if;
      elsif I_action_type = 'D' then
         if I_async_status = RMS_CONSTANTS.ASYNC_STATUS_SUCCESS then
            O_notification_type := RMS_CONSTANTS.ASYNC_PO_INDUCT_DWNLD_COMPLETE;
            O_notification_desc := 'NOTIFY_PO_DNLD_OK';
         elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_PO_INDUCT then
            O_notification_type := RMS_CONSTANTS.ASYNC_PO_INDUCT_DWNLD_FAILED;
            O_notification_desc := 'NOTIFY_PO_DNLD_FAIL';
         end if;
      end if;
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_ITEM_INDUCT then
      if L_template_type = 'IS9M' then
         if I_action_type = 'U' and L_status = 'PS' then
            if I_async_status = RMS_CONSTANTS.ASYNC_STATUS_SUCCESS then
               if I_process_source = RMS_CONSTANTS.ASYNC_SOURCE_SPREADSHEET then
                  O_notification_type := RMS_CONSTANTS.ASYNC_ITEM_FILE_UPLD_COMPLETE;
                  O_notification_desc := 'NOTIFY_ITM_FILEUPLD_OK';
               else
                  O_notification_type := RMS_CONSTANTS.ASYNC_ITEM_UPLD_COMPLETE;
                  O_notification_desc := 'NOTIFY_ITM_STGUPLD_OK';
               end if;
            end if;
         elsif I_action_type = 'D' then
            if I_async_status = RMS_CONSTANTS.ASYNC_STATUS_SUCCESS then
               O_notification_type := RMS_CONSTANTS.ASYNC_ITEM_DWNLD_COMPLETE;
               O_notification_desc := 'NOTIFY_ITM_DNLD_OK';
            else
               O_notification_type := RMS_CONSTANTS.ASYNC_ITEM_DWNLD_FAILED; 
               O_notification_desc := 'NOTIFY_ITM_DNLD_FAIL';           
            end if;
         end if;            
      elsif L_template_type = 'IS9C' then
         if I_action_type = 'U' and L_status = 'PS' then
            if I_async_status = RMS_CONSTANTS.ASYNC_STATUS_SUCCESS then
               if I_process_source = RMS_CONSTANTS.ASYNC_SOURCE_SPREADSHEET then
                  O_notification_type := RMS_CONSTANTS.ASYNC_COST_FILE_UPLD_COMPLETE;
                  O_notification_desc := 'NOTIFY_CC_FILEUPLD_OK';
               else
                  O_notification_type := RMS_CONSTANTS.ASYNC_COST_UPLD_COMPLETE;
                  O_notification_desc := 'NOTIFY_CC_STGUPLD_OK';
               end if;
            end if;
         elsif I_action_type = 'D' then
            if I_async_status = RMS_CONSTANTS.ASYNC_STATUS_SUCCESS then
               O_notification_type := RMS_CONSTANTS.ASYNC_COST_DNLD_COMPLETE;
               O_notification_desc := 'NOTIFY_CC_DNLD_OK';
            else
               O_notification_type := RMS_CONSTANTS.ASYNC_COST_DNLD_FAILED;   
               O_notification_desc := 'NOTIFY_CC_DNLD_FAIL';         
            end if;
         end if;                
      end if;    
   elsif I_job_type = RMS_CONSTANTS.ASYNC_JOB_NEW_ITEM_LOC then
      if I_async_status = RMS_CONSTANTS.ASYNC_STATUS_SUCCESS then  
         O_notification_type := RMS_CONSTANTS.ASYNC_ITEMLOC_CREATE_COMPLETE;
         O_notification_desc := 'NOTIFY_NEW_ITEMLOC_OK';
      else
         O_notification_type := RMS_CONSTANTS.ASYNC_ITEMLOC_CREATE_FAILED;
         O_notification_desc := 'NOTIFY_NEW_ITEMLOC_FAIL';
      end if;    
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;   
END GET_NOTIFICATION_INFO;     
-------------------------------------------------------------------------------
END RMS_ASYNC_PROCESS_SQL;
/
