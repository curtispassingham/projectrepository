SET DEFINE OFF

CREATE OR REPLACE PACKAGE BODY s9t_pkg
AS
PROCEDURE WRITE_S9T_ERROR(
    I_file_id IN s9t_errors.file_id%type,
    I_sheet   IN VARCHAR2,
    I_row_seq IN NUMBER,
    I_col     IN VARCHAR2,
    I_sqlcode IN NUMBER,
    I_sqlerrm IN VARCHAR2)
IS
BEGIN
  Lp_s9t_errors_tab.extend();
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).FILE_ID      := I_file_id;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_SEQ_NO := s9t_errors_seq.nextval;
  --Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).TEMPLATE_KEY := template_key;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).WKSHT_KEY  := I_sheet;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).COLUMN_KEY := I_col;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ROW_SEQ    := I_row_seq;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY  :=
  (
    CASE
    WHEN I_sqlcode IS NULL THEN
      I_sqlerrm
    ELSE
      'IIND-ORA-'||lpad(I_sqlcode,5,'0')
    END );
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_ID            := get_user;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).CREATE_DATETIME      := sysdate;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_ID       := get_user;
  Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).LAST_UPDATE_DATETIME := sysdate;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE FLUSH_S9T_ERRORS
IS
BEGIN
  forall i IN 1..Lp_s9t_errors_tab.count
  INSERT INTO s9t_errors VALUES Lp_s9t_errors_tab
    (i
    );
  Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
END FLUSH_S9T_ERRORS;
--------------------------------------------------------------------------------
PROCEDURE init_file
  (
    I_file_id   IN NUMBER,
    I_file_name IN VARCHAR2
  )
IS
  pragma autonomous_transaction;
BEGIN
  DELETE FROM s9t_folder WHERE file_id = I_file_id;
  INSERT
  INTO s9t_folder
    (
      file_id,
      file_name,
      s9t_file_obj
    )
    VALUES
    (
      I_file_id,
      I_file_name,
      s9t_file()
    );
  COMMIT;
END init_file;
--------------------------------------------------------------------------------
PROCEDURE init_obj
  (
    I_file_id IN NUMBER
  )
IS
  pragma autonomous_transaction;
BEGIN
  UPDATE s9t_folder SET s9t_file_obj = s9t_file() WHERE file_id = I_file_id;
  COMMIT;
END init_obj;
--------------------------------------------------------------------------------
PROCEDURE add_sheet(
    I_file_id    IN NUMBER,
    I_sheet_name IN VARCHAR2)
IS
  pragma autonomous_transaction;
  l_sheet_names s9t_cells;
BEGIN
  INSERT
  INTO TABLE
    (SELECT sf.s9t_file_obj.sheets
      FROM s9t_folder sf
      WHERE sf.file_id = I_file_id
    )
    VALUES
    (
      s9t_sheet(I_sheet_name,s9t_cells(),s9t_row_tab(),s9t_list_val_tab())
    );
  SELECT sf.s9t_file_obj.sheet_names
  INTO l_sheet_names
  FROM s9t_folder sf
  WHERE file_id = I_file_id;
  l_sheet_names.extend();
  l_sheet_names(l_sheet_names.count):=I_sheet_name;
  UPDATE s9t_folder sf
  SET sf.s9t_file_obj.sheet_names = l_sheet_names
  WHERE file_id                   = I_file_id;
  COMMIT;
END add_sheet;
--------------------------------------------------------------------------------
PROCEDURE set_headers(
    I_file_id    IN NUMBER,
    I_sheet_name IN VARCHAR2,
    I_headers s9t_cells )
IS
  pragma autonomous_transaction;
BEGIN
  UPDATE TABLE
    (SELECT sf.s9t_file_obj.sheets
    FROM s9t_folder sf
    WHERE sf.file_id = I_file_id
    )
  SET column_headers = I_headers
  WHERE sheet_name   = I_sheet_name;
  COMMIT;
END set_headers;
--------------------------------------------------------------------------------
PROCEDURE add_row(
    I_file_id    IN NUMBER,
    I_sheet_name IN VARCHAR2,
    I_row s9t_row )
IS
BEGIN
  add_rows(I_file_id,I_sheet_name,NEW s9t_row_tab(I_row));
END add_row;
PROCEDURE add_rows(
    I_file_id    IN NUMBER,
    I_sheet_name IN VARCHAR2,
    I_rows s9t_row_tab )
IS
  pragma autonomous_transaction;
BEGIN
  INSERT
  INTO TABLE
    (SELECT ss.s9t_rows
      FROM s9t_folder sf,
        TABLE(sf.s9t_file_obj.sheets) ss
      WHERE sf.file_id  = I_file_id
      AND ss.sheet_name = I_sheet_name
    )
  SELECT * FROM TABLE (I_rows) WHERE cells IS NOT NULL;
  COMMIT;
END add_rows;
--------------------------------------------------------------------------------
FUNCTION xml2row(
    I_xml          IN XMLTYPE,
    I_row_seq      IN NUMBER,
    I_column_count IN NUMBER)
  RETURN s9t_row
IS
  PRAGMA UDF;
  CURSOR c_cells
  IS
    SELECT x.cell,
      x.cn,
      x.cell_repeat_count
    FROM XMLTABLE                                                  --
      (                                                            --
      xmlnamespaces                                                --
      (                                                            --
      'urn:oasis:names:tc:opendocument:xmlns:table:1.0' AS "table" --
      ,'urn:oasis:names:tc:opendocument:xmlns:text:1.0' AS "text"  --
      ),                                                           --
      '/table:table-row/table:table-cell'                          --
      PASSING I_xml                                                --
      COLUMNS                                                      --
      cell               VARCHAR2(4000) PATH 'text:p[1]'                         --
      ,cell_repeat_count NUMBER PATH '@table:number-columns-repeated'            --
      ,cn FOR ORDINALITY                                                         --
      ) x
    ORDER BY x.cn;
  O_row s9t_row;
  l_cells s9t_cells           := NEW s9t_cells();
  l_not_null_cell_count NUMBER:=0;
  l_max_cols            NUMBER;
BEGIN
  -- For header row, restrict max number of columns to Lp_max_columns
  IF I_row_seq  = 1 THEN
    l_max_cols := Lp_max_columns;
  ELSE
    --for data rows, restrict max columns to number of
    -- column headers obtained from first row
    l_max_cols := NVL(I_column_count,0);
  END IF;
  FOR rec IN c_cells
  LOOP
    -- for header row, stop processing when null cell comes
    IF I_row_seq = 1 AND rec.cell IS NULL THEN
      EXIT;
    END IF;
    IF rec.cell_repeat_count IS NOT NULL AND I_row_seq > 1 -- skip repeat count processing for header row
      THEN
      FOR i IN 1..rec.cell_repeat_count
      LOOP
        IF l_cells.count <= l_max_cols THEN
          l_cells.extend();
          l_cells(l_cells.count)  :=rec.cell;
          IF rec.cell             IS NOT NULL THEN
            l_not_null_cell_count := l_not_null_cell_count+1;
          END IF;
        END IF;
      END LOOP;
    ELSE
      IF l_cells.count <= l_max_cols THEN
        l_cells.extend();
        l_cells(l_cells.count)  :=rec.cell;
        IF rec.cell             IS NOT NULL THEN
          l_not_null_cell_count := l_not_null_cell_count+1;
        END IF;
      END IF;
    END IF;
  END LOOP;
  IF l_not_null_cell_count > 0 THEN
    O_row                 := NEW s9t_row(l_cells,I_row_seq);
  END IF;
  RETURN O_row;
END xml2row;
--------------------------------------------------------------------------------
PROCEDURE ods2obj(
    I_file_id IN NUMBER )
IS
  CURSOR c_sheets
  IS
    SELECT x.*
    FROM s9t_folder t,
      XMLTABLE                                                     --
      (                                                            --
      xmlnamespaces                                                --
      (                                                            --
      'urn:oasis:names:tc:opendocument:xmlns:table:1.0' AS "table" --
      ),                                                           --
      '//table:table'                                              --
      PASSING t.content_xml                                        --
      COLUMNS                                                      --
      sheet_name VARCHAR2(4000) PATH '@table:name'                 --
      ) x
    WHERE t.file_id = I_file_id;
  CURSOR c1(I_sheet_name IN VARCHAR2)
  IS
    SELECT x.row_node,
      x.rn
    FROM s9t_folder t,
      XMLTABLE                                                     --
      (                                                            --
      xmlnamespaces                                                --
      (                                                            --
      'urn:oasis:names:tc:opendocument:xmlns:table:1.0' AS "table" --
      ,'urn:oasis:names:tc:opendocument:xmlns:text:1.0' AS "text"  --
      ),                                                           --
      '//table:table[@table:name=$sheet_name]/table:table-row'     --
      PASSING t.content_xml,I_sheet_name AS "sheet_name"           --
      COLUMNS                                                      --
      rn FOR ORDINALITY                                            --
      , row_node xmltype PATH '.'                                  --
      ) x
    WHERE t.file_id = I_file_id
    ORDER BY x.rn;
  row_tab s9t_row_tab;
type c1_tab_type
IS
  TABLE OF c1%rowtype;
  c1_tab c1_tab_type;
  c1_header_rec c1%rowtype;
  L_col_headers s9t_row;
  L_column_count NUMBER;
  l_clob CLOB;
  l_blob BLOB;
  l_file s9t_file := NEW s9t_file();
BEGIN
  SELECT ods_blob,
    file_id,
    file_name,
    template_key,
    user_lang
  INTO l_blob,
    l_file.file_id,
    l_file.file_name,
    l_file.template_key,
    l_file.user_lang
  FROM s9t_folder
  WHERE file_id = I_file_id;
  if l_blob is not null then
  dbms_lob.createtemporary(l_clob,false);
  zip.unzip(l_blob,l_clob);
  l_blob := NULL;
  UPDATE s9t_folder
  SET s9t_file_obj=l_file,
    content_xml   = xmltype(l_clob)
  WHERE file_id   = I_file_id;
  COMMIT;
  DBMS_LOB.FREETEMPORARY(l_clob);
  else
  UPDATE s9t_folder
  SET s9t_file_obj=l_file
  WHERE file_id   = I_file_id;
  commit;
  end if;
  FOR rec IN c_sheets
  LOOP
    OPEN c1(rec.sheet_name);
    -- First fetch to get column headers
    FETCH c1 INTO c1_header_rec;
    L_col_headers  := xml2row(c1_header_rec.row_node,c1_header_rec.rn,NULL);
    if L_col_headers is not null and L_col_headers.cells is not null and L_col_headers.cells.count >0 then
    s9t_pkg.add_sheet(I_file_id,rec.sheet_name);
    L_column_count := L_col_headers.cells.count;
    s9t_pkg.set_headers(I_file_id,rec.sheet_name,L_col_headers.cells);
    LOOP
      FETCH c1 bulk collect INTO c1_tab limit 1000;
      IF c1_tab.count >0 THEN
        row_tab      := NEW s9t_row_tab();
        FOR i IN 1..c1_tab.count
        LOOP
          row_tab.extend();
          row_tab(row_tab.count):=xml2row(c1_tab(i).row_node,c1_tab(i).rn,L_column_count);
        END LOOP;
        s9t_pkg.add_rows(I_file_id,rec.sheet_name,row_tab);
        row_tab := NULL;
      END IF;
      EXIT
    WHEN c1%notfound;
    END LOOP;
    end if;
    CLOSE c1;
  END LOOP;
  UPDATE s9t_folder SET content_xml = NULL WHERE file_id = I_file_id;
  COMMIT;
END ods2obj;
--------------------------------------------------------------------------------
FUNCTION get_sheet_names(
    I_file_id IN NUMBER)
  RETURN names_map_typ
IS
  l_sheet_names s9t_cells;
  l_template_key s9t_folder.template_key%type;
  l_lang s9t_folder.user_lang%type;
  O_names s9t_names_tab:=NEW s9t_names_tab();
  names_map names_map_typ;
BEGIN
  SELECT sf.s9t_file_obj.sheet_names,
    template_key,
    user_lang
  INTO l_sheet_names,
    l_template_key,
    l_lang
  FROM s9t_folder sf
  WHERE sf.file_id = I_file_id;
  FOR i IN 1..l_sheet_names.count()
  LOOP
    O_names.extend();
    O_names(O_names.count())   := NEW s9t_name(l_sheet_names(i),i);
    names_map(l_sheet_names(i)):=i;
  END LOOP;
  FOR rec IN
  (SELECT template_key,
    wksht_key,
    wksht_name,
    n.t_pos,
    n.t_name
  FROM v_s9t_tmpl_wksht_def ws,
    TABLE(o_names) n
  WHERE template_key = l_template_key
  AND wksht_name     = n.t_name
  )
  LOOP
    names_map.delete(rec.t_name);
    names_map(rec.wksht_key):=rec.t_pos;
  END LOOP;
  RETURN names_map;
END get_sheet_names;
--------------------------------------------------------------------------------
FUNCTION get_col_names(
    I_file_id    IN NUMBER,
    I_sheet_name IN VARCHAR2)
  RETURN names_map_typ
IS
  l_cols s9t_cells;
  l_template_key s9t_folder.template_key%type;
  l_lang s9t_folder.user_lang%type;
  O_names s9t_names_tab:=NEW s9t_names_tab();
  names_map names_map_typ;
  l_sheet_trans VARCHAR2(255);
BEGIN
  -- get template key and user language
  SELECT template_key,
    user_lang
  INTO l_template_key,
    l_lang
  FROM s9t_folder
  WHERE file_id = I_file_id;
  BEGIN
    -- get translated sheet name for the input sheet key
    SELECT wd.wksht_name
    INTO l_sheet_trans
    FROM v_s9t_tmpl_wksht_def wd
    WHERE wd.template_key = l_template_key
    AND wd.wksht_key      =I_sheet_name;
    --get column headers
    SELECT ss.column_headers
    INTO l_cols
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) ss
    WHERE sf.file_id  = I_file_id
    AND ss.sheet_name = l_sheet_trans;
    FOR i IN 1..l_cols.count()
    LOOP
      O_names.extend();
      O_names(O_names.count()) := NEW s9t_name(l_cols(i),i);
      names_map(l_cols(i))     :=i;
    END LOOP;
    FOR rec IN
    (SELECT column_key,
      n.t_pos,
      n.t_name
    FROM v_s9t_tmpl_cols_def cs,
      TABLE(O_names) n
    WHERE template_key = l_template_key
    AND wksht_key      = I_sheet_name
    AND n.t_name       =column_name
    )
    LOOP
      names_map.delete(rec.t_name);
      names_map(rec.column_key):=rec.t_pos;
    END LOOP;
  EXCEPTION
  WHEN no_data_found THEN
    NULL;
  WHEN OTHERS THEN
    raise;
  END;
  RETURN names_map;
END get_col_names;
--------------------------------------------------------------------------------
FUNCTION trim_row(
    I_var     IN s9t_cells,
    I_col_tab IN num_tab)
  RETURN s9t_cells
IS
  var1 s9t_cells;
BEGIN
  var1 := NEW s9t_cells();
  FOR i IN I_col_tab.first..I_col_tab.last
  LOOP
    IF I_var.exists(I_col_tab(i)) THEN
      var1.extend();
      var1(var1.count()):=I_var(I_col_tab(i));
    END IF;
  END LOOP;
  RETURN var1;
END trim_row;
--------------------------------------------------------------------------------
PROCEDURE delete_sheets(
    I_file_id IN NUMBER,
    I_sheet_names s9t_cells)
IS
  l_old_sheet_names s9t_cells;
  l_new_sheet_names s9t_cells;
  l_found BOOLEAN;
BEGIN
  SELECT sf.s9t_file_obj.sheet_names
  INTO l_old_sheet_names
  FROM s9t_folder sf
  WHERE sf.file_id   = I_file_id;
  l_new_sheet_names := NEW s9t_cells();
  FOR i IN 1..l_old_sheet_names.count()
  LOOP
    l_found := false;
    FOR j IN 1..I_sheet_names.count()
    LOOP
      IF l_old_sheet_names(i) = I_sheet_names(j) THEN
        l_found              := true;
        EXIT;
      END IF;
    END LOOP;
    IF NOT l_found THEN
      l_new_sheet_names.extend();
      l_new_sheet_names(l_new_sheet_names.count()):=l_old_sheet_names(i);
    END IF;
  END LOOP;
  -- Update new sheet names in the file
  UPDATE s9t_folder sf
  SET sf.s9t_file_obj.sheet_names = l_new_sheet_names
  WHERE sf.file_id                = I_file_id;
  DELETE
  FROM TABLE
    ( SELECT sf.s9t_file_obj.sheets FROM s9t_folder sf WHERE file_id = I_file_id
    )
  WHERE sheet_name IN
    (SELECT column_value FROM TABLE(I_sheet_names)
    );
END delete_sheets;
--------------------------------------------------------------------------------
PROCEDURE delete_columns(
    I_file_id IN NUMBER,
    I_sheet_name VARCHAR2,
    I_columns s9t_cells )
IS
  l_old_col_headers s9t_cells;
  l_new_col_headers s9t_cells;
  l_del_cols_idxs num_tab;
  l_found BOOLEAN;
BEGIN
  l_new_col_headers := NEW s9t_cells();
  l_del_cols_idxs   := NEW num_tab();
  SELECT sh.column_headers
  INTO l_old_col_headers
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) sh
  WHERE sf.file_id  = I_file_id
  AND sh.sheet_name = I_sheet_name;
  FOR i IN 1..l_old_col_headers.count()
  LOOP
    l_found := false;
    FOR j IN 1..I_columns.count()
    LOOP
      IF l_old_col_headers(i) = I_columns(j) THEN
        l_found              := true;
        EXIT;
      END IF;
    END LOOP;
    IF NOT l_found THEN
      l_new_col_headers.extend();
      l_new_col_headers(l_new_col_headers.count()):=l_old_col_headers(i);
      l_del_cols_idxs.extend();
      l_del_cols_idxs(l_del_cols_idxs.count()):=i;
    END IF;
  END LOOP;
  -- Update column header to new column header i.e. old column header without
  -- deleted columns.
  UPDATE TABLE
    (SELECT sf.s9t_file_obj.sheets sh
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) sh
    WHERE sf.file_id  = I_file_id
    AND sh.sheet_name = I_sheet_name
    ) a
  SET a.column_headers = l_new_col_headers
  WHERE sheet_name     = I_sheet_name;
  --delete columns from each row by replacing the row with a new row with all
  --columns except the deleted columns
  UPDATE TABLE
    (SELECT s.s9t_rows
    FROM s9t_folder f,
      TABLE(f.s9t_file_obj.sheets) s
    WHERE f.file_id  = I_file_id
    AND s.sheet_name = I_sheet_name
    ) a
  SET cells = s9t_pkg.trim_row(cells,l_del_cols_idxs);
  DELETE
  FROM TABLE
    (SELECT sh.s9t_list_vals
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) sh
    WHERE file_id     = I_file_id
    AND sh.sheet_name = I_sheet_name
    )
  WHERE column_name IN
    (SELECT column_value FROM TABLE(I_columns)
    );
END delete_columns;
--------------------------------------------------------------------------------
PROCEDURE apply_template(
    I_file_id  IN NUMBER,
    I_template IN VARCHAR2)
IS
  pragma autonomous_transaction;
  l_lang s9t_folder.user_lang%type;
  l_del_sheets s9t_cells;
  l_del_columns s9t_cells;
BEGIN
  -- Get non-required sheet names.
WITH sheets AS
  (SELECT sf.template_key,
    sheet_name
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) sh
  WHERE sf.file_id = I_file_id
  )
SELECT sheet_name bulk collect
INTO l_del_sheets
FROM sheets sh,
  v_s9t_tmpl_wksht_def wd
WHERE I_template       = wd.template_key(+)
AND sh.sheet_name      = wd.wksht_name(+)
AND NVL(mandatory,'N') ='N';
-- Delete non-required sheet names from the file.
s9t_pkg.delete_sheets(I_file_id,l_del_sheets);
-- Loop through each remaining sheets
--
FOR rec IN
(SELECT wd.wksht_key,
  wd.wksht_name wksht_name
FROM v_s9t_tmpl_wksht_def wd
WHERE wd.mandatory  = 'Y'
AND wd.template_key = I_template
)
LOOP
  -- for each sheet, get the non-required columns.
WITH cols AS
  (SELECT column_value AS column_name
  FROM TABLE
    (SELECT sh.column_headers
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) sh
    WHERE sf.file_id  = I_file_id
    AND sh.sheet_name = rec.wksht_name
    )
  ),
  tmpl_cols AS
  (SELECT column_name,
    mandatory
  FROM v_s9t_tmpl_cols_def
  WHERE template_key = I_template
  AND wksht_key      = rec.wksht_key
  )
SELECT c.column_name bulk collect
INTO l_del_columns
FROM cols c,
  tmpl_cols tc
WHERE c.column_name   = tc.column_name (+)
AND NVL(mandatory,'N')='N';
-- delete non required columns.
IF l_del_columns IS NOT NULL AND l_del_columns.count() >0 THEN
  s9t_pkg.delete_columns(I_file_id,rec.wksht_name,l_del_columns);
END IF;
END LOOP;
COMMIT;
END apply_template;
--------------------------------------------------------------------------------
FUNCTION ADD_LIST_VALIDATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_file_id         IN       NUMBER,
                             I_sheet           IN       VARCHAR2,
                             I_column          IN       VARCHAR2,
                             I_code_type       IN       CODE_HEAD.CODE_TYPE%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(75) := 'S9T_PKG.ADD_LIST_VALIDATION';
   L_code_desc   CODE_DETAIL.CODE_DESC%TYPE;
   L_count       NUMBER := 0;
   L_lov         s9t_cells := new s9t_cells();
BEGIN
   FOR c1 in (select code_detail.code,
                     code_detail.code_type
                from code_detail
               where code_detail.code_type in (substr(I_code_type,1,instr(I_code_type,',',1)-1),substr(I_code_type,instr(I_code_type,',',1)+1)))
                                               /*in order to accomodate multiple comma separated code types*/
   LOOP
      L_count := L_count + 1;
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    c1.code_type,
                                    c1.code,
                                    L_code_desc) = FALSE then
         return FALSE;
      end if;
      L_lov.extend;
      L_lov(L_count) := L_code_desc;
   END LOOP;

   ADD_LIST_VALIDATION(I_file_id,
                       I_sheet,
                       I_column,
                       L_lov);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END ADD_LIST_VALIDATION;
PROCEDURE ADD_LIST_VALIDATION(I_file_id   IN   NUMBER,
                              I_sheet     IN   VARCHAR2,
                              I_column    IN   VARCHAR2,
                              I_lov            s9t_cells)
IS
   COL_NOT_FOUND EXCEPTION;
   PRAGMA EXCEPTION_INIT(COL_NOT_FOUND, -22908);
   PRAGMA autonomous_transaction;
BEGIN
   if I_lov is NOT NULL and I_lov.count() > 0 then
      insert into TABLE (select sh.s9t_list_vals
                           from s9t_folder sf,
                                TABLE(sf.s9t_file_obj.sheets) sh
                          where sf.file_id  = I_file_id
                            and sh.sheet_name = I_sheet)
                 values (I_column,
                         I_lov);
   end if;
   COMMIT;
EXCEPTION

WHEN COL_NOT_FOUND THEN
  RAISE_APPLICATION_ERROR(-20999, 'Template Setup error: Worksheet '||I_sheet||' not found while trying to populate LOV '||I_column);
END ADD_LIST_VALIDATION;
--------------------------------------------------------------------------------
PROCEDURE translate_to_user_lang
  (
    I_file_id IN NUMBER
  )
IS
  pragma autonomous_transaction;
Type t_names
IS
  TABLE OF VARCHAR2
  (
    255
  )
  INDEX BY VARCHAR2
  (
    255
  )
  ;
  l_wksht t_names;
  l_cols t_names;
  l_sheet_names s9t_cells;
  l_template_key s9t_folder.template_key%type;
  l_lang s9t_folder.user_lang%type;
  l_column_headers s9t_cells;
  l_s9t_list_vals s9t_list_val_tab;
BEGIN
  SELECT sf.template_key,
    sf.user_lang,
    sf.s9t_file_obj.sheet_names
  INTO l_template_key,
    l_lang,
    l_sheet_names
  FROM s9t_folder sf
  WHERE file_id = I_file_id;
  FOR rec IN
  (SELECT template_key,
    wksht_key,
    wksht_name
  FROM v_s9t_tmpl_wksht_def wd
  WHERE template_key = l_template_key
  )
  LOOP
    l_wksht(rec.wksht_key):= rec.wksht_name;
  END LOOP;
  FOR i IN 1..l_sheet_names.count()
  LOOP
    l_cols.delete;
    FOR rec IN
    (SELECT column_key,
      column_name
    FROM v_s9t_tmpl_cols_def
    WHERE template_key = l_template_key
    AND wksht_key      = l_sheet_names(i)
    )
    LOOP
      l_cols(rec.column_key):= rec.column_name;
    END LOOP;
    SELECT sh.column_headers,
      sh.s9t_list_vals
    INTO l_column_headers,
      l_s9t_list_vals
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) sh
    WHERE file_id     = I_file_id
    AND sh.sheet_name = l_sheet_names(i);
    FOR j IN 1..l_column_headers.count()
    LOOP
      BEGIN
        FOR k IN 1..l_s9t_list_vals.count()
        LOOP
          IF l_column_headers(j)            = l_s9t_list_vals(k).column_name THEN
            l_s9t_list_vals(k).column_name := l_cols(l_column_headers(j));
          END IF;
        END LOOP;
        l_column_headers(j) := l_cols(l_column_headers(j));
      EXCEPTION
      WHEN no_data_found THEN
        NULL;
      WHEN OTHERS THEN
        raise;
      END;
    END LOOP;
    UPDATE TABLE
      ( SELECT sf.s9t_file_obj.sheets FROM s9t_folder sf WHERE file_id = I_file_id
      )
    SET column_headers = l_column_headers,
      s9t_list_vals    = l_s9t_list_vals
    WHERE sheet_name   = l_sheet_names(i);
    BEGIN
      UPDATE TABLE
        (SELECT sf.s9t_file_obj.sheets FROM s9t_folder sf WHERE file_id = I_file_id
        )
      SET sheet_name    = l_wksht(l_sheet_names(i))
      WHERE sheet_name  = l_sheet_names(i);
      l_sheet_names(i) := l_wksht(l_sheet_names(i));
    EXCEPTION
    WHEN no_data_found THEN
      NULL;
    WHEN OTHERS THEN
      raise;
    END;
  END LOOP;
  UPDATE s9t_folder sf
  SET sf.s9t_file_obj.sheet_names = l_sheet_names
  WHERE file_id                   = I_file_id;
  COMMIT;
END translate_to_user_lang;
--------------------------------------------------------------------------------
FUNCTION CODE2DESC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_templ_category   IN       VARCHAR2,
                   I_file             IN OUT   s9t_file,
                   I_reverse          IN       BOOLEAN DEFAULT FALSE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(75) := 'S9T_PKG.CODE2DESC';

   TYPE s9t_cell_decode_typ IS TABLE OF VARCHAR2(255) INDEX BY VARCHAR2(255);
   TYPE s9t_cell_decode_tab_typ IS TABLE OF s9t_cell_decode_typ INDEX BY BINARY_INTEGER;

   L_s9t_cell_decode       s9t_cell_decode_typ;
   L_s9t_cell_decode_tab   s9t_cell_decode_tab_typ;
   L_col_index             NUMBER;
   lv                      NUMBER;
   L_lang                  LANG.LANG%TYPE;
   L_code                  s9t_cells := new s9t_cells();
   L_desc                  s9t_cells := new s9t_cells();
   L_code_desc             CODE_DETAIL.CODE_DESC%TYPE;
   L_count                 NUMBER := 0;

BEGIN
   L_lang := GET_USER_LANG;
   FOR i in 1..I_file.sheets.count()
   LOOP
      L_s9t_cell_decode_tab.delete;
      FOR rec in
         (select sw.wksht_name,
                 sc.column_name,
                 code
            from s9t_list_vals sv,
                 v_s9t_tmpl_wksht_def sw,
                 v_s9t_tmpl_cols_def sc
           where sv.template_category = I_templ_category
             and sv.sheet_name        = sw.wksht_key
             and sw.template_key      = I_file.template_key
             and sw.template_key      = sc.template_key
             and sw.wksht_key         = sc.wksht_key
             and sc.column_key        = sv.column_name
             and sw.wksht_name        = I_file.sheets(i).sheet_name
         )
      LOOP
         L_s9t_cell_decode.delete;
         L_code  := new s9t_cells();
         L_desc  := new s9t_cells();
         L_count := 0;
         FOR crec in (select code_detail.code,
                             code_detail.code_type
                        from code_detail
                       where code_detail.code_type in (substr(rec.code,1,instr(rec.code,',',1)-1),substr(rec.code,instr(rec.code,',',1)+1)))
                                                       /*in order to accomodate multiple comma separated code types*/
         LOOP
            L_count := L_count + 1;
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          crec.code_type,  --code_type
                                          crec.code, --code
                                          L_code_desc) = FALSE then
               return FALSE;
            end if;
            L_code.extend;
            L_code(L_count) := crec.code;
            L_desc.extend;
            L_desc(L_count) := L_code_desc;
         END LOOP;

         FOR i in 1..L_code.count() LOOP
            if NOT I_reverse then
              l_s9t_cell_decode(L_code(i)):= L_desc(i);
            else
              l_s9t_cell_decode(L_desc(i)):= L_code(i);
            end if;
         END LOOP;
         L_col_index := I_file.sheets(i).get_col_index(rec.column_name);
         if l_col_index is NOT NULL then
            L_s9t_cell_decode_tab(l_col_index):= L_s9t_cell_decode;
         end if;
      END LOOP;

      FOR ri in 1..I_file.sheets(i).s9t_rows.count()
      LOOP
         lv := L_s9t_cell_decode_tab.first;
         WHILE lv is NOT NULL LOOP
            if L_s9t_cell_decode_tab(lv).exists(I_file.sheets(i).s9t_rows(ri).cells(lv)) then
               I_file.sheets(i).s9t_rows(ri).cells(lv) := L_s9t_cell_decode_tab(lv)(I_file.sheets(i).s9t_rows(ri).cells(lv));
            end if;
            lv := L_s9t_cell_decode_tab.next(lv);
         END LOOP;
      END LOOP;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CODE2DESC;
--------------------------------------------------------------------------------
FUNCTION POPULATE_LISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_file_id          IN       NUMBER,
                        I_templ_category   IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program     VARCHAR2(75) := 'S9T_PKG.POPULATE_LISTS';

BEGIN
   FOR rec in
      (select sheet_name,
              column_name,
              code
         from s9t_list_vals
        where template_category = I_templ_category
      )
   LOOP
      if S9T_PKG.ADD_LIST_VALIDATION(O_error_message,
                                     I_file_id,
                                     rec.sheet_name,
                                     rec.column_name,
                                     rec.code) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END POPULATE_LISTS;
--------------------------------------------------------------------------------
FUNCTION POPULATE_LISTS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_file_id          IN       NUMBER,
                        I_templ_category   IN       VARCHAR2,
                        I_template_key     IN       S9T_TEMPLATE.TEMPLATE_KEY%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(75) := 'S9T_PKG.POPULATE_LISTS';
   L_wksht_key   S9T_TMPL_WKSHT_DEF.WKSHT_KEY%TYPE;

   TYPE L_wksht_key_tbl IS TABLE OF S9T_TMPL_WKSHT_DEF.WKSHT_KEY%TYPE;

   L_wksht_key_tab     L_wksht_key_tbl := NEW L_wksht_key_tbl();

   cursor GET_SHEET_NAME is
      select WKSHT_KEY
        from S9T_TMPL_WKSHT_DEF
       where TEMPLATE_KEY = I_template_key;


BEGIN
   open GET_SHEET_NAME;
   fetch GET_SHEET_NAME bulk collect into L_wksht_key_tab;
   close GET_SHEET_NAME;

   FOR i IN 1..L_wksht_key_tab.COUNT() LOOP
      FOR rec in
         (select sheet_name,
                 column_name,
                 code
            from s9t_list_vals
           where template_category = I_templ_category
             and sheet_name = L_wksht_key_tab(i)
         )
      LOOP
         if S9T_PKG.ADD_LIST_VALIDATION(O_error_message,
                                        I_file_id,
                                        rec.sheet_name,
                                        rec.column_name,
                                        rec.code) = FALSE then
            return FALSE;
         end if;
      END LOOP;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END POPULATE_LISTS;
--------------------------------------------------------------------------------
FUNCTION sheet_trans(
    I_template_key IN s9t_template.template_key%type,
    I_lang         IN lang.lang%type)
  RETURN trans_map_typ
IS
  O_map trans_map_typ;
BEGIN
  FOR rec IN
  (SELECT wd.wksht_key,
    wd.wksht_name
  FROM v_s9t_tmpl_wksht_def wd
  WHERE wd.template_key = I_template_key
  )
  LOOP
    O_map(rec.wksht_key) := rec.wksht_name;
  END LOOP;
  RETURN O_map;
END sheet_trans;
--------------------------------------------------------------------------------
FUNCTION check_file_name(
    O_error_message IN OUT rtk_errors.rtk_text%TYPE,
    I_file_name     IN s9t_folder.file_name%type,
    O_found OUT BOOLEAN)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 's9t_pkg.check_file_name';
  L_exist   CHAR;
  CURSOR c_get_file_name
  IS
    SELECT 'Y' FROM s9t_folder WHERE UPPER(file_name) = UPPER(I_file_name);
BEGIN
  OPEN c_get_file_name;
  FETCH c_get_file_name INTO L_exist;
  IF c_get_file_name%notfound THEN
    O_found := false;
  ELSE
    O_found := true;
  END IF;
  CLOSE c_get_file_name;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END check_file_name;
--------------------------------------------------------------------------------
FUNCTION check_template(
    O_error_message IN OUT rtk_errors.rtk_text%TYPE,
    I_template_key  IN s9t_template.template_key%type,
    O_found OUT BOOLEAN)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(75) := 's9t_pkg.check_template';
  L_exist   CHAR;
  CURSOR c_validate_template
  IS
    SELECT 'Y' FROM s9t_template WHERE template_key = I_template_key;
BEGIN
  OPEN c_validate_template;
  FETCH c_validate_template INTO L_exist;
  IF c_validate_template%notfound THEN
    O_found := false;
  ELSE
    O_found := true;
  END IF;
  CLOSE c_validate_template;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, SQLCODE);
  RETURN FALSE;
END check_template;
--------------------------------------------------------------------------------
FUNCTION validate_template(
    I_file_id IN NUMBER)
  RETURN BOOLEAN
IS
  l_error_sheet_name s9t_tmpl_wksht_def.wksht_name%type;
  CURSOR c_error_sheet_name
  IS
    SELECT wksht_name
    FROM v_s9t_tmpl_wksht_def
    WHERE template_key = 'ITEM_IND_ERRORS';
BEGIN
  OPEN c_error_sheet_name;
  FETCH c_error_sheet_name INTO l_error_sheet_name;
  CLOSE c_error_sheet_name;
  INSERT
  INTO s9t_errors
    (
      file_id,
      error_seq_no,
      template_key,
      wksht_key,
      column_key,
      error_key
    )
  SELECT file_id,
    s9t_errors_seq.nextval AS error_seq_no,
    template_key,
    wksht_key,
    column_key,
    error_key
  FROM
    (SELECT sf.file_id,
      sf.s9t_file_obj.template_key AS template_key,
      wd.wksht_key,
      NULL                AS column_key,
      'S9T_MISSING_SHEET' AS error_key
    FROM s9t_folder sf,
      v_s9t_tmpl_wksht_comp wd
    WHERE sf.file_id    = I_file_id
    AND wd.template_key = sf.s9t_file_obj.template_key
    AND wd.mandatory    = 'Y'
	and wd.lang = sf.user_lang
    AND NOT EXISTS
      (SELECT 1
      FROM TABLE(sf.s9t_file_obj.sheet_names)
      WHERE column_value = wd.wksht_name
      )
    /* Commented out to suppress extra sheet error until warning feature is implemented
    UNION ALL
    SELECT sf.file_id,
      sf.s9t_file_obj.template_key AS template_key,
      sh.sheet_name,
      NULL,
      'S9T_EXTRA_SHEET' AS error_key
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) sh
    WHERE sf.file_id = I_file_id
    AND NOT EXISTS
      (SELECT 1
      FROM v_s9t_tmpl_wksht_def wd
      WHERE template_key = sf.template_key
      AND mandatory      = 'Y'
      AND wd.wksht_name  = sh.sheet_name
      )
    AND sh.sheet_name <> l_error_sheet_name*/
    UNION ALL
    SELECT sf.file_id,
      sf.s9t_file_obj.template_key AS template_key,
      wd.wksht_key,
      cd.column_key,
      'S9T_MISSING_COLUMN' AS error_key
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) ss,
      v_s9t_tmpl_wksht_comp wd,
      v_s9t_tmpl_cols_comp cd
    WHERE sf.file_id    = I_file_id
    AND wd.template_key = sf.s9t_file_obj.template_key
    AND wd.wksht_name   = ss.sheet_name
    AND cd.template_key = wd.template_key
    AND cd.wksht_key    = wd.wksht_key
    AND cd.mandatory    = 'Y'
	and wd.lang = sf.user_lang
	and cd.lang = sf.user_lang
    AND NOT EXISTS
      (SELECT 1
      FROM TABLE(ss.column_headers) ch
      WHERE cd.column_name = ch.column_value
      )
     UNION ALL
     SELECT sf.file_id,
      sf.s9t_file_obj.template_key AS template_key,
      wd.wksht_key,
      ss.column_value,
      'S9T_EXTRA_COLUMN' AS error_key
    FROM s9t_folder sf,
      TABLE(sf.s9t_file_obj.sheets) sh,
      TABLE(sh.column_headers) ss,
      v_s9t_tmpl_wksht_comp wd
    WHERE sf.file_id = I_file_id
    and wd.wksht_name  = sh.sheet_name
	and wd.lang = sf.user_lang
    and wd.template_key = sf.s9t_file_obj.template_key
    AND NOT EXISTS
      (SELECT 1
      FROM v_s9t_tmpl_cols_comp  cd
      WHERE cd.template_key = sf.template_key
      AND  cd.mandatory      = 'Y'
      and cd.wksht_key   = wd.wksht_key
      and cd.column_name = ss.column_value
	  and cd.lang = sf.user_lang));
  IF sql%rowcount > 0 THEN
    RETURN false;
  ELSE
    RETURN true;
  END IF;
END validate_template;
--------------------------------------------------------------------------------
FUNCTION basename(
    I_file_path IN VARCHAR2)
  RETURN VARCHAR2
IS
  l_file_sep_pos NUMBER:=0;
  O_basename     VARCHAR2(4000);
BEGIN
  l_file_sep_pos   := instr(I_file_path,'\',-1);
  IF l_file_sep_pos = 0 THEN
    l_file_sep_pos := instr(I_file_path,'/',-1);
  END IF;
  O_basename := SUBSTR(I_file_path,l_file_sep_pos+1,LENGTH(I_file_path));
  IF upper(O_basename) NOT LIKE upper('%.ods') THEN
    O_basename := O_basename||'.ods';
  END IF;
  RETURN O_basename;
END basename;
--------------------------------------------------------------------------------
FUNCTION BASENAME(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_basename OUT S9T_FOLDER.FILE_NAME%TYPE,
    I_file_path IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_file_sep_pos NUMBER:=0;
  L_basename     VARCHAR2(4000);
  L_program      VARCHAR2(100) := 'CORESVC_ITEM.PROCESS_ISU';
BEGIN
  L_file_sep_pos   := INSTR(I_file_path,'\',-1);
  IF L_file_sep_pos = 0 THEN
    L_file_sep_pos := INSTR(I_file_path,'/',-1);
  END IF;
  L_basename := SUBSTR(I_file_path,l_file_sep_pos+1,LENGTH(I_file_path));
  IF UPPER(L_basename) NOT LIKE UPPER('%.ods') THEN
    L_basename := L_basename||'.ods';
  END IF;
  IF LENGTH(L_basename) > 255 THEN
    O_error_message    := SQL_LIB.CREATE_MSG('ERR_FILE_NAME_MAX', NULL, NULL, NULL);
    RETURN FALSE;
  END IF;
  O_basename := L_basename;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END BASENAME;
--------------------------------------------------------------------------------
PROCEDURE load_template_config
IS
  l_file s9t_file;
BEGIN
  l_file := NEW s9t_file('template_config.ods',true);
  l_file.save;
  DELETE FROM S9T_TMPL_COLS_DEF_TL WHERE TEMPLATE_KEY NOT IN (SELECT TEMPLATE_KEY FROM S9T_TEMPLATE WHERE TEMPLATE_CATEGORY ='DOCS9T');
  DELETE FROM S9T_TMPL_COLS_DEF WHERE TEMPLATE_KEY NOT IN (SELECT TEMPLATE_KEY FROM S9T_TEMPLATE WHERE TEMPLATE_CATEGORY ='DOCS9T');
  DELETE FROM S9T_TMPL_WKSHT_DEF_TL WHERE TEMPLATE_KEY NOT IN (SELECT TEMPLATE_KEY FROM S9T_TEMPLATE WHERE TEMPLATE_CATEGORY ='DOCS9T');
  DELETE FROM S9T_TMPL_WKSHT_DEF WHERE TEMPLATE_KEY NOT IN (SELECT TEMPLATE_KEY FROM S9T_TEMPLATE WHERE TEMPLATE_CATEGORY ='DOCS9T');
  DELETE S9T_TEMPLATE_TL WHERE TEMPLATE_KEY NOT IN (SELECT TEMPLATE_KEY FROM S9T_TEMPLATE WHERE TEMPLATE_CATEGORY ='DOCS9T');
  DELETE FROM s9t_template WHERE TEMPLATE_KEY NOT IN (SELECT TEMPLATE_KEY FROM S9T_TEMPLATE WHERE TEMPLATE_CATEGORY ='DOCS9T');
  INSERT
  INTO s9t_template
    (
      TEMPLATE_KEY,
      TEMPLATE_NAME,
      TEMPLATE_DESC,
      FILE_ID,
      TEMPLATE_TYPE,
      TEMPLATE_CATEGORY
    )
  SELECT a.get_cell(1) AS TEMPLATE_KEY ,
    a.get_cell(2)      AS TEMPLATE_NAME ,
    a.get_cell(3)      AS TEMPLATE_DESC ,
    a.get_cell(4)      AS FILE_ID,
    a.get_cell(5)      AS TEMPLATE_TYPE,
    a.get_cell(6)      AS TEMPLATE_CATEGORY
  FROM TABLE(s9t_file('template_config.ods').get_sheet('S9T_TEMPLATE').s9t_rows) a;
  INSERT
  INTO S9T_TEMPLATE_TL
    (
      TEMPLATE_KEY,
      LANG,
      TEMPLATE_NAME,
      TEMPLATE_DESC
    )
  SELECT a.get_cell(1) AS TEMPLATE_KEY,
    a.get_cell(2)      AS LANG,
    a.get_cell(3)      AS TEMPLATE_NAME,
    a.get_cell(4)      AS TEMPLATE_DESC
  FROM TABLE(s9t_file('template_config.ods').get_sheet('S9T_TEMPLATE_TL').s9t_rows) a;
  INSERT
  INTO S9T_TMPL_WKSHT_DEF
    (
      TEMPLATE_KEY,
      WKSHT_KEY,
      WKSHT_NAME,
      MANDATORY,
      SEQ_NO
    )
  SELECT a.get_cell(1)   AS TEMPLATE_KEY,
    a.get_cell(2)        AS WKSHT_KEY,
    a.get_cell(3)        AS WKSHT_NAME,
    a.get_cell(4)        AS MANDATORY,
    NVL(a.get_cell(5),0) AS SEQ_NO
  FROM TABLE(s9t_file('template_config.ods').get_sheet('S9T_TMPL_WKSHT_DEF').s9t_rows) a;
  INSERT
  INTO S9T_TMPL_WKSHT_DEF_TL
    (
      TEMPLATE_KEY,
      WKSHT_KEY,
      LANG,
      WKSHT_NAME
    )
  SELECT a.get_cell(1) AS TEMPLATE_KEY,
    a.get_cell(2)      AS WKSHT_KEY,
    a.get_cell(3)      AS LANG,
    a.get_cell(4)      AS WKSHT_NAME
  FROM TABLE(s9t_file('template_config.ods').get_sheet('S9T_TMPL_WKSHT_DEF_TL').s9t_rows) a;
  INSERT
  INTO S9T_TMPL_COLS_DEF
    (
      TEMPLATE_KEY,
      WKSHT_KEY,
      COLUMN_KEY,
      COLUMN_NAME,
      MANDATORY,
      DEFAULT_VALUE,
      REQUIRED_VISUAL_IND
    )
  SELECT a.get_cell(1) AS TEMPLATE_KEY,
    a.get_cell(2)      AS WKSHT_KEY,
    a.get_cell(3)      AS COLUMN_KEY,
    a.get_cell(4)      AS COLUMN_NAME,
    a.get_cell(5)      AS MANDATORY,
    a.get_cell(6)      AS DEFAULT_VALUE,
    nvl(a.get_cell(7),'N')      AS REQUIRED_VISUAL_IND
  FROM TABLE(s9t_file('template_config.ods').get_sheet('S9T_TMPL_COLS_DEF').s9t_rows) a;
  INSERT
  INTO S9T_TMPL_COLS_DEF_TL
    (
      TEMPLATE_KEY,
      WKSHT_KEY,
      COLUMN_KEY,
      LANG,
      COLUMN_NAME
    )
  SELECT a.get_cell(1) AS TEMPLATE_KEY,
    a.get_cell(2)      AS WKSHT_KEY,
    a.get_cell(3)      AS COLUMN_KEY,
    a.get_cell(4)      AS LANG,
    a.get_cell(5)      AS COLUMN_NAME
  FROM TABLE(s9t_file('template_config.ods').get_sheet('S9T_TMPL_COLS_DEF_TL').s9t_rows) a;
  INSERT
  INTO svc_tmpl_api_map
    (
      TEMPLATE_KEY,
      DNLD_API,
      UPLD_API,
      PROCESS_API
    )
  SELECT a.get_cell(1) AS TEMPLATE_KEY,
    a.get_cell(2)      AS DNLD_API,
    a.get_cell(3)      AS UPLD_API,
    a.get_cell(4)      AS PROCESS_API
  FROM TABLE(s9t_file('template_config.ods').get_sheet('SVC_TMPL_API_MAP').s9t_rows) a;
END load_template_config;
--------------------------------------------------------------------------------
FUNCTION calc_col_head(
    I_num IN NUMBER)
  RETURN VARCHAR2
IS
  first_idx   NUMBER;
  second_idx  NUMBER;
  first_char  CHAR;
  second_char CHAR;
  out_str     VARCHAR2(2);
BEGIN
  first_idx    := ceil(I_num /26)-1;
  second_idx   := I_num      -(26*first_idx);
  IF first_idx  = 0 THEN
    first_char := NULL;
  ELSE
    first_char := chr(64+first_idx);
  END IF;
  IF second_idx  = 0 THEN
    second_char := chr(64+26);
  ELSE
    second_char := chr(64+second_idx);
  END IF;
  out_str := first_char||second_char;
  RETURN out_str;
END calc_col_head;
--------------------------------------------------------------------------------
FUNCTION get_clob(
    I_file IN S9t_file)
  RETURN CLOB
IS
  l_wbk1 VARCHAR2(32000):=
  '<?xml version="1.0" encoding="UTF-8" ?>
<office:document-content xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:dom="http://www.w3.org/2001/xml-events" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:rpt="http://openoffice.org/2005/report" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:grddl="http://www.w3.org/2003/g/data-view#" xmlns:field="urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0" office:version="1.2" grddl:transformation="http://docs.oasis-open.org/office/1.2/xslt/odf2rdf.xsl">
<office:scripts />
<office:font-face-decls>
<style:font-face style:name="Arial" svg:font-family="Arial" style:font-family-generic="swiss" style:font-pitch="variable" />
<style:font-face style:name="Arial Unicode MS" svg:font-family="Arial Unicode MS" style:font-family-generic="system" style:font-pitch="variable" />
<style:font-face style:name="FZSongTi" svg:font-family="FZSongTi" style:font-family-generic="system" style:font-pitch="variable" />
<style:font-face style:name="Mangal" svg:font-family="Mangal" style:font-family-generic="system" style:font-pitch="variable" />
<style:font-face style:name="Tahoma" svg:font-family="Tahoma" style:font-family-generic="system" style:font-pitch="variable" />
</office:font-face-decls>
<office:automatic-styles>
<style:style style:name="co1" style:family="table-column">
<style:table-column-properties fo:break-before="auto" style:column-width="0.8925in" />
</style:style>
<style:style style:name="ro1" style:family="table-row">
<style:table-row-properties style:row-height="0.1783in" fo:break-before="auto" style:use-optimal-row-height="true" />
</style:style>
<style:style style:name="ta1" style:family="table" style:master-page-name="Default">
<style:table-properties table:display="true" style:writing-mode="lr-tb" />
</style:style>
<style:style style:name="ce1" style:family="table-cell" style:parent-style-name="Default">
<style:table-cell-properties fo:background-color="#e6e6e6" fo:border="0.0008in solid #000000" />
<style:text-properties fo:font-weight="bold" style:font-weight-asian="bold" style:font-weight-complex="bold" />
</style:style>
<style:style style:name="ce2" style:family="table-cell" style:parent-style-name="Default">
<style:table-cell-properties fo:border="0.0008in solid #000000" />
</style:style>
<style:style style:name="reqcell" style:family="table-cell" style:parent-style-name="Default">
<style:table-cell-properties fo:background-color="REQUIRED_VISUAL_COLOR_CODE" fo:border="0.0008in solid #000000" />
</style:style>
<style:style style:name="ta_extref" style:family="table">
<style:table-properties table:display="false" />
</style:style>
</office:automatic-styles>
<office:body>
<office:spreadsheet> '
  ;
  l_sheet1     VARCHAR2(4000) := '
<table:table table:name="Sheet1" table:style-name="ta1" table:print="false">  ';
  l_row1       VARCHAR2(4000) := '
<table:table-row table:style-name="ro1">
';
  l_head_cell1 VARCHAR2(4000) :='
<table:table-cell table:style-name="ce1" office:value-type="string">
<text:p>';
  l_head_cell2 VARCHAR2(4000) :='</text:p></table:table-cell>';
  l_row2       VARCHAR2(4000) :='</table:table-row>';
  l_cell1      VARCHAR2(4000) :='
<table:table-cell office:value-type="string"><text:p>';
  l_cell2      VARCHAR2(4000) :='</text:p></table:table-cell>';
  l_cell_lov1a VARCHAR2(4000) :='
<table:table-cell table:content-validation-name="';
  l_cell_lov1b VARCHAR2(4000) := '" office:value-type="string"><text:p>';
  l_sheet2     VARCHAR2(4000) := '
</table:table>';
  l_wbk2       VARCHAR2(4000) := '
</office:spreadsheet>
</office:body>
</office:document-content>';
  l_clob CLOB;
  l_str           VARCHAR2(32000);
  is_content_vals BOOLEAN;
  l_lov           VARCHAR2(4000);
type list_val_arr_typ
IS
  TABLE OF VARCHAR2(255) INDEX BY binary_integer;
  l_list_val_arr list_val_arr_typ;
  lv_col_index  NUMBER;
  l_sheet_index NUMBER;

  Cursor c_s9t_config is
  select drop_down_depth,REQUIRED_VISUAL_COLOR_CODE from s9t_config;
  l_s9t_config c_s9t_config%rowtype;

  Cursor c_col_defs(I_wksht_key VARCHAR2) is
  select column_name,required_visual_ind
  from s9t_tmpl_cols_def
  where template_key = I_file.template_key
  and wksht_key = I_wksht_key
  and required_visual_ind = 'Y';

  Type typ_visual_ind is table of varchar2(1) index by varchar2(255);
  l_visual_ind_arr typ_visual_ind;

  Cursor c_get_wksht_key(I_sheet varchar2) is
  select wksht_key from v_s9t_tmpl_wksht_def
  where template_key = I_file.template_key
  and wksht_name = I_sheet;

  l_wksht_key s9t_tmpl_wksht_def.wksht_key%type;
  l_drop_down_depth number(10,0);
  L_error_tmpl_key s9t_template.template_key%type:='ITEM_IND_ERRORS';
BEGIN
   dbms_lob.createtemporary(l_clob,false);
  --fetch s9t_config
  Open c_s9t_config;
  fetch c_s9t_config into l_s9t_config;
  close c_s9t_config;
  if l_s9t_config.REQUIRED_VISUAL_COLOR_CODE is not null then
  l_wbk1 := replace(l_wbk1,'REQUIRED_VISUAL_COLOR_CODE',l_s9t_config.REQUIRED_VISUAL_COLOR_CODE);
  else
  l_wbk1 := replace(l_wbk1,'REQUIRED_VISUAL_COLOR_CODE','#e6e6e6');
  end if;
  l_drop_down_depth := nvl(l_s9t_config.drop_down_depth,1048574);
  dbms_lob.writeappend(l_clob,length(l_wbk1),l_wbk1);
  l_list_val_arr.delete;
  is_content_vals := false;
  -- Write content validations tag
  FOR rec IN
  (SELECT sh.sheet_name,
    rownum AS sheet_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) sh,
    v_s9t_tmpl_wksht_def wd
  WHERE sh.sheet_name     = wd.wksht_name
  AND sf.file_id          = I_file.file_id
  AND wd.template_key in (I_file.template_key,L_error_tmpl_key)
  ORDER BY seq_no,
    sh.sheet_name
  )
  LOOP
    l_sheet_index := I_file.get_sheet_index(rec.sheet_name);
    FOR lv IN 1..I_file.sheets(l_sheet_index).s9t_list_vals.count()
    LOOP
      IF NOT is_content_vals THEN
        l_str := '<table:content-validations>';
        dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
        is_content_vals := true;
      END IF;
      l_str := '<table:content-validation table:name="s'||l_sheet_index||'val'||lv||'" table:condition="of:cell-content-is-in-list(';
      SELECT listagg('"'
        ||column_value
        ||'"',';') within GROUP (
      ORDER BY 1)
      INTO l_lov
      FROM TABLE(I_file.sheets(l_sheet_index).s9t_list_vals(lv).list_of_values);
      l_str := l_str ||DBMS_XMLGEN.CONVERT(l_lov,0)||')" table:allow-empty-cell="true" table:display-list="unsorted" table:base-cell-address="&apos;' ||I_file.sheets(l_sheet_index).sheet_name ||'&apos;.' ||s9t_pkg.calc_col_head(I_file.sheets(l_sheet_index).get_col_index(I_file.sheets(l_sheet_index).s9t_list_vals(lv).column_name))||'1048576"><table:error-message table:message-type="stop" table:display="true" />  </table:content-validation>';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    END LOOP;
  END LOOP;
  IF is_content_vals THEN
    l_str:='</table:content-validations>';
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
  END IF;
  --finished writing content validations tag
  --Write data for each sheet
  FOR rec IN
  (SELECT sh.sheet_name,
    rownum AS sheet_seq
  FROM s9t_folder sf,
    TABLE(sf.s9t_file_obj.sheets) sh,
    v_s9t_tmpl_wksht_def wd
  WHERE sh.sheet_name     = wd.wksht_name
  AND sf.file_id          = I_file.file_id
  AND wd.template_key in (I_file.template_key,L_error_tmpl_key)
  ORDER BY seq_no,
    sh.sheet_name
  )
  LOOP
    l_sheet_index := I_file.get_sheet_index(rec.sheet_name);
    -- Initialize list validation array for thi sheet
    l_list_val_arr.delete;
    FOR lv IN 1..I_file.sheets(l_sheet_index).s9t_list_vals.count()
    LOOP
      lv_col_index                  := I_file.sheets(l_sheet_index).get_col_index(I_file.sheets(l_sheet_index).s9t_list_vals(lv).column_name);
      IF lv_col_index               IS NOT NULL THEN
        l_list_val_arr(lv_col_index):= 's'||l_sheet_index||'val'||lv;
      END IF;
    END LOOP;
    l_str := REPLACE(l_sheet1,'Sheet1',I_file.sheets(l_sheet_index).sheet_name);
    l_str := REPLACE(l_str,'COLUMN_COUNT',I_file.sheets(l_sheet_index).column_headers.count());
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    l_visual_ind_arr.delete;
    Open c_get_wksht_key(I_file.sheets(l_sheet_index).sheet_name);
    fetch c_get_wksht_key into l_wksht_key;
    close c_get_wksht_key;
    for vir in c_col_defs(l_wksht_key)
    loop
    l_visual_ind_arr(vir.column_name) := vir.required_visual_ind;
    end loop;
    FOR ch IN 1..I_file.sheets(l_sheet_index).column_headers.count()
    LOOP
       if l_visual_ind_arr.exists(I_file.sheets(l_sheet_index).column_headers(ch)) then
       l_str := '<table:table-column table:style-name="co1" table:default-cell-style-name="reqcell"/>';
       dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
       else
       l_str := '<table:table-column table:style-name="co1" table:default-cell-style-name="Default"/>';
       dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
       end if;
    END LOOP;
    l_str := l_row1;
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    FOR h IN 1..I_file.sheets(l_sheet_index).column_headers.count()
    LOOP
      l_str := l_head_cell1||I_file.sheets(l_sheet_index).column_headers(h)||l_head_cell2;
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    END LOOP;
    l_str := l_row2;
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    FOR r IN 1..I_file.sheets(l_sheet_index).s9t_rows.count()
    LOOP
      l_str := l_row1;
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      FOR d IN 1..I_file.sheets(l_sheet_index).s9t_rows(r).cells.count()
      LOOP
        IF l_list_val_arr.exists(d) and l_drop_down_depth >= r THEN
          l_str:=l_cell_lov1a||l_list_val_arr(d)||l_cell_lov1b||DBMS_XMLGEN.CONVERT(I_file.sheets(l_sheet_index).s9t_rows(r).cells(d),0)||l_cell2;
        ELSE
          l_str:=l_cell1||DBMS_XMLGEN.CONVERT(I_file.sheets(l_sheet_index).s9t_rows(r).cells(d),0)||l_cell2;
        END IF;
        dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      END LOOP;
      l_str := l_row2;
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    END LOOP;
    IF l_list_val_arr.count() > 0 and l_drop_down_depth > I_file.sheets(l_sheet_index).s9t_rows.count() THEN
      l_str                  :='<table:table-row table:style-name="ro1" table:number-rows-repeated="'||(l_drop_down_depth-I_file.sheets(l_sheet_index).s9t_rows.count())||'">';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      FOR col IN 1..I_file.sheets(l_sheet_index).column_headers.count()
      LOOP
        l_str := '<table:table-cell ';
        IF l_list_val_arr.exists(col) THEN
          l_str := l_str || 'table:content-validation-name="'||l_list_val_arr(col)||'" /> ';
        ELSE
          l_str := l_str || '/> ';
        END IF;
        dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      END LOOP;
      l_str :='</table:table-row>';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      l_str :='<table:table-row table:style-name="ro1" >';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      FOR col IN 1..I_file.sheets(l_sheet_index).column_headers.count()
      LOOP
        l_str := '<table:table-cell ';
        IF l_list_val_arr.exists(col) THEN
          l_str := l_str || 'table:content-validation-name="'||l_list_val_arr(col)||'" /> ';
        ELSE
          l_str := l_str || '/> ';
        END IF;
        dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
      END LOOP;
      l_str :='</table:table-row>';
      dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
    END IF;
    l_str := l_sheet2;
    dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
  END LOOP;
  l_str := l_wbk2;
  dbms_lob.writeappend(l_clob,LENGTH(l_str),l_str);
  RETURN l_clob;
END get_clob;
--------------------------------------------------------------------------------
PROCEDURE UPDATE_ODS(
    I_file IN S9t_file)
IS
  pragma autonomous_transaction;
  l_clob CLOB;
  l_blob BLOB;
  l_lblob BLOB;
  O_blob BLOB;
  l_tmpl_blob BLOB;
BEGIN
  dbms_lob.createtemporary(l_clob,false);
  l_clob := s9t_pkg.get_clob(I_file);
  dbms_lob.createtemporary(l_lblob,false);
  dbms_lob.createtemporary(O_blob,false);
  SELECT ODS_BLOB
  INTO l_tmpl_blob
  FROM s9t_folder
  WHERE file_name = 'ODS_SYSTEM_TEMPLATE_FOR_OUTPUT_FILES.ods';
  zip.zip(l_tmpl_blob,l_clob,l_lblob,O_blob);
  UPDATE s9t_folder
  SET ods_blob  =O_blob
  WHERE file_id = I_file.file_id;
  COMMIT;
END UPDATE_ODS;
--------------------------------------------------------------------------------
PROCEDURE SAVE_OBJ(
    I_file IN OUT NOCOPY s9t_file)
IS
  pragma autonomous_transaction;
BEGIN
  IF I_file.file_id IS NULL THEN
    I_file.file_id  := s9t_folder_seq.nextval();
  END IF;
  Merge INTO s9t_folder sf USING
  ( SELECT I_file.file_id AS file_id FROM dual
  ) sq ON (sq.file_id = sf.file_id)
WHEN Matched THEN
  UPDATE SET s9t_file_obj = I_file WHEN NOT matched THEN
  INSERT
    (
      file_id,
      file_name,
      template_key,
      user_lang,
      s9t_file_obj
    )
    VALUES
    (
      I_file.file_id,
      I_file.file_name,
      I_file.template_key,
      I_file.user_lang,
      I_file
    );
  COMMIT;
END SAVE_OBJ;
--------------------------------------------------------------------------------
FUNCTION get_obj
  (
    I_file_ID IN NUMBER,
    I_load    IN BOOLEAN DEFAULT FALSE
  )
  RETURN s9t_file
IS
  l_clob CLOB;
  myParser DBMS_XMLPARSER.parser;
  indomdoc DBMS_XMLDOM.DOMDocument;
  worksheets DBMS_XMLDOM.DOMNODELIST;
  worksheet_count NUMBER;
  worksheet_atts DBMS_XMLDOM.DOMNAMEDNODEMAP;
  worksheet_name VARCHAR2(255);
  sheet DBMS_XMLDOM.DOMNODE;
  data_tag_list DBMS_XMLDOM.DOMNODELIST;
  --wtable DBMS_XMLDOM.DOMNODE;
  drows DBMS_XMLDOM.DOMNODELIST;
  drow DBMS_XMLDOM.DOMNODE;
  cells DBMS_XMLDOM.DOMNODELIST;
  cell DBMS_XMLDOM.DOMNODE;
  data_tag DBMS_XMLDOM.DOMNODE;
  data_val VARCHAR2(4000);
  --l_sheet workbook_sheet;
  l_row s9t_cells;
  l_nodelist dbms_xmldom.DOMNodeList;
  buf               VARCHAR2(32000);
  l_count           NUMBER := 0;
  cell_repeat_count NUMBER;
  row_repeat_count  NUMBER;
  l_blob BLOB;
  l_column_count NUMBER;
  O_file s9t_file:=NEW s9t_file();
BEGIN
  IF I_load = FALSE THEN
    SELECT s9t_file_obj INTO O_file FROM s9t_folder WHERE file_ID = I_file_ID;
    RETURN O_file;
  END IF;
  SELECT ods_blob,
    file_name,
    template_key,
    user_lang
  INTO l_blob,
    O_file.file_name,
    O_file.template_key,
    O_file.user_lang
  FROM s9t_folder
  WHERE file_id = I_file_id;
  dbms_lob.createtemporary(l_clob,false);
  zip.unzip(l_blob,l_clob);
  O_file.file_id     := I_file_id;
  O_file.sheet_names := s9t_cells();
  O_file.sheets      := s9t_sheet_tab();
  /*  SELECT file_clob
  INTO l_clob
  FROM s9t_folder
  WHERE file_name = I_file_name
  And create_id = USER;*/
  myParser := DBMS_XMLPARSER.newParser;
  DBMS_XMLPARSER.parseClob(myParser, l_clob);
  indomdoc        := DBMS_XMLPARSER.getDocument(myParser);
  worksheets      := dbms_xmldom.getelementsbytagname(indomdoc,'table');
  worksheet_count := dbms_xmldom.getlength(worksheets);
  FOR i IN 1..worksheet_count
  LOOP
    sheet          := dbms_xmldom.item(Worksheets,i-1);
    worksheet_atts := dbms_xmldom.getattributes(sheet);
    worksheet_name := DBMS_XMLDOM.GETATTRIBUTE( DBMS_XMLDOM.MAKEELEMENT(sheet),'name' );
    O_file.add_sheet(worksheet_name);
    drows          := dbms_xmldom.getchildrenbytagname(DBMS_XMLDOM.MAKEELEMENT(sheet),'table-row');
    l_column_count := 0;
    FOR j IN 1..dbms_xmldom.getlength(drows)
    LOOP
      l_row               := NULL;
      l_row               :=NEW s9t_cells();
      drow                := dbms_xmldom.item(drows,j-1);
      cells               := dbms_xmldom.getchildrenbytagname(DBMS_XMLDOM.MAKEELEMENT(drow),'table-cell');
      row_repeat_count    := DBMS_XMLDOM.GETATTRIBUTE( DBMS_XMLDOM.MAKEELEMENT(drow),'number-rows-repeated' );
      IF row_repeat_count IS NOT NULL THEN
        EXIT;
      END IF;
      FOR k IN 1..dbms_xmldom.getlength(cells)
      LOOP
        cell              := dbms_xmldom.item(cells,k-1);
        cell_repeat_count := NULL;
        cell_repeat_count := DBMS_XMLDOM.GETATTRIBUTE( DBMS_XMLDOM.MAKEELEMENT(cell),'number-columns-repeated' );
        data_tag_list     := dbms_xmldom.getchildrenbytagname(DBMS_XMLDOM.MAKEELEMENT(cell),'p');
        data_tag          := dbms_xmldom.getfirstchild(dbms_xmldom.item(data_tag_list,0));
        data_val          := dbms_xmldom.getnodevalue(data_tag);
        -- For first row, exit at the first null cell.
        -- for non-header rows, exit if end of columns are reached.
        IF ((j=1 AND data_val IS NULL) OR (j>1 AND l_row.count() > l_column_count)) THEN
          EXIT;
        END IF;
        FOR rc IN 1..greatest(1,NVL(cell_repeat_count,0))
        LOOP
          -- for non-header rows, exit if end of columns are reached.
          IF (j>1 AND l_row.count() > l_column_count) THEN
            EXIT;
          END IF;
          l_row.EXTEND;
          l_row(l_row.count()) := data_val;
          l_count              := l_count + 1;
        END LOOP;
      END LOOP;
      IF l_row.count()                                                          > 0 THEN
        IF j                                                                    = 1 THEN
          O_file.sheets(O_file.get_sheet_index(worksheet_name)).column_headers := l_row;
          l_column_count                                                       := l_row.count();
        ELSE
          O_file.sheets(O_file.get_sheet_index(worksheet_name)).add_row(l_row);
        END IF;
      END IF;
    END LOOP;
  END LOOP;
  DBMS_XMLDOM.freeDocument(indomdoc);
  DBMS_XMLPARSER.freeParser(myParser);
  RETURN O_file;
END get_obj;
--------------------------------------------------------------------------------
FUNCTION INSERT_DATA(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file_ID       IN NUMBER,
    I_file_name     IN VARCHAR2,
    I_template_key  IN S9T_TEMPLATE.TEMPLATE_KEY%TYPE,
    I_user_lang     IN LANG.LANG%TYPE,
    I_status        IN VARCHAR2,
    I_action        IN VARCHAR2,
    I_action_date   IN DATE)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'S9T_PKG.INSERT_DATA';
BEGIN
  INSERT
  INTO s9t_folder
    (
      file_id,
      file_name,
      template_key,
      user_lang,
      status,
      action,
      action_date
    )
    VALUES
    (
      I_file_ID,
      I_file_name,
      I_template_key,
      I_user_lang,
      I_status,
      I_action,
      I_action_date
    );
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END INSERT_DATA;
--------------------------------------------------------------------------------
FUNCTION DELETE_DATA
  (
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_file_ID       IN NUMBER
  )
  RETURN BOOLEAN
IS
  L_program VARCHAR2(255) := 'S9T_PKG.DELETE_DATA';
BEGIN
  DELETE FROM s9t_folder WHERE file_id = I_file_id;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END DELETE_DATA;
--------------------------------------------------------------------------------
PROCEDURE ADD_SHEET_FOR_SQL(
    I_file       IN NUMBER,
    I_sheet_name IN VARCHAR2,
    I_query      IN VARCHAR2)
IS
  pragma autonomous_transaction;
  l_cells s9t_cells;
  l_column_headers s9t_cells:=NEW s9t_cells();
  c1      INTEGER;
  col_cnt INTEGER;
  rec_tab DBMS_SQL.DESC_TAB;
  col_num       NUMBER;
  d             NUMBER;
  col_val       VARCHAR2(4000);
  col_varchar   VARCHAR(4000);
  col_varchar2  VARCHAR2(4000);
  col_date      DATE;--
  col_timestamp TIMESTAMP;
  col_char      CHAR;
  col_number    NUMBER;
  l_rows_buffer s9t_row_tab:=NEW s9t_row_tab();
  l_buffer_size NUMBER     :=100;
BEGIN
  ADD_SHEET(I_file,I_sheet_name);
  c1 := dbms_sql.open_cursor;
  dbms_sql.parse(c1,I_query,dbms_sql.native);
  d := DBMS_SQL.EXECUTE(c1);
  DBMS_SQL.DESCRIBE_COLUMNS(c1, col_cnt, rec_tab);
  FOR j IN 1..col_cnt
  LOOP
    l_column_headers.extend;
    l_column_headers(l_column_headers.count):=rec_tab(j).col_name;
    IF rec_tab(j).col_type                   = dbms_types.typecode_number THEN
      dbms_sql.define_column(c1, j, col_number);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_char THEN
      dbms_sql.define_column(c1, j, col_char,1);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_varchar2 THEN
      dbms_sql.define_column(c1, j, col_varchar2,4000);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_varchar THEN
      dbms_sql.define_column(c1, j, col_varchar,4000);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_date THEN
      dbms_sql.define_column(c1, j, col_date);
    END IF;
    IF rec_tab(j).col_type = dbms_types.typecode_timestamp THEN
      dbms_sql.define_column(c1, j, col_timestamp);
    END IF;
  END LOOP;
  LOOP
    IF dbms_sql.fetch_rows(c1) > 0 THEN
      l_cells                 := s9t_cells();
      FOR j IN 1..col_cnt
      LOOP
        IF rec_tab(j).col_type = dbms_types.typecode_number THEN
          dbms_sql.column_value(c1, j, col_number);
          col_val := TO_CHAR(col_number);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_char THEN
          dbms_sql.column_value(c1, j, col_char);
          col_val := TO_CHAR(col_char);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_varchar2 THEN
          dbms_sql.column_value(c1, j, col_varchar2);
          col_val := TO_CHAR(col_varchar2);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_varchar THEN
          dbms_sql.column_value(c1, j, col_varchar);
          col_val := TO_CHAR(col_varchar);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_date THEN
          dbms_sql.column_value(c1, j, col_date);
          col_val := TO_CHAR(col_date);
        END IF;
        IF rec_tab(j).col_type = dbms_types.typecode_timestamp THEN
          dbms_sql.column_value(c1, j, col_timestamp);
          col_val := TO_CHAR(col_timestamp);
        END IF;
        l_cells.extend();
        l_cells(l_cells.count()) := col_val;
      END LOOP;
      l_rows_buffer.extend;
      l_rows_buffer(l_rows_buffer.count()):= NEW S9T_ROW(l_cells);
      IF l_rows_buffer.count()             > l_buffer_size THEN
        --flush buffer & initialize it
        add_rows(I_file,I_sheet_name,l_rows_buffer);
        l_rows_buffer := NEW s9t_row_tab();
      END IF;
    ELSE
      EXIT;
    END IF;
  END LOOP;
  IF l_rows_buffer.count() > 0 THEN
    add_rows(I_file,I_sheet_name,l_rows_buffer);
    l_rows_buffer := NEW s9t_row_tab();
  END IF;
  dbms_sql.close_cursor(C1);
  UPDATE TABLE
    (SELECT sf.s9t_file_obj.sheets FROM s9t_folder sf WHERE file_id = I_file
    ) sh
  SET column_headers  =l_column_headers
  WHERE sh.sheet_name = I_sheet_name;
  COMMIT;
END ADD_SHEET_FOR_SQL;
--------------------------------------------------------------------------------
FUNCTION GET_COL_KEYS_MAP(
    I_file_id    IN NUMBER,
    I_sheet_name IN VARCHAR2)
  RETURN names_map_typ
IS
  L_col_headers s9t_cells;
  l_template_key s9t_folder.template_key%type;
  O_col_key_map names_map_typ;
  CURSOR c_get_headers
  IS
    SELECT sh.column_headers,
      sf.template_key
    FROM s9t_folder sf,
      TABLE (sf.s9t_file_obj.sheets) sh
    WHERE sf.file_id  = I_file_id
    AND sh.sheet_name = I_sheet_name;
  CURSOR c_wksht_key
  IS
    SELECT wksht_key
    FROM v_s9t_tmpl_wksht_def
    WHERE template_key = l_template_key
    AND wksht_name     = I_sheet_name;
  l_wksht_key s9t_tmpl_wksht_def.wksht_key%type;
  CURSOR c_col_key(I_template_key VARCHAR2,I_wksht_key VARCHAR2,I_column_name VARCHAR2)
  IS
    SELECT column_key
    FROM v_s9t_tmpl_cols_def
    WHERE template_key = I_template_key
    AND wksht_key      = I_wksht_key
    AND column_name    = I_column_name;
  l_key_temp VARCHAR2(255);
BEGIN
  OPEN c_get_headers;
  FETCH c_get_headers INTO L_col_headers,l_template_key;
  CLOSE c_get_headers;
  OPEN c_wksht_key;
  FETCH c_wksht_key INTO l_wksht_key;
  CLOSE c_wksht_key;
  FOR i IN 1..L_col_headers.count
  LOOP
    l_key_temp := NULL;
    OPEN c_col_key(l_template_key,l_wksht_key,L_col_headers(i));
    FETCH c_col_key INTO l_key_temp;
    IF l_key_temp IS NULL THEN
      l_key_temp  := L_col_headers(i);
    END IF;
    O_col_key_map(l_key_temp):=i;
    CLOSE c_col_key;
  END LOOP;
  RETURN O_col_key_map;
END GET_COL_KEYS_MAP;
--------------------------------------------------------------------------------
FUNCTION GET_KEY_VAL_PAIRS_TAB(
    I_file_id    IN NUMBER,
    I_sheet_name IN VARCHAR2)
  RETURN key_val_pairs_tab PIPELINED
IS
  CURSOR c_rows
  IS
    SELECT cells,
      row_seq
    FROM TABLE
      (SELECT sh.s9t_rows
      FROM TABLE
        (SELECT sf.s9t_file_obj.sheets FROM s9t_folder sf WHERE file_id = I_file_id
        ) sh
    WHERE sh.sheet_name = I_sheet_name
      );
    l_names_map names_map_typ;
    temp_pairs key_val_pairs;
    l_key VARCHAR2(4000);
  BEGIN
    l_names_map := get_col_keys_map(I_file_id,I_sheet_name);
    FOR rec IN c_rows
    LOOP
      temp_pairs  := NEW key_val_pairs();
      l_key       := l_names_map.first;
      WHILE l_key IS NOT NULL
      LOOP
        temp_pairs.extend;
        temp_pairs(temp_pairs.count) := NEW key_value_pair(l_key,rec.cells(l_names_map(l_key)));
        l_key                        := l_names_map.next(l_key);
      END LOOP;
      temp_pairs.extend;
      temp_pairs(temp_pairs.count) := NEW key_value_pair('ROW_SEQ',rec.row_seq);
      pipe row (temp_pairs);
    END LOOP;
  END GET_KEY_VAL_PAIRS_TAB;
  --------------------------------------------------------------------------------
PROCEDURE copy_template(
    I_from_template IN s9t_template.template_key%type,
    I_to_template s9t_template.template_key%type)
IS
BEGIN
  -- delete any existing data for new template
  DELETE
  FROM s9t_tmpl_cols_def_tl
  WHERE template_key = I_to_template;
  DELETE FROM s9t_tmpl_cols_def WHERE template_key = I_to_template;
  DELETE FROM s9t_tmpl_wksht_def_tl WHERE template_key = I_to_template;
  DELETE FROM s9t_tmpl_wksht_def WHERE template_key = I_to_template;
  DELETE FROM s9t_template_tl WHERE template_key = I_to_template;
  DELETE FROM s9t_template WHERE template_key = I_to_template;
  --
  --copy template
  INSERT
  INTO s9t_template
    (
      TEMPLATE_KEY ,
      TEMPLATE_NAME ,
      TEMPLATE_DESC ,
      FILE_ID ,
      CREATE_ID ,
      CREATE_DATETIME ,
      TEMPLATE_TYPE
    )
  SELECT I_to_template ,
    I_to_template ,
    I_to_template ,
    FILE_ID ,
    GET_USER ,
    sysdate ,
    TEMPLATE_TYPE
  FROM s9t_template
  WHERE template_key = I_from_template;
  --
  INSERT
  INTO s9t_template_tl
    (
      TEMPLATE_KEY ,
      LANG ,
      TEMPLATE_NAME ,
      TEMPLATE_DESC
    )
  SELECT I_to_template ,
    LANG ,
    I_to_template ,
    I_to_template
  FROM s9t_template_tl
  WHERE template_key = I_from_template;
  --
  INSERT
  INTO s9t_tmpl_wksht_def
    (
      TEMPLATE_KEY ,
      WKSHT_KEY ,
      WKSHT_NAME ,
      MANDATORY ,
      SEQ_NO
    )
  SELECT I_to_template ,
    WKSHT_KEY ,
    WKSHT_NAME ,
    MANDATORY ,
    SEQ_NO
  FROM s9t_tmpl_wksht_def
  WHERE template_key = I_from_template;
  --
  INSERT
  INTO s9t_tmpl_wksht_def_tl
    (
      TEMPLATE_KEY ,
      WKSHT_KEY ,
      LANG ,
      WKSHT_NAME
    )
  SELECT I_to_template ,
    WKSHT_KEY ,
    LANG ,
    WKSHT_NAME
  FROM s9t_tmpl_wksht_def_tl
  WHERE template_key = I_from_template;
  --
  INSERT
  INTO s9t_tmpl_cols_def
    (
      TEMPLATE_KEY ,
      WKSHT_KEY ,
      COLUMN_KEY ,
      COLUMN_NAME ,
      MANDATORY ,
      CREATE_ID ,
      CREATE_DATETIME ,
      LAST_UPDATE_ID ,
      LAST_UPDATE_DATETIME ,
      DEFAULT_VALUE
    )
  SELECT I_to_template ,
    WKSHT_KEY ,
    COLUMN_KEY ,
    COLUMN_NAME ,
    MANDATORY ,
    CREATE_ID ,
    CREATE_DATETIME ,
    LAST_UPDATE_ID ,
    LAST_UPDATE_DATETIME ,
    DEFAULT_VALUE
  FROM s9t_tmpl_cols_def
  WHERE template_key = I_from_template;
  --
  INSERT
  INTO s9t_tmpl_cols_def_tl
    (
      TEMPLATE_KEY ,
      WKSHT_KEY ,
      COLUMN_KEY ,
      LANG ,
      COLUMN_NAME
    )
  SELECT I_to_template ,
    WKSHT_KEY ,
    COLUMN_KEY ,
    LANG ,
    COLUMN_NAME
  FROM s9t_tmpl_cols_def_tl
  WHERE template_key = I_from_template;
END copy_template;
--------------------------------------------------------------------------------
END s9t_pkg;
/
