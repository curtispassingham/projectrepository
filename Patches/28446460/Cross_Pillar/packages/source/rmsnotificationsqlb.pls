create or replace PACKAGE BODY RMS_NOTIFICATION_SQL AS
------------------------------------------------------------------------------------------------
FUNCTION INSERT_RAF_NOTIFICATION(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_notification_type      IN       RAF_NOTIFICATION.NOTIFICATION_TYPE%TYPE,
                                 I_notification_context   IN       VARCHAR2,
                                 I_key_no                 IN       NUMBER,
                                 I_severity               IN       RAF_NOTIFICATION.SEVERITY%TYPE,
                                 I_application_code       IN       RAF_NOTIFICATION.APPLICATION_CODE%TYPE,
                                 I_notification_desc      IN       RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE,
                                 I_recipient              IN       VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION WRITE_RAF_NOTIFICATION(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_taskflow_url       IN OUT   RMS_ASYNC_JOB.TASKFLOW_URL%TYPE,
                                I_job_type            IN       VARCHAR2,
                                I_key_no              IN       NUMBER,
                                I_mode                IN       VARCHAR2 DEFAULT 'VIEW',
                                I_recipient           IN       VARCHAR2 DEFAULT NULL)

RETURN BOOLEAN IS

   L_program                  VARCHAR2(61) := 'RMS_NOTIFICATION_SQL.WRITE_RAF_NOTIFICATION';
   L_notification_type        RAF_NOTIFICATION.NOTIFICATION_TYPE%TYPE;
   L_taskflow_url             RMS_ASYNC_JOB.TASKFLOW_URL%TYPE;
   L_notification_desc        RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE;
   L_notification_context     VARCHAR2(4000);
   L_title                    RAF_NOTIFICATION_TYPE_TL.NAME%TYPE;
   L_tab                      CODE_DETAIL.CODE%TYPE;

   
   cursor C_GET_TAB_TITLE is
      select code_desc
        from v_code_detail_tl
       where code_type = 'TABT'
         and code = L_tab; 
BEGIN
   if IO_taskflow_url is null then
      O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQ_FIELD_NULL', 'Taskflow URL');
      return FALSE;
   else
      L_taskflow_url  := IO_taskflow_url;
   end if;
   if I_job_type is null then
         O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQ_FIELD_NULL', 'Job type');
         return FALSE;
   end if;

   if I_job_type = RMS_CONSTANTS.NOTIFY_PO_REJECT then
      L_notification_type := RMS_CONSTANTS.NOTIFY_PO_REJECT_ID;
        if I_key_no is null then
         O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQ_FIELD_NULL', 'Order No.');
         return FALSE;
      end if;
      L_tab := 'PO';
      open C_GET_TAB_TITLE;
      fetch C_GET_TAB_TITLE into L_title;
      close C_GET_TAB_TITLE;
      L_notification_desc := 'NOTIFY_PO_REJECT';
      L_notification_context := 'title=' || L_title || '|url=' || L_taskflow_url || '|pmOrderNo=' || I_key_no || '|pmOrderMode=' || I_mode || '|pmMode=' || I_mode;
   else
      if I_job_type = RMS_CONSTANTS.NOTIFY_TRANSFER_REJECT then
         L_notification_type := RMS_CONSTANTS.NOTIFY_TRANSFER_REJECT_ID;
         if I_key_no is null then
            O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQ_FIELD_NULL', 'Transfer No.');
            return FALSE;
         end if;
      end if;
      L_tab := 'TSF';
      open C_GET_TAB_TITLE;
      fetch C_GET_TAB_TITLE into L_title;
      close C_GET_TAB_TITLE;
      L_notification_desc := 'NOTIFY_TSF_REJECT';
      L_notification_context := 'title=' || L_title || '|url=' || L_taskflow_url || '|pmTsfNo=' || I_key_no || '|pmMode=' || I_mode;
   end if;
      if I_job_type = RMS_CONSTANTS.NOTIFY_DATA_UPLD_FAILURE then
      if I_key_no is null then
         O_error_message := SQL_LIB.GET_MESSAGE_TEXT('REQ_FIELD_NULL', 'Process ID.');
         return FALSE;
      end if;
      L_tab := 'LOAD';
      open C_GET_TAB_TITLE;
      fetch C_GET_TAB_TITLE into L_title;
      close C_GET_TAB_TITLE;
      L_notification_type := RMS_CONSTANTS.DATA_UPLD_FAILED;
      L_notification_desc := 'NOTIFY_DATA_UPLD_FAILED';
      L_notification_context := 'title=' || L_title || '|url=' || L_taskflow_url || '|pmMode=' || I_mode || '|pmProcessId=' || I_key_no;
   end if;
   
   if INSERT_RAF_NOTIFICATION(O_error_message,
                              L_notification_type,
                              L_notification_context,
                              I_key_no,
                              RMS_CONSTANTS.NOTIFICATION_SEV_LOW,
                              RMS_CONSTANTS.ASYNC_APPLICATION_CODE,
                              L_notification_desc,
                              NVL(I_recipient, get_user)) = FALSE then
       return FALSE;
   end if;
   
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;   
END WRITE_RAF_NOTIFICATION;                                
-------------------------------------------------------------------------------
FUNCTION INSERT_RAF_NOTIFICATION(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_notification_type      IN       RAF_NOTIFICATION.NOTIFICATION_TYPE%TYPE,
                                 I_notification_context   IN       VARCHAR2,
                                 I_key_no                 IN       NUMBER,
                                 I_severity               IN       RAF_NOTIFICATION.SEVERITY%TYPE,
                                 I_application_code       IN       RAF_NOTIFICATION.APPLICATION_CODE%TYPE,
                                 I_notification_desc      IN       RAF_NOTIFICATION.NOTIFICATION_DESC%TYPE,
                                 I_recipient              IN       VARCHAR2)
RETURN BOOLEAN IS
 
   L_program              VARCHAR2(60)                               := 'RMS_NOTIFICATION_SQL.INSERT_RAF_NOTIFICATION';
   L_notify_desc          RAF_NOTIFICATION_TYPE_TL.DESCRIPTION%TYPE;
   L_sub_string           VARCHAR2(5)                                := '%s';
      
BEGIN
   
   L_notify_desc := SQL_LIB.GET_MESSAGE_TEXT(I_notification_desc,
                                             I_key_no);
             
   if RAF_NOTIFICATION_TASK_PKG.CREATE_NOTIFY_TASK_AUTOTRAN(O_error_message,
                                                            RAF_NOTIFICATION_SEQ.NEXTVAL,
                                                            I_notification_context,
                                                            I_notification_type,
                                                            I_severity,
                                                            I_application_code,
                                                            L_notify_desc,
                                                            'U',
                                                            null,
                                                            to_timestamp(to_char(sysdate,'MM/DD/RRRR HH24:MI:SS'), 'MM/DD/RRRR HH24:MI:SS'),
                                                            I_recipient,
                                                            get_user,
                                                            'Y',
                                                            1) = 0 then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_RAF_NOTIFICATION;
-------------------------------------------------------------------------------
END RMS_NOTIFICATION_SQL;
/
