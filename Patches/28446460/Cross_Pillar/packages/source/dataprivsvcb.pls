create or replace PACKAGE BODY DATAPRIV_SVC IS
----------------------------------------------------------------------------------
FUNCTION QUERY_DATA(IN_DATAPRIV_CTX_PARAMS   IN       RAF_DATAPRIV_CTX_PARAM_TBL,
                    OUT_ERROR_MESSAGE           OUT   VARCHAR2)
RETURN SYS_REFCURSOR IS

   L_program       VARCHAR2(50)  := 'DATAPRIV_SVC.QUERY_DATA';
   L_ret_cursor    SYS_REFCURSOR;
   L_entity_name   VARCHAR2(40)  := NULL;
   L_entity_type   VARCHAR2(6)   := NULL;
   L_entity_id     VARCHAR2(20)  := NULL;
   L_full_name     VARCHAR2(120) := NULL;
   L_phone         VARCHAR2(20)  := NULL;
   L_email         VARCHAR2(100)  := NULL;

BEGIN

   FOR i in IN_DATAPRIV_CTX_PARAMS.first..IN_DATAPRIV_CTX_PARAMS.last LOOP
      if IN_DATAPRIV_CTX_PARAMS(i).param_name = 'entityName' then
         L_entity_name := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'entityType' then
         L_entity_type := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'entityId' then
         L_entity_id := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'fullName' then
         L_full_name := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'phone' then
         L_phone := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'email' then
         L_email := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      end if;
   END LOOP;

   if L_entity_name is NOT NULL and
      UPPER(L_entity_name) NOT IN ('BUYER','MERCHANT','STORE','WAREHOUSE','SUPPLIER','PARTNER','OUTLOC','EMPLOYEE','CUSTOMER','ORDER CUSTOMER','COMP SHOPPER','USER ATTRIB') then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('INV_ENTITY_NAME_QUERY',
                                              'entityName',
                                              L_entity_name,
                                              NULL);
         return NULL;
   end if;

   open L_ret_cursor for select 'BUYER' entity_name,
                                NULL entity_type,
                                to_char(buyer) entity_id,
                                buyer_name full_name,
                                buyer_phone phone,
                                buyer_fax fax,
                                NULL telex,
                                NULL pager,
                                NULL email
                           from buyer
                          where 'BUYER' = UPPER(NVL(L_entity_name, 'BUYER'))
                            and L_entity_type is NULL
                            and to_char(buyer) = NVL(L_entity_id, to_char(buyer))
                            and (L_full_name is NULL
                             or instr(upper(buyer_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(buyer_phone, L_phone) > 0
                             or instr(buyer_fax, L_phone) > 0)
                            and L_email is NULL
                       union all
                         select 'MERCHANT' entity_name,
                                NULL entity_type,
                                to_char(merch) entity_id,
                                merch_name full_name,
                                merch_phone phone,
                                merch_fax fax,
                                NULL telex,
                                NULL pager,
                                NULL email
                           from merchant
                          where 'MERCHANT' = UPPER(NVL(L_entity_name, 'MERCHANT'))
                            and L_entity_type is NULL
                            and to_char(merch) = NVL(L_entity_id, to_char(merch))
                            and (L_full_name is NULL
                             or instr(upper(merch_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(merch_phone, L_phone) > 0
                             or instr(merch_fax, L_phone) > 0)
                            and L_email is NULL
                      union all
                         select 'STORE' entity_name,
                                NULL entity_type,
                                to_char(store) entity_id,
                                store_mgr_name full_name,
                                phone_number phone,
                                fax_number fax,
                                NULL telex,
                                NULL pager,
                                email email
                           from store
                          where 'STORE' = UPPER(NVL(L_entity_name, 'STORE'))
                            and L_entity_type is NULL
                            and to_char(store) = NVL(L_entity_id, to_char(store))
                            and (L_full_name is NULL
                             or instr(upper(store_mgr_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(phone_number, L_phone) > 0
                             or instr(fax_number, L_phone) > 0)
                            and (L_email is NULL
                             or instr(upper(email), upper(L_email)) > 0)
                      union all
                         select 'WAREHOUSE' entity_name,
                                NULL entity_type,
                                to_char(wh) entity_id,
                                NULL full_name,
                                NULL phone,
                                NULL fax,
                                NULL telex,
                                NULL pager,
                                email email
                           from wh
                          where 'WAREHOUSE' = UPPER(NVL(L_entity_name, 'WAREHOUSE'))
                            and L_entity_type is NULL
                            and to_char(wh) = NVL(L_entity_id, to_char(wh))
                            and L_full_name is NULL
                            and L_phone is NULL
                            and (L_email is NULL
                             or instr(upper(email), upper(L_email)) > 0)
                      union all
                         select 'SUPPLIER' entity_name,
                                NULL entity_type,
                                to_char(supplier) entity_id,
                                contact_name full_name,
                                contact_phone phone,
                                contact_fax fax,
                                contact_telex telex,
                                contact_pager pager,
                                contact_email email
                           from sups
                          where 'SUPPLIER' = UPPER(NVL(L_entity_name, 'SUPPLIER'))
                            and L_entity_type is NULL
                            and to_char(supplier) = NVL(L_entity_id, to_char(supplier))
                            and (L_full_name is NULL
                             or instr(upper(contact_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(contact_phone, L_phone) > 0
                             or instr(contact_fax, L_phone) > 0
                             or instr(contact_telex, L_phone) > 0
                             or instr(contact_pager, L_phone) > 0)
                            and (L_email is NULL
                             or instr(upper(contact_email), upper(L_email)) > 0)
                      union all
                         select 'PARTNER' entity_name,
                                partner_type entity_type,
                                partner_id entity_id,
                                contact_name full_name,
                                contact_phone phone,
                                contact_fax fax,
                                contact_telex telex,
                                NULL pager,
                                contact_email email
                           from partner
                          where 'PARTNER' = UPPER(NVL(L_entity_name, 'PARTNER'))
                            and partner_type = NVL(L_entity_type, partner_type)
                            and partner_id = NVL(L_entity_id, partner_id)
                            and (L_full_name is NULL
                             or instr(upper(contact_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(contact_phone, L_phone) > 0
                             or instr(contact_fax, L_phone) > 0
                             or instr(contact_telex, L_phone) > 0)
                            and (L_email is NULL
                             or instr(upper(contact_email), upper(L_email)) > 0)
                      union all
                         select 'OUTLOC' entity_name,
                                outloc_type entity_type,
                                outloc_id entity_id,
                                contact_name full_name,
                                contact_phone phone,
                                contact_fax fax,
                                contact_telex telex,
                                NULL pager,
                                contact_email email
                           from outloc
                          where 'OUTLOC' = UPPER(NVL(L_entity_name, 'OUTLOC'))
                            and outloc_type = NVL(L_entity_type, outloc_type)
                            and outloc_id = NVL(L_entity_id, outloc_id)
                            and (L_full_name is NULL
                             or instr(upper(contact_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(contact_phone, L_phone) > 0
                             or instr(contact_fax, L_phone) > 0
                             or instr(contact_telex, L_phone) > 0)
                            and (L_email is NULL
                             or instr(upper(contact_email), upper(L_email)) > 0)
                      union all
                         select 'ADDRESS' entity_name,
                                NULL entity_type,
                                to_char(addr_key) entity_id,
                                contact_name full_name,
                                contact_phone phone,
                                contact_fax fax,
                                contact_telex telex,
                                NULL pager,
                                contact_email email
                           from addr
                          where ('STORE' = UPPER(NVL(L_entity_name, 'STORE'))
                            and addr.module in ('ST', 'WFST')
                            and L_entity_type is NULL
                            and key_value_1 = NVL(L_entity_id, key_value_1)
                             or 'WAREHOUSE' = UPPER(NVL(L_entity_name, 'WAREHOUSE'))
                            and addr.module = 'WH'
                            and L_entity_type is NULL
                            and key_value_1 = NVL(L_entity_id, key_value_1)
                             or 'SUPPLIER' = UPPER(NVL(L_entity_name, 'SUPPLIER'))
                            and addr.module = 'SUPP'
                            and L_entity_type is NULL
                            and key_value_1 = NVL(L_entity_id, key_value_1)
                             or 'PARTNER' = UPPER(NVL(L_entity_name, 'PARTNER'))
                            and addr.module = 'PTNR'
                            and key_value_1 = NVL(L_entity_type, key_value_1)
                            and key_value_2 = NVL(L_entity_id, key_value_2))
                            and (L_full_name is NULL
                             or instr(upper(contact_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(contact_phone, L_phone) > 0
                             or instr(contact_fax, L_phone) > 0
                             or instr(contact_telex, L_phone) > 0)
                            and (L_email is NULL
                             or instr(upper(contact_email), upper(L_email)) > 0)
                      union all
                         select 'EMPLOYEE' entity_name,
                                NULL entity_type,
                                emp_id entity_id,
                                name full_name,
                                phone phone,
                                NULL fax,
                                NULL telex,
                                NULL pager,
                                email email
                           from sa_employee
                          where 'EMPLOYEE' = UPPER(NVL(L_entity_name, 'EMPLOYEE'))
                            and L_entity_type is NULL
                            and emp_id = NVL(L_entity_id, emp_id)
                            and (L_full_name is NULL
                             or instr(upper(name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(phone, L_phone) > 0)
                            and (L_email is NULL
                             or instr(upper(email), upper(L_email)) > 0)
                      union all
                         select 'CUSTOMER' entity_name,
                                cust_id_type entity_type,
                                cust_id entity_id,
                                name full_name,
                                case
                                   when L_phone is NULL or instr(home_phone, L_phone) > 0 then
                                      home_phone
                                   when L_phone is NULL or instr(work_phone, L_phone) > 0 then
                                      work_phone
                                end phone,
                                NULL fax,
                                NULL telex,
                                NULL pager,
                                e_mail email
                           from sa_customer
                          where 'CUSTOMER' = UPPER(NVL(L_entity_name, 'CUSTOMER'))
                            and cust_id_type = NVL(L_entity_type, cust_id_type)
                            and cust_id = NVL(L_entity_id, cust_id)
                            and (L_full_name is NULL
                             or instr(upper(name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(home_phone, L_phone) > 0
                             or instr(work_phone, L_phone) > 0)
                            and (L_email is NULL
                             or instr(upper(e_mail), upper(L_email)) > 0)
                      union all
                         select 'ORDER CUSTOMER' entity_name,
                                NULL entity_type,
                                to_char(ordcust_no) entity_id,
                                case
                                  WHEN L_full_name is NULL and upper(deliver_first_name) = upper(bill_first_name) and upper(deliver_last_name) = upper(bill_last_name) THEN
                                   'Delivername :' || deliver_first_name||' '||deliver_last_name
                                  WHEN L_full_name is NULL and ((upper(deliver_first_name) <> upper(bill_first_name) and upper(deliver_last_name) <> upper(bill_last_name)) or (upper(deliver_first_name) = upper(bill_first_name) and upper(deliver_last_name) <> upper(bill_last_name)) or (upper(deliver_first_name) <> upper(bill_first_name) and upper(deliver_last_name) = upper(bill_last_name)))THEN
                                   'Delivername :' || deliver_first_name||' '||deliver_last_name||','||'Billname :'||bill_first_name||' '||bill_last_name
                                  WHEN instr(upper(deliver_first_name||' '||deliver_last_name), upper(L_full_name)) > 0 and instr(upper(bill_first_name||' '||bill_last_name), upper(L_full_name)) > 0 THEN
                                     'Delivername :' || deliver_first_name||' '||deliver_last_name||','||'Billname :'||bill_first_name||' '||bill_last_name
                                  WHEN instr(upper(deliver_first_name||' '||deliver_last_name), upper(L_full_name)) > 0 THEN
                                    deliver_first_name||' '||deliver_last_name
                                  WHEN instr(upper(bill_first_name||' '||bill_last_name), upper(L_full_name)) > 0 THEN
                                      bill_first_name||' '||bill_last_name
                                end full_name,
                                case
                                   when L_phone is NULL or instr(deliver_phone, L_phone) > 0 then
                                      deliver_phone
                                   when L_phone is NULL or instr(bill_phone, L_phone) > 0 then
                                      bill_phone
                                end phone,
                                NULL fax,
                                NULL telex,
                                NULL pager,
                                NULL email
                           from ordcust
                          where 'ORDER CUSTOMER' = UPPER(NVL(L_entity_name, 'ORDER CUSTOMER'))
                            and L_entity_type is NULL
                            and to_char(ordcust_no) = NVL(L_entity_id, to_char(ordcust_no))
                            and (L_full_name is NULL
                             or instr(upper(bill_first_name||' '||bill_last_name), upper(L_full_name)) > 0
                             or instr(upper(deliver_first_name||' '||deliver_last_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(bill_phone, L_phone) > 0
                             or instr(deliver_phone, L_phone) > 0)
                            and L_email is NULL
                      union all
                         select 'COMP SHOPPER' entity_name,
                                NULL entity_type,
                                to_char(shopper) entity_id,
                                shopper_name full_name,
                                shopper_phone phone,
                                shopper_fax fax,
                                NULL telex,
                                NULL pager,
                                NULL email								
                           from comp_shopper
                          where 'COMP SHOPPER' = UPPER(NVL(L_entity_name, 'COMP SHOPPER'))
                            and L_entity_type is NULL
                            and to_char(shopper) = NVL(L_entity_id, to_char(shopper))
                            and (L_full_name is NULL
                             or instr(upper(shopper_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(shopper_phone, L_phone) > 0
                             or instr(shopper_fax, L_phone) > 0)
							and L_email is NULL
                      union all
                         select 'USER ATTRIB' entity_name,
                                NULL entity_type,
                                user_id entity_id,
                                user_name full_name,
                                user_phone phone,
                                user_fax fax,
                                NULL telex,
                                user_pager pager,
                                user_email email								
                           from USER_ATTRIB
                          where 'USER ATTRIB' = UPPER(NVL(L_entity_name, 'USER ATTRIB'))
                            and L_entity_type is NULL
                            and user_id = NVL(L_entity_id, user_id)
                            and (L_full_name is NULL
                             or instr(upper(user_name), upper(L_full_name)) > 0)
                            and (L_phone is NULL
                             or instr(user_phone, L_phone) > 0
                             or instr(user_fax, L_phone) > 0
                             or instr(user_pager, L_phone) > 0)
							and (L_email is NULL
                             or instr(upper(user_email), upper(L_email)) > 0);

   return L_ret_cursor;

EXCEPTION
   when OTHERS then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return NULL;
END QUERY_DATA;
----------------------------------------------------------------------------------
FUNCTION UPDATE_DATA(IN_DATAPRIV_CTX_PARAMS   IN       RAF_DATAPRIV_CTX_PARAM_TBL,
                     OUT_ERROR_MESSAGE           OUT   VARCHAR2)
RETURN NUMBER IS

   L_program        VARCHAR2(50)  := 'DATAPRIV_SVC.UPDATE_DATA';
   L_entity_name    VARCHAR2(40)  := NULL;
   L_entity_type    VARCHAR2(6)   := NULL;
   L_entity_id      VARCHAR2(20)  := NULL;
   L_full_name      VARCHAR2(120) := NULL;
   L_phone          VARCHAR2(20)  := NULL;
   L_fax            VARCHAR2(20)  := NULL;
   L_telex          VARCHAR2(20)  := NULL;
   L_pager          VARCHAR2(20)  := NULL;
   L_email          VARCHAR2(100)  := NULL;
   L_addr1          VARCHAR2(240) := NULL;
   L_addr2          VARCHAR2(240) := NULL;
   L_addr3          VARCHAR2(240) := NULL;
   L_county         VARCHAR2(250) := NULL;
   L_city           VARCHAR2(120) := NULL;
   L_state          VARCHAR2(3)   := NULL;
   L_country_id     VARCHAR2(3)   := NULL;
   L_postal_code    VARCHAR2(30)  := NULL;
   L_birthdate      DATE          := NULL;
   L_table          VARCHAR2(30)  := NULL;
   L_recs_updated   NUMBER        := 0;
   L_rowid          ROWID;
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_BUYER is
      select rowid
        from buyer
       where to_char(buyer) = L_entity_id
         for update nowait;

   cursor C_LOCK_MERCHANT is
      select rowid
        from merchant
       where to_char(merch) = L_entity_id
         for update nowait;

   cursor C_LOCK_STORE is
      select rowid
        from store
       where to_char(store) = L_entity_id
         for update nowait;

   cursor C_LOCK_WH is
      select rowid
        from wh
       where to_char(wh) = L_entity_id
         for update nowait;

   cursor C_LOCK_SUPPLIER is
      select rowid
        from sups
       where to_char(supplier) = L_entity_id
         for update nowait;

   cursor C_LOCK_PARTNER is
      select rowid
        from partner
       where partner_id = L_entity_id
         and partner_type = L_entity_type
         for update nowait;

   cursor C_LOCK_OUTLOC is
      select rowid
        from outloc
       where outloc_id = L_entity_id
         and outloc_type = L_entity_type
         for update nowait;

   cursor C_LOCK_ADDRESS is
      select rowid
        from addr
       where to_char(addr_key) = L_entity_id
         for update nowait;

   cursor C_LOCK_EMPLOYEE is
      select rowid
        from sa_employee
       where emp_id = L_entity_id
         for update nowait;

   cursor C_LOCK_CUSTOMER is
      select rowid
        from sa_customer
       where cust_id = L_entity_id
         and cust_id_type = L_entity_type
         for update nowait;

   cursor C_LOCK_ORDER_CUSTOMER is
      select rowid
        from ordcust
       where to_char(ordcust_no) = L_entity_id
         for update nowait;
		 
   cursor C_LOCK_COMP_SHOPPER is
      select rowid
        from comp_shopper
       where to_char(SHOPPER) = L_entity_id
         for update nowait;	

   cursor C_LOCK_USER_ATTRIB is
      select rowid
        from user_attrib
       where user_id = L_entity_id
         for update nowait;		 

BEGIN

  FOR i in IN_DATAPRIV_CTX_PARAMS.first..IN_DATAPRIV_CTX_PARAMS.last LOOP
      if IN_DATAPRIV_CTX_PARAMS(i).param_name = 'entityName' then
         L_entity_name := UPPER(IN_DATAPRIV_CTX_PARAMS(i).param_value);
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'entityType' then
         L_entity_type := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'entityId' then
         L_entity_id := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'fullName' then
         L_full_name := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'phone' then
         L_phone := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'fax' then
         L_fax := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'telex' then
         L_telex := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'pager' then
         L_pager := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'email' then
         L_email := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'addr1' then
         L_addr1 := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'addr2' then
         L_addr2 := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'addr3' then
         L_addr3 := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'county' then
         L_county := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'city' then
         L_city := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'state' then
         L_state := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'countryId' then
         L_country_id := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      elsif IN_DATAPRIV_CTX_PARAMS(i).param_name = 'postalCode' then
         L_postal_code := IN_DATAPRIV_CTX_PARAMS(i).param_value;
      end if;
   END LOOP;

   if L_entity_name is NULL then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                              'entityName',
                                              NULL,
                                              NULL);
         return 0;
   elsif L_entity_id is NULL then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                              'entityId',
                                              NULL,
                                              NULL);
         return 0;
   end if;

   if L_entity_name NOT IN ('BUYER','MERCHANT','STORE','WAREHOUSE','SUPPLIER','PARTNER','OUTLOC','ADDRESS','EMPLOYEE','CUSTOMER','ORDER CUSTOMER','COMP SHOPPER','USER ATTRIB') then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('INV_ENTITY_NAME_UPDATE',
                                              'entityName',
                                              L_entity_name,
                                              NULL);
         return 0;
   elsif L_entity_name IN ('PARTNER','OUTLOC','CUSTOMER') and L_entity_type is NULL then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
                                              'entityType',
                                              NULL,
                                              NULL);
         return 0;
   end if;

   if L_entity_name = 'BUYER' then
      L_table := 'BUYER';
      open C_LOCK_BUYER;
      fetch C_LOCK_BUYER into L_rowid;
      close C_LOCK_BUYER;

      update buyer
         set buyer_name  = NVL(L_full_name,'XXXXX'),
             buyer_phone = L_phone,
             buyer_fax   = L_fax
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'MERCHANT' then
      L_table := 'MERCHANT';
      open C_LOCK_MERCHANT;
      fetch C_LOCK_MERCHANT into L_rowid;
      close C_LOCK_MERCHANT;

      update merchant
         set merch_name  = NVL(L_full_name,'XXXXX'),
             merch_phone = L_phone,
             merch_fax   = L_fax
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'STORE' then
      L_table := 'STORE';
      open C_LOCK_STORE;
      fetch C_LOCK_STORE into L_rowid;
      close C_LOCK_STORE;

      update store
         set store_mgr_name = NVL(L_full_name,'XXXXX'),
             phone_number   = L_phone,
             fax_number     = L_fax,
             email          = L_email
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'WAREHOUSE' then
      L_table := 'WH';
      open C_LOCK_WH;
      fetch C_LOCK_WH into L_rowid;
      close C_LOCK_WH;

      update wh
         set email = L_email
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'SUPPLIER' then
      L_table := 'SUPS';
      open C_LOCK_SUPPLIER;
      fetch C_LOCK_SUPPLIER into L_rowid;
      close C_LOCK_SUPPLIER;

      update sups
         set contact_name  = NVL(L_full_name,'XXXXX'),
             contact_phone = NVL(L_phone,'XXXXX'),
             contact_fax   = L_fax,
             contact_telex = L_telex,
             contact_pager = L_pager,
             contact_email = L_email
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'PARTNER' then
      L_table := 'PARTNER';
      open C_LOCK_PARTNER;
      fetch C_LOCK_PARTNER into L_rowid;
      close C_LOCK_PARTNER;

      update partner
         set contact_name  = NVL(L_full_name,'XXXXX'),
             contact_phone = NVL(L_phone,'XXXXX'),
             contact_fax   = L_fax,
             contact_telex = L_telex,
             contact_email = L_email
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'OUTLOC' then
      L_table := 'OUTLOC';
      open C_LOCK_OUTLOC;
      fetch C_LOCK_OUTLOC into L_rowid;
      close C_LOCK_OUTLOC;

      update outloc
         set contact_name  = L_full_name,
             contact_phone = L_phone,
             contact_fax   = L_fax,
             contact_telex = L_telex,
             contact_email = L_email
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'ADDRESS' then
      L_table := 'ADDR';
      open C_LOCK_ADDRESS;
      fetch C_LOCK_ADDRESS into L_rowid;
      close C_LOCK_ADDRESS;

      update addr
         set contact_name  = L_full_name,
             contact_phone = L_phone,
             contact_fax   = L_fax,
             contact_telex = L_telex,
             contact_email = L_email
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'EMPLOYEE' then
      L_table := 'SA_EMPLOYEE';
      open C_LOCK_EMPLOYEE;
      fetch C_LOCK_EMPLOYEE into L_rowid;
      close C_LOCK_EMPLOYEE;

      update sa_employee
         set name  = NVL(L_full_name,'XXXXX'),
             phone = L_phone,
             email = L_email
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;

   elsif L_entity_name = 'CUSTOMER' then
      L_table := 'SA_CUSTOMER';
	  
      for rec IN C_LOCK_CUSTOMER LOOP
         update sa_customer
            set name        = L_full_name,
                addr1       = L_addr1,
                addr2       = L_addr2,
                city        = L_city,
                state       = L_state,
                postal_code = L_postal_code,
                country     = L_country_id,
                home_phone  = L_phone,
                work_phone  = L_phone,
                e_mail      = L_email,
		        birthdate   = L_birthdate
          where rowid = rec.rowid;
		  
          L_recs_updated := SQL%ROWCOUNT;	  
      END LOOP;	   

   elsif L_entity_name = 'ORDER CUSTOMER' then
      L_table := 'ORDCUST';
      open C_LOCK_ORDER_CUSTOMER;
      fetch C_LOCK_ORDER_CUSTOMER into L_rowid;
      close C_LOCK_ORDER_CUSTOMER;

      update ordcust
         set bill_first_name      = L_full_name,
             bill_last_name       = L_full_name,
			 bill_phonetic_first  = L_full_name,
			 bill_phonetic_last   = L_full_name,
			 bill_preferred_name  = L_full_name,
             bill_add1            = L_addr1,
             bill_add2            = L_addr2,
             bill_add3            = L_addr3,
             bill_county          = L_county,
             bill_city            = L_city,
             bill_state           = L_state,
             bill_country_id      = L_country_id,
             bill_post            = L_postal_code,
             bill_phone           = L_phone,
             deliver_first_name   = L_full_name,
			 deliver_phonetic_first = L_full_name,
			 deliver_phonetic_last = L_full_name,
			 deliver_preferred_name = L_full_name,
             deliver_last_name    = L_full_name,
             deliver_add1         = L_addr1,
             deliver_add2         = L_addr2,
             deliver_add3         = L_addr3,
             deliver_county       = L_county,
             deliver_city         = L_city,
             deliver_state        = L_state,
             deliver_country_id   = L_country_id,
             deliver_post         = L_postal_code,
             deliver_phone        = L_phone,
             last_update_datetime = sysdate,
             last_update_id       = get_user
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;
	  
   elsif L_entity_name = 'COMP SHOPPER' then
      L_table := 'COMP_SHOPPER';
      open C_LOCK_COMP_SHOPPER;
      fetch C_LOCK_COMP_SHOPPER into L_rowid;
      close C_LOCK_COMP_SHOPPER;

      update comp_shopper
         set shopper_name   = NVL(L_full_name,'XXXXX'),
             shopper_phone  = L_phone,
             shopper_fax    = L_fax
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;	
	  
   elsif L_entity_name = 'USER ATTRIB' then
      L_table := 'USER_ATTRIB';
      open C_LOCK_USER_ATTRIB;
      fetch C_LOCK_USER_ATTRIB into L_rowid;
      close C_LOCK_USER_ATTRIB;

      update user_attrib
         set user_name  = NVL(L_full_name,'XXXXX'),
             user_phone = L_phone,
             user_fax   = L_fax,
             user_pager = L_pager,
             user_email = L_email
       where rowid = L_rowid;

      L_recs_updated := SQL%ROWCOUNT;	  
   end if;

   if L_recs_updated = 0 then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('NO_RECORD_UPDATE',
                                              NULL,
                                              NULL,
                                              NULL);
         return 0;
   end if;

   return 1;

EXCEPTION
   when RECORD_LOCKED then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                              L_table,
                                              L_entity_id,
                                              L_entity_type);
      return 0;
   when OTHERS then
      OUT_ERROR_MESSAGE := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return 0;
END UPDATE_DATA;
-----------------------------------------------------------------------------------
END DATAPRIV_SVC;
/