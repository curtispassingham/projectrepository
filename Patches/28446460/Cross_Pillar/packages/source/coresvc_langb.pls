CREATE OR REPLACE PACKAGE BODY CORESVC_LANG AS

   cursor C_SVC_LANG(I_process_id   NUMBER,
                     I_chunk_id     NUMBER) is
      select pk_lang.rowid             as pk_lang_rid,
             st.rowid                  as st_rid,
             pk_lang.webhelp_server    as old_webhelp_server,
             pk_lang.webreports_server as old_webreports_server,
             pk_lang.reports_server    as old_reports_server,
             UPPER(st.iso_code)        as iso_code,
             st.webreports_server,
             st.reports_server,
             st.webhelp_server,
             st.description,
             st.lang,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)          as action,
             st.process$status
        from svc_lang st,
             lang pk_lang,
             dual
       where st.process_id   = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.lang         = pk_lang.lang (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab       errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab   s9t_errors_tab_typ;

   Type LANG_TAB IS TABLE OF LANG_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
   L_program VARCHAR2(64) := 'CORESVC_LANG.GET_SHEET_NAME_TRANS';
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet     IN   VARCHAR2,
                           I_row_seq   IN   NUMBER,
                           I_col       IN   VARCHAR2,
                           I_sqlcode   IN   NUMBER,
                           I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
----------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets       s9t_pkg.names_map_typ;
   lang_cols      s9t_pkg.names_map_typ;
   lang_tl_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets               :=s9t_pkg.get_sheet_names(I_file_id);
   lang_cols              :=s9t_pkg.get_col_names(I_file_id,LANG_sheet);
   LANG$ACTION            := lang_cols('ACTION');
   LANG$LANG              := lang_cols('LANG');
   LANG$DESCRIPTION       := lang_cols('DESCRIPTION');
   LANG$WEBHELP_SERVER    := lang_cols('WEBHELP_SERVER');
   LANG$REPORTS_SERVER    := lang_cols('REPORTS_SERVER');
   LANG$WEBREPORTS_SERVER := lang_cols('WEBREPORTS_SERVER');
   LANG$ISO_CODE          := lang_cols('ISO_CODE');

   lang_TL_cols           :=s9t_pkg.get_col_names(I_file_id,LANG_TL_sheet);
   LANG_TL$ACTION         := lang_tl_cols('ACTION');
   LANG_TL$LANG           := lang_tl_cols('LANG');
   LANG_TL$LANG_LANG      := lang_tl_cols('LANG_LANG');
   LANG_TL$DESCRIPTION    := lang_tl_cols('DESCRIPTION');
END POPULATE_NAMES;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_LANG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = LANG_sheet )
   select s9t_row(s9t_cells( CORESVC_LANG.action_mod,
                             lang,
                             description,
                             webhelp_server,
                             reports_server,
                             webreports_server,
                             iso_code
                             ))
     from lang ;
END POPULATE_LANG;
-------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_LANG_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = LANG_TL_sheet )
   select s9t_row(s9t_cells( CORESVC_LANG.action_mod,
                             lang,
                             lang_lang,
                             description))
     from lang_tl ;
END POPULATE_LANG_TL;
---------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(LANG_sheet);
   L_file.sheets(l_file.get_sheet_index(LANG_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                  'LANG',
                                                                                  'DESCRIPTION',
                                                                                  'WEBHELP_SERVER',
                                                                                  'REPORTS_SERVER',
                                                                                  'WEBREPORTS_SERVER',
                                                                                  'ISO_CODE');

   L_file.add_sheet(LANG_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(LANG_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                     'LANG',
                                                                                     'LANG_LANG',
                                                                                     'DESCRIPTION');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_LANG.CREATE_S9T';
   L_file s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_LANG(O_file_id);
      POPULATE_LANG_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        CORESVC_LANG.template_category,
                        L_file)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
---------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_LANG( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                            I_process_id   IN   SVC_LANG.PROCESS_ID%TYPE) IS
   TYPE svc_lang_col_typ is TABLE OF SVC_LANG%ROWTYPE;
   L_temp_rec       SVC_LANG%ROWTYPE;
   svc_lang_col     svc_lang_col_typ :=NEW svc_lang_col_typ();
   L_process_id     SVC_LANG.PROCESS_ID%TYPE;
   L_error          BOOLEAN         :=FALSE;
   L_default_rec    SVC_LANG%ROWTYPE;
   cursor C_MANDATORY_IND is
      select iso_code_mi,
             webreports_server_mi,
             reports_server_mi,
             webhelp_server_mi,
             description_mi,
             LANG_mi,
             1 as dummy
        from (SELECT column_key,
                     mandatory
                FROM s9t_tmpl_cols_def
               WHERE template_key                                = CORESVC_LANG.template_key
                 and wksht_key                                   = 'LANG'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('iso_code' as iso_code,
                                            'webreports_server' as webreports_server,
                                            'reports_server' as reports_server,
                                            'webhelp_server' as webhelp_server,
                                            'description' as description,
                                            'lang' as lang,
                                             null as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_LANG';
   L_pk_columns    VARCHAR2(255)  := 'Language';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  iso_code_dv,
                       webreports_server_dv,
                       reports_server_dv,
                       webhelp_server_dv,
                       description_dv,
                       LANG_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = CORESVC_LANG.template_key
                          and wksht_key                                       = 'LANG'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'ISO_CODE' as iso_code,
                                                      'WEBREPORTS_SERVER' as webreports_server,
                                                      'REPORTS_SERVER' as reports_server,
                                                      'WEBHELP_SERVER' as webhelp_server,
                                                      'DESCRIPTION' as description,
                                                      'LANG' as lang,
                                                       NULL as dummy)))
   LOOP
   BEGIN
      L_default_rec.iso_code := rec.iso_code_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LANG',
                         NULL,
                         'ISO_CODE',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.webreports_server := rec.webreports_server_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LANG',
                         NULL,
                         'WEBREPORTS_SERVER',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.reports_server := rec.reports_server_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LANG',
                         NULL,
                         'REPORTS_SERVER',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.webhelp_server := rec.webhelp_server_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LANG',
                         NULL,
                         'WEBHELP_SERVER',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.description := rec.description_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LANG',
                         NULL,
                         'DESCRIPTION',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.lang := rec.lang_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LANG',
                         NULL,
                         'LANG',
                         NULL,
                         'INV_DEFAULT');
   END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
   (SELECT r.get_cell(LANG$ACTION)                as action,
           r.get_cell(LANG$ISO_CODE)              as iso_code,
           r.get_cell(LANG$WEBREPORTS_SERVER)     as webreports_server,
           r.get_cell(LANG$REPORTS_SERVER)        as reports_server,
           r.get_cell(LANG$WEBHELP_SERVER)        as webhelp_server,
           r.get_cell(LANG$DESCRIPTION)           as description,
           r.get_cell(LANG$LANG)                  as LANG,
           r.get_row_seq()                        as row_seq
      FROM s9t_folder sf,
           TABLE(sf.s9t_file_obj.sheets) ss,
           TABLE(ss.s9t_rows) r
      WHERE sf.file_id  = I_file_id
        and ss.sheet_name = sheet_name_trans(LANG_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.iso_code := rec.iso_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_sheet,
                            rec.row_seq,
                            'ISO_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.webreports_server := rec.webreports_server ;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_sheet,
                            rec.row_seq,
                            'WEBREPORTS_SERVER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reports_server := rec.reports_server;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_sheet,
                            rec.row_seq,
                            'REPORTS_SERVER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.webhelp_server := rec.webhelp_server;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_sheet,
                            rec.row_seq,
                            'WEBHELP_SERVER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.description := rec.description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_sheet,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_LANG.action_new then
         L_temp_rec.iso_code          := NVL( l_temp_rec.iso_code,l_default_rec.iso_code);
         L_temp_rec.webreports_server := NVL( l_temp_rec.webreports_server,l_default_rec.webreports_server);
         L_temp_rec.reports_server    := NVL( l_temp_rec.reports_server,l_default_rec.reports_server);
         L_temp_rec.webhelp_server    := NVL( l_temp_rec.webhelp_server,l_default_rec.webhelp_server);
         L_temp_rec.description       := NVL( l_temp_rec.description,l_default_rec.description);
         L_temp_rec.lang              := NVL( l_temp_rec.lang,l_default_rec.lang);
      end if;
      if not (L_temp_rec.LANG is NOT NULL
              and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         LANG_sheet,
                         rec.row_seq,
                          NULL,
                          NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_lang_col.extend();
         svc_lang_col(svc_lang_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
     forall i IN 1..svc_lang_col.COUNT SAVE EXCEPTIONS
     merge into svc_lang st
     using (select(case
                   when L_mi_rec.iso_code_mi    = 'N'
                    and svc_lang_col(i).action = coresvc_lang.action_mod
                    and s1.iso_code             IS NULL
                   then mt.iso_code
                   else s1.iso_code
                   end) as iso_code,
                  (case
                   when L_mi_rec.webreports_server_mi    = 'N'
                    and svc_lang_col(i).action = coresvc_lang.action_mod
                    and s1.webreports_server             IS NULL
                   then mt.webreports_server
                   else s1.webreports_server
                   END) as webreports_server,
                 (case
                  when L_mi_rec.reports_server_mi    = 'N'
                   and svc_lang_col(i).action = coresvc_lang.action_mod
                   and s1.reports_server             IS NULL
                  then mt.reports_server
                  else s1.reports_server
                  end) as reports_server,
                (case
                 when L_mi_rec.webhelp_server_mi    = 'N'
                  and svc_lang_col(i).action = coresvc_lang.action_mod
                  and s1.webhelp_server             IS NULL
                 then mt.webhelp_server
                 else s1.webhelp_server
                 end) as webhelp_server,
                (case
                 when L_mi_rec.description_mi    = 'N'
                  and svc_lang_col(i).action = coresvc_lang.action_mod
                  and s1.description             IS NULL
                 then mt.description
                 else s1.description
                 end) as description,
               (case
                when L_mi_rec.lang_mi    = 'N'
                 and svc_lang_col(i).action = coresvc_lang.action_mod
                 and s1.lang             IS NULL
                then mt.lang
                else s1.lang
                end) as lang,
                null as dummy
           from (select svc_lang_col(i).iso_code as iso_code,
                        svc_lang_col(i).webreports_server as webreports_server,
                        svc_lang_col(i).reports_server as reports_server,
                        svc_lang_col(i).webhelp_server as webhelp_server,
                        svc_lang_col(i).description as description,
                        svc_lang_col(i).lang as lang,
                        null as dummy
                   from dual) s1,
                lang mt
          where mt.lang (+)     = s1.lang
            and 1 = 1) sq
            on( st.lang      = sq.lang
                and svc_lang_col(i).action in (coresvc_lang.action_mod,coresvc_lang.action_del))
     when matched then
        update
           set process_id        = svc_lang_col(i).process_id ,
               chunk_id          = svc_lang_col(i).chunk_id ,
               row_seq           = svc_lang_col(i).row_seq ,
               action            = svc_lang_col(i).action,
               process$status    = svc_lang_col(i).process$status ,
               reports_server    = sq.reports_server ,
               description       = sq.description ,
               iso_code          = sq.iso_code ,
               webreports_server = sq.webreports_server ,
               webhelp_server    = sq.webhelp_server ,
               create_id         = svc_lang_col(i).create_id ,
               create_datetime   = svc_lang_col(i).create_datetime ,
               last_upd_id       = svc_lang_col(i).last_upd_id ,
               last_upd_datetime = svc_lang_col(i).last_upd_datetime
     when NOT matched then
        insert(process_id ,
               chunk_id ,
               row_seq ,
               action ,
               process$status ,
               iso_code ,
               webreports_server ,
               reports_server ,
               webhelp_server ,
               description ,
               lang ,
               create_id ,
               create_datetime ,
               last_upd_id ,
               last_upd_datetime)
         values(svc_lang_col(i).process_id ,
                svc_lang_col(i).chunk_id ,
                svc_lang_col(i).row_seq ,
                svc_lang_col(i).action ,
                svc_lang_col(i).process$status ,
                sq.iso_code ,
                sq.webreports_server ,
                sq.reports_server ,
                sq.webhelp_server ,
                sq.description ,
                sq.lang ,
                svc_lang_col(i).create_id ,
                svc_lang_col(i).create_datetime ,
                svc_lang_col(i).last_upd_id ,
                svc_lang_col(i).last_upd_datetime);
   EXCEPTION
   
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
                L_error_code:=NULL;
                L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             LANG_sheet,
                             svc_lang_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_LANG;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_LANG_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id   IN   SVC_LANG_TL.PROCESS_ID%TYPE) IS

   TYPE svc_lang_TL_col_TYP IS TABLE OF SVC_LANG_TL%ROWTYPE;
   L_temp_rec         SVC_LANG_TL%ROWTYPE;
   svc_lang_TL_col    svc_lang_TL_col_typ := NEW svc_lang_TL_col_typ();
   L_process_id       SVC_LANG_TL.PROCESS_ID%TYPE;
   L_error            BOOLEAN := FALSE;
   L_default_rec      SVC_lang_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select description_mi,
             lang_lang_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'LANG_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('DESCRIPTION' as description,
                                       'LANG_LANG'   as lang_lang,
                                       'LANG'        as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_LANG_TL';
   L_pk_columns    VARCHAR2(255)  := 'Language, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select description_dv,
                      lang_lang_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'LANG_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('DESCRIPTION' as description,
                                                'LANG_LANG'   as lang_lang,
                                                'LANG'        as lang)))
   LOOP
      BEGIN
         L_default_rec.description := rec.description_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_TL_SHEET ,
                            NULL,
                           'DESCRIPTION' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang_lang := rec.lang_lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_TL_SHEET ,
                            NULL,
                           'LANG_LANG' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(lang_tl$action)        as action,
                      r.get_cell(lang_tl$description)   as description,
                      r.get_cell(lang_tl$lang_lang)     as lang_lang,
                      r.get_cell(lang_tl$lang)          as lang,
                      r.get_row_seq()                   as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(LANG_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            lang_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.description := rec.description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_TL_SHEET,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang_lang := rec.lang_lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_TL_SHEET,
                            rec.row_seq,
                            'LANG_LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LANG_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_lang.action_new then
         L_temp_rec.description := NVL( L_temp_rec.description,L_default_rec.description);
         L_temp_rec.lang_lang   := NVL( L_temp_rec.lang_lang,L_default_rec.lang_lang);
         L_temp_rec.lang        := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.lang_lang is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         LANG_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_lang_TL_col.extend();
         svc_lang_TL_col(svc_lang_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_lang_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_lang_TL st
      using(select
                  (case
                   when l_mi_rec.description_mi = 'N'
                    and svc_lang_TL_col(i).action = CORESVC_lang.action_mod
                    and s1.description IS NULL then
                        mt.description
                   else s1.description
                   end) as description,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_lang_TL_col(i).action = CORESVC_lang.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.lang_lang_mi = 'N'
                    and svc_lang_TL_col(i).action = CORESVC_lang.action_mod
                    and s1.lang_lang IS NULL then
                        mt.lang_lang
                   else s1.lang_lang
                   end) as lang_lang
              from (select svc_lang_TL_col(i).description as description,
                           svc_lang_TL_col(i).lang_lang        as lang_lang,
                           svc_lang_TL_col(i).lang              as lang
                      from dual) s1,
                   lang_TL mt
             where mt.lang_lang (+) = s1.lang_lang
               and mt.lang (+)       = s1.lang) sq
                on (st.lang_lang = sq.lang_lang and
                    st.lang = sq.lang and
                    svc_lang_TL_col(i).ACTION IN (CORESVC_lang.action_mod,CORESVC_lang.action_del))
      when matched then
      update
         set process_id        = svc_lang_TL_col(i).process_id ,
             chunk_id          = svc_lang_TL_col(i).chunk_id ,
             row_seq           = svc_lang_TL_col(i).row_seq ,
             action            = svc_lang_TL_col(i).action ,
             process$status    = svc_lang_TL_col(i).process$status ,
             description = sq.description
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             description ,
             lang_lang ,
             lang)
      values(svc_lang_TL_col(i).process_id ,
             svc_lang_TL_col(i).chunk_id ,
             svc_lang_TL_col(i).row_seq ,
             svc_lang_TL_col(i).action ,
             svc_lang_TL_col(i).process$status ,
             sq.description ,
             sq.lang_lang ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            LANG_TL_SHEET,
                            svc_lang_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_LANG_TL;
-------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_COUNT     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                  := 'CORESVC_LANG.PROCESS_S9T';
   L_file             s9t_file;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR       EXCEPTION;
   PRAGMA         EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;--to ensure that the record in s9t_folder is commited
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
       POPULATE_NAMES(I_file_id);
       sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                               L_file.user_lang);
       PROCESS_S9T_LANG(I_file_id,
                        I_process_id);

       PROCESS_S9T_LANG_TL(I_file_id,
                           I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 THEN
        L_process_status := 'PS';
    else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
            file_id    = I_file_id
      where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
    insert into s9t_errors
         values Lp_s9t_errors_tab(i);

      update svc_process_tracker
    set status = 'PE',
        file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END PROCESS_S9T;
------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_LANG_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_error           IN OUT   BOOLEAN,
                          I_rec             IN       C_SVC_LANG%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_LANG.PROCESS_LANG_VAL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_LANG';
   L_exists    VARCHAR2(1)                       := 'N';
   cursor C_STORE is
      select 'Y'
        from store s
       where s.lang = I_rec.lang;

   cursor C_SYSTEM_OPTIONS is
      select 'Y'
        from system_options so
       where so.data_integration_lang = I_rec.lang;
BEGIN

   if I_rec.action in (action_new,action_mod)
      and I_rec.webhelp_server is NOT NULL   then
      if I_rec.webhelp_server NOT LIKE 'http://%' then
         if I_rec.webhelp_server NOT LIKE '/%/' then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'WEBHELP_SERVER',
                        'MUST_BEGIN_END_/');
            O_error:= TRUE;
         end if;
      elsif I_rec.webhelp_server NOT LIKE '%/' then
         WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'WEBHELP_SERVER',
                        'MUST_END_/');
            O_error:= TRUE;
      end if;
   end if;

   if I_rec.action in (action_new,action_mod)
      and I_rec.reports_server is NOT NULL then
      if I_rec.reports_server LIKE '%/' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REPORTS_SERVER',
                     'RPTSRV_NOT_END_/');
         O_error:= TRUE;
      else
         if I_rec.reports_server LIKE '%/%' then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'REPORTS_SERVER',
                        'REPORT_SERVER_NO_/');
            O_error:= TRUE;
         end if;
      end if;
   end if;
   if I_rec.action in (action_new,action_mod)
      and I_rec.webreports_server is NOT NULL then
      if I_rec.webreports_server NOT LIKE 'http://%' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'WEBREPORTS_SERVER',
                     'WEB_REPORT_SERVER_BEGIN');
          O_error:= TRUE;
      end if;
      if I_rec.webreports_server LIKE '%/' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'WEBREPORTS_SERVER',
                     'WEBRPT_SRV_END');
         O_error:= TRUE;
      end if;
      if I_rec.webreports_server LIKE '%?' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'WEBREPORTS_SERVER',
                     'WEB_REPORT_SERVER_END_?');
         O_error:= TRUE;
      end if;
   end if;

   if I_rec.action=action_del
      and I_rec.pk_lang_rid is NOT NULL then
      open C_STORE;
      fetch C_STORE into L_exists;
      close C_STORE;
      if L_exists = 'Y' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'LANG',
                     'CANNOT_DEL_LANG_STORE');
         O_error:= TRUE;
      end if;
      L_exists := 'N';
      open C_SYSTEM_OPTIONS;
      fetch C_SYSTEM_OPTIONS into L_exists;
      close C_SYSTEM_OPTIONS;
      if L_exists = 'Y' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'LANG',
                     'CANNOT_DEL_LANG_SYS_OPTN');
         O_error:= TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_STORE%ISOPEN then
        close C_STORE;
     end if;
     if C_SYSTEM_OPTIONS%ISOPEN then
        close C_SYSTEM_OPTIONS;
     end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_LANG_VAL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_LANG_INS( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_lang_temp_rec     IN       LANG%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_LANG.EXEC_LANG_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_LANG';
BEGIN
   insert
     into lang
   values I_lang_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_LANG_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_LANG_UPD( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_lang_temp_rec     IN       LANG%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_LANG.EXEC_LANG_INS';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_LANG';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LANG_LOCK is
      select 'X'
       from lang
      where lang = I_lang_temp_rec.lang
        for update nowait;
BEGIN
   open C_LANG_LOCK;
   close C_LANG_LOCK;
   update lang
      set row = I_lang_temp_rec
    where 1 = 1
      and lang = I_lang_temp_rec.lang;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_lang_temp_rec.lang,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LANG_LOCK%ISOPEN then
         close C_LANG_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_LANG_UPD;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_LANG_DEL( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_lang_temp_rec     IN       LANG%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_LANG.EXEC_LANG_INS';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_LANG';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   cursor C_LANG_LOCK is
      select 'X'
       from lang
      where lang = I_lang_temp_rec.lang
        for update nowait;

   cursor C_LANG_TL_LOCK is
      select 'X'
       from lang_tl
      where lang = I_lang_temp_rec.lang
        for update nowait;
BEGIN
   open C_LANG_TL_LOCK;
   close C_LANG_TL_LOCK;
   delete
     from lang_tl
    where lang = I_lang_temp_rec.lang;

   open C_LANG_LOCK;
   close C_LANG_LOCK;
   delete
     from lang
    where lang = I_lang_temp_rec.lang;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_lang_temp_rec.lang,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LANG_LOCK%ISOPEN then
         close C_LANG_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_LANG_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LANG_TL_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_lang_tl_ins_tab    IN       LANG_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_LANG.EXEC_LANG_TL_INS';

BEGIN
   if I_lang_tl_ins_tab is NOT NULL and I_lang_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_lang_tl_ins_tab.COUNT()
         insert into lang_tl
              values I_lang_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_LANG_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LANG_TL_UPD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_lang_tl_upd_tab   IN       LANG_TAB,
                          I_lang_tl_upd_rst   IN       ROW_SEQ_TAB,
                          I_process_id        IN       SVC_LANG_TL.PROCESS_ID%TYPE,
                          I_chunk_id          IN       SVC_LANG_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_LANG.EXEC_LANG_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LANG_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_LANG_TL_UPD(I_lang_lang   LANG_TL.LANG_LANG%TYPE,
                             I_lang        LANG_TL.LANG%TYPE) is
      select 'x'
        from lang_tl
       where lang_lang = I_lang_lang
         and lang = I_lang
         for update nowait;

BEGIN
   if I_lang_tl_upd_tab is NOT NULL and I_lang_tl_upd_tab.count > 0 then
      for i in I_lang_tl_upd_tab.FIRST..I_lang_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_lang_tl_upd_tab(i).lang);
            L_key_val2 := 'Language: '||to_char(I_lang_tl_upd_tab(i).lang_lang);
            open C_LOCK_LANG_TL_UPD(I_lang_tl_upd_tab(i).lang_lang,
                                    I_lang_tl_upd_tab(i).lang);
            close C_LOCK_LANG_TL_UPD;
            
            update lang_tl
               set description = I_lang_tl_upd_tab(i).description,
                   last_update_id = I_lang_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_lang_tl_upd_tab(i).last_update_datetime
             where lang = I_lang_tl_upd_tab(i).lang
               and lang_lang = I_lang_tl_upd_tab(i).lang_lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_LANG_TL',
                           I_lang_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_LANG_TL_UPD%ISOPEN then
         close C_LOCK_LANG_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_LANG_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LANG_TL_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_lang_tl_del_tab   IN       LANG_TAB,
                          I_lang_tl_del_rst   IN       ROW_SEQ_TAB,
                          I_process_id        IN       SVC_LANG_TL.PROCESS_ID%TYPE,
                          I_chunk_id          IN       SVC_LANG_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_LANG.EXEC_LANG_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LANG_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_LANG_TL_DEL(I_lang_lang  LANG_TL.LANG_LANG%TYPE,
                             I_lang       LANG_TL.LANG%TYPE) is
      select 'x'
        from lang_tl
       where lang_lang = I_lang_lang
         and lang = I_lang
         for update nowait;

BEGIN
   if I_lang_tl_del_tab is NOT NULL and I_lang_tl_del_tab.count > 0 then
      for i in I_lang_tl_del_tab.FIRST..I_lang_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_lang_tl_del_tab(i).lang);
            L_key_val2 := 'Language: '||to_char(I_lang_tl_del_tab(i).lang_lang);
            open C_LOCK_LANG_TL_DEL(I_lang_tl_del_tab(i).lang_lang,
                                     I_lang_tl_del_tab(i).lang);
            close C_LOCK_LANG_TL_DEL;
            
            delete lang_tl
             where lang = I_lang_tl_del_tab(i).lang
               and lang_lang = I_lang_tl_del_tab(i).lang_lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_LANG_TL',
                           I_lang_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_LANG_TL_DEL%ISOPEN then
         close C_LOCK_LANG_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_LANG_TL_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_LANG( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id      IN       SVC_LANG.PROCESS_ID%TYPE,
                       I_chunk_id        IN       SVC_LANG.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                      := 'CORESVC_LANG.PROCESS_LANG';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_LANG';
   L_error           BOOLEAN;
   L_process_error   BOOLEAN                           := FALSE;
   L_lang_temp_rec   LANG%ROWTYPE;

BEGIN
   FOR rec IN C_SVC_LANG(I_process_id,
                         I_chunk_id)
   LOOP
      L_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.pk_lang_rid is NOT NULL
         and rec.lang is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                     'DUP_RECORD');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.pk_lang_rid is NULL
         and rec.lang is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                     'LANG_EXIST');
         L_error :=TRUE;
      end if;

      if rec.action in (action_new,action_mod)
         and rec.description  IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DESCRIPTION',
                     'ENTER_DESC');
         L_error :=TRUE;
      end if;

      if rec.action in (action_new,action_mod)
         and rec.iso_code  IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'ISO_CODE',
                    'ENTER_ISO_CODE');
         L_error :=TRUE;
      end if;

       if PROCESS_LANG_VAL( O_error_message,
                             L_error,
                             rec) =FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_lang_temp_rec.iso_code              := rec.iso_code;
         L_lang_temp_rec.webreports_server     := rec.webreports_server;
         L_lang_temp_rec.reports_server        := rec.reports_server;
         L_lang_temp_rec.webhelp_server        := rec.webhelp_server;
         L_lang_temp_rec.description           := rec.description;
         L_lang_temp_rec.lang                  := rec.lang;
         if rec.action = action_new then
            if EXEC_LANG_INS( O_error_message,
                           L_lang_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_LANG_UPD( O_error_message,
                           L_lang_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_LANG_DEL( O_error_message,
                           L_lang_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_LANG;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_LANG_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id     IN       SVC_LANG_TL.PROCESS_ID%TYPE,
                          I_chunk_id       IN       SVC_LANG_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_LANG.PROCESS_LANG_TL';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_LANG_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LANG_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LANG';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_lang_TL_temp_rec     LANG_TL%ROWTYPE;
   L_lang_tl_upd_rst      ROW_SEQ_TAB;
   L_lang_tl_del_rst      ROW_SEQ_TAB;

   cursor C_SVC_LANG_TL(I_process_id NUMBER,
                        I_chunk_id NUMBER) is
      select pk_lang_tl.rowid  as pk_lang_tl_rid,
             fk_lang.rowid     as fk_lang_rid,
             fk_lang2.rowid     as fk_lang2_rid,
             st.lang,
             st.lang_lang,
             st.description,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_lang_tl  st,
             lang         fk_lang,
             lang         fk_lang2,
             lang_tl      pk_lang_tl
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.lang        =  pk_lang_tl.lang (+)
         and st.lang_lang   =  pk_lang_tl.lang_lang (+)
         and st.lang        =  fk_lang.lang (+)
         and st.lang_lang   =  fk_lang2.lang(+);

   TYPE SVC_lang_TL is TABLE OF C_SVC_LANG_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_lang_tab        SVC_LANG_TL;

   L_lang_TL_ins_tab         LANG_TAB         := NEW LANG_TAB();
   L_lang_TL_upd_tab         LANG_TAB         := NEW LANG_TAB();
   L_lang_TL_del_tab         LANG_TAB         := NEW LANG_TAB();

BEGIN
   if C_SVC_LANG_TL%ISOPEN then
      close C_SVC_LANG_TL;
   end if;

   open C_SVC_LANG_TL(I_process_id,
                       I_chunk_id);
   LOOP
      fetch C_SVC_LANG_TL bulk collect into L_svc_lang_tab limit LP_bulk_fetch_limit;
      if L_svc_lang_tab.COUNT > 0 then
         FOR i in L_svc_lang_tab.FIRST..L_svc_lang_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_lang_tab(i).action is NULL
               or L_svc_lang_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_lang_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_lang_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_lang_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_lang_tab(i).action = action_new
               and L_svc_lang_tab(i).pk_lang_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_lang_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_lang_tab(i).action IN (action_mod, action_del)
               and L_svc_lang_tab(i).lang is NOT NULL
               and L_svc_lang_tab(i).lang_lang is NOT NULL
               and L_svc_lang_tab(i).pk_lang_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_lang_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            -- check if the lang_lang exists in the LANG table
            if L_svc_lang_tab(i).action = action_new
               and L_svc_lang_tab(i).lang_lang is NOT NULL
               and L_svc_lang_tab(i).fk_lang2_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_lang_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            
            if L_svc_lang_tab(i).action = action_new
               and L_svc_lang_tab(i).lang is NOT NULL 
               and L_svc_lang_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_lang_tab(i).row_seq,
                           'LANG',
                           'LANGUAGE_NOT_FOUND');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_lang_tab(i).action in (action_new, action_mod) then
               if L_svc_lang_tab(i).description is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_lang_tab(i).row_seq,
                              'DESCRIPTION',
                              'ENTER_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_lang_tab(i).lang_lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_lang_tab(i).row_seq,
                           'LANG_LANG',
                           'MUST_ENTER_FIELDS');
               L_error :=TRUE;
            end if;

            if L_svc_lang_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_lang_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_lang_TL_temp_rec.lang := L_svc_lang_tab(i).lang;
               L_lang_TL_temp_rec.lang_lang := L_svc_lang_tab(i).lang_lang;
               L_lang_TL_temp_rec.description := L_svc_lang_tab(i).description;
               L_lang_TL_temp_rec.create_datetime := SYSDATE;
               L_lang_TL_temp_rec.create_id := GET_USER;
               L_lang_TL_temp_rec.last_update_datetime := SYSDATE;
               L_lang_TL_temp_rec.last_update_id := GET_USER;

               if L_svc_lang_tab(i).action = action_new then
                  L_lang_TL_ins_tab.extend;
                  L_lang_TL_ins_tab(L_lang_TL_ins_tab.count()) := L_lang_TL_temp_rec;
               end if;

               if L_svc_lang_tab(i).action = action_mod then
                  L_lang_TL_upd_tab.extend;
                  L_lang_TL_upd_tab(L_lang_TL_upd_tab.count()) := L_lang_TL_temp_rec;
                  L_lang_tl_upd_rst(L_lang_TL_upd_tab.count()) := L_svc_lang_tab(i).row_seq;
               end if;

               if L_svc_lang_tab(i).action = action_del then
                  L_lang_TL_del_tab.extend;
                  L_lang_TL_del_tab(L_lang_TL_del_tab.count()) := L_lang_TL_temp_rec;
                  L_lang_tl_del_rst(L_lang_TL_del_tab.count()) := L_svc_lang_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_LANG_TL%NOTFOUND;
   END LOOP;
   close C_SVC_LANG_TL;

   if EXEC_LANG_TL_INS(O_error_message,
                       L_lang_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_LANG_TL_UPD(O_error_message,
                       L_lang_TL_upd_tab,
                       L_lang_tl_upd_rst,
                       I_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_LANG_TL_DEL(O_error_message,
                       L_lang_TL_del_tab,
                       L_lang_tl_del_rst,
                       I_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_LANG_TL;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete
     from svc_lang_tl
    where process_id = I_process_id;

   delete
     from svc_lang
    where process_id = I_process_id;
END;
---------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64):='CORESVC_LANG.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_LANG(O_error_message,
                   I_process_id,
                   I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_LANG_TL(O_error_message,
                      I_process_id,
                      I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
        set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
            action_date = SYSDATE
      where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
---------------------------------------------------------------------------
END CORESVC_LANG;
/