SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,	CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 4 LANG, 'TRCO' CODE_TYPE, 'B' CODE, 'Reservado' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
COMMIT;
