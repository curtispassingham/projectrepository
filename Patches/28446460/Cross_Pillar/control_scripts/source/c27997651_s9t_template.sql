--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'S9T_TEMPLATE'
UPDATE S9T_TEMPLATE SET TEMPLATE_TYPE='RSAADM' WHERE TEMPLATE_KEY IN('SA_ERROR_CODES_DATA','SA_REFERENCE_FIELD_DATA','POS_TENDER_TYPE_HEAD_DATA','SA_CONSTANTS_DATA','SA_STORE_DATA');
UPDATE S9T_TEMPLATE SET TEMPLATE_TYPE='RSAFIN' WHERE TEMPLATE_KEY IN('SA_ROUNDING_RULE_DATA','SA_FIF_GL_CROSS_REF_DATA');
/

COMMIT
/

