-- day_level_calendar_data.sql
-- This script is used to seed the day_level_calendar table.
   delete from day_level_calendar;
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
   insert into DAY_LEVEL_CALENDAR (day, eow_date, sow_date,eom_date)
      select input.calc_date,
             (min(c.first_day) - 1) - (7 * trunc(((min(c.first_day)-1) - (input.calc_date)) / 7)) as eow_date,
             (min(c.first_day) - 1) - (7 * trunc(((min(c.first_day)-1) - (input.calc_date)) / 7))-6 as sow_date,
             min(c.first_day) - 1 as eom_date
        from calendar c,
             (select date_range.min_date - 1 + rownum as calc_date
                from all_objects,
                     (select min(first_day) min_date,  
                             max(first_day + (7 * no_of_weeks)) max_date 
                        from calendar) date_range
               where date_range.min_date - 1 + rownum <= date_range.max_date) input
       where c.first_day > input.calc_date
       group by input.calc_date;
   --
   merge into DAY_LEVEL_CALENDAR target
   using (select day.day,
                 --
                 (day.day - day.day_first_day) - (((trunc(((day.day - day.day_first_day)/7))+1)-1)*7)+1 day_date_454_day,
                 trunc(((day.day - day.day_first_day) / 7)) + 1 day_date_454_week,
                 day.day_date_454_month day_date_454_month,
                 day.day_date_454_year day_date_454_year,
                 --
                 7 eow_date_454_day,
                 trunc(((week.eow_date - week.eow_first_day) / 7)) + 1 eow_date_454_week,
                 week.eow_date_454_month eow_date_454_month,
                 week.eow_date_454_year eow_date_454_year,
                 --
                 7 eom_date_454_day,
                 trunc(((month.eom_date - month.eom_first_day) / 7)) + 1 eom_date_454_week,
                 month.eom_date_454_month eom_date_454_month,
                 month.eom_date_454_year eom_date_454_year
           from (select inner_month.day,
                        inner_month.eom_date,
                        inner_month.eom_first_day,
                        inner_cal_eom.month_454 eom_date_454_month,
                        inner_month.eom_date_454_year
                   from (select dlc.day,
                                dlc.eom_date,
                                max(cal_eom.first_day) eom_first_day,
                                max(cal_eom.year_454)  eom_date_454_year
                           from DAY_LEVEL_CALENDAR dlc,
                                calendar cal_eom
                          where cal_eom.first_day      <= dlc.eom_date
                          group by dlc.day,
                                   dlc.eom_date) inner_month,
                        calendar inner_cal_eom
                  where inner_month.eom_first_day = inner_cal_eom.first_day) month,
                (select inner_month.day,
                        inner_month.eow_date,
                        inner_month.eow_first_day,
                        inner_cal_eow.month_454 eow_date_454_month,
                        inner_month.eow_date_454_year
                   from (select dlc.day,
                                dlc.eow_date,
                                max(cal_eow.first_day) eow_first_day,
                                max(cal_eow.year_454)  eow_date_454_year
                           from DAY_LEVEL_CALENDAR dlc,
                                calendar cal_eow
                          where cal_eow.first_day      <= dlc.eow_date
                          group by dlc.day,
                                   dlc.eow_date) inner_month,
                        calendar inner_cal_eow
                  where inner_month.eow_first_day= inner_cal_eow.first_day) week,
                (select inner_month.day,
                        inner_month.day_first_day,
                        inner_cal_day.month_454 day_date_454_month,
                        inner_month.day_date_454_year
                   from (select dlc.day,
                                max(cal_eow.first_day) day_first_day,
                                max(cal_eow.year_454)  day_date_454_year
                           from DAY_LEVEL_CALENDAR dlc,
                                calendar cal_eow
                          where cal_eow.first_day      <= dlc.day
                          group by dlc.day) inner_month,
                        calendar inner_cal_day
                  where inner_month.day_first_day= inner_cal_day.first_day) day
           where month.day = week.day
             and month.day = day.day) use_this
   on (    target.day = use_this.day)
   when matched then
   update set target.day_date_454_day         = use_this.day_date_454_day,
              target.day_date_454_week        = use_this.day_date_454_week,
              target.day_date_454_month       = use_this.day_date_454_month,
              target.day_date_454_year        = use_this.day_date_454_year,
              target.eow_date_454_day         = use_this.eow_date_454_day,
              target.eow_date_454_week        = use_this.eow_date_454_week,
              target.eow_date_454_month       = use_this.eow_date_454_month,
              target.eow_date_454_year        = use_this.eow_date_454_year,
              target.eom_date_454_day         = use_this.eom_date_454_day,
              target.eom_date_454_week        = use_this.eom_date_454_week,
              target.eom_date_454_month       = use_this.eom_date_454_month,
              target.eom_date_454_year        = use_this.eom_date_454_year;
              
-----------------------------------------------------------------------------------------------------------------------------------------------------
--Added by Operational Insights Team
   merge INTO DAY_LEVEL_CALENDAR target
   USING (SELECT target.day                        AS DAY,
                 MCAL_PERIOD_TMP.MCAL_YEAR         AS MCAL_YEAR,
                 MCAL_PERIOD_TMP.MCAL_PERIOD       AS MCAL_PERIOD,
                 MCAL_PERIOD_TMP.MCAL_PERIOD_ST_DT AS MCAL_PERIOD_ST_DT,
                 MCAL_PERIOD_TMP.NUMBER_OF_WEEKS   AS NUMBER_OF_WEEKS,
                 MCAL_PERIOD_TMP.MCAL_PERIOD_NAME  AS MCAL_PERIOD_NAME,
                 (CASE WHEN MCAL_PERIOD_TMP.MCAL_PERIOD >=1 AND MCAL_PERIOD_TMP.MCAL_PERIOD  <= 3
                          THEN 1
                       WHEN MCAL_PERIOD_TMP.MCAL_PERIOD >=4 AND MCAL_PERIOD_TMP.MCAL_PERIOD  <= 6
                          THEN 2
                       WHEN MCAL_PERIOD_TMP.MCAL_PERIOD >=7 AND MCAL_PERIOD_TMP.MCAL_PERIOD  <= 9
                          THEN 3
                       WHEN MCAL_PERIOD_TMP.MCAL_PERIOD >=10 AND MCAL_PERIOD_TMP.MCAL_PERIOD  <= 12
                          THEN 4
                 END) AS MCAL_QTR,
                 MCAL_PERIOD_TMP.MCAL_PERIOD_END_DT AS MCAL_PERIOD_END_DT
            FROM (SELECT CASE WHEN start_of_half_month > 0
                                 THEN TO_CHAR(DECODE(SIGN(CALENDAR.month_454 - SYSTEM_OPTIONS.start_of_half_month),
                                                     -1,CALENDAR.year_454 - 1,
                                                     CALENDAR.year_454))
                              WHEN start_of_half_month < 0
                                 THEN TO_CHAR(DECODE(SIGN(CALENDAR.month_454 + SYSTEM_OPTIONS.start_of_half_month),
                                                     -1,CALENDAR.year_454,
                                                     CALENDAR.year_454 + 1))
                         END AS MCAL_YEAR,
                         CASE WHEN start_of_half_month > 0
                                 THEN TO_CHAR(DECODE(SIGN(CALENDAR.month_454 - SYSTEM_OPTIONS.start_of_half_month+1),
                                                     1,ABS(CALENDAR.month_454 - SYSTEM_OPTIONS.start_of_half_month+1),
                                                     -1,ABS(CALENDAR.month_454 - SYSTEM_OPTIONS.start_of_half_month + 13),12))
                              WHEN start_of_half_month < 0
                                 THEN TO_CHAR(MOD((CALENDAR.month_454 + ABS(ABS(SYSTEM_OPTIONS.start_of_half_month) -12)),12)+1)
                         END AS MCAL_PERIOD,          
                         CALENDAR.first_day AS MCAL_PERIOD_ST_DT,
                         TO_CHAR(CALENDAR.NO_OF_WEEKS) AS NUMBER_OF_WEEKS,
                         CASE WHEN start_of_half_month > 0
                                 THEN 'Period'||LPAD(TO_CHAR(DECODE(SIGN(CALENDAR.month_454 - SYSTEM_OPTIONS.start_of_half_month+1),
                                                                    1,ABS(CALENDAR.month_454 - SYSTEM_OPTIONS.start_of_half_month+1),
                                                                    -1,ABS(CALENDAR.month_454 - SYSTEM_OPTIONS.start_of_half_month + 13),12)), 2, '0')
                              WHEN start_of_half_month < 0
                                 THEN 'Period' ||LPAD(TO_CHAR(MOD((CALENDAR.month_454 + ABS(ABS(SYSTEM_OPTIONS.start_of_half_month) -12)),12)+1), 2, '0')
                         END AS MCAL_PERIOD_NAME,
                         (CALENDAR.first_day + CALENDAR.NO_OF_WEEKS*7 - 1) AS MCAL_PERIOD_END_DT
                    FROM CALENDAR,
                         SYSTEM_OPTIONS) MCAL_PERIOD_TMP,
                 DAY_LEVEL_CALENDAR target
           WHERE target.day BETWEEN MCAL_PERIOD_TMP.MCAL_PERIOD_ST_DT
             AND MCAL_PERIOD_TMP.MCAL_PERIOD_END_DT)use_this
   ON (target.day=use_this.day)
   WHEN matched THEN
     UPDATE SET target.mcal_year          = use_this.mcal_year,
                target.mcal_period        = use_this.mcal_period,
                target.mcal_period_st_dt  = use_this.mcal_period_st_dt,
                target.mcal_period_end_dt = use_this.mcal_period_end_dt,
                target.mcal_qtr           = use_this.mcal_qtr,
                target.MCAL_PERIOD_NAME   = use_this.MCAL_PERIOD_NAME,
                target.NUMBER_OF_WEEKS    = use_this.NUMBER_OF_WEEKS;

-----------------------------------------------------------------------------------------------------------------------------------------------------
--Added by Operational Insights Team
   merge INTO DAY_LEVEL_CALENDAR target
   USING (SELECT DAY,
                 MCAL_YEAR,
                 MCAL_PERIOD,
                 MCAL_QTR,
                 NUMBER_OF_WEEKS
            FROM DAY_LEVEL_CALENDAR) use_this
   ON(target.day=use_this.day)
   WHEN matched THEN
      UPDATE SET target.period_key  = (target.mcal_year*100 + mcal_Period),
                 target.quarter_key = (target.mcal_year*10 + mcal_qtr),
                 target.date_key = (TO_NUMBER (TO_CHAR (day, 'yyyymmdd'))),
                 target.week_key    = 100000000 + MCAL_YEAR * 10000 + MCAL_PERIOD * 100 + 
                                      (CASE
                                         WHEN ((DAY - MCAL_PERIOD_ST_DT)+1) <=7
                                            THEN 1
                                         WHEN ((DAY - MCAL_PERIOD_ST_DT)+1) <=14
                                            THEN 2
                                         WHEN ((DAY - MCAL_PERIOD_ST_DT)+1) <=21
                                            THEN 3
                                         WHEN ((DAY - MCAL_PERIOD_ST_DT)+1) <=28
                                            THEN 4
                                            ELSE 5
                                       END);

commit;


