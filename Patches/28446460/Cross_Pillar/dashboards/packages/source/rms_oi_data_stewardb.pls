create or replace PACKAGE BODY RMS_OI_DATA_STEWARD AS
---------------------------------------------------------------------------------------------------
FUNCTION SETUP_INCOMPLETE_ITEMS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id             IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                I_depts                  IN     OBJ_NUMERIC_ID_TABLE,
                                I_classes                IN     OBJ_NUMERIC_ID_TABLE,
                                I_subclasses             IN     OBJ_NUMERIC_ID_TABLE,
                                I_supplier_sites         IN     OBJ_NUMERIC_ID_TABLE,
                                I_origin_countries       IN     OBJ_VARCHAR_ID_TABLE,
                                I_date                   IN     OBJ_VARCHAR_ID_TABLE,
                                I_locations              IN     OBJ_NUMERIC_ID_TABLE,
                                I_item_type              IN     OBJ_VARCHAR_ID_TABLE,
                                I_item_level             IN     OBJ_NUMERIC_ID_TABLE,
                                I_transaction_level      IN     OBJ_NUMERIC_ID_TABLE,
                                I_uda                    IN     OBJ_NUMERIC_ID_TABLE,
                                I_uda_value              IN     OBJ_NUMERIC_ID_TABLE,
                                I_brands                 IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS

   L_program                        VARCHAR2(61) := 'RMS_OI_DATA_STEWARD.SETUP_INCOMPLETE_ITEMS';
   L_all_supplier_sites             VARCHAR2(1)  := null;
   L_all_origin_countries           VARCHAR2(1)  := null;
   L_all_locations                  VARCHAR2(1)  := null;
   L_all_item_type                  VARCHAR2(1)  := null;
   L_all_item_level                 VARCHAR2(1)  := null;
   L_all_transaction_level          VARCHAR2(1)  := null;
   L_all_uda                        VARCHAR2(1)  := null;
   L_all_uda_value                  VARCHAR2(1)  := null;
   L_all_brands                     VARCHAR2(1)  := null;
   L_all_date                       VARCHAR2(1)  := RMS_OI_DATA_STEWARD.NO_IND;
   L_date                           DATE;
   L_start_date                     DATE;
   L_end_date                       DATE;
   L_check_days                     NUMBER(3);
   L_oi_system_opt_rec              RMS_OI_SYSTEM_OPTIONS%ROWTYPE;
   L_rpm_ind                        VARCHAR2(1);
   L_primary_currency               SYSTEM_OPTIONS.CURRENCY_CODE%TYPE;
   L_consolidation_ind              SYSTEM_OPTIONS.CONSOLIDATION_IND%TYPE;
   L_start_time                     TIMESTAMP    := SYSTIMESTAMP;
   cursor C_RPM_IND  is
      select RPM_IND
        from system_options;


   cursor C_CHECK_DAYS is
      select *
        from rms_oi_system_options;

BEGIN
   ---

   if I_supplier_sites is not NULL and I_supplier_sites.count > 0 then
      L_all_supplier_sites := RMS_OI_DATA_STEWARD.NO_IND;
   else
      L_all_supplier_sites := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   if I_origin_countries is not NULL and I_origin_countries.count > 0 then
      L_all_origin_countries := RMS_OI_DATA_STEWARD.NO_IND;
   else
      L_all_origin_countries := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   if I_locations is not NULL and I_locations.count > 0 then
      L_all_locations := RMS_OI_DATA_STEWARD.NO_IND;
   else
      L_all_locations := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   if I_item_type is not NULL and I_item_type.count > 0 then
      L_all_item_type := RMS_OI_DATA_STEWARD.NO_IND;
   else
      L_all_item_type := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   if I_item_level is not NULL and I_item_level.count > 0 then
      L_all_item_level := RMS_OI_DATA_STEWARD.NO_IND;
   else
      L_all_item_level := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   if I_transaction_level is not NULL and I_transaction_level.count > 0 then
      L_all_transaction_level := RMS_OI_DATA_STEWARD.NO_IND;
   else
      L_all_transaction_level := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   if I_brands is not NULL and I_brands.count > 0 then
      L_all_brands := RMS_OI_DATA_STEWARD.NO_IND;
   else
      L_all_brands := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   if I_uda is not NULL and I_uda.count > 0 then
      L_all_uda := RMS_OI_DATA_STEWARD.NO_IND;
   else
      L_all_uda := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   if I_date is not null and I_date.count > 0 then 
      if I_date.count > 2 then
         return OI_UTILITY.SUCCESS;
      end if;
      ---
      if I_date.count = 2 then
         L_start_date :=  to_date(I_date(I_date.first),'yyyy-mm-dd');
         L_end_date   :=  to_date(I_date(I_date.last),'yyyy-mm-dd');

         if L_start_date > L_end_date then
            return OI_UTILITY.SUCCESS;
         end if;
         if L_start_date = L_end_date then
            L_date := L_start_date;
         end if;
      end if;

      if I_date.count = 1 then
         L_date := to_date(I_date(I_date.first),'yyyy-mm-dd');
      end if;
   else
      L_all_date := RMS_OI_DATA_STEWARD.YES_IND;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_CHECK_DAYS',
                    'RMS_OI_SYSTEM_OPTIONS',
                    NULL);
   open C_CHECK_DAYS;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_CHECK_DAYS',
                    'RMS_OI_SYSTEM_OPTIONS',
                    NULL);
   fetch C_CHECK_DAYS into L_oi_system_opt_rec;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_CHECK_DAYS',
                    'RMS_OI_SYSTEM_OPTIONS',
                    NULL);
   close C_CHECK_DAYS;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_RPM_IND',
                    'SYSTEM_OPTIONS',
                    NULL);
   open C_RPM_IND;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_RPM_IND',
                    'SYSTEM_OPTIONS',
                    NULL);
   fetch C_RPM_IND into L_rpm_ind;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_RPM_IND',
                    'SYSTEM_OPTIONS',
                    NULL);
   close C_RPM_IND;
   ---
   select currency_code,
          consolidation_ind
     into L_primary_currency,
          L_consolidation_ind
     from system_options;

   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||' Start');

   -- insert merch hierarchy (subclasses) into gtt_6_num_6_str_6_date
   --  number_1   --dept
   --  number_2   --class
   --  number_3   --subclass
   if OI_UTILITY.SETUP_MERCH_HIER(O_error_message,
                                  I_session_id,
                                  I_depts,
                                  I_classes,
                                  I_subclasses
                                  ) = OI_UTILITY.FAILURE then
     return OI_UTILITY.FAILURE;
   end if;

   -- Clear the gtt table.
   delete from gtt_incomp_items;
   
   -- Clear the working table rms_oi_data_stwrd_incomp_items for the same session_id
   if RMS_OI_DATA_STEWARD.DELETE_SESSION_INCOMP_ITEMS(O_error_message,
                                                      I_session_id) = OI_UTILITY.FAILURE then
       return OI_UTILITY.FAILURE;
   end if;

   -- Insert items into gtt_incomp_items
   insert into gtt_incomp_items (item,
                                 item_parent,
                                 item_desc,
                                 status,
                                 dept,
                                 dept_name,
                                 class,
                                 class_name,
                                 subclass,
                                 subclass_name,
                                 create_date,
                                 create_id,
                                 pack_ind,
                                 item_level,
                                 tran_level,
                                 display_ind,
                                 ref_items ,
                                 vat,
                                 simple_pack,
                                 uda,
                                 location,
                                 seasons,
                                 replenishment,
                                 subs_items,
                                 dimensions,
                                 related_items,
                                 ticket,
                                 hts,
                                 import_attr,
                                 images,
                                 selling_retail)
                          select vim.item,
                                 vim.item_parent,
                                 vim.item_desc,
                                 vim.status,
                                 vim.dept,
                                 sbc.varchar2_1,
                                 vim.class,
                                 sbc.varchar2_2,
                                 vim.subclass,
                                 sbc.varchar2_3,
                                 trunc(vim.create_datetime),
                                 vim.create_id,
                                 vim.pack_ind,
                                 vim.item_level,
                                 vim.tran_level,
                                 'Y',
                                 decode(nvl(dopt.ds_show_incomp_item_ref_item,L_oi_system_opt_rec.ds_show_incomp_item_ref_item),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_vat,L_oi_system_opt_rec.ds_show_incomp_item_vat),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_spack,L_oi_system_opt_rec.ds_show_incomp_item_spack),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_uda,L_oi_system_opt_rec.ds_show_incomp_item_uda),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_loc,L_oi_system_opt_rec.ds_show_incomp_item_loc),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_seasons,L_oi_system_opt_rec.ds_show_incomp_item_seasons),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_repl,L_oi_system_opt_rec.ds_show_incomp_item_repl),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_subs_item,L_oi_system_opt_rec.ds_show_incomp_item_subs_item),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_dimen,L_oi_system_opt_rec.ds_show_incomp_item_dimen),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_rel_item,L_oi_system_opt_rec.ds_show_incomp_item_rel_item),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_tickets,L_oi_system_opt_rec.ds_show_incomp_item_tickets),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_hts,L_oi_system_opt_rec.ds_show_incomp_item_hts),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_imp_attr,L_oi_system_opt_rec.ds_show_incomp_item_imp_attr),'R',0,'O',990,'N',999),
                                 decode(nvl(dopt.ds_show_incomp_item_images,L_oi_system_opt_rec.ds_show_incomp_item_images),'R',0,'O',990,'N',999),
                                 decode(L_rpm_ind,'N',curr_selling_unit_retail,null)
                            from gtt_6_num_6_str_6_date sbc,
                                 v_item_master vim,
                                 rms_oi_dept_options dopt
                           where vim.dept         = sbc.number_1
                             and vim.class        = sbc.number_2
                             and vim.subclass     = sbc.number_3
                             and dopt.dept(+)     = vim.dept
                             and ((L_all_date = RMS_OI_DATA_STEWARD.YES_IND) or (L_date is not null and trunc(vim.create_datetime) = L_date)
                              or (L_start_date <= trunc(vim.create_datetime) and L_end_date >= trunc(vim.create_datetime))
                                 )
                             and trunc(vim.create_datetime) <= get_vdate - nvl(dopt.ds_days_after_item_create,L_oi_system_opt_rec.ds_days_after_item_create)
                             and vim.item_level   <= vim.tran_level               
                             and ((L_all_item_level = RMS_OI_DATA_STEWARD.YES_IND ) or (vim.item_level in (select value(input_item_level)
                                                                                                             from table(cast(I_item_level as OBJ_NUMERIC_ID_TABLE)) input_item_level)))

                             and ((L_all_transaction_level = RMS_OI_DATA_STEWARD.YES_IND) or (vim.tran_level in (select value(input_tran_level)
                                                                                                                   from table(cast(I_transaction_level as OBJ_NUMERIC_ID_TABLE)) input_tran_level)))
               
                             and ((L_all_supplier_sites = RMS_OI_DATA_STEWARD.YES_IND ) or exists(select 1
                                                                                                    from table(cast(I_supplier_sites as OBJ_NUMERIC_ID_TABLE)) input_sups,
                                                                                                         item_supplier isp
                                                                                                   where vim.item = isp.item
                                                                                                     and isp.supplier = value(input_sups)))
                             and ((L_all_origin_countries = RMS_OI_DATA_STEWARD.YES_IND ) or exists (select 1
                                                                                                       from table(cast(I_origin_countries as OBJ_VARCHAR_ID_TABLE)) input_ctrys,
                                                                                                            item_supp_country isu
                                                                                                      where vim.item = isu.item
                                                                                                        and value(input_ctrys) = isu.origin_country_id))
                             and ((L_all_locations = RMS_OI_DATA_STEWARD.YES_IND) or exists(select 1
                                                                                              from table(cast(I_locations as OBJ_NUMERIC_ID_TABLE)) input_locs,
                                                                                                   item_loc il
                                                                                             where il.item = vim.item
                                                                                               and value(input_locs) = il.loc ))
                             and ((L_all_brands = RMS_OI_DATA_STEWARD.YES_IND )or exists (select 1
                                                                                            from table(cast(I_brands as OBJ_VARCHAR_ID_TABLE)) input_brands
                                                                                            where vim.brand_name = value(input_brands)))
                             and ((L_all_uda = RMS_OI_DATA_STEWARD.YES_IND ) or exists (select 1
                                                                                          from table(cast(I_uda as OBJ_NUMERIC_ID_TABLE)) input_uda,
                                                                                               uda_item_ff uf
                                                                                         where uf.item   = vim.item
                                                                                           and uf.uda_id = value(input_uda)
                                                                                         union all
                                                                                         select 1
                                                                                           from table(cast(I_uda as OBJ_NUMERIC_ID_TABLE)) input_uda,
                                                                                                uda_item_date ud
                                                                                          where ud.item   = vim.item
                                                                                            and ud.uda_id = value(input_uda) ))
                            and ((L_all_uda = RMS_OI_DATA_STEWARD.YES_IND ) or exists (select 1
                                                                                         from table(cast(I_uda as OBJ_NUMERIC_ID_TABLE)) input_uda,
                                                                                              table(cast(I_uda_value as OBJ_NUMERIC_ID_TABLE)) input_uda_value,
                                                                                              uda_item_lov ul
                                                                                        where ul.item = vim.item
                                                                                          and ul.uda_id = value(input_uda)
                                                                                          and ul.uda_value = nvl(value(input_uda_value),ul.uda_value) ))
                            and not exists (select 1
                                              from daily_purge
                                             where upper(table_name)='ITEM_MASTER'
                                               and (key_value = vim.item
                                                or key_value = vim.item_parent
                                                or key_value = vim.item_grandparent)
                                           )
                            and ((L_all_item_type = RMS_OI_DATA_STEWARD.YES_IND ) or ((case when vim.sellable_ind IN ('Y','N')
                                                                                             and vim.orderable_ind      = 'Y'
                                                                                             and vim.inventory_ind      = 'Y'
                                                                                             and vim.pack_ind           = 'Y'
                                                                                             and vim.simple_pack_ind    = 'Y'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and vim.deposit_item_type  IS NULL then 'S'

                                                                                            when vim.sellable_ind  IN ('Y','N')
                                                                                             and vim.orderable_ind IN ('Y','N')
                                                                                             and vim.inventory_ind      = 'Y'
                                                                                             and vim.pack_ind           = 'Y'
                                                                                             and vim.simple_pack_ind    = 'N'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and (vim.deposit_item_type  IS NULL
                                                                                              or vim.deposit_item_type  = 'P') then 'C'

                                                                                            when vim.sellable_ind       = 'Y'
                                                                                             and vim.orderable_ind      = 'Y'
                                                                                             and vim.inventory_ind      = 'Y'
                                                                                             and vim.pack_ind           = 'N'
                                                                                             and vim.simple_pack_ind    = 'N'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and vim.deposit_item_type  = 'E' then 'E'

                                                                                            when  vim.sellable_ind       = 'Y'
                                                                                             and vim.orderable_ind      = 'N'
                                                                                             and vim.inventory_ind      = 'N'
                                                                                             and vim.pack_ind           = 'N'
                                                                                             and vim.simple_pack_ind    = 'N'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and vim.deposit_item_type  = 'A' then  'A'

                                                                                            when vim.sellable_ind       IN ('Y','N')
                                                                                             and vim.orderable_ind      = 'Y'
                                                                                             and vim.inventory_ind      = 'Y'
                                                                                             and vim.pack_ind           = 'N'
                                                                                             and vim.simple_pack_ind    = 'N'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and vim.deposit_item_type  = 'Z' then 'Z'

                                                                                            when vim.sellable_ind       = 'Y'
                                                                                             and vim.orderable_ind      = 'N'
                                                                                             and vim.inventory_ind      = 'Y'
                                                                                             and vim.pack_ind           = 'N'
                                                                                             and vim.simple_pack_ind    = 'N'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and vim.deposit_item_type  = 'T' then 'T'

                                                                                            when vim.sellable_ind       = 'Y'
                                                                                             and vim.orderable_ind      = 'N'
                                                                                             and vim.inventory_ind      = 'N'
                                                                                             and vim.pack_ind           = 'N'
                                                                                             and vim.simple_pack_ind    = 'N'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and vim.deposit_item_type  IS NULL then 'I'

                                                                                            when vim.sellable_ind       = 'N'
                                                                                             and vim.orderable_ind      = 'Y'
                                                                                             and vim.inventory_ind      = 'Y'
                                                                                             and vim.pack_ind           = 'N'
                                                                                             and vim.simple_pack_ind    = 'Y'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and vim.deposit_item_type  IS NULL then 'O'

                                                                                            when vim.sellable_ind       = 'Y'
                                                                                             and vim.orderable_ind      = 'N'
                                                                                             and vim.inventory_ind      = 'N'
                                                                                             and vim.pack_ind           = 'N'
                                                                                             and vim.simple_pack_ind    = 'N'
                                                                                             and vim.item_xform_ind     = 'Y'
                                                                                             and vim.deposit_item_type  IS NULL then 'L'

                                                                                            when vim.sellable_ind       IN ('Y','N')
                                                                                             and vim.orderable_ind      IN ('Y','N')
                                                                                             and vim.inventory_ind      IN ('Y','N')
                                                                                             and vim.pack_ind           = 'N'
                                                                                             and vim.simple_pack_ind    = 'N'
                                                                                             and vim.item_xform_ind     = 'N'
                                                                                             and vim.deposit_item_type  IS NULL then 'R'

                                                                                            else NULL
                                                                                            end ) in (select value(input_item_type)
                                                                                                        from table(cast(I_item_type as OBJ_VARCHAR_ID_TABLE)) input_item_type )));

   -- Exclude the items that are force marked as complete. Those items are saved in rms_oi_data_stwrd_cmpltd_items table.
   delete from gtt_incomp_items gtt where exists (select 1
                                                    from rms_oi_data_stwrd_cmpltd_items cmp
                                                   where cmp.completed = RMS_OI_DATA_STEWARD.YES_IND
                                                     and cmp.item = gtt.item);

   -- Retain only tran level and one level above tran level items.
   delete from gtt_incomp_items gtt where gtt.item_level = 1 and gtt.tran_level = 3;

   -- Remove the parent items that do not have child items in the working gtt table matching the filter criteria.
   delete from gtt_incomp_items gtt where gtt.item_level < gtt.tran_level and not exists (select 1 from gtt_incomp_items itm where itm.item_parent = gtt.item);

   merge into gtt_incomp_items target
   using (  select im.item item,
                   im.item_parent item_parent,
                   im.item_desc item_desc,
                   im.status status,
                   im.dept dept,
                   null dept_desc,
                   im.class class,
                   null class_desc,
                   im.subclass subclass,
                   null subclass_desc,
                   trunc(im.create_datetime) create_datetime,
                   im.create_id create_id,
                   im.pack_ind pack_ind,
                   im.item_level item_level,
                   im.tran_level tran_level,
                   'N' ,
                   decode(nvl(dopt.ds_show_incomp_item_ref_item,L_oi_system_opt_rec.ds_show_incomp_item_ref_item),'R',0,'O',990,'N',999) ref_items,
                   decode(nvl(dopt.ds_show_incomp_item_vat,L_oi_system_opt_rec.ds_show_incomp_item_vat),'R',0,'O',990,'N',999) vat,
                   decode(nvl(dopt.ds_show_incomp_item_spack,L_oi_system_opt_rec.ds_show_incomp_item_spack),'R',0,'O',990,'N',999) simple_pack,
                   decode(nvl(dopt.ds_show_incomp_item_uda,L_oi_system_opt_rec.ds_show_incomp_item_uda),'R',0,'O',990,'N',999) uda,
                   decode(nvl(dopt.ds_show_incomp_item_loc,L_oi_system_opt_rec.ds_show_incomp_item_loc),'R',0,'O',990,'N',999) location,
                   decode(nvl(dopt.ds_show_incomp_item_seasons,L_oi_system_opt_rec.ds_show_incomp_item_seasons),'R',0,'O',990,'N',999) seasons,
                   decode(nvl(dopt.ds_show_incomp_item_repl,L_oi_system_opt_rec.ds_show_incomp_item_repl),'R',0,'O',990,'N',999) replenishment,
                   decode(nvl(dopt.ds_show_incomp_item_subs_item,L_oi_system_opt_rec.ds_show_incomp_item_subs_item),'R',0,'O',990,'N',999) subs_items,
                   decode(nvl(dopt.ds_show_incomp_item_dimen,L_oi_system_opt_rec.ds_show_incomp_item_dimen),'R',0,'O',990,'N',999) dimensions,
                   decode(nvl(dopt.ds_show_incomp_item_rel_item,L_oi_system_opt_rec.ds_show_incomp_item_rel_item),'R',0,'O',990,'N',999) related_items,
                   decode(nvl(dopt.ds_show_incomp_item_tickets,L_oi_system_opt_rec.ds_show_incomp_item_tickets),'R',0,'O',990,'N',999) ticket,
                   decode(nvl(dopt.ds_show_incomp_item_hts,L_oi_system_opt_rec.ds_show_incomp_item_hts),'R',0,'O',990,'N',999) hts,
                   decode(nvl(dopt.ds_show_incomp_item_imp_attr,L_oi_system_opt_rec.ds_show_incomp_item_imp_attr),'R',0,'O',990,'N',999) import_attr,
                   decode(nvl(dopt.ds_show_incomp_item_images,L_oi_system_opt_rec.ds_show_incomp_item_images),'R',0,'O',990,'N',999) images
              from item_master im,
                   rms_oi_dept_options dopt,
                   gtt_incomp_items gtt
             where gtt.item_level < gtt.tran_level
               and gtt.item = im.item_parent
               and gtt.dept = dopt.dept(+)
         ) use_this
     on (target.item = use_this.item )
     when not matched then
           insert
                  (target.item,
                   target.item_parent,
                   target.item_desc,
                   target.status,
                   target.dept,
                   target.dept_name,
                   target.class,
                   target.class_name,
                   target.subclass,
                   target.subclass_name,
                   target.create_date,
                   target.create_id,
                   target.pack_ind,
                   target.item_level,
                   target.tran_level,
                   target.display_ind,
                   target.ref_items ,
                   target.vat,
                   target.simple_pack,
                   target.uda,
                   target.location,
                   target.seasons,
                   target.replenishment,
                   target.subs_items,
                   target.dimensions,
                   target.related_items,
                   target.ticket,
                   target.hts,
                   target.import_attr,
                   target.images)
          values ( use_this.item,
                   use_this.item_parent,
                   use_this.item_desc,
                   use_this.status,
                   use_this.dept,
                   use_this.dept_desc,
                   use_this.class,
                   use_this.class_desc,
                   use_this.subclass,
                   use_this.subclass_desc,
                   use_this.create_datetime,
                   use_this.create_id,
                   use_this.pack_ind,
                   use_this.item_level,
                   use_this.tran_level,
                   'N',
                   use_this.ref_items ,
                   use_this.vat,
                   use_this.simple_pack,
                   use_this.uda,
                   use_this.location,
                   use_this.seasons,
                   use_this.replenishment,
                   use_this.subs_items,
                   use_this.dimensions,
                   use_this.related_items,
                   use_this.ticket,
                   use_this.hts,
                   use_this.import_attr,
                   use_this.images);


   merge into gtt_incomp_items target using
   ( select (decode(al.ref_items,100,al.ref_items,0) + decode(al.vat,100,al.vat,0) + decode(al.simple_pack,100,al.simple_pack,0) + decode(al.uda,100,al.uda,0)
             + decode(al.location,100,al.location,0) + decode(al.seasons,100,al.seasons,0) + decode(al.replenishment,100,al.replenishment,0) + decode(al.subs_items,100,al.subs_items,0)
             + decode(al.dimensions,100,al.dimensions,0) + decode(al.related_items,100,al.related_items,0) + decode(al.ticket,100,al.ticket,0) + decode(al.hts,100,al.hts,0)
             + decode(al.import_attr,100,al.import_attr,0) + decode(al.images,100,al.images,0)
             )  /
            (decode(al.ref_items,0,1,100,1,0) + decode(al.vat,0,1,100,1,0) + decode(al.simple_pack,0,1,100,1,0) + decode(al.uda,0,1,100,1,0)
            + decode(al.location,0,1,100,1,0) + decode(al.seasons,0,1,100,1,0) + decode(al.replenishment,0,1,100,1,0) + decode(al.subs_items,0,1,100,1,0)
            + decode(al.dimensions,0,1,100,1,0) + decode(al.related_items,0,1,100,1,0) + decode(al.ticket,0,1,100,1,0) + decode(al.hts,0,1,100,1,0)
            + decode(al.import_attr,0,1,100,1,0) + decode(al.images,0,1,100,1,0)
            ) reqd_completion,
              al.ref_items ,
              al.vat,
              al.simple_pack,
              al.uda,
              al.location,
              al.seasons,
              al.replenishment,
              al.subs_items,
              al.dimensions,
              al.related_items,
              al.ticket,
              al.hts,
              al.import_attr,
              al.images,
              al.item

       from (select nvl((case when ref_items in (0,990) then
                                   (select decode(ref_items,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from item_master im
                                                    where im.item_parent = vim.item
                                                      and im.primary_ref_item_ind = RMS_OI_DATA_STEWARD.YES_IND)) else ref_items end),ref_items) ref_items,

                    nvl((case when vat in (0,990) then
                                   (select decode(vat,0,100,990,991)
                                      from dual
                                      where exists (select 1
                                                      from vat_item vt
                                                     where vt.item = vim.item)) else vat end),vat) vat,

                    nvl((case when simple_pack in (0,990) then
                                   (select decode(vat,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from item_master im,
                                                          packitem pi
                                                    where pi.item = vim.item
                                                      and pi.pack_no =im.item
                                                      and im.simple_pack_ind = RMS_OI_DATA_STEWARD.YES_IND)) else simple_pack end),simple_pack) simple_pack,

                    nvl((case when uda in (0,990) then
                                   (select decode(uda,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from uda_item_lov ul
                                                    where ul.item = vim.item
                                                      and rownum = 1
                                                   union all
                                                   select 1
                                                     from uda_item_date ud
                                                    where ud.item = vim.item
                                                      and rownum = 1
                                                   union all
                                                   select 1
                                                     from uda_item_ff uf
                                                    where uf.item = vim.item
                                                      and rownum = 1)) else uda end) ,uda) uda,

                    nvl((case when location in (0,990) then
                                  (select decode(location,0,100,990,991)
                                     from dual
                                    where exists (select 1
                                                    from item_loc il
                                                    where il.item = vim.item)) else location end),location) location,

                    nvl((case when seasons in (0,990) then
                                   (select decode(seasons,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from item_seasons ims
                                                    where ims.item = vim.item)) else seasons end) ,seasons) seasons,
                   
                    nvl((case when replenishment in (0,990) then
                                   (select decode(replenishment,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from repl_item_loc repl
                                                    where repl.item = vim.item)) else replenishment end),replenishment) replenishment,

                    nvl((case when subs_items in (0,990) then
                                   (select decode(subs_items,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from sub_items_detail sbs
                                                    where sbs.item = vim.item)) else subs_items end),subs_items) subs_items,

                    nvl((case when dimensions in (0,990) then
                                   (select decode(dimensions,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from item_supp_country_dim isd
                                                    where isd.item = vim.item)) else dimensions end),dimensions) dimensions,

                    nvl((case when related_items in (0,990) then
                                   (select decode(related_items,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from related_item_head rh,
                                                          related_item_detail rd
                                                    where rd.relationship_id = rh.relationship_id
                                                      and rh.item = vim.item)) else related_items end),related_items) related_items,

                    nvl((case when ticket in (0,990) then
                                   (select decode(ticket,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from item_ticket it
                                                    where it.item = vim.item)) else ticket end),ticket) ticket,

                    nvl((case when hts in (0,990) then
                                   (select decode(hts,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from item_hts ih
                                                     where ih.item = vim.item)) else hts end),hts) hts,

                    nvl((case when import_attr in (0,990) then
                                   (select decode(import_attr,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from item_import_attr ima
                                                     where ima.item = vim.item))else import_attr end),import_attr) import_attr,

                    nvl((case when images in (0,990) then
                                   (select decode(images,0,100,990,991)
                                      from dual
                                     where exists (select 1
                                                     from item_image img
                                                     where img.item = vim.item))else images end),images)images,
                    vim.item item
             from  gtt_incomp_items vim
            where  vim.item_level = vim.tran_level)al
   )use_this
   on (use_this.item = target.item)
   when matched then update
    set target.reqd_completion = use_this.reqd_completion,
        target.ref_items = use_this.ref_items,
        target.vat = use_this.vat,
        target.simple_pack = use_this.simple_pack,
        target.uda = use_this.uda,
        target.location = use_this.location,
        target.seasons = use_this.seasons,
        target.replenishment = use_this.replenishment,
        target.subs_items = use_this.subs_items,
        target.dimensions = use_this.dimensions,
        target.related_items = use_this.related_items,
        target.ticket = use_this.ticket,
        target.hts = use_this.hts,
        target.import_attr = use_this.import_attr,
        target.images = use_this.images;

   merge into gtt_incomp_items target using
   (
     select (decode(itm.ref_items,0,al.ref_items,0) + decode(itm.vat,0,al.vat,0) + decode(itm.simple_pack,0,al.simple_pack,0) + decode(itm.uda,0,al.uda,0)
             + decode(itm.location,0,al.location,0) + decode(itm.seasons,0,al.seasons,0) + decode(itm.replenishment,0,al.replenishment,0) + decode(itm.subs_items,0,al.subs_items,0)
             + decode(itm.dimensions,0,al.dimensions,0) + decode(itm.related_items,0,al.related_items,0) + decode(itm.ticket,0,al.ticket,0) + decode(itm.hts,0,al.hts,0)
             + decode(itm.import_attr,0,al.import_attr,0) + decode(itm.images,0,al.images,0)
             )  /
            (decode(itm.ref_items,0,1,0) + decode(itm.vat,0,1,0) + decode(itm.simple_pack,0,1,0) + decode(itm.uda,0,1,0)
            + decode(itm.location,0,1,0) + decode(itm.seasons,0,1,0) + decode(itm.replenishment,0,1,0) + decode(itm.subs_items,0,1,0)
            + decode(itm.dimensions,0,1,0) + decode(itm.related_items,0,1,0) + decode(itm.ticket,0,1,0) + decode(itm.hts,0,1,0)
            + decode(itm.import_attr,0,1,0) + decode(itm.images,0,1,0)
            ) reqd_completion,
              al.ref_items ,
            al.vat,
            al.simple_pack,
            al.uda,
            al.location,
            al.seasons,
            al.replenishment,
            al.subs_items,
            al.dimensions,
            al.related_items,
            al.ticket,
            al.hts,
            al.import_attr,
            al.images,
            al.item
       from (
                select (case count(item) when count(decode(ref_items,100,1,991,1)) then 100
                                         when count(decode(ref_items,0,1,990,1)) then 0
                                         when count(decode(ref_items,999,1)) then 999
                                         else 50 end) ref_items,
                       (case count(item) when count(decode(vat,100,1,991,1)) then 100
                                         when count(decode(vat,0,1,990,1)) then 0
                                         when count(decode(vat,999,1)) then 999
                                         else 50 end) vat,
                       (case count(item) when count(decode(uda,100,1,991,1)) then 100
                                         when count(decode(uda,0,1,990,1)) then 0
                                         when count(decode(uda,999,1)) then 999
                                         else 50 end) uda,
                       (case count(item) when count(decode(location,100,1,991,1)) then 100
                                         when count(decode(location,0,1,990,1)) then 0
                                         when count(decode(location,999,1)) then 999
                                         else 50 end) location,
                       (case count(item) when count(decode(seasons,100,1,991,1)) then 100
                                         when count(decode(seasons,0,1,990,1)) then 0
                                         when count(decode(seasons,999,1)) then 999
                                         else 50 end) seasons,
                       (case count(item) when count(decode(replenishment,100,1,991,1)) then 100
                                         when count(decode(replenishment,0,1,990,1)) then 0
                                         when count(decode(replenishment,999,1)) then 999
                                         else 50 end) replenishment,
                       (case count(item) when count(decode(subs_items,100,1,991,1)) then 100
                                         when count(decode(subs_items,0,1,990,1)) then 0
                                         when count(decode(subs_items,999,1)) then 999
                                         else 50 end) subs_items,
                       (case count(item) when count(decode(dimensions,100,1,991,1)) then 100
                                         when count(decode(dimensions,0,1,990,1)) then 0
                                         when count(decode(dimensions,999,1)) then 999
                                         else 50 end) dimensions,
                       (case count(item) when count(decode(related_items,100,1,991,1)) then 100
                                         when count(decode(related_items,0,1,990,1)) then 0
                                         when count(decode(related_items,999,1)) then 999
                                         else 50 end) related_items,
                       (case count(item) when count(decode(ticket,100,1,991,1)) then 100
                                         when count(decode(ticket,0,1,990,1)) then 0
                                         when count(decode(ticket,999,1)) then 999
                                         else 50 end) ticket,
                       (case count(item) when count(decode(simple_pack,100,1,991,1)) then 100
                                         when count(decode(simple_pack,0,1,990,1)) then 0
                                         when count(decode(simple_pack,999,1)) then 999
                                         else 50 end) simple_pack,
                       (case count(item) when count(decode(hts,100,1,991,1)) then 100
                                         when count(decode(hts,0,1,990,1)) then 0
                                         when count(decode(hts,999,1)) then 999
                                         else 50 end) hts,
                       (case count(item) when count(decode(import_attr,100,1,991,1)) then 100
                                         when count(decode(import_attr,0,1,990,1)) then 0
                                         when count(decode(import_attr,999,1)) then 999
                                         else 50 end) import_attr,
                       (case count(item) when count(decode(images,100,1,991,1)) then 100
                                         when count(decode(images,0,1,990,1)) then 0
                                         when count(decode(images,999,1)) then 999
                                         else 50 end) images,
                       vim.item_parent item
                  from gtt_incomp_items vim
                 where vim.item_level = vim.tran_level
                 group by vim.item_parent) al,
           gtt_incomp_items itm
           where itm.item = al.item ) use_this
   on (use_this.item = target.item)
   when matched then update
    set target.reqd_completion = use_this.reqd_completion,
        target.ref_items = use_this.ref_items,
        target.vat = use_this.vat,
        target.simple_pack = use_this.simple_pack,
        target.uda = use_this.uda,
        target.location = use_this.location,
        target.seasons = use_this.seasons,
        target.replenishment = use_this.replenishment,
        target.subs_items = use_this.subs_items,
        target.dimensions = use_this.dimensions,
        target.related_items = use_this.related_items,
        target.ticket = use_this.ticket,
        target.hts = use_this.hts,
        target.import_attr = use_this.import_attr,
        target.images = use_this.images;

   -- Exclude items that are approved and completed.
   delete from  gtt_incomp_items where status = 'A' and reqd_completion = 100;

   merge into gtt_incomp_items target using
   (select isp.supplier prim_supplier,
           su.sup_name prim_supplier_name,
           isc.origin_country_id prim_country,
           isc.unit_cost unit_cost,
           su.currency_code sup_currency,
           isp.vpn vpn,
           gtt.item item
      from gtt_incomp_items gtt,
           item_supplier isp,
           item_supp_country isc ,
           v_sups su
     where gtt.display_ind = 'Y'
       and gtt.item = isp.item(+)
       and isp.primary_supp_ind(+) = 'Y'
       and isp.item = isc.item(+)
       and isp.supplier = isc.supplier(+)
       and isc.primary_supp_ind(+) = 'Y'
       and isc.primary_country_ind (+) = 'Y'
       and isp.supplier = su.supplier(+)) use_this
   on (use_this.item = target.item)
   when matched then update
    set target.prim_supplier = use_this.prim_supplier,
        target.prim_supplier_name = use_this.prim_supplier_name,
        target.prim_country = use_this.prim_country,
        target.unit_cost = use_this.unit_cost,
        target.sup_currency = use_this.sup_currency,
        target.vpn = use_this.vpn;

   if L_rpm_ind = 'Y' then

      merge into gtt_incomp_items target using
      (select gtt.item,
              avg(riz.selling_retail * curr_conv.exchange_rate) selling_retail
         from gtt_incomp_items gtt,
              rpm_item_zone_price riz,
              rpm_zone z,
              rpm_zone_group g,
              rpm_merch_retail_def_expl d,
              (select from_currency,
                      to_currency,
                      effective_date,
                      exchange_rate,
                      exchange_type
                from (select from_currency,
                             to_currency,
                             effective_date,
                             exchange_rate,
                             exchange_type,
                             rank() over (partition by from_currency,
                                                       to_currency
                                              order by decode(exchange_type, 'P', 1, 'C', 2, 'O', 3) asc,
                                                                     effective_date desc) date_rank
                        from mv_currency_conversion_rates
                       where effective_date <= get_vdate
                         and ((L_consolidation_ind = 'Y' and exchange_type in('P', 'C', 'O'))
                               or
                               (L_consolidation_ind = 'N' and exchange_type in('P', 'O')))
                      )
                where date_rank = 1
               ) curr_conv
       where gtt.display_ind = 'Y'
         and gtt.item = riz.item
         and gtt.dept = d.dept
         and gtt.class = d.class
         and gtt.subclass = d.subclass
         and g.zone_group_id = d.regular_zone_group
         and g.zone_group_id = z.zone_group_id
         and z.base_ind = 1
         and riz.zone_id = z.zone_id         
         and curr_conv.from_currency = riz.selling_retail_currency
         and curr_conv.to_currency = gtt.sup_currency
         group by gtt.item
      )use_this
      on (use_this.item = target.item)
    when matched then update
     set target.selling_retail = use_this.selling_retail;

   end if;

   -- Insert items from gtt into the main working table.
   insert all
   when item_level = tran_level then
   into rms_oi_data_stwrd_incomp_items(session_id,
                                       reqd_completion,
                                       item_parent,
                                       item,
                                       item_desc,
                                       status,
                                       dept,
                                       dept_name,
                                       class,
                                       class_name,
                                       subclass,
                                       subclass_name,
                                       create_date,
                                       create_id,
                                       prim_supplier,
                                       prim_supplier_name,
                                       prim_country,
                                       unit_cost,
                                       selling_retail,
                                       sup_currency,
                                       vpn,
                                       pack_ind,
                                       ref_items,
                                       vat,
                                       simple_pack,
                                       uda,
                                       location,
                                       seasons,
                                       replenishment,
                                       subs_items,
                                       dimensions,
                                       related_items,
                                       ticket,
                                       hts,
                                       import_attr,
                                       images
                                       )
                               values (session_id,
                                       reqd_completion,
                                       item_parent,
                                       item,
                                       item_desc,
                                       status,
                                       dept,
                                       dept_name,
                                       class,
                                       class_name,
                                       subclass,
                                       subclass_name,
                                       create_date,
                                       create_id,
                                       prim_supplier,
                                       prim_supplier_name,
                                       prim_country,
                                       unit_cost,
                                       selling_retail,
                                       sup_currency,
                                       vpn,
                                       pack_ind,
                                       ref_items,
                                       vat,
                                       simple_pack,
                                       uda,
                                       location,
                                       seasons,
                                       replenishment,
                                       subs_items,
                                       dimensions,
                                       related_items,
                                       ticket,
                                       hts,
                                       import_attr,
                                       images
                                       )
   when item_level < tran_level then
    into rms_oi_data_stwrd_incomp_items(session_id,
                                       reqd_completion,
                                       item_parent,
                                       item,
                                       item_desc,
                                       status,
                                       dept,
                                       dept_name,
                                       class,
                                       class_name,
                                       subclass,
                                       subclass_name,
                                       create_date,
                                       create_id,
                                       prim_supplier,
                                       prim_supplier_name,
                                       prim_country,
                                       unit_cost,
                                       selling_retail,
                                       sup_currency,
                                       vpn,
                                       pack_ind,
                                       ref_items,
                                       vat,
                                       simple_pack,
                                       uda,
                                       location,
                                       seasons,
                                       replenishment,
                                       subs_items,
                                       dimensions,
                                       related_items,
                                       ticket,
                                       hts,
                                       import_attr,
                                       images
                                       )
                               values (session_id,
                                       reqd_completion,
                                       item,
                                       null,
                                       item_desc,
                                       status,
                                       dept,
                                       dept_name,
                                       class,
                                       class_name,
                                       subclass,
                                       subclass_name,
                                       create_date,
                                       create_id,
                                       prim_supplier,
                                       prim_supplier_name,
                                       prim_country,
                                       unit_cost,
                                       selling_retail,
                                       sup_currency,
                                       vpn,
                                       pack_ind,
                                       ref_items,
                                       vat,
                                       simple_pack,
                                       uda,
                                       location,
                                       seasons,
                                       replenishment,
                                       subs_items,
                                       dimensions,
                                       related_items,
                                       ticket,
                                       hts,
                                       import_attr,
                                       images
                                       )
                                select I_session_id session_id,
                                       gtt.reqd_completion reqd_completion,
                                       gtt.item_parent item_parent,
                                       gtt.item item,
                                       gtt.item_desc item_desc,
                                       gtt.status status,
                                       gtt.dept dept,
                                       gtt.dept_name dept_name,
                                       gtt.class class,
                                       gtt.class_name class_name,
                                       gtt.subclass subclass,
                                       gtt.subclass_name subclass_name,
                                       gtt.create_date create_date,
                                       gtt.create_id create_id,
                                       gtt.prim_supplier prim_supplier,
                                       gtt.prim_supplier_name prim_supplier_name,
                                       gtt.prim_country prim_country,
                                       gtt.unit_cost unit_cost,
                                       gtt.selling_retail selling_retail,
                                       gtt.sup_currency sup_currency,
                                       gtt.vpn vpn,
                                       gtt.pack_ind pack_ind,
                                       gtt.ref_items ref_items,
                                       gtt.vat vat,
                                       gtt.simple_pack simple_pack,
                                       gtt.uda uda,
                                       gtt.location location,
                                       gtt.seasons seasons,
                                       gtt.replenishment replenishment,
                                       gtt.subs_items subs_items,
                                       gtt.dimensions dimensions,
                                       gtt.related_items related_items,
                                       gtt.ticket ticket,
                                       gtt.hts hts,
                                       gtt.import_attr import_attr,
                                       gtt.images images,
                                       gtt.item_level item_level,
                                       gtt.tran_level tran_level
                                  from gtt_incomp_items gtt
                                 where gtt.display_ind = 'Y';

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_INCOMPLETE_ITEMS;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_RECORDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_session_id    IN     oi_session_id_log.session_id%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_DATA_STEWARD.DELETE_SESSION_RECORDS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;
   
BEGIN

   delete from rms_oi_data_stwrd_incomp_items where session_id = I_session_id;

   if OI_UTILITY.DELETE_SESSION_ID_LOG(O_error_message,
                                       I_session_id) = OI_UTILITY.FAILURE then
      return OI_UTILITY.FAILURE;
   end if;

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      OI_UTILITY.HANDLE_ERRORS(O_error_message,
                               L_program);
      return OI_UTILITY.FAILURE;
END DELETE_SESSION_RECORDS;
---------------------------------------------------------------------------------------------------
FUNCTION DELETE_SESSION_INCOMP_ITEMS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_session_id         IN     OI_SESSION_ID_LOG.SESSION_ID%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'RMS_OI_DATA_STEWARD.DELETE_SESSION_INCOMP_ITEMS';
   L_start_time          TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   delete from rms_oi_data_stwrd_incomp_items where session_id =  I_session_id;
   
   LOGGER.LOG_INFORMATION(L_program||' session_id: '||I_session_id||
                          ' delete rms_oi_data_stwrd_incomp_items - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   OI_UTILITY.LOG_TIME(L_program||' session_id: '||I_session_id, L_start_time);
   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OI_UTILITY.PROGRAM_ERROR then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
   when OTHERS then
      OI_UTILITY.HANDLE_ERRORS(O_error_message, L_program);
      RETURN OI_UTILITY.FAILURE;
END DELETE_SESSION_INCOMP_ITEMS;
---------------------------------------------------------------------------------------------------
FUNCTION SETUP_MARK_COMPLETE_ITEMS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_session_id             IN OI_SESSION_ID_LOG.SESSION_ID%TYPE,
                                   I_items                  IN OBJ_VARCHAR_ID_TABLE,
                                   I_parent_items           IN OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS
   L_program             VARCHAR2(61) := 'RMS_OI_DATA_STEWARD.SETUP_MARK_COMPLETE_ITEMS';
BEGIN

   -- Insert the child items in the completed table after excluding the duplicate items, if any.
   merge into rms_oi_data_stwrd_cmpltd_items target using
   (        select distinct value(complete_item) item
              from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) complete_item ) use_this
   on (use_this.item = target.item)
     when not matched then insert(item,
                                  completed)
                           values(use_this.item,
                                  'Y');
   
   -- Insert the child items of the passed in parent item in the completed table.
   merge into rms_oi_data_stwrd_cmpltd_items target using
   (      select distinct ctm.item item                          
            from item_master itm,
                 table(cast(I_parent_items as OBJ_VARCHAR_ID_TABLE)) ptm,
                 rms_oi_data_stwrd_incomp_items ctm
           where itm.item_parent = value(ptm)
             and itm.item = ctm.item             
             and ctm.session_id = I_session_id ) use_this
    on (use_this.item = target.item)
      when not matched then insert(item,
                                   completed)
                            values(use_this.item,
                                  'Y'); 
                                  

   return OI_UTILITY.SUCCESS;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return OI_UTILITY.FAILURE;
END SETUP_MARK_COMPLETE_ITEMS;
---------------------------------------------------------------------------------------------------
END RMS_OI_DATA_STEWARD;
/
