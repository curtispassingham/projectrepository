--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  TABLE ALTERED:   GTT_10_NUM_10_STR_10_DATE
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Alter Table
--------------------------------------
PROMPT Altering Table 'GTT_10_NUM_10_STR_10_DATE'
ALTER TABLE GTT_10_NUM_10_STR_10_DATE
      MODIFY(VARCHAR2_4                VARCHAR2(2000) )
/

COMMENT ON COLUMN GTT_10_NUM_10_STR_10_DATE.VARCHAR2_4 IS 'string value 4';
/
