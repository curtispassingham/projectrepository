--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RMS_OI_SYSTEM_OPTIONS'
PROMPT deleting CONSTRAINT 'CHK_B_PO_PENDING_APPROVAL_LVL'
DECLARE
  L_cons_exists number := 0;
BEGIN
  SELECT count(*) INTO L_cons_exists
    FROM USER_CONSTRAINTS
   WHERE CONSTRAINT_NAME = 'CHK_B_PO_PENDING_APPROVAL_LVL'
     AND CONSTRAINT_TYPE = 'C';

  if (L_cons_exists != 0) then
      execute immediate 'ALTER TABLE RMS_OI_SYSTEM_OPTIONS DROP CONSTRAINT CHK_B_PO_PENDING_APPROVAL_LVL';
  end if;
end;
/

UPDATE RMS_OI_SYSTEM_OPTIONS SET B_PO_PENDING_APPROVAL_LEVEL ='S'
/

COMMIT
/

ALTER TABLE RMS_OI_SYSTEM_OPTIONS MODIFY B_PO_PENDING_APPROVAL_LEVEL VARCHAR2 (1) DEFAULT 'S' NOT NULL
/

COMMENT ON COLUMN RMS_OI_SYSTEM_OPTIONS.B_PO_PENDING_APPROVAL_LEVEL is 'When set to S only submitted Purchase Orders will be fetched. When set to W then both worksheet and submitted Purchase Orders will be fetched.'
/

PROMPT ADDING CONSTRAINT 'CHK_B_PO_PENDING_APPROVAL_LVL'
ALTER TABLE RMS_OI_SYSTEM_OPTIONS ADD CONSTRAINT
 CHK_B_PO_PENDING_APPROVAL_LVL CHECK (B_PO_PENDING_APPROVAL_LEVEL IN ('W','S'))
/
