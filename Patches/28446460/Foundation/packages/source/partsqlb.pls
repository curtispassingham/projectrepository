CREATE OR REPLACE PACKAGE BODY PARTNER_SQL AS
-------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message   IN OUT   VARCHAR2,
                  O_partner_desc    IN OUT   partner.partner_desc%TYPE,
                  I_partner_id      IN       partner.partner_id%TYPE,
                  I_partner_type    IN       partner.partner_type%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50)    :=      'PARTNER_SQL.GET_DESC';
   L_code_desc   code_detail.code_desc%TYPE;
   ---
   cursor C_GET_PARTNER_DESC is
      select partner_desc
        from v_partner_tl
       where partner_id = I_partner_id
         and partner_type = I_partner_type;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_PARTNER_DESC','V_PARTNER_TL',
                       'partner_id: '||I_partner_id);
   open  C_GET_PARTNER_DESC;
   SQL_LIB.SET_MARK('FETCH','C_GET_PARTNER_DESC','V_PARTNER_TL',
                       'partner_id: '||I_partner_id);
   fetch C_GET_PARTNER_DESC into O_partner_desc;
   if C_GET_PARTNER_DESC%NOTFOUND then
      if LANGUAGE_SQL.GET_CODE_DESC (O_error_message,
                                     'PTAL',
                                     I_partner_type,
                                     L_code_desc) = FALSE then
         return FALSE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('NO_ACTIVE_PARTNER',I_partner_id,L_code_desc,NULL);
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_PARTNER_DESC','V_PARTNER_TL',
                          'partner_id: '||I_partner_id);
      close C_GET_PARTNER_DESC;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_PARTNER_DESC','V_PARTNER_TL',
                          'partner_id: '||I_partner_id);
   close C_GET_PARTNER_DESC;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_DESC;
-------------------------------------------------------------------------------
FUNCTION GET_INFO(O_error_message          IN OUT   VARCHAR2,
                  O_partner_desc           IN OUT   partner.partner_desc%TYPE,
                  O_currency_code          IN OUT   partner.currency_code%TYPE,
                  O_status                 IN OUT   partner.status%TYPE,
                  O_principle_country_id   IN OUT   partner.principle_country_id%TYPE,
                  O_mfg_id                 IN OUT   partner.mfg_id%TYPE,
                  O_lang                   IN OUT   partner.lang%TYPE,
                  I_partner_id             IN       partner.partner_id%TYPE,
                  I_partner_type           IN       partner.partner_type%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)        := 'PARTNER_SQL.GET_INFO';
   ---
   cursor C_GET_PARTNER_INFO is
      select ptl.partner_desc,
             p.currency_code,
             p.status,
             p.principle_country_id,
             p.mfg_id,
             p.lang
        from partner p, 
             v_partner_tl ptl
       where p.partner_id = I_partner_id
         and p.partner_type = I_partner_type
         and p.partner_type = ptl.partner_type
         and p.partner_id = ptl.partner_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_PARTNER_INFO','PARTNER, V_PARTNER_TL',
                       'partner_id: '||I_partner_id);
   open  C_GET_PARTNER_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_PARTNER_INFO','PARTNER , V_PARTNER_TL',
                       'partner_id: '||I_partner_id);
   fetch C_GET_PARTNER_INFO into O_partner_desc,
                                 O_currency_code,
                                 O_status,
                                 O_principle_country_id,
                                 O_mfg_id,
                                 O_lang;
   if C_GET_PARTNER_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NOT_PARTNER_DESC');
      SQL_LIB.SET_MARK('CLOSE','C_GET_PARTNER_INFO','PARTNER , V_PARTNER_TL',
                          'partner_id: '||I_partner_id);
      close C_GET_PARTNER_INFO;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_PARTNER_INFO','PARTNER , V_PARTNER_TL',
                          'partner_id: '||I_partner_id);
   close C_GET_PARTNER_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_INFO;
-------------------------------------------------------------------------------
FUNCTION CHECK_DELETE_PARTNER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE,
                              I_partner_id      IN       PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50)    := 'PARTNER_SQL.CHECK_DELETE_PARTNER';
   L_partner_exists  VARCHAR2(1)     := 'N';

   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;


   ---
   cursor C_CHECK_ORDHEAD is
      select 'Y'
        from ordhead
       where decode(I_partner_type, 'AG', agent,
                                    'FA', factory) = I_partner_id;
   ---
   cursor C_CHECK_LC is
      select 'Y'
        from lc_head
       where issuing_bank      = I_partner_id
          or advising_bank     = I_partner_id
          or confirming_bank   = I_partner_id
          or transferring_bank = I_partner_id
          or negotiating_bank  = I_partner_id
          or paying_bank       = I_partner_id;
   ---
   cursor C_CHECK_TRANS is
      select 'Y'
        from transportation
       where trans_partner_id = I_partner_id;
   ---
   cursor C_CHECK_CE is
      select 'Y'
        from ce_head
       where decode(I_partner_type, 'CN', consignee_id,
                                    'BR', broker_id,
                                    'IM', importer_id) = I_partner_id;
   ---
   cursor C_CHECK_OBLIG is
      select 'Y'
        from obligation
       where partner_type = I_partner_type
         and partner_id   = I_partner_id;
   ---
   cursor C_CHECK_CLAIMS is
      select 'Y'
        from trans_claims
       where claim_against_type = I_partner_type
         and claim_against_id   = I_partner_id;
   ---
   cursor C_CHECK_DEAL_HEAD is
      select 'Y'
        from deal_head
       where partner_type = I_partner_type
         and partner_id   = I_partner_id;
   ---
   cursor C_CHECK_INVC_HEAD is
      select 'Y'
        from invc_head
       where partner_type = I_partner_type
         and partner_id   = I_partner_id;
   ---
   cursor C_CHECK_SUPP_HIER_LVL_1 is
      select 'Y'
        from item_supp_country
       where supp_hier_lvl_1 = I_partner_id;
   ---
   cursor C_CHECK_SUPP_HIER_LVL_2 is
      select 'Y'
        from item_supp_country
       where supp_hier_lvl_2 = I_partner_id;
   ---
   cursor C_CHECK_SUPP_HIER_LVL_3 is
      select 'Y'
        from item_supp_country
       where supp_hier_lvl_3 = I_partner_id;
   ---
   cursor C_CHECK_PRIM_IA is
      select 'Y'
        from partner a, partner b
       where a.partner_id            = I_partner_id
         and a.partner_type          = I_partner_type
         and a.primary_ia_ind        = 'Y'
         and b.partner_type          = 'IA'
         and b.principle_country_id  = a.principle_country_id
         and b.partner_id           != a.partner_id;
   ---
   cursor C_CHECK_BANK_STORE is
      select 'Y'
        from sa_bank_store
       where partner_type = I_partner_type
         and partner_id   = I_partner_id;
   ---
   cursor C_CHECK_SUP_IMPORT_ATTR is
      select 'Y'
        from sup_import_attr
       where agent = I_partner_id;
   ---
   cursor C_CHECK_FIXED_DEAL is
      select 'Y'
        from fixed_deal
       where partner_type = I_partner_type
         and partner_id   = I_partner_id;

   cursor C_CHECK_SUP_IMPORT_ATTR1 is
      select 'Y'
        from sup_import_attr
       where partner_type_1 = I_partner_type
         and partner_1      = I_partner_id
       union all
      select 'Y'
        from sup_import_attr
       where partner_type_2 = I_partner_type
         and partner_2      = I_partner_id
       union all
      select 'Y'
        from sup_import_attr
       where partner_type_3 = I_partner_type
         and partner_3      = I_partner_id;
         
   cursor C_CHECK_ORDHEAD_PTR is
         select 'Y'
           from ordhead
          where (partner_type_1 = I_partner_type and partner1   = I_partner_id)
             or (partner_type_2 = I_partner_type and partner2   = I_partner_id)
             or (partner_type_3 = I_partner_type and partner3   = I_partner_id);
         
BEGIN
   O_exists := FALSE;

   ---
   if I_partner_type in ('S1','S2','S3') then
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_DEAL_HEAD','DEAL_HEAD',
                          'partner_id: '||I_partner_id||',partner_type: '||I_partner_type);
      open  C_CHECK_DEAL_HEAD;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_DEAL_HEAD','DEAL_HEAD',
                          'partner_id: '||I_partner_id||',partner_type: '||I_partner_type);
      fetch C_CHECK_DEAL_HEAD into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_DEAL_HEAD','DEAL_HEAD',
                          'partner_id: '||I_partner_id||',partner_type: '||I_partner_type);
      close C_CHECK_DEAL_HEAD;
      ---
      if L_partner_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_DEAL_HEAD');
         O_exists := TRUE;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_FIXED_DEAL','FIXED_DEAL',
                                'partner_id: '||I_partner_id||',partner_type: '||I_partner_type);
      open  C_CHECK_FIXED_DEAL;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_FIXED_DEAL','FIXED_DEAL',
                                'partner_id: '||I_partner_id||',partner_type: '||I_partner_type);
      fetch C_CHECK_FIXED_DEAL into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_FIXED_DEAL','FIXED_DEAL',
                                'partner_id: '||I_partner_id||',partner_type: '||I_partner_type);
      close C_CHECK_FIXED_DEAL;
      ---
      if L_partner_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_DEAL_HEAD');
         O_exists := TRUE;
         return TRUE;
      end if;
      ---     
      SQL_LIB.SET_MARK('OPEN','C_CHECK_INVC_HEAD','INVC_HEAD','partner_id: '||I_partner_id||',
                                                            partner_type: '||I_partner_type);
      open  C_CHECK_INVC_HEAD;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_INVC_HEAD','INVC_HEAD','partner_id: '||I_partner_id||',
                                                            partner_type: '||I_partner_type);
      fetch C_CHECK_INVC_HEAD into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_INVC_HEAD','INVC_HEAD',
                          'partner_id: '||I_partner_id||',partner_type: '||I_partner_type);
      close C_CHECK_INVC_HEAD;
      ---
      if L_partner_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_INVC_HEAD');
         O_exists := TRUE;
         return TRUE;
      end if;
      ---
      if I_partner_type = 'S1' then
         ---
         SQL_LIB.SET_MARK('OPEN','C_CHECK_SUPP_HIER_LVL_1','ITEM_SUPP_COUNTRY','supp_hier_lvl_1: '||I_partner_id);
         open  C_CHECK_SUPP_HIER_LVL_1;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_SUPP_HIER_LVL_1','ITEM_SUPP_COUNTRY','supp_hier_lvl_1: '||I_partner_id);
         fetch C_CHECK_SUPP_HIER_LVL_1 into L_partner_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUPP_HIER_LVL_1','ITEM_SUPP_COUNTRY','supp_hier_lvl_1: '||I_partner_id);
         close C_CHECK_SUPP_HIER_LVL_1;
         ---
         if L_partner_exists = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_ITEM');
            O_exists := TRUE;
            return TRUE;
         end if;
         ---
      elsif I_partner_type = 'S2' then
         ---
         SQL_LIB.SET_MARK('OPEN','C_CHECK_SUPP_HIER_LVL_2','ITEM_SUPP_COUNTRY','supp_hier_lvl_2: '||I_partner_id);
         open  C_CHECK_SUPP_HIER_LVL_2;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_SUPP_HIER_LVL_2','ITEM_SUPP_COUNTRY','supp_hier_lvl_2: '||I_partner_id);
         fetch C_CHECK_SUPP_HIER_LVL_2 into L_partner_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUPP_HIER_LVL_2','ITEM_SUPP_COUNTRY','supp_hier_lvl_2: '||I_partner_id);
         close C_CHECK_SUPP_HIER_LVL_2;
         ---
         if L_partner_exists = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_ITEM');
            O_exists := TRUE;
            return TRUE;
         end if;
         ---
      else    --I_partner_type = 'S3'
         ---
         SQL_LIB.SET_MARK('OPEN','C_CHECK_SUPP_HIER_LVL_3','ITEM_SUPP_COUNTRY','supp_hier_lvl_3: '||I_partner_id);
         open  C_CHECK_SUPP_HIER_LVL_3;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_SUPP_HIER_LVL_3','ITEM_SUPP_COUNTRY','supp_hier_lvl_3: '||I_partner_id);
         fetch C_CHECK_SUPP_HIER_LVL_3 into L_partner_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUPP_HIER_LVL_3','ITEM_SUPP_COUNTRY','supp_hier_lvl_3: '||I_partner_id);
         close C_CHECK_SUPP_HIER_LVL_3;
         ---
         if L_partner_exists = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_ITEM');
            O_exists := TRUE;
            return TRUE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   if I_partner_type in ('AG','FA') then
      SQL_LIB.SET_MARK('OPEN','C_CHECK_ORDHEAD','ORDHEAD','partner_id: '||I_partner_id||',
                                                            partner_type: '||I_partner_type);
      open  C_CHECK_ORDHEAD;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_ORDHEAD','ORDHEAD','partner_id: '||I_partner_id||',
                                                            partner_type: '||I_partner_type);
      fetch C_CHECK_ORDHEAD into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_ORDHEAD','ORDHEAD',
                          'partner_id: '||I_partner_id);
      close C_CHECK_ORDHEAD;
      ---
      if L_partner_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_ORDHEAD');
         O_exists := TRUE;
         return TRUE;
      end if;
      ---
      if I_partner_type = 'AG' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_SUP_IMPORT_ATTR',
                          'SUP_IMPORT_ATTR',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
         open  C_CHECK_SUP_IMPORT_ATTR;
         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_SUP_IMPORT_ATTR',
                          'SUP_IMPORT_ATTR',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
         fetch C_CHECK_SUP_IMPORT_ATTR into L_partner_exists;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_SUP_IMPORT_ATTR',
                          'SUP_IMPORT_ATTR',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
         close C_CHECK_SUP_IMPORT_ATTR;
         ---
         if L_partner_exists = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('PARTNR_IN_SUP_IMPORT_ATTR');
            O_exists := TRUE;
            return TRUE;
         end if;
         ---
      end if;
      ---
   elsif I_partner_type = 'BK' then
      SQL_LIB.SET_MARK('OPEN','C_CHECK_LC','LC_HEAD',NULL);
      open  C_CHECK_LC;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_LC','LC_HEAD',NULL);
      fetch C_CHECK_LC into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_LC','LC_HEAD',NULL);
      close C_CHECK_LC;
      ---
      if L_partner_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_LC_HEAD');
         O_exists := TRUE;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_BANK_STORE','SA_BANK_STORE',NULL);
      open  C_CHECK_BANK_STORE;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_BANK_STORE','SA_BANK_STORE',NULL);
      fetch C_CHECK_BANK_STORE into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_BANK_STORE','SA_BANK_STORE',NULL);
      close C_CHECK_BANK_STORE;
      ---
      if L_partner_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('BANK_NO_DELETE_BANK_STORE');
         O_exists := TRUE;
         return TRUE;
      end if;
   elsif I_partner_type in ('CN','BR','IM') then
      SQL_LIB.SET_MARK('OPEN','C_CHECK_CE','CE_HEAD',NULL);
      open  C_CHECK_CE;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_CE','CE_HEAD',NULL);
      fetch C_CHECK_CE into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_CE','CE_HEAD',NULL);
      close C_CHECK_CE;
      ---
      if L_partner_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_CE');
         O_exists := TRUE;
         return TRUE;
      end if;
   elsif I_partner_type = 'IA' then
      SQL_LIB.SET_MARK('OPEN','C_CHECK_PRIM_IA','PARTNER',NULL);
      open C_CHECK_PRIM_IA;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_PRIM_IA','PARTNER',NULL);
      fetch C_CHECK_PRIM_IA into L_partner_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_PRIM_IA','PARTNER',NULL);
      close C_CHECK_PRIM_IA;
      ---
      if L_partner_exists = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('PRIM_IA_EXISTS');
         O_exists := TRUE;
         return TRUE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_TRANS','TRANSPORTATION',NULL);

   open  C_CHECK_TRANS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_TRANS','TRANSPORTATION',NULL);

   fetch C_CHECK_TRANS into L_partner_exists;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_TRANS','TRANSPORTATION',NULL);

   close C_CHECK_TRANS;
   ---
   if L_partner_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_TRANS');
      O_exists := TRUE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_OBLIG','OBLIGATION','partner_id: '||I_partner_id||
                                                        ', partner_type: '||I_partner_type);
   open  C_CHECK_OBLIG;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_OBLIG','OBLIGATION','partner_id: '||I_partner_id||
                                                         ', partner_type: '||I_partner_type);
   fetch C_CHECK_OBLIG into L_partner_exists;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_OBLIG','OBLIGATION','partner_id: '||I_partner_id||
                                                         ', partner_type: '||I_partner_type);
   close C_CHECK_OBLIG;
   ---
   if L_partner_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_OBLIG');
      O_exists := TRUE;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_CLAIMS','TRANS_CLAIMS','partner_id: '||I_partner_id||
                                                           ', partner_type: '||I_partner_type);
   open  C_CHECK_CLAIMS;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_CLAIMS','TRANS_CLAIMS','partner_id: '||I_partner_id||
                                                            ', partner_type: '||I_partner_type);
   fetch C_CHECK_CLAIMS into L_partner_exists;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_CLAIMS','TRANS_CLAIMS','partner_id: '||I_partner_id||
                                                            ', partner_type: '||I_partner_type);
   close C_CHECK_CLAIMS;
   ---
   if L_partner_exists = 'Y' then
      O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_CLAIMS');
      O_exists := TRUE;
      return TRUE;
   end if;
   ---
   if I_partner_type is not null then
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_SUP_IMPORT_ATTR1',
                          'SUP_IMPORT_ATTR',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
         open  C_CHECK_SUP_IMPORT_ATTR1;
         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_SUP_IMPORT_ATTR1',
                          'SUP_IMPORT_ATTR',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
         fetch C_CHECK_SUP_IMPORT_ATTR1 into L_partner_exists;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_SUP_IMPORT_ATTR1',
                          'SUP_IMPORT_ATTR',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
         close C_CHECK_SUP_IMPORT_ATTR1;
         ---
         if L_partner_exists = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('PARTNR_IN_SUP_IMPORT_ATTR');
            O_exists := TRUE;
            return TRUE;
         end if;
         ---
 
         SQL_LIB.SET_MARK('OPEN',
                          'C_CHECK_ORDHEAD_PTR',
                          'ORDHEAD',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
         open  C_CHECK_ORDHEAD_PTR;
         SQL_LIB.SET_MARK('FETCH',
                          'C_CHECK_ORDHEAD_PTR',
                          'ORDHEAD',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
         fetch C_CHECK_ORDHEAD_PTR into L_partner_exists;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_CHECK_ORDHEAD_PTR',
                          'ORDHEAD',
                          'partner_id: '||I_partner_id||', partner_type: '||I_partner_type);
        close C_CHECK_ORDHEAD_PTR;
        ---
        if L_partner_exists = 'Y' then
           O_error_message := SQL_LIB.CREATE_MSG('PARTNER_IN_ORDHEAD');
           O_exists := TRUE;
           return TRUE;
        end if;
        --- 
   end if; 
   ---
   if PARTNER_SQL.DEL_PARTNER_CHILD_REC(O_error_message,
                                        I_partner_type,
                                        I_partner_id) = FALSE then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                                            I_partner_id, I_partner_type);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END CHECK_DELETE_PARTNER;
-------------------------------------------------------------------------------
FUNCTION DEL_PARTNER_CHILD_REC(O_error_message   IN OUT   VARCHAR2,
                               I_partner_type    IN       partner.partner_type%TYPE,
                               I_partner_id      IN       partner.partner_id%TYPE)
   RETURN BOOLEAN IS

   L_table          VARCHAR2(30);
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_program        VARCHAR2(50) := 'PARTNER_SQL.DEL_PARTNER_CHILD_REC';

   cursor C_LOCK_EXP_PROF_DETAIL is
      select 'x'
        from exp_prof_detail epd, exp_prof_head eph
       where epd.exp_prof_key = eph.exp_prof_key
         and module = 'PTNR'
         and key_value_1 = I_partner_type
         and key_value_2 = I_partner_id
         for update nowait;
   ---
   cursor C_LOCK_EXP_PROF_HEAD is
      select 'x'
        from exp_prof_head
       where module = 'PTNR'
         and key_value_1 = I_partner_type
         and key_value_2 = I_partner_id
         for update nowait;
   ---
   cursor C_LOCK_REQ_DOC is
      select 'x'
        from req_doc
       where module = 'PTNR'
         and key_value_1 = I_partner_type
         and key_value_2 = I_partner_id
         for update nowait;
   ---
   cursor C_LOCK_ADDR_TL is
      select 'x'
        from addr_tl
        where addr_key in  (select addr_key
                              from addr
                             where module = 'PTNR'
                               and key_value_1 = I_partner_type
                               and key_value_2 = I_partner_id)   
          for update nowait;        
   ---
     cursor C_LOCK_ADDR is
      select 'x'
        from addr
       where module = 'PTNR'
         and key_value_1 = I_partner_type
         and key_value_2 = I_partner_id
         for update nowait;

   ---
   cursor C_LOCK_STOCK_LEDGER_INSERTS is
      select 'x'
        from stock_ledger_inserts
       where location = to_number(I_partner_id)
         for update nowait;

   ---
   cursor C_LOCK_PARTNER_L10N_EXT is
      select 'x'
        from partner_l10n_ext
       where partner_id = I_partner_id
         and partner_type = I_partner_type
         for update nowait;
    ---     
   cursor C_LOCK_PARTNER_CFA_EXT is
      select 'x'
        from partner_cfa_ext
       where partner_id = I_partner_id
         and partner_type = I_partner_type
         for update nowait;
    ---   
    cursor C_LOCK_ADDR_CFA_EXT is
       select 'x'
         from addr_cfa_ext
        where exists (select 'x'
                        from addr
                       where module = 'PTNR'
                        and key_value_1 = I_partner_type
                        and key_value_2 = I_partner_id)   
          for update nowait;              

BEGIN
   L_table := 'PARTNER_L10N_EXT';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_PARTNER_L10N_EXT', L_table,'partner_id: '||I_partner_id||',
                                                               partner_type: '||I_partner_type);
   open  C_LOCK_PARTNER_L10N_EXT;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_PARTNER_L10N_EXT', L_table, 'partner_id: '||I_partner_id||',
                                                                 partner_type: '||I_partner_type);
   close C_LOCK_PARTNER_L10N_EXT;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, L_table,'partner_id: '||I_partner_id||',
                                             partner_type: '||I_partner_type);
   delete from partner_l10n_ext
         where partner_id = I_partner_id
           and partner_type = I_partner_type;
   ---
   L_table := 'PARTNER_CFA_EXT';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_PARTNER_CFA_EXT', L_table,'partner_id: '||I_partner_id||',
                                                               partner_type: '||I_partner_type);
   open  C_LOCK_PARTNER_CFA_EXT;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_PARTNER_CFA_EXT', L_table, 'partner_id: '||I_partner_id||',
                                                                 partner_type: '||I_partner_type);
   close C_LOCK_PARTNER_CFA_EXT;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, L_table,'partner_id: '||I_partner_id||',
                                             partner_type: '||I_partner_type);
   delete from partner_cfa_ext
         where partner_id = I_partner_id
           and partner_type = I_partner_type;
   ---
   L_table := 'EXP_PROF_DETAIL';
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_EXP_PROF_DETAIL', L_table,'partner_id: '||I_partner_id||',
                                                               partner_type: '||I_partner_type);
   open  C_LOCK_EXP_PROF_DETAIL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_EXP_PROF_DETAIL', L_table, 'partner_id: '||I_partner_id||',
                                                                 partner_type: '||I_partner_type);
   close C_LOCK_EXP_PROF_DETAIL;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, L_table,'partner_id: '||I_partner_id||',
                                             partner_type: '||I_partner_type);
   delete from exp_prof_detail
      where exp_prof_key in (select exp_prof_key
                              from exp_prof_head
                             where module = 'PTNR'
                               and key_value_1 = I_partner_type
                               and key_value_2 = I_partner_id);
   ---
   L_table := 'EXP_PROF_HEAD';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_EXP_PROF_HEAD', L_table,'partner_id: '||I_partner_id||',
                                                            partner_type: '||I_partner_type);
   open  C_LOCK_EXP_PROF_HEAD;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_EXP_PROF_HEAD', L_table,'partner_id: '||I_partner_id||',
                                                             partner_type: '||I_partner_type);
   close C_LOCK_EXP_PROF_HEAD;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, L_table,'partner_id: '||I_partner_id||',
                                             partner_type: '||I_partner_type);
   delete from exp_prof_head
      where module = 'PTNR'
        and key_value_1 = I_partner_type
        and key_value_2 = I_partner_id;
   ---
   L_table := 'REQ_DOC';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_REQ_DOC', L_table,'partner_id: '||I_partner_id||',
                                                      partner_type: '||I_partner_type);
   open  C_LOCK_REQ_DOC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_REQ_DOC', L_table,'partner_id: '||I_partner_id||',
                                                       partner_type: '||I_partner_type);
   close C_LOCK_REQ_DOC;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, L_table,'partner_id: '||I_partner_id||',
                                             partner_type: '||I_partner_type);
   delete from req_doc
      where module = 'PTNR'
        and key_value_1 = I_partner_type
        and key_value_2 = I_partner_id;
   ---
   L_table := 'ADDR_CFA_EXT';
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_ADDR_CFA_EXT',
                    'ADDR',
                    'partner_id: '||I_partner_id||',
                    partner_type: '||I_partner_type);
   open C_LOCK_ADDR_CFA_EXT;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_ADDR_CFA_EXT',
                    'ADDR',
                    'partner_id: '||I_partner_id||',
                    partner_type: '||I_partner_type);
   close C_LOCK_ADDR_CFA_EXT;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'ADDR',
                    'partner_id: '||I_partner_id||'
                    partner_type: '||I_partner_type);

   delete from addr_cfa_ext
         where addr_key in (select addr_key
                              from addr
                             where module = 'PTNR'
                               and key_value_1 = I_partner_type
                               and key_value_2 = I_partner_id);   
   ---
   
   L_table := 'ADDR_TL ';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ADDR_TL', L_table,'partner_id: '||I_partner_id||',
                                                   partner_type: '||I_partner_type);
   open  C_LOCK_ADDR_TL;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ADDR_TL', L_table,'partner_id: '||I_partner_id||',
                                                    partner_type: '||I_partner_type);
   close C_LOCK_ADDR_TL;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, L_table,'partner_id: '||I_partner_id||',
                                             partner_type: '||I_partner_type);
   delete from addr_tl
      where addr_key in (select addr_key
                           from addr
                          where module = 'PTNR'
                            and key_value_1 = I_partner_type
                            and key_value_2 = I_partner_id);
   
   ---   
   
   
   L_table := 'ADDR ';
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ADDR', L_table,'partner_id: '||I_partner_id||',
                                                   partner_type: '||I_partner_type);
   open  C_LOCK_ADDR;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ADDR', L_table,'partner_id: '||I_partner_id||',
                                                    partner_type: '||I_partner_type);
   close C_LOCK_ADDR;
   ---
   SQL_LIB.SET_MARK('DELETE', NULL, L_table,'partner_id: '||I_partner_id||',
                                             partner_type: '||I_partner_type);
   delete from addr
      where module = 'PTNR'
        and key_value_1 = I_partner_type
        and key_value_2 = I_partner_id;
   ---   
   if DELETE_PARTNER_TL(O_error_message,
                                        I_partner_type,
                                        I_partner_id) = FALSE then
      return FALSE;
   end if;
   
   if I_partner_type = 'E' then
      L_table := 'STOCK_LEDGER_INSERTS';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_STOCK_LEDGER_INSERTS', L_table,'partner_id: '|| I_partner_id);

      open  C_LOCK_STOCK_LEDGER_INSERTS;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_STOCK_LEDGER_INSERTS', L_table,'partner_id: '|| I_partner_id );

      close C_LOCK_STOCK_LEDGER_INSERTS;
      ---
      SQL_LIB.SET_MARK('DELETE', NULL, L_table,'partner_id: '|| I_partner_id);

      delete from stock_ledger_inserts
       where location = to_number(I_partner_id);
   end if;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', L_table,
                                            I_partner_id, I_partner_type);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END DEL_PARTNER_CHILD_REC;
-------------------------------------------------------------------------------
FUNCTION VALID_ACTIVE(O_error_message   IN OUT   VARCHAR2,
                      O_valid           IN OUT   BOOLEAN,
                      I_partner_type    IN       partner.partner_type%TYPE,
                      I_partner_id      IN       partner.partner_id%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(30)  :=  'PARTNER_SQL.VALID_ACTIVE';
   L_exists      VARCHAR2(1);
   ---
   cursor C_PARTNER_ACTIVE is
      select 'x'
        from partner
       where partner_id   =  I_partner_id
         and partner_type =  I_partner_type
         and status       =  'A';
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_PARTNER_ACTIVE','PARTNER','partner_id: '||I_partner_id||',
                                                         partner_type: '||I_partner_type);
   open  C_PARTNER_ACTIVE;
   SQL_LIB.SET_MARK('FETCH','C_PARTNER_ACTIVE','PARTNER','partner_id: '||I_partner_id||',
                                                         partner_type: '||I_partner_type);
   fetch C_PARTNER_ACTIVE into L_exists;
   if C_PARTNER_ACTIVE%FOUND then
       O_valid := TRUE;
   else
       O_valid := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_PARTNER_ACTIVE','PARTNER','partner_id: '||I_partner_id||',
                                                         partner_type: '||I_partner_type);
   close C_PARTNER_ACTIVE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END VALID_ACTIVE;
-------------------------------------------------------------------------------
FUNCTION GET_BANK_CREDIT(O_error_message          IN OUT   VARCHAR2,
                         O_line_of_credit         IN OUT   partner.line_of_credit%TYPE,
                         O_outstand_credit        IN OUT   partner.outstand_credit%TYPE,
                         O_open_credit            IN OUT   partner.open_credit%TYPE,
                         O_line_of_credit_prim    IN OUT   partner.line_of_credit%TYPE,
                         O_outstand_credit_prim   IN OUT   partner.outstand_credit%TYPE,
                         O_open_credit_prim       IN OUT   partner.open_credit%TYPE,
                         I_bank                   IN       partner.partner_id%TYPE,
                         I_convert                IN       BOOLEAN)

   RETURN BOOLEAN IS

   L_program     VARCHAR2(30)  :=  'PARTNER_SQL.GET_BANK_CREDIT';
   ---
   cursor C_BANK_CREDIT is
      select line_of_credit,
             outstand_credit,
             open_credit
        from partner
       where partner_id   =  I_bank
         and partner_type =  'BK';

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_BANK_CREDIT','PARTNER','partner_id: '||I_bank||',
                                                      partner_type: '||'BK');
   open  C_BANK_CREDIT;
   SQL_LIB.SET_MARK('FETCH','C_BANK_CREDIT','PARTNER','partner_id: '||I_bank||',
                                                       partner_type: '||'BK');
   fetch C_BANK_CREDIT into O_line_of_credit,
                            O_outstand_credit,
                            O_open_credit;
   if C_BANK_CREDIT%NOTFOUND then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_BANK_CREDIT','PARTNER','partner_id: '||I_bank||',
                                                       partner_type: '||'BK');
   close C_BANK_CREDIT;

   if I_convert = TRUE then
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_bank,
                                          'BK',
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          O_line_of_credit,
                                          O_line_of_credit_prim,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_bank,
                                          'BK',
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          O_outstand_credit,
                                          O_outstand_credit_prim,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
      if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                          I_bank,
                                          'BK',
                                          NULL,
                                          NULL,
                                          NULL,
                                          NULL,
                                          O_open_credit,
                                          O_open_credit_prim,
                                          'C',
                                          NULL,
                                          NULL) = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_BANK_CREDIT;
-------------------------------------------------------------------------------
FUNCTION BANK_STORE_EXISTS(O_error_message   IN OUT   VARCHAR2,
                           O_exists          IN OUT   BOOLEAN,
                           I_partner_type    IN       partner.partner_type%TYPE,
                           I_partner_id      IN       partner.partner_id%TYPE,
                           I_store           IN       store.store%TYPE)
    RETURN BOOLEAN IS

   L_exists      VARCHAR2(4)   := 'N';
   L_program     VARCHAR2(30)  := 'PARTNER_SQL.BANK_STORE_EXISTS';
   ---
   cursor C_BANK_STORE is
      select 'Y'
        from sa_bank_store
       where partner_type = I_partner_type
         and partner_id   = I_partner_id
         and store        = I_store;

BEGIN
   O_exists := FALSE;
   ---
   if I_partner_type is NULL or I_partner_id is NULL or I_store is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_BANK_STORE','SA_BANK_STORE','partner_id: '||I_partner_id||
                                                           ', partner_type: '||I_partner_type||
                                                           ', store:        '||to_char(I_store));
   open  C_BANK_STORE;
   SQL_LIB.SET_MARK('FETCH','C_BANK_STORE','SA_BANK_STORE','partner_id: '||I_partner_id||
                                                           ', partner_type: '||I_partner_type||
                                                           ', store:        '||to_char(I_store));
   fetch C_BANK_STORE into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_BANK_STORE','SA_BANK_STORE','partner_id: '||I_partner_id||
                                                           ', partner_type: '||I_partner_type||
                                                           ', store:        '||to_char(I_store));
   close C_BANK_STORE;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END BANK_STORE_EXISTS;
---------------------------------------------------------------------------------
FUNCTION STORES_EXIST(O_error_message   IN OUT   VARCHAR2,
                      O_exists          IN OUT   BOOLEAN,
                      I_partner_type    IN       partner.partner_type%TYPE,
                      I_partner_id      IN       partner.partner_id%TYPE)
   RETURN BOOLEAN IS

   L_exists      VARCHAR2(4)   := 'N';
   L_program     VARCHAR2(30)  := 'PARTNER_SQL.STORES_EXIST';
   ---
   cursor C_STORE_EXIST is
      select 'Y'
        from sa_bank_store
       where partner_type = I_partner_type
         and partner_id   = I_partner_id;
BEGIN
   if I_partner_type is NULL or I_partner_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_STORE_EXIST','SA_BANK_STORE','partner_id: '||I_partner_id||
                                                           ', partner_type: '||I_partner_type);
   open C_STORE_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_STORE_EXIST','SA_BANK_STORE','partner_id: '||I_partner_id||
                                                            ', partner_type: '||I_partner_type);
   fetch C_STORE_EXIST into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_STORE_EXIST','SA_BANK_STORE','partner_id: '||I_partner_id||
                                                            ', partner_type: '||I_partner_type);
   close C_STORE_EXIST;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STORES_EXIST;
---------------------------------------------------------------------------------
FUNCTION GET_PART_PRIMARY_ADDR(O_error_message   IN OUT   VARCHAR2,
                               O_addr_key        IN OUT   addr.addr_key%TYPE,
                               I_partner_id      IN       partner.partner_id%TYPE,
                               I_partner_type    IN       partner.partner_type%TYPE,
                               I_addr_type       IN       addr.addr_type%TYPE)
RETURN BOOLEAN IS

L_program   VARCHAR2(50) := 'PARTNER_SQL.GET_PART_PRIMARY_ADDR';

cursor C_GET_PRIMARY_ADDR is
   select addr_key
     from addr
    where addr.module = 'PTNR'
      and addr.key_value_1 = I_partner_type
      and addr.key_value_2 = I_partner_id
      and addr.primary_addr_ind = 'Y'
      and addr.addr_type =I_addr_type;

BEGIN
   if I_partner_type is NULL or I_partner_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('c',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PRIMARY_ADDR',
                    'ADDR',
                    'partner_id: '||I_partner_id||',
                    partner_type: '||I_partner_type||',
                    addr_type: '||I_addr_type);
   open C_GET_PRIMARY_ADDR;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PRIMARY_ADDR',
                    'ADDR',
                    'partner_id: '||I_partner_id||',
                    partner_type: '||I_partner_type||',
                    addr_type: '||I_addr_type);
   fetch C_GET_PRIMARY_ADDR into O_addr_key;

   if C_GET_PRIMARY_ADDR%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('VALID_PART_INFO',
                                            NULL,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PRIMARY_ADDR',
                       'ADDR',
                       'partner_id: '||I_partner_id||',
                       partner_type: '||I_partner_type||',
                       addr_type: '||I_addr_type);

      close C_GET_PRIMARY_ADDR;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PRIMARY_ADDR',
                    'ADDR',
                    'partner_id: '||I_partner_id||',
                    partner_type: '||I_partner_type||',
                    addr_type: '||I_addr_type);

   close C_GET_PRIMARY_ADDR;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PART_PRIMARY_ADDR;
---------------------------------------------------------------------------------------
FUNCTION GET_PRIM_IA(O_error_message   IN OUT   VARCHAR2,
                     O_partner_type    IN OUT   partner.partner_type%TYPE,
                     O_partner_id      IN OUT   partner.partner_id%TYPE,
                     I_country_id      IN       ce_head.import_country_id%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'PARTNER_SQL.GET_PRIM_IA';

   cursor C_GET_PRIM_IA is
      select partner_id, partner_type
        from partner
       where partner_type         = 'IA'
         and principle_country_id = I_country_id
         and primary_ia_ind       = 'Y';
BEGIN
   if I_country_id is null then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_PRIM_IA',
                       'PARTNER',
                       'country_id:'||I_country_id);
      open C_GET_PRIM_IA;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_PRIM_IA',
                       'PARTNER',
                       'country_id:'||I_country_id);
      fetch C_GET_PRIM_IA into O_partner_id,
                               O_partner_type;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PRIM_IA',
                       'PARTNER',
                       'country_id:'|| I_country_id);
      close C_GET_PRIM_IA;
       ---
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIM_IA;
-------------------------------------------------------------------------------------------
FUNCTION UPDATE_PRIM_IA(O_error_message   IN OUT   VARCHAR2,
                        I_country_id      IN       ce_head.import_country_id%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'PARTNER_SQL.UPDATE_PRIM_IA';


   Cursor C_LOCK_PARTNER is
      select 'X'
        from partner
       where partner_type = 'IA'
         and primary_ia_ind = 'Y'
         and import_country_id = I_country_id
         for update nowait;


BEGIN
   if I_country_id is null then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_PARTNER',
                       'PARTNER',
                       'COUNTRY:'||I_country_id);
      open C_LOCK_PARTNER;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_PARTNER',
                       'PARTNER',
                       'COUNTRY:'||I_country_id);
      close C_LOCK_PARTNER;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'PARTNER',
                       'COUNTRY:'||I_country_id);

      update partner
         set primary_ia_ind = 'N'
       where partner_type = 'IA'
         and import_country_id = I_country_id
         and primary_ia_ind = 'Y';

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_PRIM_IA;
---------------------------------------------------------------------------------------
FUNCTION GET_TERMS(O_error_message   IN OUT   VARCHAR2,
                   O_terms           IN OUT   partner.terms%TYPE,
                   I_partner_id      IN       partner.partner_id%TYPE,
                   I_partner_type    IN       partner.partner_type%TYPE)
RETURN BOOLEAN IS

L_program   VARCHAR2(50) := 'PARTNER_SQL.GET_TERMS';

CURSOR C_get_terms is
      select terms
        from partner
       where partner_id   = I_partner_id
         and partner_type = I_partner_type;
BEGIN


if I_partner_type is NULL or I_partner_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                            L_program,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TERMS',
                    'PARTNER',
                    'partner_id: '||I_partner_id||
                    ',partner_type: '||I_partner_type);
   open C_GET_TERMS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TERMS',
                    'PARTNER',
                    'partner_id: '||I_partner_id||
                    ',partner_type: '||I_partner_type);

   fetch C_GET_TERMS into O_terms;

   if C_GET_TERMS%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('VALID_PART_INFO',
                                            NULL,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_TERMS',
                    'PARTNER',
                    'partner_id: '||I_partner_id||
                    ',partner_type: '||I_partner_type);


      close C_GET_TERMS;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TERMS',
                    'PARTNER',
                    'partner_id: '||I_partner_id||
                    ',partner_type: '||I_partner_type);


   close C_GET_TERMS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TERMS;
-------------------------------------------------------------------------------------------

FUNCTION NEXT_PARTNER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_partner_id      IN OUT   PARTNER.PARTNER_ID%TYPE,
                      I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN IS
   L_program        VARCHAR2(64) := 'PARTNER_SQL.NEXT_PARTNER';
   L_seq            NUMBER(10);
   L_wrap_number    NUMBER(10)   := 0;
   L_first_time     VARCHAR2(1)  := 'Y';
   L_seq_exists     BOOLEAN      := FALSE;
   cursor C_PARTNER is
      select partner_seq.NEXTVAL
        from dual;
BEGIN    
   if I_partner_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_partner_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   -- Retrieve Sequence Number
   LOOP
      SQL_LIB.SET_MARK('OPEN','C_partner','partner',NULL);
      open C_PARTNER;
      SQL_LIB.SET_MARK('FETCH','C_partner','partner',NULL);
      fetch C_PARTNER into L_seq;
      SQL_LIB.SET_MARK('CLOSE','C_partner','partner',NULL);
      close C_PARTNER;
      if (L_first_time = 'Y') then
         L_wrap_number := L_seq;
         L_first_time  := 'N';
      elsif (L_seq = L_wrap_number) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      -- Check if sequence is existing
      if PARTNER_EXISTS(O_error_message,
                        L_seq_exists,
                        L_seq,
                        I_partner_type) = FALSE then
         return FALSE;
      end if;

      if L_seq_exists = FALSE then
         if I_partner_type = 'E' then
            if LOCATION_ATTRIB_SQL.ID_EXISTS(O_error_message,
                                             L_seq_exists,
                                             L_seq)= FALSE then
               return FALSE;
            end if;
         end if;

         if not L_seq_exists then
            O_partner_id := L_seq;
            return TRUE;
         end if;
      end if;
   END LOOP;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END NEXT_PARTNER;
-------------------------------------------------------------------------------------------
FUNCTION STATE_EXISTS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists                IN OUT   BOOLEAN,
                      O_dup_exists            IN OUT   VARCHAR2,
                      O_principal_country_id  IN OUT   PARTNER.PRINCIPLE_COUNTRY_ID%TYPE,
                      I_partner_id            IN       PARTNER.PARTNER_ID%TYPE)
   RETURN BOOLEAN IS

   L_exists                 VARCHAR2(1)  := 'Y';
   L_program                VARCHAR2(64) := 'PARTNER_SQL.STATE_EXISTS';
   L_principal_country_id   COUNTRY.COUNTRY_ID%TYPE;
   L_rowcount               NUMBER := 0;
   O_principle_country_desc COUNTRY.COUNTRY_DESC%TYPE;

   cursor C_CHECK_STATE is
      select 'X', 
             country_id
        from state
       where state = I_partner_id;

BEGIN
   if I_partner_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_partner_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_check_state',
                    'state',
                    'PARTNER_ID:'||I_partner_id);

   open C_CHECK_STATE;
   LOOP
      SQL_LIB.SET_MARK('FETCH',
                       'C_check_state',
                       'state',
                       'PARTNER_ID:'||I_partner_id);
      fetch C_CHECK_STATE into L_exists,
                               L_principal_country_id;
      L_rowcount := C_CHECK_STATE%ROWCOUNT;
      Exit when C_CHECK_STATE%ROWCOUNT > 1 or C_CHECK_STATE%NOTFOUND;
   END LOOP;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_check_state',
                    'state',
                    'PARTNER_ID:'||I_partner_id);
   close C_CHECK_STATE;

   if L_rowcount = 1 then
      O_exists := TRUE;
      O_dup_exists := 'N';
      O_principal_country_id := L_principal_country_id;
   elsif L_rowcount = 0 then
      O_exists := FALSE;
   elsif L_rowcount > 1 then
      O_exists := TRUE;
      O_dup_exists := 'Y';
      O_principal_country_id := L_principal_country_id;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STATE_EXISTS;
-------------------------------------------------------------------------------------------
FUNCTION PARTNER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_exists          IN OUT   BOOLEAN,
                        I_partner_id      IN       PARTNER.PARTNER_ID%TYPE,
                        I_partner_type    IN       PARTNER.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_exists    VARCHAR2(1)  := 'y';
   L_program   VARCHAR2(64) := 'PARTNER_SQL.PARTNER_EXISTS';

   cursor C_PARTNER_EXISTS is
      select 'x'
        from partner
       where partner_id = I_partner_id
         and partner_type = I_partner_type;

BEGIN
   if I_partner_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_partner_id',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_partner_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_partner_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_PARTNER_EXISTS',
                    'partner',
                    'PARTNER_ID:'||I_partner_id||','||
                    'PARTNER_TYPE:'||I_partner_type);

   open C_PARTNER_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_PARTNER_EXISTS',
                    'partner',
                    'PARTNER_ID:'||I_partner_id||','||
                    'PARTNER_TYPE:'||I_partner_type);

   fetch C_PARTNER_EXISTS into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_PARTNER_EXISTS',
                    'partner',
                    'PARTNER_ID:'||I_partner_id||','||
                    'PARTNER_TYPE:'||I_partner_type);

   close C_PARTNER_EXISTS;

   O_exists := (L_exists = 'x');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PARTNER_EXISTS;

-------------------------------------------------------------------------------------------
FUNCTION DEL_PARTNER_ATTRIB(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_partner_id      IN       PARTNER_L10N_EXT.PARTNER_ID%TYPE,
                            I_partner_type    IN       PARTNER_L10N_EXT.PARTNER_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'PARTNER_SQL.DEL_PARTNER_ATTRIB';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PARTNER_CFA_EXT is
      select 'x'
        from partner_cfa_ext
       where partner_id = I_partner_id
         and partner_type = I_partner_type
         for update nowait;
         
   cursor C_LOCK_PARTNER_L10N_EXT is
      select 'x'
        from partner_l10n_ext
       where partner_id = I_partner_id
         and partner_type = I_partner_type
         for update nowait;

BEGIN

   L_table := 'PARTNER_CFA_EXT';
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_PARTNER_CFA_EXT',
                    L_table,
                    'partner_id: '||I_partner_id||',
                    partner_type: '||I_partner_type);

   open  C_LOCK_PARTNER_CFA_EXT;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_PARTNER_CFA_EXT',
                    L_table,
                    'partner_id: '||I_partner_id||',
                     partner_type: '||I_partner_type);

   close C_LOCK_PARTNER_CFA_EXT;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'partner_id: '||I_partner_id||',
                     partner_type: '||I_partner_type);

   delete from partner_cfa_ext
         where partner_id = I_partner_id
           and partner_type = I_partner_type;
   ---

   L_table := 'PARTNER_L10N_EXT';
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_PARTNER_L10N_EXT',
                    L_table,
                    'partner_id: '||I_partner_id||',
                    partner_type: '||I_partner_type);

   open  C_LOCK_PARTNER_L10N_EXT;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_PARTNER_L10N_EXT',
                    L_table,
                    'partner_id: '||I_partner_id||',
                     partner_type: '||I_partner_type);

   close C_LOCK_PARTNER_L10N_EXT;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    L_table,
                    'partner_id: '||I_partner_id||',
                     partner_type: '||I_partner_type);

   delete from partner_l10n_ext
         where partner_id = I_partner_id
           and partner_type = I_partner_type;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 
                                            L_table,
                                            I_partner_id, 
                                            I_partner_type);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program, 
                                            to_char(SQLCODE));
      return FALSE;
END DEL_PARTNER_ATTRIB;
-------------------------------------------------------------------------------------------
FUNCTION DELETE_PARTNER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_partner_type    IN       PARTNER_TL.PARTNER_TYPE%TYPE,
                           I_partner_id      IN       PARTNER_TL.PARTNER_ID%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'PARTNER_SQL.DELETE_PARTNER_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PARTNER_TL is
      select 'x'
        from partner_tl
       where partner_type = I_partner_type
         and partner_id   = I_partner_id 
         for update nowait;
BEGIN

   if I_partner_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_partner_type',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   
   if I_partner_id is NULL then
        O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                          'I_partner_id',
                                           L_program,
                                           NULL);
        return FALSE;
   end if;

   ---
   L_table := 'PARTNER_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_PARTNER_TL',
                     L_table,
                     'partner_id: '||I_partner_id|| ', partner_type: '||I_partner_type);

   open C_LOCK_PARTNER_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_PARTNER_TL',
                     L_table,
                    'partner_id: '||I_partner_id|| ', partner_type: '||I_partner_type);

   close C_LOCK_PARTNER_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                     NULL,
                     L_table,
                     'partner_id: '||I_partner_id|| ', partner_type: '||I_partner_type);   
    
    delete from partner_tl
          where partner_type = I_partner_type
            and partner_id   = I_partner_id ;

   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_PARTNER_TL;
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_partner_id        IN       PARTNER.PARTNER_ID%TYPE,
                    I_partner_type      IN       PARTNER.PARTNER_TYPE%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(75)   := 'PARTNER_SQL.CUSTOM_VAL';
   L_custom_obj_rec      CUSTOM_OBJ_REC :=  CUSTOM_OBJ_REC();

BEGIN

   L_custom_obj_rec.function_key:= I_function_key;
   L_custom_obj_rec.call_seq_no:= I_seq_no;
   L_custom_obj_rec.partner:= I_partner_id;
   L_custom_obj_rec.partner_type:= I_partner_type;


   --call the custom code for client specific order approval
   if CALL_CUSTOM_SQL.EXEC_FUNCTION(O_error_message,
                                    L_custom_obj_rec) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            SQLCODE);
      return FALSE;
END CUSTOM_VAL;   
---------------------------------------------------------------------------------------------------------
END;
/