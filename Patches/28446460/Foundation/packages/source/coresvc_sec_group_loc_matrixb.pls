CREATE OR REPLACE PACKAGE BODY CORESVC_SEC_GROUP_LOC_MATRIX AS

   cursor C_SVC_SEC_GROUP_LOC_MATRIX(I_process_id NUMBER,
                                     I_chunk_id   NUMBER) is
      select uk_sec_group_loc_matrix.rowid                 as uk_sec_group_loc_matrix_rid,
             st.rowid                                      as st_rid,
             sgl_dis_fk.rowid                              as sgl_dis_fk_rid,
             sgl_reg_fk.rowid                              as sgl_reg_fk_rid,
             sgl_sgu_fk.rowid                              as sgl_sgu_fk_rid,
             sgl_str_fk.rowid                              as sgl_str_fk_rid,
             sgl_wah_fk.rowid                              as sgl_wah_fk_rid,
             cd_fk.rowid                                   as cd_fk_rid,
             st.update_ind,
             st.select_ind,
             st.wh,
             st.store,
             sgl_str_fk.district                           as str_dist,
             sgl_dis_fk.region                             as dist_reg,
             NVL(st.district,sgl_str_fk.district)          as district, 
             NVL(st.region,sgl_dis_fk.region)              as region,
             st.group_id,
             st.column_code,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)                              as action,
             st.process$status
        from svc_sec_group_loc_matrix st,
             sec_group_loc_matrix uk_sec_group_loc_matrix,
             district sgl_dis_fk,
             region sgl_reg_fk,
             sec_group sgl_sgu_fk,
             store sgl_str_fk,
             wh sgl_wah_fk,
             code_detail cd_fk
       where st.process_id                                  = I_process_id
         and st.chunk_id                                    = I_chunk_id
         and NVL(st.wh,-1)                                  = NVL(uk_sec_group_loc_matrix.wh (+),-1)
         and NVL(st.store,-1)                               = NVL(uk_sec_group_loc_matrix.store (+),-1)
         and NVL(NVL(st.district,sgl_str_fk.district),-1)   = NVL(uk_sec_group_loc_matrix.district (+),-1)  
         and NVL(NVL(st.region,sgl_dis_fk.region),-1)       = NVL(uk_sec_group_loc_matrix.region (+),-1)
         and st.group_id                                    = uk_sec_group_loc_matrix.group_id (+)
         and st.column_code                                 = uk_sec_group_loc_matrix.column_code (+)
         and st.wh                                          = sgl_wah_fk.wh (+)
         and st.store                                       = sgl_str_fk.store (+)
         and st.group_id                                    = sgl_sgu_fk.group_id (+)
         and NVL(st.region,sgl_dis_fk.region)               = sgl_reg_fk.region (+)
         and NVL(st.district,sgl_str_fk.district)           = sgl_dis_fk.district (+)
         and st.column_code                                 = cd_fk.code(+)
         and cd_fk.code_type(+)                             = 'LSEC';


   TYPE ERRORS_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab ERRORS_tab_typ;
   TYPE S9T_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab S9T_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet     IN   VARCHAR2,
                           I_row_seq   IN   NUMBER,
                           I_col       IN   VARCHAR2,
                           I_sqlcode   IN   NUMBER,
                           I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := I_sqlcode||':'||I_sqlerrm;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets               s9t_pkg.names_map_typ;
   SEC_GRP_LC_MTRX_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets                          := S9T_PKG.get_sheet_names(I_file_id);
   SEC_GRP_LC_MTRX_cols              := S9T_PKG.get_col_names(I_file_id,SEC_GRP_LC_MTRX_sheet);
   SEC_GRP_LC_MTRX$ACTION            := SEC_GRP_LC_MTRX_cols('ACTION');
   SEC_GRP_LC_MTRX$COLUMN_CODE       := SEC_GRP_LC_MTRX_cols('COLUMN_CODE');
   SEC_GRP_LC_MTRX$GROUP_ID          := SEC_GRP_LC_MTRX_cols('GROUP_ID');
   SEC_GRP_LC_MTRX$REGION            := SEC_GRP_LC_MTRX_cols('REGION');
   SEC_GRP_LC_MTRX$DISTRICT          := SEC_GRP_LC_MTRX_cols('DISTRICT');
   SEC_GRP_LC_MTRX$STORE             := SEC_GRP_LC_MTRX_cols('STORE');
   SEC_GRP_LC_MTRX$WH                := SEC_GRP_LC_MTRX_cols('WH');
   SEC_GRP_LC_MTRX$SELECT_IND        := SEC_GRP_LC_MTRX_cols('SELECT_IND');
   SEC_GRP_LC_MTRX$UPDATE_IND        := SEC_GRP_LC_MTRX_cols('UPDATE_IND');

END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_SEC_GROUP_LOC_MATRIX( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into
      TABLE( select ss.s9t_rows
               from s9t_folder sf,
                    TABLE(sf.s9t_file_obj.sheets) ss
              where sf.file_id  = I_file_id
                and ss.sheet_name = SEC_GRP_LC_MTRX_sheet )
             select s9t_row(s9t_cells(coresvc_SEC_GROUP_LOC_MATRIX.action_mod,
                                      column_code,
                                      group_id,
                                      region,
                                      district,
                                      store,
                                      wh,
                                      select_ind,
                                      update_ind))
               from sec_group_loc_matrix ;
END POPULATE_SEC_GROUP_LOC_MATRIX;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   s9t_folder.file_name%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(SEC_GRP_LC_MTRX_sheet);
   L_file.sheets(L_file.get_sheet_index(SEC_GRP_LC_MTRX_sheet)).column_headers := s9t_cells('ACTION',
                                                                                            'COLUMN_CODE',
                                                                                            'GROUP_ID',
                                                                                            'REGION',
                                                                                            'DISTRICT',
                                                                                            'STORE',
                                                                                            'WH',
                                                                                            'SELECT_IND',
                                                                                            'UPDATE_IND');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
------------------------------------------------------------------------------- 
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_program   VARCHAR2(64):='CORESVC_SEC_GROUP_LOC_MATRIX.CREATE_S9T';
   L_file      s9t_file;
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;


   if I_template_only_ind = 'N' then
      POPULATE_SEC_GROUP_LOC_MATRIX(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);

   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);

   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SEC_GRP_LC_MTRX( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                       I_process_id   IN   SVC_SEC_GROUP_LOC_MATRIX.PROCESS_ID%TYPE) IS

   TYPE svc_SEC_GRP_LC_MTRX_col_typ IS TABLE OF SVC_SEC_GROUP_LOC_MATRIX%ROWTYPE;
   L_temp_rec                                   SVC_SEC_GROUP_LOC_MATRIX%ROWTYPE;
   svc_SEC_GRP_LC_MTRX_col                      svc_SEC_GRP_LC_MTRX_col_typ :=NEW svc_SEC_GRP_LC_MTRX_col_typ();
   L_process_id                                 SVC_SEC_GROUP_LOC_MATRIX.PROCESS_ID%TYPE;
   L_error                                      BOOLEAN:=FALSE;
   L_default_rec                                SVC_SEC_GROUP_LOC_MATRIX%ROWTYPE;
   L_pk_columns    VARCHAR2(255)  := 'Column code,Group_id';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select update_ind_mi,
             select_ind_mi,
             wh_mi,
             store_mi,
             district_mi,
             region_mi,
             group_id_mi,
             column_code_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = CORESVC_SEC_GROUP_LOC_MATRIX.template_key
                 and wksht_key                                 = 'SEC_GROUP_LOC_MATRIX'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('update_ind' as update_ind,
                                            'select_ind' as select_ind,
                                            'wh' as wh,
                                            'store' as store,
                                            'district' as district,
                                            'region' as region,
                                            'group_id' as group_id,
                                            'column_code' as column_code,
                                            NULL as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select update_ind_dv,
                      select_ind_dv,
                      wh_dv,
                      store_dv,
                      district_dv,
                      region_dv,
                      group_id_dv,
                      column_code_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = CORESVC_SEC_GROUP_LOC_MATRIX.template_key
                          and wksht_key                                     = 'SEC_GROUP_LOC_MATRIX'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'update_ind' as update_ind,
                                                      'select_ind' as select_ind,
                                                      'wh' as wh,
                                                      'store' as store,
                                                      'district' as district,
                                                      'region' as region,
                                                      'group_id' as group_id,
                                                      'column_code' as column_code,
                                                      NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.UPDATE_IND := rec.UPDATE_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_GROUP_LOC_MATRIX',
                            NULL,
                           'UPDATE_IND',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SELECT_IND := rec.SELECT_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_GROUP_LOC_MATRIX',
                            NULL,
                           'SELECT_IND',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.WH := rec.WH_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_GROUP_LOC_MATRIX',
                            NULL,
                           'WH',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.STORE := rec.STORE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_GROUP_LOC_MATRIX',
                            NULL,
                           'STORE',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DISTRICT := rec.DISTRICT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_GROUP_LOC_MATRIX',
                            NULL,
                           'DISTRICT',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.REGION := rec.REGION_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_GROUP_LOC_MATRIX',
                            NULL,
                           'REGION',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.GROUP_ID := rec.GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_GROUP_LOC_MATRIX',
                            NULL,
                           'GROUP_ID',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COLUMN_CODE := rec.COLUMN_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SEC_GROUP_LOC_MATRIX',
                            NULL,
                           'COLUMN_CODE',
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(SEC_GRP_LC_MTRX$Action)                  as action,
          r.get_cell(SEC_GRP_LC_MTRX$UPDATE_IND)              as update_ind,
          r.get_cell(SEC_GRP_LC_MTRX$SELECT_IND)              as select_ind,
          r.get_cell(SEC_GRP_LC_MTRX$WH)                      as wh,
          r.get_cell(SEC_GRP_LC_MTRX$STORE)                   as store,
          r.get_cell(SEC_GRP_LC_MTRX$DISTRICT)                as district,
          r.get_cell(SEC_GRP_LC_MTRX$REGION)                  as region,
          r.get_cell(SEC_GRP_LC_MTRX$GROUP_ID)                as group_id,
          r.get_cell(SEC_GRP_LC_MTRX$COLUMN_CODE)             as column_code,
          r.get_row_seq()                                     as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(SEC_GRP_LC_MTRX_sheet)
  )
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.ACTION := rec.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UPDATE_IND := rec.UPDATE_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            'UPDATE_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SELECT_IND := rec.SELECT_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            'SELECT_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.WH := rec.WH;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            'WH',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.STORE := rec.STORE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            'STORE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DISTRICT := rec.DISTRICT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            'DISTRICT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REGION := rec.REGION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            'REGION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_ID := rec.GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            'GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COLUMN_CODE := rec.COLUMN_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GRP_LC_MTRX_sheet,
                            rec.row_seq,
                            'COLUMN_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_SEC_GROUP_LOC_MATRIX.action_new then
         L_temp_rec.UPDATE_IND  := NVL( L_temp_rec.UPDATE_IND,L_default_rec.UPDATE_IND);
         L_temp_rec.SELECT_IND  := NVL( L_temp_rec.SELECT_IND,L_default_rec.SELECT_IND);
         L_temp_rec.WH          := NVL( L_temp_rec.WH,L_default_rec.WH);
         L_temp_rec.STORE       := NVL( L_temp_rec.STORE,L_default_rec.STORE);
         L_temp_rec.DISTRICT    := NVL( L_temp_rec.DISTRICT,L_default_rec.DISTRICT);
         L_temp_rec.REGION      := NVL( L_temp_rec.REGION,L_default_rec.REGION);
         L_temp_rec.GROUP_ID    := NVL( L_temp_rec.GROUP_ID,L_default_rec.GROUP_ID);
         L_temp_rec.COLUMN_CODE := NVL( L_temp_rec.COLUMN_CODE,L_default_rec.COLUMN_CODE);
      end if;

      if L_temp_rec.column_code is NULL 
         or L_temp_rec.group_id is NULL then
         WRITE_S9T_ERROR(I_file_id,
                         SEC_GRP_LC_MTRX_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_SEC_GRP_LC_MTRX_col.extend();
         svc_SEC_GRP_LC_MTRX_col(svc_SEC_GRP_LC_MTRX_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_SEC_GRP_LC_MTRX_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SEC_GROUP_LOC_MATRIX st
      using(select (case
                    when l_mi_rec.UPDATE_IND_mi    = 'N'
                     and svc_SEC_GRP_LC_MTRX_col(i).action = CORESVC_SEC_GROUP_LOC_MATRIX.action_mod
                     and s1.update_ind is NULL
                    then mt.update_ind
                    else s1.update_ind
                    end) as update_ind,

                   (case
                    when l_mi_rec.SELECT_IND_mi    = 'N'
                     and svc_SEC_GRP_LC_MTRX_col(i).action = CORESVC_SEC_GROUP_LOC_MATRIX.action_mod
                     and s1.select_ind is NULL
                    then mt.select_ind
                    else s1.select_ind
                    end) as select_ind,

                   (case
                    when l_mi_rec.WH_mi    = 'N'
                     and svc_SEC_GRP_LC_MTRX_col(i).action = CORESVC_SEC_GROUP_LOC_MATRIX.action_mod
                     and s1.wh is NULL
                    then mt.wh
                    else s1.wh
                    end) as wh,

                   (case
                    when l_mi_rec.STORE_mi    = 'N'
                     and svc_SEC_GRP_LC_MTRX_col(i).action = CORESVC_SEC_GROUP_LOC_MATRIX.action_mod
                     and s1.store is NULL
                    then mt.store
                    else s1.store
                    end) as store,

                   (case
                    when l_mi_rec.DISTRICT_mi    = 'N'
                     and svc_SEC_GRP_LC_MTRX_col(i).action = CORESVC_SEC_GROUP_LOC_MATRIX.action_mod
                     and s1.district is NULL
                    then mt.district
                    else s1.district
                    end) as district,

                   (case
                    when l_mi_rec.REGION_mi    = 'N'
                     and svc_SEC_GRP_LC_MTRX_col(i).action = CORESVC_SEC_GROUP_LOC_MATRIX.action_mod
                     and s1.region is NULL
                    then mt.region
                    else s1.region
                    end) as region,

                   (case
                    when l_mi_rec.GROUP_ID_mi    = 'N'
                     and svc_SEC_GRP_LC_MTRX_col(i).action = CORESVC_SEC_GROUP_LOC_MATRIX.action_mod
                     and s1.group_id is NULL
                    then mt.group_id
                    else s1.group_id
                    end) as group_id,

                   (case
                    when l_mi_rec.COLUMN_CODE_mi    = 'N'
                     and svc_SEC_GRP_LC_MTRX_col(i).action = CORESVC_SEC_GROUP_LOC_MATRIX.action_mod
                     and s1.column_code is NULL
                    then mt.column_code
                    else s1.column_code
                    end) as column_code,
 
                   NULL as dummy
              from (select svc_SEC_GRP_LC_MTRX_col(i).update_ind as update_ind,
                           svc_SEC_GRP_LC_MTRX_col(i).select_ind as select_ind,
                           svc_SEC_GRP_LC_MTRX_col(i).wh as wh,
                           svc_SEC_GRP_LC_MTRX_col(i).store as store,
                           svc_SEC_GRP_LC_MTRX_col(i).district as district,
                           svc_SEC_GRP_LC_MTRX_col(i).region as region,
                           svc_SEC_GRP_LC_MTRX_col(i).group_id as group_id,
                           svc_SEC_GRP_LC_MTRX_col(i).column_code as column_code,
                           NULL as dummy
                      from dual ) s1,
                           sec_group_loc_matrix mt
                     where mt.wh (+)     = s1.wh   
                       and mt.store (+)     = s1.store 
                       and mt.district (+)     = s1.district
                       and mt.region (+)     = s1.region
                       and mt.group_id (+)     = s1.group_id
                       and mt.column_code (+)     = s1.column_code
                       and 1 = 1 )sq
         on (st.wh            = sq.wh and
             st.store         = sq.store and
             st.district      = sq.district and
             st.region        = sq.region and
             st.group_id      = sq.group_id and
             st.column_code   = sq.column_code and
             svc_SEC_GRP_LC_MTRX_col(i).action IN (CORESVC_SEC_GROUP_LOC_MATRIX.action_del,
                                                   CORESVC_SEC_GROUP_LOC_MATRIX.action_mod))
       when matched then
         update
             set process_id        = svc_SEC_GRP_LC_MTRX_col(i).process_id ,
                 chunk_id          = svc_SEC_GRP_LC_MTRX_col(i).chunk_id ,
                 row_seq           = svc_SEC_GRP_LC_MTRX_col(i).row_seq ,
                 action            = svc_SEC_GRP_LC_MTRX_col(i).action ,
                 process$status    = svc_SEC_GRP_LC_MTRX_col(i).process$status ,
                 update_ind        = sq.update_ind ,
                 select_ind        = sq.select_ind ,
                 create_id         = svc_SEC_GRP_LC_MTRX_col(i).create_id ,
                 create_datetime   = svc_SEC_GRP_LC_MTRX_col(i).create_datetime ,
                 last_upd_id       = svc_SEC_GRP_LC_MTRX_col(i).last_upd_id ,
                 last_upd_datetime = svc_SEC_GRP_LC_MTRX_col(i).last_upd_datetime
       when NOT matched then
         insert(process_id,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                update_ind ,
                select_ind ,
                wh ,
                store ,
                district ,
                region ,
                group_id ,
                column_code ,
                create_id ,
                create_datetime ,
                last_upd_id ,
                last_upd_datetime) values(svc_SEC_GRP_LC_MTRX_col(i).process_id ,
                                          svc_SEC_GRP_LC_MTRX_col(i).chunk_id ,
                                          svc_SEC_GRP_LC_MTRX_col(i).row_seq ,
                                          svc_SEC_GRP_LC_MTRX_col(i).action ,
                                          svc_SEC_GRP_LC_MTRX_col(i).process$status ,
                                          sq.update_ind ,
                                          sq.select_ind ,
                                          sq.wh ,
                                          sq.store ,
                                          sq.district ,
                                          sq.region ,
                                          sq.group_id ,
                                          sq.column_code ,
                                          svc_SEC_GRP_LC_MTRX_col(i).create_id ,
                                          svc_SEC_GRP_LC_MTRX_col(i).create_datetime ,
                                          svc_SEC_GRP_LC_MTRX_col(i).last_upd_id ,
                                          svc_SEC_GRP_LC_MTRX_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
               L_error_code:=sql%bulk_exceptions(i).error_code;
               if L_error_code=1 then
                  L_error_code:=NULL;
                  L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
               end if;

               WRITE_S9T_ERROR( I_file_id,
                               SEC_GRP_LC_MTRX_sheet,
                               svc_SEC_GRP_LC_MTRX_col(sql%bulk_exceptions(i).error_index).row_seq,
                               NULL,
                               L_error_code,
                               L_error_msg);
      END LOOP;
   END;
END PROCESS_S9T_SEC_GRP_LC_MTRX;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64):='CORESVC_SEC_GROUP_LOC_MATRIX.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW S9T_errors_tab_typ();

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_SEC_GRP_LC_MTRX(I_file_id,I_process_id);
   end if;

   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW S9T_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW S9T_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();

      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;

      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_LOC_MATRIX_INS(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_sec_grp_lc_mtrx_temp_rec   IN       SEC_GROUP_LOC_MATRIX%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_SEC_GROUP_LOC_MATRIX.EXEC_SEC_GROUP_LOC_MATRIX_INS';
BEGIN
   insert into sec_group_loc_matrix
        values I_sec_grp_lc_mtrx_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_LOC_MATRIX_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_LOC_MATRIX_UPD(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_sec_grp_lc_mtrx_temp_rec   IN       SEC_GROUP_LOC_MATRIX%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_SEC_GROUP_LOC_MATRIX.EXEC_SEC_GROUP_LOC_MATRIX_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_SEC_GROUP_LOC_MATRIX_LOCK is 
      select 'X'
        from SEC_GROUP_LOC_MATRIX
       where NVL(wh,-1)          = NVL(I_sec_grp_lc_mtrx_temp_rec.wh,-1)
         and NVL(store,-1)       = NVL(I_sec_grp_lc_mtrx_temp_rec.store,-1)
         and NVL(district,-1)    = NVL(I_sec_grp_lc_mtrx_temp_rec.district,-1)
         and NVL(region,-1)      = NVL(I_sec_grp_lc_mtrx_temp_rec.region,-1)
         and NVL(group_id,-1)    = NVL(I_sec_grp_lc_mtrx_temp_rec.group_id,-1)
         and NVL(column_code,-1) = NVL(I_sec_grp_lc_mtrx_temp_rec.column_code,-1)
         for update nowait;
BEGIN
   open C_SEC_GROUP_LOC_MATRIX_LOCK;
   close C_SEC_GROUP_LOC_MATRIX_LOCK;

   update sec_group_loc_matrix
      set select_ind = I_sec_grp_lc_mtrx_temp_rec.select_ind,
          update_ind = I_sec_grp_lc_mtrx_temp_rec.update_ind
    where 1 = 1
      and NVL(wh,-1)          = NVL(I_sec_grp_lc_mtrx_temp_rec.wh,-1)
      and NVL(store,-1)       = NVL(I_sec_grp_lc_mtrx_temp_rec.store,-1)
      and NVL(district,-1)    = NVL(I_sec_grp_lc_mtrx_temp_rec.district,-1)
      and NVL(region,-1)      = NVL(I_sec_grp_lc_mtrx_temp_rec.region,-1)
      and NVL(group_id,-1)    = NVL(I_sec_grp_lc_mtrx_temp_rec.group_id,-1)
      and NVL(column_code,-1) = NVL(I_sec_grp_lc_mtrx_temp_rec.column_code,-1);
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SEC_GROUP_LOC_MTRX',
                                                                I_sec_grp_lc_mtrx_temp_rec.group_id,
                                                                I_sec_grp_lc_mtrx_temp_rec.column_code);
      close C_SEC_GROUP_LOC_MATRIX_LOCK;
      return FALSE;

   when OTHERS then
      if C_SEC_GROUP_LOC_MATRIX_LOCK%ISOPEN then
         close C_SEC_GROUP_LOC_MATRIX_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_LOC_MATRIX_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_LOC_MATRIX_DEL(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_sec_grp_lc_mtrx_temp_rec   IN       SEC_GROUP_LOC_MATRIX%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_SEC_GROUP_LOC_MATRIX.EXEC_SEC_GROUP_LOC_MATRIX_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_SEC_GROUP_LOC_MATRIX_LOCK is 
      select 'X'
        from SEC_GROUP_LOC_MATRIX
       where NVL(wh,-1)          = NVL(I_sec_grp_lc_mtrx_temp_rec.wh,-1)
         and NVL(store,-1)       = NVL(I_sec_grp_lc_mtrx_temp_rec.store,-1)
         and NVL(district,-1)    = NVL(I_sec_grp_lc_mtrx_temp_rec.district,-1)
         and NVL(region,-1)      = NVL(I_sec_grp_lc_mtrx_temp_rec.region,-1)
         and NVL(group_id,-1)    = NVL(I_sec_grp_lc_mtrx_temp_rec.group_id,-1)
         and NVL(column_code,-1) = NVL(I_sec_grp_lc_mtrx_temp_rec.column_code,-1)
         for update nowait;
BEGIN
   open C_SEC_GROUP_LOC_MATRIX_LOCK;
   close C_SEC_GROUP_LOC_MATRIX_LOCK;

   delete
     from sec_group_loc_matrix
    where 1 = 1
      and NVL(wh,-1)          = NVL(I_sec_grp_lc_mtrx_temp_rec.wh,-1)
      and NVL(store,-1)       = NVL(I_sec_grp_lc_mtrx_temp_rec.store,-1)
      and NVL(district,-1)    = NVL(I_sec_grp_lc_mtrx_temp_rec.district,-1)
      and NVL(region,-1)      = NVL(I_sec_grp_lc_mtrx_temp_rec.region,-1)
      and NVL(group_id,-1)    = NVL(I_sec_grp_lc_mtrx_temp_rec.group_id,-1)
      and NVL(column_code,-1) = NVL(I_sec_grp_lc_mtrx_temp_rec.column_code,-1);
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SEC_GROUP_LOC_MTRX',
                                                                I_sec_grp_lc_mtrx_temp_rec.group_id,
                                                                I_sec_grp_lc_mtrx_temp_rec.column_code);
      close C_SEC_GROUP_LOC_MATRIX_LOCK;
      return FALSE;

   when OTHERS then
      if C_SEC_GROUP_LOC_MATRIX_LOCK%ISOPEN then
         close C_SEC_GROUP_LOC_MATRIX_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_LOC_MATRIX_DEL;
--------------------------------------------------------------------------------
FUNCTION GET_HIER_LVL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_error           IN OUT   BOOLEAN,
                      I_rec             IN OUT   C_SVC_SEC_GROUP_LOC_MATRIX%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)   :='CORESVC_SEC_GROUP_LOC_MATRIX.GET_HIER_LVL';  
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SEC_GROUP_LOC_MATRIX';
 
BEGIN
    if I_rec.sgl_dis_fk_rid is NOT NULL 
       and I_rec.store is NOT NULL  
       and I_rec.sgl_str_fk_rid is NOT NULL then 
       if NVL(I_rec.district,'-1') <> NVL(I_rec.str_dist,'-1') then
          WRITE_ERROR(I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'DISTRICT',
                      'NO_STORE_IN_DISTRICT');
          O_error :=TRUE;
       end if;
    end if;

    if I_rec.sgl_reg_fk_rid is NOT NULL 
       and I_rec.district is NOT NULL
       and I_rec.sgl_dis_fk_rid is NOT NULL then     
       if NVL(I_rec.region,'-1') <> NVL(I_rec.dist_reg,'-1') then
          WRITE_ERROR(I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'REGION',
                      'NO_DISTRICT_IN_REGION');
          O_error :=TRUE;
       end if;
    end if;
   return TRUE;
   
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END GET_HIER_LVL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_SEC_GROUP_LOC_MATRIX( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_process_id      IN       SVC_SEC_GROUP_LOC_MATRIX.PROCESS_ID%TYPE,
                                       I_chunk_id        IN       SVC_SEC_GROUP_LOC_MATRIX.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64)                      :='CORESVC_SEC_GROUP_LOC_MATRIX.PROCESS_SEC_GROUP_LOC_MATRIX';
   L_error                      BOOLEAN;
   L_process_error              BOOLEAN                           := FALSE;
   L_sec_grp_lc_mtrx_temp_rec   SEC_GROUP_LOC_MATRIX%ROWTYPE;
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SEC_GROUP_LOC_MATRIX';
BEGIN
   FOR rec IN C_SVC_SEC_GROUP_LOC_MATRIX(I_process_id,
                                         I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      if rec.action is NULL
      or rec.action NOT IN (action_new,action_mod,action_del) then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'ACTION',
                  'INV_ACT');
      L_error :=TRUE;
      end if;
      
      if rec.action = action_new
      and rec.UK_SEC_GROUP_LOC_MATRIX_rid is NOT NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'UK_SEC_GROUP_LOC_MATRIX',
                  'LOC_SEC_EXIST '); 
      L_error :=TRUE;
      end if;
      
      if rec.action IN (action_mod,action_del)
      and rec.UK_SEC_GROUP_LOC_MATRIX_rid is NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'UK_SEC_GROUP_LOC_MATRIX',
                  'INV_LOC_SEC_GROUP_MATRIX ');
      L_error :=TRUE;
      end if; 
      
      if rec.district is NULL
      and rec.region is NULL
      and rec.store is NULL
      and rec.wh is NULL then
      
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'HIER_LVL',
                  'SELECT_HIER');
      L_error :=TRUE;
      end if;
      
      if rec.district is NOT NULL
      and rec.sgl_dis_fk_rid is NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'DISTRICT',
                  'INV_DIST');
      L_error :=TRUE;
      end if;   
      
      if rec.region is NOT NULL
      and rec.sgl_reg_fk_rid is NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'REGION',
                  'INV_REG');
      L_error :=TRUE;
      end if;
      
      if rec.group_id is NOT NULL
      and rec.sgl_sgu_fk_rid is NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'GROUP_ID',
                  'INV_GROUP_ID');
      L_error :=TRUE;
      end if;
      
      if rec.column_code is NOT NULL
      and rec.cd_fk_rid is NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'COLUMN_CODE',
                  'INV_COLUMN_CODE'); 
      L_error :=TRUE;
      end if;
      
      if rec.store is NOT NULL
      and rec.sgl_str_fk_rid is NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'STORE',
                 'INV_STORE');
      L_error :=TRUE;
      end if;
      
      if rec.wh is NOT NULL
      and rec.sgl_wah_fk_rid is NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'WH',
                  'INV_WH');
      L_error :=TRUE;
      end if;
      
      if rec.wh is NOT NULL and rec.store is NOT NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'STORE,WH',
                  'EITHER_STORE_WH'); 
      L_error :=TRUE;
      end if;

      if rec.wh is NOT NULL and (rec.district is NOT NULL or rec.region is NOT NULL) then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'WH',
                  'HIER_NOT_ALLOWED'); 
      L_error :=TRUE;
      end if; 
      
      if GET_HIER_LVL(O_error_message,
                      L_error,
                      rec)=FALSE then
      WRITE_ERROR(I_process_id,
             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
             I_chunk_id,
             L_table,
             rec.row_seq,
             'HIER_LVL',
             O_error_message);
      L_error :=TRUE;
      end if;
      
       
      if rec.select_ind is NULL then
         rec.select_ind:='N';
      end if;
      
      if rec.update_ind is NULL then
         rec.update_ind:='N';
      end if;
 
      if rec.action=action_new 
       and NOT((rec.update_ind !='Y') or (rec.update_ind = 'Y' and rec.select_ind = 'Y' )) then
       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_table,
                   rec.row_seq,
                   'SELECT_IND',
                   'INV_SEL_UPD_IND');
       L_error :=TRUE;
      end if;
      
      if rec.action=action_mod
       and rec.select_ind = 'N' then
       rec.update_ind:='N';
      end if;
      
      if NOT( rec.store is NULL or (rec.store is NOT NULL and rec.district is NOT NULL )) then
       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_table,
                   rec.row_seq,
                   'STORE',
                   'NO_DIST_FOR_STORE');
       L_error :=TRUE;
      end if;
      
      if NOT(rec.district is NULL or (rec.district is NOT NULL and rec.region is NOT NULL )) then
       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_table,
                   rec.row_seq,
                   'DISTRICT',
                   'NO_REG_FOR_DIST');
       L_error :=TRUE;
      end if;
      
      if rec.update_ind is NOT NULL 
       and rec.update_ind NOT IN ( 'Y','N' ) then
       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_table,
                   rec.row_seq,
                   'UPDATE_IND',
                   'INV_Y_N_IND');
       L_error :=TRUE;
      end if;
      
      if rec.select_ind is NOT NULL
       and rec.select_ind NOT IN ( 'Y','N' ) then
       WRITE_ERROR(I_process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_chunk_id,
                   L_table,
                   rec.row_seq,
                   'SELECT_IND',
                   'INV_Y_N_IND');
       L_error :=TRUE;
      end if;
     
      if NOT L_error then
         L_sec_grp_lc_mtrx_temp_rec.column_code           := rec.column_code;
         L_sec_grp_lc_mtrx_temp_rec.group_id              := rec.group_id;
         L_sec_grp_lc_mtrx_temp_rec.region                := rec.region;
         L_sec_grp_lc_mtrx_temp_rec.district              := rec.district;
         L_sec_grp_lc_mtrx_temp_rec.store                 := rec.store;
         L_sec_grp_lc_mtrx_temp_rec.wh                    := rec.wh;
         L_sec_grp_lc_mtrx_temp_rec.select_ind            := rec.select_ind;
         L_sec_grp_lc_mtrx_temp_rec.update_ind            := rec.update_ind;
   
         if rec.action = action_new then
            if EXEC_SEC_GROUP_LOC_MATRIX_INS( O_error_message,
                                              L_sec_grp_lc_mtrx_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
            
         if rec.action = action_mod then
            if EXEC_SEC_GROUP_LOC_MATRIX_UPD( O_error_message,
                                              L_sec_grp_lc_mtrx_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
                  
         if rec.action = action_del then
            if EXEC_SEC_GROUP_LOC_MATRIX_DEL( O_error_message,
                                              L_sec_grp_lc_mtrx_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
       end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_SEC_GROUP_LOC_MATRIX%ISOPEN then
         CLOSE C_SVC_SEC_GROUP_LOC_MATRIX;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SEC_GROUP_LOC_MATRIX;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_sec_group_loc_matrix 
      where process_id = I_process_id;
END;
--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64):='CORESVC_SEC_GROUP_LOC_MATRIX.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE   :='PS';     
BEGIN
   LP_errors_tab := NEW ERRORS_tab_typ();

   if PROCESS_SEC_GROUP_LOC_MATRIX(O_error_message,
                                   I_process_id,
                                   I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();

   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);

   LP_errors_tab := NEW ERRORS_tab_typ();
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
 else
    L_process_status := 'PE';
 end if;
  
   update svc_process_tracker
    set status = (CASE
                 when status = 'PE'
                 then 'PE'
                    else L_process_status
                    END),
        action_date = SYSDATE
  where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);     
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_SEC_GROUP_LOC_MATRIX;
--------------------------------------------------------------------------------------------------------------------------------------
/
