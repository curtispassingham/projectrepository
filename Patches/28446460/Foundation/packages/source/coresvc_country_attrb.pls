create or replace PACKAGE BODY CORESVC_COUNTRY_ATTR AS

   cursor C_SVC_COUNTRY_ATTR(I_process_id NUMBER,I_chunk_id NUMBER) is
      select pk_country_attrib.rowid                     as pk_country_attrib_rid,
             st.rowid                                    as st_rid,
             coat_cnt_fk.rowid                           as coat_cnt_fk_rid,
             UPPER(st.default_loc_type)                  as default_loc_type,
             st.default_loc,
             pk_country_attrib.default_loc               as old_default_loc,
             UPPER(st.default_cost_comp_cost)            as default_cost_comp_cost,
             UPPER(st.default_deal_cost)                 as default_deal_cost,
             UPPER(st.default_po_cost)                   as default_po_cost,
             pk_country_attrib.default_po_cost           as old_default_po_cost,
             UPPER(st.item_cost_tax_incl_ind)            as item_cost_tax_incl_ind,
             pk_country_attrib.item_cost_tax_incl_ind    as old_item_cost_tax_incl,
             UPPER(st.localized_ind)                     as localized_ind,
             st.country_desc,
             UPPER(st.country_id)                        as country_id,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)                            as action,
             st.process$status
        from svc_country_attr st,
             country_attrib pk_country_attrib,
             country coat_cnt_fk,
             dual
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
         and UPPER(st.country_id) = pk_country_attrib.country_id (+)
         and UPPER(st.country_id) = coat_cnt_fk.country_id (+);

   LP_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   TYPE errors_tab_typ IS TABLE OF svc_admin_upld_er%ROWTYPE;
   LP_errors_tab           errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab       s9t_errors_tab_typ;
   Type CTRY_TL_TAB IS TABLE OF COUNTRY_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit     CONSTANT NUMBER(12) := 1000;
   LP_primary_lang         LANG.LANG%TYPE;

--------------------------------------------------------------------------------

FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------

PROCEDURE WRITE_S9T_ERROR(I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet   IN VARCHAR2,
                          I_row_seq IN NUMBER,
                          I_col     IN VARCHAR2,
                          I_sqlcode IN NUMBER,
                          I_sqlerrm IN VARCHAR2) IS
BEGIN

   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key             :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;

END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------

PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN

   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   LP_errors_tab(LP_errors_tab.COUNT()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------

PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS

   L_sheets            s9t_pkg.names_map_typ;
   COUNTRY_ATTR_cols   s9t_pkg.names_map_typ;
   CTRY_TL_cols        s9t_pkg.names_map_typ;

BEGIN

   L_sheets                       := s9t_pkg.get_sheet_names(I_file_id);
   COUNTRY_ATTR_cols              := s9t_pkg.get_col_names(I_file_id,COUNTRY_ATTR_sheet);
   COUNTRY_ATTR$Action            := COUNTRY_ATTR_cols('ACTION');
   COUNTRY_ATTR$DEFAULT_LOC_TYP   := COUNTRY_ATTR_cols('DEFAULT_LOC_TYPE');
   COUNTRY_ATTR$DEFAULT_LOC       := COUNTRY_ATTR_cols('DEFAULT_LOC');
   CNTR_ATTR$DEF_COST_COMP_COST   := COUNTRY_ATTR_cols('DEFAULT_COST_COMP_COST');
   COUNTRY_ATTR$DEFAULT_DEAL_COST := COUNTRY_ATTR_cols('DEFAULT_DEAL_COST');
   COUNTRY_ATTR$DEFAULT_PO_COST   := COUNTRY_ATTR_cols('DEFAULT_PO_COST');
   CNTR_ATTR$ITM_CST_TAX_INCL_IND := COUNTRY_ATTR_cols('ITEM_COST_TAX_INCL_IND');
   COUNTRY_ATTR$LOCALIZED_IND     := COUNTRY_ATTR_cols('LOCALIZED_IND');
   COUNTRY_ATTR$COUNTRY_DESC      := COUNTRY_ATTR_cols('COUNTRY_DESC');
   COUNTRY_ATTR$COUNTRY_ID        := COUNTRY_ATTR_cols('COUNTRY_ID');

   CTRY_TL_cols                   := s9t_pkg.get_col_names(I_file_id,CTRY_TL_sheet);
   CTRY_TL$Action                 := CTRY_TL_cols('ACTION');
   CTRY_TL$LANG                   := CTRY_TL_cols('LANG');
   CTRY_TL$COUNTRY_ID             := CTRY_TL_cols('COUNTRY_ID');
   CTRY_TL$COUNTRY_DESC           := CTRY_TL_cols('COUNTRY_DESC');

END POPULATE_NAMES;
--------------------------------------------------------------------------------

PROCEDURE POPULATE_COUNTRY_ATTR(I_file_id IN NUMBER) IS
BEGIN

   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
        TABLE (sf.s9t_file_obj.sheets) ss
        where sf.file_id  = I_file_id
          and ss.sheet_name = COUNTRY_ATTR_sheet)

       select s9t_row(s9t_cells (CORESVC_COUNTRY_ATTR.action_mod,
                      cnt.country_id,
                      cnt.country_desc,
                      ca.localized_ind,
                      ca.item_cost_tax_incl_ind,
                      ca.default_po_cost,
                      ca.default_deal_cost,
                      ca.default_cost_comp_cost,
                      ca.default_loc,
                      ca.default_loc_type
                      ))
       from country cnt,
            country_attrib ca
      where cnt.country_id =  ca.country_id;

END POPULATE_COUNTRY_ATTR;
--------------------------------------------------------------------------------

PROCEDURE POPULATE_CTRY_TL(I_file_id   IN   NUMBER) IS
BEGIN

   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
        TABLE (sf.s9t_file_obj.sheets) ss
        where sf.file_id  = I_file_id
          and ss.sheet_name = CTRY_TL_sheet)
       select s9t_row(s9t_cells (CORESVC_COUNTRY_ATTR.action_mod,
                      lang,
                      country_id,
                      country_desc))
       from country_tl
      where lang <> LP_primary_lang;

END POPULATE_CTRY_TL;
--------------------------------------------------------------------------------

PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS

   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN

   L_file              := NEW s9t_file();
   O_file_id           := S9T_FOLDER_SEQ.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(COUNTRY_ATTR_sheet);
   L_file.sheets(L_file.get_sheet_index(COUNTRY_ATTR_sheet)).column_headers := s9t_cells('ACTION',
                                                                                         'COUNTRY_ID',
                                                                                         'COUNTRY_DESC',
                                                                                         'LOCALIZED_IND',
                                                                                         'ITEM_COST_TAX_INCL_IND',
                                                                                         'DEFAULT_PO_COST',
                                                                                         'DEFAULT_DEAL_COST',
                                                                                         'DEFAULT_COST_COMP_COST',
                                                                                         'DEFAULT_LOC',
                                                                                         'DEFAULT_LOC_TYPE');

   L_file.add_sheet(CTRY_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(CTRY_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                    'LANG',
                                                                                    'COUNTRY_ID',
                                                                                    'COUNTRY_DESC');
   s9t_pkg.save_obj(L_file);

END INIT_S9T;
--------------------------------------------------------------------------------

FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_COUNTRY_ATTR.CREATE_S9T';
   L_file      s9t_file;

BEGIN

   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_COUNTRY_ATTR(O_file_id);
      POPULATE_CTRY_TL(O_file_id);
      commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);

   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);

   L_file := S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file)= FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);

   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;

END CREATE_S9T;
--------------------------------------------------------------------------------

PROCEDURE PROCESS_S9T_COUNTRY_ATTR(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id IN SVC_COUNTRY_ATTR.PROCESS_ID%TYPE) IS

   TYPE svc_COUNTRY_ATTR_col_typ IS TABLE OF SVC_COUNTRY_ATTR%ROWTYPE;
   L_temp_rec                                SVC_COUNTRY_ATTR%ROWTYPE;
   svc_COUNTRY_ATTR_col                      svc_COUNTRY_ATTR_col_typ :=NEW svc_COUNTRY_ATTR_col_typ();
   L_process_id                              SVC_COUNTRY_ATTR.process_id%TYPE;
   L_error                                   BOOLEAN                  :=FALSE;
   L_default_rec                             SVC_COUNTRY_ATTR%ROWTYPE;
   cursor C_MANDATORY_IND is
    select DEFAULT_LOC_TYPE_mi,
           DEFAULT_LOC_mi,
           DEFAULT_COST_COMP_COST_mi,
           DEFAULT_DEAL_COST_mi,
           DEFAULT_PO_COST_mi,
           ITEM_COST_TAX_INCL_IND_mi,
           LOCALIZED_IND_mi,
           COUNTRY_DESC_mi,
           COUNTRY_ID_mi
      from (select column_key,
                   mandatory
              from s9t_tmpl_cols_def
             where template_key = CORESVC_COUNTRY_ATTR.template_key
               and wksht_key    = COUNTRY_ATTR_sheet)
     pivot (MAX(mandatory) AS mi FOR (column_key) IN (
                        'DEFAULT_LOC_TYPE' AS DEFAULT_LOC_TYPE,
                        'DEFAULT_LOC' AS DEFAULT_LOC,
                        'DEFAULT_COST_COMP_COST' AS DEFAULT_COST_COMP_COST,
                        'DEFAULT_DEAL_COST' AS DEFAULT_DEAL_COST,
                        'DEFAULT_PO_COST' AS DEFAULT_PO_COST,
                        'ITEM_COST_TAX_INCL_IND' AS ITEM_COST_TAX_INCL_IND,
                        'LOCALIZED_IND' AS LOCALIZED_IND,
                        'COUNTRY_DESC' AS COUNTRY_DESC,
                        'COUNTRY_ID' AS COUNTRY_ID));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COUNTRY_ATTR';
   L_pk_columns    VARCHAR2(255)  := 'Country Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec IN (select DEFAULT_LOC_TYPE_dv,
                      DEFAULT_LOC_dv,
                      DEFAULT_COST_COMP_COST_dv,
                      DEFAULT_DEAL_COST_dv,
                      DEFAULT_PO_COST_dv,
                      ITEM_COST_TAX_INCL_IND_dv,
                      LOCALIZED_IND_dv,
                      COUNTRY_DESC_dv,
                      COUNTRY_ID_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_COUNTRY_ATTR.template_key
                          and wksht_key    = COUNTRY_ATTR_sheet)
                pivot (MAX(default_value) AS dv FOR (column_key) IN ('DEFAULT_LOC_TYPE' AS DEFAULT_LOC_TYPE,
                                                                     'DEFAULT_LOC' AS DEFAULT_LOC,
                                                                     'DEFAULT_COST_COMP_COST' AS DEFAULT_COST_COMP_COST,
                                                                     'DEFAULT_DEAL_COST' AS DEFAULT_DEAL_COST,
                                                                     'DEFAULT_PO_COST' AS DEFAULT_PO_COST,
                                                                     'ITEM_COST_TAX_INCL_IND' AS ITEM_COST_TAX_INCL_IND,
                                                                     'LOCALIZED_IND' AS LOCALIZED_IND,
                                                                     'COUNTRY_DESC' AS COUNTRY_DESC,
                                                                     'COUNTRY_ID' AS COUNTRY_ID))
              )
   LOOP
      BEGIN
         L_default_rec.DEFAULT_LOC_TYPE := rec.DEFAULT_LOC_TYPE_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'DEFAULT_LOC_TYPE',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DEFAULT_LOC := rec.DEFAULT_LOC_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'DEFAULT_LOC',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DEFAULT_COST_COMP_COST := rec.DEFAULT_COST_COMP_COST_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'DEFAULT_COST_COMP_COST',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DEFAULT_DEAL_COST := rec.DEFAULT_DEAL_COST_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'DEFAULT_DEAL_COST',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DEFAULT_PO_COST := rec.DEFAULT_PO_COST_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'DEFAULT_PO_COST',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ITEM_COST_TAX_INCL_IND := rec.ITEM_COST_TAX_INCL_IND_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'ITEM_COST_TAX_INCL_IND',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LOCALIZED_IND := rec.LOCALIZED_IND_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'LOCALIZED_IND',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COUNTRY_DESC := rec.COUNTRY_DESC_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'COUNTRY_DESC',
                               NULL,
                               'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COUNTRY_ID := rec.COUNTRY_ID_dv;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               'V_COUNTRY_ATTR',
                               NULL,
                               'COUNTRY_ID',
                               NULL,
                               'INV_DEFAULT');
      END;

   END LOOP;

   OPEN C_MANDATORY_IND;
   FETCH C_MANDATORY_IND INTO L_mi_rec;
   CLOSE C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(COUNTRY_ATTR$Action)            AS Action,
                      r.get_cell(COUNTRY_ATTR$DEFAULT_LOC_TYP)   AS DEFAULT_LOC_TYPE,
                      r.get_cell(COUNTRY_ATTR$DEFAULT_LOC)       AS DEFAULT_LOC,
                      r.get_cell(CNTR_ATTR$DEF_COST_COMP_COST)   AS DEFAULT_COST_COMP_COST,
                      r.get_cell(COUNTRY_ATTR$DEFAULT_DEAL_COST) AS DEFAULT_DEAL_COST,
                      r.get_cell(COUNTRY_ATTR$DEFAULT_PO_COST)   AS DEFAULT_PO_COST,
                      r.get_cell(CNTR_ATTR$ITM_CST_TAX_INCL_IND) AS ITEM_COST_TAX_INCL_IND,
                      r.get_cell(COUNTRY_ATTR$LOCALIZED_IND)     AS LOCALIZED_IND,
                      r.get_cell(COUNTRY_ATTR$COUNTRY_DESC)      AS COUNTRY_DESC,
                      UPPER(r.get_cell(COUNTRY_ATTR$COUNTRY_ID)) AS COUNTRY_ID,
                      r.get_row_seq()                            AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(COUNTRY_ATTR_sheet)
              )
   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

      BEGIN
         L_temp_rec.Action := rec.Action;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               action_column,
                               SQLCODE,SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEFAULT_LOC_TYPE := rec.DEFAULT_LOC_TYPE;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'DEFAULT_LOC_TYPE',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEFAULT_LOC := rec.DEFAULT_LOC;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'DEFAULT_LOC',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEFAULT_COST_COMP_COST := rec.DEFAULT_COST_COMP_COST;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'DEFAULT_COST_COMP_COST',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEFAULT_DEAL_COST := rec.DEFAULT_DEAL_COST;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'DEFAULT_DEAL_COST',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEFAULT_PO_COST := rec.DEFAULT_PO_COST;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'DEFAULT_PO_COST',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ITEM_COST_TAX_INCL_IND := rec.ITEM_COST_TAX_INCL_IND;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'ITEM_COST_TAX_INCL_IND',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LOCALIZED_IND := rec.LOCALIZED_IND;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'LOCALIZED_IND',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COUNTRY_DESC := rec.COUNTRY_DESC;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'COUNTRY_DESC',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COUNTRY_ID := rec.COUNTRY_ID;
         EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               COUNTRY_ATTR_sheet,
                               rec.row_seq,
                               'COUNTRY_ID',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;

      if rec.action = CORESVC_COUNTRY_ATTR.action_new then
         L_temp_rec.DEFAULT_LOC_TYPE       := NVL( L_temp_rec.DEFAULT_LOC_TYPE,L_default_rec.DEFAULT_LOC_TYPE);
         L_temp_rec.DEFAULT_LOC            := NVL( L_temp_rec.DEFAULT_LOC,L_default_rec.DEFAULT_LOC);
         L_temp_rec.DEFAULT_COST_COMP_COST := NVL( L_temp_rec.DEFAULT_COST_COMP_COST,L_default_rec.DEFAULT_COST_COMP_COST);
         L_temp_rec.DEFAULT_DEAL_COST      := NVL( L_temp_rec.DEFAULT_DEAL_COST,L_default_rec.DEFAULT_DEAL_COST);
         L_temp_rec.DEFAULT_PO_COST        := NVL( L_temp_rec.DEFAULT_PO_COST,L_default_rec.DEFAULT_PO_COST);
         L_temp_rec.ITEM_COST_TAX_INCL_IND := NVL( L_temp_rec.ITEM_COST_TAX_INCL_IND,L_default_rec.ITEM_COST_TAX_INCL_IND);
         L_temp_rec.LOCALIZED_IND          := NVL( L_temp_rec.LOCALIZED_IND,L_default_rec.LOCALIZED_IND);
         L_temp_rec.COUNTRY_DESC           := NVL( L_temp_rec.COUNTRY_DESC,L_default_rec.COUNTRY_DESC);
         L_temp_rec.COUNTRY_ID             := NVL( L_temp_rec.COUNTRY_ID,L_default_rec.COUNTRY_ID);
      end if;

      if NOT (L_temp_rec.COUNTRY_ID is NOT NULL) then
         WRITE_S9T_ERROR( I_file_id,
                          COUNTRY_ATTR_sheet,
                          rec.row_seq,

                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
        L_error:=TRUE;
      end if;

      if NOT L_error then
         svc_COUNTRY_ATTR_col.EXTEND();
         svc_COUNTRY_ATTR_col(svc_COUNTRY_ATTR_col.COUNT()):=L_temp_rec;
      end if;

   END LOOP;

   BEGIN

   FORALL i IN 1..svc_COUNTRY_ATTR_col.COUNT
      SAVE EXCEPTIONS Merge INTO SVC_COUNTRY_ATTR st USING
         (select (case
                     when L_mi_rec.DEFAULT_LOC_TYPE_mi    = 'N'
                      and svc_COUNTRY_ATTR_col(i).action  = CORESVC_COUNTRY_ATTR.action_mod
                      and s1.DEFAULT_LOC_TYPE             IS NULL
                     then mta.DEFAULT_LOC_TYPE
                     else s1.DEFAULT_LOC_TYPE
                 end) AS DEFAULT_LOC_TYPE,
                (case
                    when L_mi_rec.DEFAULT_LOC_mi        = 'N'
                     and svc_COUNTRY_ATTR_col(i).action = CORESVC_COUNTRY_ATTR.action_mod
                     and s1.DEFAULT_LOC             IS NULL
                    then mta.DEFAULT_LOC
                    else s1.DEFAULT_LOC
                end) AS DEFAULT_LOC,
                (case
                    when L_mi_rec.DEFAULT_COST_COMP_COST_mi    = 'N'
                     and svc_COUNTRY_ATTR_col(i).action        = CORESVC_COUNTRY_ATTR.action_mod
                     and s1.DEFAULT_COST_COMP_COST             IS NULL
                    then mta.DEFAULT_COST_COMP_COST
                    else s1.DEFAULT_COST_COMP_COST
                end) AS DEFAULT_COST_COMP_COST,
                (case
                    when L_mi_rec.DEFAULT_DEAL_COST_mi    = 'N'
                     and svc_COUNTRY_ATTR_col(i).action   = CORESVC_COUNTRY_ATTR.action_mod
                     and s1.DEFAULT_DEAL_COST             IS NULL
                    then mta.DEFAULT_DEAL_COST
                    else s1.DEFAULT_DEAL_COST
                end) AS DEFAULT_DEAL_COST,
                (case
                    when L_mi_rec.DEFAULT_PO_COST_mi    = 'N'
                     and svc_COUNTRY_ATTR_col(i).action = CORESVC_COUNTRY_ATTR.action_mod
                     and s1.DEFAULT_PO_COST             IS NULL
                    then mta.DEFAULT_PO_COST
                    else s1.DEFAULT_PO_COST
                end) AS DEFAULT_PO_COST,
                (case
                    when L_mi_rec.ITEM_COST_TAX_INCL_IND_mi    = 'N'
                     and svc_COUNTRY_ATTR_col(i).action        = CORESVC_COUNTRY_ATTR.action_mod
                     and s1.ITEM_COST_TAX_INCL_IND             IS NULL
                    then mta.ITEM_COST_TAX_INCL_IND
                    else s1.ITEM_COST_TAX_INCL_IND
                end) AS ITEM_COST_TAX_INCL_IND,
                (case
                    when L_mi_rec.LOCALIZED_IND_mi      = 'N'
                     and svc_COUNTRY_ATTR_col(i).action = CORESVC_COUNTRY_ATTR.action_mod
                     and s1.LOCALIZED_IND             IS NULL
                    then mta.LOCALIZED_IND
                    else s1.LOCALIZED_IND
                end) AS LOCALIZED_IND,
                (case
                    when L_mi_rec.COUNTRY_DESC_mi       = 'N'
                     and svc_COUNTRY_ATTR_col(i).action = CORESVC_COUNTRY_ATTR.action_mod
                     and s1.COUNTRY_DESC             IS NULL
                    then mtc.COUNTRY_DESC
                    else s1.COUNTRY_DESC
                end) AS COUNTRY_DESC,
                (case
                    when L_mi_rec.COUNTRY_ID_mi         = 'N'
                     and svc_COUNTRY_ATTR_col(i).action = CORESVC_COUNTRY_ATTR.action_mod
                     and s1.COUNTRY_ID             IS NULL
                    then mtc.COUNTRY_ID
                    else s1.COUNTRY_ID
                end) AS COUNTRY_ID,
                null as dummy
         from (select svc_COUNTRY_ATTR_col(i).DEFAULT_LOC_TYPE       AS DEFAULT_LOC_TYPE,
                      svc_COUNTRY_ATTR_col(i).DEFAULT_LOC            AS DEFAULT_LOC,
                      svc_COUNTRY_ATTR_col(i).DEFAULT_COST_COMP_COST AS DEFAULT_COST_COMP_COST,
                      svc_COUNTRY_ATTR_col(i).DEFAULT_DEAL_COST      AS DEFAULT_DEAL_COST,
                      svc_COUNTRY_ATTR_col(i).DEFAULT_PO_COST        AS DEFAULT_PO_COST,
                      svc_COUNTRY_ATTR_col(i).ITEM_COST_TAX_INCL_IND AS ITEM_COST_TAX_INCL_IND,
                      svc_COUNTRY_ATTR_col(i).LOCALIZED_IND          AS LOCALIZED_IND,
                      svc_COUNTRY_ATTR_col(i).COUNTRY_DESC           AS COUNTRY_DESC,
                      svc_COUNTRY_ATTR_col(i).COUNTRY_ID             AS COUNTRY_ID
               from dual) s1,
                          country mtc,
                          country_attrib mta
         where mtc.COUNTRY_ID (+) = s1.COUNTRY_ID
           and mta.COUNTRY_ID (+) = s1.COUNTRY_ID) sq
                  ON (st.COUNTRY_ID = sq.COUNTRY_ID
                 and svc_COUNTRY_ATTR_col(i).ACTION in (CORESVC_COUNTRY_ATTR.action_mod,CORESVC_COUNTRY_ATTR.action_del))
        when matched then
           update set PROCESS_ID               = svc_COUNTRY_ATTR_col(i).PROCESS_ID ,
                      CHUNK_ID                 = svc_COUNTRY_ATTR_col(i).CHUNK_ID ,
                      ROW_SEQ                  = svc_COUNTRY_ATTR_col(i).ROW_SEQ ,
                      PROCESS$STATUS           = svc_COUNTRY_ATTR_col(i).PROCESS$STATUS ,
                      action                   = svc_COUNTRY_ATTR_col(i).ACTION,
                      DEFAULT_LOC              = sq.DEFAULT_LOC ,
                      COUNTRY_DESC             = sq.COUNTRY_DESC ,
                      LOCALIZED_IND            = sq.LOCALIZED_IND ,
                      DEFAULT_PO_COST          = sq.DEFAULT_PO_COST ,
                      DEFAULT_COST_COMP_COST   = sq.DEFAULT_COST_COMP_COST ,
                      DEFAULT_DEAL_COST        = sq.DEFAULT_DEAL_COST ,
                      ITEM_COST_TAX_INCL_IND   = sq.ITEM_COST_TAX_INCL_IND ,
                      DEFAULT_LOC_TYPE         = sq.DEFAULT_LOC_TYPE ,
                      CREATE_ID                = svc_COUNTRY_ATTR_col(i).CREATE_ID ,
                      CREATE_DATETIME          = svc_COUNTRY_ATTR_col(i).CREATE_DATETIME ,
                      LAST_UPD_ID              = svc_COUNTRY_ATTR_col(i).LAST_UPD_ID ,
                      LAST_UPD_DATETIME        = svc_COUNTRY_ATTR_col(i).LAST_UPD_DATETIME
        when NOT matched then
           insert(PROCESS_ID ,
                  CHUNK_ID ,
                  ROW_SEQ ,
                  ACTION ,
                  PROCESS$STATUS ,
                  DEFAULT_LOC_TYPE ,
                  DEFAULT_LOC ,
                  DEFAULT_COST_COMP_COST ,
                  DEFAULT_DEAL_COST ,
                  DEFAULT_PO_COST ,
                  ITEM_COST_TAX_INCL_IND ,
                  LOCALIZED_IND ,
                  COUNTRY_DESC ,
                  COUNTRY_ID ,
                  CREATE_ID ,
                  CREATE_DATETIME ,
                  LAST_UPD_ID ,
                  LAST_UPD_DATETIME)
           values(svc_COUNTRY_ATTR_col(i).PROCESS_ID ,
                  svc_COUNTRY_ATTR_col(i).CHUNK_ID ,
                  svc_COUNTRY_ATTR_col(i).ROW_SEQ ,
                  svc_COUNTRY_ATTR_col(i).ACTION ,
                  svc_COUNTRY_ATTR_col(i).PROCESS$STATUS ,
                  sq.DEFAULT_LOC_TYPE ,
                  sq.DEFAULT_LOC ,
                  sq.DEFAULT_COST_COMP_COST ,
                  sq.DEFAULT_DEAL_COST ,
                  sq.DEFAULT_PO_COST ,
                  sq.ITEM_COST_TAX_INCL_IND ,
                  sq.LOCALIZED_IND ,
                  sq.COUNTRY_DESC ,
                  sq.COUNTRY_ID ,
                  svc_COUNTRY_ATTR_col(i).CREATE_ID ,
                  svc_COUNTRY_ATTR_col(i).CREATE_DATETIME ,
                  svc_COUNTRY_ATTR_col(i).LAST_UPD_ID ,
                  svc_COUNTRY_ATTR_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            COUNTRY_ATTR_sheet,
                            svc_COUNTRY_ATTR_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   when OTHERS then
      if C_MANDATORY_IND%ISOPEN then
         close C_MANDATORY_IND;
      end if;
      rollback;
END;
END PROCESS_S9T_COUNTRY_ATTR;

--------------------------------------------------------------------------------

PROCEDURE PROCESS_S9T_CTRY_TL(I_file_id     IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id  IN   SVC_COUNTRY_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_CTRY_TL_COL_TYP is TABLE OF SVC_COUNTRY_TL%ROWTYPE;
   L_temp_rec           SVC_COUNTRY_TL%ROWTYPE;
   SVC_COUNTRY_TL_col   SVC_CTRY_TL_COL_TYP := NEW SVC_CTRY_TL_COL_TYP();
   L_process_id         SVC_COUNTRY_TL.PROCESS_ID%TYPE;
   L_error              BOOLEAN                  :=FALSE;
   L_default_rec        SVC_COUNTRY_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             country_id_mi,
             country_desc_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = CORESVC_COUNTRY_ATTR.template_key
                 and wksht_key    = CTRY_TL_sheet)
       pivot (MAX(mandatory) AS mi FOR (column_key) IN ('LANG' AS LANG,
                                                        'COUNTRY_ID' AS COUNTRY_ID,
                                                        'COUNTRY_DESC' AS COUNTRY_DESC));

   L_mi_rec             C_MANDATORY_IND%ROWTYPE;
   dml_errors           EXCEPTION;
   PRAGMA               exception_init(dml_errors, -24381);
   L_table              VARCHAR2(30)   := 'SVC_COUNTRY_TL';
   L_pk_columns         VARCHAR2(255)  := 'Country Code, Lang';
   L_error_code         NUMBER;
   L_error_msg          RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec IN (select lang_dv,
                      country_id_dv,
                      country_desc_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_COUNTRY_ATTR.template_key
                          and wksht_key    = CTRY_TL_sheet)
                pivot (MAX(default_value) AS dv FOR (column_key) IN ('LANG' AS LANG,
                                                                     'COUNTRY_ID' AS COUNTRY_ID,
                                                                     'COUNTRY_DESC' AS COUNTRY_DESC))
              )
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CTRY_TL_sheet,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.country_id := rec.country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CTRY_TL_sheet,
                            NULL,
                            'COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.country_desc := rec.country_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CTRY_TL_sheet,
                            NULL,
                            'COUNTRY_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   OPEN C_MANDATORY_IND;
   FETCH C_MANDATORY_IND INTO L_mi_rec;
   CLOSE C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(ctry_tl$action)            as action,
                      r.get_cell(ctry_tl$lang)              as lang,
                      UPPER(r.get_cell(ctry_tl$country_id)) as country_id,
                      r.get_cell(ctry_tl$country_desc)      as country_desc,
                      r.get_row_seq()                       as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(CTRY_TL_sheet)
              )
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CTRY_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CTRY_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.country_id := rec.country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CTRY_TL_sheet,
                            rec.row_seq,
                            'COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.country_desc := rec.country_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CTRY_TL_sheet,
                            rec.row_seq,
                            'COUNTRY_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_COUNTRY_ATTR.action_new then
         L_temp_rec.lang         := NVL( L_temp_rec.lang, L_default_rec.lang);
         L_temp_rec.country_id   := NVL( L_temp_rec.country_id,L_default_rec.country_id);
         L_temp_rec.country_desc := NVL( L_temp_rec.country_desc,L_default_rec.country_desc);
      end if;

      if NOT (L_temp_rec.country_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         CTRY_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error:=TRUE;
      end if;

      if NOT L_error then
         SVC_COUNTRY_TL_col.EXTEND();
         SVC_COUNTRY_TL_col(SVC_COUNTRY_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..SVC_COUNTRY_TL_col.COUNT SAVE EXCEPTIONS
         merge into svc_country_tl st
         using (select (case
                        when L_mi_rec.lang_mi      = 'N'
                         and SVC_COUNTRY_TL_col(i).action = CORESVC_COUNTRY_ATTR.action_mod
                         and s1.lang is NULL
                        then mt.lang
                        else s1.lang
                        end) as lang,
                       (case
                        when L_mi_rec.country_id_mi         = 'N'
                         and SVC_COUNTRY_TL_col(i).action = CORESVC_COUNTRY_ATTR.action_mod
                         and s1.country_id is NULL
                        then mt.country_id
                        else s1.country_id
                        end) as country_id,
                       (case
                        when L_mi_rec.country_desc_mi       = 'N'
                         and SVC_COUNTRY_TL_col(i).action = CORESVC_COUNTRY_ATTR.action_mod
                         and s1.country_desc is NULL
                        then mt.country_desc
                        else s1.country_desc
                        end) as country_desc,
                       NULL as dummy
               from (select SVC_COUNTRY_TL_col(i).lang         as lang,
                            SVC_COUNTRY_TL_col(i).country_id   as country_id,
                            SVC_COUNTRY_TL_col(i).country_desc as country_desc
                       from dual) s1,
                     country_tl mt
               where mt.lang (+) = s1.lang
                 and mt.country_id (+) = s1.country_id) sq
                 ON (st.country_id = sq.country_id
                     and st.lang = sq.lang
                     and SVC_COUNTRY_TL_col(i).action in (CORESVC_COUNTRY_ATTR.action_mod,
                                                          CORESVC_COUNTRY_ATTR.action_del))
         when matched then
            update set process_id     = SVC_COUNTRY_TL_col(i).process_id,
                       chunk_id       = SVC_COUNTRY_TL_col(i).chunk_id,
                       row_seq        = SVC_COUNTRY_TL_col(i).row_seq,
                       action         = SVC_COUNTRY_TL_col(i).action,
                       process$status = SVC_COUNTRY_TL_col(i).process$status,
                       country_desc   = sq.country_desc
         when NOT matched then
            insert(process_id,
                   chunk_id,
                   row_seq,
                   action,
                   process$status,
                   lang,
                   country_id,
                   country_desc)
            values(SVC_COUNTRY_TL_col(i).process_id,
                   SVC_COUNTRY_TL_col(i).chunk_id,
                   SVC_COUNTRY_TL_col(i).row_seq,
                   SVC_COUNTRY_TL_col(i).action,
                   SVC_COUNTRY_TL_col(i).process$status,
                   sq.lang,
                   sq.country_id,
                   sq.country_desc);

   EXCEPTION
      when DML_ERRORS then
         for i in 1..SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            CTRY_TL_sheet,
                            SVC_COUNTRY_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;

      when OTHERS then
         if C_MANDATORY_IND%ISOPEN then
            close C_MANDATORY_IND;
         end if;
         rollback;

   END;

END PROCESS_S9T_CTRY_TL;

--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'CORESVC_COUNTRY_ATTR.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN

   commit;
   S9T_PKG.ODS2OBJ(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
         return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.sheet_trans(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_COUNTRY_ATTR(I_file_id,
                               I_process_id);
      PROCESS_S9T_CTRY_TL(I_file_id,
                          I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   FORALL i in 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab (i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status  = L_process_status,
          file_id = I_file_id
    where process_id = I_process_id;
   commit;

   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                            NULL,
                                            NULL,
                                            NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status  = 'PE',
             file_id = i_file_id
       where process_id = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status  = 'PE',
             file_id = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_S9T;
--------------------------------------------------------------------------------

FUNCTION EXEC_COUNTRY_ATTR_POST_INS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_country_attrib_temp_rec IN     COUNTRY_ATTRIB%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_DLVY_SLOT.EXEC_DELIVERY_SLOT_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COUNTRY_ATTRIB';
   L_exists         BOOLEAN;

BEGIN

   insert into COUNTRY_ATTRIB
      values I_country_attrib_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;

END EXEC_COUNTRY_ATTR_POST_INS;
----------------------------------------------------------------------------------------------

FUNCTION EXEC_COUNTRY_ATTR_INS(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_country_attrib_temp_rec IN OUT COUNTRY_ATTRIB%ROWTYPE,
                               I_rec                     IN     C_SVC_COUNTRY_ATTR%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_COUNTRY_ATTR.EXEC_COUNTRY_ATTR_INS';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COUNTRY';
BEGIN

   insert into COUNTRY (COUNTRY_DESC,COUNTRY_ID)
        values (I_rec.COUNTRY_DESC,I_rec.COUNTRY_ID);

   if EXEC_COUNTRY_ATTR_POST_INS(O_error_message,
                                 I_country_attrib_temp_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
   end if;
   return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
      return FALSE;

END EXEC_COUNTRY_ATTR_INS;
--------------------------------------------------------------------------------

FUNCTION EXEC_COUNTRY_ATTR_UPD(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_country_attrib_temp_rec IN     COUNTRY_ATTRIB%ROWTYPE,
                               I_rec                     IN     C_SVC_COUNTRY_ATTR%ROWTYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                      := 'CORESVC_COUNTRY_ATTR.EXEC_COUNTRY_ATTR_UPD';
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_ATTR';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_COUNTRY_UPD is
      select 'x'
        from country
       where country_id = I_rec.country_id
         for update nowait;

   cursor C_LOCK_COUNTRY_ATTR_UPD is
      select 'x'
        from country_attrib
       where country_id = I_rec.country_id
         for update nowait;

BEGIN
   L_table := 'COUNTRY';
   open  C_LOCK_COUNTRY_UPD;
   close C_LOCK_COUNTRY_UPD;
   update country
      set country_desc = I_rec.country_desc
    where country_id = I_rec.country_id;

   L_table := 'COUNTRY_ATTRIB';
   open  C_LOCK_COUNTRY_ATTR_UPD;
   close C_LOCK_COUNTRY_ATTR_UPD;
   update country_attrib
      set row = I_country_attrib_temp_rec
    where country_id = I_rec.country_id;

   return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                L_table,
                                                NULL,
                                                I_rec.country_id);

      return FALSE;

      when OTHERS then

      if C_LOCK_COUNTRY_UPD%ISOPEN then
         close C_LOCK_COUNTRY_UPD;
      end if;

      if C_LOCK_COUNTRY_ATTR_UPD%ISOPEN then
         close C_LOCK_COUNTRY_ATTR_UPD;
      end if;

         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
      return FALSE;
END EXEC_COUNTRY_ATTR_UPD;
--------------------------------------------------------------------------------

FUNCTION EXEC_COUNTRY_ATTR_DEL( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_rec           IN     C_SVC_COUNTRY_ATTR%ROWTYPE)

RETURN BOOLEAN IS
   L_program  VARCHAR2(64)                      := 'CORESVC_COUNTRY_ATTR.EXEC_COUNTRY_ATTR_DEL';
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_ATTR';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_CTRY_TL_DEL is
      select 'x'
        from country_tl
       where country_id = I_rec.country_id
         for update nowait;

   cursor C_LOCK_COUNTRY_DEL is
      select 'x'
        from country
       where country_id = I_rec.country_id
         for update nowait;

BEGIN
   L_table := 'COUNTRY_TL';
   open C_LOCK_CTRY_TL_DEL;
   close C_LOCK_CTRY_TL_DEL;
   delete from country_tl
         where country_id = I_rec.country_id;

   L_table := 'COUNTRY_ATTRIB';
   if COUNTRY_VALIDATE_SQL.DEL_COUNTRY_ATTRIB(O_error_message,
                                              I_rec.country_id) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'COUNTRY_ID',
                  O_error_message);
   end if;

   L_table := 'COUNTRY';
   open  C_LOCK_COUNTRY_DEL;
   close C_LOCK_COUNTRY_DEL;
   delete from COUNTRY
    where country_id = I_rec.country_id;

   return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
         O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                L_table,
                                                NULL,
                                                I_rec.country_id);

         return FALSE;
      when OTHERS then
         if C_LOCK_CTRY_TL_DEL%ISOPEN then
            close C_LOCK_CTRY_TL_DEL;
         end if;
         if C_LOCK_COUNTRY_DEL%ISOPEN then
            close C_LOCK_COUNTRY_DEL;
         end if;

         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
END EXEC_COUNTRY_ATTR_DEL;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_ENV(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_error         IN OUT BOOLEAN,
                      O_rec           IN OUT C_SVC_COUNTRY_ATTR%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_COUNTRY_ATTR.VALIDATE_ENV';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_ATTR';
   L_exists           BOOLEAN;
   L_dlvy_ctry_exists BOOLEAN;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;


   if LP_system_options_row.default_tax_type = 'GTAX' then
      if ITEM_COUNTRY_SQL.CHECK_COUNTRY_EXISTS(O_error_message,
                                               L_exists,
                                               O_rec.country_id) = FALSE then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'COUNTRY_ID',
                     O_error_message);
         O_error := TRUE;
      end if;

      if L_exists = TRUE then
         if ITEM_COST_SQL.CHECK_DLVY_CTRY_EXISTS (O_error_message,
                                                  L_dlvy_ctry_exists,
                                                  O_rec.country_id) = FALSE then
            WRITE_ERROR(O_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        O_rec.chunk_id,
                        L_table,
                        O_rec.row_seq,
                        'COUNTRY_ID',
                        O_error_message);
            O_error := TRUE;
         end if;
      end if;

      if L_dlvy_ctry_exists = TRUE then
         if O_rec.action = action_mod then
            if O_rec.default_loc <> O_rec.old_default_loc then
               WRITE_ERROR(O_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           O_rec.chunk_id,
                           L_table,
                           O_rec.row_seq,
                           'DEFAULT_LOC',
                           'DEFAULT_LOC_NO_EDIT');
               O_error := TRUE;
            end if;
            if O_rec.item_cost_tax_incl_ind <> O_rec.old_item_cost_tax_incl then
               WRITE_ERROR(O_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           O_rec.chunk_id,
                           L_table,
                           O_rec.row_seq,
                           'ITEM_COST_TAX_INCL_IND',
                           'ITM_CST_TAX_NO_EDIT');
               O_error := TRUE;
            end if;
            if O_rec.default_po_cost <> O_rec.old_default_po_cost then
               WRITE_ERROR(O_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           O_rec.chunk_id,
                           L_table,
                           O_rec.row_seq,
                           'DEFAULT_PO_COST',
                           'DEFAULT_PO_COST_NO_EDIT');
               O_error := TRUE;
            end if;
         end if;
      end if;

   elsif LP_system_options_row.default_tax_type = 'SALES' then
      O_rec.default_po_cost        := 'BC';
      O_rec.default_deal_cost      := 'BC';
      O_rec.default_cost_comp_cost := 'BC';

      O_rec.item_cost_tax_incl_ind := 'N';
      O_rec.default_loc            := NULL;

   elsif LP_system_options_row.default_tax_type = 'SVAT' then
      if ITEM_COUNTRY_SQL.CHECK_COUNTRY_EXISTS(O_error_message,
                                               L_exists,
                                               O_rec.country_id) = FALSE then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'COUNTRY_ID',
                     O_error_message);
         O_error := TRUE;
      end if;

      if L_exists = TRUE then
         if O_rec.action = action_mod then
            if O_rec.default_loc != O_rec.old_default_loc then
               WRITE_ERROR(O_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           O_rec.chunk_id,
                           L_table,
                           O_rec.row_seq,
                           'DEFAULT_LOC',
                           'DEFAULT_LOC_NO_EDIT');
               O_error := TRUE;
            end if;
         end if;

      end if;

      O_rec.default_po_cost        := 'BC';
      O_rec.default_deal_cost      := 'BC';
      O_rec.default_cost_comp_cost := 'BC';
      O_rec.item_cost_tax_incl_ind := 'N';
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END;
--------------------------------------------------------------------------------

FUNCTION PROCESS_VAL_COUNTRY_ID( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error         IN OUT BOOLEAN,
                                 O_rec           IN OUT C_SVC_COUNTRY_ATTR%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_COUNTRY_ATTR.PROCESS_VAL_COUNTRY_ID';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_ATTR';
   L_exists           BOOLEAN;
   L_country_id       COUNTRY.COUNTRY_ID%TYPE;
   L_loc_type         VARCHAR2(1);
   L_module           ADDR.MODULE%TYPE;
   L_add_1            ADDR.ADD_1%TYPE;
   L_add_2            ADDR.ADD_2%TYPE;
   L_add_3            ADDR.ADD_3%TYPE;
   L_city             ADDR.CITY%TYPE;
   L_state            ADDR.STATE%TYPE;
   L_post             ADDR.POST%TYPE;
   L_key_value_2      ADDR.KEY_VALUE_2%TYPE;
   L_default_loc_desc WH.WH_NAME%TYPE;
BEGIN

   if VALIDATE_ENV(O_error_message,
                   O_error,
                   O_rec) = FALSE then
      WRITE_ERROR(O_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  O_rec.chunk_id,
                  L_table,
                  O_rec.row_seq,
                  'COUNTRY_ID',
                  O_error_message);
         O_error :=TRUE;
   end if;
   if O_rec.action = action_del then

      if O_rec.COUNTRY_ID = '99' then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'COUNTRY_ID',
                     'CANNOT_DEL_99');
         O_error :=TRUE;
      end if;

      if COUNTRY_VALIDATE_SQL.CONSTRAINTS_EXIST(O_rec.country_id,
                                                O_error_message,
                                                L_exists) = FALSE then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'COUNTRY_ID',
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_exists then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'COUNTRY_ID',
                     'COUNTRY_FRGN_CONSTRAINT');
         O_error := TRUE;
      end if;
   end if;

   if O_rec.default_loc is NOT NULL then
      if LOCATION_ATTRIB_SQL.GET_TYPE(O_error_message,
                                      L_loc_type,
                                      O_rec.default_loc) = FALSE then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'DEFAULT_LOC',
                     O_error_message);
         O_error := TRUE;
      end if;

      if L_loc_type is not NULL then
        if L_loc_type = 'W' then
            L_module := 'WH';
         else
            L_module := 'ST';
         end if;

         if ADDRESS_SQL.GET_PRIM_ADDR(O_error_message,
                                      L_add_1,
                                      L_add_2,
                                      L_add_3,
                                      L_city,
                                      L_state,
                                      L_country_id,
                                      L_post,
                                      L_module,
                                      O_rec.default_loc,
                                      L_key_value_2) = FALSE then
            WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'DEFAULT_LOC',
                     O_error_message);
            O_error := TRUE;
         end if;

         if L_country_id <> O_rec.country_id then
            WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'DEFAULT_LOC',
                     'NO_COUNTRY_LOC');
            O_error := TRUE;
         end if;

         if L_loc_type = 'W' then
            if WH_ATTRIB_SQL.GET_NAME(O_error_message,
                                      O_rec.default_loc,
                                      L_default_loc_desc) = FALSE then
               WRITE_ERROR(O_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           O_rec.chunk_id,
                           L_table,
                           O_rec.row_seq,
                           'DEFAULT_LOC',
                           O_error_message);
               O_error := TRUE;
            end if;
         else
            if STORE_ATTRIB_SQL.GET_NAME(O_error_message,
                                         O_rec.default_loc,
                                         L_default_loc_desc) = FALSE then
               WRITE_ERROR(O_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           O_rec.chunk_id,
                           L_table,
                           O_rec.row_seq,
                           'DEFAULT_LOC',
                           O_error_message);
               O_error := TRUE;
            end if;
         end if;

         O_rec.default_loc_type := L_loc_type;
      end if;
   else
      O_rec.default_loc_type := NULL;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
  return FALSE;

END PROCESS_VAL_COUNTRY_ID;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   SVC_COUNTRY_ATTR.PROCESS_ID%TYPE) IS

BEGIN
   delete from svc_country_tl
    where process_id = I_process_id;

   delete from svc_country_attr
    where process_id = I_process_id;

END CLEAR_STAGING_DATA;
--------------------------------------------------------------------------------
FUNCTION PROCESS_COUNTRY_ATTR(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id    IN     SVC_COUNTRY_ATTR.PROCESS_ID%TYPE,
                              I_chunk_id      IN     SVC_COUNTRY_ATTR.CHUNK_ID%TYPE )

RETURN BOOLEAN IS
   L_program                 VARCHAR2(64)                       := 'CORESVC_COUNTRY_ATTR.PROCESS_COUNTRY_ATTR';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  :='SVC_COUNTRY_ATTR';
   L_error                   BOOLEAN;
   L_process_error           BOOLEAN;
   L_country_attrib_temp_rec COUNTRY_ATTRIB%ROWTYPE;

BEGIN
   FOR rec IN C_SVC_COUNTRY_ATTR(I_process_id,I_chunk_id)
   LOOP
      L_error       := FALSE;

      if rec.action is NULL or
         rec.action NOT IN (action_new,action_mod,action_del) then

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.action = action_new and
         rec.coat_cnt_fk_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COUNTRY_ID',
                     'NO_DUPLICATE_COUNTRY_ID');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_del) and
         rec.pk_country_attrib_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COUNTRY_ID',
                     'COUNTRY_ID_MISSING');
         L_error := TRUE;
      end if;

      if PROCESS_VAL_COUNTRY_ID(O_error_message,
                                L_error,
                                rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT( rec.default_loc_type IN ('S','W','E') ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEFAULT_LOC_TYPE',
                     'CA_DEFAULT_LOC_TYPE');
         L_error := TRUE;
      end if;

      if NOT( rec.default_cost_comp_cost IN ( 'NIC','BC' ) ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEFAULT_COST_COMP_COST',
                     'INV_COST_VAL');
         L_error := TRUE;
      end if;

      if NOT( rec.default_deal_cost IN ( 'NIC','BC' ) ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEFAULT_DEAL_COST',
                     'INV_COST_VAL');
         L_error := TRUE;
      end if;

      if NOT( rec.default_po_cost IN ( 'NIC','BC' ) ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEFAULT_PO_COST',
                     'INV_COST_VAL');
         L_error := TRUE;
      end if;

      if NOT( rec.item_cost_tax_incl_ind IN ( 'Y','N' ) ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ITEM_COST_TAX_INCL_IND',
                     'INV_Y_N_IND');
         L_error := TRUE;
      end if;

      if NOT( rec.localized_ind IN  ( 'Y','N' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LOCALIZED_IND',
                     'INV_Y_N_IND');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_new) then

         if NOT(  rec.localized_ind  is NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'LOCALIZED_IND',
                        'LOCALIZED_IND_IS_NULL');
            L_error := TRUE;
         end if;
         if NOT(  rec.item_cost_tax_incl_ind  is NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ITEM_COST_TAX_INCL_IND',
                        'ITM_CST_TAX_NULL');
            L_error := TRUE;
         end if;
         if NOT(  rec.country_desc  is NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COUNTRY_DESC',
                        'NO_COUNTRY_DESC');
            L_error := TRUE;
         end if;

      end if;

      if NOT L_error then
         L_country_attrib_temp_rec.DEFAULT_LOC_TYPE         := rec.DEFAULT_LOC_TYPE;
         L_country_attrib_temp_rec.DEFAULT_LOC              := rec.DEFAULT_LOC;
         L_country_attrib_temp_rec.DEFAULT_COST_COMP_COST   := rec.DEFAULT_COST_COMP_COST;
         L_country_attrib_temp_rec.DEFAULT_DEAL_COST        := rec.DEFAULT_DEAL_COST;
         L_country_attrib_temp_rec.DEFAULT_PO_COST          := rec.DEFAULT_PO_COST;
         L_country_attrib_temp_rec.ITEM_COST_TAX_INCL_IND   := rec.ITEM_COST_TAX_INCL_IND;
         L_country_attrib_temp_rec.LOCALIZED_IND            := rec.LOCALIZED_IND;
         L_country_attrib_temp_rec.COUNTRY_ID               := rec.COUNTRY_ID;
         L_country_attrib_temp_rec.CREATE_ID                := GET_USER;
         L_country_attrib_temp_rec.CREATE_DATETIME          := SYSDATE;

         L_process_error := FALSE;
         if rec.action = action_new then
            if EXEC_COUNTRY_ATTR_INS(O_error_message,
                                     L_country_attrib_temp_rec,
                                     rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_COUNTRY_ATTR_UPD(O_error_message,
                                     L_country_attrib_temp_rec,
                                     rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_COUNTRY_ATTR_DEL(O_error_message,
                                     rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

      end if;

   END LOOP;
   return TRUE;
   EXCEPTION
      when OTHERS then
         if C_SVC_COUNTRY_ATTR%ISOPEN then
            close C_SVC_COUNTRY_ATTR;
         end if;

         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;
END PROCESS_COUNTRY_ATTR;
--------------------------------------------------------------------------------
FUNCTION EXEC_CTRY_TL_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_ctry_tl_ins_tab   IN       CTRY_TL_TAB)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_COUNTRY_ATTR.EXEC_CTRY_TL_INS';

BEGIN
   if I_ctry_tl_ins_tab is NOT NULL and I_ctry_tl_ins_tab.COUNT > 0 then
      FORALL i IN 1..I_ctry_tl_ins_tab.COUNT()
         insert into country_tl
              values I_ctry_tl_ins_tab(i);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CTRY_TL_INS;
---------------------------------------------------------------------------------
FUNCTION EXEC_CTRY_TL_UPD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_ctry_tl_upd_tab   IN       CTRY_TL_TAB,
                          I_ctry_tl_upd_rst   IN       ROW_SEQ_TAB,
                          I_process_id        IN       SVC_COUNTRY_TL.PROCESS_ID%TYPE,
                          I_chunk_id          IN       SVC_COUNTRY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_COUNTRY_ATTR.EXEC_CTRY_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COUNTRY_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CTRY_TL_UPD(I_country_id   COUNTRY_TL.COUNTRY_ID%TYPE,
                             I_lang         COUNTRY_TL.LANG%TYPE) is
      select 'x'
        from country_tl
       where country_id = I_country_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_ctry_tl_upd_tab is NOT NULL and I_ctry_tl_upd_tab.COUNT > 0 then
      for i in I_ctry_tl_upd_tab.FIRST..I_ctry_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_ctry_tl_upd_tab(i).lang);
            L_key_val2 := 'Country Code: '||I_ctry_tl_upd_tab(i).country_id;
            open C_LOCK_CTRY_TL_UPD(I_ctry_tl_upd_tab(i).country_id,
                                    I_ctry_tl_upd_tab(i).lang);
            close C_LOCK_CTRY_TL_UPD;
            
            update country_tl
               set country_desc = I_ctry_tl_upd_tab(i).country_desc,
                   last_update_id = I_ctry_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_ctry_tl_upd_tab(i).last_update_datetime
             where lang = I_ctry_tl_upd_tab(i).lang
               and country_id = I_ctry_tl_upd_tab(i).country_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_COUNTRY_TL',
                           I_ctry_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CTRY_TL_UPD%ISOPEN then
         close C_LOCK_CTRY_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CTRY_TL_UPD;
---------------------------------------------------------------------------------
FUNCTION EXEC_CTRY_TL_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_ctry_tl_del_tab   IN       CTRY_TL_TAB,
                          I_ctry_tl_del_rst   IN       ROW_SEQ_TAB,
                          I_process_id        IN       SVC_COUNTRY_TL.PROCESS_ID%TYPE,
                          I_chunk_id          IN       SVC_COUNTRY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_COUNTRY_ATTR.EXEC_CTRY_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COUNTRY_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   --Cursor to lock the record
   cursor C_LOCK_CTRY_TL_DEL(I_country_id   COUNTRY_TL.COUNTRY_ID%TYPE,
                             I_lang         COUNTRY_TL.LANG%TYPE) is
      select 'x'
        from country_tl
       where country_id = I_country_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_ctry_tl_del_tab is NOT NULL and I_ctry_tl_del_tab.COUNT > 0 then
      for i in I_ctry_tl_del_tab.FIRST..I_ctry_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_ctry_tl_del_tab(i).lang);
            L_key_val2 := 'Country Code: '||I_ctry_tl_del_tab(i).country_id;
            open C_LOCK_CTRY_TL_DEL(I_ctry_tl_del_tab(i).country_id,
                                    I_ctry_tl_del_tab(i).lang);
            close C_LOCK_CTRY_TL_DEL;
            
            delete country_tl
             where lang = I_ctry_tl_del_tab(i).lang
               and country_id = I_ctry_tl_del_tab(i).country_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_COUNTRY_TL',
                           I_ctry_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CTRY_TL_DEL%ISOPEN then
         close C_LOCK_CTRY_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CTRY_TL_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CTRY_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_COUNTRY_TL.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_COUNTRY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)                      := 'CORESVC_COUNTRY_ATTR.PROCESS_CTRY_TL';
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COUNTRY_TL';
   L_base_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COUNTRY';
   L_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COUNTRY_TL';
   L_error                    BOOLEAN                           := FALSE;
   L_process_error            BOOLEAN                           := FALSE;
   L_ctry_tl_temp_rec         COUNTRY_TL%ROWTYPE;
   L_ctry_tl_upd_rst          ROW_SEQ_TAB;
   L_ctry_tl_del_rst          ROW_SEQ_TAB;

   cursor C_SVC_CTRY_TL(I_process_id   NUMBER,
                        I_chunk_id     NUMBER) is
      select pk_ctry_tl.rowid       as pk_ctry_tl_rid,
             fk_ctry.rowid          as fk_ctry_rid,
             fk_lang.rowid          as fk_lang_rid,
             st.lang,
             UPPER(st.country_id)   as country_id,
             st.country_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)       as action,
             st.process$status
        from svc_country_tl st,
             country        fk_ctry,
             country_tl     pk_ctry_tl,
             lang           fk_lang
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.country_id = fk_ctry.country_id (+)
         and st.lang       = pk_ctry_tl.lang (+)
         and st.country_id = pk_ctry_tl.country_id (+)
         and st.lang       = fk_lang.lang (+);

   TYPE SVC_CTRY_TL is TABLE OF C_SVC_CTRY_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_ctry_tl_tab          SVC_CTRY_TL;
   L_ctry_tl_ins_tab          ctry_tl_tab                  := NEW ctry_tl_tab();
   L_ctry_tl_upd_tab          ctry_tl_tab                  := NEW ctry_tl_tab();
   L_ctry_tl_del_tab          ctry_tl_tab                  := NEW ctry_tl_tab();

BEGIN
   if C_SVC_CTRY_TL%ISOPEN then
      close C_SVC_CTRY_TL;
   end if;

   open C_SVC_CTRY_TL(I_process_id,
                      I_chunk_id);
   LOOP
      fetch C_SVC_CTRY_TL bulk collect into L_svc_ctry_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_ctry_tl_tab.COUNT > 0 then
         FOR i in L_svc_ctry_tl_tab.FIRST..L_svc_ctry_tl_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_ctry_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ctry_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if action is valid
            if L_svc_ctry_tl_tab(i).action is NULL
               or L_svc_ctry_tl_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ctry_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_ctry_tl_tab(i).action = action_new
               and L_svc_ctry_tl_tab(i).pk_ctry_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ctry_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_ctry_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_ctry_tl_tab(i).lang is NOT NULL
               and L_svc_ctry_tl_tab(i).country_id is NOT NULL
               and L_svc_ctry_tl_tab(i).pk_ctry_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_ctry_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            -- check for FK
            if L_svc_ctry_tl_tab(i).action = action_new
               and L_svc_ctry_tl_tab(i).country_id is NOT NULL
               and L_svc_ctry_tl_tab(i).fk_ctry_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_ctry_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_ctry_tl_tab(i).action = action_new
               and L_svc_ctry_tl_tab(i).lang is NOT NULL
               and L_svc_ctry_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ctry_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_ctry_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ctry_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if L_svc_ctry_tl_tab(i).country_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ctry_tl_tab(i).row_seq,
                           'COUNTRY_ID',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_ctry_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_ctry_tl_tab(i).country_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_ctry_tl_tab(i).row_seq,
                              'COUNTRY_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if NOT L_error then
               L_ctry_tl_temp_rec.lang := L_svc_ctry_tl_tab(i).lang;
               L_ctry_tl_temp_rec.country_id := L_svc_ctry_tl_tab(i).country_id;
               L_ctry_tl_temp_rec.country_desc := L_svc_ctry_tl_tab(i).country_desc;
               L_ctry_tl_temp_rec.create_datetime := SYSDATE;
               L_ctry_tl_temp_rec.create_id := GET_USER;
               L_ctry_tl_temp_rec.last_update_datetime := SYSDATE;
               L_ctry_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_ctry_tl_tab(i).action = action_new then
                  L_ctry_tl_ins_tab.extend;
                  L_ctry_tl_ins_tab(L_ctry_tl_ins_tab.count()) := L_ctry_tl_temp_rec;
               end if;

               if L_svc_ctry_tl_tab(i).action = action_mod then
                  L_ctry_tl_upd_tab.extend;
                  L_ctry_tl_upd_tab(L_ctry_tl_upd_tab.count()) := L_ctry_tl_temp_rec;
                  L_ctry_tl_upd_rst(L_ctry_tl_upd_tab.count()) := L_svc_ctry_tl_tab(i).row_seq;
               end if;

               if L_svc_ctry_tl_tab(i).action = action_del then
                  L_ctry_tl_del_tab.extend;
                  L_ctry_tl_del_tab(L_ctry_tl_del_tab.count()) := L_ctry_tl_temp_rec;
                  L_ctry_tl_del_rst(L_ctry_tl_del_tab.count()) := L_svc_ctry_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_CTRY_TL%NOTFOUND;
   END LOOP;
   close C_SVC_CTRY_TL;

   if EXEC_CTRY_TL_INS(O_error_message,
                       L_ctry_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_CTRY_TL_UPD(O_error_message,
                       L_ctry_tl_upd_tab,
                       L_ctry_tl_upd_rst,
                       I_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_CTRY_TL_DEL(O_error_message,
                       L_ctry_tl_del_tab,
                       L_ctry_tl_del_rst,
                       I_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_CTRY_TL;
--------------------------------------------------------------------------------

FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)                    := 'CORESVC_COUNTRY_ATTR.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_COUNTRY_ATTR(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;
   if PROCESS_CTRY_TL(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();
   open  C_GET_ERR_COUNT;
   fetch C_GET_ERR_COUNT into L_err_count;
   close C_GET_ERR_COUNT;

   open  C_GET_WARN_COUNT;
   fetch C_GET_WARN_COUNT into L_warn_count;
   close C_GET_WARN_COUNT;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------

END CORESVC_COUNTRY_ATTR;
/
