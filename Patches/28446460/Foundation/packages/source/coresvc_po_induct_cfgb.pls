create or replace PACKAGE BODY CORESVC_PO_INDUCT_CFG AS
   cursor C_SVC_SVC_ORDER_PM_CFG(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             st.rowid AS st_rid,
             st.apply_deals,
             st.apply_scaling,
             st.skip_open_shipment,
             st.cancel_alloc,
             st.recalc_replenishment,
             st.otb_override, 
             st.max_order_expiry_days,
             st.max_order_no_qty,
             st.wait_btwn_threads,
             st.max_threads,
             st.max_chunk_size, 
             st.apply_brackets,
             st.override_manl_cost_src,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from SVC_SVC_ORDER_PARAMETER_CONFIG st,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
;
   cursor C_SVC_PO_INDUCT_CONFIG(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             st.rowid AS st_rid,
             st.max_po_for_dnld,
             st.max_po_for_sync_dnld,
             st.max_file_size_for_upld,
             st.max_file_size_for_sync_upld,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_po_induct_config st,
             dual
       where st.process_id = I_process_id
         and st.chunk_id     = I_chunk_id
;

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
---------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
----------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   SVC_ORDER_PM_CFG_cols s9t_pkg.names_map_typ;
   PO_INDUCT_CONFIG_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                                    :=s9t_pkg.get_sheet_names(I_file_id);
   SVC_ORDER_PM_CFG_cols                       :=s9t_pkg.get_col_names(I_file_id,SVC_ORD_PM_CFG_sheet);
   SVC_ORD_PM_CFG_$Action                      := SVC_ORDER_PM_CFG_cols('ACTION');
   SVC_ORD_PM_CFG_$APPLY_DEALS                 := SVC_ORDER_PM_CFG_cols('APPLY_DEALS');
   SVC_ORD_PM_CFG_$APPLY_SCALING               := SVC_ORDER_PM_CFG_cols('APPLY_SCALING');
   SVC_ORD_PM_CFG_$SKIP_OPEN_SPMT              := SVC_ORDER_PM_CFG_cols('SKIP_OPEN_SHIPMENT');
   SVC_ORD_PM_CFG_$CANCEL_ALLOC                := SVC_ORDER_PM_CFG_cols('CANCEL_ALLOC');
   SVC_ORD_PM_CFG_$RECALC_RPL                  := SVC_ORDER_PM_CFG_cols('RECALC_REPLENISHMENT');
   SVC_ORD_PM_CFG_$OTB_OVERRIDE                := SVC_ORDER_PM_CFG_cols('OTB_OVERRIDE');
   SVC_ORD_PM_CFG_$MAX_ORD_EXP_DY              := SVC_ORDER_PM_CFG_cols('MAX_ORDER_EXPIRY_DAYS');
   SVC_ORD_PM_CFG_$MAX_ORD_NO_QTY              := SVC_ORDER_PM_CFG_cols('MAX_ORDER_NO_QTY');
   SVC_ORD_PM_CFG_$WAIT_BTWN_THRD              := SVC_ORDER_PM_CFG_cols('WAIT_BTWN_THREADS');
   SVC_ORD_PM_CFG_$MAX_THREADS                 := SVC_ORDER_PM_CFG_cols('MAX_THREADS');
   SVC_ORD_PM_CFG_$MAX_CHUNK_SIZE              := SVC_ORDER_PM_CFG_cols('MAX_CHUNK_SIZE');
   SVC_ORD_PM_CFG_$APPLY_BRACKETS              := SVC_ORDER_PM_CFG_cols('APPLY_BRACKETS');
   SVC_ORD_PM_CFG_$OVRD_ML_CT_SRC              := SVC_ORDER_PM_CFG_cols('OVERRIDE_MANL_COST_SRC');

   PO_INDUCT_CONFIG_cols                       :=s9t_pkg.get_col_names(I_file_id,PO_INDUCT_CONFIG_sheet);
   PO_INDUCT_CONFIG$Action                     := PO_INDUCT_CONFIG_cols('ACTION');
   PO_INDUCT_CONFIG$MXPOFR_DNLD                := PO_INDUCT_CONFIG_cols('MAX_PO_FOR_DNLD');
   PO_INDUCT_CONFIG$MXPOFRSN_DNLD              := PO_INDUCT_CONFIG_cols('MAX_PO_FOR_SYNC_DNLD');
   PO_INDUCT_CONFIG$MXFLSZFR_UPLD              := PO_INDUCT_CONFIG_cols('MAX_FILE_SIZE_FOR_UPLD');
   PO_INDUCT_CONFIG$MXFLSZFRSC_UL              := PO_INDUCT_CONFIG_cols('MAX_FILE_SIZE_FOR_SYNC_UPLD');
END POPULATE_NAMES;
----------------------------------------------------------
PROCEDURE POPULATE_SVC_ORDER_PM_CFG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SVC_ORD_PM_CFG_sheet )
   select s9t_row(s9t_cells(CORESVC_PO_INDUCT_CFG.action_mod ,max_chunk_size,max_threads,wait_btwn_threads,max_order_no_qty,max_order_expiry_days,apply_scaling,
                           apply_deals,apply_brackets,otb_override,recalc_replenishment,cancel_alloc,skip_open_shipment,override_manl_cost_src
                           ))
     from svc_order_parameter_config ;
END POPULATE_SVC_ORDER_PM_CFG;
-------------------------------------------------------------------------------
PROCEDURE POPULATE_PO_INDUCT_CONFIG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = PO_INDUCT_CONFIG_sheet )
   select s9t_row(s9t_cells(CORESVC_PO_INDUCT_CFG.action_mod ,max_po_for_dnld,max_po_for_sync_dnld,max_file_size_for_upld,max_file_size_for_sync_upld
                           ))
     from po_induct_config ;
END POPULATE_PO_INDUCT_CONFIG;
------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
   cursor C_USER_LANG is
      select lang
        from user_attrib
       where user_id = get_user;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(SVC_ORD_PM_CFG_sheet);
   L_file.sheets(l_file.get_sheet_index(SVC_ORD_PM_CFG_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                                         'MAX_CHUNK_SIZE',
																										 'MAX_THREADS',
																										 'WAIT_BTWN_THREADS',
																										 'MAX_ORDER_NO_QTY',
																										 'MAX_ORDER_EXPIRY_DAYS',
																										 'APPLY_SCALING',
                                                                                                         'APPLY_DEALS',
																										 'APPLY_BRACKETS',
																										 'OTB_OVERRIDE',
																										 'RECALC_REPLENISHMENT',
																										 'CANCEL_ALLOC',
																										 'SKIP_OPEN_SHIPMENT',
																										 'OVERRIDE_MANL_COST_SRC');
                                                                                            
   L_file.add_sheet(PO_INDUCT_CONFIG_sheet);
   L_file.sheets(l_file.get_sheet_index(PO_INDUCT_CONFIG_sheet)).column_headers := s9t_cells( 'ACTION',
																							   'MAX_PO_FOR_DNLD',
																							   'MAX_PO_FOR_SYNC_DNLD',
																							   'MAX_FILE_SIZE_FOR_UPLD',
																						       'MAX_FILE_SIZE_FOR_SYNC_UPLD'
                                                                                            
);
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_PO_INDUCT_CFG.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;   

   if I_template_only_ind = 'N' then
      POPULATE_SVC_ORDER_PM_CFG(O_file_id);
      POPULATE_PO_INDUCT_CONFIG(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SVC_ORDER_PM_CFG( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_SVC_ORDER_PARAMETER_CONFIG.process_id%TYPE) IS
   TYPE svc_SVC_ORDER_PM_CFG_col_typ IS TABLE OF SVC_SVC_ORDER_PARAMETER_CONFIG%ROWTYPE;
   L_temp_rec SVC_SVC_ORDER_PARAMETER_CONFIG%ROWTYPE;
   svc_SVC_ORDER_PM_CFG_col svc_SVC_ORDER_PM_CFG_col_typ :=NEW svc_SVC_ORDER_PM_CFG_col_typ();
   L_process_id SVC_SVC_ORDER_PARAMETER_CONFIG.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_SVC_ORDER_PARAMETER_CONFIG%ROWTYPE;
      L_pk_columns    VARCHAR2(255)  := 'SVC Order Parameter Configuration';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             APPLY_DEALS_mi,
             APPLY_SCALING_mi,
             SKIP_OPEN_SHIPMENT_mi,
             CANCEL_ALLOC_mi,
             RECALC_REPLENISHMENT_mi,
             OTB_OVERRIDE_mi,
             MAX_ORDER_EXPIRY_DAYS_mi,
             MAX_ORDER_NO_QTY_mi,
             WAIT_BTWN_THREADS_mi,
             MAX_THREADS_mi,
             MAX_CHUNK_SIZE_mi,
             APPLY_BRACKETS_mi,
             OVERRIDE_MANL_COST_SRC_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'SVC_ORDER_PM_CFG'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'APPLY_DEALS' AS APPLY_DEALS,
                                         'APPLY_SCALING' AS APPLY_SCALING,
                                         'SKIP_OPEN_SHIPMENT' AS SKIP_OPEN_SHIPMENT,
                                         'CANCEL_ALLOC' AS CANCEL_ALLOC,
                                         'RECALC_REPLENISHMENT' AS RECALC_REPLENISHMENT,
                                         'OTB_OVERRIDE' AS OTB_OVERRIDE,
                                         'MAX_ORDER_EXPIRY_DAYS' AS MAX_ORDER_EXPIRY_DAYS,
                                         'MAX_ORDER_NO_QTY' AS MAX_ORDER_NO_QTY,
                                         'WAIT_BTWN_THREADS' AS WAIT_BTWN_THREADS,
                                         'MAX_THREADS' AS MAX_THREADS,
                                         'MAX_CHUNK_SIZE' AS MAX_CHUNK_SIZE,
                                         'APPLY_BRACKETS' AS APPLY_BRACKETS,
                                         'OVERRIDE_MANL_COST_SRC' AS OVERRIDE_MANL_COST_SRC,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       APPLY_DEALS_dv,
                       APPLY_SCALING_dv,
                       SKIP_OPEN_SHIPMENT_dv,
                       CANCEL_ALLOC_dv,
                       RECALC_REPLENISHMENT_dv,
                       OTB_OVERRIDE_dv,
                       MAX_ORDER_EXPIRY_DAYS_dv,
                       MAX_ORDER_NO_QTY_dv,
                       WAIT_BTWN_THREADS_dv,
                       MAX_THREADS_dv,
                       MAX_CHUNK_SIZE_dv,
                       APPLY_BRACKETS_dv,
                       OVERRIDE_MANL_COST_SRC_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'SVC_ORDER_PM_CFG'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'APPLY_DEALS' AS APPLY_DEALS,
                                                      'APPLY_SCALING' AS APPLY_SCALING,
                                                      'SKIP_OPEN_SHIPMENT' AS SKIP_OPEN_SHIPMENT,
                                                      'CANCEL_ALLOC' AS CANCEL_ALLOC,
                                                      'RECALC_REPLENISHMENT' AS RECALC_REPLENISHMENT,
                                                      'OTB_OVERRIDE' AS OTB_OVERRIDE,
                                                      'MAX_ORDER_EXPIRY_DAYS' AS MAX_ORDER_EXPIRY_DAYS,
                                                      'MAX_ORDER_NO_QTY' AS MAX_ORDER_NO_QTY,
                                                      'WAIT_BTWN_THREADS' AS WAIT_BTWN_THREADS,
                                                      'MAX_THREADS' AS MAX_THREADS,
                                                      'MAX_CHUNK_SIZE' AS MAX_CHUNK_SIZE,
                                                      'APPLY_BRACKETS' AS APPLY_BRACKETS,
                                                      'OVERRIDE_MANL_COST_SRC' AS OVERRIDE_MANL_COST_SRC,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.APPLY_DEALS := rec.APPLY_DEALS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'APPLY_DEALS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.APPLY_SCALING := rec.APPLY_SCALING_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'APPLY_SCALING ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.SKIP_OPEN_SHIPMENT := rec.SKIP_OPEN_SHIPMENT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'SKIP_OPEN_SHIPMENT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CANCEL_ALLOC := rec.CANCEL_ALLOC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'CANCEL_ALLOC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.RECALC_REPLENISHMENT := rec.RECALC_REPLENISHMENT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'RECALC_REPLENISHMENT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.OTB_OVERRIDE := rec.OTB_OVERRIDE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'OTB_OVERRIDE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.MAX_ORDER_EXPIRY_DAYS := rec.MAX_ORDER_EXPIRY_DAYS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'MAX_ORDER_EXPIRY_DAYS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.MAX_ORDER_NO_QTY := rec.MAX_ORDER_NO_QTY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'MAX_ORDER_NO_QTY ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.WAIT_BTWN_THREADS := rec.WAIT_BTWN_THREADS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'WAIT_BTWN_THREADS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.MAX_THREADS := rec.MAX_THREADS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'MAX_THREADS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.MAX_CHUNK_SIZE := rec.MAX_CHUNK_SIZE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'MAX_CHUNK_SIZE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.APPLY_BRACKETS := rec.APPLY_BRACKETS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'APPLY_BRACKETS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.OVERRIDE_MANL_COST_SRC := rec.OVERRIDE_MANL_COST_SRC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SVC_ORDER_PM_CFG ' ,
                            NULL,
                           'OVERRIDE_MANL_COST_SRC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(SVC_ORD_PM_CFG_$Action)      AS Action,
          r.get_cell(SVC_ORD_PM_CFG_$APPLY_DEALS)              AS APPLY_DEALS,
          r.get_cell(SVC_ORD_PM_CFG_$APPLY_SCALING)              AS APPLY_SCALING,
          r.get_cell(SVC_ORD_PM_CFG_$SKIP_OPEN_SPMT)              AS SKIP_OPEN_SHIPMENT,
          r.get_cell(SVC_ORD_PM_CFG_$CANCEL_ALLOC)              AS CANCEL_ALLOC,
          r.get_cell(SVC_ORD_PM_CFG_$RECALC_RPL)              AS RECALC_REPLENISHMENT,
          r.get_cell(SVC_ORD_PM_CFG_$OTB_OVERRIDE)              AS OTB_OVERRIDE,
          r.get_cell(SVC_ORD_PM_CFG_$MAX_ORD_EXP_DY)              AS MAX_ORDER_EXPIRY_DAYS,
          r.get_cell(SVC_ORD_PM_CFG_$MAX_ORD_NO_QTY )              AS MAX_ORDER_NO_QTY,
          r.get_cell(SVC_ORD_PM_CFG_$WAIT_BTWN_THRD)              AS WAIT_BTWN_THREADS,
          r.get_cell(SVC_ORD_PM_CFG_$MAX_THREADS)              AS MAX_THREADS,
          r.get_cell(SVC_ORD_PM_CFG_$MAX_CHUNK_SIZE)              AS MAX_CHUNK_SIZE,
          r.get_cell(SVC_ORD_PM_CFG_$APPLY_BRACKETS)              AS APPLY_BRACKETS,
          r.get_cell(SVC_ORD_PM_CFG_$OVRD_ML_CT_SRC)              AS OVERRIDE_MANL_COST_SRC,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(SVC_ORD_PM_CFG_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.APPLY_DEALS := rec.APPLY_DEALS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'APPLY_DEALS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.APPLY_SCALING := rec.APPLY_SCALING;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'APPLY_SCALING',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SKIP_OPEN_SHIPMENT := rec.SKIP_OPEN_SHIPMENT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'SKIP_OPEN_SHIPMENT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CANCEL_ALLOC := rec.CANCEL_ALLOC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'CANCEL_ALLOC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.RECALC_REPLENISHMENT := rec.RECALC_REPLENISHMENT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'RECALC_REPLENISHMENT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.OTB_OVERRIDE := rec.OTB_OVERRIDE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'OTB_OVERRIDE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_ORDER_EXPIRY_DAYS := rec.MAX_ORDER_EXPIRY_DAYS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'MAX_ORDER_EXPIRY_DAYS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_ORDER_NO_QTY := rec.MAX_ORDER_NO_QTY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'MAX_ORDER_NO_QTY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.WAIT_BTWN_THREADS := rec.WAIT_BTWN_THREADS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'WAIT_BTWN_THREADS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_THREADS := rec.MAX_THREADS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'MAX_THREADS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_CHUNK_SIZE := rec.MAX_CHUNK_SIZE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'MAX_CHUNK_SIZE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.APPLY_BRACKETS := rec.APPLY_BRACKETS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'APPLY_BRACKETS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.OVERRIDE_MANL_COST_SRC := rec.OVERRIDE_MANL_COST_SRC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                            rec.row_seq,
                            'OVERRIDE_MANL_COST_SRC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_PO_INDUCT_CFG.action_new then
         L_temp_rec.APPLY_DEALS := NVL( L_temp_rec.APPLY_DEALS,L_default_rec.APPLY_DEALS);
         L_temp_rec.APPLY_SCALING := NVL( L_temp_rec.APPLY_SCALING,L_default_rec.APPLY_SCALING);
         L_temp_rec.SKIP_OPEN_SHIPMENT := NVL( L_temp_rec.SKIP_OPEN_SHIPMENT,L_default_rec.SKIP_OPEN_SHIPMENT);
         L_temp_rec.CANCEL_ALLOC := NVL( L_temp_rec.CANCEL_ALLOC,L_default_rec.CANCEL_ALLOC);
         L_temp_rec.RECALC_REPLENISHMENT := NVL( L_temp_rec.RECALC_REPLENISHMENT,L_default_rec.RECALC_REPLENISHMENT);
         L_temp_rec.OTB_OVERRIDE := NVL( L_temp_rec.OTB_OVERRIDE,L_default_rec.OTB_OVERRIDE);
         L_temp_rec.MAX_ORDER_EXPIRY_DAYS := NVL( L_temp_rec.MAX_ORDER_EXPIRY_DAYS,L_default_rec.MAX_ORDER_EXPIRY_DAYS);
         L_temp_rec.MAX_ORDER_NO_QTY := NVL( L_temp_rec.MAX_ORDER_NO_QTY,L_default_rec.MAX_ORDER_NO_QTY);
         L_temp_rec.WAIT_BTWN_THREADS := NVL( L_temp_rec.WAIT_BTWN_THREADS,L_default_rec.WAIT_BTWN_THREADS);
         L_temp_rec.MAX_THREADS := NVL( L_temp_rec.MAX_THREADS,L_default_rec.MAX_THREADS);
         L_temp_rec.MAX_CHUNK_SIZE := NVL( L_temp_rec.MAX_CHUNK_SIZE,L_default_rec.MAX_CHUNK_SIZE);
         L_temp_rec.APPLY_BRACKETS := NVL( L_temp_rec.APPLY_BRACKETS,L_default_rec.APPLY_BRACKETS);
         L_temp_rec.OVERRIDE_MANL_COST_SRC := NVL( L_temp_rec.OVERRIDE_MANL_COST_SRC,L_default_rec.OVERRIDE_MANL_COST_SRC);
      end if;
      if not (
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
SVC_ORD_PM_CFG_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                        'S9T_SVC_ORDER_PM_CFG_PK');
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_SVC_ORDER_PM_CFG_col.extend();
         svc_SVC_ORDER_PM_CFG_col(svc_SVC_ORDER_PM_CFG_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_SVC_ORDER_PM_CFG_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SVC_ORDER_PARAMETER_CONFIG st
      using(select
                  (case
                   when l_mi_rec.APPLY_DEALS_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.APPLY_DEALS IS NULL
                   then mt.APPLY_DEALS
                   else s1.APPLY_DEALS
                   end) AS APPLY_DEALS,
                  (case
                   when l_mi_rec.APPLY_SCALING_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.APPLY_SCALING IS NULL
                   then mt.APPLY_SCALING
                   else s1.APPLY_SCALING
                   end) AS APPLY_SCALING,
                  (case
                   when l_mi_rec.SKIP_OPEN_SHIPMENT_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.SKIP_OPEN_SHIPMENT IS NULL
                   then mt.SKIP_OPEN_SHIPMENT
                   else s1.SKIP_OPEN_SHIPMENT
                   end) AS SKIP_OPEN_SHIPMENT,
                  (case
                   when l_mi_rec.CANCEL_ALLOC_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.CANCEL_ALLOC IS NULL
                   then mt.CANCEL_ALLOC
                   else s1.CANCEL_ALLOC
                   end) AS CANCEL_ALLOC,
                  (case
                   when l_mi_rec.RECALC_REPLENISHMENT_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.RECALC_REPLENISHMENT IS NULL
                   then mt.RECALC_REPLENISHMENT
                   else s1.RECALC_REPLENISHMENT
                   end) AS RECALC_REPLENISHMENT,
                  (case
                   when l_mi_rec.OTB_OVERRIDE_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.OTB_OVERRIDE IS NULL
                   then mt.OTB_OVERRIDE
                   else s1.OTB_OVERRIDE
                   end) AS OTB_OVERRIDE,
                  (case
                   when l_mi_rec.MAX_ORDER_EXPIRY_DAYS_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.MAX_ORDER_EXPIRY_DAYS IS NULL
                   then mt.MAX_ORDER_EXPIRY_DAYS
                   else s1.MAX_ORDER_EXPIRY_DAYS
                   end) AS MAX_ORDER_EXPIRY_DAYS,
                  (case
                   when l_mi_rec.MAX_ORDER_NO_QTY_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.MAX_ORDER_NO_QTY IS NULL
                   then mt.MAX_ORDER_NO_QTY
                   else s1.MAX_ORDER_NO_QTY
                   end) AS MAX_ORDER_NO_QTY,
                  (case
                   when l_mi_rec.WAIT_BTWN_THREADS_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.WAIT_BTWN_THREADS IS NULL
                   then mt.WAIT_BTWN_THREADS
                   else s1.WAIT_BTWN_THREADS
                   end) AS WAIT_BTWN_THREADS,
                  (case
                   when l_mi_rec.MAX_THREADS_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.MAX_THREADS IS NULL
                   then mt.MAX_THREADS
                   else s1.MAX_THREADS
                   end) AS MAX_THREADS,
                  (case
                   when l_mi_rec.MAX_CHUNK_SIZE_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.MAX_CHUNK_SIZE IS NULL
                   then mt.MAX_CHUNK_SIZE
                   else s1.MAX_CHUNK_SIZE
                   end) AS MAX_CHUNK_SIZE,
                  (case
                   when l_mi_rec.APPLY_BRACKETS_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.APPLY_BRACKETS IS NULL
                   then mt.APPLY_BRACKETS
                   else s1.APPLY_BRACKETS
                   end) AS APPLY_BRACKETS,
                  (case
                   when l_mi_rec.OVERRIDE_MANL_COST_SRC_mi    = 'N'
                    and svc_SVC_ORDER_PM_CFG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.OVERRIDE_MANL_COST_SRC IS NULL
                   then mt.OVERRIDE_MANL_COST_SRC
                   else s1.OVERRIDE_MANL_COST_SRC
                   end) AS OVERRIDE_MANL_COST_SRC,
                  null as dummy
              from (select
                          svc_SVC_ORDER_PM_CFG_col(i).APPLY_DEALS AS APPLY_DEALS,
                          svc_SVC_ORDER_PM_CFG_col(i).APPLY_SCALING AS APPLY_SCALING,
                          svc_SVC_ORDER_PM_CFG_col(i).SKIP_OPEN_SHIPMENT AS SKIP_OPEN_SHIPMENT,
                          svc_SVC_ORDER_PM_CFG_col(i).CANCEL_ALLOC AS CANCEL_ALLOC,
                          svc_SVC_ORDER_PM_CFG_col(i).RECALC_REPLENISHMENT AS RECALC_REPLENISHMENT,
                          svc_SVC_ORDER_PM_CFG_col(i).OTB_OVERRIDE AS OTB_OVERRIDE,
                          svc_SVC_ORDER_PM_CFG_col(i).MAX_ORDER_EXPIRY_DAYS AS MAX_ORDER_EXPIRY_DAYS,
                          svc_SVC_ORDER_PM_CFG_col(i).MAX_ORDER_NO_QTY AS MAX_ORDER_NO_QTY,
                          svc_SVC_ORDER_PM_CFG_col(i).WAIT_BTWN_THREADS AS WAIT_BTWN_THREADS,
                          svc_SVC_ORDER_PM_CFG_col(i).MAX_THREADS AS MAX_THREADS,
                          svc_SVC_ORDER_PM_CFG_col(i).MAX_CHUNK_SIZE AS MAX_CHUNK_SIZE,
                          svc_SVC_ORDER_PM_CFG_col(i).APPLY_BRACKETS AS APPLY_BRACKETS,
                          svc_SVC_ORDER_PM_CFG_col(i).OVERRIDE_MANL_COST_SRC AS OVERRIDE_MANL_COST_SRC,
                          null as dummy
                      from dual ) s1,
            SVC_ORDER_PARAMETER_CONFIG mt
             where
                  1 = 1 )sq
                on (
                    svc_SVC_ORDER_PM_CFG_col(i).ACTION IN (CORESVC_PO_INDUCT_CFG.action_mod,CORESVC_PO_INDUCT_CFG.action_del))
      when matched then
      update
         set process_id      = svc_SVC_ORDER_PM_CFG_col(i).process_id ,
             chunk_id        = svc_SVC_ORDER_PM_CFG_col(i).chunk_id ,
             row_seq         = svc_SVC_ORDER_PM_CFG_col(i).row_seq ,
             action          = svc_SVC_ORDER_PM_CFG_col(i).action ,
             process$status  = svc_SVC_ORDER_PM_CFG_col(i).process$status ,
             apply_brackets              = sq.apply_brackets ,
             max_chunk_size              = sq.max_chunk_size ,
             wait_btwn_threads              = sq.wait_btwn_threads ,
             max_order_expiry_days              = sq.max_order_expiry_days ,
             otb_override              = sq.otb_override ,
             recalc_replenishment              = sq.recalc_replenishment ,
             max_threads              = sq.max_threads ,
             skip_open_shipment              = sq.skip_open_shipment ,
             cancel_alloc              = sq.cancel_alloc ,
             apply_scaling              = sq.apply_scaling ,
             apply_deals              = sq.apply_deals ,
             max_order_no_qty              = sq.max_order_no_qty ,
             override_manl_cost_src              = sq.override_manl_cost_src ,
             create_id       = svc_SVC_ORDER_PM_CFG_col(i).create_id ,
             create_datetime = svc_SVC_ORDER_PM_CFG_col(i).create_datetime ,
             last_upd_id     = svc_SVC_ORDER_PM_CFG_col(i).last_upd_id ,
             last_upd_datetime = svc_SVC_ORDER_PM_CFG_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             apply_deals ,
             apply_scaling ,
             skip_open_shipment ,
             cancel_alloc ,
             recalc_replenishment ,
             otb_override ,
             max_order_expiry_days ,
             max_order_no_qty ,
             wait_btwn_threads ,
             max_threads ,
             max_chunk_size ,
             apply_brackets ,
             override_manl_cost_src ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_SVC_ORDER_PM_CFG_col(i).process_id ,
             svc_SVC_ORDER_PM_CFG_col(i).chunk_id ,
             svc_SVC_ORDER_PM_CFG_col(i).row_seq ,
             svc_SVC_ORDER_PM_CFG_col(i).action ,
             svc_SVC_ORDER_PM_CFG_col(i).process$status ,
             sq.apply_deals ,
             sq.apply_scaling ,
             sq.skip_open_shipment ,
             sq.cancel_alloc ,
             sq.recalc_replenishment ,
             sq.otb_override ,
             sq.max_order_expiry_days ,
             sq.max_order_no_qty ,
             sq.wait_btwn_threads ,
             sq.max_threads ,
             sq.max_chunk_size ,
             sq.apply_brackets ,
             sq.override_manl_cost_src ,
             svc_SVC_ORDER_PM_CFG_col(i).create_id ,
             svc_SVC_ORDER_PM_CFG_col(i).create_datetime ,
             svc_SVC_ORDER_PM_CFG_col(i).last_upd_id ,
             svc_SVC_ORDER_PM_CFG_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
							SVC_ORD_PM_CFG_sheet,
                            svc_SVC_ORDER_PM_CFG_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;
     
END PROCESS_S9T_SVC_ORDER_PM_CFG;
--------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_PO_INDUCT_CONFIG( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_PO_INDUCT_CONFIG.process_id%TYPE) IS
   TYPE svc_PO_INDUCT_CONFIG_col_typ IS TABLE OF SVC_PO_INDUCT_CONFIG%ROWTYPE;
   L_temp_rec SVC_PO_INDUCT_CONFIG%ROWTYPE;
   svc_PO_INDUCT_CONFIG_col svc_PO_INDUCT_CONFIG_col_typ :=NEW svc_PO_INDUCT_CONFIG_col_typ();
   L_process_id SVC_PO_INDUCT_CONFIG.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_PO_INDUCT_CONFIG%ROWTYPE;
      L_pk_columns    VARCHAR2(255)  := 'PO Induction Config';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             MAX_PO_FOR_DNLD_mi,
             MAX_PO_FOR_SYNC_DNLD_mi,
             MAX_FILE_SIZE_FOR_UPLD_mi,
             MAX_FILE_SIZE_FOR_SYNC_UPLD_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'PO_INDUCT_CONFIG'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'MAX_PO_FOR_DNLD' AS MAX_PO_FOR_DNLD,
                                         'MAX_PO_FOR_SYNC_DNLD' AS MAX_PO_FOR_SYNC_DNLD,
                                         'MAX_FILE_SIZE_FOR_UPLD' AS MAX_FILE_SIZE_FOR_UPLD,
                                         'MAX_FILE_SIZE_FOR_SYNC_UPLD' AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       MAX_PO_FOR_DNLD_dv,
                       MAX_PO_FOR_SYNC_DNLD_dv,
                       MAX_FILE_SIZE_FOR_UPLD_dv,
                       MAX_FILE_SIZE_FOR_SYNC_UPLD_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'PO_INDUCT_CONFIG'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'MAX_PO_FOR_DNLD' AS MAX_PO_FOR_DNLD,
                                                      'MAX_PO_FOR_SYNC_DNLD' AS MAX_PO_FOR_SYNC_DNLD,
                                                      'MAX_FILE_SIZE_FOR_UPLD' AS MAX_FILE_SIZE_FOR_UPLD,
                                                      'MAX_FILE_SIZE_FOR_SYNC_UPLD' AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.MAX_PO_FOR_DNLD := rec.MAX_PO_FOR_DNLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_PO_FOR_DNLD ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.MAX_PO_FOR_SYNC_DNLD := rec.MAX_PO_FOR_SYNC_DNLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_PO_FOR_SYNC_DNLD ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.MAX_FILE_SIZE_FOR_UPLD := rec.MAX_FILE_SIZE_FOR_UPLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_FILE_SIZE_FOR_UPLD ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD := rec.MAX_FILE_SIZE_FOR_SYNC_UPLD_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'PO_INDUCT_CONFIG ' ,
                            NULL,
                           'MAX_FILE_SIZE_FOR_SYNC_UPLD ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(PO_INDUCT_CONFIG$Action)      AS Action,
          r.get_cell(PO_INDUCT_CONFIG$MXPOFR_DNLD)              AS MAX_PO_FOR_DNLD,
          r.get_cell(PO_INDUCT_CONFIG$MXPOFRSN_DNLD)              AS MAX_PO_FOR_SYNC_DNLD,
          r.get_cell(PO_INDUCT_CONFIG$MXFLSZFR_UPLD)              AS MAX_FILE_SIZE_FOR_UPLD,
          r.get_cell(PO_INDUCT_CONFIG$MXFLSZFRSC_UL)              AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(PO_INDUCT_CONFIG_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_PO_FOR_DNLD := rec.MAX_PO_FOR_DNLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_PO_FOR_DNLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_PO_FOR_SYNC_DNLD := rec.MAX_PO_FOR_SYNC_DNLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_PO_FOR_SYNC_DNLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_FILE_SIZE_FOR_UPLD := rec.MAX_FILE_SIZE_FOR_UPLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_FILE_SIZE_FOR_UPLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD := rec.MAX_FILE_SIZE_FOR_SYNC_UPLD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
PO_INDUCT_CONFIG_sheet,
                            rec.row_seq,
                            'MAX_FILE_SIZE_FOR_SYNC_UPLD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_PO_INDUCT_CFG.action_new then
         L_temp_rec.MAX_PO_FOR_DNLD := NVL( L_temp_rec.MAX_PO_FOR_DNLD,L_default_rec.MAX_PO_FOR_DNLD);
         L_temp_rec.MAX_PO_FOR_SYNC_DNLD := NVL( L_temp_rec.MAX_PO_FOR_SYNC_DNLD,L_default_rec.MAX_PO_FOR_SYNC_DNLD);
         L_temp_rec.MAX_FILE_SIZE_FOR_UPLD := NVL( L_temp_rec.MAX_FILE_SIZE_FOR_UPLD,L_default_rec.MAX_FILE_SIZE_FOR_UPLD);
         L_temp_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD := NVL( L_temp_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD,L_default_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD);
      end if;
      if not (
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
						 PO_INDUCT_CONFIG_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
          L_error := TRUE;
      end if;
      if NOT L_error then
         svc_PO_INDUCT_CONFIG_col.extend();
         svc_PO_INDUCT_CONFIG_col(svc_PO_INDUCT_CONFIG_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_PO_INDUCT_CONFIG_col.COUNT SAVE EXCEPTIONS
      merge into SVC_PO_INDUCT_CONFIG st
      using(select
                  (case
                   when l_mi_rec.MAX_PO_FOR_DNLD_mi    = 'N'
                    and svc_PO_INDUCT_CONFIG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.MAX_PO_FOR_DNLD IS NULL
                   then mt.MAX_PO_FOR_DNLD
                   else s1.MAX_PO_FOR_DNLD
                   end) AS MAX_PO_FOR_DNLD,
                  (case
                   when l_mi_rec.MAX_PO_FOR_SYNC_DNLD_mi    = 'N'
                    and svc_PO_INDUCT_CONFIG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.MAX_PO_FOR_SYNC_DNLD IS NULL
                   then mt.MAX_PO_FOR_SYNC_DNLD
                   else s1.MAX_PO_FOR_SYNC_DNLD
                   end) AS MAX_PO_FOR_SYNC_DNLD,
                  (case
                   when l_mi_rec.MAX_FILE_SIZE_FOR_UPLD_mi    = 'N'
                    and svc_PO_INDUCT_CONFIG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.MAX_FILE_SIZE_FOR_UPLD IS NULL
                   then mt.MAX_FILE_SIZE_FOR_UPLD
                   else s1.MAX_FILE_SIZE_FOR_UPLD
                   end) AS MAX_FILE_SIZE_FOR_UPLD,
                  (case
                   when l_mi_rec.MAX_FILE_SIZE_FOR_SYNC_UPLD_mi    = 'N'
                    and svc_PO_INDUCT_CONFIG_col(i).action = CORESVC_PO_INDUCT_CFG.action_mod
                    and s1.MAX_FILE_SIZE_FOR_SYNC_UPLD IS NULL
                   then mt.MAX_FILE_SIZE_FOR_SYNC_UPLD
                   else s1.MAX_FILE_SIZE_FOR_SYNC_UPLD
                   end) AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
                  null as dummy
              from (select
                          svc_PO_INDUCT_CONFIG_col(i).MAX_PO_FOR_DNLD AS MAX_PO_FOR_DNLD,
                          svc_PO_INDUCT_CONFIG_col(i).MAX_PO_FOR_SYNC_DNLD AS MAX_PO_FOR_SYNC_DNLD,
                          svc_PO_INDUCT_CONFIG_col(i).MAX_FILE_SIZE_FOR_UPLD AS MAX_FILE_SIZE_FOR_UPLD,
                          svc_PO_INDUCT_CONFIG_col(i).MAX_FILE_SIZE_FOR_SYNC_UPLD AS MAX_FILE_SIZE_FOR_SYNC_UPLD,
                          null as dummy
                      from dual ) s1,
            PO_INDUCT_CONFIG mt
             where
                  1 = 1 )sq
                on (
                    svc_PO_INDUCT_CONFIG_col(i).ACTION IN (CORESVC_PO_INDUCT_CFG.action_mod,CORESVC_PO_INDUCT_CFG.action_del))
      when matched then
      update
         set process_id      = svc_PO_INDUCT_CONFIG_col(i).process_id ,
             chunk_id        = svc_PO_INDUCT_CONFIG_col(i).chunk_id ,
             row_seq         = svc_PO_INDUCT_CONFIG_col(i).row_seq ,
             action          = svc_PO_INDUCT_CONFIG_col(i).action ,
             process$status  = svc_PO_INDUCT_CONFIG_col(i).process$status ,
             max_po_for_sync_dnld              = sq.max_po_for_sync_dnld ,
             max_po_for_dnld              = sq.max_po_for_dnld ,
             max_file_size_for_sync_upld              = sq.max_file_size_for_sync_upld ,
             max_file_size_for_upld              = sq.max_file_size_for_upld ,
             create_id       = svc_PO_INDUCT_CONFIG_col(i).create_id ,
             create_datetime = svc_PO_INDUCT_CONFIG_col(i).create_datetime ,
             last_upd_id     = svc_PO_INDUCT_CONFIG_col(i).last_upd_id ,
             last_upd_datetime = svc_PO_INDUCT_CONFIG_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             max_po_for_dnld ,
             max_po_for_sync_dnld ,
             max_file_size_for_upld ,
             max_file_size_for_sync_upld ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_PO_INDUCT_CONFIG_col(i).process_id ,
             svc_PO_INDUCT_CONFIG_col(i).chunk_id ,
             svc_PO_INDUCT_CONFIG_col(i).row_seq ,
             svc_PO_INDUCT_CONFIG_col(i).action ,
             svc_PO_INDUCT_CONFIG_col(i).process$status ,
             sq.max_po_for_dnld ,
             sq.max_po_for_sync_dnld ,
             sq.max_file_size_for_upld ,
             sq.max_file_size_for_sync_upld ,
             svc_PO_INDUCT_CONFIG_col(i).create_id ,
             svc_PO_INDUCT_CONFIG_col(i).create_datetime ,
             svc_PO_INDUCT_CONFIG_col(i).last_upd_id ,
             svc_PO_INDUCT_CONFIG_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
							PO_INDUCT_CONFIG_sheet,
                            svc_PO_INDUCT_CONFIG_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;

END PROCESS_S9T_PO_INDUCT_CONFIG;
-----------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_PO_INDUCT_CFG.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_SVC_ORDER_PM_CFG(I_file_id,I_process_id);
      PROCESS_S9T_PO_INDUCT_CONFIG(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;

---------------------------------------------------------------------------------------------
FUNCTION EXEC_SVC_ORDER_PM_CFG_UPD( L_SVC_ORDER_PM_CFG_temp_rec   IN   SVC_SVC_ORDER_PARAMETER_CONFIG%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_PO_INDUCT_CFG.EXEC_SVC_ORDER_PM_CFG_UPD';
   L_table   VARCHAR2(255):= 'SVC_SVC_ORDER_PARAMETER_CONFIG';
BEGIN
   update SVC_SVC_ORDER_PARAMETER_CONFIG
      set row = L_SVC_ORDER_PM_CFG_temp_rec
    where 1 = 1
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SVC_ORDER_PM_CFG_UPD;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_SVC_ORDER_PM_CFG( I_process_id   IN   SVC_SVC_ORDER_PARAMETER_CONFIG.PROCESS_ID%TYPE,
                                             I_chunk_id     IN SVC_SVC_ORDER_PARAMETER_CONFIG.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_PO_INDUCT_CFG.PROCESS_SVC_ORDER_PM_CFG';
   L_error_message VARCHAR2(600);
   L_SVC_ORDER_PM_CFG_temp_rec SVC_SVC_ORDER_PARAMETER_CONFIG%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_SVC_ORDER_PARAMETER_CONFIG';
BEGIN
   FOR rec IN C_SVC_SVC_ORDER_PM_CFG(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
	  if rec.action is NULL
         or rec.action NOT IN (action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      
	  if NOT(  rec.MAX_CHUNK_SIZE  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_CHUNK_SIZE',
                     'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.MAX_THREADS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_THREADS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.WAIT_BTWN_THREADS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'WAIT_BTWN_THREADS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.MAX_ORDER_NO_QTY  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_ORDER_NO_QTY',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.MAX_ORDER_EXPIRY_DAYS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_ORDER_EXPIRY_DAYS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.APPLY_BRACKETS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'APPLY_BRACKETS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.OTB_OVERRIDE  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OTB_OVERRIDE',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
     
	  if NOT(  rec.RECALC_REPLENISHMENT  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'RECALC_REPLENISHMENT',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.CANCEL_ALLOC  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CANCEL_ALLOC',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.APPLY_SCALING  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'APPLY_SCALING',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.SKIP_OPEN_SHIPMENT  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SKIP_OPEN_SHIPMENT',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.APPLY_DEALS  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'APPLY_DEALS',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if NOT(  rec.OVERRIDE_MANL_COST_SRC  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OVERRIDE_MANL_COST_SRC',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      
	  if rec.apply_scaling is not null 
	     and NOT( rec.APPLY_SCALING IN  ( 'Y', 'N' )  ) then

	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'APPLY_SCALING',
                                               NULL,
                                               NULL); 

	     WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'APPLY_SCALING',
                    L_error_message);
         L_error :=TRUE;
      end if;
	  
     if rec.APPLY_DEALS is not null 
	     and NOT( rec.APPLY_DEALS IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'APPLY_DEALS',
                                               NULL,
                                               NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'APPLY_DEALS',
                     L_error_message);
         L_error :=TRUE;
      end if;

     if rec.APPLY_BRACKETS is not null 
	     and NOT( rec.APPLY_BRACKETS IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'APPLY_BRACKETS',
                                               NULL,
                                               NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'APPLY_BRACKETS',
                     L_error_message);
         L_error :=TRUE;
      end if;

	  if rec.OTB_OVERRIDE is not null 
	     and NOT( rec.OTB_OVERRIDE IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'OTB_OVERRIDE',
                                               NULL,
                                               NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OTB_OVERRIDE',
                     L_error_message);
         L_error :=TRUE;
      end if;	  


	  if rec.RECALC_REPLENISHMENT is not null 
	     and NOT( rec.RECALC_REPLENISHMENT IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'RECALC_REPLENISHMENT',
                                               NULL,
                                               NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'RECALC_REPLENISHMENT',
                     L_error_message);
         L_error :=TRUE;
      end if;

	  if rec.CANCEL_ALLOC is not null 
	     and NOT( rec.CANCEL_ALLOC IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'CANCEL_ALLOC',
                                               NULL,
                                               NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CANCEL_ALLOC',
                     L_error_message);
         L_error :=TRUE;
      end if;

	  if rec.SKIP_OPEN_SHIPMENT is not null 
	     and NOT( rec.SKIP_OPEN_SHIPMENT IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'SKIP_OPEN_SHIPMENT',
                                               NULL,
                                               NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SKIP_OPEN_SHIPMENT',
                     L_error_message);
         L_error :=TRUE;
      end if;	  

	  if rec.OVERRIDE_MANL_COST_SRC is not null 
	     and NOT( rec.OVERRIDE_MANL_COST_SRC IN  ( 'Y', 'N' )  ) then
	     L_error_message := SQL_LIB.CREATE_MSG('INV_INDICATOR',
                                               'OVERRIDE_MANL_COST_SRC',
                                               NULL,
                                               NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'OVERRIDE_MANL_COST_SRC',
                     L_error_message);
         L_error :=TRUE;
      end if;	  

      if NOT L_error then
         L_SVC_ORDER_PM_CFG_temp_rec.override_manl_cost_src    := rec.override_manl_cost_src;
         L_SVC_ORDER_PM_CFG_temp_rec.apply_brackets            := rec.apply_brackets;
         L_SVC_ORDER_PM_CFG_temp_rec.max_chunk_size            := rec.max_chunk_size;
         L_SVC_ORDER_PM_CFG_temp_rec.max_threads               := rec.max_threads;
         L_SVC_ORDER_PM_CFG_temp_rec.wait_btwn_threads         := rec.wait_btwn_threads;
         L_SVC_ORDER_PM_CFG_temp_rec.max_order_no_qty          := rec.max_order_no_qty;
         L_SVC_ORDER_PM_CFG_temp_rec.max_order_expiry_days     := rec.max_order_expiry_days;
         L_SVC_ORDER_PM_CFG_temp_rec.otb_override              := rec.otb_override;
         L_SVC_ORDER_PM_CFG_temp_rec.recalc_replenishment      := rec.recalc_replenishment;
         L_SVC_ORDER_PM_CFG_temp_rec.cancel_alloc              := rec.cancel_alloc;
         L_SVC_ORDER_PM_CFG_temp_rec.skip_open_shipment        := rec.skip_open_shipment;
         L_SVC_ORDER_PM_CFG_temp_rec.apply_scaling             := rec.apply_scaling;
         L_SVC_ORDER_PM_CFG_temp_rec.apply_deals               := rec.apply_deals;
         
         if rec.action = action_mod then
            if EXEC_SVC_ORDER_PM_CFG_UPD( L_SVC_ORDER_PM_CFG_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         
      else
         update SVC_SVC_ORDER_PARAMETER_CONFIG st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update SVC_SVC_ORDER_PARAMETER_CONFIG st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update SVC_SVC_ORDER_PARAMETER_CONFIG st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_SVC_ORDER_PM_CFG%isopen then
         close C_SVC_SVC_ORDER_PM_CFG;
      end if;   
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SVC_ORDER_PM_CFG;
-----------------------------------------------------------------------
FUNCTION EXEC_PO_INDUCT_CONFIG_UPD( L_po_induct_config_temp_rec   IN   PO_INDUCT_CONFIG%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_PO_INDUCT_CFG.EXEC_PO_INDUCT_CONFIG_UPD';
   L_table   VARCHAR2(255):= 'SVC_PO_INDUCT_CONFIG';
BEGIN
   update po_induct_config
      set row = L_po_induct_config_temp_rec
    where 1 = 1
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_PO_INDUCT_CONFIG_UPD;
-------------------------------------------------------------------------------
FUNCTION PROCESS_PO_INDUCT_CONFIG( I_process_id   IN   SVC_PO_INDUCT_CONFIG.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_PO_INDUCT_CONFIG.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_PO_INDUCT_CFG.PROCESS_PO_INDUCT_CONFIG';
   L_error_message VARCHAR2(600);
   L_PO_INDUCT_CONFIG_temp_rec PO_INDUCT_CONFIG%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_PO_INDUCT_CONFIG';
BEGIN
   FOR rec IN c_svc_PO_INDUCT_CONFIG(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
    if NOT(  rec.MAX_PO_FOR_DNLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_PO_FOR_DNLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_PO_FOR_SYNC_DNLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_PO_FOR_SYNC_DNLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_FILE_SIZE_FOR_UPLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_FILE_SIZE_FOR_UPLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
    if NOT(  rec.MAX_FILE_SIZE_FOR_SYNC_UPLD  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'MAX_FILE_SIZE_FOR_SYNC_UPLD',
                    'ENTER_REC');
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_po_induct_config_temp_rec.max_file_size_for_sync_upld              := rec.max_file_size_for_sync_upld;
         L_po_induct_config_temp_rec.max_file_size_for_upld              := rec.max_file_size_for_upld;
         L_po_induct_config_temp_rec.max_po_for_sync_dnld              := rec.max_po_for_sync_dnld;
         L_po_induct_config_temp_rec.max_po_for_dnld              := rec.max_po_for_dnld;

         if rec.action = action_mod then
            if EXEC_PO_INDUCT_CONFIG_UPD( L_po_induct_config_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

      else
         update svc_po_induct_config st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_po_induct_config st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_po_induct_config st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_PO_INDUCT_CONFIG%isopen then
         close c_svc_PO_INDUCT_CONFIG;
      end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_PO_INDUCT_CONFIG;
-----------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete 
     from svc_po_induct_config 
    where process_id=I_process_id;

   delete 
     from svc_SVC_ORDER_PARAMETER_CONFIG
    where process_id=I_process_id;

END;

-------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER
                  )

RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_PO_INDUCT_CFG.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';

BEGIN
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_SVC_ORDER_PM_CFG(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;

   commit;

   if PROCESS_PO_INDUCT_CONFIG(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;

   commit;

   CLEAR_STAGING_DATA(I_process_id);

   O_error_count := LP_errors_tab.COUNT();

   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;


   return TRUE;
EXCEPTION
   when OTHERS then
    CLEAR_STAGING_DATA(I_process_id);
    if c_get_err_count%isopen then
       close c_get_err_count;
    end if;
    if c_get_warn_count%isopen then
       close c_get_warn_count;
    end if;
	  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_PO_INDUCT_CFG;
/