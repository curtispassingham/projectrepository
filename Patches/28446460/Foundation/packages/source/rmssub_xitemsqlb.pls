create or replace PACKAGE BODY RMSSUB_XITEM_SQL AS

LP_isc_unit_cost        ITEM_SUPP_COUNTRY.UNIT_COST%TYPE; -- used for item create price_hist record
LP_prim_supp            SUPS.SUPPLIER%TYPE; -- used for converting currency for item create price_hist record
LP_supp_currency_code   SUPS.CURRENCY_CODE%TYPE;
LP_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
LP_process_id           SVC_ITEM_MASTER.PROCESS_ID%TYPE;
LP_chunk_id             SVC_ITEM_MASTER.CHUNK_ID%TYPE;
LP_process_status       SVC_ITEM_MASTER.PROCESS$STATUS%TYPE;
LP_user                 VARCHAR2(30) := get_user;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
FUNCTION GET_VALIDATION_ERRORS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_ITEM
   -- Purpose      :  Inserts a record on the item_master table
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message         IN       "RIB_XItemDesc_REC",
                     I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                     I_action          IN       SVC_ITEM_MASTER.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISUP
   -- Purpose      :  Inserts records on the svc_item_supplier table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sup_rows        IN       "RIB_XItemSupDesc_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                         I_action          IN       SVC_ITEM_SUPPLIER.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISUP
   -- Purpose      :  Inserts records on the svc_item_supplier table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sup_rows        IN       "RIB_XItemSupRef_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_action          IN       SVC_ITEM_SUPPLIER.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISC
   -- Purpose      :  Inserts records on the svc_item_supp_country table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_isup_rows       IN       "RIB_XItemSupDesc_TBL",
                        I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                        I_message_type    IN       VARCHAR2,
                        I_action          IN       SVC_ITEM_SUPP_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISC
   -- Purpose   :  Inserts records on the svc_item_supp_country table
--------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISCL(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item_rec      IN       RMSSUB_XITEM.ITEM_API_REC,
                         I_hier_level    IN       VARCHAR2,
                         I_message_type  IN       VARCHAR2,
                         I_action        IN       SVC_ITEM_SUPP_COUNTRY_LOC.ACTION%TYPE)
   RETURN BOOLEAN;
   
----------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISC
   -- Purpose      :  Inserts records on the svc_item_supp_country table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_isup_rows       IN       "RIB_XItemSupRef_TBL",
                        I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                        I_action          IN       SVC_ITEM_SUPP_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_hier_level      IN       VARCHAR2,
                         I_action          IN       SVC_ITEM_SUPP_COUNTRY_LOC.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISMC
   -- Purpose      :  Inserts records on the svc_item_supp_manu_country table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISMC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_isup_rows       IN       "RIB_XItemSupDesc_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                         I_action          IN       SVC_ITEM_SUPP_MANU_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISMC
   -- Purpose      :  Inserts records on the svc_item_supp_manu_country table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISMC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sup_rows        IN       "RIB_XItemSupRef_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_action          IN       SVC_ITEM_SUPP_MANU_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISCD
   -- Purpose      :  Creates or updates a record on the svc_item_supp_country_dim table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISCD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_isup_rows       IN       "RIB_XItemSupDesc_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                         I_message_type    IN       VARCHAR2,
                         I_action          IN       SVC_ITEM_SUPP_COUNTRY_DIM.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_ISCD
   -- Purpose      :  Creates or updates a record on the svc_item_supp_country_dim table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISCD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sup_rows        IN       "RIB_XItemSupRef_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_action          IN       SVC_ITEM_SUPP_COUNTRY_DIM.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_IZP
   -- Purpose      :  Inserts records into the svc_rpm_item_zone_price table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IZP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                        I_action          IN       SVC_RPM_ITEM_ZONE_PRICE.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_PACKITEM
   -- Purpose      :  Inserts records on the svc_packitem table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_PACKITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                             I_action          IN       SVC_PACKITEM.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_VATITEM
   -- Purpose      :  Inserts records on the svc_vat_item table
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_VATITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_vat_rows        IN       "RIB_XItemVATDesc_TBL",
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                            I_action          IN       SVC_VAT_ITEM.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  INSERT_SVC_VATITEM
   -- Purpose      :  Inserts records on the svc_vat_item table
-------------------------------------------------------------------------------------------------------
   FUNCTION INSERT_SVC_VATITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_vat_rows        IN       "RIB_XItemVATRef_TBL",
                               I_item            IN       ITEM_MASTER.ITEM%TYPE,
                               I_action          IN       SVC_VAT_ITEM.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_ITEMSEASON_DETAIL
   -- Purpose      : This function will insert records to svc_item_seasons table .
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEMSEASON_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                                      I_action          IN       SVC_ITEM_SEASONS.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_ITEMSEASON_DETAIL
   -- Purpose      : This function will insert records to svc_item_seasons table .
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEMSEASON_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                                      I_action          IN       SVC_ITEM_SEASONS.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_ITEMUDA_DETAIL
   -- Purpose      : This function will insert records to the svc_uda_item_lov, svc_uda_item_dte and
   --                svc_uda_item_ff tables.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEMUDA_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                                   I_action          IN       SVC_UDA_ITEM_FF.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_ITEMUDA_DETAIL
   -- Purpose      : This function will insert records to the svc_uda_item_lov, svc_uda_item_dte and
   --                svc_item_uda_ff tables.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEMUDA_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                                   I_action          IN       SVC_UDA_ITEM_FF.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_MASTER(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item_rec        IN        ITEM_MASTER%ROWTYPE,
                                I_action          IN        SVC_ITEM_MASTER.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_MASTER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                                I_action          IN       SVC_ITEM_MASTER.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_COUNTRY(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_ctry_rows       IN        "RIB_XItemCtryDesc_TBL",
                                 I_item_rec        IN        RMSSUB_XITEM.ITEM_API_REC,
                                 I_action          IN        SVC_ITEM_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_COUNTRY(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_ctry_rows       IN        "RIB_XItemCtryRef_TBL",
                                 I_item_rec        IN        RMSSUB_XITEM.ITEM_API_DEL_REC,
                                 I_action          IN        SVC_ITEM_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION CONVERT_PRICE_HIST_UNIT_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_pack_type       IN       ITEM_MASTER.PACK_TYPE%TYPE,
                                      I_sellable_ind    IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                                      I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                      I_orderable_ind   IN       ITEM_MASTER.ORDERABLE_IND%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  DEFAULT_VATITEM
   -- Purpose      :  Inserts records on the vat_item table with default values from the item's dept.
-------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_VATITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_IMAGE
   -- Purpose      : This function will insert records to the svc_item_image table.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IMAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                          I_action          IN       SVC_ITEM_IMAGE.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_IMAGE
   -- Purpose      : This function will insert records to the svc_item_image table.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IMAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                          I_action          IN       SVC_ITEM_IMAGE.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_ITEM_TL
   -- Purpose      : This function will insert records to the svc_item_master_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                            I_action          IN       SVC_ITEM_MASTER_TL.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_ITEM_TL
   -- Purpose      : This function will insert records to the svc_item_master_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                            I_action          IN       SVC_ITEM_MASTER_TL.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_ISUP_TL
   -- Purpose      : This function will insert records to the svc_item_supplier_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                            I_action          IN       SVC_ITEM_SUPPLIER_TL.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_ISUP_TL
   -- Purpose      : This function will insert records to the svc_item_supplier_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                            I_action          IN       SVC_ITEM_SUPPLIER_TL.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_IIMG_TL
   -- Purpose      : This function will insert records to the svc_item_image_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IIMG_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                            I_action          IN       SVC_ITEM_IMAGE_TL.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_SVC_IIMG_TL
   -- Purpose      : This function will insert records to the svc_item_image_tl table.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IIMG_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                            I_action          IN       SVC_ITEM_IMAGE_TL.ACTION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message_type    IN       VARCHAR2,
                 I_message         IN       "RIB_XItemDesc_REC",
                 I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC)

   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XITEM_SQL.PERSIST_MESSAGE';
   L_rms_async_id   RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;

BEGIN
   -- Re-initialize global variables
   LP_isc_unit_cost      := NULL;
   LP_prim_supp          := NULL;
   LP_supp_currency_code := NULL;
   LP_process_id         := NULL;
   LP_chunk_id           := 1;
   LP_process_status     := 'N';

   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                LP_system_options_row) then
      return FALSE;
   end if;

   LP_process_id := CORESVC_ITEM_PSEQ.NEXTVAL;

   if LOWER(I_message_type) = RMSSUB_XITEM.ITEM_ADD then

      if CREATE_ITEM(O_error_message,
                     I_message,
                     I_item_rec,
                     CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.IMTL_ADD then
      if INSERT_SVC_ITEM_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ITEM_UPD then
      if INSERT_SVC_ITEM_MASTER(O_error_message,
                                I_item_rec.item_master_def,
                                CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.IMTL_UPD then
      if INSERT_SVC_ITEM_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ICTRY_ADD then
      if LP_system_options_row.default_tax_type = 'GTAX' then -- Country information can not be defaulted/inserted for NON GTAX tax types.
         if NOT INSERT_SVC_ITEM_COUNTRY(O_error_message,
                                        I_message.XITEMCTRYDESC_TBL,
                                        I_item_rec,
                                        CORESVC_ITEM.ACTION_NEW) then
            return FALSE;
         end if;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISUP_ADD then
      if INSERT_SVC_ISUP(O_error_message,
                         I_message.XITEMSUPDESC_TBL,
                         I_item_rec,
                         CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
      if INSERT_SVC_ISUP_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISTL_ADD then
      if INSERT_SVC_ISUP_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISC_ADD then
      if INSERT_SVC_ISC(O_error_message,
                        I_message.XITEMSUPDESC_TBL,
                        I_item_rec,
                        I_message_type,
                        CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISMC_ADD then
      if INSERT_SVC_ISMC(O_error_message,
                         I_message.XITEMSUPDESC_TBL,
                         I_item_rec,
                         CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.IVAT_ADD and
         LP_system_options_row.default_tax_type='SVAT' and
         I_message.item_level <= I_message.tran_level  then
      if INSERT_SVC_VATITEM(O_error_message,
                            I_message.XITEMVATDESC_TBL,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISUP_UPD then
      if INSERT_SVC_ISUP(O_error_message,
                         I_message.XITEMSUPDESC_TBL,
                         I_item_rec,
                         CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISTL_UPD then
      if INSERT_SVC_ISUP_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
    elsif LOWER(I_message_type) = RMSSUB_XITEM.ISC_UPD then
      if INSERT_SVC_ISC(O_error_message,
                        I_message.XITEMSUPDESC_TBL,
                        I_item_rec,
                        I_message_type,
                        CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
    elsif LOWER(I_message_type) = RMSSUB_XITEM.ISCL_ADD  then
       if INSERT_SVC_ISCL(O_error_message,
                          I_item_rec,
                          I_message.iscloc_hier_level,  
                          I_message_type,
                          CORESVC_ITEM.ACTION_NEW)= FALSE then
         return FALSE;
      end if;
    elsif LOWER(I_message_type) = RMSSUB_XITEM.ISCL_UPD then
       if INSERT_SVC_ISCL(O_error_message,
                          I_item_rec,
                          I_message.iscloc_hier_level,  
                          I_message_type,
                          CORESVC_ITEM.ACTION_MOD)= FALSE then
         return FALSE;
      end if;
   
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISMC_UPD then
      if INSERT_SVC_ISMC(O_error_message,
                         I_message.XITEMSUPDESC_TBL,
                         I_item_rec,
                         CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISCD_ADD then
      if INSERT_SVC_ISCD(O_error_message,
                         I_message.XITEMSUPDESC_TBL,
                         I_item_rec,
                         I_message_type,
                         CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISCD_UPD then
      if INSERT_SVC_ISCD(O_error_message,
                         I_message.XITEMSUPDESC_TBL,
                         I_item_rec,
                         I_message_type,
                         CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ITEMUDA_ADD then
      if INSERT_SVC_ITEMUDA_DETAIL(O_error_message,
                                   I_item_rec,
                                   CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ITEMUDA_UPD then
      if INSERT_SVC_ITEMUDA_DETAIL(O_error_message,
                                   I_item_rec,
                                   CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.IMAGE_ADD then
      if INSERT_SVC_IMAGE(O_error_message,
                          I_item_rec,
                          CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
      if INSERT_SVC_IIMG_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.IITL_ADD then
      if INSERT_SVC_IIMG_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.IMAGE_UPD then
      if INSERT_SVC_IMAGE(O_error_message,
                          I_item_rec,
                          CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.IITL_UPD then
      if INSERT_SVC_IIMG_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.SEASON_ADD then
      if INSERT_SVC_ITEMSEASON_DETAIL(O_error_message,
                                      I_item_rec,
                                      CORESVC_ITEM.ACTION_NEW) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) in (RMSSUB_XITEM.ITCOST_ADD, RMSSUB_XITEM.ITEMUDA_UPD) then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_rms_async_id := rms_async_id_seq.nextval;

   insert into svc_process_tracker(process_id,
                                   process_desc,
                                   template_key,
                                   action_type,
                                   process_source,
                                   process_destination,
                                   status,
                                   rms_async_id,
                                   action_date,
                                   user_id)
                            values(LP_process_id,
                                   'XITEM',
                                   'RMSSUB_XITEM',
                                   'U',
                                   'EXT',
                                   NVL(I_message.attempt_rms_load, 'RMS'),
                                   DECODE(NVL(I_message.attempt_rms_load, 'RMS'),'RMS','N','PS'),
                                   DECODE(NVL(I_message.attempt_rms_load, 'RMS'), 'RMS', L_rms_async_id, NULL),
                                   SYSDATE,
                                   LP_user);

   if NVL(I_message.attempt_rms_load, 'RMS') = 'RMS' then
      if ITEM_INDUCT_SQL.EXEC_ASYNC(O_error_message,
                                    L_rms_async_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_message_type    IN       VARCHAR2,
                 I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                 I_message         IN       "RIB_XItemRef_REC")

   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'RMSSUB_XITEM_SQL.PERSIST_MESSAGE';
   L_rms_async_id   RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;

BEGIN
   LP_process_id := NULL;
   LP_chunk_id   := 1;
   LP_process_status := 'N';

   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                LP_system_options_row) then
      return FALSE;
   end if;

   LP_process_id := CORESVC_ITEM_PSEQ.NEXTVAL;

   if I_message_type = RMSSUB_XITEM.ITEM_DEL then
      -- insert into svc_item_master with action = MOD but status = 'D'
      if INSERT_SVC_ITEM_MASTER(O_error_message,
                                I_item_rec,
                                CORESVC_ITEM.ACTION_MOD) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.IMTL_DEL then
      if INSERT_SVC_ITEM_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ICTRY_DEL then
      if LP_system_options_row.default_tax_type = 'GTAX' then -- Country information can not be defaulted/inserted for NON GTAX tax types.
         if NOT INSERT_SVC_ITEM_COUNTRY(O_error_message,
                                        I_message.XITEMCTRYREF_TBL,
                                        I_item_rec,
                                        CORESVC_ITEM.ACTION_DEL) then
            return FALSE;
         end if;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ISUP_DEL then
      if INSERT_SVC_ISUP(O_error_message,
                         I_message.XITEMSUPREF_TBL,
                         I_item_rec,
                         CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.ISTL_DEL then
      if INSERT_SVC_ISUP_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ISC_DEL then
      if INSERT_SVC_ISC(O_error_message,
                        I_message.XITEMSUPREF_TBL,
                        I_item_rec,
                        CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
    elsif I_message_type = RMSSUB_XITEM.ISCL_DEL  then
      if INSERT_SVC_ISCL(O_error_message,
                         I_item_rec,
                         I_message.hier_level, 
                         CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ISMC_DEL then
      if INSERT_SVC_ISMC(O_error_message,
                         I_message.XITEMSUPREF_TBL,
                         I_item_rec,
                         CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
    elsif I_message_type = RMSSUB_XITEM.IVAT_DEL then
      if INSERT_SVC_VATITEM(O_error_message,
                            I_message.XITEMVATREF_TBL,
                            I_message.item,
                            CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ISCD_DEL then
      if INSERT_SVC_ISCD(O_error_message,
                         I_message.XITEMSUPREF_TBL,
                         I_item_rec,
                         CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.ITEMUDA_DEL then
      if INSERT_SVC_ITEMUDA_DETAIL(O_error_message,
                                   I_item_rec,
                                   CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.IMAGE_DEL then
      if INSERT_SVC_IMAGE(O_error_message,
                          I_item_rec,
                          CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) = RMSSUB_XITEM.IITL_DEL then
      if INSERT_SVC_IIMG_TL(O_error_message,
                            I_item_rec,
                            CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif I_message_type = RMSSUB_XITEM.SEASON_DEL then
      if INSERT_SVC_ITEMSEASON_DETAIL(O_error_message,
                                      I_item_rec,
                                      CORESVC_ITEM.ACTION_DEL) = FALSE then
         return FALSE;
      end if;
   elsif LOWER(I_message_type) in (RMSSUB_XITEM.ISCL_DEL, RMSSUB_XITEM.ITCOST_DEL) then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   L_rms_async_id := rms_async_id_seq.nextval;

   insert into svc_process_tracker(process_id,
                                   process_desc,
                                   template_key,
                                   action_type,
                                   process_source,
                                   process_destination,
                                   status,
                                   rms_async_id,
                                   action_date,
                                   user_id)
                            values(LP_process_id,
                                   'XITEM',
                                   'RMSSUB_XITEM',
                                   'U',
                                   'EXT',
                                   'RMS', --NVL(I_message.attempt_rms_load, 'STG'),
                                   'N',
                                   L_rms_async_id,  --DECODE(I_message.attempt_rms_load, 'RMS', L_rms_async_id, NULL),
                                   SYSDATE,
                                   LP_user);

--   if NVL(I_message.attempt_rms_load, 'RMS') = 'RMS' then
      if ITEM_INDUCT_SQL.EXEC_ASYNC(O_error_message,
                                    L_rms_async_id) = FALSE then
         return FALSE;
      end if;
--   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION GET_VALIDATION_ERRORS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.GET_VALIDATION_ERRORS';
   L_status       SVC_PROCESS_TRACKER.STATUS%TYPE;
   L_error_key    CORESVC_ITEM_ERR.ERROR_MSG%TYPE;

   cursor C_GET_SVC_PROCESS_STATUS is
      select status
        from svc_process_tracker
       where process_id = LP_process_id;

   cursor C_GET_ERROR is
      select error_msg
        from coresvc_item_err
       where process_id = LP_process_id;

BEGIN
   open C_GET_SVC_PROCESS_STATUS;
   fetch C_GET_SVC_PROCESS_STATUS into L_status;
   close C_GET_SVC_PROCESS_STATUS;

   if L_status = 'PE' then
      open C_GET_ERROR;
      fetch C_GET_ERROR into L_error_key;
      close C_GET_ERROR;

      O_error_message := substr(('PID: '||LP_process_id||';'||L_error_key), 1, 255);
   else
      O_error_message := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_VALIDATION_ERRORS;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_ITEM(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_message       IN       "RIB_XItemDesc_REC",
                     I_item_rec      IN       RMSSUB_XITEM.ITEM_API_REC,
                     I_action        IN       SVC_ITEM_MASTER.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.CREATE_ITEM';

   L_item_rec     RMSSUB_XITEM.ITEM_API_REC := I_item_rec;  --- Use with pricing
   L_item_row    ITEM_MASTER%ROWTYPE := I_item_rec.item_master_def;

BEGIN

   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                LP_system_options_row) then
      return FALSE;
   end if;
   if NOT INSERT_SVC_ITEM_MASTER(O_error_message,
                                 L_item_row,
                                 I_action) then
      return FALSE;
   end if;
   if I_item_rec.Item_Master_lang.lang is NOT NULL and I_item_rec.Item_Master_lang.lang.COUNT > 0 then
      if INSERT_SVC_ITEM_TL(O_error_message,
                            I_item_rec,
                            I_action) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type = 'GTAX' then -- Country information can not be defaulted/inserted for NON GTAX tax types.
      if NOT INSERT_SVC_ITEM_COUNTRY(O_error_message,
                                     I_message.xitemctrydesc_TBL,
                                     I_item_rec,
                                     I_action) then
         return FALSE;
      end if;
   end if;

   if NOT INSERT_SVC_ISUP(O_error_message,
                          I_message.xitemsupdesc_TBL,
                          I_item_rec,
                          I_action) then
      return FALSE;
   end if;
   if I_item_rec.Item_Supp_lang.lang is NOT NULL and I_item_rec.Item_Supp_lang.lang.COUNT > 0 then
      if INSERT_SVC_ISUP_TL(O_error_message,
                            I_item_rec,
                            I_action) = FALSE then
         return FALSE;
      end if;
   end if;

   --- Add components for pack first so that unit_cost can
   --- be calculated
   if NVL(I_message.pack_ind, 'N') = 'Y' and I_message.item_level = I_message.tran_level then
      if NOT INSERT_SVC_PACKITEM(O_error_message,
                                 I_item_rec,
                                 I_action) then
         return FALSE;
      end if;
   end if;

   if I_message.item_level <= I_message.tran_level and
      LP_system_options_row.default_tax_type = 'SVAT' then

      if I_message.xitemvatdesc_TBL is NOT NULL and
         I_message.xitemvatdesc_TBL.COUNT > 0 then

         if NOT INSERT_SVC_VATITEM(O_error_message,
                                   I_message.xitemvatdesc_TBL,
                                   I_item_rec,
                                   I_action) then
            return FALSE;
         end if;
      end if;
   end if;

   if I_message.item_level <= I_message.tran_level then
      -- insert into svc_item_supp_country and svc_item_supp_country_dim
      if NOT INSERT_SVC_ISC(O_error_message,
                            I_message.xitemsupdesc_TBL,
                            I_item_rec,
                            RMSSUB_XITEM.ITEM_ADD,
                            I_action) then
         return FALSE;
      end if;

      if NOT INSERT_SVC_ISMC(O_error_message,
                             I_message.xitemsupdesc_TBL,
                             I_item_rec,
                             I_action) then
         return FALSE;
      end if;

      if LP_system_options_row.rpm_ind = 'Y' then
         if NOT INSERT_SVC_IZP(O_error_message,
                               L_item_rec,
                               I_action) then
            return FALSE;
         end if;
      end if;
   end if;

   if I_item_rec.seasons.season_id is NOT NULL and
      I_item_rec.seasons.season_id.count > 0 then
         if INSERT_SVC_ITEMSEASON_DETAIL(O_error_message,
                                         I_item_rec,
                                         I_action) = FALSE then
            return FALSE;
         end if;
   end if;

   if (I_item_rec.udaslov.uda_id is NOT NULL and
       I_item_rec.udaslov.uda_id.count> 0 ) or
      (I_item_rec.udadate.uda_id is NOT NULL and
       I_item_rec.udadate.uda_id.count> 0 ) or
      (I_item_rec.udaff.uda_id is NOT NULL and
       I_item_rec.udaff.uda_id.count> 0 )   then

      if INSERT_SVC_ITEMUDA_DETAIL(O_error_message,
                                   I_item_rec,
                                   I_action) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_item_rec.images.image_name is NOT NULL and I_item_rec.images.image_name.COUNT> 0 then
      if INSERT_SVC_IMAGE(O_error_message,
                          I_item_rec,
                          I_action) = FALSE then
         return FALSE;
      end if;
      if I_item_rec.Item_Images_lang.lang is NOT NULL and I_item_rec.Item_Images_lang.lang.COUNT > 0 then
         if INSERT_SVC_IIMG_TL(O_error_message,
                               I_item_rec,
                               I_action) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CREATE_ITEM;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISUP(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sup_rows      IN       "RIB_XItemSupDesc_TBL",
                         I_item_rec      IN       RMSSUB_XITEM.ITEM_API_REC,
                         I_action        IN       SVC_ITEM_SUPPLIER.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISUP';
   L_isup_row     ITEM_SUPPLIER%ROWTYPE := I_item_rec.item_supp_def;
   L_purch_type   DEPS.PURCHASE_TYPE%TYPE;
   L_dept         DEPS.DEPT%TYPE;
   L_diff_count   NUMBER;

   cursor C_GET_IM IS
      select dept,
             nvl2(diff_1,1,0)+nvl2(diff_2,1,0)+nvl2(diff_3,1,0)+nvl2(diff_4,1,0) diff_count
        from item_master
       where item = I_item_rec.item_supp_def.item;

BEGIN
   -- Get Item Master Info
   open C_GET_IM;
   fetch C_GET_IM into L_dept,
                       L_diff_count;
   close C_GET_IM;

   if L_dept is NOT NULL then -- item already exists in RMS
      -- Get purchase type
      if DEPT_ATTRIB_SQL.GET_PURCHASE_TYPE(O_error_message,
                                           L_purch_type,
                                           L_dept) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_sup_rows is NOT NULL and
      I_sup_rows.COUNT > 0  then
      FOR i IN I_sup_rows.FIRST..I_sup_rows.LAST LOOP
         L_isup_row.supplier := I_sup_rows(i).supplier;
         L_isup_row.primary_supp_ind := NVL(I_sup_rows(i).primary_supp_ind, 'N');
         L_isup_row.vpn := I_sup_rows(i).vpn;
         L_isup_row.supp_label := I_sup_rows(i).supp_label;
         if (L_purch_type = 1) then
            L_isup_row.consignment_rate := I_sup_rows(i).consignment_rate;
         elsif (L_purch_type = 2) then
            L_isup_row.concession_rate := I_sup_rows(i).concession_rate;
         elsif L_purch_type is NULL then
            -- L_purchase type is NULL if item does not exist in RMS.  Let coresvc validate the consignment/concession rate
            L_isup_row.consignment_rate := I_sup_rows(i).consignment_rate;
            L_isup_row.concession_rate := I_sup_rows(i).concession_rate;
         end if;
         L_isup_row.supp_discontinue_date := I_sup_rows(i).supp_discontinue_date;
         L_isup_row.pallet_name := NVL(I_sup_rows(i).pallet_name, I_item_rec.item_supp_def.pallet_name);
         L_isup_row.case_name := NVL(I_sup_rows(i).case_name, I_item_rec.item_supp_def.case_name);
         L_isup_row.inner_name := NVL(I_sup_rows(i).inner_name, I_item_rec.item_supp_def.inner_name);
         L_isup_row.direct_ship_ind := NVL(I_sup_rows(i).direct_ship_ind, I_item_rec.item_supp_def.direct_ship_ind);
         L_isup_row.last_update_id := LP_user;

         if I_item_rec.item_master_def.aip_case_type = 'I' then
            L_isup_row.primary_case_size := I_sup_rows(i).primary_case_size;
         else
            L_isup_row.primary_case_size := NULL;
         end if;

         if L_dept is NOT NULL then
            case L_diff_count
               when 4 then L_isup_row.supp_diff_4 := I_sup_rows(i).supp_diff_4;
                           L_isup_row.supp_diff_3 := I_sup_rows(i).supp_diff_3;
                           L_isup_row.supp_diff_2 := I_sup_rows(i).supp_diff_2;
                           L_isup_row.supp_diff_1 := I_sup_rows(i).supp_diff_1;
               when 3 then L_isup_row.supp_diff_3 := I_sup_rows(i).supp_diff_3;
                           L_isup_row.supp_diff_2 := I_sup_rows(i).supp_diff_2;
                           L_isup_row.supp_diff_1 := I_sup_rows(i).supp_diff_1;
               when 2 then L_isup_row.supp_diff_2 := I_sup_rows(i).supp_diff_2;
                           L_isup_row.supp_diff_1 := I_sup_rows(i).supp_diff_1;
               when 1 then L_isup_row.supp_diff_1 := I_sup_rows(i).supp_diff_1;
               else null;
            end case;
         else
            L_isup_row.supp_diff_4 := I_sup_rows(i).supp_diff_4;
            L_isup_row.supp_diff_3 := I_sup_rows(i).supp_diff_3;
            L_isup_row.supp_diff_2 := I_sup_rows(i).supp_diff_2;
            L_isup_row.supp_diff_1 := I_sup_rows(i).supp_diff_1;
         end if;
         ---
        delete svc_item_supplier 
         where item           = I_item_rec.item_master_def.item
           and supplier       = I_sup_rows(i).supplier 
           and process$status = 'P';

         merge into svc_item_supplier sis
              using (select LP_process_id as process_id,
                            LP_chunk_id as chunk_id,
                            NVL(max(rownum+1), 1) as row_seq,
                            I_action as action,
                            LP_process_status as process_status,
                            I_item_rec.item_master_def.item as item,
                            L_isup_row.supplier as supplier,
                            L_isup_row.primary_supp_ind as primary_supp_ind,
                            L_isup_row.vpn as vpn,
                            L_isup_row.supp_label as supp_label,
                            L_isup_row.consignment_rate as consignment_rate,
                            L_isup_row.supp_diff_1 as supp_diff_1,
                            L_isup_row.supp_diff_2 as supp_diff_2,
                            L_isup_row.supp_diff_3 as supp_diff_3,
                            L_isup_row.supp_diff_4 as supp_diff_4,
                            L_isup_row.pallet_name as pallet_name,
                            L_isup_row.case_name as case_name,
                            L_isup_row.inner_name as inner_name,
                            L_isup_row.supp_discontinue_date as supp_discontinue_date,
                            L_isup_row.direct_ship_ind as direct_ship_ind,
                            L_isup_row.concession_rate as concession_rate,
                            L_isup_row.primary_case_size as primary_case_size,
                            LP_user as create_id,
                            SYSDATE as create_datetime,
                            LP_user as last_upd_id,
                            SYSDATE as last_upd_datetime
                       from svc_item_supplier
                      where process_id = LP_process_id
                        and chunk_id = LP_chunk_id) temp
                 on (temp.supplier = sis.supplier
                     and temp.item = sis.item
                     and temp.action = CORESVC_ITEM.ACTION_MOD)
               when matched then
             update
                set process_id            = temp.process_id,
                    chunk_id              = temp.chunk_id,
                    row_seq               = temp.row_seq,
                    action                = temp.action,
                    process$status        = temp.process_status,
                    consignment_rate      = temp.consignment_rate,
                    supp_diff_1           = temp.supp_diff_1,
                    supp_discontinue_date = temp.supp_discontinue_date,
                    supp_label            = temp.supp_label,
                    primary_case_size     = temp.primary_case_size,
                    inner_name            = temp.inner_name,
                    primary_supp_ind      = temp.primary_supp_ind,
                    case_name             = temp.case_name,
                    vpn                   = temp.vpn,
                    concession_rate       = temp.concession_rate,
                    supp_diff_3           = temp.supp_diff_3,
                    supp_diff_2           = temp.supp_diff_2,
                    supp_diff_4           = temp.supp_diff_4,
                    direct_ship_ind       = temp.direct_ship_ind,
                    pallet_name           = temp.pallet_name,
                    last_upd_id           = temp.last_upd_id,
                    last_upd_datetime     = temp.last_upd_datetime
               when not matched then
             insert (process_id,
                     chunk_id,
                     row_seq,
                     action,
                     process$status,
                     item,
                     supplier,
                     primary_supp_ind,
                     vpn,
                     supp_label,
                     consignment_rate,
                     supp_diff_1,
                     supp_diff_2,
                     supp_diff_3,
                     supp_diff_4,
                     pallet_name,
                     case_name,
                     inner_name,
                     supp_discontinue_date,
                     direct_ship_ind,
                     concession_rate,
                     primary_case_size,
                     create_id,
                     create_datetime,
                     last_upd_id,
                     last_upd_datetime)
             values (temp.process_id,
                     temp.chunk_id,
                     temp.row_seq,
                     temp.action,
                     temp.process_status,
                     temp.item,
                     temp.supplier,
                     temp.primary_supp_ind,
                     temp.vpn,
                     temp.supp_label,
                     temp.consignment_rate,
                     temp.supp_diff_1,
                     temp.supp_diff_2,
                     temp.supp_diff_3,
                     temp.supp_diff_4,
                     temp.pallet_name,
                     temp.case_name,
                     temp.inner_name,
                     temp.supp_discontinue_date,
                     temp.direct_ship_ind,
                     temp.concession_rate,
                     temp.primary_case_size,
                     temp.create_id,
                     temp.create_datetime,
                     temp.last_upd_id,
                     temp.last_upd_datetime);
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item and supplier',
                                            'SVC_ITEM_SUPPLIER',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISUP;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISUP(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sup_rows      IN       "RIB_XItemSupRef_TBL",
                         I_item_rec      IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_action        IN       SVC_ITEM_SUPPLIER.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISUP';

BEGIN
   -- For Delete, we only need item and supplier

   if I_sup_rows is NOT NULL and
      I_sup_rows.COUNT > 0  then
      FOR i IN I_sup_rows.FIRST..I_sup_rows.LAST LOOP
         ---
         merge into svc_item_supplier sis
              using (select LP_process_id as process_id,
                            LP_chunk_id as chunk_id,
                            NVL(max(rownum+1), 1) as row_seq,
                            I_action as action,
                            LP_process_status as process_status,
                            I_item_rec.item as item,
                            I_sup_rows(i).supplier as supplier,
                            LP_user as create_id,
                            SYSDATE as create_datetime,
                            LP_user as last_upd_id,
                            SYSDATE as last_upd_datetime
                       from svc_item_supplier
                      where process_id = LP_process_id
                        and chunk_id = LP_chunk_id) temp
                 on (temp.supplier = sis.supplier
                     and temp.item = sis.item
                     and temp.action = CORESVC_ITEM.ACTION_DEL)
               when matched then
             update
                set process_id        = temp.process_id,
                    chunk_id          = temp.chunk_id,
                    row_seq           = temp.row_seq,
                    action            = temp.action,
                    process$status    = temp.process_status,
                    last_upd_id       = temp.last_upd_id,
                    last_upd_datetime = temp.last_upd_datetime
               when not matched then
             insert (process_id,
                     chunk_id,
                     row_seq,
                     action,
                     process$status,
                     item,
                     supplier,
                     create_id,
                     create_datetime,
                     last_upd_id,
                     last_upd_datetime)
             values (temp.process_id,
                     temp.chunk_id,
                     temp.row_seq,
                     temp.action,
                     temp.process_status,
                     temp.item,
                     temp.supplier,
                     temp.create_id,
                     temp.create_datetime,
                     temp.last_upd_id,
                     temp.last_upd_datetime);
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item and supplier',
                                            'SVC_ITEM_SUPPLIER',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISUP;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISC(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_isup_rows     IN       "RIB_XItemSupDesc_TBL",
                        I_item_rec      IN       RMSSUB_XITEM.ITEM_API_REC,
                        I_message_type  IN       VARCHAR2,
                        I_action        IN       SVC_ITEM_SUPP_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISC';
   L_isup_rows              "RIB_XItemSupDesc_TBL" := I_isup_rows;
   L_isc_row                ITEM_SUPP_COUNTRY%ROWTYPE := I_item_rec.isc_def;
   L_countries              "RIB_XItemSupCtyDesc_TBL";
   L_buyer_pack             BOOLEAN := FALSE;
   L_uom_class              UOM_CLASS.UOM_CLASS%TYPE := NULL;
   L_elc_ind                SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_row_seq                 SVC_ITEM_SUPP_COUNTRY.ROW_SEQ%TYPE;
   L_dims                   "RIB_XISCDimDesc_TBL";
   L_iscd_row               ITEM_SUPP_COUNTRY_DIM%ROWTYPE := I_item_rec.iscd_def;
   L_default_tax_type       SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

   cursor C_ELC_IND is
      select elc_ind
        from system_options;

BEGIN

   if NOT SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_message,
                                                  L_default_tax_type) then
      return FALSE;
   end if;

   open C_ELC_IND;
   fetch C_ELC_IND into L_elc_ind;
   close C_ELC_IND;

   if NVL(I_item_rec.item_master_def.pack_type,'V') = 'B'     and
      NVL(I_item_rec.item_master_def.orderable_ind,'N') = 'Y' then
      L_buyer_pack := TRUE;
   end if;

   if I_isup_rows is NOT NULL and I_isup_rows.COUNT > 0  then
      FOR i IN L_isup_rows.FIRST..L_isup_rows.LAST LOOP
         L_isc_row.supplier := L_isup_rows(i).supplier;
         L_isc_row.primary_supp_ind := NVL(L_isup_rows(i).primary_supp_ind, 'N');
         L_countries := L_isup_rows(i).xitemsupctydesc_TBL;
         L_isc_row.last_update_id     := LP_user;
         if L_countries is NOT NULL and L_countries.COUNT > 0 then
            FOR j IN L_countries.FIRST..L_countries.LAST LOOP

               L_isc_row.origin_country_id := L_countries(j).origin_country_id;
               L_isc_row.primary_country_ind := NVL(L_countries(j).primary_country_ind, 'N');
               L_isc_row.supp_pack_size := NVL(L_countries(j).supp_pack_size, I_item_rec.isc_def.supp_pack_size);
               L_isc_row.inner_pack_size := NVL(L_countries(j).inner_pack_size, I_item_rec.isc_def.inner_pack_size);
               L_isc_row.ti := NVL(L_countries(j).ti, I_item_rec.isc_def.ti);
               L_isc_row.hi := NVL(L_countries(j).hi, I_item_rec.isc_def.hi);
               L_isc_row.lead_time := NVL(L_countries(j).lead_time, I_item_rec.isc_def.lead_time);
               L_isc_row.pickup_lead_time := NVL(L_countries(j).pickup_lead_time, I_item_rec.isc_def.pickup_lead_time);
               L_isc_row.max_order_qty := NVL(L_countries(j).max_order_qty, I_item_rec.isc_def.max_order_qty);
               L_isc_row.min_order_qty := NVL(L_countries(j).min_order_qty, I_item_rec.isc_def.min_order_qty);
               L_isc_row.cost_uom := NVL(L_countries(j).cost_uom,I_item_rec.item_master_def.standard_uom);
               L_isc_row.default_uop := L_countries(j).default_uop;
               L_isc_row.round_lvl :=  NVL(L_countries(j).round_lvl, I_item_rec.isc_def.round_lvl);
               L_isc_row.round_to_inner_pct :=  NVL(L_countries(j).round_to_inner_pct, I_item_rec.isc_def.round_to_inner_pct);
               L_isc_row.round_to_case_pct :=  NVL(L_countries(j).round_to_case_pct, I_item_rec.isc_def.round_to_case_pct);
               L_isc_row.round_to_layer_pct :=  NVL(L_countries(j).round_to_layer_pct, I_item_rec.isc_def.round_to_layer_pct);
               L_isc_row.round_to_pallet_pct :=  NVL(L_countries(j).round_to_pallet_pct, I_item_rec.isc_def.round_to_pallet_pct);
               L_isc_row.packing_method :=  L_countries(j).packing_method;
               L_isc_row.supp_hier_lvl_1 := L_countries(j).supp_hier_lvl_1;
               L_isc_row.supp_hier_lvl_2 := L_countries(j).supp_hier_lvl_2;
               L_isc_row.supp_hier_lvl_3 := L_countries(j).supp_hier_lvl_3;
               L_isc_row.supp_hier_type_1 :=  L_countries(j).supp_hier_type_1;
               L_isc_row.supp_hier_type_2 :=  L_countries(j).supp_hier_type_2;
               L_isc_row.supp_hier_type_3 :=  L_countries(j).supp_hier_type_3;

               if I_item_rec.item_master_def.catch_weight_ind = 'Y' then
                  if I_item_rec.item_master_def.simple_pack_ind = 'Y' then
                     if I_item_rec.item_master_def.order_type = 'V' then
                        if L_countries(j).tolerance_type is NULL or
                           L_countries(j).max_tolerance is NULL or
                           L_countries(j).min_tolerance is NULL  then
                           O_error_message := SQL_LIB.CREATE_MSG('TOLR_MUST_ENTER','Item: '||I_item_rec.item_master_def.item, 'Message Type: '||I_message_type);
                           return FALSE;
                        end if;
                        if UOM_SQL.GET_CLASS(O_error_message,
                                             L_uom_class,
                                             L_countries(j).cost_uom) = FALSE then
                           return FALSE;
                        end if;
                        if L_uom_class != 'MASS' then
                           O_error_message := SQL_LIB.CREATE_MSG('INVALID_COST_UOM','UOM Class: '||L_uom_class, 'Cost UOM: '||L_countries(j).cost_uom);
                           return FALSE;
                        end if;
                     end if;
                  else
                     if L_countries(j).cost_uom != I_item_rec.item_master_def.standard_uom then
                        if UOM_SQL.GET_CLASS(O_error_message,
                                             L_uom_class,
                                             L_countries(j).cost_uom) = FALSE then
                           return FALSE;
                        end if;
                        if L_uom_class != 'MASS' then
                           O_error_message := SQL_LIB.CREATE_MSG('ITEM_CANNOT_BE_CW','UOM Class: '||L_uom_class, 'Cost UOM: '||L_countries(j).cost_uom);
                           return FALSE;
                        end if;
                     end if;
                  end if;
               end if;
               L_isc_row.tolerance_type := L_countries(j).tolerance_type;
               L_isc_row.max_tolerance := L_countries(j).max_tolerance;
               L_isc_row.min_tolerance := L_countries(j).min_tolerance;

               if L_default_tax_type = 'GTAX' then
                  if L_buyer_pack then
                     L_isc_row.unit_cost             := 0;
                     L_isc_row.extended_base_cost    := 0;
                     L_isc_row.inclusive_cost        := 0;
                     L_isc_row.negotiated_item_cost  := 0;
                     L_isc_row.base_cost             := 0;
                  else
                     L_isc_row.unit_cost              := L_countries(j).unit_cost;
                     L_isc_row.extended_base_cost     := L_countries(j).unit_cost;
                     L_isc_row.inclusive_cost         := L_countries(j).unit_cost;
                     L_isc_row.negotiated_item_cost   := L_countries(j).unit_cost;
                     L_isc_row.base_cost              := L_countries(j).unit_cost;
                  end if;
               else
                  L_isc_row.unit_cost              := L_countries(j).unit_cost;
                  L_isc_row.extended_base_cost     := NULL;
                  L_isc_row.inclusive_cost         := NULL;
                  L_isc_row.negotiated_item_cost   := NULL;
                  L_isc_row.base_cost              := NULL;
               end if;

               select NVL(max(rownum+1), 1)
                 into L_row_seq
                 from svc_item_supp_country
                where process_id = LP_process_id
                  and chunk_id = LP_chunk_id;
                   
            delete svc_item_supp_country 
             where item                = I_item_rec.item_master_def.item 
               and supplier            = L_isc_row.supplier 
               and origin_country_id   = L_isc_row.origin_country_id 
               and process$status = 'P';
       
               BEGIN
                  merge into svc_item_supp_country sisc
                       using (select LP_process_id as process_id,
                                     LP_chunk_id as chunk_id,
                                     L_row_seq as row_seq,
                                     I_action as action,
                                     LP_process_status as process_status,
                                     I_item_rec.item_master_def.item as item,
                                     L_isc_row.supplier as supplier,
                                     L_isc_row.origin_country_id as origin_country_id,
                                     L_isc_row.unit_cost as unit_cost,
                                     L_isc_row.lead_time as lead_time,
                                     L_isc_row.pickup_lead_time as pickup_lead_time,
                                     L_isc_row.supp_pack_size as supp_pack_size,
                                     L_isc_row.inner_pack_size as inner_pack_size,
                                     L_isc_row.round_lvl as round_lvl,
                                     L_isc_row.round_to_inner_pct as round_to_inner_pct,
                                     L_isc_row.round_to_case_pct as round_to_case_pct,
                                     L_isc_row.round_to_layer_pct as round_to_layer_pct,
                                     L_isc_row.round_to_pallet_pct as round_to_pallet_pct,
                                     L_isc_row.min_order_qty as min_order_qty,
                                     L_isc_row.max_order_qty as max_order_qty,
                                     L_isc_row.packing_method as packing_method,
                                     L_isc_row.primary_supp_ind as primary_supp_ind,
                                     L_isc_row.primary_country_ind as primary_country_ind,
                                     L_isc_row.default_uop as default_uop,
                                     L_isc_row.ti as ti,
                                     L_isc_row.hi as hi,
                                     DECODE(L_isc_row.supp_hier_type_1,NULL,DECODE(L_isc_row.supp_hier_lvl_1,NULL,NULL,'S1'),L_isc_row.supp_hier_type_1) as supp_hier_type_1,
                                     L_isc_row.supp_hier_lvl_1 as supp_hier_lvl_1,
                                     DECODE(L_isc_row.supp_hier_type_2,NULL,DECODE(L_isc_row.supp_hier_lvl_2,NULL,NULL,'S2'),L_isc_row.supp_hier_type_2) as supp_hier_type_2,
                                     L_isc_row.supp_hier_lvl_2 as supp_hier_lvl_2,
                                     DECODE(L_isc_row.supp_hier_type_3,NULL,DECODE(L_isc_row.supp_hier_lvl_1,NULL,NULL,'S3'),L_isc_row.supp_hier_type_3) as supp_hier_type_3,
                                     L_isc_row.supp_hier_lvl_3 as supp_hier_lvl_3,
                                     L_isc_row.cost_uom as cost_uom,
                                     L_isc_row.tolerance_type as tolerance_type,
                                     L_isc_row.max_tolerance as max_tolerance,
                                     L_isc_row.min_tolerance as min_tolerance,
                                     L_isc_row.negotiated_item_cost as negotiated_item_cost,
                                     L_isc_row.extended_base_cost as extended_base_cost,
                                     L_isc_row.inclusive_cost as inclusive_cost,
                                     L_isc_row.base_cost as base_cost,
                                     LP_user as create_id,
                                     SYSDATE as create_datetime,
                                     LP_user as last_upd_id,
                                     SYSDATE as last_upd_datetime
                                from dual) temp
                          on (temp.origin_country_id = sisc.origin_country_id
                              and temp.supplier = sisc.supplier
                              and temp.item = sisc.item
                              and temp.action = CORESVC_ITEM.ACTION_MOD)
                        when matched then
                      update
                         set process_id           = temp.process_id,
                             chunk_id             = temp.chunk_id,
                             row_seq              = temp.row_seq,
                             action               = temp.action,
                             process$status       = temp.process_status,
                             round_lvl            = temp.round_lvl,
                             supp_hier_lvl_2      = temp.supp_hier_lvl_2,
                             inner_pack_size      = temp.inner_pack_size,
                             round_to_pallet_pct  = temp.round_to_pallet_pct,
                             hi                   = temp.hi,
                             primary_supp_ind     = temp.primary_supp_ind,
                             ti                   = temp.ti,
                             round_to_case_pct    = temp.round_to_case_pct,
                             max_tolerance        = temp.max_tolerance,
                             tolerance_type       = temp.tolerance_type,
                             packing_method       = temp.packing_method,
                             max_order_qty        = temp.max_order_qty,
                             unit_cost            = temp.unit_cost,
                             cost_uom             = temp.cost_uom,
                             round_to_layer_pct   = temp.round_to_layer_pct,
                             supp_hier_lvl_3      = temp.supp_hier_lvl_3,
                             pickup_lead_time     = temp.pickup_lead_time,
                             default_uop          = temp.default_uop,
                             primary_country_ind  = temp.primary_country_ind,
                             lead_time            = temp.lead_time,
                             min_order_qty        = temp.min_order_qty,
                             supp_hier_lvl_1      = temp.supp_hier_lvl_1,
                             round_to_inner_pct   = temp.round_to_inner_pct,
                             supp_pack_size       = temp.supp_pack_size,
                             min_tolerance        = temp.min_tolerance,
                             last_upd_id          = temp.last_upd_id,
                             last_upd_datetime    = temp.last_upd_datetime
                        when not matched then
                      insert (process_id,
                              chunk_id,
                              row_seq,
                              action,
                              process$status,
                              item,
                              supplier,
                              origin_country_id,
                              unit_cost,
                              lead_time,
                              pickup_lead_time,
                              supp_pack_size,
                              inner_pack_size,
                              round_lvl,
                              round_to_inner_pct,
                              round_to_case_pct,
                              round_to_layer_pct,
                              round_to_pallet_pct,
                              min_order_qty,
                              max_order_qty,
                              packing_method,
                              primary_supp_ind,
                              primary_country_ind,
                              default_uop,
                              ti,
                              hi,
                              supp_hier_type_1,
                              supp_hier_lvl_1,
                              supp_hier_type_2,
                              supp_hier_lvl_2,
                              supp_hier_type_3,
                              supp_hier_lvl_3,
                              cost_uom,
                              tolerance_type,
                              max_tolerance,
                              min_tolerance,
                              negotiated_item_cost,
                              extended_base_cost,
                              inclusive_cost,
                              base_cost,
                              create_id,
                              create_datetime,
                              last_upd_id,
                              last_upd_datetime)
                      values (temp.process_id,
                              temp.chunk_id,
                              temp.row_seq,
                              temp.action,
                              temp.process_status,
                              temp.item,
                              temp.supplier,
                              temp.origin_country_id,
                              temp.unit_cost,
                              temp.lead_time,
                              temp.pickup_lead_time,
                              temp.supp_pack_size,
                              temp.inner_pack_size,
                              temp.round_lvl,
                              temp.round_to_inner_pct,
                              temp.round_to_case_pct,
                              temp.round_to_layer_pct,
                              temp.round_to_pallet_pct,
                              temp.min_order_qty,
                              temp.max_order_qty,
                              temp.packing_method,
                              temp.primary_supp_ind,
                              temp.primary_country_ind,
                              temp.default_uop,
                              temp.ti,
                              temp.hi,
                              temp.supp_hier_type_1,
                              temp.supp_hier_lvl_1,
                              temp.supp_hier_type_2,
                              temp.supp_hier_lvl_2,
                              temp.supp_hier_type_3,
                              temp.supp_hier_lvl_3,
                              temp.cost_uom,
                              temp.tolerance_type,
                              temp.max_tolerance,
                              temp.min_tolerance,
                              temp.negotiated_item_cost,
                              temp.extended_base_cost,
                              temp.inclusive_cost,
                              temp.base_cost,
                              temp.create_id,
                              temp.create_datetime,
                              temp.last_upd_id,
                              temp.last_upd_datetime);

               EXCEPTION
                  when DUP_VAL_ON_INDEX then
                     O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                           'item, supplier and origin_country_id',
                                                           'SVC_ITEM_SUPP_COUNTRY',
                                                           NULL);
                     return FALSE;
               END;

               -- insert into svc_item_supp_country_dim
               if L_countries(j).xiscdimdesc_TBL is NOT NULL and L_countries(j).xiscdimdesc_TBL.COUNT > 0 then
                  L_dims                    := L_countries(j).xiscdimdesc_TBL;
                  L_iscd_row.origin_country := L_countries(j).origin_country_id;
                  L_iscd_row.item           := I_item_rec.item_master_def.item;
                  L_iscd_row.supplier       := L_isup_rows(i).supplier;
                  L_iscd_row.last_update_id := LP_user;

                  if L_dims.COUNT = 0 and
                     I_item_rec.item_master_def.catch_weight_ind = 'Y' and
                     I_item_rec.item_master_def.simple_pack_ind  = 'Y' and
                     I_item_rec.item_master_def.order_type = 'V' then
                     O_error_message := SQL_LIB.CREATE_MSG('DIM_MUST_ENTER','Item: '||I_item_rec.item_master_def.item, 'Message Type: '||I_message_type);
                     return FALSE;
                  end if;

                  if L_dims is NOT NULL and L_dims.COUNT > 0 then
                     FOR k in L_dims.FIRST..L_dims.LAST LOOP
                        L_iscd_row.dim_object          := L_dims(k).dim_object;
                        L_iscd_row.tare_weight         := L_dims(k).tare_weight;
                        L_iscd_row.tare_type           := L_dims(k).tare_type;
                        L_iscd_row.lwh_uom             := L_dims(k).lwh_uom;
                        L_iscd_row.length              := L_dims(k).length;
                        L_iscd_row.width               := L_dims(k).width;
                        L_iscd_row.height              := L_dims(k).dim_height;
                        L_iscd_row.liquid_volume       := L_dims(k).liquid_volume;
                        L_iscd_row.liquid_volume_uom   := L_dims(k).liquid_volume_uom;
                        L_iscd_row.stat_cube           := L_dims(k).stat_cube;
                        L_iscd_row.weight_uom          := L_dims(k).weight_uom;
                        L_iscd_row.weight              := L_dims(k).weight;
                        L_iscd_row.net_weight          := L_dims(k).net_weight;
                        L_iscd_row.presentation_method := L_dims(k).presentation_method;
                        ---
                        BEGIN
                        delete from svc_item_supp_country_dim 
                              where item                = I_item_rec.item_master_def.item 
                                and supplier            = L_iscd_row.supplier 
                                and origin_country      = L_iscd_row.origin_country 
                                and process$status      = 'P';
                           merge into svc_item_supp_country_dim iscd
                                using (select LP_process_id as process_id,
                                              LP_chunk_id as chunk_id,
                                              NVL(max(rownum+1), 1) as row_seq,
                                              I_action as action,
                                              LP_process_status as process_status,
                                              L_iscd_row.item as item,
                                              L_iscd_row.supplier as supplier,
                                              L_iscd_row.origin_country as origin_country,
                                              L_iscd_row.dim_object as dim_object,
                                              L_iscd_row.presentation_method as presentation_method,
                                              L_iscd_row.length as length,
                                              L_iscd_row.width as width,
                                              L_iscd_row.height as height,
                                              L_iscd_row.lwh_uom as lwh_uom,
                                              L_iscd_row.weight as weight,
                                              L_iscd_row.net_weight as net_weight,
                                              L_iscd_row.weight_uom as weight_uom,
                                              L_iscd_row.liquid_volume as liquid_volume,
                                              L_iscd_row.liquid_volume_uom as liquid_volume_uom,
                                              L_iscd_row.stat_cube as stat_cube,
                                              L_iscd_row.tare_weight as tare_weight,
                                              L_iscd_row.tare_type as tare_type,
                                              LP_user as create_id,
                                              SYSDATE as create_datetime,
                                              LP_user as last_upd_id,
                                              SYSDATE as last_upd_datetime
                                         from svc_item_supp_country_dim
                                        where process_id = LP_process_id
                                          and chunk_id = LP_chunk_id) temp
                                   on (temp.dim_object = iscd.dim_object
                                       and temp.origin_country = iscd.origin_country
                                       and temp.supplier = iscd.supplier
                                       and temp.item = iscd.item
                                       and temp.action = CORESVC_ITEM.ACTION_MOD)
                                 when matched then
                               update
                                  set process_id          = temp.process_id,
                                      chunk_id            = temp.chunk_id,
                                      row_seq             = temp.row_seq,
                                      action              = temp.action,
                                      process$status      = temp.process_status,
                                      weight              = temp.weight,
                                      height              = temp.height,
                                      weight_uom          = temp.weight_uom,
                                      stat_cube           = temp.stat_cube,
                                      tare_weight         = temp.tare_weight,
                                      presentation_method = temp.presentation_method,
                                      liquid_volume_uom   = temp.liquid_volume_uom,
                                      length              = temp.length,
                                      liquid_volume       = temp.liquid_volume,
                                      width               = temp.width,
                                      lwh_uom             = temp.lwh_uom,
                                      tare_type           = temp.tare_type,
                                      net_weight          = temp.net_weight,
                                      last_upd_id         = temp.last_upd_id,
                                      last_upd_datetime   = temp.last_upd_datetime
                                 when not matched then
                               insert (process_id,
                                       chunk_id,
                                       row_seq,
                                       action,
                                       process$status,
                                       item,
                                       supplier,
                                       origin_country,
                                       dim_object,
                                       presentation_method,
                                       length,
                                       width,
                                       height,
                                       lwh_uom,
                                       weight,
                                       net_weight,
                                       weight_uom,
                                       liquid_volume,
                                       liquid_volume_uom,
                                       stat_cube,
                                       tare_weight,
                                       tare_type,
                                       create_id,
                                       create_datetime,
                                       last_upd_id,
                                       last_upd_datetime)
                               values (temp.process_id,
                                       temp.chunk_id,
                                       temp.row_seq,
                                       temp.action,
                                       temp.process_status,
                                       temp.item,
                                       temp.supplier,
                                       temp.origin_country,
                                       temp.dim_object,
                                       temp.presentation_method,
                                       temp.length,
                                       temp.width,
                                       temp.height,
                                       temp.lwh_uom,
                                       temp.weight,
                                       temp.net_weight,
                                       temp.weight_uom,
                                       temp.liquid_volume,
                                       temp.liquid_volume_uom,
                                       temp.stat_cube,
                                       temp.tare_weight,
                                       temp.tare_type,
                                       temp.create_id,
                                       temp.create_datetime,
                                       temp.last_upd_id,
                                       temp.last_upd_datetime);
                        EXCEPTION
                           when DUP_VAL_ON_INDEX then
                              O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                                    'item, supplier, origin country and dim object',
                                                                    'SVC_ITEM_SUPP_COUNTRY_DIM',
                                                                    NULL);
                              return FALSE;
                        END;
                     END LOOP; -- dimensions
                  end if;
               end if;
            END LOOP; -- countries
         end if;
      END LOOP; -- suppliers
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISC;

-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISCL(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item_rec      IN       RMSSUB_XITEM.ITEM_API_REC,
                         I_hier_level    IN       VARCHAR2,
                         I_message_type  IN       VARCHAR2,
                         I_action        IN       SVC_ITEM_SUPP_COUNTRY_LOC.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISCL';
   
   L_iscl_row               ITEM_SUPP_COUNTRY_LOC%ROWTYPE := I_item_rec.iscl_def;
   L_iscl_tbl               RMSSUB_XITEM.ISCL_TBLTYPE := I_item_rec.iscl_tbl;
   L_user                   USER_ATTRIB.USER_ID%TYPE := user;
   L_vdate                  PERIOD.VDATE%TYPE := GET_VDATE;
   L_cur_sup                SUPS.SUPPLIER%TYPE;
   L_cur_cty                COUNTRY.COUNTRY_ID%TYPE;
   L_old_prim_loc           ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_new_loc                ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_loc_type               ITEM_SUPP_COUNTRY_LOC.LOC_TYPE%TYPE;
   L_item_cost_head_rec     ITEM_COST_HEAD%ROWTYPE;
   L_add_1                  ADDR.ADD_1%TYPE;
   L_add_2                  ADDR.ADD_2%TYPE;
   L_add_3                  ADDR.ADD_3%TYPE;
   L_city                   ADDR.CITY%TYPE;
   L_state                  ADDR.STATE%TYPE;
   L_dlvry_country_id       ADDR.COUNTRY_ID%TYPE;
   L_post                   ADDR.POST%TYPE;
   L_module                 ADDR.MODULE%TYPE;
   L_key_value_1            ADDR.KEY_VALUE_1%TYPE ;
   L_key_value_2            ADDR.KEY_VALUE_2%TYPE := NULL;
   L_exists                 BOOLEAN;
   L_store_row              STORE%ROWTYPE;
   L_base_cost              ITEM_COST_HEAD.BASE_COST%TYPE;
   L_extended_base_cost     ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost         ITEM_COST_HEAD.INCLUSIVE_COST%TYPE;
   L_negotiated_item_cost   ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE;
   L_comp_item_cost_rec     OBJ_COMP_ITEM_COST_REC := OBJ_COMP_ITEM_COST_REC();
   L_comp_item_cost_tbl     OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();
   L_country_attrib_row     COUNTRY_ATTRIB%ROWTYPE;
   L_key_value_1_temp       ADDR.KEY_VALUE_1%TYPE;
   L_row_seq                 SVC_ITEM_SUPP_COUNTRY_LOC.ROW_SEQ%TYPE;

   cursor C_GET_PHYSICAL_WH is
      select TO_CHAR(w.physical_wh)
        from wh w
       where TO_CHAR(w.wh) = L_key_value_1_temp;


BEGIN

   if L_iscl_tbl is NOT NULL AND L_iscl_tbl.COUNT > 0  THEN

      L_iscl_row.item := I_item_rec.item_master_def.item;
      if I_hier_level = 'W' then
         L_iscl_row.loc_type := 'W';
     else
         L_iscl_row.loc_type := 'S';
    end if;
      L_iscl_row.last_update_id      := L_user;

      FOR i IN L_iscl_tbl.FIRST..L_iscl_tbl.LAST LOOP

         L_iscl_row.supplier           := L_iscl_tbl(i).supplier;
         L_iscl_row.origin_country_id  := L_iscl_tbl(i).country;
         L_iscl_row.pickup_lead_time   := L_iscl_tbl(i).pickup_lead_time;
         L_iscl_row.round_lvl          := NVL(L_iscl_tbl(i).round_lvl,I_item_rec.iscl_def.round_lvl);
         L_iscl_row.round_to_inner_pct  := NVL(L_iscl_tbl(i).round_to_inner_pct,I_item_rec.iscl_def.round_to_inner_pct);
         L_iscl_row.round_to_case_pct   := NVL(L_iscl_tbl(i).round_to_case_pct,I_item_rec.iscl_def.round_to_case_pct);
         L_iscl_row.round_to_layer_pct  := NVL(L_iscl_tbl(i).round_to_layer_pct,I_item_rec.iscl_def.round_to_layer_pct);
         L_iscl_row.round_to_pallet_pct := NVL(L_iscl_tbl(i).round_to_pallet_pct,I_item_rec.iscl_def.round_to_pallet_pct);
         L_iscl_row.supp_hier_lvl_1 := L_iscl_tbl(i).supp_hier_lvl_1;

         if L_iscl_tbl(i).supp_hier_lvl_1 is NOT NULL then
            L_iscl_row.supp_hier_type_1 := 'S1';
         else
            L_iscl_row.supp_hier_type_1 := NULL;
         end if;
         L_iscl_row.supp_hier_lvl_2 := L_iscl_tbl(i).supp_hier_lvl_2;
         if L_iscl_tbl(i).supp_hier_lvl_2 is NOT NULL then
            L_iscl_row.supp_hier_type_2 := 'S2';
         else
            L_iscl_row.supp_hier_type_2 := NULL;
         end if;
         L_iscl_row.supp_hier_lvl_3 := L_iscl_tbl(i).supp_hier_lvl_3;
         if L_iscl_tbl(i).supp_hier_lvl_3 is NOT NULL then
            L_iscl_row.supp_hier_type_3 := 'S3';
         else
            L_iscl_row.supp_hier_type_3 := NULL;
         end if;

   
          FOR k in L_iscl_tbl(i).loc_cost.FIRST..L_iscl_tbl(i).loc_cost.LAST LOOP
             if  I_hier_level NOT IN ('S','W') then
                L_iscl_row.primary_loc_ind   := 'N';
                L_iscl_row.loc := L_iscl_tbl(i).loc_cost(k).location;
                L_iscl_row.base_cost               := L_iscl_tbl(i).loc_cost(k).base_cost;
                L_iscl_row.negotiated_item_cost    := L_iscl_tbl(i).loc_cost(k).negotiated_item_cost;
                L_iscl_row.extended_base_cost      := L_iscl_tbl(i).loc_cost(k).extended_base_cost;
                L_iscl_row.inclusive_cost          := L_iscl_tbl(i).loc_cost(k).inclusive_cost;
                L_iscl_row.unit_cost               := L_iscl_tbl(i).loc_cost(k).unit_cost; 
            
            else  --  S or W
               L_iscl_row.primary_loc_ind  := NVL(L_iscl_tbl(i).primary_loc_ind,'N');
               L_iscl_row.loc := L_iscl_tbl(i).locs(1);
               L_iscl_row.base_cost               := L_iscl_tbl(i).loc_cost(1).base_cost;
               L_iscl_row.negotiated_item_cost    := L_iscl_tbl(i).loc_cost(1).negotiated_item_cost;
               L_iscl_row.extended_base_cost      := L_iscl_tbl(i).loc_cost(1).extended_base_cost;
               L_iscl_row.inclusive_cost          := L_iscl_tbl(i).loc_cost(1).inclusive_cost;
               L_iscl_row.unit_cost               := L_iscl_tbl(i).loc_cost(1).unit_cost; 
           end if;

          
               select NVL(max(rownum+1), 1)
                 into L_row_seq
                 from svc_item_supp_country_loc
                where process_id = LP_process_id
                  and chunk_id = LP_chunk_id;
                  
               BEGIN
              
              delete from svc_item_supp_country_loc 
                    where item                = I_item_rec.item_master_def.item 
                      and supplier            = L_iscl_row.supplier 
                      and origin_country_id   = L_iscl_row.origin_country_id
                      and loc                 = L_iscl_row.loc
                      and process$status      = 'P';
             
                  merge into svc_item_supp_country_loc siscl
                       using (select LP_process_id as process_id,
                                     LP_chunk_id as chunk_id,
                                     L_row_seq as row_seq,
                                     I_action as action,
                                     LP_process_status as process_status,
                                     I_item_rec.item_master_def.item as item,
                                     L_iscl_row.supplier as supplier,
                                     L_iscl_row.origin_country_id as origin_country_id,
                                     L_iscl_row.loc as loc,
                                     L_iscl_row.loc_type as loc_type,
                                     L_iscl_row.primary_loc_ind as primary_loc_ind,
                                     L_iscl_row.unit_cost as unit_cost,
                                     L_iscl_row.round_lvl as round_lvl,
                                     L_iscl_row.round_to_inner_pct as round_to_inner_pct,
                                     L_iscl_row.round_to_case_pct as round_to_case_pct,
                                     L_iscl_row.round_to_layer_pct as round_to_layer_pct,
                                     L_iscl_row.round_to_pallet_pct as round_to_pallet_pct,
                                     L_iscl_row.supp_hier_type_1 as supp_hier_type_1,
                                     L_iscl_row.supp_hier_lvl_1 as supp_hier_lvl_1,
                                     L_iscl_row.supp_hier_type_2 as supp_hier_type_2,
                                     L_iscl_row.supp_hier_lvl_2 as supp_hier_lvl_2,
                                     L_iscl_row.supp_hier_type_3 as supp_hier_type_3,
                                     L_iscl_row.supp_hier_lvl_3 as supp_hier_lvl_3,
                                     L_iscl_row.pickup_lead_time as pickup_lead_time,
                                     SYSDATE as create_datetime,
                                     SYSDATE as last_upd_datetime,
                                     USER as last_upd_id,
                                     L_iscl_row.negotiated_item_cost as negotiated_item_cost,
                                     L_iscl_row.extended_base_cost as extended_base_cost,
                                     L_iscl_row.inclusive_cost as inclusive_cost,
                                     L_iscl_row.base_cost as base_cost,
                                     USER as create_id
                                    from dual) temp
                          on (temp.origin_country_id = siscl.origin_country_id
                              and temp.supplier = siscl.supplier
                              and temp.item = siscl.item
                              and temp.loc = siscl.loc
                              and temp.action = CORESVC_ITEM.ACTION_MOD)
                        when matched then
                      update
                         set process_id           = temp.process_id,
                             chunk_id             = temp.chunk_id,
                             row_seq              = temp.row_seq,
                             action               = temp.action,
                             process$status       = temp.process_status,
                             round_lvl            = temp.round_lvl,
                             round_to_inner_pct   = temp.round_to_inner_pct,
                             round_to_pallet_pct  = temp.round_to_pallet_pct,
                             round_to_case_pct    = temp.round_to_case_pct,
                             round_to_layer_pct   = temp.round_to_layer_pct,
                             supp_hier_lvl_1      = temp.supp_hier_lvl_1,
                             supp_hier_type_1     = temp.supp_hier_type_1,                    
                             supp_hier_lvl_2      = temp.supp_hier_lvl_2,
                             supp_hier_type_2     = temp.supp_hier_type_2,
                             supp_hier_lvl_3      = temp.supp_hier_lvl_3,
                             supp_hier_type_3     = temp.supp_hier_type_3,
                             pickup_lead_time     = temp.pickup_lead_time,
                             last_upd_id          = temp.last_upd_id,
                             last_upd_datetime    = temp.last_upd_datetime
                        when not matched then
                      insert (process_id,
                              chunk_id,
                              row_seq,
                              action,
                              process$status,
                              item,
                              supplier,
                              origin_country_id,
                              loc,
                              loc_type,
                              primary_loc_ind,
                              unit_cost,
                              round_lvl,
                              round_to_inner_pct,
                              round_to_case_pct,
                              round_to_layer_pct,
                              round_to_pallet_pct,
                              supp_hier_type_1,
                              supp_hier_lvl_1,
                              supp_hier_type_2,
                              supp_hier_lvl_2,
                              supp_hier_type_3,
                              supp_hier_lvl_3,
                              pickup_lead_time,
                              create_datetime,
                              last_upd_datetime,
                              last_upd_id,
                              negotiated_item_cost,
                              extended_base_cost,
                              inclusive_cost,
                              base_cost,
                              create_id
                              )
                      values (temp.process_id,
                              temp.chunk_id,
                              temp.row_seq,
                              temp.action,
                              temp.process_status,
                              temp.item,
                              temp.supplier,
                              temp.origin_country_id,
                              temp.loc,
                              temp.loc_type,
                              temp.primary_loc_ind,
                              temp.unit_cost,
                              temp.round_lvl,
                              temp.round_to_inner_pct,
                              temp.round_to_case_pct,
                              temp.round_to_layer_pct,
                              temp.round_to_pallet_pct,
                              temp.supp_hier_type_1,
                              temp.supp_hier_lvl_1,
                              temp.supp_hier_type_2,
                              temp.supp_hier_lvl_2,
                              temp.supp_hier_type_3,
                              temp.supp_hier_lvl_3,
                              temp.pickup_lead_time,
                              temp.create_datetime,
                              temp.last_upd_datetime,
                              temp.last_upd_id,
                              temp.negotiated_item_cost,
                              temp.extended_base_cost,
                              temp.inclusive_cost,
                              temp.base_cost,
                              temp.create_id
                               );

               EXCEPTION
                  when DUP_VAL_ON_INDEX then
                     O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                           'item, supplier,origin_country_id,loc',
                                                           'SVC_ITEM_SUPP_COUNTRY_LOC',
                                                           NULL);
                     return FALSE;
              END;
           END LOOP; --Locations
      END LOOP;     
        
   end if;  -- message type

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISCL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISC(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_isup_rows     IN       "RIB_XItemSupRef_TBL",
                        I_item_rec      IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                        I_action        IN       SVC_ITEM_SUPP_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISC';
   L_countries    "RIB_XItemSupCtyRef_TBL";

BEGIN
   if I_isup_rows is NOT NULL and I_isup_rows.COUNT > 0 then
      FOR i IN I_isup_rows.FIRST..I_isup_rows.LAST LOOP
         L_countries := I_isup_rows(i).xitemsupctyref_TBL;
         if L_countries is NOT NULL and L_countries.COUNT > 0 then
            FOR j IN L_countries.FIRST..L_countries.LAST LOOP
               merge into svc_item_supp_country sisc
                    using (select LP_process_id as process_id,
                                  LP_chunk_id as chunk_id,
                                  NVL(max(rownum+1), 1) as row_seq,
                                  I_action as action,
                                  LP_process_status as process_status,
                                  I_item_rec.item as item,
                                  I_isup_rows(i).supplier as supplier,
                                  L_countries(j).origin_country_id as origin_country_id,
                                  LP_user as create_id,
                                  SYSDATE as create_datetime,
                                  LP_user as last_upd_id,
                                  SYSDATE as last_upd_datetime
                             from svc_item_supp_country
                            where process_id = LP_process_id
                              and chunk_id = LP_chunk_id) temp
                       on (temp.origin_country_id = sisc.origin_country_id
                           and temp.supplier = sisc.supplier
                           and temp.item = sisc.item
                           and temp.action = CORESVC_ITEM.ACTION_DEL)
                     when matched then
                   update
                      set process_id = temp.process_id,
                          chunk_id = temp.chunk_id,
                          row_seq = temp.row_seq,
                          action = temp.action,
                          process$status = temp.process_status,
                          last_upd_id = temp.last_upd_id,
                          last_upd_datetime = temp.last_upd_datetime
                     when not matched then
                   insert (process_id,
                           chunk_id,
                           row_seq,
                           action,
                           process$status,
                           item,
                           supplier,
                           origin_country_id,
                           create_id,
                           create_datetime,
                           last_upd_id,
                           last_upd_datetime)
                   values (temp.process_id,
                           temp.chunk_id,
                           temp.row_seq,
                           temp.action,
                           temp.process_status,
                           temp.item,
                           temp.supplier,
                           temp.origin_country_id,
                           temp.create_id,
                           temp.create_datetime,
                           temp.last_upd_id,
                           temp.last_upd_datetime);

            END LOOP;
         end if;
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, supplier and origin country',
                                            'SVC_ITEM_SUPP_COUNTRY',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISC;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_hier_level      IN       VARCHAR2,
                         I_action          IN       SVC_ITEM_SUPP_COUNTRY_LOC.ACTION%TYPE)
  RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISCL';
   L_iscl_tbl     RMSSUB_XITEM.ISCL_DEL_TBLTYPE := I_item_rec.iscl_tbl;
   L_row_seq      SVC_ITEM_SUPP_COUNTRY_LOC.ROW_SEQ%TYPE;

BEGIN

        select NVL(max(rownum+1), 1)
                 into L_row_seq
                 from svc_item_supp_country_loc
                where process_id = LP_process_id
                  and chunk_id = LP_chunk_id;
                  
   if L_iscl_tbl is NOT NULL AND L_iscl_tbl.COUNT > 0  then
      FOR i IN L_iscl_tbl.FIRST..L_iscl_tbl.LAST LOOP
           merge into svc_item_supp_country_loc siscl
                    using (select LP_process_id as process_id,
                                  LP_chunk_id as chunk_id,
                                  L_row_seq as row_seq,
                                  I_action as action,
                                  LP_process_status as process_status,
                                  I_item_rec.item as item,
                                  column_value  as loc,
                                  decode(I_hier_level,'W','W','S') as loc_type,
                                  L_iscl_tbl(i).supplier as supplier,
                                  L_iscl_tbl(i).country as origin_country_id,
                                  USER as create_id,
                                  SYSDATE as create_datetime,
                                  USER as last_upd_id,
                                  SYSDATE as last_upd_datetime
                             from TABLE(cast(L_iscl_tbl(i).locs as LOC_TBL))) temp
                               on( temp.loc=siscl.loc
                              and temp.origin_country_id = siscl.origin_country_id
                              and temp.supplier = siscl.supplier
                              and temp.item = siscl.item
                              and temp.action = CORESVC_ITEM.ACTION_DEL)
                     when matched then
                   update
                      set process_id = temp.process_id,
                          chunk_id = temp.chunk_id,
                          row_seq = temp.row_seq,
                          action = temp.action,
                          process$status = temp.process_status,
                          last_upd_id = temp.last_upd_id,
                          last_upd_datetime = temp.last_upd_datetime
                     when not matched then
                   insert (process_id,
                           chunk_id,
                           row_seq,
                           action,
                           process$status,
                           item,
                           supplier,
                           origin_country_id,
                           loc,
                           loc_type,
                           create_id,
                           create_datetime,
                           last_upd_id,
                           last_upd_datetime)
                   values (temp.process_id,
                           temp.chunk_id,
                           temp.row_seq,
                           temp.action,
                           temp.process_status,
                           temp.item,
                           temp.supplier,
                           temp.origin_country_id,
                           temp.loc,
                           temp.loc_type,
                           temp.create_id,
                           temp.create_datetime,
                           temp.last_upd_id,
                           temp.last_upd_datetime);
 

         
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, supplier and origin country,loc',
                                            'SVC_ITEM_SUPP_COUNTRY_LOC',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISCL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISMC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_isup_rows       IN       "RIB_XItemSupDesc_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                         I_action          IN       SVC_ITEM_SUPP_MANU_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISMC';
   L_ismc_row     ITEM_SUPP_MANU_COUNTRY%ROWTYPE := I_item_rec.ismc_def;
   L_countries    "RIB_XItmSupCtyMfrDesc_TBL";

BEGIN
   if I_isup_rows is NOT NULL and I_isup_rows.COUNT > 0  then
     FOR i IN I_isup_rows.FIRST..I_isup_rows.LAST LOOP
        L_ismc_row.supplier := I_isup_rows(i).supplier;
        L_countries := I_isup_rows(i).XItmSupCtyMfrDesc_TBL;
        --
        if L_countries is NOT NULL and L_countries.COUNT > 0 then
           FOR j IN L_countries.FIRST..L_countries.LAST LOOP
              L_ismc_row.manu_country_id       := L_countries(j).manufacturer_ctry_id;
              L_ismc_row.primary_manu_ctry_ind := NVL(L_countries(j).primary_manufacturer_ctry_ind, 'N');
              delete from svc_item_supp_manu_country 
                    where item                = I_item_rec.item_master_def.item 
                      and supplier            = L_ismc_row.supplier 
                      and manu_country_id     = L_ismc_row.manu_country_id 
                      and process$status      = 'P';

              merge into svc_item_supp_manu_country ismc
                   using (select LP_process_id as process_id,
                                 LP_chunk_id as chunk_id,
                                 NVL(max(rownum+1), 1) as row_seq,
                                 I_action as action,
                                 LP_process_status as process_status,
                                 I_item_rec.item_master_def.item as item,
                                 L_ismc_row.supplier as supplier,
                                 L_ismc_row.manu_country_id as manu_country_id,
                                 L_ismc_row.primary_manu_ctry_ind as primary_manu_ctry_ind,
                                 LP_user as create_id,
                                 SYSDATE as create_datetime,
                                 LP_user as last_upd_id,
                                 SYSDATE as last_upd_datetime
                            from svc_item_supp_manu_country
                           where process_id = LP_process_id
                             and chunk_id = LP_chunk_id) temp
                      on (temp.manu_country_id = ismc.manu_country_id
                          and temp.supplier = ismc.supplier
                          and temp.item = ismc.item
                          and temp.action = CORESVC_ITEM.ACTION_MOD)
                    when matched then
                  update
                     set process_id            = temp.process_id,
                         chunk_id              = temp.chunk_id,
                         row_seq               = temp.row_seq,
                         action                = temp.action,
                         process$status        = temp.process_status,
                         primary_manu_ctry_ind = temp.primary_manu_ctry_ind,
                         last_upd_id           = temp.last_upd_id,
                         last_upd_datetime     = temp.last_upd_datetime
                    when not matched then
                  insert (process_id,
                          chunk_id,
                          row_seq,
                          action,
                          process$status,
                          item,
                          supplier,
                          manu_country_id,
                          primary_manu_ctry_ind,
                          create_id,
                          create_datetime,
                          last_upd_id,
                          last_upd_datetime)
                  values (temp.process_id,
                          temp.chunk_id,
                          temp.row_seq,
                          temp.action,
                          temp.process_status,
                          temp.item,
                          temp.supplier,
                          temp.manu_country_id,
                          temp.primary_manu_ctry_ind,
                          temp.create_id,
                          temp.create_datetime,
                          temp.last_upd_id,
                          temp.last_upd_datetime);
           END LOOP;
        end if;
     END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, supplier and country of manufacture',
                                            'SVC_ITEM_SUPP_MANU_COUNTRY',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISMC;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISMC(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sup_rows      IN       "RIB_XItemSupRef_TBL",
                         I_item_rec      IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_action        IN       SVC_ITEM_SUPP_MANU_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISMC';
   L_countries    "RIB_XItemSupCtyMfrRef_TBL";

BEGIN
   if I_sup_rows is NOT NULL and I_sup_rows.COUNT > 0 then
      FOR i IN I_sup_rows.FIRST..I_sup_rows.LAST LOOP
         L_countries := I_sup_rows(i).XItemSupCtyMfrRef_TBL;
         ---
         if L_countries is NOT NULL and L_countries.COUNT > 0 then
            FOR j IN L_countries.FIRST..L_countries.LAST LOOP
              merge into svc_item_supp_manu_country ismc
                   using (select LP_process_id as process_id,
                                 LP_chunk_id as chunk_id,
                                 NVL(max(rownum+1), 1) as row_seq,
                                 I_action as action,
                                 LP_process_status as process_status,
                                 I_item_rec.item as item,
                                 I_sup_rows(i).supplier as supplier,
                                 L_countries(j).manufacturer_ctry_id as manu_country_id,
                                 LP_user as create_id,
                                 SYSDATE as create_datetime,
                                 LP_user as last_upd_id,
                                 SYSDATE as last_upd_datetime
                            from svc_item_supp_manu_country
                           where process_id = LP_process_id
                             and chunk_id = LP_chunk_id) temp
                      on (temp.manu_country_id = ismc.manu_country_id
                          and temp.supplier = ismc.supplier
                          and temp.item = ismc.item
                          and temp.action = CORESVC_ITEM.ACTION_DEL)
                    when matched then
                  update
                     set process_id            = temp.process_id,
                         chunk_id              = temp.chunk_id,
                         row_seq               = temp.row_seq,
                         action                = temp.action,
                         process$status        = temp.process_status,
                         last_upd_id           = temp.last_upd_id,
                         last_upd_datetime     = temp.last_upd_datetime
                    when not matched then
                  insert (process_id,
                          chunk_id,
                          row_seq,
                          action,
                          process$status,
                          item,
                          supplier,
                          manu_country_id,
                          create_id,
                          create_datetime,
                          last_upd_id,
                          last_upd_datetime)
                  values (temp.process_id,
                          temp.chunk_id,
                          temp.row_seq,
                          temp.action,
                          temp.process_status,
                          temp.item,
                          temp.supplier,
                          temp.manu_country_id,
                          temp.create_id,
                          temp.create_datetime,
                          temp.last_upd_id,
                          temp.last_upd_datetime);
            END LOOP;  ---  countries
         end if;
      END LOOP;  --- suppliers
   end if;
   ---

   if O_error_message is NOT NULL then
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, supplier and country of manufacture',
                                            'SVC_ITEM_SUPP_MANU_COUNTRY',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISMC;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISCD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_isup_rows       IN       "RIB_XItemSupDesc_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                         I_message_type    IN       VARCHAR2,
                         I_action          IN       SVC_ITEM_SUPP_COUNTRY_DIM.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISCD';
   L_iscd_row     ITEM_SUPP_COUNTRY_DIM%ROWTYPE := I_item_rec.iscd_def;
   L_countries    "RIB_XItemSupCtyDesc_TBL";
   L_dims         "RIB_XISCDimDesc_TBL";

BEGIN
   if I_isup_rows is NOT NULL and I_isup_rows.COUNT > 0 then
      FOR i IN I_isup_rows.FIRST..I_isup_rows.LAST LOOP
         L_iscd_row.item := I_item_rec.item_master_def.item;
         L_iscd_row.supplier := I_isup_rows(i).supplier;
         L_countries := I_isup_rows(i).xitemsupctydesc_TBL;
         L_iscd_row.last_update_id     := LP_user;

         if L_countries is NOT NULL and L_countries.COUNT > 0 then
            FOR j IN L_countries.FIRST..L_countries.LAST LOOP
               if L_countries(j).xiscdimdesc_TBL is NOT NULL and L_countries(j).xiscdimdesc_TBL.count > 0 then
                  L_dims := L_countries(j).xiscdimdesc_TBL;
                  L_iscd_row.origin_country := L_countries(j).origin_country_id;

                  if L_dims.COUNT = 0 and
                     I_item_rec.item_master_def.catch_weight_ind = 'Y' and
                     I_item_rec.item_master_def.simple_pack_ind  = 'Y' and
                     I_item_rec.item_master_def.order_type = 'V' then
                     O_error_message := SQL_LIB.CREATE_MSG('DIM_MUST_ENTER','Item: '||I_item_rec.item_master_def.item, 'Message Type: '||I_message_type);
                     return FALSE;
                  end if;

                  if L_dims is not NULL and L_dims.COUNT > 0 then
                     FOR k in L_dims.FIRST..L_dims.LAST LOOP
                        L_iscd_row.dim_object          := L_dims(k).dim_object;
                        L_iscd_row.tare_weight         := L_dims(k).tare_weight;
                        L_iscd_row.tare_type           := L_dims(k).tare_type;
                        L_iscd_row.lwh_uom             := L_dims(k).lwh_uom;
                        L_iscd_row.length              := L_dims(k).length;
                        L_iscd_row.width               := L_dims(k).width;
                        L_iscd_row.height              := L_dims(k).dim_height;
                        L_iscd_row.liquid_volume       := L_dims(k).liquid_volume;
                        L_iscd_row.liquid_volume_uom   := L_dims(k).liquid_volume_uom;
                        L_iscd_row.stat_cube           := L_dims(k).stat_cube;
                        L_iscd_row.weight_uom          := L_dims(k).weight_uom;
                        L_iscd_row.weight              := L_dims(k).weight;
                        L_iscd_row.net_weight          := L_dims(k).net_weight;
                        L_iscd_row.presentation_method := L_dims(k).presentation_method;
                        ---
                        delete from svc_item_supp_country_dim 
                              where item                = I_item_rec.item_master_def.item 
                                and supplier            = L_iscd_row.supplier 
                                and origin_country      = L_iscd_row.origin_country 
                                and process$status      = 'P';
                        merge into svc_item_supp_country_dim iscd
                             using (select LP_process_id as process_id,
                                           LP_chunk_id as chunk_id,
                                           NVL(max(rownum+1), 1) as row_seq,
                                           I_action as action,
                                           LP_process_status as process_status,
                                           L_iscd_row.item as item,
                                           L_iscd_row.supplier as supplier,
                                           L_iscd_row.origin_country as origin_country,
                                           L_iscd_row.dim_object as dim_object,
                                           L_iscd_row.presentation_method as presentation_method,
                                           L_iscd_row.length as length,
                                           L_iscd_row.width as width,
                                           L_iscd_row.height as height,
                                           L_iscd_row.lwh_uom as lwh_uom,
                                           L_iscd_row.weight as weight,
                                           L_iscd_row.net_weight as net_weight,
                                           L_iscd_row.weight_uom as weight_uom,
                                           L_iscd_row.liquid_volume as liquid_volume,
                                           L_iscd_row.liquid_volume_uom as liquid_volume_uom,
                                           L_iscd_row.stat_cube as stat_cube,
                                           L_iscd_row.tare_weight as tare_weight,
                                           L_iscd_row.tare_type as tare_type,
                                           LP_user as create_id,
                                           SYSDATE as create_datetime,
                                           LP_user as last_upd_id,
                                           SYSDATE as last_upd_datetime
                                      from svc_item_supp_country_dim
                                     where process_id = LP_process_id
                                       and chunk_id = LP_chunk_id) temp
                                on (temp.dim_object = iscd.dim_object
                                    and temp.origin_country = iscd.origin_country
                                    and temp.supplier = iscd.supplier
                                    and temp.item = iscd.item
                                    and temp.action = CORESVC_ITEM.ACTION_MOD)
                              when matched then
                            update
                               set process_id          = temp.process_id,
                                   chunk_id            = temp.chunk_id,
                                   row_seq             = temp.row_seq,
                                   action              = temp.action,
                                   process$status      = temp.process_status,
                                   weight              = temp.weight,
                                   height              = temp.height,
                                   weight_uom          = temp.weight_uom,
                                   stat_cube           = temp.stat_cube,
                                   tare_weight         = temp.tare_weight,
                                   presentation_method = temp.presentation_method,
                                   liquid_volume_uom   = temp.liquid_volume_uom,
                                   length              = temp.length,
                                   liquid_volume       = temp.liquid_volume,
                                   width               = temp.width,
                                   lwh_uom             = temp.lwh_uom,
                                   tare_type           = temp.tare_type,
                                   net_weight          = temp.net_weight,
                                   last_upd_id         = temp.last_upd_id,
                                   last_upd_datetime   = temp.last_upd_datetime
                              when not matched then
                            insert (process_id,
                                    chunk_id,
                                    row_seq,
                                    action,
                                    process$status,
                                    item,
                                    supplier,
                                    origin_country,
                                    dim_object,
                                    presentation_method,
                                    length,
                                    width,
                                    height,
                                    lwh_uom,
                                    weight,
                                    net_weight,
                                    weight_uom,
                                    liquid_volume,
                                    liquid_volume_uom,
                                    stat_cube,
                                    tare_weight,
                                    tare_type,
                                    create_id,
                                    create_datetime,
                                    last_upd_id,
                                    last_upd_datetime)
                            values (temp.process_id,
                                    temp.chunk_id,
                                    temp.row_seq,
                                    temp.action,
                                    temp.process_status,
                                    temp.item,
                                    temp.supplier,
                                    temp.origin_country,
                                    temp.dim_object,
                                    temp.presentation_method,
                                    temp.length,
                                    temp.width,
                                    temp.height,
                                    temp.lwh_uom,
                                    temp.weight,
                                    temp.net_weight,
                                    temp.weight_uom,
                                    temp.liquid_volume,
                                    temp.liquid_volume_uom,
                                    temp.stat_cube,
                                    temp.tare_weight,
                                    temp.tare_type,
                                    temp.create_id,
                                    temp.create_datetime,
                                    temp.last_upd_id,
                                    temp.last_upd_datetime);
                     END LOOP;
                  end if;
               end if;
            END LOOP;
         end if;
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, supplier, origin country and dim object',
                                            'SVC_ITEM_SUPP_COUNTRY_DIM',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISCD;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISCD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_sup_rows        IN       "RIB_XItemSupRef_TBL",
                         I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                         I_action          IN       SVC_ITEM_SUPP_COUNTRY_DIM.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISCD';
   L_countries    "RIB_XItemSupCtyRef_TBL";
   L_dims         "RIB_XISCDimRef_TBL";

BEGIN
   if I_sup_rows is NOT NULL and I_sup_rows.COUNT > 0 then
      FOR i IN I_sup_rows.FIRST..I_sup_rows.LAST LOOP
         L_countries := I_sup_rows(i).xitemsupctyref_TBL;
         if L_countries is NOT NULL and L_countries.COUNT > 0 then
            FOR j IN L_countries.FIRST..L_countries.LAST LOOP
               L_dims := L_countries(j).xiscdimref_TBL;
               if L_dims is NOT NULL and L_dims.COUNT > 0 then
                  FOR k IN L_dims.FIRST..L_dims.LAST LOOP
                     merge into svc_item_supp_country_dim iscd
                          using (select LP_process_id as process_id,
                                        LP_chunk_id as chunk_id,
                                        NVL(max(rownum+1), 1) as row_seq,
                                        I_action as action,
                                        LP_process_status as process_status,
                                        I_item_rec.item as item,
                                        I_sup_rows(i).supplier as supplier,
                                        L_countries(j).origin_country_id as origin_country_id,
                                        L_dims(k).dim_object as dim_object,
                                        LP_user as create_id,
                                        SYSDATE as create_datetime,
                                        LP_user as last_upd_id,
                                        SYSDATE as last_upd_datetime
                                   from svc_item_supp_country_dim
                                  where process_id = LP_process_id
                                    and chunk_id = LP_chunk_id) temp
                             on (temp.dim_object = iscd.dim_object
                                 and temp.origin_country_id = iscd.origin_country
                                 and temp.supplier = iscd.supplier
                                 and temp.item = iscd.item
                                 and temp.action = CORESVC_ITEM.ACTION_DEL)
                           when matched then
                         update
                            set process_id          = temp.process_id,
                                chunk_id            = temp.chunk_id,
                                row_seq             = temp.row_seq,
                                action              = temp.action,
                                process$status      = temp.process_status,
                                last_upd_id         = temp.last_upd_id,
                                last_upd_datetime   = temp.last_upd_datetime
                           when not matched then
                         insert (process_id,
                                 chunk_id,
                                 row_seq,
                                 action,
                                 process$status,
                                 item,
                                 supplier,
                                 origin_country,
                                 dim_object,
                                 create_id,
                                 create_datetime,
                                 last_upd_id,
                                 last_upd_datetime)
                         values (temp.process_id,
                                 temp.chunk_id,
                                 temp.row_seq,
                                 temp.action,
                                 temp.process_status,
                                 temp.item,
                                 temp.supplier,
                                 temp.origin_country_id,
                                 temp.dim_object,
                                 temp.create_id,
                                 temp.create_datetime,
                                 temp.last_upd_id,
                                 temp.last_upd_datetime);
                  END LOOP;  --- dimensions
               end if;
            END LOOP;  ---  countries
         end if;
      END LOOP;  --- suppliers
   end if;
   ---

   if O_error_message is NOT NULL then
      return FALSE;
   end if;
   ---

   return TRUE;


EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, supplier, origin country and dim object',
                                            'SVC_ITEM_SUPP_COUNTRY_DIM',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ISCD;
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- Insert records into svc_xitem_rizp. These records will be later processed in
-- coresvc_item.process_IZP function.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IZP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item_rec      IN RMSSUB_XITEM.ITEM_API_REC,
                        I_action        IN SVC_RPM_ITEM_ZONE_PRICE.ACTION%TYPE)
RETURN BOOLEAN is

   L_program  VARCHAR2(50)               := 'RMSSUB_XITEM_SQL.INSERT_SVC_IZP';
   L_izp_tbl  RMSSUB_XITEM.IZP_TBLTYPE   := I_item_rec.izp_tbl;
   L_item_rec RMSSUB_XITEM.ITEM_API_REC  := I_item_rec;
BEGIN
   if L_izp_tbl is NOT NULL and L_izp_tbl.COUNT > 0 THEN
      delete from svc_xitem_rizp
       where item = L_item_rec.item_master_def.item;
      ---
      for i in L_izp_tbl.FIRST..L_izp_tbl.LAST loop
         insert into svc_xitem_rizp(process_id,
                                    row_seq,
                                    action,
                                    PROCESS$STATUS,
                                    item,
                                    unit_retail,
                                    selling_unit_retail,
                                    selling_uom,
                                    multi_selling_uom,
                                    multi_units,
                                    multi_unit_retail,
                                    currency_code)
                             values(LP_process_id,
                                    i,
                                    I_action,
                                    LP_process_status,
                                    L_item_rec.item_master_def.item,
                                    L_izp_tbl(i).UNIT_RETAIL,
                                    L_izp_tbl(i).SELLING_UNIT_RETAIL,
                                    L_izp_tbl(i).SELLING_UOM,
                                    L_izp_tbl(i).MULTI_SELLING_UOM,
                                    L_izp_tbl(i).MULTI_UNITS,
                                    L_izp_tbl(i).MULTI_UNIT_RETAIL,
                                    L_izp_tbl(i).CURRENCY_CODE);
         if L_izp_tbl(i).zones is NOT NULL and L_izp_tbl(i).zones.count > 0 then
            delete from svc_xitem_rizp_locs
                  where item = L_item_rec.item_master_def.item
                    and loc in(select column_value from TABLE(L_izp_tbl(i).zones));
            insert into svc_xitem_rizp_locs(process_id,
                                            head_row_seq,
                                            item,
                                            loc)
                                     select LP_process_id as process_id,
                                            i AS head_row_seq,
                                            L_item_rec.item_master_def.item as item,
                                            column_value AS loc
                                       from TABLE(L_izp_tbl(i).zones);
         end if;
      end loop;
   end if;
   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS', 'item and zone_id', 'SVC_RPM_ITEM_ZONE_PRICE', NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_SVC_IZP;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_PACKITEM(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item_rec      IN       RMSSUB_XITEM.ITEM_API_REC,
                             I_action        IN       SVC_PACKITEM.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_PACKITEM';
   L_pack_row     PACKITEM%ROWTYPE ;
   L_pack_tbl     RMSSUB_XITEM.PACKITEM_TBLTYPE := I_item_rec.packs;

BEGIN
   if L_pack_tbl is NOT NULL and L_pack_tbl.COUNT > 0 then
      FOR i IN L_pack_tbl.FIRST..L_pack_tbl.LAST LOOP
         L_pack_row.pack_no := I_item_rec.item_master_def.item;
         L_pack_row.seq_no := L_pack_tbl(i).seq_no;
         L_pack_row.item := L_pack_tbl(i).comp_item;
         L_pack_row.pack_qty := L_pack_tbl(i).comp_qty;
         L_pack_row.create_datetime := I_item_rec.item_master_def.create_datetime;
         L_pack_row.last_update_id := LP_user;
         ---
         delete from svc_packitem
               where pack_no               = L_pack_row.pack_no
                 and NVL(item,'X')         = NVL(L_pack_row.item,'X')
                 and NVL(item_parent,'X')  = NVL(L_pack_row.item_parent,'X')
                 and NVL(pack_tmpl_id,0) = NVL(L_pack_row.pack_tmpl_id,0)
                 and process$status = 'P';
         merge into svc_packitem spi
              using (select LP_process_id as process_id,
                            LP_chunk_id as chunk_id,
                            NVL(max(rownum+1), 1) as row_seq,
                            I_action as action,
                            LP_process_status as process_status,
                            L_pack_row.pack_no as pack_no,
                            L_pack_row.seq_no as seq_no,
                            L_pack_row.item as item,
                            L_pack_row.item_parent as item_parent,
                            L_pack_row.pack_tmpl_id as pack_tmpl_id,
                            L_pack_row.pack_qty as pack_qty,
                            L_pack_row.last_update_id as create_id,
                            L_pack_row.create_datetime as create_datetime,
                            L_pack_row.last_update_id as last_upd_id,
                            SYSDATE as last_upd_datetime
                       from svc_packitem
                      where process_id = LP_process_id
                        and chunk_id = LP_chunk_id) temp
                 on (temp.PACK_NO = spi.PACK_NO
                     and NVL(temp.item,'X') = NVL(spi.item,'X')
                     and NVL(temp.item_parent,'X') = NVL(spi.item_parent,'X')
                     and NVL(temp.PACK_TMPL_ID,0) = NVL(spi.PACK_TMPL_ID,0)
                     and temp.action = CORESVC_ITEM.ACTION_MOD)
               when matched then
             update
                set process_id  = temp.process_id,
                    chunk_id = temp.chunk_id,
                    row_seq = temp.row_seq,
                    action = temp.action,
                    process$status = temp.process_status,
                    pack_qty = temp.pack_qty,
                    last_upd_id = temp.last_upd_id,
                    last_upd_datetime = temp.last_upd_datetime
               when not matched then
             insert (process_id,
                     chunk_id,
                     row_seq,
                     action,
                     process$status,
                     pack_no,
                     seq_no,
                     item,
                     item_parent,
                     pack_tmpl_id,
                     pack_qty,
                     create_id,
                     create_datetime,
                     last_upd_id,
                     last_upd_datetime)
             values (temp.process_id,
                     temp.chunk_id,
                     temp.row_seq,
                     temp.action,
                     temp.process_status,
                     temp.pack_no,
                     temp.seq_no,
                     temp.item,
                     temp.item_parent,
                     temp.pack_tmpl_id,
                     temp.pack_qty,
                     temp.create_id,
                     temp.create_datetime,
                     temp.last_upd_id,
                     temp.last_upd_datetime);
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item or item parent and pack no',
                                            'SVC_PACKITEM',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_PACKITEM;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_VATITEM(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_vat_rows          IN       "RIB_XItemVATDesc_TBL",
                            I_item_rec          IN       RMSSUB_XITEM.ITEM_API_REC,
                            I_action            IN       SVC_VAT_ITEM.ACTION%TYPE)

   RETURN BOOLEAN IS

   L_program                  VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_VATITEM';
   L_tax_code_rate_rec        OBJ_TAX_CODE_RATE_REC := OBJ_TAX_CODE_RATE_REC();
   L_tax_code_rate_tbl        OBJ_TAX_CODE_RATE_TBL := OBJ_TAX_CODE_RATE_TBL();

BEGIN

   if I_vat_rows is NOT NULL and
      I_vat_rows.COUNT > 0 then
      ---
      L_tax_code_rate_tbl.DELETE;
      L_tax_code_rate_tbl.EXTEND;

      FOR i IN I_vat_rows.FIRST..I_vat_rows.LAST LOOP
      ---
         -- Get the rates
         L_tax_code_rate_rec.I_tax_code            := I_vat_rows(i).vat_code;
         L_tax_code_rate_rec.I_tax_type            := I_vat_rows(i).vat_type;
         L_tax_code_rate_rec.I_effective_from_date := trunc(I_vat_rows(i).active_date);

         -- L_tax_code_rate_tbl should only contain 1 record and overwritten for each I_vat_rows rec
         L_tax_code_rate_tbl(1) := L_tax_code_rate_rec;

         if TAX_SQL.GET_TAX_RATE_BY_CODE(O_error_message,
                                         L_tax_code_rate_tbl) = FALSE then
             return FALSE;
         end if;
         ---
         delete from svc_vat_item
               where item           = I_item_rec.item_master_def.item
                 and vat_region     = I_vat_rows(i).vat_region
                 and vat_type       = I_vat_rows(i).vat_type
                 and active_date    = I_vat_rows(i).active_date
                 and process$status = 'P';
         insert into svc_vat_item(process_id,
                                  chunk_id,
                                  row_seq,
                                  action,
                                  process$status,
                                  item,
                                  vat_region,
                                  active_date,
                                  vat_code,
                                  vat_rate,
                                  vat_type,
                                  reverse_vat_ind,
                                  create_id,
                                  create_datetime,
                                  last_upd_id,
                                  last_upd_datetime)
                           select LP_process_id,
                                  LP_chunk_id,
                                  NVL(max(rownum+1), 1) rowseq,
                                  I_action,
                                  LP_process_status,
                                  I_item_rec.item_master_def.item,
                                  I_vat_rows(i).vat_region,
                                  I_vat_rows(i).active_date,
                                  I_vat_rows(i).vat_code,
                                  L_tax_code_rate_tbl(L_tax_code_rate_tbl.count).O_tax_rate,
                                  I_vat_rows(i).vat_type,
                                  I_vat_rows(i).reverse_vat_ind,
                                  LP_user,
                                  SYSDATE,
                                  LP_user,
                                  SYSDATE
                             from svc_vat_item
                            where process_id = LP_process_id
                              and chunk_id = LP_chunk_id;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, vat region, vat type and active date',
                                            'SVC_VAT_ITEM',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_VATITEM;
-------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_VATITEM(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item_rec          IN       RMSSUB_XITEM.ITEM_API_REC)

   RETURN BOOLEAN IS

   L_program                  VARCHAR2(50) := 'RMSSUB_XITEM_SQL.DEFAULT_VATITEM';
   L_obj_tax_info_rec         OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();
   L_obj_tax_info_tbl         OBJ_TAX_INFO_TBL := OBJ_TAX_INFO_TBL();
   O_run_report               BOOLEAN;

BEGIN
   --- Default vat_item records with vat from item's department if vat_ind is on ('Y')

   ---Assign the item and dept to the ABO object
   L_obj_tax_info_rec.item                := I_item_rec.item_master_def.item;
   L_obj_tax_info_rec.merch_hier_level    := 4;
   L_obj_tax_info_rec.merch_hier_value    := I_item_rec.item_master_def.dept;

   L_obj_tax_info_tbl.EXTEND;
   L_obj_tax_info_tbl(L_obj_tax_info_tbl.COUNT) := L_obj_tax_info_rec;

   ---Copy the tax info for item's department
   if TAX_SQL.GET_TAX_INFO(O_error_message,
                           L_obj_tax_info_tbl) = FALSE then
      return FALSE;
   end if;
   ---
   if TAX_SQL.INSERT_UPDATE_TAX_SKU(O_error_message,
                                    O_run_report,
                                    L_obj_tax_info_tbl) = FALSE then
      return FALSE;
   end if;

   if O_error_message is NOT NULL then
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DEFAULT_VATITEM;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_VATITEM(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_vat_rows      IN       "RIB_XItemVATRef_TBL",
                            I_item          IN       ITEM_MASTER.ITEM%TYPE,
                            I_action        IN       SVC_VAT_ITEM.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_VATITEM';

BEGIN
   if I_vat_rows is NOT NULL and I_vat_rows.COUNT > 0 then
      FOR i IN I_vat_rows.FIRST..I_vat_rows.LAST LOOP
         merge into svc_vat_item svi
              using (select LP_process_id as process_id,
                            LP_chunk_id as chunk_id,
                            NVL(max(rownum+1), 1) as row_seq,
                            I_action as action,
                            LP_process_status as process_status,
                            I_item as item,
                            I_vat_rows(i).vat_region as vat_region,
                            trunc(I_vat_rows(i).active_date) as active_date,
                            I_vat_rows(i).vat_code as vat_code,
                            I_vat_rows(i).vat_type as vat_type,
                            'N' as reverse_vat_ind,
                            LP_user as create_id,
                            SYSDATE as create_datetime,
                            LP_user as last_upd_id,
                            SYSDATE as last_upd_datetime
                       from svc_vat_item
                      where process_id = LP_process_id
                        and chunk_id = LP_chunk_id) temp
                 on (temp.item = svi.item
                     and temp.vat_region = svi.vat_region
                     and to_char(temp.active_date, 'DD-MON-RR') = to_char(svi.active_date, 'DD-MON-RR')
                     and temp.vat_type = svi.vat_type
                     and temp.action = CORESVC_ITEM.ACTION_DEL)
               when matched then
             update
                set process_id = temp.process_id,
                    chunk_id = temp.chunk_id,
                    row_seq = temp.row_seq,
                    action = temp.action,
                    process$status = temp.process_status,
                    vat_code = temp.vat_code,
                    last_upd_id = temp.last_upd_id,
                    last_upd_datetime = temp.last_upd_datetime
               when not matched then
             insert (process_id,
                     chunk_id,
                     row_seq,
                     action,
                     process$status,
                     item,
                     vat_region,
                     active_date,
                     vat_code,
                     vat_type,
                     create_id,
                     create_datetime,
                     last_upd_id,
                     last_upd_datetime)
             values (temp.process_id,
                     temp.chunk_id,
                     temp.row_seq,
                     temp.action,
                     temp.process_status,
                     temp.item,
                     temp.vat_region,
                     temp.active_date,
                     temp.vat_code,
                     temp.vat_type,
                     temp.create_id,
                     temp.create_datetime,
                     temp.last_upd_id,
                     temp.last_upd_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, vat region, vat type and active date',
                                            'SVC_VAT_ITEM',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_VATITEM;
-------------------------------------------------------------------------------------------------------
FUNCTION CONVERT_PRICE_HIST_UNIT_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_pack_type       IN       ITEM_MASTER.PACK_TYPE%TYPE,
                                      I_sellable_ind    IN       ITEM_MASTER.SELLABLE_IND%TYPE,
                                      I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                      I_orderable_ind   IN       ITEM_MASTER.ORDERABLE_IND%TYPE)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'RMSSUB_XITEM_SQL.CONVERT_PRICE_HIST_UNIT_COST';
   --- Only used for item_attrib_sql call
   L_standard_unit_retail_prim ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_prim         ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_prim  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_prim          ITEM_LOC.SELLING_UOM%TYPE;
   ---
BEGIN
   ---
   if I_orderable_ind ='Y' or (NVL(I_orderable_ind, 'N') ='N' and LP_prim_supp is not NULL)  then
      if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                           LP_supp_currency_code,
                                           LP_prim_supp) = FALSE then
         return FALSE;
      end if;
   end if;

   if I_pack_type = 'B'    and
      I_sellable_ind = 'Y' then
      --- get pack cost from components
      if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                              LP_isc_unit_cost,
                                              L_standard_unit_retail_prim,
                                              L_standard_uom_prim,
                                              L_selling_unit_retail_prim,
                                              L_selling_uom_prim,
                                              I_item,
                                              'C') = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_isc_unit_cost is NULL or LP_isc_unit_cost=0 then
      LP_isc_unit_cost :=0;
   else --- global LP_isc_unit_cost is set for the item when inserting into item_supp_country

      LP_isc_unit_cost :=  CURRENCY_SQL.CONVERT_VALUE('C',
                                                      NULL,
                                                      LP_supp_currency_code,
                                                      LP_isc_unit_cost);

   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CONVERT_PRICE_HIST_UNIT_COST;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEMSEASON_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                                      I_action          IN       SVC_ITEM_SEASONS.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEMSEASON_DETAIL';
   L_seq_no           ITEM_SEASONS.ITEM_SEASON_SEQ_NO%TYPE;

BEGIN

   if SEASON_SQL.NEXT_SEQ_NO(O_error_message,
                             L_seq_no,
                             I_item_rec.item_master_def.item) = FALSE then
      return FALSE;
   end if;

   if I_item_rec.seasons.season_id is NOT NULL and I_item_rec.seasons.season_id.COUNT > 0 then
      FOR i in I_item_rec.seasons.season_id.FIRST..I_item_rec.seasons.season_id.LAST LOOP
         ---
      delete from svc_item_seasons 
            where season_id      = I_item_rec.seasons.season_id(i)
              and item           = I_item_rec.item_master_def.item 
              and process$status = 'P';
         insert into svc_item_seasons(process_id,
                                      chunk_id,
                                      row_seq,
                                      action,
                                      process$status,
                                      item,
                                      season_id,
                                      phase_id,
                                      item_season_seq_no,
                                      diff_id,
                                      create_id,
                                      create_datetime,
                                      last_upd_id,
                                      last_upd_datetime)
                               select LP_process_id,
                                      LP_chunk_id,
                                      NVL(max(rownum+1), 1) rowseq,
                                      I_action,
                                      LP_process_status,
                                      I_item_rec.item_master_def.item,
                                      I_item_rec.seasons.season_id(i),
                                      I_item_rec.seasons.phase_id(i),
                                      L_seq_no,
                                      I_item_rec.seasons.diff_id(i),
                                      LP_user,
                                      NVL(I_item_rec.seasons.create_date(i), SYSDATE),
                                      NVL(I_item_rec.seasons.last_update_id(i), LP_user),
                                      NVL(I_item_rec.seasons.last_update_date(i), SYSDATE)
                                 from svc_item_seasons
                                where process_id = LP_process_id
                                  and chunk_id = LP_chunk_id;
         L_seq_no := L_seq_no + 1;
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ITEMSEASON_DETAIL;
--------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEMSEASON_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                                      I_action          IN       SVC_ITEM_SEASONS.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEMSEASON_DETAIL';
   L_item             ITEM_SEASONS.ITEM%TYPE;

BEGIN
   L_item := I_item_rec.item;
   if I_item_rec.seasons.season_id is NOT NULL and I_item_rec.seasons.season_id.COUNT > 0 then
      FOR i in I_item_rec.seasons.season_id.FIRST..I_item_rec.seasons.season_id.LAST LOOP
      ---
         merge into svc_item_seasons svis
              using (select LP_process_id as process_id,
                            LP_chunk_id as chunk_id,
                            NVL(max(rownum+1), 1) as row_seq,
                            I_action as action,
                            LP_process_status as process_status,
                            L_item as item,
                            I_item_rec.seasons.season_id(i) as season_id,
                            I_item_rec.seasons.phase_id(i) as phase_id,
                            I_item_rec.seasons.diff_id(i) as diff_id,
                            LP_user as create_id,
                            SYSDATE as create_datetime,
                            LP_user as last_upd_id,
                            SYSDATE as last_upd_datetime
                       from svc_item_seasons
                      where process_id = LP_process_id
                        and chunk_id = LP_chunk_id) temp
                  on (temp.item = svis.item
                      and temp.season_id = svis.season_id
                      and temp.phase_id = svis.phase_id
                      and temp.diff_id = svis.diff_id
                      and temp.action = CORESVC_ITEM.ACTION_DEL)
                when matched then
              update
                 set process_id = temp.process_id,
                     chunk_id = temp.chunk_id,
                     row_seq = temp.row_seq,
                     action = temp.action,
                     process$status = temp.process_status,
                     last_upd_id = temp.last_upd_id,
                     last_upd_datetime = temp.last_upd_datetime
                when not matched then
              insert (process_id,
                      chunk_id,
                      row_seq,
                      action,
                      process$status,
                      item,
                      season_id,
                      phase_id,
                      diff_id,
                      create_id,
                      create_datetime,
                      last_upd_id,
                      last_upd_datetime)
              values (temp.process_id,
                      temp.chunk_id,
                      temp.row_seq,
                      temp.action,
                      temp.process_status,
                      temp.item,
                      temp.season_id,
                      temp.phase_id,
                      temp.diff_id,
                      temp.create_id,
                      temp.create_datetime,
                      temp.last_upd_id,
                      temp.last_upd_datetime);
      END LOOP;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ITEMSEASON_DETAIL;
--------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEMUDA_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                                   I_action          IN       SVC_UDA_ITEM_FF.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEMUDA_DETAIL';

BEGIN

   if I_item_rec.udaslov.uda_id is NOT NULL and I_item_rec.udaslov.uda_id.COUNT > 0 then
      BEGIN
         FOR i in I_item_rec.udaslov.uda_id.FIRST..I_item_rec.udaslov.uda_id.LAST LOOP
            ---
            delete svc_uda_item_lov
             where item           = I_item_rec.item_master_def.item
               and uda_id         = I_item_rec.udaslov.uda_id(i)
               and uda_value      = I_item_rec.udaslov.uda_value(i)
               and process$status = 'P';
            insert into svc_uda_item_lov(process_id,
                                         chunk_id,
                                         row_seq,
                                         action,
                                         process$status,
                                         item,
                                         uda_id,
                                         uda_value,
                                         create_id,
                                         create_datetime,
                                         last_upd_id,
                                         last_upd_datetime)
                                  select LP_process_id,
                                         LP_chunk_id,
                                         NVL(max(rownum+1), 1) rowseq,
                                         I_action,
                                         LP_process_status,
                                         I_item_rec.item_master_def.item,
                                         I_item_rec.udaslov.uda_id(i),
                                         I_item_rec.udaslov.uda_value(i),
                                         LP_user,
                                         NVL(I_item_rec.udaslov.create_date(i), SYSDATE),
                                         NVL(I_item_rec.udaslov.last_update_id(i), LP_user),
                                         NVL(I_item_rec.udaslov.last_update_date(i), SYSDATE)
                                    from svc_uda_item_lov
                                   where process_id = LP_process_id
                                     and chunk_id = LP_chunk_id;
         END LOOP;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                  'item, uda id and uda value',
                                                  'SVC_UDA_ITEM_LOV',
                                                  NULL);
            return FALSE;
      END;
   end if;

   if I_item_rec.udadate.uda_id is NOT NULL and I_item_rec.udadate.uda_id.COUNT > 0 then
      BEGIN
         FOR i in I_item_rec.udadate.uda_id.FIRST..I_item_rec.udadate.uda_id.LAST LOOP
            ---
             delete svc_uda_item_date
             where item           = I_item_rec.item_master_def.item
               and uda_id         = I_item_rec.udadate.uda_id(i)
               and uda_date       = I_item_rec.udadate.uda_date(i)
               and process$status = 'P';
            insert into svc_uda_item_date(process_id,
                                          chunk_id,
                                          row_seq,
                                          action,
                                          process$status,
                                          item,
                                          uda_id,
                                          uda_date,
                                          create_id,
                                          create_datetime,
                                          last_upd_id,
                                          last_upd_datetime)
                                   select LP_process_id,
                                          LP_chunk_id,
                                          NVL(max(rownum+1), 1) rowseq,
                                          I_action,
                                          LP_process_status,
                                          I_item_rec.item_master_def.item,
                                          I_item_rec.udadate.uda_id(i),
                                          I_item_rec.udadate.uda_date(i),
                                          LP_user,
                                          NVL(I_item_rec.udadate.create_date(i), SYSDATE),
                                          NVL(I_item_rec.udadate.last_update_id(i), LP_user),
                                          NVL(I_item_rec.udadate.last_update_date(i), SYSDATE)
                                     from svc_uda_item_date
                                    where process_id = LP_process_id
                                      and chunk_id = LP_chunk_id;
         END LOOP;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                  'item, uda id and uda date',
                                                  'SVC_UDA_ITEM_DATE',
                                                  NULL);
            return FALSE;
      END;
   end if;

   if I_item_rec.udaff.uda_id is NOT NULL and I_item_rec.udaff.uda_id.COUNT > 0 then
      BEGIN
         FOR i in I_item_rec.udaff.uda_id.FIRST..I_item_rec.udaff.uda_id.LAST LOOP
            delete svc_uda_item_ff
             where item           = I_item_rec.item_master_def.item
               and uda_id         = I_item_rec.udaff.uda_id(i)
               and uda_text       = I_item_rec.udaff.uda_text(i)
               and process$status = 'P';
            insert into svc_uda_item_ff(process_id,
                                        chunk_id,
                                        row_seq,
                                        action,
                                        process$status,
                                        item,
                                        uda_id,
                                        uda_text,
                                        create_id,
                                        create_datetime,
                                        last_upd_id,
                                        last_upd_datetime)
                                 select LP_process_id,
                                        LP_chunk_id,
                                        NVL(max(rownum+1), 1) rowseq,
                                        I_action,
                                        LP_process_status,
                                        I_item_rec.item_master_def.item,
                                        I_item_rec.udaff.uda_id(i),
                                        I_item_rec.udaff.uda_text(i),
                                        LP_user,
                                        NVL(I_item_rec.udaff.create_date(i), SYSDATE),
                                        NVL(I_item_rec.udaff.last_update_id(i), LP_user),
                                        NVL(I_item_rec.udaff.last_update_date(i), SYSDATE)
                                   from svc_uda_item_ff
                                  where process_id = LP_process_id
                                    and chunk_id = LP_chunk_id;
         END LOOP;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                  'item, uda id and uda text',
                                                  'SVC_UDA_ITEM_FF',
                                                  NULL);
            return FALSE;
      END;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ITEMUDA_DETAIL;
--------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEMUDA_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                                   I_action          IN       SVC_UDA_ITEM_FF.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEMUDA_DETAIL';
   L_item             UDA_ITEM_LOV.ITEM%TYPE;

BEGIN
   L_item := I_item_rec.item;
   if I_item_rec.udaslov.uda_id is NOT NULL and I_item_rec.udaslov.uda_id.COUNT > 0 then
      BEGIN
         FOR i in I_item_rec.udaslov.uda_id.FIRST..I_item_rec.udaslov.uda_id.LAST LOOP
            ---
            merge into svc_uda_item_lov suil
                 using (select LP_process_id as process_id,
                               LP_chunk_id as chunk_id,
                               NVL(max(rownum+1), 1) as row_seq,
                               I_action as action,
                               LP_process_status as process_status,
                               L_item as item,
                               I_item_rec.udaslov.uda_id(i) as uda_id,
                               I_item_rec.udaslov.uda_value(i) as uda_value,
                               LP_user as create_id,
                               SYSDATE as create_datetime,
                               LP_user as last_upd_id,
                               SYSDATE as last_upd_datetime
                          from svc_uda_item_lov
                         where process_id = LP_process_id
                           and chunk_id = LP_chunk_id) temp
                     on (temp.item = suil.item
                         and temp.uda_id = suil.uda_id
                         and temp.uda_value = suil.uda_value
                         and temp.action = CORESVC_ITEM.ACTION_DEL)
                   when matched then
                 update
                    set process_id = temp.process_id,
                        chunk_id = temp.chunk_id,
                        row_seq = temp.row_seq,
                        action = temp.action,
                        process$status = temp.process_status,
                        last_upd_id = temp.last_upd_id,
                        last_upd_datetime = temp.last_upd_datetime
                   when not matched then
                 insert (process_id,
                         chunk_id,
                         row_seq,
                         action,
                         process$status,
                         item,
                         uda_id,
                         uda_value,
                         create_id,
                         create_datetime,
                         last_upd_id,
                         last_upd_datetime)
                 values (temp.process_id,
                         temp.chunk_id,
                         temp.row_seq,
                         temp.action,
                         temp.process_status,
                         temp.item,
                         temp.uda_id,
                         temp.uda_value,
                         temp.create_id,
                         temp.create_datetime,
                         temp.last_upd_id,
                         temp.last_upd_datetime);
         END LOOP;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                  'item, uda id and uda value',
                                                  'SVC_UDA_ITEM_LOV',
                                                  NULL);
            return FALSE;
      END;
   end if;

   if I_item_rec.udadate.uda_id is NOT NULL and I_item_rec.udadate.uda_id.COUNT > 0 then
      BEGIN
         FOR i in I_item_rec.udadate.uda_id.FIRST..I_item_rec.udadate.uda_id.LAST LOOP
            ---
            merge into svc_uda_item_date suid
                 using (select LP_process_id as process_id,
                               LP_chunk_id as chunk_id,
                               NVL(max(rownum+1), 1) as row_seq,
                               I_action as action,
                               LP_process_status as process_status,
                               L_item as item,
                               I_item_rec.udadate.uda_id(i) as uda_id,
                               I_item_rec.udadate.uda_date(i) as uda_date,
                               LP_user as create_id,
                               SYSDATE as create_datetime,
                               LP_user as last_upd_id,
                               SYSDATE as last_upd_datetime
                          from svc_uda_item_date
                         where process_id = LP_process_id
                           and chunk_id = LP_chunk_id) temp
                     on (temp.item = suid.item
                         and temp.uda_id = suid.uda_id
                         and temp.uda_date = suid.uda_date
                         and temp.action = CORESVC_ITEM.ACTION_DEL)
                   when matched then
                 update
                    set process_id = temp.process_id,
                        chunk_id = temp.chunk_id,
                        row_seq = temp.row_seq,
                        action = temp.action,
                        process$status = temp.process_status,
                        last_upd_id = temp.last_upd_id,
                        last_upd_datetime = temp.last_upd_datetime
                   when not matched then
                 insert (process_id,
                         chunk_id,
                         row_seq,
                         action,
                         process$status,
                         item,
                         uda_id,
                         uda_date,
                         create_id,
                         create_datetime,
                         last_upd_id,
                         last_upd_datetime)
                 values (temp.process_id,
                         temp.chunk_id,
                         temp.row_seq,
                         temp.action,
                         temp.process_status,
                         temp.item,
                         temp.uda_id,
                         temp.uda_date,
                         temp.create_id,
                         temp.create_datetime,
                         temp.last_upd_id,
                         temp.last_upd_datetime);
         END LOOP;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                  'item, uda id and uda date',
                                                  'SVC_UDA_ITEM_DATE',
                                                  NULL);
            return FALSE;
      END;
   end if;

   if I_item_rec.udaff.uda_id is NOT NULL and I_item_rec.udaff.uda_id.COUNT > 0 then
      BEGIN
         FOR i in I_item_rec.udaff.uda_id.FIRST..I_item_rec.udaff.uda_id.LAST LOOP
            merge into svc_uda_item_ff suif
                 using (select LP_process_id as process_id,
                               LP_chunk_id as chunk_id,
                               NVL(max(rownum+1), 1) as row_seq,
                               I_action as action,
                               LP_process_status as process_status,
                               L_item as item,
                               I_item_rec.udaff.uda_id(i) as uda_id,
                               I_item_rec.udaff.uda_text(i) as uda_text,
                               LP_user as create_id,
                               SYSDATE as create_datetime,
                               LP_user as last_upd_id,
                               SYSDATE as last_upd_datetime
                          from svc_uda_item_ff
                         where process_id = LP_process_id
                           and chunk_id = LP_chunk_id) temp
                     on (temp.item = suif.item
                         and temp.uda_id = suif.uda_id
                         and temp.uda_text = suif.uda_text
                         and temp.action = CORESVC_ITEM.ACTION_DEL)
                   when matched then
                 update
                    set process_id = temp.process_id,
                        chunk_id = temp.chunk_id,
                        row_seq = temp.row_seq,
                        action = temp.action,
                        process$status = temp.process_status,
                        last_upd_id = temp.last_upd_id,
                        last_upd_datetime = temp.last_upd_datetime
                   when not matched then
                 insert (process_id,
                         chunk_id,
                         row_seq,
                         action,
                         process$status,
                         item,
                         uda_id,
                         uda_text,
                         create_id,
                         create_datetime,
                         last_upd_id,
                         last_upd_datetime)
                 values (temp.process_id,
                         temp.chunk_id,
                         temp.row_seq,
                         temp.action,
                         temp.process_status,
                         temp.item,
                         temp.uda_id,
                         temp.uda_text,
                         temp.create_id,
                         temp.create_datetime,
                         temp.last_upd_id,
                         temp.last_upd_datetime);

         END LOOP;
      EXCEPTION
         when DUP_VAL_ON_INDEX then
            O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                                  'item, uda id and uda text',
                                                  'SVC_UDA_ITEM_FF',
                                                  NULL);
            return FALSE;
      END;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SVC_ITEMUDA_DETAIL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_MASTER(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item_rec      IN       ITEM_MASTER%ROWTYPE,
                                I_action        IN       SVC_ITEM_MASTER.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEM_MASTER';

BEGIN
   delete from svc_item_master 
         where item = I_item_rec.item 
           and process$status = 'P';
   merge into svc_item_master sim
        using (select LP_process_id as process_id,
                      LP_chunk_id as chunk_id,
                      NVL(max(rownum+1), 1) as rowseq,
                      I_action as action,
                      LP_process_status as process_status,
                      I_item_rec.item as item,
                      I_item_rec.item_number_type as item_number_type,
                      I_item_rec.format_id as format_id,
                      I_item_rec.prefix as prefix,
                      I_item_rec.item_parent as item_parent,
                      I_item_rec.item_grandparent as item_grandparent,
                      I_item_rec.pack_ind as pack_ind,
                      I_item_rec.item_level as item_level,
                      I_item_rec.tran_level as tran_level,
                      I_item_rec.item_aggregate_ind as item_aggregate_ind,
                      I_item_rec.diff_1 as diff_1,
                      I_item_rec.diff_1_aggregate_ind as diff_1_aggregate_ind,
                      I_item_rec.diff_2 as diff_2,
                      I_item_rec.diff_2_aggregate_ind as diff_2_aggregate_ind,
                      I_item_rec.diff_3 as diff_3,
                      I_item_rec.diff_3_aggregate_ind as diff_3_aggregate_ind,
                      I_item_rec.diff_4 as diff_4,
                      I_item_rec.diff_4_aggregate_ind as diff_4_aggregate_ind,
                      I_item_rec.dept as dept,
                      I_item_rec.CLASS as CLASS,
                      I_item_rec.subclass as subclass,
                      NVL(I_item_rec.status,'W') as status,
                      I_item_rec.item_desc as item_desc,
                      I_item_rec.item_desc_secondary as item_desc_secondary,
                      I_item_rec.short_desc as short_desc,
                      I_item_rec.primary_ref_item_ind as primary_ref_item_ind,
                      I_item_rec.cost_zone_group_id as cost_zone_group_id,
                      I_item_rec.standard_uom as standard_uom,
                      I_item_rec.uom_conv_factor as uom_conv_factor,
                      I_item_rec.package_size as package_size,
                      I_item_rec.package_uom as package_uom,
                      I_item_rec.merchandise_ind as merchandise_ind,
                      I_item_rec.store_ord_mult as store_ord_mult,
                      I_item_rec.forecast_ind as forecast_ind,
                      I_item_rec.original_retail as original_retail,
                      I_item_rec.mfg_rec_retail as mfg_rec_retail,
                      I_item_rec.retail_label_type as retail_label_type,
                      I_item_rec.retail_label_value as retail_label_value,
                      I_item_rec.handling_temp as handling_temp,
                      I_item_rec.handling_sensitivity as handling_sensitivity,
                      I_item_rec.catch_weight_ind as catch_weight_ind,
                      I_item_rec.waste_type as waste_type,
                      I_item_rec.waste_pct as waste_pct,
                      I_item_rec.default_waste_pct as default_waste_pct,
                      I_item_rec.const_dimen_ind as const_dimen_ind,
                      I_item_rec.simple_pack_ind as simple_pack_ind,
                      I_item_rec.contains_inner_ind as contains_inner_ind,
                      I_item_rec.sellable_ind as sellable_ind,
                      I_item_rec.orderable_ind as orderable_ind,
                      I_item_rec.pack_type as pack_type,
                      I_item_rec.order_as_type as order_as_type,
                      I_item_rec.comments as comments,
                      I_item_rec.item_service_level as item_service_level,
                      I_item_rec.gift_wrap_ind as gift_wrap_ind,
                      I_item_rec.ship_alone_ind as ship_alone_ind,
                      I_item_rec.item_xform_ind as item_xform_ind,
                      I_item_rec.inventory_ind as inventory_ind,
                      I_item_rec.order_type as order_type,
                      I_item_rec.sale_type as sale_type,
                      I_item_rec.deposit_item_type as deposit_item_type,
                      I_item_rec.container_item as container_item,
                      I_item_rec.deposit_in_price_per_uom as deposit_in_price_per_uom,
                      I_item_rec.aip_case_type as aip_case_type,
                      I_item_rec.perishable_ind as perishable_ind,
                      NVL(I_item_rec.notional_pack_ind, 'N') as notional_pack_ind,
                      NVL(I_item_rec.soh_inquiry_at_pack_ind, 'N') as soh_inquiry_at_pack_ind,
                      I_item_rec.product_classification as product_classification,
                      I_item_rec.brand_name as brand_name,
                      LP_user as create_id,
                      I_item_rec.create_datetime as create_datetime,
                      I_item_rec.last_update_id as last_update_id,
                      NULL as next_upd_id,
                      SYSDATE as last_upd_datetime,
                      NULL as orig_ref_no
                 from svc_item_master
                where process_id = LP_process_id
                  and chunk_id = LP_chunk_id) temp
             on (sim.item = temp.item and temp.action = CORESVC_ITEM.ACTION_MOD)
           when matched then
         update
            set process_id = temp.process_id,
                chunk_id = temp.chunk_id,
                row_seq = temp.rowseq,
                action = temp.action,
                process$status = temp.process_status,
                item_number_type = temp.item_number_type,
                format_id = temp.format_id,
                prefix = temp.prefix,
                item_parent = temp.item_parent,
                item_grandparent = temp.item_grandparent,
                pack_ind = temp.pack_ind,
                item_level = temp.item_level,
                tran_level = temp.tran_level,
                item_aggregate_ind = temp.item_aggregate_ind,
                diff_1 = temp.diff_1,
                diff_1_aggregate_ind = temp.diff_1_aggregate_ind,
                diff_2 = temp.diff_2,
                diff_2_aggregate_ind = temp.diff_2_aggregate_ind,
                diff_3 = temp.diff_3,
                diff_3_aggregate_ind = temp.diff_3_aggregate_ind,
                diff_4 = temp.diff_4,
                diff_4_aggregate_ind = temp.diff_4_aggregate_ind,
                dept = temp.dept,
                class = temp.class,
                subclass = temp.subclass,
                status = temp.status,
                item_desc = temp.item_desc,
                item_desc_secondary = temp.item_desc_secondary,
                short_desc = temp.short_desc,
                primary_ref_item_ind = temp.primary_ref_item_ind,
                cost_zone_group_id = temp.cost_zone_group_id,
                standard_uom = temp.standard_uom,
                uom_conv_factor = temp.uom_conv_factor,
                package_size = temp.package_size,
                package_uom = temp.package_uom,
                merchandise_ind = temp.merchandise_ind,
                store_ord_mult = temp.store_ord_mult,
                forecast_ind = temp.forecast_ind,
                original_retail = temp.original_retail,
                mfg_rec_retail = temp.mfg_rec_retail,
                retail_label_type = temp.retail_label_type,
                retail_label_value = temp.retail_label_value,
                handling_temp = temp.handling_temp,
                handling_sensitivity = temp.handling_sensitivity,
                catch_weight_ind = temp.catch_weight_ind,
                waste_type = temp.waste_type,
                waste_pct = temp.waste_pct,
                default_waste_pct = temp.default_waste_pct,
                const_dimen_ind = temp.const_dimen_ind,
                simple_pack_ind = temp.simple_pack_ind,
                contains_inner_ind = temp.contains_inner_ind,
                sellable_ind = temp.sellable_ind,
                orderable_ind = temp.orderable_ind,
                pack_type = temp.pack_type,
                order_as_type = temp.order_as_type,
                comments = temp.comments,
                item_service_level = temp.item_service_level,
                gift_wrap_ind = temp.gift_wrap_ind,
                ship_alone_ind = temp.ship_alone_ind,
                item_xform_ind = temp.item_xform_ind,
                inventory_ind = temp.inventory_ind,
                order_type = temp.order_type,
                sale_type = temp.sale_type,
                deposit_item_type = temp.deposit_item_type,
                container_item = temp.container_item,
                deposit_in_price_per_uom = temp.deposit_in_price_per_uom,
                aip_case_type = temp.aip_case_type,
                perishable_ind = temp.perishable_ind,
                notional_pack_ind = temp.notional_pack_ind,
                soh_inquiry_at_pack_ind = temp.soh_inquiry_at_pack_ind,
                product_classification = temp.product_classification,
                brand_name = temp.brand_name,
                last_upd_id = temp.last_update_id,
                next_upd_id = temp.next_upd_id,
                last_upd_datetime = temp.last_upd_datetime,
                orig_ref_no = orig_ref_no
           when not matched then
         insert (process_id,
                 chunk_id,
                 row_seq,
                 action,
                 process$status,
                 item,
                 item_number_type,
                 format_id,
                 prefix,
                 item_parent,
                 item_grandparent,
                 pack_ind,
                 item_level,
                 tran_level,
                 item_aggregate_ind,
                 diff_1,
                 diff_1_aggregate_ind,
                 diff_2,
                 diff_2_aggregate_ind,
                 diff_3,
                 diff_3_aggregate_ind,
                 diff_4,
                 diff_4_aggregate_ind,
                 dept,
                 class,
                 subclass,
                 status,
                 item_desc,
                 item_desc_secondary,
                 short_desc,
                 primary_ref_item_ind,
                 cost_zone_group_id,
                 standard_uom,
                 uom_conv_factor,
                 package_size,
                 package_uom,
                 merchandise_ind,
                 store_ord_mult,
                 forecast_ind,
                 original_retail,
                 mfg_rec_retail,
                 retail_label_type,
                 retail_label_value,
                 handling_temp,
                 handling_sensitivity,
                 catch_weight_ind,
                 waste_type,
                 waste_pct,
                 default_waste_pct,
                 const_dimen_ind,
                 simple_pack_ind,
                 contains_inner_ind,
                 sellable_ind,
                 orderable_ind,
                 pack_type,
                 order_as_type,
                 comments,
                 item_service_level,
                 gift_wrap_ind,
                 ship_alone_ind,
                 item_xform_ind,
                 inventory_ind,
                 order_type,
                 sale_type,
                 deposit_item_type,
                 container_item,
                 deposit_in_price_per_uom,
                 aip_case_type,
                 perishable_ind,
                 notional_pack_ind,
                 soh_inquiry_at_pack_ind,
                 product_classification,
                 brand_name,
                 create_id,
                 create_datetime,
                 last_upd_id,
                 next_upd_id,
                 last_upd_datetime,
                 orig_ref_no)
          values(temp.process_id,
                 temp.chunk_id,
                 temp.rowseq,
                 temp.action,
                 temp.process_status,
                 temp.item,
                 temp.item_number_type,
                 temp.format_id,
                 temp.prefix,
                 temp.item_parent,
                 temp.item_grandparent,
                 temp.pack_ind,
                 temp.item_level,
                 temp.tran_level,
                 temp.item_aggregate_ind,
                 temp.diff_1,
                 temp.diff_1_aggregate_ind,
                 temp.diff_2,
                 temp.diff_2_aggregate_ind,
                 temp.diff_3,
                 temp.diff_3_aggregate_ind,
                 temp.diff_4,
                 temp.diff_4_aggregate_ind,
                 temp.dept,
                 temp.CLASS,
                 temp.subclass,
                 NVL(temp.status,'W'),
                 temp.item_desc,
                 temp.item_desc_secondary,
                 temp.short_desc,
                 temp.primary_ref_item_ind,
                 temp.cost_zone_group_id,
                 temp.standard_uom,
                 temp.uom_conv_factor,
                 temp.package_size,
                 temp.package_uom,
                 temp.merchandise_ind,
                 temp.store_ord_mult,
                 temp.forecast_ind,
                 temp.original_retail,
                 temp.mfg_rec_retail,
                 temp.retail_label_type,
                 temp.retail_label_value,
                 temp.handling_temp,
                 temp.handling_sensitivity,
                 temp.catch_weight_ind,
                 temp.waste_type,
                 temp.waste_pct,
                 temp.default_waste_pct,
                 temp.const_dimen_ind,
                 temp.simple_pack_ind,
                 temp.contains_inner_ind,
                 temp.sellable_ind,
                 temp.orderable_ind,
                 temp.pack_type,
                 temp.order_as_type,
                 temp.comments,
                 temp.item_service_level,
                 temp.gift_wrap_ind,
                 temp.ship_alone_ind,
                 temp.item_xform_ind,
                 temp.inventory_ind,
                 temp.order_type,
                 temp.sale_type,
                 temp.deposit_item_type,
                 temp.container_item,
                 temp.deposit_in_price_per_uom,
                 temp.aip_case_type,
                 temp.perishable_ind,
                 temp.notional_pack_ind,
                 temp.soh_inquiry_at_pack_ind,
                 temp.product_classification,
                 temp.brand_name,
                 temp.create_id,
                 temp.create_datetime,
                 temp.last_update_id,
                 temp.next_upd_id,
                 temp.last_upd_datetime,
                 temp.orig_ref_no);
   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item',
                                            'SVC_ITEM_MASTER',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_SVC_ITEM_MASTER;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_MASTER(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_item_rec      IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                                I_action        IN       SVC_ITEM_MASTER.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEM_MASTER';

BEGIN
   merge into svc_item_master sim
        using (select LP_process_id as process_id,
                      LP_chunk_id as chunk_id,
                      NVL(max(rownum+1), 1) as row_seq,
                      I_action as action,
                      I_item_rec.item as item,
                      LP_process_status as process_status,
                      'D' as status,
                      LP_user as create_id,
                      SYSDATE as create_datetime,
                      LP_user as last_update_id,
                      NULL as next_upd_id,
                      SYSDATE as last_upd_datetime
                 from svc_item_master
                where process_id = LP_process_id
                  and chunk_id = LP_chunk_id) temp
           on (sim.item = temp.item and temp.action = CORESVC_ITEM.ACTION_MOD)
         when matched then
       update
          set process_id = temp.process_id,
              chunk_id = temp.chunk_id,
              row_seq = temp.row_seq,
              action = temp.action,
              process$status = temp.process_status,
              status = temp.status,
              last_upd_id = temp.last_update_id,
              next_upd_id = temp.next_upd_id,
              last_upd_datetime = temp.last_upd_datetime
         when not matched then
       insert (process_id,
               chunk_id,
               row_seq,
               action,
               item,
               process$status,
               status,
               create_id,
               create_datetime,
               last_upd_id,
               next_upd_id,
               last_upd_datetime)
        values (temp.process_id,
                temp.chunk_id,
                temp.row_seq,
                temp.action,
                temp.item,
                temp.process_status,
                temp.status,
                temp.create_id,
                temp.create_datetime,
                temp.last_update_id,
                temp.next_upd_id,
                temp.last_upd_datetime);

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item',
                                            'SVC_ITEM_MASTER',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_SVC_ITEM_MASTER;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_COUNTRY(O_error_message   IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_ctry_rows       IN         "RIB_XItemCtryDesc_TBL",
                                 I_item_rec        IN         RMSSUB_XITEM.ITEM_API_REC,
                                 I_action          IN         SVC_ITEM_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEM_COUNTRY';
   L_country_attrib_row   COUNTRY_ATTRIB%ROWTYPE;

BEGIN
   if I_ctry_rows is NULL or I_ctry_rows.COUNT <= 0 then
      -- do not default the item_country record for child items
      if I_item_rec.item_master_def.item_level = 1 then
         if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                    L_country_attrib_row,
                                                    NULL,
                                                    LP_system_options_row.base_country_id) = FALSE then
            return FALSE;
         end if;
         ---
         if (L_country_attrib_row.default_loc is not NULL and
             L_country_attrib_row.default_loc_type is not NULL and
             L_country_attrib_row.localized_ind = 'N') then
              -- insert the base country id
             delete from svc_item_country 
                   where item = I_item_rec.item_master_def.item
                     and country_id = LP_system_options_row.base_country_id
                     and process$status = 'P';
              insert into svc_item_country(process_id,
                                           chunk_id,
                                           row_seq,
                                           action,
                                           process$status,
                                           item,
                                           country_id,
                                           create_id,
                                           create_datetime,
                                           last_upd_id,
                                           last_upd_datetime)
                                           select LP_process_id,
                                                  LP_chunk_id,
                                                  NVL(max(rownum+1), 1) as row_seq,
                                                  I_action,
                                                  LP_process_status,
                                                  I_item_rec.item_master_def.item,
                                                  LP_system_options_row.base_country_id,
                                                  LP_user,
                                                  SYSDATE,
                                                  LP_user,
                                                  SYSDATE
                                             from svc_item_country
                                            where process_id = LP_process_id
                                              and chunk_id = LP_chunk_id;
         end if;
      end if;
   else
      FOR i in I_ctry_rows.FIRST..I_ctry_rows.LAST LOOP
      delete from svc_item_country 
            where item = I_item_rec.item_master_def.item
              and country_id = I_ctry_rows(i).country_id
              and process$status = 'P';  
         insert into svc_item_country(process_id,
                                      chunk_id,
                                      row_seq,
                                      action,
                                      process$status,
                                      item,
                                      country_id,
                                      create_id,
                                      create_datetime,
                                      last_upd_id,
                                      last_upd_datetime)
                                      select LP_process_id,
                                             LP_chunk_id,
                                             NVL(max(rownum+1), 1) row_seq,
                                             I_action,
                                             LP_process_status,
                                             I_item_rec.item_master_def.item,
                                             I_ctry_rows(i).country_id,
                                             LP_user,
                                             SYSDATE,
                                             LP_user,
                                             SYSDATE
                                        from svc_item_country
                                       where process_id = LP_process_id
                                         and chunk_id = LP_chunk_id;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item and country id',
                                            'SVC_ITEM_COUNTRY',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_SVC_ITEM_COUNTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_COUNTRY(O_error_message   IN OUT     RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_ctry_rows       IN         "RIB_XItemCtryRef_TBL",
                                 I_item_rec        IN         RMSSUB_XITEM.ITEM_API_DEL_REC,
                                 I_action          IN         SVC_ITEM_COUNTRY.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEM_COUNTRY';
   L_country_attrib_row   COUNTRY_ATTRIB%ROWTYPE;

BEGIN
   if I_ctry_rows is NULL or I_ctry_rows.COUNT <= 0 then
      if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                 L_country_attrib_row,
                                                 NULL,
                                                 LP_system_options_row.base_country_id) = FALSE then
         return FALSE;
      end if;
      ---
      if (L_country_attrib_row.default_loc is not NULL and
          L_country_attrib_row.default_loc_type is not NULL and
          L_country_attrib_row.localized_ind = 'N') then

           -- insert the base country id
           merge into svc_item_country sic
                using (select LP_process_id as process_id,
                              LP_chunk_id as chunk_id,
                              NVL(max(rownum+1), 1) row_seq,
                              I_action as action,
                              LP_process_status as status,
                              I_item_rec.item as item,
                              LP_system_options_row.base_country_id as country_id,
                              LP_user as create_id,
                              SYSDATE as create_datetime,
                              LP_user as last_upd_id,
                              SYSDATE as last_upd_datetime
                         from svc_item_country
                        where process_id = LP_process_id
                          and chunk_id = LP_chunk_id) temp
                   on (temp.item = sic.item and
                       temp.country_id = sic.country_id and
                       temp.action = CORESVC_ITEM.ACTION_DEL)
                 when matched then
               update
                  set process_id = temp.process_id,
                      chunk_id = temp.chunk_id,
                      row_seq = temp.row_seq,
                      action = temp.action,
                      process$status = temp.status,
                      last_upd_id = temp.last_upd_id,
                      last_upd_datetime = temp.last_upd_datetime
                  when not matched then
                insert (process_id,
                        chunk_id,
                        row_seq,
                        action,
                        process$status,
                        item,
                        country_id,
                        create_id,
                        create_datetime,
                        last_upd_id,
                        last_upd_datetime)
                values (temp.process_id,
                        temp.chunk_id,
                        temp.row_seq,
                        temp.action,
                        temp.status,
                        temp.item,
                        temp.country_id,
                        temp.create_id,
                        temp.create_datetime,
                        temp.last_upd_id,
                        temp.last_upd_datetime);

      end if;
   else
      FOR i in I_ctry_rows.FIRST..I_ctry_rows.LAST LOOP
           merge into svc_item_country sic
                using (select LP_process_id as process_id,
                              LP_chunk_id as chunk_id,
                              NVL(max(rownum+1), 1) row_seq,
                              I_action as action,
                              LP_process_status as status,
                              I_item_rec.item as item,
                              I_ctry_rows(i).country_id as country_id,
                              LP_user as create_id,
                              SYSDATE as create_datetime,
                              LP_user as last_upd_id,
                              SYSDATE as last_upd_datetime
                         from svc_item_country
                        where process_id = LP_process_id
                          and chunk_id = LP_chunk_id) temp
                   on (temp.item = sic.item and
                       temp.country_id = sic.country_id and
                       temp.action = CORESVC_ITEM.ACTION_DEL)
                 when matched then
               update
                  set process_id = temp.process_id,
                      chunk_id = temp.chunk_id,
                      row_seq = temp.row_seq,
                      action = temp.action,
                      process$status = temp.status,
                      last_upd_id = temp.last_upd_id,
                      last_upd_datetime = temp.last_upd_datetime
                  when not matched then
                insert (process_id,
                        chunk_id,
                        row_seq,
                        action,
                        process$status,
                        item,
                        country_id,
                        create_id,
                        create_datetime,
                        last_upd_id,
                        last_upd_datetime)
                values (temp.process_id,
                        temp.chunk_id,
                        temp.row_seq,
                        temp.action,
                        temp.status,
                        temp.item,
                        temp.country_id,
                        temp.create_id,
                        temp.create_datetime,
                        temp.last_upd_id,
                        temp.last_upd_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item and country_id',
                                            'SVC_ITEM_COUNTRY',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_SVC_ITEM_COUNTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IMAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                          I_action          IN       SVC_ITEM_IMAGE.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)       := 'RMSSUB_XITEM_SQL.INSERT_SVC_IMAGE';

BEGIN

   if I_item_rec.images.image_name is NOT NULL and I_item_rec.images.image_name.COUNT > 0 then
      FOR i in I_item_rec.images.image_name.FIRST..I_item_rec.images.image_name.LAST LOOP
         delete svc_item_image
         where item           = I_item_rec.item_master_def.item
           and image_name     = I_item_rec.images.image_name(i)
           and process$status = 'P';
         merge into svc_item_image sii
         using (select LP_process_id as process_id,
                       LP_chunk_id as chunk_id,
                       NVL(MAX(rownum+1), 1) as row_seq,
                       I_action as action,
                       LP_process_status as process_status,
                       I_item_rec.item_master_def.item as item,
                       I_item_rec.images.image_name(i) as image_name,
                       I_item_rec.images.image_addr(i) as image_addr,
                       I_item_rec.images.image_desc(i) as image_desc,
                       I_item_rec.images.image_type(i) as image_type,
                       I_item_rec.images.primary_ind(i) as primary_ind,
                       I_item_rec.images.display_priority(i) as display_priority,
                       LP_user as create_id,
                       SYSDATE as create_datetime,
                       LP_user as last_update_id,
                       SYSDATE as last_update_datetime
                  from svc_item_image
                 where process_id = LP_process_id
                   and chunk_id = LP_chunk_id) temp
            on (temp.image_name = sii.image_name
                and temp.item = sii.item
                and temp.action = CORESVC_ITEM.ACTION_MOD)
         when matched then
            update set process_id           = temp.process_id,
                       chunk_id             = temp.chunk_id,
                       row_seq              = temp.row_seq,
                       action               = temp.action,
                       process$status       = temp.process_status,
                       image_addr           = temp.image_addr,
                       image_desc           = temp.image_desc,
                       image_type           = temp.image_type,
                       primary_ind          = temp.primary_ind,
                       display_priority     = temp.display_priority,
                       last_update_id       = temp.last_update_id,
                       last_update_datetime = temp.last_update_datetime
         when not matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    item,
                    image_name,
                    image_addr,
                    image_desc,
                    image_type,
                    primary_ind,
                    display_priority,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (temp.process_id,
                    temp.chunk_id,
                    temp.row_seq,
                    temp.action,
                    temp.process_status,
                    temp.item,
                    temp.image_name,
                    temp.image_addr,
                    temp.image_desc,
                    temp.image_type,
                    temp.primary_ind,
                    temp.display_priority,
                    temp.create_id,
                    temp.create_datetime,
                    temp.last_update_id,
                    temp.last_update_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item and image name',
                                            'SVC_ITEM_IMAGE',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_SVC_IMAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IMAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                          I_action          IN       SVC_ITEM_IMAGE.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_IMAGE';

BEGIN

   if I_item_rec.images.image_name is NOT NULL and I_item_rec.images.image_name.COUNT > 0 then
      FOR i in I_item_rec.images.image_name.FIRST..I_item_rec.images.image_name.LAST LOOP
         merge into svc_item_image sii
         using (select LP_process_id as process_id,
                       LP_chunk_id as chunk_id,
                       NVL(MAX(rownum+1), 1) as row_seq,
                       I_action as action,
                       LP_process_status as process_status,
                       I_item_rec.item as item,
                       I_item_rec.images.image_name(i) as image_name,
                       LP_user as create_id,
                       SYSDATE as create_datetime,
                       LP_user as last_update_id,
                       SYSDATE as last_update_datetime
                  from svc_item_image
                 where process_id = LP_process_id
                   and chunk_id = LP_chunk_id) temp
            on (temp.image_name = sii.image_name
                and temp.item = sii.item
                and temp.action = CORESVC_ITEM.ACTION_DEL)
         when matched then
            update set process_id           = temp.process_id,
                       chunk_id             = temp.chunk_id,
                       row_seq              = temp.row_seq,
                       action               = temp.action,
                       process$status       = temp.process_status,
                       last_update_id       = temp.last_update_id,
                       last_update_datetime = temp.last_update_datetime
         when not matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    item,
                    image_name,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (temp.process_id,
                    temp.chunk_id,
                    temp.row_seq,
                    temp.action,
                    temp.process_status,
                    temp.item,
                    temp.image_name,
                    temp.create_id,
                    temp.create_datetime,
                    temp.last_update_id,
                    temp.last_update_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item and image name',
                                            'SVC_ITEM_IMAGE',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_SVC_IMAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                            I_action          IN       SVC_ITEM_MASTER_TL.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEM_TL';

BEGIN

   if I_item_rec.Item_Master_lang.lang is NOT NULL and I_item_rec.Item_Master_lang.lang.COUNT > 0 then
      FOR i in I_item_rec.Item_Master_lang.lang.FIRST..I_item_rec.Item_Master_lang.lang.LAST LOOP
         delete from svc_item_master_tl 
               where item = I_item_rec.item_master_def.item 
                 and process$status = 'P';
         merge into svc_item_master_tl siml
         using (select LP_process_id as process_id,
                       LP_chunk_id as chunk_id,
                       NVL(MAX(rownum+1), 1) as row_seq,
                       I_action as action,
                       LP_process_status as process_status,
                       I_item_rec.Item_Master_lang.lang(i) as lang,
                       I_item_rec.item_master_def.item as item,
                       I_item_rec.Item_Master_lang.item_desc(i) as item_desc,
                       I_item_rec.Item_Master_lang.item_desc_sec(i) as item_desc_secondary,
                       I_item_rec.Item_Master_lang.short_desc(i) as short_desc,
                       LP_user as create_id,
                       SYSDATE as create_datetime,
                       LP_user as last_update_id,
                       SYSDATE as last_update_datetime
                  from svc_item_master_tl
                 where process_id = LP_process_id
                   and chunk_id = LP_chunk_id) temp
            on (temp.item = siml.item
                and temp.lang = siml.lang
                and temp.action = CORESVC_ITEM.ACTION_MOD)
         when matched then
            update set process_id           = temp.process_id,
                       chunk_id             = temp.chunk_id,
                       row_seq              = temp.row_seq,
                       action               = temp.action,
                       process$status       = temp.process_status,
                       item_desc            = temp.item_desc,
                       item_desc_secondary  = temp.item_desc_secondary,
                       short_desc           = temp.short_desc,
                       last_update_id       = temp.last_update_id,
                       last_update_datetime = temp.last_update_datetime
         when not matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    lang,
                    item,
                    item_desc,
                    item_desc_secondary,
                    short_desc,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (temp.process_id,
                    temp.chunk_id,
                    temp.row_seq,
                    temp.action,
                    temp.process_status,
                    temp.lang,
                    temp.item,
                    temp.item_desc,
                    temp.item_desc_secondary,
                    temp.short_desc,
                    temp.create_id,
                    temp.create_datetime,
                    temp.last_update_id,
                    temp.last_update_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item and lang',
                                            'SVC_ITEM_MASTER_TL',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_SVC_ITEM_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ITEM_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                            I_action          IN       SVC_ITEM_MASTER_TL.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ITEM_TL';

BEGIN

   if I_item_rec.Item_Master_lang.lang is NOT NULL and I_item_rec.Item_Master_lang.lang.COUNT > 0 then
      FOR i in I_item_rec.Item_Master_lang.lang.FIRST..I_item_rec.Item_Master_lang.lang.LAST LOOP
         merge into svc_item_master_tl siml
         using (select LP_process_id as process_id,
                       LP_chunk_id as chunk_id,
                       NVL(MAX(rownum+1), 1) as row_seq,
                       I_action as action,
                       LP_process_status as process_status,
                       I_item_rec.Item_Master_lang.lang(i) as lang,
                       I_item_rec.item as item,
                       LP_user as create_id,
                       SYSDATE as create_datetime,
                       LP_user as last_update_id,
                       SYSDATE as last_update_datetime
                  from svc_item_master_tl
                 where process_id = LP_process_id
                   and chunk_id = LP_chunk_id) temp
            on (temp.item = siml.item
                and temp.lang = siml.lang
                and temp.action = CORESVC_ITEM.ACTION_DEL)
         when matched then
            update set process_id           = temp.process_id,
                       chunk_id             = temp.chunk_id,
                       row_seq              = temp.row_seq,
                       action               = temp.action,
                       process$status       = temp.process_status,
                       last_update_id       = temp.last_update_id,
                       last_update_datetime = temp.last_update_datetime
         when not matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    lang,
                    item,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (temp.process_id,
                    temp.chunk_id,
                    temp.row_seq,
                    temp.action,
                    temp.process_status,
                    temp.lang,
                    temp.item,
                    temp.create_id,
                    temp.create_datetime,
                    temp.last_update_id,
                    temp.last_update_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item and lang',
                                            'SVC_ITEM_MASTER_TL',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_SVC_ITEM_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                            I_action          IN       SVC_ITEM_SUPPLIER_TL.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISUP_TL';

BEGIN

   if I_item_rec.Item_Supp_lang.lang is NOT NULL and I_item_rec.Item_Supp_lang.lang.COUNT > 0 then
      FOR i in I_item_rec.Item_Supp_lang.lang.FIRST..I_item_rec.Item_Supp_lang.lang.LAST LOOP
         delete from svc_item_supplier_tl
               where item = I_item_rec.item_master_def.item
                 and supplier =  I_item_rec.Item_Supp_lang.supplier(i)
                 and process$status  = 'P';

         merge into svc_item_supplier_tl sisl
         using (select LP_process_id as process_id,
                       LP_chunk_id as chunk_id,
                       NVL(MAX(rownum+1), 1) as row_seq,
                       I_action as action,
                       LP_process_status as process_status,
                       I_item_rec.Item_Supp_lang.lang(i) as lang,
                       I_item_rec.item_master_def.item as item,
                       I_item_rec.Item_Supp_lang.supplier(i) as supplier,
                       I_item_rec.Item_Supp_lang.supp_label(i) as supp_label,
                       I_item_rec.Item_Supp_lang.supp_diff_1(i) as supp_diff_1,
                       I_item_rec.Item_Supp_lang.supp_diff_2(i) as supp_diff_2,
                       I_item_rec.Item_Supp_lang.supp_diff_3(i) as supp_diff_3,
                       I_item_rec.Item_Supp_lang.supp_diff_4(i) as supp_diff_4,
                       LP_user as create_id,
                       SYSDATE as create_datetime,
                       LP_user as last_update_id,
                       SYSDATE as last_update_datetime
                  from svc_item_supplier_tl
                 where process_id = LP_process_id
                   and chunk_id = LP_chunk_id) temp
            on (temp.item = sisl.item
                and temp.lang = sisl.lang
                and temp.supplier = sisl.supplier
                and temp.action = CORESVC_ITEM.ACTION_MOD)
         when matched then
            update set process_id           = temp.process_id,
                       chunk_id             = temp.chunk_id,
                       row_seq              = temp.row_seq,
                       action               = temp.action,
                       process$status       = temp.process_status,
                       supp_label           = temp.supp_label,
                       supp_diff_1          = temp.supp_diff_1,
                       supp_diff_2          = temp.supp_diff_2,
                       supp_diff_3          = temp.supp_diff_3,
                       supp_diff_4          = temp.supp_diff_4,
                       last_update_id       = temp.last_update_id,
                       last_update_datetime = temp.last_update_datetime
         when not matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    lang,
                    item,
                    supplier,
                    supp_label,
                    supp_diff_1,
                    supp_diff_2,
                    supp_diff_3,
                    supp_diff_4,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (temp.process_id,
                    temp.chunk_id,
                    temp.row_seq,
                    temp.action,
                    temp.process_status,
                    temp.lang,
                    temp.item,
                    temp.supplier,
                    temp.supp_label,
                    temp.supp_diff_1,
                    temp.supp_diff_2,
                    temp.supp_diff_3,
                    temp.supp_diff_4,
                    temp.create_id,
                    temp.create_datetime,
                    temp.last_update_id,
                    temp.last_update_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, supplier and lang',
                                            'SVC_ITEM_SUPPLIER_TL',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_SVC_ISUP_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_ISUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                            I_action          IN       SVC_ITEM_SUPPLIER_TL.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_ISUP_TL';

BEGIN

   if I_item_rec.Item_Supp_lang.lang is NOT NULL and I_item_rec.Item_Supp_lang.lang.COUNT > 0 then
      FOR i in I_item_rec.Item_Supp_lang.lang.FIRST..I_item_rec.Item_Supp_lang.lang.LAST LOOP
         merge into svc_item_supplier_tl sisl
         using (select LP_process_id as process_id,
                       LP_chunk_id as chunk_id,
                       NVL(MAX(rownum+1), 1) as row_seq,
                       I_action as action,
                       LP_process_status as process_status,
                       I_item_rec.Item_Supp_lang.lang(i) as lang,
                       I_item_rec.item as item,
                       I_item_rec.Item_Supp_lang.supplier(i) as supplier,
                       LP_user as create_id,
                       SYSDATE as create_datetime,
                       LP_user as last_update_id,
                       SYSDATE as last_update_datetime
                  from svc_item_supplier_tl
                 where process_id = LP_process_id
                   and chunk_id = LP_chunk_id) temp
            on (temp.item = sisl.item
                and temp.lang = sisl.lang
                and temp.supplier = sisl.supplier
                and temp.action = CORESVC_ITEM.ACTION_DEL)
         when matched then
            update set process_id           = temp.process_id,
                       chunk_id             = temp.chunk_id,
                       row_seq              = temp.row_seq,
                       action               = temp.action,
                       process$status       = temp.process_status,
                       last_update_id       = temp.last_update_id,
                       last_update_datetime = temp.last_update_datetime
         when not matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    lang,
                    item,
                    supplier,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (temp.process_id,
                    temp.chunk_id,
                    temp.row_seq,
                    temp.action,
                    temp.process_status,
                    temp.lang,
                    temp.item,
                    temp.supplier,
                    temp.create_id,
                    temp.create_datetime,
                    temp.last_update_id,
                    temp.last_update_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, supplier and lang',
                                            'SVC_ITEM_SUPPLIER_TL',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_SVC_ISUP_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IIMG_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_REC,
                            I_action          IN       SVC_ITEM_IMAGE_TL.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_IIMG_TL';

BEGIN

   if I_item_rec.Item_Images_lang.lang is NOT NULL and I_item_rec.Item_Images_lang.lang.COUNT > 0 then
      FOR i in I_item_rec.Item_Images_lang.lang.FIRST..I_item_rec.Item_Images_lang.lang.LAST LOOP

         delete from svc_item_image_tl
               where item = I_item_rec.item_master_def.item
                 and process$status  = 'P';

         merge into svc_item_image_tl siil
         using (select LP_process_id as process_id,
                       LP_chunk_id as chunk_id,
                       NVL(MAX(rownum+1), 1) as row_seq,
                       I_action as action,
                       LP_process_status as process_status,
                       I_item_rec.Item_Images_lang.lang(i) as lang,
                       I_item_rec.item_master_def.item as item,
                       I_item_rec.Item_Images_lang.image_name(i) as image_name,
                       I_item_rec.Item_Images_lang.image_desc(i) as image_desc,
                       LP_user as create_id,
                       SYSDATE as create_datetime,
                       LP_user as last_update_id,
                       SYSDATE as last_update_datetime
                  from svc_item_image_tl
                 where process_id = LP_process_id
                   and chunk_id = LP_chunk_id) temp
            on (temp.item = siil.item
                and temp.lang = siil.lang
                and temp.image_name = siil.image_name
                and temp.action = CORESVC_ITEM.ACTION_MOD)
         when matched then
            update set process_id           = temp.process_id,
                       chunk_id             = temp.chunk_id,
                       row_seq              = temp.row_seq,
                       action               = temp.action,
                       process$status       = temp.process_status,
                       image_desc           = temp.image_desc,
                       last_update_id       = temp.last_update_id,
                       last_update_datetime = temp.last_update_datetime
         when not matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    lang,
                    item,
                    image_name,
                    image_desc,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (temp.process_id,
                    temp.chunk_id,
                    temp.row_seq,
                    temp.action,
                    temp.process_status,
                    temp.lang,
                    temp.item,
                    temp.image_name,
                    temp.image_desc,
                    temp.create_id,
                    temp.create_datetime,
                    temp.last_update_id,
                    temp.last_update_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, image name and lang',
                                            'SVC_ITEM_IMAGE_TL',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_SVC_IIMG_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SVC_IIMG_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       RMSSUB_XITEM.ITEM_API_DEL_REC,
                            I_action          IN       SVC_ITEM_IMAGE_TL.ACTION%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RMSSUB_XITEM_SQL.INSERT_SVC_IIMG_TL';

BEGIN

   if I_item_rec.Item_Images_lang.lang is NOT NULL and I_item_rec.Item_Images_lang.lang.COUNT > 0 then
      FOR i in I_item_rec.Item_Images_lang.lang.FIRST..I_item_rec.Item_Images_lang.lang.LAST LOOP
         merge into svc_item_image_tl siil
         using (select LP_process_id as process_id,
                       LP_chunk_id as chunk_id,
                       NVL(MAX(rownum+1), 1) as row_seq,
                       I_action as action,
                       LP_process_status as process_status,
                       I_item_rec.Item_Images_lang.lang(i) as lang,
                       I_item_rec.item as item,
                       I_item_rec.Item_Images_lang.image_name(i) as image_name,
                       LP_user as create_id,
                       SYSDATE as create_datetime,
                       LP_user as last_update_id,
                       SYSDATE as last_update_datetime
                  from svc_item_image_tl
                 where process_id = LP_process_id
                   and chunk_id = LP_chunk_id) temp
            on (temp.item = siil.item
                and temp.lang = siil.lang
                and temp.image_name = siil.image_name
                and temp.action = CORESVC_ITEM.ACTION_DEL)
         when matched then
            update set process_id           = temp.process_id,
                       chunk_id             = temp.chunk_id,
                       row_seq              = temp.row_seq,
                       action               = temp.action,
                       process$status       = temp.process_status,
                       last_update_id       = temp.last_update_id,
                       last_update_datetime = temp.last_update_datetime
         when not matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    lang,
                    item,
                    image_name,
                    create_id,
                    create_datetime,
                    last_update_id,
                    last_update_datetime)
            values (temp.process_id,
                    temp.chunk_id,
                    temp.row_seq,
                    temp.action,
                    temp.process_status,
                    temp.lang,
                    temp.item,
                    temp.image_name,
                    temp.create_id,
                    temp.create_datetime,
                    temp.last_update_id,
                    temp.last_update_datetime);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS',
                                            'item, image name and lang',
                                            'SVC_ITEM_IMAGE_TL',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_SVC_IIMG_TL;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XITEM_SQL;
/
