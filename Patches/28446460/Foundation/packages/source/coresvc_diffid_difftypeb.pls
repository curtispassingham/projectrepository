CREATE OR REPLACE PACKAGE BODY CORESVC_DIFFID_DIFFTYPE AS
   cursor C_SVC_DIFF_IDS (I_process_id   NUMBER,
                          I_chunk_id     NUMBER) is
      select pk_diff_ids.rowid      as pk_diff_ids_rid,
             did_dte_fk.rowid       as did_dte_fk_rid,
             st.rowid               as st_rid,
             st.industry_subgroup,
             st.industry_code,
             st.diff_desc,
             UPPER(st.diff_type)    as diff_type,
             UPPER(st.diff_id)      as diff_id,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             pk_diff_ids.diff_type  as old_diff_type,
             pk_diff_ids.diff_desc  as old_diff_desc,
             UPPER(st.action)       as action,
             st.process$status
        from svc_diff_ids st,
             diff_ids pk_diff_ids,
             diff_type did_dte_fk
       where st.process_id          = I_process_id
         and st.chunk_id            = I_chunk_id
         and UPPER(st.diff_id)      = pk_diff_ids.diff_id (+)
         and UPPER(st.diff_type)    = did_dte_fk.diff_type (+);
   
   cursor C_SVC_DIFF_TYPE(I_process_id NUMBER,
                          I_chunk_id   NUMBER)  is
      select pk_diff_type.rowid  as pk_diff_type_rid,
             st.diff_type_desc,
             UPPER(st.diff_type) as diff_type,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)    as action,
             st.process$status
        from svc_diff_type st,
             diff_type pk_diff_type
       where st.process_id          = I_process_id
         and st.chunk_id            = I_chunk_id
         and UPPER(st.diff_type)    = pk_diff_type.diff_type (+)
         and NVL(st.action,'X') <> CORESVC_DIFFID_DIFFTYPE.action_del;  

   cursor C_SVC_DIFF_TYPE_DEL(I_process_id NUMBER,
                              I_chunk_id   NUMBER)  is
      select pk_diff_type.rowid  as pk_diff_type_rid,
             UPPER(st.diff_type) as diff_type,
             st.process_id,
             st.chunk_id,
             st.row_seq
        from svc_diff_type st,
             diff_type pk_diff_type
       where st.process_id          = I_process_id
         and st.chunk_id            = I_chunk_id
         and UPPER(st.diff_type)    = pk_diff_type.diff_type (+)
         and st.action = CORESVC_DIFFID_DIFFTYPE.action_del;

   TYPE errors_tab_typ is TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ is TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   TYPE DIFF_IDS_TL_TAB IS TABLE OF DIFF_IDS_TL%ROWTYPE;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   TYPE DIFF_TYPE_TL_TAB IS TABLE OF DIFF_TYPE_TL%ROWTYPE;
   
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;

   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if SHEET_NAME_TRANS.EXISTS(I_sheet_name) then
      return SHEET_NAME_TRANS(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
---------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet   IN VARCHAR2,
                          I_row_seq IN NUMBER,
                          I_col     IN VARCHAR2,
                          I_sqlcode IN NUMBER,
                          I_sqlerrm IN VARCHAR2) IS
BEGIN

   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := s9t_errors_seq.nextval;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).row_seq              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key            := (
                                                                        CASE
                                                                           WHEN I_sqlcode is NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;

END WRITE_S9T_ERROR;
---------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets                S9T_PKG.NAMES_MAP_TYP;
   DIFF_IDS_cols           S9T_PKG.NAMES_MAP_TYP;
   DIFF_IDS_TL_cols        S9T_PKG.NAMES_MAP_TYP;
   
   DIFF_TYPE_cols          S9T_PKG.NAMES_MAP_TYP;
   DIFF_TYPE_TL_cols       S9T_PKG.NAMES_MAP_TYP;
   DIFF_TYPE_CFA_EXT_cols  S9T_PKG.NAMES_MAP_TYP;
   
   FUNCTION get_col_indx(I_names     IN   S9T_PKG.NAMES_MAP_TYP,
                         I_col_key   IN   VARCHAR2)
   RETURN NUMBER IS
   BEGIN
      if I_names.exists(I_col_key) then
         return I_names(I_col_key);
      else
         return NULL;
      end if;
   END get_col_indx;

BEGIN

   L_sheets                       := S9T_PKG.GET_SHEET_NAMES(I_file_id); 
   DIFF_TYPE_cols                 := S9T_PKG.GET_COL_NAMES(I_file_id,DIFF_TYPE_SHEET);
   DIFF_TYPE$action               := DIFF_TYPE_cols('ACTION');
   DIFF_TYPE$diff_type            := DIFF_TYPE_cols('DIFF_TYPE');
   DIFF_TYPE$diff_type_desc       := DIFF_TYPE_cols('DIFF_TYPE_DESC');

   DIFF_TYPE_TL_cols              := S9T_PKG.GET_COL_NAMES(I_file_id,DIFF_TYPE_TL_SHEET);
   DIFF_TYPE_TL$action            := DIFF_TYPE_TL_cols('ACTION');
   DIFF_TYPE_TL$lang              := DIFF_TYPE_TL_cols('LANG');
   DIFF_TYPE_TL$diff_type         := DIFF_TYPE_TL_cols('DIFF_TYPE');
   DIFF_TYPE_TL$diff_type_desc    := DIFF_TYPE_TL_cols('DIFF_TYPE_DESC');
   
   --DIFF_TYPE_CFA_EXT
   DIFF_TYPE_CFA_EXT_cols         := S9T_PKG.GET_COL_NAMES(I_file_id,DIFF_TYPE_CFA_EXT_sheet);
   DIFF_TYPE_CFA_EXT$Action       := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'ACTION');
   DIFF_TYPE_CFA_EXT$DIFF_TYPE    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'DIFF_TYPE');
   DIFF_TYPE_CFA_EXT$GROUP_ID     := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'GROUP_ID');
   DIFF_TYPE_CFA_EXT$VARCHAR2_1   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_1');
   DIFF_TYPE_CFA_EXT$VARCHAR2_2   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_2');
   DIFF_TYPE_CFA_EXT$VARCHAR2_3   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_3');
   DIFF_TYPE_CFA_EXT$VARCHAR2_4   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_4');
   DIFF_TYPE_CFA_EXT$VARCHAR2_5   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_5');
   DIFF_TYPE_CFA_EXT$VARCHAR2_6   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_6');
   DIFF_TYPE_CFA_EXT$VARCHAR2_7   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_7');
   DIFF_TYPE_CFA_EXT$VARCHAR2_8   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_8');
   DIFF_TYPE_CFA_EXT$VARCHAR2_9   := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_9');
   DIFF_TYPE_CFA_EXT$VARCHAR2_10  := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'VARCHAR2_10');
   DIFF_TYPE_CFA_EXT$NUMBER_11    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_11');
   DIFF_TYPE_CFA_EXT$NUMBER_12    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_12');
   DIFF_TYPE_CFA_EXT$NUMBER_13    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_13');
   DIFF_TYPE_CFA_EXT$NUMBER_14    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_14');
   DIFF_TYPE_CFA_EXT$NUMBER_15    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_15');
   DIFF_TYPE_CFA_EXT$NUMBER_16    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_16');
   DIFF_TYPE_CFA_EXT$NUMBER_17    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_17');
   DIFF_TYPE_CFA_EXT$NUMBER_18    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_18');
   DIFF_TYPE_CFA_EXT$NUMBER_19    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_19');
   DIFF_TYPE_CFA_EXT$NUMBER_20    := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'NUMBER_20');
   DIFF_TYPE_CFA_EXT$DATE_21      := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'DATE_21');
   DIFF_TYPE_CFA_EXT$DATE_22      := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'DATE_22');
   DIFF_TYPE_CFA_EXT$DATE_23      := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'DATE_23');
   DIFF_TYPE_CFA_EXT$DATE_24      := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'DATE_24');
   DIFF_TYPE_CFA_EXT$DATE_25      := get_col_indx(DIFF_TYPE_CFA_EXT_cols,'DATE_25');
   
   DIFF_IDS_cols                  := S9T_PKG.GET_COL_NAMES(I_file_id,DIFF_IDS_sheet);
   DIFF_IDS$Action                := DIFF_IDS_cols('ACTION');
   DIFF_IDS$DIFF_ID               := DIFF_IDS_cols('DIFF_ID');
   DIFF_IDS$DIFF_DESC             := DIFF_IDS_cols('DIFF_DESC');
   DIFF_IDS$DIFF_TYPE             := DIFF_IDS_cols('DIFF_TYPE');
   DIFF_IDS$INDUSTRY_CODE         := DIFF_IDS_cols('INDUSTRY_CODE');
   DIFF_IDS$INDUSTRY_SUBGROUP     := DIFF_IDS_cols('INDUSTRY_SUBGROUP');

   DIFF_IDS_TL_cols               := S9T_PKG.GET_COL_NAMES(I_file_id,DIFF_IDS_TL_SHEET);
   DIFF_IDS_TL$Action             := DIFF_IDS_TL_cols('ACTION');
   DIFF_IDS_TL$LANG               := DIFF_IDS_TL_cols('LANG');
   DIFF_IDS_TL$DIFF_ID            := DIFF_IDS_TL_cols('DIFF_ID');
   DIFF_IDS_TL$DIFF_DESC          := DIFF_IDS_TL_cols('DIFF_DESC');

END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_DIFF_TYPE(I_file_id IN NUMBER) IS
BEGIN
   insert into table
         (select ss.s9t_rows
            from s9t_folder sf,
                 TABLE(sf.s9t_file_obj.sheets) ss
           where sf.file_id = I_file_id
             and ss.sheet_name = DIFF_TYPE_SHEET)
          select s9t_row(s9t_cells(CORESVC_DIFFID_DIFFTYPE.action_mod,
                                   diff_type,
                                   diff_type_desc))
            from diff_type;
END POPULATE_DIFF_TYPE;
----------------------------------------------------------------------------------------
PROCEDURE POPULATE_DIFF_TYPE_TL(I_file_id IN NUMBER) IS
BEGIN
   insert into table
         (select ss.s9t_rows
            from s9t_folder sf,
                 TABLE(sf.s9t_file_obj.sheets) ss
           where sf.file_id = I_file_id
             and ss.sheet_name = DIFF_TYPE_TL_SHEET)
          select s9t_row(s9t_cells(CORESVC_DIFFID_DIFFTYPE.action_mod,
                                   lang,
                                   diff_type,
                                   diff_type_desc))
            from diff_type_tl;
END POPULATE_DIFF_TYPE_TL;
------------------------------------------------------------------------------------------
PROCEDURE POPULATE_DIFF_IDS(I_file_id IN NUMBER) IS
BEGIN
   insert into table
       (select ss.s9t_rows
          from s9t_folder sf,
               TABLE(sf.s9t_file_obj.sheets) ss
         where sf.file_id = I_file_id
           and ss.sheet_name = DIFF_IDS_sheet)
        select s9t_row(s9t_cells(CORESVC_DIFFID_DIFFTYPE.action_mod,
                                 DIFF_ID,
                                 DIFF_DESC,
                                 DIFF_TYPE,
                                 INDUSTRY_CODE,
                                 INDUSTRY_SUBGROUP))
          from diff_ids;
END POPULATE_DIFF_IDS;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_DIFF_IDS_TL(I_file_id IN NUMBER) IS
BEGIN
   insert into table
       (select ss.s9t_rows
          from s9t_folder sf,
               TABLE(sf.s9t_file_obj.sheets) ss
         where sf.file_id = I_file_id
           and ss.sheet_name = DIFF_IDS_TL_SHEET)
        select s9t_row(s9t_cells(CORESVC_DIFFID_DIFFTYPE.action_mod,
                                 LANG,
                                 DIFF_ID,
                                 DIFF_DESC))
          from diff_ids_tl;
END POPULATE_DIFF_IDS_TL;
-----------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id IN OUT NUMBER) IS
   L_file        S9T_FILE;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;
BEGIN

   L_file               := NEW s9t_file();
   O_file_id            := s9t_folder_seq.nextval;
   L_file.file_id       := O_file_id;
   L_file_name          := template_key||'_'||USER||'_'||sysdate||'.ods';
   L_file.file_name     := L_file_name;
   L_file.template_key  := template_key;
   L_file.user_lang     := GET_USER_LANG;
   L_file.add_sheet(DIFF_TYPE_SHEET);
   L_file.sheets(L_file.get_sheet_index(DIFF_TYPE_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                      'DIFF_TYPE',
                                                                                      'DIFF_TYPE_DESC');

   L_file.add_sheet(DIFF_TYPE_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(DIFF_TYPE_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                         'LANG',
                                                                                         'DIFF_TYPE',
                                                                                         'DIFF_TYPE_DESC');

   L_file.add_sheet(DIFF_IDS_sheet);
   L_file.sheets(L_file.GET_SHEET_INDEX(DIFF_IDS_sheet)).column_headers := s9t_cells('ACTION',
                                                                                     'DIFF_ID',
                                                                                     'DIFF_DESC',
                                                                                     'DIFF_TYPE',
                                                                                     'INDUSTRY_CODE',
                                                                                     'INDUSTRY_SUBGROUP');

   L_file.add_sheet(DIFF_IDS_TL_SHEET);
   L_file.sheets(L_file.GET_SHEET_INDEX(DIFF_IDS_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                        'LANG',
                                                                                        'DIFF_ID',
                                                                                        'DIFF_DESC');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
---------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.CREATE_S9T';
   L_file      S9T_FILE;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_DIFF_TYPE(O_file_id);
      POPULATE_DIFF_TYPE_TL(O_file_id);
      POPULATE_DIFF_IDS(O_file_id);
      POPULATE_DIFF_IDS_TL(O_file_id);
      COMMIT;
   end if;
   ---
   if CORESVC_CFLEX.ADD_CFLEX_SHEETS(O_error_message,
                                     O_file_id,
                                     template_key,
                                     I_template_only_ind) = FALSE   then
      return FALSE;
   end if;
   ---
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DIFF_TYPE(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id IN SVC_DIFF_TYPE.PROCESS_ID%TYPE) IS
   TYPE svc_diff_type_col_typ is TABLE OF SVC_DIFF_TYPE%ROWTYPE;
   L_temp_rec          SVC_DIFF_TYPE%ROWTYPE;
   svc_diff_type_col   svc_diff_type_col_typ          := NEW svc_diff_type_col_typ();
   L_process_id        SVC_DIFF_TYPE.PROCESS_ID%TYPE;
   L_error             BOOLEAN                        := FALSE;
   L_default_rec       SVC_DIFF_TYPE%ROWTYPE;

   cursor C_MANDATORY_IND is
      select diff_type_desc_mi,
             diff_type_mi,
             1 as dummy
        from(select column_key,
                    mandatory
               from s9t_tmpl_cols_def
              where template_key                = template_key
                and wksht_key                   = 'DIFF_TYPE')
            PIVOT (MAX(mandatory) as mi
              FOR (column_key) IN ( 'DIFF_TYPE'      as diff_type,
                                    'DIFF_TYPE_DESC' as diff_type_desc,
                                     null            as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DIFF_TYPE';
   L_pk_columns    VARCHAR2(255)  := 'Diff Type';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN
      (select diff_type_dv,
              diff_type_desc_dv,
              null as dummy
         from(select column_key,
                    default_value
               from s9t_tmpl_cols_def
              where template_key                    =  template_key
                and wksht_key                       = 'DIFF_TYPE')
            PIVOT(MAX(default_value) AS dv
              FOR (column_key) IN ('DIFF_TYPE'      as diff_type,
                                   'DIFF_TYPE_DESC' as diff_type_desc,
                                    NULL            as dummy)))

   LOOP
      BEGIN
         L_default_rec.diff_type_desc := rec.diff_type_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DIFF_TYPE',
                            NULL,
                            'DIFF_TYPE_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.diff_type := rec.diff_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DIFF_TYPE',
                            NULL,
                            'DIFF_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select UPPER(r.get_cell(diff_type$action))       as action,
                     UPPER(r.get_cell(diff_type$diff_type))    as diff_type,
                     r.get_cell(diff_type$diff_type_desc)      as diff_type_desc,
                     r.get_row_seq()                           as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(DIFF_TYPE_SHEET))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            if C_MANDATORY_IND%ISOPEN then
                 close C_MANDATORY_IND;
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_type_desc := rec.diff_type_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            rec.row_seq,
                            'DIFF_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_type := rec.diff_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            rec.row_seq,
                            'DIFF_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_DIFFID_DIFFTYPE.action_new then
         L_temp_rec.diff_type      := NVL(L_temp_rec.diff_type,L_default_rec.diff_type);
         L_temp_rec.diff_type_desc := NVL(L_temp_rec.diff_type_desc,L_default_rec.diff_type_desc);
      end if;
      if NOT (L_temp_rec.diff_type is NOT NULL and
              1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         DIFF_TYPE_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_diff_type_col.extend();
         svc_diff_type_col(svc_diff_type_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_diff_type_col.COUNT SAVE EXCEPTIONS
      merge into svc_diff_type st
      using(select   (case
                      when l_mi_rec.diff_type_mi       = 'N'
                       and svc_diff_type_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                       and s1.diff_type      is NULL
                      then mt.diff_type
                      else s1.diff_type
                  end) as diff_type,
                     (case
                      when l_mi_rec.diff_type_desc_mi    = 'N'
                       and svc_diff_type_col(i).action   = CORESVC_DIFFID_DIFFTYPE.action_mod
                       and s1.diff_type_desc is NULL
                      then mt.diff_type_desc
                      else s1.diff_type_desc
                  end) as diff_type_desc,
                   null as dummy
              from (select svc_diff_type_col(i).diff_type_desc as diff_type_desc,
                           svc_diff_type_col(i).diff_type      as diff_type,
                           null as dummy
                      from dual)s1,
                    diff_type mt
             where mt.diff_type (+)     = s1.diff_type
               and 1 = 1) sq
                on (st.diff_type      = sq.diff_type
                    and svc_diff_type_col(i).action in (CORESVC_DIFFID_DIFFTYPE.action_mod,
                                                        CORESVC_DIFFID_DIFFTYPE.action_del))
      when matched then
         update
            set process_id        = svc_diff_type_col(i).process_id,
                chunk_id          = svc_diff_type_col(i).chunk_id,
                row_seq           = svc_diff_type_col(i).row_seq,
                action            = svc_diff_type_col(i).action,
                process$status    = svc_diff_type_col(i).process$status,
                diff_type_desc    = sq.diff_type_desc,
                create_id         = svc_diff_type_col(i).create_id,
                create_datetime   = svc_diff_type_col(i).create_datetime,
                last_upd_id       = svc_diff_type_col(i).last_upd_id,
                last_upd_datetime = svc_diff_type_col(i).last_upd_datetime
      when not matched then
         insert(process_id,
                chunk_id,
                row_seq,
                action,
                process$status,
                diff_type,
                diff_type_desc,
                create_id,
                create_datetime,
                last_upd_id,
                last_upd_datetime)
         values(svc_diff_type_col(i).process_id,
                svc_diff_type_col(i).chunk_id,
                svc_diff_type_col(i).row_seq,
                svc_diff_type_col(i).action,
                svc_diff_type_col(i).process$status,
                sq.diff_type,
                sq.diff_type_desc,
                svc_diff_type_col(i).create_id,
                svc_diff_type_col(i).create_datetime,
                svc_diff_type_col(i).last_upd_id,
                svc_diff_type_col(i).last_upd_datetime);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          DIFF_TYPE_TL_SHEET,
                          svc_diff_type_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_DIFF_TYPE;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DIFF_TYPE_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_DIFF_TYPE_TL.PROCESS_ID%TYPE) IS

   TYPE svc_diff_type_tl_col_typ IS TABLE OF SVC_DIFF_TYPE_TL%ROWTYPE;
   L_temp_rec             SVC_DIFF_TYPE_TL%ROWTYPE;
   svc_diff_type_tl_col   svc_diff_type_tl_col_typ := NEW svc_diff_type_tl_col_typ();
   L_process_id           SVC_DIFF_TYPE_TL.PROCESS_ID%TYPE;
   L_error                BOOLEAN := FALSE;
   L_default_rec          SVC_DIFF_TYPE_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select diff_type_desc_mi,
             diff_type_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'DIFF_TYPE_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('DIFF_TYPE_DESC'   as diff_type_desc,
                                       'DIFF_TYPE'        as diff_type,
                                       'LANG'             as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DIFF_TYPE_TL';
   L_pk_columns    VARCHAR2(255)  := 'Diff Type, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select diff_type_desc_dv,
                      diff_type_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'DIFF_TYPE_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('DIFF_TYPE_DESC'    as diff_type_desc,
                                                'DIFF_TYPE'         as diff_type,
                                                'LANG'              as lang)))
   LOOP
      BEGIN
         L_default_rec.diff_type_desc := rec.diff_type_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            NULL,
                            'DIFF_TYPE_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.diff_type := rec.diff_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            NULL,
                            'DIFF_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select upper(r.get_cell(diff_type_tl$action))            as action,
                      r.get_cell(diff_type_tl$lang)                     as lang,
                      r.get_cell(diff_type_tl$diff_type_desc)           as diff_type_desc,
                      upper(r.get_cell(diff_type_tl$diff_type))         as diff_type,
                      r.get_row_seq()                                   as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(DIFF_TYPE_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_type_desc := rec.diff_type_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            rec.row_seq,
                            'DIFF_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_type := rec.diff_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            rec.row_seq,
                            'DIFF_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_DIFFID_DIFFTYPE.action_new then
         L_temp_rec.diff_type_desc   := NVL(L_temp_rec.diff_type_desc,L_default_rec.diff_type_desc);
         L_temp_rec.diff_type        := NVL(L_temp_rec.diff_type,L_default_rec.diff_type);
         L_temp_rec.lang             := NVL(L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.diff_type is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         DIFF_TYPE_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_diff_type_tl_col.extend();
         svc_diff_type_tl_col(svc_diff_type_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_diff_type_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_DIFF_TYPE_TL st
      using(select
                  (case
                   when l_mi_rec.diff_type_desc_mi = 'N'
                    and svc_diff_type_tl_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                    and s1.diff_type_desc is NULL then
                        mt.diff_type_desc
                   else s1.diff_type_desc
                   end) as diff_type_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_diff_type_tl_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                    and s1.lang is NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.diff_type_mi = 'N'
                    and svc_diff_type_tl_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                    and s1.diff_type is NULL then
                        mt.diff_type
                   else s1.diff_type
                   end) as diff_type
              from (select svc_diff_type_tl_col(i).diff_type_desc as diff_type_desc,
                           svc_diff_type_tl_col(i).diff_type        as diff_type,
                           svc_diff_type_tl_col(i).lang              as lang
                      from dual) s1,
                   diff_type_tl mt
             where mt.diff_type (+) = s1.diff_type
               and mt.lang (+)      = s1.lang) sq
                on (st.diff_type = sq.diff_type and
                    st.lang = sq.lang and
                    svc_diff_type_tl_col(i).ACTION IN (CORESVC_DIFFID_DIFFTYPE.action_mod,CORESVC_DIFFID_DIFFTYPE.action_del))
      when matched then
      update
         set process_id        = svc_diff_type_tl_col(i).process_id,
             chunk_id          = svc_diff_type_tl_col(i).chunk_id,
             row_seq           = svc_diff_type_tl_col(i).row_seq,
             action            = svc_diff_type_tl_col(i).action,
             process$status    = svc_diff_type_tl_col(i).process$status,
             diff_type_desc = sq.diff_type_desc
      when NOT matched then
      insert(process_id,
             chunk_id,
             row_seq,
             action,
             process$status,
             diff_type_desc,
             diff_type,
             lang)
      values(svc_diff_type_tl_col(i).process_id,
             svc_diff_type_tl_col(i).chunk_id,
             svc_diff_type_tl_col(i).row_seq,
             svc_diff_type_tl_col(i).action,
             svc_diff_type_tl_col(i).process$status,
             sq.diff_type_desc,
             sq.diff_type,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            DIFF_TYPE_TL_SHEET,
                            svc_diff_type_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_DIFF_TYPE_TL;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DIFF_IDS(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id IN SVC_DIFF_IDS.PROCESS_ID%TYPE) IS

   TYPE svc_DIFF_IDS_col_typ IS TABLE OF SVC_DIFF_IDS%ROWTYPE;
   L_temp_rec                            SVC_DIFF_IDS%ROWTYPE;
   svc_DIFF_IDS_col                      svc_DIFF_IDS_col_typ           := NEW svc_DIFF_IDS_col_typ();
   L_process_id                          SVC_DIFF_IDS.PROCESS_ID%TYPE;
   L_error                               BOOLEAN                        := FALSE;
   L_default_rec                         SVC_DIFF_IDS%ROWTYPE;
   cursor C_MANDATORY_IND is
      select INDUSTRY_SUBGROUP_mi,
             INDUSTRY_CODE_mi,
             DIFF_DESC_mi,
             DIFF_TYPE_mi,
             DIFF_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                                  = CORESVC_DIFFID_DIFFTYPE.template_key
                 and wksht_key                                     = 'DIFF_IDS'
             ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('INDUSTRY_SUBGROUP' as INDUSTRY_SUBGROUP,
                                                                'INDUSTRY_CODE'     as INDUSTRY_CODE,
                                                                'DIFF_DESC'         as DIFF_DESC,
                                                                'DIFF_TYPE'         as DIFF_TYPE,
                                                                'DIFF_ID'           as DIFF_ID,
                                                                 NULL               as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DIFF_IDS';
   L_pk_columns    VARCHAR2(255)  := 'Diff ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN
   (select INDUSTRY_SUBGROUP_dv,
           INDUSTRY_CODE_dv,
           DIFF_DESC_dv,
           DIFF_TYPE_dv,
           DIFF_ID_dv,
           null as dummy
      from (select column_key,
                   default_value
              from s9t_tmpl_cols_def
             where template_key                                  = CORESVC_DIFFID_DIFFTYPE.template_key
               and wksht_key                                     = 'DIFF_IDS'
            ) PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('INDUSTRY_SUBGROUP' as INDUSTRY_SUBGROUP,
                                                                   'INDUSTRY_CODE'     as INDUSTRY_CODE,
                                                                   'DIFF_DESC'         as DIFF_DESC,
                                                                   'DIFF_TYPE'         as DIFF_TYPE,
                                                                   'DIFF_ID'           as DIFF_ID,
                                                                    NULL               as dummy)))LOOP
      BEGIN

         L_default_rec.INDUSTRY_SUBGROUP := rec.INDUSTRY_SUBGROUP_dv;

      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DIFF_IDS',
                            NULL,
                            'INDUSTRY_SUBGROUP',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.INDUSTRY_CODE := rec.INDUSTRY_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DIFF_IDS',
                            NULL,
                            'INDUSTRY_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DIFF_DESC := rec.DIFF_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DIFF_IDS',
                            NULL,
                            'DIFF_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DIFF_TYPE := rec.DIFF_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DIFF_IDS',
                            NULL,
                            'DIFF_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DIFF_ID := rec.DIFF_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DIFF_IDS',
                            NULL,
                            'DIFF_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(DIFF_IDS$Action))                  as ACTION,
                      r.get_cell(DIFF_IDS$INDUSTRY_SUBGROUP)              as INDUSTRY_SUBGROUP,
                      r.get_cell(DIFF_IDS$INDUSTRY_CODE)                  as INDUSTRY_CODE,
                      r.get_cell(DIFF_IDS$DIFF_DESC)                      as DIFF_DESC,
                      UPPER(r.get_cell(DIFF_IDS$DIFF_TYPE))               as DIFF_TYPE,
                      UPPER(r.get_cell(DIFF_IDS$DIFF_ID))                 as DIFF_ID,
                      r.get_row_seq()                                     as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(DIFF_IDS_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.ACTION := rec.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.INDUSTRY_SUBGROUP := rec.INDUSTRY_SUBGROUP;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_sheet,
                            rec.row_seq,
                            'INDUSTRY_SUBGROUP',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.INDUSTRY_CODE := rec.INDUSTRY_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_sheet,
                            rec.row_seq,
                            'INDUSTRY_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DIFF_DESC := rec.DIFF_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_sheet,
                            rec.row_seq,
                            'DIFF_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DIFF_TYPE := rec.DIFF_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_sheet,
                            rec.row_seq,
                            'DIFF_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DIFF_ID := rec.DIFF_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_sheet,
                            rec.row_seq,
                            'DIFF_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_DIFFID_DIFFTYPE.action_new then
         L_temp_rec.INDUSTRY_SUBGROUP := NVL( L_temp_rec.INDUSTRY_SUBGROUP,
                                              L_default_rec.INDUSTRY_SUBGROUP);
         L_temp_rec.INDUSTRY_CODE     := NVL( L_temp_rec.INDUSTRY_CODE,
                                              L_default_rec.INDUSTRY_CODE);
         L_temp_rec.DIFF_DESC         := NVL( L_temp_rec.DIFF_DESC,
                                              L_default_rec.DIFF_DESC);
         L_temp_rec.DIFF_TYPE         := NVL( L_temp_rec.DIFF_TYPE,
                                              L_default_rec.DIFF_TYPE);
         L_temp_rec.DIFF_ID           := NVL( L_temp_rec.DIFF_ID,
                                              L_default_rec.DIFF_ID);
      end if;
      if NOT (L_temp_rec.DIFF_ID is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         DIFF_IDS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_DIFF_IDS_col.EXTEND();
         svc_DIFF_IDS_col(svc_DIFF_IDS_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

BEGIN
   FORALL i IN 1..svc_DIFF_IDS_col.COUNT
   SAVE EXCEPTIONS merge into SVC_DIFF_IDS st using
   (select (case
               when L_mi_rec.INDUSTRY_SUBGROUP_mi    = 'N'
                and svc_DIFF_IDS_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                and s1.INDUSTRY_SUBGROUP             is NULL
               then mt.INDUSTRY_SUBGROUP
               else s1.INDUSTRY_SUBGROUP
               end) as INDUSTRY_SUBGROUP,
           (case
               when L_mi_rec.INDUSTRY_CODE_mi    = 'N'
                and svc_DIFF_IDS_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                and s1.INDUSTRY_CODE             is NULL
               then mt.INDUSTRY_CODE
               else s1.INDUSTRY_CODE
               end) as INDUSTRY_CODE,
           (case
               when L_mi_rec.DIFF_DESC_mi    = 'N'
                and svc_DIFF_IDS_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                and s1.DIFF_DESC             is NULL
               then mt.DIFF_DESC
               else s1.DIFF_DESC
               end) as DIFF_DESC,
           (case
               when L_mi_rec.DIFF_TYPE_mi    = 'N'
                and svc_DIFF_IDS_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                and s1.DIFF_TYPE             is NULL
               then mt.DIFF_TYPE
               else s1.DIFF_TYPE
               end) as DIFF_TYPE,
           (case
               when L_mi_rec.DIFF_ID_mi    = 'N'
                and svc_DIFF_IDS_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                and s1.DIFF_ID             is NULL
               then mt.DIFF_ID
               else s1.DIFF_ID
               end) as DIFF_ID,
               null as dummy
      from (select svc_DIFF_IDS_col(i).INDUSTRY_SUBGROUP as INDUSTRY_SUBGROUP,
                   svc_DIFF_IDS_col(i).INDUSTRY_CODE     as INDUSTRY_CODE,
                   svc_DIFF_IDS_col(i).DIFF_DESC         as DIFF_DESC,
                   svc_DIFF_IDS_col(i).DIFF_TYPE         as DIFF_TYPE,
                   svc_DIFF_IDS_col(i).DIFF_ID           as DIFF_ID,
                   null as dummy
              from dual) s1,
                   diff_ids mt
     where mt.DIFF_ID (+)     = s1.DIFF_ID
       and 1 = 1) sq ON (st.DIFF_ID      = sq.DIFF_ID and
                         --svc_DIFF_IDS_col(i).ACTION IN (coresvc_diffid_difftype.action_new, coresvc_diffid_difftype.action_mod, coresvc_diffid_difftype.action_del))
                         svc_DIFF_IDS_col(i).action IN (CORESVC_DIFFID_DIFFTYPE.action_mod, CORESVC_DIFFID_DIFFTYPE.action_del))
     when matched then update
                          set PROCESS_ID                     = svc_DIFF_IDS_col(i).PROCESS_ID,
                              CHUNK_ID                       = svc_DIFF_IDS_col(i).CHUNK_ID,
                              ROW_SEQ                        = svc_DIFF_IDS_col(i).ROW_SEQ,
                              ACTION                         = svc_DIFF_IDS_col(i).ACTION,
                              PROCESS$STATUS                 = svc_DIFF_IDS_col(i).PROCESS$STATUS,
                              DIFF_DESC                      = sq.DIFF_DESC,
                              DIFF_TYPE                      = sq.DIFF_TYPE,
                              INDUSTRY_CODE                  = sq.INDUSTRY_CODE,
                              INDUSTRY_SUBGROUP              = sq.INDUSTRY_SUBGROUP,
                              CREATE_ID                      = svc_DIFF_IDS_col(i).CREATE_ID,
                              CREATE_DATETIME                = svc_DIFF_IDS_col(i).CREATE_DATETIME,
                              LAST_UPD_ID                    = svc_DIFF_IDS_col(i).LAST_UPD_ID,
                              LAST_UPD_DATETIME              = svc_DIFF_IDS_col(i).LAST_UPD_DATETIME WHEN NOT matched THEN
                       insert(process_id,
                              chunk_id,
                              row_seq,
                              action,
                              process$status,
                              industry_subgroup,
                              industry_code,
                              diff_desc,
                              diff_type,
                              diff_id,
                              create_id,
                              create_datetime,
                              last_upd_id,
                              last_upd_datetime)
                       values(svc_DIFF_IDS_col(i).PROCESS_ID,
                              svc_DIFF_IDS_col(i).CHUNK_ID,
                              svc_DIFF_IDS_col(i).ROW_SEQ,
                              svc_DIFF_IDS_col(i).ACTION,
                              svc_DIFF_IDS_col(i).PROCESS$STATUS,
                              sq.INDUSTRY_SUBGROUP,
                              sq.INDUSTRY_CODE,
                              sq.DIFF_DESC,
                              sq.DIFF_TYPE,
                              sq.DIFF_ID,
                              svc_DIFF_IDS_col(i).CREATE_ID,
                              svc_DIFF_IDS_col(i).CREATE_DATETIME,
                              svc_DIFF_IDS_col(i).LAST_UPD_ID,
                              svc_DIFF_IDS_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
         L_error_code := sql%bulk_exceptions(i).error_code;
         if L_error_code = 1 then
            L_error_code := NULL;
            L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                               L_pk_columns);
         end if;
         WRITE_S9T_ERROR(I_file_id,
                         DIFF_IDS_sheet,
                         svc_DIFF_IDS_col(sql%bulk_exceptions(i).error_index).row_seq,
                         NULL,
                         L_error_code,
                         L_error_msg);
      END LOOP;
  END;
END PROCESS_S9T_DIFF_IDS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DIFF_IDS_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id   IN   SVC_DIFF_IDS_TL.PROCESS_ID%TYPE) IS

   TYPE svc_diff_ids_tl_col_TYP IS TABLE OF SVC_DIFF_IDS_TL%ROWTYPE;
   L_temp_rec            SVC_DIFF_IDS_TL%ROWTYPE;
   svc_diff_ids_tl_col   svc_diff_ids_tl_col_TYP := NEW svc_diff_ids_tl_col_TYP();
   L_process_id          SVC_DIFF_IDS_TL.PROCESS_ID%TYPE;
   L_error               BOOLEAN := FALSE;
   L_default_rec         SVC_DIFF_IDS_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select diff_desc_mi,
             diff_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'DIFF_IDS_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('DIFF_DESC'    as diff_desc,
                                       'DIFF_ID'      as diff_id,
                                       'LANG'         as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DIFF_IDS_TL';
   L_pk_columns    VARCHAR2(255)  := 'Diff ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select diff_desc_dv,
                      diff_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'DIFF_IDS_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('DIFF_DESC'    as diff_desc,
                                                'DIFF_ID'      as diff_id,
                                                'LANG'         as lang)))
   LOOP
      BEGIN
         L_default_rec.diff_desc := rec.diff_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_TL_SHEET,
                            NULL,
                            'DIFF_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.diff_id := rec.diff_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_TL_SHEET,
                            NULL,
                            'DIFF_ID' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_TL_SHEET,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select upper(r.get_cell(DIFF_IDS_TL$ACTION))               as ACTION,
                      r.get_cell(DIFF_IDS_TL$DIFF_ID)                     as DIFF_ID,
                      UPPER(r.get_cell(DIFF_IDS_TL$DIFF_DESC))            as DIFF_DESC,
                      r.get_cell(DIFF_IDS_TL$LANG)                        as LANG,
                      r.get_row_seq()                                     as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(DIFF_IDS_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            diff_ids_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_desc := rec.diff_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_TL_SHEET,
                            rec.row_seq,
                            'DIFF_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.diff_id := rec.diff_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_TL_SHEET,
                            rec.row_seq,
                            'DIFF_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_DIFFID_DIFFTYPE.action_new then
         L_temp_rec.diff_desc      := NVL( L_temp_rec.diff_desc,L_default_rec.diff_desc);
         L_temp_rec.diff_id        := NVL( L_temp_rec.diff_id,L_default_rec.diff_id);
         L_temp_rec.lang           := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.diff_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         DIFF_IDS_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_diff_ids_tl_col.extend();
         svc_diff_ids_tl_col(svc_diff_ids_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_diff_ids_tl_col.COUNT SAVE EXCEPTIONS
      merge into svc_diff_ids_tl st
      using(select
                  (case
                   when l_mi_rec.diff_desc_mi = 'N'
                    and svc_diff_ids_tl_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                    and s1.diff_desc is NULL then
                        mt.diff_desc
                   else s1.diff_desc
                   end) as diff_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_diff_ids_tl_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                    and s1.lang is NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.diff_id_mi = 'N'
                    and svc_diff_ids_tl_col(i).action = CORESVC_DIFFID_DIFFTYPE.action_mod
                    and s1.diff_id is NULL then
                        mt.diff_id
                   else s1.diff_id
                   end) as diff_id
              from (select svc_diff_ids_tl_col(i).diff_desc         as diff_desc,
                           svc_diff_ids_tl_col(i).diff_id           as diff_id,
                           svc_diff_ids_tl_col(i).lang              as lang
                      from dual) s1,
                   diff_ids_tl mt
             where mt.diff_id (+)    = s1.diff_id
               and mt.lang (+)       = s1.lang) sq
                on (st.diff_id = sq.diff_id and
                    st.lang = sq.lang and
                    svc_diff_ids_tl_col(i).ACTION IN (CORESVC_DIFFID_DIFFTYPE.action_mod,CORESVC_DIFFID_DIFFTYPE.action_del))
      when matched then
      update
         set process_id        = svc_diff_ids_tl_col(i).process_id,
             chunk_id          = svc_diff_ids_tl_col(i).chunk_id,
             row_seq           = svc_diff_ids_tl_col(i).row_seq,
             action            = svc_diff_ids_tl_col(i).action,
             process$status    = svc_diff_ids_tl_col(i).process$status,
             diff_desc         = sq.diff_desc
      when NOT matched then
      insert(process_id,
             chunk_id,
             row_seq,
             action,
             process$status,
             diff_desc,
             diff_id,
             lang)
      values(svc_diff_ids_tl_col(i).process_id,
             svc_diff_ids_tl_col(i).chunk_id,
             svc_diff_ids_tl_col(i).row_seq,
             svc_diff_ids_tl_col(i).action,
             svc_diff_ids_tl_col(i).process$status,
             sq.diff_desc,
             sq.diff_id,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            DIFF_IDS_TL_SHEET,
                            svc_diff_ids_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_DIFF_IDS_TL;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;--to ensure that the record in s9t_folder is commited
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_DIFF_TYPE(I_file_id,
                            I_process_id);

      PROCESS_S9T_DIFF_TYPE_TL(I_file_id,
                               I_process_id);

      PROCESS_S9T_DIFF_IDS(I_file_id,
                           I_process_id);

      PROCESS_S9T_DIFF_IDS_TL(I_file_id,
                              I_process_id);
   end if;
   ---
   if CORESVC_CFLEX.PROCESS_S9T_SHEETS_ADMINAPI(O_error_message,
                                                I_file_id,
                                                I_process_id,
                                                template_key) = FALSE   then
      return FALSE;
   end if;
   ---
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   -- Update SVC_PROCESS_TRACKER
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   commit;

   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
----------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_DIFF_TYPE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error           IN OUT   BOOLEAN,
                               I_rec             IN       C_SVC_DIFF_TYPE_DEL%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.PROCESS_VAL_DIFF_TYPE';
   L_assoc_exists   BOOLEAN                           := FALSE;
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DIFF_TYPE';
BEGIN
   --FOR foreign key constraint
   if I_rec.diff_type is NOT NULL then
      if DIFF_TYPE_SQL.CHECK_ASSOCIATION(O_error_message,
                                         L_assoc_exists,
                                         I_rec.diff_type) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      elsif L_assoc_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_TYPE',
                     'DIFF_TYPE_HAS_ASSOC');
         O_error := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_VAL_DIFF_TYPE;
-----------------------------------------------------------------------------
FUNCTION EXEC_DIFF_TYPE_INS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_diff_type_temp_rec   IN       DIFF_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_TYPE_INS';
BEGIN
   insert into diff_type
      values I_diff_type_temp_rec;
   return TRUE;
EXCEPTION
   when others then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_TYPE_INS;
----------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_TYPE_UPD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_diff_type_temp_rec   IN       DIFF_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_TYPE_UPD';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DIFF_TYPE';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor C_DIFF_TYPE is
      select 'x'
        from diff_type
       where diff_type = I_diff_type_temp_rec.diff_type
         for update nowait;

BEGIN
   open C_DIFF_TYPE;
   close C_DIFF_TYPE;
   update diff_type
      set row = I_diff_type_temp_rec
    where diff_type = I_diff_type_temp_rec.diff_type;
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_diff_type_temp_rec.diff_type,
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_DIFF_TYPE%ISOPEN then
         close C_DIFF_TYPE;
      end if;
      O_error_message :=SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_TYPE_UPD;
-----------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_CFA_STG_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id           IN       NUMBER,
                              I_chunk_id             IN       NUMBER,
                              I_diff_type_temp_rec   IN       DIFF_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_CFA_STG_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CFA_EXT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED,-54);

   cursor C_RECS_DELETE is
     select sc.process_id,
            sc.chunk_id,
            sc.row_seq,
            sc.action,
            sc.base_rms_table,
            sc.group_set_view_name,
            sc.keys_col,
            sc.attrs_col,
            gs.group_set_id,
            gs.default_func,
            sf.template_key,
            sc.rowid as rid
       from svc_cfa_ext          sc,
            cfa_attrib_group_set gs,
            svc_process_tracker  sp,
            s9t_folder           sf
      where sc.process_id          = I_process_id
        and chunk_id               = I_chunk_id
        and sp.process_id          = I_process_id
        and sp.file_id             = sf.file_id (+)
        and sc.group_set_view_name = gs.group_set_view_name;

BEGIN
   open C_RECS_DELETE;
   close C_RECS_DELETE;

   FOR rec IN C_RECS_DELETE LOOP
      if KEY_VAL_PAIRS_PKG.GET_ATTR(rec.KEYS_COL,'DIFF_TYPE') = I_diff_type_temp_rec.diff_type then
         delete from svc_cfa_ext
            where row_seq = rec.row_seq;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED   then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_diff_type_temp_rec.diff_type,
                                                               NULL);
      return FALSE;

   when OTHERS   then
      if C_RECS_DELETE%ISOPEN then
         close C_RECS_DELETE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DIFF_CFA_STG_DEL;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_TYPE_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id           IN       NUMBER,
                            I_chunk_id             IN       NUMBER,
                            I_diff_type_temp_rec   IN       DIFF_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_TYPE_DEL';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_DIFF_TYPE is
      select 'x'
        from diff_type
       where diff_type = I_diff_type_temp_rec.diff_type
         for update nowait;

   cursor C_DIFF_TYPE_TL is
      select 'x'
        from diff_type_tl
       where diff_type = I_diff_type_temp_rec.diff_type
         for update nowait;

   cursor C_DIFF_TYPE_EXT is
      select 'x'
        from diff_type_cfa_ext
       where diff_type = I_diff_type_temp_rec.diff_type
         for update nowait;

BEGIN
   L_table := 'DIFF_TYPE_TL';
   open C_DIFF_TYPE_TL;
   close C_DIFF_TYPE_TL;

   L_table := 'DIFF_TYPE';
   open C_DIFF_TYPE;
   close C_DIFF_TYPE;

   L_table := 'DIFF_TYPE_CFA_EXT';
   open C_DIFF_TYPE_EXT;
   close C_DIFF_TYPE_EXT;
   
   if EXEC_DIFF_CFA_STG_DEL(O_error_message,
                              I_process_id,
                              I_chunk_id,
                              I_diff_type_temp_rec) = FALSE then
        return FALSE;
   else
     delete
       from diff_type_cfa_ext
      where diff_type = I_diff_type_temp_rec.diff_type;
   
     delete
       from diff_type_tl
      where diff_type = I_diff_type_temp_rec.diff_type;
    
     delete
       from diff_type
      where diff_type = I_diff_type_temp_rec.diff_type; 
    
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_diff_type_temp_rec.diff_type,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_DIFF_TYPE%ISOPEN then
         close C_DIFF_TYPE;
      end if;
      O_error_message :=SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            null,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_TYPE_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_TYPE_TL_INS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_diff_type_tl_ins_tab    IN       DIFF_TYPE_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_TYPE_TL_INS';

BEGIN
   if I_diff_type_tl_ins_tab is NOT NULL and I_diff_type_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_diff_type_tl_ins_tab.COUNT()
         insert into diff_type_tl
              values I_diff_type_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_TYPE_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_TYPE_TL_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_diff_type_tl_upd_tab   IN       DIFF_TYPE_TL_TAB,
                               I_diff_type_tl_upd_rst   IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_DIFF_TYPE_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_DIFF_TYPE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_TYPE_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DIFF_TYPE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_DIFF_TYPE_TL_UPD(I_diff_type  DIFF_TYPE_TL.DIFF_TYPE%TYPE,
                                  I_lang       DIFF_TYPE_TL.LANG%TYPE) is
      select 'x'
        from diff_type_tl
       where diff_type = I_diff_type
         and lang = I_lang
         for update nowait;

BEGIN
   if I_diff_type_tl_upd_tab is NOT NULL and I_diff_type_tl_upd_tab.count > 0 then
      FOR i in I_diff_type_tl_upd_tab.FIRST..I_diff_type_tl_upd_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_diff_type_tl_upd_tab(i).lang);
            L_key_val2 := 'Diff Type: '||to_char(I_diff_type_tl_upd_tab(i).diff_type);
            open C_LOCK_DIFF_TYPE_TL_UPD(I_diff_type_tl_upd_tab(i).diff_type,
                                         I_diff_type_tl_upd_tab(i).lang);
            close C_LOCK_DIFF_TYPE_TL_UPD;
            
            update diff_type_tl
               set diff_type_desc = I_diff_type_tl_upd_tab(i).diff_type_desc,
                   last_update_id = I_diff_type_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_diff_type_tl_upd_tab(i).last_update_datetime
             where lang = I_diff_type_tl_upd_tab(i).lang
               and diff_type = I_diff_type_tl_upd_tab(i).diff_type;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     L_table,
                                                     L_key_val1,
                                                     L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_diff_type_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_DIFF_TYPE_TL_UPD%ISOPEN then
         close C_LOCK_DIFF_TYPE_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_TYPE_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_TYPE_TL_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_diff_type_tl_del_tab   IN       DIFF_TYPE_TL_TAB,
                               I_diff_type_tl_del_rst   IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_DIFF_TYPE_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_DIFF_TYPE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_TYPE_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DIFF_TYPE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_DIFF_TYPE_TL_DEL(I_diff_type  DIFF_TYPE_TL.DIFF_TYPE%TYPE,
                                  I_lang       DIFF_TYPE_TL.LANG%TYPE) is
      select 'x'
        from diff_type_tl
       where diff_type = I_diff_type
         and lang = I_lang
         for update nowait;

BEGIN
   if I_diff_type_tl_del_tab is NOT NULL and I_diff_type_tl_del_tab.count > 0 then
      FOR i in I_diff_type_tl_del_tab.FIRST..I_diff_type_tl_del_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_diff_type_tl_del_tab(i).lang);
            L_key_val2 := 'Diff Type: '||to_char(I_diff_type_tl_del_tab(i).diff_type);
            open C_LOCK_DIFF_TYPE_TL_DEL(I_diff_type_tl_del_tab(i).diff_type,
                                         I_diff_type_tl_del_tab(i).lang);
            close C_LOCK_DIFF_TYPE_TL_DEL;
            
            delete diff_type_tl
             where lang = I_diff_type_tl_del_tab(i).lang
               and diff_type = I_diff_type_tl_del_tab(i).diff_type;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     L_table,
                                                     L_key_val1,
                                                     L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_diff_type_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;   
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_DIFF_TYPE_TL_DEL%ISOPEN then
         close C_LOCK_DIFF_TYPE_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_TYPE_TL_DEL;
-------------------------------------------------------------------------------------
FUNCTION PROCESS_DIFF_TYPE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id         IN     SVC_DIFF_TYPE.PROCESS_ID%TYPE,
                           I_chunk_id           IN     SVC_DIFF_TYPE.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.PROCESS_DIFF_TYPE';
   L_error                BOOLEAN;
   L_process_error        BOOLEAN;
   L_assoc_exists         BOOLEAN;
   L_diff_type_temp_rec   DIFF_TYPE%ROWTYPE;
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DIFF_TYPE';
BEGIN
   FOR rec IN C_SVC_DIFF_TYPE(I_process_id,I_chunk_id) LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new, action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      --for duplicate entries
      if rec.action = action_new
         and rec.pk_diff_type_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIFF_TYPE',
                     'DUP_DIFF_TYPE');
         L_error := TRUE;
      end if;

      --for difftype doesn�t exist during delete,update
      if rec.action = action_mod
         and rec.diff_type is NOT NULL then
         if rec.pk_diff_type_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DIFF_TYPE',
                        'INVALID_DIFF_TYPE');
            L_error := TRUE;
         end if;
      end if;

      --for difftypedesc null IN insert, update
      if rec.action IN (action_new, action_mod)
         and rec.diff_type_desc is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,rec.row_seq,
                     'DIFF_TYPE_DESC',
                     'DIFF_TYPE_DESC_REQ');
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_diff_type_temp_rec.diff_type_desc  := rec.diff_type_desc;
         L_diff_type_temp_rec.diff_type       := rec.diff_type;
         L_diff_type_temp_rec.create_id       := GET_USER;
         L_diff_type_temp_rec.create_datetime := SYSDATE;

         if rec.action = action_new then
            if EXEC_DIFF_TYPE_INS(O_error_message,
                                  L_diff_type_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_DIFF_TYPE_UPD(O_error_message,
                                  L_diff_type_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;            
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_DIFF_TYPE%ISOPEN then
         close C_SVC_DIFF_TYPE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));


      return FALSE;
END PROCESS_DIFF_TYPE;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DIFF_TYPE_DEL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id         IN     SVC_DIFF_TYPE.PROCESS_ID%TYPE,
                               I_chunk_id           IN     SVC_DIFF_TYPE.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.PROCESS_DIFF_TYPE_DEL';
   L_error                BOOLEAN;
   L_process_error        BOOLEAN;
   L_assoc_exists         BOOLEAN;
   L_diff_type_temp_rec   DIFF_TYPE%ROWTYPE;
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DIFF_TYPE';
BEGIN
   FOR rec IN C_SVC_DIFF_TYPE_DEL(I_process_id,I_chunk_id) LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      --for difftype doesn�t exist
      if rec.diff_type is NOT NULL and 
         rec.pk_diff_type_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DIFF_TYPE',
                        'INVALID_DIFF_TYPE');
            L_error := TRUE;
      end if;

      --for referential integrity check
      if PROCESS_VAL_DIFF_TYPE(O_error_message,
                               L_error,
                               rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_diff_type_temp_rec.diff_type := rec.diff_type;
         if EXEC_DIFF_TYPE_DEL(O_error_message,
                               rec.process_id,
                               rec.chunk_id,
                               L_diff_type_temp_rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_process_error := TRUE;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_DIFF_TYPE_DEL%ISOPEN then
         close C_SVC_DIFF_TYPE_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_DIFF_TYPE_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DIFF_TYPE_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_DIFF_TYPE_TL.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_DIFF_TYPE_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.PROCESS_DIFF_TYPE_TL';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DIFF_TYPE_TL';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DIFF_TYPE_TL';
   L_error                   BOOLEAN := FALSE;
   L_process_error           BOOLEAN := FALSE;
   L_diff_type_tl_temp_rec   DIFF_TYPE_TL%ROWTYPE;
   L_diff_type_tl_upd_rst    ROW_SEQ_TAB;
   L_diff_type_tl_del_rst    ROW_SEQ_TAB;

   cursor C_SVC_DIFF_TYPE_TL(I_process_id NUMBER,
                             I_chunk_id NUMBER) is
      select pk_diff_type_tl.rowid  as pk_diff_type_tl_rid,
             fk_diff_type.rowid     as fk_diff_type_rid,
             fk_lang.rowid          as fk_lang_rid,
             st.lang,
             st.diff_type,
             st.diff_type_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_diff_type_tl  st,
             diff_type         fk_diff_type,
             diff_type_tl      pk_diff_type_tl,
             lang              fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.diff_type   =  fk_diff_type.diff_type (+)
         and st.lang        =  pk_diff_type_tl.lang (+)
         and st.diff_type   =  pk_diff_type_tl.diff_type (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_DIFF_TYPE_TL is TABLE OF C_SVC_DIFF_TYPE_TL%ROWTYPE INDEX BY BINARY_INTEGER; 
   L_svc_diff_type_tl_tab   SVC_DIFF_TYPE_TL;

   L_diff_type_tl_ins_tab   diff_type_tl_tab := NEW diff_type_tl_tab();
   L_diff_type_tl_upd_tab   diff_type_tl_tab := NEW diff_type_tl_tab();
   L_diff_type_tl_del_tab   diff_type_tl_tab := NEW diff_type_tl_tab();

BEGIN
   if C_SVC_DIFF_TYPE_TL%ISOPEN then
      close C_SVC_DIFF_TYPE_TL;
   end if;

   open C_SVC_DIFF_TYPE_TL(I_process_id,
                           I_chunk_id);
   LOOP
      fetch C_SVC_DIFF_TYPE_TL bulk collect into L_svc_diff_type_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_diff_type_tl_tab.COUNT > 0 then
         FOR i in L_svc_diff_type_tl_tab.FIRST..L_svc_diff_type_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_diff_type_tl_tab(i).action is NULL
               or L_svc_diff_type_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            --check for primary_lang
            if L_svc_diff_type_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_diff_type_tl_tab(i).action = action_new
               and L_svc_diff_type_tl_tab(i).pk_diff_type_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_diff_type_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_diff_type_tl_tab(i).lang is NOT NULL
               and L_svc_diff_type_tl_tab(i).diff_type is NOT NULL
               and L_svc_diff_type_tl_tab(i).pk_diff_type_tl_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            -- check for FK
            if L_svc_diff_type_tl_tab(i).action = action_new
               and L_svc_diff_type_tl_tab(i).diff_type is NOT NULL
               and L_svc_diff_type_tl_tab(i).fk_diff_type_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           'DIFF_TYPE',
                           'INVALID_DIFF_TYPE');
               L_error := TRUE;
            end if;

            if L_svc_diff_type_tl_tab(i).action = action_new
               and L_svc_diff_type_tl_tab(i).lang is NOT NULL
               and L_svc_diff_type_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_diff_type_tl_tab(i).action in (action_new, action_mod) and L_svc_diff_type_tl_tab(i).diff_type_desc is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           'DIFF_TYPE_DESC',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_diff_type_tl_tab(i).diff_type is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           'DIFF_TYPE',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_diff_type_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_type_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if NOT L_error then
               L_diff_type_tl_temp_rec.lang                 := L_svc_diff_type_tl_tab(i).lang;
               L_diff_type_tl_temp_rec.diff_type            := L_svc_diff_type_tl_tab(i).diff_type;
               L_diff_type_tl_temp_rec.diff_type_desc       := L_svc_diff_type_tl_tab(i).diff_type_desc;
               L_diff_type_tl_temp_rec.create_datetime      := SYSDATE;
               L_diff_type_tl_temp_rec.create_id            := GET_USER;
               L_diff_type_tl_temp_rec.last_update_datetime := SYSDATE;
               L_diff_type_tl_temp_rec.last_update_id       := GET_USER;

               if L_svc_diff_type_tl_tab(i).action = action_new then
                  L_diff_type_tl_ins_tab.extend;
                  L_diff_type_tl_ins_tab(L_diff_type_tl_ins_tab.count()) := L_diff_type_tl_temp_rec;
               end if;

               if L_svc_diff_type_tl_tab(i).action = action_mod then
                  L_diff_type_tl_upd_tab.extend;
                  L_diff_type_tl_upd_tab(L_diff_type_tl_upd_tab.count()) := L_diff_type_tl_temp_rec;
                  L_diff_type_tl_upd_rst(L_diff_type_tl_upd_tab.count()) := L_svc_diff_type_tl_tab(i).row_seq;
               end if;

               if L_svc_diff_type_tl_tab(i).action = action_del then
                  L_diff_type_tl_del_tab.extend;
                  L_diff_type_tl_del_tab(L_diff_type_tl_del_tab.count()) := L_diff_type_tl_temp_rec;
                  L_diff_type_tl_del_rst(L_diff_type_tl_del_tab.count()) := L_svc_diff_type_tl_tab(i).row_seq;                  
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_DIFF_TYPE_TL%NOTFOUND;
   END LOOP;
   close C_SVC_DIFF_TYPE_TL;

   if EXEC_DIFF_TYPE_TL_INS(O_error_message,
                            L_diff_type_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_DIFF_TYPE_TL_UPD(O_error_message,
                            L_diff_type_tl_upd_tab,
                            L_diff_type_tl_upd_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_DIFF_TYPE_TL_DEL(O_error_message,
                            L_diff_type_tl_del_tab,
                            L_diff_type_tl_del_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DIFF_TYPE_TL;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_IDS_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diff_ids_temp_rec   IN       DIFF_IDS%ROWTYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_IDS_INS';
BEGIN
   insert into diff_ids
        values I_diff_ids_temp_rec;
   
   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('DIFF_ID_EXISTS',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DIFF_IDS_INS;
---------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_IDS_UPD(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diff_ids_temp_rec  IN     DIFF_IDS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_IDS_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DIFF_IDS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED,-54);

   cursor C_DIFF_IDS_LOCK is
      select 'x'
        from diff_ids
       where diff_id = I_diff_ids_temp_rec.diff_id
         for update nowait;

BEGIN
   open C_DIFF_IDS_LOCK;
   close C_DIFF_IDS_LOCK;

   update diff_ids
      set row     = I_diff_ids_temp_rec
    where 1       = 1
      and diff_id = I_diff_ids_temp_rec.diff_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_diff_ids_temp_rec.diff_id,
                                                               NULL);
      return FALSE;

   when OTHERS then
      if C_DIFF_IDS_LOCK%ISOPEN then
         close C_DIFF_IDS_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END EXEC_DIFF_IDS_UPD;
-------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_IDS_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_diff_ids_temp_rec   IN       DIFF_IDS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_IDS_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED,-54);

   cursor C_DIFF_IDS_TL_LOCK is
      select 'x'
        from diff_ids_tl
       where diff_id = I_diff_ids_temp_rec.diff_id
         for update nowait;

   cursor C_DIFF_IDS_LOCK is
      select 'x'
        from diff_ids
       where diff_id = I_diff_ids_temp_rec.diff_id
         for update nowait;
BEGIN
   L_table := 'DIFF_IDS_TL';
   open C_DIFF_IDS_TL_LOCK;
   close C_DIFF_IDS_TL_LOCK;

   delete
     from diff_ids_tl
    where diff_id = I_diff_ids_temp_rec.diff_id;

   L_table := 'DIFF_IDS';
   open C_DIFF_IDS_LOCK;
   close C_DIFF_IDS_LOCK;

   delete
     from diff_ids
    where diff_id = I_diff_ids_temp_rec.diff_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_diff_ids_temp_rec.diff_id,
                                                               NULL);
      return FALSE;
   when OTHERS then
      if C_DIFF_IDS_TL_LOCK%ISOPEN then
         close C_DIFF_IDS_TL_LOCK;
      end if;

      if C_DIFF_IDS_LOCK%ISOPEN then
         close C_DIFF_IDS_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END EXEC_DIFF_IDS_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_IDS_TL_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_diff_ids_tl_ins_tab   IN       DIFF_IDS_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_IDS_TL_INS';

BEGIN
   if I_diff_ids_tl_ins_tab is NOT NULL and I_diff_ids_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_diff_ids_tl_ins_tab.COUNT()
         insert into diff_ids_tl
              values I_diff_ids_tl_ins_tab(i);
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_IDS_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_IDS_TL_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_diff_ids_tl_upd_tab   IN       DIFF_IDS_TL_TAB,
                              I_diff_ids_tl_upd_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_DIFF_IDS_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_DIFF_IDS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_IDS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DIFF_IDS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_DIFF_IDS_TL_UPD(I_diff_id   DIFF_IDS_TL.DIFF_ID%TYPE,
                                 I_lang      DIFF_IDS_TL.LANG%TYPE) is
      select 'x'
        from diff_ids_tl
       where diff_id = I_diff_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_diff_ids_tl_upd_tab is NOT NULL and I_diff_ids_tl_upd_tab.count > 0 then
      FOR i in I_diff_ids_tl_upd_tab.FIRST..I_diff_ids_tl_upd_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_diff_ids_tl_upd_tab(i).lang);
            L_key_val2 := 'Diff ID '||to_char(I_diff_ids_tl_upd_tab(i).diff_id);
            open C_LOCK_DIFF_IDS_TL_UPD(I_diff_ids_tl_upd_tab(i).diff_id,
                                        I_diff_ids_tl_upd_tab(i).lang);
            close C_LOCK_DIFF_IDS_TL_UPD;

            update diff_ids_tl
               set diff_desc = I_diff_ids_tl_upd_tab(i).diff_desc,
                   last_update_id = I_diff_ids_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_diff_ids_tl_upd_tab(i).last_update_datetime
             where lang = I_diff_ids_tl_upd_tab(i).lang
               and diff_id = I_diff_ids_tl_upd_tab(i).diff_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     L_table,
                                                     L_key_val1,
                                                     L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_diff_ids_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_DIFF_IDS_TL_UPD%ISOPEN then
         close C_LOCK_DIFF_IDS_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_IDS_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DIFF_IDS_TL_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_diff_ids_tl_del_tab   IN       DIFF_IDS_TL_TAB,
                              I_diff_ids_tl_del_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_DIFF_IDS_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_DIFF_IDS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.EXEC_DIFF_IDS_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DIFF_IDS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_DIFF_IDS_TL_DEL(I_diff_id   DIFF_IDS_TL.DIFF_ID%TYPE,
                                 I_lang      DIFF_IDS_TL.LANG%TYPE) is
      select 'x'
        from diff_ids_tl
       where diff_id = I_diff_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_diff_ids_tl_del_tab is NOT NULL and I_diff_ids_tl_del_tab.count > 0 then
      FOR i in I_diff_ids_tl_del_tab.FIRST..I_diff_ids_tl_del_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_diff_ids_tl_del_tab(i).lang);
            L_key_val2 := 'Diff ID: '||to_char(I_diff_ids_tl_del_tab(i).diff_id);
            open C_LOCK_DIFF_IDS_TL_DEL(I_diff_ids_tl_del_tab(i).diff_id,
                                        I_diff_ids_tl_del_tab(i).lang);
            close C_LOCK_DIFF_IDS_TL_DEL;

            delete diff_ids_tl
             where lang = I_diff_ids_tl_del_tab(i).lang
               and diff_id = I_diff_ids_tl_del_tab(i).diff_id;
         EXCEPTION
           when RECORD_LOCKED then
              O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                    L_table,
                                                    L_key_val1,
                                                    L_key_val2);
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.nextval,
                          I_chunk_id,
                          L_table,
                          I_diff_ids_tl_del_rst(i),
                          NULL,
                          O_error_message);
         END;
      END LOOP;
   end if;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_DIFF_IDS_TL_DEL%ISOPEN then
         close C_LOCK_DIFF_IDS_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_DIFF_IDS_TL_DEL;
--------------------------------------------------------------------------------------
FUNCTION DIFF_ID_EXISTS (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT  BOOLEAN,
                         I_diff_id         IN      DIFF_IDS.DIFF_ID%TYPE)

RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_DIFFID_DIFFTYPE.DIFF_ID_EXISTS';
   L_dummy     VARCHAR2(1)  := 'X';

   cursor C_DIFF_ID_EXISTS is
      select 'Y'
        from diff_group_head
       where diff_group_id = I_diff_id;

BEGIN
   open C_DIFF_ID_EXISTS;
   fetch C_DIFF_ID_EXISTS into L_dummy;
   close C_DIFF_ID_EXISTS;

   if L_dummy = 'Y' then
      O_exists:= TRUE;
   else
      O_exists:= FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_DIFF_ID_EXISTS%ISOPEN then
         close C_DIFF_ID_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                         SQLERRM,
                         L_program,
                         to_char(SQLCODE));
      ROLLBACK;
   return FALSE;
END DIFF_ID_EXISTS;
----------------------------------------------------------------------------------------
FUNCTION CHECK_DIFF_TYPE_CHANGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_flag            IN OUT   BOOLEAN,
                                I_diff_id         IN       DIFF_IDS.DIFF_ID%TYPE,
                                I_diff_type       IN       DIFF_IDS.DIFF_TYPE%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)            := 'CORESVC_DIFFID_DIFFTYPE.CHECK_DIFF_TYPE_CHANGE';
   L_dummy     DIFF_IDS.DIFF_TYPE%TYPE := NULL;

   cursor C_DIFF_TYPE is
      select diff_type
        from diff_ids
       where diff_id = I_diff_id;

BEGIN
   open C_DIFF_TYPE;
   fetch C_DIFF_TYPE into L_dummy;
   close C_DIFF_TYPE;

   if L_dummy <> I_diff_type then
      O_flag          := TRUE;
      O_error_message := 'DFID_CANT_CHNG_DIF_TYP';
   end if;

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_DIFF_TYPE%ISOPEN then
         close C_DIFF_TYPE;
      end if;
      ROLLBACK;
      return FALSE;
END CHECK_DIFF_TYPE_CHANGE;
----------------------------------------------------------------------------------------
FUNCTION CHECK_DELETE (O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_items_exist                IN OUT   BOOLEAN,
                       O_group_details_exist        IN OUT   BOOLEAN,
                       O_range_details_exist        IN OUT   BOOLEAN,
                       O_pack_tmpl_detail_exist     IN OUT   BOOLEAN,
                       O_tsf_packing_detail_exist   IN OUT   BOOLEAN,
                       O_tsf_xform_request_exist    IN OUT   BOOLEAN,
                       O_item_seasons_exist         IN OUT   BOOLEAN,
                       I_diff_id                    IN       DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)  := 'CORESVC_DIFFID_DIFFTYPE.CHECK_DELETE';
   L_dummy1    VARCHAR2(1)   := NULL;
   L_dummy2    VARCHAR2(1)   := NULL;
   L_dummy3    VARCHAR2(1)   := NULL;
   L_dummy4    VARCHAR2(1)   := NULL;
   L_dummy5    VARCHAR2(1)   := NULL;
   L_dummy6    VARCHAR2(1)   := NULL;
   L_dummy7    VARCHAR2(1)   := NULL;

   cursor C_ITEM_MASTER is
      select 'Y'
        from item_master
       where diff_1 = I_diff_id
          or diff_2 = I_diff_id
          or diff_3 = I_diff_id
          or diff_4 = I_diff_id;

   cursor C_DIFF_GROUP_DETAIL is
      select 'Y'
        from diff_group_detail
       where diff_id = I_diff_id;

   cursor C_DIFF_RANGE_DETAIL is
      select 'Y'
        from diff_range_detail
       where (diff_1 = I_diff_id or
              diff_2 = I_diff_id or
              diff_3 = I_diff_id);

   cursor C_PACK_TMPL_DETAIL is
      select 'Y'
        from pack_tmpl_detail
       where diff_1 = I_diff_id
          or diff_2 = I_diff_id;

   cursor C_TSF_PACKING_DETAIL is
      select 'Y'
        from tsf_packing_detail
       where diff_id = I_diff_id;

   cursor C_TSF_XFORM_REQUEST is
      select 'Y'
        from tsf_xform_request
       where diff_id = I_diff_id;

   cursor C_ITEM_SEASONS is
      select 'Y'
        from item_seasons
       where diff_id = I_diff_id;

BEGIN

   open C_ITEM_MASTER;
   fetch C_ITEM_MASTER into L_dummy1;
   close C_ITEM_MASTER;

   open C_DIFF_GROUP_DETAIL;
   fetch C_DIFF_GROUP_DETAIL into L_dummy2;
   close C_DIFF_GROUP_DETAIL;

   open C_DIFF_RANGE_DETAIL;
   fetch C_DIFF_RANGE_DETAIL into L_dummy3;
   close C_DIFF_RANGE_DETAIL;

   open C_PACK_TMPL_DETAIL;
   fetch C_PACK_TMPL_DETAIL into L_dummy4;
   close C_PACK_TMPL_DETAIL;

   open C_TSF_PACKING_DETAIL;
   fetch C_TSF_PACKING_DETAIL into L_dummy5;
   close C_TSF_PACKING_DETAIL;

   open C_TSF_XFORM_REQUEST;
   fetch C_TSF_XFORM_REQUEST into L_dummy6;
   close C_TSF_XFORM_REQUEST;

   open C_ITEM_SEASONS;
   fetch C_ITEM_SEASONS into L_dummy7;
   close C_ITEM_SEASONS;

   O_items_exist              := FALSE;
   O_group_details_exist      := FALSE;
   O_range_details_exist      := FALSE;
   O_item_seasons_exist       := FALSE;
   O_pack_tmpl_detail_exist   := FALSE;
   O_tsf_packing_detail_exist := FALSE;
   O_tsf_xform_request_exist  := FALSE;

   if L_dummy1 = 'Y' then
      O_items_exist := TRUE;
   end if;

   if L_dummy2 = 'Y' then
      O_group_details_exist := TRUE;
   end if;

   if L_dummy3 = 'Y' then
      O_range_details_exist := TRUE;
   end if;

   if L_dummy4 = 'Y' then
      O_pack_tmpl_detail_exist := TRUE;
   end if;

   if L_dummy5 = 'Y' then
      O_tsf_packing_detail_exist := TRUE;
   end if;

   if L_dummy6 = 'Y' then
      O_tsf_xform_request_exist := TRUE;
   end if;

   if L_dummy7 = 'Y' then
      O_item_seasons_exist := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_ITEM_MASTER%ISOPEN then
         close C_ITEM_MASTER;
      end if;

      if C_DIFF_GROUP_DETAIL%ISOPEN then
         close C_DIFF_GROUP_DETAIL;
      end if;

      if C_DIFF_RANGE_DETAIL%ISOPEN then
         close C_DIFF_RANGE_DETAIL;
      end if;

      if C_PACK_TMPL_DETAIL%ISOPEN then
         close C_PACK_TMPL_DETAIL;
      end if;

      if C_TSF_PACKING_DETAIL%ISOPEN then
         close C_TSF_PACKING_DETAIL;
      end if;

      if C_TSF_XFORM_REQUEST%ISOPEN then
         close C_TSF_XFORM_REQUEST;
      end if;

      if C_ITEM_SEASONS%ISOPEN then
         close C_ITEM_SEASONS;
      end if;

      ROLLBACK;
   return FALSE;
END CHECK_DELETE;
-----------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_DIFF_IDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error           IN OUT   BOOLEAN,
                              I_rec             IN OUT   C_SVC_DIFF_IDS%ROWTYPE)
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.PROCESS_VAL_DIFF_IDS';
   L_item_exist                 BOOLEAN;
   L_group_details_exist        BOOLEAN;
   L_range_details_exist        BOOLEAN;
   L_pack_tmpL_detail_exist     BOOLEAN;
   L_tsf_packing_detail_exist   BOOLEAN;
   L_item_seasons_exist         BOOLEAN;
   L_tsf_xform_request_exist    BOOLEAN;
   L_exists                     BOOLEAN;
   L_flag                       BOOLEAN                           := FALSE;
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DIFF_IDS';
BEGIN
   /* This validation checks that the new diff id is not present in the DIFF_IDS
   and DIFF_GROUP_HEAD tables and that the diff id does not contain any blank
   spaces (' ') or underscores ('_')*/

   if I_rec.action = action_new then
      if DIFF_ID_EXISTS(O_error_message,
                        L_exists,
                        I_rec.diff_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_exists = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'DIFF_ID_GROUP_EXIST');
         O_error := TRUE;
      end if;
      if INSTR(I_rec.diff_id, ' ') > 0
         or INSTR(I_rec.diff_id, '_') > 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'ENT_VALID_DIFF_ID');
         O_error := TRUE;
      end if;
   end if;

   /* This validation checks that the diff type of a diff id is not changed
   during modification*/
   if I_rec.action = action_mod then
      if CHECK_DIFF_TYPE_CHANGE(O_error_message,
                                L_flag,
                                I_rec.diff_id,
                                I_rec.diff_type) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_TYPE',
                     O_error_message);
         O_error := TRUE;
      end if;
      if(L_flag = TRUE) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_TYPE',
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;

   /*this validation checks that the record to be deleted is not present in
   any of its child tables*/
   if I_rec.action = action_del then
      if CHECK_DELETE(O_error_message,
                      L_item_exist,
                      L_group_details_exist,
                      L_range_details_exist,
                      L_pack_tmpL_detail_exist,
                      L_tsf_packing_detail_exist,
                      L_tsf_xform_request_exist,
                      L_item_seasons_exist,
                      I_rec.diff_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     O_error_message);
         O_error:= TRUE;
      end if;
      if L_item_exist = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'ITEM_DIFF_EXISTS');
         O_error := TRUE;
      end if;
      if L_group_details_exist then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'DIFF_GROUP');
         O_error := TRUE;
      end if;
      if L_range_details_exist then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'DIFF_RANGE');
         O_error := TRUE;
      end if;
      if L_pack_tmpl_detail_exist then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'DFID_PACK_TMPL');
         O_error := TRUE;
      end if;
      if L_tsf_packing_detail_exist then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'DFID_TSF_PACKING');
         O_error := TRUE;
      end if;
      if L_tsf_xform_request_exist then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'DFID_TSF_XFORM_RQST');
         O_error := TRUE;
      end if;
      if L_item_seasons_exist then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DIFF_ID',
                     'DFID_ITEM_SEASONS');
         O_error := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_DIFF_IDS;
------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DIFF_IDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN       SVC_DIFF_IDS.PROCESS_ID%TYPE,
                          I_chunk_id        IN       SVC_DIFF_IDS.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.PROCESS_DIFF_IDS';
   L_error               BOOLEAN;
   L_process_error       BOOLEAN                           := FALSE;
   L_diff_ids_temp_rec   DIFF_IDS%ROWTYPE;
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DIFF_IDS';
BEGIN
   FOR rec IN c_svc_DIFF_IDS(I_process_id,I_chunk_id) LOOP
      L_error          := FALSE;
      L_process_error  := FALSE;
      if rec.action is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action is NOT NULL
         and rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.pk_diff_ids_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIFF_ID',
                     'DIFF_ID_GROUP_EXIST');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.diff_id is NOT NULL
         and rec.pk_diff_ids_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIFF_ID',
                     'INVALID_DIFF_ID');
         L_error := TRUE;
      end if;
      if rec.did_dte_fk_rid is NULL
         and rec.diff_type is NOT NULL
         and rec.action IN (action_new) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIFF_TYPE',
                     'INVALID_DIFF_TYPE');
         L_error := TRUE;
      end if;
      if NOT(rec.DIFF_ID  is NOT NULL) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIFF_ID',
                     'FIELD_NOT_NULL');
         L_error := TRUE;
      end if;
      if NOT(rec.DIFF_TYPE  is NOT NULL)
         and rec.action IN (action_mod,action_new) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIFF_TYPE',
                     'FIELD_NOT_NULL');
         L_error := TRUE;
      end if;
      if NOT(rec.DIFF_DESC  is NOT NULL)
         and rec.action IN (action_mod,action_new) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIFF_DESC',
                     'FIELD_NOT_NULL');
         L_error := TRUE;
      end if;
      -- Other validations that were present in the form
      if PROCESS_VAL_DIFF_IDS(O_error_message,
                              L_error,
                              rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_diff_ids_temp_rec.INDUSTRY_SUBGROUP    := rec.INDUSTRY_SUBGROUP;
         L_diff_ids_temp_rec.INDUSTRY_CODE        := rec.INDUSTRY_CODE;
         L_diff_ids_temp_rec.DIFF_DESC            := rec.DIFF_DESC;
         L_diff_ids_temp_rec.DIFF_TYPE            := rec.DIFF_TYPE;
         L_diff_ids_temp_rec.DIFF_ID              := UPPER(rec.DIFF_ID);
         L_diff_ids_temp_rec.CREATE_ID            := GET_USER;
         L_diff_ids_temp_rec.CREATE_DATETIME      := SYSDATE;
         L_diff_ids_temp_rec.LAST_UPDATE_ID       := GET_USER;
         L_diff_ids_temp_rec.LAST_UPDATE_DATETIME := SYSDATE;
         if rec.action = action_new then
            if EXEC_DIFF_IDS_INS(O_error_message,
                                 L_diff_ids_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_DIFF_IDS_UPD(O_error_message,
                                 L_diff_ids_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_DIFF_IDS_DEL(O_error_message,
                                 L_diff_ids_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DIFF_IDS;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DIFF_IDS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_DIFF_IDS_TL.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_DIFF_IDS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                      := 'CORESVC_DIFFID_DIFFTYPE.PROCESS_DIFF_IDS_TL';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DIFF_IDS_TL';
   L_error_message          RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DIFF_IDS_TL';
   L_error                  BOOLEAN := FALSE;
   L_process_error          BOOLEAN := FALSE;
   L_diff_ids_tl_temp_rec   DIFF_IDS_TL%ROWTYPE;
   L_diff_ids_tl_upd_rst    ROW_SEQ_TAB;
   L_diff_ids_tl_del_rst    ROW_SEQ_TAB;

   cursor C_SVC_DIFF_IDS_TL(I_process_id NUMBER,
                            I_chunk_id   NUMBER) is
      select pk_diff_ids_tl.rowid  as pk_diff_ids_tl_rid,
             fk_diff_ids.rowid     as fk_diff_ids_rid,
             fk_lang.rowid         as fk_lang,
             st.lang,
             st.diff_id,
             st.diff_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_diff_ids_tl  st,
             diff_ids         fk_diff_ids,
             diff_ids_tl      pk_diff_ids_tl,
             lang             fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.diff_id     =  fk_diff_ids.diff_id (+)
         and st.lang        =  pk_diff_ids_tl.lang (+)
         and st.diff_id     =  pk_diff_ids_tl.diff_id (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_diff_ids_TL is TABLE OF C_SVC_DIFF_IDS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_diff_ids_tl_tab   SVC_diff_ids_TL;

   L_diff_ids_tl_ins_tab   DIFF_IDS_TL_TAB  := NEW diff_ids_tl_tab();
   L_diff_ids_tl_upd_tab   DIFF_IDS_TL_TAB  := NEW diff_ids_tl_tab();
   L_diff_ids_tl_del_tab   DIFF_IDS_TL_TAB  := NEW diff_ids_tl_tab();

BEGIN
   if C_SVC_DIFF_IDS_TL%ISOPEN then
      close C_SVC_DIFF_IDS_TL;
   end if;

   open C_SVC_DIFF_IDS_TL(I_process_id,
                          I_chunk_id);
   LOOP
      fetch C_SVC_DIFF_IDS_TL bulk collect into L_svc_diff_ids_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_diff_ids_tl_tab.COUNT > 0 then
         FOR i in L_svc_diff_ids_tl_tab.FIRST..L_svc_diff_ids_tl_tab.LAST LOOP
            L_error := FALSE;

            --check if action is valid
            if L_svc_diff_ids_tl_tab(i).action is NULL
               or L_svc_diff_ids_tl_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_ids_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            --check for primary_lang
            if L_svc_diff_ids_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_ids_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            --check if primary key values already exist
            if L_svc_diff_ids_tl_tab(i).action = action_new
               and L_svc_diff_ids_tl_tab(i).pk_diff_ids_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_ids_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_diff_ids_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_diff_ids_tl_tab(i).lang is NOT NULL
               and L_svc_diff_ids_tl_tab(i).diff_id is NOT NULL
               and L_svc_diff_ids_tl_tab(i).pk_diff_ids_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_diff_ids_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            --check for FK
            if L_svc_diff_ids_tl_tab(i).action = action_new
               and L_svc_diff_ids_tl_tab(i).diff_id is NOT NULL
               and L_svc_diff_ids_tl_tab(i).fk_diff_ids_rid is NULL then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_diff_ids_tl_tab(i).row_seq,
                            'DIFF_ID',
                            'INVALID_DIFF_ID');
               L_error := TRUE;
            end if;

            if L_svc_diff_ids_tl_tab(i).action = action_new
               and L_svc_diff_ids_tl_tab(i).fk_lang is NULL
               and L_svc_diff_ids_tl_tab(i).lang is NOT NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_ids_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_diff_ids_tl_tab(i).action in (action_new, action_mod) and L_svc_diff_ids_tl_tab(i).diff_desc is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_ids_tl_tab(i).row_seq,
                           'DIFF_DESC',
                           'ENTER_DESC');
               L_error := TRUE;
            end if;

            if L_svc_diff_ids_tl_tab(i).diff_id is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_ids_tl_tab(i).row_seq,
                           'DIFF_ID',
                           'DIFF_ID_NOT_NULL');
               L_error := TRUE;
            end if;

            if L_svc_diff_ids_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_diff_ids_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if NOT L_error then
               L_diff_ids_tl_temp_rec.lang                 := L_svc_diff_ids_tl_tab(i).lang;
               L_diff_ids_tl_temp_rec.diff_desc            := L_svc_diff_ids_tl_tab(i).diff_desc;
               L_diff_ids_tl_temp_rec.diff_id              := L_svc_diff_ids_tl_tab(i).diff_id;
               L_diff_ids_tl_temp_rec.create_datetime      := SYSDATE;
               L_diff_ids_tl_temp_rec.create_id            := GET_USER;
               L_diff_ids_tl_temp_rec.last_update_datetime := SYSDATE;
               L_diff_ids_tl_temp_rec.last_update_id       := GET_USER;

               if L_svc_diff_ids_tl_tab(i).action = action_new then
                  L_diff_ids_tl_ins_tab.extend;
                  L_diff_ids_tl_ins_tab(L_diff_ids_tl_ins_tab.count()) := L_diff_ids_tl_temp_rec;
               end if;

               if L_svc_diff_ids_tl_tab(i).action = action_mod then
                  L_diff_ids_tl_upd_tab.extend;
                  L_diff_ids_tl_upd_tab(L_diff_ids_tl_upd_tab.count()) := L_diff_ids_tl_temp_rec;
                  L_diff_ids_tl_upd_rst(L_diff_ids_tl_upd_tab.count()) := L_svc_diff_ids_tl_tab(i).row_seq;
               end if;

               if L_svc_diff_ids_tl_tab(i).action = action_del then
                  L_diff_ids_tl_del_tab.extend;
                  L_diff_ids_tl_del_tab(L_diff_ids_tl_del_tab.count()) := L_diff_ids_tl_temp_rec;
                  L_diff_ids_tl_del_rst(L_diff_ids_tl_del_tab.count()) := L_svc_diff_ids_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_DIFF_IDS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_DIFF_IDS_TL;

   if EXEC_DIFF_IDS_TL_INS(O_error_message,
                           L_diff_ids_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_DIFF_IDS_TL_UPD(O_error_message,
                           L_diff_ids_tl_upd_tab,
                           L_diff_ids_tl_upd_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_DIFF_IDS_TL_DEL(O_error_message,
                           L_diff_ids_tl_del_tab,
                           L_diff_ids_tl_del_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DIFF_IDS_TL;
------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
       from svc_diff_type_tl
      where process_id = I_process_id;

   delete
       from svc_diff_type
      where process_id = I_process_id;

   delete
       from svc_diff_ids_tl
      where process_id = I_process_id;

   delete
       from svc_diff_ids
      where process_id = I_process_id;
END;
----------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count   IN OUT NUMBER,
                 I_process_id    IN     NUMBER,
                 I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    := 'CORESVC_DIFFID_DIFFTYPE.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_err_tab          CORESVC_CFLEX.TYP_ERR_TAB;

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_DIFF_TYPE(O_error_message,
                        I_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_DIFF_TYPE_TL(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;
   ---
   if CORESVC_CFLEX.PROCESS_CFA(O_error_message,
                                L_err_tab,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   FOR i in 1..L_err_tab.count LOOP
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_err_tab(i).view_name,
                  L_err_tab(i).row_seq,
                  L_err_tab(i).attrib,
                  L_err_tab(i).err_msg);
   END LOOP;
   ---
   if PROCESS_DIFF_IDS(O_error_message,
                       I_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_DIFF_IDS_TL(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;
   ---
   if PROCESS_DIFF_TYPE_DEL(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  C_GET_ERR_COUNT;
   fetch C_GET_ERR_COUNT into L_err_count;
   close C_GET_ERR_COUNT;

   open  C_GET_WARN_COUNT;
   fetch C_GET_WARN_COUNT into L_warn_count;
   close C_GET_WARN_COUNT;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                      when status = 'PE'
                      then 'PE'
                    else L_process_status
                    END),
          action_date = sysdate
    where process_id = I_process_id;
   --clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
---------------------------------------------------------------------------------------
END CORESVC_DIFFID_DIFFTYPE;
/