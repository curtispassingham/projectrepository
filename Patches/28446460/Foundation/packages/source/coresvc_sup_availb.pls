-- File Name : CORESVC_SUP_AVAIL_body.pls
CREATE OR REPLACE PACKAGE BODY CORESVC_SUP_AVAIL AS
   cursor C_SVC_SUP_AVAIL(I_process_id NUMBER,
                          I_chunk_id NUMBER) is
      select pk_sup_avail.rowid              as pk_sup_avail_rid,
             st.rowid                        as st_rid,
             sav_sup_fk.rowid                as sav_sup_fk_rid,
             sav_item_fk.item                as sav_item_fk_item,
		     sav_item_sup_fk.rowid           as  sav_item_sup_fk_rid,
             st.last_declared_date,
				 pk_sup_avail.last_declared_date as old_last_declared_date,
             st.last_update_date,
             st.qty_avail,
             UPPER(st.ref_item)              as ref_item,
				 pk_sup_avail.ref_item           as old_ref_item,
             UPPER(st.item)                  as item,
             st.supplier,
				 sav_item_fk.item_level          as item_level,
				 sav_item_fk.tran_level          as tran_level,
				 sav_item_fk.item_grandparent    as item_grandparent,
				 sav_item_fk.item_parent         as item_parent,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_sup_avail st,
             sup_avail     pk_sup_avail,
             sups          sav_sup_fk,
             v_item_master sav_item_fk,
				 item_supplier sav_item_sup_fk
       where st.process_id   = I_process_id
         and st.chunk_id     = I_chunk_id
         and st.supplier     = pk_sup_avail.supplier (+)
         and UPPER(st.item)  = pk_sup_avail.item (+)
         and st.supplier     = sav_sup_fk.supplier (+)
         and UPPER(st.item)  = sav_item_fk.item (+)
			and st.supplier     = sav_item_sup_fk.supplier(+)
			and UPPER(st.item)  = sav_item_sup_fk.item(+)
			and st.item is NOT NULL
			and st.ref_item is NULL
		union all
		select pk_sup_avail.rowid              as pk_sup_avail_rid,
             st.rowid                        as st_rid,
             sav_sup_fk.rowid                as sav_sup_fk_rid,
             sav_item_fk.item               as sav_item_fk_item,
				 sav_item_sup_fk.rowid           as  sav_item_sup_fk_rid,
             st.last_declared_date,
		       pk_sup_avail.last_declared_date as old_last_declared_date,
             st.last_update_date,
             st.qty_avail,
             UPPER(st.ref_item)              as ref_item,
			    pk_sup_avail.ref_item           as old_ref_item,
             decode((sav_item_fk.item_level-sav_item_fk.tran_level),2,sav_item_fk.item_grandparent,
                                                                    1,sav_item_fk.item_parent,'-1') item,
             st.supplier,
				 sav_item_fk.item_level          as item_level,
				 sav_item_fk.tran_level          as tran_level,
				 sav_item_fk.item_grandparent    as item_grandparent,
				 sav_item_fk.item_parent         as item_parent,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_sup_avail st,
             sup_avail     pk_sup_avail,
             sups          sav_sup_fk,
             v_item_master sav_item_fk,
				 item_supplier sav_item_sup_fk
       where st.process_id      = I_process_id
         and st.chunk_id        = I_chunk_id
         and st.supplier        = pk_sup_avail.supplier (+)
         and decode((sav_item_fk.item_level-sav_item_fk.tran_level),2,sav_item_fk.item_grandparent,
                                                                    1,sav_item_fk.item_parent,'-1') = pk_sup_avail.item (+)
         and st.supplier        = sav_sup_fk.supplier (+)
         and UPPER(st.ref_item) = sav_item_fk.item (+)
			and st.supplier        = sav_item_sup_fk.supplier(+)
			and decode((sav_item_fk.item_level-sav_item_fk.tran_level),2,sav_item_fk.item_grandparent,
                                                                    1,sav_item_fk.item_parent,'-1') = sav_item_sup_fk.item(+)
			and st.item is NULL
			and st.ref_item is NOT NULL
	    union all
		 select pk_sup_avail.rowid              as pk_sup_avail_rid,
             st.rowid                        as st_rid,
             sav_sup_fk.rowid                as sav_sup_fk_rid,
             sav_item_fk.item                as sav_item_fk_item,
				 sav_item_sup_fk.rowid           as  sav_item_sup_fk_rid,
             st.last_declared_date,
				 pk_sup_avail.last_declared_date as old_last_declared_date,
             st.last_update_date,
             st.qty_avail,
             UPPER(st.ref_item)              as ref_item,
				 pk_sup_avail.ref_item           as old_ref_item,
             UPPER(st.item),
             st.supplier,
				 sav_item_fk.item_level          as item_level,
				 sav_item_fk.tran_level          as tran_level,
				 sav_item_fk.item_grandparent    as item_grandparent,
				 sav_item_fk.item_parent         as item_parent,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) as action,
             st.process$status
        from svc_sup_avail st,
             sup_avail     pk_sup_avail,
             sups          sav_sup_fk,
             v_item_master sav_item_fk,
				 item_supplier sav_item_sup_fk
       where st.process_id       = I_process_id
         and st.chunk_id         = I_chunk_id
         and st.supplier         = pk_sup_avail.supplier (+)
         and UPPER(st.item)      = pk_sup_avail.item (+)
         and st.supplier         = sav_sup_fk.supplier (+)
         and UPPER(st.ref_item)  = sav_item_fk.item (+)
			and st.supplier         = sav_item_sup_fk.supplier(+)
			and UPPER(st.item)      = sav_item_sup_fk.item(+)
			and st.item is NOT NULL
			and st.ref_item is NOT NULL;
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
---------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   SUP_AVAIL_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                        := s9t_pkg.get_sheet_names(I_file_id);
   SUP_AVAIL_cols                  := s9t_pkg.get_col_names(I_file_id,SUP_AVAIL_sheet);
   SUP_AVAIL$Action                := SUP_AVAIL_cols('ACTION');
   SUP_AVAIL$LAST_DECLARED_DATE    := SUP_AVAIL_cols('LAST_DECLARED_DATE');
   SUP_AVAIL$LAST_UPDATE_DATE      := SUP_AVAIL_cols('LAST_UPDATE_DATE');
   SUP_AVAIL$QTY_AVAIL             := SUP_AVAIL_cols('QTY_AVAIL');
   SUP_AVAIL$REF_ITEM              := SUP_AVAIL_cols('REF_ITEM');
   SUP_AVAIL$ITEM                  := SUP_AVAIL_cols('ITEM');
   SUP_AVAIL$SUPPLIER              := SUP_AVAIL_cols('SUPPLIER');
END POPULATE_NAMES;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_SUP_AVAIL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SUP_AVAIL_sheet )
   select s9t_row(s9t_cells(CORESVC_SUP_AVAIL.action_mod ,
                            SUPPLIER,
									 ITEM,
                            REF_ITEM,
                            QTY_AVAIL,
                            LAST_UPDATE_DATE,
									 LAST_DECLARED_DATE))
     from sup_avail ;
END POPULATE_SUP_AVAIL;
-----------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(SUP_AVAIL_sheet);
   L_file.sheets(l_file.get_sheet_index(SUP_AVAIL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                       'SUPPLIER',
                                                                                       'ITEM',
                                                                                       'REF_ITEM',
                                                                                       'QTY_AVAIL',
                                                                                       'LAST_UPDATE_DATE',
                                                                                       'LAST_DECLARED_DATE');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_SUP_AVAIL.CREATE_S9T';
	L_file s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
	                          O_file_id,
                             template_category,
									  template_key)=FALSE then
	   return FALSE;
	end if;
   if I_template_only_ind = 'N' then
      POPULATE_SUP_AVAIL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
	                     template_category,
                        L_file)=FALSE then
	   return FALSE;
	end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SUP_AVAIL( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id IN   SVC_SUP_AVAIL.PROCESS_ID%TYPE) IS
   TYPE svc_sup_avail_col_typ IS TABLE OF SVC_SUP_AVAIL%ROWTYPE;
   L_temp_rec SVC_SUP_AVAIL%ROWTYPE;
   svc_sup_avail_col svc_sup_avail_col_typ :=NEW svc_sup_avail_col_typ();
   L_process_id  SVC_SUP_AVAIL.PROCESS_ID%TYPE;
   L_error       BOOLEAN:=FALSE;
   L_default_rec SVC_SUP_AVAIL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select LAST_DECLARED_DATE_mi,
             LAST_UPDATE_DATE_mi,
             QTY_AVAIL_mi,
             REF_ITEM_mi,
             ITEM_mi,
             SUPPLIER_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'SUP_AVAIL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('LAST_DECLARED_DATE' as LAST_DECLARED_DATE,
                                            'LAST_UPDATE_DATE'   as LAST_UPDATE_DATE,
                                            'QTY_AVAIL'          as QTY_AVAIL,
                                            'REF_ITEM'           as REF_ITEM,
                                            'ITEM'               as ITEM,
                                            'SUPPLIER'           as SUPPLIER,
                                             null                as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_SUP_AVAIL';
   L_pk_columns    VARCHAR2(255)  := 'Supplier,Item or Ref. item';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;      
BEGIN
  -- Get default values.
   FOR rec IN (select  LAST_DECLARED_DATE_dv,
                       LAST_UPDATE_DATE_dv,
                       QTY_AVAIL_dv,
                       REF_ITEM_dv,
                       ITEM_dv,
                       SUPPLIER_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                    = template_key
                          and wksht_key                                       = 'SUP_AVAIL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'LAST_DECLARED_DATE'  as LAST_DECLARED_DATE,
                                                      'LAST_UPDATE_DATE'    as LAST_UPDATE_DATE,
                                                      'QTY_AVAIL'           as QTY_AVAIL,
                                                      'REF_ITEM'            as REF_ITEM,
                                                      'ITEM'                as ITEM,
                                                      'SUPPLIER'            as SUPPLIER,
                                                       NULL                 as dummy)))
   LOOP
      BEGIN
         L_default_rec.LAST_DECLARED_DATE := rec.LAST_DECLARED_DATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_AVAIL ' ,
                            NULL,
                           'LAST_DECLARED_DATE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LAST_UPDATE_DATE := rec.LAST_UPDATE_DATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_AVAIL ' ,
                            NULL,
                           'LAST_UPDATE_DATE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.QTY_AVAIL := rec.QTY_AVAIL_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_AVAIL ' ,
                            NULL,
                           'QTY_AVAIL ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.REF_ITEM := rec.REF_ITEM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_AVAIL ' ,
                            NULL,
                           'REF_ITEM ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.ITEM := rec.ITEM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_AVAIL ' ,
                            NULL,
                           'ITEM ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.SUPPLIER := rec.SUPPLIER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'SUP_AVAIL ' ,
                            NULL,
                           'SUPPLIER ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(SUP_AVAIL$Action)                 as Action,
          r.get_cell(SUP_AVAIL$LAST_DECLARED_DATE)     as LAST_DECLARED_DATE,
          r.get_cell(SUP_AVAIL$LAST_UPDATE_DATE)       as LAST_UPDATE_DATE,
          r.get_cell(SUP_AVAIL$QTY_AVAIL)              as QTY_AVAIL,
          UPPER(r.get_cell(SUP_AVAIL$REF_ITEM))        as REF_ITEM,
          UPPER(r.get_cell(SUP_AVAIL$ITEM))            as ITEM,
          r.get_cell(SUP_AVAIL$SUPPLIER)               as SUPPLIER,
          r.get_row_seq()                              as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(SUP_AVAIL_sheet))
   LOOP
      L_temp_rec                   := NULL;        
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_AVAIL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LAST_DECLARED_DATE := TO_DATE(rec.LAST_DECLARED_DATE,'DD-MM-YY');
		EXCEPTION
		   when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_AVAIL_sheet,
                            rec.row_seq,
                            'LAST_DECLARED_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LAST_UPDATE_DATE := TO_DATE(rec.LAST_UPDATE_DATE,'DD-MM-YY');
      EXCEPTION
	      when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_AVAIL_sheet,
                            rec.row_seq,
                            'LAST_UPDATE_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.QTY_AVAIL := rec.QTY_AVAIL;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_AVAIL_sheet,
                            rec.row_seq,
                            'QTY_AVAIL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REF_ITEM := rec.REF_ITEM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_AVAIL_sheet,
                            rec.row_seq,
                            'REF_ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ITEM := rec.ITEM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_AVAIL_sheet,
                            rec.row_seq,
                            'ITEM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SUPPLIER := rec.SUPPLIER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SUP_AVAIL_sheet,
                            rec.row_seq,
                            'SUPPLIER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SUP_AVAIL.action_new then
         L_temp_rec.LAST_DECLARED_DATE := NVL( L_temp_rec.LAST_DECLARED_DATE,L_default_rec.LAST_DECLARED_DATE);
         L_temp_rec.LAST_UPDATE_DATE   := NVL( L_temp_rec.LAST_UPDATE_DATE,L_default_rec.LAST_UPDATE_DATE);
         L_temp_rec.QTY_AVAIL          := NVL( L_temp_rec.QTY_AVAIL,L_default_rec.QTY_AVAIL);
         L_temp_rec.REF_ITEM           := NVL( L_temp_rec.REF_ITEM,L_default_rec.REF_ITEM);
         L_temp_rec.ITEM               := NVL( L_temp_rec.ITEM,L_default_rec.ITEM);
         L_temp_rec.SUPPLIER           := NVL( L_temp_rec.SUPPLIER,L_default_rec.SUPPLIER);
      end if;
      if (L_temp_rec.SUPPLIER is NULL
          or (L_temp_rec.item is NULL 
              and L_temp_rec.ref_item is NULL))then
         WRITE_S9T_ERROR(I_file_id,
                         SUP_AVAIL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_sup_avail_col.extend();
         svc_sup_avail_col(svc_sup_avail_col.COUNT()):=L_temp_rec;
      end if;
 
   END LOOP;
   BEGIN
      forall i IN 1..svc_sup_avail_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SUP_AVAIL st
      using(select
                  (case
                   when L_mi_rec.LAST_DECLARED_DATE_mi    = 'N'
                    and svc_sup_avail_col(i).action = CORESVC_SUP_AVAIL.action_mod
                    and s1.LAST_DECLARED_DATE IS NULL
                   then mt.LAST_DECLARED_DATE
                   else s1.LAST_DECLARED_DATE
                   end) as LAST_DECLARED_DATE,
                  (case
                   when L_mi_rec.LAST_UPDATE_DATE_mi    = 'N'
                    and svc_sup_avail_col(i).action = CORESVC_SUP_AVAIL.action_mod
                    and s1.LAST_UPDATE_DATE IS NULL
                   then mt.LAST_UPDATE_DATE
                   else s1.LAST_UPDATE_DATE
                   end) as LAST_UPDATE_DATE,
                  (case
                   when L_mi_rec.QTY_AVAIL_mi    = 'N'
                    and svc_sup_avail_col(i).action = CORESVC_SUP_AVAIL.action_mod
                    and s1.QTY_AVAIL IS NULL
                   then mt.QTY_AVAIL
                   else s1.QTY_AVAIL
                   end) as QTY_AVAIL,
                  (case
                   when L_mi_rec.REF_ITEM_mi    = 'N'
                    and svc_sup_avail_col(i).action = CORESVC_SUP_AVAIL.action_mod
                    and s1.REF_ITEM IS NULL
                   then mt.REF_ITEM
                   else s1.REF_ITEM
                   end) as REF_ITEM,
                  (case
                   when L_mi_rec.ITEM_mi    = 'N'
                    and svc_sup_avail_col(i).action = CORESVC_SUP_AVAIL.action_mod
                    and s1.ITEM IS NULL
                   then mt.ITEM
                   else s1.ITEM
                   end) as ITEM,
                  (case
                   when L_mi_rec.SUPPLIER_mi    = 'N'
                    and svc_sup_avail_col(i).action = CORESVC_SUP_AVAIL.action_mod
                    and s1.SUPPLIER IS NULL
                   then mt.SUPPLIER
                   else s1.SUPPLIER
                   end) as SUPPLIER,
                  null as dummy
              from (select
                          svc_sup_avail_col(i).LAST_DECLARED_DATE as LAST_DECLARED_DATE,
                          svc_sup_avail_col(i).LAST_UPDATE_DATE as LAST_UPDATE_DATE,
                          svc_sup_avail_col(i).QTY_AVAIL as QTY_AVAIL,
                          svc_sup_avail_col(i).REF_ITEM as REF_ITEM,
                          svc_sup_avail_col(i).ITEM as ITEM,
                          svc_sup_avail_col(i).SUPPLIER as SUPPLIER,
                          null as dummy
                      from dual ) s1,
            SUP_AVAIL mt
             where mt.SUPPLIER (+)     = s1.SUPPLIER   
                   and mt.ITEM (+)     = s1.ITEM   
                   and nvl(mt.REF_ITEM(+),'-1')  = nvl(s1.REF_ITEM,'-1')
						 and 1 = 1 )sq
                on (st.SUPPLIER      = sq.SUPPLIER 
                    and st.ITEM      = sq.ITEM 
						  and st.REF_ITEM  = sq.REF_ITEM 
                    and svc_sup_avail_col(i).ACTION IN (CORESVC_SUP_AVAIL.action_mod,CORESVC_SUP_AVAIL.action_del))
      when matched then
      update
         set process_id            = svc_sup_avail_col(i).process_id ,
             chunk_id              = svc_sup_avail_col(i).chunk_id ,
             row_seq               = svc_sup_avail_col(i).row_seq ,
             action                = svc_sup_avail_col(i).action ,
             process$status        = svc_sup_avail_col(i).process$status ,
             qty_avail             = sq.qty_avail ,
             last_declared_date    = sq.last_declared_date ,
             last_update_date      = sq.last_update_date ,
             create_id             = svc_sup_avail_col(i).create_id ,
             create_datetime       = svc_sup_avail_col(i).create_datetime ,
             last_upd_id           = svc_sup_avail_col(i).last_upd_id ,
             last_upd_datetime     = svc_sup_avail_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             last_declared_date ,
             last_update_date ,
             qty_avail ,
             ref_item ,
             item ,
             supplier ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_sup_avail_col(i).process_id ,
             svc_sup_avail_col(i).chunk_id ,
             svc_sup_avail_col(i).row_seq ,
             svc_sup_avail_col(i).action ,
             svc_sup_avail_col(i).process$status ,
             sq.last_declared_date ,
             sq.last_update_date ,
             sq.qty_avail ,
             sq.ref_item ,
             sq.item ,
             sq.supplier ,
             svc_sup_avail_col(i).create_id ,
             svc_sup_avail_col(i).create_datetime ,
             svc_sup_avail_col(i).last_upd_id ,
             svc_sup_avail_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             SUP_AVAIL_sheet,
                             svc_sup_avail_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);                            
         END LOOP;
   END;
END PROCESS_S9T_SUP_AVAIL;
-----------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     IN OUT   NUMBER,
							 I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_SUP_AVAIL.PROCESS_S9T';
	L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	    EXCEPTION;
   PRAGMA	    EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if s9t_pkg.code2desc(O_error_message,
	                     template_category,
                        L_file,
                        TRUE)=FALSE then
	   return FALSE;
	end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_SUP_AVAIL(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_SUP_AVAIL_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error           IN OUT   BOOLEAN,
                               I_rec             IN       C_SVC_SUP_AVAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64):= 'CORESVC_SUP_AVAIL.PROCESS_SUP_AVAIL_VAL';
	L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_AVAIL';
   L_system_options_row     SYSTEM_OPTIONS%ROWTYPE;
	L_supplier_access_exists BOOLEAN;
   L_supplier_rec           SUPS%ROWTYPE;
	L_contract_exists        VARCHAR2(1) := 'N';
	cursor C_CHECK_CONTRACT is
      select 'Y'
        from contract_header ch,
             contract_detail cd
       where ch.contract_no = cd.contract_no
         and ch.supplier = I_rec.supplier
         and cd.item = I_rec.item
         and ch.contract_type = 'D'
      union all
      select 'Y'
        from contract_header ch,
             contract_cost cc
       where ch.contract_no = cc.contract_no
         and ch.supplier = I_rec.supplier
         and cc.item = I_rec.item
         and ch.contract_type = 'D';
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error :=TRUE;
   end if;
   if I_rec.action IN (action_new,action_mod)
		and I_rec.qty_avail is NOT NULL
	   and I_rec.QTY_AVAIL < 0then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'QTY_AVAIL',
                  'GREATER_0');
      O_error :=TRUE;
   end if;
	if I_rec.action=action_new then
	   if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(O_error_message,
                                                   L_supplier_access_exists,
                                                   L_supplier_rec,
                                                   I_rec.supplier) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SUPPLIER',
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_supplier_rec.sup_status != 'A' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SUPPLIER',
                     'INACTIVE_SUPPLIER');
         O_error := TRUE;
      end if;
      if NOT L_supplier_access_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SUPPLIER',
                     O_error_message);
         O_error := TRUE;
      elsif L_system_options_row.supplier_sites_ind = 'Y' 
		      and L_supplier_rec.supplier_parent is NULL then
            WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SUPPLIER',
                     'INV_SUPPLIER_SITE');
            O_error := TRUE;
      elsif L_system_options_row.supplier_sites_ind = 'N' 
		      and L_supplier_rec.supplier_parent is NOT NULL then
            WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SUPPLIER',
                     'INV_SUPPLIER');
            O_error := TRUE;
      end if;
		if I_rec.ref_item is NULL 
		   and I_rec.item is NOT NULL
		   and I_rec.item_level!=I_rec.tran_level then
		   WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'ITEM',
                     'NOT_ITEM_TRAN_LEVEL');
         O_error := TRUE;
		end if;
		if I_rec.sav_sup_fk_rid is NOT NULL
		   and I_rec.sav_item_fk_item is NOT NULL
			and I_rec.sav_item_sup_fk_rid is NULL then
		   WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Supplier,Item',
                     'SUPP_ITEM_NOT_EXIST');
		   O_error := TRUE;
		end if;
	   --validate ref_item for corresponding item
	   if I_rec.ref_item is NOT NULL then
         if I_rec.item_level<=I_rec.tran_level then
		      WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'REF_ITEM',
                        'INV_REF_ITEM_LEVEL');
            O_error :=TRUE;
			elsif I_rec.item is NOT NULL 
			      and (((I_rec.item_level-I_rec.tran_level)= 1 and I_rec.item_parent <> I_rec.item)
            or ((I_rec.item_level-I_rec.tran_level)= 2 and I_rec.item_grandparent <> I_rec.item))then
				WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'REF_ITEM',
                        'INV_REF_ITEM');
            O_error :=TRUE;
         end if;   				
	   end if; 
   end if;
	if I_rec.action=action_mod then
	   if NVL(I_rec.old_ref_item,'-1')<>NVL(I_rec.ref_item,'-1') then
		   WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REF_ITEM',
                     'CANNOT_MOD_REF_ITEM');
         O_error := TRUE;
		end if;
		if NVL(to_char(I_rec.last_declared_date,'DD-MM-YY'),'00-JAN-00')<> NVL(to_char(I_rec.old_last_declared_date,'DD-MM-YY'),'00-JAN-00') then
		   WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REF_ITEM',
                     'CANNOT_MOD_DECLARED_DATE');
         O_error := TRUE;
		end if;
	end if;  
	if I_rec.action=action_del then
	   open C_CHECK_CONTRACT;
      fetch C_CHECK_CONTRACT into L_contract_exists;
      close C_CHECK_CONTRACT;
      if L_contract_exists = 'Y' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Supplier,Item',
                     'CANNOT_DEL_SUP_AVAIL');
         O_error := TRUE;    
		end if;
	end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SUP_AVAIL_VAL;
---------------------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_AVAIL_INS( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sup_avail_temp_rec   IN       SUP_AVAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_SUP_AVAIL.EXEC_SUP_AVAIL_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_AVAIL';
BEGIN
   insert
     into sup_avail
   values I_sup_avail_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SUP_AVAIL_INS;
-------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_AVAIL_UPD( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sup_avail_temp_rec   IN       SUP_AVAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_SUP_AVAIL.EXEC_SUP_AVAIL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_AVAIL';
	RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
	cursor C_SUP_AVAIL_LOCK is
	   select  'X'
		  from sup_avail
		 where supplier = I_sup_avail_temp_rec.supplier
         and item = I_sup_avail_temp_rec.item
			for update nowait;
BEGIN
   open C_SUP_AVAIL_LOCK;
	close C_SUP_AVAIL_LOCK;
   update sup_avail
      set row = I_sup_avail_temp_rec
    where 1 = 1
      and supplier = I_sup_avail_temp_rec.supplier
      and item = I_sup_avail_temp_rec.item;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sup_avail_temp_rec.supplier,
                                             I_sup_avail_temp_rec.item);
      return FALSE;
   when OTHERS then
	   if C_SUP_AVAIL_LOCK%ISOPEN then
		   close C_SUP_AVAIL_LOCK;
		end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SUP_AVAIL_UPD;
------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_SUP_AVAIL_DEL( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sup_avail_temp_rec   IN       SUP_AVAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_SUP_AVAIL.EXEC_SUP_AVAIL_DEL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_AVAIL';
	RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
	cursor C_SUP_AVAIL_LOCK is
	   select  'X'
		  from sup_avail
		 where supplier = I_sup_avail_temp_rec.supplier
         and item = I_sup_avail_temp_rec.item
			for update nowait;
BEGIN
   open C_SUP_AVAIL_LOCK;
	close C_SUP_AVAIL_LOCK;
   delete
     from sup_avail
    where 1 = 1
      and supplier = I_sup_avail_temp_rec.supplier
      and item = I_sup_avail_temp_rec.item;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sup_avail_temp_rec.supplier,
                                             I_sup_avail_temp_rec.item);
      return FALSE;
   when OTHERS then
	   if C_SUP_AVAIL_LOCK%ISOPEN then
		   close C_SUP_AVAIL_LOCK;
		end if;
		O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SUP_AVAIL_DEL;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SUP_AVAIL( O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id     IN       SVC_SUP_AVAIL.PROCESS_ID%TYPE,
                            I_chunk_id       IN       SVC_SUP_AVAIL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      := 'CORESVC_SUP_AVAIL.PROCESS_SUP_AVAIL';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SUP_AVAIL';
   L_error              BOOLEAN;
   L_process_error      BOOLEAN := FALSE;
   L_sup_avail_temp_rec SUP_AVAIL%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_SUP_AVAIL(I_process_id,
                              I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
		if rec.action = action_new
         and rec.pk_sup_avail_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Supplier,Item',
                     'PK_SUP_AVAIL');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.pk_sup_avail_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Supplier,Item',
                     'PK_SUP_AVAIL_MISSING');
         L_error := TRUE;
      end if;
      if rec.action=action_new
		   and rec.sav_item_fk_item is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ITEM',
                     'INVALID_ITEM');
         L_error := TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
		   and rec.QTY_AVAIL  IS NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'QTY_AVAIL',
                     'ENTER_NEW_AVAIL');
         L_error := TRUE;
      end if;
      if PROCESS_SUP_AVAIL_VAL(O_error_message,
                               L_error,
                               rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;
      if NOT L_error then
         L_sup_avail_temp_rec.supplier              := rec.supplier;
         L_sup_avail_temp_rec.item                  := rec.item;
         L_sup_avail_temp_rec.ref_item              := rec.ref_item;
         L_sup_avail_temp_rec.qty_avail             := rec.qty_avail;
         L_sup_avail_temp_rec.last_declared_date    := rec.last_declared_date;
			if rec.action IN (action_new,action_mod) then
			   L_sup_avail_temp_rec.last_update_date      := GET_VDATE;
			end if;	
			if rec.action = action_new then
            if EXEC_SUP_AVAIL_INS(O_error_message,
                                  L_sup_avail_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_SUP_AVAIL_UPD(O_error_message,
                                  L_sup_avail_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_SUP_AVAIL_DEL( O_error_message,
                                   L_sup_avail_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_SUP_AVAIL;
------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete 
	   from svc_sup_avail 
	  where process_id = I_process_id;
END;
------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
						I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_SUP_AVAIL.PROCESS';
	L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_SUP_AVAIL(O_error_message,
	                     I_process_id,
                        I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
	if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
	    L_process_status := 'PE';
   end if;
		
   update svc_process_tracker
	    set status = (CASE
		                when status = 'PE'
		                then 'PE'
                    else L_process_status
                    END),
		  action_date = sysdate
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
	   CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_SUP_AVAIL;
/
