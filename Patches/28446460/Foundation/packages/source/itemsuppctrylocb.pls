CREATE OR REPLACE PACKAGE BODY ITEM_SUPP_COUNTRY_LOC_SQL AS
-----------------------------------------------------------------------------------------------
   TYPE ROWID_TBL IS TABLE OF ROWID;

-------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
-- Function Name: LOCK_ITEM_SUPP_COUNTRY_LOC
-- Purpose      : This function will lock the ITEM_SUPP_COUNTRY table for update or delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_SUPP_COUNTRY_LOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item          IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                                    I_supplier      IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                                    I_country       IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                                    I_loc           IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION LOC_EXISTS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists              IN OUT   BOOLEAN,
                    I_item                IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                    I_supplier            IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                    I_origin_country_id   IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                    I_loc                 IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.LOC_EXISTS';
   L_exists    VARCHAR2(1)  := 'N';

   cursor C_EXISTS is
      select 'Y'
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country_id
         and loc               = I_loc;
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| I_Loc);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| I_Loc);
   fetch C_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| I_Loc);
   close C_EXISTS;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOC_EXISTS;
-----------------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_loc               IN     ITEM_LOC.LOC%TYPE,
                         I_like_store        IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type          IN     ITEM_LOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(62)    := 'ITEM_SUPP_COUNTRY_LOC_SQL.CREATE_LOCATION';

   L_unit_cost     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_cost_priority NUMBER;
   L_store_type    STORE.STORE_TYPE%TYPE;
   L_source_wh     ITEM_LOC.SOURCE_WH%TYPE;
   L_wf_ind        VARCHAR2(1) := 'N';

   cursor C_GET_STORE_TYPE is
      select store_type
        from store
       where store = I_loc;

   cursor C_GET_SOURCE_WH is
      select source_wh
        from item_loc
       where loc = I_loc;

   cursor C_GET_WH_UNIT_COST is
      select 1 cost_priority, isl.unit_cost
        from item_supp_country_loc isl
       where isl.loc               = NVL(L_source_wh,-1)
         and isl.item              = I_item
         and isl.supplier          = NVL(I_supplier, supplier)
         and isl.origin_country_id = NVL(I_origin_country_id, origin_country_id)
      UNION ALL
      select 2 cost_priority, isc.unit_cost
        from item_supp_country isc
       where isc.item              = I_item
         and isc.primary_supp_ind  = 'Y'
      order by 1;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_loc_type = 'S' then
      SQL_LIB.SET_MARK('OPEN','C_GET_STORE_TYPE','STORE',' Location: '|| I_Loc);
      open C_GET_STORE_TYPE;
      
      SQL_LIB.SET_MARK('FETCH','C_GET_STORE_TYPE','STORE',' Location: '|| I_Loc);  
      fetch C_GET_STORE_TYPE into L_store_type;
      
      SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_TYPE','STORE',' Location: '|| I_Loc);
      close C_GET_STORE_TYPE;
      ---
      if L_store_type != 'C' then  ---franchise store
         L_wf_ind := 'Y';
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_SOURCE_WH','SOURCE_WH',' Location: '|| I_Loc);         
         open C_GET_SOURCE_WH;
         
         SQL_LIB.SET_MARK('FETCH','C_GET_SOURCE_WH','SOURCE_WH',' Location: '|| I_Loc);         
         fetch C_GET_SOURCE_WH into L_source_wh;

         SQL_LIB.SET_MARK('CLOSE','C_GET_SOURCE_WH','SOURCE_WH',' Location: '|| I_Loc);         
         close C_GET_SOURCE_WH;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_WH_UNIT_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_source_wh);         
         open C_GET_WH_UNIT_COST;
         
         SQL_LIB.SET_MARK('FETCH','C_GET_WH_UNIT_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_source_wh);         
         fetch C_GET_WH_UNIT_COST into L_cost_priority, L_unit_cost;

         SQL_LIB.SET_MARK('CLOSE','C_GET_WH_UNIT_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_source_wh);         
         close C_GET_WH_UNIT_COST;
         ---
      end if;
      ---
   end if;
   ---
   if CREATE_LOCATION(O_error_message,
                      I_item,
                      I_supplier,
                      I_origin_country_id,
                      I_loc,
                      NULL,
                      I_loc_type,
                      L_unit_cost,
                      L_wf_ind) = FALSE then

      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_LOCATION;
-----------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_LOC(O_error_message     IN OUT VARCHAR2,
                         O_exists            IN OUT BOOLEAN,
                         O_loc               IN OUT ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.GET_PRIMARY_LOC';

   cursor C_EXISTS is
      select loc
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country_id
         and primary_loc_ind   = 'Y';
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   O_loc := NULL;
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   open C_EXISTS;
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   fetch C_EXISTS into O_loc;
   ---
   if C_EXISTS%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   close C_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PRIMARY_LOC;
-----------------------------------------------------------------------------------------------
FUNCTION ALL_PACK_COMPONENTS_EXIST(O_error_message     IN OUT VARCHAR2,
                                   O_exists            IN OUT BOOLEAN,
                                   I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                                   I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                                   I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                                   I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.ALL_PACK_COMPONENTS_EXIST';
   L_exists     VARCHAR2(1);

   cursor C_PACK_ITEMS_EXIST is
      select 'x'
        from v_packsku_qty v
       where v.pack_no = I_item
         and not exists(select 'x'
                          from item_supp_country_loc
                         where item              = v.item
                           and supplier          = I_supplier
                           and origin_country_id = I_origin_country_id
                           and loc               = I_loc
                           and rownum = 1)
         and rownum = 1;
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_PACK_ITEMS_EXIST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                      ||' Location: '|| I_Loc);
   open C_PACK_ITEMS_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_PACK_ITEMS_EXIST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                      ||' Location: '|| I_Loc);
   fetch C_PACK_ITEMS_EXIST into L_exists;
   ---
   if C_PACK_ITEMS_EXIST%FOUND then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_PACK_ITEMS_EXIST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                      ||' Location: '|| I_Loc);
   close C_PACK_ITEMS_EXIST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ALL_PACK_COMPONENTS_EXIST;
-----------------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message     IN OUT VARCHAR2,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_loc               IN     Item_Loc.LOC%TYPE)
   RETURN BOOLEAN IS
BEGIN
   if CREATE_LOCATION(O_error_message,
                      I_item,
                      I_supplier,
                      I_origin_country_id,
                      I_loc,
                      NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;
END CREATE_LOCATION;
-----------------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message     IN OUT VARCHAR2,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_loc               IN     ITEM_LOC.LOC%TYPE,
                         I_like_store        IN     ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.CREATE_LOCATION';
   L_table                 VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY_LOC';
   L_min_loc               ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_buyer_pack            VARCHAR2(1)    := 'N';
   L_all_locs              VARCHAR2(1);
   L_dept                  ITEM_MASTER.DEPT%TYPE;
   L_update                VARCHAR2(1)    := NULL;
   L_exists                BOOLEAN;
   L_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
   L_unit_cost             ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_cost_priority         NUMBER(1);
   L_store_type            STORE.STORE_TYPE%TYPE;
   L_source_wh             ITEM_LOC.SOURCE_WH%TYPE;

   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_STORE_TYPE is
      select store_type
        from store
       where store = I_loc
      UNION ALL
      select 'x'
        from wh
       where wh = I_loc;

   cursor C_GET_SOURCE_WH is
      select source_wh
        from item_loc
       where loc = I_loc;

   cursor C_GET_WH_UNIT_COST is
      select 1 cost_priority, isl.unit_cost
        from item_supp_country_loc isl
       where isl.loc               = NVL(L_source_wh,-1)
         and isl.item              = I_item
         and isl.supplier          = NVL(I_supplier, supplier)
         and isl.origin_country_id = NVL(I_origin_country_id, origin_country_id)
      UNION ALL
      select 2 cost_priority, isc.unit_cost
        from item_supp_country isc
       where isc.item              = I_item
         and isc.primary_supp_ind  = 'Y'
      order by 1;

   cursor C_GET_DEPT is
      select dept
        from item_master
       where item = I_item;

   cursor C_GET_MIN_ISC_LOC is
      select MIN(isl.loc)
        from item_supp_country_loc isl
       where isl.item              = I_item;

   cursor C_UPDATE_PRIM_LOC_IND is
      select 'x'
        from item_supp_country_loc isl
       where isl.loc               = L_min_loc
         and isl.item              = I_item
         and isl.supplier          = NVL(I_supplier, supplier)
         and isl.origin_country_id = NVL(I_origin_country_id, origin_country_id)
         and not exists (select 'x'
                           from item_supp_country_loc isl3
                          where isl3.loc              != L_min_loc
                            and isl3.item              = isl.item
                            and isl3.supplier          = NVL(I_supplier, supplier)
                            and isl3.origin_country_id = NVL(I_origin_country_id, origin_country_id)
                            and isl3.primary_loc_ind   = 'Y'
                            and rownum = 1)
          for update nowait;


BEGIN
   if I_item is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            'NULL');
      return FALSE;
   end if;
   ---
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_STORE_TYPE','STORE',' Location: '|| I_Loc);   
   open C_GET_STORE_TYPE;

   SQL_LIB.SET_MARK('FETCH','C_GET_STORE_TYPE','STORE',' Location: '|| I_Loc);
   fetch C_GET_STORE_TYPE into L_store_type;
   
   SQL_LIB.SET_MARK('CLOSE','C_GET_STORE_TYPE','STORE',' Location: '|| I_Loc);
   close C_GET_STORE_TYPE;
   ---
   if L_store_type = 'F' then  --- wholesale/franchise store
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_SOURCE_WH','SOURCE_WH',' Location: '|| I_Loc);      
      open C_GET_SOURCE_WH;
      
      SQL_LIB.SET_MARK('FETCH','C_GET_SOURCE_WH','SOURCE_WH',' Location: '|| I_Loc);      
      fetch C_GET_SOURCE_WH into L_source_wh;

      SQL_LIB.SET_MARK('CLOSE','C_GET_SOURCE_WH','SOURCE_WH',' Location: '|| I_Loc);      
      close C_GET_SOURCE_WH;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_WH_UNIT_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_source_wh);      
      open C_GET_WH_UNIT_COST;
      
      SQL_LIB.SET_MARK('FETCH','C_GET_WH_UNIT_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_source_wh);      
      fetch C_GET_WH_UNIT_COST into L_cost_priority, L_unit_cost;
      
      SQL_LIB.SET_MARK('CLOSE','C_GET_WH_UNIT_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_source_wh);      
      close C_GET_WH_UNIT_COST;
      ---
   end if;

   if I_like_store IS NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      open C_GET_DEPT;
      SQL_LIB.SET_MARK('FETCH','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      fetch C_GET_DEPT into L_dept;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      close C_GET_DEPT;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
      insert into item_supp_country_loc(item,
                                        supplier,
                                        origin_country_id,
                                        loc,
                                        loc_type,
                                        primary_loc_ind,
                                        unit_cost,
                                        round_lvl,
                                        round_to_inner_pct,
                                        round_to_case_pct,
                                        round_to_layer_pct,
                                        round_to_pallet_pct,
                                        supp_hier_type_1,
                                        supp_hier_lvl_1,
                                        supp_hier_type_2,
                                        supp_hier_lvl_2,
                                        supp_hier_type_3,
                                        supp_hier_lvl_3,
                                        pickup_lead_time,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        negotiated_item_cost,
                                        extended_base_cost,
                                        inclusive_cost,
                                        base_cost,
                                        create_id)
                                 select I_item,
                                        isc.supplier,
                                        isc.origin_country_id,
                                        il.loc,
                                        il.loc_type,
                                        'N',
                                        isc.unit_cost,
                                        sim.round_lvl,
                                        sim.round_to_inner_pct,
                                        sim.round_to_case_pct,
                                        sim.round_to_layer_pct,
                                        sim.round_to_pallet_pct,
                                        isc.supp_hier_type_1,
                                        isc.supp_hier_lvl_1,
                                        isc.supp_hier_type_2,
                                        isc.supp_hier_lvl_2,
                                        isc.supp_hier_type_3,
                                        isc.supp_hier_lvl_3,
                                        isc.pickup_lead_time,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        isc.negotiated_item_cost,
                                        isc.extended_base_cost,
                                        isc.inclusive_cost,
                                        isc.base_cost,
                                        get_user
                                   from item_loc il,
                                        item_supp_country isc,
                                        sups s,
                                        sup_inv_mgmt sim,
                                        (select store location,
                                                store phy_loc,
                                                'N' finisher_ind
                                           from store
                                          union all
                                         select wh location,
                                                physical_wh phy_loc,
                                                finisher_ind
                                           from wh) loc
                                  where il.item = I_item
                                    and il.loc = NVL(I_loc, il.loc)
                                    and il.loc_type <> 'E'
                                    and isc.item = il.item
                                    and isc.supplier = NVL(I_supplier, isc.supplier)
                                    and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                    and s.supplier = isc.supplier
                                    and s.inv_mgmt_lvl = 'A'
                                    and loc.location = il.loc
                                    and loc.finisher_ind = 'N'
                                    and sim.supplier = isc.supplier
                                    and sim.dept     = L_dept
                                    and sim.location = loc.phy_loc
                                    and not exists(select 'x'
                                                     from item_supp_country_loc isl
                                                    where isl.item              = il.item
                                                      and isl.loc               = il.loc
                                                      and isl.supplier          = isc.supplier
                                                      and isl.origin_country_id = isc.origin_country_id
                                                      and rownum = 1)
                                 UNION ALL
                                 select I_item,
                                        isc.supplier,
                                        isc.origin_country_id,
                                        il.loc,
                                        il.loc_type,
                                        'N',
                                        isc.unit_cost,
                                        sim.round_lvl,
                                        sim.round_to_inner_pct,
                                        sim.round_to_case_pct,
                                        sim.round_to_layer_pct,
                                        sim.round_to_pallet_pct,
                                        isc.supp_hier_type_1,
                                        isc.supp_hier_lvl_1,
                                        isc.supp_hier_type_2,
                                        isc.supp_hier_lvl_2,
                                        isc.supp_hier_type_3,
                                        isc.supp_hier_lvl_3,
                                        isc.pickup_lead_time,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        isc.negotiated_item_cost,
                                        isc.extended_base_cost,
                                        isc.inclusive_cost,
                                        isc.base_cost,
                                        get_user
                                   from item_loc il,
                                        item_supp_country isc,
                                        sups s,
                                        sup_inv_mgmt sim,
                                        (select store location,
                                                store phy_loc,
                                                'N' finisher_ind
                                           from store
                                          union all
                                         select wh location,
                                                physical_wh phy_loc,
                                                finisher_ind
                                           from wh) loc
                                  where il.item = I_item
                                    and il.loc = NVL(I_loc, il.loc)
                                    and il.loc_type <> 'E'
                                    and isc.item = il.item
                                    and isc.supplier = NVL(I_supplier, isc.supplier)
                                    and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                    and s.supplier = isc.supplier
                                    and s.inv_mgmt_lvl = 'L'
                                    and loc.location = il.loc
                                    and loc.finisher_ind = 'N'
                                    and sim.supplier = isc.supplier
                                    and sim.dept is NULL
                                    and sim.location = loc.phy_loc
                                    and not exists(select 'x'
                                                     from item_supp_country_loc isl
                                                    where isl.item              = il.item
                                                      and isl.loc               = il.loc
                                                      and isl.supplier          = isc.supplier
                                                      and isl.origin_country_id = isc.origin_country_id
                                                      and rownum = 1)
                                 UNION ALL
                                 select I_item,
                                        isc.supplier,
                                        isc.origin_country_id,
                                        il.loc,
                                        il.loc_type,
                                        'N',
                                        isc.unit_cost,
                                        isc.round_lvl,
                                        isc.round_to_inner_pct,
                                        isc.round_to_case_pct,
                                        isc.round_to_layer_pct,
                                        isc.round_to_pallet_pct,
                                        isc.supp_hier_type_1,
                                        isc.supp_hier_lvl_1,
                                        isc.supp_hier_type_2,
                                        isc.supp_hier_lvl_2,
                                        isc.supp_hier_type_3,
                                        isc.supp_hier_lvl_3,
                                        isc.pickup_lead_time,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        isc.negotiated_item_cost,
                                        isc.extended_base_cost,
                                        isc.inclusive_cost,
                                        isc.base_cost,
                                        get_user
                                   from item_loc il,
                                        item_supp_country isc,
                                        (select store location,
                                                store phy_loc,
                                                'N' finisher_ind
                                           from store
                                          union all
                                         select wh location,
                                                physical_wh phy_loc,
                                                finisher_ind
                                           from wh) loc,
                                        sups s
                                  where il.item = I_item
                                    and loc.location = il.loc
                                    and loc.finisher_ind = 'N'
                                    and il.loc = NVL(I_loc, il.loc)
                                    and isc.item = il.item
                                    and isc.supplier = NVL(I_supplier, isc.supplier)
                                    and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                    and s.supplier = isc.supplier
                                    and il.loc_type <> 'E'
                                    and ((s.inv_mgmt_lvl = 'A'
                                             and not exists(select 'x'
                                                                   from sup_inv_mgmt sim2
                                                                  where sim2.supplier = isc.supplier
                                                                    and sim2.dept     = L_dept
                                                                    and sim2.location = loc.phy_loc
                                                                    and rownum = 1))
                                        or (s.inv_mgmt_lvl = 'L'
                                             and not exists(select 'x'
                                                              from sup_inv_mgmt sim2
                                                             where sim2.supplier = isc.supplier
                                                               and sim2.location = loc.phy_loc
                                                               and rownum = 1)))
                                    and not exists(select 'x'
                                                     from item_supp_country_loc isl
                                                    where isl.item              = il.item
                                                      and isl.loc               = il.loc
                                                      and isl.supplier          = isc.supplier
                                                      and isl.origin_country_id = isc.origin_country_id
                                                      and rownum = 1)
                                 UNION ALL
                                 select I_item,
                                        isc.supplier,
                                        isc.origin_country_id,
                                        il.loc,
                                        il.loc_type,
                                        'N',
                                        NVL(L_unit_cost,isc.unit_cost),  -- L_unit_cost is WH for WF stores
                                        isc.round_lvl,
                                        isc.round_to_inner_pct,
                                        isc.round_to_case_pct,
                                        isc.round_to_layer_pct,
                                        isc.round_to_pallet_pct,
                                        isc.supp_hier_type_1,
                                        isc.supp_hier_lvl_1,
                                        isc.supp_hier_type_2,
                                        isc.supp_hier_lvl_2,
                                        isc.supp_hier_type_3,
                                        isc.supp_hier_lvl_3,
                                        isc.pickup_lead_time,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        isc.negotiated_item_cost,
                                        isc.extended_base_cost,
                                        isc.inclusive_cost,
                                        isc.base_cost,
                                        get_user
                                   from item_loc il,
                                        item_supp_country isc,
                                        sups s
                                  where il.item = I_item
                                    and il.loc = NVL(I_loc, il.loc)
                                    and isc.item = il.item
                                    and isc.supplier = NVL(I_supplier, isc.supplier)
                                    and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                    and s.supplier = isc.supplier
                                    and (il.loc_type = 'E' or s.inv_mgmt_lvl in ('S', 'D'))
                                    and not exists(select 'x'
                                                     from item_supp_country_loc isl
                                                    where isl.item              = il.item
                                                      and isl.loc               = il.loc
                                                      and isl.supplier          = isc.supplier
                                                      and isl.origin_country_id = isc.origin_country_id
                                                      and rownum = 1);
      ---
      -- The minimum lowest location number will be selected to be the primary location on mass inserts.
      ---
      L_update := 'Y';
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
     open C_GET_MIN_ISC_LOC;
     SQL_LIB.SET_MARK('FETCH','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
     fetch C_GET_MIN_ISC_LOC into L_min_loc;
     ---
     if C_GET_MIN_ISC_LOC%NOTFOUND then
        L_update := 'N';
     end if;
     ---
     SQL_LIB.SET_MARK('CLOSE','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
     close C_GET_MIN_ISC_LOC;
     ---
     if L_update = 'Y' then
        SQL_LIB.SET_MARK('OPEN','C_UPDATE_PRIM_LOC_IND','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_min_loc);
        open C_UPDATE_PRIM_LOC_IND;
        SQL_LIB.SET_MARK('CLOSE','C_UPDATE_PRIM_LOC_IND','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_min_loc);
        close C_UPDATE_PRIM_LOC_IND;
        ---
        SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                    ||' Location: '|| L_min_loc);        
        update item_supp_country_loc isl
           set primary_loc_ind      = 'Y',
               last_update_datetime = sysdate,
               last_update_id       = get_user
         where isl.item              = I_item
           and isl.loc               = L_min_loc
           and isl.supplier          = NVL(I_supplier, supplier)
           and isl.origin_country_id = NVL(I_origin_country_id, origin_country_id)
           and not exists (select 'x'
                             from item_supp_country_loc isl3
                            where isl3.item              = isl.item
                              and isl3.loc              != isl.loc
                              and isl3.supplier          = NVL(I_supplier, supplier)
                              and isl3.origin_country_id = NVL(I_origin_country_id, origin_country_id)
                              and isl3.primary_loc_ind   = 'Y'
                              and rownum = 1);
      end if;
      ---
      if I_loc is NOT NULL then
         L_all_locs := 'N';
      else
         L_all_locs := 'Y';
      end if;
      ---
      if L_system_options_rec.bracket_costing_ind = 'Y' then
         if ITEM_BRACKET_COST_SQL.CREATE_BRACKET(O_error_message,
                                                I_item,
                                                I_supplier,
                                                I_origin_country_id,
                                                I_loc,
                                                L_all_locs) = FALSE then
           return FALSE;
        end if;
     end if;
   else  -- I_like_store IS NOT NULL --
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id
                       ||' Location: '|| I_like_store);
      insert into item_supp_country_loc(item,
                                        supplier,
                                        origin_country_id,
                                        loc,
                                        loc_type,
                                        primary_loc_ind,
                                        unit_cost,
                                        round_lvl,
                                        round_to_inner_pct,
                                        round_to_case_pct,
                                        round_to_layer_pct,
                                        round_to_pallet_pct,
                                        supp_hier_type_1,
                                        supp_hier_lvl_1,
                                        supp_hier_type_2,
                                        supp_hier_lvl_2,
                                        supp_hier_type_3,
                                        supp_hier_lvl_3,
                                        pickup_lead_time,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        negotiated_item_cost,
                                        extended_base_cost,
                                        inclusive_cost,
                                        base_cost,
                                        create_id)
                                 select I_item,
                                        iscl.supplier,
                                        iscl.origin_country_id,
                                        I_loc,
                                        iscl.loc_type,
                                        'N',
                                        iscl.unit_cost,
                                        iscl.round_lvl,
                                        iscl.round_to_inner_pct,
                                        iscl.round_to_case_pct,
                                        iscl.round_to_layer_pct,
                                        iscl.round_to_pallet_pct,
                                        iscl.supp_hier_type_1,
                                        iscl.supp_hier_lvl_1,
                                        iscl.supp_hier_type_2,
                                        iscl.supp_hier_lvl_2,
                                        iscl.supp_hier_type_3,
                                        iscl.supp_hier_lvl_3,
                                        iscl.pickup_lead_time,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        iscl.negotiated_item_cost,
                                        iscl.extended_base_cost,
                                        iscl.inclusive_cost,
                                        iscl.base_cost,
                                        get_user
                                   from item_supp_country_loc iscl
                                  where iscl.origin_country_id = NVL(I_origin_country_id, iscl.origin_country_id)
                                    and iscl.item = I_item
                                    and iscl.loc = I_like_store
                                    and iscl.supplier = NVL(I_supplier, iscl.supplier)
                                    and exists (select 'x'
                                                  from item_supp_country_loc iscl2
                                                 where iscl2.origin_country_id = iscl.origin_country_id
                                                   and iscl2.item = iscl.item
                                                   and iscl2.supplier = iscl.supplier
                                                   and iscl2.loc != I_loc
                                                   and iscl2.loc = iscl.loc
                                                   and rownum = 1);

      if L_system_options_rec.bracket_costing_ind = 'Y' then
        if ITEM_BRACKET_COST_SQL.CREATE_BRACKET(O_error_message,
                                                I_item,
                                                I_supplier,
                                                I_origin_country_id,
                                                I_loc,
                                                'N') = FALSE then

        return FALSE;
        end if;
     end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      -- if the record is locked skip the update because the
      -- item_supp_country_loc.primary_loc_ind is being updated by another
      -- thread for the same item/minimum location
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_LOCATION;
-----------------------------------------------------------------------------------------------
FUNCTION UPDATE_LOCATION(O_error_message     IN OUT VARCHAR2,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                         I_edit_cost         IN     VARCHAR2,
                         I_update_all_locs   IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.UPDATE_LOCATION';
   L_table               VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY_LOC';
   L_primary_loc_ind     VARCHAR2(20)   := NULL;
   L_round_lvl           ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE;
   L_round_to_inner_pct  ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE;
   L_round_to_case_pct   ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE;
   L_round_to_layer_pct  ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE;
   L_round_to_pallet_pct ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE;
   L_supp_hier_lvl_1     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE;
   L_supp_hier_type_1    ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_1%TYPE;
   L_supp_hier_lvl_2     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE;
   L_supp_hier_type_2    ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_2%TYPE;
   L_supp_hier_lvl_3     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE;
   L_supp_hier_type_3    ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_3%TYPE;
   L_pickup_lead_time    ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE;
   L_physical_wh         WH.PHYSICAL_WH%TYPE := NULL;
   L_system_options_rec  SYSTEM_OPTIONS%ROWTYPE := NULL;
   RECORD_LOCKED         EXCEPTION;
   PRAGMA                EXCEPTION_INIT(Record_Locked, -54);

   cursor C_PRIM_LOC_VIRTUAL is
      select wh.physical_wh
        from item_supp_country_loc iscl,
             wh
       where iscl.item              = I_item
         and iscl.supplier          = I_supplier
         and iscl.origin_country_id = I_origin_country_id
         and iscl.primary_loc_ind   = 'Y'
         and iscl.loc               = wh.wh
         and wh.stockholding_ind    = 'Y';

   cursor C_UPDATE_LOCATIONS is
      select round_lvl,
             round_to_inner_pct,
             round_to_case_pct,
             round_to_layer_pct,
             round_to_pallet_pct,
             supp_hier_lvl_1,
             supp_hier_type_1,
             supp_hier_lvl_2,
             supp_hier_type_2,
             supp_hier_lvl_3,
             supp_hier_type_3,
             pickup_lead_time
        from item_supp_country
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country_id;

   cursor C_LOCK_ALL_LOCS is
      select 'x'
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country_id
         for update nowait;

   cursor C_LOCK_PRIM_LOC is
      select 'x'
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country_id
         and primary_loc_ind   = 'Y'
         for update nowait;

   cursor C_LOCK_PRIM_VIRTUALS is
      select 'x'
        from item_supp_country_loc iscl, wh w
       where iscl.item              = I_item
         and iscl.supplier          = I_supplier
         and iscl.origin_country_id = I_origin_country_id
         and iscl.loc                 = w.wh
         and w.physical_wh            = L_physical_wh
      for update of item nowait;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_UPDATE_LOCATIONS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
   open C_UPDATE_LOCATIONS;
   SQL_LIB.SET_MARK('FETCH','C_UPDATE_LOCATIONS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
   fetch C_UPDATE_LOCATIONS into L_round_lvl,
                                 L_round_to_inner_pct,
                                 L_round_to_case_pct,
                                 L_round_to_layer_pct,
                                 L_round_to_pallet_pct,
                                 L_supp_hier_lvl_1,
                                 L_supp_hier_type_1,
                                 L_supp_hier_lvl_2,
                                 L_supp_hier_type_2,
                                 L_supp_hier_lvl_3,
                                 L_supp_hier_type_3,
                                 L_pickup_lead_time;
   SQL_LIB.SET_MARK('CLOSE','C_UPDATE_LOCATIONS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
   close C_UPDATE_LOCATIONS;
   ---
   if I_update_all_locs = 'Y' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ALL_LOCS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
      open C_LOCK_ALL_LOCS;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ALL_LOCS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
      close C_LOCK_ALL_LOCS;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
      update item_supp_country_loc
         set round_lvl            = L_round_lvl,
             round_to_inner_pct   = L_round_to_inner_pct,
             round_to_case_pct    = L_round_to_case_pct,
             round_to_layer_pct   = L_round_to_layer_pct,
             round_to_pallet_pct  = L_round_to_pallet_pct,
             supp_hier_lvl_1      = L_supp_hier_lvl_1,
             supp_hier_type_1     = L_supp_hier_type_1,
             supp_hier_lvl_2      = L_supp_hier_lvl_2,
             supp_hier_type_2     = L_supp_hier_type_2,
             supp_hier_lvl_3      = L_supp_hier_lvl_3,
             supp_hier_type_3     = L_supp_hier_type_3,
             pickup_lead_time     = L_pickup_lead_time,
             last_update_datetime = SYSDATE,
             last_update_id       = GET_USER
       where item                 = I_item
         and supplier             = I_supplier
         and origin_country_id    = I_origin_country_id;
      ---
   else  /* I_update_all_locs = 'N' */
      ---
      SQL_LIB.SET_MARK('OPEN','C_PRIM_LOC_VIRTUAL','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
      open C_PRIM_LOC_VIRTUAL;
      SQL_LIB.SET_MARK('FETCH','C_PRIM_LOC_VIRTUAL','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
      fetch C_PRIM_LOC_VIRTUAL into L_physical_wh;
      ---
      if C_PRIM_LOC_VIRTUAL%NOTFOUND then
         ---
         SQL_LIB.SET_MARK('CLOSE','C_PRIM_LOC_VIRTUAL','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
         close C_PRIM_LOC_VIRTUAL;
         ---
         SQL_LIB.SET_MARK('OPEN','C_LOCK_PRIM_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
         open C_LOCK_PRIM_LOC;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_PRIM_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
         close C_LOCK_PRIM_LOC;
         ---
         if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                  L_system_options_rec) = FALSE then
            return FALSE;
         end if;

         if L_system_options_rec.default_tax_type != 'GTAX' then
            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                             ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);         
            update item_supp_country_loc
               set round_lvl            = nvl(L_round_lvl, round_lvl),
                   round_to_inner_pct   = nvl(L_round_to_inner_pct, round_to_inner_pct),
                   round_to_case_pct    = nvl(L_round_to_case_pct, round_to_case_pct),
                   round_to_layer_pct   = nvl(L_round_to_layer_pct, round_to_layer_pct),
                   round_to_pallet_pct  = nvl(L_round_to_pallet_pct, round_to_pallet_pct),
                   supp_hier_lvl_1      = nvl(L_supp_hier_lvl_1, supp_hier_lvl_1),
                   supp_hier_type_1     = nvl(L_supp_hier_type_1, supp_hier_type_1),
                   supp_hier_lvl_2      = nvl(L_supp_hier_lvl_2, supp_hier_lvl_2),
                   supp_hier_type_2     = nvl(L_supp_hier_type_2, supp_hier_type_2),
                   supp_hier_lvl_3      = nvl(L_supp_hier_lvl_3, supp_hier_lvl_3),
                   supp_hier_type_3     = nvl(L_supp_hier_type_3, supp_hier_type_3),
                   pickup_lead_time     = nvl(L_pickup_lead_time, pickup_lead_time),
                   last_update_datetime = SYSDATE,
                   last_update_id       = GET_USER
             where item                 = I_item
               and supplier             = I_supplier
               and origin_country_id    = I_origin_country_id
               and primary_loc_ind      = 'Y';
         end if;
         ---
      else  /* C_PRIM_LOC_VIRTUAL%FOUND */
         ---
         SQL_LIB.SET_MARK('CLOSE','C_PRIM_LOC_VIRTUAL','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
         close C_PRIM_LOC_VIRTUAL;
         ---
         SQL_LIB.SET_MARK('OPEN','C_LOCK_PRIM_VIRTUALS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
         open C_LOCK_PRIM_VIRTUALS;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_PRIM_VIRTUALS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
         close C_LOCK_PRIM_VIRTUALS;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);         
         update item_supp_country_loc iscl
            set round_lvl            = nvl(L_round_lvl, round_lvl),
                round_to_inner_pct   = nvl(L_round_to_inner_pct, round_to_inner_pct),
                round_to_case_pct    = nvl(L_round_to_case_pct, round_to_case_pct),
                round_to_layer_pct   = nvl(L_round_to_layer_pct, round_to_layer_pct),
                round_to_pallet_pct  = nvl(L_round_to_pallet_pct, round_to_pallet_pct),
                supp_hier_lvl_1      = nvl(L_supp_hier_lvl_1, supp_hier_lvl_1),
                supp_hier_type_1     = nvl(L_supp_hier_type_1, supp_hier_type_1),
                supp_hier_lvl_2      = nvl(L_supp_hier_lvl_2, supp_hier_lvl_2),
                supp_hier_type_2     = nvl(L_supp_hier_type_2, supp_hier_type_2),
                supp_hier_lvl_3      = nvl(L_supp_hier_lvl_3, supp_hier_lvl_3),
                supp_hier_type_3     = nvl(L_supp_hier_type_3, supp_hier_type_3),
                pickup_lead_time     = nvl(L_pickup_lead_time, pickup_lead_time),
                last_update_datetime = SYSDATE,
                last_update_id       = GET_USER
          where item                 = I_item
            and supplier             = I_supplier
            and origin_country_id    = I_origin_country_id
            and exists                (select 'x'
                                         from wh
                                        where iscl.loc = wh.wh
                                          and wh.physical_wh = L_physical_wh);
         ---
      end if;
      ---
   end if;
   ---
   if I_edit_cost = 'Y' then
      if UPDATE_BASE_COST.CHANGE_ISC_COST(O_error_message,
                                          I_item,
                                          I_supplier,
                                          I_origin_country_id,
                                          I_update_all_locs) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             'Item :'||I_item,
                                             'Supplier :'||to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_LOCATION;
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_LOCATION(O_error_message     IN OUT VARCHAR2,
                        O_exists            IN OUT BOOLEAN,
                        I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                        I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                        I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.CHECK_LOCATION';
   L_exists    VARCHAR2(1)    := 'N';

   cursor C_CHECK_COUNTRY_LOC is
      select 'Y'
        from item_supp_country i
       where i.item              = I_item
         and i.supplier          = I_supplier
         and i.origin_country_id = I_origin_country_id
         and exists (select 'x'
                       from item_supp_country_loc i2
                      where i2.item                 = I_item
                        and i2.supplier             = I_supplier
                        and i2.origin_country_id    = I_origin_country_id
                        and (i2.unit_cost          != i.unit_cost
                         or NVL(i2.supp_hier_lvl_1,     'x')  != NVL(i.supp_hier_lvl_1,     'x')
                         or NVL(i2.supp_hier_lvl_2,     'x')  != NVL(i.supp_hier_lvl_2,     'x')
                         or NVL(i2.supp_hier_lvl_3,     'x')  != NVL(i.supp_hier_lvl_3,     'x')
                         or NVL(i2.pickup_lead_time,    -999) != NVL(i.pickup_lead_time,    -999)
                         or NVL(i2.round_lvl,           'x')  != NVL(i.round_lvl,           'x')
                         or NVL(i2.round_to_case_pct,   -999) != NVL(i.round_to_case_pct,   -999)
                         or NVL(i2.round_to_layer_pct,  -999) != NVL(i.round_to_layer_pct,  -999)
                         or NVL(i2.round_to_pallet_pct, -999) != NVL(i.round_to_pallet_pct, -999)));


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;      
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   open C_CHECK_COUNTRY_LOC;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   fetch C_CHECK_COUNTRY_LOC into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   close C_CHECK_COUNTRY_LOC;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_LOCATION;
-----------------------------------------------------------------------------------------------
FUNCTION DEFAULT_LOCATION_TO_CHILDREN(O_error_message       IN OUT VARCHAR2,
                                      I_item                IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                                      I_supplier            IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                                      I_origin_country_id   IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                                      I_edit_cost           IN     VARCHAR2,
                                      I_update_all_locs     IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_SQL.DEFAULT_LOCATION_TO_CHILDREN';
   L_child_item            ITEM_MASTER.ITEM%TYPE;
   L_insert_update_locs    VARCHAR2(1)    := 'N';

   cursor C_GET_CHILD_ITEMS is
       select item
         from item_master i
        where (i.item_parent         = I_item
               or i.item_grandparent = I_item)
          and i.item_level          <= i.tran_level
          and exists (select 'x'
                        from item_supp_country isc
                       where isc.item              = i.item
                         and isc.supplier          = I_supplier
                         and isc.origin_country_id = I_origin_country_id);


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   -- If I_update_all_locs is passed in as NULL, then both update and create logic will be performed. --
   ---
   if I_update_all_locs is NULL then
      L_insert_update_locs := 'Y';
   end if;
   ---
   
   SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS','ITEM_MASTER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);      
   for C_rec in C_GET_CHILD_ITEMS loop
      L_child_item := C_rec.item;
      ---
      if L_insert_update_locs = 'Y' then
         if ITEM_SUPP_COUNTRY_LOC_SQL.UPDATE_LOCATION(O_error_message,
                                                      L_child_item,
                                                      I_supplier,
                                                      I_origin_country_id,
                                                      I_edit_cost,
                                                      NVL(I_update_all_locs,L_insert_update_locs)) = FALSE then
            return FALSE;
         end if;
         ---
         if ITEM_SUPP_COUNTRY_LOC_SQL.CREATE_LOCATION(O_error_message,
                                                      L_child_item,
                                                      I_supplier,
                                                      I_origin_country_id,
                                                      NULL) = FALSE then
            return FALSE;
         end if;
      else
          if ITEM_SUPP_COUNTRY_LOC_SQL.UPDATE_LOCATION(O_error_message,
                                                       L_child_item,
                                                       I_supplier,
                                                       I_origin_country_id,
                                                       I_edit_cost,
                                                       I_update_all_locs) = FALSE then
            return FALSE;
         end if;
      end if;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_LOCATION_TO_CHILDREN;
-----------------------------------------------------------------------------------------------------------
FUNCTION DEFAULT_LOC_TO_CHILDREN_ISCL(O_error_message       IN OUT VARCHAR2,
                                      I_item                IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                                      I_supplier            IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                                      I_origin_country_id   IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                                      I_loc                 IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_SQL.DEFAULT_LOC_TO_CHILDREN_ISCL';
   L_child_item            ITEM_MASTER.ITEM%TYPE;
  

   cursor C_GET_CHILD_ITEMS is
       select im.item, isc.unit_cost,isc.base_cost,isc.negotiated_item_cost,isc.extended_base_cost,isc.inclusive_cost
         from item_master im, item_supp_country isc
        where (im.item_parent         = I_item
               or im.item_grandparent = I_item)
          and im.item_level          <= im.tran_level
          and isc.item              = im.item
          and isc.supplier          = I_supplier
          and isc.origin_country_id = I_origin_country_id
          and exists (select 'x'
                        from  item_loc il
                        where il.item = im.item
                        and   il.loc=I_loc);
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   

   SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS','ITEM_MASTER','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   for C_rec in C_GET_CHILD_ITEMS loop
      L_child_item := C_rec.item;
      ---
       merge into item_supp_country_loc iscl
                       using (select I_item as item,
                                     I_supplier as supplier,
                                     I_origin_country_id as origin_country_id,
                                     I_loc as loc,
                                     loc_type,
                                     primary_loc_ind,
                                     unit_cost,
                                     round_lvl,
                                     round_to_inner_pct,
                                     round_to_case_pct ,
                                     round_to_layer_pct ,
                                     round_to_pallet_pct ,
                                     supp_hier_type_1 ,
                                     supp_hier_lvl_1 ,
                                     supp_hier_type_2 ,
                                     supp_hier_lvl_2 ,
                                     supp_hier_type_3 ,
                                     supp_hier_lvl_3 ,
                                     pickup_lead_time ,
                                     SYSDATE as create_datetime,
                                     SYSDATE as last_update_datetime,
                                     USER as last_update_id,
                                     negotiated_item_cost ,
                                     extended_base_cost ,
                                     inclusive_cost ,
                                     base_cost,
                                     USER as create_id
                               from item_supp_country_loc
                              where item = I_item
                                and supplier = I_supplier
                                and origin_country_id = I_origin_country_id
                                and loc = I_loc) temp
                          on (temp.origin_country_id = iscl.origin_country_id
                              and temp.supplier = iscl.supplier
                              and L_child_item = iscl.item
                              and temp.loc = iscl.loc)
                        when matched then
                      update
                         set round_lvl            = temp.round_lvl,
                             round_to_inner_pct   = temp.round_to_inner_pct,
                             round_to_pallet_pct  = temp.round_to_pallet_pct,
                             round_to_case_pct    = temp.round_to_case_pct,
                             round_to_layer_pct   = temp.round_to_layer_pct,
                             supp_hier_lvl_1      = temp.supp_hier_lvl_1,
                             supp_hier_type_1     = temp.supp_hier_type_1,                    
                             supp_hier_lvl_2      = temp.supp_hier_lvl_2,
                             supp_hier_type_2     = temp.supp_hier_type_2,
                             supp_hier_lvl_3      = temp.supp_hier_lvl_3,
                             supp_hier_type_3     = temp.supp_hier_type_3,
                             pickup_lead_time     = temp.pickup_lead_time,
                             last_update_id          = temp.last_update_id,
                             last_update_datetime    = temp.last_update_datetime
                        when not matched then
                      insert (item,
                              supplier,
                              origin_country_id,
                              loc,
                              loc_type,
                              primary_loc_ind,
                              unit_cost,
                              round_lvl,
                              round_to_inner_pct,
                              round_to_case_pct,
                              round_to_layer_pct,
                              round_to_pallet_pct,
                              supp_hier_type_1,
                              supp_hier_lvl_1,
                              supp_hier_type_2,
                              supp_hier_lvl_2,
                              supp_hier_type_3,
                              supp_hier_lvl_3,
                              pickup_lead_time,
                              create_datetime,
                              last_update_datetime,
                              last_update_id,
                              negotiated_item_cost,
                              extended_base_cost,
                              inclusive_cost,
                              base_cost,
                              create_id
                              )
                      values (L_child_item,
                              temp.supplier,
                              temp.origin_country_id,
                              temp.loc,
                              temp.loc_type,
                              temp.primary_loc_ind,
                              DECODE(temp.unit_cost,C_rec.unit_cost,temp.unit_cost,C_rec.unit_cost),
                              temp.round_lvl,
                              temp.round_to_inner_pct,
                              temp.round_to_case_pct,
                              temp.round_to_layer_pct,
                              temp.round_to_pallet_pct,
                              temp.supp_hier_type_1,
                              temp.supp_hier_lvl_1,
                              temp.supp_hier_type_2,
                              temp.supp_hier_lvl_2,
                              temp.supp_hier_type_3,
                              temp.supp_hier_lvl_3,
                              temp.pickup_lead_time,
                              temp.create_datetime,
                              temp.last_update_datetime,
                              temp.last_update_id,
                              DECODE(temp.negotiated_item_cost,C_rec.negotiated_item_cost,temp.negotiated_item_cost,C_rec.negotiated_item_cost),
                              DECODE(temp.extended_base_cost,C_rec.extended_base_cost,temp.extended_base_cost,C_rec.extended_base_cost),
                              DECODE(temp.inclusive_cost,C_rec.inclusive_cost,temp.inclusive_cost,C_rec.inclusive_cost),
                              DECODE(temp.base_cost,C_rec.base_cost,temp.base_cost,C_rec.base_cost),
                              temp.create_id
                               );
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEFAULT_LOC_TO_CHILDREN_ISCL;
------------------------------------------------------------------------------------------------------------
FUNCTION MASS_UPDATE(O_error_message       IN OUT VARCHAR2,
                     O_virtual_wh_exists   IN OUT BOOLEAN,
                     I_group_type          IN     VARCHAR2,
                     I_group_value         IN     VARCHAR2,
                     I_unit_cost           IN     ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                     I_change_rnd_lvl_ind  IN     VARCHAR2,
                     I_round_lvl           IN     ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
                     I_change_inner_ind    IN     VARCHAR2,
                     I_round_to_inner_pct  IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
                     I_change_case_ind     IN     VARCHAR2,
                     I_round_to_case_pct   IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
                     I_change_layer_ind    IN     VARCHAR2,
                     I_round_to_layer_pct  IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
                     I_change_pallet_ind   IN     VARCHAR2,
                     I_round_to_pallet_pct IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
                     I_change_lvl_1        IN     VARCHAR2,
                     I_supp_hier_lvl_1     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE,
                     I_change_lvl_2        IN     VARCHAR2,
                     I_supp_hier_lvl_2     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE,
                     I_change_lvl_3        IN     VARCHAR2,
                     I_supp_hier_lvl_3     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE,
                     I_change_pickup_ind   IN     VARCHAR2,
                     I_pickup_lead_time    IN     ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                     I_item                IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                     I_supplier            IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                     I_origin_country_id   IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                     I_process_children    IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.MASS_UPDATE';
   L_supp_hier_type_1   ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_1%TYPE := NULL;
   L_supp_hier_type_2   ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_2%TYPE := NULL;
   L_supp_hier_type_3   ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_3%TYPE := NULL;
   L_loc                ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_unit_cost          ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE;
   L_primary_loc_ind    ITEM_SUPP_COUNTRY_LOC.PRIMARY_LOC_IND%TYPE;
   L_where_clause       VARCHAR2(3000);
   L_no_update          VARCHAR2(100);
   L_group_label        CODE_DETAIL.CODE_DESC%TYPE;
   ---
   sql_stmt             VARCHAR2(3000);
   lock_stmt            VARCHAR2(3000);
   L_sql_stmt_cur       PLS_INTEGER;
   L_fetch_rows         INTEGER;
   L_lock_stmt_cur      PLS_INTEGER;
   TYPE LOC_CURSOR is REF CURSOR;
   C_LOC                LOC_CURSOR;
   ---
   L_table              VARCHAR2(30) := 'ITEM_SUPP_COUNTRY_LOC';
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   L_tax_calc_rec       OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_tax_calc_tbl       OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_inclusive_cost     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_tax_rate           ITEM_COST_DETAIL.COMP_RATE%TYPE := 0;
   L_default_tax_type   SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;    
   ---
   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if; 
   
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
   if I_group_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_group_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;      
   ----
   --- SET DYNAMIC WHERE CLAUSE
   ----
   if I_group_type in ('LLW','W') then
      ---
      if LOCATION_ATTRIB_SQL.BUILD_GROUP_TYPE_VIRTUAL_WHERE(O_error_message,
                                                            L_where_clause,
                                                            'loc',
                                                            I_group_type,
                                                            I_group_value) = FALSE then
         return FALSE;
      end if;
      ---
      if I_group_type = 'W' then
         L_where_clause := LPAD(L_where_clause, LENGTH(L_where_clause) - 1);
   
         L_where_clause := L_where_clause
                           || ' union '
                           || '(select s.store '
                           || 'from store s, '
                           || 'item_supp_country_loc iscl '
                           || 'where s.store_type = ''W'' '
                           || 'and iscl.loc = s.store '
                           || 'and iscl.item = ''' || I_item || ''' '
                           || 'and iscl.supplier = ' || I_supplier || ' '
                           || 'and iscl.origin_country_id = ''' || I_origin_country_id || ''' '
                           || '))';
      end if;
   else
      ---
      if LOCATION_ATTRIB_SQL.BUILD_GROUP_TYPE_WHERE_CLAUSE(O_error_message,
                                                           L_where_clause,
                                                           I_group_type,
                                                           I_group_value,
                                                           'loc') = FALSE then
         return FALSE;
      end if;
      ---
   end if;
   ---
   if I_group_type != 'AL' then
      L_where_clause := LPAD(L_where_clause, LENGTH(L_where_clause) + 5, ' and ');
   end if;
   ---
   if I_supp_hier_lvl_1 is not NULL then
      L_supp_hier_type_1 := 'S1';
   end if;
   ---
   if I_supp_hier_lvl_2 is not NULL then
      L_supp_hier_type_2 := 'S2';
   end if;
   ---
   if I_supp_hier_lvl_3 is not NULL then
      L_supp_hier_type_3 := 'S3';
   end if;

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then
      return FALSE;
   end if;

   ---
   -- LOCK RECORDS FOR UPDATE
   ---
   SQL_LIB.SET_MARK('UPDATE', NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   ---
   L_no_update := ' for update nowait';
   ---
   lock_stmt := 'select loc,
                        primary_loc_ind,
                        unit_cost
                   from item_supp_country_loc
                  where item              = :I_item
                    and supplier          = :I_supplier
                    and origin_country_id = :I_origin_country_id
                    and (loc in (select store from store)
                         or loc in (select wh from wh))' || L_where_clause;
   
   L_lock_stmt_cur             := DBMS_SQL.OPEN_CURSOR;
      
   DBMS_SQL.PARSE(L_lock_stmt_cur,lock_stmt || L_no_update,DBMS_SQL.NATIVE);
            
   DBMS_SQL.BIND_VARIABLE(L_lock_stmt_cur, ':I_item', I_item);
   DBMS_SQL.BIND_VARIABLE(L_lock_stmt_cur, ':I_supplier', I_supplier);
   DBMS_SQL.BIND_VARIABLE(L_lock_stmt_cur, ':I_origin_country_id', I_origin_country_id);
          
   L_fetch_rows := DBMS_SQL.EXECUTE(L_lock_stmt_cur);
            
   DBMS_SQL.CLOSE_CURSOR (L_lock_stmt_cur);                         
   
   ---
   -- UPDATE RECORDS ON ITEM_SUPP_COUNRY_LOC
   ---
   L_default_tax_type := L_system_options_rec.default_tax_type;
   L_fetch_rows := 0;
   if L_system_options_rec.default_tax_type = 'GTAX' then
      
      sql_stmt := 'update item_supp_country_loc
            set unit_cost            = NVL(:I_unit_cost, unit_cost),
                round_lvl            = DECODE(:I_change_rnd_lvl_ind, ''Y'', :I_round_lvl,           round_lvl),
                round_to_inner_pct   = DECODE(:I_change_inner_ind,   ''Y'', :I_round_to_inner_pct,  round_to_inner_pct),
                round_to_case_pct    = DECODE(:I_change_case_ind,    ''Y'', :I_round_to_case_pct,   round_to_case_pct),
                round_to_layer_pct   = DECODE(:I_change_layer_ind,   ''Y'', :I_round_to_layer_pct,  round_to_layer_pct),
                round_to_pallet_pct  = DECODE(:I_change_pallet_ind,  ''Y'', :I_round_to_pallet_pct, round_to_pallet_pct),
                supp_hier_type_1     = DECODE(:I_change_lvl_1,       ''Y'', :L_supp_hier_type_1,    supp_hier_type_1),
                supp_hier_lvl_1      = DECODE(:I_change_lvl_1,       ''Y'', :I_supp_hier_lvl_1,     supp_hier_lvl_1),
                supp_hier_type_2     = DECODE(:I_change_lvl_2,       ''Y'', :L_supp_hier_type_2,    supp_hier_type_2),
                supp_hier_lvl_2      = DECODE(:I_change_lvl_2,       ''Y'', :I_supp_hier_lvl_2,     supp_hier_lvl_2),
                supp_hier_type_3     = DECODE(:I_change_lvl_3,       ''Y'', :L_supp_hier_type_3,    supp_hier_type_3),
                supp_hier_lvl_3      = DECODE(:I_change_lvl_3,       ''Y'', :I_supp_hier_lvl_3,     supp_hier_lvl_3),
                pickup_lead_time     = DECODE(:I_change_pickup_ind,  ''Y'', :I_pickup_lead_time,    pickup_lead_time),
                last_update_datetime = :s,
                last_update_id       = :u
          where item = :I_item
            and supplier = :I_supplier
            and origin_country_id = :I_origin_country_id
            and (loc in (select store from store)
                or loc in (select wh from wh))' || L_where_clause;
      
      L_sql_stmt_cur := DBMS_SQL.OPEN_CURSOR;
            
      DBMS_SQL.PARSE( L_sql_stmt_cur,sql_stmt,DBMS_SQL.NATIVE);
            
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_unit_cost', I_unit_cost);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_rnd_lvl_ind', I_change_rnd_lvl_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_lvl', I_round_lvl);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_inner_ind', I_change_inner_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_to_inner_pct', I_round_to_inner_pct);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_case_ind', I_change_case_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_to_case_pct', I_round_to_case_pct);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_layer_ind', I_change_layer_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_to_layer_pct', I_round_to_layer_pct);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_pallet_ind', I_change_pallet_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_to_pallet_pct', I_round_to_pallet_pct);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_lvl_1', I_change_lvl_1);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_lvl_2', I_change_lvl_2);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_lvl_3', I_change_lvl_3);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'L_supp_hier_type_1', L_supp_hier_type_1);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supp_hier_lvl_1', I_supp_hier_lvl_1);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'L_supp_hier_type_2', L_supp_hier_type_2);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supp_hier_lvl_2', I_supp_hier_lvl_2);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'L_supp_hier_type_3', L_supp_hier_type_3);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supp_hier_lvl_3', I_supp_hier_lvl_3);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_pickup_ind', I_change_pickup_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_pickup_lead_time', I_pickup_lead_time);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 's', sysdate);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'u', get_user);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_item', I_item);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supplier', I_supplier);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_origin_country_id', I_origin_country_id);
                  
      L_fetch_rows := DBMS_SQL.EXECUTE(L_sql_stmt_cur);
                  
      DBMS_SQL.CLOSE_CURSOR (L_sql_stmt_cur);                

 
   else
      ---
      sql_stmt := 'update item_supp_country_loc
            set unit_cost            = NVL(:I_unit_cost, unit_cost),
                round_lvl            = DECODE(:I_change_rnd_lvl_ind, ''Y'', :I_round_lvl,           round_lvl),
                round_to_inner_pct   = DECODE(:I_change_inner_ind,   ''Y'', :I_round_to_inner_pct,  round_to_inner_pct),
                round_to_case_pct    = DECODE(:I_change_case_ind,    ''Y'', :I_round_to_case_pct,   round_to_case_pct),
                round_to_layer_pct   = DECODE(:I_change_layer_ind,   ''Y'', :I_round_to_layer_pct,  round_to_layer_pct),
                round_to_pallet_pct  = DECODE(:I_change_pallet_ind,  ''Y'', :I_round_to_pallet_pct, round_to_pallet_pct),
                supp_hier_type_1     = DECODE(:I_change_lvl_1,       ''Y'', :L_supp_hier_type_1,    supp_hier_type_1),
                supp_hier_lvl_1      = DECODE(:I_change_lvl_1,       ''Y'', :I_supp_hier_lvl_1,     supp_hier_lvl_1),
                supp_hier_type_2     = DECODE(:I_change_lvl_2,       ''Y'', :L_supp_hier_type_2,    supp_hier_type_2),
                supp_hier_lvl_2      = DECODE(:I_change_lvl_2,       ''Y'', :I_supp_hier_lvl_2,     supp_hier_lvl_2),
                supp_hier_type_3     = DECODE(:I_change_lvl_3,       ''Y'', :L_supp_hier_type_3,    supp_hier_type_3),
                supp_hier_lvl_3      = DECODE(:I_change_lvl_3,       ''Y'', :I_supp_hier_lvl_3,     supp_hier_lvl_3),
                pickup_lead_time     = DECODE(:I_change_pickup_ind,  ''Y'', :I_pickup_lead_time,    pickup_lead_time),
                last_update_datetime = :s,
                last_update_id       = :u,
                negotiated_item_cost =  NULL,
                extended_base_cost   =  NULL,
                inclusive_cost       =  NULL,                              
                base_cost            =  NULL
          where item = :I_item
            and supplier = :I_supplier
            and origin_country_id = :I_origin_country_id
            and (loc in (select store from store)
                or loc in (select wh from wh))' || L_where_clause;
     
      L_sql_stmt_cur := DBMS_SQL.OPEN_CURSOR;
            
      DBMS_SQL.PARSE( L_sql_stmt_cur,sql_stmt,DBMS_SQL.NATIVE);
            
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_unit_cost', I_unit_cost);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_rnd_lvl_ind', I_change_rnd_lvl_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_lvl', I_round_lvl);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_inner_ind', I_change_inner_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_to_inner_pct', I_round_to_inner_pct);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_case_ind', I_change_case_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_to_case_pct', I_round_to_case_pct);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_layer_ind', I_change_layer_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_to_layer_pct', I_round_to_layer_pct);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_pallet_ind', I_change_pallet_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_round_to_pallet_pct', I_round_to_pallet_pct);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_lvl_1', I_change_lvl_1);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_lvl_2', I_change_lvl_2);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_lvl_3', I_change_lvl_3);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'L_supp_hier_type_1', L_supp_hier_type_1);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supp_hier_lvl_1', I_supp_hier_lvl_1);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'L_supp_hier_type_2', L_supp_hier_type_2);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supp_hier_lvl_2', I_supp_hier_lvl_2);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'L_supp_hier_type_3', L_supp_hier_type_3);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supp_hier_lvl_3', I_supp_hier_lvl_3);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_change_pickup_ind', I_change_pickup_ind);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_pickup_lead_time', I_pickup_lead_time);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 's', sysdate);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'u', get_user);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_item', I_item);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supplier', I_supplier);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_origin_country_id', I_origin_country_id);
                  
      L_fetch_rows := DBMS_SQL.EXECUTE(L_sql_stmt_cur);
                  
      DBMS_SQL.CLOSE_CURSOR (L_sql_stmt_cur);
      
   end if;


   ---
   -- loop THROUGH EACH UPDATED LOCATION AND UPDATE ITEM_LOC AND/OR ITEM_SUPP_COUNTRY
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   open C_LOC for lock_stmt USING I_item, I_supplier, I_origin_country_id;
   loop
      SQL_LIB.SET_MARK('FETCH','C_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id ||
                       ' Location: '||to_char(L_loc));
      fetch C_LOC into L_loc,
                       L_primary_loc_ind,
                       L_unit_cost;
      ---
      EXIT WHEN C_LOC%NOTFOUND;
      ---
      O_virtual_wh_exists := TRUE;
      if I_unit_cost is not NULL and L_primary_loc_ind = 'Y' then
         if UPDATE_BASE_COST.CHANGE_COST(O_error_message,
                                         I_item,
                                         I_supplier,
                                         I_origin_country_id,
                                         L_loc,
                                         I_process_children,
                                         'Y',
                                         NULL /* Cost Change Number */ ) = FALSE then   -- update unit cost indicator
            return FALSE;
         end if;
      end if;
      ---
      if I_process_children = 'Y' then
         if UPDATE_CHILD_LOCATION(O_error_message,
                                  I_change_rnd_lvl_ind,
                                  I_round_lvl,
                                  I_change_inner_ind,
                                  I_round_to_inner_pct,
                                  I_change_case_ind,
                                  I_round_to_case_pct,
                                  I_change_layer_ind,
                                  I_round_to_layer_pct,
                                  I_change_pallet_ind,
                                  I_round_to_pallet_pct,
                                  I_change_lvl_1,
                                  L_supp_hier_type_1,
                                  I_supp_hier_lvl_1,
                                  I_change_lvl_2,
                                  L_supp_hier_type_2,
                                  I_supp_hier_lvl_2,
                                  I_change_lvl_3,
                                  L_supp_hier_type_3,
                                  I_supp_hier_lvl_3,
                                  I_change_pickup_ind,
                                  I_pickup_lead_time,
                                  I_item,
                                  I_supplier,
                                  I_origin_country_id,
                                  L_loc) = FALSE then
             return FALSE;
          end if;
       end if;
   end loop;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   close C_LOC;
   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'Item: '||I_item,
                                            'Supplier: '||to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MASS_UPDATE;
----------------------------------------------------------------------------------------------
FUNCTION UPDATE_CHILD_LOCATION(O_error_message       IN OUT VARCHAR2,
                               I_change_rnd_lvl_ind  IN     VARCHAR2,
                               I_round_lvl           IN     ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
                               I_change_inner_ind    IN     VARCHAR2,
                               I_round_to_inner_pct  IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
                               I_change_case_ind     IN     VARCHAR2,
                               I_round_to_case_pct   IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
                               I_change_layer_ind    IN     VARCHAR2,
                               I_round_to_layer_pct  IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
                               I_change_pallet_ind   IN     VARCHAR2,
                               I_round_to_pallet_pct IN     ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
                               I_change_lvl_1        IN     VARCHAR2,
                               I_supp_hier_type_1    IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_1%TYPE,
                               I_supp_hier_lvl_1     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE,
                               I_change_lvl_2        IN     VARCHAR2,
                               I_supp_hier_type_2    IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_2%TYPE,
                               I_supp_hier_lvl_2     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE,
                               I_change_lvl_3        IN     VARCHAR2,
                               I_supp_hier_type_3    IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_3%TYPE,
                               I_supp_hier_lvl_3     IN     ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE,
                               I_change_pickup_ind   IN     VARCHAR2,
                               I_pickup_lead_time    IN     ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                               I_item                IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                               I_supplier            IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                               I_origin_country_id   IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                               I_location            IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.UPDATE_CHILD_LOCATION';
   L_table                 VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY_LOC';
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_LOCS is
      select 'x'
        from item_supp_country_loc iscl,
             item_master im
       where (im.item_parent         = I_item
              or im.item_grandparent = I_item)
         and im.item_level          <= im.tran_level
         and iscl.item               = im.item
         and iscl.supplier           = I_supplier
         and iscl.origin_country_id  = I_origin_country_id
         and iscl.loc                = I_location
         for update nowait;
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_LOCS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   open C_LOCK_LOCS;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_LOCS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
   close C_LOCK_LOCS;
   ---
   SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);

   update item_supp_country_loc
      set round_lvl            = DECODE(I_change_rnd_lvl_ind, 'Y', I_round_lvl,           round_lvl),
          round_to_inner_pct   = DECODE(I_change_inner_ind,   'Y', I_round_to_inner_pct,  round_to_inner_pct),
          round_to_case_pct    = DECODE(I_change_case_ind,    'Y', I_round_to_case_pct,   round_to_case_pct),
          round_to_layer_pct   = DECODE(I_change_layer_ind,   'Y', I_round_to_layer_pct,  round_to_layer_pct),
          round_to_pallet_pct  = DECODE(I_change_pallet_ind,  'Y', I_round_to_pallet_pct, round_to_pallet_pct),
          supp_hier_lvl_1      = DECODE(I_change_lvl_1,       'Y', I_supp_hier_lvl_1,     supp_hier_lvl_1),
          supp_hier_type_1     = DECODE(I_change_lvl_1,       'Y', I_supp_hier_type_1,    supp_hier_type_1),
          supp_hier_lvl_2      = DECODE(I_change_lvl_2,       'Y', I_supp_hier_lvl_2,     supp_hier_lvl_2),
          supp_hier_type_2     = DECODE(I_change_lvl_2,       'Y', I_supp_hier_type_2,    supp_hier_type_2),
          supp_hier_lvl_3      = DECODE(I_change_lvl_3,       'Y', I_supp_hier_lvl_3,     supp_hier_lvl_3),
          supp_hier_type_3     = DECODE(I_change_lvl_3,       'Y', I_supp_hier_type_3,    supp_hier_type_3),
          pickup_lead_time     = DECODE(I_change_pickup_ind,  'Y', I_pickup_lead_time,    pickup_lead_time),
          last_update_datetime = sysdate,
          last_update_id       = get_user
    where supplier          = I_supplier
      and origin_country_id = I_origin_country_id
      and loc               = I_location
      and item in (select im.item
                     from item_supp_country_loc iscl,
                          item_master im
                    where (im.item_parent         = I_item
                           or im.item_grandparent = I_item)
                      and im.item_level          <= im.tran_level
                      and iscl.item               = im.item
                      and iscl.supplier           = I_supplier
                      and iscl.origin_country_id  = I_origin_country_id
                      and iscl.loc                = I_location);
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                              'Item: '||I_item,
                                             'Supplier: '||to_char(I_supplier));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_CHILD_LOCATION;
----------------------------------------------------------------------------------------------
FUNCTION GET_ALL_COST(O_error_message        IN OUT VARCHAR2,
                      O_base_cost            IN OUT ITEM_SUPP_COUNTRY_LOC.BASE_COST%TYPE,
                      O_negotiated_item_cost IN OUT ITEM_SUPP_COUNTRY_LOC.NEGOTIATED_ITEM_COST%TYPE,
                      O_extended_base_cost   IN OUT ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE,
                      O_inclusive_cost       IN OUT ITEM_SUPP_COUNTRY_LOC.INCLUSIVE_COST%TYPE,
                      I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                      I_supplier             IN     SUPS.SUPPLIER%TYPE,
                      I_origin_country_id    IN     COUNTRY.COUNTRY_ID%TYPE,
                      I_loc                  IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.GET_ALL_COST';
      cursor C_GET_COSTS is
      select base_cost,
             negotiated_item_cost,
             extended_base_cost,
             inclusive_cost
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country_id
         and loc               = I_loc;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_COSTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id ||
                    ' Location: '||to_char(I_loc));

   open C_GET_COSTS;
   SQL_LIB.SET_MARK('FETCH','C_GET_COSTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id||
                    ' Location: '||to_char(I_loc));
   fetch C_GET_COSTS into O_base_cost,O_negotiated_item_cost,O_extended_base_cost,O_inclusive_cost;
   SQL_LIB.SET_MARK('CLOSE','C_GET_COSTS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id ||
                    ' Location: '||to_char(I_loc));
   close C_GET_COSTS;
   ----
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ALL_COST;
----------------------------------------------------------------------------------------------
FUNCTION GET_COST(O_error_message     IN OUT VARCHAR2,
                  O_unit_cost         IN OUT ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                  I_item              IN     ITEM_MASTER.ITEM%TYPE,
                  I_supplier          IN     SUPS.SUPPLIER%TYPE,
                  I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                  I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.GET_COST';

   cursor C_GET_COST is
      select unit_cost
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_origin_country_id
         and loc               = I_loc;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id ||
                    ' Location: '||to_char(I_loc));

   open C_GET_COST;
   SQL_LIB.SET_MARK('FETCH','C_GET_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id||
                    ' Location: '||to_char(I_loc));
   fetch C_GET_COST into O_unit_cost;
   SQL_LIB.SET_MARK('CLOSE','C_GET_COST','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id ||
                    ' Location: '||to_char(I_loc));
   close C_GET_COST;
   ----
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_COST;
----------------------------------------------------------------------------------------------
FUNCTION CHECK_LOCATION_FOR_CHILDREN(O_error_message     IN OUT VARCHAR2,
                                     O_exists            IN OUT BOOLEAN,
                                     I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                                     I_supplier          IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                                     I_origin_country_id IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.CHECK_LOCATION_FOR_CHILDREN';
   L_record       VARCHAR2(1)    := NULL;
   L_child_item   ITEM_MASTER.ITEM%TYPE;

  cursor C_GET_CHILD_ITEMS is
     select im.item
       from item_master im
      where (im.item_parent          = I_item
              or im.item_grandparent = I_item)
        and im.item_level           <= im.tran_level
        and exists (select 'x'
                      from item_supp_country isc
                     where isc.item              = im.item
                       and isc.supplier          = I_supplier
                       and isc.origin_country_id = I_origin_country_id);

   cursor C_CHECK_COUNTRY_LOC is
      select 'x'
       from item_supp_country_loc isl
      where isl.item              = I_item
        and isl.supplier          = I_supplier
        and isl.origin_country_id = I_origin_country_id
        and exists (select 'x'
                      from item_supp_country_loc isl2
                     where isl2.item                 = L_child_item
                       and isl2.supplier             = isl.supplier
                       and isl2.origin_country_id    = isl.origin_country_id
                       and (isl2.unit_cost           != isl.unit_cost
                            or NVL(isl2.supp_hier_lvl_1,  'x')  != NVL(isl.supp_hier_lvl_1,  'x')
                            or NVL(isl2.supp_hier_lvl_2,  'x')  != NVL(isl.supp_hier_lvl_2,  'x')
                            or NVL(isl2.supp_hier_lvl_3,  'x')  != NVL(isl.supp_hier_lvl_3,  'x')
                            or NVL(isl2.pickup_lead_time, -999) != NVL(isl.pickup_lead_time, -999)));
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_CHILD_ITEMS','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);   
   for C_rec in C_GET_CHILD_ITEMS loop
      L_child_item := C_rec.item;

      SQL_LIB.SET_MARK('OPEN','C_CHECK_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
      open C_CHECK_COUNTRY_LOC;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
      fetch C_CHECK_COUNTRY_LOC into L_record;
      ---
      if C_CHECK_COUNTRY_LOC%FOUND then
         O_exists := TRUE;
      else
         O_exists := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||L_child_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
      close C_CHECK_COUNTRY_LOC;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_LOCATION_FOR_CHILDREN;
-----------------------------------------------------------------------------------------------
FUNCTION GET_LEAD_TIMES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_total_lead_time     IN OUT   NUMBER,
                        O_lead_time           IN OUT   ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                        O_pickup_lead_time    IN OUT   ITEM_SUPP_COUNTRY.LEAD_TIME%TYPE,
                        I_item                IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                        I_supplier            IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                        I_origin_country_id   IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                        I_location            IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   return BOOLEAN is

   L_program   VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.GET_LEAD_TIMES';

   cursor C_GET_LEAD_TIME is
      select NVL(lead_time, 0)
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id;

   cursor C_GET_PICKUP_LEAD_TIME is
      select NVL(pickup_lead_time, 0)
        from item_supp_country_loc
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id
         and loc = I_location;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
   open C_GET_LEAD_TIME;

   SQL_LIB.SET_MARK('FETCH','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
   fetch C_GET_LEAD_TIME into O_lead_time;

   SQL_LIB.SET_MARK('CLOSE','C_GET_LEAD_TIME','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id);
   close C_GET_LEAD_TIME;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id||
                      ', Location: '||to_char(I_location));
   open C_GET_PICKUP_LEAD_TIME;

   SQL_LIB.SET_MARK('FETCH','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id||
                      ', Location: '||to_char(I_location));
   fetch C_GET_PICKUP_LEAD_TIME into O_pickup_lead_time;

   SQL_LIB.SET_MARK('CLOSE','C_GET_PICKUP_LEAD_TIME','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ', Supplier: '||to_char(I_supplier)||', Country: '||I_origin_country_id||
                      ', Location: '||to_char(I_location));
   close C_GET_PICKUP_LEAD_TIME;
   ---
   O_total_lead_time := O_lead_time + O_pickup_lead_time;

   return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_SUPP_COUNTRY_LOC_SQL.GET_LEAD_TIMES',
                                            to_char(SQLCODE));
      return FALSE;
END GET_LEAD_TIMES;
-----------------------------------------------------------------------------------------------
FUNCTION SINGLE_UPDATE(O_error_message       IN OUT  VARCHAR2,
                       I_item                IN      ITEM_MASTER.ITEM%TYPE,
                       I_supplier            IN      SUPS.SUPPLIER%TYPE,
                       I_country             IN      COUNTRY.COUNTRY_ID%TYPE,
                       I_location            IN      ITEM_LOC.LOC%TYPE,
                       I_loc_type            IN      ITEM_LOC.LOC_TYPE%TYPE,
                       I_item_status         IN      ITEM_MASTER.STATUS%TYPE,
                       I_edit_unit_cost      IN      VARCHAR2,
                       I_unit_cost           IN      ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                       I_edit_lvl_1          IN      VARCHAR2,
                       I_supp_hier_lvl_1     IN      ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE,
                       I_edit_lvl_2          IN      VARCHAR2,
                       I_supp_hier_lvl_2     IN      ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE,
                       I_edit_lvl_3          IN      VARCHAR2,
                       I_supp_hier_lvl_3     IN      ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE,
                       I_edit_pickup_ind     IN      VARCHAR2,
                       I_pickup_lead_time    IN      ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                       I_edit_round_lvl      IN      VARCHAR2,
                       I_round_lvl           IN      ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
                       I_edit_inner_pct      IN      VARCHAR2,
                       I_round_to_inner_pct  IN      ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
                       I_edit_case_pct       IN      VARCHAR2,
                       I_round_to_case_pct   IN      ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
                       I_edit_layer_pct      IN      VARCHAR2,
                       I_round_to_layer_pct  IN      ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
                       I_edit_pallet_pct     IN      VARCHAR2,
                       I_round_to_pallet_pct IN      ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
                       I_default_down_ind    IN      VARCHAR2,
                       I_primary_loc_ind     IN      VARCHAR2)
   return BOOLEAN IS

   L_program   VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.SINGLE_UPDATE';
   ---
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   cursor C_ITEM_SUPP_COUNTRY_LOC is
      select unit_cost,
             supp_hier_type_1,
             supp_hier_lvl_1,
             supp_hier_type_2,
             supp_hier_lvl_2,
             supp_hier_type_3,
             supp_hier_lvl_3,
             pickup_lead_time,
             round_lvl,
             round_to_inner_pct,
             round_to_case_pct,
             round_to_layer_pct,
             round_to_pallet_pct,
             primary_loc_ind
        from item_supp_country_loc
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_country
         and loc = I_location
         for update nowait;

   iscl_rec                C_ITEM_SUPP_COUNTRY_LOC%ROWTYPE;
   L_last_update_datetime  DATE := SYSDATE;
   L_last_update_id        VARCHAR2(30) := GET_USER;
   ---
   L_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
   L_tax_calc_rec           OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_tax_calc_tbl           OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_inclusive_cost         ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE := NULL;
   L_tax_rate               ITEM_COST_DETAIL.COMP_RATE%TYPE := 0;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                                                     ' Supplier: '||to_char(I_supplier)||
                                                     ' Origin Country: '||I_country||
                                                     ' Location: '||to_char(I_location));
   open C_ITEM_SUPP_COUNTRY_LOC;
   SQL_LIB.SET_MARK('FETCH','C_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                                                      ' Supplier: '||to_char(I_supplier)||
                                                      ' Origin Country: '||I_country||
                                                      ' Location: '||to_char(I_location));
   fetch C_ITEM_SUPP_COUNTRY_LOC into iscl_rec;
   SQL_LIB.SET_MARK('CLOSE','C_ITEM_SUPP_COUNTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                                                      ' Supplier: '||to_char(I_supplier)||
                                                      ' Origin Country: '||I_country||
                                                      ' Location: '||to_char(I_location));
   close C_ITEM_SUPP_COUNTRY_LOC;
   ---
   if I_edit_unit_cost = 'Y' then
      iscl_rec.unit_cost := I_unit_cost;
   end if;
   ---
   if I_edit_lvl_1 = 'Y' then
      iscl_rec.supp_hier_lvl_1 := I_supp_hier_lvl_1;
      --
      if I_supp_hier_lvl_1 is NOT NULL then
         iscl_rec.supp_hier_type_1 := 'S1';
      else
         iscl_rec.supp_hier_type_1 := NULL;
      end if;
   end if;
   ---
   if I_edit_lvl_2 = 'Y' then
      iscl_rec.supp_hier_lvl_2 := I_supp_hier_lvl_2;
      ---
      if I_supp_hier_lvl_2 is NOT NULL then
         iscl_rec.supp_hier_type_2 := 'S2';
      else
         iscl_rec.supp_hier_type_2 := NULL;
      end if;
   end if;
   ---
   if I_edit_lvl_3 = 'Y' then
      iscl_rec.supp_hier_lvl_3 := I_supp_hier_lvl_3;
      ---
      if I_supp_hier_lvl_3 is NOT NULL then
         iscl_rec.supp_hier_type_3 := 'S3';
      else
         iscl_rec.supp_hier_type_3 := NULL;
      end if;
   end if;
   ---
   if I_edit_pickup_ind = 'Y' then
      iscl_rec.pickup_lead_time := I_pickup_lead_time;
   end if;
   ---
   if I_edit_round_lvl = 'Y' then
      iscl_rec.round_lvl := I_round_lvl;
   end if;
   ---
   if I_edit_inner_pct = 'Y' then
      iscl_rec.round_to_inner_pct := I_round_to_inner_pct;
   end if;
   ---
   if I_edit_case_pct = 'Y' then
      iscl_rec.round_to_case_pct := I_round_to_case_pct;
   end if;
   ---
   if I_edit_layer_pct = 'Y' then
      iscl_rec.round_to_layer_pct := I_round_to_layer_pct;
   end if;
   ---
   if I_edit_pallet_pct = 'Y' then
      iscl_rec.round_to_pallet_pct := I_round_to_pallet_pct;
   end if;
   ---
   if I_primary_loc_ind = 'Y' then
      iscl_rec.primary_loc_ind := I_primary_loc_ind;
   end if;
   ---
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then
      return FALSE;
   end if;
   ---
   if L_system_options_rec.default_tax_type = 'GTAX' then
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                                                             ' Supplier: '||to_char(I_supplier)||
                                                             ' Origin Country: '||I_country||
                                                             ' Location: '||to_char(I_location));
      UPDATE item_supp_country_loc
         SET unit_cost = iscl_rec.unit_cost,
             round_lvl = iscl_rec.round_lvl,
             round_to_inner_pct = iscl_rec.round_to_inner_pct,
             round_to_case_pct = iscl_rec.round_to_case_pct,
             round_to_layer_pct = iscl_rec.round_to_layer_pct,
             round_to_pallet_pct = iscl_rec.round_to_pallet_pct,
             supp_hier_type_1 = iscl_rec.supp_hier_type_1,
             supp_hier_lvl_1 = iscl_rec.supp_hier_lvl_1,
             supp_hier_type_2 = iscl_rec.supp_hier_type_2,
             supp_hier_lvl_2 = iscl_rec.supp_hier_lvl_2,
             supp_hier_type_3 = iscl_rec.supp_hier_type_3,
             supp_hier_lvl_3 = iscl_rec.supp_hier_lvl_3,
             pickup_lead_time = iscl_rec.pickup_lead_time,
             primary_loc_ind = iscl_rec.primary_loc_ind,
             last_update_datetime = L_last_update_datetime,
             last_update_id = L_last_update_id
       WHERE item = I_item
         AND supplier = I_supplier
         AND origin_country_id = I_country
         AND loc = I_location;
   else
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                                                             ' Supplier: '||to_char(I_supplier)||
                                                             ' Origin Country: '||I_country||
                                                             ' Location: '||to_char(I_location));

      UPDATE item_supp_country_loc
         SET unit_cost = iscl_rec.unit_cost,
             round_lvl = iscl_rec.round_lvl,
             round_to_inner_pct = iscl_rec.round_to_inner_pct,
             round_to_case_pct = iscl_rec.round_to_case_pct,
             round_to_layer_pct = iscl_rec.round_to_layer_pct,
             round_to_pallet_pct = iscl_rec.round_to_pallet_pct,
             supp_hier_type_1 = iscl_rec.supp_hier_type_1,
             supp_hier_lvl_1 = iscl_rec.supp_hier_lvl_1,
             supp_hier_type_2 = iscl_rec.supp_hier_type_2,
             supp_hier_lvl_2 = iscl_rec.supp_hier_lvl_2,
             supp_hier_type_3 = iscl_rec.supp_hier_type_3,
             supp_hier_lvl_3 = iscl_rec.supp_hier_lvl_3,
             pickup_lead_time = iscl_rec.pickup_lead_time,
             primary_loc_ind = iscl_rec.primary_loc_ind,
             last_update_datetime = L_last_update_datetime,
             last_update_id = L_last_update_id,
             negotiated_item_cost = NULL,
             extended_base_cost = NULL,
             inclusive_cost = NULL,
             base_cost = NULL
       WHERE item = I_item
         AND supplier = I_supplier
         AND origin_country_id = I_country
         AND loc = I_location;
   end if;
   ---
   if I_default_down_ind = 'Y' then
      if ITEM_SUPP_COUNTRY_LOC_SQL.UPDATE_CHILD_LOCATION(O_error_message,
                                                         I_edit_round_lvl,
                                                         I_round_lvl,
                                                         I_edit_inner_pct,
                                                         I_round_to_inner_pct,
                                                         I_edit_case_pct,
                                                         I_round_to_case_pct,
                                                         I_edit_layer_pct,
                                                         I_round_to_layer_pct,
                                                         I_edit_pallet_pct,
                                                         I_round_to_pallet_pct,
                                                         I_edit_lvl_1,
                                                         iscl_rec.supp_hier_type_1,
                                                         I_supp_hier_lvl_1,
                                                         I_edit_lvl_2,
                                                         iscl_rec.supp_hier_type_2,
                                                         I_supp_hier_lvl_2,
                                                         I_edit_lvl_3,
                                                         iscl_rec.supp_hier_type_3,
                                                         I_supp_hier_lvl_3,
                                                         I_edit_pickup_ind,
                                                         I_pickup_lead_time,
                                                         I_item,
                                                         I_supplier,
                                                         I_country,
                                                         I_location) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_primary_loc_ind = 'Y' then
      if SUPP_ITEM_SQL.UPDATE_PRIMARY_INDICATORS(O_error_message,
                                                 I_item,
                                                 I_supplier,
                                                 I_country,
                                                 I_location,
                                                 I_default_down_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   --- Change loc needs cost changes first.
   if I_item_status != 'A' and (I_edit_unit_cost = 'Y' or I_primary_loc_ind = 'Y') then
      if UPDATE_BASE_COST.CHANGE_COST(O_error_message,
                                      I_item,
                                      I_supplier,
                                      I_country,
                                      I_location,
                                      I_default_down_ind,
                                      I_edit_unit_cost,
                                      NULL) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   --- Change loc writes price_hist records after primary locations change.
   if I_primary_loc_ind = 'Y' then
      if UPDATE_BASE_COST.CHANGE_LOC(O_error_message,
                                     I_item,
                                     I_supplier,
                                     I_country,
                                     I_location,
                                     I_default_down_ind) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             'ITEM_SUPP_COUNTRY_LOC',
                                             'Item: '||I_item,
                                             'Supplier: '||to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ITEM_SUPP_COUNTRY_LOC_SQL.SINGLE_UPDATE',
                                            to_char(SQLCODE));
      return FALSE;
END SINGLE_UPDATE;
-----------------------------------------------------------------------------------------------
FUNCTION GET_ISCL(O_error_message       IN OUT VARCHAR2,
                  O_currency_code       IN OUT STORE.CURRENCY_CODE%TYPE,
                  O_case_cost           IN OUT NUMBER,
                  O_unit_cost           IN OUT ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                  O_round_lvl           IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_LVL%TYPE,
                  O_round_to_inner_pct  IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_TO_INNER_PCT%TYPE,
                  O_round_to_case_pct   IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_TO_CASE_PCT%TYPE,
                  O_round_to_layer_pct  IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_TO_LAYER_PCT%TYPE,
                  O_round_to_pallet_pct IN OUT ITEM_SUPP_COUNTRY_LOC.ROUND_TO_PALLET_PCT%TYPE,
                  O_supp_hier_type_1    IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_1%TYPE,
                  O_supp_hier_type_2    IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_2%TYPE,
                  O_supp_hier_type_3    IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_TYPE_3%TYPE,
                  O_supp_hier_lvl_1     IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_1%TYPE,
                  O_supp_hier_lvl_2     IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_2%TYPE,
                  O_supp_hier_lvl_3     IN OUT ITEM_SUPP_COUNTRY_LOC.SUPP_HIER_LVL_3%TYPE,
                  O_pickup_lead_time    IN OUT ITEM_SUPP_COUNTRY_LOC.PICKUP_LEAD_TIME%TYPE,
                  I_item              IN     ITEM_MASTER.ITEM%TYPE,
                  I_supplier          IN     SUPS.SUPPLIER%TYPE,
                  I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                  I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.GET_ISCL';

   cursor C_GET_ISCL is
      select decode(w.currency_code, NULL, s.currency_code, w.currency_code) curr_code,
             isc.supp_pack_size * iscl.unit_cost case_cost,
             iscl.unit_cost,
             iscl.round_lvl,
             iscl.round_to_inner_pct,
             iscl.round_to_case_pct,
             iscl.round_to_layer_pct,
             iscl.round_to_pallet_pct,
             iscl.supp_hier_type_1,
             iscl.supp_hier_type_2,
             iscl.supp_hier_type_3,
             iscl.supp_hier_lvl_1,
             iscl.supp_hier_lvl_2,
             iscl.supp_hier_lvl_3,
             iscl.pickup_lead_time
        from item_supp_country_loc iscl,
             store s,
             wh w,
             item_supp_country isc
       where iscl.item         = I_item
         and iscl.supplier          = I_supplier
         and iscl.origin_country_id = I_origin_country_id
         and iscl.loc          = I_loc
         and iscl.loc          = w.wh(+)
         and iscl.loc          = s.store(+)
         and isc.item          = iscl.item
         and isc.supplier      = iscl.supplier;


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_loc',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_ISCL','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id ||
                    ' Location: '||to_char(I_loc));

   open C_GET_ISCL;
   SQL_LIB.SET_MARK('FETCH','C_GET_ISCL','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id||
                    ' Location: '||to_char(I_loc));
   fetch C_GET_ISCL into O_currency_code,
                         O_case_cost,
                         O_unit_cost,
                         O_round_lvl,
                         O_round_to_inner_pct,
                         O_round_to_case_pct,
                         O_round_to_layer_pct,
                         O_round_to_pallet_pct,
                         O_supp_hier_type_1,
                         O_supp_hier_type_2,
                         O_supp_hier_type_3,
                         O_supp_hier_lvl_1,
                         O_supp_hier_lvl_2,
                         O_supp_hier_lvl_3,
                         O_pickup_lead_time;
   SQL_LIB.SET_MARK('CLOSE','C_GET_ISCL','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                    ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id ||
                    ' Location: '||to_char(I_loc));
   close C_GET_ISCL;
   ----
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ISCL;
------------------------------------------------------------------------------------
FUNCTION BULK_INSERT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_loc_cost        IN       RMSSUB_XITEM.LOC_COST_TBLTYPE,
                     I_iscl_def        IN       ITEM_SUPP_COUNTRY_LOC%ROWTYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC.BULK_INSERT';
   L_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
   
BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;

   FORALL i in I_loc_cost.FIRST..I_loc_cost.LAST
      insert into item_supp_country_loc(item,
                                        supplier,
                                        origin_country_id,
                                        loc,
                                        loc_type,
                                        primary_loc_ind,
                                        unit_cost,
                                        round_lvl,
                                        round_to_inner_pct,
                                        round_to_case_pct,
                                        round_to_layer_pct,
                                        round_to_pallet_pct,
                                        supp_hier_type_1,
                                        supp_hier_lvl_1,
                                        supp_hier_type_2,
                                        supp_hier_lvl_2,
                                        supp_hier_type_3,
                                        supp_hier_lvl_3,
                                        pickup_lead_time,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        negotiated_item_cost,
                                        extended_base_cost,
                                        inclusive_cost,
                                        base_cost,
                                        create_id)
                                values( I_iscl_def.item,
                                        I_iscl_def.supplier,
                                        I_iscl_def.origin_country_id,
                                        I_loc_cost(i).location,
                                        I_iscl_def.loc_type,
                                        I_iscl_def.primary_loc_ind,
                                        I_loc_cost(i).unit_cost,
                                        I_iscl_def.round_lvl,
                                        I_iscl_def.round_to_inner_pct,
                                        I_iscl_def.round_to_case_pct,
                                        I_iscl_def.round_to_layer_pct,
                                        I_iscl_def.round_to_pallet_pct,
                                        I_iscl_def.supp_hier_type_1,
                                        I_iscl_def.supp_hier_lvl_1,
                                        I_iscl_def.supp_hier_type_2,
                                        I_iscl_def.supp_hier_lvl_2,
                                        I_iscl_def.supp_hier_type_3,
                                        I_iscl_def.supp_hier_lvl_3,
                                        I_iscl_def.pickup_lead_time,
                                        I_iscl_def.create_datetime,
                                        sysdate,
                                        I_iscl_def.last_update_id,
                                        DECODE(L_system_options_rec.default_tax_type, 'GTAX', I_loc_cost(i).negotiated_item_cost, NULL),
                                        DECODE(L_system_options_rec.default_tax_type, 'GTAX', I_loc_cost(i).extended_base_cost, NULL),
                                        DECODE(L_system_options_rec.default_tax_type, 'GTAX', I_loc_cost(i).inclusive_cost, NULL),
                                        DECODE(L_system_options_rec.default_tax_type, 'GTAX', I_loc_cost(i).base_cost, NULL),
                                        I_iscl_def.create_id);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BULK_INSERT;
------------------------------------------------------------------------------------
FUNCTION BULK_DELETE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_locs            IN       LOC_TBL,
                     I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                     I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                     I_item            IN       ITEM_MASTER.ITEM%TYPE,
                     I_cascade_ind     IN       CORESVC_ITEM_CONFIG.CASCADE_IUD_ITEM_SUPP_COUNTRY%TYPE DEFAULT 'N')
   RETURN BOOLEAN IS

   L_program                 VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.BULK_DELETE';
   L_user                    VARCHAR2(30) := get_user;
   TAB_rowids                ROWID_TBL; --- Type declared at package level
   L_rowid                   rowid;
   RECORD_LOCKED             EXCEPTION;
   PRAGMA                    EXCEPTION_INIT(Record_Locked, -54);
   L_TABLE                   VARCHAR2(30);
   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_sc_cost_event_tbl       OBJ_SC_COST_EVENT_TBL;

  cursor C_LOCK_ISCL_CE is
        select isclce.rowid
          from item_supp_country_loc_cfa_ext isclce , item_master im
         where loc in (select *
                         from TABLE(cast(I_locs as LOC_TBL)))
           and supplier = I_supplier
           and origin_country_id = I_country
           and isclce.item = im.item
           and (( I_cascade_ind ='N' and im.item=I_ITEM) or
                (I_cascade_ind ='Y' and (im.item=I_ITEM or im.item_parent=I_ITEM or im.item_grandparent=I_ITEM)))
           for update nowait;
           
   cursor C_LOCK_CSS_ISL is
        select cssisl.rowid
          from COST_SUSP_SUP_DETAIL_LOC cssisl , item_master im
         where loc in (select *
                         from TABLE(cast(I_locs as LOC_TBL)))
           and supplier = I_supplier
           and origin_country_id = I_country
           and cssisl.item = im.item
           and (( I_cascade_ind ='N' and im.item=I_ITEM) or
                (I_cascade_ind ='Y' and (im.item=I_ITEM or im.item_parent=I_ITEM or im.item_grandparent=I_ITEM)))
           for update nowait;


  cursor C_LOCK_ISCL is
        select iscl.rowid
          from item_supp_country_loc iscl , item_master im
         where loc in (select *
                         from TABLE(cast(I_locs as LOC_TBL)))
           and supplier = I_supplier
           and origin_country_id = I_country
           and iscl.item =  im.item
           and (( I_cascade_ind ='N' and im.item=I_ITEM) or
                (I_cascade_ind ='Y' and (im.item=I_ITEM or im.item_parent=I_ITEM or im.item_grandparent=I_ITEM)))
           for update nowait;

    
     cursor C_LOCK_RILSD is
        select rilsd.rowid
          from repl_item_loc_supp_dist rilsd, item_master im
         where location in (select *
                              from TABLE(cast(I_locs as LOC_TBL)))
           and supplier = I_supplier
           and origin_country_id = I_country
           and rilsd.item =  im.item
           and (( I_cascade_ind ='N' and im.item=I_ITEM) or
                (I_cascade_ind ='Y' and (im.item=I_ITEM or im.item_parent=I_ITEM or im.item_grandparent=I_ITEM)))
           for update nowait;

     
     cursor C_LOCK_BWM is
        select bwm.rowid
          from buyer_wksht_manual bwm, item_master im
         where location in (select *
                              from TABLE(cast(I_locs as LOC_TBL)))
           and supplier = I_supplier
           and origin_country_id = I_country
           and bwm.item =  im.item
           and (( I_cascade_ind ='N' and im.item=I_ITEM) or
                (I_cascade_ind ='Y' and (im.item=I_ITEM or im.item_parent=I_ITEM or im.item_grandparent=I_ITEM)))
           for update nowait;

 

     cursor C_GET_ITEM_CTRY_LOC is
           select obj_sc_cost_event_rec (iscl.item,
                                         loc ,
                                         supplier,
                                         origin_country_id)
                                    from item_supp_country_loc iscl,item_master im
                                   where loc in (select *
                                    from TABLE(cast(I_locs as LOC_TBL)))
                                     and supplier = I_supplier
                                     and origin_country_id = I_country
                                     and iscl.item =  im.item
                                     and (( I_cascade_ind ='N' and im.item=I_ITEM) or
                                          (I_cascade_ind ='Y' and (im.item=I_ITEM or im.item_parent=I_ITEM or im.item_grandparent=I_ITEM)));
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_ITEM_CTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item :'||I_item||
                                                                         ' Supplier :'||to_char(I_supplier)||
                                                                         ' Origin_Country :'||I_country); 
   open C_GET_ITEM_CTRY_LOC;

   SQL_LIB.SET_MARK('FETCH','C_GET_ITEM_CTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item :'||I_item||
                                                                         ' Supplier :'||to_char(I_supplier)||
                                                                         ' Origin_Country :'||I_country);      
   fetch C_GET_ITEM_CTRY_LOC bulk collect into L_sc_cost_event_tbl;
  
   SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_CTRY_LOC','ITEM_SUPP_COUNTRY_LOC','Item :'||I_item||
                                                                         ' Supplier :'||to_char(I_supplier)||
                                                                         ' Origin_Country :'||I_country);       
   close C_GET_ITEM_CTRY_LOC;


      L_TABLE:='REPL_ITEM_LOC_SUPP_DIST';

      SQL_LIB.SET_MARK('OPEN','C_LOCK_RILSD',L_TABLE,'Item :'||I_item||
                                                     ' Supplier :'||to_char(I_supplier)||
                                                     ' Origin_Country :'||I_country);
      open  C_LOCK_RILSD;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_RILSD',L_TABLE,'Item :'||I_item||
                                                      ' Supplier :'||to_char(I_supplier)||
                                                      ' Origin_Country :'||I_country);
      fetch  C_LOCK_RILSD BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_RILSD',L_TABLE,'Item :'||I_item||
                                                      ' Supplier :'||to_char(I_supplier)||
                                                      ' Origin_Country :'||I_country);
      close C_LOCK_RILSD;

      SQL_LIB.SET_MARK('DELETE',NULL,L_TABLE,'Item :'||I_item||
                                             ' Supplier :'||to_char(I_supplier)||
                                             ' Origin_Country :'||I_country);

      if TAB_rowids is NOT NULL and TAB_rowids.COUNT > 0 then
         FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            delete from repl_item_loc_supp_dist
             where rowid = TAB_rowids(i);
         ---
         if SQL%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_DELETE_REC', 'Program :'||L_program,'Table :'||L_TABLE, 'Item :'||I_item||
                                                                                                                     ' Supplier :'||to_char(I_supplier)||
                                                                                                                     ' Origin_Country :'||I_country);
            return FALSE;
         end if;
      end if;
       L_TABLE:='COST_SUSP_SUP_DETAIL_LOC';

      SQL_LIB.SET_MARK('OPEN','C_LOCK_CSS_ISL',L_TABLE,'Item :'||I_item||
                                                     ' Supplier :'||to_char(I_supplier)||
                                                     ' Origin_Country :'||I_country);
      open  C_LOCK_CSS_ISL;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_CSS_ISL',L_TABLE,'Item :'||I_item||
                                                      ' Supplier :'||to_char(I_supplier)||
                                                      ' Origin_Country :'||I_country);
      fetch  C_LOCK_CSS_ISL BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_CSS_ISL',L_TABLE,'Item :'||I_item||
                                                      ' Supplier :'||to_char(I_supplier)||
                                                      ' Origin_Country :'||I_country);
      close C_LOCK_CSS_ISL;

      SQL_LIB.SET_MARK('DELETE',NULL,L_TABLE,'Item :'||I_item||
                                             ' Supplier :'||to_char(I_supplier)||
                                             ' Origin_Country :'||I_country);

      if TAB_rowids is NOT NULL and TAB_rowids.COUNT > 0 then
         FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            delete from COST_SUSP_SUP_DETAIL_LOC
             where rowid = TAB_rowids(i);
         ---
         if SQL%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_DELETE_REC', 'Program :'||L_program,'Table :'||L_TABLE, 'Item :'||I_item||
                                                                                                                     ' Supplier :'||to_char(I_supplier)||
                                                                                                                     ' Origin_Country :'||I_country);
            return FALSE;
         end if;
      end if;

      L_TABLE:='BUYER_WKSHT_MANUAL';

      SQL_LIB.SET_MARK('OPEN','C_LOCK_BWM',L_TABLE,'Item :'||I_item||
                                                   ' Supplier :'||to_char(I_supplier)||
                                                   ' Origin_Country :'||I_country);
      open  C_LOCK_BWM;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_BWM',L_TABLE,'Item :'||I_item||
                                                    ' Supplier :'||to_char(I_supplier)||
                                                    ' Origin_Country :'||I_country);
      fetch  C_LOCK_BWM BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_BWM',L_TABLE,'Item :'||I_item||
                                                    ' Supplier :'||to_char(I_supplier)||
                                                    ' Origin_Country :'||I_country);
      close C_LOCK_BWM;

      SQL_LIB.SET_MARK('DELETE',NULL,L_TABLE,'Item :'||I_item||
                                             ' Supplier :'||to_char(I_supplier)||
                                             ' Origin_Country :'||I_country);
      if TAB_rowids is NOT NULL and TAB_rowids.COUNT > 0 then
         FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            delete from buyer_wksht_manual
             where rowid = TAB_rowids(i);
         ---
         if SQL%NOTFOUND then
      SQL_LIB.SET_MARK('DELETE',NULL,L_TABLE,'Item :'||I_item||
                                             ' Supplier :'||to_char(I_supplier)||
                                             ' Origin_Country :'||I_country);
            return FALSE;
         end if;
      end if;
      ---
      L_TABLE:='ITEM_SUPP_COUNTRY_LOC_CFA_EXT';

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ISCL_CE',L_TABLE,'Item :'||I_item||
                                                        ' Supplier :'||to_char(I_supplier)||
                                                        ' Origin_Country :'||I_country);
      open  C_LOCK_ISCL_CE;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_ISCL_CE',L_TABLE,'Item :'||I_item||
                                                        ' Supplier :'||to_char(I_supplier)||
                                                        ' Origin_Country :'||I_country);
      fetch  C_LOCK_ISCL_CE BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ISCL_CE',L_TABLE,'Item :'||I_item||
                                                        ' Supplier :'||to_char(I_supplier)||
                                                        ' Origin_Country :'||I_country);
      close C_LOCK_ISCL_CE;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,L_TABLE,'Item :'||I_item||
                                                        ' Supplier :'||to_char(I_supplier)||
                                                        ' Origin_Country :'||I_country);            
      if TAB_rowids is NOT NULL and TAB_rowids.COUNT > 0 then
         FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            delete from item_supp_country_loc_cfa_ext
             where rowid = TAB_rowids(i);
      end if;
      ---
      L_TABLE:='ITEM_SUPP_COUNTRY_LOC';

      SQL_LIB.SET_MARK('OPEN','C_LOCK_ISCL',L_TABLE,'Item :'||I_item||
                                                     ' Supplier :'||to_char(I_supplier)||
                                                     ' Origin_Country :'||I_country);
      open  C_LOCK_ISCL;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_ISCL',L_TABLE,'Item :'||I_item||
                                                     ' Supplier :'||to_char(I_supplier)||
                                                     ' Origin_Country :'||I_country);
      fetch  C_LOCK_ISCL BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ISCL',L_TABLE,'Item :'||I_item||
                                                     ' Supplier :'||to_char(I_supplier)||
                                                     ' Origin_Country :'||I_country);
      close C_LOCK_ISCL;
      ---
      if FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY(O_error_message,
                                                L_cost_event_process_id,
                                                FUTURE_COST_EVENT_SQL.REMOVE_EVENT,
                                                L_sc_cost_event_tbl,
                                                L_user) = FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('DELETE',NULL,L_TABLE,'Item :'||I_item||
                                             ' Supplier :'||to_char(I_supplier)||
                                             ' Origin_Country :'||I_country);
      if TAB_rowids is NOT NULL and TAB_rowids.COUNT > 0 then
         FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            delete from item_supp_country_loc
             where rowid = TAB_rowids(i);
         ---
         if SQL%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_DELETE_REC','Program :'||L_program,'Table :'||L_TABLE, 'Item :'||I_item||
                                                                                                                     ' Supplier :'||to_char(I_supplier)||
                                                                                                                     ' Origin_Country :'||I_country);
            return FALSE;
         end if;
      end if;
   

   ---

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_TABLE,
                                            'Item :'||I_item,
                                            'Supplier :'||to_char(I_supplier));
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BULK_DELETE;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_YES_PRIM_IND(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_iscl_rec      IN ITEM_SUPP_COUNTRY_LOC%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.UPDATE_YES_PRIM_IND';

--- NOTE:  only used for XITEM subscription API after UPDATE_PRIMARY_INDICATORS is called
---
BEGIN
   if not LOCK_ITEM_SUPP_COUNTRY_LOC(O_error_message,
                                     I_iscl_rec.item,
                                     I_iscl_rec.supplier,
                                     I_iscl_rec.origin_country_id,
                                     I_iscl_rec.loc) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE', NULL, 'ITEM_SUPP_COUNTRY_LOC','item: '||I_iscl_rec.item||
                                                    ' supplier: '||I_iscl_rec.supplier||
                                                    ' country: '||I_iscl_rec.origin_country_id||
                                                    ' loc: '||I_iscl_rec.loc);

   update ITEM_SUPP_COUNTRY_LOC
      set  primary_loc_ind = 'Y',
           last_update_datetime = sysdate,
           last_update_id = I_iscl_rec.last_update_id
    where item = I_iscl_rec.item
      and supplier = I_iscl_rec.supplier
      and origin_country_id = I_iscl_rec.origin_country_id
      and loc = I_iscl_rec.loc;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC','Program :'||L_program,
                                                                   'Table :ITEM_SUPP_COUNTRY_LOC','Item :'||I_iscl_rec.item||
                                                                                                  ' Supplier :'||to_char(I_iscl_rec.supplier)||
                                                                                                  ' Origin_Country :'||I_iscl_rec.origin_country_id);
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_YES_PRIM_IND;
------------------------------------------------------------------------------------
FUNCTION SINGLE_INSERT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_iscl_def        IN       ITEM_SUPP_COUNTRY_LOC%ROWTYPE)

   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC.SINGLE_INSERT';

BEGIN
   SQL_LIB.SET_MARK('INSERT',NULL, 'ITEM_SUPP_COUNTRY_LOC','item: '||I_iscl_def.item||
                                                           ' supplier: '||to_char(I_iscl_def.supplier)||
                                                           ' country: '||I_iscl_def.origin_country_id||
                                                           ' loc: '||to_char(I_iscl_def.loc));
   insert into item_supp_country_loc(item,
                                     supplier,
                                     origin_country_id,
                                     loc,
                                     loc_type,
                                     primary_loc_ind,
                                     unit_cost,
                                     round_lvl,
                                     round_to_inner_pct,
                                     round_to_case_pct,
                                     round_to_layer_pct,
                                     round_to_pallet_pct,
                                     supp_hier_type_1,
                                     supp_hier_lvl_1,
                                     supp_hier_type_2,
                                     supp_hier_lvl_2,
                                     supp_hier_type_3,
                                     supp_hier_lvl_3,
                                     pickup_lead_time,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     negotiated_item_cost,
                                     extended_base_cost,
                                     inclusive_cost,
                                     base_cost,
                                     create_id)
                             values( I_iscl_def.item,
                                     I_iscl_def.supplier,
                                     I_iscl_def.origin_country_id,
                                     I_iscl_def.loc,
                                     I_iscl_def.loc_type,
                                     I_iscl_def.primary_loc_ind,
                                     I_iscl_def.unit_cost,
                                     I_iscl_def.round_lvl,
                                     I_iscl_def.round_to_inner_pct,
                                     I_iscl_def.round_to_case_pct,
                                     I_iscl_def.round_to_layer_pct,
                                     I_iscl_def.round_to_pallet_pct,
                                     I_iscl_def.supp_hier_type_1,
                                     I_iscl_def.supp_hier_lvl_1,
                                     I_iscl_def.supp_hier_type_2,
                                     I_iscl_def.supp_hier_lvl_2,
                                     I_iscl_def.supp_hier_type_3,
                                     I_iscl_def.supp_hier_lvl_3,
                                     I_iscl_def.pickup_lead_time,
                                     I_iscl_def.create_datetime,
                                     sysdate,
                                     I_iscl_def.last_update_id,
                                     I_iscl_def.negotiated_item_cost,
                                     I_iscl_def.extended_base_cost,
                                     I_iscl_def.inclusive_cost,
                                     I_iscl_def.base_cost,
                                     I_iscl_def.create_id);
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SINGLE_INSERT;
-----------------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item              IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_loc               IN     ITEM_LOC.LOC%TYPE,
                         I_like_store        IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type          IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_wh_unit_cost      IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                         I_wf_ind            IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.CREATE_LOCATION';
   L_table                    VARCHAR2(30)   := 'ITEM_SUPP_COUNTRY_LOC';
   L_min_loc                  ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_buyer_pack               VARCHAR2(1)    := 'N';
   L_all_locs                 VARCHAR2(1);
   L_dept                     ITEM_MASTER.DEPT%TYPE;
   L_exists                   BOOLEAN;
   L_update                   VARCHAR2(1)    := NULL;
   L_system_options_rec       SYSTEM_OPTIONS%ROWTYPE;
   L_country_attrib_rec       COUNTRY_ATTRIB%ROWTYPE;
   L_item_cost_head_rec       ITEM_COST_HEAD%ROWTYPE;
   L_nic_static_ind           ITEM_COST_HEAD.NIC_STATIC_IND%TYPE;
   L_origin_country_id        ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_pack_type                ITEM_MASTER.PACK_TYPE%TYPE;
   L_unit_cost                ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_supplier                 ITEM_SUPP_COUNTRY.SUPPLIER%TYPE;
   L_base_cost                ITEM_COST_HEAD.BASE_COST%TYPE;
   L_negotiated_item_cost     ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE;
   L_extended_base_cost       ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost           ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE;
   L_loc                      ITEM_LOC.LOC%TYPE;
   L_item_cost_info_rec       ITEM_COST_SQL.ITEM_COST_INFO_REC;
   L_dlvry_country_id         ADDR.COUNTRY_ID%TYPE;
   L_default_po_cost          COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;
   L_item_cost_tax_incl_ind   VARCHAR2(1);
   L_store_type               STORE.STORE_TYPE%TYPE;

   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_DEPT is
      select dept
        from item_master
       where item = I_item;

   cursor C_GET_MIN_ISC_LOC is
      select MIN(isl.loc)
        from item_supp_country_loc isl
       where isl.item = I_item;

   cursor C_UPDATE_PRIM_LOC_IND is
      select 'x'
        from item_supp_country_loc isl
       where isl.loc               = L_min_loc
         and isl.item              = I_item
         and isl.supplier          = NVL(I_supplier, supplier)
         and isl.origin_country_id = NVL(I_origin_country_id, origin_country_id)
         and not exists (select 'x'
                           from item_supp_country_loc isl3
                          where isl3.loc              != L_min_loc
                            and isl3.item              = isl.item
                            and isl3.supplier          = NVL(I_supplier, supplier)
                            and isl3.origin_country_id = NVL(I_origin_country_id, origin_country_id)
                            and isl3.primary_loc_ind   = 'Y'
                            and rownum = 1)
          for update nowait;

   cursor C_PACK_TYPE is
      select NVL(pack_type,'N')
        from item_master
       where item = I_item;

   cursor C_SUP_COUNTRY is
      select supplier,
             origin_country_id
        from item_supp_country
       where item = I_item
         and supplier = NVL(I_supplier,supplier)
         and origin_country_id = NVL(I_origin_country_id,origin_country_id);
      
   cursor C_GET_PRIMARY_CTRY is
      select mva.country_id,
             NVL(default_po_cost, 'BC') default_po_cost
        from mv_loc_prim_addr mva,
             country_attrib ca
       where mva.loc = I_loc
         and mva.country_id = ca.country_id;      
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   ---
   if I_like_store IS NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      open C_GET_DEPT;
      SQL_LIB.SET_MARK('FETCH','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      fetch C_GET_DEPT into L_dept;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      close C_GET_DEPT;
      ---

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);

      if L_system_options_rec.default_tax_type = 'GTAX' then

         --- Get the Address country id as delivery country id for item cost
         SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB','Loc: '||TO_CHAR(I_loc));
         open C_GET_PRIMARY_CTRY;
         SQL_LIB.SET_MARK('FETCH','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB','Loc: '||TO_CHAR(I_loc));
         fetch C_GET_PRIMARY_CTRY into L_dlvry_country_id, 
                                       L_default_po_cost;
         SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB','Loc: '||TO_CHAR(I_loc));
         close C_GET_PRIMARY_CTRY;

         FOR C_sup_ctry_rec in C_SUP_COUNTRY LOOP
            L_supplier           := C_sup_ctry_rec.supplier;
            L_origin_country_id  := C_sup_ctry_rec.origin_country_id;
            
            ---Get the NIC/BC/Static IND from GET_ITEM_COST_HEAD fun
            if ITEM_COST_SQL.GET_ITEM_COST_HEAD(O_error_message,
                                                L_item_cost_head_rec,
                                                I_item,
                                                L_supplier,
                                                L_origin_country_id,
                                                L_dlvry_country_id) = FALSE then
               return FALSE;
            end if;

            ---Assign the static ind, NIC and BC to local variables
            L_nic_static_ind        := NVL(L_item_cost_head_rec.nic_static_ind,'N');

            if L_nic_static_ind = 'Y' then
               L_negotiated_item_cost  := L_item_cost_head_rec.negotiated_item_cost;
               L_item_cost_tax_incl_ind := 'Y';
            else
               L_base_cost := L_item_cost_head_rec.base_cost;
               L_item_cost_tax_incl_ind := 'N';
            end if;

            if I_loc_type = 'S' then
               ---
               if I_wf_ind = 'Y' and L_nic_static_ind = 'Y' then --- this is a wholesale/franchise store use the source wh unit cost
                  L_negotiated_item_cost := I_wh_unit_cost;
               elsif I_wf_ind = 'Y' and L_nic_static_ind = 'N' then
                  L_base_cost := I_wh_unit_cost;
               end if;
               ---
               if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                                  L_base_cost,
                                                  L_extended_base_cost,
                                                  L_inclusive_cost,
                                                  L_negotiated_item_cost,
                                                  I_item,
                                                  L_nic_static_ind,
                                                  L_supplier,
                                                  I_loc,
                                                  I_loc_type,
                                                  'ITEMSUPPCTRYLOC',
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  'N',
                                                  'N',
                                                  'N',
                                                  NULL,
                                                  L_item_cost_tax_incl_ind) = FALSE then
                  return FALSE;
               end if;
               ---
               if L_negotiated_item_cost = 0 then
                  L_negotiated_item_cost := NULL;
               end if;

               if L_extended_base_cost = 0 then
                  L_extended_base_cost := NULL;
               end if;

               if L_inclusive_cost = 0 then
                  L_inclusive_cost := NULL;
               end if;

               if L_base_cost = 0 then
                  L_base_cost := NULL;
               end if;

               ---
               insert into item_supp_country_loc(item,
                                                 supplier,
                                                 origin_country_id,
                                                 loc,
                                                 loc_type,
                                                 primary_loc_ind,
                                                 unit_cost,
                                                 round_lvl,
                                                 round_to_inner_pct,
                                                 round_to_case_pct,
                                                 round_to_layer_pct,
                                                 round_to_pallet_pct,
                                                 supp_hier_type_1,
                                                 supp_hier_lvl_1,
                                                 supp_hier_type_2,
                                                 supp_hier_lvl_2,
                                                 supp_hier_type_3,
                                                 supp_hier_lvl_3,
                                                 pickup_lead_time,
                                                 create_datetime,
                                                 last_update_datetime,
                                                 last_update_id,
                                                 negotiated_item_cost,
                                                 extended_base_cost,
                                                 inclusive_cost,
                                                 base_cost,
                                                 create_id)
                                          select I_item,
                                                 isc.supplier,
                                                 isc.origin_country_id,
                                                 I_loc,
                                                 I_loc_type,
                                                 'N',
                                                 DECODE(L_default_po_cost,'NIC',NVL(L_negotiated_item_cost,isc.unit_cost),
                                                                          'BC',NVL(L_base_cost,isc.unit_cost)),
                                                 isc.round_lvl,
                                                 isc.round_to_inner_pct,
                                                 isc.round_to_case_pct,
                                                 isc.round_to_layer_pct,
                                                 isc.round_to_pallet_pct,
                                                 isc.supp_hier_type_1,
                                                 isc.supp_hier_lvl_1,
                                                 isc.supp_hier_type_2,
                                                 isc.supp_hier_lvl_2,
                                                 isc.supp_hier_type_3,
                                                 isc.supp_hier_lvl_3,
                                                 isc.pickup_lead_time,
                                                 sysdate,
                                                 sysdate,
                                                 get_user,
                                                 L_negotiated_item_cost,
                                                 L_extended_base_cost,
                                                 L_inclusive_cost,
                                                 L_base_cost,
                                                 get_user
                                             from sups s,
                                                  item_supp_country isc
                                            where isc.item = I_item
                                              and isc.supplier = NVL(I_supplier, c_sup_ctry_rec.supplier)
                                              and isc.origin_country_id = NVL(I_origin_country_id, c_sup_ctry_rec.origin_country_id)
                                              and s.supplier = isc.supplier
                                              and (I_loc_type = 'S' or s.inv_mgmt_lvl in ('S', 'D'))
                                              and not exists(select 'x'
                                                               from item_supp_country_loc isl
                                                              where isl.item              = I_item
                                                                and isl.loc               = I_loc
                                                                and isl.supplier          = isc.supplier
                                                                and isl.origin_country_id = isc.origin_country_id
                                                                and rownum = 1);
            else -- I_loc_type != 'S'
               if ITEM_COST_SQL.COMPUTE_ITEM_COST(O_error_message,
                                                  L_base_cost,
                                                  L_extended_base_cost,
                                                  L_inclusive_cost,
                                                  L_negotiated_item_cost,
                                                  I_item,
                                                  L_nic_static_ind,
                                                  L_supplier,
                                                  I_loc,
                                                  I_loc_type,
                                                  'ITEMSUPPCTRYLOC',
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  'N',
                                                  'N',
                                                  'N',
                                                  NULL,
                                                  L_item_cost_tax_incl_ind) = FALSE then
                  return FALSE;
               end if;
               ---
               if L_negotiated_item_cost = 0 then
                  L_negotiated_item_cost := NULL;
               end if;

               if L_extended_base_cost = 0 then
                  L_extended_base_cost := NULL;
               end if;

               if L_inclusive_cost = 0 then
                  L_inclusive_cost := NULL;
               end if;

               if L_base_cost = 0 then
                  L_base_cost := NULL;
               end if;

               ---
               insert into item_supp_country_loc(item,
                                                 supplier,
                                                 origin_country_id,
                                                 loc,
                                                 loc_type,
                                                 primary_loc_ind,
                                                 unit_cost,
                                                 round_lvl,
                                                 round_to_inner_pct,
                                                 round_to_case_pct,
                                                 round_to_layer_pct,
                                                 round_to_pallet_pct,
                                                 supp_hier_type_1,
                                                 supp_hier_lvl_1,
                                                 supp_hier_type_2,
                                                 supp_hier_lvl_2,
                                                 supp_hier_type_3,
                                                 supp_hier_lvl_3,
                                                 pickup_lead_time,
                                                 create_datetime,
                                                 last_update_datetime,
                                                 last_update_id,
                                                 negotiated_item_cost,
                                                 extended_base_cost,
                                                 inclusive_cost,
                                                 base_cost,
                                                 create_id)
                                          select I_item,
                                                 isc.supplier,
                                                 isc.origin_country_id,
                                                 I_loc,
                                                 I_loc_type,
                                                 'N',
                                                 DECODE(L_default_po_cost,'NIC',NVL(L_negotiated_item_cost,isc.unit_cost),
                                                                          'BC',NVL(L_base_cost,isc.unit_cost)),
                                                 sim.round_lvl,
                                                 sim.round_to_inner_pct,
                                                 sim.round_to_case_pct,
                                                 sim.round_to_layer_pct,
                                                 sim.round_to_pallet_pct,
                                                 isc.supp_hier_type_1,
                                                 isc.supp_hier_lvl_1,
                                                 isc.supp_hier_type_2,
                                                 isc.supp_hier_lvl_2,
                                                 isc.supp_hier_type_3,
                                                 isc.supp_hier_lvl_3,
                                                 isc.pickup_lead_time,
                                                 sysdate,
                                                 sysdate,
                                                 get_user,
                                                 L_negotiated_item_cost,
                                                 L_extended_base_cost,
                                                 L_inclusive_cost,
                                                 L_base_cost,
                                                 get_user
                                            from item_supp_country isc,
                                                 sups s,
                                                 sup_inv_mgmt sim,
                                                 (select store location,
                                                         store phy_loc
                                                    from store
                                                   where store = I_loc
                                                     and I_loc_type = 'S'
                                                   union all
                                                  select wh location,
                                                         physical_wh phy_loc
                                                    from wh
                                                   where wh = I_loc
                                                     and I_loc_type = 'W') loc
                                           where isc.item = I_item
                                             and isc.supplier = NVL(I_supplier, c_sup_ctry_rec.supplier)
                                             and isc.origin_country_id = NVL(I_origin_country_id, c_sup_ctry_rec.origin_country_id)
                                             and s.supplier = isc.supplier
                                             and s.inv_mgmt_lvl = 'A'
                                             and sim.supplier = isc.supplier
                                             and sim.dept     = L_dept
                                             and sim.location = loc.phy_loc
                                             and not exists(select 'x'
                                                              from item_supp_country_loc isl
                                                             where isl.item              = I_item
                                                               and isl.loc               = I_loc
                                                               and isl.supplier          = isc.supplier
                                                               and isl.origin_country_id = isc.origin_country_id
                                                               and rownum = 1)
                                          UNION ALL
                                          select I_item,
                                                 isc.supplier,
                                                 isc.origin_country_id,
                                                 I_loc,
                                                 I_loc_type,
                                                 'N',
                                                 DECODE(L_default_po_cost,'NIC',NVL(L_negotiated_item_cost,isc.unit_cost),
                                                                          'BC',NVL(L_base_cost,isc.unit_cost)),
                                                 sim.round_lvl,
                                                 sim.round_to_inner_pct,
                                                 sim.round_to_case_pct,
                                                 sim.round_to_layer_pct,
                                                 sim.round_to_pallet_pct,
                                                 isc.supp_hier_type_1,
                                                 isc.supp_hier_lvl_1,
                                                 isc.supp_hier_type_2,
                                                 isc.supp_hier_lvl_2,
                                                 isc.supp_hier_type_3,
                                                 isc.supp_hier_lvl_3,
                                                 isc.pickup_lead_time,
                                                 sysdate,
                                                 sysdate,
                                                 get_user,
                                                 L_negotiated_item_cost,
                                                 L_extended_base_cost,
                                                 L_inclusive_cost,
                                                 L_base_cost,
                                                 get_user
                                            from item_supp_country isc,
                                                 sups s,
                                                 sup_inv_mgmt sim,
                                                 (select store location,
                                                         store phy_loc
                                                    from store
                                                   where store = I_loc
                                                     and I_loc_type = 'S'
                                                   union all
                                                  select wh location,
                                                         physical_wh phy_loc
                                                    from wh
                                                   where wh = I_loc
                                                     and I_loc_type = 'W') loc
                                           where isc.item = I_item
                                             and isc.supplier = NVL(I_supplier, c_sup_ctry_rec.supplier)
                                             and isc.origin_country_id = NVL(I_origin_country_id, c_sup_ctry_rec.origin_country_id)
                                             and s.supplier = isc.supplier
                                             and s.inv_mgmt_lvl = 'L'
                                             and sim.supplier = isc.supplier
                                             and sim.dept is NULL
                                             and sim.location = loc.phy_loc
                                             and not exists(select 'x'
                                                              from item_supp_country_loc isl
                                                             where isl.item              = I_item
                                                               and isl.loc               = I_loc
                                                               and isl.supplier          = isc.supplier
                                                               and isl.origin_country_id = isc.origin_country_id
                                                               and rownum = 1)
                                          UNION ALL
                                          select I_item,
                                                 isc.supplier,
                                                 isc.origin_country_id,
                                                 I_loc,
                                                 I_loc_type,
                                                 'N',
                                                 DECODE(L_default_po_cost,'NIC',NVL(L_negotiated_item_cost,isc.unit_cost),
                                                                          'BC',NVL(L_base_cost,isc.unit_cost)),
                                                 isc.round_lvl,
                                                 isc.round_to_inner_pct,
                                                 isc.round_to_case_pct,
                                                 isc.round_to_layer_pct,
                                                 isc.round_to_pallet_pct,
                                                 isc.supp_hier_type_1,
                                                 isc.supp_hier_lvl_1,
                                                 isc.supp_hier_type_2,
                                                 isc.supp_hier_lvl_2,
                                                 isc.supp_hier_type_3,
                                                 isc.supp_hier_lvl_3,
                                                 isc.pickup_lead_time,
                                                 sysdate,
                                                 sysdate,
                                                 get_user,
                                                 L_negotiated_item_cost,
                                                 L_extended_base_cost,
                                                 L_inclusive_cost,
                                                 L_base_cost,
                                                 get_user
                                            from item_supp_country isc,
                                                 (select store location,
                                                         store phy_loc
                                                    from store
                                                   where store = I_loc
                                                     and I_loc_type = 'S'
                                                   union all
                                                  select wh location,
                                                         physical_wh phy_loc
                                                    from wh
                                                   where wh = I_loc
                                                     and I_loc_type = 'W') loc,
                                                 sups s
                                           where isc.item = I_item
                                             and isc.supplier = NVL(I_supplier, c_sup_ctry_rec.supplier)
                                             and isc.origin_country_id = NVL(I_origin_country_id, c_sup_ctry_rec.origin_country_id)
                                             and s.supplier = isc.supplier
                                             and (  (s.inv_mgmt_lvl = 'A'
                                                     and not exists(select 'x'
                                                                      from sup_inv_mgmt sim2
                                                                      where sim2.supplier = isc.supplier
                                                                       and sim2.dept     = L_dept
                                                                       and sim2.location = loc.phy_loc
                                                       and rownum = 1))
                                                 or (s.inv_mgmt_lvl = 'L'
                                                     and not exists(select 'x'
                                                                      from sup_inv_mgmt sim2
                                                                     where sim2.supplier = isc.supplier
                                                                       and sim2.location = loc.phy_loc
                                                        and rownum = 1)))
                                             and not exists(select 'x'
                                                              from item_supp_country_loc isl
                                                             where isl.item              = I_item
                                                               and isl.loc               = I_loc
                                                               and isl.supplier          = isc.supplier
                                                               and isl.origin_country_id = isc.origin_country_id
                                                               and rownum = 1)
                                          UNION ALL
                                          select I_item,
                                                 isc.supplier,
                                                 isc.origin_country_id,
                                                 I_loc,
                                                 I_loc_type,
                                                 'N',
                                                 DECODE(L_default_po_cost,'NIC',NVL(L_negotiated_item_cost,isc.unit_cost),
                                                                          'BC',NVL(L_base_cost,isc.unit_cost)),
                                                 isc.round_lvl,
                                                 isc.round_to_inner_pct,
                                                 isc.round_to_case_pct,
                                                 isc.round_to_layer_pct,
                                                 isc.round_to_pallet_pct,
                                                 isc.supp_hier_type_1,
                                                 isc.supp_hier_lvl_1,
                                                 isc.supp_hier_type_2,
                                                 isc.supp_hier_lvl_2,
                                                 isc.supp_hier_type_3,
                                                 isc.supp_hier_lvl_3,
                                                 isc.pickup_lead_time,
                                                 sysdate,
                                                 sysdate,
                                                 get_user,
                                                 L_negotiated_item_cost,
                                                 L_extended_base_cost,
                                                 L_inclusive_cost,
                                                 L_base_cost,
                                                 get_user
                                            from item_supp_country isc,
                                                 sups s
                                           where isc.item = I_item
                                             and isc.supplier = NVL(I_supplier, c_sup_ctry_rec.supplier)
                                             and isc.origin_country_id = NVL(I_origin_country_id, c_sup_ctry_rec.origin_country_id)
                                             and s.supplier = isc.supplier
                                             and (s.inv_mgmt_lvl in ('S', 'D'))
                                             and not exists(select 'x'
                                                              from item_supp_country_loc isl
                                                             where isl.item              = I_item
                                                               and isl.loc               = I_loc
                                                               and isl.supplier          = isc.supplier
                                                               and isl.origin_country_id = isc.origin_country_id);
            end if;
         end LOOP;
      ---
      else   ---else the default_tax_type is OTHERS
         if I_loc_type = 'S' then
             ---
            if I_wf_ind = 'Y' then --- this is a wholesale/franchise store use the source wh unit cost
               L_unit_cost := I_wh_unit_cost;
            end if;

               insert into item_supp_country_loc(item,
                                                 supplier,
                                                 origin_country_id,
                                                 loc,
                                                 loc_type,
                                                 primary_loc_ind,
                                                 unit_cost,
                                                 round_lvl,
                                                 round_to_inner_pct,
                                                 round_to_case_pct,
                                                 round_to_layer_pct,
                                                 round_to_pallet_pct,
                                                 supp_hier_type_1,
                                                 supp_hier_lvl_1,
                                                 supp_hier_type_2,
                                                 supp_hier_lvl_2,
                                                 supp_hier_type_3,
                                                 supp_hier_lvl_3,
                                                 pickup_lead_time,
                                                 create_datetime,
                                                 last_update_datetime,
                                                 last_update_id,
                                                 negotiated_item_cost,
                                                 extended_base_cost,
                                                 inclusive_cost,
                                                 base_cost,
                                                 create_id)
                                          select I_item,
                                                 isc.supplier,
                                                 isc.origin_country_id,
                                                 I_loc,
                                                 I_loc_type,
                                                 'N',
                                                 NVL(L_unit_cost,isc.unit_cost),
                                                 isc.round_lvl,
                                                 isc.round_to_inner_pct,
                                                 isc.round_to_case_pct,
                                                 isc.round_to_layer_pct,
                                                 isc.round_to_pallet_pct,
                                                 isc.supp_hier_type_1,
                                                 isc.supp_hier_lvl_1,
                                                 isc.supp_hier_type_2,
                                                 isc.supp_hier_lvl_2,
                                                 isc.supp_hier_type_3,
                                                 isc.supp_hier_lvl_3,
                                                 isc.pickup_lead_time,
                                                 sysdate,
                                                 sysdate,
                                                 get_user,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 NULL,
                                                 get_user
                                            from sups s,
                                                 item_supp_country isc
                                           where isc.item = I_item
                                             and isc.supplier = NVL(I_supplier, isc.supplier)
                                             and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                             and s.supplier = isc.supplier
                                             and (I_loc_type = 'S' or s.inv_mgmt_lvl in ('S', 'D'))
                                             and not exists(select 'x'
                                                              from item_supp_country_loc isl
                                                             where isl.item              = I_item
                                                               and isl.loc               = I_loc
                                                               and isl.supplier          = isc.supplier
                                                               and isl.origin_country_id = isc.origin_country_id
                                                            and rownum = 1);

        else

           insert into item_supp_country_loc(item,
                                              supplier,
                                              origin_country_id,
                                              loc,
                                              loc_type,
                                              primary_loc_ind,
                                              unit_cost,
                                              round_lvl,
                                              round_to_inner_pct,
                                              round_to_case_pct,
                                              round_to_layer_pct,
                                              round_to_pallet_pct,
                                              supp_hier_type_1,
                                              supp_hier_lvl_1,
                                              supp_hier_type_2,
                                              supp_hier_lvl_2,
                                              supp_hier_type_3,
                                              supp_hier_lvl_3,
                                              pickup_lead_time,
                                              create_datetime,
                                              last_update_datetime,
                                              last_update_id,
                                              negotiated_item_cost,
                                              extended_base_cost,
                                              inclusive_cost,
                                              base_cost,
                                              create_id)
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              isc.unit_cost,
                                              sim.round_lvl,
                                              sim.round_to_inner_pct,
                                              sim.round_to_case_pct,
                                              sim.round_to_layer_pct,
                                              sim.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              get_user
                                         from item_supp_country isc,
                                              sups s,
                                              sup_inv_mgmt sim,
                                              (select store location,
                                                      store phy_loc
                                                 from store
                                                where store = I_loc
                                                  and I_loc_type = 'S'
                                                union all
                                               select wh location,
                                                      physical_wh phy_loc
                                                 from wh
                                                where wh = I_loc
                                                  and I_loc_type = 'W') loc
                                        where isc.item = I_item
                                          and isc.supplier = NVL(I_supplier, isc.supplier)
                                          and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                          and s.supplier = isc.supplier
                                          and s.inv_mgmt_lvl = 'A'
                                          and sim.supplier = isc.supplier
                                          and sim.dept     = L_dept
                                          and sim.location = loc.phy_loc
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id
                                                            and rownum = 1)
                                       UNION ALL
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              isc.unit_cost,
                                              sim.round_lvl,
                                              sim.round_to_inner_pct,
                                              sim.round_to_case_pct,
                                              sim.round_to_layer_pct,
                                              sim.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL,
                                              NULL,
                                              NULL, 
                                              NULL,
                                              get_user
                                         from item_supp_country isc,
                                              sups s,
                                              sup_inv_mgmt sim,
                                              (select store location,
                                                      store phy_loc
                                                 from store
                                                where store = I_loc
                                                  and I_loc_type = 'S'
                                                union all
                                               select wh location,
                                                      physical_wh phy_loc
                                                 from wh
                                                where wh = I_loc
                                                  and I_loc_type = 'W') loc
                                        where isc.item = I_item
                                          and isc.supplier = NVL(I_supplier, isc.supplier)
                                          and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                          and s.supplier = isc.supplier
                                          and s.inv_mgmt_lvl = 'L'
                                          and sim.supplier = isc.supplier
                                          and sim.dept is NULL
                                          and sim.location = loc.phy_loc
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id
                                                            and rownum = 1)
                                       UNION ALL
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              isc.unit_cost,
                                              isc.round_lvl,
                                              isc.round_to_inner_pct,
                                              isc.round_to_case_pct,
                                              isc.round_to_layer_pct,
                                              isc.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              get_user 
                                         from item_supp_country isc,
                                              (select store location,
                                                      store phy_loc
                                                 from store
                                                where store = I_loc
                                                  and I_loc_type = 'S'
                                                union all
                                               select wh location,
                                                      physical_wh phy_loc
                                                 from wh
                                                where wh = I_loc
                                                  and I_loc_type = 'W') loc,
                                              sups s
                                        where isc.item = I_item
                                          and isc.supplier = NVL(I_supplier, isc.supplier)
                                          and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                          and s.supplier = isc.supplier
                                          and (  (s.inv_mgmt_lvl = 'A'
                                                  and not exists(select 'x'
                                                                   from sup_inv_mgmt sim2
                                                                   where sim2.supplier = isc.supplier
                                                                    and sim2.dept     = L_dept
                                                                    and sim2.location = loc.phy_loc
                                                    and rownum = 1))
                                              or (s.inv_mgmt_lvl = 'L'
                                                  and not exists(select 'x'
                                                                   from sup_inv_mgmt sim2
                                                                  where sim2.supplier = isc.supplier
                                                                    and sim2.location = loc.phy_loc
                                                     and rownum = 1)))
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id
                                                            and rownum = 1)
                                       UNION ALL
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              isc.unit_cost,
                                              isc.round_lvl,
                                              isc.round_to_inner_pct,
                                              isc.round_to_case_pct,
                                              isc.round_to_layer_pct,
                                              isc.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL, 
                                              NULL,
                                              NULL,
                                              NULL,
                                              get_user
                                         from item_supp_country isc,
                                              sups s
                                        where isc.item = I_item
                                          and isc.supplier = NVL(I_supplier, isc.supplier)
                                          and isc.origin_country_id = NVL(I_origin_country_id, isc.origin_country_id)
                                          and s.supplier = isc.supplier
                                          and (s.inv_mgmt_lvl in ('S', 'D'))
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id);
          end if;
      ---
      end if;
      ---
      -- The minimum lowest location number will be selected to be the primary location on mass inserts.
      ---
      L_update := 'Y';
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
      open C_GET_MIN_ISC_LOC;
      SQL_LIB.SET_MARK('FETCH','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
      fetch C_GET_MIN_ISC_LOC into L_min_loc;
      ---
      if C_GET_MIN_ISC_LOC%NOTFOUND then
         L_update := 'N';
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
      close C_GET_MIN_ISC_LOC;
      ---
      if L_update = 'Y' then
         SQL_LIB.SET_MARK('OPEN','C_UPDATE_PRIM_LOC_IND','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
         open C_UPDATE_PRIM_LOC_IND;
         SQL_LIB.SET_MARK('CLOSE','C_UPDATE_PRIM_LOC_IND','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
         close C_UPDATE_PRIM_LOC_IND;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);

         update item_supp_country_loc isl
            set primary_loc_ind      = 'Y',
                last_update_datetime = sysdate,
                last_update_id       = get_user
          where isl.item              = I_item
            and isl.loc               = L_min_loc
            and isl.supplier          = NVL(I_supplier, supplier)
            and isl.origin_country_id = NVL(I_origin_country_id, origin_country_id)
            and not exists (select 'x'
                              from item_supp_country_loc isl3
                             where isl3.item              = isl.item
                               and isl3.loc              != isl.loc
                               and isl3.supplier          = NVL(I_supplier, supplier)
                               and isl3.origin_country_id = NVL(I_origin_country_id, origin_country_id)
                               and isl3.primary_loc_ind   = 'Y'
                               and rownum = 1);
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_PACK_TYPE','ITEM_MASTER','Item: '||I_item);
      open C_PACK_TYPE;
      SQL_LIB.SET_MARK('FETCH', 'C_PACK_TYPE','ITEM_MASTER','Item: '||I_item);
      fetch C_PACK_TYPE into L_pack_type;
      SQL_LIB.SET_MARK('CLOSE','C_PACK_TYPE','ITEM_MASTER','Item: '||I_item);
      close C_PACK_TYPE;
      ---
      if L_pack_type = 'B' then
         if L_system_options_rec.default_tax_type = 'GTAX' then
            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','ITEM: '||I_item);
            update item_supp_country_loc isc1
               set isc1.unit_cost            = ( select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                                   from item_supp_country_loc isc2,
                                                        v_packsku_qty vpq
                                                  where vpq.pack_no            = I_item
                                                    and isc2.item              = vpq.item
                                                    and isc2.supplier          = isc1.supplier
                                                    and isc2.origin_country_id = isc1.origin_country_id
                                                    and isc2.loc               = isc1.loc ),
                   isc1.last_update_id       = get_user,
                   isc1.last_update_datetime = sysdate
             where isc1.item = I_item
               and exists (select 'x'
                             from item_supp_country_loc isc3,
                                  v_packsku_qty vpq
                            where vpq.pack_no            = I_item
                              and isc3.item              = vpq.item
                              and isc3.supplier          = isc1.supplier
                              and isc3.origin_country_id = isc1.origin_country_id
                              and isc3.loc               = isc1.loc );
         else
            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','ITEM: '||I_item);
            update item_supp_country_loc isc1
               set isc1.unit_cost            = ( select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                                   from item_supp_country_loc isc2,
                                                        v_packsku_qty vpq
                                                  where vpq.pack_no            = I_item
                                                    and isc2.item              = vpq.item
                                                    and isc2.supplier          = isc1.supplier
                                                    and isc2.origin_country_id = isc1.origin_country_id
                                                    and isc2.loc               = isc1.loc ),
                   isc1.last_update_id       = get_user,
                   isc1.last_update_datetime = sysdate,
                   isc1.negotiated_item_cost = NULL,
                   isc1.extended_base_cost = NULL,
                   isc1.inclusive_cost = NULL,
                   isc1.base_cost = NULL
             where isc1.item = I_item
               and exists (select 'x'
                             from item_supp_country_loc isc3,
                                  v_packsku_qty vpq
                            where vpq.pack_no            = I_item
                              and isc3.item              = vpq.item
                              and isc3.supplier          = isc1.supplier
                              and isc3.origin_country_id = isc1.origin_country_id
                              and isc3.loc               = isc1.loc );
         end if;
      end if;
      ---
      if I_loc is NOT NULL then
         L_all_locs := 'N';
      else
         L_all_locs := 'Y';
      end if;
      ---
      if L_system_options_rec.bracket_costing_ind = 'Y' then
       if ITEM_BRACKET_COST_SQL.CREATE_BRACKET(O_error_message,
                                                I_item,
                                                I_supplier,
                                                I_origin_country_id,
                                                I_loc,
                                                L_all_locs) = FALSE then
           return FALSE;
        end if;
     end if;
     ---
   else  -- I_like_store IS NOT NULL --
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
      insert into item_supp_country_loc(item,
                                        supplier,
                                        origin_country_id,
                                        loc,
                                        loc_type,
                                        primary_loc_ind,
                                        unit_cost,
                                        round_lvl,
                                        round_to_inner_pct,
                                        round_to_case_pct,
                                        round_to_layer_pct,
                                        round_to_pallet_pct,
                                        supp_hier_type_1,
                                        supp_hier_lvl_1,
                                        supp_hier_type_2,
                                        supp_hier_lvl_2,
                                        supp_hier_type_3,
                                        supp_hier_lvl_3,
                                        pickup_lead_time,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        negotiated_item_cost,
                                        extended_base_cost,
                                        inclusive_cost,
                                        base_cost,
                                        create_id)
                                 select I_item,
                                        iscl.supplier,
                                        iscl.origin_country_id,
                                        I_loc,
                                        iscl.loc_type,
                                        'N',
                                        iscl.unit_cost,
                                        iscl.round_lvl,
                                        iscl.round_to_inner_pct,
                                        iscl.round_to_case_pct,
                                        iscl.round_to_layer_pct,
                                        iscl.round_to_pallet_pct,
                                        iscl.supp_hier_type_1,
                                        iscl.supp_hier_lvl_1,
                                        iscl.supp_hier_type_2,
                                        iscl.supp_hier_lvl_2,
                                        iscl.supp_hier_type_3,
                                        iscl.supp_hier_lvl_3,
                                        iscl.pickup_lead_time,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        iscl.negotiated_item_cost,
                                        iscl.extended_base_cost,
                                        iscl.inclusive_cost,
                                        iscl.base_cost,
                                        get_user
                                   from item_supp_country_loc iscl
                                  where iscl.origin_country_id = NVL(I_origin_country_id, iscl.origin_country_id)
                                    and iscl.item = I_item
                                    and iscl.loc = I_like_store
                                    and iscl.supplier = NVL(I_supplier, iscl.supplier)
                                    and exists (select 'x'
                                                  from item_supp_country_loc iscl2
                                                 where iscl2.origin_country_id = iscl.origin_country_id
                                                   and iscl2.item = iscl.item
                                                   and iscl2.supplier = iscl.supplier
                                                   and iscl2.loc != I_loc
                                                   and iscl2.loc = iscl.loc
                                                   and rownum = 1);
     ---
      if L_system_options_rec.bracket_costing_ind = 'Y' then
       if ITEM_BRACKET_COST_SQL.CREATE_BRACKET(O_error_message,
                                                I_item,
                                                I_supplier,
                                                I_origin_country_id,
                                                I_loc,
                                                'N') = FALSE then
           return FALSE;
        end if;
      end if;
     ---
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      -- if the record is locked skip the update because the item_supp_country_loc.primary_loc_ind
      -- is being updated by another thread for the same item/minimum location
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_LOCATION;
-------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_comp_item_cost_tbl IN OUT NOCOPY OBJ_COMP_ITEM_COST_TBL,
                         I_item                IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                         I_loc                 IN     ITEM_LOC.LOC%TYPE,
                         I_like_store          IN     ITEM_LOC.LOC%TYPE,
                         I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                         I_wh_unit_cost        IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                         I_wf_ind              IN     VARCHAR2,
                         I_itemloc_ind         IN     VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.CREATE_LOCATION';
   L_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
   L_dept                  ITEM_MASTER.DEPT%TYPE;
   L_add_1                 ADDR.ADD_1%TYPE;
   L_add_2                 ADDR.ADD_2%TYPE;
   L_add_3                 ADDR.ADD_3%TYPE;
   L_city                  ADDR.CITY%TYPE;
   L_state                 ADDR.STATE%TYPE;
   L_dlvry_country_id      ADDR.COUNTRY_ID%TYPE;
   L_post                  ADDR.POST%TYPE;
   L_module                ADDR.MODULE%TYPE;
   L_key_value_1           ADDR.KEY_VALUE_1%TYPE := I_loc;
   L_key_value_2           ADDR.KEY_VALUE_2%TYPE := NULL;
   L_supplier              ITEM_SUPP_COUNTRY.SUPPLIER%TYPE;
   L_origin_country_id     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_item_cost_head_rec    ITEM_COST_HEAD%ROWTYPE;
   L_nic_static_ind        ITEM_COST_HEAD.NIC_STATIC_IND%TYPE;
   L_base_cost             ITEM_COST_HEAD.BASE_COST%TYPE               := NULL;
   L_negotiated_item_cost  ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE := NULL;
   L_extended_base_cost    ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE   := NULL;
   L_inclusive_cost        ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE       := NULL;
   L_unit_cost             ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_update                VARCHAR2(1)    := NULL;
   L_min_loc               ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_pack_type             ITEM_MASTER.PACK_TYPE%TYPE;
   L_all_locs              VARCHAR2(1);
   L_default_po_cost       COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;
   L_store_type            STORE.STORE_TYPE%TYPE;
   L_vdate                 PERIOD.VDATE%TYPE;
   L_comp_item_cost_rec    OBJ_COMP_ITEM_COST_REC := NEW OBJ_COMP_ITEM_COST_REC();
   L_prim_loc_exist        VARCHAR2(1) := 'N';
   L_supp_cntry_cost       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_update_itemcost_ind   VARCHAR2(1) := NULL;

   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_GET_DEPT is
      select dept
        from item_master
       where item = I_item;

   cursor C_SUP_COUNTRY is
      select supplier,
             origin_country_id
        from item_supp_country
       where item = I_item;

   cursor C_GET_MIN_ISC_LOC is
      select MIN(isl.loc)
        from item_supp_country_loc isl
       where isl.item = I_item;

   cursor C_UPDATE_PRIM_LOC_IND is
      select 'x'
        from item_supp_country_loc isl
       where isl.loc               = L_min_loc
         and isl.item              = I_item
         and not exists (select 'x'
                           from item_supp_country_loc isl3
                          where isl3.item              = isl.item
                            and isl3.loc              != isl.loc
                            and isl3.supplier          = isl.supplier
                            and isl3.origin_country_id = isl.origin_country_id
                            and isl3.primary_loc_ind   = 'Y'
                            and rownum = 1)
          for update nowait;

   cursor C_PACK_TYPE is
      select NVL(pack_type,'N')
        from item_master
       where item = I_item;

   cursor C_VDATE is
     select vdate
       from period;
       
   cursor C_GET_PRIMARY_CTRY is
      select mva.country_id,
             NVL(default_po_cost, 'BC') default_po_cost
        from mv_loc_prim_addr mva,
             country_attrib ca
       where mva.loc = I_loc
         and mva.country_id = ca.country_id; 
   
   cursor C_GET_UNIT_COST(I_supplier ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,I_origin_country_id ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE) is
      select unit_cost
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id;

BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   ---
   if I_like_store IS NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      open C_GET_DEPT;
      SQL_LIB.SET_MARK('FETCH','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      fetch C_GET_DEPT into L_dept;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DEPT','ITEM_MASTER','Item: '||I_item);
      close C_GET_DEPT;
      ---
  
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);

      if L_system_options_rec.default_tax_type = 'GTAX' then

         --- Get the Address country id as delivery country id for item cost
         SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB','Loc: '||TO_CHAR(I_loc));
         open C_GET_PRIMARY_CTRY;
         SQL_LIB.SET_MARK('FETCH','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB','Loc: '||TO_CHAR(I_loc));
         fetch C_GET_PRIMARY_CTRY into L_dlvry_country_id, 
                                       L_default_po_cost;
         SQL_LIB.SET_MARK('OPEN','C_GET_PRIMARY_CTRY','MV_LOC_PRIM_ADDR,COUNTRY_ATTRIB','Loc: '||TO_CHAR(I_loc));
         close C_GET_PRIMARY_CTRY;         
         ---
         open C_VDATE;
         fetch C_VDATE into L_vdate;
         close C_VDATE;         
         ---
         FOR C_sup_ctry_rec in C_SUP_COUNTRY LOOP
            L_supplier           := C_sup_ctry_rec.supplier;
            L_origin_country_id  := C_sup_ctry_rec.origin_country_id;

            ---Get the NIC/BC/Static IND from GET_ITEM_COST_HEAD fun
            if ITEM_COST_SQL.GET_ITEM_COST_HEAD(O_error_message,
                                                L_item_cost_head_rec,
                                                I_item,
                                                L_supplier,
                                                L_origin_country_id,
                                                L_dlvry_country_id) = FALSE then
               return FALSE;
            end if;
            
            ---Assign the static ind, NIC and BC to local variables
          
            L_nic_static_ind       := NVL(L_item_cost_head_rec.nic_static_ind,'N');
            L_negotiated_item_cost := L_item_cost_head_rec.negotiated_item_cost;
            L_extended_base_cost   := L_item_cost_head_rec.extended_base_cost;
            L_inclusive_cost       := L_item_cost_head_rec.inclusive_cost;
            L_base_cost            := L_item_cost_head_rec.base_cost;
           
            if L_system_options_rec.default_tax_type = 'GTAX' and L_nic_static_ind = 'N' then
               L_negotiated_item_cost := NULL;
            end if;
            if L_system_options_rec.default_tax_type != 'GTAX' then
           
               L_nic_static_ind       := 'N';
               L_inclusive_cost       := NULL;
               L_negotiated_item_cost := NULL;
               L_extended_base_cost   := NULL;
               L_base_cost            := NULL;
            end if;
            
            -- Set item cost table indicators                       
            if L_system_options_rec.default_tax_type = 'GTAX' then  
               L_update_itemcost_ind := 'Y';                        
            else                                                    
               L_update_itemcost_ind := 'N';                        
            end if;
            
            if I_loc_type = 'S' then
               ---
               --- save item/sup/contry/loc info for cost update later
               L_comp_item_cost_rec := OBJ_COMP_ITEM_COST_REC(I_item,                  --I_item
                                                              L_nic_static_ind,        --I_nic_static_ind
                                                              L_supplier,              --I_supplier
                                                              I_loc,                   --I_location
                                                              I_loc_type,              --I_loc_type
                                                              L_vdate,                 --I_effective_date
                                                              'ITEMSUPPCTRYLOC',       --I_calling_form
                                                              L_origin_country_id,     --I_origin_country_id
                                                              L_dlvry_country_id,      --I_delivery_country_id
                                                              'N',                    --I_prim_dlvy_ctry_ind
                                                              L_update_itemcost_ind,   --I_update_itemcost_ind
                                                              NULL,                    --I_update_itemcost_child_ind
                                                              L_nic_static_ind,        --I_item_cost_tax_incl_ind, use nic_static_ind on ITEM_COST_HEAD
                                                              L_base_cost,             --O_base_cost
                                                              L_extended_base_cost,    --O_extended_base_cost
                                                              L_inclusive_cost,        --O_inclusive_cost
                                                              L_negotiated_item_cost,  --O_negotiated_item_cost
                                                              NULL,                    --svat_tax_rate
                                                              NULL,                    --tax_loc_type
                                                              NULL,                    --pack_ind
                                                              NULL,                    --pack_type
                                                              L_dept,                  --dept
                                                              NULL,                    --prim_supp_currency_code
                                                              NULL,                    --loc_prim_country
                                                              NULL,                    --loc_prim_country_tax_incl_ind
                                                              NULL,                    --gtax_total_tax_amount
                                                              NULL,                    --gtax_total_tax_amount_nic
                                                              NULL                     --gtax_total_recover_amount
                                                             );

               IO_comp_item_cost_tbl.EXTEND;
               IO_comp_item_cost_tbl(IO_comp_item_cost_tbl.COUNT) := L_comp_item_cost_rec;
               ---
               -- I_itemloc_ind = 'Y' are calls originating from the itemloc.fmb form
               if I_itemloc_ind != 'Y' then
                  insert into item_supp_country_loc(item,
                                                    supplier,
                                                    origin_country_id,
                                                    loc,
                                                    loc_type,
                                                    primary_loc_ind,
                                                    unit_cost,
                                                    round_lvl,
                                                    round_to_inner_pct,
                                                    round_to_case_pct,
                                                    round_to_layer_pct,
                                                    round_to_pallet_pct,
                                                    supp_hier_type_1,
                                                    supp_hier_lvl_1,
                                                    supp_hier_type_2,
                                                    supp_hier_lvl_2,
                                                    supp_hier_type_3,
                                                    supp_hier_lvl_3,
                                                    pickup_lead_time,
                                                    create_datetime,
                                                    last_update_datetime,
                                                    last_update_id,
                                                    negotiated_item_cost,
                                                    extended_base_cost,
                                                    inclusive_cost,
                                                    base_cost,
                                                    create_id)
                                             select I_item,
                                                    isc.supplier,
                                                    isc.origin_country_id,
                                                    I_loc,
                                                    I_loc_type,
                                                    'N',
                                                    DECODE(L_default_po_cost, 'BC', NVL(L_base_cost,isc.unit_cost),
                                                                              'NIC', NVL(L_negotiated_item_cost,isc.unit_cost)),
                                                    isc.round_lvl,
                                                    isc.round_to_inner_pct,
                                                    isc.round_to_case_pct,
                                                    isc.round_to_layer_pct,
                                                    isc.round_to_pallet_pct,
                                                    isc.supp_hier_type_1,
                                                    isc.supp_hier_lvl_1,
                                                    isc.supp_hier_type_2,
                                                    isc.supp_hier_lvl_2,
                                                    isc.supp_hier_type_3,
                                                    isc.supp_hier_lvl_3,
                                                    isc.pickup_lead_time,
                                                    sysdate,
                                                    sysdate,
                                                    get_user,
                                                    L_negotiated_item_cost,
                                                    L_extended_base_cost,
                                                    L_inclusive_cost,
                                                    L_base_cost,
                                                    get_user
                                                from sups s,
                                                     item_supp_country isc
                                               where isc.item = I_item
                                                 and isc.supplier = c_sup_ctry_rec.supplier
                                                 and isc.origin_country_id = c_sup_ctry_rec.origin_country_id
                                                 and s.supplier = isc.supplier
                                                 and (I_loc_type = 'S' or s.inv_mgmt_lvl in ('S', 'D'))
                                                 and not exists(select 'x'
                                                                  from item_supp_country_loc isl
                                                                 where isl.item              = I_item
                                                                   and isl.loc               = I_loc
                                                                   and isl.supplier          = isc.supplier
                                                                   and isl.origin_country_id = isc.origin_country_id
                                                                   and rownum = 1);
               end if;                                                 
            else -- I_loc_type != 'S'
               --- save item/sup/contry/loc info for cost update later
               L_comp_item_cost_rec := OBJ_COMP_ITEM_COST_REC(I_item,                  --I_item
                                                              L_nic_static_ind,        --I_nic_static_ind
                                                              L_supplier,              --I_supplier
                                                              I_loc,                   --I_location
                                                              I_loc_type,              --I_loc_type
                                                              L_vdate,                 --I_effective_date
                                                              'ITEMSUPPCTRYLOC',       --I_calling_form
                                                              L_origin_country_id,     --I_origin_country_id
                                                              L_dlvry_country_id,      --I_delivery_country_id
                                                              'N',                    --I_prim_dlvy_ctry_ind
                                                              L_update_itemcost_ind,  --I_update_itemcost_ind
                                                              NULL,                    --I_update_itemcost_child_ind
                                                              L_nic_static_ind,        --I_item_cost_tax_incl_ind, use nic_static_ind on ITEM_COST_HEAD
                                                              L_base_cost,             --O_base_cost
                                                              L_extended_base_cost,    --O_extended_base_cost
                                                              L_inclusive_cost,                    --O_inclusive_cost
                                                              L_negotiated_item_cost,  --O_negotiated_item_cost
                                                              NULL,                    --svat_tax_rate
                                                              NULL,                    --tax_loc_type
                                                              NULL,                    --pack_ind
                                                              NULL,                    --pack_type
                                                              L_dept,                  --dept
                                                              NULL,                    --prim_supp_currency_code
                                                              NULL,                    --loc_prim_country
                                                              NULL,                    --loc_prim_country_tax_incl_ind
                                                              NULL,                    --gtax_total_tax_amount
                                                              NULL,                    --gtax_total_tax_amount_nic
                                                              NULL                     --gtax_total_recover_amount
                                                             );

               IO_comp_item_cost_tbl.EXTEND;
               IO_comp_item_cost_tbl(IO_comp_item_cost_tbl.COUNT) := L_comp_item_cost_rec;
               ---
               -- I_itemloc_ind = 'Y' are calls originating from the itemloc.fmb form
               if I_itemloc_ind != 'Y' then
                  insert into item_supp_country_loc(item,
                                                    supplier,
                                                    origin_country_id,
                                                    loc,
                                                    loc_type,
                                                    primary_loc_ind,
                                                    unit_cost,
                                                    round_lvl,
                                                    round_to_inner_pct,
                                                    round_to_case_pct,
                                                    round_to_layer_pct,
                                                    round_to_pallet_pct,
                                                    supp_hier_type_1,
                                                    supp_hier_lvl_1,
                                                    supp_hier_type_2,
                                                    supp_hier_lvl_2,
                                                    supp_hier_type_3,
                                                    supp_hier_lvl_3,
                                                    pickup_lead_time,
                                                    create_datetime,
                                                    last_update_datetime,
                                                    last_update_id,
                                                    negotiated_item_cost,
                                                    extended_base_cost,
                                                    inclusive_cost,
                                                    base_cost,
                                                    create_id)
                                             select I_item,
                                                    isc.supplier,
                                                    isc.origin_country_id,
                                                    I_loc,
                                                    I_loc_type,
                                                    'N',
                                                    DECODE(L_default_po_cost, 'BC', NVL(L_base_cost,isc.unit_cost),
                                                                              'NIC', NVL(L_negotiated_item_cost,isc.unit_cost)),
                                                    sim.round_lvl,
                                                    sim.round_to_inner_pct,
                                                    sim.round_to_case_pct,
                                                    sim.round_to_layer_pct,
                                                    sim.round_to_pallet_pct,
                                                    isc.supp_hier_type_1,
                                                    isc.supp_hier_lvl_1,
                                                    isc.supp_hier_type_2,
                                                    isc.supp_hier_lvl_2,
                                                    isc.supp_hier_type_3,
                                                    isc.supp_hier_lvl_3,
                                                    isc.pickup_lead_time,
                                                    sysdate,
                                                    sysdate,
                                                    get_user,
                                                    L_negotiated_item_cost,
                                                    L_extended_base_cost,
                                                    L_inclusive_cost,
                                                    L_base_cost,
                                                    get_user
                                               from item_supp_country isc,
                                                    sups s,
                                                    sup_inv_mgmt sim,
                                                    (select store location,
                                                            store phy_loc
                                                       from store
                                                      where store = I_loc
                                                        and I_loc_type = 'S'
                                                      union all
                                                     select wh location,
                                                            physical_wh phy_loc
                                                       from wh
                                                      where wh = I_loc
                                                        and I_loc_type = 'W') loc
                                              where isc.item = I_item
                                                and isc.supplier = c_sup_ctry_rec.supplier
                                                and isc.origin_country_id = c_sup_ctry_rec.origin_country_id
                                                and s.supplier = isc.supplier
                                                and s.inv_mgmt_lvl = 'A'
                                                and sim.supplier = isc.supplier
                                                and sim.dept     = L_dept
                                                and sim.location = loc.phy_loc
                                                and not exists(select 'x'
                                                                 from item_supp_country_loc isl
                                                                where isl.item              = I_item
                                                                  and isl.loc               = I_loc
                                                                  and isl.supplier          = isc.supplier
                                                                  and isl.origin_country_id = isc.origin_country_id
                                                                  and rownum = 1)
                                             UNION ALL
                                             select I_item,
                                                    isc.supplier,
                                                    isc.origin_country_id,
                                                    I_loc,
                                                    I_loc_type,
                                                    'N',
                                                    DECODE(L_default_po_cost, 'BC', NVL(L_base_cost,isc.unit_cost),
                                                                              'NIC', NVL(L_negotiated_item_cost,isc.unit_cost)),
                                                    sim.round_lvl,
                                                    sim.round_to_inner_pct,
                                                    sim.round_to_case_pct,
                                                    sim.round_to_layer_pct,
                                                    sim.round_to_pallet_pct,
                                                    isc.supp_hier_type_1,
                                                    isc.supp_hier_lvl_1,
                                                    isc.supp_hier_type_2,
                                                    isc.supp_hier_lvl_2,
                                                    isc.supp_hier_type_3,
                                                    isc.supp_hier_lvl_3,
                                                    isc.pickup_lead_time,
                                                    sysdate,
                                                    sysdate,
                                                    get_user,
                                                    L_negotiated_item_cost,
                                                    L_extended_base_cost,
                                                    L_inclusive_cost,
                                                    L_base_cost,
                                                    get_user
                                               from item_supp_country isc,
                                                    sups s,
                                                    sup_inv_mgmt sim,
                                                    (select store location,
                                                            store phy_loc
                                                       from store
                                                      where store = I_loc
                                                        and I_loc_type = 'S'
                                                      union all
                                                     select wh location,
                                                            physical_wh phy_loc
                                                       from wh
                                                      where wh = I_loc
                                                        and I_loc_type = 'W') loc
                                              where isc.item = I_item
                                                and isc.supplier = c_sup_ctry_rec.supplier
                                                and isc.origin_country_id = c_sup_ctry_rec.origin_country_id
                                                and s.supplier = isc.supplier
                                                and s.inv_mgmt_lvl = 'L'
                                                and sim.supplier = isc.supplier
                                                and sim.dept is NULL
                                                and sim.location = loc.phy_loc
                                                and not exists(select 'x'
                                                                 from item_supp_country_loc isl
                                                                where isl.item              = I_item
                                                                  and isl.loc               = I_loc
                                                                  and isl.supplier          = isc.supplier
                                                                  and isl.origin_country_id = isc.origin_country_id
                                                                  and rownum = 1)
                                             UNION ALL
                                             select I_item,
                                                    isc.supplier,
                                                    isc.origin_country_id,
                                                    I_loc,
                                                    I_loc_type,
                                                    'N',
                                                    DECODE(L_default_po_cost, 'BC', NVL(L_base_cost,isc.unit_cost),
                                                                              'NIC', NVL(L_negotiated_item_cost,isc.unit_cost)),
                                                    isc.round_lvl,
                                                    isc.round_to_inner_pct,
                                                    isc.round_to_case_pct,
                                                    isc.round_to_layer_pct,
                                                    isc.round_to_pallet_pct,
                                                    isc.supp_hier_type_1,
                                                    isc.supp_hier_lvl_1,
                                                    isc.supp_hier_type_2,
                                                    isc.supp_hier_lvl_2,
                                                    isc.supp_hier_type_3,
                                                    isc.supp_hier_lvl_3,
                                                    isc.pickup_lead_time,
                                                    sysdate,
                                                    sysdate,
                                                    get_user,
                                                    L_negotiated_item_cost,
                                                    L_extended_base_cost,
                                                    L_inclusive_cost,
                                                    L_base_cost,
                                                    get_user
                                               from item_supp_country isc,
                                                    (select store location,
                                                            store phy_loc
                                                       from store
                                                      where store = I_loc
                                                        and I_loc_type = 'S'
                                                      union all
                                                     select wh location,
                                                            physical_wh phy_loc
                                                       from wh
                                                      where wh = I_loc
                                                        and I_loc_type = 'W') loc,
                                                    sups s
                                              where isc.item = I_item
                                                and isc.supplier = c_sup_ctry_rec.supplier
                                                and isc.origin_country_id = c_sup_ctry_rec.origin_country_id
                                                and s.supplier = isc.supplier
                                                and (  (s.inv_mgmt_lvl = 'A'
                                                        and not exists(select 'x'
                                                                         from sup_inv_mgmt sim2
                                                                         where sim2.supplier = isc.supplier
                                                                          and sim2.dept     = L_dept
                                                                          and sim2.location = loc.phy_loc
                                                                          and rownum = 1))
                                                    or (s.inv_mgmt_lvl = 'L'
                                                        and not exists(select 'x'
                                                                         from sup_inv_mgmt sim2
                                                                        where sim2.supplier = isc.supplier
                                                                          and sim2.location = loc.phy_loc
                                                                          and rownum = 1)))
                                                and not exists(select 'x'
                                                                 from item_supp_country_loc isl
                                                                where isl.item              = I_item
                                                                  and isl.loc               = I_loc
                                                                  and isl.supplier          = isc.supplier
                                                                  and isl.origin_country_id = isc.origin_country_id
                                                                  and rownum = 1)
                                             UNION ALL
                                             select I_item,
                                                    isc.supplier,
                                                    isc.origin_country_id,
                                                    I_loc,
                                                    I_loc_type,
                                                    'N',
                                                    DECODE(L_default_po_cost, 'BC', NVL(L_base_cost,isc.unit_cost),
                                                                              'NIC', NVL(L_negotiated_item_cost,isc.unit_cost)),
                                                    isc.round_lvl,
                                                    isc.round_to_inner_pct,
                                                    isc.round_to_case_pct,
                                                    isc.round_to_layer_pct,
                                                    isc.round_to_pallet_pct,
                                                    isc.supp_hier_type_1,
                                                    isc.supp_hier_lvl_1,
                                                    isc.supp_hier_type_2,
                                                    isc.supp_hier_lvl_2,
                                                    isc.supp_hier_type_3,
                                                    isc.supp_hier_lvl_3,
                                                    isc.pickup_lead_time,
                                                    sysdate,
                                                    sysdate,
                                                    get_user,
                                                    L_negotiated_item_cost,
                                                    L_extended_base_cost,
                                                    L_inclusive_cost,
                                                    L_base_cost,
                                                    get_user
                                               from item_supp_country isc,
                                                    sups s
                                              where isc.item = I_item
                                                and isc.supplier = c_sup_ctry_rec.supplier
                                                and isc.origin_country_id = c_sup_ctry_rec.origin_country_id
                                                and s.supplier = isc.supplier
                                                and (I_loc_type = 'E' or s.inv_mgmt_lvl in ('S', 'D'))
                                                and not exists(select 'x'
                                                                 from item_supp_country_loc isl
                                                                where isl.item              = I_item
                                                                  and isl.loc               = I_loc
                                                                  and isl.supplier          = isc.supplier
                                                                  and isl.origin_country_id = isc.origin_country_id);
                                                               
               end if; -- I_itemloc_ind != 'Y'
            end if;
         end LOOP; -- FOR C_sup_ctry_rec in C_SUP_COUNTRY
      ---
      else   ---else the default_tax_type is OTHERS
         
         if I_loc_type = 'S' then
             ---
            if I_wf_ind = 'Y' then --- this is a wholesale/franchise store use the source wh unit cost
               L_unit_cost := I_wh_unit_cost;
            end if;

            insert into item_supp_country_loc(item,
                                              supplier,
                                              origin_country_id,
                                              loc,
                                              loc_type,
                                              primary_loc_ind,
                                              unit_cost,
                                              round_lvl,
                                              round_to_inner_pct,
                                              round_to_case_pct,
                                              round_to_layer_pct,
                                              round_to_pallet_pct,
                                              supp_hier_type_1,
                                              supp_hier_lvl_1,
                                              supp_hier_type_2,
                                              supp_hier_lvl_2,
                                              supp_hier_type_3,
                                              supp_hier_lvl_3,
                                              pickup_lead_time,
                                              create_datetime,
                                              last_update_datetime,
                                              last_update_id,
                                              negotiated_item_cost,
                                              extended_base_cost,
                                              inclusive_cost,
                                              base_cost,
                                              create_id)
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              NVL(L_unit_cost,isc.unit_cost),
                                              isc.round_lvl,
                                              isc.round_to_inner_pct,
                                              isc.round_to_case_pct,
                                              isc.round_to_layer_pct,
                                              isc.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              get_user
                                         from sups s,
                                              item_supp_country isc
                                        where isc.item = I_item
                                          and s.supplier = isc.supplier
                                          and (I_loc_type = 'S' or s.inv_mgmt_lvl in ('S', 'D'))
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id
                                                            and rownum = 1);
         else -- I_loc_type != 'S'
            insert into item_supp_country_loc(item,
                                              supplier,
                                              origin_country_id,
                                              loc,
                                              loc_type,
                                              primary_loc_ind,
                                              unit_cost,
                                              round_lvl,
                                              round_to_inner_pct,
                                              round_to_case_pct,
                                              round_to_layer_pct,
                                              round_to_pallet_pct,
                                              supp_hier_type_1,
                                              supp_hier_lvl_1,
                                              supp_hier_type_2,
                                              supp_hier_lvl_2,
                                              supp_hier_type_3,
                                              supp_hier_lvl_3,
                                              pickup_lead_time,
                                              create_datetime,
                                              last_update_datetime,
                                              last_update_id,
                                              negotiated_item_cost,
                                              extended_base_cost,
                                              inclusive_cost,
                                              base_cost,
                                              create_id)
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              isc.unit_cost,
                                              sim.round_lvl,
                                              sim.round_to_inner_pct,
                                              sim.round_to_case_pct,
                                              sim.round_to_layer_pct,
                                              sim.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              get_user
                                         from item_supp_country isc,
                                              sups s,
                                              sup_inv_mgmt sim,
                                              (select store location,
                                                      store phy_loc
                                                 from store
                                                where store = I_loc
                                                  and I_loc_type = 'S'
                                                union all
                                               select wh location,
                                                      physical_wh phy_loc
                                                 from wh
                                                where wh = I_loc
                                                  and I_loc_type = 'W') loc
                                        where isc.item = I_item
                                          and s.supplier = isc.supplier
                                          and s.inv_mgmt_lvl = 'A'
                                          and sim.supplier = isc.supplier
                                          and sim.dept     = L_dept
                                          and sim.location = loc.phy_loc
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id
                                                            and rownum = 1)
                                       UNION ALL
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              isc.unit_cost,
                                              sim.round_lvl,
                                              sim.round_to_inner_pct,
                                              sim.round_to_case_pct,
                                              sim.round_to_layer_pct,
                                              sim.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              get_user
                                         from item_supp_country isc,
                                              sups s,
                                              sup_inv_mgmt sim,
                                              (select store location,
                                                      store phy_loc
                                                 from store
                                                where store = I_loc
                                                  and I_loc_type = 'S'
                                                union all
                                               select wh location,
                                                      physical_wh phy_loc
                                                 from wh
                                                where wh = I_loc
                                                  and I_loc_type = 'W') loc
                                        where isc.item = I_item
                                          and s.supplier = isc.supplier
                                          and s.inv_mgmt_lvl = 'L'
                                          and sim.supplier = isc.supplier
                                          and sim.dept is NULL
                                          and sim.location = loc.phy_loc
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id
                                                            and rownum = 1)
                                       UNION ALL
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              isc.unit_cost,
                                              isc.round_lvl,
                                              isc.round_to_inner_pct,
                                              isc.round_to_case_pct,
                                              isc.round_to_layer_pct,
                                              isc.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              get_user
                                         from item_supp_country isc,
                                              (select store location,
                                                      store phy_loc
                                                 from store
                                                where store = I_loc
                                                  and I_loc_type = 'S'
                                                union all
                                               select wh location,
                                                      physical_wh phy_loc
                                                 from wh
                                                where wh = I_loc
                                                  and I_loc_type = 'W') loc,
                                              sups s
                                        where isc.item = I_item
                                          and s.supplier = isc.supplier
                                          and (  (s.inv_mgmt_lvl = 'A'
                                                  and not exists(select 'x'
                                                                   from sup_inv_mgmt sim2
                                                                   where sim2.supplier = isc.supplier
                                                                    and sim2.dept     = L_dept
                                                                    and sim2.location = loc.phy_loc
                                                                    and rownum = 1))
                                              or (s.inv_mgmt_lvl = 'L'
                                                  and not exists(select 'x'
                                                                   from sup_inv_mgmt sim2
                                                                  where sim2.supplier = isc.supplier
                                                                    and sim2.location = loc.phy_loc
                                                                    and rownum = 1)))
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id
                                                            and rownum = 1)
                                       UNION ALL
                                       select I_item,
                                              isc.supplier,
                                              isc.origin_country_id,
                                              I_loc,
                                              I_loc_type,
                                              'N',
                                              isc.unit_cost,
                                              isc.round_lvl,
                                              isc.round_to_inner_pct,
                                              isc.round_to_case_pct,
                                              isc.round_to_layer_pct,
                                              isc.round_to_pallet_pct,
                                              isc.supp_hier_type_1,
                                              isc.supp_hier_lvl_1,
                                              isc.supp_hier_type_2,
                                              isc.supp_hier_lvl_2,
                                              isc.supp_hier_type_3,
                                              isc.supp_hier_lvl_3,
                                              isc.pickup_lead_time,
                                              sysdate,
                                              sysdate,
                                              get_user,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              get_user
                                         from item_supp_country isc,
                                              sups s
                                        where isc.item = I_item
                                          and s.supplier = isc.supplier
                                          and (I_loc_type = 'E' or s.inv_mgmt_lvl in ('S', 'D'))
                                          and not exists(select 'x'
                                                           from item_supp_country_loc isl
                                                          where isl.item              = I_item
                                                            and isl.loc               = I_loc
                                                            and isl.supplier          = isc.supplier
                                                            and isl.origin_country_id = isc.origin_country_id);
        end if;
      end if;

      -- I_itemloc_ind = 'Y' are calls originating from the itemloc.fmb form
      -- primary_loc_ind is already set when the call originates from item_loc.fmb for GTAX tax type. 
      -- Update the primary_loc_ind for other conditions. 
      
      if I_itemloc_ind != 'Y' or (I_itemloc_ind = 'Y' and L_system_options_rec.default_tax_type != 'GTAX') then
         -- The minimum lowest location number will be selected to be the primary location on mass inserts.
         ---
         L_update := 'Y';
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
         open C_GET_MIN_ISC_LOC;
         SQL_LIB.SET_MARK('FETCH','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
         fetch C_GET_MIN_ISC_LOC into L_min_loc;
         ---
         if C_GET_MIN_ISC_LOC%NOTFOUND then
            L_update := 'N';
         end if;
         ---
         SQL_LIB.SET_MARK('CLOSE','C_GET_MIN_ISC_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
         close C_GET_MIN_ISC_LOC;
         ---
         if L_update = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_UPDATE_PRIM_LOC_IND','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
            open C_UPDATE_PRIM_LOC_IND;
            SQL_LIB.SET_MARK('CLOSE','C_UPDATE_PRIM_LOC_IND','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
            close C_UPDATE_PRIM_LOC_IND;
            ---
            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
            update item_supp_country_loc isl
               set primary_loc_ind      = 'Y',
                   last_update_datetime = sysdate,
                   last_update_id       = get_user
             where isl.item              = I_item
               and isl.loc               = L_min_loc
               and not exists (select 'x'
                                 from item_supp_country_loc isl3
                                where isl3.item              = isl.item
                                  and isl3.loc              != isl.loc
                                  and isl3.supplier          = isl.supplier
                                  and isl3.origin_country_id = isl.origin_country_id
                                  and isl3.primary_loc_ind   = 'Y'
                                  and rownum = 1);
         end if;
      end if;
      -- I_itemloc_ind = 'Y' are calls originating from the itemloc.fmb form
      if I_itemloc_ind != 'Y' then
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_PACK_TYPE','ITEM_MASTER','Item: '||I_item);
         open C_PACK_TYPE;
         SQL_LIB.SET_MARK('FETCH', 'C_PACK_TYPE','ITEM_MASTER','Item: '||I_item);
         fetch C_PACK_TYPE into L_pack_type;
         SQL_LIB.SET_MARK('CLOSE','C_PACK_TYPE','ITEM_MASTER','Item: '||I_item);
         close C_PACK_TYPE;
         ---
         if L_pack_type = 'B' then
            if L_system_options_rec.default_tax_type = 'GTAX' then
               SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','ITEM: '||I_item);
               update item_supp_country_loc isc1
                  set isc1.unit_cost           = (select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                                   from item_supp_country_loc isc2,
                                                        v_packsku_qty vpq
                                                  where vpq.pack_no            = I_item
                                                    and isc2.item              = vpq.item
                                                    and isc2.supplier          = isc1.supplier
                                                    and isc2.origin_country_id = isc1.origin_country_id
                                                    and isc2.loc               = isc1.loc),
                      isc1.last_update_id       = get_user,
                      isc1.last_update_datetime = sysdate
                where isc1.item = I_item
                  and exists (select 'x'
                                from item_supp_country_loc isc3,
                                     v_packsku_qty vpq
                               where vpq.pack_no            = I_item
                                 and isc3.item              = vpq.item
                                 and isc3.supplier          = isc1.supplier
                                 and isc3.origin_country_id = isc1.origin_country_id
                                 and isc3.loc               = isc1.loc
                                 and rownum                 = 1);
            else -- L_system_options_rec.default_tax_type NOT in ('GTAX')
               SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC','ITEM: '||I_item);
               update item_supp_country_loc isc1
                  set isc1.unit_cost            = (select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                                     from item_supp_country_loc isc2,
                                                          v_packsku_qty vpq
                                                    where vpq.pack_no            = I_item
                                                      and isc2.item              = vpq.item
                                                      and isc2.supplier          = isc1.supplier
                                                      and isc2.origin_country_id = isc1.origin_country_id
                                                      and isc2.loc               = isc1.loc),
                      isc1.last_update_id       = get_user,
                      isc1.last_update_datetime = sysdate,
                      isc1.negotiated_item_cost = NULL,
                      isc1.extended_base_cost = NULL,
                      isc1.inclusive_cost = NULL,
                      isc1.base_cost = NULL
                where isc1.item = I_item
                  and exists (select 'x'
                                from item_supp_country_loc isc3,
                                     v_packsku_qty vpq
                               where vpq.pack_no            = I_item
                                 and isc3.item              = vpq.item
                                 and isc3.supplier          = isc1.supplier
                                 and isc3.origin_country_id = isc1.origin_country_id
                                 and isc3.loc               = isc1.loc
                                 and rownum = 1);
         end if;
      end if;
      ---
      if I_loc is NOT NULL then
         L_all_locs := 'N';
      else
         L_all_locs := 'Y';
      end if;
      ---
      if L_system_options_rec.bracket_costing_ind = 'Y' then
       if ITEM_BRACKET_COST_SQL.CREATE_BRACKET(O_error_message,
                                                I_item,
                                                NULL, --supplier
                                                NULL, --origin_country_id
                                                I_loc,
                                                L_all_locs) = FALSE then
           return FALSE;
        end if;
     end if;
     ---
      end if;
   else  -- I_like_store IS NOT NULL --
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item);
      insert into item_supp_country_loc(item,
                                        supplier,
                                        origin_country_id,
                                        loc,
                                        loc_type,
                                        primary_loc_ind,
                                        unit_cost,
                                        round_lvl,
                                        round_to_inner_pct,
                                        round_to_case_pct,
                                        round_to_layer_pct,
                                        round_to_pallet_pct,
                                        supp_hier_type_1,
                                        supp_hier_lvl_1,
                                        supp_hier_type_2,
                                        supp_hier_lvl_2,
                                        supp_hier_type_3,
                                        supp_hier_lvl_3,
                                        pickup_lead_time,
                                        create_datetime,
                                        last_update_datetime,
                                        last_update_id,
                                        negotiated_item_cost,
                                        extended_base_cost,
                                        inclusive_cost,
                                        base_cost,
                                        create_id)
                                 select I_item,
                                        iscl.supplier,
                                        iscl.origin_country_id,
                                        I_loc,
                                        iscl.loc_type,
                                        'N',
                                        iscl.unit_cost,
                                        iscl.round_lvl,
                                        iscl.round_to_inner_pct,
                                        iscl.round_to_case_pct,
                                        iscl.round_to_layer_pct,
                                        iscl.round_to_pallet_pct,
                                        iscl.supp_hier_type_1,
                                        iscl.supp_hier_lvl_1,
                                        iscl.supp_hier_type_2,
                                        iscl.supp_hier_lvl_2,
                                        iscl.supp_hier_type_3,
                                        iscl.supp_hier_lvl_3,
                                        iscl.pickup_lead_time,
                                        sysdate,
                                        sysdate,
                                        get_user,
                                        iscl.negotiated_item_cost,
                                        iscl.extended_base_cost,
                                        iscl.inclusive_cost,
                                        iscl.base_cost,
                                        get_user
                                   from item_supp_country_loc iscl
                                  where iscl.item = I_item
                                    and iscl.loc = I_like_store
                                    and exists (select 'x'
                                                  from item_supp_country_loc iscl2
                                                 where iscl2.origin_country_id = iscl.origin_country_id
                                                   and iscl2.item = iscl.item
                                                   and iscl2.supplier = iscl.supplier
                                                   and iscl2.loc != I_loc
                                                   and iscl2.loc = iscl.loc);
     ---
      if L_system_options_rec.bracket_costing_ind = 'Y' then
       if ITEM_BRACKET_COST_SQL.CREATE_BRACKET(O_error_message,
                                                I_item,
                                                NULL, --supplier
                                                NULL, --origin_country_id
                                                I_loc,
                                                'N') = FALSE then
           return FALSE;
        end if;
      end if;
     ---
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      -- if the record is locked skip the update because the item_supp_country_loc.primary_loc_ind
      -- is being updated by another thread for the same item/minimum location
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_LOCATION;
-------------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_ITEM_SUPP_COUNTRY_LOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item          IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                                    I_supplier      IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                                    I_country       IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                                    I_loc           IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_SQL.LOCK_ITEM_SUPP_COUNTRY_LOC';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   CURSOR C_LOCK_ISCL IS
      select 'x'
        from ITEM_SUPP_COUNTRY_LOC
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_country
         and loc = I_loc
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ISCL', 'ITEM_SUPP_COUNTRY_LOC','item: '||I_item||
                                                    ' supplier: '||I_supplier||
                                                    ' country: '||I_country||
                                                    ' loc: '||I_loc);

   open C_LOCK_ISCL;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ISCL', 'ITEM_SUPP_COUNTRY_LOC','item: '||I_item||
                                                    ' supplier: '||I_supplier||
                                                    ' country: '||I_country||
                                                    ' loc: '||I_loc);

   close C_LOCK_ISCL;
   ---
   return TRUE;
EXCEPTION

   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_COUNTRY_LOC',
                                             'Item :'||I_item,
                                             'Supplier :'||I_supplier);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_ITEM_SUPP_COUNTRY_LOC;
-----------------------------------------------------------------------------------------------
FUNCTION GET_ROW(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_exists          IN OUT   BOOLEAN,
                 O_iscl_rec        IN OUT   ITEM_SUPP_COUNTRY_LOC%ROWTYPE,
                 I_item            IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                 I_supplier        IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                 I_country         IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                 I_location        IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.GET_ROW';
   cursor C_GET_ISCL_REC is
      select *
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_country
         and loc               = I_location;
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   if I_country is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_country',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   O_exists := NULL;
   SQL_LIB.SET_MARK('OPEN','C_GET_ISCL_REC','ITEM_SUPP_COUNTRY_LOC','item: '||I_item||
                                                                    ' supplier: '||to_char(I_supplier)||
                                                                    ' country: '||I_country||
                                                                    ' loc: '||to_char(I_location));
   open C_GET_ISCL_REC;
   SQL_LIB.SET_MARK('FETCH','C_GET_ISCL_REC','ITEM_SUPP_COUNTRY_LOC','item: '||I_item||
                                                                    ' supplier: '||to_char(I_supplier)||
                                                                    ' country: '||I_country||
                                                                    ' loc: '||to_char(I_location));
   fetch C_GET_ISCL_REC INTO O_iscl_rec;
   SQL_LIB.SET_MARK('CLOSE','C_GET_ISCL_REC','ITEM_SUPP_COUNTRY_LOC','item: '||I_item||
                                                                    ' supplier: '||to_char(I_supplier)||
                                                                    ' country: '||I_country||
                                                                    ' loc: '||to_char(I_location));
   close C_GET_ISCL_REC;
   ---

   O_exists := (O_iscl_rec.item is NOT NULL);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_ROW;
----------------------------------------------------------------------------------------------

FUNCTION GET_LAST_RECEIPT_COST(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_last_receipt_cost   IN OUT  SHIPSKU.UNIT_COST%TYPE,
                               I_item                IN      SHIPSKU.ITEM%TYPE,
                               I_supplier            IN      ORDHEAD.SUPPLIER%TYPE,
                               I_location            IN      SHIPMENT.TO_LOC%TYPE)
   RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.GET_LAST_RECEIPT_COST';
  L_order_no  ORDHEAD.ORDER_NO%TYPE := NULL;
   L_loc_type  SHIPMENT.TO_LOC_TYPE%TYPE := NULL;
   L_last_receipt_cost  SHIPSKU.UNIT_COST%TYPE := NULL;
   L_ord_curr  ORDHEAD.CURRENCY_CODE%TYPE := NULL;
   L_sups_curr SUPS.CURRENCY_CODE%TYPE := NULL;
 
   cursor C_GET_LAST_RECEIPT_COST is
      select max(sk.unit_cost),
             oh.order_no,
             oh.currency_code,
             sh.to_loc_type
        from shipment sh,
             shipsku  sk,
             ordhead  oh
       where oh.order_no       = sh.order_no
         and oh.supplier       = I_supplier
         and sh.shipment       = sk.shipment
         and sh.to_loc         = I_location
         and sk.item           = I_item
         and sk.unit_cost      > 0
         and to_char(sh.receive_date,'YYYYMMDD') = ( select max(to_char(sh1.receive_date,'YYYYMMDD'))
                                                       from shipment sh1,
                                                            shipsku  sk1,
                                                            ordhead  oh1
                                                      where oh1.order_no       = sh1.order_no
                                                        and oh1.supplier       = I_supplier
                                                        and sh1.shipment       = sk1.shipment
                                                        and sh1.to_loc         = I_location
                                                        and sk1.item           = I_item
                                                       and sk1.unit_cost      > 0)
            group by oh.order_no, oh.currency_code, sh.to_loc_type
            order by 1 desc;                                             
BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   O_last_receipt_cost := NULL;
   --Get Supplier Currency
   if CURRENCY_SQL.GET_CURR_LOC(O_error_message,
                                I_supplier,
                                'V',
                                NULL,
                                L_sups_curr) = FALSE then
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_LAST_RECEIPT_COST',
                    'ORDHEAD,SHIPMENT,SHIPSKU',
                    'Item '||I_item||' Supplier '||I_supplier||' Location '||I_location);
   open C_GET_LAST_RECEIPT_COST;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_LAST_RECEIPT_COST',
                    'ORDHEAD,SHIPMENT,SHIPSKU',
                    'Item '||I_item||' Supplier '||I_supplier||' Location '||I_location);
   fetch C_GET_LAST_RECEIPT_COST INTO L_last_receipt_cost, L_order_no, L_ord_curr, L_loc_type;
   
   if C_GET_LAST_RECEIPT_COST%FOUND then
      if L_ord_curr<>L_sups_curr then
         --Convert Last Receipt Cost from Order Currency to Supplier Currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_order_no,
                                             'O',
                                             NULL,
                                             I_supplier,
                                             'V',
                                             NULL,
                                             L_last_receipt_cost,
                                             O_last_receipt_cost,
                                             'C',
                                             NULL,
                                             NULL) = FALSE THEN
            return false;
         end if;
      else
         O_last_receipt_cost := L_last_receipt_cost;
      end if;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_LAST_RECEIPT_COST',
                    'ORDHEAD,SHIPMENT,SHIPSKU',
                    'Item '||I_item||' Supplier '||I_supplier||' Location '||I_location);
   close C_GET_LAST_RECEIPT_COST;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_LAST_RECEIPT_COST;
----------------------------------------------------------------------------------------------
FUNCTION ITEMLOC_SUPP_FILTER_LIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_diff_ind        IN OUT VARCHAR2,
                                  I_item            IN     ITEM_LOC.ITEM%TYPE,
                                  I_group_type      IN     CODE_DETAIL.CODE%TYPE,
                                  I_group_id        IN     PARTNER.PARTNER_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ITEM_VALIDATE_SQL.ITEMLOC_FILTER_LIST';
   L_total_itemloc   NUMBER(6) := 0;
   L_access_itemloc  NUMBER(6) := 0;

   cursor C_total_itemloc is
      select count(1)
        from item_loc
       where item = I_item;

   cursor C_access_itemloc is
      select count(1)
        from item_loc
       where item = I_item
         and (primary_supp is NULL or primary_supp in (select supplier from v_sups))
         and (exists (select 'x'
                        from v_store
                       where store = loc
                         and rownum = 1)
              or exists (select 'x'
                           from v_wh
                          where wh = loc
                            and stockholding_ind = 'Y'
                            and rownum = 1)
              or exists (select 'x'
                           from v_internal_finisher
                          where finisher_id = loc
                            and rownum = 1)
              or exists (select 'x'
                           from v_external_finisher
                          where finisher_id = loc
                            and rownum = 1));

   cursor C_total_item_st is
      select count(1)
        from item_loc il,
             store s,
             district d,
             region r
       where il.loc = s.store
         and d.district = s.district
         and d.region = r.region
         and il.item = I_item
         and ((I_group_type = 'A'
               and r.area = I_group_id)
              or (I_group_type = 'R'
                  and d.region = I_group_id)
              or (I_group_type = 'D'
                  and s.district = I_group_id)
              or I_group_type = 'AS'
              or (I_group_type = 'C'
                  and s.store_class = I_group_id)
              or (I_group_type = 'T'
                  and s.transfer_zone = I_group_id));

   cursor C_access_item_st is
      select count(1)
        from item_loc il,
             v_store vs
       where il.item = I_item
         and il.loc_type = 'S'
         and il.loc = vs.store
         and (il.primary_supp is NULL or il.primary_supp in (select supplier from v_sups))
         and ((I_group_type = 'R'
               and vs.region = I_group_id)
              or (I_group_type = 'A'
                  and vs.area = I_group_id)
              or (I_group_type = 'D'
                  and vs.district = I_group_id)
              or I_group_type = 'AS'
              or (I_group_type = 'C'
                  and vs.store_class = I_group_id)
              or (I_group_type = 'DW'
                  and vs.default_wh = I_group_id)
              or (I_group_type = 'S'
                  and il.loc = I_group_id)
              or (I_group_type = 'T'
                  and vs.transfer_zone = I_group_id));

   cursor C_total_lls is
      select count(1)
        from item_loc il,
             loc_list_detail lld
       where il.loc = lld.location
         and lld.loc_type = 'S'
         and il.item = I_item
         and lld.loc_list = I_group_id;

   cursor C_access_lls is
      select count(1)
        from item_loc il,
             loc_list_detail lld,
             v_store vs
       where lld.loc_type = 'S'
         and il.loc = lld.location
         and il.loc = vs.store
         and il.item = I_item
         and lld.loc_list = I_group_id
         and (il.primary_supp is NULL or il.primary_supp in (select supplier from v_sups));

   cursor C_total_llw is
      select count(1)
        from item_loc il,
             loc_list_detail lld,
             wh
       where il.loc = lld.location
         and lld.location = wh.wh
         and wh.stockholding_ind = 'Y'
         and il.item = I_item
         and lld.loc_list = I_group_id;

   cursor C_access_llw is
      select count(1)
        from item_loc il,
             loc_list_detail lld,
             v_wh vwh
       where il.loc = lld.location
         and il.loc = vwh.wh
         and vwh.stockholding_ind = 'Y'
         and il.item = I_item
         and lld.loc_list = I_group_id
         and (il.primary_supp is NULL or il.primary_supp in (select supplier from v_sups));

   cursor C_total_wh is
      select count(1)
        from item_loc il,
             wh
       where il.loc = wh.wh
         and il.item = I_item
         and wh.finisher_ind = 'N'
         and wh.stockholding_ind = 'Y'
         and (I_group_type = 'AW'
              or (I_group_type = 'W'
                  and il.loc = I_group_id));

   cursor C_access_wh is
      select count(1)
        from item_loc il,
             v_wh wh
       where il.loc = wh.wh
         and il.item = I_item
         and wh.stockholding_ind = 'Y'
         and (I_group_type = 'AW'
              or (I_group_type = 'W'
                  and il.loc = I_group_id))
         and (il.primary_supp is NULL or il.primary_supp in (select supplier from v_sups));

   cursor C_total_int_finisher is
      select count(1)
        from item_loc il,
             wh
       where il.loc = wh.wh
         and il.item = I_item
         and wh.finisher_ind = 'Y'
         and wh.stockholding_ind = 'Y'
         and (I_group_type = 'AI'
              or (I_group_type = 'I'
                  and il.loc = I_group_id));

   cursor C_access_int_finisher is
      select count(1)
        from item_loc il,
             v_internal_finisher vif
       where il.loc = vif.finisher_id
         and il.item = I_item
         and (I_group_type = 'AI'
              or (I_group_type = 'I'
                  and il.loc = I_group_id))
         and (il.primary_supp is NULL or il.primary_supp in (select supplier from v_sups));

   cursor C_total_ext_finisher is
      select count(1)
        from item_loc il,
             partner p
       where il.loc = p.partner_id
         and p.partner_type = 'E'
         and il.item = I_item
         and (I_group_type = 'AE'
              or (I_group_type = 'E'
                  and il.loc = I_group_id));

   cursor C_access_ext_finisher is
      select count(1)
        from item_loc il,
             v_external_finisher vef
       where il.loc = vef.finisher_id
         and il.loc_type = 'E'
         and il.item = I_item
         and (il.primary_supp is NULL or il.primary_supp in (select supplier from v_sups))
         and (I_group_type = 'AE'
              or (I_group_type = 'E'
                  and il.loc = I_group_id));

   cursor C_total_pw is
      select count(1)
        from item_loc il,
             wh
       where il.loc = wh.wh
         and wh.stockholding_ind = 'Y'
         and wh.finisher_ind = 'N'
         and wh.physical_wh = I_group_id
         and il.item = I_item;

   cursor C_access_pw is
      select count(1) cnt
        from item_loc il,
             v_wh wh
       where il.loc = wh.wh
         and wh.stockholding_ind = 'Y'
         and il.item = I_item
         and wh.physical_wh = I_group_id
         and (il.primary_supp is NULL or il.primary_supp in (select supplier from v_sups));

   cursor C_total_loc_traits is
      select count(1)
        from item_loc il,
             loc_traits_matrix ltm
       where il.loc = ltm.store
         and il.item = I_item
         and ltm.loc_trait = I_group_id;

   cursor C_access_loc_traits is
      select count(1)
        from item_loc il,
             v_loc_traits vlt,
             loc_traits_matrix ltm,
             v_store vs
       where il.loc = ltm.store
         and ltm.store = vs.store
         and vlt.loc_trait = ltm.loc_trait
         and il.item = I_item
         and ltm.loc_trait = I_group_id
         and (il.primary_supp is NULL or il.primary_supp in (select supplier from v_sups));

   cursor C_total_item_st_s is
      select count(1)
        from item_loc il,
             store s
       where il.loc = s.store
         and il.item = I_item
         and il.loc = I_group_id;

   cursor C_total_item_st_w is
   select count(1)
     from item_loc il,
          store s
    where il.loc = s.store
      and il.item = I_item
      and s.default_wh = I_group_id;
BEGIN
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_group_type = 'AL' or I_group_type is NULL then
      ---
      SQL_LIB.SET_MARK('OPEN','C_TOTAL_ITEMLOC','ITEM_LOC','Item :'||I_item);  
      open C_TOTAL_ITEMLOC;
      SQL_LIB.SET_MARK('FETCH','C_TOTAL_ITEMLOC','ITEM_LOC','Item :'||I_item);
      fetch C_TOTAL_ITEMLOC into L_total_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_TOTAL_ITEMLOC','ITEM_LOC','Item :'||I_item);      
      close C_TOTAL_ITEMLOC;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_ITEMLOC','ITEM_LOC','Item :'||I_item);      
      open C_ACCESS_ITEMLOC;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_ITEMLOC','ITEM_LOC','Item :'||I_item);      
      fetch C_ACCESS_ITEMLOC into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_ITEMLOC','ITEM_LOC','Item :'||I_item);
      close C_ACCESS_ITEMLOC;
      ---
   elsif I_group_type in ('A','R','D','AS', 'C', 'DW', 'S', 'T', 'P') then
      ---
      if I_group_type = 'S' then
         ---
         SQL_LIB.SET_MARK('OPEN','C_TOTAL_ITEM_ST_S','ITEM_LOC','Item :'||I_item||' Loc :'||I_group_id);           
         open C_TOTAL_ITEM_ST_S;
         SQL_LIB.SET_MARK('FETCH','C_TOTAL_ITEM_ST_S','ITEM_LOC','Item :'||I_item||' Loc :'||I_group_id);           
         fetch C_TOTAL_ITEM_ST_S into L_total_itemloc;
         SQL_LIB.SET_MARK('CLOSE','C_TOTAL_ITEM_ST_S','ITEM_LOC','Item :'||I_item||' Loc :'||I_group_id);           
         close C_TOTAL_ITEM_ST_S;
         ---
      elsif I_group_type = 'DW' then
         ---
         SQL_LIB.SET_MARK('OPEN','C_TOTAL_ITEM_ST_W','ITEM_LOC,STORE','Item :'||I_item||' Default WH :'||I_group_id);
         open C_TOTAL_ITEM_ST_W;
         SQL_LIB.SET_MARK('FETCH','C_TOTAL_ITEM_ST_W','ITEM_LOC,STORE','Item :'||I_item||' Default WH :'||I_group_id);                                                 
         fetch C_TOTAL_ITEM_ST_W into L_total_itemloc;
         SQL_LIB.SET_MARK('CLOSE','C_TOTAL_ITEM_ST_W','ITEM_LOC,STORE','Item :'||I_item||' Default WH :'||I_group_id);               
         close C_TOTAL_ITEM_ST_W;
         ---
      else
         SQL_LIB.SET_MARK('OPEN','C_TOTAL_ITEM_ST','ITEM_LOC,STORE,DISTRICT,REGION','Item :'||I_item||' Group :'||I_group_id);               
         open C_TOTAL_ITEM_ST;
         SQL_LIB.SET_MARK('FETCH','C_TOTAL_ITEM_ST','ITEM_LOC,STORE,DISTRICT,REGION','Item :'||I_item||' Group :'||I_group_id);
         fetch C_TOTAL_ITEM_ST into L_total_itemloc;
         SQL_LIB.SET_MARK('CLOSE','C_TOTAL_ITEM_ST','ITEM_LOC,STORE,DISTRICT,REGION','Item :'||I_item||' Group :'||I_group_id);
         close C_TOTAL_ITEM_ST;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_ITEM_ST','ITEM_LOC,V_STORE','Item :'||I_item||' group :'||I_group_id);
      open C_ACCESS_ITEM_ST;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_ITEM_ST','ITEM_LOC,V_STORE','Item :'||I_item||' group :'||I_group_id);
      fetch C_ACCESS_ITEM_ST into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_ITEM_ST','ITEM_LOC,V_STORE','Item :'||I_item||' group :'||I_group_id);
      close C_ACCESS_ITEM_ST;
      ---
   elsif I_group_type = 'PW' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_TOTAL_PW','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);     
      open C_TOTAL_PW;
      SQL_LIB.SET_MARK('FETCH','C_TOTAL_PW','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);
      fetch C_TOTAL_PW into L_total_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_TOTAL_PW','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);      
      close C_TOTAL_PW;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_PW','ITEM_LOC,V_WH','Item :'||I_item||' Loc :'||I_group_id);
      open C_ACCESS_PW;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_PW','ITEM_LOC,V_WH','Item :'||I_item||' Loc :'||I_group_id);
      fetch C_ACCESS_PW into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_PW','ITEM_LOC,V_WH','Item :'||I_item||' Loc :'||I_group_id);
      close C_ACCESS_PW;
      ---
   elsif I_group_type = 'LLS' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_TOTAL_LLS','ITEM_LOC,LOC_LIST_DETAIL','Item :'||I_item||' Loc List :'||I_group_id);
      open C_TOTAL_LLS;
      SQL_LIB.SET_MARK('FETCH','C_TOTAL_LLS','ITEM_LOC,LOC_LIST_DETAIL','Item :'||I_item||' Loc List :'||I_group_id);
      fetch C_TOTAL_LLS into L_total_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_TOTAL_LLS','ITEM_LOC,LOC_LIST_DETAIL','Item :'||I_item||' Loc List :'||I_group_id);      
      close C_TOTAL_LLS;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_LLS','ITEM_LOC,LOC_LIST_DETAIL,V_STORE','Item :'||I_item||' Loc List :'||I_group_id);      
      open C_access_lls;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_LLS','ITEM_LOC,LOC_LIST_DETAIL,V_STORE','Item :'||I_item||' Loc List :'||I_group_id);      
      fetch C_access_lls into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_LLS','ITEM_LOC,LOC_LIST_DETAIL,V_STORE','Item :'||I_item||' Loc List :'||I_group_id);      
      close C_access_lls;
      ---
   elsif I_group_type = 'LLW' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_TOTAL_LLW','ITEM_LOC,LOC_LIST_DETAIL,WH','Item :'||I_item||' Loc List :'||I_group_id);
      open C_total_llw;
      SQL_LIB.SET_MARK('FETCH','C_TOTAL_LLW','ITEM_LOC,LOC_LIST_DETAIL,WH','Item :'||I_item||' Loc List :'||I_group_id);
      fetch C_total_llw into L_total_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_TOTAL_LLW','ITEM_LOC,LOC_LIST_DETAIL,WH','Item :'||I_item||' Loc List :'||I_group_id);
      close C_total_llw;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_LLW','ITEM_LOC,LOC_LIST_DETAIL,V_WH','Item :'||I_item||' Loc List :'||I_group_id);
      open C_access_llw;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_LLW','ITEM_LOC,LOC_LIST_DETAIL,V_WH','Item :'||I_item||' Loc List :'||I_group_id);      
      fetch C_access_llw into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_LLW','ITEM_LOC,LOC_LIST_DETAIL,V_WH','Item :'||I_item||' Loc List :'||I_group_id);      
      close C_access_llw;
      ---
   elsif I_group_type in ('AW','W') then
      ---
      SQL_LIB.SET_MARK('OPEN','C_TOTAL_WH','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);
      open C_total_wh;
      SQL_LIB.SET_MARK('FETCH','C_TOTAL_WH','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);
      fetch C_total_wh into L_total_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_TOTAL_WH','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);
      close C_total_wh;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_WH','ITEM_LOC,V_WH','Item :'||I_item||' Loc :'||I_group_id);      
      open C_access_wh;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_WH','ITEM_LOC,V_WH','Item :'||I_item||' Loc :'||I_group_id);      
      fetch C_access_wh into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_WH','ITEM_LOC,V_WH','Item :'||I_item||' Loc :'||I_group_id);      
      close C_access_wh;
      ---
   elsif I_group_type in ('AI', 'I') then
      ---
      SQL_LIB.SET_MARK('OPEN','C_TOTAL_INT_FINISHER','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);
      open C_total_int_finisher;
      SQL_LIB.SET_MARK('FETCH','C_TOTAL_INT_FINISHER','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);      
      fetch C_total_int_finisher into L_total_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_TOTAL_INT_FINISHER','ITEM_LOC,WH','Item :'||I_item||' Loc :'||I_group_id);      
      close C_total_int_finisher;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_INT_FINISHER','ITEM_LOC,V_INTERNAL_FINISHER','Item :'||I_item||' Loc :'||I_group_id);      
      open C_access_int_finisher;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_INT_FINISHER','ITEM_LOC,V_INTERNAL_FINISHER','Item :'||I_item||' Loc :'||I_group_id);      
      fetch C_access_int_finisher into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_INT_FINISHER','ITEM_LOC,V_INTERNAL_FINISHER','Item :'||I_item||' Loc :'||I_group_id);      
      close C_access_int_finisher;
      ---
   elsif I_group_type in ('AE', 'E') then
      ---
      SQL_LIB.SET_MARK('OPEN','C_TOTAL_EXT_FINISHER','ITEM_LOC,PARTNER','Item :'||I_item||' Loc :'||I_group_id);      
      open C_total_ext_finisher;
      SQL_LIB.SET_MARK('FETCH','C_TOTAL_EXT_FINISHER','ITEM_LOC,PARTNER','Item :'||I_item||' Loc :'||I_group_id);      
      fetch C_total_ext_finisher into L_total_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_TOTAL_EXT_FINISHER','ITEM_LOC,PARTNER','Item :'||I_item||' Loc :'||I_group_id);      
      close C_total_ext_finisher;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_EXT_FINISHER','ITEM_LOC,V_EXTERNAL_FINISHER','Item :'||I_item||' Loc :'||I_group_id);      
      open C_access_ext_finisher;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_EXT_FINISHER','ITEM_LOC,V_EXTERNAL_FINISHER','Item :'||I_item||' Loc :'||I_group_id);      
      fetch C_access_ext_finisher into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_EXT_FINISHER','ITEM_LOC,V_EXTERNAL_FINISHER','Item :'||I_item||' Loc :'||I_group_id);      
      close C_access_ext_finisher;
      ---   
   elsif I_group_type = 'L' then
      ---
      SQL_LIB.SET_MARK('OPEN','C_TOTAL_LOC_TRAITS','ITEM_LOC,LOC_TRAITS_MATRIX','Item :'||I_item||' Loc Trait :'||I_group_id);      
      open C_total_loc_traits;
      SQL_LIB.SET_MARK('FETCH','C_TOTAL_LOC_TRAITS','ITEM_LOC,LOC_TRAITS_MATRIX','Item :'||I_item||' Loc Trait :'||I_group_id);      
      fetch C_total_loc_traits into L_total_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_TOTAL_LOC_TRAITS','ITEM_LOC,LOC_TRAITS_MATRIX','Item :'||I_item||' Loc Trait :'||I_group_id);
      close C_total_loc_traits;
      ---
      SQL_LIB.SET_MARK('OPEN','C_ACCESS_LOC_TRAITS','ITEM_LOC,V_LOC_TRAITS,LOC_TRAITS_MATRIX,V_STORE','Item :'||I_item||' Loc Trait :'||I_group_id);      
      open C_access_loc_traits;
      SQL_LIB.SET_MARK('FETCH','C_ACCESS_LOC_TRAITS','ITEM_LOC,V_LOC_TRAITS,LOC_TRAITS_MATRIX,V_STORE','Item :'||I_item||' Loc Trait :'||I_group_id);      
      fetch C_access_loc_traits into L_access_itemloc;
      SQL_LIB.SET_MARK('CLOSE','C_ACCESS_LOC_TRAITS','ITEM_LOC,V_LOC_TRAITS,LOC_TRAITS_MATRIX,V_STORE','Item :'||I_item||' Loc Trait :'||I_group_id);      
      close C_access_loc_traits;
      ---
   end if;

   if L_total_itemloc != L_access_itemloc then
      O_diff_ind := 'Y';
   else
      O_diff_ind := 'N';
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
   RETURN FALSE;

END ITEMLOC_SUPP_FILTER_LIST;
--------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
FUNCTION INSERT_RECORDS_GTT (O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_item                IN      ITEM_MASTER.ITEM%TYPE,
                             I_supplier            IN      SUPS.SUPPLIER%TYPE,
                             I_origin_country_id   IN      COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50):= 'ITEM_SUPP_COUNTRY_LOC_SQL.INSERT_RECORDS_GTT';

BEGIN
   SQL_LIB.SET_MARK('INSERT',NULL,'GTT_ITEM_SUPP_COUNTRY_LOC','Item :'||I_item||' Supplier :'||to_char(I_supplier)||' Origin_Country :'||I_origin_country_id);
   insert into gtt_item_supp_country_loc (item,
                                          supplier,
                                          origin_country_id,
                                          loc,
                                          loc_type,
                                          primary_loc_ind,
                                          unit_cost,
                                          supp_hier_type_1,
                                          supp_hier_lvl_1,
                                          supp_hier_type_2,
                                          supp_hier_lvl_2,
                                          supp_hier_type_3,
                                          supp_hier_lvl_3)
                                   select item,
                                          supplier,
                                          origin_country_id,
                                          loc,
                                          loc_type,
                                          primary_loc_ind,
                                          unit_cost,
                                          supp_hier_type_1,
                                          supp_hier_lvl_1,
                                          supp_hier_type_2,
                                          supp_hier_lvl_2,
                                          supp_hier_type_3,
                                          supp_hier_lvl_3
                                     from item_supp_country_loc
                                    where item = I_item
                                      and supplier = I_supplier
                                      and origin_country_id = I_origin_country_id;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END INSERT_RECORDS_GTT;
------------------------------------------------------------------------------------------------------
FUNCTION FUTURE_COST_UPD (O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)

   RETURN BOOLEAN IS

   L_program                  VARCHAR2(50) := 'ITEM_SUPP_COUNTRY_LOC_SQL.FUTURE_COST_UPD';
   L_iscl_supp_hier_chg_tbl   OBJ_ISCL_SUPP_HIER_CHG_TBL;
   L_cost_event_process_id    COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;

   cursor C_GET_CHG_ITEM_DETL is
      select obj_iscl_supp_hier_chg_rec (iscl.item,
                                         iscl.supplier,
                                         iscl.origin_country_id,
                                         iscl.loc,
                                         giscl.supp_hier_lvl_1,
                                         giscl.supp_hier_lvl_2,
                                         giscl.supp_hier_lvl_3,
                                         iscl.supp_hier_lvl_1,
                                         iscl.supp_hier_lvl_2,
                                         iscl.supp_hier_lvl_3)
                                    from gtt_item_supp_country_loc giscl,
                                         item_supp_country_loc iscl
                                   where iscl.item = giscl.item
                                     and iscl.supplier = giscl.supplier
                                     and iscl.origin_country_id = giscl.origin_country_id
                                     and iscl.loc = giscl.loc
                                     and (NVL(iscl.supp_hier_lvl_1,'x') != NVL(giscl.supp_hier_lvl_1,'x')
                                      or NVL(iscl.supp_hier_lvl_2,'x') != NVL(giscl.supp_hier_lvl_2,'x')
                                      or NVL(iscl.supp_hier_lvl_3,'x') != NVL(giscl.supp_hier_lvl_3,'x'));

BEGIN
    SQL_LIB.SET_MARK('OPEN','C_GET_CHG_ITEM_DETL','ITEM_SUPP_COUNTRY_LOC,GTT_ITEM_SUPP_COUNTRY_LOC',NULL);
    open C_GET_CHG_ITEM_DETL;
    SQL_LIB.SET_MARK('FETCH','C_GET_CHG_ITEM_DETL','ITEM_SUPP_COUNTRY_LOC,GTT_ITEM_SUPP_COUNTRY_LOC',NULL);
    fetch C_GET_CHG_ITEM_DETL bulk collect into L_iscl_supp_hier_chg_tbl;
    SQL_LIB.SET_MARK('CLOSE','C_GET_CHG_ITEM_DETL','ITEM_SUPP_COUNTRY_LOC,GTT_ITEM_SUPP_COUNTRY_LOC',NULL);    
    close C_GET_CHG_ITEM_DETL;

    if L_iscl_supp_hier_chg_tbl.COUNT > 0 then
       if FUTURE_COST_EVENT_SQL.ADD_SUPP_HIER_CHANGE(O_error_message,
                                                     L_cost_event_process_id,
                                                     L_iscl_supp_hier_chg_tbl,
                                                     GET_USER) = FALSE then
          return FALSE;
       end if;
    end if;
    return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END FUTURE_COST_UPD;
------------------------------------------------------------------------------------------------------
FUNCTION CREATE_LOCATION(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_comp_item_cost_tbl IN OUT NOCOPY OBJ_COMP_ITEM_COST_TBL)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(64)   := 'ITEM_SUPP_COUNTRY_LOC_SQL.CREATE_LOCATION';
   L_system_options_rec    SYSTEM_OPTIONS%ROWTYPE;
   L_min_loc               ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_item                  item_supp_country_loc.item%type;
   L_supplier              item_supp_country_loc.supplier%TYPE;
   L_origin_country_id     item_supp_country_loc.origin_country_id%TYPE;
   L_update                varchar2(1);

   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_UPDATE_PRIM_LOC_IND is 
      select 'x'
        from item_supp_country_loc isl
       where isl.loc               = L_min_loc
         and isl.item              = L_item
         and isl.supplier          = NVL(L_supplier, supplier)
         and isl.origin_country_id = NVL(L_origin_country_id, origin_country_id)
         and not exists (select 'x'
                           from item_supp_country_loc isl3
                          where isl3.loc              != L_min_loc
                            and isl3.item              = isl.item
                            and isl3.supplier          = NVL(L_supplier, supplier)
                            and isl3.origin_country_id = NVL(L_origin_country_id, origin_country_id)
                            and isl3.primary_loc_ind   = 'Y'
                            and rownum = 1)
          for update nowait;
                    
BEGIN

   if IO_comp_item_cost_tbl is NULL or IO_comp_item_cost_tbl.COUNT = 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'IO_comp_item_cost_tbl',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
 
   -- insert records into item_supp_country_loc for records on OBJ_COMP_ITEM_COST_TBL
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC',NULL);   
   insert into item_supp_country_loc(item,
                                     supplier,
                                     origin_country_id,
                                     loc,
                                     loc_type,
                                     primary_loc_ind,
                                     unit_cost,
                                     round_lvl,
                                     round_to_inner_pct,
                                     round_to_case_pct,
                                     round_to_layer_pct,
                                     round_to_pallet_pct,
                                     supp_hier_type_1,
                                     supp_hier_lvl_1,
                                     supp_hier_type_2,
                                     supp_hier_lvl_2,
                                     supp_hier_type_3,
                                     supp_hier_lvl_3,
                                     pickup_lead_time,
                                     create_datetime,
                                     last_update_datetime,
                                     last_update_id,
                                     negotiated_item_cost,
                                     extended_base_cost,
                                     inclusive_cost,
                                     base_cost,
                                     create_id)
   --WITH clause
   with iscl_existence as (select iscli.item,
                                  iscli.supplier,
                                  iscli.origin_country_id,
                                  'Y' prev_isc_exists
                             from item_supp_country_loc iscli,
                                  TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) toi
                            where iscli.item = toi.I_item
                              and iscli.supplier = toi.I_supplier
                              and iscli.origin_country_id = toi.I_origin_country_id
                              group by iscli.item, iscli.supplier, iscli.origin_country_id
                           ),
       --
        table_obj_iscl as (select iscl_query.I_item,
                                  iscl_query.I_supplier,
                                  iscl_query.I_origin_country_id,
                                  iscl_query.I_location,
                                  iscl_query.I_loc_type,
                                  iscl_query.I_delivery_country_id,
                                  iscl_query.dept,
                                  case
                                     when NVL(iscl.prev_isc_exists,'N') = 'Y' then
                                        'N'
                                     else
                                        case
                                           when iscl_query.I_location = MIN(iscl_query.I_location)
                                                                          OVER (PARTITION BY iscl_query.I_item,
                                                                                             iscl_query.I_supplier,
                                                                                             iscl_query.I_origin_country_id) then
                                              'Y'
                                           else
                                              'N'
                                        end
                                  end I_primary_loc_ind,
                                  iscl_query.O_base_cost,
                                  iscl_query.O_negotiated_item_cost,
                                  iscl_query.O_extended_base_cost,
                                  iscl_query.O_inclusive_cost
                             from TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_query,
                                  iscl_existence iscl
                            where iscl_query.I_item = iscl.item (+)
                              and iscl_query.I_supplier = iscl.supplier (+)
                              and iscl_query.I_origin_country_id = iscl.origin_country_id (+)
                           )
   --end of WITH clause
   
                              select iscl_table.I_item,
                                     iscl_table.I_supplier,
                                     iscl_table.I_origin_country_id,
                                     iscl_table.I_location,
                                     iscl_table.I_loc_type,
                                     iscl_table.I_primary_loc_ind,
                                     DECODE(NVL(ca.default_po_cost, 'BC'), 'BC', NVL(iscl_table.O_base_cost,isc.unit_cost),
                                                                           'NIC', NVL(iscl_table.O_negotiated_item_cost,isc.unit_cost)),
                                     isc.round_lvl,
                                     isc.round_to_inner_pct,
                                     isc.round_to_case_pct,
                                     isc.round_to_layer_pct,
                                     isc.round_to_pallet_pct,
                                     isc.supp_hier_type_1,
                                     isc.supp_hier_lvl_1,
                                     isc.supp_hier_type_2,
                                     isc.supp_hier_lvl_2,
                                     isc.supp_hier_type_3,
                                     isc.supp_hier_lvl_3,
                                     isc.pickup_lead_time,
                                     sysdate,
                                     sysdate,
                                     get_user,
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_negotiated_item_cost, NULL), -- negotiated_item_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_extended_base_cost, NULL), -- extended_base_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_inclusive_cost, NULL), -- inclusive_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_base_cost, NULL), -- base_cost
                                     get_user
                                from sups s,
                                     item_supp_country isc,
                                     table_obj_iscl iscl_table,
                                     country_attrib ca
                               where isc.item = iscl_table.I_item
                                 and isc.supplier = iscl_table.I_supplier
                                 and isc.origin_country_id = iscl_table.I_origin_country_id
                                 and s.supplier = isc.supplier
                                 and (iscl_table.I_loc_type in ('S','E') or s.inv_mgmt_lvl in ('S', 'D'))
                                 and iscl_table.I_delivery_country_id = ca.country_id
                                 and not exists(select 'x'
                                                  from item_supp_country_loc isl
                                                 where isl.item              = iscl_table.I_item
                                                   and isl.loc               = iscl_table.I_location
                                                   and isl.supplier          = iscl_table.I_supplier
                                                   and isl.origin_country_id = iscl_table.I_origin_country_id
                                                   and rownum = 1)
                              UNION ALL
                              -- for supplier inv_mgmt_lvl = 'A'
                              select iscl_table.I_item,
                                     iscl_table.I_supplier,
                                     iscl_table.I_origin_country_id,
                                     iscl_table.I_location,
                                     iscl_table.I_loc_type,
                                     iscl_table.I_primary_loc_ind,
                                     DECODE(NVL(ca.default_po_cost, 'BC'), 'BC', NVL(iscl_table.O_base_cost,isc.unit_cost),
                                                                           'NIC', NVL(iscl_table.O_negotiated_item_cost,isc.unit_cost)),
                                     sim.round_lvl,
                                     sim.round_to_inner_pct,
                                     sim.round_to_case_pct,
                                     sim.round_to_layer_pct,
                                     sim.round_to_pallet_pct,
                                     isc.supp_hier_type_1,
                                     isc.supp_hier_lvl_1,
                                     isc.supp_hier_type_2,
                                     isc.supp_hier_lvl_2,
                                     isc.supp_hier_type_3,
                                     isc.supp_hier_lvl_3,
                                     isc.pickup_lead_time,
                                     sysdate,
                                     sysdate,
                                     get_user,
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_negotiated_item_cost, NULL ), --negotiated_item_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_extended_base_cost, NULL ), -- extended_base_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_inclusive_cost, NULL ), -- inclusive_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_base_cost, NULL ), -- base_cost
                                     get_user
                                from sups s,
                                     sup_inv_mgmt sim,
                                     item_supp_country isc,
                                     table_obj_iscl iscl_table,
                                     country_attrib ca,
                                     wh w
                               where isc.item = iscl_table.I_item
                                 and isc.supplier = iscl_table.I_supplier
                                 and isc.origin_country_id = iscl_table.I_origin_country_id
                                 and s.supplier = isc.supplier
                                 and s.inv_mgmt_lvl = 'A'
                                 and iscl_table.I_loc_type = 'W'
                                 and iscl_table.I_location = w.wh
                                 and iscl_table.I_delivery_country_id = ca.country_id
                                 and sim.supplier = isc.supplier
                                 and sim.dept     = iscl_table.dept
                                 and sim.location = w.physical_wh
                                 and not exists(select 'x'
                                                  from item_supp_country_loc isl
                                                 where isl.item              = iscl_table.I_item
                                                   and isl.loc               = iscl_table.I_location
                                                   and isl.supplier          = iscl_table.I_supplier
                                                   and isl.origin_country_id = iscl_table.I_origin_country_id
                                                   and rownum = 1)
                              UNION ALL
                              -- for supplier inv_mgmt_lvl = 'L'
                              select iscl_table.I_item,
                                     iscl_table.I_supplier,
                                     iscl_table.I_origin_country_id,
                                     iscl_table.I_location,
                                     iscl_table.I_loc_type,
                                     iscl_table.I_primary_loc_ind,
                                     DECODE(NVL(ca.default_po_cost, 'BC'), 'BC', NVL(iscl_table.O_base_cost,isc.unit_cost),
                                                                           'NIC', NVL(iscl_table.O_negotiated_item_cost,isc.unit_cost)),
                                     sim.round_lvl,
                                     sim.round_to_inner_pct,
                                     sim.round_to_case_pct,
                                     sim.round_to_layer_pct,
                                     sim.round_to_pallet_pct,
                                     isc.supp_hier_type_1,
                                     isc.supp_hier_lvl_1,
                                     isc.supp_hier_type_2,
                                     isc.supp_hier_lvl_2,
                                     isc.supp_hier_type_3,
                                     isc.supp_hier_lvl_3,
                                     isc.pickup_lead_time,
                                     sysdate,
                                     sysdate,
                                     get_user,
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_negotiated_item_cost, NULL ), --negotiated_item_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_extended_base_cost, NULL ), -- extended_base_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_inclusive_cost, NULL ), -- inclusive_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_base_cost, NULL ), -- base_cost
                                     get_user
                                from sups s,
                                     sup_inv_mgmt sim,
                                     item_supp_country isc,
                                     table_obj_iscl iscl_table,
                                     country_attrib ca,
                                     wh w
                               where isc.item = iscl_table.I_item
                                 and isc.supplier = iscl_table.I_supplier
                                 and isc.origin_country_id = iscl_table.I_origin_country_id
                                 and s.supplier = isc.supplier
                                 and s.inv_mgmt_lvl = 'L'
                                 and iscl_table.I_loc_type = 'W'
                                 and iscl_table.I_location = w.wh
                                 and iscl_table.I_delivery_country_id = ca.country_id
                                 and sim.supplier = isc.supplier
                                 and sim.dept is NULL
                                 and sim.location = w.physical_wh
                                 and not exists(select 'x'
                                                  from item_supp_country_loc isl
                                                 where isl.item              = iscl_table.I_item
                                                   and isl.loc               = iscl_table.I_location
                                                   and isl.supplier          = iscl_table.I_supplier
                                                   and isl.origin_country_id = iscl_table.I_origin_country_id
                                                   and rownum = 1)
                              UNION ALL
                              -- for supplier inv_mgmt_lvl in ('A', 'L') with no sup_inv_mgmt record
                              select iscl_table.I_item,
                                     iscl_table.I_supplier,
                                     iscl_table.I_origin_country_id,
                                     iscl_table.I_location,
                                     iscl_table.I_loc_type,
                                     iscl_table.I_primary_loc_ind,
                                     DECODE(NVL(ca.default_po_cost, 'BC'), 'BC', NVL(iscl_table.O_base_cost,isc.unit_cost),
                                                                           'NIC', NVL(iscl_table.O_negotiated_item_cost,isc.unit_cost)),
                                     isc.round_lvl,
                                     isc.round_to_inner_pct,
                                     isc.round_to_case_pct,
                                     isc.round_to_layer_pct,
                                     isc.round_to_pallet_pct,
                                     isc.supp_hier_type_1,
                                     isc.supp_hier_lvl_1,
                                     isc.supp_hier_type_2,
                                     isc.supp_hier_lvl_2,
                                     isc.supp_hier_type_3,
                                     isc.supp_hier_lvl_3,
                                     isc.pickup_lead_time,
                                     sysdate,
                                     sysdate,
                                     get_user,
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_negotiated_item_cost, NULL ), --negotiated_item_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_extended_base_cost, NULL ), -- extended_base_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_inclusive_cost, NULL ), -- inclusive_cost
                                     DECODE(L_system_options_rec.default_tax_type, 'GTAX', iscl_table.O_base_cost, NULL ), -- base_cost
                                     get_user
                                from sups s,
                                     item_supp_country isc,
                                     table_obj_iscl iscl_table,
                                     country_attrib ca,
                                     wh w
                               where isc.item = iscl_table.I_item
                                 and isc.supplier = iscl_table.I_supplier
                                 and isc.origin_country_id = iscl_table.I_origin_country_id
                                 and s.supplier = isc.supplier
                                 and iscl_table.I_loc_type = 'W'
                                 and iscl_table.I_location = w.wh
                                 and iscl_table.I_delivery_country_id = ca.country_id
                                 and (  (s.inv_mgmt_lvl = 'A'
                                         and not exists(select 'x'
                                                          from sup_inv_mgmt sim2
                                                          where sim2.supplier = isc.supplier
                                                           and sim2.dept     = iscl_table.dept
                                                           and sim2.location = w.physical_wh
                                           and rownum = 1))
                                     or (s.inv_mgmt_lvl = 'L'
                                         and not exists(select 'x'
                                                          from sup_inv_mgmt sim2
                                                         where sim2.supplier = isc.supplier
                                                           and sim2.location = w.physical_wh
                                            and rownum = 1)))
                                 and not exists(select 'x'
                                                  from item_supp_country_loc isl
                                                 where isl.item              = iscl_table.I_item
                                                   and isl.loc               = iscl_table.I_location
                                                   and isl.supplier          = iscl_table.I_supplier
                                                   and isl.origin_country_id = iscl_table.I_origin_country_id
                                                   and rownum = 1);

   -- update the unit cost based on components for buyer packs
   merge into item_supp_country_loc iscl
      using (select iscl_table.I_item,
                    iscl_table.I_supplier,
                    iscl_table.I_origin_country_id,
                    iscl_table.I_location,
                    NVL(sum(isl.unit_cost * vpq.qty), 0) unit_cost
               from item_supp_country_loc isl,
                    TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table,
                    item_master im,
                    v_packsku_qty vpq
              where isl.item               = vpq.item
                and vpq.pack_no            = iscl_table.I_item
                and isl.supplier           = iscl_table.I_supplier
                and isl.origin_country_id  = iscl_table.I_origin_country_id
                and isl.loc                = iscl_table.I_location
                and im.item                = iscl_table.I_item
                and NVL(im.pack_type, 'N') = 'B'
                group by iscl_table.I_item,
                         iscl_table.I_supplier,
                         iscl_table.I_origin_country_id,
                         iscl_table.I_location) update_set
         on (iscl.item              = update_set.I_item and
             iscl.supplier          = update_set.I_supplier and
             iscl.origin_country_id = update_set.I_origin_country_id and
             iscl.loc               = update_set.I_location)
       when matched then
          update set iscl.unit_cost = update_set.unit_cost;

   -- bracket costing functionality
   if L_system_options_rec.bracket_costing_ind = 'Y' then
      for i in IO_comp_item_cost_tbl.FIRST..IO_comp_item_cost_tbl.lAST LOOP
         if ITEM_BRACKET_COST_SQL.CREATE_BRACKET(O_error_message,
                                                 IO_comp_item_cost_tbl(i).I_item,
                                                 NULL, --supplier
                                                 NULL, --origin_country_id
                                                 IO_comp_item_cost_tbl(i).I_location,
                                                 'N') = FALSE then
             return FALSE;
         end if;
      end LOOP;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      -- if the record is locked skip the update because the item_supp_country_loc.primary_loc_ind
      -- is being updated by another thread for the same item/minimum location
      return TRUE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_LOCATION;
------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_FRANCHISE_COST(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_virtual_wh_exists    IN OUT BOOLEAN,
                               I_item                 IN     ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                               I_supplier             IN     ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                               I_origin_country_id    IN     ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                               I_group_type           IN     VARCHAR2,
                               I_group_value          IN     VARCHAR2,
                               I_process_children     IN     VARCHAR2,
                               I_unit_cost            IN     ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                               I_base_cost            IN     ITEM_SUPP_COUNTRY_LOC.BASE_COST%TYPE,
                               I_negotiated_item_cost IN     ITEM_SUPP_COUNTRY_LOC.NEGOTIATED_ITEM_COST%TYPE,
                               I_extended_base_cost   IN     ITEM_SUPP_COUNTRY_LOC.EXTENDED_BASE_COST%TYPE,
                               I_inclusive_cost       IN     ITEM_SUPP_COUNTRY_LOC.INCLUSIVE_COST%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64) := 'ITEM_SUPP_COUNTRY_LOC_SQL.UPDATE_FRNACHISE_COST';
  
   L_table              VARCHAR2(30) := 'ITEM_SUPP_COUNTRY_LOC';
   L_where_clause       VARCHAR2(3000);
   L_no_update          VARCHAR2(100);
   L_loc                ITEM_SUPP_COUNTRY_LOC.LOC%TYPE;
   L_unit_cost          ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE;
   L_primary_loc_ind    ITEM_SUPP_COUNTRY_LOC.PRIMARY_LOC_IND%TYPE;
   
   ---
   sql_stmt             VARCHAR2(3000);
   lock_stmt            VARCHAR2(3000);
   L_sql_stmt_cur       PLS_INTEGER;
   L_fetch_rows         INTEGER;
   L_lock_stmt_cur      PLS_INTEGER;
   TYPE LOC_CURSOR is REF CURSOR;
   C_LOC                LOC_CURSOR;
   ---
   ---
   RECORD_LOCKED        EXCEPTION;
   PRAGMA               EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   L_default_tax_type   SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;  
   
BEGIN
   if I_group_type ='W' then
      if LOCATION_ATTRIB_SQL.BUILD_GROUP_TYPE_VIRTUAL_WHERE(O_error_message,
                                                            L_where_clause,
                                                            'loc',
                                                            I_group_type,
                                                            I_group_value) = FALSE then
         return FALSE;
      end if;
      ---
      if I_group_type = 'W' then
         L_where_clause := LPAD(L_where_clause, LENGTH(L_where_clause) - 1);
         
         L_where_clause := L_where_clause
                           || ' union '
                           || '(select s.store '
                           || 'from store s, '
                           || 'item_supp_country_loc iscl '
                           || 'where s.store_type = ''W'' '
                           || 'and iscl.loc = s.store '
                           || 'and iscl.item = ''' || I_item || ''' '
                           || 'and iscl.supplier = ' || I_supplier || ' '
                           || 'and iscl.origin_country_id = ''' || I_origin_country_id || ''' '
                           || '))';
                           
         L_where_clause := LPAD(L_where_clause, LENGTH(L_where_clause) + 5, ' and ');
      end if;
      if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                   L_system_options_rec) then
         return FALSE;
      end if;
   
      ---
      -- LOCK RECORDS FOR UPDATE
      ---
      SQL_LIB.SET_MARK('UPDATE', NULL,'ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id);
      ---
      L_no_update := ' for update nowait';
      ---
      lock_stmt := 'select loc,
                           primary_loc_ind,
                           unit_cost
                      from item_supp_country_loc
                     where item              = :I_item
                       and supplier          = :I_supplier
                       and origin_country_id = :I_origin_country_id
                       and (loc in (select store from store)
                           or loc in (select wh from wh))' || L_where_clause;
                           
      L_lock_stmt_cur             := DBMS_SQL.OPEN_CURSOR;       
      
      DBMS_SQL.PARSE(L_lock_stmt_cur,lock_stmt || L_no_update,DBMS_SQL.NATIVE);
      
      DBMS_SQL.BIND_VARIABLE(L_lock_stmt_cur, ':I_item', I_item);
      DBMS_SQL.BIND_VARIABLE(L_lock_stmt_cur, ':I_supplier', I_supplier);
      DBMS_SQL.BIND_VARIABLE(L_lock_stmt_cur, ':I_origin_country_id', I_origin_country_id);
    
      L_fetch_rows := DBMS_SQL.EXECUTE_AND_FETCH(L_lock_stmt_cur);
      
      DBMS_SQL.CLOSE_CURSOR (L_lock_stmt_cur);                           
    
      ---
      -- UPDATE RECORDS ON ITEM_SUPP_COUNRY_LOC
      ---
      
      L_default_tax_type := L_system_options_rec.default_tax_type;
      L_fetch_rows :=  0;
      
      sql_stmt := 'update item_supp_country_loc
                  set unit_cost            = NVL(:I_unit_cost, unit_cost),
                      negotiated_item_cost = DECODE(:L_default_tax_type,''GTAX'', :I_negotiated_item_cost, NULL),
                      extended_base_cost   = DECODE(:L_default_tax_type,''GTAX'', :I_extended_base_cost, NULL),
                      inclusive_cost       = DECODE(:L_default_tax_type,''GTAX'', :I_inclusive_cost, NULL),
                      base_cost            = DECODE(:L_default_tax_type,''GTAX'', :I_base_cost, NULL),
                      last_update_datetime = :s,
                      last_update_id       = :u
                where item = :I_item
                  and supplier = :I_supplier
                  and origin_country_id = :I_origin_country_id
                  and (loc in (select store from store)
                      or loc in (select wh from wh))' || L_where_clause;
      
      L_sql_stmt_cur := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE( L_sql_stmt_cur,sql_stmt,DBMS_SQL.NATIVE);
            
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_unit_cost', I_unit_cost);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'L_default_tax_type', L_default_tax_type);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_negotiated_item_cost', I_negotiated_item_cost);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_extended_base_cost', I_extended_base_cost);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_inclusive_cost', I_inclusive_cost);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_base_cost', I_base_cost);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 's', sysdate);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'u', get_user);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_item', I_item);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_supplier', I_supplier);
      DBMS_SQL.BIND_VARIABLE(L_sql_stmt_cur, 'I_origin_country_id', I_origin_country_id);
            
      L_fetch_rows := DBMS_SQL.EXECUTE(L_sql_stmt_cur);
            
      DBMS_SQL.CLOSE_CURSOR (L_sql_stmt_cur); 
    
      open C_LOC for lock_stmt USING I_item, I_supplier, I_origin_country_id;
      loop
         SQL_LIB.SET_MARK('FETCH','C_LOC','ITEM_SUPP_COUNTRY_LOC','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '|| I_origin_country_id ||
                          ' Location: '||to_char(L_loc));
         fetch C_LOC into L_loc,
                          L_primary_loc_ind,
                          L_unit_cost;
         ---
         EXIT WHEN C_LOC%NOTFOUND;
         ---
         O_virtual_wh_exists := TRUE; 
         if I_unit_cost is not NULL and L_primary_loc_ind = 'Y' then
            if UPDATE_BASE_COST.CHANGE_COST(O_error_message,
                                            I_item,
                                            I_supplier,
                                            I_origin_country_id,
                                            L_loc,
                                            I_process_children,
                                            'Y',
                                            NULL /* Cost Change Number */ ) = FALSE then   -- update unit cost indicator
               return FALSE;
            end if;
         end if;
         ---
      end loop;
      close C_LOC;
   end if;
     
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_item,
                                            to_char(I_supplier));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_FRANCHISE_COST;   
--------------------------------------------------------------------------------------------------
END ITEM_SUPP_COUNTRY_LOC_SQL;
/