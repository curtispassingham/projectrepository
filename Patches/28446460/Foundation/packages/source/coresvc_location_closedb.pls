-- File Name : CORESVC_LOCATION_CLOSED_body.pls
CREATE OR REPLACE PACKAGE BODY CORESVC_LOCATION_CLOSED as
   cursor C_SVC_LOCATION_CLOSED(I_process_id NUMBER,
                                I_chunk_id NUMBER) is
      select pk_location_closed.rowid     as pk_location_closed_rid,
             pk_location_closed.ship_ind  as base_ship_ind,
             pk_location_closed.loc_type  as base_loc_type,
             pk_location_closed.recv_ind  as base_recv_ind,
             pk_location_closed.sales_ind as base_sales_ind,
             st.location,
             st.close_date,
             UPPER(st.loc_type)           as loc_type,
             UPPER(st.sales_ind)          as sales_ind,
             UPPER(st.recv_ind)           as recv_ind,
             UPPER(st.ship_ind)           as ship_ind,
             st.reason,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)             as action,
             st.process$status
        from svc_location_closed  st,
             location_closed      pk_location_closed,
             dual
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.close_date = pk_location_closed.close_date (+)
         and st.location   = pk_location_closed.location (+);

   L_min_date         PERIOD.VDATE%TYPE := GET_VDATE;
   TYPE errors_tab_typ IS TABLE OF svc_admin_upld_er%ROWTYPE;
   LP_errors_tab      errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab  s9t_errors_tab_typ;

   Type LOCATION_CLOSED_TAB IS TABLE OF LOCATION_CLOSED_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
end GET_SHEET_NAME_TRANS;
-----------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);

   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES( I_file_id   NUMBER) IS
   L_sheets                S9T_PKG.NAMES_MAP_TYP;
   LOCATION_CLOSED_COLS    S9T_PKG.NAMES_MAP_TYP;
   LOCATION_CLOSED_TL_cols S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                   := s9t_pkg.get_sheet_names(I_file_id);
   LOCATION_CLOSED_COLS       := s9t_pkg.get_col_names(I_file_id,LOCATION_CLOSED_sheet);
   LOCATION_CLOSED$Action     := LOCATION_CLOSED_COLS('ACTION');
   LOCATION_CLOSED$LOCATION   := LOCATION_CLOSED_COLS('LOCATION');
   LOCATION_CLOSED$CLOSE_DATE := LOCATION_CLOSED_COLS('CLOSE_DATE');
   LOCATION_CLOSED$LOC_TYPE   := LOCATION_CLOSED_COLS('LOC_TYPE');
   LOCATION_CLOSED$SALES_IND  := LOCATION_CLOSED_COLS('SALES_IND');
   LOCATION_CLOSED$RECV_IND   := LOCATION_CLOSED_COLS('RECV_IND');
   LOCATION_CLOSED$SHIP_IND   := LOCATION_CLOSED_COLS('SHIP_IND');
   LOCATION_CLOSED$REASON     := LOCATION_CLOSED_COLS('REASON');

   LOCATION_CLOSED_TL_COLS       := s9t_pkg.get_col_names(I_file_id,LOCATION_CLOSED_TL_sheet);
   LOCATION_CLOSED_TL$Action     := LOCATION_CLOSED_TL_COLS('ACTION');
   LOCATION_CLOSED_TL$Lang       := LOCATION_CLOSED_TL_COLS('LANG');
   LOCATION_CLOSED_TL$LOCATION   := LOCATION_CLOSED_TL_COLS('LOCATION');
   LOCATION_CLOSED_TL$CLOSE_DATE := LOCATION_CLOSED_TL_COLS('CLOSE_DATE');
   LOCATION_CLOSED_TL$REASON     := LOCATION_CLOSED_TL_COLS('REASON');
END POPULATE_NAMES;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_LOCATION_CLOSED( I_file_id IN NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = i_file_id
          and ss.sheet_name = LOCATION_CLOSED_sheet )
   select s9t_row(s9t_cells(CORESVC_LOCATION_CLOSED.action_mod,
                             location,
                             close_date,
                             loc_type,
                             sales_ind,
                             recv_ind,
                             ship_ind,
                             reason ))
     from location_closed ;
END POPULATE_LOCATION_CLOSED;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_LOCATION_CLOSED_TL( I_file_id IN NUMBER ) IS
BEGIN
   insert into TABLE
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = i_file_id
          and ss.sheet_name = LOCATION_CLOSED_TL_SHEET )
   select s9t_row(s9t_cells(CORESVC_LOCATION_CLOSED.action_mod,
                            lang,
                            location,
                            close_date,
                            reason ))
     from location_closed_tl ;
END POPULATE_LOCATION_CLOSED_TL;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id IN OUT NUMBER ) IS
   L_file      S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW S9T_FILE();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang := GET_USER_LANG;
   L_file.add_sheet(LOCATION_CLOSED_sheet);
   L_file.sheets(L_file.get_sheet_index(LOCATION_CLOSED_sheet)).column_headers := S9T_CELLS( 'ACTION',
                                                                                             'LOCATION',
                                                                                             'CLOSE_DATE',
                                                                                             'LOC_TYPE',
                                                                                             'SALES_IND',
                                                                                             'RECV_IND',
                                                                                             'SHIP_IND',
                                                                                             'REASON' );
   s9t_pkg.save_obj(L_file);

   L_file.add_sheet(LOCATION_CLOSED_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(LOCATION_CLOSED_TL_SHEET)).column_headers := S9T_CELLS( 'ACTION',
                                                                                                'LANG',
                                                                                                'LOCATION',
                                                                                                'CLOSE_DATE',
                                                                                                'REASON' );
   s9t_pkg.save_obj(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message       IN OUT   rtk_errors.rtk_text%TYPE,
                     O_file_id             IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind   IN       CHAR  DEFAULT 'N')
RETURN BOOLEAN IS
   L_file      S9T_FILE;
   L_program   VARCHAR2(255) := 'CORESVC_LOCATION_CLOSED.create_s9t';
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N'   then
      POPULATE_LOCATION_CLOSED(O_file_id);
      POPULATE_LOCATION_CLOSED_TL(O_file_id);
      Commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);

   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);

   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_LOCATION_CLOSED( I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                                       I_process_id IN SVC_LOCATION_CLOSED.process_id%type ) IS

   TYPE svc_LOCATION_CLOSED_col_typ IS TABLE OF SVC_LOCATION_CLOSED%ROWTYPE;
   L_error                   BOOLEAN                     := FALSE;
   L_temp_rec                SVC_LOCATION_CLOSED%ROWTYPE;
   svc_location_closed_col   svc_LOCATION_CLOSED_col_typ := NEW svc_LOCATION_CLOSED_col_typ();
   L_process_id              SVC_LOCATION_CLOSED.process_id%TYPE;
   L_default_rec             SVC_LOCATION_CLOSED%ROWTYPE;

   cursor C_MANDATORY_IND is
      select location_mi,
             close_date_mi,
             loc_type_mi,
             sales_ind_mi,
             recv_ind_mi,
             ship_ind_mi,
             reason_mi,
             1   as   dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_LOCATION_CLOSED.template_key
                 and wksht_key     = 'LOCATION_CLOSED')
               PIVOT (MAX(mandatory)   as   mi FOR (column_key) in ('LOCATION'   as LOCATION,
                                                                    'CLOSE_DATE' as CLOSE_DATE,
                                                                    'LOC_TYPE'   as LOC_TYPE,
                                                                    'SALES_IND'  as SALES_IND,
                                                                    'RECV_IND'   as RECV_IND,
                                                                    'SHIP_IND'   as SHIP_IND,
                                                                    'REASON'     as REASON,
                                                                     NULL        as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA EXCEPTION_INIT(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_LOCATION_CLOSED';
   L_pk_columns    VARCHAR2(255)  := 'Close Date,Location';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   FOR rec in
   (select LOCATION_dv,
           CLOSE_DATE_dv,
           LOC_TYPE_dv,
           SALES_IND_dv,
           RECV_IND_dv,
           SHIP_IND_dv,
           REASON_dv,
           NULL as dummy
      from
         (select column_key,
                 default_value
            from s9t_tmpl_cols_def
            where template_key  = CORESVC_LOCATION_CLOSED.template_key
              and wksht_key      = 'LOCATION_CLOSED')
           PIVOT (MAX(default_value)   as   dv FOR (column_key) in ( 'LOCATION'     as   LOCATION,
                                                                     'CLOSE_DATE'   as   CLOSE_DATE,
                                                                     'LOC_TYPE'     as   LOC_TYPE,
                                                                     'SALES_IND'    as   SALES_IND,
                                                                     'RECV_IND'     as   RECV_IND,
                                                                     'SHIP_IND'     as   SHIP_IND,
                                                                     'REASON'       as   REASON,
                                                                      NULL          as   dummy)))   LOOP
   BEGIN
      L_default_rec.LOCATION   := rec.LOCATION_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'LOCATION_CLOSED',
                          NULL,
                         'LOCATION',
                          NULL,
                         'INV_DEFAULT' );
   END;


   BEGIN
      L_default_rec.CLOSE_DATE := rec.CLOSE_DATE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'LOCATION_CLOSED',
                          NULL,'CLOSE_DATE',
                          NULL,
                         'INV_DEFAULT' );
   END;

   BEGIN
      L_default_rec.LOC_TYPE := rec.LOC_TYPE_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'LOCATION_CLOSED',
                          NULL,
                         'LOC_TYPE',
                          NULL,
                         'INV_DEFAULT' );
   END;

   BEGIN
      L_default_rec.SALES_IND := rec.SALES_IND_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'LOCATION_CLOSED',
                          NULL,
                         'SALES_IND',
                          NULL,
                         'INV_DEFAULT' );
   END;


   BEGIN
      L_default_rec.RECV_IND := rec.RECV_IND_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'LOCATION_CLOSED',
                          NULL,
                         'RECV_IND',
                          NULL,
                         'INV_DEFAULT' );
   END;

   BEGIN
      L_default_rec.SHIP_IND := rec.SHIP_IND_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'LOCATION_CLOSED',
                          NULL,
                         'SHIP_IND',
                          NULL,
                         'INV_DEFAULT' );
   END;

   BEGIN
       L_default_rec.REASON := rec.REASON_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                         'LOCATION_CLOSED',
                          NULL,
                         'REASON',
                          NULL,
                         'INV_DEFAULT' );
   END;

END LOOP;

   open c_mandatory_ind;
   fetch c_mandatory_ind
   into l_mi_rec;
   close c_mandatory_ind;
   FOR rec in
    (select r.get_cell(LOCATION_CLOSED$Action)     as Action,
            r.get_cell(LOCATION_CLOSED$LOCATION)   as LOCATION,
            r.get_cell(LOCATION_CLOSED$CLOSE_DATE) as CLOSE_DATE,
            r.get_cell(LOCATION_CLOSED$LOC_TYPE)   as LOC_TYPE,
            r.get_cell(LOCATION_CLOSED$SALES_IND)  as SALES_IND,
            r.get_cell(LOCATION_CLOSED$RECV_IND)   as RECV_IND,
            r.get_cell(LOCATION_CLOSED$SHIP_IND)   as SHIP_IND,
            r.get_cell(LOCATION_CLOSED$REASON)     as REASON,
            r.get_row_seq()                        as row_seq
       from s9t_folder sf,
            TABLE(sf.s9t_file_obj.sheets) ss,
            TABLE(ss.s9t_rows) r
      where sf.file_id    = I_file_id
        and ss.sheet_name = sheet_name_trans(LOCATION_CLOSED_sheet))  LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

   BEGIN
      L_temp_rec.Action := rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          rec.row_seq,
                          action_column,
                          SQLCODE,
                          SQLERRM );
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.LOCATION := rec.LOCATION;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          rec.row_seq,
                         'LOCATION',
                          SQLCODE,
                          SQLERRM );
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CLOSE_DATE := rec.CLOSE_DATE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          rec.row_seq,
                         'CLOSE_DATE',
                          SQLCODE,
                          SQLERRM );
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.LOC_TYPE := rec.LOC_TYPE;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          rec.row_seq,
                         'LOC_TYPE',
                          SQLCODE,
                          SQLERRM );
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.SALES_IND := rec.SALES_IND;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          rec.row_seq,
                         'SALES_IND',
                          SQLCODE,
                          SQLERRM );
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.RECV_IND := rec.RECV_IND;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          rec.row_seq,
                         'RECV_IND',
                          SQLCODE,
                          SQLERRM );
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.SHIP_IND := rec.SHIP_IND;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          rec.row_seq,
                         'SHIP_IND',
                          SQLCODE,
                          SQLERRM );
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.REASON := rec.REASON;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          rec.row_seq,
                         'REASON',
                          SQLCODE,
                          SQLERRM );
         L_error := TRUE;
   END;

   if rec.action = CORESVC_LOCATION_CLOSED.action_new then
      L_temp_rec.LOCATION   := NVL( L_temp_rec.LOCATION,
                                    L_default_rec.LOCATION);
      L_temp_rec.CLOSE_DATE := NVL( L_temp_rec.CLOSE_DATE,
                                    L_default_rec.CLOSE_DATE);
      L_temp_rec.LOC_TYPE   := NVL( L_temp_rec.LOC_TYPE,
                                    L_default_rec.LOC_TYPE);
      L_temp_rec.SALES_IND  := NVL( L_temp_rec.SALES_IND,
                                    L_default_rec.SALES_IND);
      L_temp_rec.RECV_IND   := NVL( L_temp_rec.RECV_IND,
                                    L_default_rec.RECV_IND);
      L_temp_rec.SHIP_IND   := NVL( L_temp_rec.SHIP_IND,
                                    L_default_rec.SHIP_IND);
      L_temp_rec.REASON     := NVL( L_temp_rec.REASON,
                                    L_default_rec.REASON);
   end if;

   If NOT ( L_temp_rec.CLOSE_DATE is NOT NULL
      and L_temp_rec.LOCATION is NOT NULL and
          1 = 1 ) then
           WRITE_S9T_ERROR( I_file_id,
                       LOCATION_CLOSED_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
               L_error := TRUE;
   end if;

   if NOT L_error THEN
      svc_location_closed_col.extend();
      svc_location_closed_col(svc_LOCATION_CLOSED_col.count()):=L_temp_rec;
   end if;

   END LOOP;

BEGIN
   forall i IN 1..svc_location_closed_col.count SAVE EXCEPTIONS
      merge INTO svc_location_closed st
      using (select (case
                        when l_mi_rec.LOCATION_mi    = 'N' and
                             svc_LOCATION_CLOSED_col(i).action = CORESVC_LOCATION_CLOSED.action_mod and
                             s1.LOCATION             is NULL then
                             mt.LOCATION
                        else s1.LOCATION
                        end) as LOCATION,
                    (case
                        when l_mi_rec.CLOSE_DATE_mi  = 'N' and
                             svc_location_closed_col(i).action = CORESVC_LOCATION_CLOSED.action_mod and
                             s1.CLOSE_DATE           is NULL then
                             mt.CLOSE_DATE
                        else s1.CLOSE_DATE
                        end) as CLOSE_DATE,
                    (case
                        when l_mi_rec.LOC_TYPE_mi    = 'N' and
                             svc_location_closed_col(i).action = CORESVC_LOCATION_CLOSED.action_mod and
                             s1.LOC_TYPE             is NULL then
                             mt.LOC_TYPE
                        else s1.LOC_TYPE
                        end) as LOC_TYPE,
                    (case
                        when l_mi_rec.SALES_IND_mi   = 'N' and
                             svc_location_closed_col(i).action = CORESVC_LOCATION_CLOSED.action_mod and
                             s1.SALES_IND            is NULL then
                             mt.SALES_IND
                        else s1.SALES_IND
                        end) as SALES_IND,
                    (case
                        when l_mi_rec.RECV_IND_mi    = 'N' and
                             svc_location_closed_col(i).action = CORESVC_LOCATION_CLOSED.action_mod and
                             s1.RECV_IND             is NULL then
                             mt.RECV_IND
                        else s1.RECV_IND
                        end) as RECV_IND,
                    (case
                        when l_mi_rec.SHIP_IND_mi    = 'N' and
                             svc_location_closed_col(i).action = CORESVC_LOCATION_CLOSED.action_mod and
                             s1.SHIP_IND             is NULL then
                             mt.SHIP_IND
                        else s1.SHIP_IND
                        end) as SHIP_IND,
                    (case
                        when l_mi_rec.REASON_mi      = 'N' and
                             svc_location_closed_col(i).action = CORESVC_LOCATION_CLOSED.action_mod and
                             s1.REASON               is NULL then
                             mt.REASON
                        else s1.REASON
                        end) as REASON,
                        null as dummy
               from (select svc_location_closed_col(i).LOCATION   as LOCATION,
                            svc_location_closed_col(i).CLOSE_DATE as CLOSE_DATE,
                            svc_location_closed_col(i).LOC_TYPE   as LOC_TYPE,
                            svc_location_closed_col(i).SALES_IND  as SALES_IND,
                            svc_location_closed_col(i).RECV_IND   as RECV_IND,
                            svc_location_closed_col(i).SHIP_IND   as SHIP_IND,
                            svc_location_closed_col(i).REASON     as REASON,
                            null                                  as dummy
                       from dual ) s1, LOCATION_CLOSED mt
                      where mt.CLOSE_DATE (+) = s1.CLOSE_DATE
                        and mt.LOCATION (+)   = s1.LOCATION
                        and 1                 = 1) sq
         ON (       st.CLOSE_DATE  = sq.CLOSE_DATE and
                    st.LOCATION    = sq.LOCATION   and
                    svc_LOCATION_CLOSED_col(i).action in (CORESVC_LOCATION_CLOSED.action_mod,CORESVC_LOCATION_CLOSED.action_del))

         when matched then
            update
               set process_id        = svc_LOCATION_CLOSED_col(i).PROCESS_ID ,
                   chunk_id          = svc_LOCATION_CLOSED_col(i).CHUNK_ID ,
                   row_seq           = svc_LOCATION_CLOSED_col(i).ROW_SEQ ,
                   action            = svc_LOCATION_CLOSED_col(i).ACTION,
                   process$status    = svc_LOCATION_CLOSED_col(i).PROCESS$STATUS ,
                   ship_ind          = sq.SHIP_IND ,
                   reason            = sq.REASON ,
                   loc_type          = sq.LOC_TYPE ,
                   sales_ind         = sq.SALES_IND ,
                   recv_ind          = sq.RECV_IND ,
                   create_id         = svc_LOCATION_CLOSED_col(i).CREATE_ID ,
                   create_datetime   = svc_LOCATION_CLOSED_col(i).CREATE_DATETIME ,
                   last_upd_id       = svc_LOCATION_CLOSED_col(i).LAST_UPD_ID ,
                   last_upd_datetime = svc_LOCATION_CLOSED_col(i).LAST_UPD_DATETIME
         when NOT matched then
            insert( process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    location ,
                    close_date ,
                    loc_type ,
                    sales_ind ,
                    recv_ind ,
                    ship_ind ,
                    reason ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime )
            values( svc_LOCATION_CLOSED_col(i).PROCESS_ID ,
                    svc_LOCATION_CLOSED_col(i).CHUNK_ID ,
                    svc_LOCATION_CLOSED_col(i).ROW_SEQ ,
                    svc_LOCATION_CLOSED_col(i).ACTION ,
                    svc_LOCATION_CLOSED_col(i).PROCESS$STATUS ,
                    sq.LOCATION ,
                    sq.CLOSE_DATE ,
                    sq.LOC_TYPE ,
                    sq.SALES_IND ,
                    sq.RECV_IND ,
                    sq.SHIP_IND ,
                    sq.REASON ,
                    svc_LOCATION_CLOSED_col(i).CREATE_ID ,
                    svc_LOCATION_CLOSED_col(i).CREATE_DATETIME ,
                    svc_LOCATION_CLOSED_col(i).LAST_UPD_ID ,
                    svc_LOCATION_CLOSED_col(i).LAST_UPD_DATETIME );
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.count LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
           if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
         WRITE_S9T_ERROR( I_file_id,
                          LOCATION_CLOSED_sheet,
                          svc_LOCATION_CLOSED_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
        END LOOP;
END;
END PROCESS_S9T_LOCATION_CLOSED;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_LOCATION_CLOSED_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                         I_process_id   IN   SVC_LOCATION_CLOSED_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_LOCATION_CLOSED_TL_COL_TYP IS TABLE OF SVC_LOCATION_CLOSED_TL%ROWTYPE;
   L_temp_rec                   SVC_LOCATION_CLOSED_TL%ROWTYPE;
   SVC_LOCATION_CLOSED_TL_COL   SVC_LOCATION_CLOSED_TL_COL_TYP := NEW SVC_LOCATION_CLOSED_TL_COL_TYP();
   L_process_id                 SVC_LOCATION_CLOSED_TL.PROCESS_ID%TYPE;
   L_error                      BOOLEAN := FALSE;
   L_default_rec                SVC_LOCATION_CLOSED_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select reason_mi,
             close_date_mi,
             location_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'LOCATION_CLOSED_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('REASON'     as reason,
                                       'CLOSE_DATE' as close_date,
                                       'LOCATION'   as location,
                                       'LANG'       as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_LOCATION_CLOSED_TL';
   L_pk_columns    VARCHAR2(255)  := 'Location, Close Date, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select reason_dv,
                      close_date_dv,
                      location_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'LOCATION_CLOSED_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('REASON'     as reason,
                                                'CLOSE_DATE' as close_date,
                                                'LOCATION'   as location,
                                                'LANG'       as lang)))
   LOOP
      BEGIN
         L_default_rec.reason := rec.reason_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOCATION_CLOSED_TL_SHEET ,
                            NULL,
                           'REASON' ,
                            NULL,
                           'INV_DEFAULT' );
      END;
      BEGIN
         L_default_rec.close_date := rec.close_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           LOCATION_CLOSED_TL_SHEET ,
                            NULL,
                           'CLOSE_DATE' ,
                            NULL,
                           'INV_DEFAULT' );
      END;
      BEGIN
         L_default_rec.location := rec.location_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           LOCATION_CLOSED_TL_SHEET ,
                            NULL,
                           'LOCATION' ,
                            NULL,
                           'INV_DEFAULT' );
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOCATION_CLOSED_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT' );
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select upper(r.get_cell(location_closed_TL$action)) as action,
                      r.get_cell(location_closed_TL$reason)        as reason,
                      r.get_cell(location_closed_TL$close_date)    as close_date,
                      r.get_cell(location_closed_TL$location)      as location,
                      r.get_cell(location_closed_TL$lang)          as lang,
                      r.get_row_seq()                              as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(LOCATION_CLOSED_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            location_closed_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reason := rec.reason;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOCATION_CLOSED_TL_SHEET,
                            rec.row_seq,
                            'REASON',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.close_date := rec.close_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOCATION_CLOSED_TL_SHEET,
                            rec.row_seq,
                            'CLOSE_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.location := rec.location;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOCATION_CLOSED_TL_SHEET,
                            rec.row_seq,
                            'LOCATION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOCATION_CLOSED_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_LOCATION_CLOSED.action_new then
         L_temp_rec.reason := NVL( L_temp_rec.reason,L_default_rec.reason);
         L_temp_rec.close_date := NVL( L_temp_rec.close_date,L_default_rec.close_date);
         L_temp_rec.location := NVL( L_temp_rec.location,L_default_rec.location);
         L_temp_rec.lang := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.location is NOT NULL and L_temp_rec.close_date is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         LOCATION_CLOSED_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_LOCATION_CLOSED_TL_COL.extend();
         SVC_LOCATION_CLOSED_TL_COL(SVC_LOCATION_CLOSED_TL_COL.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_LOCATION_CLOSED_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_LOCATION_CLOSED_TL st
      using(select
                  (case
                   when l_mi_rec.reason_mi = 'N'
                    and SVC_LOCATION_CLOSED_TL_col(i).action = CORESVC_LOCATION_CLOSED.action_mod
                    and s1.reason IS NULL then
                        mt.reason
                   else s1.reason
                   end) as reason,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_LOCATION_CLOSED_TL_col(i).action = CORESVC_LOCATION_CLOSED.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.close_date_mi = 'N'
                    and SVC_LOCATION_CLOSED_TL_col(i).action = CORESVC_LOCATION_CLOSED.action_mod
                    and s1.close_date IS NULL then
                        mt.close_date
                   else s1.close_date
                   end) as close_date,
                  (case
                   when l_mi_rec.location_mi = 'N'
                    and SVC_LOCATION_CLOSED_TL_col(i).action = CORESVC_LOCATION_CLOSED.action_mod
                    and s1.location IS NULL then
                        mt.location
                   else s1.location
                   end) as location
              from (select SVC_LOCATION_CLOSED_TL_col(i).reason     as reason,
                           SVC_LOCATION_CLOSED_TL_col(i).close_date as close_date,
                           SVC_LOCATION_CLOSED_TL_col(i).location   as location,
                           SVC_LOCATION_CLOSED_TL_col(i).lang       as lang
                      from dual) s1,
                   location_closed_tl mt
             where mt.location (+) = s1.location
               and mt.close_date (+) = s1.close_date
               and mt.lang (+)       = s1.lang) sq
                on (st.location = sq.location and
                    st.close_date = sq.close_date and
                    st.lang = sq.lang and
                    SVC_LOCATION_CLOSED_TL_col(i).action in (coresvc_location_closed.action_mod,coresvc_location_closed.action_del))
      when matched then
      update
         set process_id        = SVC_LOCATION_CLOSED_TL_col(i).process_id ,
             chunk_id          = SVC_LOCATION_CLOSED_TL_col(i).chunk_id ,
             row_seq           = SVC_LOCATION_CLOSED_TL_col(i).row_seq ,
             action            = SVC_LOCATION_CLOSED_TL_col(i).action ,
             process$status    = SVC_LOCATION_CLOSED_TL_col(i).process$status ,
             reason            = sq.reason
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             reason ,
             close_date ,
             location ,
             lang)
      values(SVC_LOCATION_CLOSED_TL_COL(i).process_id ,
             SVC_LOCATION_CLOSED_TL_COL(i).chunk_id ,
             SVC_LOCATION_CLOSED_TL_COL(i).row_seq ,
             SVC_LOCATION_CLOSED_TL_COL(i).action ,
             SVC_LOCATION_CLOSED_TL_COL(i).process$status ,
             sq.reason ,
             sq.close_date ,
             sq.location ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            LOCATION_CLOSED_TL_SHEET,
                            SVC_LOCATION_CLOSED_TL_COL(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_LOCATION_CLOSED_TL;
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count   IN OUT NUMBER,
                      I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id    IN     NUMBER )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)  :=  'CORESVC_LOCATION_CLOSED.PROCESS_S9T';
   INVALID_FORMAT    EXCEPTION;
   PRAGMA            EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR          EXCEPTION;
   PRAGMA            EXCEPTION_INIT(MAX_CHAR, -01706);

   L_file            s9t_file;
   L_sheets          s9t_pkg.names_map_typ;
   L_process_status  SVC_PROCESS_TRACKER.STATUS%TYPE;
BEGIN
   commit;
   s9t_pkg.ods2obj(I_file_id);
   commit;

   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE   then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);

    if s9t_pkg.validate_template(I_file_id) = false then
       WRITE_S9T_ERROR( I_file_id,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                       'S9T_INVALID_TEMPLATE');
    else
       POPULATE_NAMES(I_file_id);

       sheet_name_trans := s9t_pkg.sheet_trans(L_file.template_key,
                                               L_file.user_lang);

       PROCESS_S9T_LOCATION_CLOSED(I_file_id,
                                   I_process_id);
       PROCESS_S9T_LOCATION_CLOSED_TL(I_file_id,
                                      I_process_id);
    end if;

   O_error_count := LP_s9t_errors_tab.COUNT();

   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   commit;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values lp_s9t_errors_tab(i);
         update svc_process_tracker
            set status  = 'PE',
                file_id = I_file_id
          where process_id = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_LOCATION_CLOSED_INS(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_location_closed_temp_rec  IN     LOCATION_CLOSED%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      :='CORESVC_LOCATION_CLOSED.EXEC_LOCATION_CLOSED_INS';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_LOCATION_CLOSED';

BEGIN

  insert into location_closed
       values I_location_closed_temp_rec;


   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_LOCATION_CLOSED_INS;
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_LOCATION_CLOSED_UPD(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_location_closed_temp_rec  IN      LOCATION_CLOSED%ROWTYPE )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                      :='CORESVC_LOCATION_CLOSED.EXEC_LOCATION_CLOSED_UPD';
   L_delete_choise   BOOLEAN                           := TRUE;
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_LOCATION_CLOSED';

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,-54);


   cursor C_LOCATION_CLOSED_LOCK is
      select 'X'
        from LOCATION_CLOSED
       where CLOSE_DATE = I_location_closed_temp_rec.CLOSE_DATE
         and LOCATION   = I_location_closed_temp_rec.LOCATION
      for update nowait;
BEGIN

   open C_LOCATION_CLOSED_LOCK;
   close C_LOCATION_CLOSED_LOCK;

   update LOCATION_CLOSED
      set row = I_location_closed_temp_rec
    where CLOSE_DATE = I_location_closed_temp_rec.close_date
      and LOCATION   = I_location_closed_temp_rec.location;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'LOCATION_CLOSED',
                                                                I_location_closed_temp_rec.close_date,
                                                                I_location_closed_temp_rec.location);
      return FALSE;

   when OTHERS then
      if C_LOCATION_CLOSED_LOCK%ISOPEN then
         close C_LOCATION_CLOSED_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_LOCATION_CLOSED_UPD;
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_LOCATION_CLOSED_DEL(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_location_closed_temp_rec  IN      LOCATION_CLOSED%ROWTYPE )
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      :='CORESVC_LOCATION_CLOSED.EXEC_LOCATION_CLOSED_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_LOCATION_CLOSED';

   cursor C_LOCATION_CLOSED_LOCK is
      select 'x'
        from location_closed
       where close_date = I_location_closed_temp_rec.close_date
         and location   = I_location_closed_temp_rec.location
      for update nowait;

   cursor C_LOCATION_CLOSED_TL_LOCK is
      select 'x'
        from location_closed_tl
       where close_date = I_location_closed_temp_rec.close_date
         and location   = I_location_closed_temp_rec.location
      for update nowait;
BEGIN
   open C_LOCATION_CLOSED_TL_LOCK;
   close C_LOCATION_CLOSED_TL_LOCK;

   delete from location_closed_tl
    where close_date = i_location_closed_temp_rec.close_date
      and location   = i_location_closed_temp_rec.location;

   open C_LOCATION_CLOSED_LOCK;
   close C_LOCATION_CLOSED_LOCK;

   delete from location_closed
    where close_date = i_location_closed_temp_rec.close_date
      and location   = i_location_closed_temp_rec.location;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'LOCATION_CLOSED',
                                                                I_location_closed_temp_rec.close_date,
                                                                I_location_closed_temp_rec.location);
      return FALSE;

   when OTHERS then
      if C_LOCATION_CLOSED_TL_LOCK%ISOPEN then
         close C_LOCATION_CLOSED_TL_LOCK;
      end if;
      if C_LOCATION_CLOSED_LOCK%ISOPEN then
         close C_LOCATION_CLOSED_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_LOCATION_CLOSED_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LOCATION_CLOSED_TL_INS(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_location_closed_tl_ins_tab    IN       LOCATION_CLOSED_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_LOCATION_CLOSED.EXEC_LOCATION_CLOSED_TL_INS';

BEGIN
   if I_location_closed_tl_ins_tab is NOT NULL and I_location_closed_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_location_closed_tl_ins_tab.COUNT()
         insert into location_closed_tl
              values I_location_closed_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_LOCATION_CLOSED_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LOCATION_CLOSED_TL_UPD(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_location_closed_tl_upd_tab   IN       LOCATION_CLOSED_TAB,
                                     I_loc_closed_tl_upd_rst        IN       ROW_SEQ_TAB,
                                     I_process_id                   IN       SVC_LOCATION_CLOSED_TL.PROCESS_ID%TYPE,
                                     I_chunk_id                     IN       SVC_LOCATION_CLOSED_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_LOCATION_CLOSED.EXEC_LOCATION_CLOSED_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LOCATION_CLOSED_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_LOCATION_CLOSED_TL_UPD(I_close_date     LOCATION_CLOSED_TL.CLOSE_DATE%TYPE,
                                        I_location       LOCATION_CLOSED_TL.LOCATION%TYPE,
                                        I_lang           LOCATION_CLOSED_TL.LANG%TYPE) is
      select 'x'
        from location_closed_tl
       where close_date = I_close_date
         and location = I_location
         and lang = I_lang
         for update nowait;

BEGIN
   if I_location_closed_tl_upd_tab is NOT NULL and I_location_closed_tl_upd_tab.count > 0 then
      for i in I_location_closed_tl_upd_tab.FIRST..I_location_closed_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_location_closed_tl_upd_tab(i).lang);
            L_key_val2 := 'Location: '||to_char(I_location_closed_tl_upd_tab(i).location);
            open C_LOCK_LOCATION_CLOSED_TL_UPD(I_location_closed_tl_upd_tab(i).close_date,
                                               I_location_closed_tl_upd_tab(i).location,
                                               I_location_closed_tl_upd_tab(i).lang);
            close C_LOCK_LOCATION_CLOSED_TL_UPD;
            
            update location_closed_tl
               set reason = I_location_closed_tl_upd_tab(i).reason,
                   last_update_id = I_location_closed_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_location_closed_tl_upd_tab(i).last_update_datetime
             where lang = I_location_closed_tl_upd_tab(i).lang
               and location = I_location_closed_tl_upd_tab(i).location
               and close_date = I_location_closed_tl_upd_tab(i).close_date;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_LOCATION_CLOSED_TL',
                           I_loc_closed_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_LOCATION_CLOSED_TL_UPD%ISOPEN then
         close C_LOCK_LOCATION_CLOSED_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_LOCATION_CLOSED_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LOCATION_CLOSED_TL_DEL(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_exec_location_closed_tl_del  IN       LOCATION_CLOSED_TAB,
                                     I_loc_closed_tl_del_rst        IN       ROW_SEQ_TAB,
                                     I_process_id                   IN       SVC_LOCATION_CLOSED_TL.PROCESS_ID%TYPE,
                                     I_chunk_id                     IN       SVC_LOCATION_CLOSED_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_LOCATION_CLOSED.EXEC_LOCATION_CLOSED_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LOCATION_CLOSED_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_LOCATION_CLOSED_TL_DEL(I_close_date     LOCATION_CLOSED_TL.CLOSE_DATE%TYPE,
                                        I_location       LOCATION_CLOSED_TL.LOCATION%TYPE,
                                        I_lang           LOCATION_CLOSED_TL.LANG%TYPE) is
      select 'x'
        from location_closed_tl
       where close_date = I_close_date
         and location = I_location
         and lang = I_lang
         for update nowait;

BEGIN
   if I_exec_location_closed_tl_del is NOT NULL and I_exec_location_closed_tl_del.count > 0 then
      for i in I_exec_location_closed_tl_del.FIRST..I_exec_location_closed_tl_del.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_exec_location_closed_tl_del(i).lang);
            L_key_val2 := 'Location: '||to_char(I_exec_location_closed_tl_del(i).location);
            open C_LOCK_LOCATION_CLOSED_TL_DEL(I_exec_location_closed_tl_del(i).close_date,
                                               I_exec_location_closed_tl_del(i).location,
                                               I_exec_location_closed_tl_del(i).lang);
            close C_LOCK_LOCATION_CLOSED_TL_DEL;
            
            delete location_closed_tl
             where lang = I_exec_location_closed_tl_del(i).lang
               and location = I_exec_location_closed_tl_del(i).location
               and close_date = I_exec_location_closed_tl_del(i).close_date;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_LOCATION_CLOSED_TL',
                           I_loc_closed_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_LOCATION_CLOSED_TL_DEL%ISOPEN then
         close C_LOCK_LOCATION_CLOSED_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_LOCATION_CLOSED_TL_DEL;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_LOC_VALUE(O_error_message  IN OUT  VARCHAR2,
                         O_error          IN OUT  BOOLEAN,
                         O_exist          IN OUT  BOOLEAN,
                         I_rec            IN OUT  C_SVC_LOCATION_CLOSED%ROWTYPE )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                      :='CORESVC_LOCATION_CLOSED.CHECK_LOC_VALUE';
   L_second_value    VARCHAR2(20)                      := NULL;
   L_loc_type        VARCHAR2(5)                       := NULL;
   L_company_exist   BOOLEAN;
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_LOCATION_CLOSED';
   L_group_desc      VARCHAR2(255);
BEGIN
   if I_rec.loc_type = 'W'   then
      L_loc_type := 'PW';
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION_TYPE( O_error_message,
                                                         O_exist,
                                                         L_group_desc,
                                                         L_second_value,
                                                         L_loc_type,
                                                         I_rec.location )  = FALSE then
      WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'LOCATION',
                     O_error_message);
         O_error := TRUE;
      end if;

      if O_exist = TRUE   then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'LOCATION',
                     'INV_VWH' );
         O_error := TRUE;
      elsif O_error_message <>'@0INV_PHYSICAL_WH'   then
            WRITE_ERROR( I_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'LOCATION',
                        'NO_VIS_HIERARCHY');
            O_error := TRUE;
      end if;

   end if;

   if I_rec.loc_type = 'S'   then
      if FILTER_LOV_VALIDATE_SQL.COMPANY_STORE(O_error_message,
                                               L_company_exist,
                                               L_group_desc,
                                               I_rec.location) = FALSE   then
         if O_error_message <> '@0INV_STORE'   then
            O_error := TRUE;
            WRITE_ERROR( I_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'LOCATION',
                        'NO_VIS_HIERARCHY');
         end if;
      end if;
      if L_company_exist = FALSE   then
         L_group_desc    := NULL;
         O_exist         := FALSE;
         O_error_message :='MUST_BE_COMP_STORE';
         O_error         := TRUE;
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'LOCATION',
                      O_error_message );
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_LOC_VALUE;

--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_LOCATION_CLOSED(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error         IN OUT  BOOLEAN,
                                     I_rec           IN OUT  C_SVC_LOCATION_CLOSED%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      :='CORESVC_LOCATION_CLOSED.PROCESS_VAL_LOCATION_CLOSED';
   L_stockholding_ind  WH.STOCKHOLDING_IND%TYPE;
   L_exist             BOOLEAN                           := NULL;
   L_status            BOOLEAN                           := FALSE;
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_LOCATION_CLOSED';
BEGIN
   if I_rec.action IN (action_new,action_mod,action_del)   then
      if I_rec.close_date < L_min_date   then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'CLOSE_DATE',
                     'INV_DATE_EARLY');
         O_error := TRUE;
      end if;
   end if;

   if I_rec.action IN (action_new, action_mod)   then
      if I_rec.sales_ind     = 'N'
         and I_rec.ship_ind  = 'N'
         and I_rec.recv_ind  = 'N'   then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'Close Date,Location',
                     'MUST_ENTER_CLOSE_TYPE');
         O_error :=TRUE;
      end if;
   end if;

   if (I_rec.action = action_new
      and I_rec.loc_type = 'W' )
      or (I_rec.action = action_mod
      and I_rec.loc_type = 'W'
      and I_rec.base_sales_ind <> NVL(I_rec.sales_ind,'-1'))   then
      if I_rec.sales_ind = 'Y'   then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'SALES_IND',
                     'IND_NOT_Y');
         O_error :=TRUE;
      end if;
   end if;

   if (I_rec.action = action_new
      and I_rec.location is NOT NULL
      and I_rec.loc_type IN ('S','W'))
      or (I_rec.action = action_mod
      and I_rec.location is NOT NULL
      and I_rec.loc_type IN ('S','W')
      and I_rec.pk_location_closed_rid is NOT NULL)   then

      if LOCATION_ATTRIB_SQL.CHECK_STOCKHOLDING(O_error_message,
                                                L_stockholding_ind,
                                                I_rec.location,
                                                I_rec.loc_type) = FALSE   then
         O_error := TRUE;
         L_status := TRUE;
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'LOCATION',
                      O_error_message );
      end if;

      if I_rec.action = action_new
         and L_status = FALSE   then
         if CHECK_LOC_VALUE(O_error_message,
                            O_error,
                            L_exist,
                            I_rec ) = FALSE   then
            return FALSE;
         end if;
      end if;

      if L_stockholding_ind is NOT NULL
         and I_rec.loc_type     = 'S'
         and L_stockholding_ind = 'N'   then

         if I_rec.action = action_mod   then
            if I_rec.base_recv_ind <> NVL(I_rec.recv_ind,'-1')   then
               WRITE_ERROR( I_rec.process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_rec.chunk_id,
                            L_table,
                            I_rec.row_seq,
                           'RECV_IND',
                           'CANT_UPD_NSTOCK');
               O_error :=TRUE;
            end if;

            if I_rec.base_ship_ind <> NVL(I_rec.ship_ind,'-1')   then
               WRITE_ERROR( I_rec.process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_rec.chunk_id,
                            L_table,
                            I_rec.row_seq,
                           'SHIP_IND',
                           'CANT_UPD_NSTOCK');
               O_error :=TRUE;
            end if;

         elsif I_rec.action = action_new   then
               if I_rec.recv_ind = 'Y'   then
                  WRITE_ERROR( I_rec.process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               I_rec.chunk_id,
                               L_table,
                               I_rec.row_seq,
                              'RECV_IND',
                              'NON_STOCK_Y');
                  O_error :=TRUE;
               end if;
               if I_rec.ship_ind = 'Y'   then
                  WRITE_ERROR( I_rec.process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               I_rec.chunk_id,
                               L_table,
                               I_rec.row_seq,
                              'SHIP_IND',
                              'NON_STOCK_Y');
                  O_error :=TRUE;
               end if;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_VAL_LOCATION_CLOSED;

------------------------------------------------------------------------------
FUNCTION PROCESS_LOCATION_CLOSED( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id    IN     SVC_LOCATION_CLOSED.PROCESS_ID%TYPE,
                                  I_chunk_id      IN     SVC_LOCATION_CLOSED.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                     BOOLEAN;
   L_location_closed_temp_rec  LOCATION_CLOSED%ROWTYPE;
   L_process_error             BOOLEAN                           := FALSE;
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_LOCATION_CLOSED';
BEGIN
   FOR rec IN c_svc_LOCATION_CLOSED(I_process_id,I_chunk_id)
   LOOP
      L_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_LOCATION_CLOSED_rid is NOT NULL   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'Close Date,Location',
                     'CLOSE_DATE_EXISTS');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod, action_del)
         and rec.PK_LOCATION_CLOSED_rid is NULL
         and rec.LOCATION    is NOT NULL
         and rec.CLOSE_DATE  is NOT NULL then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'Close Date,Location',
                     'NO_RECORD');
         L_error :=TRUE;
      end if;

      if NOT( rec.LOCATION is NOT NULL ) then
              WRITE_ERROR( I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                          'LOCATION',
                          'ENTER_LOC');
              L_error := TRUE;
      end if;

      if NOT(  rec.CLOSE_DATE is NOT NULL ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'CLOSE_DATE',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
      end if;

      if rec.action IN (action_new, action_mod)   then

         if (rec.action = action_new
             or (rec.action = action_mod
             and NVL(rec.base_ship_ind,'-2') <> NVL(rec.ship_ind,'-1')))   then
            if rec.SHIP_IND is NULL
               or rec.SHIP_IND NOT IN ('Y','N')   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                           'SHIP_IND',
                           'INV_Y_N_IND');
               L_error :=TRUE;
            end if;
         end if;

         if (rec.action = action_new
             or ( rec.action = action_mod
             and NVL( rec.base_recv_ind,'-2') <> NVL(rec.recv_ind,'-1')))   then
             if rec.RECV_IND is NULL
                or rec.RECV_IND NOT IN ( 'Y','N' )   then
                WRITE_ERROR( I_process_id,
                             svc_admin_upld_er_seq.NEXTVAL,
                             I_chunk_id,
                             L_table,
                             rec.row_seq,
                            'RECV_IND',
                            'INV_Y_N_IND');
                L_error :=TRUE;
             end if;
         end if;

         if (rec.action = action_new
             or (rec.action = action_mod
             and NVL( rec.base_sales_ind,'-2') <> NVL(rec.sales_ind,'-1'))) then
             if rec.sales_ind is NULL
                or rec.sales_ind NOT IN ( 'Y','N' )   then
                WRITE_ERROR( I_process_id,
                             svc_admin_upld_er_seq.NEXTVAL,
                             I_chunk_id,
                             L_table,
                             rec.row_seq,
                            'SALES_IND',
                            'INV_Y_N_IND');
                L_error :=TRUE;
             end if;
         end if;

         if (rec.action = action_new
            or (rec.action = action_mod
            and NVL(rec.base_loc_type,'-2') <> NVL(rec.loc_type,'-1')))  then
            if rec.loc_type is NULL
               or rec.loc_type NOT IN ( 'S','W' )   then
               WRITE_ERROR( I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                          'LOC_TYPE',
                          'INVALID_LOC_TYPE_S_W');
               L_error :=TRUE;
            end if;
         end if;
      end if;
      if PROCESS_VAL_LOCATION_CLOSED(O_error_message,
                                     L_error,
                                     rec ) = FALSE   then
         WRITE_ERROR( I_process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     NULL,
                      O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error   then
         L_location_closed_temp_rec.LOCATION    := rec.LOCATION;
         L_location_closed_temp_rec.CLOSE_DATE  := rec.CLOSE_DATE;
         L_location_closed_temp_rec.LOC_TYPE    := rec.LOC_TYPE;
         L_location_closed_temp_rec.SALES_IND   := rec.SALES_IND;
         L_location_closed_temp_rec.RECV_IND    := rec.RECV_IND;
         L_location_closed_temp_rec.SHIP_IND    := rec.SHIP_IND;
         L_location_closed_temp_rec.REASON      := rec.REASON;

         if rec.action = action_new   then
            if(EXEC_LOCATION_CLOSED_INS(O_error_message,
                                        L_location_closed_temp_rec ) = FALSE )   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod   then
            if(EXEC_LOCATION_CLOSED_UPD(O_error_message,
                                        L_location_closed_temp_rec ) = FALSE ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del   then
            if(EXEC_LOCATION_CLOSED_DEL(O_error_message,
                                        L_location_closed_temp_rec ) = FALSE ) then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_LOCATION_CLOSED.PROCESS',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_LOCATION_CLOSED;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_LOCATION_CLOSED_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_process_id      IN       SVC_LOCATION_CLOSED_TL.PROCESS_ID%TYPE,
                                    I_chunk_id        IN       SVC_LOCATION_CLOSED_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_LOCATION_CLOSED.PROCESS_LOCATION_CLOSED_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LOCATION_CLOSED_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LOCATION_CLOSED';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_LOCATION_CLOSED_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_location_closed_tl_temp_rec    LOCATION_CLOSED_TL%ROWTYPE;
   L_loc_closed_tl_upd_rst    ROW_SEQ_TAB;
   L_loc_closed_tl_del_rst    ROW_SEQ_TAB;

   cursor C_SVC_LOCATION_CLOSED_TL(I_process_id NUMBER,
                                   I_chunk_id NUMBER) is
      select pk_location_closed_tl.rowid  as pk_location_closed_tl_rid,
             fk_location_closed.rowid     as fk_location_closed_rid,
             fk_lang.rowid                as fk_lang_rid,
             st.lang,
             st.location,
             st.close_date,
             st.reason,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_location_closed_tl  st,
             location_closed         fk_location_closed,
             location_closed_tl      pk_location_closed_TL,
             lang                    fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.location    =  fk_location_closed.location (+)
         and st.close_date  =  fk_location_closed.close_date (+)
         and st.lang        =  pk_location_closed_TL.lang (+)
         and st.location    =  pk_location_closed_TL.location (+)
         and st.close_date  =  pk_location_closed_TL.close_date (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_LOCATION_CLOSED_TL is TABLE OF C_SVC_LOCATION_CLOSED_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_location_closed_tab        SVC_LOCATION_CLOSED_TL;

   L_location_closed_TL_ins_tab         location_closed_tab         := NEW location_closed_tab();
   L_location_closed_TL_upd_tab         location_closed_tab         := NEW location_closed_tab();
   L_location_closed_TL_del_tab         location_closed_tab         := NEW location_closed_tab();

BEGIN
   if C_SVC_LOCATION_CLOSED_TL%ISOPEN then
      close C_SVC_LOCATION_CLOSED_TL;
   end if;

   open C_SVC_LOCATION_CLOSED_TL(I_process_id,
                                 I_chunk_id);
   LOOP
      fetch C_SVC_LOCATION_CLOSED_TL bulk collect into L_svc_location_closed_tab limit LP_bulk_fetch_limit;
      if L_svc_location_closed_tab.COUNT > 0 then
         FOR i in L_svc_location_closed_tab.FIRST..L_svc_location_closed_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_location_closed_tab(i).action is NULL
               or L_svc_location_closed_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_location_closed_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_location_closed_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_location_closed_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_location_closed_tab(i).action = action_new
               and L_svc_location_closed_tab(i).pk_location_closed_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_location_closed_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_location_closed_tab(i).action IN (action_mod, action_del)
               and L_svc_location_closed_tab(i).lang is NOT NULL
               and L_svc_location_closed_tab(i).location is NOT NULL
               and L_svc_location_closed_tab(i).close_date is NOT NULL
               and L_svc_location_closed_tab(i).pk_location_closed_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_location_closed_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_location_closed_tab(i).action = action_new
               and L_svc_location_closed_tab(i).location is NOT NULL
               and L_svc_location_closed_tab(i).close_date is NOT NULL
               and L_svc_location_closed_tab(i).fk_location_closed_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_location_closed_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_location_closed_tab(i).action = action_new
               and L_svc_location_closed_tab(i).lang is NOT NULL
               and L_svc_location_closed_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_location_closed_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_location_closed_tab(i).action in (action_new, action_mod) then
               if L_svc_location_closed_tab(i).reason is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_location_closed_tab(i).row_seq,
                              'REASON',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_location_closed_tab(i).close_date is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_location_closed_tab(i).row_seq,
                           'CLOSE_DATE',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_location_closed_tab(i).location is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_location_closed_tab(i).row_seq,
                           'LOCATION',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_location_closed_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_location_closed_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_location_closed_tl_temp_rec.lang := L_svc_location_closed_tab(i).lang;
               L_location_closed_tl_temp_rec.location := L_svc_location_closed_tab(i).location;
               L_location_closed_tl_temp_rec.close_date := L_svc_location_closed_tab(i).close_date;
               L_location_closed_tl_temp_rec.reason := L_svc_location_closed_tab(i).reason;
               L_location_closed_tl_temp_rec.create_datetime := SYSDATE;
               L_location_closed_tl_temp_rec.create_id := GET_USER;
               L_location_closed_tl_temp_rec.last_update_datetime := SYSDATE;
               L_location_closed_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_location_closed_tab(i).action = action_new then
                  L_location_closed_TL_ins_tab.extend;
                  L_location_closed_TL_ins_tab(L_location_closed_TL_ins_tab.count()) := L_location_closed_tl_temp_rec;
               end if;

               if L_svc_location_closed_tab(i).action = action_mod then
                  L_location_closed_TL_upd_tab.extend;
                  L_location_closed_TL_upd_tab(L_location_closed_TL_upd_tab.count()) := L_location_closed_tl_temp_rec;
                  L_loc_closed_tl_upd_rst(L_location_closed_TL_upd_tab.count()) := L_svc_location_closed_tab(i).row_seq;
               end if;

               if L_svc_location_closed_tab(i).action = action_del then
                  L_location_closed_TL_del_tab.extend;
                  L_location_closed_TL_del_tab(L_location_closed_TL_del_tab.count()) := L_location_closed_tl_temp_rec;
                  L_loc_closed_tl_del_rst(L_location_closed_TL_del_tab.count()) := L_svc_location_closed_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_LOCATION_CLOSED_TL%NOTFOUND;
   END LOOP;
   close C_SVC_LOCATION_CLOSED_TL;

   if EXEC_LOCATION_CLOSED_TL_INS(O_error_message,
                                  L_location_closed_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_LOCATION_CLOSED_TL_UPD(O_error_message,
                                  L_location_closed_TL_upd_tab,
                                  L_loc_closed_tl_upd_rst,
                                  I_process_id,
                                  I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_LOCATION_CLOSED_TL_DEL(O_error_message,
                                  L_location_closed_TL_del_tab,
                                  L_loc_closed_tl_del_rst,
                                  I_process_id,
                                  I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_LOCATION_CLOSED_TL;
----------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_LOCATION_CLOSED.PROCESS_ID%TYPE) IS

BEGIN
   delete
     from svc_location_closed_tl
    where process_id= I_process_id;

   delete
     from svc_location_closed
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;
--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count    IN OUT  NUMBER,
                  I_process_id     IN      NUMBER,
                  I_chunk_id       IN      NUMBER)
RETURN BOOLEAN is
   L_program          VARCHAR2(64) := 'CORESVC_LOCATION_CLOSED.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';


BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_LOCATION_CLOSED(O_error_message,
                              I_process_id,
                              I_chunk_id) = FALSE then
       return FALSE;
   end if;
   if PROCESS_LOCATION_CLOSED_TL(O_error_message,
                                 I_process_id,
                                 I_chunk_id) = FALSE   then
       return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_LOCATION_CLOSED;
/
