create or replace PACKAGE BODY CORESVC_LOC_TRAITS AS
----------------------------------------------------------------
   cursor C_SVC_LOC_TRAITS(I_process_id NUMBER,
                           I_chunk_id NUMBER) is
      select pk_loc_traits.rowid         as pk_loc_traits_rid,
             st.description,
             pk_loc_traits.description   as old_description,
             st.filter_org_id,
             pk_loc_traits.filter_org_id as old_filter_org_id,
             st.loc_trait,
             st.process_id,
             st.row_seq,
             st.rowid                    as st_rid,
             st.chunk_id,
             ch.chain_name,
             UPPER(st.action)            as action,
             st.process$status
        from svc_loc_traits st,
             loc_traits pk_loc_traits,
             v_chain ch
       where st.process_id    = I_process_id
         and st.chunk_id      = I_chunk_id
         and st.filter_org_id = ch.chain(+)
         and st.loc_trait     = pk_loc_traits.loc_trait (+);

   TYPE ERRORS_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab ERRORS_tab_typ;
   TYPE s9t_ERRORS_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_ERRORS_tab_typ;

   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
----------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
---------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets           s9t_pkg.names_map_typ;
   LOC_TRAITS_cols    s9t_pkg.names_map_typ;
   LOC_TRAITS_TL_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                     := S9T_PKG.get_sheet_names(I_file_id);
   LOC_TRAITS_cols              := S9T_PKG.get_coL_names(I_file_id,LOC_TRAITS_sheet);
   LOC_TRAITS$Action            := LOC_TRAITS_cols('ACTION');
   LOC_TRAITS$DESCRIPTION       := LOC_TRAITS_cols('DESCRIPTION');
   LOC_TRAITS$FILTER_ORG_ID     := LOC_TRAITS_cols('FILTER_ORG_ID');
   LOC_TRAITS$LOC_TRAIT         := LOC_TRAITS_cols('LOC_TRAIT');

   LOC_TRAITS_TL_cols              := S9T_PKG.get_coL_names(I_file_id,LOC_TRAITS_TL_SHEET);
   LOC_TRAITS_TL$Action            := LOC_TRAITS_TL_cols('ACTION');
   LOC_TRAITS_TL$LANG              := LOC_TRAITS_TL_cols('LANG');
   LOC_TRAITS_TL$LOC_TRAIT         := LOC_TRAITS_TL_cols('LOC_TRAIT');
   LOC_TRAITS_TL$DESCRIPTION       := LOC_TRAITS_TL_cols('DESCRIPTION');

END POPULATE_NAMES;
---------------------------------------------------------------------------------
PROCEDURE POPULATE_LOC_TRAITS(I_file_id IN NUMBER) IS
BEGIN
   insert into TABLE
     (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id  = I_file_id
         and ss.sheet_name = LOC_TRAITS_sheet )
      select s9t_row(s9t_cells(CORESVC_LOC_TRAITS.action_mod,
                               loc_trait,
                               filter_org_id,
                               description))
        from loc_traits ;
END POPULATE_LOC_TRAITS;
---------------------------------------------------------------------------------
PROCEDURE POPULATE_LOC_TRAITS_TL(I_file_id IN NUMBER) IS
BEGIN
   insert into TABLE
     (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id  = I_file_id
         and ss.sheet_name = LOC_TRAITS_TL_SHEET )
      select s9t_row(s9t_cells(CORESVC_LOC_TRAITS.action_mod,
                               lang,
                               loc_trait,
                               description))
        from loc_traits_TL ;
END POPULATE_LOC_TRAITS_TL;
------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id IN OUT NUMBER) IS
   L_file S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW S9T_FILE();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(LOC_TRAITS_sheet);
   L_file.sheets(L_file.get_sheet_index(LOC_TRAITS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'LOC_TRAIT',
                                                                                        'FILTER_ORG_ID',
                                                                                        'DESCRIPTION');
   S9T_PKG.SAVE_OBJ(L_file);

   L_file.add_sheet(LOC_TRAITS_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(LOC_TRAITS_TL_SHEET)).column_headers := s9t_cells( 'ACTION',
                                                                                           'LANG',
                                                                                           'LOC_TRAIT',
                                                                                           'DESCRIPTION');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id           IN OUT S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind IN CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_LOC_TRAITS.CREATE_S9T';
   L_file    S9T_FILE;
BEGIN
   INIT_S9T(O_file_id);--13835
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_LOC_TRAITS(O_file_id);
      POPULATE_LOC_TRAITS_TL(O_file_id);
   commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_LOC_TRAITS( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id IN   SVC_LOC_TRAITS.PROCESS_ID%TYPE) IS

   TYPE SVC_LOC_TRAITS_COL_TYP IS TABLE OF SVC_LOC_TRAITS%ROWTYPE;
   L_temp_rec                              SVC_LOC_TRAITS%ROWTYPE;
   svc_LOC_TRAITS_col                      svc_LOC_TRAITS_coL_typ :=NEW svc_LOC_TRAITS_coL_typ();
   L_process_id                            SVC_LOC_TRAITS.PROCESS_ID%TYPE;
   L_error                                 BOOLEAN                :=FALSE;
   L_default_rec                           SVC_LOC_TRAITS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select DESCRIPTION_mi,
             FILTER_ORG_ID_mi,
             LOC_TRAIT_mi,
             1 as dummy
        from (SELECT column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key    = CORESVC_LOC_TRAITS.template_key
                 and wksht_key       = 'Location Trait'
              )PIVOT (MAX(mandatory) as mi
                      FOR (column_key) IN ('description'   as description,
                                           'filter_org_id' as filter_org_id,
                                           'loc_trait'     as loc_trait,
                                            NULL           as dummy));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_LOC_TRAITS';
   L_pk_columns    VARCHAR2(255)  := 'Location Trait';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   FOR rec IN (select DESCRIPTION_dv,
                      FILTER_ORG_ID_dv,
                      LOC_TRAIT_dv,
                      null as dummy
                 from (SELECT column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key  = CORESVC_LOC_TRAITS.template_key
                          and wksht_key     = 'LOC_TRAITS'
                       )PIVOT (MAX(default_value) as dv
                               FOR (column_key) IN ('description' as description,
                                                    'filter_org_id' as filter_org_id,
                                                    'loc_trait' as loc_trait,
                                                     NULL as dummy)))
   LOOP
   BEGIN
      L_default_rec.DESCRIPTION := rec.DESCRIPTION_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LOC_TRAITS',
                         NULL,
                         'DESCRIPTION',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.FILTER_ORG_ID := rec.FILTER_ORG_ID_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LOC_TRAITS',
                         NULL,
                         'FILTER_ORG_ID',
                         NULL,
                         'INV_DEFAULT');
   END;
   BEGIN
      L_default_rec.LOC_TRAIT := rec.LOC_TRAIT_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'LOC_TRAITS',
                         NULL,
                         'LOC_TRAIT',
                         NULL,
                         'INV_DEFAULT');
   END;
   END LOOP;
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
      (select r.get_cell(LOC_TRAITS$Action)                   as action,
              r.get_cell(LOC_TRAITS$DESCRIPTION)              as description,
              r.get_cell(LOC_TRAITS$FILTER_ORG_ID)            as filter_org_id,
              r.get_cell(LOC_TRAITS$LOC_TRAIT)                as loc_trait,
              r.get_row_seq()                                 as row_seq
         from s9t_folder sf,
        TABLE(sf.s9t_file_obj.sheets) ss,
        TABLE(ss.s9t_rows) r
        where sf.file_id  = I_file_id
          and ss.sheet_name = sheet_name_trans(LOC_TRAITS_sheet))
   LOOP
   L_temp_rec                   := NULL;
	L_temp_rec.process_id        := I_process_id;
   L_temp_rec.chunk_id          := 1;
   L_temp_rec.row_seq           := rec.row_seq;
   L_temp_rec.process$status    := 'N';
   L_temp_rec.create_id         := GET_USER;
   L_temp_rec.last_upd_id       := GET_USER;
   L_temp_rec.create_datetime   := SYSDATE;
   L_temp_rec.last_upd_datetime := SYSDATE;
   L_error := FALSE;
   BEGIN
      L_temp_rec.action := rec.action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         LOC_TRAITS_sheet,
                         rec.row_seq,
                         action_column,
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.description := rec.description;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         LOC_TRAITS_sheet,
                         rec.row_seq,
                         'DESCRIPTION',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.filter_org_id := rec.filter_org_id;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         LOC_TRAITS_sheet,
                         rec.row_seq,
                         'FILTER_ORG_ID',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.loc_trait := rec.loc_trait;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         LOC_TRAITS_sheet,
                         rec.row_seq,
                         'LOC_TRAIT',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
   END;
   if rec.action = CORESVC_LOC_TRAITS.action_new then
      L_temp_rec.DESCRIPTION   := NVL( L_temp_rec.DESCRIPTION,
                                       L_default_rec.DESCRIPTION);
      L_temp_rec.FILTER_ORG_ID := NVL( L_temp_rec.FILTER_ORG_ID,
                                       L_default_rec.FILTER_ORG_ID);
      L_temp_rec.LOC_TRAIT     := NVL( L_temp_rec.LOC_TRAIT,
                                       L_default_rec.LOC_TRAIT);
   end if;

   if rec.action IN (action_mod,action_del)
      and NOT(L_temp_rec.loc_trait is NOT NULL)then
      WRITE_S9T_ERROR( I_file_id,
                       LOC_TRAITS_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
      L_error := TRUE;
   end if;

   if NOT L_error then
      svc_LOC_TRAITS_col.extend();
      svc_LOC_TRAITS_col(svc_LOC_TRAITS_col.COUNT()):=L_temp_rec;
   end if;
   END LOOP;
   BEGIN
     forall i IN 1..svc_LOC_TRAITS_col.COUNT SAVE EXCEPTIONS
        merge into svc_loc_traits st
        using (select (case
                          when L_mi_rec.DESCRIPTION_mi    = 'N'
                           and svc_LOC_TRAITS_col(i).action = CORESVC_LOC_TRAITS.action_mod
                           and s1.description             is NULL
                          then mt.description
                          else s1.description
                          end) as description,

                      (case
                          when L_mi_rec.FILTER_ORG_ID_mi    = 'N'
                          AND svc_LOC_TRAITS_col(i).action = CORESVC_LOC_TRAITS.action_mod
                          and s1.filter_org_id             is NULL
                          then mt.filter_org_id
                          else s1.filter_org_id
                          end) as filter_org_id,
                      (case
                          when L_mi_rec.LOC_TRAIT_mi    = 'N'
                          AND svc_LOC_TRAITS_col(i).action = CORESVC_LOC_TRAITS.action_mod
                          and s1.loc_trait             is NULL
                          then mt.loc_trait
                          else s1.loc_trait
                          end) as loc_trait,
                      NULL as dummy from (select svc_LOC_TRAITS_col(i).description as description,
                                                 svc_LOC_TRAITS_col(i).filter_org_id as filter_org_id,
                                                 svc_LOC_TRAITS_col(i).loc_trait as loc_trait,
                                                 NULL as dummy
                                            from dual) s1,
                                                 LOC_TRAITS mt
                                           where mt.LOC_TRAIT (+)     = s1.LOC_TRAIT
                                             and 1 = 1) sq
           on (st.loc_trait      = sq.loc_trait
               and svc_LOC_TRAITS_col(i).action in (CORESVC_LOC_TRAITS.action_mod,
                                                    CORESVC_LOC_TRAITS.action_del))
         when matched then
            update
               set process_id        = svc_LOC_TRAITS_col(i).process_id ,
                   chunk_id          = svc_LOC_TRAITS_col(i).chunk_id ,
                   row_seq           = svc_LOC_TRAITS_col(i).row_seq ,
                   action            = svc_LOC_TRAITS_col(i).action,
                   process$status    = svc_LOC_TRAITS_col(i).process$status ,
                   description       = sq.description ,
                   filter_org_id     = sq.filter_org_id ,
                   create_id         = svc_LOC_TRAITS_col(i).create_id ,
                   create_datetime   = svc_LOC_TRAITS_col(i).create_datetime ,
                   last_upd_id       = svc_LOC_TRAITS_col(i).last_upd_id ,
                   last_upd_datetime = svc_LOC_TRAITS_col(i).last_upd_datetime
         when NOT matched then
            INSERT (process_id ,
                    chunk_id ,
                    row_seq ,
                    action ,
                    process$status ,
                    description ,
                    filter_org_id ,
                    loc_trait ,
                    create_id ,
                    create_datetime ,
                    last_upd_id ,
                    last_upd_datetime) values (svc_LOC_TRAITS_col(i).process_id ,
                                               svc_LOC_TRAITS_col(i).chunk_id ,
                                               svc_LOC_TRAITS_col(i).row_seq ,
                                               svc_LOC_TRAITS_col(i).action ,
                                               svc_LOC_TRAITS_col(i).process$status ,
                                               sq.description ,
                                               sq.filter_org_id ,
                                               sq.loc_trait ,
                                               svc_LOC_TRAITS_col(i).create_id ,
                                               svc_LOC_TRAITS_col(i).create_datetime ,
                                               svc_LOC_TRAITS_col(i).last_upd_id ,
                                               svc_LOC_TRAITS_col(i).last_upd_datetime);
   EXCEPTION
      when DML_errorS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             LOC_TRAITS_sheet,
                             svc_LOC_TRAITS_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_LOC_TRAITS;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_LOC_TRAITS_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_LOC_TRAITS_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_LOC_TRAITS_TL_COL_TYP IS TABLE OF SVC_LOC_TRAITS_TL%ROWTYPE;
   L_temp_rec              SVC_LOC_TRAITS_TL%ROWTYPE;
   SVC_LOC_TRAITS_TL_COL   SVC_LOC_TRAITS_TL_COL_TYP := NEW SVC_LOC_TRAITS_TL_COL_TYP();
   L_process_id            SVC_LOC_TRAITS_TL.PROCESS_ID%TYPE;
   L_error                 BOOLEAN := FALSE;
   L_default_rec           SVC_LOC_TRAITS_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select description_mi,
             loc_trait_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'LOC_TRAITS_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('DESCRIPTION' as description,
                                       'LOC_TRAIT'   as loc_trait,
                                       'LANG'        as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_LOC_TRAITS_TL';
   L_pk_columns    VARCHAR2(255)  := 'Location Trait, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select description_dv,
                      loc_trait_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'LOC_TRAITS_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('DESCRIPTION' as description,
                                                'LOC_TRAIT'   as loc_trait,
                                                'LANG'        as lang)))
   LOOP
      BEGIN
         L_default_rec.description := rec.description_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOC_TRAITS_TL_SHEET ,
                            NULL,
                           'DESCRIPTION' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.loc_trait := rec.loc_trait_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           LOC_TRAITS_TL_SHEET ,
                            NULL,
                           'LOC_TRAIT' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOC_TRAITS_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(loc_traits_TL$action)         as action,
                      r.get_cell(loc_traits_TL$description)    as description,
                      r.get_cell(loc_traits_TL$loc_trait)      as loc_trait,
                      r.get_cell(loc_traits_TL$lang)           as lang,
                      r.get_row_seq()                          as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(LOC_TRAITS_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOC_TRAITS_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.description := rec.description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOC_TRAITS_TL_SHEET,
                            rec.row_seq,
                            'DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.loc_trait := rec.loc_trait;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOC_TRAITS_TL_SHEET,
                            rec.row_seq,
                            'LOC_TRAIT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            LOC_TRAITS_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_LOC_TRAITS.action_new then
         L_temp_rec.description := NVL( L_temp_rec.description,L_default_rec.description);
         L_temp_rec.loc_trait   := NVL( L_temp_rec.loc_trait,L_default_rec.loc_trait);
         L_temp_rec.lang        := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.loc_trait is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         LOC_TRAITS_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_LOC_TRAITS_TL_col.extend();
         SVC_LOC_TRAITS_TL_col(SVC_LOC_TRAITS_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..SVC_LOC_TRAITS_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_LOC_TRAITS_TL st
      using(select
                  (case
                   when l_mi_rec.description_mi = 'N'
                    and SVC_LOC_TRAITS_TL_col(i).action = CORESVC_LOC_TRAITS.action_mod
                    and s1.description IS NULL then
                        mt.description
                   else s1.description
                   end) as description,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and SVC_LOC_TRAITS_TL_col(i).action = CORESVC_LOC_TRAITS.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.loc_trait_mi = 'N'
                    and SVC_LOC_TRAITS_TL_col(i).action = CORESVC_LOC_TRAITS.action_mod
                    and s1.loc_trait IS NULL then
                        mt.loc_trait
                   else s1.loc_trait
                   end) as loc_trait
              from (select SVC_LOC_TRAITS_TL_col(i).description as description,
                           SVC_LOC_TRAITS_TL_col(i).loc_trait        as loc_trait,
                           SVC_LOC_TRAITS_TL_col(i).lang              as lang
                      from dual) s1,
                   loc_traits_TL mt
             where mt.loc_trait (+) = s1.loc_trait
               and mt.lang (+)       = s1.lang) sq
                on (st.loc_trait = sq.loc_trait and
                    st.lang = sq.lang and
                    SVC_LOC_TRAITS_TL_col(i).ACTION IN (CORESVC_LOC_TRAITS.action_mod,CORESVC_LOC_TRAITS.action_del))
      when matched then
      update
         set process_id        = SVC_LOC_TRAITS_TL_col(i).process_id ,
             chunk_id          = SVC_LOC_TRAITS_TL_col(i).chunk_id ,
             row_seq           = SVC_LOC_TRAITS_TL_col(i).row_seq ,
             action            = SVC_LOC_TRAITS_TL_col(i).action ,
             process$status    = SVC_LOC_TRAITS_TL_col(i).process$status ,
             description       = sq.description
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             description ,
             loc_trait ,
             lang)
      values(SVC_LOC_TRAITS_TL_col(i).process_id ,
             SVC_LOC_TRAITS_TL_col(i).chunk_id ,
             SVC_LOC_TRAITS_TL_col(i).row_seq ,
             SVC_LOC_TRAITS_TL_col(i).action ,
             SVC_LOC_TRAITS_TL_col(i).process$status ,
             sq.description ,
             sq.loc_trait ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            LOC_TRAITS_TL_SHEET,
                            SVC_LOC_TRAITS_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_LOC_TRAITS_TL;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN  OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count        OUT NUMBER,
                     I_file_id        IN      S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id     IN      NUMBER)
   RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_LOC_TRAITS.PROCESS_S9T';
   L_file           S9T_FILE;
   L_sheets         S9T_PKG.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR         EXCEPTION;
   PRAGMA           EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   commit;
   s9t_pkg.ODS2OBJ(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(l_file);
   if s9t_pkg.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.sheet_trans(l_file.template_key,l_file.user_lang);
      PROCESS_S9T_LOC_TRAITS(I_file_id,I_process_id);
      PROCESS_S9T_LOC_TRAITS_TL(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_ERRORS_tab_typ();

   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   commit;

   return TRUE;
EXCEPTION
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
           values Lp_s9t_errors_tab(i);

      update svc_process_tracker
      set status = 'PE',
          file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;

--------------------------------------------------------------------------------------
FUNCTION EXEC_LOC_TRAITS_INS( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_loc_traits_temp_rec   IN       LOC_TRAITS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                       := 'CORESVC_LOC_TRAITS.EXEC_LOC_TRAITS_INS';
BEGIN
   insert into loc_traits
        values I_loc_traits_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END EXEC_LOC_TRAITS_INS;
----------------------------------------------------------------------------------------
FUNCTION EXEC_LOC_TRAITS_UPD( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_loc_traits_temp_rec   IN       LOC_TRAITS%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64) := 'CORESVC_LOC_TRAITS.EXEC_LOC_TRAITS_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_LOC_TRAITS_LOCK is
      select 'X'
        from loc_traits
       where loc_trait = I_loc_traits_temp_rec.loc_trait
         for update nowait;
BEGIN
   open  C_LOC_TRAITS_LOCK;
   close C_LOC_TRAITS_LOCK;

   update loc_traits
      set row = I_loc_traits_temp_rec
    where loc_trait = I_loc_traits_temp_rec.loc_trait;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'LOC_TRAITS',
                                                                I_loc_traits_temp_rec.loc_trait,
                                                                NULL);

      return FALSE;

   when OTHERS then
      if C_LOC_TRAITS_LOCK%ISOPEN then
         close C_LOC_TRAITS_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END EXEC_LOC_TRAITS_UPD;
------------------------------------------------------------------------------------------
FUNCTION EXEC_LOC_TRAITS_DEL( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_loc_traits_temp_rec   IN       LOC_TRAITS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_LOC_TRAITS.EXEC_LOC_TRAITS_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_LOC_TRAITS_TL_LOCK is
      select 'X'
        from loc_traits_tl
       where loc_trait = I_loc_traits_temp_rec.loc_trait
         for update nowait;

   cursor C_LOC_TRAITS_LOCK is
      select 'X'
        from loc_traits
       where loc_trait = I_loc_traits_temp_rec.loc_trait
         for update nowait;
BEGIN
   open  C_LOC_TRAITS_TL_LOCK;
   close C_LOC_TRAITS_TL_LOCK;

   delete
     from loc_traits_tl
    where loc_trait = I_loc_traits_temp_rec.loc_trait;

   open  C_LOC_TRAITS_LOCK;
   close C_LOC_TRAITS_LOCK;

   delete
     from loc_traits
    where loc_trait = I_loc_traits_temp_rec.loc_trait;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'LOC_TRAITS',
                                                                I_loc_traits_temp_rec.loc_trait,
                                                                NULL);

      return FALSE;

   when OTHERS then
      if C_LOC_TRAITS_LOCK%ISOPEN then
         close C_LOC_TRAITS_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END EXEC_LOC_TRAITS_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LOC_TRAITS_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_loc_traits_tl_rec        IN       LOC_TRAITS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_LOC_TRAITS.EXEC_LOC_TRAITS_TL_INS';

BEGIN
   insert into loc_traits_tl
        values I_loc_traits_tl_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_LOC_TRAITS_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LOC_TRAITS_TL_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_loc_traits_tl_rec  IN       LOC_TRAITS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_LOC_TRAITS.EXEC_LOC_TRAITS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LOC_TRAITS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := 'Loc Trait '||I_loc_traits_tl_rec.loc_trait;
   L_key_val2      VARCHAR2(30) := 'Lang '||I_loc_traits_tl_rec.lang;

    --Cursor to lock the record
   cursor C_LOCK_LOC_TRAITS_TL_UPD is
      select 'x'
        from loc_traits_tl
       where loc_trait = I_loc_traits_tl_rec.loc_trait
         and lang = I_loc_traits_tl_rec.lang
         for update nowait;

BEGIN
   open C_LOCK_LOC_TRAITS_TL_UPD;
   close C_LOCK_LOC_TRAITS_TL_UPD;
   
   update loc_traits_tl
      set row = I_loc_traits_tl_rec
    where lang = I_loc_traits_tl_rec.lang
      and loc_trait = I_loc_traits_tl_rec.loc_trait;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_LOC_TRAITS_TL_UPD%ISOPEN then
         close C_LOCK_LOC_TRAITS_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_LOC_TRAITS_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_LOC_TRAITS_TL_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_loc_traits_tl_rec   IN       LOC_TRAITS_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_LOC_TRAITS.EXEC_LOC_TRAITS_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'EXEC_LOC_TRAITS_TL_DEL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := 'Loc Trait '||I_loc_traits_tl_rec.loc_trait;
   L_key_val2      VARCHAR2(30) := 'Lang '||I_loc_traits_tl_rec.lang;

    --Cursor to lock the record
   cursor C_LOCK_LOC_TRAITS_TL_DEL is
      select 'x'
        from loc_traits_tl
       where loc_trait = I_loc_traits_tl_rec.loc_trait
         and lang = I_loc_traits_tl_rec.lang
         for update nowait;

BEGIN
   open C_LOCK_LOC_TRAITS_TL_DEL;
   close C_LOCK_LOC_TRAITS_TL_DEL;

   delete loc_traits_tl
    where lang = I_loc_traits_tl_rec.lang
      and loc_trait = I_loc_traits_tl_rec.loc_trait;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_LOC_TRAITS_TL_DEL%ISOPEN then
         close C_LOCK_LOC_TRAITS_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_LOC_TRAITS_TL_DEL;
------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_LOC_TRAITS(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error          IN OUT BOOLEAN,
                                I_rec            IN OUT C_SVC_LOC_TRAITS%ROWTYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                       :='CORESVC_LOC_TRAITS.PROCESS_VAL_LOC_TRAITS';
   L_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_LOC_TRAITS';
   L_sys_opt_row              SYSTEM_OPTIONS%ROWTYPE;
   L_valid                    BOOLEAN;
   L_exists                   BOOLEAN;
   L_batch_status             RMS_BATCH_STATUS.BATCH_RUNNING_IND%TYPE;
   L_form_access_during_batch VARCHAR2(1)                        :='Y';
   L_confirm_del              VARCHAR2(1)                        :='Y';

BEGIN
   O_error       := FALSE;

   if I_rec.action is NULL
      or I_rec.action NOT IN (action_new,action_mod,action_del) then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'ACTION',
                  'INV_ACT');
      O_error :=TRUE;
   end if;

   if I_rec.action IN (action_mod,action_del)
      and I_rec.LOC_TRAIT is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'LOC_TRAIT',
                  'ENT_LOC_TRAIT');
      O_error :=TRUE;
   end if;

   if I_rec.action IN (action_mod,action_del)
      and I_rec.LOC_TRAIT is NOT NULL
      and I_rec.PK_LOC_TRAITS_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'LOC_TRAIT',
                  'INV_LOC_TRAIT');
      O_error :=TRUE;
   end if;

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_sys_opt_row) then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error :=TRUE;
   else
      if (I_rec.action =action_new
         and I_rec.filter_org_id is NOT NULL)
         or (I_rec.action=action_mod
             and I_rec.filter_org_id is NOT NULL
             and NVL(I_rec.old_filter_org_id,'-1') <> NVL(I_rec.filter_org_id,'-1')) then
           if FILTER_LOV_VALIDATE_SQL.VALIDATE_ORG_LEVEL(O_error_message,
                                                         L_valid,
                                                         I_rec.chain_name,
                                                         L_sys_opt_row.loc_trait_org_leveL_code,
                                                         I_rec.filter_org_id) = FALSE then
              WRITE_ERROR(I_rec.process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_rec.chunk_id,
                          L_table,
                          I_rec.row_seq,
                          'FILTER_ORG_ID',
                          O_error_message);
              O_error :=TRUE;
           else
              if NOT L_valid then
                 WRITE_ERROR(I_rec.process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_rec.chunk_id,
                             L_table,
                             I_rec.row_seq,
                             'FILTER_ORG_ID',
                             O_error_message);
                 O_error :=TRUE;
              end if;
           end if;
      end if;
   end if;

   if I_rec.action=action_del
      and I_rec.loc_trait is NOT NULL
      and L_confirm_del='Y'
      and I_rec.PK_LOC_TRAITS_rid is NOT NULL then
      if LOC_TRAITS_SQL.RELATION_EXISTS(O_error_message,
                                        L_exists,
                                        I_rec.loc_trait) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'LOC_TRAIT',
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_exists then
         savepoint DELETE_TRAIT;
         if LOC_TRAITS_SQL.DELETE_LOC_TRAITS_MATRIX(O_error_message,
                                                    I_rec.loc_trait,
                                                    NULL,
                                                    NULL,
                                                    NULL) = FALSE then
            rollback to DELETE_TRAIT;
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'LOC_TRAIT',
                        O_error_message);
            O_error :=TRUE;
         end if;
      end if;
   end if;
   if I_rec.action IN (action_new,action_mod)
      and I_rec.description is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'DESCRIPTION',
                  'ENTER_DESC');
      O_error :=TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_VAL_LOC_TRAITS;
------------------------------------------------------------------------------------
FUNCTION PROCESS_LOC_TRAITS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id    IN     SVC_LOC_TRAITS.PROCESS_ID%TYPE,
                             I_chunk_id      IN     SVC_LOC_TRAITS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                        VARCHAR2(64)                       :='CORESVC_LOC_TRAITS.PROCESS_LOC_TRAITS';
   L_error                          BOOLEAN;
   L_process_error                  BOOLEAN                            := FALSE;
   L_loc_traits_temp_rec            LOC_TRAITS%ROWTYPE;
   L_table                          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_LOC_TRAITS';
   L_sys_opt_row                    SYSTEM_OPTIONS%ROWTYPE;
   L_valid                          BOOLEAN;
   L_exists                         BOOLEAN;
   L_next_loc_trait_seq             LOC_TRAITS.LOC_TRAIT%TYPE          := NULL;
   L_return_code                    VARCHAR2(50);

BEGIN
   FOR rec IN C_SVC_LOC_TRAITS(I_process_id,
                               I_chunk_id)
   LOOP
      if PROCESS_VAL_LOC_TRAITS(O_error_message,
                                L_error,
                                rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if L_error then
         update svc_loc_traits st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;

      if NOT L_error
         and rec.action=action_new
         and L_next_loc_trait_seq is NULL then
         NEXT_LOC_TRAIT(L_next_loc_trait_seq,
                        L_return_code,
                        O_error_message);
         if L_return_code = 'FALSE' then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'LOC_TRAIT',
                        O_error_message);
            L_error :=TRUE;
         else
            update svc_loc_traits_tl
               set loc_trait = L_next_loc_trait_seq
             where process_id =  I_process_id
               and chunk_id = I_chunk_id
               and action = action_new
               and loc_trait = rec.loc_trait;
            L_loc_traits_temp_rec.loc_trait              := L_next_loc_trait_seq;
            L_next_loc_trait_seq:=NULL;
         end if;
      end if;

      if NOT L_error then
         if rec.action IN (action_del,action_mod) then
            L_loc_traits_temp_rec.loc_trait             := rec.loc_trait;
         end if;

         L_loc_traits_temp_rec.filter_org_id            := rec.filter_org_id;
         L_loc_traits_temp_rec.description              := rec.description;

         if rec.action = action_new then
            if EXEC_LOC_TRAITS_INS(O_error_message,
                                   L_loc_traits_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            else
            L_next_loc_trait_seq:=NULL;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_LOC_TRAITS_UPD(O_error_message,
                                   L_loc_traits_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_LOC_TRAITS_DEL(O_error_message,
                                   L_loc_traits_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_LOC_TRAITS%ISOPEN then
         close C_SVC_LOC_TRAITS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_LOC_TRAITS;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_LOC_TRAITS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_LOC_TRAITS_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_LOC_TRAITS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64) := 'CORESVC_LOC_TRAITS.PROCESS_LOC_TRAITS_TL';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'LOC_TRAITS_TL';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_LOC_TRAITS_TL';
   L_error                     BOOLEAN := FALSE;
   L_loc_traits_tl_temp_rec    LOC_TRAITS_TL%ROWTYPE;

   cursor C_SVC_LOC_TRAITS_TL(I_process_id NUMBER,
                              I_chunk_id NUMBER) is
      select pk_loc_traits_tl.rowid  as pk_loc_traits_tl_rid,
             fk_loc_traits.rowid     as fk_loc_traits_rid,
             fk_lang.rowid           as fk_lang_rid,
             st.lang,
             st.loc_trait,
             st.description,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_loc_traits_tl  st,
             loc_traits         fk_loc_traits,
             loc_traits_tl      pk_loc_traits_tl,
             lang               fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.loc_trait   =  fk_loc_traits.loc_trait (+)
         and st.lang        =  pk_loc_traits_tl.lang (+)
         and st.loc_trait   =  pk_loc_traits_tl.loc_trait (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_LOC_TRAITS_TL is TABLE OF C_SVC_LOC_TRAITS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_loc_traits_tab        SVC_LOC_TRAITS_TL;

BEGIN
   if C_SVC_LOC_TRAITS_TL%ISOPEN then
      close C_SVC_LOC_TRAITS_TL;
   end if;

   open C_SVC_LOC_TRAITS_TL(I_process_id,
                       I_chunk_id);
   LOOP
      fetch C_SVC_LOC_TRAITS_TL bulk collect into L_svc_loc_traits_tab limit LP_bulk_fetch_limit;
      if L_svc_loc_traits_tab.COUNT > 0 then
         FOR i in L_svc_loc_traits_tab.FIRST..L_svc_loc_traits_tab.LAST LOOP
            L_error := FALSE;
            -- check if action is valid
            if L_svc_loc_traits_tab(i).action is NULL
               or L_svc_loc_traits_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_loc_traits_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;
            
            --check for primary_lang
            if L_svc_loc_traits_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_loc_traits_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;
            
            -- check if primary key values already exist
            if L_svc_loc_traits_tab(i).action = action_new
               and L_svc_loc_traits_tab(i).pk_loc_traits_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_loc_traits_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;
            
            if L_svc_loc_traits_tab(i).action IN (action_mod, action_del)
               and L_svc_loc_traits_tab(i).lang is NOT NULL
               and L_svc_loc_traits_tab(i).loc_trait is NOT NULL
               and L_svc_loc_traits_tab(i).pk_loc_traits_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_loc_traits_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;
            
            -- check for FK
            if L_svc_loc_traits_tab(i).action = action_new
               and L_svc_loc_traits_tab(i).loc_trait is NOT NULL
               and L_svc_loc_traits_tab(i).fk_loc_traits_rid is NULL then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_loc_traits_tab(i).row_seq,
                            'LOC_TRAIT',
                            'INV_LOC_TRAIT');
               L_error :=TRUE;
            end if;
            
            if L_svc_loc_traits_tab(i).action = action_new
               and L_svc_loc_traits_tab(i).lang is NOT NULL
               and L_svc_loc_traits_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_loc_traits_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;
            
            --check for required fields
            if L_svc_loc_traits_tab(i).action in (action_new, action_mod) then
               if L_svc_loc_traits_tab(i).description is NULL then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_loc_traits_tab(i).row_seq,
                              'DESCRIPTION',
                              'ENTER_DESC');
                  L_error :=TRUE;
               end if;
            end if;
            
            if L_svc_loc_traits_tab(i).loc_trait is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_loc_traits_tab(i).row_seq,
                           'LOC_TRAIT',
                           'ENT_LOCATION_TRAIT');
               L_error :=TRUE;
            end if;
            
            if L_svc_loc_traits_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_loc_traits_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;
            
            if NOT L_error then
               L_loc_traits_TL_temp_rec.lang := L_svc_loc_traits_tab(i).lang;
               L_loc_traits_TL_temp_rec.loc_trait := L_svc_loc_traits_tab(i).loc_trait;
               L_loc_traits_TL_temp_rec.description := L_svc_loc_traits_tab(i).description;
               L_loc_traits_TL_temp_rec.create_datetime := SYSDATE;
               L_loc_traits_TL_temp_rec.create_id := GET_USER;
               L_loc_traits_TL_temp_rec.last_update_datetime := SYSDATE;
               L_loc_traits_TL_temp_rec.last_update_id := GET_USER;
            
               if L_svc_loc_traits_tab(i).action = action_new then
                  if EXEC_LOC_TRAITS_TL_INS(O_error_message,
                                            L_loc_traits_TL_temp_rec) = FALSE then
                     return FALSE;
                  end if;
               end if;
            
               if L_svc_loc_traits_tab(i).action = action_mod then
                  if EXEC_LOC_TRAITS_TL_UPD(O_error_message,
                                            L_loc_traits_TL_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_loc_traits_tab(i).row_seq,
                                 NULL,
                                 O_error_message);
                  end if;
               end if;
            
               if L_svc_loc_traits_tab(i).action = action_del then
                  if EXEC_LOC_TRAITS_TL_DEL(O_error_message,
                                            L_loc_traits_TL_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_loc_traits_tab(i).row_seq,
                                 NULL,
                                 O_error_message);
                  end if;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_LOC_TRAITS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_LOC_TRAITS_TL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_loc_traits_TL;
--------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_LOC_TRAITS.PROCESS_ID%TYPE) IS

BEGIN
   delete
     from svc_loc_traits_tl
    where process_id= I_process_id;

   delete
     from svc_loc_traits
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;
---------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64):='CORESVC_LOC_TRAITS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';


BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW ERRORS_tab_typ();
   if PROCESS_LOC_TRAITS(O_error_message,
                         I_process_id,
                         I_chunk_id)=FALSE then
      return FALSE;
   end if;

   if PROCESS_LOC_TRAITS_TL(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_LOC_TRAITS;
/
