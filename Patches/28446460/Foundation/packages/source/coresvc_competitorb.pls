CREATE OR REPLACE PACKAGE BODY CORESVC_COMPETITOR AS
----------------------------------------------------------------------------------------------
   cursor C_SVC_COMPETITOR(I_process_id   NUMBER,
                           I_chunk_id     NUMBER) is
      select pk_competitor.rowid    as   pk_competitor_rid,
             cmp_cnt_fk.rowid       as   cmp_cnt_fk_rid,
             UPPER(st.jurisdiction_code) as jurisdiction_code,
             st.website           as website,
             st.fax,
             st.phone,
             st.post_code,
             UPPER(st.country_id)  as country_id,
             UPPER(st.state)       as state,
             UPPER(st.city)        as city,
             st.address_3,
             st.address_2,
             st.address_1,
             st.comp_name,
             st.competitor,
             st.process_id,
             st.chunk_id,
             st.rowid               as   st_rid,
             st.row_seq,
             ctj.jurisdiction_desc,
             upper(st.action)       as   action,
             st.process$status
        from svc_competitor st,
             competitor pk_competitor,
             country cmp_cnt_fk,
             country_tax_jurisdiction ctj
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
         and st.competitor        = pk_competitor.competitor (+)
         and UPPER(st.country_id)        = UPPER(cmp_cnt_fk.country_id (+))
         and UPPER(st.jurisdiction_code) = UPPER(ctj.jurisdiction_code (+))
         and NVL(st.action, 'X') <> action_del;
         
   cursor C_SVC_COMPETITOR_DEL(I_process_id   NUMBER,
                               I_chunk_id     NUMBER) is
      select pk_competitor.rowid    as   pk_competitor_rid,
             cmp_cnt_fk.rowid       as   cmp_cnt_fk_rid,
             UPPER(st.jurisdiction_code) as jurisdiction_code,
             st.website           as website,
             st.fax,
             st.phone,
             st.post_code,
             UPPER(st.country_id)  as country_id,
             UPPER(st.state)       as state,
             UPPER(st.city)        as city,
             st.address_3,
             st.address_2,
             st.address_1,
             st.comp_name,
             st.competitor,
             st.process_id,
             st.chunk_id,
             st.rowid               as   st_rid,
             st.row_seq,
             ctj.jurisdiction_desc,
             upper(st.action)       as   action,
             st.process$status
        from svc_competitor st,
             competitor pk_competitor,
             country cmp_cnt_fk,
             country_tax_jurisdiction ctj
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
         and st.competitor        = pk_competitor.competitor (+)
         and UPPER(st.country_id)        = UPPER(cmp_cnt_fk.country_id (+))
         and UPPER(st.jurisdiction_code) = UPPER(ctj.jurisdiction_code (+))
         and st.action = action_del;

   cursor C_SVC_COMP_STORE(I_process_id   NUMBER,
                           I_chunk_id     NUMBER) is
      select pk_comp_store.rowid        as pk_comp_store_rid,
             cms_cmp_fk.rowid           as cms_cmp_fk_rid,
             cms_cnt_fk.rowid           as cms_cnt_fk_rid,
             UPPER(st.jurisdiction_code) as jurisdiction_code,
             st.estimated_volume,
             st.close_date,
             st.open_date,
             st.selling_square_feet,
             st.total_square_feet,
             UPPER(st.currency_code)   as currency_code,
             st.store_format,
             st.fax,
             st.phone,
             st.post_code,
             UPPER(st.country_id)      as country_id,
             UPPER(st.state)           as state,
             UPPER(st.city)            as city,
             st.address_3,
             st.address_2,
             st.address_1,
             st.store_name,
             st.competitor,
             st.store,
             pk_comp_store.competitor   as old_competitor,
             st.rowid                   as st_rid,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             curr.currency_desc,
             UPPER(ctj.jurisdiction_desc) as jurisdiction_desc,
             stform.format_name,
             UPPER(st.action) as action,
             st.process$status
        from svc_comp_STORE st,
             comp_store pk_comp_store,
             competitor cms_cmp_fk,
             country cms_cnt_fk,
             country_tax_jurisdiction ctj,
             currencies curr,
             store_format stform
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
         and st.store             = pk_comp_store.store (+)
         and UPPER(st.country_id)        = UPPER(cms_cnt_fk.country_id (+))
         and st.competitor        = cms_cmp_fk.competitor (+)
         and UPPER(st.currency_code)     = UPPER(curr.currency_code (+))
         and UPPER(st.jurisdiction_code) = UPPER(ctj.jurisdiction_code (+))
         and st.store_format      = stform.STORE_format (+)
         and NVL(st.action, 'X') <> action_del;
         
   cursor C_SVC_COMP_STORE_DEL(I_process_id   NUMBER,
                               I_chunk_id     NUMBER) is
      select pk_comp_store.rowid        as pk_comp_store_rid,
             cms_cmp_fk.rowid           as cms_cmp_fk_rid,
             cms_cnt_fk.rowid           as cms_cnt_fk_rid,
             UPPER(st.jurisdiction_code) as jurisdiction_code,
             st.estimated_volume,
             st.close_date,
             st.open_date,
             st.selling_square_feet,
             st.total_square_feet,
             UPPER(st.currency_code)   as currency_code,
             st.store_format,
             st.fax,
             st.phone,
             st.post_code,
             UPPER(st.country_id)      as country_id,
             UPPER(st.state)           as state,
             UPPER(st.city)            as city,
             st.address_3,
             st.address_2,
             st.address_1,
             st.store_name,
             st.competitor,
             st.store,
             pk_comp_store.competitor   as old_competitor,
             st.rowid                   as st_rid,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             curr.currency_desc,
             UPPER(ctj.jurisdiction_desc) as jurisdiction_desc,
             stform.format_name,
             UPPER(st.action) as action,
             st.process$status
        from svc_comp_STORE st,
             comp_store pk_comp_store,
             competitor cms_cmp_fk,
             country cms_cnt_fk,
             country_tax_jurisdiction ctj,
             currencies curr,
             store_format stform
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
         and st.store             = pk_comp_store.store (+)
         and UPPER(st.country_id)        = UPPER(cms_cnt_fk.country_id (+))
         and st.competitor        = cms_cmp_fk.competitor (+)
         and UPPER(st.currency_code)     = UPPER(curr.currency_code (+))
         and UPPER(st.jurisdiction_code) = UPPER(ctj.jurisdiction_code (+))
         and st.store_format      = stform.STORE_format (+)
         and st.action = action_del;

  cursor C_SVC_COMP_STORE_LINK(I_process_id   NUMBER,
                                I_chunk_id     NUMBER) is
      select pk_comp_store_link.rowid                                      as pk_comp_store_link_rid,
             cmk_cms_fk.rowid                                              as cmk_cms_fk_rid,
             cmk_str_fk.rowid                                              as cmk_str_fk_rid,
             st.uom,
             st.rowid                                                      as stcm_rid,
             st.distance,
             st.rank,
             st.tar_comp_ind,
             st.comp_store,
             st.store,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             pk_comp_store_link.comp_store                                 as old_comp_store,
             pk_comp_store_link.target_comp_ind                            as old_target_comp_ind,
             pk_comp_store_link.rank                                       as old_rank,
             LEAD(st.store, 1, 0) over (order by st.store)                 as next_store,
             row_number() over (partition BY st.store order by st.store)   as ch_rank,
             upper(st.action)                                              as action,
             st.process$status
        from svc_comp_store_link st,
             comp_store_link pk_comp_store_link,
             comp_store cmk_cms_fk,
             store cmk_str_fk
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.comp_store = pk_comp_store_link.comp_store (+)
         and st.store      = pk_comp_store_link.store (+)
         and st.store      = cmk_str_fk.store (+)
         and st.comp_store = cmk_cms_fk.store (+);

   cursor C_SVC_COMP_SHOPPER(I_process_id   NUMBER,
                             I_chunk_id     NUMBER) is
      select pk_comp_shopper.rowid  as pk_comp_shopper_rid,
             st.rowid               as st_rid,
             st.shopper_fax,
             st.shopper_phone,
             st.shopper_name,
             st.shopper,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)       as action,
             st.process$status
        from svc_comp_shopper st,
             comp_shopper     pk_comp_shopper,
             dual
       where st.process_id       = I_process_id
         and st.chunk_id         = I_chunk_id
         and st.shopper          = pk_comp_shopper.shopper (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   Lp_errors_tab       errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   Lp_s9t_errors_tab   s9t_errors_tab_typ;
--------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id    IN   S9T_ERRORS.file_id%TYPE,
                          I_sheet      IN   VARCHAR2,
                          I_row_seq    IN   NUMBER,
                          I_col        IN   VARCHAR2,
                          I_sqlcode    IN   NUMBER,
                          I_sqlerrm    IN   VARCHAR2)IS
BEGIN
   Lp_s9t_errors_tab.EXTEND();
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := CORESVC_COMPETITOR.template_key;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE) IS
BEGIN
   Lp_errors_tab.EXTEND();
   Lp_errors_tab(Lp_errors_tab.COUNT()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.COUNT()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.COUNT()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.COUNT()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.COUNT()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets               S9T_PKG.names_map_typ;
   L_competitor_cols      S9T_PKG.names_map_typ;
   COMP_STORE_cols        S9T_PKG.names_map_typ;
   COMP_STORE_LINK_cols   S9T_PKG.names_map_typ;
   COMP_SHOPPER_cols      S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                     :=S9T_PKG.GET_SHEET_NAMES(I_file_id);
   L_competitor_cols            :=S9T_PKG.GET_COL_NAMES(I_file_id,
                                                          COMPETITOR_sheet);
   competitor$action            := L_competitor_cols('ACTION');
   competitor$competitor        := L_competitor_cols('COMPETITOR');
   competitor$comp_name         := L_competitor_cols('COMP_NAME');
   competitor$address_1         := L_competitor_cols('ADDRESS_1');
   competitor$address_2         := L_competitor_cols('ADDRESS_2');
   competitor$address_3         := L_competitor_cols('ADDRESS_3');
   competitor$city              := L_competitor_cols('CITY');
   competitor$state             := L_competitor_cols('STATE');
   competitor$country_id        := L_competitor_cols('COUNTRY_ID');
   competitor$post_code         := L_competitor_cols('POST_CODE');
   competitor$phone             := L_competitor_cols('PHONE');
   competitor$fax               := L_competitor_cols('FAX');
   competitor$website           := L_competitor_cols('WEBSITE');
   competitor$jurisdiction_code := L_competitor_cols('JURISDICTION_CODE');

   COMP_STORE_cols                := S9T_PKG.get_col_names(I_file_id,COMP_STORE_sheet);
   COMP_STORE$ACTION              := COMP_STORE_cols('ACTION');
   COMP_STORE$STORE               := COMP_STORE_cols('STORE');
   COMP_STORE$COMPETITOR          := COMP_STORE_cols('COMPETITOR');
   COMP_STORE$STORE_NAME          := COMP_STORE_cols('STORE_NAME');
   COMP_STORE$ADDRESS_1           := COMP_STORE_cols('ADDRESS_1');
   COMP_STORE$ADDRESS_2           := COMP_STORE_cols('ADDRESS_2');
   COMP_STORE$ADDRESS_3           := COMP_STORE_cols('ADDRESS_3');
   COMP_STORE$CITY                := COMP_STORE_cols('CITY');
   COMP_STORE$STATE               := COMP_STORE_cols('STATE');
   COMP_STORE$COUNTRY_ID          := COMP_STORE_cols('COUNTRY_ID');
   COMP_STORE$POST_CODE           := COMP_STORE_cols('POST_CODE');
   COMP_STORE$PHONE               := COMP_STORE_cols('PHONE');
   COMP_STORE$FAX                 := COMP_STORE_cols('FAX');
   COMP_STORE$STORE_FORMAT        := COMP_STORE_cols('STORE_FORMAT');
   COMP_STORE$CURRENCY_CODE       := COMP_STORE_cols('CURRENCY_CODE');
   COMP_STORE$TOTAL_SQUARE_FEET   := COMP_STORE_cols('TOTAL_SQUARE_FEET');
   COMP_STORE$SELLING_SQUARE_FEET := COMP_STORE_cols('SELLING_SQUARE_FEET');
   COMP_STORE$OPEN_DATE           := COMP_STORE_cols('OPEN_DATE');
   COMP_STORE$CLOSE_DATE          := COMP_STORE_cols('CLOSE_DATE');
   COMP_STORE$ESTIMATED_VOLUME    := COMP_STORE_cols('ESTIMATED_VOLUME');
   COMP_STORE$JURISDICTION_CODE   := COMP_STORE_cols('JURISDICTION_CODE');

   COMP_STORE_LINK_cols         := S9T_PKG.get_col_names(I_file_id,COMP_STORE_LINK_sheet);
   COMP_STORE_LINK$ACTION       := COMP_STORE_LINK_cols('ACTION');
   COMP_STORE_LINK$STORE        := COMP_STORE_LINK_cols('STORE');
   COMP_STORE_LINK$COMP_STORE   := COMP_STORE_LINK_cols('COMP_STORE');
   COMP_STORE_LINK$tar_comp_ind := COMP_STORE_LINK_cols('TARGET_COMP_IND');
   COMP_STORE_LINK$RANK         := COMP_STORE_LINK_cols('RANK');
   COMP_STORE_LINK$DISTANCE     := COMP_STORE_LINK_cols('DISTANCE');
   COMP_STORE_LINK$UOM          := COMP_STORE_LINK_cols('UOM');

   COMP_SHOPPER_cols          := s9t_pkg.get_col_names(I_file_id,COMP_SHOPPER_sheet);
   COMP_SHOPPER$Action        := COMP_SHOPPER_cols('ACTION');
   COMP_SHOPPER$SHOPPER       := COMP_SHOPPER_cols('SHOPPER');
   COMP_SHOPPER$SHOPPER_NAME  := COMP_SHOPPER_cols('SHOPPER_NAME');
   COMP_SHOPPER$SHOPPER_PHONE := COMP_SHOPPER_cols('SHOPPER_PHONE');
   COMP_SHOPPER$SHOPPER_FAX   := COMP_SHOPPER_cols('SHOPPER_FAX');
END POPULATE_NAMES;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COMPETITOR(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = COMPETITOR_sheet)
               select s9t_row(s9t_cells(CORESVC_COMPETITOR.action_mod,
                                        competitor,
                                        comp_name,
                                        address_1,
                                        address_2,
                                        address_3,
                                        city,
                                        state,
                                        country_id,
                                        post_code,
                                        phone,
                                        fax,
                                        website,
                                        jurisdiction_code))
                 from competitor ;
END POPULATE_COMPETITOR;
--------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COMP_STORE(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = COMP_STORE_sheet)
               select s9t_row(s9t_cells(CORESVC_COMPETITOR.action_mod,
                                        store,
                                        competitor,
                                        store_name,
                                        address_1,
                                        address_2,
                                        address_3,
                                        city,
                                        state,
                                        country_id,
                                        post_code,
                                        phone,
                                        fax,
                                        store_format,
                                        currency_code,
                                        total_square_feet,
                                        selling_square_feet,
                                        open_date,
                                        close_date,
                                        estimated_volume,
                                        jurisdiction_code))
                 from comp_store ;
END POPULATE_COMP_STORE;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COMP_STORE_LINK(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = COMP_STORE_LINK_sheet)
               select s9t_row(s9t_cells(CORESVC_COMPETITOR.action_mod,
                                        store,
                                        comp_store,
                                        target_comp_ind,
                                        rank,
                                        distance,
                                        uom))
                 from comp_store_link;
END POPULATE_COMP_STORE_LINK;
----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_COMP_SHOPPER(I_file_id IN NUMBER) IS
BEGIN
   insert into table
      (select ss.s9t_rows
         from s9t_folder sf,
              TABLE(sf.s9t_file_obj.sheets) ss
        where sf.file_id    = I_file_id
          and ss.sheet_name = COMP_SHOPPER_sheet)
   select s9t_row(s9t_cells( CORESVC_COMPETITOR.action_mod,
                             SHOPPER,
                             SHOPPER_NAME,
                             SHOPPER_PHONE,
                             SHOPPER_FAX))
     from COMP_SHOPPER ;
END POPULATE_COMP_SHOPPER;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS

   L_file S9T_FILE;
   L_file_name s9t_folder.file_name%TYPE;

BEGIN
   L_file              := NEW S9T_FILE();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := CORESVC_COMPETITOR.template_key||'_'||GET_USER||'_'||sysdate||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := CORESVC_COMPETITOR.template_key;
   L_file.user_lang    := GET_USER_LANG ;
   L_file.ADD_SHEET(COMPETITOR_sheet);
   L_file.SHEETS(L_file.get_sheet_index(COMPETITOR_sheet)).column_headers := s9t_cells('ACTION',
                                                                                       'COMPETITOR',
                                                                                       'COMP_NAME',
                                                                                       'ADDRESS_1',
                                                                                       'ADDRESS_2',
                                                                                       'ADDRESS_3',
                                                                                       'CITY',
                                                                                       'STATE',
                                                                                       'COUNTRY_ID',
                                                                                       'POST_CODE',
                                                                                       'PHONE',
                                                                                       'FAX',
                                                                                       'WEBSITE',
                                                                                       'JURISDICTION_CODE');

   L_file.ADD_SHEET(COMP_STORE_sheet);
   L_file.SHEETS(L_file.get_sheet_index(COMP_STORE_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                        'STORE',
                                                                                        'COMPETITOR',
                                                                                        'STORE_NAME',
                                                                                        'ADDRESS_1',
                                                                                        'ADDRESS_2',
                                                                                        'ADDRESS_3',
                                                                                        'CITY',
                                                                                        'STATE',
                                                                                        'COUNTRY_ID',
                                                                                        'POST_CODE',
                                                                                        'PHONE',
                                                                                        'FAX',
                                                                                        'STORE_FORMAT',
                                                                                        'CURRENCY_CODE',
                                                                                        'TOTAL_SQUARE_FEET',
                                                                                        'SELLING_SQUARE_FEET',
                                                                                        'OPEN_DATE',
                                                                                        'CLOSE_DATE',
                                                                                        'ESTIMATED_VOLUME',
                                                                                        'JURISDICTION_CODE');

   L_file.ADD_SHEET(COMP_STORE_LINK_sheet);
   L_file.SHEETS(L_file.get_sheet_index(COMP_STORE_LINK_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                             'STORE',
                                                                                             'COMP_STORE',
                                                                                             'TARGET_COMP_IND',
                                                                                             'RANK',
                                                                                             'DISTANCE',
                                                                                             'UOM');
   L_file.ADD_SHEET(COMP_SHOPPER_sheet);
   L_file.sheets(L_file.GET_SHEET_INDEX(COMP_SHOPPER_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                          'SHOPPER',
                                                                                          'SHOPPER_NAME',
                                                                                          'SHOPPER_PHONE',
                                                                                          'SHOPPER_FAX');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
---------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COMPETITOR.CREATE_S9T';
   L_file      S9T_FILE;

BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             CORESVC_COMPETITOR.template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_COMPETITOR(O_file_id);
      POPULATE_COMP_STORE(O_file_id);
      POPULATE_COMP_STORE_LINK(O_file_id);
      POPULATE_COMP_SHOPPER(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          CORESVC_COMPETITOR.template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                                        L_program,
                                                        TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
---------------------------------------=-----------------------------------------
---  Name: PROCESS_S9T_COMPETITOR
--- Purpose: Load data to staging table SVC_COMPETITOR
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COMPETITOR(I_file_id      IN   s9t_folder.file_id%TYPE,
                                 I_process_id   IN   SVC_COMPETITOR.process_id%TYPE) IS

   Type svc_COMPETITOR_col_typ is TABLE OF SVC_COMPETITOR%ROWTYPE;
   L_temp_rec           SVC_COMPETITOR%ROWTYPE;
   svc_COMPETITOR_col   svc_COMPETITOR_col_typ := NEW svc_COMPETITOR_col_typ();
   L_process_id         SVC_COMPETITOR.process_id%TYPE;
   L_error              BOOLEAN := FALSE;
   L_default_rec        SVC_COMPETITOR%ROWTYPE;

   cursor C_MANDATORY_IND is
      select JURISDICTION_CODE_mi,
             WEBSITE_mi,
             FAX_mi,
             PHONE_mi,
             POST_CODE_mi,
             COUNTRY_ID_mi,
             STATE_mi,
             CITY_mi,
             ADDRESS_3_mi,
             ADDRESS_2_mi,
             ADDRESS_1_mi,
             COMP_NAME_mi,
             COMPETITOR_mi,
             1 as dummy
        from (select column_key,
                     mandatory
			       from s9t_tmpl_cols_def
			      where template_key = CORESVC_COMPETITOR.template_key
			        and wksht_key    = 'COMPETITOR')
			      PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('JURISDICTION_CODE' as JURISDICTION_CODE,
                                       'WEBSITE'           as WEBSITE,
                                       'FAX'               as FAX,
                                       'PHONE'             as PHONE,
                                       'POST_CODE'         as POST_CODE,
                                       'COUNTRY_ID'        as COUNTRY_ID,
                                       'STATE'             as STATE,
                                       'CITY'              as CITY,
                                       'ADDRESS_3'         as ADDRESS_3,
                                       'ADDRESS_2'         as ADDRESS_2,
                                       'ADDRESS_1'         as ADDRESS_1,
                                       'COMP_NAME'         as COMP_NAME,
                                       'COMPETITOR'        as COMPETITOR,
                                       NULL                as dummy));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMPETITOR';
   L_pk_columns    VARCHAR2(255)  := 'Competitor';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN (select JURISDICTION_CODE_dv,
                      WEBSITE_dv,
                      FAX_dv,
                      PHONE_dv,
                      POST_CODE_dv,
                      COUNTRY_ID_dv,
                      STATE_dv,
                      CITY_dv,
                      ADDRESS_3_dv,
                      ADDRESS_2_dv,
                      ADDRESS_1_dv,
                      COMP_NAME_dv,
                      COMPETITOR_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key    = CORESVC_COMPETITOR.template_key
                          and wksht_key       = 'COMPETITOR')
                       PIVOT (MAX(default_value) as dv
                         FOR (column_key) IN ('JURISDICTION_CODE' as JURISDICTION_CODE,
                                              'WEBSITE'           as WEBSITE,
                                              'FAX'               as FAX,
                                              'PHONE'             as PHONE,
                                              'POST_CODE'         as POST_CODE,
                                              'COUNTRY_ID'        as COUNTRY_ID,
                                              'STATE'             as STATE,
                                              'CITY'              as CITY,
                                              'ADDRESS_3'         as ADDRESS_3,
                                              'ADDRESS_2'         as ADDRESS_2,
                                              'ADDRESS_1'         as ADDRESS_1,
                                              'COMP_NAME'         as COMP_NAME,
                                              'COMPETITOR'        as COMPETITOR,
                                              NULL                as dummy)))
   LOOP
      BEGIN
         L_default_rec.JURISDICTION_CODE := rec.JURISDICTION_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'JURISDICTION_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.WEBSITE := rec.WEBSITE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'WEBSITE',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.FAX := rec.FAX_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'FAX',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.PHONE := rec.PHONE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'PHONE',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.POST_CODE := rec.POST_CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'POST_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.COUNTRY_ID := rec.COUNTRY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.STATE := rec.STATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'STATE',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.CITY := rec.CITY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'CITY',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.ADDRESS_3 := rec.ADDRESS_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'ADDRESS_3',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.ADDRESS_2 := rec.ADDRESS_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'ADDRESS_2',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.ADDRESS_1 := rec.ADDRESS_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'ADDRESS_1',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.COMP_NAME := rec.COMP_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'COMP_NAME',
                            NULL,
                            SQLERRM);
      END;

      BEGIN
         L_default_rec.COMPETITOR := rec.COMPETITOR_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMPETITOR',
                            NULL,
                            'COMPETITOR',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.GET_CELL(COMPETITOR$ACTION)            as ACTION,
                      UPPER(r.GET_CELL(COMPETITOR$JURISDICTION_CODE)) as JURISDICTION_CODE,
                      r.GET_CELL(COMPETITOR$WEBSITE)           as WEBSITE,
                      r.GET_CELL(COMPETITOR$FAX)               as FAX,
                      r.GET_CELL(COMPETITOR$PHONE)             as PHONE,
                      r.GET_CELL(COMPETITOR$POST_CODE)         as POST_CODE,
                      UPPER(r.GET_CELL(COMPETITOR$COUNTRY_ID))        as COUNTRY_ID,
                      UPPER(r.GET_CELL(COMPETITOR$STATE))             as STATE,
                      UPPER(r.GET_CELL(COMPETITOR$CITY))              as CITY,
                      r.GET_CELL(COMPETITOR$ADDRESS_3)         as ADDRESS_3,
                      r.GET_CELL(COMPETITOR$ADDRESS_2)         as ADDRESS_2,
                      r.GET_CELL(COMPETITOR$ADDRESS_1)         as ADDRESS_1,
                      r.GET_CELL(COMPETITOR$COMP_NAME)         as COMP_NAME,
                      r.GET_CELL(COMPETITOR$COMPETITOR)        as COMPETITOR,
                      r.GET_ROW_SEQ()                          as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(COMPETITOR_sheet))
   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error := FALSE;

      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            if c_mandatory_ind%ISOPEN then
              close c_mandatory_ind;
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.JURISDICTION_CODE := rec.JURISDICTION_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'JURISDICTION_CODE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.WEBSITE := rec.WEBSITE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'WEBSITE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.FAX := rec.FAX;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'FAX',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.PHONE := rec.PHONE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'PHONE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.POST_CODE := rec.POST_CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'POST_CODE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
       L_temp_rec.COUNTRY_ID := rec.COUNTRY_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.STATE := rec.STATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'STATE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.CITY := rec.CITY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'CITY',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.ADDRESS_3 := rec.ADDRESS_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'ADDRESS_3',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.ADDRESS_2 := rec.ADDRESS_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'ADDRESS_2',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.ADDRESS_1 := rec.ADDRESS_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'ADDRESS_1',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.COMP_NAME := rec.COMP_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'COMP_NAME',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.COMPETITOR := rec.COMPETITOR;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMPETITOR_sheet,
                            rec.row_seq,
                            'COMPETITOR',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      if rec.action = CORESVC_COMPETITOR.action_new then
         L_temp_rec.JURISDICTION_CODE := NVL( L_temp_rec.JURISDICTION_CODE,l_default_rec.JURISDICTION_CODE);
         L_temp_rec.WEBSITE           := NVL( L_temp_rec.WEBSITE,l_default_rec.WEBSITE);
         L_temp_rec.FAX               := NVL( L_temp_rec.FAX,l_default_rec.FAX);
         L_temp_rec.PHONE             := NVL( L_temp_rec.PHONE,l_default_rec.PHONE);
         L_temp_rec.POST_CODE         := NVL( L_temp_rec.POST_CODE,l_default_rec.POST_CODE);
         L_temp_rec.COUNTRY_ID        := NVL( L_temp_rec.COUNTRY_ID,l_default_rec.COUNTRY_ID);
         L_temp_rec.STATE             := NVL( L_temp_rec.STATE,l_default_rec.STATE);
         L_temp_rec.CITY              := NVL( L_temp_rec.CITY,l_default_rec.CITY);
         L_temp_rec.ADDRESS_3         := NVL( L_temp_rec.ADDRESS_3,l_default_rec.ADDRESS_3);
         L_temp_rec.ADDRESS_2         := NVL( L_temp_rec.ADDRESS_2,l_default_rec.ADDRESS_2);
         L_temp_rec.ADDRESS_1         := NVL( L_temp_rec.ADDRESS_1,l_default_rec.ADDRESS_1);
         L_temp_rec.COMP_NAME         := NVL( L_temp_rec.COMP_NAME,l_default_rec.COMP_NAME);
         L_temp_rec.COMPETITOR        := NVL( L_temp_rec.COMPETITOR,l_default_rec.COMPETITOR);
      end if;

      if NOT (L_temp_rec.COMPETITOR is NOT NULL
         and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                     COMPETITOR_sheet,
                     rec.row_seq,
                     NULL,
                     NULL,
                     SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_COMPETITOR_col.EXTEND();
         svc_COMPETITOR_col(svc_COMPETITOR_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_COMPETITOR_col.COUNT SAVE EXCEPTIONS
      merge into SVC_COMPETITOR st
      using (select
                    (case
                     when L_mi_rec.JURISDICTION_CODE_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.JURISDICTION_CODE is NULL then
                          mt.JURISDICTION_CODE
                     else s1.JURISDICTION_CODE
                     end) as JURISDICTION_CODE,

                    (case
                     when l_mi_rec.WEBSITE_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.WEBSITE is NULL then
                          mt.WEBSITE
                     else s1.WEBSITE
                     end) as WEBSITE,

                    (case
                     when l_mi_rec.FAX_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.FAX is NULL then
                          mt.FAX
                      else s1.FAX
                     end) as FAX,

                    (case
                     when l_mi_rec.PHONE_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.PHONE is NULL then
                          mt.PHONE
                     ELSE s1.PHONE
                     end) as PHONE,

                    (case
                     when l_mi_rec.POST_CODE_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.POST_CODE is NULL then
                          mt.POST_CODE
                     else s1.POST_CODE
                     end) as POST_CODE,

                    (case
                     when l_mi_rec.COUNTRY_ID_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.COUNTRY_ID is NULL then
                          mt.COUNTRY_ID
                     else s1.COUNTRY_ID
                     end) as COUNTRY_ID,

                    (case
                     when l_mi_rec.STATE_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.STATE is NULL then
                          mt.STATE
                     else s1.STATE
                     end) as STATE,

                    (case
                     when l_mi_rec.CITY_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.CITY is NULL then
                          mt.CITY
                     else s1.CITY
                     end) as CITY,

                    (case
                     when l_mi_rec.ADDRESS_3_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.ADDRESS_3 is NULL then
                          mt.ADDRESS_3
                     else s1.ADDRESS_3
                     end) as ADDRESS_3,

                    (case
                     when l_mi_rec.ADDRESS_2_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.ADDRESS_2 is NULL then
                          mt.ADDRESS_2
                     else s1.ADDRESS_2
                     end) as ADDRESS_2,

                    (case
                     when l_mi_rec.ADDRESS_1_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.ADDRESS_1 is NULL then
                          mt.ADDRESS_1
                     else s1.ADDRESS_1
                     end) as ADDRESS_1,

                    (case
                     when l_mi_rec.COMP_NAME_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.COMP_NAME is NULL then
                          mt.COMP_NAME
                     else s1.COMP_NAME
                     end) as COMP_NAME,

                    (case
                     when l_mi_rec.COMPETITOR_mi = 'N'
                      and svc_COMPETITOR_col(i).action = coresvc_competitor.action_mod
                      and s1.COMPETITOR is NULL then
                          mt.COMPETITOR
                     else s1.COMPETITOR
                     end) as COMPETITOR,
                    null as dummy
             from (select svc_COMPETITOR_col(i).JURISDICTION_CODE as JURISDICTION_CODE,
                          svc_COMPETITOR_col(i).WEBSITE           as WEBSITE,
                          svc_COMPETITOR_col(i).FAX               as FAX,
                          svc_COMPETITOR_col(i).PHONE             as PHONE,
                          svc_COMPETITOR_col(i).POST_CODE         as POST_CODE,
                          svc_COMPETITOR_col(i).COUNTRY_ID        as COUNTRY_ID,
                          svc_COMPETITOR_col(i).STATE             as STATE,
                          svc_COMPETITOR_col(i).CITY              as CITY,
                          svc_COMPETITOR_col(i).ADDRESS_3         as ADDRESS_3,
                          svc_COMPETITOR_col(i).ADDRESS_2         as ADDRESS_2,
                          svc_COMPETITOR_col(i).ADDRESS_1         as ADDRESS_1,
                          svc_COMPETITOR_col(i).COMP_NAME         as COMP_NAME,
                          svc_COMPETITOR_col(i).COMPETITOR        as COMPETITOR,
                          null                                    as dummy
                     from dual) s1,
                          COMPETITOR mt
                    where mt.COMPETITOR (+) = s1.COMPETITOR
                      and 1 = 1) sq
                       on ( st.COMPETITOR = sq.COMPETITOR and
                            svc_COMPETITOR_col(i).action in (CORESVC_COMPETITOR.action_mod,CORESVC_COMPETITOR.action_del))

      when matched then
      update
         set PROCESS_ID        = svc_COMPETITOR_col(i).PROCESS_ID ,
             CHUNK_ID          = svc_COMPETITOR_col(i).CHUNK_ID ,
             ROW_SEQ           = svc_COMPETITOR_col(i).ROW_SEQ ,
             PROCESS$STATUS    = svc_COMPETITOR_col(i).PROCESS$STATUS ,
             ADDRESS_1         = sq.ADDRESS_1 ,
             ADDRESS_3         = sq.ADDRESS_3 ,
             POST_CODE         = sq.POST_CODE ,
             ACTION            = svc_COMPETITOR_col(i).ACTION,
             COMP_NAME         = sq.COMP_NAME ,
             JURISDICTION_CODE = sq.JURISDICTION_CODE ,
             ADDRESS_2         = sq.ADDRESS_2 ,
             COUNTRY_ID        = sq.COUNTRY_ID ,
             FAX               = sq.FAX ,
             WEBSITE           = sq.WEBSITE ,
             CITY              = sq.CITY ,
             PHONE             = sq.PHONE ,
             STATE             = sq.STATE ,
             CREATE_ID         = svc_COMPETITOR_col(i).CREATE_ID ,
             CREATE_DATETIME   = svc_COMPETITOR_col(i).CREATE_DATETIME ,
             LAST_UPD_ID       = svc_COMPETITOR_col(i).LAST_UPD_ID ,
             LAST_UPD_DATETIME = svc_COMPETITOR_col(i).LAST_UPD_DATETIME

      when NOT matched then
      insert(PROCESS_ID ,
             CHUNK_ID ,
             ROW_SEQ ,
             ACTION ,
             PROCESS$STATUS ,
             JURISDICTION_CODE ,
             WEBSITE ,
             FAX ,
             PHONE ,
             POST_CODE ,
             COUNTRY_ID ,
             STATE ,
             CITY ,
             ADDRESS_3 ,
             ADDRESS_2 ,
             ADDRESS_1 ,
             COMP_NAME ,
             COMPETITOR ,
             CREATE_ID ,
             CREATE_DATETIME ,
             LAST_UPD_ID ,
             LAST_UPD_DATETIME)
       values(svc_COMPETITOR_col(i).PROCESS_ID ,
              svc_COMPETITOR_col(i).CHUNK_ID ,
              svc_COMPETITOR_col(i).ROW_SEQ ,
              svc_COMPETITOR_col(i).ACTION ,
              svc_COMPETITOR_col(i).PROCESS$STATUS ,
              sq.JURISDICTION_CODE ,
              sq.WEBSITE ,
              sq.FAX ,
              sq.PHONE ,
              sq.POST_CODE ,
              sq.COUNTRY_ID ,
              sq.STATE ,
              sq.CITY ,
              sq.ADDRESS_3 ,
              sq.ADDRESS_2 ,
              sq.ADDRESS_1 ,
              sq.COMP_NAME ,
              sq.COMPETITOR ,
              svc_COMPETITOR_col(i).CREATE_ID ,
              svc_COMPETITOR_col(i).CREATE_DATETIME ,
              svc_COMPETITOR_col(i).LAST_UPD_ID ,
              svc_COMPETITOR_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS THEN
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             COMPETITOR_sheet,
                             svc_COMPETITOR_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_COMPETITOR;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COMP_STORE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_COMP_STORE.PROCESS_ID%TYPE) IS

   TYPE svc_COMP_STORE_col_typ is TABLE OF SVC_COMP_STORE%ROWTYPE;
   L_temp_rec           SVC_COMP_STORE%ROWTYPE;
   svc_COMP_STORE_col   svc_COMP_STORE_col_typ := NEW svc_COMP_STORE_col_typ();
   L_process_id         SVC_COMP_STORE.PROCESS_ID%TYPE;
   L_error              BOOLEAN:=FALSE;
   L_default_rec        SVC_COMP_STORE%ROWTYPE;

   cursor C_MANDATORY_IND is
      select jurisdiction_code_mi,
             estimated_volume_mi,
             close_date_mi,
             open_date_mi,
             selling_square_feet_mi,
             total_square_feet_mi,
             currency_code_mi,
             store_format_mi,
             fax_mi,
             phone_mi,
             post_code_mi,
             country_id_mi,
             state_mi,
             city_mi,
             address_3_mi,
             address_2_mi,
             address_1_mi,
             store_name_mi,
             competitor_mi,
             store_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_COMPETITOR.template_key
                 and wksht_key = 'COMP_STORE')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('JURISDICTION_CODE'   as JURISDICTION_CODE,
                                       'ESTIMATED_VOLUME'    as ESTIMATED_VOLUME,
                                       'CLOSE_DATE'          as CLOSE_DATE,
                                       'OPEN_DATE'           as OPEN_DATE,
                                       'SELLING_SQUARE_FEET' as SELLING_SQUARE_FEET,
                                       'TOTAL_SQUARE_FEET'   as TOTAL_SQUARE_FEET,
                                       'CURRENCY_CODE'       as CURRENCY_CODE,
                                       'STORE_FORMAT'        as STORE_FORMAT,
                                       'FAX'                 as FAX,
                                       'PHONE'               as PHONE,
                                       'POST_CODE'           as POST_CODE,
                                       'COUNTRY_ID'          as COUNTRY_ID,
                                       'STATE'               as STATE,
                                       'CITY'                as CITY,
                                       'ADDRESS_3'           as ADDRESS_3,
                                       'ADDRESS_2'           as ADDRESS_2,
                                       'ADDRESS_1'           as ADDRESS_1,
                                       'STORE_NAME'          as STORE_NAME,
                                       'COMPETITOR'          as COMPETITOR,
                                       'STORE'               as STORE,
                                       NULL                  as DUMMY));

   L_mi_rec     C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS   EXCEPTION;
   PRAGMA       EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMP_STORE';
   L_pk_columns    VARCHAR2(255)  := 'Competitor Store';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select JURISDICTION_CODE_dv,
                      ESTIMATED_VOLUME_dv,
                      CLOSE_DATE_dv,
                      OPEN_DATE_dv,
                      SELLING_SQUARE_FEET_dv,
                      TOTAL_SQUARE_FEET_dv,
                      CURRENCY_CODE_dv,
                      STORE_FORMAT_dv,
                      FAX_dv,
                      PHONE_dv,
                      POST_CODE_dv,
                      COUNTRY_ID_dv,
                      STATE_dv,
                      CITY_dv,
                      ADDRESS_3_dv,
                      ADDRESS_2_dv,
                      ADDRESS_1_dv,
                      STORE_NAME_dv,
                      COMPETITOR_dv,
                      STORE_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_COMPETITOR.template_key
                          and wksht_key  	= 'COMP_STORE')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('JURISDICTION_CODE'   as JURISDICTION_CODE,
                                                'ESTIMATED_VOLUME'    as ESTIMATED_VOLUME,
                                                'CLOSE_DATE'          as CLOSE_DATE,
                                                'OPEN_DATE'           as OPEN_DATE,
                                                'SELLING_SQUARE_FEET' as SELLING_SQUARE_FEET,
                                                'TOTAL_SQUARE_FEET'   as TOTAL_SQUARE_FEET,
                                                'CURRENCY_CODE'       as CURRENCY_CODE,
                                                'STORE_FORMAT'        as STORE_FORMAT,
                                                'FAX'                 as FAX,
                                                'PHONE'               as PHONE,
                                                'POST_CODE'           as POST_CODE,
                                                'COUNTRY_ID'          as COUNTRY_ID,
                                                'STATE'               as STATE,
                                                'CITY'                as CITY,
                                                'ADDRESS_3'           as ADDRESS_3,
                                                'ADDRESS_2'           as ADDRESS_2,
                                                'ADDRESS_1'           as ADDRESS_1,
                                                'STORE_NAME'          as STORE_NAME,
                                                'COMPETITOR'          as COMPETITOR,
                                                'STORE'               as STORE,
                                                NULL                  as DUMMY)))
   LOOP
      BEGIN
         L_default_rec.jurisdiction_code := rec.jurisdiction_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'JURISDICTION_CODE',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.estimated_volume := rec.estimated_volume_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'ESTIMATED_VOLUME',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.close_date := rec.close_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'CLOSE_DATE',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.open_date := rec.open_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'OPEN_DATE',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.selling_square_feet := rec.selling_square_feet_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'SELLING_SQUARE_FEET',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.total_square_feet := rec.total_square_feet_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'TOTAL_SQUARE_FEET',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.currency_code := rec.currency_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'CURRENCY_CODE',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.store_format := rec.store_format_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'STORE_FORMAT',
                           NULL,
                           'INV_DEFAULT');

      END;

      BEGIN
         L_default_rec.fax := rec.fax_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'FAX',
                           NULL,
                           'INV_DEFAULT');

      END;

      BEGIN
         L_default_rec.phone := rec.phone_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'PHONE',
                           NULL,
                           'INV_DEFAULT');

      END;

      BEGIN
         L_default_rec.post_code := rec.post_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'POST_CODE',
                           NULL,
                           'INV_DEFAULT');

      END;

      BEGIN
         L_default_rec.country_id := rec.country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'COUNTRY_ID',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
        L_default_rec.state := rec.state_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'STATE',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.city := rec.city_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'CITY',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.address_3 := rec.address_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'ADDRESS_3',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.address_2 := rec.address_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'ADDRESS_2',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.address_1 := rec.address_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'ADDRESS_1',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.store_name := rec.store_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'STORE_NAME',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.competitor := rec.competitor_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'COMPETITOR',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.store := rec.store_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'COMP_STORE',
                           NULL,
                           'STORE',
                           NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.GET_CELL(COMP_STORE$ACTION)              as ACTION,
                      UPPER(r.GET_CELL(COMP_STORE$JURISDICTION_CODE))   as JURISDICTION_CODE,
                      r.GET_CELL(COMP_STORE$ESTIMATED_VOLUME)    as ESTIMATED_VOLUME,
                      r.GET_CELL(COMP_STORE$CLOSE_DATE)          as CLOSE_DATE,
                      r.GET_CELL(COMP_STORE$OPEN_DATE)           as OPEN_DATE,
                      r.GET_CELL(COMP_STORE$SELLING_SQUARE_FEET) as SELLING_SQUARE_FEET,
                      r.GET_CELL(COMP_STORE$TOTAL_SQUARE_FEET)   as TOTAL_SQUARE_FEET,
                      UPPER(r.GET_CELL(COMP_STORE$CURRENCY_CODE))       as CURRENCY_CODE,
                      r.GET_CELL(COMP_STORE$STORE_FORMAT)        as STORE_FORMAT,
                      r.GET_CELL(COMP_STORE$FAX)                 as FAX,
                      r.GET_CELL(COMP_STORE$PHONE)               as PHONE,
                      r.GET_CELL(COMP_STORE$POST_CODE)           as POST_CODE,
                      UPPER(r.GET_CELL(COMP_STORE$COUNTRY_ID))          as COUNTRY_ID,
                      UPPER(r.GET_CELL(COMP_STORE$STATE))               as STATE,
                      UPPER(r.GET_CELL(COMP_STORE$CITY))                as CITY,
                      r.GET_CELL(COMP_STORE$ADDRESS_3)           as ADDRESS_3,
                      r.GET_CELL(COMP_STORE$ADDRESS_2)           as ADDRESS_2,
                      r.GET_CELL(COMP_STORE$ADDRESS_1)           as ADDRESS_1,
                      r.GET_CELL(COMP_STORE$STORE_NAME)          as STORE_NAME,
                      r.GET_CELL(COMP_STORE$COMPETITOR)          as COMPETITOR,
                      r.GET_CELL(COMP_STORE$STORE)               as STORE,
                      r.GET_ROW_SEQ()                            as ROW_SEQ
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(COMP_STORE_sheet))

   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            if C_MANDATORY_IND%ISOPEN then
               close C_MANDATORY_IND;
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.jurisdiction_code := rec.jurisdiction_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'JURISDICTION_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.estimated_volume := rec.estimated_volume;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'ESTIMATED_VOLUME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.close_date := rec.close_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'CLOSE_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.open_date := rec.open_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'OPEN_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.selling_square_feet := rec.selling_square_feet;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'SELLING_SQUARE_FEET',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.total_square_feet := rec.total_square_feet;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'TOTAL_SQUARE_FEET',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.currency_code := rec.currency_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'CURRENCY_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.store_format := rec.STORE_format;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'STORE_FORMAT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.fax := rec.fax;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'FAX',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.phone := rec.phone;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'PHONE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.post_code := rec.post_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'POST_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.country_id := rec.country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.state := rec.state;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'STATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.city := rec.city;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'CITY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.address_3 := rec.address_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'ADDRESS_3',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.address_2 := rec.address_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'ADDRESS_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.address_1 := rec.address_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'ADDRESS_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.store_name := rec.STORE_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'STORE_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.competitor := rec.competitor;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'COMPETITOR',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.store := rec.store;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_sheet,
                            rec.row_seq,
                            'STORE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;

      if rec.action = CORESVC_COMPETITOR.action_new then
         L_temp_rec.JURISDICTION_CODE   := NVL( L_temp_rec.JURISDICTION_CODE,l_default_rec.JURISDICTION_CODE);
         L_temp_rec.ESTIMATED_VOLUME    := NVL( L_temp_rec.ESTIMATED_VOLUME,l_default_rec.ESTIMATED_VOLUME);
         L_temp_rec.CLOSE_DATE          := NVL( L_temp_rec.CLOSE_DATE,l_default_rec.CLOSE_DATE);
         L_temp_rec.OPEN_DATE           := NVL( L_temp_rec.OPEN_DATE,l_default_rec.OPEN_DATE);
         L_temp_rec.SELLING_SQUARE_FEET := NVL( L_temp_rec.SELLING_SQUARE_FEET,l_default_rec.SELLING_SQUARE_FEET);
         L_temp_rec.TOTAL_SQUARE_FEET   := NVL( L_temp_rec.TOTAL_SQUARE_FEET,l_default_rec.TOTAL_SQUARE_FEET);
         L_temp_rec.CURRENCY_CODE       := NVL( L_temp_rec.CURRENCY_CODE,l_default_rec.CURRENCY_CODE);
         L_temp_rec.STORE_FORMAT        := NVL( L_temp_rec.STORE_FORMAT,l_default_rec.STORE_FORMAT);
         L_temp_rec.FAX                 := NVL( L_temp_rec.FAX,l_default_rec.FAX);
         L_temp_rec.PHONE               := NVL( L_temp_rec.PHONE,l_default_rec.PHONE);
         L_temp_rec.POST_CODE           := NVL( L_temp_rec.POST_CODE,l_default_rec.POST_CODE);
         L_temp_rec.COUNTRY_ID          := NVL( L_temp_rec.COUNTRY_ID,l_default_rec.COUNTRY_ID);
         L_temp_rec.STATE               := NVL( L_temp_rec.STATE,l_default_rec.STATE);
         L_temp_rec.CITY                := NVL( L_temp_rec.CITY,l_default_rec.CITY);
         L_temp_rec.ADDRESS_3           := NVL( L_temp_rec.ADDRESS_3,l_default_rec.ADDRESS_3);
         L_temp_rec.ADDRESS_2           := NVL( L_temp_rec.ADDRESS_2,l_default_rec.ADDRESS_2);
         L_temp_rec.ADDRESS_1           := NVL( L_temp_rec.ADDRESS_1,l_default_rec.ADDRESS_1);
         L_temp_rec.STORE_NAME          := NVL( L_temp_rec.STORE_NAME,l_default_rec.STORE_NAME);
         L_temp_rec.COMPETITOR          := NVL( L_temp_rec.COMPETITOR,l_default_rec.COMPETITOR);
         L_temp_rec.STORE               := NVL( L_temp_rec.STORE,l_default_rec.STORE);
      end if;
	    if (rec.action IN (action_mod,action_del)
          and L_temp_rec.store is NULL
          and 1 = 1) then
            WRITE_S9T_ERROR( I_file_id,
                          COMP_STORE_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_COMP_STORE_col.EXTEND();
         svc_COMP_STORE_col(svc_COMP_STORE_col.count()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_COMP_STORE_col.COUNT SAVE EXCEPTIONS
      merge into SVC_COMP_STORE st
      using (select (case
                     when L_mi_rec.jurisdiction_code_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.jurisdiction_code is NULL then
                          mt.jurisdiction_code
                      else s1.jurisdiction_code
                      end) as jurisdiction_code,
                    (case
                     when L_mi_rec.estimated_volume_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.estimated_volume is NULL then
                          mt.estimated_volume
                     else s1.estimated_volume
                     end) as estimated_volume,
                    (case
                     when L_mi_rec.close_date_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.close_date is NULL then
                          mt.close_date
                     else s1.close_date
                     end) as close_date,
                    (case
                     when L_mi_rec.open_date_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.open_date is NULL then
                          mt.open_date
                     else s1.open_date
                     end) as open_date,
                    (case
                     when L_mi_rec.selling_square_feet_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.selling_square_feet is NULL then
                          mt.selling_square_feet
                     else s1.selling_square_feet
                     end) as selling_square_feet,
                    (case
                     when L_mi_rec.total_square_feet_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.total_square_feet is NULL then
                          mt.total_square_feet
                     else s1.total_square_feet
                     end) as total_square_feet,
                    (case
                     when L_mi_rec.currency_code_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.currency_code is NULL then
                          mt.currency_code
                     else s1.currency_code
                     end) as currency_code,
                    (case
                     when L_mi_rec.STORE_format_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.STORE_format is NULL then
                          mt.STORE_format
                     else s1.STORE_format
                     end) as STORE_format,
                    (case
                     when L_mi_rec.fax_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.fax is NULL then
                          mt.fax
                     else s1.fax
                     end) as fax,
                    (case
                     when L_mi_rec.phone_mi = 'N'
                      and svc_COMP_STORE_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.phone is NULL then
                          mt.phone
                     else s1.phone
                     end) as phone,
                    (case
                     when l_mi_rec.post_code_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.post_code is NULL then
                          mt.post_code
                     else s1.post_code
                     end) as post_code,
                    (case
                     when l_mi_rec.country_id_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.country_id is NULL then
                          mt.country_id
                     else s1.country_id
                     end) as country_id,
                    (case
                     when l_mi_rec.state_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.state is NULL then
                          mt.state
                     else s1.state
                     end) as state,
                    (case
                     when l_mi_rec.city_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.city is NULL then
                          mt.city
                     else s1.city
                     end) as city,
                    (case
                     when l_mi_rec.address_3_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.address_3 is NULL then
                          mt.address_3
                     else s1.address_3
                     end) as address_3,
                     (case
                     when l_mi_rec.address_2_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.address_2 is NULL then
                          mt.address_2
                     else s1.address_2
                     end) as address_2,
                    (case
                     when l_mi_rec.address_1_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.address_1 is NULL then
                          mt.address_1
                     else s1.address_1
                     end) as address_1,
                    (case
                     when l_mi_rec.store_name_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.store_name is NULL then
                          mt.store_name
                     else s1.store_name
                     end) as store_name,
                    (case
                     when l_mi_rec.competitor_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.competitor is NULL then
                          mt.competitor
                     else s1.competitor
                     end) as competitor,
                    (case
                     when l_mi_rec.store_mi = 'N'
                      and svc_comp_store_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.store is NULL then
                          mt.store
                     else s1.store
                     end) as store,
                     null as dummy
                from (select svc_COMP_STORE_col(i).jurisdiction_code   as jurisdiction_code,
                             svc_COMP_STORE_col(i).estimated_volume    as estimated_volume,
                             svc_COMP_STORE_col(i).close_date          as close_date,
                             svc_COMP_STORE_col(i).open_date           as open_date,
                             svc_COMP_STORE_col(i).selling_square_feet as selling_square_feet,
                             svc_COMP_STORE_col(i).total_square_feet   as total_square_feet,
                             svc_COMP_STORE_col(i).currency_code       as currency_code,
                             svc_COMP_STORE_col(i).store_format        as store_format,
                             svc_COMP_STORE_col(i).fax                 as fax,
                             svc_COMP_STORE_col(i).phone               as phone,
                             svc_COMP_STORE_col(i).post_code           as post_code,
                             svc_COMP_STORE_col(i).country_id          as country_id,
                             svc_COMP_STORE_col(i).state               as state,
                             svc_COMP_STORE_col(i).city                as city,
                             svc_COMP_STORE_col(i).address_3           as address_3,
                             svc_COMP_STORE_col(i).address_2           as address_2,
                             svc_COMP_STORE_col(i).address_1           as address_1,
                             svc_COMP_STORE_col(i).store_name          as store_name,
                             svc_COMP_STORE_col(i).competitor          as competitor,
                             svc_COMP_STORE_col(i).store               as store,
                             null                                      as dummy
                        from dual) s1,
                             COMP_STORE mt
                       where mt.STORE (+)     = s1.STORE
                         and 1 = 1) sq
                          on (st.STORE = sq.STORE
                          and svc_COMP_STORE_col(i).action in (CORESVC_COMPETITOR.action_mod,CORESVC_COMPETITOR.action_del))
      when matched then
      update
         set process_id          = svc_COMP_STORE_col(i).process_id ,
             chunk_id            = svc_COMP_STORE_col(i).chunk_id ,
             row_seq             = svc_COMP_STORE_col(i).row_seq ,
             action              = svc_COMP_STORE_col(i).action,
             process$status      = svc_COMP_STORE_col(i).process$status ,
             estimated_volume    = sq.estimated_volume ,
             address_1           = sq.address_1 ,
             address_3           = sq.address_3 ,
             post_code           = sq.post_code ,
             open_date           = sq.open_date ,
             store_name          = sq.store_name ,
             jurisdiction_code   = sq.jurisdiction_code ,
             address_2           = sq.address_2 ,
             country_id          = sq.country_id ,
             currency_code       = sq.currency_code ,
             fax                 = sq.fax ,
             competitor          = sq.competitor ,
             city                = sq.city ,
             total_square_feet   = sq.total_square_feet ,
             selling_square_feet = sq.selling_square_feet ,
             phone               = sq.phone ,
             state               = sq.state ,
             close_date          = sq.close_date ,
             store_format        = sq.store_format ,
             create_id           = svc_COMP_STORE_col(i).create_id ,
             create_datetime     = svc_COMP_STORE_col(i).create_datetime ,
             last_upd_id         = svc_COMP_STORE_col(i).last_upd_id ,
             last_upd_datetime   = svc_COMP_STORE_col(i).last_upd_datetime

      when NOT matched then
      insert( process_id ,
              chunk_id ,
              row_seq ,
              action ,
              process$status ,
              jurisdiction_code ,
              estimated_volume ,
              close_date ,
              open_date ,
              selling_square_feet ,
              total_square_feet ,
              currency_code ,
              store_format ,
              fax ,
              phone ,
              post_code ,
              country_id ,
              state ,
              city ,
              address_3 ,
              address_2 ,
              address_1 ,
              store_name ,
              competitor ,
              store ,
              create_id ,
              create_datetime ,
              last_upd_id ,
              last_upd_datetime)
      values (svc_COMP_STORE_col(i).process_id ,
              svc_COMP_STORE_col(i).chunk_id ,
              svc_COMP_STORE_col(i).row_seq ,
              svc_COMP_STORE_col(i).action ,
              svc_COMP_STORE_col(i).process$status ,
              sq.jurisdiction_code ,
              sq.estimated_volume ,
              sq.close_date ,
              sq.open_date ,
              sq.selling_square_feet ,
              sq.total_square_feet ,
              sq.currency_code ,
              sq.store_format ,
              sq.fax ,
              sq.phone ,
              sq.post_code ,
              sq.country_id ,
              sq.state ,
              sq.city ,
              sq.address_3 ,
              sq.address_2 ,
              sq.address_1 ,
              sq.store_name ,
              sq.competitor ,
              sq.store ,
              svc_COMP_STORE_col(i).create_id ,
              svc_COMP_STORE_col(i).create_datetime ,
              svc_COMP_STORE_col(i).last_upd_id ,
              svc_COMP_STORE_col(i).last_upd_datetime);

   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            COMP_STORE_sheet,
                            svc_COMP_STORE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
        END LOOP;
   END;
END PROCESS_S9T_COMP_STORE;
---------------------------------------=-----------------------------------------
PROCEDURE PROCESS_S9T_COMP_STORE_LINK(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                      I_process_id   IN   SVC_COMP_STORE_LINK.process_id%TYPE) IS

   TYPE svc_COMP_STORE_LINK_col_typ IS TABLE OF SVC_COMP_STORE_LINK%ROWTYPE;
   L_temp_rec                SVC_COMP_STORE_LINK%ROWTYPE;
   svc_COMP_STORE_LINK_col   svc_COMP_STORE_LINK_col_typ := NEW svc_COMP_STORE_LINK_col_typ();
   L_process_id              SVC_COMP_STORE_LINK.process_id%TYPE;
   L_error                   BOOLEAN:=FALSE;
   L_default_rec             SVC_COMP_STORE_LINK%ROWTYPE;

   cursor C_MANDATORY_IND is
      select UOM_mi,
             DISTANCE_mi,
             RANK_mi,
             TAR_COMP_IND_MI,
             COMP_STORE_mi,
             STORE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_COMPETITOR.template_key
                 and wksht_key    = 'COMP_STORE_LINK')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('UOM'             as UOM,
                                       'DISTANCE'        as DISTANCE,
                                       'RANK'            as RANK,
                                       'TAR_COMP_IND' as tar_comp_ind,
                                       'COMP_STORE'      as COMP_STORE,
                                       'STORE'           as STORE,
                                       NULL              as dummy));

   L_mi_rec     C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS   EXCEPTION;
   PRAGMA       EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMP_STORE_LINK';
   L_pk_columns    VARCHAR2(255)  := 'Competitive Store,Store';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select UOM_dv,
                      DISTANCE_dv,
                      RANK_dv,
                      TAR_COMP_IND_dv,
                      COMP_STORE_dv,
                      STORE_dv,
                      NULL as DUMMY
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_COMPETITOR.template_key
                          and wksht_key = 'COMP_STORE_LINK')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('UOM'             as UOM,
                                                'DISTANCE'        as DISTANCE,
                                                'RANK'            as RANK,
                                                'TAR_COMP_IND'	as tar_comp_ind,
                                                'COMP_STORE'      as COMP_STORE,
                                                'STORE'           as STORE,
                                                NULL              as DUMMY )))

   LOOP
      BEGIN
         L_default_rec.UOM := rec.UOM_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMP_STORE_LINK',
                            NULL,
                            'UOM',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.DISTANCE := rec.DISTANCE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMP_STORE_LINK',
                            NULL,
                            'DISTANCE',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.RANK := rec.RANK_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMP_STORE_LINK',
                            NULL,
                            'RANK',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.tar_comp_ind := rec.TAR_COMP_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMP_STORE_LINK',
                            NULL,
                            'tar_comp_ind',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.COMP_STORE := rec.COMP_STORE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMP_STORE_LINK',
                            NULL,
                            'COMP_STORE',
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.STORE := rec.STORE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'COMP_STORE_LINK',
                            NULL,
                            'STORE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.GET_CELL(COMP_STORE_LINK$ACTION)       as ACTION,
                      r.GET_CELL(COMP_STORE_LINK$UOM)          as UOM,
                      r.GET_CELL(COMP_STORE_LINK$DISTANCE)     as DISTANCE,
                      r.GET_CELL(COMP_STORE_LINK$RANK)         as RANK,
                      r.GET_CELL(COMP_STORE_LINK$tar_comp_ind) as tar_comp_ind,
                      r.GET_CELL(COMP_STORE_LINK$COMP_STORE)   as COMP_STORE,
                      r.GET_CELL(COMP_STORE_LINK$STORE)        as STORE,
                      r.GET_ROW_SEQ()                          as ROW_SEQ
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(COMP_STORE_LINK_sheet))

   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            if C_MANDATORY_IND%ISOPEN then
               close C_MANDATORY_IND;
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_LINK_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.UOM := rec.UOM;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_LINK_sheet,
                            rec.row_seq,
                            'UOM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.DISTANCE := rec.DISTANCE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_LINK_sheet,
                            rec.row_seq,
                            'DISTANCE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.RANK := rec.RANK;
      EXCEPTION
        when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_LINK_sheet,
                            rec.row_seq,
                            'RANK',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.tar_comp_ind := rec.tar_comp_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_LINK_sheet,
                            rec.row_seq,
                            'tar_comp_ind',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.COMP_STORE := rec.COMP_STORE;
      EXCEPTION
        when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_LINK_sheet,
                            rec.row_seq,
                            'COMP_STORE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.STORE := rec.STORE;
      EXCEPTION
        when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_STORE_LINK_sheet,
                            rec.row_seq,
                            'STORE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_COMPETITOR.action_new then
         L_temp_rec.UOM          := NVL( L_temp_rec.UOM,L_default_rec.UOM);
         L_temp_rec.DISTANCE     := NVL( L_temp_rec.DISTANCE,L_default_rec.DISTANCE);
         L_temp_rec.RANK         := NVL( L_temp_rec.RANK,L_default_rec.RANK);
         L_temp_rec.tar_comp_ind := NVL( L_temp_rec.tar_comp_ind,L_default_rec.tar_comp_ind);
         L_temp_rec.COMP_STORE   := NVL( L_temp_rec.COMP_STORE,L_default_rec.COMP_STORE);
         L_temp_rec.STORE        := NVL( L_temp_rec.STORE,L_default_rec.STORE);
      end if;

      if NOT (L_temp_rec.COMP_STORE is NOT NULL
         and L_temp_rec.STORE is NOT NULL
         and 1 = 1)then
         WRITE_S9T_ERROR(I_file_id,
                         COMP_STORE_LINK_sheet,
                         rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
       L_error := TRUE;
      end if;

      if NOT L_error then
         svc_COMP_STORE_LINK_col.EXTEND();
         svc_COMP_STORE_LINK_col(svc_COMP_STORE_LINK_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_COMP_STORE_LINK_col.COUNT SAVE EXCEPTIONS
      merge into SVC_COMP_STORE_LINK st
      using (select (case
                     when L_mi_rec.UOM_mi = 'N'
                      and svc_COMP_STORE_LINK_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.UOM is NULL then
                          mt.UOM
                     else s1.UOM
                     end) as UOM,

                    (case
                     when L_mi_rec.DISTANCE_mi = 'N'
                      and svc_COMP_STORE_LINK_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.DISTANCE is NULL then
                          mt.DISTANCE
                     else s1.DISTANCE
                     end) as DISTANCE,

                    (case
                     when L_mi_rec.RANK_mi = 'N'
                      and svc_COMP_STORE_LINK_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.RANK is NULL then
                          mt.RANK
                     else s1.RANK
                     end) as RANK,

                    (case
                     when L_mi_rec.TAR_COMP_IND_MI = 'N'
                      and svc_COMP_STORE_LINK_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.tar_comp_ind is NULL then
                          mt.target_comp_ind
                     else s1.tar_comp_ind
                     end) as tar_comp_ind,

                    (case
                     when L_mi_rec.COMP_STORE_mi    = 'N'
                      and svc_COMP_STORE_LINK_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.COMP_STORE is NULL then
                          mt.COMP_STORE
                     else s1.COMP_STORE
                     end) as COMP_STORE,

                    (case
                     when L_mi_rec.STORE_mi = 'N'
                      and svc_COMP_STORE_LINK_col(i).action = CORESVC_COMPETITOR.action_mod
                      and s1.STORE IS NULL then
                          mt.STORE
                     else s1.STORE
                     end) as STORE,
                     null as dummy
               from (select svc_COMP_STORE_LINK_col(i).UOM       as UOM,
                         svc_COMP_STORE_LINK_col(i).DISTANCE     as DISTANCE,
                         svc_COMP_STORE_LINK_col(i).RANK         as RANK,
                         svc_COMP_STORE_LINK_col(i).tar_comp_ind as tar_comp_ind,
                         svc_COMP_STORE_LINK_col(i).COMP_STORE   as COMP_STORE,
                         svc_COMP_STORE_LINK_col(i).STORE        as STORE,
                         null as dummy
                    from dual) s1,
                         COMP_STORE_LINK mt
                   where mt.COMP_STORE (+) = s1.COMP_STORE
                     and mt.STORE (+)      = s1.STORE
                     and 1 = 1) sq
                      on (st.COMP_STORE = sq.COMP_STORE
                      and st.STORE      = sq.STORE
                      and svc_COMP_STORE_LINK_col(i).action in (CORESVC_COMPETITOR.action_mod,CORESVC_COMPETITOR.action_del ))

      when matched then
      update
         set PROCESS_ID      	 = svc_COMP_STORE_LINK_col(i).PROCESS_ID ,
             CHUNK_ID          = svc_COMP_STORE_LINK_col(i).CHUNK_ID ,
             ROW_SEQ           = svc_COMP_STORE_LINK_col(i).ROW_SEQ ,
             PROCESS$STATUS    = svc_COMP_STORE_LINK_col(i).PROCESS$STATUS ,
             ACTION            = svc_COMP_STORE_LINK_col(i).ACTION,
             RANK              = sq.RANK ,
             tar_comp_ind      = sq.tar_comp_ind ,
             UOM               = sq.UOM ,
             DISTANCE          = sq.DISTANCE ,
             CREATE_ID      	 = svc_COMP_STORE_LINK_col(i).CREATE_ID ,
             CREATE_DATETIME   = svc_COMP_STORE_LINK_col(i).CREATE_DATETIME ,
             LAST_UPD_ID       = svc_COMP_STORE_LINK_col(i).LAST_UPD_ID ,
             LAST_UPD_DATETIME = svc_COMP_STORE_LINK_col(i).LAST_UPD_DATETIME

      when NOT matched then
      insert(PROCESS_ID ,
             CHUNK_ID ,
             ROW_SEQ ,
             ACTION ,
             PROCESS$STATUS ,
             UOM ,
             DISTANCE ,
             RANK ,
             tar_comp_ind ,
             COMP_STORE ,
             STORE ,
             CREATE_ID ,
             CREATE_DATETIME ,
             LAST_UPD_ID ,
             LAST_UPD_DATETIME)
      values(svc_COMP_STORE_LINK_col(i).PROCESS_ID ,
             svc_COMP_STORE_LINK_col(i).CHUNK_ID ,
             svc_COMP_STORE_LINK_col(i).ROW_SEQ ,
             svc_COMP_STORE_LINK_col(i).ACTION ,
             svc_COMP_STORE_LINK_col(i).PROCESS$STATUS ,
             sq.UOM ,
             sq.DISTANCE ,
             sq.RANK ,
             sq.tar_comp_ind ,
             sq.COMP_STORE ,
             sq.STORE ,
             svc_COMP_STORE_LINK_col(i).CREATE_ID ,
             svc_COMP_STORE_LINK_col(i).CREATE_DATETIME ,
             svc_COMP_STORE_LINK_col(i).LAST_UPD_ID ,
             svc_COMP_STORE_LINK_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
              L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
             end if;

            WRITE_S9T_ERROR( I_file_id,
                            COMP_STORE_LINK_sheet,
                            svc_COMP_STORE_LINK_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_COMP_STORE_LINK;
-----------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_COMP_SHOPPER(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_COMP_SHOPPER.PROCESS_ID%TYPE) IS

   TYPE svc_COMP_SHOPPER_col_typ IS TABLE OF SVC_COMP_SHOPPER%ROWTYPE;
   L_temp_rec           SVC_COMP_SHOPPER%ROWTYPE;
   svc_COMP_SHOPPER_col svc_COMP_SHOPPER_col_typ :=NEW svc_COMP_SHOPPER_col_typ();
   L_error              BOOLEAN                  :=FALSE;
   L_process_id         SVC_COMP_SHOPPER.PROCESS_ID%TYPE;
   L_default_rec        SVC_COMP_SHOPPER%ROWTYPE;
   cursor C_MANDATORY_IND is
      select SHOPPER_FAX_mi,
             SHOPPER_PHONE_mi,
             SHOPPER_NAME_mi,
             SHOPPER_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = CORESVC_COMPETITOR.template_key
                 and wksht_key                                 = 'COMP_SHOPPER'
              ) PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('SHOPPER_FAX'   as SHOPPER_FAX,
                                                                 'SHOPPER_PHONE' as SHOPPER_PHONE,
                                                                 'SHOPPER_NAME'  as SHOPPER_NAME,
                                                                 'SHOPPER'       as SHOPPER,
                                                                  NULL           as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_COMP_SHOPPER';
   L_pk_columns    VARCHAR2(255)  := 'Shopper';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
   FOR rec IN
   (select SHOPPER_FAX_dv,
           SHOPPER_PHONE_dv,
           SHOPPER_NAME_dv,
           SHOPPER_dv,
           NULL as dummy
      from (select column_key,
                   default_value
              from s9t_tmpl_cols_def
             where template_key                                  = CORESVC_COMPETITOR.template_key
               and wksht_key                                     = 'COMP_SHOPPER')
             PIVOT (MAX(default_value) AS dv FOR (column_key) IN ('SHOPPER_FAX'   as SHOPPER_FAX,
                                                                  'SHOPPER_PHONE' as SHOPPER_PHONE,
                                                                  'SHOPPER_NAME'  as SHOPPER_NAME,
                                                                  'SHOPPER'       as SHOPPER,
                                                                   NULL           as dummy))) LOOP
      BEGIN
         L_default_rec.SHOPPER_FAX := rec.SHOPPER_FAX_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'COMP_SHOPPER',
                             NULL,
                            'SHOPPER_FAX',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SHOPPER_PHONE := rec.SHOPPER_PHONE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'COMP_SHOPPER',
                             NULL,
                            'SHOPPER_PHONE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SHOPPER_NAME := rec.SHOPPER_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'COMP_SHOPPER',
                             NULL,
                            'SHOPPER_NAME',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
      L_default_rec.SHOPPER := rec.SHOPPER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'COMP_SHOPPER',
                             NULL,
                            'SHOPPER',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.GET_CELL(COMP_SHOPPER$Action)        as ACTION,
                      r.GET_CELL(COMP_SHOPPER$SHOPPER_FAX)   as SHOPPER_FAX,
                      r.GET_CELL(COMP_SHOPPER$SHOPPER_PHONE) as SHOPPER_PHONE,
                      r.GET_CELL(COMP_SHOPPER$SHOPPER_NAME)  as SHOPPER_NAME,
                      r.GET_CELL(COMP_SHOPPER$SHOPPER)       as SHOPPER,
                      r.GET_ROW_SEQ()                        as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = SHEET_NAME_TRANS(COMP_SHOPPER_sheet))
   LOOP
      L_temp_rec :=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.ACTION := rec.ACTION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOPPER_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.SHOPPER_FAX := rec.SHOPPER_FAX;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOPPER_sheet,
                            rec.row_seq,
                            'SHOPPER_FAX',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.SHOPPER_PHONE := rec.SHOPPER_PHONE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOPPER_sheet,
                            rec.row_seq,
                            'SHOPPER_PHONE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.SHOPPER_NAME := rec.SHOPPER_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOPPER_sheet,
                            rec.row_seq,
                            'SHOPPER_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.SHOPPER := rec.SHOPPER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            COMP_SHOPPER_sheet,
                            rec.row_seq,
                            'SHOPPER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_COMPETITOR.action_new then
         L_temp_rec.SHOPPER_FAX   := NVL( L_temp_rec.SHOPPER_FAX,
                                          L_default_rec.SHOPPER_FAX);
         L_temp_rec.SHOPPER_PHONE := NVL( L_temp_rec.SHOPPER_PHONE,
                                          L_default_rec.SHOPPER_PHONE);
         L_temp_rec.SHOPPER_NAME  := NVL( L_temp_rec.SHOPPER_NAME,
                                          L_default_rec.SHOPPER_NAME);
         L_temp_rec.SHOPPER       := NVL( L_temp_rec.SHOPPER,
                                          L_default_rec.SHOPPER);
      end if;

      if L_temp_rec.shopper is NULL
         and L_temp_rec.action IN (action_mod, action_del) then
         WRITE_S9T_ERROR(I_file_id,
                         COMP_SHOPPER_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_COMP_SHOPPER_col.EXTEND();
         svc_COMP_SHOPPER_col(svc_COMP_SHOPPER_col.COUNT()) := L_temp_rec;
      end if;

   END LOOP;
BEGIN
   forall i IN 1..svc_COMP_SHOPPER_col.count SAVE EXCEPTIONS
   merge into SVC_COMP_SHOPPER st using
   (select (case
               when L_mi_rec.SHOPPER_FAX_mi = 'N'
                and svc_COMP_SHOPPER_col(i).action = CORESVC_COMPETITOR.action_mod
                and s1.SHOPPER_FAX   is NULL
               then mt.SHOPPER_FAX
               else s1.SHOPPER_FAX
               end) as SHOPPER_FAX,
           (case
               when L_mi_rec.SHOPPER_PHONE_mi = 'N'
                and svc_COMP_SHOPPER_col(i).action = CORESVC_COMPETITOR.action_mod
                and s1.SHOPPER_PHONE is NULL
               then mt.SHOPPER_PHONE
               else s1.SHOPPER_PHONE
               end) as SHOPPER_PHONE,
           (case
               when L_mi_rec.SHOPPER_NAME_mi = 'N'
                and svc_COMP_SHOPPER_col(i).action = CORESVC_COMPETITOR.action_mod
                and s1.SHOPPER_NAME  is NULL
               then mt.SHOPPER_NAME
               else s1.SHOPPER_NAME
               end) as SHOPPER_NAME,
           (CASE
               when L_mi_rec.SHOPPER_mi = 'N'
                and svc_COMP_SHOPPER_col(i).action = CORESVC_COMPETITOR.action_mod
                and s1.SHOPPER       is NULL
               then mt.SHOPPER
               else s1.SHOPPER
               end) as SHOPPER,
               null as dummy
      from (select svc_COMP_SHOPPER_col(i).SHOPPER_FAX   as SHOPPER_FAX,
                   svc_COMP_SHOPPER_col(i).SHOPPER_PHONE as SHOPPER_PHONE,
                   svc_COMP_SHOPPER_col(i).SHOPPER_NAME  as SHOPPER_NAME,
                   svc_COMP_SHOPPER_col(i).SHOPPER       as SHOPPER,
                   null                                  as dummy
              from dual) s1,
                   COMP_SHOPPER mt
     where mt.SHOPPER (+) = s1.SHOPPER   and
           1 = 1) sq ON (
           st.SHOPPER      = sq.SHOPPER
           and svc_COMP_SHOPPER_col(i).action IN (CORESVC_COMPETITOR.action_mod , CORESVC_COMPETITOR.action_del))
     when matched then
        update
           set PROCESS_ID        = svc_COMP_SHOPPER_col(i).PROCESS_ID ,
               CHUNK_ID          = svc_COMP_SHOPPER_col(i).CHUNK_ID ,
               ACTION            = svc_COMP_SHOPPER_col(i).ACTION,
               ROW_SEQ           = svc_COMP_SHOPPER_col(i).ROW_SEQ ,
               PROCESS$STATUS    = svc_COMP_SHOPPER_col(i).PROCESS$STATUS ,
               SHOPPER_NAME      = sq.SHOPPER_NAME ,
               SHOPPER_PHONE     = sq.SHOPPER_PHONE ,
               SHOPPER_FAX       = sq.SHOPPER_FAX ,
               CREATE_ID         = svc_COMP_SHOPPER_col(i).CREATE_ID ,
               CREATE_DATETIME   = svc_COMP_SHOPPER_col(i).CREATE_DATETIME ,
               LAST_UPD_ID       = svc_COMP_SHOPPER_col(i).LAST_UPD_ID ,
               LAST_UPD_DATETIME = svc_COMP_SHOPPER_col(i).LAST_UPD_DATETIME
     when NOT matched then
        insert(PROCESS_ID ,
               CHUNK_ID ,
               ROW_SEQ ,
               ACTION ,
               PROCESS$STATUS ,
               SHOPPER_FAX ,
               SHOPPER_PHONE ,
               SHOPPER_NAME ,
               SHOPPER ,
               CREATE_ID ,
               CREATE_DATETIME ,
               LAST_UPD_ID ,
               LAST_UPD_DATETIME)
        values (svc_COMP_SHOPPER_col(i).PROCESS_ID ,
                svc_COMP_SHOPPER_col(i).CHUNK_ID ,
                svc_COMP_SHOPPER_col(i).ROW_SEQ ,
                svc_COMP_SHOPPER_col(i).ACTION ,
                svc_COMP_SHOPPER_col(i).PROCESS$STATUS ,
                sq.SHOPPER_FAX ,
                sq.SHOPPER_PHONE ,
                sq.SHOPPER_NAME ,
                sq.SHOPPER ,
                svc_COMP_SHOPPER_col(i).CREATE_ID ,
                svc_COMP_SHOPPER_col(i).CREATE_DATETIME ,
                svc_COMP_SHOPPER_col(i).LAST_UPD_ID ,
                svc_COMP_SHOPPER_col(i).LAST_UPD_DATETIME);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          COMP_SHOPPER_sheet,
                          svc_COMP_SHOPPER_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
   END;
END PROCESS_S9T_COMP_SHOPPER;
--------------------------------------------------------------------------------
---  Name: PROCESS_S9T
---Purpose: Called from UI to uplaod data from excel to staging table
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count 	   IN OUT   NUMBER,
                     I_file_id	      IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id 	   IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_COMPETITOR.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	      EXCEPTION;
   PRAGMA	      EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_COMPETITOR(I_file_id,
                             I_process_id);

      PROCESS_S9T_COMP_STORE(I_file_id,
                             I_process_id);

      PROCESS_S9T_COMP_STORE_LINK(I_file_id,
                                  I_process_id);

      PROCESS_S9T_COMP_SHOPPER(I_file_id,
                               I_process_id);
   end if;

   O_error_count := Lp_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert into s9t_errors
           values Lp_s9t_errors_tab(i);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();

   --Update process$status in svc_process_tracker
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPETITOR_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_competitor_temp_rec   IN       COMPETITOR%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMPETITOR_INS';

BEGIN
   insert into competitor
		  values I_competitor_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END EXEC_COMPETITOR_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPETITOR_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_competitor_temp_rec   IN       COMPETITOR%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMPETITOR_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMPETITOR';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

--Cursor to lock the record
   cursor C_LOCK_COMPETITOR_UPD is
      select 'x'
        from competitor
       where competitor = I_competitor_temp_rec.competitor
         for update nowait;

BEGIN
   open C_LOCK_COMPETITOR_UPD;
   close C_LOCK_COMPETITOR_UPD;

   update competitor
  	   set row = I_competitor_temp_rec
    where 1 = 1
      and competitor = I_competitor_temp_rec.competitor;
	return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_competitor_temp_rec.competitor,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_COMPETITOR_UPD%ISOPEN then
         close C_LOCK_COMPETITOR_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_COMPETITOR_UPD;
--------------------------------------------------------------------------------------------
FUNCTION EXEC_COMPETITOR_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_competitor_temp_rec   IN       COMPETITOR%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMPETITOR_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPETITOR';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

--Cursor to lock the record
   cursor C_LOCK_COMPETITOR_DEL is
      select 'x'
        from competitor
       where competitor = I_competitor_temp_rec.competitor
         for update nowait;

BEGIN
   open C_LOCK_COMPETITOR_DEL;
   close C_LOCK_COMPETITOR_DEL;
   delete from competitor
      where 1 = 1
	     and competitor = I_competitor_temp_rec.competitor;
	return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_competitor_temp_rec.competitor,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_COMPETITOR_DEL%ISOPEN then
         close C_LOCK_COMPETITOR_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_COMPETITOR_DEL;
------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STATECOUNTRYJURIS(O_error_message  	    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_state               IN OUT   STATE.STATE%TYPE,
                                    O_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                                    O_country_id          IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                                    O_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                                    O_jurisdiction_code   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                                    O_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                    O_error 			       IN OUT   BOOLEAN,
                                    I_rec 				    IN       C_SVC_COMPETITOR%ROWTYPE,
                                    I_calling_module      IN       VARCHAR2,
                                    I_calling_context     IN       VARCHAR2)
RETURN BOOLEAN IS

	L_program           VARCHAR2(64) := 'CORESVC_COMPETITOR.VALIDATE_STATECOUNTRYJURIS';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPETITOR';
   L_calling_context   VARCHAR2(12);
   L_dup_exists        VARCHAR2(1) := 'N';

BEGIN
   L_calling_context := I_calling_context;
   if (L_calling_context = ADDRESS_SQL.COUNTRY_CON and O_country_id is NOT NULL)
      or (L_calling_context = ADDRESS_SQL.STATE_CON and O_state is NOT NULL)
      or (L_calling_context = ADDRESS_SQL.JURIS_CON and O_jurisdiction_code is NOT NULL) then
      if O_jurisdiction_code is NOT NULL then
         L_calling_context := ADDRESS_SQL.JURIS_CON;
      elsif O_state is NOT NULL then
         L_calling_context := ADDRESS_SQL.STATE_CON;
      end if;
      if ADDRESS_SQL.CHECK_ADDR (O_error_message,
                                 O_country_id,
                                 O_country_desc,
                                 O_state,
                                 O_state_desc,
                                 O_jurisdiction_code,
                                 O_jurisdiction_desc,
                                 L_dup_exists,
                                 I_calling_module,
                                 L_calling_context) = FALSE then
         O_error := TRUE;
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;
      if L_dup_exists = 'Y' then
         O_error := TRUE;
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;
   else
      if I_calling_module = ADDRESS_SQL.COUNTRY_CON then
         O_country_desc       := NULL;
         O_jurisdiction_code  := NULL;
         O_jurisdiction_desc  := NULL;
         O_state              := NULL;
         O_state_desc 			:= NULL;
      elsif I_calling_module = ADDRESS_SQL.STATE_CON then
         O_state_desc         := NULL;
         O_jurisdiction_code  := NULL;
         O_jurisdiction_desc  := NULL;
      elsif I_calling_module = ADDRESS_SQL.JURIS_CON then
         O_jurisdiction_desc  := NULL;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS THEN
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
		return FALSE;

END VALIDATE_STATECOUNTRYJURIS;
-----------------------------------------------------------------
FUNCTION PROCESS_VAL_COMPETITOR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                L_error           IN OUT   BOOLEAN,
                                I_rec             IN       C_SVC_COMPETITOR%ROWTYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64) 	:= 'CORESVC_COMPETITOR.PROCESS_VAL_COMPETITOR';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPETITOR';
   L_exist 	             boolean;
   L_state_desc          STATE.DESCRIPTION%TYPE;
   L_country_desc        COUNTRY.COUNTRY_DESC%TYPE;
   L_jurisdiction_desc   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   L_calling_module      VARCHAR2(20);
   L_calling_context     VARCHAR2(12);
   L_state               COMPETITOR.STATE%TYPE := I_rec.STATE;
   L_country_id          COMPETITOR.COUNTRY_ID%TYPE := I_rec.country_id;
   L_jurisdiction_code   COMPETITOR.JURISDICTION_CODE%TYPE := I_rec.jurisdiction_code;

BEGIN
	-- Delete from child table (Referential integrity)
   if I_rec.action=action_del then
      if COMPETITOR_SQL.CHECK_COMP_DELETE(O_error_message,
                                          L_exist,
                                          I_rec.competitor) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMPETITOR',
                     O_error_message);
         L_error :=TRUE;
      end if;

      if L_exist then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMPETITOR',
                     'CANT_DEL_COMP');
         L_error :=TRUE;
      end if;
	end if;

	-- jurisdiction_code
   if I_rec.action in (action_new,action_mod)
      and I_rec.jurisdiction_code is NOT NULL then
         if I_rec.country_id is NULL then
            WRITE_ERROR(I_rec.process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'COUNTRY_ID',
                        'ENT_COUNTRY');
            L_error :=TRUE;
         end if;

         if I_rec.state is NULL then
            WRITE_ERROR(I_rec.process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'STATE',
                        'ENTER_STATE');
            L_error :=TRUE;
         end if;
   end if;

   O_error_message := NULL;

   if(I_rec.COUNTRY_ID is NOT NULL ) then
      L_calling_module := ADDRESS_SQL.COUNTRY_CON ;
      L_calling_context := ADDRESS_SQL.COUNTRY_CON;

      if(VALIDATE_STATECOUNTRYJURIS(O_error_message,
                                    L_state,
                                    L_state_desc,
                                    L_country_id,
                                    L_country_desc,
                                    L_jurisdiction_code,
                                    L_jurisdiction_desc,
                                    L_error,
                                    I_rec,
                                    L_calling_module,
                                    L_calling_context) = FALSE) then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;
   elsif(I_rec.STATE is NOT NULL) then
      L_calling_module := ADDRESS_SQL.STATE_CON;
      L_calling_context := ADDRESS_SQL.STATE_CON;

      if(VALIDATE_STATECOUNTRYJURIS(O_error_message,
                                    L_state,
                                    L_state_desc,
                                    L_country_id,
                                    L_country_desc,
                                    L_jurisdiction_code,
                                    L_jurisdiction_desc,
                                    L_error,
                                    I_rec,
                                    L_calling_module,
                                    L_calling_context)=FALSE) then

         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;

   elsif(I_rec.JURISDICTION_CODE is NOT NULL) then
      L_calling_module := ADDRESS_SQL.JURIS_CON;
      L_calling_module := ADDRESS_SQL.JURIS_CON;

      if(VALIDATE_STATECOUNTRYJURIS(O_error_message,
                                    L_state,
                                    L_state_desc,
                                    L_country_id,
                                    L_country_desc,
                                    L_jurisdiction_code,
                                    L_jurisdiction_desc,
                                    L_error,
                                    I_rec,
                                    L_calling_module,
                                    L_calling_context)=FALSE) then

         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;
   end if;
   return TRUE;

EXCEPTION
    when OTHERS THEN
        O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
        return FALSE;
END PROCESS_VAL_COMPETITOR;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_COMPETITOR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_COMPETITOR.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_COMPETITOR.CHUNK_ID%TYPE)

RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'CORESVC_COMPETITOR.PROCESS_COMPETITOR';
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPETITOR';
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN  :=FALSE;
   L_competitor_temp_rec   COMPETITOR%ROWTYPE;
   L_input_competitor      COMPETITOR.COMPETITOR%TYPE;

BEGIN
   FOR rec IN C_SVC_COMPETITOR(I_process_id,
                               I_chunk_id)
   LOOP
      L_error := FALSE;
      L_input_competitor := rec.competitor;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_COMPETITOR_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMPETITOR',
                     'COMPETITOR_EXISTS');
         L_error :=TRUE;
      end if;

      if rec.action = action_mod
         and rec.PK_COMPETITOR_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMPETITOR',
                     'INV_COMPETITOR');
         L_error :=TRUE;
      end if;

      if rec.action = action_mod
         and NOT(rec.COMP_NAME  is NOT NULL) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMP_NAME',
                     'ENT_COMP_NAME');
         L_error :=TRUE;
      end if;

      -- Calling PROCESS_VAL_COMPETITOR
      if PROCESS_VAL_COMPETITOR(O_error_message,
                                 L_error,
                                 rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      -- Auto generation of Primary key
      if NOT L_error AND rec.action = action_new and rec.competitor is NULL then
         if COMPETITOR_SQL.NEXT_COMP_NUMBER(O_error_message,
                                            rec.competitor) = FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'COMPETITOR',
                        O_error_message);
            L_error :=TRUE;
         end if;

         update svc_comp_store
            set competitor = rec.competitor
          where competitor = L_input_competitor
            and action = action_new
            and process_id = I_process_id
            and chunk_id = I_chunk_id;
      end if;

      if NOT L_error then
         L_competitor_temp_rec.COMPETITOR          := rec.COMPETITOR;
         L_competitor_temp_rec.COMP_NAME           := rec.COMP_NAME;
         L_competitor_temp_rec.ADDRESS_1           := rec.ADDRESS_1;
         L_competitor_temp_rec.ADDRESS_2           := rec.ADDRESS_2;
         L_competitor_temp_rec.ADDRESS_3           := rec.ADDRESS_3;
         L_competitor_temp_rec.CITY                := rec.CITY;
         L_competitor_temp_rec.STATE               := rec.STATE;
         L_competitor_temp_rec.COUNTRY_ID          := rec.COUNTRY_ID;
         L_competitor_temp_rec.POST_CODE           := rec.POST_CODE;
         L_competitor_temp_rec.PHONE               := rec.PHONE;
         L_competitor_temp_rec.FAX                 := rec.FAX;
         L_competitor_temp_rec.WEBSITE             := rec.WEBSITE;
         L_competitor_temp_rec.JURISDICTION_CODE   := rec.JURISDICTION_CODE;

         if rec.action = action_new then
            if EXEC_COMPETITOR_INS(O_error_message,
                                   L_competitor_temp_rec) = FALSE then

               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_COMPETITOR_UPD(O_error_message,
                                   L_competitor_temp_rec) = FALSE then

               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);

               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;

END PROCESS_COMPETITOR;
---------------------------------------------------------------------------------------
FUNCTION PROCESS_COMPETITOR_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_COMPETITOR.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_COMPETITOR.CHUNK_ID%TYPE)

RETURN BOOLEAN IS

   L_program               VARCHAR2(64) := 'CORESVC_COMPETITOR.PROCESS_COMPETITOR_DEL';
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMPETITOR';
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN  :=FALSE;
   L_competitor_temp_rec   COMPETITOR%ROWTYPE;
   L_input_competitor      COMPETITOR.COMPETITOR%TYPE;

BEGIN
   FOR rec IN C_SVC_COMPETITOR_DEL(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error := FALSE;
      L_input_competitor := rec.competitor;

      if rec.PK_COMPETITOR_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMPETITOR',
                     'INV_COMPETITOR');
         L_error :=TRUE;
      end if;

      -- Calling PROCESS_VAL_COMPETITOR
      if PROCESS_VAL_COMPETITOR(O_error_message,
                                 L_error,
                                 rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then
         L_competitor_temp_rec.COMPETITOR          := rec.COMPETITOR;
         L_competitor_temp_rec.COMP_NAME           := rec.COMP_NAME;
         L_competitor_temp_rec.ADDRESS_1           := rec.ADDRESS_1;
         L_competitor_temp_rec.ADDRESS_2           := rec.ADDRESS_2;
         L_competitor_temp_rec.ADDRESS_3           := rec.ADDRESS_3;
         L_competitor_temp_rec.CITY                := rec.CITY;
         L_competitor_temp_rec.STATE               := rec.STATE;
         L_competitor_temp_rec.COUNTRY_ID          := rec.COUNTRY_ID;
         L_competitor_temp_rec.POST_CODE           := rec.POST_CODE;
         L_competitor_temp_rec.PHONE               := rec.PHONE;
         L_competitor_temp_rec.FAX                 := rec.FAX;
         L_competitor_temp_rec.WEBSITE             := rec.WEBSITE;
         L_competitor_temp_rec.JURISDICTION_CODE   := rec.JURISDICTION_CODE;

         if EXEC_COMPETITOR_DEL(O_error_message,
                                L_competitor_temp_rec) = FALSE then

            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_process_error := TRUE;
         end if;
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               to_char(SQLCODE));
         return FALSE;

END PROCESS_COMPETITOR_DEL;
------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_STORE_INS(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_comp_store_temp_rec   IN      COMP_STORE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMP_STORE_INS';

BEGIN
   insert into comp_store
		  values I_comp_store_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_COMP_STORE_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_STORE_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_comp_store_temp_rec   IN       COMP_STORE%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMP_STORE_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

--Cursor to lock the record
   cursor C_LOCK_STORE_UPD is
      select 'x'
        from COMP_STORE
       where STORE = I_comp_store_temp_rec.STORE
         for update nowait;

BEGIN
   open C_LOCK_STORE_UPD;
   close C_LOCK_STORE_UPD;

   update COMP_STORE
	  set row = I_comp_store_temp_rec
    where 1 = 1
      and STORE = I_comp_store_temp_rec.STORE;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_comp_store_temp_rec.STORE,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_STORE_UPD%ISOPEN then
         close C_LOCK_STORE_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_COMP_STORE_UPD;
----------------------------------------------------------------------
FUNCTION EXEC_COMP_STORE_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_comp_store_temp_rec   IN       COMP_STORE%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMP_STORE_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

--Cursor to lock the record
   cursor C_LOCK_STORE_DEL is
      select 'x'
        from COMP_STORE
       where STORE = I_comp_store_temp_rec.STORE
         for update nowait;

BEGIN
   open C_LOCK_STORE_DEL;
   close C_LOCK_STORE_DEL;

	delete
	  from comp_store
	 where 1 = 1
	   and STORE = I_comp_store_temp_rec.STORE;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_comp_store_temp_rec.STORE,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_STORE_DEL%ISOPEN then
         close C_LOCK_STORE_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_COMP_STORE_DEL;
----------------------------------------------------------------------
FUNCTION VALIDATE_STATECOUNTRYJURIS(O_state               IN OUT   STATE.STATE%TYPE,
                                    O_state_desc          IN OUT   STATE.DESCRIPTION%TYPE,
                                    O_country_id          IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                                    O_country_desc        IN OUT   COUNTRY.COUNTRY_DESC%TYPE,
                                    O_jurisdiction_code   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_CODE%TYPE,
                                    O_jurisdiction_desc   IN OUT   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE,
                                    O_error_message  	    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error               IN OUT   BOOLEAN,
                                    I_rec                 IN OUT   C_SVC_COMP_STORE%ROWTYPE,
                                    I_calling_module      IN       VARCHAR2,
                                    I_calling_context     IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64)	:= 'CORESVC_COMPETITOR.VALIDATE_STATECOUNTRYJURIS';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE';
   L_error_message     RTK_ERRORS.RTK_TEXT%TYPE;
   L_calling_context   VARCHAR2(12);
   L_dup_exists        VARCHAR2(1) := 'N';

BEGIN

   L_calling_context := I_calling_context;
   if (L_calling_context = ADDRESS_SQL.COUNTRY_CON and O_country_id is NOT NULL)
      or (L_calling_context = ADDRESS_SQL.STATE_CON and O_state is NOT NULL)
      or (L_calling_context = ADDRESS_SQL.JURIS_CON and O_jurisdiction_code is NOT NULL) then
      if O_jurisdiction_code is NOT NULL then
         L_calling_context := ADDRESS_SQL.JURIS_CON;
      elsif O_state is NOT NULL then
         L_calling_context := ADDRESS_SQL.STATE_CON;
      end if;

      if ADDRESS_SQL.CHECK_ADDR (O_error_message,
                                 O_country_id,
                                 O_country_desc,
                                 O_state,
                                 O_state_desc,
                                 O_jurisdiction_code,
                                 O_jurisdiction_desc,
                                 L_dup_exists,
                                 I_calling_module,
                                 L_calling_context) = FALSE then
         O_error := TRUE;
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;

      if L_dup_exists = 'Y' then
         O_error := TRUE;
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;
   else
      if I_calling_module = ADDRESS_SQL.COUNTRY_CON then
         O_country_desc      := NULL;
         O_jurisdiction_code := NULL;
         O_jurisdiction_desc := NULL;
         O_state             := NULL;
         O_state_desc        := NULL;
      elsif I_calling_module = ADDRESS_SQL.STATE_CON then
         O_state_desc        := NULL;
         O_jurisdiction_code := NULL;
         O_jurisdiction_desc := NULL;
      elsif I_calling_module = ADDRESS_SQL.JURIS_CON then
         O_jurisdiction_desc := NULL;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
		O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
		return FALSE;

END VALIDATE_STATECOUNTRYJURIS;
-------------------------------------------------------------------------
FUNCTION PROCESS_VAL_COMP_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error           IN OUT   BOOLEAN,
                                I_rec             IN OUT   C_SVC_COMP_STORE%ROWTYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(64)	:= 'CORESVC_COMPETITOR.PROCESS_VAL_COMP_STORE';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE';
   L_exist               boolean;
   L_state_desc          STATE.DESCRIPTION%TYPE;
   L_country_desc        COUNTRY.COUNTRY_DESC%TYPE;
   L_jurisdiction_desc   COUNTRY_TAX_JURISDICTION.JURISDICTION_DESC%TYPE;
   L_calling_module      VARCHAR2(20);
   L_calling_context     VARCHAR2(12);

BEGIN
	-- Currency_code
   if I_rec.action in (action_new,action_mod)
      and I_rec.currency_code is NULL then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'CURRENCY_CODE',
                  'ENTER_CURRENCY_CODE');
      O_error :=TRUE;
   end if;

   if I_rec.action in (action_new,action_mod)
      and I_rec.currency_code is NOT NULL then
      if CURRENCY_SQL.GET_NAME(O_error_message,
                               I_rec.currency_code,
                               I_rec.currency_desc) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CURRENCY_CODE',
                     O_error_message);
         O_error :=TRUE;
      end if;
   end if;

	-- jurisdiction_code
   if I_rec.action in (action_new,action_mod)
      and I_rec.jurisdiction_code is NOT NULL then
      if I_rec.country_id is NULL then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COUNTRY_ID',
                     'ENT_COUNTRY');
         O_error :=TRUE;
      end if;

      if I_rec.state is NULL then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'STATE',
                     'ENTER_STATE');
         O_error :=TRUE;
      end if;
   end if;

    -- store_format
   if I_rec.action in (action_new,action_mod)
      and I_rec.store_format is NOT NULL then
      if STORE_ATTRIB_SQL.FORMAT_NAME(O_error_message,
                                      I_rec.store_format,
                                      I_rec.format_name) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'STORE_FORMAT',
                     O_error_message);
         O_error:=TRUE;
      end if;
   end if;

	-- close_date > open_date
   if I_rec.action in (action_new,action_mod)
      and I_rec.open_date is NOT NULL then
      if I_rec.close_date <= I_rec.open_date then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CLOSE_DATE',
                     'STR_OPEN_B4_CLOSE');
         O_error:=TRUE;
      end if;
   end if;

	--cannot update competitor
   if I_rec.action = action_mod
      and I_rec.competitor <> I_rec.old_competitor then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'COMPETITOR',
                  'CANNOT_UPD_COMPETITOR');
      O_error :=TRUE;
   end if;

	-- Referential Integrity Constraint (Referred To)
   if I_rec.action=action_del then
      if COMPETITOR_SQL.CHECK_COMP_ST_DELETE(O_error_message,
                                             L_exist,
                                             I_rec.STORE) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'STORE',
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_exist then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'STORE',
                     O_error_message);
         O_error :=TRUE;
      end if;
   end if;

   O_error_message := NULL;

   if(I_rec.country_id is NOT NULL ) then
      L_calling_module := ADDRESS_SQL.COUNTRY_CON ;
      L_calling_context := ADDRESS_SQL.COUNTRY_CON;

      if VALIDATE_STATECOUNTRYJURIS(I_rec.state,
                                    L_state_desc,
                                    I_rec.country_id,
                                    L_country_desc,
                                    I_rec.jurisdiction_code,
                                    L_jurisdiction_desc,
                                    O_error_message,
                                    O_error,
                                    I_rec,
                                    L_calling_module,
                                    L_calling_context) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;
   elsif(I_rec.state is NOT NULL) then
      L_calling_module := ADDRESS_SQL.STATE_CON;
      L_calling_context := ADDRESS_SQL.STATE_CON;

      if VALIDATE_STATECOUNTRYJURIS(I_rec.state,
                                    L_state_desc,
                                    I_rec.country_id,
                                    L_country_desc,
                                    I_rec.jurisdiction_code,
                                    L_jurisdiction_desc,
                                    O_error_message,
                                    O_error,
                                    I_rec,
                                    L_calling_module,
                                    L_calling_context)=FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;
   elsif(I_rec.jurisdiction_code is NOT NULL) then
      L_calling_module := ADDRESS_SQL.JURIS_CON;
      L_calling_context := ADDRESS_SQL.JURIS_CON;

      if VALIDATE_STATECOUNTRYJURIS(I_rec.state,
                                    L_state_desc,
                                    I_rec.country_id,
                                    L_country_desc,
                                    I_rec.jurisdiction_code,
                                    L_jurisdiction_desc,
                                    O_error_message,
                                    O_error,
                                    I_rec,
                                    L_calling_module,
                                    L_calling_context )=FALSE then

         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'State,Country,Jurisdiction',
                     O_error_message);
      end if;
   end if;
	return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END PROCESS_VAL_COMP_STORE;
-------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMP_STORE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_COMP_STORE.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_COMP_STORE.CHUNK_ID%TYPE )

RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'CORESVC_COMPETITOR.PROCESS_COMP_STORE';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE';
   L_error               BOOLEAN;
   L_process_error       BOOLEAN := FALSE;
   COMP_STORE_temp_rec   COMP_STORE%ROWTYPE;
   L_exist               BOOLEAN;
   L_input_comp_store    COMP_STORE_LINK.COMP_STORE%TYPE;

BEGIN
   FOR rec IN C_SVC_COMP_STORE(I_process_id,
                               I_chunk_id)
   LOOP
      L_error := FALSE;
      L_input_comp_store := rec.store;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.store is NOT NULL
         and rec.pk_comp_store_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STORE',
                     'DUP_COMP_STORE');
         L_error :=TRUE;
      end if;

      if rec.action = action_mod
         and rec.pk_comp_store_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STORE',
                     'INV_COMP_STORE');
         L_error :=TRUE;
      end if;

      if rec.action = action_mod
         and rec.CMS_CMP_FK_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMPETITOR',
                     'INV_COMPETITOR');
         L_error :=TRUE;
      end if;

      if rec.action = action_mod
         and rec.store is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STORE',
                     'ENTER_COMP_STORE');
         L_error :=TRUE;
      end if;

      if rec.competitor is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMPETITOR',
                     'ENTER_COMP');
         L_error :=TRUE;
      end if;

      if rec.store_name is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STORE_NAME',
                     'ENTER_COMP_STORE_DESC');
         L_error :=TRUE;
      end if;

    -- Calling PROCESS_VAL_COMP_STORE
      if (PROCESS_VAL_COMP_STORE(O_error_message,
                                 L_error,
                                 rec) = FALSE) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

    -- Auto generation of Primary key
      if NOT L_error and rec.action = action_new
         and rec.store is NULL then
         if COMPETITOR_SQL.NEXT_COMP_ST_NUMBER(O_error_message,
                                               rec.store) = FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'STORE',
                        O_error_message);
            L_error :=TRUE;
         end if;

         update svc_comp_store_link
            set comp_store = rec.store
          where comp_store = L_input_comp_store
            and action = action_new
            and process_id = I_process_id
            and chunk_id = I_chunk_id;
      end if;

      if NOT L_error then
         COMP_STORE_temp_rec.STORE               := rec.STORE;
         COMP_STORE_temp_rec.competitor          := rec.competitor;
         COMP_STORE_temp_rec.STORE_name          := rec.STORE_name;
         COMP_STORE_temp_rec.address_1           := rec.address_1;
         COMP_STORE_temp_rec.address_2           := rec.address_2;
         COMP_STORE_temp_rec.address_3           := rec.address_3;
         COMP_STORE_temp_rec.city              	 := rec.city;
         COMP_STORE_temp_rec.state               := rec.state;
         COMP_STORE_temp_rec.country_id          := rec.country_id;
         COMP_STORE_temp_rec.post_code           := rec.post_code;
         COMP_STORE_temp_rec.phone               := rec.phone;
         COMP_STORE_temp_rec.fax              	 := rec.fax;
         COMP_STORE_temp_rec.STORE_format        := rec.STORE_format;
         COMP_STORE_temp_rec.currency_code       := rec.currency_code;
         COMP_STORE_temp_rec.total_square_feet   := rec.total_square_feet;
         COMP_STORE_temp_rec.selling_square_feet := rec.selling_square_feet;
         COMP_STORE_temp_rec.open_date           := rec.open_date;
         COMP_STORE_temp_rec.close_date          := rec.close_date;
         COMP_STORE_temp_rec.estimated_volume    := rec.estimated_volume;
         COMP_STORE_temp_rec.jurisdiction_code   := rec.jurisdiction_code;

         if rec.action = action_new then
            if EXEC_COMP_STORE_INS(O_error_message,
                                   COMP_STORE_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
            L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_COMP_STORE_UPD(O_error_message,
                                   COMP_STORE_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
    END LOOP;
  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_COMP_STORE;
-------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMP_STORE_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_COMP_STORE.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_COMP_STORE.CHUNK_ID%TYPE )

RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'CORESVC_COMPETITOR.PROCESS_COMP_STORE_DEL';
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE';
   L_error               BOOLEAN;
   L_process_error       BOOLEAN := FALSE;
   COMP_STORE_temp_rec   COMP_STORE%ROWTYPE;
   L_exist               BOOLEAN;
   L_input_comp_store    COMP_STORE_LINK.COMP_STORE%TYPE;

BEGIN
   FOR rec IN C_SVC_COMP_STORE_DEL(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error := FALSE;
      L_input_comp_store := rec.store;

      if rec.pk_comp_store_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STORE',
                     'INV_COMP_STORE');
         L_error :=TRUE;
      end if;

      if rec.CMS_CMP_FK_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMPETITOR',
                     'INV_COMPETITOR');
         L_error :=TRUE;
      end if;

      if rec.store is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STORE',
                     'ENTER_COMP_STORE');
         L_error :=TRUE;
      end if;

      if rec.competitor is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMPETITOR',
                     'ENTER_COMP');
         L_error :=TRUE;
      end if;

    -- Calling PROCESS_VAL_COMP_STORE
      if (PROCESS_VAL_COMP_STORE(O_error_message,
                                 L_error,
                                 rec) = FALSE) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then
         COMP_STORE_temp_rec.STORE               := rec.STORE;
         COMP_STORE_temp_rec.competitor          := rec.competitor;
         COMP_STORE_temp_rec.STORE_name          := rec.STORE_name;
         COMP_STORE_temp_rec.address_1           := rec.address_1;
         COMP_STORE_temp_rec.address_2           := rec.address_2;
         COMP_STORE_temp_rec.address_3           := rec.address_3;
         COMP_STORE_temp_rec.city                := rec.city;
         COMP_STORE_temp_rec.state               := rec.state;
         COMP_STORE_temp_rec.country_id          := rec.country_id;
         COMP_STORE_temp_rec.post_code           := rec.post_code;
         COMP_STORE_temp_rec.phone               := rec.phone;
         COMP_STORE_temp_rec.fax                 := rec.fax;
         COMP_STORE_temp_rec.STORE_format        := rec.STORE_format;
         COMP_STORE_temp_rec.currency_code       := rec.currency_code;
         COMP_STORE_temp_rec.total_square_feet   := rec.total_square_feet;
         COMP_STORE_temp_rec.selling_square_feet := rec.selling_square_feet;
         COMP_STORE_temp_rec.open_date           := rec.open_date;
         COMP_STORE_temp_rec.close_date          := rec.close_date;
         COMP_STORE_temp_rec.estimated_volume    := rec.estimated_volume;
         COMP_STORE_temp_rec.jurisdiction_code   := rec.jurisdiction_code;

         if EXEC_COMP_STORE_DEL(O_error_message,
                                COMP_STORE_temp_rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_process_error := TRUE;
         end if;
      end if;
    END LOOP;
  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_COMP_STORE_DEL;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_STORE_LINK_INS(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_comp_store_link_temp_rec   IN       COMP_STORE_LINK%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMP_STORE_LINK_INS';

BEGIN
   insert into COMP_STORE_LINK
        values I_comp_store_link_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_COMP_STORE_LINK_INS;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_STORE_LINK_UPD(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_comp_store_link_temp_rec    IN       COMP_STORE_LINK%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMP_STORE_LINK_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE_LINK';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STR_LINK_UPD is
      select 'x'
        from comp_store_link
       where comp_store = I_comp_store_link_temp_rec.comp_store
	       and store = I_comp_store_link_temp_rec.store
         for update nowait;

BEGIN
   open C_LOCK_STR_LINK_UPD;
   close C_LOCK_STR_LINK_UPD;

   update COMP_STORE_LINK
	    set row = I_comp_store_link_temp_rec
	  where 1 = 1
	    and comp_store = I_comp_store_link_temp_rec.comp_store
	    and store = I_comp_store_link_temp_rec.store;
	 return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_comp_store_link_temp_rec.COMP_STORE,
                                             I_comp_store_link_temp_rec.STORE);
      return FALSE;
   when OTHERS then
      if C_LOCK_STR_LINK_UPD%ISOPEN then
         close C_LOCK_STR_LINK_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END EXEC_COMP_STORE_LINK_UPD;
---------------------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_STORE_LINK_DEL(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_comp_store_link_temp_rec   IN       COMP_STORE_LINK%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_COMPETITOR.EXEC_COMP_STORE_LINK_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE_LINK';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_STR_LINK_DEL is
      select 'x'
        from comp_store_link
       where COMP_STORE = I_comp_store_link_temp_rec.comp_store
	       and STORE = I_comp_store_link_temp_rec.store
         for update nowait;

BEGIN
   open C_LOCK_STR_LINK_DEL;
   close C_LOCK_STR_LINK_DEL;

	delete from COMP_STORE_LINK
	 where 1 = 1
	   and comp_store = I_comp_store_link_temp_rec.comp_store
	   and store = I_comp_store_link_temp_rec.store;
	return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_comp_store_link_temp_rec.COMP_STORE,
                                             I_comp_store_link_temp_rec.STORE);
      return FALSE;
   when OTHERS then
      if C_LOCK_STR_LINK_DEL%ISOPEN then
         close C_LOCK_STR_LINK_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END EXEC_COMP_STORE_LINK_DEL;
--------------------------------------------------------------------------------------------
FUNCTION UPD_TARGET_IND (O_error_message 	IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_rec 				IN OUT   c_svc_COMP_STORE_LINK%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_COMPETITOR.UPD_TARGET_IND';

BEGIN
   update COMP_STORE_LINK csl
	    set target_comp_ind = 'N'
	  where csl.STORE = I_rec.STORE
	    and csl.target_comp_ind = 'Y';
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
	   return FALSE;
END UPD_TARGET_IND;
--------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_COMP_STORE_LINK(O_error_message 	 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_error           IN OUT   BOOLEAN,
                                     I_rec             IN OUT   C_SVC_COMP_STORE_LINK%ROWTYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)	                     := 'CORESVC_COMPETITOR.PROCESS_VAL_COMP_STORE_LINK';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE_LINK';
   L_exist           BOOLEAN;
   L_rank            NUMBER(2);
   L_count           NUMBER(4,0);
   L_code_desc       CODE_DETAIL.CODE_DESC%TYPE;
   L_rec_exists      VARCHAR2(1)                       := NULL;

   cursor C_GET_DUP_RANK is
      select 'x'
        from COMP_STORE_LINK
       where COMP_STORE_LINK.RANK  = I_rec.RANK
	      and COMP_STORE_LINK.STORE = I_rec.STORE;

   rank_tab C_GET_DUP_RANK%ROWTYPE;

BEGIN
   L_rec_exists := NULL;
   if I_rec.action = action_new
      or (I_rec.action = action_mod and I_rec.rank <> I_rec.old_rank)
      and I_rec.rank is NOT NULL then
      open C_GET_DUP_RANK;
      fetch C_GET_DUP_RANK  into L_rec_exists;
      close C_GET_DUP_RANK;

      if L_rec_exists is NOT NULL then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'RANK',
                     'RANK_EXISTS');
         O_error :=TRUE;
      end if;
   end if;

   if I_rec.action IN (action_new,action_mod)
      and I_rec.tar_comp_ind = 'Y'
      and NVL(I_rec.tar_comp_ind,'-1') <> NVL(I_rec.OLD_TARGET_COMP_IND,-1) then
      if UPD_TARGET_IND(O_error_message,
                        I_rec) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TARGET_COMP_IND',
                     O_error_message);
         O_error :=TRUE;
      end if;
   end if;

   if I_rec.action in (action_new,action_mod) then
      if I_rec.UOM is NOT NULL then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'MIKI',
                                       I_rec.UOM ,
                                       L_code_desc ) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'UOM',
                        O_error_message);
            O_error :=TRUE;
         end if;
      end if;

      if I_rec.rank <= 0 then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'RANK',
                     'NEG_RANK');
         O_error :=TRUE;
      end if;

      if I_rec.distance is NOT NULL then
         if I_rec.distance <= 0 then
            WRITE_ERROR(I_rec.process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'DISTANCE',
                        'NEG_DISTANCE');
            O_error :=TRUE;
         end if;

         if (I_rec.distance > 0 and I_rec.UOM is NULL) then
            WRITE_ERROR(I_rec.process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'DISTANCE',
                        'NEED_UOM');
            O_error :=TRUE;
         end if;
      end if;

      if I_rec.DISTANCE is NULL
         and I_rec.UOM is NOT NULL then
         WRITE_ERROR(I_rec.process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DISTANCE',
                     'NEED_DISTANCE');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_DUP_RANK%ISOPEN then
         close C_GET_DUP_RANK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
	   return FALSE;
END PROCESS_VAL_COMP_STORE_LINK;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMP_STORE_LINK(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN       SVC_COMP_STORE_LINK.PROCESS_ID%TYPE,
                                 I_chunk_id        IN       SVC_COMP_STORE_LINK.CHUNK_ID%TYPE )
RETURN BOOLEAN IS

   L_program                    VARCHAR2(64)	:= 'CORESVC_COMPETITOR.PROCESS_COMP_STORE_LINK';
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_STORE_LINK';
   L_error                      BOOLEAN := FALSE;
   L_process_error              BOOLEAN := FALSE;
   L_comp_store_link_temp_rec   COMP_STORE_LINK%ROWTYPE;
   L_count                      NUMBER;
   L_exists                     VARCHAR2(1) := NULL;
   L_rowseq_count               NUMBER;
   c                            NUMBER      := 1;

   TYPE L_row_seq_tab_type IS TABLE OF SVC_COMP_STORE_LINK.ROW_SEQ%TYPE;
   L_row_seq_tab                       L_row_seq_tab_type;

   cursor C_GET_ONE_TARID(L_store COMP_STORE_LINK.store%TYPE) is
      select count(target_comp_ind)
        from COMP_STORE_LINK
       where COMP_STORE_LINK.target_comp_ind='Y'
         and COMP_STORE_LINK.store = L_store;

   cursor C_CHECK_RECS_EXISTS(L_store COMP_STORE_LINK.store%TYPE) is
      select 'x'
        from COMP_STORE_LINK
       where COMP_STORE_LINK.store = L_store;

 BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   FOR rec in c_svc_COMP_STORE_LINK(I_process_id,
                                    I_chunk_id)
   LOOP
      L_error    := FALSE;
      L_exists   := NULL;
      L_process_error := FALSE;
      if rec.ch_rank = 1 then
         SAVEPOINT  successful_item;
         L_row_seq_tab.DELETE;
         c:=1;
      end if;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.pk_comp_store_link_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Store,Competitor Store',
                     'DUP_COMP_LINK');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.pk_comp_store_link_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Store,Competitor Store',
                     'INVALID_COMP_LINK');
         L_error :=TRUE;
      end if;

      if rec.cmk_cms_fk_rid is NULL
         and rec.comp_store is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COMP_STORE',
                     'INV_COMP_STORE');
         L_error :=TRUE;
      end if;

      if rec.cmk_str_fk_rid is NULL
         and rec.store is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'STORE',
                     'INV_STORE');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new,action_mod) then
         if NOT( rec.tar_comp_ind IN  ( 'Y', 'N' )  ) then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TARGET_COMP_IND',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;

         if rec.tar_comp_ind is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TARGET_COMP_IND',
                        'TARGET_IND');
            L_error :=TRUE;
         end if;
      end if;

	-- Calling PROCESS_VAL_COMP_STORE_LINK (New Validations)
      if PROCESS_VAL_COMP_STORE_LINK(O_error_message,
                                     L_error,
                                     rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
 -- If there is no validation error then L_error is FALSE
      if NOT L_error then
         L_comp_store_link_temp_rec.store           := rec.store;
         L_comp_store_link_temp_rec.comp_store      := rec.comp_store;
         L_comp_store_link_temp_rec.target_comp_ind := rec.tar_comp_ind;
         L_comp_store_link_temp_rec.rank            := rec.rank;
         L_comp_store_link_temp_rec.distance        := rec.distance;
         L_comp_store_link_temp_rec.uom             := rec.uom;
         if rec.action = action_new then
            if EXEC_COMP_STORE_LINK_INS(O_error_message,
                                        L_comp_store_link_temp_rec) = FALSE then

               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;

            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_COMP_STORE_LINK_UPD(O_error_message,
                                        L_comp_store_link_temp_rec) = FALSE then

               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);

               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_COMP_STORE_LINK_DEL(O_error_message,
                                        L_comp_store_link_temp_rec) = FALSE then

               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);

               L_process_error := TRUE;
            end if;
         end if;
      if NOT L_process_error then
         L_row_seq_tab.extend();
         L_row_seq_tab(c)  := rec.row_seq;
          c:=c+1;
      end if;

      end if;

       if rec.store <> rec.next_store  then
         L_count := 0;
         L_exists := NULL;

         open C_CHECK_RECS_EXISTS(rec.store);
         fetch C_CHECK_RECS_EXISTS into L_exists;
         close C_CHECK_RECS_EXISTS;

         open C_GET_ONE_TARID(rec.store);
         fetch C_GET_ONE_TARID into L_count;
         close C_GET_ONE_TARID;
         if L_count != 1 and L_exists is NOT NULL then
            L_process_error:=TRUE;
            L_rowseq_count := L_row_seq_tab.count();
            if L_rowseq_count > 0 then
              for i in 1..L_rowseq_count
               LOOP
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_row_seq_tab(i),
                              'Target Competitor,Store',
                              'ONE_TARGET_ID');
               END LOOP;
            end if;
            ROLLBACK TO SAVEPOINT successful_item;
        end if;
    end if;

  END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_GET_ONE_TARID%ISOPEN then
         close C_GET_ONE_TARID;
      end if;
      if C_CHECK_RECS_EXISTS%ISOPEN then
         close C_CHECK_RECS_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_COMP_STORE_LINK;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_SHOPPER_INS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_comp_shopper_temp_rec   IN       COMP_SHOPPER%ROWTYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_COMPETITOR.EXEC_COMP_SHOPPER_INS';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_SHOPPER';
BEGIN

   insert into COMP_SHOPPER
        values I_comp_shopper_temp_rec;

   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('SHOPPER_EXISTS',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPETITOR.EXEC_COMP_SHOPPER_INS',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMP_SHOPPER_INS;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_SHOPPER_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_comp_shopper_temp_rec   IN       COMP_SHOPPER%ROWTYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_COMPETITOR.EXEC_COMP_SHOPPER_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMP_SHOPPER';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);
   cursor C_COMP_SHOPPER_LOCK is
      select 'X'
        from COMP_SHOPPER
       where SHOPPER = I_comp_shopper_temp_rec.SHOPPER
       for update nowait;
BEGIN
   open C_COMP_SHOPPER_LOCK;
   close C_COMP_SHOPPER_LOCK;

   update comp_shopper
      set row     = I_comp_shopper_temp_rec
    where SHOPPER = I_comp_shopper_temp_rec.SHOPPER;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_comp_shopper_temp_rec.SHOPPER,
                                                               NULL);
      return FALSE;

   when OTHERS then
      if C_COMP_SHOPPER_LOCK%ISOPEN then
         close C_COMP_SHOPPER_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPETITOR.EXEC_COMP_SHOPPER_UPD',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMP_SHOPPER_UPD;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_COMP_SHOPPER_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_comp_shopper_temp_rec   IN       COMP_SHOPPER%ROWTYPE )
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_COMPETITOR.EXEC_COMP_SHOPPER_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'COMP_SHOPPER';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_COMP_SHOPPER_LOCK is
      select 'X'
        from COMP_SHOPPER
       where SHOPPER = I_comp_shopper_temp_rec.SHOPPER
       for update nowait;
BEGIN
   open C_COMP_SHOPPER_LOCK;
   close C_COMP_SHOPPER_LOCK;

   delete from COMP_SHOPPER
       where shopper = I_comp_shopper_temp_rec.shopper;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_comp_shopper_temp_rec.SHOPPER,
                                                               NULL);
      return FALSE;

   when OTHERS then
      if C_COMP_SHOPPER_LOCK%ISOPEN then
         close C_COMP_SHOPPER_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPETITOR.EXEC_COMP_SHOPPER_DEL',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_COMP_SHOPPER_DEL;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_COMP_SHOPPER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error           IN OUT   BOOLEAN,
                                  I_rec             IN       C_SVC_COMP_SHOPPER%ROWTYPE )
RETURN BOOLEAN IS
   L_exists   BOOLEAN;
   L_table    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_COMP_SHOPPER';

BEGIN
   --Referential integrity validation
   if I_rec.action = action_del then
      if COMPETITOR_SQL.CHECK_COMP_SHOPPER_DELETE(O_error_message,
                                                  L_exists,
                                                  I_rec.SHOPPER) = FALSE then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'SHOPPER',
                      O_error_message);
         O_error:= TRUE;
      end if;
      if L_exists then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'SHOPPER',
                      O_error_message);
         O_error := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                           'CORESVC_COMPETITOR.PROCESS_VAL_COMP_SHOPPER',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_COMP_SHOPPER;
----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_COMP_SHOPPER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_COMP_SHOPPER.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_COMP_SHOPPER.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN;
   L_comp_shopper_temp_rec COMP_SHOPPER%ROWTYPE;
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_COMP_SHOPPER';
BEGIN
   FOR rec IN C_SVC_COMP_SHOPPER(I_process_id,
                                 I_chunk_id)
   LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del)   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.shopper is NOT NULL
         and rec.pk_comp_shopper_rid is NOT NULL  then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SHOPPER',
                     'SHOPPER_EXISTS');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.shopper is NOT NULL
         and rec.pk_comp_shopper_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SHOPPER',
                     'COMP_SHOPPER_MISSING');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new,action_mod)
         and rec.shopper is NOT NULL
         and rec.shopper < 1 then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SHOPPER',
                     'GREATER_0');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_new)
         and rec.shopper_name is NULL   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SHOPPER_NAME',
                     'ENTER_SHOPPER_NAME');
         L_error :=TRUE;
      end if;

    -- OTHER VALIDATIONS THAT WERE PRESENT IN THE FORMS
      if PROCESS_VAL_COMP_SHOPPER(O_error_message,
                                  L_error,
                                  rec) = FALSE   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error
         and rec.action = action_new
         and rec.shopper IS NULL then
         if COMPETITOR_SQL.NEXT_SHOPPER_NUMBER(O_error_message,
                                               rec.shopper) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'SHOPPER',
                        O_error_message);
            L_error := TRUE;
         end if;
      end if;

      if NOT L_error then
         L_comp_shopper_temp_rec.shopper       := rec.shopper;
         L_comp_shopper_temp_rec.shopper_name  := rec.shopper_name;
         L_comp_shopper_temp_rec.shopper_phone := rec.shopper_phone;
         L_comp_shopper_temp_rec.shopper_fax   := rec.shopper_fax;

         if rec.action = action_new then
            if EXEC_COMP_SHOPPER_INS (O_error_message,
                                      L_comp_shopper_temp_rec) = FALSE   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            l_table,
                            rec.row_seq,
                            NULL,
                            O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_COMP_SHOPPER_UPD(O_error_message,
                                     L_comp_shopper_temp_rec) = FALSE   then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           l_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_COMP_SHOPPER_DEL(O_error_message,
                                     L_comp_shopper_temp_rec ) = FALSE   then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'CORESVC_COMPETITOR.PROCESS_COMP_SHOPPER',
                                             TO_CHAR(SQLCODE));

      return FALSE;
END PROCESS_COMP_SHOPPER;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_comp_store_link
      where process_id = I_process_id;

   delete from svc_comp_store
      where process_id = I_process_id;
      
   delete from svc_competitor
      where process_id = I_process_id;

   delete from svc_comp_shopper
      where process_id = I_process_id;
END;
----------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count 	  IN OUT   NUMBER,
                 I_process_id      IN 	     NUMBER,
                 I_chunk_id        IN 	     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_COMPETITOR.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
BEGIN
   Lp_errors_tab := NEW errors_tab_typ();
   if PROCESS_COMPETITOR(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_COMP_STORE(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_COMP_STORE_LINK(O_error_message,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   if PROCESS_COMP_STORE_DEL(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;
   
   if PROCESS_COMPETITOR_DEL(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_COMP_SHOPPER(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
     return FALSE;
   end if;

   O_error_count := Lp_errors_tab.COUNT();
   forall i in 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values Lp_errors_tab(i);
   Lp_errors_tab := NEW errors_tab_typ();

   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status = (CASE
                        when status = 'PE'
                        then 'PE'
                        else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;

EXCEPTION
   when OTHERS THEN
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-----------------------------------------------------------------------------------------
END CORESVC_COMPETITOR;
/
