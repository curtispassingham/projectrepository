



CREATE OR REPLACE PACKAGE BODY UOM_SQL AS

--------------------------------------------------------------------------------
--                          PRIVATE GLOBALS                                   --
--------------------------------------------------------------------------------

   type UOM_CONVERSION_RECORD is record
   (
   factor     UOM_CONVERSION.FACTOR%TYPE,
   operator   UOM_CONVERSION.OPERATOR%TYPE
   );

   type UOM_CONVERSION_TABLE is table of UOM_CONVERSION_RECORD
   index by UOM_CLASS.UOM%TYPE;

   type UOM_RECORD is record
   (
   class              UOM_CLASS.UOM_CLASS%TYPE,
   --
   conversion_table   UOM_CONVERSION_TABLE
   );

   type UOM_TABLE is table of UOM_RECORD
   index by UOM_CLASS.UOM%TYPE;

   LP_uom_cache_date   DATE := null;
   LP_uom_cache        UOM_TABLE;

   LP_item           ITEM_SUPP_COUNTRY_DIM.ITEM%TYPE;
   LP_supplier       ITEM_SUPP_COUNTRY_DIM.SUPPLIER%TYPE;
   LP_origin_country ITEM_SUPP_COUNTRY_DIM.ORIGIN_COUNTRY%TYPE;
   LP_standard_uom   UOM_CLASS.UOM%TYPE;
   LP_standard_class UOM_CLASS.UOM_CLASS%TYPE;
   LP_conv_factor    NUMBER;


--------------------------------------------------------------------------------
--                          PRIVATE PROTOTYPES                                --
--------------------------------------------------------------------------------

FUNCTION GET_UOM_INFO(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION SUPPLIER_UOM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_lwh_uom        IN OUT ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                      O_weight_uom     IN OUT ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                      O_liquid_uom     IN OUT ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(50) := 'UOM_SQL.SUPPLIER_UOM';

   cursor C_SUPPLIER_UOM is
      select cd.lwh_uom,
             cd.weight_uom,
             cd.liquid_volume_uom
        from item_supp_country_dim cd,
             item_supp_country c
       where cd.item                  = c.item
         and cd.supplier              = LP_supplier
         and cd.item                  = LP_item
         and cd.dim_object = 'CA'
         and ((LP_origin_country is NULL
         and   c.primary_country_ind = 'Y')
          or  (cd.origin_country = LP_origin_country));

BEGIN
   --- Retrieve the unit of measure from the supplier.
   SQL_LIB.SET_MARK('OPEN','C_SUPPLIER_UOM','ITEM_SUPP_COUNTRY_DIM',
                    'Supplier: ' || to_char(LP_supplier) ||
                    ', Item: ' || LP_item);
   open C_SUPPLIER_UOM;
   SQL_LIB.SET_MARK('FETCH','C_SUPPLIER_UOM','ITEM_SUPP_COUNTRY_DIM',
                    'Supplier: ' || to_char(LP_supplier) ||
                    ', Item: ' || LP_item);
   fetch C_SUPPLIER_UOM into O_lwh_uom,
                             O_weight_uom,
                             O_liquid_uom;

   if C_SUPPLIER_UOM%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_SUPPLIER_UOM','ITEM_SUPP_COUNTRY_DIM',
                       'Supplier: ' || to_char(LP_supplier) ||
                       ', Item: ' || LP_item);
      close C_SUPPLIER_UOM;
      O_lwh_uom := NULL;
      O_weight_uom := NULL;
      O_liquid_uom := NULL;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_SUPPLIER_UOM','ITEM_SUPP_COUNTRY_DIM',
                    'Supplier: ' || to_char(LP_supplier) ||
                    ', Item: ' || LP_item);
   close C_SUPPLIER_UOM;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SUPPLIER_UOM;
--------------------------------------------------------------------------------------
FUNCTION WITHIN_CLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_to_value      IN OUT NUMBER,
                      I_to_uom        IN     UOM_CLASS.UOM%TYPE,
                      I_from_value    IN     NUMBER,
                      I_from_uom      IN     UOM_CLASS.UOM%TYPE,
                      I_uom_class     IN     UOM_CLASS.UOM_CLASS%TYPE)
RETURN BOOLEAN IS
   L_misc_from_value ITEM_SUPP_UOM.VALUE%TYPE;
   L_misc_to_value   ITEM_SUPP_UOM.VALUE%TYPE;
   L_misc_uom        UOM_CLASS.UOM%TYPE;
   L_factor          UOM_CONVERSION.FACTOR%TYPE;
   L_operator        UOM_CONVERSION.OPERATOR%TYPE;
   L_unit_of_work    VARCHAR2(250);
   L_program         VARCHAR2(50) := 'UOM_SQL.WITHIN_CLASS';

   cursor C_MISC_CONVERSION is
      select value
        from item_supp_uom
       where item     = LP_item
         and supplier = LP_supplier
         and uom      = L_misc_uom;

BEGIN

   if I_to_uom = I_from_uom then
      O_to_value := I_from_value;
      return TRUE;
   end if;
   --- The 'MISC' class references the item_supp_uom table for values.
   if I_uom_class = 'MISC' then
      --- convert from
      L_misc_uom := I_from_uom;
      SQL_LIB.SET_MARK('OPEN','C_MISC_CONVERSION','ITEM_SUPP_UOM',
                       'Item: '|| LP_item ||
                       ', Supplier: ' || to_char(LP_supplier) ||
                       ', UOM: ' || L_misc_uom);
      open C_MISC_CONVERSION;
      SQL_LIB.SET_MARK('FETCH','C_MISC_CONVERSION','ITEM_SUPP_UOM',
                       'Item: '|| LP_item ||
                       ', Supplier: ' || to_char(LP_supplier) ||
                       ', UOM: ' || L_misc_uom);
      fetch C_MISC_CONVERSION into L_misc_from_value;

      if C_MISC_CONVERSION%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_MISC_CONVERSION','ITEM_SUPP_UOM',
                          'Item: '|| LP_item ||
                          ', Supplier: ' || to_char(LP_supplier) ||
                          ', UOM: ' || L_misc_uom);
          CLOSE C_MISC_CONVERSION;
         L_unit_of_work := 'Item '||LP_item||', From UOM '||I_from_uom||
                           ', To UOM '||I_to_uom;
         if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                 SQL_LIB.GET_MESSAGE_TEXT('NO_MISC_CONV_INFO',
                                                                          LP_item,
                                                                          to_char(LP_supplier),
                                                                          NULL),
                                                 L_program,
                                                 L_unit_of_work) = FALSE then
            return FALSE;
         end if;
         O_to_value := 0;
         return TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_MISC_CONVERSION','ITEM_SUPP_UOM',
                       'Item: '|| LP_item ||
                       ', Supplier: ' || to_char(LP_supplier) ||
                       ', UOM: ' || L_misc_uom);
      close C_MISC_CONVERSION;

      --- convert to
      L_misc_uom := I_to_uom;
      SQL_LIB.SET_MARK('OPEN','C_MISC_CONVERSION','ITEM_SUPP_UOM',
                       'Item: '|| LP_item ||
                       ', Supplier: ' || to_char(LP_supplier) ||
                       ', UOM: ' || L_misc_uom);
      open C_MISC_CONVERSION;
      SQL_LIB.SET_MARK('FETCH','C_MISC_CONVERSION','ITEM_SUPP_UOM',
                       'Item: '|| LP_item ||
                       ', Supplier: ' || to_char(LP_supplier) ||
                       ', UOM: ' || L_misc_uom);
      fetch C_MISC_CONVERSION into L_misc_to_value;

      if C_MISC_CONVERSION%NOTFOUND then
        SQL_LIB.SET_MARK('CLOSE','C_MISC_CONVERSION','ITEM_SUPP_UOM',
                         'Item: '|| LP_item ||
                         ', Supplier: ' || to_char(LP_supplier) ||
                         ', UOM: ' || L_misc_uom);
        close C_MISC_CONVERSION;
         L_unit_of_work := 'Item '||LP_item||', From UOM '||I_from_uom||
                           ', To UOM '||I_to_uom;
         if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                 SQL_LIB.GET_MESSAGE_TEXT('NO_MISC_CONV_INFO',
                                                                          LP_item,
                                                                          to_char(LP_supplier),
                                                                          NULL),
                                                 L_program,
                                                 L_unit_of_work) = FALSE then
            return FALSE;
         end if;
         O_to_value := 0;
         return TRUE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_MISC_CONVERSION','ITEM_SUPP_UOM',
                       'Item: '|| LP_item ||
                       ', Supplier: ' || to_char(LP_supplier) ||
                       ', UOM: ' || L_misc_uom);
      close C_MISC_CONVERSION;

      --- value in requested unit of measure
      O_to_value := ROUND((I_from_value * (L_misc_to_value / L_misc_from_value)),4);
      return TRUE;
   end if;

   ---

   if GET_UOM_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   if not LP_uom_cache.exists(I_from_uom) then
      O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
      return FALSE;
   else
      if not LP_uom_cache(I_from_uom).conversion_table.exists(I_to_uom) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_FACTOR', I_from_uom, I_to_uom, null);
         return FALSE;
      end if;
   end if;

   L_factor   := LP_uom_cache(I_from_uom).conversion_table(I_to_uom).factor;
   L_operator := LP_uom_cache(I_from_uom).conversion_table(I_to_uom).operator;

   --- convert current quantity to the new quantity in the requested unit of measure
   if L_operator = 'M' then
      O_to_value := I_from_value * L_factor;
   else
      O_to_value := I_from_value / L_factor;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_MISC_CONVERSION%ISOPEN THEN
         close C_MISC_CONVERSION;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WITHIN_CLASS;

--------------------------------------------------------------------------------------
FUNCTION BETWEEN_CLASS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_to_value           IN OUT   NUMBER,
                       I_to_uom             IN       UOM_CLASS.UOM%TYPE,
                       I_from_value         IN       NUMBER,
                       I_from_uom           IN       UOM_CLASS.UOM%TYPE,
                       I_to_class           IN       UOM_CLASS.UOM_CLASS%TYPE,
                       I_from_class         IN       UOM_CLASS.UOM_CLASS%TYPE,
                       I_calling_function   IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS

   L_from_value     NUMBER;
   L_cursor         NUMBER;
   L_convert_rate   NUMBER;
   L_return_value   NUMBER;
   L_convert_value  NUMBER;
   L_dimen_uom      UOM_CLASS.UOM%TYPE;
   L_area_uom       UOM_CLASS.UOM%TYPE;
   L_vol_uom        UOM_CLASS.UOM%TYPE;
   L_mass_uom       UOM_CLASS.UOM%TYPE;
   L_liquid_uom     UOM_CLASS.UOM%TYPE;
   L_qty_uom        UOM_CLASS.UOM%TYPE;
   L_pack_uom       UOM_CLASS.UOM%TYPE;
   L_misc_uom       UOM_CLASS.UOM%TYPE;
   L_convert_sql    UOM_X_CONVERSION.CONVERT_SQL%TYPE;
   L_convert        BOOLEAN;
   L_unit_of_work   VARCHAR2(250);
   L_program        VARCHAR2(50) := 'UOM_SQL.BETWEEN_CLASS';
   L_to_class       UOM_CLASS.UOM_CLASS%TYPE := I_to_class;

   cursor C_SQL is
     select convert_sql
       from uom_x_conversion
      where from_uom_class = I_from_class
        and to_uom_class   = L_to_class;

BEGIN
   if I_calling_function = 'OBLIGATION' and I_to_class  = 'MASS' and I_from_class = 'QTY' then 
      L_to_class :='GMAS';
   end if;

   --- In order to use the convert sql, the unit of measures need to be
   --- in the supplier's unit of measures.  Retrieve the unit of measures
   --- from the item_supp_country_dim table.
   if ((I_from_class in ('DIMEN','AREA','VOL','MASS','LVOL')) or
       (I_to_class in ('DIMEN','AREA','VOL','MASS','LVOL'))) then
      if UOM_SQL.SUPPLIER_UOM(O_error_message,
                              L_dimen_uom,
                              L_mass_uom,
                              L_liquid_uom) = FALSE then
         return FALSE;
      end if;
   end if;

   if (I_from_class = 'QTY' or I_to_class = 'QTY') then
      if LP_standard_class = 'QTY' then
         L_qty_uom := LP_standard_uom;
      else
         L_qty_uom := 'EA';
      end if;
   end if;

   if (I_from_class = 'PACK' or I_to_class = 'PACK') then
      --- When converting with the 'PACK' class, the unit of measure
      --- is in packs ('PACK').
      L_pack_uom := 'PACK';
   end if;

   --- Unit of measures retrieved from the item_supp_country_dim are
   --- in the dimension unit of measure.  Need to convert to
   --- appropriate class.
   if (I_from_class = 'AREA' or I_to_class = 'AREA') then
      L_area_uom := L_dimen_uom || '2';
   elsif (I_from_class = 'VOL' or I_to_class = 'VOL') then
      L_vol_uom := L_dimen_uom || '3';
   end if;

   --- Classes have multiple unit of measures.  Therefore, if
   --- the unit of measure is not the same as the supplier's
   --- unit of measure, then convert to the supplier's unit of measure.
   if I_from_class = 'DIMEN' then
      if I_from_uom != L_dimen_uom then
         if UOM_SQL.VALIDATE_CONVERSION(O_error_message,
                                        L_convert,
                                        I_from_class,
                                        I_from_class,
                                        L_dimen_uom,
                                        L_mass_uom,
                                        L_liquid_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_convert = FALSE then
            O_to_value := 0;
            return TRUE;
         end if;
         ---
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 L_from_value,
                                 L_dimen_uom,
                                 I_from_value,
                                 I_from_uom,
                                 I_from_class) = FALSE then
            return FALSE;
         end if;
      else
         L_from_value := I_from_value;
      end if;
   elsif I_from_class = 'AREA' then
      if I_from_uom != L_area_uom then
         if UOM_SQL.VALIDATE_CONVERSION(O_error_message,
                                        L_convert,
                                        I_from_class,
                                        I_from_class,
                                        L_dimen_uom,
                                        L_mass_uom,
                                        L_liquid_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_convert = FALSE then
            O_to_value := 0;
            return TRUE;
         end if;
         ---
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 L_from_value,
                                 L_area_uom,
                                 I_from_value,
                                 I_from_uom,
                                 I_from_class) = FALSE then
            return FALSE;
         end if;
      else
         L_from_value := I_from_value;
      end if;
   elsif I_from_class = 'VOL' then
      if I_from_uom != L_vol_uom then
         if UOM_SQL.VALIDATE_CONVERSION(O_error_message,
                                        L_convert,
                                        I_from_class,
                                        I_from_class,
                                        L_dimen_uom,
                                        L_mass_uom,
                                        L_liquid_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_convert = FALSE then
            O_to_value := 0;
            return TRUE;
         end if;
         ---
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 L_from_value,
                                 L_vol_uom,
                                 I_from_value,
                                 I_from_uom,
                                 I_from_class) = FALSE then
            return FALSE;
         end if;
      else
         L_from_value := I_from_value;
      end if;
   elsif I_from_class = 'MASS' then
      if I_from_uom != L_mass_uom then
         if UOM_SQL.VALIDATE_CONVERSION(O_error_message,
                                        L_convert,
                                        I_from_class,
                                        I_from_class,
                                        L_dimen_uom,
                                        L_mass_uom,
                                        L_liquid_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_convert = FALSE then
            O_to_value := 0;
            return TRUE;
         end if;
         ---
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 L_from_value,
                                 L_mass_uom,
                                 I_from_value,
                                 I_from_uom,
                                 I_from_class) = FALSE then
            return FALSE;
         end if;
      else
         L_from_value := I_from_value;
      end if;
   elsif I_from_class = 'QTY' then
      if I_from_uom != L_qty_uom then
         if UOM_SQL.VALIDATE_CONVERSION(O_error_message,
                                        L_convert,
                                        I_from_class,
                                        I_from_class,
                                        L_dimen_uom,
                                        L_mass_uom,
                                        L_liquid_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_convert = FALSE then
            O_to_value := 0;
            return TRUE;
         end if;
         ---
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 L_from_value,
                                 L_qty_uom,
                                 I_from_value,
                                 I_from_uom,
                                 I_from_class) = FALSE then
            return FALSE;
         end if;
      else
         L_from_value := I_from_value;
      end if;
   elsif I_from_class = 'PACK' then
      if I_from_uom != L_pack_uom then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 L_from_value,
                                 L_pack_uom,
                                 I_from_value,
                                 I_from_uom,
                                 I_from_class) = FALSE then
            return FALSE;
         end if;
      else
         L_from_value := I_from_value;
      end if;
   elsif I_from_class = 'LVOL' then
      if I_from_uom != L_liquid_uom then
         if UOM_SQL.VALIDATE_CONVERSION(O_error_message,
                                        L_convert,
                                        I_from_class,
                                        I_from_class,
                                        L_dimen_uom,
                                        L_mass_uom,
                                        L_liquid_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_convert = FALSE then
            O_to_value := 0;
            return TRUE;
         end if;
         ---
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 L_from_value,
                                 L_liquid_uom,
                                 I_from_value,
                                 I_from_uom,
                                 I_from_class) = FALSE then
            return FALSE;
         end if;
      else
         L_from_value := I_from_value;
      end if;
   elsif I_from_class = 'MISC' then
      L_misc_uom   := I_from_uom;
      L_from_value := I_from_value;
   end if;

   --- The 'MISC' class references item_supp_uom based on the uom.
   --- Therefore, if 'MISC' is one of the classes, the variable must
   --- be populated for the convert sql.
   if I_to_class = 'MISC' then
      L_misc_uom := I_to_uom;
   end if;
   ---
   if UOM_SQL.VALIDATE_CONVERSION(O_error_message,
                                  L_convert,
                                  I_from_class,
                                  I_to_class,
                                  L_dimen_uom,
                                  L_mass_uom,
                                  L_liquid_uom) = FALSE then
      return FALSE;
   end if;
   ---
   if L_convert = FALSE then
      O_to_value := 0;
      return TRUE;
   end if;
   ---
   --- Processing of the sql needed to convert to different classes. The dynamic sql
   --- will get the convert sql statement off the uom_x_conversion table based on the
   --- input variables.  Then execute the sql statement resulting in a conversion rate.
   L_cursor := DBMS_SQL.OPEN_CURSOR;

   --- get the sql to convert.
   SQL_LIB.SET_MARK('OPEN', 'C_SQL','UOM_X_CONVERSION','From class: '|| I_from_class
                    || ' and to class: ' || I_to_class);
   open C_SQL;
   SQL_LIB.SET_MARK('FETCH', 'C_SQL','UOM_X_CONVERSION','From class: '|| I_from_class
                    || ' and to class: ' || I_to_class);
   fetch C_SQL into L_convert_sql;

   if C_SQL%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE', 'C_SQL','UOM_X_CONVERSION','From class: '||
                       I_from_class || ' and to class: ' || I_to_class);
      close C_SQL;
      DBMS_SQL.CLOSE_CURSOR(L_cursor);
      O_error_message := SQL_LIB.CREATE_MSG('NO_DATA_EXISTS',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_SQL','UOM_X_CONVERSION','From class: '|| I_from_class
                    || ' and to class: ' || I_to_class);
   close C_SQL;

   --- send sql to be executed
   DBMS_SQL.PARSE(L_cursor, L_convert_sql, DBMS_SQL.V7);

   --- 'MISC' class needs to reference the item_supp_uom table in conjuction with
   --- the item_supp_country table.  If the class is anything else, only the
   --- item_supp_country table will be referenced.
   DBMS_SQL.BIND_VARIABLE(L_cursor, ':i', LP_item);
   DBMS_SQL.BIND_VARIABLE(L_cursor, ':s', LP_supplier);
   DBMS_SQL.BIND_VARIABLE(L_cursor, ':o', LP_origin_country);
   DBMS_SQL.BIND_VARIABLE(L_cursor, ':u', L_misc_uom);

   --- define output of processing which is the rate between classes.
   DBMS_SQL.DEFINE_COLUMN(L_cursor, 1, L_convert_rate);

   L_return_value := DBMS_SQL.EXECUTE(L_cursor);
   L_return_value := DBMS_SQL.FETCH_ROWS(L_cursor);
   DBMS_SQL.COLUMN_VALUE(L_cursor, 1, L_convert_rate);
   DBMS_SQL.CLOSE_CURSOR(L_cursor);
   ---
   if L_convert_rate is NULL then
      L_unit_of_work := 'Item '||LP_item||', Supplier '||to_char(LP_supplier)||
                        ', Misc. UOM '||L_misc_uom;
      if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                              SQL_LIB.GET_MESSAGE_TEXT('NO_CROSS_CONV_INFO',
                                                                       LP_item,
                                                                       to_char(LP_supplier),
                                                                       LP_origin_country),
                                              L_program,
                                              L_unit_of_work) = FALSE then
         return FALSE;
      end if;
      O_to_value := 0;
      return TRUE;
   end if;

   --- the new value is the quantity in the supplier's unit of measure.
   L_convert_value := ROUND(L_convert_rate * L_from_value,4);

   --- Need to check if the uom after the convert sql is the
   --- requested uom.  If it is not the appropriate uom, then
   --- convert to appropriate uom before output parameter is assigned.
   if I_to_class = 'DIMEN' then
      if L_dimen_uom != I_to_uom then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 L_convert_value,
                                 L_dimen_uom,
                                 I_to_class) = FALSE then
            return FALSE;
         end if;
      else
         O_to_value := L_convert_value;
      end if;
   elsif I_to_class = 'AREA' then
      if L_area_uom != I_to_uom then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 L_convert_value,
                                 L_area_uom,
                                 I_to_class) = FALSE then
            return FALSE;
         end if;
      else
         O_to_value := L_convert_value;
      end if;
   elsif I_to_class = 'VOL' then
      if L_vol_uom != I_to_uom then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 L_convert_value,
                                 L_vol_uom,
                                 I_to_class) = FALSE then
            return FALSE;
         end if;
      else
         O_to_value := L_convert_value;
      end if;
   elsif I_to_class = 'MASS' then
      if L_mass_uom != I_to_uom then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 L_convert_value,
                                 L_mass_uom,
                                 I_to_class) = FALSE then
            return FALSE;
         end if;
      else
         O_to_value := L_convert_value;
      end if;
   elsif I_to_class = 'QTY' then
      if L_qty_uom != I_to_uom then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 L_convert_value,
                                 L_qty_uom,
                                 I_to_class) = FALSE then
            return FALSE;
         end if;
      else
         O_to_value := L_convert_value;
      end if;
   elsif I_to_class = 'PACK' then
      if L_pack_uom != I_to_uom then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 L_convert_value,
                                 L_pack_uom,
                                 I_to_class) = FALSE then
            return FALSE;
         end if;
      else
         O_to_value := L_convert_value;
      end if;
   elsif I_to_class = 'LVOL' then
      if L_liquid_uom != I_to_uom then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 L_convert_value,
                                 L_liquid_uom,
                                 I_to_class) = FALSE then
            return FALSE;
         end if;
      else
         O_to_value := L_convert_value;
      end if;
   elsif I_to_class = 'MISC' then
         O_to_value := L_convert_value;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if DBMS_SQL.IS_OPEN(L_cursor) THEN 
         DBMS_SQL.CLOSE_CURSOR(L_cursor); 
      end if;
      if C_SQL%ISOPEN THEN
         close C_SQL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BETWEEN_CLASS;
--------------------------------------------------------------------------------------
FUNCTION STANDARD_CONVERSION (O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_to_value           IN OUT   NUMBER,
                              I_to_uom             IN       UOM_CLASS.UOM%TYPE,
                              I_to_uom_class       IN       UOM_CLASS.UOM_CLASS%TYPE,
                              I_from_value         IN       NUMBER,
                              I_from_uom           IN       UOM_CLASS.UOM%TYPE,
                              I_from_uom_class     IN       UOM_CLASS.UOM_CLASS%TYPE,
                              I_calling_function   IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS
   L_program            VARCHAR2(50) := 'UOM_SQL.STANDARD_CONVERSION';
   L_from_value         NUMBER := I_from_value;
   L_unit_of_work       VARCHAR2(250);
   L_calling_function   VARCHAR2(30) := I_calling_function;

BEGIN
   if (I_from_uom_class = 'QTY' or I_to_uom_class = 'QTY') then
      if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                          LP_standard_uom,
                                          LP_standard_class,
                                          LP_conv_factor,
                                          LP_item,
                                          'Y') = FALSE then
         return FALSE;
      end if;
   end if;
   if LP_standard_class is not NULL then
      if (LP_standard_class != 'QTY' and LP_conv_factor is NULL) then
         L_unit_of_work := 'Item '||LP_item;
         if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                      SQL_LIB.GET_MESSAGE_TEXT('NO_CONV_FACTOR',
                                                LP_item,
                                                NULL,
                                                NULL),
                      L_program,
                      L_unit_of_work) = FALSE then
            return FALSE;
         end if;
         O_to_value := 0;
         return TRUE;
      end if;
   end if;

   if I_from_uom_class = 'QTY' then
      if LP_standard_class = 'QTY' then
         if I_from_uom != LP_standard_uom then
            -- for example, LP_standard_uom='DOZ', I_from_uom='EA'
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                      L_from_value,
                      LP_standard_uom,
                      I_from_value,
                      I_from_uom,
                      'QTY') = FALSE then
               return FALSE;
            end if;
         end if;
         if UOM_SQL.BETWEEN_CLASS(O_error_message,
                                  O_to_value,
                                  I_to_uom,
                                  L_from_value,
                                  LP_standard_uom,
                                  I_to_uom_class,
                                  I_from_uom_class,
                                  I_calling_function) = FALSE then
            return FALSE;
         end if;
         return TRUE;
      end if;

      -- the following is for the case where LP_standard_class != 'QTY'
      if I_from_uom != 'EA' then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                      L_from_value,    --- I_from_value outputted in eaches
                      'EA',
                      I_from_value,    --- I_from_value as originally inputted
                      I_from_uom,
                      'QTY') = FALSE then
            return FALSE;
         end if;
      end if;
      ---
      L_from_value := L_from_value/nvl(LP_conv_factor,1);
      ---
      if LP_standard_uom = I_to_uom then
         O_to_value := L_from_value;
         return TRUE;
      end if;
      ---
      if LP_standard_class = I_to_uom_class then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 L_from_value,
                                 LP_standard_uom,
                                 I_to_uom_class) = FALSE then
            return FALSE;
         end if;
      else
         if UOM_SQL.BETWEEN_CLASS(O_error_message,
                                  O_to_value,
                                  I_to_uom,
                                  L_from_value,
                                  LP_standard_uom,
                                  I_to_uom_class,
                                  LP_standard_class,
                                  I_calling_function) = FALSE then
            return FALSE;
         end if;
      end if;
   elsif I_to_uom_class = 'QTY' then
      if LP_standard_class = 'QTY' then
         if UOM_SQL.BETWEEN_CLASS(O_error_message,
                                  O_to_value,
                                  LP_standard_uom,  -- to uom
                                  I_from_value,
                                  I_from_uom,
                                  I_to_uom_class,  -- 'QTY'
                                  I_from_uom_class,
                                  I_calling_function) = FALSE then
            return FALSE;
         end if;
         if I_to_uom != LP_standard_uom then
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                      O_to_value,
                      I_to_uom,
                      O_to_value,
                      LP_standard_uom,
                      'QTY') = FALSE then
               return FALSE;
            end if;
         end if;
         return TRUE;
      end if;

      -- the following is for the case where LP_standard_class != 'QTY'
      if LP_standard_uom = I_from_uom then
         L_from_value := I_from_value;
      elsif LP_standard_class = I_from_uom_class then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 L_from_value,
                                 LP_standard_uom,
                                 I_from_value,
                                 I_from_uom,
                                 LP_standard_class) = FALSE then
            return FALSE;
         end if;
      else
         if UOM_SQL.BETWEEN_CLASS(O_error_message,
                                  L_from_value,
                                  LP_standard_uom,
                                  I_from_value,
                                  I_from_uom,
                                  LP_standard_class,
                                  I_from_uom_class,
                                  I_calling_function) = FALSE then 
            return FALSE;
         end if;
      end if;
      ---
      O_to_value := L_from_value * nvl(LP_conv_factor,1);
      if I_to_uom != 'EA' then
         if UOM_SQL.WITHIN_CLASS(O_error_message,
                                 O_to_value,
                                 I_to_uom,
                                 O_to_value,
                                 'EA',
                                 'QTY') = FALSE then
            return FALSE;
         end if;
      end if;
   else
      if UOM_SQL.BETWEEN_CLASS(O_error_message,
                               O_to_value,
                               I_to_uom,
                               I_from_value,
                               I_from_uom,
                               I_to_uom_class,
                               I_from_uom_class,
                               I_calling_function) = FALSE then 
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END STANDARD_CONVERSION;
--------------------------------------------------------------------------------------
FUNCTION BUYER_PACK_CONVERT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_to_value      IN OUT NUMBER,
                            I_to_uom        IN     UOM_CLASS.UOM%TYPE,
                            I_to_uom_class  IN     UOM_CLASS.UOM_CLASS%TYPE,
                            I_pack_no       IN     ITEM_SUPP_COUNTRY.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(50):= 'UOM_SQL.BUYER_PACK_CONVERT';
   L_from_uom               UOM_CLASS.UOM%TYPE;
   L_from_class             UOM_CLASS.UOM_CLASS%TYPE;
   L_item                   ITEM_MASTER.ITEM%TYPE;
   L_qty                    V_PACKSKU_QTY.QTY%TYPE    := 0;
   L_packsku_qty            V_PACKSKU_QTY.QTY%TYPE    := 0;
   L_total_pack_qty         V_PACKSKU_QTY.QTY%TYPE    := 0;

   cursor C_GET_PACKSKUS is
      select item,
             qty
        from v_packsku_qty
       where pack_no = I_pack_no;
---
---This function will convert the quantity of a sku item within a pack to the input
---UOM and then the sum total of quantity of each sku item in the input UOM will be
---the output value. The output value equals to one each pack (sum total quantity of
---its componet in the input UOM).
---
BEGIN
   FOR C_rec in C_GET_PACKSKUS LOOP
      L_item := C_rec.item;
      LP_item := L_item;
      L_qty := C_rec.qty;

      if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                          L_from_uom,     ---output of function
                                          L_from_class,   ---output of function
                                          LP_conv_factor,
                                          L_item,
                                          'Y') = FALSE then
         return FALSE;
      end if;
      ---
      LP_standard_uom := L_from_uom;
      LP_standard_class := L_from_class;
      ---
      if UOM_SQL.STANDARD_CONVERSION(O_error_message,
                                     L_packsku_qty,       ---output value
                                     I_to_uom,            ---convert to UOM
                                     I_to_uom_class,
                                     L_qty,
                                     L_from_uom,
                                     L_from_class) = FALSE then
         return FALSE;
      end if;
      ---
      L_total_pack_qty := L_total_pack_qty + L_packsku_qty;
   end LOOP;
   ---
   O_to_value := L_total_pack_qty;   ---equals to one each pack in input UOM
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BUYER_PACK_CONVERT;
--------------------------------------------------------------------------------------
FUNCTION CONVERT(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_to_value           IN OUT   NUMBER,
                 I_to_uom             IN       UOM_CONVERSION.TO_UOM%TYPE,
                 I_from_value         IN       NUMBER,
                 I_from_uom           IN       UOM_CONVERSION.FROM_UOM%TYPE,
                 I_item               IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                 I_supplier           IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                 I_origin_country     IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                 I_calling_function   IN       VARCHAR2 DEFAULT NULL) 
RETURN BOOLEAN IS
   L_error_message      VARCHAR2(255);
   L_from_uom           UOM_CLASS.UOM%TYPE     := I_from_uom;
   L_from_class         UOM_CLASS.UOM_CLASS%TYPE;
   L_to_class           UOM_CLASS.UOM_CLASS%TYPE;
   L_program            VARCHAR2(50) := 'UOM_SQL.CONVERT';
   L_buyer_pack         VARCHAR2(1)            := 'N';
   L_qty                V_PACKSKU_QTY.QTY%TYPE := 0;
   L_packsku_qty        V_PACKSKU_QTY.QTY%TYPE := 0;
   L_total_pack_qty     V_PACKSKU_QTY.QTY%TYPE := 0;
   L_pack_to_value      V_PACKSKU_QTY.QTY%TYPE := 0;
   L_pack_from_value    V_PACKSKU_QTY.QTY%TYPE := 0;
   L_temp_out_value     V_PACKSKU_QTY.QTY%TYPE := 0;
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind       ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE;
   L_calling_function   VARCHAR2(30) := I_calling_function;

   cursor C_GET_PRIM_SUPP is
      select supplier
        from item_supplier
       where item             = I_item
         and primary_supp_ind = 'Y';

 BEGIN
   LP_supplier       := I_supplier;
   LP_origin_country := I_origin_country;
   ---
   if I_supplier is NULL then
      open  C_GET_PRIM_SUPP;
      fetch C_GET_PRIM_SUPP into LP_supplier;
      close C_GET_PRIM_SUPP;
   end if;
   ---
   if ITEM_ATTRIB_SQL.GET_PACK_INDS (O_error_message,
                                     L_pack_ind,
                                     L_sellable_ind,
                                     L_orderable_ind,
                                     L_pack_type,
                                     I_item) = FALSE then
      return FALSE;
   end if;

   ---
   if UOM_SQL.GET_CLASS(O_error_message,
                        L_to_class,
                        I_to_uom) =  FALSE then
      return FALSE;
   end if;
   ---
   if L_from_uom is not NULL then
      if UOM_SQL.GET_CLASS(O_error_message,
                           L_from_class,
                           I_from_uom) =  FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if L_from_uom is NULL then
      if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                          L_from_uom,
                                          L_from_class,
                                          LP_conv_factor,
                                          I_item,
                                          'Y') = FALSE then
         return FALSE;
      end if;

      LP_standard_uom := L_from_uom;
      LP_standard_class := L_from_class;
   end if;
   ---
   if L_from_uom = I_to_uom then
      O_to_value := I_from_value;
   else
      if L_pack_type = 'B' then
         if L_from_class = L_to_class then
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                                    O_to_value,
                                    I_to_uom,
                                    I_from_value,
                                    L_from_uom,
                                    L_from_class) =  FALSE then
               return FALSE;
            end if;
         elsif (L_from_class != 'QTY' and L_to_class = 'QTY') then
            if UOM_SQL.BUYER_PACK_CONVERT(O_error_message,
                                          L_pack_to_value, ---output from function
                                          L_from_uom,
                                          L_from_class,
                                          I_item) =  FALSE then
               return FALSE;
            end if;
            ---
            ---L_pack_to_value will be equivalent to one pack in the UOM of L_from_uom
            L_temp_out_value := I_from_value/L_pack_to_value;
            ---
            --- if I_to_uom is not EA but is in QTY class
            if I_to_uom != 'EA' then
               if UOM_SQL.WITHIN_CLASS(O_error_message,
                                       O_to_value,
                                       I_to_uom,
                                       L_temp_out_value,
                                       'EA',
                                       'QTY') =  FALSE then
                  return FALSE;
               end if;
            else
               O_to_value := L_temp_out_value;
            end if;
         ---
         elsif (L_from_class = 'QTY' and L_to_class != 'QTY') then
            if L_from_uom != 'EA' then
               if UOM_SQL.WITHIN_CLASS(O_error_message,
                                       L_qty,
                                       'EA',
                                       I_from_value,
                                       L_from_uom,
                                       L_from_class) =  FALSE then
                  return FALSE;
               end if;
            else
               L_qty := I_from_value;
            end if;
            ---
            if UOM_SQL.BUYER_PACK_CONVERT(O_error_message,
                                          L_pack_to_value,
                                          I_to_uom,
                                          L_to_class,
                                          I_item) =  FALSE then
               return FALSE;
            end if;
            ---
            ---L_pack_to_value will be equivalent to one pack in the UOM of I_to_uom
            O_to_value := L_qty * L_pack_to_value;
            ---
         elsif (L_from_class != 'QTY' and L_to_class != 'QTY') then
            if UOM_SQL.BUYER_PACK_CONVERT(O_error_message,
                                          L_pack_from_value,
                                          L_from_uom,
                                          L_from_class,
                                          I_item) =  FALSE then
               return FALSE;
             end if;
             ---
             if UOM_SQL.BUYER_PACK_CONVERT(O_error_message,
                                           L_pack_to_value,
                                           I_to_uom,
                                           L_to_class,
                                           I_item) =  FALSE then
               return FALSE;
             end if;
             ---
             O_to_value := ((1/L_pack_from_value) * L_pack_to_value)*I_from_value;
         end if;
      else --- if item is not buyer_pack
         LP_item := I_item;

         --- compare classes of the units of measure to decide what function to
         --- use in the conversion process.
         if (L_from_class = L_to_class) then
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                                    O_to_value,
                                    I_to_uom,
                                    I_from_value,
                                    L_from_uom,
                                    L_from_class) = FALSE then
               return FALSE;
            end if;
         else
            if UOM_SQL.STANDARD_CONVERSION(O_error_message,
                                           O_to_value,
                                           I_to_uom,
                                           L_to_class,
                                           I_from_value,
                                           L_from_uom,
                                           L_from_class,
                                           L_calling_function) = FALSE then 
               return FALSE;
            end if;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONVERT;
------------------------------------------------------------------------------------------------
FUNCTION CONVERT_WRAPPER(I_unit_retail IN  ITEM_LOC.REGULAR_UNIT_RETAIL%TYPE,
                         I_to_uom      IN  UOM_CONVERSION.TO_UOM%TYPE,
                         I_from_uom    IN  UOM_CONVERSION.FROM_UOM%TYPE,
                         I_item        IN  ITEM_SUPP_COUNTRY.ITEM%TYPE)
RETURN NUMBER IS
   L_error_message      VARCHAR2(255);
   L_qty                NUMBER;
   L_conv_value         NUMBER;
BEGIN
   if UOM_SQL.CONVERT(L_error_message,
                      L_qty,
                      I_to_uom,
                      1,
                      I_from_uom,
                      I_item ,
                      NULL,
                      NULL) = TRUE then
       L_conv_value := I_unit_retail/L_qty;
   end if;
   RETURN L_conv_value;
EXCEPTION
   when OTHERS then
      raise;
      return NULL;
END CONVERT_WRAPPER;
--------------------------------------------------------------------------------------
FUNCTION UOM_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists        IN OUT BOOLEAN,
                    I_uom           IN     UOM_CLASS.UOM%TYPE,
                    I_uom_class     IN     UOM_CLASS.UOM_CLASS%TYPE)
RETURN BOOLEAN IS
   L_class_desc           CODE_DETAIL.CODE_DESC%TYPE;
   L_program VARCHAR2(50) := 'UOM_SQL.UOM_EXISTS';

BEGIN
   O_exists := TRUE;

   if GET_UOM_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   if not LP_uom_cache.exists(I_uom) then
      O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
      O_exists := FALSE;
   else
      if I_uom_class is not null then
         if LP_uom_cache(I_uom).class != I_uom_class then
            if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                          'UOMC',
                                          I_uom_class,
                                          L_class_desc) = FALSE then
               return FALSE;
            end if;
            --
            O_error_message := SQL_LIB.CREATE_MSG('NO_UOM_CLASS_PASSED',L_class_desc,NULL,NULL);
            O_exists := FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UOM_EXISTS;
--------------------------------------------------------------------------------------
FUNCTION GET_DESC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_uom_desc      IN OUT UOM_CLASS_TL.UOM_DESC_TRANS%TYPE,
                  I_uom           IN     UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(50) := 'UOM_SQL.GET_DESC';

   cursor c_get_uom_desc is
   select uom_desc_trans
     from v_uom_class_tl
    where uom = I_uom;

BEGIN

   if GET_UOM_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   if not LP_uom_cache.exists(I_uom) then
      O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
      return FALSE;
   end if;

   open c_get_uom_desc;
   fetch c_get_uom_desc into O_uom_desc;
   
   if c_get_uom_desc%NOTFOUND THEN
      O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
      close c_get_uom_desc;
      return FALSE;
   end if;
   close c_get_uom_desc;

   return TRUE;

EXCEPTION
   when OTHERS then
      if c_get_uom_desc%ISOPEN then
         close c_get_uom_desc;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_DESC;
--------------------------------------------------------------------------------------
FUNCTION GET_CLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_uom_class     IN OUT UOM_CLASS.UOM_CLASS%TYPE,
                   I_uom           IN     UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(50) := 'UOM_SQL.GET_CLASS';

BEGIN

   if GET_UOM_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   if not LP_uom_cache.exists(I_uom) then
      O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
      return FALSE;
   end if;

   O_uom_class := LP_uom_cache(I_uom).class;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_CLASS;
--------------------------------------------------------------------------------------
FUNCTION CHECK_CLASS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_same_class    IN OUT BOOLEAN,
                     I_first_uom     IN     UOM_CLASS.UOM%TYPE,
                     I_second_uom    IN     UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN IS
   L_exists       VARCHAR2(1);
   L_program      VARCHAR2(50) := 'UOM_SQL.CHECK_CLASS';

BEGIN
   if I_first_uom is NULL or I_second_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   if GET_UOM_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   if not LP_uom_cache.exists(I_first_uom) or
   not LP_uom_cache.exists(I_second_uom) then
      O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
      O_same_class := FALSE;
      return FALSE;
   end if;

   if LP_uom_cache(I_first_uom).class = LP_uom_cache(I_second_uom).class then
      O_same_class := TRUE;
   else
      O_same_class := FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_CLASS;
--------------------------------------------------------------------------------------
FUNCTION GET_STANDARD_UOM (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_standard_uom    IN OUT UOM_CLASS.UOM%TYPE,
                           O_uom_conv_factor IN OUT ITEM_MASTER.UOM_CONV_FACTOR%TYPE,
                           I_item            IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'UOM_SQL.GET_STANDARD_UOM';

   cursor C_GET_ITEM_UOM is
      select standard_uom,
             uom_conv_factor
        from item_master
       where item = I_item;

BEGIN

   open  C_GET_ITEM_UOM;
   fetch C_GET_ITEM_UOM into O_standard_uom,
                             O_uom_conv_factor;
   close C_GET_ITEM_UOM;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_STANDARD_UOM;
--------------------------------------------------------------------------------------
FUNCTION VALID_STANDARD_UOM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT BOOLEAN,
                            I_standard_uom   IN     UOM_CLASS.UOM%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'UOM_SQL.VALID_STANDARD_UOM';
   L_uom_class  UOM_CLASS.UOM_CLASS%TYPE;

BEGIN
   if UOM_SQL.GET_CLASS(O_error_message,
                        L_uom_class,
                        I_standard_uom) = FALSE then
      return FALSE;
   end if;
   ---
   if L_uom_class in ('MISC','PACK') then
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_STANDARD_UOM;
--------------------------------------------------------------------------------------
FUNCTION GET_UOM_FROM_DESC(O_error_message OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_uom           OUT uom_class.uom%TYPE,
                           O_uom_class     OUT uom_class.uom_class%TYPE,
                           I_uom_desc      IN  uom_class_tl.uom_desc_trans%TYPE)
   return BOOLEAN IS

   L_program    VARCHAR2(50) := 'UOM_SQL.GET_UOM_FROM_DESC';
   L_return_successful BOOLEAN := TRUE;

   cursor C_GET_UOM is
    select uc.uom,
           uc.uom_class
      from uom_class uc, uom_class_tl ul
     where uc.uom = ul.uom
       and UPPER(ul.uom_desc_trans) = UPPER(I_uom_desc)
       and lang = get_primary_lang;

BEGIN
   open C_GET_UOM;
   fetch C_GET_UOM into O_uom,
                        O_uom_class;
   if C_GET_UOM%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST',NULL,NULL,NULL);
      L_return_successful := FALSE;
   end if;
   close C_GET_UOM;
   return L_return_successful;
EXCEPTION
   when OTHERS then
      if C_GET_UOM%ISOPEN then
         close C_GET_UOM;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));   return FALSE;
END GET_UOM_FROM_DESC;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_CONVERSION(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_convert         IN OUT BOOLEAN,
                             I_from_uom_class  IN     UOM_CLASS.UOM_CLASS%TYPE,
                             I_to_uom_class    IN     UOM_CLASS.UOM_CLASS%TYPE,
                             I_supp_lwh_dim    IN     ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE,
                             I_supp_weight_dim IN     ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE,
                             I_supp_lvol_uom   IN     ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE)
return BOOLEAN is

   L_program VARCHAR2(50) := 'UOM_SQL.VALIDATE_CONVERSION';

BEGIN
   if (I_from_uom_class = 'QTY' or I_to_uom_class = 'QTY') then
      ---
      if (I_from_uom_class = 'DIMEN' or I_to_uom_class = 'DIMEN')
        or (I_from_uom_class = 'AREA' or I_to_uom_class = 'AREA')
        or (I_from_uom_class = 'VOL' or I_to_uom_class = 'VOL') then
         ---
         if I_supp_lwh_dim is NULL then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   if (I_from_uom_class = 'QTY' and I_to_uom_class = 'MASS') then
      ---
      if I_supp_weight_dim is NULL then
         O_convert := FALSE;
         return TRUE;
      end if;
      ---
   end if;
   ---
   if (I_from_uom_class = 'QTY' and I_to_uom_class = 'LVOL') then
      ---
      if (I_supp_lwh_dim is NULL or I_supp_lvol_uom is NULL) then
         O_convert := FALSE;
         return TRUE;
      end if;
      ---
   end if;
   ---
   if (I_from_uom_class = 'DIMEN' or I_to_uom_class = 'DIMEN') then
      ---
      if (I_from_uom_class = 'DIMEN' and I_to_uom_class = 'DIMEN')
        or (I_from_uom_class = 'AREA' or I_to_uom_class = 'AREA')
        or (I_from_uom_class = 'VOL' or I_to_uom_class = 'VOL') then
         ---
         if I_supp_lwh_dim is NULL then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif (I_from_uom_class = 'MASS' or I_to_uom_class = 'MASS') then
         ---
         if (I_supp_lwh_dim is NULL or I_supp_weight_dim is NULL) then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif (I_from_uom_class = 'LVOL' or I_to_uom_class = 'LVOL') then
         ---
         if (I_supp_lwh_dim is NULL or I_supp_lvol_uom is NULL) then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   if I_from_uom_class = 'MASS' then
      ---
      if (I_to_uom_class = 'MASS' or I_to_uom_class = 'QTY') then
         ---
         if I_supp_weight_dim is NULL then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif (I_to_uom_class = 'AREA' or I_to_uom_class = 'VOL') then
         ---
         if (I_supp_weight_dim is NULL or I_supp_lwh_dim is NULL) then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif I_to_uom_class = 'LVOL' then
         ---
         if I_supp_lvol_uom is NULL or I_supp_weight_dim is NULL then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   if I_from_uom_class = 'AREA' then
      ---
      if (I_to_uom_class = 'AREA' or I_to_uom_class = 'VOL' or I_to_uom_class = 'LVOL') then
         ---
         if I_supp_lwh_dim is NULL then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif I_to_uom_class = 'MASS' then
         ---
         if (I_supp_lwh_dim is NULL or I_supp_weight_dim is NULL) then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   if I_from_uom_class = 'VOL' then
      ---
      if (I_to_uom_class = 'VOL' or I_to_uom_class = 'AREA') then
         ---
         if I_supp_lwh_dim is NULL then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif I_to_uom_class = 'MASS' then
         ---
         if (I_supp_lwh_dim is NULL or I_supp_weight_dim is NULL) then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif I_to_uom_class = 'LVOL' then
         ---
         if (I_supp_lwh_dim is NULL or I_supp_lvol_uom is NULL) then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   if I_from_uom_class = 'LVOL' then
      ---
      if (I_to_uom_class = 'QTY' or I_to_uom_class = 'LVOL') then
         ---
         if I_supp_lvol_uom is NULL then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif (I_to_uom_class = 'AREA' or I_to_uom_class = 'VOL') then
         ---
         if (I_supp_lwh_dim is NULL or I_supp_lvol_uom is NULL) then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      elsif I_to_uom_class = 'MASS' then
         ---
         if (I_supp_weight_dim is NULL or I_supp_lvol_uom is NULL) then
            O_convert := FALSE;
            return TRUE;
         end if;
         ---
      end if;
      ---
   end if;
   ---
   O_convert := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_CONVERSION;
--------------------------------------------------------------------------------------
FUNCTION VALID_UOM_FOR_ITEMS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid         IN OUT BOOLEAN,
                             I_from_uom      IN     UOM_CLASS.UOM%TYPE,
                             I_item          IN     ITEM_MASTER.ITEM%TYPE,
                             I_diff_id       IN     ITEM_MASTER.DIFF_1%TYPE,
                             I_dept          IN     DEPS.DEPT%TYPE,
                             I_class         IN     CLASS.CLASS%TYPE,
                             I_subclass      IN     SUBCLASS.SUBCLASS%TYPE,
                             I_item_list     IN     SKULIST_HEAD.SKULIST%TYPE)
   return BOOLEAN IS

   L_program     VARCHAR2(50)            := 'UOM_SQL.VALID_UOM_FOR_ITEMS';
   L_from_class  UOM_CLASS.UOM_CLASS%TYPE;
   L_to_class    UOM_CLASS.UOM_CLASS%TYPE;
   L_to_value    NUMBER;
   L_to_uom      UOM_CLASS.UOM%TYPE;
   L_conv_factor UOM_CONVERSION.FACTOR%TYPE;

   cursor C_GET_ITEMS_DEPT is
      select item
        from item_master
       where dept         = I_dept
         and class        = NVL(I_class, class)
         and subclass     = NVL(I_subclass, subclass)
         and item_level   = tran_level
         and status       = 'A';

   cursor C_GET_ITEMS_LIST is
      select im.item
        from skulist_detail sd,
             item_master im
       where sd.skulist               = I_item_list
         and ((im.item                = sd.item
              and sd.item_level       = sd.tran_level)
          or ((im.item_parent         = sd.item
               or im.item_grandparent = sd.item)
              and sd.item_level       < sd.tran_level));

   cursor C_GET_ITEMS_PARENT_DIFF is
      select item
        from item_master
       where (item                = I_item
              or item_parent      = I_item
              or item_grandparent = I_item)
         and (diff_1              = I_diff_id
              or diff_2           = I_diff_id
              or diff_3           = I_diff_id
              or diff_4           = I_diff_id
              or I_diff_id        is NULL)
         and item_level           = tran_level;

BEGIN

   if GET_CLASS(O_error_message,
                L_from_class,
                I_from_uom) = FALSE then
      return FALSE;
   end if;
   ---
   if I_dept is NOT NULL then
      if I_item is NOT NULL or I_item_list is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('Only one');
         return FALSE;
      end if;
      ---
      for dept_rec in C_GET_ITEMS_DEPT LOOP
         LP_item := dept_rec.item;
        ---
         if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                             L_to_uom,
                                             L_to_class,
                                             L_conv_factor,
                                             LP_item,
                                             'Y') = FALSE then
            return FALSE;
         end if;
         ---
         if L_to_class != L_from_class then
            ---
            if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP_COUNTRY(O_error_message,
                                                             LP_supplier,
                                                             LP_origin_country,
                                                             LP_item) = FALSE then
               -- this is nonstandard code, but the 'no supplier/country of origin exists' message
               -- is not helpful to the user because nonorderable items don't have that information
               -- but still need to function for UOM conversions.
               if O_error_message like '%NO_SUPP_COOG%' then 
                  O_error_message := SQL_LIB.CREATE_MSG('NO_CROSS_CONV_INFO_ITEM',LP_item, NULL,NULL);
               end if;
               return FALSE;
            end if;
            ---
            if BETWEEN_CLASS(O_error_message,
                             L_to_value,
                             L_to_uom,
                             1,                -- I_from_value
                             I_from_uom,
                             L_to_class,
                             L_from_class) = FALSE then
               return FALSE;
            end if;
         else
            L_to_value := 1;
         end if;
         ---
         if L_to_value = 0 then
            O_valid := FALSE;
            Exit;
         end if;
      END LOOP;
   elsif I_item_list is NOT NULL then
      if I_item is NOT NULL or I_dept is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('Only one');
         return FALSE;
      end if;
      ---
      for item_list_rec IN C_GET_ITEMS_LIST LOOP
         LP_item := item_list_rec.item;
         ---
         if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                             L_to_uom,
                                             L_to_class,
                                             L_conv_factor,
                                             LP_item,
                                             'Y') = FALSE then
            return FALSE;
         end if;
         ---
         if GET_CLASS(O_error_message,
                      L_to_class,
                      L_to_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_to_class != L_from_class then
            if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP_COUNTRY(O_error_message,
                                                             LP_supplier,
                                                             LP_origin_country,
                                                             LP_item) = FALSE then
               if O_error_message like '%NO_SUPP_COOG%' then 
                  O_error_message := SQL_LIB.CREATE_MSG('NO_CROSS_CONV_INFO_ITEM',LP_item, NULL,NULL);
               end if;
               return FALSE;
            end if;
            ---
            if BETWEEN_CLASS(O_error_message,
                             L_to_value,
                             L_to_uom,
                             1,                -- I_from_value
                             I_from_uom,
                             L_to_class,
                             L_from_class) = FALSE then
               return FALSE;
            end if;
         else
            L_to_value := 1;
         end if;
         ---
         if L_to_value = 0 then
            O_valid := FALSE;
            Exit;
         end if;
      END LOOP;
   elsif I_item is NOT NULL then
      if I_dept is NOT NULL or I_item_list is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('Only one');
         return FALSE;
      end if;
      ---
      for parent_diff_rec in C_GET_ITEMS_PARENT_DIFF LOOP
         LP_item := parent_diff_rec.item;
         ---
        if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                             L_to_uom,
                                             L_to_class,
                                             L_conv_factor,
                                             LP_item,
                                             'Y') = FALSE then
            return FALSE;
         end if;
         ---
         if GET_CLASS(O_error_message,
                      L_to_class,
                      L_to_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_to_class != L_from_class then
            if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP_COUNTRY(O_error_message,
                                                             LP_supplier,
                                                             LP_origin_country,
                                                             LP_item) = FALSE then
               if O_error_message like '%NO_SUPP_COOG%' then 
                  O_error_message := SQL_LIB.CREATE_MSG('NO_CROSS_CONV_INFO_ITEM',LP_item, NULL,NULL);
               end if;
               return FALSE;
            end if;
            ---
            if BETWEEN_CLASS(O_error_message,
                             L_to_value,
                             L_to_uom,
                             1,                -- I_from_value
                             I_from_uom,
                             L_to_class,
                             L_from_class) = FALSE then
               return FALSE;
            end if;
         else
            L_to_value := 1;
         end if;
         ---
         if L_to_value = 0 then
            O_valid := FALSE;
            Exit;
         end if;
      END LOOP;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALID_UOM_FOR_ITEMS;
--------------------------------------------------------------------------------------
FUNCTION GET_CONV_FACTOR(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_conv_factor   IN OUT UOM_CONVERSION.FACTOR%TYPE,
                         I_from_uom      IN     UOM_CONVERSION.FROM_UOM%TYPE,
                         I_to_uom        IN     UOM_CONVERSION.TO_UOM%TYPE)

   return BOOLEAN IS

   L_program  VARCHAR2(50) := 'UOM_SQL.GET_CONV_FACTOR';

BEGIN

   if I_from_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_from_uom,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_to_uom is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_to_uom,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   --

   if GET_UOM_INFO(O_error_message) = FALSE then
      return FALSE;
   end if;

   if not LP_uom_cache.exists(I_from_uom) then
      O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
      return FALSE;
   else
      if not LP_uom_cache(I_from_uom).conversion_table.exists(I_to_uom) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_FACTOR', I_from_uom, I_to_uom, null);
         return FALSE;
      end if;
   end if;

   O_conv_factor := LP_uom_cache(I_from_uom).conversion_table(I_to_uom).factor;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_CONV_FACTOR;
---------------------------------------------------------------------------
FUNCTION CONVERT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_to_value       IN OUT NUMBER,
                 I_to_uom         IN     UOM_CONVERSION.TO_UOM%TYPE,
                 I_from_value     IN     NUMBER,
                 I_from_uom       IN     UOM_CONVERSION.FROM_UOM%TYPE,
                 I_item           IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                 I_supplier       IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                 I_origin_country IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                 I_pack_type      IN     ITEM_MASTER.PACK_TYPE%TYPE,
                 I_to_class       IN     UOM_CLASS.UOM_CLASS%TYPE)
   RETURN BOOLEAN IS
   L_error_message      VARCHAR2(255);
   L_from_uom           UOM_CLASS.UOM%TYPE         := I_from_uom;
   L_from_class         UOM_CLASS.UOM_CLASS%TYPE;
   L_to_class           UOM_CLASS.UOM_CLASS%TYPE   := I_to_class;
   L_program            VARCHAR2(50)               := 'UOM_SQL.CONVERT';
   L_buyer_pack         VARCHAR2(1)                := 'N';
   L_qty                V_PACKSKU_QTY.QTY%TYPE     := 0;
   L_packsku_qty        V_PACKSKU_QTY.QTY%TYPE     := 0;
   L_total_pack_qty     V_PACKSKU_QTY.QTY%TYPE     := 0;
   L_pack_to_value      V_PACKSKU_QTY.QTY%TYPE     := 0;
   L_pack_from_value    V_PACKSKU_QTY.QTY%TYPE     := 0;
   L_temp_out_value     V_PACKSKU_QTY.QTY%TYPE     := 0;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE := I_pack_type;

   cursor C_GET_PRIM_SUPP is
      select supplier
        from item_supplier
       where item             = I_item
         and primary_supp_ind = 'Y';

 BEGIN
   LP_supplier       := I_supplier;
   LP_origin_country := I_origin_country;
   ---
   if I_supplier is NULL then
      open  C_GET_PRIM_SUPP;
      fetch C_GET_PRIM_SUPP into LP_supplier;
      close C_GET_PRIM_SUPP;
   end if;
   ---
   if L_to_class is null then
      if UOM_SQL.GET_CLASS(O_error_message,
                           L_to_class,
                           I_to_uom) =  FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if L_from_uom is not NULL then
      if UOM_SQL.GET_CLASS(O_error_message,
                           L_from_class,
                           I_from_uom) =  FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if L_from_uom is NULL then
      if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                          L_from_uom,
                                          L_from_class,
                                          LP_conv_factor,
                                          I_item,
                                          'Y') = FALSE then
         return FALSE;
      end if;

      LP_standard_uom := L_from_uom;
      LP_standard_class := L_from_class;
   end if;
   ---
   if L_from_uom = I_to_uom then
      O_to_value := I_from_value;
   else
      if L_pack_type = 'B' then
         if L_from_class = L_to_class then
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                                    O_to_value,
                                    I_to_uom,
                                    I_from_value,
                                    L_from_uom,
                                    L_from_class) =  FALSE then
               return FALSE;
            end if;
         elsif (L_from_class != 'QTY' and L_to_class = 'QTY') then
            if UOM_SQL.BUYER_PACK_CONVERT(O_error_message,
                                          L_pack_to_value, ---output from function
                                          L_from_uom,
                                          L_from_class,
                                          I_item) =  FALSE then
               return FALSE;
            end if;
            ---
            ---L_pack_to_value will be equivalent to one pack in the UOM of L_from_uom
            L_temp_out_value := I_from_value/L_pack_to_value;
            ---
            --- if I_to_uom is not EA but is in QTY class
            if I_to_uom != 'EA' then
               if UOM_SQL.WITHIN_CLASS(O_error_message,
                                       O_to_value,
                                       I_to_uom,
                                       L_temp_out_value,
                                       'EA',
                                       'QTY') =  FALSE then
                  return FALSE;
               end if;
            else
               O_to_value := L_temp_out_value;
            end if;
         ---
         elsif (L_from_class = 'QTY' and L_to_class != 'QTY') then
            if L_from_uom != 'EA' then
               if UOM_SQL.WITHIN_CLASS(O_error_message,
                                       L_qty,
                                       'EA',
                                       I_from_value,
                                       L_from_uom,
                                       L_from_class) =  FALSE then
                  return FALSE;
               end if;
            else
               L_qty := I_from_value;
            end if;
            ---
            if UOM_SQL.BUYER_PACK_CONVERT(O_error_message,
                                          L_pack_to_value,
                                          I_to_uom,
                                          L_to_class,
                                          I_item) =  FALSE then
               return FALSE;
            end if;
            ---
            ---L_pack_to_value will be equivalent to one pack in the UOM of I_to_uom
            O_to_value := L_qty * L_pack_to_value;
            ---
         elsif (L_from_class != 'QTY' and L_to_class != 'QTY') then
            if UOM_SQL.BUYER_PACK_CONVERT(O_error_message,
                                          L_pack_from_value,
                                          L_from_uom,
                                          L_from_class,
                                          I_item) =  FALSE then
               return FALSE;
             end if;
             ---
             if UOM_SQL.BUYER_PACK_CONVERT(O_error_message,
                                           L_pack_to_value,
                                           I_to_uom,
                                           L_to_class,
                                           I_item) =  FALSE then
               return FALSE;
             end if;
             ---
             O_to_value := ((1/L_pack_from_value) * L_pack_to_value)*I_from_value;
         end if;
      else --- if item is not buyer_pack
         LP_item := I_item;

         --- compare classes of the units of measure to decide what function to
         --- use in the conversion process.
         if (L_from_class = L_to_class) then
            if UOM_SQL.WITHIN_CLASS(O_error_message,
                                    O_to_value,
                                    I_to_uom,
                                    I_from_value,
                                    L_from_uom,
                                    L_from_class) = FALSE then
               return FALSE;
            end if;
         else
            if UOM_SQL.STANDARD_CONVERSION(O_error_message,
                                           O_to_value,
                                           I_to_uom,
                                           L_to_class,
                                           I_from_value,
                                           L_from_uom,
                                           L_from_class) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONVERT;

--------------------------------------------------------------------------------
--                         PRIVATE PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION GET_UOM_INFO(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

L_program        VARCHAR2(100) := 'UOM_SQL.GET_UOM_INFO';
L_vdate          DATE := GET_VDATE;
L_get_uom_info   BOOLEAN := FALSE;

cursor C_UOM is
select u.uom,
       u.uom_class,
       --
       uc.to_uom,
       uc.factor,
       uc.operator
  from uom_class u,
       uom_conversion uc
 where u.uom = uc.from_uom (+);

BEGIN

   if LP_uom_cache_date is null then
      L_get_uom_info := TRUE;
   else
      if LP_uom_cache_date != L_vdate then
         L_get_uom_info := TRUE;
      end if;
   end if;

   if L_get_uom_info then
      LP_uom_cache.delete;
      for rec in C_UOM loop
         if not LP_uom_cache.exists(rec.uom) then
            LP_uom_cache(rec.uom).class := rec.uom_class;
         end if;
         --
         if rec.to_uom is not null then
            LP_uom_cache(rec.uom).conversion_table(rec.to_uom).factor := rec.factor;
            LP_uom_cache(rec.uom).conversion_table(rec.to_uom).operator := rec.operator;
         end if;
      end loop;
      LP_uom_cache_date := L_vdate;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_UOM_INFO;
--------------------------------------------------------------------------------
FUNCTION UOM_LANG_EXIST
return CHAR IS

   cursor C_UOM_LANG_EXIST is
      select 'X'
        from UOM_CLASS_TL
       where LANG = get_user_lang;
         
   L_uom                 VARCHAR2(1);
   LP_uom_lang_exist     VARCHAR2(1);
   L_program             VARCHAR2(50) := 'UOM_SQL.UOM_LANG_EXIST';

BEGIN
   if LP_uom_lang_exist is null then
      open  C_UOM_LANG_EXIST;
      fetch C_UOM_LANG_EXIST into L_uom;
      if C_UOM_LANG_EXIST%NOTFOUND then
         LP_uom_lang_exist := 'N';
      else
         LP_uom_lang_exist := 'Y';
      end if;   
      close C_UOM_LANG_EXIST;
   end if;

   return LP_uom_lang_exist;

END UOM_LANG_EXIST;
--------------------------------------------------------------------------------
FUNCTION GET_UOM_LANG (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_standard_uom  IN     UOM_CLASS.UOM%TYPE,
                       O_uom_trans     IN OUT UOM_CLASS_TL.UOM_TRANS%TYPE)
return BOOLEAN IS

   L_language    LANG.LANG%TYPE;
   L_program     VARCHAR2(50) := 'UOM_SQL.GET_UOM_LANG';

   cursor C_GET_UOM_LANG is
      select UOM_TRANS
        from UOM_CLASS_TL
       where UOM = I_standard_uom
         and LANG = L_language;

BEGIN
   L_language :=  LANGUAGE_SQL.GET_USER_LANGUAGE; 

   if L_language = -999 then
      O_error_message := 'Error in function GET_USER_LANG';
      RETURN FALSE;
   end if;

   if UOM_LANG_EXIST ='Y' then
      if I_standard_uom is not null then
         open  C_GET_UOM_LANG;
         fetch C_GET_UOM_LANG into O_uom_trans;
         if C_GET_UOM_LANG%NOTFOUND then
            close C_GET_UOM_LANG;
            O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
            return FALSE;
         end if;
         close C_GET_UOM_LANG;
      end if;
   else
       O_uom_trans:=I_standard_uom;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_UOM_LANG;
--------------------------------------------------------------------------------
FUNCTION LOOKUP_UOM   (O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_standard_uom_trans  IN     UOM_CLASS.UOM%TYPE,
                       O_uom                 IN OUT UOM_CLASS_TL.UOM_TRANS%TYPE)
return BOOLEAN IS

   L_language         LANG.LANG%TYPE;   
   L_c_get_uom_old    VARCHAR2(1);  
   L_program          VARCHAR2(50) := 'UOM_SQL.LOOKUP_UOM';



   cursor C_GET_UOM_OLD is
      select 'X' 
        from UOM_CLASS
       where UOM= I_standard_uom_trans;

BEGIN
   L_language :=  LANGUAGE_SQL.GET_USER_LANGUAGE; 
   if L_language = -999 then
      O_error_message := 'Error in function GET_USER_LANG';
      RETURN FALSE;
   end if;

      open  C_GET_UOM_OLD;
      fetch C_GET_UOM_OLD into L_c_get_uom_old;
      if C_GET_UOM_OLD%NOTFOUND then
         close C_GET_UOM_OLD;
         O_error_message := SQL_LIB.CREATE_MSG('UOM_NOT_EXIST', null, null, null);
         return FALSE;
      else
         O_uom:=I_standard_uom_trans;
      end if;
      close C_GET_UOM_OLD;
   

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOOKUP_UOM;
--------------------------------------------------------------------------------
FUNCTION GET_UOM   (I_uom  IN     UOM_CLASS.UOM%TYPE,
                    I_mode IN  VARCHAR2)
return VARCHAR2 IS

   L_language         LANG.LANG%TYPE;   
   L_program          VARCHAR2(50) := 'UOM_SQL.GET_UOM';
   O_uom              UOM_CLASS.UOM%TYPE;
   
   cursor C_GET_UOM is
      select UOM
        from (select uom ,uom_trans, lang, RANK() OVER(PARTITION BY UOM_TRANS ORDER BY UOM) uom_rank from v_uom_class_tl) uom1
       where uom1.uom_trans = I_uom
         and uom1.uom_rank=1
	 and uom1.lang = L_language;
	
		
   cursor C_GET_UOM_TRANS is
      select UOM_TRANS
        from V_UOM_CLASS_TL 
       where UOM = I_uom
         and LANG = L_language;

BEGIN
   L_language :=  LANGUAGE_SQL.GET_USER_LANGUAGE; 

   if I_mode='Upload' then
      open  C_GET_UOM;
      fetch C_GET_UOM into O_uom;
      if C_GET_UOM%NOTFOUND then
         O_uom:=NULL;
      end if;   
      close C_GET_UOM;
   elsif I_mode='Download' then
      open  C_GET_UOM_TRANS;
      fetch C_GET_UOM_TRANS into O_uom;
      if C_GET_UOM_TRANS%NOTFOUND then
         O_uom:=NULL;
      end if;
      close C_GET_UOM_TRANS;
   end if;

   return O_uom;

EXCEPTION
   when OTHERS then 
      return NULL;
END GET_UOM;
--------------------------------------------------------------------------------
END UOM_SQL;
/
