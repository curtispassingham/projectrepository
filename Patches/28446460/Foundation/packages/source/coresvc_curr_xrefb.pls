-- File Name : CORESVC_CURR_XREF_body.pls
CREATE OR REPLACE PACKAGE BODY CORESVC_CURR_XREF as
   cursor C_SVC_FIF_CURRENCY_XREF(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select pk_curr_xref.rowid   as pk_curr_xref_rid,
		       st.rowid             as st_rid,
             st.rms_exchange_type,
             st.fif_exchange_type,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)      as action,
             st.process$status
        from svc_fif_currency_xref st,
		       fif_currency_xref     pk_curr_xref
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
			and st.fif_exchange_type = pk_curr_xref.fif_exchange_type(+);
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key             :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   fif_currency_xref_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                                         := s9t_pkg.get_sheet_names(I_file_id);
   fif_currency_xref_cols                           := s9t_pkg.get_col_names(I_file_id,FIF_CURRENCY_XREF_sheet);
   FIF_CURRENCY_XREF$ACTION                         := fif_currency_xref_cols('ACTION');   FIF_CURR_XREF$RMS_EXCHNG_TYP                     := fif_currency_xref_cols('RMS_EXCHANGE_TYPE');
   FIF_CURR_XREF$FIF_EXCHNG_TYP                     := fif_currency_xref_cols('FIF_EXCHANGE_TYPE');
END POPULATE_NAMES;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_FIF_CURRENCY_XREF( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = FIF_CURRENCY_XREF_sheet )
   select s9t_row(s9t_cells(CORESVC_CURR_XREF.action_mod ,
                            fif_exchange_type,
                            rms_exchange_type))
     from fif_currency_xref ;
END POPULATE_FIF_CURRENCY_XREF;
--------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file      s9t_file;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
   
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   if l_file.user_lang IS NULL then
      L_file.user_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   end if;
   L_file.add_sheet(FIF_CURRENCY_XREF_sheet);
   L_file.sheets(l_file.get_sheet_index(FIF_CURRENCY_XREF_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                               'FIF_EXCHANGE_TYPE',
                                                                                               'RMS_EXCHANGE_TYPE');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
-----------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_CURR_XREF.CREATE_S9T';
   L_file s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
		        					  template_key)=FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_FIF_CURRENCY_XREF(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FIF_CURRENCY_XREF( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                         I_process_id IN   SVC_FIF_CURRENCY_XREF.PROCESS_ID%TYPE) IS
   TYPE svc_fif_currency_xref_col_typ IS TABLE OF SVC_FIF_CURRENCY_XREF%ROWTYPE;
   L_temp_rec SVC_FIF_CURRENCY_XREF%ROWTYPE;
   svc_fif_currency_xref_col svc_fif_currency_xref_col_typ :=NEW svc_fif_currency_xref_col_typ();
   L_process_id  SVC_FIF_CURRENCY_XREF.PROCESS_ID%TYPE;
   L_error       BOOLEAN:=FALSE;
   L_default_rec SVC_FIF_CURRENCY_XREF%ROWTYPE;
   cursor C_MANDATORY_IND is
      select rms_exchange_type_mi,
             fif_exchange_type_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'FIF_CURRENCY_XREF'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('RMS_EXCHANGE_TYPE' as rms_exchange_type,
                                            'FIF_EXCHANGE_TYPE' as fif_exchange_type,
                                             null               as dummy));
   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_FIF_CURRENCY_XREF';
   L_pk_columns    VARCHAR2(255)  := NULL;
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
      
BEGIN
  -- Get default values.
   FOR rec IN (select  rms_exchange_type_dv,
                       fif_exchange_type_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                    = template_key
                          and wksht_key                                       = 'FIF_CURRENCY_XREF'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'RMS_EXCHANGE_TYPE' as rms_exchange_type,
                                                      'FIF_EXCHANGE_TYPE' as fif_exchange_type,
                                                       NULL               as dummy)))
   LOOP
      BEGIN
         L_default_rec.rms_exchange_type := rec.rms_exchange_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_CURRENCY_XREF ' ,
                            NULL,
                           'RMS_EXCHANGE_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.fif_exchange_type := rec.fif_exchange_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FIF_CURRENCY_XREF ' ,
                            NULL,
                           'FIF_EXCHANGE_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(FIF_CURRENCY_XREF$ACTION)                         as action,
          r.get_cell(FIF_CURR_XREF$RMS_EXCHNG_TYP)                     as rms_exchange_type,
          r.get_cell(FIF_CURR_XREF$FIF_EXCHNG_TYP)                     as fif_exchange_type,
          r.get_row_seq()                                              as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(FIF_CURRENCY_XREF_sheet))
   LOOP
      L_temp_rec                   := NULL;      
	  L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_CURRENCY_XREF_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.rms_exchange_type := rec.rms_exchange_type;
	   EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_CURRENCY_XREF_sheet,
                            rec.row_seq,
                            'RMS_EXCHANGE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fif_exchange_type := rec.fif_exchange_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FIF_CURRENCY_XREF_sheet,
                            rec.row_seq,
                            'FIF_EXCHANGE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      
      if not (1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                          FIF_CURRENCY_XREF_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_fif_currency_xref_col.extend();
         svc_fif_currency_xref_col(svc_fif_currency_xref_col.COUNT()):= L_temp_rec;
      end if;
      
   END LOOP;
   BEGIN
      forall i IN 1..svc_fif_currency_xref_col.COUNT SAVE EXCEPTIONS
      merge into SVC_FIF_CURRENCY_XREF st
      using(select
                  (case
                   when L_mi_rec.rms_exchange_type_mi    = 'N'
                    and svc_fif_currency_xref_col(i).action = CORESVC_CURR_XREF.action_mod
                    and s1.rms_exchange_type IS NULL
                   then mt.rms_exchange_type
                   else s1.rms_exchange_type
                   end) as rms_exchange_type,
                  (case
                   when L_mi_rec.fif_exchange_type_mi    = 'N'
                    and svc_fif_currency_xref_col(i).action = CORESVC_CURR_XREF.action_mod
                    and s1.fif_exchange_type IS NULL
                   then mt.fif_exchange_type
                   else s1.fif_exchange_type
                   end) as fif_exchange_type,
                  null as dummy
              from (select svc_fif_currency_xref_col(i).rms_exchange_type as rms_exchange_type,
                           svc_fif_currency_xref_col(i).fif_exchange_type as fif_exchange_type,
                           null                                           as dummy
                      from dual ) s1,
            FIF_CURRENCY_XREF mt
             where mt.fif_exchange_type (+)     = s1.fif_exchange_type   
				       and 1 = 1 )sq
                on (st.fif_exchange_type      = sq.fif_exchange_type  
					     and svc_fif_currency_xref_col(i).action IN (CORESVC_CURR_XREF.action_mod))
      when matched then
      update
         set process_id        = svc_fif_currency_xref_col(i).process_id ,
             chunk_id          = svc_fif_currency_xref_col(i).chunk_id ,
             row_seq           = svc_fif_currency_xref_col(i).row_seq ,
             action            = svc_fif_currency_xref_col(i).action ,
             process$status    = svc_fif_currency_xref_col(i).process$status ,
             rms_exchange_type = sq.rms_exchange_type ,
             create_id         = svc_fif_currency_xref_col(i).create_id ,
             create_datetime   = svc_fif_currency_xref_col(i).create_datetime ,
             last_upd_id       = svc_fif_currency_xref_col(i).last_upd_id ,
             last_upd_datetime = svc_fif_currency_xref_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             rms_exchange_type ,
             fif_exchange_type ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_fif_currency_xref_col(i).process_id ,
             svc_fif_currency_xref_col(i).chunk_id ,
             svc_fif_currency_xref_col(i).row_seq ,
             svc_fif_currency_xref_col(i).action ,
             svc_fif_currency_xref_col(i).process$status ,
             sq.rms_exchange_type ,
             sq.fif_exchange_type ,
             svc_fif_currency_xref_col(i).create_id ,
             svc_fif_currency_xref_col(i).create_datetime ,
             svc_fif_currency_xref_col(i).last_upd_id ,
             svc_fif_currency_xref_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            
            WRITE_S9T_ERROR( I_file_id,
                            FIF_CURRENCY_XREF_sheet,
                            svc_fif_currency_xref_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_FIF_CURRENCY_XREF;
----------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count     IN OUT   NUMBER,
					            I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):= 'CORESVC_CURR_XREF.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	    EXCEPTION;
   PRAGMA	    EXCEPTION_INIT(MAX_CHAR, -01706);   
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE)=FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_FIF_CURRENCY_XREF(I_file_id,
	                                I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
      
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_FIF_CURRENCY_XREF_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_error           IN OUT   BOOLEAN,
                                       I_rec             IN       C_SVC_FIF_CURRENCY_XREF%ROWTYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64)                          := 'CORESVC_CURR_XREF.PROCESS_FIF_CURRENCY_XREF_VAL';
	L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE     := 'SVC_FIF_CURRENCY_XREF';
   L_code_desc            CODE_DETAIL.CODE_DESC%TYPE;
	L_consolidation_ind    SYSTEM_OPTIONS.CONSOLIDATION_IND%TYPE := NULL;
BEGIN
   --for consolidation index 'Y' it should be in extp
	if SYSTEM_OPTIONS_SQL.CONSOLIDATION_IND(O_error_message,
                                           L_consolidation_ind) = FALSE then
     
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  0,
                  NULL,
                  O_error_message);
      O_error :=TRUE;
   end if;
	if I_rec.rms_exchange_type is NOT NULL then
	   if L_consolidation_ind='Y'	then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message, 
                                       'EXTP', 
                                       I_rec.rms_exchange_type , 
                                       L_code_desc ) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'RMS_EXCHANGE_TYPE',
                        'EXCHANGE_TYPE_NOT_FOUND');
            O_error :=TRUE;
         end if;
      else
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       'OXTP', 
                                       I_rec.rms_exchange_type ,
                                       L_code_desc ) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'RMS_EXCHANGE_TYPE',
                        'EXCHANGE_TYPE_NOT_FOUND');
            O_error :=TRUE;
         end if;
      end if;
   end if;
	return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FIF_CURRENCY_XREF_VAL;
-------------------------------------------------------------------------------------------------------
FUNCTION EXEC_FIF_CURRENCY_XREF_UPD( O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_fif_currency_xref_temp_rec    IN       FIF_CURRENCY_XREF%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_CURR_XREF.EXEC_FIF_CURRENCY_XREF_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_FIF_CURRENCY_XREF';
	RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(RECORD_LOCKED, -54);
   cursor C_CURR_XREF_LOCK is
	   select 'X' 
		  from fif_currency_xref
		 where fif_exchange_type=I_fif_currency_xref_temp_rec.fif_exchange_type
		   for update nowait;
BEGIN
   open C_CURR_XREF_LOCK;
	close C_CURR_XREF_LOCK;
   update fif_currency_xref
      set row = I_fif_currency_xref_temp_rec
    where 1 = 1
	   and fif_exchange_type=I_fif_currency_xref_temp_rec.fif_exchange_type;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_fif_currency_xref_temp_rec.fif_exchange_type,  
                                             NULL);
      return FALSE;
   when OTHERS then
	   if C_CURR_XREF_LOCK%ISOPEN then
		   close C_CURR_XREF_LOCK;
		end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FIF_CURRENCY_XREF_UPD;
--------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_FIF_CURRENCY_XREF( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_process_id      IN       SVC_FIF_CURRENCY_XREF.PROCESS_ID%TYPE,
                                    I_chunk_id        IN       SVC_FIF_CURRENCY_XREF.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64)                         := 'CORESVC_CURR_XREF.PROCESS_FIF_CURRENCY_XREF';
   L_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    := 'SVC_FIF_CURRENCY_XREF';
   L_error                      BOOLEAN;
   L_process_error              BOOLEAN                              := FALSE;
   L_fif_currency_xref_temp_rec FIF_CURRENCY_XREF%ROWTYPE;
 BEGIN
   FOR rec IN C_SVC_FIF_CURRENCY_XREF(I_process_id,
                                      I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
		if rec.action=action_mod
		   and rec.rms_exchange_type is NULL then
			WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'RMS_EXCHANGE_TYPE',
                     'MUST_ENTER_EXCHANGE_TYPE');
         L_error := TRUE;
	   end if;
		if rec.action=action_mod then 
		   if rec.fif_exchange_type is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'FIF_EXCHANGE_TYPE',
                        'MUST_ENTER_EXCHANGE_TYPE');
            L_error :=TRUE;
         
         else if rec.pk_curr_xref_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'FIF_EXCHANGE_TYPE',
                           'EXCHANGE_TYPE_NOT_FOUND');
               L_error :=TRUE;
            end if;   
         end if;		
      end if;   
      if PROCESS_FIF_CURRENCY_XREF_VAL(O_error_message,
                                       L_error,
                                       rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_fif_currency_xref_temp_rec.rms_exchange_type              := rec.rms_exchange_type;
         L_fif_currency_xref_temp_rec.fif_exchange_type              := rec.fif_exchange_type;
         if rec.action = action_mod then
            if EXEC_FIF_CURRENCY_XREF_UPD( O_error_message,
                                           L_fif_currency_xref_temp_rec )=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
     end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FIF_CURRENCY_XREF;
------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete 
     from svc_fif_currency_xref
    where process_id = I_process_id;
END;
--------------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
				      I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_CURR_XREF.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_FIF_CURRENCY_XREF(O_error_message,
                                I_process_id,
                                I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
		  set status = (CASE
			              when status = 'PE'
			              then 'PE'
                    else L_process_status
                    END),
		      action_date = sysdate
		where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_CURR_XREF;
/

