CREATE OR REPLACE PACKAGE BODY CORESVC_FLTR_GRP_HIER AS
----------------------------------------------------------------------------------------------   
   cursor C_SVC_FILTER_GROUP_ORG(I_process_id   NUMBER,
                                 I_chunk_id     NUMBER) is
      select pk_filter_group_org.rowid  as pk_filter_group_org_rid,
             st.rowid as st_rid,
             fgo_sgu_fk.rowid           as fgo_sgu_fk_rid,
             st.filter_org_id,
             st.filter_org_level,
             st.sec_group_id,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)           as action,
             st.process$status
        from svc_filter_group_org st,
             filter_group_org pk_filter_group_org,
             sec_group fgo_sgu_fk,
             dual
       where st.process_id       = I_process_id
         and st.chunk_id         = I_chunk_id
         and st.filter_org_id    = pk_filter_group_org.filter_org_id (+)
         and st.filter_org_level = pk_filter_group_org.filter_org_level (+)
         and st.sec_group_id     = pk_filter_group_org.sec_group_id (+)
         and st.sec_group_id     = fgo_sgu_fk.group_id (+);

   cursor C_SVC_FILTER_GROUP_MERCH(I_process_id NUMBER,
                                   I_chunk_id   NUMBER) is
      select uk_filter_group_merch.rowid                    as uk_filter_group_merch_rid,
             st.rowid                                       as st_rid,
             fgm_sgu_fk.rowid                               as fgm_sgu_fk_rid,
             uk_filter_group_merch.filter_merch_id_subclass as mt_subclass,
             uk_filter_group_merch.filter_merch_id_class    as mt_class,
             st.filter_merch_id_subclass,
             st.filter_merch_id_class,
             st.filter_merch_id,
             st.filter_merch_level,
             st.sec_group_id,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)                               as action,
             st.process$status
        from svc_filter_group_merch st,
             filter_group_merch uk_filter_group_merch,
             sec_group fgm_sgu_fk,
             dual
       where st.process_id                        = I_process_id
         and st.chunk_id                          = I_chunk_id
         and NVL(st.filter_merch_id_subclass,-1)  = NVL(uk_filter_group_merch.filter_merch_id_subclass (+),-1)
         and NVL(st.filter_merch_id_class,-1)     = NVL(uk_filter_group_merch.filter_merch_id_class (+),-1)
         and st.filter_merch_id                   = uk_filter_group_merch.filter_merch_id (+)
         and st.filter_merch_level                = uk_filter_group_merch.filter_merch_level (+)
         and st.sec_group_id                      = uk_filter_group_merch.sec_group_id (+)
         and st.sec_group_id                      = fgm_sgu_fk.group_id (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet     IN   VARCHAR2,
                           I_row_seq   IN   NUMBER,
                           I_col       IN   VARCHAR2,
                           I_sqlcode   IN   NUMBER,
                           I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
       CASE
          WHEN I_sqlcode IS NULL THEN
             I_sqlerrm
          ELSE
             'IIND-ORA-'||lpad(I_sqlcode,5,'0')
          END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
----------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id    IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq     IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id      IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name    IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq       IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name   IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg     IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
                       I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E')IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets                  s9t_pkg.names_map_typ;
   FILTER_GROUP_ORG_cols     s9t_pkg.names_map_typ;
   FILTER_GROUP_MERCH_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets                       := s9t_pkg.get_sheet_names(I_file_id);
   FILTER_GROUP_ORG_cols          := s9t_pkg.get_col_names(I_file_id,FLTR_GRP_ORG_sheet);
   FLTR_GRP_ORG$Action            := FILTER_GROUP_ORG_cols('ACTION');
   FLTR_GRP_ORG$FILTER_ORG_ID     := FILTER_GROUP_ORG_cols('FILTER_ORG_ID');
   FLTR_GRP_ORG$FILTER_ORG_LEVEL  := FILTER_GROUP_ORG_cols('FILTER_ORG_LEVEL');
   FLTR_GRP_ORG$SEC_GROUP_ID      := FILTER_GROUP_ORG_cols('SEC_GROUP_ID');
   FILTER_GROUP_MERCH_cols        := s9t_pkg.get_col_names(I_file_id,FLTR_GRP_MERCH_sheet);
   FLTR_GRP_MERCH$Action          := FILTER_GROUP_MERCH_cols('ACTION');
   FLTR_GRP_MERCH$ID_SUBCLASS     := FILTER_GROUP_MERCH_cols('FILTER_MERCH_ID_SUBCLASS');
   FLTR_GRP_MERCH$ID_CLASS        := FILTER_GROUP_MERCH_cols('FILTER_MERCH_ID_CLASS');
   FLTR_GRP_MERCH$FLTR_MRCH_ID    := FILTER_GROUP_MERCH_cols('FILTER_MERCH_ID');
   FLTR_GRP_MERCH$FLTR_MRCH_LEVEL := FILTER_GROUP_MERCH_cols('FILTER_MERCH_LEVEL');
   FLTR_GRP_MERCH$SEC_GROUP_ID    := FILTER_GROUP_MERCH_cols('SEC_GROUP_ID');
END POPULATE_NAMES;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_FILTER_GROUP_ORG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE( select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = FLTR_GRP_ORG_sheet )
   select s9t_row(s9t_cells(NULL ,
                            sec_group_id,
                            filter_org_level,
                            filter_org_id))
     from filter_group_org ;
END POPULATE_FILTER_GROUP_ORG;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_FILTER_GROUP_MERCH( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE( select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = FLTR_GRP_MERCH_sheet )
   select s9t_row(s9t_cells(NULL ,
                            sec_group_id,
                            filter_merch_level,
                            filter_merch_id,
                            filter_merch_id_class,
                            filter_merch_id_subclass))
     from filter_group_merch ;
END POPULATE_FILTER_GROUP_MERCH;
----------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   s9t_folder.file_name%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.USER_lang    := GET_USER_LANG;
   L_file.add_sheet(FLTR_GRP_ORG_sheet);
   L_file.sheets(l_file.get_sheet_index(FLTR_GRP_ORG_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                          'SEC_GROUP_ID',
                                                                                          'FILTER_ORG_LEVEL',
                                                                                          'FILTER_ORG_ID');
   L_file.add_sheet(FLTR_GRP_MERCH_sheet);
   L_file.sheets(l_file.get_sheet_index(FLTR_GRP_MERCH_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                            'SEC_GROUP_ID',
                                                                                            'FILTER_MERCH_LEVEL',
                                                                                            'FILTER_MERCH_ID',
                                                                                            'FILTER_MERCH_ID_CLASS',
                                                                                            'FILTER_MERCH_ID_SUBCLASS');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64):='CORESVC_FLTR_GRP_HIER.CREATE_S9T';
   L_file      s9t_file;
   
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_FILTER_GROUP_ORG(O_file_id);
      POPULATE_FILTER_GROUP_MERCH(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file)= FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FILTER_GROUP_ORG( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                        I_process_id   IN   SVC_FILTER_GROUP_ORG.process_id%TYPE) IS
   TYPE svc_FILTER_GROUP_ORG_col_typ IS TABLE OF SVC_FILTER_GROUP_ORG%ROWTYPE;
   L_temp_rec                 SVC_FILTER_GROUP_ORG%ROWTYPE;
   svc_FILTER_GROUP_ORG_col   svc_FILTER_GROUP_ORG_col_typ := NEW svc_FILTER_GROUP_ORG_col_typ();
   L_process_id               SVC_FILTER_GROUP_ORG.process_id%TYPE;
   L_error                    BOOLEAN := FALSE;
   L_default_rec              SVC_FILTER_GROUP_ORG%ROWTYPE;
   cursor C_MANDATORY_IND is
      select
             FILTER_ORG_ID_mi,
             FILTER_ORG_LEVEL_mi,
             SEC_GROUP_ID_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = template_key
                 and wksht_key      = 'FILTER_GROUP_ORG') 
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('FILTER_ORG_ID' as FILTER_ORG_ID,
                                       'FILTER_ORG_LEVEL' as FILTER_ORG_LEVEL,
                                       'SEC_GROUP_ID' as SEC_GROUP_ID));
   l_mi_rec c_mandatory_ind%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
   L_pk_columns    VARCHAR2(255)  := 'FILTER_ORG_ID,FILTER_ORG_LEVEL,SEC_GROUP_ID';
   L_table         VARCHAR2(30)   := 'SVC_FILTER_GROUP_ORG';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select
                       FILTER_ORG_ID_dv,
                       FILTER_ORG_LEVEL_dv,
                       SEC_GROUP_ID_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key     = template_key
                          and wksht_key        = 'FILTER_GROUP_ORG')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ( 'FILTER_ORG_ID' as FILTER_ORG_ID,
                                                 'FILTER_ORG_LEVEL' as FILTER_ORG_LEVEL,
                                                 'SEC_GROUP_ID' as SEC_GROUP_ID)))
   LOOP
      BEGIN
         L_default_rec.FILTER_ORG_ID := rec.FILTER_ORG_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FILTER_GROUP_ORG' ,
                            NULL,
                           'FILTER_ORG_ID' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.FILTER_ORG_LEVEL := rec.FILTER_ORG_LEVEL_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FILTER_GROUP_ORG' ,
                            NULL,
                           'FILTER_ORG_LEVEL' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.SEC_GROUP_ID := rec.SEC_GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FILTER_GROUP_ORG' ,
                            NULL,
                           'SEC_GROUP_ID' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(FLTR_GRP_ORG$Action)           as Action,
          r.get_cell(FLTR_GRP_ORG$FILTER_ORG_ID)    as FILTER_ORG_ID,
          r.get_cell(FLTR_GRP_ORG$FILTER_ORG_LEVEL) as FILTER_ORG_LEVEL,
          r.get_cell(FLTR_GRP_ORG$SEC_GROUP_ID)     as SEC_GROUP_ID,
          r.get_row_seq()                           as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(FLTR_GRP_ORG_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_ORG_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_ORG_ID := rec.FILTER_ORG_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_ORG_sheet,
                            rec.row_seq,
                            'FILTER_ORG_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_ORG_LEVEL := rec.FILTER_ORG_LEVEL;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_ORG_sheet,
                            rec.row_seq,
                            'FILTER_ORG_LEVEL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SEC_GROUP_ID := rec.SEC_GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_ORG_sheet,
                            rec.row_seq,
                            'SEC_GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_FLTR_GRP_HIER.action_new then
         L_temp_rec.FILTER_ORG_ID    := NVL( L_temp_rec.FILTER_ORG_ID,L_default_rec.FILTER_ORG_ID);
         L_temp_rec.FILTER_ORG_LEVEL := NVL( L_temp_rec.FILTER_ORG_LEVEL,L_default_rec.FILTER_ORG_LEVEL);
         L_temp_rec.SEC_GROUP_ID     := NVL( L_temp_rec.SEC_GROUP_ID,L_default_rec.SEC_GROUP_ID);
      end if;
      if NOT ( L_temp_rec.FILTER_ORG_ID is NOT NULL and
               L_temp_rec.FILTER_ORG_LEVEL is NOT NULL and
               L_temp_rec.SEC_GROUP_ID is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         FLTR_GRP_ORG_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_FILTER_GROUP_ORG_col.extend();
         svc_FILTER_GROUP_ORG_col(svc_FILTER_GROUP_ORG_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      FORALL i IN 1..svc_FILTER_GROUP_ORG_col.COUNT SAVE EXCEPTIONS
      merge into SVC_FILTER_GROUP_ORG st
      using(select
                  (case
                   when l_mi_rec.FILTER_ORG_ID_mi    = 'N'
                    and svc_FILTER_GROUP_ORG_col(i).action = CORESVC_FLTR_GRP_HIER.action_mod
                    and s1.FILTER_ORG_ID IS NULL
                   then mt.FILTER_ORG_ID
                   else s1.FILTER_ORG_ID
                   end) as FILTER_ORG_ID,
                  (case
                   when l_mi_rec.FILTER_ORG_LEVEL_mi    = 'N'
                    and svc_FILTER_GROUP_ORG_col(i).action = CORESVC_FLTR_GRP_HIER.action_mod
                    and s1.FILTER_ORG_LEVEL IS NULL
                   then mt.FILTER_ORG_LEVEL
                   else s1.FILTER_ORG_LEVEL
                   end) as FILTER_ORG_LEVEL,
                  (case
                   when l_mi_rec.SEC_GROUP_ID_mi    = 'N'
                    and svc_FILTER_GROUP_ORG_col(i).action = CORESVC_FLTR_GRP_HIER.action_mod
                    and s1.SEC_GROUP_ID IS NULL
                   then mt.SEC_GROUP_ID
                   else s1.SEC_GROUP_ID
                   end) as SEC_GROUP_ID
              from (select svc_FILTER_GROUP_ORG_col(i).FILTER_ORG_ID as FILTER_ORG_ID,
                           svc_FILTER_GROUP_ORG_col(i).FILTER_ORG_LEVEL as FILTER_ORG_LEVEL,
                           svc_FILTER_GROUP_ORG_col(i).SEC_GROUP_ID as SEC_GROUP_ID
                      from dual ) s1,
            FILTER_GROUP_ORG mt
             where
                  mt.FILTER_ORG_ID (+)     = s1.FILTER_ORG_ID   and
                  mt.FILTER_ORG_LEVEL (+)  = s1.FILTER_ORG_LEVEL   and
                  mt.SEC_GROUP_ID (+)      = s1.SEC_GROUP_ID  )sq
                on (st.FILTER_ORG_ID      = sq.FILTER_ORG_ID and
                    st.FILTER_ORG_LEVEL   = sq.FILTER_ORG_LEVEL and
                    st.SEC_GROUP_ID       = sq.SEC_GROUP_ID and
                    svc_FILTER_GROUP_ORG_col(i).ACTION IN (CORESVC_FLTR_GRP_HIER.action_mod,CORESVC_FLTR_GRP_HIER.action_del))
      when matched then
      update
         set process_id        = svc_FILTER_GROUP_ORG_col(i).process_id ,
             chunk_id          = svc_FILTER_GROUP_ORG_col(i).chunk_id ,
             row_seq           = svc_FILTER_GROUP_ORG_col(i).row_seq ,
             action            = svc_FILTER_GROUP_ORG_col(i).action ,
             process$status    = svc_FILTER_GROUP_ORG_col(i).process$status ,
             create_id         = svc_FILTER_GROUP_ORG_col(i).create_id ,
             create_datetime   = svc_FILTER_GROUP_ORG_col(i).create_datetime ,
             last_upd_id       = svc_FILTER_GROUP_ORG_col(i).last_upd_id ,
             last_upd_datetime = svc_FILTER_GROUP_ORG_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             filter_org_id ,
             filter_org_level ,
             sec_group_id ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_FILTER_GROUP_ORG_col(i).process_id ,
             svc_FILTER_GROUP_ORG_col(i).chunk_id ,
             svc_FILTER_GROUP_ORG_col(i).row_seq ,
             svc_FILTER_GROUP_ORG_col(i).action ,
             svc_FILTER_GROUP_ORG_col(i).process$status ,
             sq.filter_org_id ,
             sq.filter_org_level ,
             sq.sec_group_id ,
             svc_FILTER_GROUP_ORG_col(i).create_id ,
             svc_FILTER_GROUP_ORG_col(i).create_datetime ,
             svc_FILTER_GROUP_ORG_col(i).last_upd_id ,
             svc_FILTER_GROUP_ORG_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_ORG_sheet,
                            svc_FILTER_GROUP_ORG_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            sql%bulk_exceptions(i).error_code,
                            'S9T_DUP_REC');
         END LOOP;
   END;
END PROCESS_S9T_FILTER_GROUP_ORG;
----------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_FILTER_GROUP_MERCH( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                          I_process_id   IN   SVC_FILTER_GROUP_MERCH.process_id%TYPE) IS
   TYPE svc_FILTER_GROUP_MERCH_col_typ IS TABLE OF SVC_FILTER_GROUP_MERCH%ROWTYPE;
   L_temp_rec                   SVC_FILTER_GROUP_MERCH%ROWTYPE;
   svc_FILTER_GROUP_MERCH_col   svc_FILTER_GROUP_MERCH_col_typ :=NEW svc_FILTER_GROUP_MERCH_col_typ();
   L_process_id                 SVC_FILTER_GROUP_MERCH.process_id%TYPE;
   L_error                      BOOLEAN:=FALSE;
   L_default_rec                SVC_FILTER_GROUP_MERCH%ROWTYPE;
   cursor C_MANDATORY_IND is
      select
             FILTER_MERCH_ID_SUBCLASS_mi,
             FILTER_MERCH_ID_CLASS_mi,
             FILTER_MERCH_ID_mi,
             FILTER_MERCH_LEVEL_mi,
             SEC_GROUP_ID_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key     = template_key
                 and wksht_key        = 'FILTER_GROUP_MERCH')
               PIVOT (MAX(mandatory) as mi
                  FOR(column_key)IN('FILTER_MERCH_ID_SUBCLASS' as FILTER_MERCH_ID_SUBCLASS,
                                    'FILTER_MERCH_ID_CLASS'    as FILTER_MERCH_ID_CLASS,
                                    'FILTER_MERCH_ID'          as FILTER_MERCH_ID,
                                    'FILTER_MERCH_LEVEL'       as FILTER_MERCH_LEVEL,
                                    'SEC_GROUP_ID'             as SEC_GROUP_ID));
   l_mi_rec c_mandatory_ind%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
      L_table         VARCHAR2(30)   := 'SVC_FILTER_GROUP_MERCH';
      L_pk_columns    VARCHAR2(255)  := 'FILTER_MERCH_ID,FILTER_MERCH_LEVEL,SEC_GROUP_ID';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select
                       FILTER_MERCH_ID_SUBCLASS_dv,
                       FILTER_MERCH_ID_CLASS_dv,
                       FILTER_MERCH_ID_dv,
                       FILTER_MERCH_LEVEL_dv,
                       SEC_GROUP_ID_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key    = template_key
                          and wksht_key       = 'FILTER_GROUP_MERCH')
                        PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'FILTER_MERCH_ID_SUBCLASS' as FILTER_MERCH_ID_SUBCLASS,
                                                      'FILTER_MERCH_ID_CLASS'    as FILTER_MERCH_ID_CLASS,
                                                      'FILTER_MERCH_ID'          as FILTER_MERCH_ID,
                                                      'FILTER_MERCH_LEVEL'       as FILTER_MERCH_LEVEL,
                                                      'SEC_GROUP_ID'             as SEC_GROUP_ID)))
   LOOP
      BEGIN
         L_default_rec.FILTER_MERCH_ID_SUBCLASS := rec.FILTER_MERCH_ID_SUBCLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FILTER_GROUP_MERCH' ,
                            NULL,
                           'FILTER_MERCH_ID_SUBCLASS' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.FILTER_MERCH_ID_CLASS := rec.FILTER_MERCH_ID_CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FILTER_GROUP_MERCH' ,
                            NULL,
                           'FILTER_MERCH_ID_CLASS' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.FILTER_MERCH_ID := rec.FILTER_MERCH_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FILTER_GROUP_MERCH' ,
                            NULL,
                           'FILTER_MERCH_ID' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.FILTER_MERCH_LEVEL := rec.FILTER_MERCH_LEVEL_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FILTER_GROUP_MERCH' ,
                            NULL,
                           'FILTER_MERCH_LEVEL' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.SEC_GROUP_ID := rec.SEC_GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'FILTER_GROUP_MERCH' ,
                            NULL,
                           'SEC_GROUP_ID' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(FLTR_GRP_MERCH$Action)          as Action,
          r.get_cell(FLTR_GRP_MERCH$ID_SUBCLASS)     as FILTER_MERCH_ID_SUBCLASS,
          r.get_cell(FLTR_GRP_MERCH$ID_CLASS)        as FILTER_MERCH_ID_CLASS,
          r.get_cell(FLTR_GRP_MERCH$FLTR_MRCH_ID)    as FILTER_MERCH_ID,
          r.get_cell(FLTR_GRP_MERCH$FLTR_MRCH_LEVEL) as FILTER_MERCH_LEVEL,
          r.get_cell(FLTR_GRP_MERCH$SEC_GROUP_ID)    as SEC_GROUP_ID,
          r.get_row_seq()                            as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(FLTR_GRP_MERCH_sheet))
   LOOP
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_MERCH_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID_SUBCLASS := rec.FILTER_MERCH_ID_SUBCLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_MERCH_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID_SUBCLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID_CLASS := rec.FILTER_MERCH_ID_CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_MERCH_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID_CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID := rec.FILTER_MERCH_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_MERCH_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_LEVEL := rec.FILTER_MERCH_LEVEL;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_MERCH_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_LEVEL',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SEC_GROUP_ID := rec.SEC_GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_MERCH_sheet,
                            rec.row_seq,
                            'SEC_GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_FLTR_GRP_HIER.action_new then
         L_temp_rec.FILTER_MERCH_ID_SUBCLASS := NVL( L_temp_rec.FILTER_MERCH_ID_SUBCLASS,L_default_rec.FILTER_MERCH_ID_SUBCLASS);
         L_temp_rec.FILTER_MERCH_ID_CLASS    := NVL( L_temp_rec.FILTER_MERCH_ID_CLASS,L_default_rec.FILTER_MERCH_ID_CLASS);
         L_temp_rec.FILTER_MERCH_ID          := NVL( L_temp_rec.FILTER_MERCH_ID,L_default_rec.FILTER_MERCH_ID);
         L_temp_rec.FILTER_MERCH_LEVEL       := NVL( L_temp_rec.FILTER_MERCH_LEVEL,L_default_rec.FILTER_MERCH_LEVEL);
         L_temp_rec.SEC_GROUP_ID             := NVL( L_temp_rec.SEC_GROUP_ID,L_default_rec.SEC_GROUP_ID);
      end if;
      if not ( L_temp_rec.FILTER_MERCH_ID is NOT NULL and
               L_temp_rec.FILTER_MERCH_LEVEL is NOT NULL and
               L_temp_rec.SEC_GROUP_ID is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         FLTR_GRP_MERCH_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_FILTER_GROUP_MERCH_col.extend();
         svc_FILTER_GROUP_MERCH_col(svc_FILTER_GROUP_MERCH_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_FILTER_GROUP_MERCH_col.COUNT SAVE EXCEPTIONS
      merge into SVC_FILTER_GROUP_MERCH st
      using(select
                  (case
                   when l_mi_rec.FILTER_MERCH_ID_SUBCLASS_mi    = 'N'
                    and svc_FILTER_GROUP_MERCH_col(i).action = CORESVC_FLTR_GRP_HIER.action_mod
                    and s1.FILTER_MERCH_ID_SUBCLASS IS NULL
                   then mt.FILTER_MERCH_ID_SUBCLASS
                   else s1.FILTER_MERCH_ID_SUBCLASS
                   end) as FILTER_MERCH_ID_SUBCLASS,
                  (case
                   when l_mi_rec.FILTER_MERCH_ID_CLASS_mi    = 'N'
                    and svc_FILTER_GROUP_MERCH_col(i).action = CORESVC_FLTR_GRP_HIER.action_mod
                    and s1.FILTER_MERCH_ID_CLASS IS NULL
                   then mt.FILTER_MERCH_ID_CLASS
                   else s1.FILTER_MERCH_ID_CLASS
                   end) as FILTER_MERCH_ID_CLASS,
                  (case
                   when l_mi_rec.FILTER_MERCH_ID_mi    = 'N'
                    and svc_FILTER_GROUP_MERCH_col(i).action = CORESVC_FLTR_GRP_HIER.action_mod
                    and s1.FILTER_MERCH_ID IS NULL
                   then mt.FILTER_MERCH_ID
                   else s1.FILTER_MERCH_ID
                   end) as FILTER_MERCH_ID,
                  (case
                   when l_mi_rec.FILTER_MERCH_LEVEL_mi    = 'N'
                    and svc_FILTER_GROUP_MERCH_col(i).action = CORESVC_FLTR_GRP_HIER.action_mod
                    and s1.FILTER_MERCH_LEVEL IS NULL
                   then mt.FILTER_MERCH_LEVEL
                   else s1.FILTER_MERCH_LEVEL
                   end) as FILTER_MERCH_LEVEL,
                  (case
                   when l_mi_rec.SEC_GROUP_ID_mi    = 'N'
                    and svc_FILTER_GROUP_MERCH_col(i).action = CORESVC_FLTR_GRP_HIER.action_mod
                    and s1.SEC_GROUP_ID IS NULL
                   then mt.SEC_GROUP_ID
                   else s1.SEC_GROUP_ID
                   end) as SEC_GROUP_ID
              from (select svc_FILTER_GROUP_MERCH_col(i).FILTER_MERCH_ID_SUBCLASS as FILTER_MERCH_ID_SUBCLASS,
                           svc_FILTER_GROUP_MERCH_col(i).FILTER_MERCH_ID_CLASS    as FILTER_MERCH_ID_CLASS,
                           svc_FILTER_GROUP_MERCH_col(i).FILTER_MERCH_ID          as FILTER_MERCH_ID,
                           svc_FILTER_GROUP_MERCH_col(i).FILTER_MERCH_LEVEL       as FILTER_MERCH_LEVEL,
                           svc_FILTER_GROUP_MERCH_col(i).SEC_GROUP_ID             as SEC_GROUP_ID
                      from dual ) s1,
                   FILTER_GROUP_MERCH mt
             where mt.FILTER_MERCH_ID_SUBCLASS (+) = s1.FILTER_MERCH_ID_SUBCLASS   and
                   mt.FILTER_MERCH_ID_CLASS (+)    = s1.FILTER_MERCH_ID_CLASS   and
                   mt.FILTER_MERCH_ID (+)          = s1.FILTER_MERCH_ID   and
                   mt.FILTER_MERCH_LEVEL (+)       = s1.FILTER_MERCH_LEVEL   and
                   mt.SEC_GROUP_ID (+)             = s1.SEC_GROUP_ID )sq
                on (st.FILTER_MERCH_ID_SUBCLASS   = sq.FILTER_MERCH_ID_SUBCLASS and
                    st.FILTER_MERCH_ID_CLASS      = sq.FILTER_MERCH_ID_CLASS and
                    st.FILTER_MERCH_ID            = sq.FILTER_MERCH_ID and
                    st.FILTER_MERCH_LEVEL         = sq.FILTER_MERCH_LEVEL and
                    st.SEC_GROUP_ID               = sq.SEC_GROUP_ID and
                    svc_FILTER_GROUP_MERCH_col(i).ACTION IN (CORESVC_FLTR_GRP_HIER.action_mod,CORESVC_FLTR_GRP_HIER.action_del))
      when matched then
      update
         set process_id      = svc_FILTER_GROUP_MERCH_col(i).process_id ,
             chunk_id        = svc_FILTER_GROUP_MERCH_col(i).chunk_id ,
             row_seq         = svc_FILTER_GROUP_MERCH_col(i).row_seq ,
             action          = svc_FILTER_GROUP_MERCH_col(i).action ,
             process$status  = svc_FILTER_GROUP_MERCH_col(i).process$status ,
             create_id       = svc_FILTER_GROUP_MERCH_col(i).create_id ,
             create_datetime = svc_FILTER_GROUP_MERCH_col(i).create_datetime ,
             last_upd_id     = svc_FILTER_GROUP_MERCH_col(i).last_upd_id ,
             last_upd_datetime = svc_FILTER_GROUP_MERCH_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             filter_merch_id_subclass ,
             filter_merch_id_class ,
             filter_merch_id ,
             filter_merch_level ,
             sec_group_id ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_FILTER_GROUP_MERCH_col(i).process_id ,
             svc_FILTER_GROUP_MERCH_col(i).chunk_id ,
             svc_FILTER_GROUP_MERCH_col(i).row_seq ,
             svc_FILTER_GROUP_MERCH_col(i).action ,
             svc_FILTER_GROUP_MERCH_col(i).process$status ,
             sq.filter_merch_id_subclass ,
             sq.filter_merch_id_class ,
             sq.filter_merch_id ,
             sq.filter_merch_level ,
             sq.sec_group_id ,
             svc_FILTER_GROUP_MERCH_col(i).create_id ,
             svc_FILTER_GROUP_MERCH_col(i).create_datetime ,
             svc_FILTER_GROUP_MERCH_col(i).last_upd_id ,
             svc_FILTER_GROUP_MERCH_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;         
            WRITE_S9T_ERROR(I_file_id,
                            FLTR_GRP_MERCH_sheet,
                            svc_FILTER_GROUP_MERCH_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            sql%bulk_exceptions(i).error_code,
                            'S9T_DUP_REC');
         END LOOP;
   END;
END PROCESS_S9T_FILTER_GROUP_MERCH;
----------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count     IN OUT   NUMBER,
                      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN       NUMBER )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64):='CORESVC_FLTR_GRP_HIER.PROCESS_S9T';
   L_file             s9t_file;
   L_sheets           s9t_pkg.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if s9t_pkg.code2desc(O_error_message,
                     template_category,
                     L_file,
                     TRUE)= FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_FILTER_GROUP_ORG(I_file_id,I_process_id);
      PROCESS_S9T_FILTER_GROUP_MERCH(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when MAX_CHAR then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
              values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_FILTER_GROUP_ORG_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error           IN OUT   BOOLEAN,
                                      I_rec             IN       C_SVC_FILTER_GROUP_ORG%ROWTYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                        := 'CORESVC_FLTR_GRP_HIER.PROCESS_FILTER_GROUP_ORG_VAL';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_FILTER_GROUP_ORG';
   L_tsf_entity         TSF_ENTITY%ROWTYPE;
   L_wh                 WH_ATTRIB_SQL.WH_RECTYPE;
   L_exists             BOOLEAN;
   L_filter_id_desc     PARTNER.PARTNER_DESC%TYPE := NULL;
   L_org_hier_type      NUMBER;

BEGIN
-- Update action should not be allowed.   
   if I_rec.action = action_mod then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'ACTION',
                  'MOD_ACTION_NA');
      O_error := TRUE;
   end if;

-- Validate the Organization level ID. 
   if I_rec.filter_org_level = 'W' then
      if WH_ATTRIB_SQL.GET_WH_INFO(O_error_message,
                                   L_wh,
                                   I_rec.filter_org_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     O_error_message);
         O_error := TRUE;
      end if;

   elsif I_rec.filter_org_level = 'I' then
      if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                      L_exists,
                                      L_filter_id_desc,
                                      I_rec.filter_org_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_exists = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     'INV_INT_FINISHER');
         O_error := TRUE;
      end if;

   elsif I_rec.filter_org_level = 'E' then
      if PARTNER_SQL.GET_DESC(O_error_message,
                              L_filter_id_desc,
                              I_rec.filter_org_id,
                              I_rec.filter_org_level) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     O_error_message);
         O_error := TRUE;
      end if;

   elsif I_rec.filter_org_level = 'O' then
      if ORG_UNIT_SQL.GET_DESC(O_error_message,
                               L_filter_id_desc,
                               I_rec.filter_org_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     O_error_message);
         O_error := TRUE;
      end if;

   elsif I_rec.filter_org_level = 'T' then
      if TSF_ENTITY_SQL.GET_ROW(O_error_message,
                                L_exists,
                                L_tsf_entity,
                                I_rec.filter_org_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_exists = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     'INV_ENTITY');
         O_error := TRUE;
      end if;

   else
      if I_rec.filter_org_level = 'C' then
         L_org_hier_type := 10;
      elsif I_rec.filter_org_level = 'A' then
         L_org_hier_type := 20;
      elsif I_rec.filter_org_level = 'R' then
         L_org_hier_type := 30;
      elsif I_rec.filter_org_level = 'D' then
         L_org_hier_type := 40;
      end if;
      ---
      if ORGANIZATION_ATTRIB_SQL.GET_NAME(O_error_message,
                                          L_org_hier_type,
                                          I_rec.filter_org_id,
                                          L_filter_id_desc) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     O_error_message);
         O_error := TRUE;
      end if;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FILTER_GROUP_ORG_VAL;
----------------------------------------------------------------------------------
FUNCTION EXEC_FILTER_GROUP_ORG_INS( O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_filter_group_org_temp_rec   IN       FILTER_GROUP_ORG%ROWTYPE )
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                        := 'CORESVC_FLTR_GRP_HIER.EXEC_FILTER_GROUP_ORG_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_FILTER_GROUP_ORG';
BEGIN
   insert into filter_group_org
        values I_filter_group_org_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FILTER_GROUP_ORG_INS;
----------------------------------------------------------------------------------
FUNCTION EXEC_FILTER_GROUP_ORG_DEL( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_process_error     IN OUT   BOOLEAN,
                                    I_rec                IN       C_SVC_FILTER_GROUP_ORG%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_FLTR_GRP_HIER.EXEC_FILTER_GROUP_ORG_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_FILTER_GROUP_ORG';
   L_got_lock       BOOLEAN;
   L_code_desc      CODE_DETAIL.CODE_DESC%TYPE;
   L_filter_level   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE;
   L_org            VARCHAR2(25) := 'ORG';

BEGIN

   if FILTER_GROUP_HIER_SQL.LOCK_FILTER_GROUP_RECORD( O_error_message,
                                                      L_got_lock,
                                                      L_org,
                                                      I_rec.sec_group_id,
                                                      I_rec.filter_org_level,
                                                      I_rec.filter_org_id,
                                                      NULL,
                                                      NULL) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'LOCK_FILTER_GROUP_RECORD',
                  O_error_message); 
      O_process_error := TRUE;
   end if;
   if L_got_lock = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'FILTER_GROUP_ORG',
                  'REC_LOCKED'); 
      O_process_error := TRUE;                                                     
   else
      delete
        from filter_group_org
       where filter_org_id    = I_rec.filter_org_id
         and filter_org_level = I_rec.filter_org_level
         and sec_group_id     = I_rec.sec_group_id;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FILTER_GROUP_ORG_DEL;
----------------------------------------------------------------------------------
FUNCTION PROCESS_FILTER_GROUP_ORG( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN       SVC_FILTER_GROUP_ORG.PROCESS_ID%TYPE,
                                   I_chunk_id        IN       SVC_FILTER_GROUP_ORG.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                    VARCHAR2(64)   :='CORESVC_FLTR_GRP_HIER.PROCESS_FILTER_GROUP_ORG';
   L_error                      BOOLEAN;
   L_process_error              BOOLEAN         := FALSE;
   L_FILTER_GROUP_ORG_temp_rec  FILTER_GROUP_ORG%ROWTYPE;
   L_table                      VARCHAR2(255)   :='SVC_FILTER_GROUP_ORG';
BEGIN
   FOR rec IN c_svc_FILTER_GROUP_ORG(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      if rec.action = action_new
         and rec.PK_FILTER_GROUP_ORG_rid is NOT NULL then
          O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                'FILTER_GROUP_ORG',
                                                NULL,
                                                NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Sec Group ID, Filter Org Level, Filter Org ID',
                     'O_error_message');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_FILTER_GROUP_ORG_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Sec Group ID, Filter Org Level, Filter Org ID',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;
      if rec.fgo_sgu_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'GROUP_ID',
                     'INV_VALUE');
         L_error :=TRUE;
      end if;
      if NOT( rec.FILTER_ORG_LEVEL IN  ( 'C','A','R','D','W', 'I', 'E', 'T','O' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FILTER_ORG_LEVEL',
                     'INV_VALUE');
         L_error :=TRUE;
      end if;
      if PROCESS_FILTER_GROUP_ORG_VAL(O_error_message,
                                      L_error,
                                      rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_filter_group_org_temp_rec.filter_org_id    := rec.filter_org_id;
         L_filter_group_org_temp_rec.filter_org_level := rec.filter_org_level;
         L_filter_group_org_temp_rec.sec_group_id     := rec.sec_group_id;
         if rec.action = action_new then
            if EXEC_FILTER_GROUP_ORG_INS( O_error_message,
                                          L_filter_group_org_temp_rec ) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_FILTER_GROUP_ORG_DEL( O_error_message,
                                          L_process_error,  
                                          rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FILTER_GROUP_ORG;
----------------------------------------------------------------------------------
FUNCTION PROCESS_FILTER_GROUP_MERCH_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error           IN OUT   BOOLEAN,
                                        I_rec              IN       C_SVC_FILTER_GROUP_MERCH%ROWTYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                      := 'CORESVC_FLTR_GRP_HIER.PROCESS_FILTER_GROUP_MERCH_VAL';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FILTER_GROUP_MERCH';
   L_filter_id_desc   VARCHAR2(255);
   L_class_desc       CLASS.CLASS_NAME%TYPE;
   L_subclass_desc    SUBCLASS.SUB_NAME%TYPE;

BEGIN
-- Update action should not be allowed.   
   if I_rec.action = action_mod then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'ACTION',
                  'INV_ACT');
      O_error := TRUE;
   end if;

-- Validate the merchandise level ID.    
   if I_rec.filter_merch_level = 'D' then
      if I_rec.filter_merch_id_class is NOT NULL
         or I_rec.filter_merch_id_subclass is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COMBO',
                                               'FILTER_MERCH_LEVEL',
                                               'FILTER_MERCH_ID_CLASS',
                                               'FILTER_MERCH_ID_SUBCLASS');
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
       
      if MERCH_ATTRIB_SQL.DIVISION_NAME(O_error_message,
                                        I_rec.filter_merch_id,
                                        L_filter_id_desc) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_MERCH_LEVEL',
                     O_error_message);
         O_error := TRUE;                                       
      end if;
   elsif I_rec.filter_merch_level = 'G' then
      if I_rec.filter_merch_id_class is NOT NULL
         or I_rec.filter_merch_id_subclass is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COMBO',
                                               'FILTER_MERCH_LEVEL',
                                               'FILTER_MERCH_ID_CLASS',
                                               'FILTER_MERCH_ID_SUBCLASS');
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
      if MERCH_ATTRIB_SQL.GROUP_NAME(O_error_message,
                                     I_rec.filter_merch_id,
                                     L_filter_id_desc) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_MERCH_LEVEL',
                     O_error_message);
         O_error := TRUE;
      end if;
   elsif I_rec.filter_merch_level in ('P', 'C', 'S') then
      if I_rec.filter_merch_level = 'P' 
         and (I_rec.filter_merch_id_class is NOT NULL
         or I_rec.filter_merch_id_subclass is NOT NULL) then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COMBO',
                                               'FILTER_MERCH_LEVEL',
                                               'FILTER_MERCH_ID_CLASS',
                                               'FILTER_MERCH_ID_SUBCLASS');
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
      if I_rec.filter_merch_level = 'C' 
         and I_rec.filter_merch_id_subclass is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COMBO',
                                               'FILTER_MERCH_LEVEL',
                                               'FILTER_MERCH_ID_SUBCLASS',
                                               NULL);
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     'CLASS_SUBCLASS_NA');
         O_error := TRUE;
      end if;
      if DEPT_ATTRIB_SQL.GET_NAME(O_error_message,
                                  I_rec.filter_merch_id,
                                  L_filter_id_desc) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_MERCH_LEVEL',
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;
  
  if I_rec.filter_merch_id_class is NOT NULL then
      if CLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                   I_rec.filter_merch_id,
                                   I_rec.filter_merch_id_class,
                                   L_class_desc) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_MERCH_ID_CLASS',
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;
   
   if I_rec.filter_merch_id_subclass is NOT NULL then
      if SUBCLASS_ATTRIB_SQL.GET_NAME(O_error_message,
                                      I_rec.filter_merch_id,
                                      I_rec.filter_merch_id_class,
                                      I_rec.filter_merch_id_subclass,
                                      L_subclass_desc) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FILTER_MERCH_ID_SUBCLASS',
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FILTER_GROUP_MERCH_VAL;
----------------------------------------------------------------------------------
FUNCTION EXEC_FILTER_GROUP_MERCH_INS(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_filter_group_merch_temp_rec   IN       FILTER_GROUP_MERCH%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_FLTR_GRP_HIER.EXEC_FILTER_GROUP_MERCH_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_FILTER_GROUP_MERCH';
BEGIN
   insert into filter_group_merch
        values I_filter_group_merch_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FILTER_GROUP_MERCH_INS;
----------------------------------------------------------------------------------
FUNCTION EXEC_FILTER_GROUP_MERCH_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_process_error   IN OUT   BOOLEAN,
                                     I_rec             IN       C_SVC_FILTER_GROUP_MERCH%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                        := 'CORESVC_FLTR_GRP_HIER.EXEC_FILTER_GROUP_MERCH_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'SVC_FILTER_GROUP_MERCH';
   L_got_lock       BOOLEAN;
   L_code_desc      CODE_DETAIL.CODE_DESC%TYPE;
   L_filter_level   FILTER_GROUP_ORG.FILTER_ORG_LEVEL%TYPE;
   L_org            VARCHAR2(25) := 'MERCH';

BEGIN
   if FILTER_GROUP_HIER_SQL.LOCK_FILTER_GROUP_RECORD( O_error_message,
                                                      L_got_lock,
                                                      L_org,
                                                      I_rec.sec_group_id,
                                                      I_rec.filter_merch_level,
                                                      I_rec.filter_merch_id,
                                                      I_rec.filter_merch_id_class,
                                                      I_rec.filter_merch_id_subclass) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'LOCK_FILTER_GROUP_RECORD',
                  O_error_message); 
      O_process_error := TRUE;
   end if;
   if L_got_lock = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'FILTER_GROUP_MERCH',
                  'REC_LOCKED'); 
      O_process_error := TRUE;                                                     
   else
      delete
        from filter_group_merch
       where ( (filter_merch_id_subclass = NVL(I_rec.filter_merch_id_subclass,-1) )
               or (filter_merch_id_subclass is NULL and I_rec.filter_merch_id_subclass is NULL))
         and ( (filter_merch_id_class = NVL(I_rec.filter_merch_id_class,-1) )
               or (filter_merch_id_class is NULL and I_rec.filter_merch_id_class is NULL))

         and filter_merch_id          = I_rec.filter_merch_id
         and filter_merch_level       = I_rec.filter_merch_level
         and sec_group_id             = I_rec.sec_group_id;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_FILTER_GROUP_MERCH_DEL;
----------------------------------------------------------------------------------
FUNCTION PROCESS_FILTER_GROUP_MERCH( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN       SVC_FILTER_GROUP_MERCH.PROCESS_ID%TYPE,
                                     I_chunk_id        IN       SVC_FILTER_GROUP_MERCH.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                       VARCHAR2(64)                      :='CORESVC_FLTR_GRP_HIER.PROCESS_FILTER_GROUP_MERCH';
   L_table                         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_FILTER_GROUP_MERCH';
   L_error                         BOOLEAN;
   L_process_error                 BOOLEAN;
   L_filter_group_merch_temp_rec   FILTER_GROUP_MERCH%ROWTYPE;
   
BEGIN
   FOR rec IN c_svc_FILTER_GROUP_MERCH(I_process_id,
                                       I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.UK_FILTER_GROUP_MERCH_rid is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'FILTER_GROUP_MERCH',
                                               NULL,
                                               NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Sec Group ID, Filter Merch Level, Filter Merch ID, Filter Merch ID Class, Filter Merch ID Subclass',
                     O_error_message);
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.UK_FILTER_GROUP_MERCH_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Sec Group ID, Filter Merch Level, Filter Merch ID, Filter Merch ID Class, Filter Merch ID Subclass',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;
      if rec.fgm_sgu_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'GROUP_ID',
                     'INV_VALUE');
         L_error :=TRUE;
      end if;

      if NOT( ( rec.FILTER_MERCH_LEVEL = 'S' 
                and rec.FILTER_MERCH_ID_CLASS IS NOT NULL 
                and rec.FILTER_MERCH_ID_SUBCLASS IS NOT NULL )  
                or ( rec.FILTER_MERCH_LEVEL != 'S' ) ) then
         WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                    I_chunk_id,
                    L_table,
                    rec.row_seq,
                    'FILTER_MERCH_ID_SUBCLASS',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
      if NOT( ( rec.FILTER_MERCH_LEVEL = 'C' 
                and rec.FILTER_MERCH_ID_CLASS IS NOT NULL )  
                or ( rec.FILTER_MERCH_LEVEL != 'C' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FILTER_MERCH_ID_CLASS',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
      if NOT( rec.FILTER_MERCH_LEVEL IN  ( 'D','G','P','C','S' )  ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'FILTER_MERCH_LEVEL',
                     'INV_VALUE');
         L_error :=TRUE;
      end if;
      if PROCESS_FILTER_GROUP_MERCH_VAL(O_error_message,
                                        L_error,
                                        rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_filter_group_merch_temp_rec.filter_merch_id_subclass  := rec.filter_merch_id_subclass;
         L_filter_group_merch_temp_rec.filter_merch_id_class     := rec.filter_merch_id_class;
         L_filter_group_merch_temp_rec.filter_merch_id           := rec.filter_merch_id;
         L_filter_group_merch_temp_rec.filter_merch_level        := rec.filter_merch_level;
         L_filter_group_merch_temp_rec.sec_group_id              := rec.sec_group_id;
         if rec.action = action_new then
            if EXEC_FILTER_GROUP_MERCH_INS( O_error_message,
                                            L_filter_group_merch_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_FILTER_GROUP_MERCH_DEL( O_error_message,
                                            L_process_error,
                                            rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_FILTER_GROUP_MERCH;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete from svc_filter_group_org 
      where process_id = I_process_id;
   
   delete from svc_filter_group_merch 
      where process_id = I_process_id;
END;
----------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    := 'CORESVC_FLTR_GRP_HIER.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';    
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_FILTER_GROUP_ORG(O_error_message,
                               I_process_id,
                               I_chunk_id) = FALSE then
      return FALSE;
   end if;
   if PROCESS_FILTER_GROUP_MERCH(O_error_message,
                                 I_process_id,
                                 I_chunk_id) = FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
      
   update svc_process_tracker
        set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
            action_date = SYSDATE
      where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;   
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);         
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------
END CORESVC_FLTR_GRP_HIER;
/