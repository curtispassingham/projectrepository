CREATE OR REPLACE PACKAGE BODY ITEM_APPROVAL_SQL AS
----------------------------------------------------------------

   -- This collection is being defined to hold item-status-change records in cache
   -- instead of applying them immediately. This is done to make sure that if
   -- related sibling item fails approval then main-item is also not approved.
   -- So, instead of applying status updates immediately, records will be held in this cache.
   -- If related sibling fails validation then main-item's record will be removed from this cache.
   Type process_item_typ
   IS
     record
     (
       new_status ITEM_MASTER.STATUS%TYPE,
       single_record VARCHAR2(1),
       item ITEM_MASTER.ITEM%TYPE,
       REJECT_IND CHAR);

   Type process_item_cache_typ
   IS
     TABLE OF process_item_typ INDEX BY VARCHAR2(25);

  process_item_cache process_item_cache_typ;
  Lp_approval_list item_tbl;

----------------------------------------------------------------
FUNCTION APPROVAL_CHECK(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_approved             IN OUT   BOOLEAN,
                        I_skip_component_chk   IN       VARCHAR2,
                        I_parent_status        IN       ITEM_MASTER.STATUS%TYPE,
                        I_new_status           IN       ITEM_MASTER.STATUS%TYPE,
                        I_item                 IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_program                    VARCHAR2(64)  := 'ITEM_APPROVAL_SQL.APPROVAL_CHECK';
   L_supp_exists                BOOLEAN;
   L_exists                      BOOLEAN       :=FALSE;
   L_unit_retail_exists          VARCHAR2(1)   := 'N';
   L_component_exists            VARCHAR2(1)   := 'N';
   L_nonappr_component_exists    VARCHAR2(1)   := 'N';
   L_nonsub_component_exists     VARCHAR2(1)   := 'N';
   L_deleted_component_exists    VARCHAR2(1)   := 'N';
   L_all_exist                   BOOLEAN       := NULL;
   L_supplier                    ITEM_SUPPLIER.SUPPLIER%TYPE       := NULL;

   L_primary_manu_country_exists VARCHAR2(1)   := 'N';

   L_cost_change_exists          VARCHAR2(1)   := 'N';
   L_item_parent                 ITEM_MASTER.ITEM%TYPE;
   L_parent_status               ITEM_MASTER.STATUS%TYPE;

   L_req_no_value                VARCHAR2(1);
   L_group_exist                 VARCHAR2(1)   := 'N';
   L_loc_req_ind                 VARCHAR2(1);
   L_loc_exists                  VARCHAR2(1)   := 'N';
   L_docs_req_ind                VARCHAR2(1);
   L_docs_exist                  VARCHAR2(1)   := 'N';
   L_hts_req_ind                 VARCHAR2(1);
   L_hts_exists                  VARCHAR2(1)   := 'N';
   L_tariff_req_ind              VARCHAR2(1);
   L_tariff_exists               BOOLEAN       := FALSE;
   L_exp_req_ind                 VARCHAR2(1);
   L_expense_exists              VARCHAR2(1)   := 'N';
   L_dimension_req_ind           VARCHAR2(1);
   L_dimensions_exist            VARCHAR2(1)   := 'N';
   L_diff_1                      ITEM_MASTER.DIFF_1%TYPE;
   L_diff_2                      ITEM_MASTER.DIFF_2%TYPE;
   L_diff_3                      ITEM_MASTER.DIFF_3%TYPE;
   L_diff_4                      ITEM_MASTER.DIFF_4%TYPE;
   L_diffs_req_ind               VARCHAR2(1);
   L_wastage_req_ind             VARCHAR2(1);
   L_pack_sz_req_ind             VARCHAR2(1);

   L_retail_lb_req_ind           VARCHAR2(1);

   L_mfg_rec_req_ind             VARCHAR2(1);

   L_handling_req_ind            VARCHAR2(1);
   L_handling_temp_req_ind       VARCHAR2(1);

   L_comments_req_ind            VARCHAR2(1);
   L_itattrib_exists             VARCHAR2(1)   := 'N';
   L_impattrib_req_ind           VARCHAR2(1);
   L_impattrib_exists            BOOLEAN       := FALSE;
   L_tickets_req_ind             VARCHAR2(1);
   L_tickets_exist               BOOLEAN       := FALSE;
   L_timeline_req_ind            VARCHAR2(1);
   L_timeline_exists             VARCHAR2(1)   := 'N';
   L_image_req_ind               VARCHAR2(1);
   L_image_exists                VARCHAR2(1)   := 'N';
   L_sub_tr_items_req_ind        VARCHAR2(1);
   L_sub_tr_exists               VARCHAR2(1)   := 'N';
   L_seasons_req_ind             VARCHAR2(1);
   L_seasons_exist               VARCHAR2(1)   := 'N';
   L_dummy                       VARCHAR2(62);
   L_ndummy                      NUMBER(20);
   L_bracket_no_cost             VARCHAR2(1)   := 'N';
   L_store_exists                VARCHAR2(1)   := 'N';
   L_item_cost_exists            VARCHAR2(1)   := 'N';
   L_innerpack_exists            VARCHAR2(1)   := 'N';

   L_item_master_rec             ITEM_MASTER%ROWTYPE;

   L_purchase_type               DEPS.DEPT%TYPE;

   L_sellable_exists             VARCHAR2(1)   := 'N' ;

   L_null_tolerances_exist       VARCHAR2(1)   := 'N' ;

   L_system_options_rec          SYSTEM_OPTIONS%ROWTYPE;
   L_no_supp_loc_exist           BOOLEAN       := FALSE;
   L_item_ctry_exist             BOOLEAN       := FALSE;
   L_ctry_attrib_exist           BOOLEAN       := FALSE;
   L_loc_ctry_ind                VARCHAR2(1)   := 'N';
   L_country_id                  COUNTRY_ATTRIB.COUNTRY_ID%TYPE;
   L_uom_mismatch_exists         VARCHAR2(1)   := 'N' ;
   L_custom_obj_rec              "CUSTOM_OBJ_REC"            := CUSTOM_OBJ_REC();
   L_multi_parent_ind            BOOLEAN       := FALSE;
   L_sum_of_yield_percentages    NUMBER(12);
   L_deleted_inner_exists        VARCHAR2(1)   := 'N';

   cursor C_COMPONENT_EXISTS is
      select 'Y'
        from packitem
       where pack_no = I_item;

   cursor C_COMPONENT_NOT_APPROVED is
      select 'Y'
        from packitem pi,
             item_master im
       where I_item     = pi.pack_no
         and im.item    = pi.item
         and im.status != 'A';

   cursor C_COMPONENT_NOT_SUBMITTED is
      select 'Y'
        from packitem pi,
             item_master im
       where I_item    = pi.pack_no
         and im.item   = pi.item
         and im.status not in ('S','A');

   cursor C_DELETED_COMPONENT_EXISTS is
      select 'Y'
        from packitem pi,
             item_master im
       where pi.pack_no = I_item
         and im.item = pi.item
         and exists( select 'x'
                       from daily_purge
                      where key_value = pi.item
                        and table_name = 'ITEM_MASTER'
                        and rownum = 1 );

   cursor C_DELETED_INNER_PACK_EXISTS is
      select 'Y'
        from packitem pi1,
             packitem pi2,
             item_master im
       where pi1.pack_no = I_item
         and pi1.item = pi2.pack_no
         and im.item = pi2.pack_no
         and im.simple_pack_ind = 'Y'
         and exists (select 'x'
                       from daily_purge
                      where key_value = pi2.item
                        and table_name = 'ITEM_MASTER'
                        and rownum = 1);

   cursor C_PRIMARY_MANU_COUNTRY_EXIST is
      select 'X'
        from item_supplier isp
       where isp.item = I_item
         and not exists( select 'x'
                           from item_supp_manu_country ismc
                          where ismc.item = isp.item
                            and ismc.supplier = isp.supplier
                            and ismc.primary_manu_ctry_ind = 'Y');

   cursor C_NULL_TOLERANCES_EXIST is
      select 'Y'
        from item_supp_country
       where item = I_item
         and (   max_tolerance is NULL
              or min_tolerance is NULL);

   cursor C_GET_PARENT_STATUS is
      select status
        from item_master
       where item = L_item_master_rec.item_parent;

   cursor C_ITEM_GROUPS is
      select 'Y'
        from item_master
       where item_level          = tran_level
         and (item_parent        = I_item
             or item_grandparent = I_item);

   cursor C_CHECK_LOC is
      select 'Y'
        from item_loc
       where item = I_item;

   cursor C_CHECK_SEASONS is
      select 'Y'
        from item_seasons
       where item = I_item;

   cursor C_CHECK_DOCS is
      select 'Y'
        from req_doc
       where module      = 'IT'
         and key_value_1 = I_item;

   cursor C_CHECK_HTS is
      select 'Y'
        from item_hts
       where item = I_item;

   cursor C_CHECK_EXPENSE is
      select 'Y'
        from item_exp_head
       where item = I_item;

   cursor C_CHECK_TIMELINE is
      select 'Y'
        from timeline
       where timeline_type = 'IT'
         and key_value_1   = I_item;

   cursor C_CHECK_IMAGE is
      select 'Y'
        from item_image
       where item = I_item
       and ROWNUM = 1;

   cursor C_CHECK_SUB_TR is
   select 'Y'
     from item_master
    where tran_level < item_level
      and (item_parent            = I_item
              or item_grandparent = I_item);

   cursor C_CHECK_DIMS is
      select 'Y'
        from item_supp_country_dim
       where dim_object = 'CA'
         and item       = I_item;

   cursor C_CHECK_BRACKET_SUPPLIER is
      select 'Y'
        from item_supp_country_bracket_cost
       where nvl(unit_cost,0) = 0
         and item             = I_item;

   cursor C_CHECK_FOR_CC is
      select 'Y'
        from cost_susp_sup_head ch,
             cost_susp_sup_detail cd,
             sups,
             item_supplier its
       where ch.status in ('W','S','A','R')
         and ch.reason in (1,2,3)
         and ch.cost_change_origin    = 'SUP'
         and cd.cost_change           = ch.cost_change
         and its.supplier             = cd.supplier
         and sups.bracket_costing_ind = 'Y'
         and its.supplier             = sups.supplier
         and its.item                 = cd.item
         and its.item                 = I_item
   UNION ALL
      select 'Y'
        from cost_susp_sup_head ch,
             cost_susp_sup_detail_loc cdl,
             sups,
             item_supplier its
       where ch.status in ('W','S','A','R')
         and ch.reason in (1,2,3)
         and ch.cost_change_origin    = 'SUP'
         and cdl.cost_change          = ch.cost_change
         and its.supplier             = cdl.supplier
         and sups.bracket_costing_ind = 'Y'
         and its.supplier             = sups.supplier
         and its.item                 = cdl.item
         and its.item                 = I_item;

   cursor C_DEPT_INFO is
      select purchase_type
        from deps
       where dept = L_item_master_rec.dept;

   cursor C_XFORM_EXISTS is
      select 'Y'
        from item_xform_head
       where head_item = L_item_master_rec.item
         and ROWNUM    = 1;

   cursor C_GET_LOC_CTRY is
      select 'Y',
             ic.country_id
        from item_country ic,
             country_attrib ca
       where ic.item           = I_item
         and ic.country_id     = ca.country_id
         and ca.localized_ind  = 'Y';

   cursor C_CHECK_ITEM_COST is
      select 'Y'
        from item_cost_head ich
       where ich.item = I_item;

   cursor C_CHECK_INNER_PACK is
      select 'Y'
        from packitem pcki,
             item_master im
       where im.item            = pcki.item
         and im.pack_ind        = 'Y'
         and pcki.pack_no       = I_item
         and ROWNUM             = 1;

   cursor C_RELATED_UOM_MISMATCH is
      select 'Y'
        from item_master mim,
             item_master rim,
             related_item_head rih,
             related_item_detail rid
       where mim.item = I_item
         and rih.item = mim.item
         and rih.relationship_type = 'SUBS'
         and rid.relationship_id = rih.relationship_id
         and rim.item = rid.related_item
         and mim.standard_uom <> rim.standard_uom;

BEGIN
   O_approved := TRUE;
   ---
   delete from item_approval_error
    where item in (select item
                 from item_master
                where (   I_item = item
                       or I_item = item_parent
                       or I_item = item_grandparent))
      and override_ind = 'N';
   ---
   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_system_options_rec) then
      return FALSE;
   end if;
   ---

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_master_rec,
                                      I_item) = FALSE then

      return FALSE;
   end if;
   ----------------------------------------------------------------------------
   --- ALL ITEM LEVELS
   ----------------------------------------------------------------------------
   --- if I_new_status is 'A' and current item status = 'W' item can't be approved.

   if I_new_status = 'A' and L_item_master_rec.status = 'W' then
      if INSERT_ERROR(O_error_message,
                      I_item,
                      'IT_NO_APPROVE',
                      'Y',
                      'N') = FALSE then
         return FALSE;
      end if;
      O_approved := FALSE;
   end if;

   --- Get the status of the parent
   if I_parent_status is NULL then
      -- Since status update records are being cached in collection instead of
      -- applying immediately, hence first check if status update for parent is
      -- scheduled in collection. If not, then check actual tables.
      if process_item_cache.exists(L_item_master_rec.item_parent)
      then
          L_parent_status := I_new_status;
      else
          --- Fetch the status of the parent
          SQL_LIB.SET_MARK('OPEN','C_GET_PARENT_STATUS','item_parent: '||L_item_parent,NULL);
          open C_GET_PARENT_STATUS;
          SQL_LIB.SET_MARK('FETCH','C_GET_PARENT_STATUS','item_parent: '||L_item_parent,NULL);
          fetch C_GET_PARENT_STATUS into L_parent_status;
          SQL_LIB.SET_MARK('CLOSE','C_GET_PARENT_STATUS','item_parent: '||L_item_parent,NULL);
          close C_GET_PARENT_STATUS;
      --- If no parent exists (level 1 item) then set status to A so processing can continue

          if L_parent_status is NULL then
             L_parent_status := 'A';
          end if;
      end if;
      ---
   else
      L_parent_status := I_parent_status;
   end if;
   --- If parent status is below child status then write to errors table;
   --- child status can't pass parent status
   if I_new_status = 'S' and L_parent_status not in ('A','S') then
      if INSERT_ERROR(O_error_message,
                      I_item,
                      'IT_PARENT_NOT_S',
                      'Y',
                      'N') = FALSE then
         return FALSE;
      end if;
      O_approved := FALSE;
   elsif I_new_status = 'A' and L_parent_status != 'A' then
      if INSERT_ERROR(O_error_message,
                      I_item,
                      'IT_PARENT_NOT_A',
                      'Y',
                      'N') = FALSE then
         return FALSE;
      end if;
      O_approved := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_DEPT_INFO',I_item,NULL);
   open C_DEPT_INFO;
   SQL_LIB.SET_MARK('FETCH','C_DEPT_INFO',I_item,NULL);
   fetch C_DEPT_INFO into L_purchase_type;
   SQL_LIB.SET_MARK('CLOSE','C_DEPT_INFO',I_item,NULL);
   close C_DEPT_INFO;

   if L_item_master_rec.catch_weight_ind = 'Y' then
      if L_item_master_rec.sale_type is NULL and L_item_master_rec.sellable_ind = 'Y' and L_item_master_rec.item_level <= L_item_master_rec.tran_level then

         if INSERT_ERROR(O_error_message,
                         I_item,
                         'IT_CWITEM_NO_SALE_TYPE',
                         'Y',
                         'N') = FALSE then
            return FALSE;

         end if;
         O_approved := FALSE;
      end if;
      if L_item_master_rec.order_type is NULL and L_item_master_rec.item_level <= L_item_master_rec.tran_level then

         if INSERT_ERROR(O_error_message,
                         I_item,
                         'IT_CWITEM_NO_ORDER_TYPE',
                         'Y',
                         'N') = FALSE then
            return FALSE;

         end if;
         O_approved := FALSE;
      end if;
   end if; -- catchweight_ind chk

   if L_system_options_rec.default_tax_type = 'GTAX' then
      if ITEM_COUNTRY_SQL.CHECK_ITEM_COUNTRY_EXIST(O_error_message,
                                                   L_item_ctry_exist,
                                                   I_item ) = FALSE then
         return FALSE;
      end if;

      if L_item_ctry_exist = FALSE then
         if INSERT_ERROR(O_error_message,
                         I_item,
                         'IT_NO_ITEM_CTRY',
                         'Y',
                         'N') = FALSE then
            return FALSE;
         end if;
         O_approved := FALSE;
      end if; -- item country check
   end if;

   SQL_LIB.SET_MARK('OPEN','C_GET_LOC_CTRY','item: '||I_item,NULL);
   open C_GET_LOC_CTRY;
   SQL_LIB.SET_MARK('FETCH','C_GET_LOC_CTRY','item: '||I_item,NULL);
   fetch C_GET_LOC_CTRY into L_loc_ctry_ind,
                             L_country_id;
   SQL_LIB.SET_MARK('CLOSE','C_GET_LOC_CTRY','item: '||I_item,NULL);
   close C_GET_LOC_CTRY;

   if L_loc_ctry_ind = 'Y' and L_item_master_rec.pack_ind ='N' then
      if ITEM_COUNTRY_SQL.CHECK_COUNTRY_ATTRIB_EXIST(O_error_message,
                                                     L_ctry_attrib_exist,
                                                     L_country_id,
                                                     I_item ) = FALSE then
         return FALSE;
      end if;

      if L_ctry_attrib_exist = FALSE then
         if INSERT_ERROR(O_error_message,
                         I_item,
                         'IT_NO_CTRY_ATTRIB',
                         'Y',
                         'N') = FALSE then
            return FALSE;
         end if;
         O_approved := FALSE;
      end if;
   end if; -- country attrib check
   ----------------------------------------------------------------------------
   --- XFORM ITEMS
   ----------------------------------------------------------------------------
   if L_item_master_rec.item_xform_ind = 'Y'
      and L_item_master_rec.orderable_ind = 'Y'
      and L_item_master_rec.tran_level = L_item_master_rec.item_level then

      SQL_LIB.SET_MARK('OPEN','C_XFORM_EXISTS','item: '||L_item_master_rec.item,NULL);
      open C_XFORM_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_XFORM_EXISTS','item: '||L_item_master_rec.item,NULL);
      fetch C_XFORM_EXISTS into L_sellable_exists;
      SQL_LIB.SET_MARK('CLOSE','C_XFORM_EXISTS','item: '||L_item_master_rec.item,NULL);
      close C_XFORM_EXISTS;

      if L_sellable_exists != 'Y' then

         if INSERT_ERROR(O_error_message,
                         I_item,
                         'IT_ORDITEM_NO_SELL_DATA',
                         'Y',
                         'N') = FALSE then
            return FALSE;

         end if;
         O_approved := FALSE;
      end if;

      if ITEM_XFORM_SQL.XFORM_HEAD_DETAIL_COUNT(O_error_message,
                                                L_multi_parent_ind,
                                                L_sum_of_yield_percentages,
                                                L_item_master_rec.item) = FALSE then
         return FALSE;
      else
         if L_multi_parent_ind then
            if L_sum_of_yield_percentages <> 100 or L_sum_of_yield_percentages is NULL then
               if INSERT_ERROR(O_error_message,
                               I_item,
                               'ADJ_XFORM_YIELD',
                               'Y',
                               'N') = FALSE then
                  return FALSE;
               end if;
               O_approved := FALSE;
            end if;
         end if;
      end if;
   end if;

   if L_item_master_rec.orderable_ind = 'Y' or
      L_item_master_rec.deposit_item_type in ('E', 'A') or-- Contents item, Container item
      L_purchase_type in ('1','2')
      then

      if SUPP_ITEM_SQL.EXIST(O_error_message,
                             L_supp_exists,
                             I_item) = FALSE then
         return FALSE;
      end if;
      if L_supp_exists = FALSE then
         if INSERT_ERROR(O_error_message,
                         I_item,
                         'IT_NO_SUPP',
                         'Y',
                         'N') = FALSE then
            return FALSE;
         end if;
         O_approved := FALSE;
      end if;
   end if;

   if L_item_master_rec.sellable_ind ='Y' then

      if PM_RETAIL_API_SQL.CHECK_RETAIL_EXISTS( O_ERROR_MESSAGE,
                                                L_unit_retail_exists,
                                                I_item) = FALSE then
         return FALSE;
      end if;

      if L_unit_retail_exists = 'N' then

        if L_item_master_rec.item_parent is null and L_item_master_rec.item_grandparent is null then

            if INSERT_ERROR(O_error_message,
                            I_item,
                            'IT_NO_UNITRETAIL',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
      end if;
      end if;
   end if;

   ----------------------------------------------------------------------------
   --- SUB-TRANSACTION LEVEL
   ----------------------------------------------------------------------------
   if L_item_master_rec.tran_level < L_item_master_rec.item_level then

      --- Check for supplier records if item is not a pack, or if it is a pack
      --- and it is orderable

      if L_item_master_rec.orderable_ind = 'Y' then


         --- Check that item supplier records exist
         if SUPP_ITEM_SQL.EXIST(O_error_message,
                                L_supp_exists,
                                I_item) = FALSE then
            return FALSE;
         end if;
         ---
         if L_supp_exists = FALSE then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'IT_NO_SUPP',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;
      end if;

   else
   ----------------------------------------------------------------------------
   --- TRANSACTION LEVEL
   ----------------------------------------------------------------------------
      if L_item_master_rec.tran_level = L_item_master_rec.item_level then
         if (L_item_master_rec.deposit_item_type = 'E' and
             L_item_master_rec.container_item is NULL) then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'INV_DEP_ITM1',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;
      end if;

   ----------------------------------------------------------------------------
   --- TRANSACTION LEVEL OR ABOVE
   ----------------------------------------------------------------------------

      if L_item_master_rec.pack_ind = 'Y' then

         --- if the item is a pack item, it must contain at least one component
         SQL_LIB.SET_MARK('OPEN','C_COMPONENT_EXISTS',I_item,NULL);
         open C_COMPONENT_EXISTS;
         SQL_LIB.SET_MARK('FETCH','C_COMPONENT_EXISTS',I_item,NULL);
         fetch C_COMPONENT_EXISTS into L_component_exists;
         SQL_LIB.SET_MARK('CLOSE','C_COMPONENT_EXISTS',I_item,NULL);
         close C_COMPONENT_EXISTS;
         if L_component_exists = 'N' then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'PACKITEM_COMP_REQ',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;
         --- we can skip the component check if this is being called for a
         --- simple pack being approved or submitted along with its component
         --- item
         if I_skip_component_chk = 'N' then
          --- All components of the pack item must have approved status for pack to get approved.
            SQL_LIB.SET_MARK('OPEN','C_COMPONENT_NOT_APPROVED',I_item,NULL);
            open C_COMPONENT_NOT_APPROVED;
            SQL_LIB.SET_MARK('FETCH','C_COMPONENT_NOT_APPROVED',I_item,NULL);
            fetch C_COMPONENT_NOT_APPROVED into L_nonappr_component_exists;
            SQL_LIB.SET_MARK('CLOSE','C_COMPONENT_NOT_APPROVED',I_item,NULL);
            close C_COMPONENT_NOT_APPROVED;

            --- All components of the pack item must have atleast submitted status for pack to get submitted.
            SQL_LIB.SET_MARK('OPEN','C_COMPONENT_NOT_SUBMITTED',I_item,NULL);
            open C_COMPONENT_NOT_SUBMITTED;
            SQL_LIB.SET_MARK('FETCH','C_COMPONENT_NOT_SUBMITTED',I_item,NULL);
            fetch C_COMPONENT_NOT_SUBMITTED into L_nonsub_component_exists;
            SQL_LIB.SET_MARK('CLOSE','C_COMPONENT_NOT_SUBMITTED',I_item,NULL);
            close C_COMPONENT_NOT_SUBMITTED;

            -- The components of the pack should not be in delete pending status
            SQL_LIB.SET_MARK('OPEN','C_DELETED_COMPONENT_EXISTS',I_item,NULL);
            open C_DELETED_COMPONENT_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_DELETED_COMPONENT_EXISTS',I_item,NULL);
            fetch C_DELETED_COMPONENT_EXISTS into L_deleted_component_exists;
            SQL_LIB.SET_MARK('CLOSE','C_DELETED_COMPONENT_EXISTS',I_item,NULL);
            close C_DELETED_COMPONENT_EXISTS;

            -- The components of the inner pack of a simple pack should not be in delete pending status
            SQL_LIB.SET_MARK('OPEN','C_DELETED_INNER_PACK_EXISTS',I_item,NULL);
            open C_DELETED_INNER_PACK_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_DELETED_INNER_PACK_EXISTS',I_item,NULL);
            fetch C_DELETED_INNER_PACK_EXISTS into L_deleted_inner_exists;
            SQL_LIB.SET_MARK('CLOSE','C_DELETED_INNER_PACK_EXISTS',I_item,NULL);
            close C_DELETED_INNER_PACK_EXISTS;

            if I_new_status = 'A' and L_nonappr_component_exists = 'Y' then
               if INSERT_ERROR(O_error_message,
                               I_item,
                               'APPR_COMP_REQ',
                               'Y',
                               'N') = FALSE then
                  return FALSE;
               end if;
               O_approved := FALSE;
            elsif I_new_status = 'S' and L_nonsub_component_exists = 'Y' then
               if INSERT_ERROR(O_error_message,
                               I_item,
                               'SUB_COMP_REQ',
                               'Y',
                               'N') = FALSE then
                  return FALSE;
               end if;
               O_approved := FALSE;
            elsif I_new_status in ('A','S') then
               if L_deleted_component_exists = 'Y' then
                  if INSERT_ERROR(O_error_message,
                                  I_item,
                                  'ITEM_DEL_PEND',
                                  'Y',
                                  'N') = FALSE then
                     return FALSE;
                  end if;
                  O_approved := FALSE;
               elsif L_deleted_inner_exists = 'Y' then
                  if INSERT_ERROR(O_error_message,
                                  I_item,
                                  'SP_INNER_DEL_PEND',
                                  'Y',
                                  'N') = FALSE then
                     return FALSE;
                  end if;
                  O_approved := FALSE;
               end if;
            end if;
         end if;
      end if;
     --------------------------------------------------------------------

      if (L_item_master_rec.pack_ind = 'N' or
          (L_item_master_rec.pack_ind = 'Y' and L_item_master_rec.orderable_ind = 'Y'))
          and L_item_master_rec.sellable_ind ='Y' then

         --- all items, except non-sellable packs, must have an item_zone_price record.

         if PM_RETAIL_API_SQL.CHECK_RETAIL_EXISTS( O_ERROR_MESSAGE,
                                                   L_unit_retail_exists,
                                                   I_item) = FALSE then
            return FALSE;
         end if;

         if L_unit_retail_exists = 'N' then

         --if L_item_master_rec.item_level >= L_item_master_rec.tran_level then
         if L_item_master_rec.item_parent is null and L_item_master_rec.item_grandparent is null then

               if INSERT_ERROR(O_error_message,
                               I_item,
                               'IT_NO_UNITRETAIL',
                               'Y',
                               'N') = FALSE then
                  return FALSE;
               end if;
               O_approved := FALSE;
         end if;
         end if;
      end if;

      --------------------------------------------------------------------

       If (L_item_master_rec.orderable_ind = 'Y' or L_purchase_type in ('1', '2')
       or L_item_master_rec.deposit_item_type = 'A') then

         --- all items, except non-orderable packs, must have an item_supp_country record.
         if SUPP_ITEM_SQL.CNTRY_EXISTS_FOR_ALL_SUPPS(O_error_message,
                                                     L_all_exist,
                                                     L_supplier,
                                                     I_item) = FALSE then
            return FALSE;
         end if;

         if NOT L_all_exist then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'NO_CNTRY_FOR_ALL_SUPPS',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;

         --- all items, except non-orderable packs, must have an item_supp_manu_country record.
         SQL_LIB.SET_MARK('OPEN','C_PRIMARY_MANU_COUNTRY_EXIST', 'ITEM_SUPP_MANU_COUNTRY', I_item);

         open C_PRIMARY_MANU_COUNTRY_EXIST;
         SQL_LIB.SET_MARK('FETCH','C_PRIMARY_MANU_COUNTRY_EXIST', 'ITEM_SUPP_MANU_COUNTRY', I_item);
         fetch C_PRIMARY_MANU_COUNTRY_EXIST into L_primary_manu_country_exists;
         SQL_LIB.SET_MARK('CLOSE','C_PRIMARY_MANU_COUNTRY_EXIST', 'ITEM_SUPP_MANU_COUNTRY', I_item);
         close C_PRIMARY_MANU_COUNTRY_EXIST;
         if L_primary_manu_country_exists != 'N' then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'IT_NO_PRIM_MANU_CTRY',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;

         --- Verify supplier/country/location/bracket have a cost
         ---
         SQL_LIB.SET_MARK('OPEN','C_CHECK_BRACKET_SUPPLIER',I_item,NULL);
         open C_CHECK_BRACKET_SUPPLIER;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_BRACKET_SUPPLIER',I_item,NULL);
         fetch C_CHECK_BRACKET_SUPPLIER into L_BRACKET_NO_COST;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_BRACKET_SUPPLIER',I_item,NULL);
         close C_CHECK_BRACKET_SUPPLIER;
         if L_BRACKET_NO_COST = 'Y' then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'IT_SUPP_BRACKET_NO_COST',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;

         --- If default tax type is GTAX, verify item costing information
         ---
         if L_system_options_rec.default_tax_type = 'GTAX' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_ITEM_COST',I_item,NULL);
            open C_CHECK_ITEM_COST;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_ITEM_COST',I_item,NULL);
            fetch C_CHECK_ITEM_COST into L_item_cost_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_ITEM_COST',I_item,NULL);
            close C_CHECK_ITEM_COST;
            if L_item_cost_exists = 'N' then
               if INSERT_ERROR(O_error_message,
                               I_item,
                               'INV_ITEM_COST_HEAD',
                               'Y',
                               'N') = FALSE then
                  return FALSE;
               end if;
               O_approved := FALSE;
            end if;
         end if;
      end if;
      -------------------------------------------------------------------

      -------------------------------------------------------------------
       If (    L_item_master_rec.catch_weight_ind = 'Y'
           and L_item_master_rec.order_type       = 'V'
           and L_item_master_rec.orderable_ind    = 'Y'
           and L_item_master_rec.simple_pack_ind  = 'Y' ) then

         --- all items, except non-orderable packs, must have an item_supp_country record.
         SQL_LIB.SET_MARK('OPEN','C_NULL_TOLERANCES_EXIST',I_item,NULL);
         open C_NULL_TOLERANCES_EXIST;
         SQL_LIB.SET_MARK('FETCH','C_NULL_TOLERANCES_EXIST',I_item,NULL);
         fetch C_NULL_TOLERANCES_EXIST into L_null_tolerances_exist;
         SQL_LIB.SET_MARK('CLOSE','C_NULL_TOLERANCES_EXIST',I_item,NULL);
         close C_NULL_TOLERANCES_EXIST;
         if L_null_tolerances_exist = 'Y' then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'NULL_TOLERANCES',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;
      end if;
      -------------------------------------------------------------------

      -------------------------------------------------------------------

      if L_item_master_rec.item_level < L_item_master_rec.tran_level then

         SQL_LIB.SET_MARK('OPEN','C_ITEM_GROUPS',I_item,NULL);
         open C_ITEM_GROUPS;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_GROUPS',I_item,NULL);
         fetch C_ITEM_GROUPS into L_group_exist;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_GROUPS',I_item,NULL);
         close C_ITEM_GROUPS;
         if L_group_exist = 'N' then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'TR_ITEM_NOTIN_GRP',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;
      end if;
      --------------------------------------------------------------------


      --------------------------------------------------------------------
      SQL_LIB.SET_MARK('OPEN','C_CHECK_FOR_CC',I_item,NULL);
      open C_CHECK_FOR_CC;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_FOR_CC',I_item,NULL);
      fetch C_CHECK_FOR_CC into L_cost_change_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_FOR_CC',I_item,NULL);
      close C_CHECK_FOR_CC;
      if L_cost_change_exists = 'Y' then
         if INSERT_ERROR(O_error_message,
                         I_item,
                         'IT_NO_A_CC',
                         'Y',
                         'N') = FALSE then
            return FALSE;
         end if;
         O_approved := FALSE;
      end if;
      --------------------------------------------------------------------
      --- if item is tranlevel or above check that UDAs have been entered
      if L_item_master_rec.item_level <= L_item_master_rec.tran_level then
         if UDA_SQL.CHECK_REQD_NO_VALUE(O_error_message,
                                        L_req_no_value,
                                        I_item,
                                        L_item_master_rec.dept,
                                        L_item_master_rec.class,
                                        L_item_master_rec.subclass) = FALSE then


            return FALSE;
         end if;
         if L_req_no_value = 'Y' then
            if INSERT_ERROR(O_error_message,
                            I_item,
                            'NO_UDA_VALUES',
                            'Y',
                            'N') = FALSE then
               return FALSE;
            end if;
            O_approved := FALSE;
         end if;
      end if;
      --------------------------------------------------------------------
      --------------------------------------------------------------------

      if L_item_master_rec.pack_ind = 'N' or
        (L_item_master_rec.pack_ind = 'Y' and L_item_master_rec.pack_type = 'V') then

         if MERCH_DEFAULT_SQL.GET_REQ_INDS(O_error_message,
                                           L_loc_req_ind,
                                           L_seasons_req_ind,
                                           L_impattrib_req_ind,
                                           L_docs_req_ind,
                                           L_hts_req_ind,
                                           L_tariff_req_ind,
                                           L_exp_req_ind,
                                           L_timeline_req_ind,
                                           L_tickets_req_ind,
                                           L_image_req_ind,
                                           L_sub_tr_items_req_ind,
                                           L_dimension_req_ind,
                                           L_diffs_req_ind,
                                           L_mfg_rec_req_ind,
                                           L_pack_sz_req_ind,
                                           L_retail_lb_req_ind,
                                           L_handling_req_ind,
                                           L_handling_temp_req_ind,
                                           L_wastage_req_ind,
                                           L_comments_req_ind,
                                           L_item_master_rec.dept,
                                           L_item_master_rec.class,
                                           L_item_master_rec.subclass) = FALSE then


            return FALSE;
         end if;
        --------------------------------------------------------------------
         if L_loc_req_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_LOC',I_item,NULL);
            open C_CHECK_LOC;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_LOC',I_item,NULL);
            fetch C_CHECK_LOC into L_loc_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_LOC',I_item,NULL);
            close C_CHECK_LOC;
            if L_loc_exists = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_LOCS',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_LOCS'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_seasons_req_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_SEASONS',I_item,NULL);
            open C_CHECK_SEASONS;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_SEASONS',I_item,NULL);
            fetch C_CHECK_SEASONS into L_seasons_exist;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_SEASONS',I_item,NULL);
            close C_CHECK_SEASONS;
            if L_seasons_exist = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_SEASON',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_SEASON'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_docs_req_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_DOCS',I_item,NULL);
            open C_CHECK_DOCS;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_DOCS',I_item,NULL);
            fetch C_CHECK_DOCS into L_docs_exist;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_DOCS',I_item,NULL);
            close C_CHECK_DOCS;
            if L_docs_exist = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_DOCS',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_DOCS'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_hts_req_ind = 'Y' and L_item_master_rec.orderable_ind  = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_HTS',I_item,NULL);
            open C_CHECK_HTS;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_HTS',I_item,NULL);
            fetch C_CHECK_HTS into L_hts_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_HTS',I_item,NULL);
            close C_CHECK_HTS;
            if L_hts_exists = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_HTS',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_HTS'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_tariff_req_ind = 'Y' then
            if ITEM_ATTRIB_SQL.ITEM_ELIGIBLE_EXISTS(O_error_message,
                                                    L_tariff_exists,
                                                    I_item) = FALSE then
               return FALSE;
            end if;
            if L_tariff_exists = FALSE then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_ETT',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_ETT'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_exp_req_ind = 'Y' and L_item_master_rec.orderable_ind  = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_EXPENSE',I_item,NULL);
            open C_CHECK_EXPENSE;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_EXPENSE',I_item,NULL);
            fetch C_CHECK_EXPENSE into L_expense_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXPENSE',I_item,NULL);
            close C_CHECK_EXPENSE;
            if L_expense_exists = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_EXPENSE',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_EXPENSE'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_tickets_req_ind = 'Y' then
            if ITEM_ATTRIB_SQL.TICKET_EXISTS(O_error_message,
                                             L_tickets_exist,
                                             I_item) = FALSE then
               return FALSE;
            end if;
            if L_tickets_exist = FALSE then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_TICKETS',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_TICKETS'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_timeline_req_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_TIMELINE',I_item,NULL);
            open C_CHECK_TIMELINE;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_TIMELINE',I_item,NULL);
            fetch C_CHECK_TIMELINE into L_timeline_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_TIMELINE',I_item,NULL);
            close C_CHECK_TIMELINE;
            if L_timeline_exists = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_TIMELINE',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_TIMELINE'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_image_req_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_IMAGE',I_item,NULL);
            open C_CHECK_IMAGE;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_IMAGE',I_item,NULL);
            fetch C_CHECK_IMAGE into L_image_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_IMAGE',I_item,NULL);
            close C_CHECK_IMAGE;
            if L_image_exists = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_IMAGE',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_IMAGE'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_sub_tr_items_req_ind = 'Y' then

            if L_item_master_rec.tran_level < 3 then

               SQL_LIB.SET_MARK('OPEN','C_CHECK_SUB_TR',I_item,NULL);
               open C_CHECK_SUB_TR;
               SQL_LIB.SET_MARK('FETCH','C_CHECK_SUB_TR',I_item,NULL);
               fetch C_CHECK_SUB_TR into L_sub_tr_exists;
               SQL_LIB.SET_MARK('CLOSE','C_CHECK_SUB_TR',I_item,NULL);
               close C_CHECK_SUB_TR;
               if L_sub_tr_exists = 'N' then
                  insert into item_approval_error
                              (item,
                               error_key,
                               system_req_ind,
                               override_ind,
                               last_update_id,
                               last_update_datetime)
                   select I_item,
                          'IT_NO_SUB_TR',
                          'N',
                          'N',
                          get_user,
                          sysdate
                     from dual
                    where not exists (select 'x'
                                        from item_approval_error
                                       where item = I_item
                                         and error_key = 'IT_NO_SUB_TR'
                                         and override_ind = 'Y');
                  if SQL%FOUND then
                     O_approved := FALSE;
                  end if;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_dimension_req_ind = 'Y' then
            SQL_LIB.SET_MARK('OPEN','C_CHECK_DIMS',I_item,NULL);
            open C_CHECK_DIMS;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_DIMS',I_item,NULL);
            fetch C_CHECK_DIMS into L_dimensions_exist;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_DIMS',I_item,NULL);
            close C_CHECK_DIMS;
            if L_dimensions_exist = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_DIMS',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_DIMS'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_diffs_req_ind = 'Y' and L_item_master_rec.pack_ind != 'Y' then
            if ITEM_ATTRIB_SQL.GET_DIFFS(O_error_message,
                                         L_diff_1,
                                         L_diff_2,
                                         L_diff_3,
                                         L_diff_4,
                                         I_item) = FALSE then
               return FALSE;
            end if;
            if L_diff_1 is NULL and L_diff_2 is NULL and L_diff_3 is NULL and L_diff_4 is NULL then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_DIFFS',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_DIFFS'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------

         --------------------------------------------------------------------
         if L_mfg_rec_req_ind = 'Y' and L_item_master_rec.mfg_rec_retail is NULL and
            L_item_master_rec.sellable_ind = 'Y' then

            insert into item_approval_error
                        (item,
                         error_key,
                         system_req_ind,
                         override_ind,
                         last_update_id,
                         last_update_datetime)
             select I_item,
                    'IT_NO_MFG',
                    'N',
                    'N',
                    get_user,
                    sysdate
               from dual
              where not exists (select 'x'
                                  from item_approval_error
                                 where item = I_item
                                   and error_key = 'IT_NO_MFG'
                                   and override_ind = 'Y');
            if SQL%FOUND then
               O_approved := FALSE;
            end if;
         end if;
         --------------------------------------------------------------------

         if L_pack_sz_req_ind = 'Y' and (L_item_master_rec.package_size is NULL or L_item_master_rec.package_uom is NULL) then

            insert into item_approval_error
                        (item,
                         error_key,
                         system_req_ind,
                         override_ind,
                         last_update_id,
                         last_update_datetime)
             select I_item,
                    'IT_NO_PACKSIZE',
                    'N',
                    'N',
                    get_user,
                    sysdate
               from dual
              where not exists (select 'x'
                                  from item_approval_error
                                 where item = I_item
                                   and error_key = 'IT_NO_PACKSIZE'
                                   and override_ind = 'Y');
            if SQL%FOUND then
               O_approved := FALSE;
            end if;
         end if;
        --------------------------------------------------------------------

         if L_retail_lb_req_ind = 'Y' and
            (L_item_master_rec.retail_label_type is NULL or L_item_master_rec.retail_label_value is NULL) then

            insert into item_approval_error
                        (item,
                         error_key,
                         system_req_ind,
                         override_ind,
                         last_update_id,
                         last_update_datetime)
             select I_item,
                    'IT_NO_RET_LABEL',
                    'N',
                    'N',
                    get_user,
                    sysdate
               from dual
              where not exists (select 'x'
                                  from item_approval_error
                                 where item = I_item
                                   and error_key = 'IT_NO_RET_LABEL'
                                   and override_ind = 'Y');
            if SQL%FOUND then
               O_approved := FALSE;
            end if;
         end if;
         --------------------------------------------------------------------

         if  L_handling_req_ind = 'Y' and L_item_master_rec.handling_sensitivity is NULL then

            insert into item_approval_error
                        (item,
                         error_key,
                         system_req_ind,
                         override_ind,
                         last_update_id,
                         last_update_datetime)
             select I_item,
                    'IT_NO_HAND_SENS',
                    'N',
                    'N',
                    get_user,
                    sysdate
               from dual
              where not exists (select 'x'
                                  from item_approval_error
                                 where item = I_item
                                   and error_key = 'IT_NO_HAND_SENS'
                                   and override_ind = 'Y');
            if SQL%FOUND then
               O_approved := FALSE;
            end if;
         end if;
         --------------------------------------------------------------------

         if L_handling_temp_req_ind = 'Y' and L_item_master_rec.handling_temp is NULL then

            insert into item_approval_error
                        (item,
                         error_key,
                         system_req_ind,
                         override_ind,
                         last_update_id,
                         last_update_datetime)
             select I_item,
                    'IT_NO_HAND_TEMP',
                    'N',
                    'N',
                    get_user,
                    sysdate
               from dual
              where not exists (select 'x'
                                  from item_approval_error
                                 where item = I_item
                                   and error_key = 'IT_NO_HAND_TEMP'
                                   and override_ind = 'Y');
            if SQL%FOUND then
               O_approved := FALSE;
            end if;

         end if;
         --------------------------------------------------------------------

         if L_comments_req_ind = 'Y' and L_item_master_rec.comments is NULL then

            insert into item_approval_error
                        (item,
                         error_key,
                         system_req_ind,
                         override_ind,
                         last_update_id,
                         last_update_datetime)
             select I_item,
                    'IT_NO_COMMENTS',
                    'N',
                    'N',
                    get_user,
                    sysdate
               from dual
              where not exists (select 'x'
                                  from item_approval_error
                                 where item = I_item
                                   and error_key = 'IT_NO_COMMENTS'
                                   and override_ind = 'Y');
            if SQL%FOUND then
               O_approved := FALSE;
            end if;
         end if;
         --------------------------------------------------------------------
         if L_wastage_req_ind = 'Y' then

            if L_item_master_rec.waste_pct is NULL or
               L_item_master_rec.default_waste_pct is NULL or
               L_item_master_rec.waste_type is NULL then

               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'IT_NO_WASTE',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'IT_NO_WASTE'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
         end if;
         --------------------------------------------------------------------
      end if;
    ----------------------------------------------------------------------------
      if L_item_master_rec.pack_ind = 'Y' and L_item_master_rec.simple_pack_ind = 'N' then

            SQL_LIB.SET_MARK('OPEN','C_CHECK_INNER_PACK',I_item,NULL);
            open C_CHECK_INNER_PACK;
            SQL_LIB.SET_MARK('FETCH','C_CHECK_INNER_PACK',I_item,NULL);
            fetch C_CHECK_INNER_PACK into L_innerpack_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHECK_INNER_PACK',I_item,NULL);
            close C_CHECK_INNER_PACK;

            if L_innerpack_exists = 'Y' and L_item_master_rec.contains_inner_ind = 'N' then
               insert into item_approval_error
                           (item,
                            error_key,
                            system_req_ind,
                            override_ind,
                            last_update_id,
                            last_update_datetime)
                select I_item,
                       'CONTAINS_INNER_IND',
                       'N',
                       'N',
                       get_user,
                       sysdate
                  from dual
                 where not exists (select 'x'
                                     from item_approval_error
                                    where item = I_item
                                      and error_key = 'CONTAINS_INNER_IND'
                                      and override_ind = 'Y');
               if SQL%FOUND then
                  O_approved := FALSE;
               end if;
            end if;
       end if;
    ---------------------------------------------------------------------------------
   end if;

   -----------------------------------------------------------------------------------------
   -- Following section validates if the item being approved has any substitute related item
   -- having different UOM.
   -----------------------------------------------------------------------------------------
   SQL_LIB.SET_MARK('OPEN','C_RELATED_UOM_MISMATCH','item: '||I_item,NULL);
   open C_RELATED_UOM_MISMATCH;
   SQL_LIB.SET_MARK('FETCH','C_RELATED_UOM_MISMATCH','item: '||I_item,NULL);
   fetch C_RELATED_UOM_MISMATCH into L_uom_mismatch_exists;
   SQL_LIB.SET_MARK('CLOSE','C_RELATED_UOM_MISMATCH','item: '||I_item,NULL);
   close C_RELATED_UOM_MISMATCH;

   if L_uom_mismatch_exists = 'Y' then

      if INSERT_ERROR(O_error_message,
                      I_item,
                      'RELIM_APPR_UOM_MISMATCH',
                      'Y',
                      'N') = FALSE then
         return FALSE;

      end if;
      O_approved := FALSE;
   end if;

  ----------------------------------------------------------------------------------------------------------------
   --This code below is to call the custom functions which allow customers to customize the validations during approval of the item.
   ---------------------------------------------------------------------------------------------------------------
     if I_new_status ='A' then
        L_custom_obj_rec.function_key:='ITEM_APPROVE_UI';
     elsif I_new_status ='S' then
        L_custom_obj_rec.function_key:='ITEM_SUBMIT_UI';
     end if ;
     L_custom_obj_rec.call_seq_no:=1;
     L_custom_obj_rec.item:=I_item;


   if CALL_CUSTOM_SQL.EXEC_FUNCTION (O_error_message,
                                     L_custom_obj_rec) = FALSE then
      return FALSE;
   end if;

   if ERRORS_EXIST(O_error_message,
                   L_exists,
                   I_item) = FALSE then
      return FALSE;
   end if;

   if L_exists=TRUE then
      O_approved :=FALSE;
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      NULL;
      RETURN TRUE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END APPROVAL_CHECK;
----------------------------------------------------------------
-- This function checks that all the related siblings are also
-- submitted for submit/approve.
FUNCTION CHECK_RELATED_SIBLINGS(
    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_item_approved     IN OUT BOOLEAN,
    O_children_approved IN OUT BOOLEAN,
    I_appr_children_ind IN VARCHAR2,
    I_new_status        IN VARCHAR2,
    I_item              IN ITEM_MASTER.ITEM%TYPE)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(64) := 'ITEM_APPROVAL_SQL.CHECK_RELATED_SIBLINGS';
  CURSOR C_related_siblings
  IS
  WITH approve_list AS
    (SELECT item,
      item_parent,
      item_grandparent
    FROM item_master im,
      TABLE(Lp_approval_list) t
    WHERE t.column_value = im.item
    )
SELECT ri.item
FROM related_item_head rih,
  item_master ri,
  approve_list al,
  related_item_detail rid
WHERE rih.relationship_id = rid.relationship_id
AND rih.item              = al.item
AND rid.related_item      = ri.item
AND ((I_new_status        = 'A'
AND ri.status            <> 'A')
OR (I_new_status          = 'S'
AND ri.status NOT        IN ('S','A')))
AND cardinality(item_tbl(al.item,NVL(al.item_parent,'!'),NVL(al.item_grandparent,'@')) multiset
INTERSECT item_tbl(ri.item,NVL(ri.item_parent,'#'),NVL(ri.item_grandparent,'$'))) > 0
AND NOT EXISTS
  (SELECT 1 FROM approve_list al2 WHERE al2.item = ri.item
  );
L_related_sibling item_master.item%type;
BEGIN
  process_item_cache.delete;
  Lp_approval_list:=item_tbl();
  -- Initialize Approval list collection
  SELECT item bulk collect
  INTO Lp_approval_list
  FROM item_master im
  WHERE status <> 'A'
  AND NOT EXISTS
    (SELECT 1
    FROM daily_purge dp
    WHERE dp.table_name = 'ITEM_MASTER'
    AND key_value      IN (item,item_parent,item_grandparent)
    )
  AND ( item              = I_item
  OR (item_parent         = I_item
  AND I_appr_children_ind = 'Y')
  OR (item_grandparent    = I_item
  AND I_appr_children_ind = 'Y'));
  SQL_LIB.SET_MARK('OPEN','C_RELATED_SIBLINGS','item: '||I_item,NULL);
  OPEN C_RELATED_SIBLINGS;
  SQL_LIB.SET_MARK('FETCH','C_RELATED_SIBLINGS','item: '||I_item,NULL);
  L_related_sibling := NULL;
  FETCH C_RELATED_SIBLINGS INTO L_related_sibling;
  SQL_LIB.SET_MARK('CLOSE','C_RELATED_SIBLINGS','item: '||I_item,NULL);
  CLOSE C_RELATED_SIBLINGS;
  IF L_related_sibling                                                     IS NOT NULL THEN
    IF INSERT_ERROR(O_error_message, I_item, 'RELIM_IT_REL_NO_APPROVE', 'Y', 'N') = FALSE THEN
      RETURN FALSE;
    END IF;
    O_item_approved := FALSE;
  END IF;
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END CHECK_RELATED_SIBLINGS;
----------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_new_status        IN       ITEM_MASTER.STATUS%TYPE,
                       I_item              IN       ITEM_MASTER.ITEM%TYPE,
                       I_item_level        IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                       I_tran_level        IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                       I_catch_weight_type IN       ITEM_MASTER.CATCH_WEIGHT_TYPE%TYPE)
RETURN BOOLEAN IS

   L_table                  VARCHAR2(30)                     := 'ITEM_MASTER';
   L_program                VARCHAR2(40)                     := 'ITEM_APPROVAL_SQL.UPDATE_STATUS';
   L_dummy                  VARCHAR2(1);
   L_unit_cost              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := NULL;
   L_standard_unit_retail   ITEM_LOC.UNIT_RETAIL%TYPE        := NULL;
   L_standard_uom           ITEM_MASTER.STANDARD_UOM%TYPE    := NULL;
   L_selling_unit_retail    ITEM_LOC.UNIT_RETAIL%TYPE        := NULL;
   L_selling_uom            ITEM_LOC.SELLING_UOM%TYPE        := NULL;
   ---
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_ITEM_MASTER is
      select 'x'
        from item_master
       where item = I_item
         for update nowait;

BEGIN
   if I_new_status = 'A' AND
      NVL(I_tran_level, 1) >= NVL(I_item_level, 1) then
      -- Get base retail
      if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                              L_unit_cost,
                                              L_standard_unit_retail,
                                              L_standard_uom,
                                              L_selling_unit_retail,
                                              L_selling_uom,
                                              I_item) = FALSE then
         return FALSE;
      end if;
   end if;

   --- Lock the record
   open C_ITEM_MASTER;
   fetch C_ITEM_MASTER into L_dummy;
   close C_ITEM_MASTER;

   --- Update the status
   update item_master
      set status               = I_new_status,
          original_retail      = L_standard_unit_retail,
          catch_weight_type    = I_catch_weight_type,
          last_update_datetime = sysdate,
          last_update_id = get_user
    where item = I_item;

   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('DELRECS_REC_LOC',
                                            L_table,
                                            I_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_STATUS;
----------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_new_status      IN       ITEM_MASTER.STATUS%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(40)                     := 'ITEM_APPROVAL_SQL.UPDATE_STATUS';

BEGIN

   if UPDATE_STATUS(O_error_message,
                    I_new_status,
                    I_item,
                    NULL, -- item level
                    NULL,-- tran level
                    NULL) = FALSE then  --catch weight type
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_STATUS;
----------------------------------------------------------------
FUNCTION PROCESS_ITEM(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    I_new_status    IN ITEM_MASTER.STATUS%TYPE,
    I_single_record IN VARCHAR2,
    I_item          IN ITEM_MASTER.ITEM%TYPE)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(64) := 'ITEM_APPROVAL_SQL.PROCESS_ITEM';
BEGIN
  -- Save the record in cache instead of applying immediately.
  -- Because, submit/approve of an item may need to be reversed
  -- if corresponding related sibling failed approval.
  if (process_item_cache.exists(I_item)) and (NVL(process_item_cache(I_item).single_record, 'N') != NVL(I_single_record,'N')) then
     process_item_cache(I_item).single_record    := 'B';
  else
     process_item_cache(I_item).single_record := I_single_record;
  end if;

  process_item_cache(I_item).new_status    := I_new_status;
  process_item_cache(I_item).item          := I_item;
  process_item_cache(I_item).reject_ind    := 'N';
  RETURN TRUE;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END PROCESS_ITEM;
----------------------------------------------------------------
FUNCTION PERSIST_PROCESS_ITEM(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_new_status          IN     ITEM_MASTER.STATUS%TYPE,
                              I_single_record       IN     VARCHAR2,
                              I_item                IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_program                     VARCHAR2(64) := 'ITEM_APPROVAL_SQL.PERSIST_PROCESS_ITEM';

   L_vdate                       DATE := get_vdate;
   L_dept                        DEPS.DEPT%TYPE := NULL;
   L_class                       CLASS.CLASS%TYPE := NULL;
   L_subclass                    SUBCLASS.SUBCLASS%TYPE := NULL;
   L_temp_item                   ITEM_MASTER.ITEM%TYPE := NULL;
   L_unit_retail                 ITEM_LOC.UNIT_RETAIL%TYPE := NULL;
   L_unit_cost                   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := NULL;
   L_selling_unit_retail         ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := NULL;
   L_selling_uom                 ITEM_LOC.SELLING_UOM%TYPE := NULL;
   L_loc                         ITEM_SUPP_COUNTRY_LOC.LOC%TYPE := NULL;
   --
   L_dept_name                   DEPS.DEPT_NAME%TYPE;
   L_class_name                  CLASS.CLASS_NAME%TYPE;
   L_subclass_name               SUBCLASS.SUB_NAME%TYPE;
   L_diff_1_desc                 V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_1_type                 V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_1_id_group_ind         V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   L_diff_2                      ITEM_MASTER.DIFF_2%TYPE;
   L_diff_2_desc                 V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_2_type                 V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_2_id_group_ind         V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
   --
   L_primary_supp                ITEM_LOC.PRIMARY_SUPP%TYPE;
   L_primary_cntry               ITEM_LOC.PRIMARY_CNTRY%TYPE;
   L_item_record                 ITEM_MASTER%ROWTYPE;
   L_itemloc_TBL                 OBJ_ITEMLOC_TBL                          := OBJ_ITEMLOC_TBL();
   L_user                        COST_EVENT.USER_ID%TYPE                  DEFAULT get_user;
   L_ce_process_id               COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;

   L_item_loc_cnt                NUMBER := 0;
   L_item_loc_cnt2               NUMBER := 0;
   L_price_hist_rec              PRICE_HIST%ROWTYPE;
   L_catch_weight_uom            ITEM_MASTER.CATCH_WEIGHT_UOM%TYPE;

   L_item_tbl                    ITEM_TBL := ITEM_TBL(I_item);

   cursor C_ITEM_MASTER is
      select 'x'
        from item_master
       where item = I_item
         for update nowait;

   cursor C_LOCS is
      select il.item,
             il.loc,
             il.loc_type,
             il.local_item_desc,
             ils.unit_cost,
             il.taxable_ind,
             il.status,
             il.primary_supp,
             il.primary_cntry,
             il.uin_type,
             il.uin_label,
             il.capture_time,
             il.ext_uin_ind,
             s.store_type
        from item_loc il,
             item_loc_soh ils,
             store s
       where il.item = I_item
         and il.item = ils.item(+)
         and il.loc  = ils.loc(+)
         and il.loc = s.store(+);

  TYPE L_pos_mods_rec is RECORD
      (unit_retail_loc             ITEM_LOC.UNIT_RETAIL%TYPE,
       uom_loc                     ITEM_MASTER.STANDARD_UOM%TYPE,
       selling_unit_retail_loc     ITEM_LOC.SELLING_UNIT_RETAIL%TYPE,
       selling_uom_loc             ITEM_LOC.SELLING_UOM%TYPE,
       multi_units_loc             ITEM_LOC.MULTI_UNITS%TYPE,
       multi_unit_retail_loc       ITEM_LOC.MULTI_UNIT_RETAIL%TYPE,
       multi_selling_uom_loc       ITEM_LOC.MULTI_SELLING_UOM%TYPE,
       unit_cost_sup               ITEM_LOC_SOH.UNIT_COST%TYPE,
       unit_cost_loc               ITEM_LOC_SOH.UNIT_COST%TYPE);

   TYPE L_price_hist_arr is TABLE of PRICE_HIST%ROWTYPE index by binary_integer;
   TYPE L_item_loc_arr   is TABLE of C_LOCS%ROWTYPE index by binary_integer;
   TYPE L_pos_mods_arr   is TABLE of L_pos_mods_rec;
   L_price_hist_tbl      L_price_hist_arr;
   L_item_loc_tbl        L_item_loc_arr;
   L_pos_mods_tbl        L_pos_mods_arr := L_pos_mods_arr();
   L_item_loc_limit      NUMBER := 100;

BEGIN
   ---
   if I_new_status = 'A' then
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                          L_item_record,
                                          I_item) = FALSE then
         return FALSE;
      end if;

      if (L_item_record.simple_pack_ind   = 'Y' AND
          L_item_record.catch_weight_ind  = 'Y' AND
          L_item_record.pack_type         = 'V') then

         if CATCH_WEIGHT_SQL.DETERMINE_CATCH_WEIGHT_TYPE (O_error_message,
                                                          L_item_record.catch_weight_type,
                                                          I_item) = FALSE then
            return FALSE;
         end if;
      else
         L_item_record.catch_weight_type := NULL;
      end if;
   end if;

   if L_item_record.catch_weight_ind  = 'Y' and L_item_record.item_level <= L_item_record.tran_level then
       if ITEM_ATTRIB_SQL.GET_CATCH_WEIGHT_UOM (O_error_message,
                                                L_catch_weight_uom,
                                                I_item) = FALSE then
          return FALSE;
       end if;
   end if;

   if ((nvl(I_single_record,'N') = 'N') OR (nvl(I_single_record,'N') = 'B')) then
      if UPDATE_STATUS(O_error_message,
                       I_new_status,
                       I_item,
                       L_item_record.item_level,
                       L_item_record.tran_level,
                       L_item_record.catch_weight_type) = FALSE then

         return FALSE;
      end if;
   end if; --status update will have to be done in calling form for record locking issues
   ---
   if I_new_status = 'A' then
      ---
      if L_item_record.item_level <= L_item_record.tran_level then
         -- if the item is at or above the transaction level, then get the retail for the item
         L_temp_item := I_item;
      else
         -- if the item is below the transaction level, then get the retail for its parent
         L_temp_item := L_item_record.item_parent;
      end if;
      if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                              L_unit_cost,
                                              L_unit_retail,
                                              L_item_record.standard_uom,
                                              L_selling_unit_retail,
                                              L_selling_uom,
                                              L_temp_item) = FALSE then
         return FALSE;
      end if;
      ---
      if L_item_record.item_level = L_item_record.tran_level then
         insert into repl_item_loc_updates(item,
                                           change_type)
                                   (select distinct item,
                                                    'IM'
                                      from repl_item_loc
                                     where item = I_item
                                       and rownum = 1);
      end if;
      ---
      if L_item_record.item_level <= L_item_record.tran_level then
         ---
         insert into price_hist(tran_type,
                                reason,
                                event,
                                item,
                                loc,
                                loc_type,
                                unit_cost,
                                unit_retail,
                                selling_unit_retail,
                                selling_uom,
                                action_date,
                                multi_units,
                                multi_unit_retail,
                                multi_selling_uom)
                        values (0, --tran_type
                                0, --reason
                                NULL, --event
                                I_item,
                                0, --loc
                                NULL, --loc_type
                                L_unit_cost,
                                L_unit_retail,
                                L_selling_unit_retail,
                                L_selling_uom,
                                L_vdate, --action_date
                                NULL, --multi_units
                                NULL, --multi_unit_retail
                                NULL --multi_selling_uom
                                );

         OPEN C_LOCS;
         LOOP
            FETCH C_LOCS BULK COLLECT INTO L_item_loc_tbl LIMIT L_item_loc_limit;
            L_item_loc_cnt := 0;
            EXIT WHEN L_item_loc_tbl.COUNT = 0;
               FOR k in L_item_loc_tbl.FIRST..L_item_loc_tbl.LAST LOOP
                  if L_item_loc_tbl(k).loc_type = 'S' and L_item_loc_tbl(k).store_type in ('C','F') then
                     L_item_loc_cnt := L_item_loc_cnt + 1;
                  end if;
               END LOOP;
               ---
               BEGIN
                  ---
                  FOR j in L_item_loc_tbl.FIRST..L_item_loc_tbl.LAST LOOP
                     L_item_loc_cnt2 := L_item_loc_cnt2 + 1;
                     L_pos_mods_tbl.extend(1);
                     if L_item_record.sellable_ind = 'Y' then

                        if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                                         L_pos_mods_tbl(j).unit_retail_loc,
                                                         L_pos_mods_tbl(j).uom_loc,
                                                         L_pos_mods_tbl(j).selling_unit_retail_loc,
                                                         L_pos_mods_tbl(j).selling_uom_loc,
                                                         L_pos_mods_tbl(j).multi_units_loc,
                                                         L_pos_mods_tbl(j).multi_unit_retail_loc,
                                                         L_pos_mods_tbl(j).multi_selling_uom_loc,
                                                         L_item_loc_tbl(j).item,
                                                         L_item_loc_tbl(j).loc_type,
                                                         L_item_loc_tbl(j).loc) = FALSE then
                           return FALSE;
                        end if;
                     end if; -- end sellable_ind = 'Y'
                     ---
                     /* Cost of Packs/non-tran level Items is not on item_loc_soh, so fetch cost from supplier tables */
                     if ((L_item_record.pack_ind = 'Y' and L_item_record.orderable_ind = 'Y') or L_item_record.item_level < L_item_record.tran_level) then
                        L_pos_mods_tbl(j).unit_cost_sup := NVL(L_pos_mods_tbl(j).unit_cost_sup,0);
                        L_pos_mods_tbl(j).unit_cost_loc := NVL(L_pos_mods_tbl(j).unit_cost_loc,0);
                        if L_pos_mods_tbl(j).unit_cost_sup is NULL or L_pos_mods_tbl(j).unit_cost_sup = 0 then
                           if SUPP_ITEM_SQL.GET_COST(O_error_message,
                                                     L_pos_mods_tbl(j).unit_cost_sup,
                                                     L_item_loc_tbl(j).item,
                                                     L_item_loc_tbl(j).primary_supp,
                                                     L_item_loc_tbl(j).primary_cntry,
                                                     L_item_loc_tbl(j).loc) = FALSE then
                              return FALSE;
                           end if;
                        end if;
                        ---
                        if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                            L_item_loc_tbl(j).primary_supp,
                                                            'V',
                                                            NULL,
                                                            L_item_loc_tbl(j).loc,
                                                            L_item_loc_tbl(j).loc_type,
                                                            NULL,
                                                            L_pos_mods_tbl(j).unit_cost_sup,
                                                            L_pos_mods_tbl(j).unit_cost_loc,
                                                            'C',
                                                            NULL,
                                                            NULL) = FALSE then
                           return FALSE;
                        end if;
                     else
                        L_pos_mods_tbl(j).unit_cost_loc := L_item_loc_tbl(j).unit_cost;
                     end if;

                     L_price_hist_rec.tran_type           := 0;
                     L_price_hist_rec.reason              := 0;
                     L_price_hist_rec.event               := NULL;
                     L_price_hist_rec.item                := L_item_loc_tbl(j).item;
                     L_price_hist_rec.loc                 := L_item_loc_tbl(j).loc;
                     L_price_hist_rec.loc_type            := L_item_loc_tbl(j).loc_type;
                     L_price_hist_rec.unit_cost           := L_pos_mods_tbl(j).unit_cost_loc;
                     L_price_hist_rec.unit_retail         := L_pos_mods_tbl(j).unit_retail_loc;
                     L_price_hist_rec.selling_unit_retail := L_pos_mods_tbl(j).selling_unit_retail_loc;
                     L_price_hist_rec.selling_uom         := L_pos_mods_tbl(j).selling_uom_loc;
                     L_price_hist_rec.action_date         := L_vdate;
                     L_price_hist_rec.multi_units         := L_pos_mods_tbl(j).multi_units_loc;
                     L_price_hist_rec.multi_unit_retail   := L_pos_mods_tbl(j).multi_unit_retail_loc;
                     L_price_hist_rec.multi_selling_uom   := L_pos_mods_tbl(j).multi_selling_uom_loc;

                     L_price_hist_tbl(L_item_loc_cnt2)    := L_price_hist_rec;

                     if L_item_record.item_level = L_item_record.tran_level then
                        L_itemloc_TBL.EXTEND;
                        L_itemloc_TBL(L_itemloc_TBL.COUNT) := OBJ_ITEMLOC_REC(L_item_loc_tbl(j).item, L_item_loc_tbl(j).loc);
                     end if;
                  END LOOP;
                  ---
               END;
         end loop;
         ---
         if L_price_hist_tbl.count > 0 then
            FORALL i in L_price_hist_tbl.FIRST..L_price_hist_tbl.LAST
               insert into price_hist(tran_type,
                                      reason,
                                      event,
                                      item,
                                      loc,
                                      loc_type,
                                      unit_cost,
                                      unit_retail,
                                      selling_unit_retail,
                                      selling_uom,
                                      action_date,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_selling_uom)
                              values (L_price_hist_tbl(i).tran_type,
                                      L_price_hist_tbl(i).reason,
                                      L_price_hist_tbl(i).event,
                                      L_price_hist_tbl(i).item,
                                      L_price_hist_tbl(i).loc,
                                      L_price_hist_tbl(i).loc_type,
                                      L_price_hist_tbl(i).unit_cost,
                                      L_price_hist_tbl(i).unit_retail,
                                      L_price_hist_tbl(i).selling_unit_retail,
                                      L_price_hist_tbl(i).selling_uom,
                                      L_price_hist_tbl(i).action_date,
                                      L_price_hist_tbl(i).multi_units,
                                      L_price_hist_tbl(i).multi_unit_retail,
                                      L_price_hist_tbl(i).multi_selling_uom
                                      );
         end if;
         ---
         if L_itemloc_TBL.COUNT > 0 then
            if FUTURE_COST_EVENT_SQL.ADD_NIL(O_error_message,
                                             L_ce_process_id,
                                             L_itemloc_TBL,
                                             L_user) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if;
      ---

      --call ALC logic to populate item_master.alc_item_type.
      if SET_ALC_ITEM_TYPE(O_error_message,
                           L_item_tbl) = FALSE then
         return FALSE;
      end if;
      ---
      delete from item_approval_error
            where item = I_item;

   else
      --------------------------------------
      --- I_new_status = 'S'
      --------------------------------------
      delete from item_approval_error
            where item = I_item
              and override_ind = 'N';
   end if;
   ---
   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END PERSIST_PROCESS_ITEM;
----------------------------------------------------------------
-- This function processes all the status change records from
-- the cache. If related sibling of an item failed approval then
-- that item will also be rejected.
-- And if item being rejected happens to have child items then
-- child items will also be rejected.
FUNCTION FLUSH_PROCESS_ITEM(
    O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_item_approved     IN OUT BOOLEAN,
    O_children_approved IN OUT BOOLEAN,
    I_item              IN VARCHAR2,
    I_new_status        IN VARCHAR2)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(64) := 'ITEM_APPROVAL_SQL.FLUSH_PROCESS_ITEM';
  L_idx     VARCHAR2(25);
  L_error_key RTK_ERRORS.RTK_KEY%TYPE;
BEGIN
  FOR rec IN
  ( WITH approve_list AS
  (SELECT item,
    item_parent,
    item_grandparent
  FROM item_master im,
    TABLE(Lp_approval_list) t
  WHERE t.column_value = im.item
  )
SELECT al.item AS main_item,
  ri.item      AS rel_item
FROM related_item_head rih,
  item_master ri,
  approve_list al,
  related_item_detail rid
WHERE rih.relationship_id = rid.relationship_id
AND rih.item              = al.item
AND rid.related_item      = ri.item
AND ((I_new_status        = 'A'
AND ri.status            <> 'A')
OR (I_new_status          = 'S'
AND ri.status NOT        IN ('S','A')))
AND cardinality(item_tbl(al.item,NVL(al.item_parent,'!'),NVL(al.item_grandparent,'@')) multiset
INTERSECT item_tbl(ri.item,NVL(ri.item_parent,'#'),NVL(ri.item_grandparent,'$'))) > 0
AND EXISTS
  (SELECT 1 FROM approve_list al2 WHERE al2.item = ri.item
  )
  )
  LOOP
    -- if main-item has passed approval but related item has not
    IF process_item_cache.exists(rec.main_item) AND (NOT process_item_cache.exists(rec.rel_item)) THEN
      -- main item cannot be approved until
      -- related item is also approved
      process_item_cache(rec.main_item).reject_ind := 'Y';
      IF rec.main_item                              = I_item THEN
        O_item_approved                            := false;
      ELSE
        O_children_approved := false;
      END IF;
      IF INSERT_ERROR(O_error_message, rec.main_item, 'RELIM_IT_REL_NO_APPROVE', 'Y', 'N') = FALSE THEN
        RETURN FALSE;
      END IF;
      -- since main item is getting rejected hence
      -- reject any children also from getting approved as
      -- children cannot be approved until parent is approved
      FOR chld IN
      (SELECT item
      FROM item_master
      WHERE item_parent   = rec.main_item
      OR item_grandparent = rec.main_item
      )
      LOOP
        IF process_item_cache.exists(chld.item) THEN
          process_item_cache(chld.item).reject_ind := 'Y';
          IF chld.item                              = I_item THEN
            O_item_approved                        := false;
          ELSE
            O_children_approved := false;
          END IF;
          IF I_new_status = 'A'
          then
              L_error_key := 'IT_PARENT_NOT_A';
          else
              L_error_key := 'IT_PARENT_NOT_S';
          end if;
          IF INSERT_ERROR(O_error_message, chld.item, L_error_key, 'Y', 'N') = FALSE THEN
            RETURN FALSE;
          END IF;
        END IF;
      END LOOP;
    END IF;
  END LOOP;
  L_idx        := process_item_cache.first;
  WHILE (L_idx IS NOT NULL)
  LOOP
    -- Apply status change transactions for non-rejected records.
    IF process_item_cache(L_idx).reject_ind                                                                                                                  = 'N' THEN
      IF PERSIST_PROCESS_ITEM(O_error_message ,process_item_cache(L_idx).new_status ,process_item_cache(L_idx).single_record ,process_item_cache(L_idx).item)=FALSE THEN
        RETURN false;
      END IF;
    END IF;
    L_idx := process_item_cache.next(L_idx);
  END LOOP;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := sql_lib.create_msg('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END FLUSH_PROCESS_ITEM;
------------------------------------------------------------------------------------------
FUNCTION ERRORS_EXIST(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exist                IN OUT BOOLEAN,
                      I_item                 IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_program       VARCHAR2(64) := 'ITEM_APPROVAL_SQL.ERRORS_EXIST';
   L_error_exists  VARCHAR2(1)  := 'N';

   cursor C_ERRORS is
      select /*+ INDEX(iae) */  'Y'
        from item_approval_error iae
           , item_master         im
       where im.item = iae.item
         and (I_item = im.item
              or I_item = im.item_parent
              or I_item = im.item_grandparent)
         and override_ind = 'N'
      and  rownum = 1;

BEGIN
   if I_item is NULL then
      O_error_message  := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   O_exist := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ERRORS',I_item,NULL);
   open C_ERRORS;
   SQL_LIB.SET_MARK('FETCH','C_ERRORS',I_item,NULL);
   fetch C_ERRORS into L_error_exists;
   SQL_LIB.SET_MARK('CLOSE','C_ERRORS',I_item,NULL);
   close C_ERRORS;
   ---
   if L_error_exists = 'Y' then
      O_exist := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END ERRORS_EXIST;
----------------------------------------------------------------
FUNCTION WORKSHEET(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_children_worksheet  IN OUT  BOOLEAN,
                   I_item                IN      ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN is

   L_dummy          VARCHAR2(1);
   L_program        VARCHAR2(64) := 'ITEM_APPROVAL_SQL.WORKSHEET';
   L_exists_delete  BOOLEAN;

   cursor C_ITEM_MASTER is
      select 'x'
        from item_master
       where (   I_item = item_parent
              or I_item = item_grandparent)
         and status = 'S'
         for update nowait;

   cursor C_NOT_IN_WORKSHEET is
      select 'x'
        from item_master
       where (   I_item = item_parent
              or I_item = item_grandparent)
         and status in ('A','S');

     cursor C_SUBMITTED_PACK is
      select item as pack_no
        from item_master
       where item in (select pack_no
                        from packitem
                       where item = I_item)
         and status = 'S';

BEGIN
   --- Validate input
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

  --- Check if a pack in submitted status exists with this item as its component
   for rec in C_SUBMITTED_PACK loop
      if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                       L_exists_delete,
                                                       rec.pack_no,
                                                       'ITEM_MASTER')= FALSE then
         return FALSE;
      end if;

      --- If a pack with this item as its component exists in submitted status
      --- and is not on daily purge, L_exists_delete will be false.
      if L_exists_delete = FALSE then
         O_children_worksheet := FALSE;
         return TRUE;
      end  if;
   end loop;

   --- Lock records for updating
   open C_ITEM_MASTER;
   fetch C_ITEM_MASTER into L_dummy;
   close C_ITEM_MASTER;

   --- Set to worksheet status
   update item_master
      set status = 'W',
          last_update_datetime = sysdate,
          last_update_id = get_user
    where item in (select item
                     from item_master
                    where (   I_item = item_parent
                           or I_item = item_grandparent)
                      and status = 'S');

   --- Delete approval errors
   delete from item_approval_error
         where item in (select item
                          from item_master
                         where (   I_item = item
                                or I_item = item_parent
                                or I_item = item_grandparent));

   --- Check if any children/grandchildren are not in worksheet status
   open C_NOT_IN_WORKSHEET;
   fetch C_NOT_IN_WORKSHEET into L_dummy;
   if C_NOT_IN_WORKSHEET%FOUND then
      O_children_worksheet := FALSE;
   else
      O_children_worksheet := TRUE;
   end if;
   close C_NOT_IN_WORKSHEET;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END WORKSHEET;
----------------------------------------------------------------
FUNCTION APPROVE(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_item_approved      IN OUT  BOOLEAN,
                 O_children_approved  IN OUT  BOOLEAN,
                 I_appr_children_ind  IN      VARCHAR2,
                 I_item               IN      ITEM_MASTER.ITEM%TYPE)
return BOOLEAN is

   L_program                VARCHAR2(64)            := 'ITEM_APPROVAL_SQL.APPROVE';
   L_original_item_status   ITEM_MASTER.STATUS%TYPE := NULL;
   L_item_status            ITEM_MASTER.STATUS%TYPE := NULL;
   L_child_status           ITEM_MASTER.STATUS%TYPE := NULL;
   L_tran_level             ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_item_level             ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_grandchilds_parent     ITEM_MASTER.ITEM%TYPE;
   L_grandchild             ITEM_MASTER.ITEM%TYPE;
   L_child                  ITEM_MASTER.ITEM%TYPE;
   L_pack_approved          BOOLEAN;
   L_all_packs_approved     BOOLEAN;
   L_child_approved         BOOLEAN;
   L_grandchild_approved    BOOLEAN;
   L_exists                 BOOLEAN;
   L_exists_delete          BOOLEAN;
   L_exists_grand_child     BOOLEAN;

   cursor C_ITEM_INFO is
      select status, item_level, tran_level
        from item_master
       where I_item = item;

   cursor C_CHILDREN is
      select item
           , tran_level
           , item_level
           , status
        from item_master
       where item_parent = I_item;

   cursor C_GRANDCHILDREN is
      select item
           , tran_level
           , item_level
           , status
        from item_master
       where item_grandparent = I_item
         and item_parent      = L_grandchilds_parent;

   cursor C_PACK(L_item ITEM_MASTER.ITEM%TYPE) is
     select distinct pack_no
       from packitem
      where item = L_item;

   cursor C_ITEM_PACKS(L_packitem ITEM_MASTER.ITEM%TYPE) is
       select item pack_no
         from item_master im
        where (im.item = L_packitem or
               im.item_parent=L_packitem or
               im.item_grandparent=L_packitem)
          and im.status in ('W','S')
          and im.simple_pack_ind='Y';

BEGIN
   --- Check for invalid input parameters
   if I_appr_children_ind is NULL or I_item is NULL then
      O_error_message  := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;

   --- Initialize output parameters
   O_item_approved     := TRUE;
   O_children_approved := TRUE;

   --- Get status of the item passed in
   open C_ITEM_INFO;
   fetch C_ITEM_INFO into L_original_item_status, L_item_level, L_tran_level;
   close C_ITEM_INFO;

   --- Skip the approval and processing logic if this item is already approved
   if L_original_item_status = 'A' then
      O_item_approved := TRUE;
   else
      --- Do approval_check
      if APPROVAL_CHECK(O_error_message,
                        O_item_approved,
                        'N',              --- I_skip_component_chk
                        NULL,             --- I_parent_status
                        'A',              --- I_new_status
                        I_item) = FALSE then
         return FALSE;
      end if;
   end if;
   IF CHECK_RELATED_SIBLINGS(O_error_message,O_item_approved,O_children_approved,I_appr_children_ind,'A',I_item)=false THEN
      RETURN false;
   END IF;
   --- Set variable based on success of approval check and processing item
   if O_item_approved = TRUE then
      L_item_status := 'A';
   else
      L_item_status := 'S';
   end if;

   if L_item_level = L_tran_level then
      L_all_packs_approved := TRUE;
      for rec in c_pack(I_item) loop
         --- Check if pack item is on daily purge
         if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                          L_exists_delete,
                                                          rec.pack_no,
                                                          'ITEM_MASTER')= FALSE then
            return FALSE;
         end if;
         --- Skip the approval and processing logic if this pack item is already approved or has been set for deletion
         if L_exists_delete = FALSE then
            for pack in C_ITEM_PACKS(rec.pack_no) LOOP
               if NOT APPROVAL_CHECK(O_error_message,
                                     L_pack_approved,
                                     'Y',              --- I_skip_component_chk
                                     L_item_status,
                                     'A',
                                     pack.pack_no) then
                  return FALSE;
               end if;
               if NOT L_pack_approved then
                  L_all_packs_approved := FALSE;
               end if;
            end LOOP;
            if NOT L_all_packs_approved then
               if NOT INSERT_ERROR(O_error_message,
                                   I_item,
                                   'NOT_APP_PACK',
                                   'Y',
                                   'N') then
                  return FALSE;
               end if;
               O_item_approved := FALSE;
               L_item_status := L_original_item_status;
            elsif L_item_status = 'A' then
               for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                  if PROCESS_ITEM(O_error_message,
                                  'A',
                                  NULL, -- I_single_record, updates item_master
                                  pack.pack_no) = FALSE then
                     return FALSE;
                  end if;
               end LOOP;
            end if;
         end if;
      end loop; -- end of c_pack(I_item) loop
   end if;

   --- If input parameter is Y, loop for children.  This should be done if I_item passed or failed so that all
   --- approval errors can be written right away.
   if I_appr_children_ind = 'Y' then

      for child in C_CHILDREN loop

         --- Check if item or its parent/grandparent is on daily purge
         if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                          L_exists,
                                                          child.item,
                                                          'ITEM_MASTER')= FALSE then
            return FALSE;
         end if;

         --- Skip the approval and processing logic if this child is already approved or has been set for deletion
         if child.status = 'A' or L_exists = TRUE then
            L_child_approved := TRUE;
         else
            --- Do approval_check;
            if APPROVAL_CHECK(O_error_message,
                              L_child_approved,
                              'N',              --- I_skip_component_chk
                              L_item_status,    --- I_parent_status
                              'A',              --- I_new_status
                              child.item) = FALSE then
               return FALSE;
            end if;

            --- If any child fails to get updated then set output parameter to FALSE
            if L_child_approved = FALSE then
               O_children_approved := FALSE;
            end if;
            ---
         end if; -- if child.status = 'A'

         --- Set variable based on success of approval check and processing child, also whether item is set for deletion
         if L_child_approved = TRUE and L_exists = FALSE then
            L_child_status := 'A';
         else
            L_child_status := 'S';
         end if;

         if L_item_level + 1 = L_tran_level then
            L_all_packs_approved := TRUE;
            L_child := child.item;
            for rec in c_pack(L_child) loop
               --- Check if pack item is on daily purge
               if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                                L_exists_delete,
                                                                rec.pack_no,
                                                                'ITEM_MASTER')= FALSE then
                  return FALSE;
               end if;
               --- Skip the approval and processing logic if this pack item is already approved or has been set for deletion
               if L_exists_delete = FALSE then
                  for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                     if NOT APPROVAL_CHECK(O_error_message,
                                           L_pack_approved,
                                           'Y',              --- I_skip_component_chk
                                           L_item_status,
                                           'A',
                                           pack.pack_no) then
                        return FALSE;
                     end if;
                     if NOT L_pack_approved then
                        L_all_packs_approved := FALSE;
                     end if;
                  end LOOP;
                  if NOT L_all_packs_approved then
                     if NOT INSERT_ERROR(O_error_message,
                                         child.item,
                                         'NOT_APP_PACK',
                                         'Y',
                                         'N') then
                        return FALSE;
                     end if;
                     L_child_approved := FALSE;
                     L_child_status := 'S';
                  elsif L_child_status = 'A' then
                     for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                        if PROCESS_ITEM(O_error_message,
                                        'A',
                                        NULL, -- I_single_record, updates item_master
                                        pack.pack_no) = FALSE then
                           return FALSE;
                        end if;
                     end LOOP;
                  end if;
               end if;
            end loop; -- end of c_pack(L_child) loop
         end if;

         --- Loop through grandchildren for each child as it is processed.
         if L_exists = FALSE then    --only process grandchildren if the parent isn't on the daily purge (deletion) list

            L_grandchilds_parent := child.item;
            for grandchild in C_GRANDCHILDREN loop
               if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                                L_exists_grand_child,
                                                                grandchild.item,
                                                                'ITEM_MASTER')= FALSE then
                  return FALSE;
               end if;
               --- Skip the approval and processing logic if this grandchild is already approved or has been set for deletion
               if grandchild.status = 'A' or L_exists_grand_child = TRUE then
                  L_grandchild_approved := TRUE;
               else
                  --- Do approval_check
                  if APPROVAL_CHECK(O_error_message,
                                    L_grandchild_approved,
                                    'N',              --- I_skip_component_chk
                                    L_child_status,   --- I_parent_status
                                    'A',              --- I_new_status
                                    grandchild.item) = FALSE then
                     return FALSE;
                  end if;

                  if L_item_level + 2 = L_tran_level then
                     L_all_packs_approved := TRUE;
                     L_grandchild := grandchild.item;
                     for rec in c_pack(L_grandchild) loop
                        --- Check if pack item is on daily purge
                        if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                                         L_exists_delete,
                                                                         rec.pack_no,
                                                                         'ITEM_MASTER')= FALSE then
                           return FALSE;
                        end if;
                        --- Skip the approval and processing logic if this pack item is already approved or has been set for deletion
                        if L_exists_delete = FALSE then
                           for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                              if NOT APPROVAL_CHECK(O_error_message,
                                                    L_pack_approved,
                                                    'N',      --- I_skip_component_chk
                                                    grandchild.status,
                                                    'A',
                                                    pack.pack_no) then
                                 return FALSE;
                              end if;
                              if NOT L_pack_approved then
                                 L_all_packs_approved := FALSE;
                              end if;
                           end LOOP;
                           if NOT L_all_packs_approved then
                              if NOT INSERT_ERROR(O_error_message,
                                                  grandchild.item,
                                                  'NOT_APP_PACK',
                                                  'Y',
                                                  'N') then
                                 return FALSE;
                              end if;
                              L_grandchild_approved := FALSE;
                           elsif L_grandchild_approved then
                              for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                                 if PROCESS_ITEM(O_error_message,
                                                 'S',
                                                 NULL, -- I_single_record, updates item_master
                                                 pack.pack_no) = FALSE then
                                    return FALSE;
                                 end if;
                              end LOOP;
                           end if;
                        end if;
                     end loop; -- end of c_pack(L_grandchild) loop
                  end if;

                  --- If grandchild passed approval check then update grandchild
                  if L_grandchild_approved = TRUE then
                     if PROCESS_ITEM(O_error_message,
                                     'A',
                                     NULL, -- I_single_record, updates item_master
                                     grandchild.item) = FALSE then
                        return FALSE;
                     end if;
                     if grandchild.item_level > grandchild.tran_level and L_child_status = 'A' then
                        if PROCESS_ITEM(O_error_message,
                                        'A',
                                        'Y',
                                        grandchild.item) = FALSE then
                           return FALSE;
                        end if;
                     end if;
                  else
                  --- If any grandchild fails to get updated then set output parameter to FALSE
                     O_children_approved := FALSE;
                  end if;

               end if;  -- if grandchild.status = 'A'
               ---
            end loop;  -- end C_GRANDCHILDREN loop
         end if; ---- child on daily purge list and will be deleted.

         --- If child passed approval check, is not already approved, and is not set for deletion then update child
         if L_child_approved = TRUE and child.status != 'A' and L_exists = FALSE then
            if PROCESS_ITEM(O_error_message,
                            'A',
                            NULL, -- I_single_record, updates item_master
                            child.item) = FALSE then
               return FALSE;
            end if;

            if child.item_level > child.tran_level and L_original_item_status = 'A' then
               if PROCESS_ITEM(O_error_message,
                               'A',
                               'Y', -- I_single_record, doesn't update item_master
                               child.item) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;
         ---
      end loop;  -- end C_CHILDREN loop
      ---
   end if;

   --- If item passed approval check and it is not already approved, then update item

   if O_item_approved = TRUE and L_original_item_status != 'A' then
      if PROCESS_ITEM(O_error_message,
                      'A',
                      'Y', -- I_single_record, doesn't update item_master
                      I_item)= FALSE then
         return FALSE;
      end if;
   end if;
   if FLUSH_PROCESS_ITEM(O_error_message, O_item_approved, O_children_approved,I_Item,'A')=false
   then
       return false;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      RETURN FALSE;
END APPROVE;
----------------------------------------------------------------
FUNCTION SUBMIT(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                O_item_submitted      IN OUT  BOOLEAN,
                O_children_submitted  IN OUT  BOOLEAN,
                I_sub_children_ind    IN      VARCHAR2,
                I_item                IN      ITEM_MASTER.ITEM%TYPE)
return BOOLEAN is
   L_program                VARCHAR2(64)            := 'ITEM_APPROVAL_SQL.SUBMIT';
   L_item_status            ITEM_MASTER.STATUS%TYPE := NULL;
   L_original_item_status   ITEM_MASTER.STATUS%TYPE := NULL;
   L_child_status           ITEM_MASTER.STATUS%TYPE := NULL;
   L_tran_level             ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_item_level             ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_grandchilds_parent     ITEM_MASTER.ITEM%TYPE;
   L_grandchild             ITEM_MASTER.ITEM%TYPE;
   L_child                  ITEM_MASTER.ITEM%TYPE;
   L_child_submitted        BOOLEAN;
   L_grandchild_submitted   BOOLEAN;
   L_pack_submitted         BOOLEAN;
   L_all_packs_submitted    BOOLEAN;
   L_exists                 BOOLEAN;
   L_exists_delete          BOOLEAN;
   L_exists_grand_child     BOOLEAN;

   cursor C_ITEM_INFO is
      select status, item_level, tran_level
        from item_master
       where I_item = item;

   cursor C_CHILDREN is
      select item
           , tran_level
           , item_level
           , status
        from item_master
       where item_parent = I_item;

   cursor C_GRANDCHILDREN is
      select item
           , tran_level
           , item_level
           , status
        from item_master
       where item_grandparent = I_item
         and item_parent      = L_grandchilds_parent;

   cursor C_PACK(L_item ITEM_MASTER.ITEM%TYPE)  is
     select distinct pack_no
       from packitem
      where item = L_item;

   cursor C_ITEM_PACKS(L_packitem ITEM_MASTER.ITEM%TYPE) is
       select item pack_no
         from item_master im
        where (im.item = L_packitem or
               im.item_parent=L_packitem or
               im.item_grandparent=L_packitem)
           and im.status='W'
          and im.simple_pack_ind='Y';

BEGIN
   --- Check for invalid input parameters

   if I_sub_children_ind is NULL or I_item is NULL then
      O_error_message  := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;

   --- Initialize output parameters
   O_item_submitted     := FALSE;
   O_children_submitted := TRUE;

   --- Get status of the item passed in
   open C_ITEM_INFO;
   fetch C_ITEM_INFO into L_original_item_status, L_item_level, L_tran_level;
   close C_ITEM_INFO;

   --- Skip the approval check if this item is already submitted or approved
   if L_original_item_status != 'W' then
      O_item_submitted := TRUE;
   else
      --- Do approval check
      if APPROVAL_CHECK(O_error_message,
                        O_item_submitted,
                        'N',             --- I_skip_component_chk
                        NULL,            --- I_parent_status
                        'S',             --- I_new_status
                        I_item) = FALSE then
         return FALSE;
      end if;
   end if;
   IF CHECK_RELATED_SIBLINGS(O_error_message,O_item_submitted,O_children_submitted,I_sub_children_ind,'A',I_item)=false THEN
      RETURN false;
   END IF;
   --- Set variable based on success of approval check
   if O_item_submitted = TRUE then
      L_item_status := 'S';
   else
      L_item_status := 'W';
   end if;

   if L_item_level = L_tran_level then
      L_all_packs_submitted := TRUE;
      for rec in c_pack(I_item) loop
         --- Check if pack item is on daily purge
         if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                          L_exists_delete,
                                                          rec.pack_no,
                                                          'ITEM_MASTER')= FALSE then
            return FALSE;
         end if;
         --- Skip the approval and processing logic if this pack item is already approved or has been set for deletion
         if L_exists_delete = FALSE then
            for pack in C_ITEM_PACKS(rec.pack_no) LOOP
               if NOT APPROVAL_CHECK(O_error_message,
                                     L_pack_submitted,
                                     'Y',             --- I_skip_component_chk
                                     L_item_status,
                                     'S',
                                     pack.pack_no) then
                  return FALSE;
               end if;
               if NOT L_pack_submitted then
                  L_all_packs_submitted := FALSE;
               end if;
            end LOOP;
            if NOT L_all_packs_submitted then
               if NOT INSERT_ERROR(O_error_message,
                                   I_item,
                                   'NOT_SUB_PACK',
                                   'Y',
                                   'N') then
                  return FALSE;
               end if;
               O_item_submitted := FALSE;
               L_item_status := L_original_item_status;
            elsif L_item_status = 'S' then
               for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                  if PROCESS_ITEM(O_error_message,
                                  'S',
                                  NULL, -- I_single_record, updates item_master
                                  pack.pack_no) = FALSE then
                     return FALSE;
                  end if;
               end LOOP;
            end if;
         end if;
      end loop; -- end of c_pack(I_item) loop
   end if;

   --- If input parameter is Y, loop for children.
   --- Only if parent item is submitted successfully then the children items are checked for the submittal errors.

   if (O_item_submitted = TRUE and I_sub_children_ind = 'Y') then

      for child in C_CHILDREN loop

         --- Check if item or its parent/grandparent is on daily purge
         if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                          L_exists,
                                                          child.item,
                                                          'ITEM_MASTER')= FALSE then
            return FALSE;
         end if;

         --- Skip the approval check if this child is already submitted or approved or is set to be deleted
         if child.status != 'W' or L_exists = TRUE then
            L_child_submitted := TRUE;
         else
            --- Do approval_check
            if APPROVAL_CHECK(O_error_message,
                              L_child_submitted,
                              'N',               --- I_skip_component_chk
                              L_item_status,     --- I_parent_status
                              'S',               --- I_new_status
                              child.item) = FALSE then
               return FALSE;
            end if;

            --- If any child fails to get updated then set output parameter to FALSE
            if L_child_submitted = FALSE then
               O_children_submitted := FALSE;
            end if;
         end if;

         --- Set variable based on success of approval check and processing child and whether the item will be deleted
         if L_child_submitted = TRUE and L_exists = FALSE then
             L_child_status := 'S';
         else
            L_child_status := 'W';
         end if;

         if L_item_level + 1 = L_tran_level then
            L_all_packs_submitted := TRUE;
            L_child := child.item;
            for rec in c_pack(L_child) loop
               --- Check if pack item is on daily purge
               if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                                L_exists_delete,
                                                                rec.pack_no,
                                                                'ITEM_MASTER')= FALSE then
                  return FALSE;
               end if;
               --- Skip the approval and processing logic if this pack item is already approved or has been set for deletion
               if L_exists_delete = FALSE then
                  for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                     if NOT APPROVAL_CHECK(O_error_message,
                                           L_pack_submitted,
                                           'Y',               --- I_skip_component_chk
                                           L_item_status,
                                           'S',
                                           pack.pack_no) then
                        return FALSE;
                     end if;
                     if NOT L_pack_submitted then
                        L_all_packs_submitted := FALSE;
                     end if;
                  end LOOP;
                  if NOT L_all_packs_submitted then
                     if NOT INSERT_ERROR(O_error_message,
                                         child.item,
                                         'NOT_SUB_PACK',
                                         'Y',
                                         'N') then
                        return FALSE;
                     end if;
                     L_child_submitted := FALSE;
                     L_child_status := 'W';
                  elsif L_child_status = 'S' then
                     for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                        if PROCESS_ITEM(O_error_message,
                                        'S',
                                        NULL, -- I_single_record, updates item_master
                                        pack.pack_no) = FALSE then
                           return FALSE;
                        end if;
                     end LOOP;
                  end if;
               end if;
            end loop; -- end of c_pack(L_child) loop
         end if;

         --- Loop through grandchildren for each child as it is processed.
         if L_exists = FALSE then    --only process grandchildren if the parent isn't on the daily purge (deletion) list
            L_grandchilds_parent := child.item;
            for grandchild in C_GRANDCHILDREN loop
               if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                                L_exists_grand_child,
                                                                grandchild.item,
                                                                'ITEM_MASTER')= FALSE then
                  return FALSE;
               end if;
               --- Skip the approval and processing logic if this grandchild is already submitted or approved or set to be deleted
               if grandchild.status != 'W' or L_exists_grand_child = TRUE then
                  L_grandchild_submitted := TRUE;
               else
                  --- Do approval_check
                  if APPROVAL_CHECK(O_error_message,
                                    L_grandchild_submitted,
                                    'N',                --- I_skip_component_chk
                                    L_child_status,     --- I_parent_status
                                    'S',                --- I_new_status
                                    grandchild.item) = FALSE then
                     return FALSE;
                  end if;
                  if L_item_level + 2 = L_tran_level then
                     L_all_packs_submitted := TRUE;
                     L_grandchild := grandchild.item;
                     for rec in c_pack(L_grandchild) loop
                        --- Check if pack item is on daily purge
                        if DAILY_PURGE_SQL.ITEM_PARENT_GRANDPARENT_EXIST(O_error_message,
                                                                         L_exists_delete,
                                                                         rec.pack_no,
                                                                         'ITEM_MASTER')= FALSE then
                            return FALSE;
                        end if;
                        --- Skip the approval and processing logic if this pack item is already approved or has been set for deletion
                        if L_exists_delete = FALSE then
                           for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                              if NOT APPROVAL_CHECK(O_error_message,
                                                    L_pack_submitted,
                                                    'N',      --- I_skip_component_chk
                                                    grandchild.status,
                                                    'S',
                                                    pack.pack_no) then
                                 return FALSE;
                              end if;
                              if NOT L_pack_submitted then
                                 L_all_packs_submitted := FALSE;
                              end if;
                           end LOOP;
                           if NOT L_all_packs_submitted then
                              if NOT INSERT_ERROR(O_error_message,
                                                  grandchild.item,
                                                  'NOT_SUB_PACK',
                                                  'Y',
                                                  'N') then
                                 return FALSE;
                              end if;
                              L_grandchild_submitted := FALSE;
                           elsif L_grandchild_submitted then
                              for pack in C_ITEM_PACKS(rec.pack_no) LOOP
                                 if PROCESS_ITEM(O_error_message,
                                                 'S',
                                                 NULL, -- I_single_record, updates item_master
                                                 pack.pack_no) = FALSE then
                                    return FALSE;
                                 end if;
                              end LOOP;
                           end if;
                        end if;
                     end loop; -- end of c_pack(L_grandchild) loop
                  end if;

                  --- If grandchild passed approval check then update grandchild
                  if L_grandchild_submitted = TRUE then
                     if PROCESS_ITEM(O_error_message,
                                     'S',
                                     NULL, -- I_single_record, updates item_master
                                     grandchild.item) = FALSE then
                        return FALSE;
                     end if;
                  else
                     --- If any grandchild fails to get updated then set output parameter to FALSE
                     O_children_submitted := FALSE;
                  end if;
               end if;  -- if grandchild.status = 'A'

            end loop;  -- end C_GRANDCHILDREN loop

         end if; ---- child on daily purge list and will be deleted.

         --- If child passed approval check and it is not already approved or is set to be deleted then update child
         if L_child_submitted = TRUE and child.status != 'A' and L_exists = FALSE then
            if PROCESS_ITEM(O_error_message,
                            'S',
                            NULL, -- I_single_record, updates item_master
                            child.item) = FALSE then
               return FALSE;
            end if;
         end if;

      end loop;  -- end C_CHILDREN loop
      ---
   end if;

   --- If item passed approval check and it is not already approved, then update item
   if O_item_submitted = TRUE and L_original_item_status != 'A' then
      if PROCESS_ITEM(O_error_message,
                      'S',
                      'Y', -- I_single_record, doesn't update item_master
                      I_item)= FALSE then
         return FALSE;
      end if;
   end if;
   if FLUSH_PROCESS_ITEM(O_error_message, O_item_submitted, O_children_submitted,I_item,'S')=false
   then
       return false;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return false;
END SUBMIT;
--------------------------------------------------------------------------------------
FUNCTION INSERT_ERROR(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_item            IN      ITEM_APPROVAL_ERROR.ITEM%TYPE,
                      I_error_key       IN      ITEM_APPROVAL_ERROR.ERROR_KEY%TYPE,
                      I_system_req_ind  IN      ITEM_APPROVAL_ERROR.SYSTEM_REQ_IND%TYPE,
                      I_override_ind    IN      ITEM_APPROVAL_ERROR.OVERRIDE_IND%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(40) := 'ITEM_APPROVAL_SQL.INSERT_ERROR';

BEGIN
   insert into item_approval_error
             ( item
             , error_key
             , system_req_ind
             , override_ind
             , last_update_id
             , last_update_datetime)
       values( I_item
             , I_error_key
             , I_system_req_ind
             , I_override_ind
             , get_user
             , sysdate);

   return TRUE;

EXCEPTION
   when DUP_VAL_ON_INDEX then
      NULL;
      RETURN TRUE;
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END INSERT_ERROR;
----------------------------------------------------------------
FUNCTION SET_ALC_ITEM_TYPE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_items            IN     ITEM_TBL)
RETURN BOOLEAN IS

   L_program    VARCHAR2(61) := 'ITEM_APPROVAL_SQL.SET_ALC_ITEM_TYPE';

BEGIN

   --STYLES, NON-PACK TRAN ITEMS, AND SELLABLE PACKS
   --
   merge into item_master target
   using (
      select im.item,
             case when im.item_level = im.tran_level + 1 then
                     null
                  when im.item_level = im.tran_level - 1 and im.item_aggregate_ind = 'Y' then
                     'STYLE'
                  when im.item_level = im.tran_level and im.pack_ind = 'N' and im.item_parent IS NULL then
                     'ST'
                  when im.item_level = im.tran_level and im.pack_ind = 'N' and im.item_parent IS NOT NULL and imp.item_aggregate_ind = 'N' then
                     'ST'
                  when im.item_level = im.tran_level and im.pack_ind = 'N' and im.item_parent IS NOT NULL and imp.item_aggregate_ind = 'Y' then
                     'FASHIONSKU'
                  when im.sellable_ind = 'Y' and im.pack_ind = 'Y' then
                     'SELLPACK'
             end alc_item_type
        from table(cast(I_items as ITEM_TBL)) input_items,
             item_master im,
             item_master imp
       where im.item        = value(input_items)
         and ((im.pack_ind = 'N') or (im.pack_ind = 'Y' and im.sellable_ind = 'Y'))
         and im.item_parent = imp.item(+) ) use_this
   on (    target.item     = use_this.item)
   when matched then update
    set target.alc_item_type = use_this.alc_item_type;

   --NON-SELLABLE SIMPLE PACKS
   --
   merge into item_master target
   using (
      select impack.item,
             case when imparent.item_aggregate_ind = 'Y' then
                     'NSFSP'
                  else
                     'NSSSP'
             end alc_item_type
        from table(cast(I_items as ITEM_TBL)) input_items,
             item_master impack,
             packitem_breakout pb,
             item_master imcomp,
             item_master imparent
       where impack.item            = value(input_items)
         and impack.pack_ind        = 'Y'
         and impack.sellable_ind    = 'N'
         and impack.simple_pack_ind = 'Y'
         --
         and impack.item            = pb.pack_no
         and pb.item                = imcomp.item
         and imcomp.item_parent     = imparent.item(+) ) use_this
   on (    target.item     = use_this.item)
   when matched then update
    set target.alc_item_type = use_this.alc_item_type;

   --NON-SELLABLE COMPLEX PACKS
   --

   merge into item_master target
   using (
      select i.item,
             case when i.item_count = i.non_agg_count then
                     'NSSCP'
                  when i.item_count   = i.agg_count and
                       i.parent_count = 1 then
                       --
                       case when ((i.agg_1_count > 0 and i.diff_1_cnt = 1) or i.agg_1_count = 0) and
                                 ((i.agg_2_count > 0 and i.diff_2_cnt = 1) or i.agg_2_count = 0) and
                                 ((i.agg_3_count > 0 and i.diff_3_cnt = 1) or i.agg_3_count = 0) and
                                 ((i.agg_4_count > 0 and i.diff_4_cnt = 1) or i.agg_4_count = 0) then
                               'NSFSCP'
                            else
                               'NSFMCP'
                       end
                       --
                  else
                     'MULTI-PARENT'
             end alc_item_type
        from (select impack.item,
                     count(imcomp.item) item_count,
                     count(distinct nvl(imparent.item,'-999')) parent_count,
                     sum(decode(nvl(imparent.item_aggregate_ind,'N'),'N',1,0)) non_agg_count,
                     sum(decode(nvl(imparent.item_aggregate_ind,'N'),'Y',1,0)) agg_count,
                     --
                     sum(decode(nvl(imparent.diff_1_aggregate_ind,'N'),'Y',1,0)) agg_1_count,
                     sum(decode(nvl(imparent.diff_2_aggregate_ind,'N'),'Y',1,0)) agg_2_count,
                     sum(decode(nvl(imparent.diff_3_aggregate_ind,'N'),'Y',1,0)) agg_3_count,
                     sum(decode(nvl(imparent.diff_4_aggregate_ind,'N'),'Y',1,0)) agg_4_count,
                     --
                     count(distinct nvl(imcomp.diff_1,'-999')) diff_1_cnt,
                     count(distinct nvl(imcomp.diff_2,'-999')) diff_2_cnt,
                     count(distinct nvl(imcomp.diff_3,'-999')) diff_3_cnt,
                     count(distinct nvl(imcomp.diff_4,'-999')) diff_4_cnt
                from table(cast(I_items as ITEM_TBL)) input_items,
                     item_master impack,
                     packitem_breakout pb,
                     item_master imcomp,
                     item_master imparent
               where impack.item            = value(input_items)
                 and impack.sellable_ind    = 'N'
                 and impack.pack_ind        = 'Y'
                 and impack.simple_pack_ind = 'N'
                 and pb.pack_no             = impack.item
                 and pb.item                = imcomp.item
                 and imcomp.item_parent     = imparent.item(+)
               group by impack.item) i ) use_this
   on (    target.item     = use_this.item)
   when matched then update
    set target.alc_item_type = use_this.alc_item_type;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END SET_ALC_ITEM_TYPE;
----------------------------------------------------------------
END ITEM_APPROVAL_SQL;
/