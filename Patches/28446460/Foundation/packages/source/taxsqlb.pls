CREATE OR REPLACE PACKAGE BODY TAX_SQL AS


LP_vdate                    period.vdate%TYPE := get_vdate;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_VAT_REGION_DESC(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_ENTITY_VAT_REGION(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_INVC_VAT_REGION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_vat_region      IN OUT   VAT_REGION.VAT_REGION%TYPE,
                             I_invc_id         IN       INVC_HEAD.INVC_ID%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                             I_order_billtoloc IN       SHIPMENT.BILL_TO_LOC%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION VAT_DEPT_REGION_EXISTS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_VAT_CODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_ZERO_RATE_VAT_CODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_GTAX_INFO(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_VAT_INFO(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION ITEMLIST_VAT_EXPLODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              O_run_report      IN OUT            BOOLEAN,
                              IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_GTAX_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_VAT_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_VAT_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_GTAX_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_VAT_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_VAT_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION POST_VAT_AMOUNT(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION VATRATE_ITEM_DEPS_DELETION(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION VATCODE_ITEM_DEPS_DELETION(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_GTAX(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tax_result          IN OUT            OBJ_TAX_TBL,
                       I_tax_calc_tbl        IN OUT   NOCOPY   OBJ_TAX_CALC_TBL,
                       I_cost_retail_ind     IN OUT            VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION WRITE_RETAIL_TAX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tax_tbl       IN OUT OBJ_TAX_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION WRITE_POS_MODS_TAX_INFO (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_tax_tbl       IN OUT OBJ_TAX_TBL)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------------
FUNCTION CALC_VAT_COST_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_tax_calc_tbl   IN OUT   NOCOPY   OBJ_TAX_CALC_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_VAT_RETAIL_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_tax_calc_tbl   IN OUT   NOCOPY   OBJ_TAX_CALC_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_GTAX_COST_TAX(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_tax_calc_tbl       IN OUT   NOCOPY   OBJ_TAX_CALC_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_GTAX_RETAIL_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_tax_calc_tbl   IN OUT   NOCOPY   OBJ_TAX_CALC_TBL,
                              I_call_type       IN                VARCHAR2 DEFAULT LP_CALL_TYPE_DEFAULT)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION ADD_VAT_RETAIL_TAX(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_VAT_RETAIL_TAX(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION ADD_REMOVE_VAT_RETAIL_TAX(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL,
                                   I_add_remove_ind        IN                VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_GTAX_RETAIL_TAX(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_VAT_RATE_BY_CODE(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_tax_code_rate_tbl    IN OUT   NOCOPY   OBJ_TAX_CODE_RATE_TBL)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_VAT_REGION_RATE(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_cum_tax_pct          IN OUT  VAT_ITEM.VAT_RATE%TYPE,
                                     I_item                 IN      VAT_ITEM.ITEM%TYPE,
                                     I_cost_retail_ind      IN      VAT_ITEM.VAT_TYPE%TYPE,
                                     I_effective_from_date  IN      VAT_ITEM.ACTIVE_DATE%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------



-----------------------------------------------------------------------------------------------------
FUNCTION LOCK_VAT_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50)  := 'TAX_SQL.LOCK_VAT_ITEM';
   L_table       VARCHAR2(10)  := 'VAT_ITEM';
   L_item        ITEM_MASTER.ITEM%TYPE;
   L_vat_region  VAT_ITEM.VAT_REGION%TYPE;
   L_vat_type    VAT_ITEM.VAT_TYPE%TYPE;
   L_active_date VAT_ITEM.ACTIVE_DATE%TYPE;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_VAT_ITEM IS
      select 'x'
        from vat_item
       where item = L_item
         and vat_region = L_vat_region
         and vat_type = L_vat_type
         and active_date = L_active_date
         for update nowait;

BEGIN

   FOR rec in IO_tax_info_tbl.first..IO_tax_info_tbl.last LOOP
      L_item := IO_tax_info_tbl(rec).item;
      L_vat_region := IO_tax_info_tbl(rec).from_tax_region;
      L_vat_type := IO_tax_info_tbl(rec).cost_retail_ind;
      L_active_date := IO_tax_info_tbl(rec).active_date;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_VAT_ITEM',
                       'VAT_ITEM, OBJ_TAX_INFO_TBL',
                       NULL);
      open C_LOCK_VAT_ITEM;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_VAT_ITEM',
                       'VAT_ITEM, OBJ_TAX_INFO_TBL',
                        NULL);
      close C_LOCK_VAT_ITEM;
   END LOOP;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'Item '||L_item||', vat region '||L_vat_region||
                                            ', vat type '||L_vat_type||', active date '||L_active_date,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_VAT_ITEM;
-----------------------------------------------------------------------------------------------------
FUNCTION LOCK_GTAX_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50)  := 'TAX_SQL.LOCK_GTAX_ITEM';

   L_table                    VARCHAR2(20) := 'GTAX_ITEM_ROLLUP';
   L_item                     GTAX_ITEM_ROLLUP.ITEM%TYPE;
   L_loc                      GTAX_ITEM_ROLLUP.LOC%TYPE;
   L_loc_type                 GTAX_ITEM_ROLLUP.LOC_TYPE%TYPE;
   L_effective_from_date      GTAX_ITEM_ROLLUP.EFFECTIVE_FROM_DATE%TYPE;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_GTAX_ITEM IS
      select 'x'
        from gtax_item_rollup
       where item = L_item
         and loc = L_loc
         and loc_type = L_loc_type
         and effective_from_date = L_effective_from_date
         for update nowait;

BEGIN

   FOR rec in IO_tax_info_tbl.first..IO_tax_info_tbl.last LOOP
      L_item := IO_tax_info_tbl(rec).item;
      L_loc := IO_tax_info_tbl(rec).from_entity;
      L_loc_type := IO_tax_info_tbl(rec).from_entity_type;
      L_effective_from_date := IO_tax_info_tbl(rec).active_date;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_GTAX_ITEM',
                       'GTAX_ITEM_ROLLUP',
                       NULL);
      open C_LOCK_GTAX_ITEM;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_GTAX_ITEM',
                       'GTAX_ITEM_ROLLUP',
                       NULL);
      close C_LOCK_GTAX_ITEM;
   END LOOP;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            L_item||','||L_loc||','||L_loc_type||','||to_char(L_effective_from_date),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_GTAX_ITEM;
-----------------------------------------------------------------------------------------------------
FUNCTION LOCK_VAT_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)  := 'TAX_SQL.LOCK_VAT_DEPS';
   L_table     VARCHAR2(10)  := 'VAT_DEPS';

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_VAT_DEPS IS
      select 'x'
        from vat_deps vi
       where exists (select 'x'
                       from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vt
                      where vi.dept        = vt.merch_hier_value
                        and vi.vat_region  = NVL(vt.from_tax_region, vi.vat_region)
                        and vi.vat_type    = NVL(vt.cost_retail_ind, vi.vat_type)
                        and vi.vat_code    = NVL(vt.tax_code, vi.vat_code)
                        and vt.merch_hier_level = LP_dept_merch)
         for update nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_VAT_DEPS',
                    'VAT_DEPS, OBJ_TAX_INFO_TBL',
                    NULL);
   open C_LOCK_VAT_DEPS;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_VAT_DEPS',
                    'VAT_DEPS, OBJ_TAX_INFO_TBL',
                    NULL);
   close C_LOCK_VAT_DEPS;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'OBJ_TAX_INFO_REC',
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_VAT_DEPS;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_REGION_DESC(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'TAX_SQL.GET_TAX_REGION_DESC';

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type = LP_SVAT then
      if GET_VAT_REGION_DESC(O_error_message,
                             IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   elsif LP_system_options_row.default_tax_type in (LP_GTAX,LP_SALES) then
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TAX_REGION_DESC;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_VAT_REGION_DESC(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'TAX_SQL.GET_VAT_REGION_DESC';
   L_initial_count   NUMBER(10);

   cursor C_GET_VAT_REGION_DESC is
      select OBJ_TAX_INFO_REC(tax.item,
                              tax.item_parent,
                              tax.item_grandparent,
                              tax.pack_ind,
                              tax.merch_hier_level,
                              tax.merch_hier_value,
                              tax.from_tax_region,
                              vr.vat_region_name,
                              tax.to_tax_region,
                              tax.from_entity,
                              tax.from_entity_type,
                              tax.to_entity,
                              tax.to_entity_type,
                              tax.amount,
                              tax.cost_retail_ind,
                              tax.tax_incl_ind,
                              tax.tax_amount,
                              tax.tax_rate,
                              tax.tax_code,
                              tax.tax_code_desc,
                              tax.active_date,
                              tax.inventory_ind,
                              tax.ref_no_1,
                              tax.ref_no_2,
                              tax.tran_code,
                              tax.taxable_base,
                              tax.modified_taxable_base,
                              tax.calculation_basis,
                              tax.recoverable_amount,
                              tax.currency,
                              tax.reverse_vat_ind)
        from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) tax,
             v_vat_region_tl vr
       where vr.vat_region = tax.from_tax_region;

BEGIN

   L_initial_count := IO_tax_info_tbl.count;

   open C_GET_VAT_REGION_DESC;
   IO_tax_info_tbl.delete();
   fetch C_GET_VAT_REGION_DESC BULK COLLECT into IO_tax_info_tbl;
   close C_GET_VAT_REGION_DESC;
   --
   if IO_tax_info_tbl.count != L_initial_count then
      O_error_message := SQL_LIB.CREATE_MSG('INV_VAT_REGION',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_VAT_REGION_DESC;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_CODE_DESC(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'TAX_SQL.GET_TAX_CODE_DESC';
   L_initial_count   NUMBER(10);

   cursor C_GET_TAX_CODE_DESC is
      select OBJ_TAX_INFO_REC(vit.item,
                              vit.item_parent,
                              vit.item_grandparent,
                              vit.pack_ind,
                              vit.merch_hier_level,
                              vit.merch_hier_value,
                              vit.from_tax_region,
                              vit.from_tax_region_desc,
                              vit.to_tax_region,
                              vit.from_entity,
                              vit.from_entity_type,
                              vit.to_entity,
                              vit.to_entity_type,
                              vit.amount,
                              vit.cost_retail_ind,
                              vit.tax_incl_ind,
                              vit.tax_amount,
                              vit.tax_rate,
                              vit.tax_code,
                              vc.vat_code_desc,
                              vit.active_date,
                              vit.inventory_ind,
                              vit.ref_no_1,
                              vit.ref_no_2,
                              vit.tran_code,
                              vit.taxable_base,
                              vit.modified_taxable_base,
                              vit.calculation_basis,
                              vit.recoverable_amount,
                              vit.currency,
                              vit.reverse_vat_ind)
        from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vit,
             v_vat_codes_tl vc
       where vc.vat_code = vit.tax_code;

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   --for GTAX, tax codes are defined on VAT_CODES table as well.
   if LP_system_options_row.default_tax_type in (LP_SVAT, LP_GTAX) then
      ---
      L_initial_count := IO_tax_info_tbl.count;
      open C_GET_TAX_CODE_DESC;
      IO_tax_info_tbl.DELETE;
      fetch C_GET_TAX_CODE_DESC bulk collect into IO_tax_info_tbl;
      close C_GET_TAX_CODE_DESC;
      --
      if IO_tax_info_tbl.count != L_initial_count then
         if LP_system_options_row.default_tax_type = LP_GTAX then
            O_error_message := SQL_LIB.CREATE_MSG('INV_IGTAX_CODE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
         else
            O_error_message := SQL_LIB.CREATE_MSG('INV_VAT_CODE',
                                                  NULL,
                                                  NULL,
                                                  NULL);
         end if;
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_TAX_CODE_DESC;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_CODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'TAX_SQL.GET_TAX_CODE';

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type =LP_SVAT then
      if GET_VAT_CODE(O_error_message,
                      IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_TAX_CODE;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_VAT_CODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'TAX_SQL.GET_VAT_CODE';
   L_vdate     DATE         := GET_VDATE;

   cursor C_GET_TAX_CODE_INFO is
      select OBJ_TAX_INFO_REC(vit.item,
                              vit.item_parent,
                              vit.item_grandparent,
                              vit.pack_ind,
                              vit.merch_hier_level,
                              vit.merch_hier_value,
                              vit.from_tax_region,
                              vit.from_tax_region_desc,
                              vit.to_tax_region,
                              vit.from_entity,
                              vit.from_entity_type,
                              vit.to_entity,
                              vit.to_entity_type,
                              vit.amount,
                              vit.cost_retail_ind,
                              vit.tax_incl_ind,
                              vit.tax_amount,
                              vcr.vat_rate,
                              NVL(vcr.vat_code, vit.tax_code),
                              vit.tax_code_desc,
                              vcr.active_date,
                              vit.inventory_ind,
                              vit.ref_no_1,
                              vit.ref_no_2,
                              vit.tran_code,
                              vit.taxable_base,
                              vit.modified_taxable_base,
                              vit.calculation_basis,
                              vit.recoverable_amount,
                              vit.currency,
                              vit.reverse_vat_ind)
         from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vit,
              vat_code_rates vcr
        where vcr.vat_code = NVL(vit.tax_code, vcr.vat_code)
          and vcr.vat_rate = NVL(vit.tax_rate, vcr.vat_rate)
          and vcr.active_date = NVL(vit.active_date, (select MIN(vcr2.active_date)
                                                        from vat_code_rates vcr2
                                                       where vcr2.vat_code = vcr.vat_code));

BEGIN

   open C_GET_TAX_CODE_INFO;
   IO_tax_info_tbl.DELETE;
   fetch C_GET_TAX_CODE_INFO bulk collect into IO_tax_info_tbl;
   close C_GET_TAX_CODE_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_VAT_CODE;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_ZERO_RATE_TAX_CODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)

   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'TAX_SQL.GET_ZERO_RATE_TAX_CODE';

BEGIN

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX,LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if GET_ZERO_RATE_VAT_CODE(O_error_message,
                                IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_ZERO_RATE_TAX_CODE;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_ZERO_RATE_VAT_CODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(60) := 'TAX_SQL.GET_ZERO_RATE_VAT_CODE';
   L_vdate     DATE         := GET_VDATE;

   cursor C_GET_ZERO_RATE_TAX_CODE is
      select OBJ_TAX_INFO_REC(NULL, --item
                              NULL, --item_parent
                              NULL, --item_grandparent
                              NULL, --pack_ind
                              NULL, --merch_hier_level
                              NULL, --merch_hier_value
                              NULL, --from_tax_region
                              NULL, --from_tax_region_desc
                              NULL, --to_tax_region
                              NULL, --from_entity
                              NULL, --from_entity_type
                              NULL, --to_entity
                              NULL, --to_entity_type
                              NULL, --amount
                              NULL, --cost_retail_ind
                              NULL, --tax_incl_ind
                              NULL, --tax_amount
                              vcr.vat_rate,
                              vcr.vat_code,
                              NULL, --tax_code_desc
                              vcr.active_date,
                              NULL, --inventory_ind
                              NULL, --ref_no_1
                              NULL, --ref_no_2
                              NULL, --tran_code
                              NULL, --taxable_base
                              NULL, --modified_taxable_base
                              NULL, --calculation_basis
                              NULL, --recoverable_amount
                              NULL, --currency
                              NULL) --reverse_vat_ind
        from vat_code_rates vcr
       where vcr.vat_rate = 0
         and vcr.active_date <= L_vdate
         and not exists (select 'x'
                           from vat_code_rates vcr2
                          where vcr2.vat_code = vcr.vat_code
                            and vcr2.active_date > vcr.active_date
                            and rownum = 1);

BEGIN

   open C_GET_ZERO_RATE_TAX_CODE;
   fetch C_GET_ZERO_RATE_TAX_CODE bulk collect into IO_tax_info_tbl;
   close C_GET_ZERO_RATE_TAX_CODE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_ZERO_RATE_VAT_CODE;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_ENTITY_TAX_REGION(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'TAX_SQL.GET_ENTITY_TAX_REGION';
   L_initial_count   NUMBER(10);

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type = LP_SVAT then
      if GET_ENTITY_VAT_REGION(O_error_message,
                               IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   elsif LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ENTITY_TAX_REGION;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_ENTITY_VAT_REGION(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'TAX_SQL.GET_ENTITY_VAT_REGION';
   L_initial_count   NUMBER(10);

   cursor C_GET_VAT_REGION is
      select OBJ_TAX_INFO_REC(vtbl.item,
                              vtbl.item_parent,
                              vtbl.item_grandparent,
                              vtbl.pack_ind,
                              vtbl.merch_hier_level,
                              vtbl.merch_hier_value,
                              NVL(eg.tax_region, vtbl.from_tax_region),
                              vr.vat_region_name,
                              vtbl.to_tax_region,
                              vtbl.from_entity,
                              vtbl.from_entity_type,
                              vtbl.to_entity,
                              vtbl.to_entity_type,
                              vtbl.amount,
                              vtbl.cost_retail_ind,
                              vtbl.tax_incl_ind,
                              vtbl.tax_amount,
                              vtbl.tax_rate,
                              vtbl.tax_code,
                              vtbl.tax_code_desc,
                              vtbl.active_date,
                              vtbl.inventory_ind,
                              vtbl.ref_no_1,
                              vtbl.ref_no_2,
                              vtbl.tran_code,
                              vtbl.taxable_base,
                              vtbl.modified_taxable_base,
                              vtbl.calculation_basis,
                              vtbl.recoverable_amount,
                              vtbl.currency,
                              vtbl.reverse_vat_ind)
        from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vtbl,
             vat_region vr,
             (select TO_CHAR(store) entity,
                     vat_region tax_region,
                     'ST' entity_type
                from store
              UNION ALL
              select TO_CHAR(wh) entity,
                     vat_region tax_region,
                     'WH' entity_type
                from wh
              UNION ALL
              select TO_CHAR(supplier) entity,
                     vat_region tax_region,
                    'SUP' entity_type
                from sups
              UNION ALL
              select partner_id entity,
                     vat_region tax_region,
                     'P' entity_type
                from partner
               where partner_type = 'E') eg
       where vtbl.from_entity = eg.entity
         and vtbl.from_entity_type = eg.entity_type
         and eg.tax_region = vr.vat_region;

BEGIN

   L_initial_count := IO_tax_info_tbl.count;
   -- Retrieve vat region information.
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_VAT_REGION',
                    NULL,
                    NULL);
   open C_GET_VAT_REGION;

   IO_tax_info_tbl.delete();

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_VAT_REGION',
                    NULL,
                    NULL);
   fetch C_GET_VAT_REGION BULK COLLECT into IO_tax_info_tbl;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_VAT_REGION',
                    NULL,
                    NULL);
   close C_GET_VAT_REGION;
   --
   if IO_tax_info_tbl.count != L_initial_count then
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC',
                                            'IO_tax_info_tbl',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ENTITY_VAT_REGION;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_INVC_TAX_REGION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tax_region      IN OUT   VAT_REGION.VAT_REGION%TYPE,
                             I_invc_id         IN       INVC_HEAD.INVC_ID%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                             I_order_billtoloc IN       SHIPMENT.BILL_TO_LOC%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(60) := 'TAX_SQL.GET_INVC_TAX_REGION';

BEGIN

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if GET_INVC_VAT_REGION(O_error_message,
                             O_tax_region,
                             I_invc_id,
                             I_order_no,
                             I_order_billtoloc) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_INVC_TAX_REGION;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_INVC_VAT_REGION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_vat_region      IN OUT   VAT_REGION.VAT_REGION%TYPE,
                             I_invc_id         IN       INVC_HEAD.INVC_ID%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                             I_order_billtoloc IN       SHIPMENT.BILL_TO_LOC%TYPE DEFAULT NULL)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(60) := 'TAX_SQL.GET_INVC_VAT_REGION';

   L_bill_to_id            SHIPMENT.BILL_TO_LOC%TYPE;
   L_outloc_vat_region     OUTLOC.OUTLOC_VAT_REGION%TYPE;
   L_sup_vat_region        SUPS.VAT_REGION%TYPE;

   cursor C_ORDER is
      select sh.bill_to_loc,
             (select str.vat_region
                from store str
               where str.store = sh.bill_to_loc
              union all
              select wh.vat_region
                from wh wh
               where wh.wh = sh.bill_to_loc)
        from shipment sh
       where sh.order_no = I_order_no
         and rownum = 1;

  cursor C_ORDER_VAT_REGION is 
      select input_loc.region
        from vat_region vr,
             (select store as loc,vat_region as region
                from store 
                union 
               select wh as loc,vat_region as region
                from wh) input_loc
       where input_loc.loc = I_order_billtoloc
         and vr.vat_region = input_loc.region;
		 
   cursor C_SUP is
      select s.vat_region
        from vat_region vr,
             invc_head ih,
             sups s
       where ih.invc_id = I_invc_id
         and ih.supplier = s.supplier
         and s.vat_region = vr.vat_region;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDER',
                    'ORDHEAD,OUTLOC,VAT_REGION',
                    NULL);
   open C_ORDER;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDER',
                    'ORDHEAD,OUTLOC,VAT_REGION',
                    NULL);
   fetch C_ORDER into L_bill_to_id,
                      L_outloc_vat_region;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDER',
                    'ORDHEAD,OUTLOC,VAT_REGION',
                    NULL);
   close C_ORDER;

   if L_bill_to_id is NULL and I_order_billtoloc is not NULL then
      open C_ORDER_VAT_REGION;
      fetch C_ORDER_VAT_REGION into L_outloc_vat_region;
      close C_ORDER_VAT_REGION;
   end if;
   if L_bill_to_id is NULL or L_outloc_vat_region is NULL then
      O_vat_region := NULL;
      return TRUE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_SUP',
                    'SUPS,VAT_REGION,INVC_HEAD',
                    NULL);
   open C_SUP;
   SQL_LIB.SET_MARK('FETCH',
                    'C_SUP',
                    'SUPS,VAT_REGION,INVC_HEAD',
                    NULL);
   fetch C_SUP into L_sup_vat_region;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_SUP',
                    'SUPS,VAT_REGION,INVC_HEAD',
                    NULL);
   close C_SUP;

   if L_sup_vat_region is NULL then
      O_vat_region := NULL;
      return TRUE;
   end if;

   O_vat_region := L_sup_vat_region;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_INVC_VAT_REGION;
-----------------------------------------------------------------------------------------------------
FUNCTION TAX_DEPT_REGION_EXISTS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_function                  VARCHAR2(60) := 'TAX_SQL.TAX_DEPT_REGION_EXISTS';

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if VAT_DEPT_REGION_EXISTS(O_error_message,
                                IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END TAX_DEPT_REGION_EXISTS;
-----------------------------------------------------------------------------------------------------
FUNCTION VAT_DEPT_REGION_EXISTS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_function                  VARCHAR2(60) := 'TAX_SQL.VAT_DEPT_REGION_EXISTS';
   L_exists                    VARCHAR2(1);
   L_region_missing            VARCHAR2(1);
   L_dept                      VAT_DEPS.DEPT%TYPE;
   L_tax_region                VAT_DEPS.VAT_REGION%TYPE;
   L_tax_type                  VAT_DEPS.VAT_TYPE%TYPE;
   L_tax_code                  VAT_DEPS.VAT_CODE%TYPE;
   L_tax_info_rec              OBJ_TAX_INFO_REC := OBJ_TAX_INFO_REC();

   cursor C_verify_regions is
      select 'x'
        from vat_region
       where exists (select vat_region from vat_region
                     minus
                     select vat_region from vat_deps
                     where dept = L_dept
                       and vat_type in (LP_TAX_TYPE_BOTH, LP_TAX_TYPE_RETAIL));

   cursor C_check_deps is
      select 'x'
        from vat_deps
       where dept = L_dept
         and vat_region = L_tax_region
         and vat_type in (LP_TAX_TYPE_BOTH, LP_TAX_TYPE_RETAIL);

   cursor C_check_tax_type is
      select vat_region,
             dept,
             vat_type,
             vat_code
        from vat_deps
       where dept = L_dept
         and vat_region = L_tax_region
         and (vat_type = decode(L_tax_type, LP_TAX_TYPE_BOTH, vat_type, L_tax_type)
          or vat_type = LP_TAX_TYPE_BOTH);

BEGIN

   for i in IO_tax_info_tbl.first..IO_tax_info_tbl.last LOOP
      L_dept       := IO_tax_info_tbl(i).merch_hier_value;
      L_tax_region := IO_tax_info_tbl(i).from_tax_region;
      L_tax_type   := IO_tax_info_tbl(i).cost_retail_ind;

      if L_tax_region is NULL then
         open C_VERIFY_REGIONS;
         fetch C_VERIFY_REGIONS into L_region_missing;

         if L_region_missing is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('VAT_DEPS_MISSING_REGION',
                                                  L_dept,
                                                  NULL,
                                                  NULL);
            close C_VERIFY_REGIONS;
            return FALSE;
         end if;
         close C_VERIFY_REGIONS;
      elsif L_tax_region is NOT NULL and L_tax_type is NULL then

         open C_CHECK_DEPS;

         fetch C_CHECK_DEPS into L_exists;

         if L_exists is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('INVC_MATCH_OFF_VAT_TYPE',
                                                  L_tax_region,
                                                  NULL,
                                                  NULL);
            close C_CHECK_DEPS;
            return FALSE;
        end if;
        close C_CHECK_DEPS;
     elsif L_tax_region is NOT NULL and L_tax_type is NOT NULL then

        open C_CHECK_TAX_TYPE;
        fetch C_CHECK_TAX_TYPE into L_tax_region,
                                    L_dept,
                                    L_tax_type,
                                    L_tax_code;

        if C_CHECK_TAX_TYPE%NOTFOUND then
           IO_tax_info_tbl.DELETE;
        else
           IO_tax_info_tbl.DELETE;
           L_tax_info_rec := OBJ_TAX_INFO_REC();

           L_tax_info_rec.merch_hier_value := L_dept;
           L_tax_info_rec.from_tax_region  := L_tax_region;
           L_tax_info_rec.cost_retail_ind  := L_tax_type;
           L_tax_info_rec.tax_code         := L_tax_code;
           IO_tax_info_tbl.EXTEND;
           IO_tax_info_tbl(IO_tax_info_tbl.COUNT) := L_tax_info_rec;
        end if;
        close C_CHECK_TAX_TYPE;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
  when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END VAT_DEPT_REGION_EXISTS;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INFO(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TAX_SQL.GET_TAX_INFO';

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type = LP_GTAX then
      if GET_GTAX_INFO(O_error_message,
                       IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if GET_VAT_INFO(O_error_message,
                      IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_TAX_INFO;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_GTAX_INFO(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TAX_SQL.GET_GTAX_INFO';
   L_vdate     DATE         := GET_VDATE;

   cursor C_GET_GTAX_INFO is
      -- This will copy tax information from an regular item to another item. Item parent doesn't
      -- necessarily mean the parent of an item. It could also mean that it is the from or copy item.
      select OBJ_TAX_INFO_REC(im.item,
                              im.item_parent, --NULL
                              im.item_grandparent,  --vt.item_grandparent
                              im.pack_ind,   --gi.pack_ind,
                              NULL,   --vt.merch_hier_level,
                              NULL,   --vt.merch_hier_value,
                              NULL,   --gi.from_tax_region,
                              NULL,   --vt.from_tax_region_desc,
                              NULL,   --gi.to_tax_region,
                              gi.loc,
                              gi.loc_type,
                              NULL,   --gi.to_entity,
                              NULL,   --gi.to_entity_type,
                              NULL,   --vt.amount,
                              NULL,   --gi.tax_type, --COST_RETAIL_IND
                              NULL,   --vt.tax_incl_ind,
                              gi.cum_tax_value, --gi.estimated_tax_value, --TAX_AMOUNT
                              gi.cum_tax_pct,   --gi.tax_rate,  --TAX_RATE
                              NULL,    --gi.tax_code,
                              NULL,    --vt.tax_code_desc,
                              gi.effective_from_date, --ACTIVE_DATE
                              im.inventory_ind,
                              NULL,    --REF_NO_1
                              NULL,    --REF_NO_2
                              NULL,    --TRAN_CODE
                              NULL,    --gi.taxable_base,
                              NULL,    --gi.modified_taxable_base,
                              NULL,    --gi.calculation_basis,
                              NULL,    --gi.recoverable_amount,
                              gi.currency_code,
                              NULL)    --vt.reverse_vat_ind
        from gtax_item_rollup gi,
             item_master im,
             TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vt
       where vt.item_parent is NOT NULL
         and gi.item = vt.item_parent
         and gi.loc = NVL(vt.from_entity, gi.loc)
         and gi.loc_type = NVL(vt.from_entity_type, gi.loc_type)
         -- if the input item is null, then explode tax information to all
         -- the child and grandchild of the input item parent
         and ((vt.item is NULL and
               (im.item = vt.item_parent or
                im.item_parent = vt.item_parent or
                im.item_grandparent = vt.item_parent) and
               im.item_level <= im.tran_level) or
              -- if the input item is not null, then copy tax information only to that item
              (vt.item is NOT NULL and
               im.item = vt.item))
      UNION ALL
      -- This will only select item tax information for a non pack item. When a pack item is passed the tax information would be retrieved
      -- at the component level
      select OBJ_TAX_INFO_REC(vt.item,
                              gi.item_parent,
                              gi.item_grandparent,
                              NULL, --gi.pack_ind,
                              NULL, --vt.merch_hier_level,
                              NULL, --vt.merch_hier_value,
                              NULL, --gi.from_tax_region,
                              NULL, --vt.from_tax_region_desc,
                              NULL, --gi.to_tax_region,
                              gi.loc,
                              gi.loc_type,
                              NULL, --gi.to_entity,
                              NULL, --gi.to_entity_type,
                              NULL, --vt.amount,
                              NULL, --gi.tax_type, --COST_RETAIL_IND
                              NULL, --vt.tax_incl_ind,
                              gi.cum_tax_value, --gi.estimated_tax_value, --TAX_AMOUNT
                              gi.cum_tax_pct,   --gi.tax_rate, --TAX_RATE
                              NULL, --gi.tax_code,
                              NULL, --vt.tax_code_desc,
                              gi.effective_from_date,  --ACTIVE_DATE
                              vt.inventory_ind,
                              NULL, --REF_NO_1
                              NULL, --REF_NO_2
                              NULL, --TRAN_CODE
                              NULL, --gi.taxable_base,
                              NULL, --gi.modified_taxable_base,
                              NULL, --gi.calculation_basis,
                              NULL, --gi.recoverable_amount,
                              gi.currency_code,
                              NULL) --vt.reverse_vat_ind
        from gtax_item_rollup gi,
             TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vt
       where vt.item_parent is NULL
         and vt.item = gi.item
         and gi.effective_from_date = NVL(vt.active_date, gi.effective_from_date)
         and gi.loc_type = NVL(vt.from_entity_type, gi.loc_type)
         and gi.loc = NVL(vt.from_entity, gi.loc);

BEGIN
   -- Retrieve tax information from the gtax_item table
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_GTAX_INFO',
                    'gtax_item, OBJ_TAX_INFO_TBL',
                    NULL);
   open C_GET_GTAX_INFO;

   IO_tax_info_tbl.delete();

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_GTAX_INFO',
                    'gtax_item, OBJ_TAX_INFO_TBL',
                    NULL);
   fetch C_GET_GTAX_INFO bulk collect into IO_tax_info_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_GTAX_INFO',
                    'gtax_item, OBJ_TAX_INFO_TBL',
                    NULL);
   close C_GET_GTAX_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_GTAX_INFO;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_VAT_INFO(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                      IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TAX_SQL.GET_VAT_INFO';
   L_vdate     DATE         := GET_VDATE;

    cursor C_GET_TAX_INFO is
         -- This will copy tax information from a dept to an item
       WITH with_vt as(
                       select * from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL))
                       )

         select OBJ_TAX_INFO_REC(im.item,
                                 vt.item_parent,
                                 vt.item_grandparent,
                                 vt.pack_ind,
                                 vt.merch_hier_level,
                                 vd.dept,
                                 vd.vat_region,
                                 vt.from_tax_region_desc,
                                 vt.to_tax_region,
                                 vt.from_entity,
                                 vt.from_entity_type,
                                 vt.to_entity,
                                 vt.to_entity_type,
                                 vt.amount,
                                 vd.vat_type,
                                 vt.tax_incl_ind,
                                 vt.tax_amount,
                                 vcr.vat_rate,
                                 vd.vat_code,
                                 vt.tax_code_desc,
                                 L_vdate,
                                 vt.inventory_ind,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 vd.reverse_vat_ind)
           from item_master im,
                vat_deps vd,
                vat_code_rates vcr,
                with_vt vt
          where vt.merch_hier_value is NOT NULL
            and vt.item_parent is NULL
            and vd.dept = vt.merch_hier_value
            and im.dept = vt.merch_hier_value
            and vt.merch_hier_level = LP_dept_merch
            and vd.vat_region = NVL(vt.from_tax_region, vd.vat_region)
            and vd.vat_code = NVL(vt.tax_code, vd.vat_code)
            and vd.vat_code = vcr.vat_code
            and vcr.active_date = (select MAX(vcr2.active_date)
                                       from vat_code_rates vcr2
                                      where vcr2.vat_code = vd.vat_code
                                        and vcr2.active_date <= NVL(vt.active_date, L_vdate))
            -- If the input item is not null, then copy tax information only to that item
            and ((vt.item is NOT NULL and
                  im.item = vt.item) or
                 -- If the input item is null, then copy tax information of dept
                 -- to all items in item master where department is the vat department
                 (vt.item is NULL
                  and im.dept = vd.dept
                 -- If the input item is null, then copy tax information of dept
                  and not exists (select 'x'
                                    from vat_item vi
                                  where vi.item = im.item
                                    and vi.vat_region = vd.vat_region
                                    and vi.active_date = NVL(vt.active_date, L_vdate)
                                    and vi.vat_type in (vd.vat_type, LP_TAX_TYPE_BOTH))))
         UNION ALL
         -- This will copy tax information from an item to another item. Item parent doesn't
         -- necessarily mean the parent of an item. It could also mean that it is the from or copy item.
         select OBJ_TAX_INFO_REC(im.item,
                                 vt.item_parent,
                                 vt.item_grandparent,
                                 vt.pack_ind,
                                 vt.merch_hier_level,
                                 vt.merch_hier_value,
                                 vi.vat_region,
                                 vt.from_tax_region_desc,
                                 vt.to_tax_region,
                                 vt.from_entity,
                                 vt.from_entity_type,
                                 vt.to_entity,
                                 vt.to_entity_type,
                                 vt.amount,
                                 vi.vat_type,
                                 vt.tax_incl_ind,
                                 vt.tax_amount,
                                 vi.vat_rate,
                                 vi.vat_code,
                                 vt.tax_code_desc,
                                 vi.active_date,
                                 vt.inventory_ind,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 vi.reverse_vat_ind)
           from vat_item vi,
                item_master im,
                with_vt vt
          where vt.merch_hier_value is NULL
            and vt.item_parent IS not NULL
            and vi.vat_region = NVL(vt.from_tax_region, vi.vat_region)
            and vi.vat_type = NVL(vt.cost_retail_ind, vi.vat_type)
            and vi.vat_code = NVL(vt.tax_code, vi.vat_code)
            and vi.item = vt.item_parent
            -- if the input item is null, then explode tax information to all
            -- the child and grandchild of the input item parent
            and ((vt.item is NULL and
                    (im.item = vt.item_parent or
                     im.item_parent = vt.item_parent or
                     im.item_grandparent = vt.item_parent) and
                  im.item_level <= im.tran_level) or
                 -- if the input item is not null, then copy tax information only to that item
                 (vt.item is NOT NULL and
                  im.item = vt.item))
         UNION ALL
         -- This will only select item tax information for a given input item information.
         -- This is different from the previous two select statements as this does not copy
         -- tax information. It's just used to retrieve tax information.
         select /*+ CARDINALITY(vt, 10) */OBJ_TAX_INFO_REC(vi.item,
                                 vt.item_parent,
                                 vt.item_grandparent,
                                 vt.pack_ind,
                                 vt.merch_hier_level,
                                 vt.merch_hier_value,
                                 vi.vat_region,
                                 vt.from_tax_region_desc,
                                 vt.to_tax_region,
                                 vt.from_entity,
                                 vt.from_entity_type,
                                 vt.to_entity,
                                 vt.to_entity_type,
                                 vt.amount,
                                 vi.vat_type,
                                 vt.tax_incl_ind,
                                 vt.tax_amount,
                                 vi.vat_rate,
                                 vi.vat_code,
                                 vt.tax_code_desc,
                                 vi.active_date,
                                 vt.inventory_ind,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 vi.reverse_vat_ind)
           from vat_item vi,
                with_vt vt
          where vt.merch_hier_value is NULL
            and vt.item_parent is NULL
            and vt.item is NULL
            and vi.vat_type in (NVL(vt.cost_retail_ind, vi.vat_type), LP_TAX_TYPE_BOTH)
            and vi.vat_code = NVL(vt.tax_code, vi.vat_code)
            and vi.vat_region = NVL(vt.from_tax_region, vi.vat_region)
         and vi.active_date = NVL(vt.active_date, vi.active_date)
      UNION ALL
      select /*+ INDEX(vi, PK_VAT_ITEM) */ OBJ_TAX_INFO_REC(vi.item,
                              vt.item_parent,
                              vt.item_grandparent,
                              vt.pack_ind,
                              vt.merch_hier_level,
                              vt.merch_hier_value,
                              vi.vat_region,
                              vt.from_tax_region_desc,
                              vt.to_tax_region,
                              vt.from_entity,
                              vt.from_entity_type,
                              vt.to_entity,
                              vt.to_entity_type,
                              vt.amount,
                              vi.vat_type,
                              vt.tax_incl_ind,
                              vt.tax_amount,
                              vi.vat_rate,
                              vi.vat_code,
                              vt.tax_code_desc,
                              vi.active_date,
                              vt.inventory_ind)
        from vat_item vi,
             with_vt vt
       where vt.merch_hier_value is NULL
         and vt.item_parent is NULL
         and vt.item is NOT NULL
         and (vi.item = vt.item or vt.item is NULL)
		 and (vi.vat_code = vt.tax_code or vt.tax_code is NULL)
         and (vi.vat_region = vt.from_tax_region or vt.from_tax_region is NULL)
         and (vi.active_date = vt.active_date or vt.active_date is NULL)
         and ((NVL(vt.cost_retail_ind, vi.vat_type) in ('C','R') and vi.vat_type in (NVL(vt.cost_retail_ind, vi.vat_type), 'B'))
              or
              (NVL(vt.cost_retail_ind, vi.vat_type) ='B'
               and ( vi.vat_type = 'B' 
                     or
                    (exists (select 'x'
                                 from vat_item vi1
                               where vi1.item = NVL(vt.item, vi1.item)
                                 and vi1.vat_region = NVL(vt.from_tax_region, vi1.vat_region)
                                 and vi1.active_date = NVL(vt.active_date, vi1.active_date)
                                 and vi1.vat_type ='C' and rownum = 1)
                     and exists (select 'x'
                                 from vat_item vi2
                               where vi2.item = NVL(vt.item, vi2.item)
                                 and vi2.vat_region = NVL(vt.from_tax_region, vi2.vat_region)
                                 and vi2.active_date = NVL(vt.active_date, vi2.active_date)
                                 and vi2.vat_type ='R' and rownum = 1)
                    )
                   )
              )
             );
     
BEGIN

   -- Retrieve tax information from the vat_item table
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TAX_INFO',
                    'vat_item, OBJ_TAX_INFO_TBL',
                    NULL);
   open  C_GET_TAX_INFO;

   IO_tax_info_tbl.delete();

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TAX_INFO',
                    'vat_item, OBJ_TAX_INFO_TBL',
                    NULL);

   fetch C_GET_TAX_INFO bulk collect into IO_tax_info_tbl;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TAX_INFO',
                    'vat_item, OBJ_TAX_INFO_TBL',
                    NULL);
   close C_GET_TAX_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END GET_VAT_INFO;
-----------------------------------------------------------------------------------------------------
FUNCTION ITEMLIST_TAX_EXPLODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              O_run_report      IN OUT            BOOLEAN,
                              IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50)               := 'TAX_SQL.ITEMLIST_TAX_EXPLODE';

BEGIN

   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if ITEMLIST_VAT_EXPLODE(O_error_message,
                              O_run_report,
                              IO_tax_info_tbl) = FALSE then
         return TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
   return FALSE;
END ITEMLIST_TAX_EXPLODE;
-----------------------------------------------------------------------------------------------------
FUNCTION ITEMLIST_VAT_EXPLODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              O_run_report      IN OUT            BOOLEAN,
                              IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(50)               := 'TAX_SQL.ITEMLIST_VAT_EXPLODE';
   L_tax_rej_tbl       OBJ_TAX_INFO_TBL           := OBJ_TAX_INFO_TBL();
   L_error_key         RTK_ERRORS.RTK_KEY%TYPE;
   L_user              VARCHAR2(30)               := get_user;

   cursor C_ITEMLIST_EXPLODE is
      select OBJ_TAX_INFO_REC(vt.item,
                              vt.item_parent,
                              vt.item_grandparent,
                              vt.pack_ind,
                              vt.merch_hier_level,
                              vt.merch_hier_value,
                              vt.from_tax_region,
                              vt.from_tax_region_desc,
                              vt.to_tax_region,
                              vt.from_entity,
                              vt.from_entity_type,
                              vt.to_entity,
                              vt.to_entity_type,
                              vt.amount,
                              vt.cost_retail_ind,
                              vt.tax_incl_ind,
                              vt.tax_amount,
                              vt.tax_rate,
                              vt.tax_code,
                              vt.tax_code_desc,
                              vt.active_date,
                              vt.inventory_ind,
                              vt.ref_no_1,
                              vt.ref_no_2,
                              vt.tran_code,
                              vt.taxable_base,
                              vt.modified_taxable_base,
                              vt.calculation_basis,
                              vt.recoverable_amount,
                              vt.currency,
                              vt.reverse_vat_ind)
      from
       (select distinct im.item,
                              vt.item_parent,
                              vt.item_grandparent,
                              vt.pack_ind,
                              vt.merch_hier_level,
                              vt.merch_hier_value,
                              vt.from_tax_region,
                              vt.from_tax_region_desc,
                              vt.to_tax_region,
                              vt.from_entity,
                              vt.from_entity_type,
                              vt.to_entity,
                              vt.to_entity_type,
                              vt.amount,
                              vt.cost_retail_ind,
                              vt.tax_incl_ind,
                              vt.tax_amount,
                              vt.tax_rate,
                              vt.tax_code,
                              vt.tax_code_desc,
                              vt.active_date,
                              vt.inventory_ind,
                              vt.ref_no_1,
                              vt.ref_no_2,
                              vt.tran_code,
                              vt.taxable_base,
                              vt.modified_taxable_base,
                              vt.calculation_basis,
                              vt.recoverable_amount,
                              vt.currency,
                              vt.reverse_vat_ind
        from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vt,
             item_master im,
             skulist_detail sd
       where sd.skulist = vt.item
         and (im.item = sd.item or
              im.item_parent = sd.item or
              im.item_grandparent = sd.item)
         and im.tran_level >= im.item_level
         and (im.pack_ind = 'N' or im.pack_type = 'V')) vt;

    cursor C_TAX_REJECT is
          select OBJ_TAX_INFO_REC(vt.item,
                                  vt.item_parent,
                                  vt.item_grandparent,
                                  vt.pack_ind,
                                  vt.merch_hier_level,
                                  vt.merch_hier_value,
                                  vt.from_tax_region,
                                  vt.from_tax_region_desc,
                                  vt.to_tax_region,
                                  vt.from_entity,
                                  vt.from_entity_type,
                                  vt.to_entity,
                                  vt.to_entity_type,
                                  vt.amount,
                                  vt.cost_retail_ind,
                                  vt.tax_incl_ind,
                                  vt.tax_amount,
                                  vt.tax_rate,
                                  vt.tax_code,
                                  vt.tax_code_desc,
                                  vt.active_date,
                                  vt.inventory_ind,
                                  vt.ref_no_1,
                                  vt.ref_no_2,
                                  vt.tran_code,
                                  vt.taxable_base,
                                  vt.modified_taxable_base,
                                  vt.calculation_basis,
                                  vt.recoverable_amount,
                                  vt.currency,
                                  vt.reverse_vat_ind)
            from vat_item vi,
                 TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vt
           where vi.item = vt.item
             and vi.vat_region = vt.from_tax_region
             and vi.active_date = vt.active_date
             and ((vi.vat_type = LP_TAX_TYPE_BOTH and vt.cost_retail_ind in (LP_TAX_TYPE_COST, LP_TAX_TYPE_RETAIL)) or
              (vi.vat_type in (LP_TAX_TYPE_COST, LP_TAX_TYPE_RETAIL) and vt.cost_retail_ind = LP_TAX_TYPE_BOTH));

BEGIN

      open C_ITEMLIST_EXPLODE;
      IO_tax_info_tbl.DELETE();
      fetch C_ITEMLIST_EXPLODE BULK COLLECT into IO_tax_info_tbl;
      close C_ITEMLIST_EXPLODE;
      --
      -- Cursor to check for rejected records
      O_run_report := FALSE;

      open  C_TAX_REJECT;
      fetch C_TAX_REJECT bulk collect into L_tax_rej_tbl;
      close C_TAX_REJECT;
      -- If there are rejected records, call INSERT_REJECTS
      -- to write the rejected records into the database
      if L_tax_rej_tbl.count != 0 and
         L_tax_rej_tbl(L_tax_rej_tbl.count).item is NOT NULL then
         ---
         O_run_report := TRUE;
         ---
         FOR i in 1..L_tax_rej_tbl.count LOOP
            ---
            if L_tax_rej_tbl(i).cost_retail_ind = 'B' then
               L_error_key := 'NO_UPD_EXIST_VAT';
            else
               L_error_key := 'NO_UPD_VAT_TYPE';
            end if;
            ---
            if ITEMLIST_MC_REJECTS_SQL.INSERT_REJECTS(O_error_message,
                                                      L_tax_rej_tbl(i).item,
                                                      NULL,
                                                      NULL,
                                                      'V',
                                                      L_error_key,
                                                      L_user,
                                                      L_tax_rej_tbl(i).item,
                                                      NULL,
                                                      NULL) = FALSE then
                return FALSE;
             end if;
             ---
          END LOOP;
          ---
      end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
   return FALSE;
END ITEMLIST_VAT_EXPLODE;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_RATE_BY_CODE(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_tax_code_rate_tbl    IN OUT   NOCOPY   OBJ_TAX_CODE_RATE_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.GET_TAX_RATE_BY_CODE';

BEGIN
   if IO_tax_code_rate_tbl is NULL or IO_tax_code_rate_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX , LP_SALES)then
      -- not supported for GTAX
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if TAX_SQL.GET_VAT_RATE_BY_CODE(O_error_message,
                                      IO_tax_code_rate_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TAX_RATE_BY_CODE;
------------------------------------------------------------------------------------------------------
FUNCTION GET_VAT_RATE_BY_CODE(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_tax_code_rate_tbl    IN OUT   NOCOPY   OBJ_TAX_CODE_RATE_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.GET_VAT_RATE_BY_CODE';

   L_input_count   NUMBER;
   L_output_count  NUMBER;

   cursor C_VAT_RATE is
      select OBJ_TAX_CODE_RATE_REC(input.I_tax_code,
                                   input.I_tax_type,
                                   input.I_effective_from_date,
                                   vr.vat_rate,  --O_tax_rate
                                   'P',          --O_calculation_basis, always 'P'ercent for SVAT
                                   NULL)         --O_currency_code, N/A for SVAT
        from TABLE(CAST(IO_tax_code_rate_tbl AS OBJ_TAX_CODE_RATE_TBL)) input,
             vat_code_rates vr
       where vr.vat_code = input.I_tax_code
         and vr.active_date = (select MAX(active_date)
                                 from vat_code_rates vr2
                                where vr2.vat_code = input.I_tax_code
                                  and vr2.active_date <= input.I_effective_from_date);

BEGIN

   L_input_count := IO_tax_code_rate_tbl.COUNT;

   open C_VAT_RATE;
   IO_tax_code_rate_tbl.delete();
   fetch C_VAT_RATE bulk collect into IO_tax_code_rate_tbl;
   close C_VAT_RATE;

   L_output_count := IO_tax_code_rate_tbl.COUNT;

   if L_input_count != L_output_count then
      O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_VAT_RATE_BY_CODE;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INCLUSIVE_RETAIL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_amount              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                  I_dept                IN       ITEM_MASTER.DEPT%TYPE,
                                  I_class               IN       ITEM_MASTER.CLASS%TYPE,
                                  I_location            IN       ITEM_LOC.LOC%TYPE,
                                  I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                                  I_effective_from_date IN       DATE,
                                  I_amount              IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT 1)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.GET_TAX_INCLUSIVE_RETAIL';

   L_tax_add_tbl OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   L_tax_add_rec OBJ_TAX_RETAIL_ADD_REMOVE_REC;

BEGIN
   L_tax_add_rec := OBJ_TAX_RETAIL_ADD_REMOVE_REC(
                       I_item,                 --I_ITEM
                       I_dept,                 --I_DEPT
                       I_class,                --I_CLASS
                       I_location,             --I_LOCATION
                       I_loc_type,             --I_LOC_TYPE
                       I_effective_from_date,  --I_EFFECTIVE_FROM_DATE
                       I_amount,               --I_AMOUNT
                       I_qty,                  --I_QTY
                       NULL);                  --O_AMOUNT
   L_tax_add_tbl.EXTEND;
   L_tax_add_tbl(L_tax_add_tbl.COUNT)   := L_tax_add_rec;
   ---
   if TAX_SQL.GET_TAX_INCLUSIVE_RETAIL(O_error_message,
                                       L_tax_add_tbl)= FALSE then
      return FALSE;
   end if;
   ---
   O_amount := L_tax_add_tbl(1).O_amount;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TAX_INCLUSIVE_RETAIL;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_EXCLUSIVE_RETAIL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_amount              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                  I_dept                IN       ITEM_MASTER.DEPT%TYPE,
                                  I_class               IN       ITEM_MASTER.CLASS%TYPE,
                                  I_location            IN       ITEM_LOC.LOC%TYPE,
                                  I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                                  I_effective_from_date IN       DATE,
                                  I_amount              IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                                  I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT 1)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL';

   L_tax_remove_tbl OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   L_tax_remove_rec OBJ_TAX_RETAIL_ADD_REMOVE_REC;

BEGIN
   L_tax_remove_rec := OBJ_TAX_RETAIL_ADD_REMOVE_REC(
                          I_item,                 --I_ITEM
                          I_dept,                 --I_DEPT
                          I_class,                --I_CLASS
                          I_location,             --I_LOCATION
                          I_loc_type,             --I_LOC_TYPE
                          I_effective_from_date,  --I_EFFECTIVE_FROM_DATE
                          I_amount,               --I_AMOUNT
                          I_qty,                  --I_QTY
                          NULL);                  --O_AMOUNT
   L_tax_remove_tbl.EXTEND;
   L_tax_remove_tbl(L_tax_remove_tbl.COUNT)   := L_tax_remove_rec;
   ---
   if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                                       L_tax_remove_tbl)= FALSE then
      return FALSE;
   end if;
   ---
   O_amount := L_tax_remove_tbl(1).O_amount;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TAX_EXCLUSIVE_RETAIL;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_INCLUSIVE_RETAIL(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.GET_TAX_INCLUSIVE_RETAIL';

   L_add_tax_tbl       OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   L_do_nothing_tbl    OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();

   cursor C_ADD_TAX_COLLECTION is
      select OBJ_TAX_RETAIL_ADD_REMOVE_REC(input.I_item,
                                           input.I_dept,
                                           input.I_class,
                                           input.I_location,
                                           input.I_loc_type,
                                           input.I_effective_from_date,
                                           input.I_amount,
                                           input.I_qty,
                                           input.O_amount)
        from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input,
             class cl
       where input.I_dept = cl.dept
         and input.I_class = cl.class
         and cl.class_vat_ind = 'N';

   cursor C_DO_NOTHING_COLLECTION is
      select OBJ_TAX_RETAIL_ADD_REMOVE_REC(input.I_item,
                                           input.I_dept,
                                           input.I_class,
                                           input.I_location,
                                           input.I_loc_type,
                                           input.I_effective_from_date,
                                           input.I_amount,
                                           input.I_qty,
                                           input.I_amount) -- In this case , the output amount will be same as input as there are no taxes
        from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input,
             class cl
       where input.I_dept = cl.dept
         and input.I_class = cl.class
         and cl.class_vat_ind = 'Y';

    cursor C_COMBINE_RESULTS is
       select OBJ_TAX_RETAIL_ADD_REMOVE_REC(rslt.I_item,
                                            rslt.I_dept,
                                            rslt.I_class,
                                            rslt.I_location,
                                            rslt.I_loc_type,
                                            rslt.I_effective_from_date,
                                            rslt.I_amount,
                                            rslt.I_qty,
                                            rslt.O_amount)
         from (select input1.I_item,
                      input1.I_dept,
                      input1.I_class,
                      input1.I_location,
                      input1.I_loc_type,
                      input1.I_effective_from_date,
                      input1.I_amount,
                      input1.I_qty,
                      input1.O_amount
                 from TABLE(CAST(L_add_tax_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input1
                union all
               select input2.I_item,
                      input2.I_dept,
                      input2.I_class,
                      input2.I_location,
                      input2.I_loc_type,
                      input2.I_effective_from_date,
                      input2.I_amount,
                      input2.I_qty,
                      input2.O_amount
                 from TABLE(CAST(L_do_nothing_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input2) rslt;

BEGIN

   if IO_tax_add_remove_tbl is NULL or IO_tax_add_remove_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   -- If tax is not used (zero tax), tax inclusive retail is the same as input retail.
   if LP_system_options_row.default_tax_type = LP_SALES  then
      ---
      for i in IO_tax_add_remove_tbl.FIRST .. IO_tax_add_remove_tbl.LAST loop
         IO_tax_add_remove_tbl(i).O_amount := IO_tax_add_remove_tbl(i).I_amount;
      end loop;
      ---
      return TRUE;
   end if;
   -- If GTAX is used, unit retail is always tax inclusive.
   -- If SVAT is used, unit retail is always tax inclusive if system's class_level_vat_ind is 'N'.
   if  LP_system_options_row.default_tax_type = LP_GTAX or
      (LP_system_options_row.default_tax_type in (LP_SVAT, LP_SALES) and
       LP_system_options_row.class_level_vat_ind = 'N') then
      ---
      for i in IO_tax_add_remove_tbl.FIRST .. IO_tax_add_remove_tbl.LAST loop
         IO_tax_add_remove_tbl(i).O_amount := IO_tax_add_remove_tbl(i).I_amount;
      end loop;
      ---
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type = LP_SVAT and
      LP_system_options_row.class_level_vat_ind = 'Y' then
      -- unit retail is tax inclusive or exclusive depending on CLASS.CLASS_VAT_IND
      -- split input into 2 collections:
      -- 1) vat is not in retail, add tax to retail

      open C_ADD_TAX_COLLECTION;
      fetch C_ADD_TAX_COLLECTION bulk collect into L_add_tax_tbl;
      close C_ADD_TAX_COLLECTION;

      if L_add_tax_tbl is NOT NULL and L_add_tax_tbl.count > 0 then
         if ADD_VAT_RETAIL_TAX(O_error_message,
                               L_add_tax_tbl) = FALSE then
            return FALSE;
         end if;
      end if;

      -- 2) vat is already in retail, do nothing
      open C_DO_NOTHING_COLLECTION;
      fetch C_DO_NOTHING_COLLECTION bulk collect into L_do_nothing_tbl;
      close C_DO_NOTHING_COLLECTION;

      -- combine the 2 collections into 1
      open C_COMBINE_RESULTS;
      IO_tax_add_remove_tbl.delete();
      fetch C_COMBINE_RESULTS bulk collect into IO_tax_add_remove_tbl;
      close C_COMBINE_RESULTS;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TAX_INCLUSIVE_RETAIL;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_EXCLUSIVE_RETAIL(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL';

   L_remove_tax_tbl    OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   L_do_nothing_tbl    OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();

   cursor C_REMOVE_TAX_COLLECTION is
      select OBJ_TAX_RETAIL_ADD_REMOVE_REC(input.I_item,
                                           input.I_dept,
                                           input.I_class,
                                           input.I_location,
                                           input.I_loc_type,
                                           input.I_effective_from_date,
                                           input.I_amount,
                                           input.I_qty,
                                           input.O_amount)
        from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input,
             class cl
       where input.I_dept = cl.dept
         and input.I_class = cl.class
         and cl.class_vat_ind = 'Y';

   cursor C_DO_NOTHING_COLLECTION is
      select OBJ_TAX_RETAIL_ADD_REMOVE_REC(input.I_item,
                                           input.I_dept,
                                           input.I_class,
                                           input.I_location,
                                           input.I_loc_type,
                                           input.I_effective_from_date,
                                           input.I_amount,
                                           input.I_qty,
                                           input.I_amount) -- In this case , the output amount will be same as input as there are no taxes
        from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input,
             class cl
       where input.I_dept = cl.dept
         and input.I_class = cl.class
         and cl.class_vat_ind = 'N';

    cursor C_COMBINE_RESULTS is
       select OBJ_TAX_RETAIL_ADD_REMOVE_REC(rslt.I_item,
                                            rslt.I_dept,
                                            rslt.I_class,
                                            rslt.I_location,
                                            rslt.I_loc_type,
                                            rslt.I_effective_from_date,
                                            rslt.I_amount,
                                            rslt.I_qty,
                                            rslt.O_amount)
         from (select input1.I_item,
                      input1.I_dept,
                      input1.I_class,
                      input1.I_location,
                      input1.I_loc_type,
                      input1.I_effective_from_date,
                      input1.I_amount,
                      input1.I_qty,
                      input1.O_amount
                 from TABLE(CAST(L_remove_tax_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input1
                union all
               select input2.I_item,
                      input2.I_dept,
                      input2.I_class,
                      input2.I_location,
                      input2.I_loc_type,
                      input2.I_effective_from_date,
                      input2.I_amount,
                      input2.I_qty,
                      input2.O_amount
                 from TABLE(CAST(L_do_nothing_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input2) rslt;

BEGIN

   if IO_tax_add_remove_tbl is NULL or IO_tax_add_remove_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   -- If tax is not used (zero tax), tax exclusive retail is the same as input retail.
   if LP_system_options_row.default_tax_type = LP_SALES  then
      ---
      for i in IO_tax_add_remove_tbl.FIRST .. IO_tax_add_remove_tbl.LAST loop
         IO_tax_add_remove_tbl(i).O_amount := IO_tax_add_remove_tbl(i).I_amount;
      end loop;
      ---
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type = LP_GTAX then
      -- If GTAX is used, unit retail is always tax inclusive, remove tax.
      if REMOVE_GTAX_RETAIL_TAX(O_error_message,
                                IO_tax_add_remove_tbl) = FALSE then
         return FALSE;
      end if;
   elsif  LP_system_options_row.default_tax_type = LP_SVAT then
      if LP_system_options_row.class_level_vat_ind = 'N' then
         -- If SVAT is used, unit retail is always tax inclusive if system's class_level_vat_ind is 'N', remove tax.
         if REMOVE_VAT_RETAIL_TAX(O_error_message,
                                  IO_tax_add_remove_tbl) = FALSE then
            return FALSE;
         end if;
      else  -- system's class_level_vat_ind is 'Y'
         -- unit retail is tax inclusive or exclusive depending on CLASS.CLASS_VAT_IND
         -- split input into 2 collections:
         -- 1) vat is in retail, remove vat
         open C_REMOVE_TAX_COLLECTION;
         fetch C_REMOVE_TAX_COLLECTION bulk collect into L_remove_tax_tbl;
         close C_REMOVE_TAX_COLLECTION;

         if L_remove_tax_tbl is NOT NULL and L_remove_tax_tbl.count > 0 then
            if REMOVE_VAT_RETAIL_TAX(O_error_message,
                                     L_remove_tax_tbl) = FALSE then
               return FALSE;
            end if;
         end if;

         -- 2) vat is not in retail, do nothing
         open C_DO_NOTHING_COLLECTION;
         fetch C_DO_NOTHING_COLLECTION bulk collect into L_do_nothing_tbl;
         close C_DO_NOTHING_COLLECTION;

         -- combine the 2 collections into 1
         IO_tax_add_remove_tbl.delete();
         open C_COMBINE_RESULTS;
         fetch C_COMBINE_RESULTS bulk collect into IO_tax_add_remove_tbl;
         close C_COMBINE_RESULTS;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TAX_EXCLUSIVE_RETAIL;
------------------------------------------------------------------------------------------------------
FUNCTION ADD_VAT_RETAIL_TAX(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.ADD_VAT_RETAIL_TAX';

BEGIN
   if ADD_REMOVE_VAT_RETAIL_TAX(O_error_message,
                                IO_tax_add_remove_tbl,
                                LP_ADD_TAX) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END ADD_VAT_RETAIL_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_VAT_RETAIL_TAX(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.REMOVE_VAT_RETAIL_TAX';

BEGIN
   if ADD_REMOVE_VAT_RETAIL_TAX(O_error_message,
                                IO_tax_add_remove_tbl,
                                LP_REMOVE_TAX) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END REMOVE_VAT_RETAIL_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION ADD_REMOVE_VAT_RETAIL_TAX(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                   IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL,
                                   I_add_remove_ind        IN                VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.ADD_REMOVE_VAT_RETAIL_TAX';

   L_tax_calc_tbl   OBJ_TAX_CALC_TBL;
   L_input_count    NUMBER;
   L_output_count   NUMBER;

   cursor C_BUILD_CALC_OBJ is
      select OBJ_TAX_CALC_REC(input.I_item,
                              im.pack_ind,
                              input.I_location,  --I_from_entity
                              decode(input.I_loc_type, 'S', 'ST', 'W', 'WH', input.I_loc_type), --I_from_entity_type
                              NULL,  --I_to_entity
                              NULL,  --I_to_entity_type
                              input.I_effective_from_date,
                              input.I_amount,
                              NULL,  --I_amount_curr, n/a for SVAT since it's only percent rate
                              'N',  --I_amount_tax_incl_ind, default to 'N'
                              NULL,  --I_origin_country_id
                              NULL,  --O_cum_tax_pct
                              NULL,  --O_cum_tax_value
                              NULL,  --O_total_tax_amount
                              NULL,  --O_total_tax_amount_curr
                              NULL,  --O_total_recover_amount
                              NULL,  --O_total_recover_amount_curr
                              NULL)  --O_tax_detail_tbl
        from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input,
             item_master im
       where input.I_item = im.item;

   cursor C_ADD_TAX is
      select OBJ_TAX_RETAIL_ADD_REMOVE_REC(input.I_item,
                                           input.I_dept,
                                           input.I_class,
                                           input.I_location,
                                           input.I_loc_type,
                                           input.I_effective_from_date,
                                           calc_obj.I_amount,
                                           NULL,  -- doesn't affect SVAT since it's only percent rate
                                           calc_obj.I_amount*(1+calc_obj.O_cum_tax_pct/100)) --O_amount
        from TABLE(CAST(L_tax_calc_tbl AS OBJ_TAX_CALC_TBL)) calc_obj,
             (select distinct
                     I_item,
                     I_location,
                     I_loc_type,
                     I_effective_from_date,
                     I_dept,
                     I_class
                from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL))) input
       where calc_obj.I_item = input.I_item
         and calc_obj.I_from_entity = input.I_location
         and calc_obj.I_from_entity_type = decode(input.I_loc_type, 'S', 'ST', 'W', 'WH', input.I_loc_type)
         and calc_obj.I_effective_from_date = input.I_effective_from_date;


   cursor C_REMOVE_TAX is
      select OBJ_TAX_RETAIL_ADD_REMOVE_REC(input.I_item,
                                           input.I_dept,
                                           input.I_class,
                                           input.I_location,
                                           input.I_loc_type,
                                           input.I_effective_from_date,
                                           calc_obj.I_amount,
                                           NULL,  -- doesn't affect SVAT since it's only percent rate
                                           --UnitRetailExlTax =  UnitRetailIncTax/(1+Tax Rate)
                                           calc_obj.I_amount/(1+calc_obj.O_cum_tax_pct/100)) --O_amount
        from TABLE(CAST(L_tax_calc_tbl AS OBJ_TAX_CALC_TBL)) calc_obj,
             (select distinct
                     I_item,
                     I_location,
                     I_loc_type,
                     I_effective_from_date,
                     I_dept,
                     I_class
                from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL))) input
       where calc_obj.I_item = input.I_item
         and calc_obj.I_from_entity = input.I_location
         and calc_obj.I_from_entity_type = decode(input.I_loc_type, 'S', 'ST', 'W', 'WH', input.I_loc_type)
         and calc_obj.I_effective_from_date = input.I_effective_from_date;

BEGIN

   L_input_count := IO_tax_add_remove_tbl.COUNT;

   -- construct OBJ_TAX_CALC_TBL records based on OBJ_TAX_RETAIL_ADD_REMOVE_TBL
   open C_BUILD_CALC_OBJ;
   fetch C_BUILD_CALC_OBJ bulk collect into L_tax_calc_tbl;
   close C_BUILD_CALC_OBJ;

   if CALC_VAT_RETAIL_TAX(O_error_message,
                          L_tax_calc_tbl) = FALSE then
      return FALSE;
   end if;

   -- construct OBJ_TAX_RETAIL_ADD_REMOVE_TBL records based on OBJ_TAX_CALC_TBL
   if I_add_remove_ind = LP_ADD_TAX then
      open C_ADD_TAX;
      IO_tax_add_remove_tbl.delete();
      fetch C_ADD_TAX bulk collect into IO_tax_add_remove_tbl;
      close C_ADD_TAX;
   else  --LP_REMOVE_TAX
      open C_REMOVE_TAX;
      IO_tax_add_remove_tbl.delete();
      fetch C_REMOVE_TAX bulk collect into IO_tax_add_remove_tbl;
      close C_REMOVE_TAX;
   end if;

   -- check count
   L_output_count := IO_tax_add_remove_tbl.COUNT;
   if L_input_count != L_output_count then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_OUTPUT',
                                            L_program,
                                            L_input_count,
                                            L_output_count);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END ADD_REMOVE_VAT_RETAIL_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_GTAX_RETAIL_TAX(O_error_message         IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_add_remove_tbl   IN OUT   NOCOPY   OBJ_TAX_RETAIL_ADD_REMOVE_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.REMOVE_GTAX_RETAIL_TAX';

   L_input_count   NUMBER;
   L_output_count  NUMBER;

   cursor C_REMOVE_TAX is
      select OBJ_TAX_RETAIL_ADD_REMOVE_REC(input.I_item,
                                           input.I_dept,
                                           input.I_class,
                                           input.I_location,
                                           input.I_loc_type,
                                           input.I_effective_from_date,
                                           --Assumption: I_amount and O_amount are both in location currency,
                                           --which is the same as GTAX_ITEM_ROLLUP.CUM_TAX_VALUE
                                           input.I_amount,  -- input: retail with tax
                                           input.I_qty,     -- input: qty associated with the I_amount
                                           --UnitRetailExlTax =  UnitRetailIncTax - (UnitRetailIncTax - Tax Value)*Tax Rate - Tax Value
                                           input.I_amount-(input.I_amount-NVL(gt.cum_tax_value,0)*NVL(input.I_qty, 1))*NVL(gt.cum_tax_pct,0)/100-NVL(gt.cum_tax_value,0)*NVL(input.I_qty, 1))   --O_amount
        from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL)) input,
             (select item,
                     loc,
                     loc_type,
                     effective_from_date,
                     cum_tax_pct,
                     cum_tax_value,
                     rank() over(partition by item, loc, loc_type order by effective_from_date desc) date_rank
                from (select distinct
                             I_item,
                             I_location,
                             I_loc_type,
                             I_effective_from_date
                        from TABLE(CAST(IO_tax_add_remove_tbl AS OBJ_TAX_RETAIL_ADD_REMOVE_TBL))) input,
                     gtax_item_rollup
               where item = input.I_item
                 and loc = input.I_location
                 and loc_type = DECODE(input.I_loc_type, 'S', 'ST', 'W', 'WH', input.I_loc_type)
                 and effective_from_date <= input.I_effective_from_date) gt
       where gt.item(+) = input.I_item
         and gt.loc(+) = input.I_location
         and gt.loc_type(+) = DECODE(input.I_loc_type, 'S', 'ST', 'W', 'WH', input.I_loc_type)
         and gt.date_rank(+) = 1;

BEGIN

   L_input_count := IO_tax_add_remove_tbl.COUNT;

   open C_REMOVE_TAX;
   IO_tax_add_remove_tbl.delete();
   fetch C_REMOVE_TAX bulk collect into IO_tax_add_remove_tbl;
   close C_REMOVE_TAX;

   L_output_count := IO_tax_add_remove_tbl.COUNT;

   if L_input_count != L_output_count then
      O_error_message := SQL_LIB.CREATE_MSG('TAX_NOTFOUND',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END REMOVE_GTAX_RETAIL_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_COST_TAX(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_tax_calc_tbl       IN OUT   NOCOPY   OBJ_TAX_CALC_TBL)
RETURN BOOLEAN IS

   L_program                       VARCHAR2(64) := 'TAX_SQL.CALC_COST_TAX';
   L_from_vat_region               VAT_REGION.VAT_REGION%TYPE;
   L_from_vat_calc_type            VAT_REGION.VAT_CALC_TYPE%TYPE;
   L_to_vat_region                 VAT_REGION.VAT_REGION%TYPE;
   L_to_vat_calc_type              VAT_REGION.VAT_CALC_TYPE%TYPE;
   L_tax_calc_tbl                  OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_tax_calc_rec                  OBJ_TAX_CALC_REC ;
   L_simple_tax_calc_tbl           OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_custom_tax_calc_tbl           OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_others_tax_calc_tbl           OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   ---
   cursor C_FROM_VAT_REGION is
      select s.vat_region
        from store s,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where s.store = tbl.I_from_entity
         and tbl.I_from_entity_type  = 'ST'
      union all
      select w.vat_region
        from wh w,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where w.wh = tbl.I_from_entity
         and tbl.I_from_entity_type  = 'WH'
      union all
      select p.vat_region
        from partner p,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where p.partner_id = tbl.I_from_entity
         and p.partner_type = 'E'
         and tbl.I_from_entity_type  = 'E'
      union all
      select sp.vat_region
        from sups sp,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where sp.supplier = tbl.I_from_entity
         and tbl.I_from_entity_type  in ('SP','SU');

   cursor C_TO_VAT_REGION is
      select s.vat_region
        from store s,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where s.store = tbl.I_to_entity
         and tbl.I_to_entity_type  = 'ST'
      union all
      select w.vat_region
        from wh w,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where w.wh = tbl.I_to_entity
         and tbl.I_to_entity_type  = 'WH'
      union all
      select p.vat_region
        from partner p,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where p.partner_id = tbl.I_to_entity
         and p.partner_type = 'E'
         and tbl.I_to_entity_type  = 'E'
      union all
      select sp.vat_region
        from sups sp,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where sp.supplier = tbl.I_to_entity
         and tbl.I_to_entity_type  in ('SP','SU');
         

BEGIN
   if IO_tax_calc_tbl is NULL or IO_tax_calc_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;
    
   
   if LP_system_options_row.default_tax_type = LP_GTAX then
      if CALC_GTAX_COST_TAX(O_error_message,
                            L_tax_calc_tbl) = FALSE then
         return FALSE;
      end if;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      ---
      L_others_tax_calc_tbl.DELETE();
      L_simple_tax_calc_tbl.DELETE();
      L_custom_tax_calc_tbl.DELETE();
      for rec in IO_tax_calc_tbl.FIRST..IO_tax_calc_tbl.LAST LOOP
         L_tax_calc_rec := IO_tax_calc_tbl(rec);
         L_tax_calc_tbl.DELETE();
         L_tax_calc_tbl.EXTEND();
         L_tax_calc_tbl(L_tax_calc_tbl.count) := L_tax_calc_rec;
         
         if L_tax_calc_rec.I_from_entity is not NULL then
            L_from_vat_region := NULL;
            L_from_vat_calc_type := NULL;
            open C_FROM_VAT_REGION;
            fetch C_FROM_VAT_REGION into L_from_vat_region;
            close C_FROM_VAT_REGION;
         
            if TAX_SQL.FETCH_VAT_APPL_TYPE (O_error_message, 
                                            L_from_vat_region,        
                                            L_from_vat_calc_type) = FALSE then     
               return FALSE;
            end if;
         end if;

         if L_tax_calc_rec.I_to_entity is not NULL then   
            L_to_vat_region := NULL;
            L_to_vat_calc_type := NULL;
            open C_TO_VAT_REGION;
            fetch C_TO_VAT_REGION into L_to_vat_region;
            close C_TO_VAT_REGION;
            
            if TAX_SQL.FETCH_VAT_APPL_TYPE (O_error_message, 
                                            L_to_vat_region,        
                                            L_to_vat_calc_type) = FALSE then     
               return FALSE;
            end if;
            
         end if;
         
         if (L_from_vat_region is not NULL and L_to_vat_region is not NULL ) then
            if L_from_vat_calc_type = 'S' and L_to_vat_calc_type = 'S' then 
               if L_from_vat_region = L_to_vat_region then
                  L_simple_tax_calc_tbl.EXTEND();
                  L_simple_tax_calc_tbl(L_simple_tax_calc_tbl.count) := L_tax_calc_rec;
               else
                  L_others_tax_calc_tbl.EXTEND();
                  L_others_tax_calc_tbl(L_others_tax_calc_tbl.count) := L_tax_calc_rec;
               end if;
               ---
            elsif ((L_from_vat_calc_type = 'E' or L_to_vat_calc_type = 'E') and  (L_from_vat_calc_type <> 'C' and L_to_vat_calc_type <> 'C')) then 
               -- For 'E'xempt regions, set tax_exempt_ind = 'Y' 
               L_tax_calc_rec.O_tax_exempt_ind := 'Y';
               L_others_tax_calc_tbl.EXTEND();
               L_others_tax_calc_tbl(L_others_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            elsif (L_from_vat_calc_type = 'C' or L_to_vat_calc_type = 'C') then
               L_custom_tax_calc_tbl.EXTEND();
               L_custom_tax_calc_tbl(L_custom_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            else
               L_others_tax_calc_tbl.EXTEND();
               L_others_tax_calc_tbl(L_others_tax_calc_tbl.count) := L_tax_calc_rec;
            end if; --if L_from_vat_calc_type = 'S' and L_to_vat_calc_type = 'S'
         elsif (L_from_vat_region is not NULL and L_to_vat_region is NULL ) then
            if L_from_vat_calc_type = 'S' then
               L_tax_calc_rec.O_tax_exempt_ind := 'N';
               L_simple_tax_calc_tbl.EXTEND();
               L_simple_tax_calc_tbl(L_simple_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            elsif L_from_vat_calc_type = 'E' then
               L_tax_calc_rec.O_tax_exempt_ind := 'Y';
               L_others_tax_calc_tbl.EXTEND();
               L_others_tax_calc_tbl(L_others_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            elsif L_from_vat_calc_type = 'C' then
               L_custom_tax_calc_tbl.EXTEND();
               L_custom_tax_calc_tbl(L_custom_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            end if;--if L_from_vat_calc_type = 'S' 
         end if;--if (L_from_vat_region is not NULL and L_to_vat_region is not NULL )
         ---
      END LOOP;
      IO_tax_calc_tbl.DELETE();
      if L_others_tax_calc_tbl is not null and L_others_tax_calc_tbl.count > 0 then
         for rec in L_others_tax_calc_tbl.FIRST..L_others_tax_calc_tbl.LAST LOOP
            IO_tax_calc_tbl.EXTEND();
            IO_tax_calc_tbl(IO_tax_calc_tbl.count) := L_others_tax_calc_tbl(rec);
         END LOOP;
      end if;
      if L_simple_tax_calc_tbl is not null and L_simple_tax_calc_tbl.count > 0 then
         if CALC_VAT_COST_TAX(O_error_message,
                              L_simple_tax_calc_tbl) = FALSE then
             return FALSE;
         end if;
         for rec in L_simple_tax_calc_tbl.FIRST..L_simple_tax_calc_tbl.LAST LOOP
            IO_tax_calc_tbl.EXTEND();
            IO_tax_calc_tbl(IO_tax_calc_tbl.count) := L_simple_tax_calc_tbl(rec);
         END LOOP;
      end if;
      if L_custom_tax_calc_tbl is not null and L_custom_tax_calc_tbl.count > 0 then
         if CUSTOM_TAX_SQL.CALC_CTAX_COST_TAX(O_error_message,
                                              L_custom_tax_calc_tbl) = FALSE then
            return FALSE;                                                 
         end if;
         for rec in L_custom_tax_calc_tbl.FIRST..L_custom_tax_calc_tbl.LAST LOOP
            IO_tax_calc_tbl.EXTEND();
            IO_tax_calc_tbl(IO_tax_calc_tbl.count) := L_custom_tax_calc_tbl(rec);
         END LOOP;
      end if;
      
   end if; -- Tax_type
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CALC_COST_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_calc_tbl   IN OUT   NOCOPY   OBJ_TAX_CALC_TBL,
                         I_call_type       IN                VARCHAR2 DEFAULT LP_CALL_TYPE_DEFAULT)
RETURN BOOLEAN IS

   L_program                       VARCHAR2(64)                   := 'TAX_SQL.CALC_RETAIL_TAX';
   L_from_vat_region               VAT_REGION.VAT_REGION%TYPE;
   L_from_vat_calc_type            VAT_REGION.VAT_CALC_TYPE%TYPE;
   L_to_vat_region                 VAT_REGION.VAT_REGION%TYPE;
   L_to_vat_calc_type              VAT_REGION.VAT_CALC_TYPE%TYPE;
   L_tax_calc_tbl                  OBJ_TAX_CALC_TBL              := OBJ_TAX_CALC_TBL (); 
   L_tax_calc_rec                  OBJ_TAX_CALC_REC;
   L_simple_tax_calc_tbl           OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_custom_tax_calc_tbl           OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_others_tax_calc_tbl           OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();

   --
   Cursor C_FROM_VAT_REGION is
     select s.vat_region
       from store s,
            TABLE(CAST (L_tax_calc_tbl as  OBJ_TAX_CALC_TBL)) tbl
      where s.store = tbl.I_from_entity
        and tbl.I_from_entity_type = 'ST' 
      union all
       select wh.vat_region
       from wh wh,
            TABLE(CAST (L_tax_calc_tbl as  OBJ_TAX_CALC_TBL)) tbl
      where wh.wh = tbl.I_from_entity
        and tbl.I_from_entity_type = 'WH'
       union all
       select p.vat_region
         from partner p,
            TABLE(CAST (L_tax_calc_tbl as  OBJ_TAX_CALC_TBL)) tbl
      where p.partner_id = tbl.I_from_entity
        and tbl.I_from_entity_type = 'E'
        and p.partner_type= 'E'
        union all
       select sp.vat_region
         from sups sp,
            TABLE(CAST (L_tax_calc_tbl as  OBJ_TAX_CALC_TBL)) tbl
      where sp.supplier            = tbl.I_from_entity
        and tbl.I_from_entity_type in ('SP','SU');

   cursor C_TO_VAT_REGION is
     select s.vat_region
       from store s,
            TABLE(CAST (L_tax_calc_tbl as  OBJ_TAX_CALC_TBL)) tbl
      where s.store = tbl.I_to_entity
        and tbl.I_to_entity_type = 'ST'	 
      union all
       select wh.vat_region
       from wh wh,
            TABLE(CAST (L_tax_calc_tbl as  OBJ_TAX_CALC_TBL)) tbl
      where wh.wh = tbl.I_to_entity
        and tbl.I_to_entity_type = 'WH'
       union all
       select p.vat_region
         from partner p,
            TABLE(CAST (L_tax_calc_tbl as  OBJ_TAX_CALC_TBL)) tbl
      where p.partner_id = tbl.I_to_entity
        and tbl.I_to_entity_type = 'E'
        and p.partner_type= 'E'
        union all
       select sp.vat_region
         from sups sp,
            TABLE(CAST (L_tax_calc_tbl as  OBJ_TAX_CALC_TBL)) tbl
      where sp.supplier = tbl.I_to_entity
        and tbl.I_to_entity_type in ('SP','SU');  
     
     
BEGIN
   if IO_tax_calc_tbl is NULL or IO_tax_calc_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type = LP_GTAX then
      if CALC_GTAX_RETAIL_TAX(O_error_message,
                              IO_tax_calc_tbl,
                              I_call_type) = FALSE then
         return FALSE;
      end if;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      L_others_tax_calc_tbl.DELETE();
      L_simple_tax_calc_tbl.DELETE();
      L_custom_tax_calc_tbl.DELETE();
      for rec in IO_tax_calc_tbl.first..IO_tax_calc_tbl.last LOOP
         -- Breaking the main collection IO_tax_calc_tbl into multiple collections for validation of vat appl type.
         L_tax_calc_rec := IO_tax_calc_tbl(rec);
         L_tax_calc_tbl.DELETE();
         L_tax_calc_tbl.EXTEND();
         L_tax_calc_tbl(L_tax_calc_tbl.count) := L_tax_calc_rec;

         if L_tax_calc_rec.I_from_entity is not null then
            L_from_vat_region := NULL;
            L_from_vat_calc_type := NULL;
            open C_FROM_VAT_REGION;
            fetch C_FROM_VAT_REGION into L_from_vat_region;
            close C_FROM_VAT_REGION;
            if TAX_SQL.FETCH_VAT_APPL_TYPE(O_error_message,
                                           L_from_vat_region,
                                           L_from_vat_calc_type) = FALSE then 
               return FALSE;
            end if;
         end if;
          
         if L_tax_calc_rec.I_to_entity is not null then
            L_to_vat_region := NULL;
            L_to_vat_calc_type := NULL;
            open C_TO_VAT_REGION;
            fetch C_TO_VAT_REGION into L_to_vat_region;
            close C_TO_VAT_REGION;
            if TAX_SQL.FETCH_VAT_APPL_TYPE(O_error_message,
                                           L_to_vat_region,
                                           L_to_vat_calc_type) = FALSE then
               return FALSE;
            end if;
         end if;
            
         if (L_from_vat_region is not NULL and L_to_vat_region is not NULL ) then
            if L_from_vat_calc_type = 'S' and L_to_vat_calc_type = 'S' then 
               if L_from_vat_region = L_to_vat_region then
                  L_simple_tax_calc_tbl.EXTEND();
                  L_simple_tax_calc_tbl(L_simple_tax_calc_tbl.count) := L_tax_calc_rec;
               else
                  L_others_tax_calc_tbl.EXTEND();
                  L_others_tax_calc_tbl(L_others_tax_calc_tbl.count) := L_tax_calc_rec;
               end if;
               ---
            elsif (L_from_vat_calc_type = 'E' and L_to_vat_calc_type = 'E') then 
               -- For 'E'xempt regions, set tax_exempt_ind = 'Y' 
               L_tax_calc_rec.O_tax_exempt_ind := 'Y';
               L_others_tax_calc_tbl.EXTEND();
               L_others_tax_calc_tbl(L_others_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            elsif (L_from_vat_calc_type = 'C' or L_to_vat_calc_type = 'C') then
               L_custom_tax_calc_tbl.EXTEND();
               L_custom_tax_calc_tbl(L_custom_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            else
               L_others_tax_calc_tbl.EXTEND();
               L_others_tax_calc_tbl(L_others_tax_calc_tbl.count) := L_tax_calc_rec;
            end if; --if L_from_vat_calc_type = 'S' and L_to_vat_calc_type = 'S'
         elsif (L_from_vat_region is not NULL and L_to_vat_region is NULL ) then
            if L_from_vat_calc_type = 'S' then
               L_tax_calc_rec.O_tax_exempt_ind := 'N';
               L_simple_tax_calc_tbl.EXTEND();
               L_simple_tax_calc_tbl(L_simple_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            elsif L_from_vat_calc_type = 'E' then
               L_tax_calc_rec.O_tax_exempt_ind := 'Y';
               L_others_tax_calc_tbl.EXTEND();
               L_others_tax_calc_tbl(L_others_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            elsif L_from_vat_calc_type = 'C' then
               L_custom_tax_calc_tbl.EXTEND();
               L_custom_tax_calc_tbl(L_custom_tax_calc_tbl.count) := L_tax_calc_rec;
               ---
            end if;--if L_from_vat_calc_type = 'S' 
         end if;--if (L_from_vat_region is not NULL and L_to_vat_region is not NULL )
         ---
      END LOOP;
      IO_tax_calc_tbl.DELETE();
      if L_others_tax_calc_tbl is not null and L_others_tax_calc_tbl.count > 0 then
         for rec in L_others_tax_calc_tbl.FIRST..L_others_tax_calc_tbl.LAST LOOP
            IO_tax_calc_tbl.EXTEND();
            IO_tax_calc_tbl(IO_tax_calc_tbl.count) := L_others_tax_calc_tbl(rec);
         END LOOP;
      end if;
      if L_simple_tax_calc_tbl is not null and L_simple_tax_calc_tbl.count > 0 then
         if CALC_VAT_RETAIL_TAX(O_error_message,
                                L_simple_tax_calc_tbl) = FALSE then
             return FALSE;
         end if;
         for rec in L_simple_tax_calc_tbl.FIRST..L_simple_tax_calc_tbl.LAST LOOP
            IO_tax_calc_tbl.EXTEND();
            IO_tax_calc_tbl(IO_tax_calc_tbl.count) := L_simple_tax_calc_tbl(rec);
         END LOOP;
      end if;
      if L_custom_tax_calc_tbl is not null and L_custom_tax_calc_tbl.count > 0 then
         if CUSTOM_TAX_SQL.CALC_CTAX_RETAIL_TAX(O_error_message,
                                                L_custom_tax_calc_tbl) = FALSE then
            return FALSE;                                                 
         end if;
         for rec in L_custom_tax_calc_tbl.FIRST..L_custom_tax_calc_tbl.LAST LOOP
            IO_tax_calc_tbl.EXTEND();
            IO_tax_calc_tbl(IO_tax_calc_tbl.count) := L_custom_tax_calc_tbl(rec);
         END LOOP;
      end if;
   end if; -- Tax_type

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CALC_RETAIL_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_VAT_COST_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_tax_calc_tbl   IN OUT   NOCOPY   OBJ_TAX_CALC_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.CALC_VAT_COST_TAX';

   L_item               ITEM_MASTER.ITEM%TYPE;
   L_active_date        VAT_CODE_RATES.ACTIVE_DATE%TYPE;
   L_vat_region         VAT_REGION.VAT_REGION%TYPE;
   L_location           ITEM_LOC.LOC%TYPE;
   L_pack_ind           ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind       ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind      ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type          ITEM_MASTER.PACK_TYPE%TYPE;
   L_packitem           ITEM_MASTER.ITEM%TYPE;
   L_packitem_unit_cost ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_pack_cost_in_vat   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := 0;
   L_pack_cost_ex_vat   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := 0;
   L_vat_code           VAT_CODE_RATES.VAT_CODE%TYPE;
   L_vat_rate           VAT_ITEM.VAT_RATE%TYPE;
   L_vat_type           VAT_ITEM.VAT_TYPE%TYPE;
   L_pack_exists        VARCHAR2(1) := 'N';

   L_tax_detail_rec     OBJ_TAX_DETAIL_REC;
   L_tax_detail_TBL     OBJ_TAX_DETAIL_TBL;

   cursor C_VAT_REGION is
      select s.store loc,
             s.vat_region,
             'ST' loc_type
        from store s,
             TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where s.store = tbl.I_from_entity
         and tbl.I_from_entity_type  = 'ST'
      union all
      select w.wh loc,
             w.vat_region,
             'WH' loc_type
        from wh w,
             TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where w.wh = tbl.I_from_entity
         and tbl.I_from_entity_type  = 'WH'
      union all
      select to_number(p.partner_id) loc,
             p.vat_region,
             'E' loc_type
        from partner p,
             TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where p.partner_id = tbl.I_from_entity
         and p.partner_type = 'E'
         and tbl.I_from_entity_type  = 'E'
      union all
      select sp.supplier loc,
             sp.vat_region,
             'SP' loc_type
        from sups sp,
             TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where sp.supplier = tbl.I_from_entity
         and tbl.I_from_entity_type  = 'SP';

   cursor C_ITEM is
      with vr as (select s.store loc,
                         s.vat_region,
                         'ST' loc_type
                    from store s,
                         TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
                   where s.store = tbl.I_from_entity
                     and tbl.I_from_entity_type  = 'ST'
                  union all
                  select w.wh loc,
                         w.vat_region,
                         'WH' loc_type
                    from wh w,
                         TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
                   where w.wh = tbl.I_from_entity
                     and tbl.I_from_entity_type  = 'WH'
                  union all
                  select to_number(p.partner_id) loc,
                         p.vat_region,
                         'E' loc_type
                    from partner p,
                         TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
                   where p.partner_id = tbl.I_from_entity
                     and p.partner_type = 'E'
                     and tbl.I_from_entity_type  = 'E'
                  union all
                  select sp.supplier loc,
                         sp.vat_region,
                         'SP' loc_type
                    from sups sp,
                         TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
                   where sp.supplier = tbl.I_from_entity
                     and tbl.I_from_entity_type  = 'SP')
      select /*+ push_pred(im)*/ im.item,
             im.pack_ind,
             im.pack_type,
             DECODE(tbl.I_vat_code,NULL,vi.vat_code,tbl.I_vat_code) vat_code,
             DECODE(tbl.I_vat_rate,NULL,vi.vat_rate,tbl.I_vat_rate) vat_rate,
             vi.vat_type
        from item_master im,
             (select DISTINCT item,
                     vat_region,
                     active_date,
                     vat_type,
                     vat_code,
                     vat_rate
                from vat_item,
                     TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl1
               where vat_type in (LP_TAX_TYPE_COST, LP_TAX_TYPE_BOTH)
                 and tbl1.I_item = vat_item.item               
              ) vi,
             TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where im.item = tbl.I_item
         and im.item = vi.item(+)
         and NVL(vi.active_date, GET_VDATE) <= NVL(tbl.I_effective_from_date, GET_VDATE)
         and (vi.active_date is NULL or
              vi.active_date = (select max(active_date)
                                  from vat_item vt, vr
                                where vt.item = tbl.I_item
                                  and vt.vat_type = vi.vat_type
                                  and vt.vat_region = vr.vat_region
                                  and vt.active_date <= NVL(tbl.I_effective_from_date, GET_VDATE)))
         and (vi.vat_region is NULL or
              exists (select 'Y'
                       from vr
                      where vr.vat_region = vi.vat_region
                        and rownum = 1))
    order by vi.active_date desc;

   cursor C_PACKSKU_COST is
      with vr as (select s.store loc,
                         s.vat_region,
                         'ST' loc_type
                    from store s,
                         TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
                   where s.store = tbl.I_from_entity
                     and tbl.I_from_entity_type  = 'ST'
                  union all
                  select w.wh loc,
                         w.vat_region,
                         'WH' loc_type
                    from wh w,
                         TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
                   where w.wh = tbl.I_from_entity
                     and tbl.I_from_entity_type  = 'WH'
                  union all
                  select to_number(p.partner_id) loc,
                         p.vat_region,
                         'E' loc_type
                    from partner p,
                         TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
                   where p.partner_id = tbl.I_from_entity
                     and p.partner_type = 'E'
                     and tbl.I_from_entity_type  = 'E'
                  union all
                  select sp.supplier loc,
                         sp.vat_region,
                         'SP' loc_type
                    from sups sp,
                         TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
                   where sp.supplier = tbl.I_from_entity
                     and tbl.I_from_entity_type  = 'SP')
      select /*+ push_pred(vp)*/
             vp.pack_no,
             vp.item,
             sum(vp.pack_item_qty) qty,
             isc.unit_cost,
             DECODE(tbl.I_vat_code,NULL,vi.vat_code,tbl.I_vat_code) vat_code,
             DECODE(tbl.I_vat_rate,NULL,vi.vat_rate,tbl.I_vat_rate) vat_rate,
             vi.vat_type,
             row_number()
             over (partition by vp.pack_no
                       order by vp.pack_no, vp.item) row_num
        from packitem_breakout vp,
             (select item,
                     vat_region,
                     active_date,
                     vat_type,
                     vat_code,
                     vat_rate
                from vat_item
               where vat_type in (LP_TAX_TYPE_COST, LP_TAX_TYPE_BOTH)) vi,
             item_supp_country isc,
             TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where vp.pack_no = tbl.I_item
         and vp.item = isc.item
         and vp.item = vi.item (+)
         and NVL(vi.active_date, GET_VDATE) <= NVL(tbl.I_effective_from_date, GET_VDATE)
         and isc.primary_country_ind = 'Y'
         and isc.primary_supp_ind = 'Y'
         and (vi.vat_region is NULL or
              exists (select 'Y'
                       from vr
                      where vr.vat_region = vi.vat_region
                        and rownum = 1))
         and exists (select 'Y'
                       from item_master im
                      where im.item = tbl.I_item
                        and im.pack_ind = 'Y'
                        and (im.pack_type = 'B' or
                             im.pack_type is NULL)
                        and rownum = 1)
           group by vp.pack_no,
                    vp.item,
                    isc.unit_cost,
                    DECODE(tbl.I_vat_code,NULL,vi.vat_code,tbl.I_vat_code),
                    DECODE(tbl.I_vat_rate,NULL,vi.vat_rate,tbl.I_vat_rate),
                    vi.vat_type,
                    vi.active_date
    order by vp.pack_no, vp.item, vi.active_date desc;

   TYPE arr_vat_region is table of store.vat_region%TYPE index by VARCHAR2(16);
   L_vat_region_tbl arr_vat_region;

   TYPE arr_item is table of c_item%ROWTYPE index by VARCHAR2(30);
   L_item_tbl arr_item;

   TYPE rec_packsku_cost is record (item      packitem_breakout.item%type,
                                    qty       packitem_breakout.item_qty%type,
                                    unit_cost item_supp_country.unit_cost%type,
                                    vat_code  vat_item.vat_code%type,
                                    vat_rate  vat_item.vat_rate%type,
                                    vat_type  vat_item.vat_type%type);
   TYPE arr_packsku_cost is table of rec_packsku_cost index by BINARY_INTEGER;

   TYPE rec_pack is record (pack_no          packitem_breakout.pack_no%type,
                            packsku_cost_tbl arr_packsku_cost);
   TYPE arr_pack is table of rec_pack index by VARCHAR2(30);
   L_pack_tbl arr_pack;

BEGIN

   for rec in C_VAT_REGION loop
      L_vat_region_tbl(to_char(rec.loc)||'~'||rec.loc_type) := rec.vat_region;
   end loop;

   for rec in C_ITEM loop
      L_item_tbl(rec.item).item := rec.item;
      L_item_tbl(rec.item).pack_ind := rec.pack_ind;
      L_item_tbl(rec.item).pack_type := rec.pack_type;
      L_item_tbl(rec.item).vat_code := rec.vat_code;
      L_item_tbl(rec.item).vat_rate := rec.vat_rate;
      L_item_tbl(rec.item).vat_type := rec.vat_type;


     if rec.pack_ind = 'Y' and L_pack_exists = 'N' then
        L_pack_exists := 'Y';
     end if;
   end loop;

   if L_pack_exists = 'Y' then
      for rec in C_PACKSKU_COST loop
         L_pack_tbl(rec.pack_no).pack_no := rec.pack_no;
         L_pack_tbl(rec.pack_no).packsku_cost_tbl(rec.row_num).item := rec.item;
         L_pack_tbl(rec.pack_no).packsku_cost_tbl(rec.row_num).qty := rec.qty;
         L_pack_tbl(rec.pack_no).packsku_cost_tbl(rec.row_num).unit_cost := rec.unit_cost;
         L_pack_tbl(rec.pack_no).packsku_cost_tbl(rec.row_num).vat_code := rec.vat_code;
         L_pack_tbl(rec.pack_no).packsku_cost_tbl(rec.row_num).vat_rate := rec.vat_rate;
         L_pack_tbl(rec.pack_no).packsku_cost_tbl(rec.row_num).vat_type := rec.vat_type;
      end loop;
   end if;

   L_tax_detail_rec := OBJ_TAX_DETAIL_REC(NULL, NULL, NULL, NULL, NULL,NULL,
                                          NULL, NULL, NULL, NULL,NULL,NULL);

   FOR rec in IO_tax_calc_tbl.first..IO_tax_calc_tbl.last LOOP
      L_item        :=   IO_tax_calc_tbl(rec).I_item;
      L_active_date :=   NVL(IO_tax_calc_tbl(rec).I_effective_from_date, GET_VDATE);
      L_location    :=   IO_tax_calc_tbl(rec).I_from_entity;

      if IO_tax_calc_tbl(rec).I_from_entity_type  = 'ST' then
         L_vat_region := L_vat_region_tbl(to_char(L_location)||'~'||'ST');
      elsif IO_tax_calc_tbl(rec).I_from_entity_type  = 'WH' then
         L_vat_region := L_vat_region_tbl(to_char(L_location)||'~'||'WH');
      elsif IO_tax_calc_tbl(rec).I_from_entity_type  = 'E' then
         L_vat_region := L_vat_region_tbl(to_char(L_location)||'~'||'E');
      elsif IO_tax_calc_tbl(rec).I_from_entity_type  = 'SP' then
         L_vat_region := L_vat_region_tbl(to_char(L_location)||'~'||'SP');
      end if;

      if L_vat_region is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                L_program,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;

      if IO_tax_calc_tbl(rec).I_item is NOT NULL then
         if L_item_tbl.count > 0 then
            if IO_tax_calc_tbl(rec).I_item = L_item_tbl(L_item).item and
               L_item_tbl(L_item).pack_ind = 'Y' and (L_item_tbl(L_item).pack_type = 'B' or L_item_tbl(L_item).pack_type is NULL) then

               FOR cnt in L_pack_tbl(L_item).packsku_cost_tbl.first..L_pack_tbl(L_item).packsku_cost_tbl.last LOOP
                  L_packitem_unit_cost := L_pack_tbl(L_item).packsku_cost_tbl(cnt).unit_cost;
                  ---
                  L_pack_cost_ex_vat := L_pack_cost_ex_vat
                                      + L_packitem_unit_cost * L_pack_tbl(L_item).packsku_cost_tbl(cnt).qty;
                  ---
                  L_vat_code := L_pack_tbl(L_item).packsku_cost_tbl(cnt).vat_code;
                  L_vat_rate := L_pack_tbl(L_item).packsku_cost_tbl(cnt).vat_rate;
                  L_vat_type := L_pack_tbl(L_item).packsku_cost_tbl(cnt).vat_type;
                  if L_vat_code is NULL then
                     O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                           L_pack_tbl(L_item).packsku_cost_tbl(cnt).item,
                                                           To_char(L_vat_region),
                                                           NULL);
                     return FALSE;
                  end if;
                  L_pack_cost_in_vat := L_pack_cost_in_vat
                                      + L_pack_tbl(L_item).packsku_cost_tbl(cnt).qty * L_packitem_unit_cost * (1 + L_vat_rate /100);
               END LOOP;

               if L_pack_cost_ex_vat > 0 then
                  L_vat_rate := ((L_pack_cost_in_vat / L_pack_cost_ex_vat) - 1) * 100;
               else
                  L_vat_rate := 0;
               end if;
            else
               L_vat_code := L_item_tbl(L_item).vat_code;
               L_vat_rate := L_item_tbl(L_item).vat_rate;
               L_vat_type := L_item_tbl(L_item).vat_type;
               if L_vat_code is NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                        L_item,
                                                        To_char(L_vat_region),
                                                        NULL);
                  return FALSE;
               end if;
            end if; -- pack_item
         else
            O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                  L_item,
                                                  To_char(L_vat_region),
                                                  NULL);
            return FALSE;
         end if;
         L_pack_cost_in_vat := 0;
         L_pack_cost_ex_vat := 0;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if; --item is not null
      L_tax_detail_rec := OBJ_TAX_DETAIL_REC(NULL, NULL, NULL, NULL, NULL, NULL,
                                             NULL, NULL, NULL, NULL, NULL, NULL);
      L_tax_detail_rec.tax_code := L_vat_code;
      L_tax_detail_rec.tax_type := L_vat_type;
      L_tax_detail_rec.calculation_basis := 'P';  -- for SVAT it's always 'P'ercent
      L_tax_detail_rec.tax_rate := L_vat_rate;

      L_tax_detail_tbl := OBJ_TAX_DETAIL_TBL();
      L_tax_detail_tbl.EXTEND;
      L_tax_detail_tbl(1) := L_tax_detail_rec;

      IO_tax_calc_tbl(rec).O_tax_detail_tbl := L_tax_detail_tbl;

      IO_tax_calc_tbl(rec).O_cum_tax_pct := L_vat_rate;
      if IO_tax_calc_tbl(rec).I_amount is not NULL then
         IO_tax_calc_tbl(rec).O_total_tax_amount := IO_tax_calc_tbl(rec).I_amount * L_vat_rate/100;
      end if;

   END LOOP;

   L_vat_region_tbl.delete;
   L_item_tbl.delete;
   L_pack_tbl.delete;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CALC_VAT_COST_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_VAT_RETAIL_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                             IO_tax_calc_tbl   IN OUT   NOCOPY   OBJ_TAX_CALC_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.CALC_VAT_RETAIL_TAX';
   L_item                    ITEM_MASTER.ITEM%TYPE;
   L_location                ITEM_LOC.LOC%TYPE;
   L_loc_type                ITEM_LOC.LOC_TYPE%TYPE;
   L_active_date             VAT_CODE_RATES.ACTIVE_DATE%TYPE;
   L_vat_region              VAT_REGION.VAT_REGION%TYPE;
   L_pack_ind                ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind            ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind           ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type               ITEM_MASTER.PACK_TYPE%TYPE;
   L_packitem                ITEM_MASTER.ITEM%TYPE;
   L_vat_code                VAT_CODE_RATES.VAT_CODE%TYPE;
   L_vat_rate                VAT_ITEM.VAT_RATE%TYPE;
   L_vat_type                VAT_ITEM.VAT_TYPE%TYPE;
   L_packitem_unit_retail    ITEM_LOC.UNIT_RETAIL%TYPE;
   L_pack_retail_in_vat      ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_pack_retail_ex_vat      ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_standard_uom_loc        ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_loc ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_loc         ITEM_LOC.SELLING_UOM%TYPE;
   L_multi_units_loc         ITEM_LOC.MULTI_UNITS%TYPE;
   L_multi_unit_retail_loc   ITEM_LOC.MULTI_UNIT_RETAIL%TYPE;
   L_multi_selling_uom_loc   ITEM_LOC.MULTI_SELLING_UOM%TYPE;
   L_class_vat_ind           CLASS.CLASS_VAT_IND%TYPE;

   L_tax_detail_rec     OBJ_TAX_DETAIL_REC;
   L_tax_detail_TBL     OBJ_TAX_DETAIL_TBL;

   cursor C_STORE_VAT_REGION is
      select vat_region
        from store
       where store = L_location;

   cursor C_WH_VAT_REGION is
      select vat_region
        from wh
       where wh = L_location;

   cursor C_EXTERNAL_FINISHER_VAT_REGION is
      select vat_region
        from partner
       where partner_id = L_location
         and partner_type = 'E';
         
   cursor C_SUP_VAT_REGION is
      select vat_region
        from sups
       where supplier = L_location;

   cursor C_VAT_SKU (item_param item_master.item%TYPE) is
      select vat_code, vat_rate, vat_type
        from vat_item
       where item = item_param
         and vat_region  = L_vat_region
         and vat_type in (LP_TAX_TYPE_RETAIL, LP_TAX_TYPE_BOTH)
         and active_date <= L_active_date
    order by active_date desc;  -- Much faster than then the correlated subquery.

   cursor C_PACKSKU is
      select item, qty
        from v_packsku_qty
       where pack_no = L_item;

   cursor C_CLASS_VAT is
      select cl.class_vat_ind
        from item_master im,
             class cl
       where im.item  = L_packitem
         and im.dept  = cl.dept
         and im.class = cl.class;

BEGIN

   FOR rec in IO_tax_calc_tbl.first..IO_tax_calc_tbl.last LOOP
      L_item        :=   IO_tax_calc_tbl(rec).I_item;
      L_active_date :=   NVL(IO_tax_calc_tbl(rec).I_effective_from_date, GET_VDATE);
      L_location    :=   IO_tax_calc_tbl(rec).I_from_entity;

      if IO_tax_calc_tbl(rec).I_from_entity_type  = 'ST' then
         L_loc_type := 'S';
      elsif IO_tax_calc_tbl(rec).I_from_entity_type = 'WH' then
         L_loc_type := 'W';
      elsif IO_tax_calc_tbl(rec).I_from_entity_type = 'E' then
         L_loc_type := 'E';
      elsif IO_tax_calc_tbl(rec).I_from_entity_type = 'SU' then
         L_loc_type := 'P';
      end if;

      if L_loc_type = 'S' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_STORE_VAT_REGION',
                          'store',
                          NULL);
         open  C_STORE_VAT_REGION;
         SQL_LIB.SET_MARK('FETCH',
                          'C_STORE_VAT_REGION',
                          'store',
                          NULL);
         fetch C_STORE_VAT_REGION into L_vat_region;
         if C_STORE_VAT_REGION%NOTFOUND then
            -- This location doesn't have associated vat region.
            O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                  L_program,
                                                  NULL,
                                                  NULL);
            SQL_LIB.SET_MARK('CLOSE',
                             'C_STORE_VAT_REGION',
                             'store',
                             NULL);
            close C_STORE_VAT_REGION;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_STORE_VAT_REGION',
                          'store',
                          NULL);
         close C_STORE_VAT_REGION;
      elsif L_loc_type = 'W' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_wh_vat_region',
                          'wh',
                          NULL);
         open  C_WH_VAT_REGION;
         SQL_LIB.SET_MARK('FETCH',
                          'C_wh_vat_region',
                          'wh',
                          NULL);
         fetch C_WH_VAT_REGION into L_vat_region;
         if C_WH_VAT_REGION%NOTFOUND then
            -- This location doesn't have associated vat region.
            O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                  L_program,
                                                  NULL,
                                                  NULL);

            SQL_LIB.SET_MARK('CLOSE',
                             'C_WH_VAT_REGION',
                             'wh',
                             NULL);
            close C_WH_VAT_REGION;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_wh_vat_region',
                          'wh',
                          NULL);
         close C_WH_VAT_REGION;
      elsif L_loc_type = 'E' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_EXTERNAL_FINISHER_VAT_REGION',
                          'external finisher',
                          NULL);
         open  C_EXTERNAL_FINISHER_VAT_REGION;
         SQL_LIB.SET_MARK('FETCH',
                          'C_EXTERNAL_FINISHER_VAT_REGION',
                          'external finisher',
                          NULL);
         fetch C_EXTERNAL_FINISHER_VAT_REGION into L_vat_region;
         if C_EXTERNAL_FINISHER_VAT_REGION%NOTFOUND then
            -- This location doesn't have associated vat region.
            O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                  L_program,
                                                  NULL,
                                                  NULL);
            SQL_LIB.SET_MARK('CLOSE',
                             'C_EXTERNAL_FINISHER_VAT_REGION',
                             'external finisher',
                             NULL);
            close C_EXTERNAL_FINISHER_VAT_REGION;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_EXTERNAL_FINISHER_VAT_REGION',
                          'external finisher',
                          NULL);
         close C_EXTERNAL_FINISHER_VAT_REGION;
      elsif L_loc_type = 'P' then
         SQL_LIB.SET_MARK('OPEN',
                          'C_SUP_VAT_REGION',
                          'sups',
                          NULL);
         open  C_SUP_VAT_REGION;
         SQL_LIB.SET_MARK('FETCH',
                          'C_SUP_VAT_REGION',
                          'sups',
                          NULL);
         fetch C_SUP_VAT_REGION into L_vat_region;
         if C_SUP_VAT_REGION%NOTFOUND then
            -- This location doesn't have associated vat region.
            O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                                  L_program,
                                                  NULL,
                                                  NULL);

            SQL_LIB.SET_MARK('CLOSE',
                             'C_SUP_VAT_REGION',
                             'sups',
                             NULL);
            close C_SUP_VAT_REGION;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_SUP_VAT_REGION',
                          'sups',
                          NULL);
         close C_SUP_VAT_REGION;
      end if; -- loc_type

      if L_vat_region is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_VAT_REGION_ASSIGNED',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

      if IO_tax_calc_tbl(rec).I_item is NOT NULL then
         if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                          L_pack_ind,
                                          L_sellable_ind,
                                          L_orderable_ind,
                                          L_pack_type,
                                          IO_tax_calc_tbl(rec).I_item) = FALSE then
            return FALSE;
         end if;

         if L_pack_ind = 'Y' and ((L_pack_type = 'V' and L_sellable_ind = 'Y') or L_pack_type is NULL) then

            for rec in C_PACKSKU LOOP
               L_packitem := rec.item;
               if PRICING_ATTRIB_SQL.GET_RETAIL(O_error_message,
                                                L_packitem_unit_retail,
                                                L_standard_uom_loc,
                                                L_selling_unit_retail_loc,
                                                L_selling_uom_loc,
                                                L_multi_units_loc,
                                                L_multi_unit_retail_loc,
                                                L_multi_selling_uom_loc,
                                                L_packitem,
                                                L_loc_type,
                                                L_location) = FALSE then
                  return FALSE;
               end if;

               SQL_LIB.SET_MARK('OPEN',
                                'C_VAT_SKU',
                                'vat_item',
                                NULL);
               open  C_VAT_SKU(L_packitem);
               SQL_LIB.SET_MARK('FETCH',
                                'C_VAT_SKU',
                                'vat_item',
                                NULL);
               fetch C_VAT_SKU into L_vat_code,
                                    L_vat_rate,
                                    L_vat_type;
               if C_VAT_SKU%NOTFOUND then
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_VAT_SKU',
                                   'vat_item',
                                   NULL);
                  close C_VAT_SKU;
                  O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                        L_packitem,
                                                        To_char(L_vat_region),
                                                        NULL);
                  return FALSE;
               else
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_VAT_SKU',
                                   'vat_item',
                                   NULL);
                  close C_VAT_SKU;
               end if;
               ---
               if LP_system_options_row.class_level_vat_ind is NULL then
                  if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                           LP_system_options_row) = FALSE then
                     return FALSE;
                  end if;
               end if;

               SQL_LIB.SET_MARK('OPEN',
                                'C_CLASS_VAT',
                                'item_master, class',
                                NULL);
               open C_CLASS_VAT;
               SQL_LIB.SET_MARK('FETCH',
                                'C_CLASS_VAT',
                                'item_master, class',
                                NULL);
               fetch C_CLASS_VAT into L_class_vat_ind;
               SQL_LIB.SET_MARK('CLOSE',
                                'C_CLASS_VAT',
                                'item_master, class',
                                NULL);
               close C_CLASS_VAT;
               --
               -- If system's class_level_vat_ind is 'N', unit retail in RMS is always tax inclusive.
               -- If system's class_level_vat_ind is 'Y', unit retail in RMS is tax inclusive or exclusive depending on the class.class_vat_ind.
               if LP_system_options_row.class_level_vat_ind = 'N' or
                  LP_system_options_row.class_level_vat_ind = 'Y' and L_class_vat_ind = 'Y' then
                  L_pack_retail_in_vat := L_pack_retail_in_vat
                    + L_packitem_unit_retail * rec.qty;
                  L_pack_retail_ex_vat := L_pack_retail_ex_vat
                    + rec.qty * L_packitem_unit_retail /(1 + L_vat_rate /100);
               else
                  L_pack_retail_ex_vat := L_pack_retail_ex_vat
                    + L_packitem_unit_retail * rec.qty;
                  L_pack_retail_in_vat := L_pack_retail_in_vat
                    + rec.qty * L_packitem_unit_retail * (1 + L_vat_rate /100);
               end if;
            END LOOP;

            SQL_LIB.SET_MARK('OPEN',
                             'C_VAT_SKU',
                             'vat_item',
                             NULL);
            open  C_VAT_SKU(L_item);
            SQL_LIB.SET_MARK('FETCH',
                             'C_VAT_SKU',
                             'vat_item',
                             NULL);
            fetch C_VAT_SKU into L_vat_code,
                                 L_vat_rate,
                                 L_vat_type;
            if C_VAT_SKU%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_VAT_SKU',
                                'vat_item',
                                NULL);
               close C_VAT_SKU;
               O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                     L_packitem,
                                                     To_char(L_vat_region),
                                                     NULL);
               return FALSE;
            else
               SQL_LIB.SET_MARK('CLOSE',
                                'C_VAT_SKU',
                                'vat_item',
                                NULL);
               close C_VAT_SKU;
            end if;

            if L_pack_retail_ex_vat > 0 then
               L_vat_rate := ((L_pack_retail_in_vat / L_pack_retail_ex_vat) - 1) * 100;
               L_vat_code := L_vat_code;
               L_vat_type := L_vat_type;
            else
               L_vat_rate := 0;
               L_vat_code := L_vat_code;
               L_vat_type := L_vat_type;
            end if;
         else
            SQL_LIB.SET_MARK('OPEN',
                             'C_VAT_SKU',
                             'vat_item',
                             NULL);
            open  C_VAT_SKU(IO_tax_calc_tbl(rec).I_item);
            SQL_LIB.SET_MARK('FETCH',
                             'C_VAT_SKU',
                             'vat_item',
                             NULL);
            fetch C_VAT_SKU into L_vat_code,
                                 L_vat_rate,
                                 L_vat_type;
            if C_VAT_SKU%NOTFOUND then
               SQL_LIB.SET_MARK('CLOSE',
                                'C_VAT_SKU',
                                'vat_item',
                                NULL);
               close C_VAT_SKU;
               O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                     L_item,
                                                     To_char(L_vat_region),
                                                     NULL);
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_VAT_SKU',
                             'vat_item',
                             NULL);
            close C_VAT_SKU;
         end if; -- pack_item
         --reset pack variable
         L_pack_retail_in_vat := 0;
         L_pack_retail_ex_vat := 0;
      else
         O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if; --item is not null


      L_tax_detail_rec := OBJ_TAX_DETAIL_REC(NULL, NULL, NULL, NULL, NULL, NULL,
                                             NULL, NULL, NULL, NULL, NULL, NULL);
      L_tax_detail_rec.tax_code := L_vat_code;
      L_tax_detail_rec.tax_type := L_vat_type;
      L_tax_detail_rec.calculation_basis := 'P';  -- for SVAT it's always 'P'ercent
      L_tax_detail_rec.tax_rate := L_vat_rate;

      L_tax_detail_tbl := OBJ_TAX_DETAIL_TBL();
      L_tax_detail_tbl.EXTEND;
      L_tax_detaiL_tbl(1) := L_tax_detail_rec;

      IO_tax_calc_tbl(rec).O_tax_detail_tbl := L_tax_detail_tbl;
      IO_tax_calc_tbl(rec).O_cum_tax_pct := L_vat_rate;
      if IO_tax_calc_tbl(rec).I_amount is not NULL then
         IO_tax_calc_tbl(rec).O_total_tax_amount := IO_tax_calc_tbl(rec).I_amount * L_vat_rate/100;
      end if;
   END LOOP;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CALC_VAT_RETAIL_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_GTAX_COST_TAX(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                            IO_tax_calc_tbl       IN OUT   NOCOPY   OBJ_TAX_CALC_TBL)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.CALC_GTAX_COST_TAX';

   L_tax_result   OBJ_TAX_TBL := OBJ_TAX_TBL();
   L_cost_retail  VARCHAR2(1) := LP_TAX_TYPE_COST;

   L_input_count   NUMBER;
   L_output_count  NUMBER;

   -- calculate cum_tax_value/pct and assign tax detail results (TAX_DETAIL_TBL) to the corresponding OBJ_TAX_INFO_REC
   cursor C_TOTAL_TAX is
      select OBJ_TAX_CALC_REC(input.I_item,
                              input.I_pack_ind,
                              input.I_from_entity,
                              input.I_from_entity_type,
                              input.I_to_entity,
                              input.I_to_entity_type,
                              input.I_effective_from_date,
                              input.I_amount,
                              input.I_amount_curr,
                              input.I_amount_tax_incl_ind,
                              input.I_origin_country_id,
                              NULL, -- O_cum_tax_pct
                              NULL, -- O_cum_tax_value
                              -- O_total_tax_amount, converted to I_amount_curr by L10N layer
                              NVL((select sum(NVL(td.estimated_tax_value, 0))
                                     from TABLE(CAST(rslt.tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td), 0),
                              input.I_amount_curr, -- O_total_tax_amount_curr,
                              -- O_total_recover_amount, converted to I_amount_curr by L10N layer
                              NVL((select sum(NVL(td.recoverable_amount, 0))
                                     from TABLE(CAST(rslt.tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td), 0),
                              input.I_amount_curr, -- O_total_recover_amount_curr
                              rslt.tax_detail_tbl)
        from TABLE(CAST(L_tax_result as OBJ_TAX_TBL)) rslt,
             TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) input
       where rslt.item = input.I_item
         and rslt.from_entity = input.I_from_entity
         and rslt.from_entity_type = input.I_from_entity_type
         and rslt.to_entity = input.I_to_entity
         and rslt.to_entity_type = input.I_to_entity_type
         and NVL(rslt.effective_from_date, to_date('01-01-1900', 'DD-MM-YYYY')) = NVL(input.I_effective_from_date, to_date('01-01-1900', 'DD-MM-YYYY'))
         and rslt.ORIGIN_COUNTRY_ID = input.I_ORIGIN_COUNTRY_ID; 
BEGIN
   L_input_count := IO_tax_calc_tbl.COUNT;

   -- Call tax engine for purchase tax
   if GET_ITEM_GTAX(O_error_message,
                    L_tax_result,
                    IO_tax_calc_tbl,
                    L_cost_retail) = FALSE then  --Cost
      return FALSE;
   end if;

   -- pass tax detail results to the corresponding IO_tax_calc_tbl object and sum up total tax values
   open C_TOTAL_TAX;
   IO_tax_calc_tbl.delete();
   fetch C_TOTAL_TAX bulk collect into IO_tax_calc_tbl;
   close C_TOTAL_TAX;

   L_output_count := IO_tax_calc_tbl.COUNT;
   if L_input_count != L_output_count then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_OUTPUT',
                                            L_program,
                                            L_input_count,
                                            L_output_count);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CALC_GTAX_COST_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION CALC_GTAX_RETAIL_TAX(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                              IO_tax_calc_tbl   IN OUT   NOCOPY   OBJ_TAX_CALC_TBL,
                              I_call_type       IN                VARCHAR2 DEFAULT LP_CALL_TYPE_DEFAULT)
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.CALC_GTAX_RETAIL_TAX';

   L_tax_result              OBJ_TAX_TBL := OBJ_TAX_TBL();
   L_cost_retail             VARCHAR2(1) := LP_TAX_TYPE_RETAIL;
   L_no_gtax_item_rollup_rec VARCHAR2(1)  := 'Y';
   L_tax_calc_tbl            OBJ_TAX_CALC_TBL;

   L_input_count   NUMBER;
   L_output_count  NUMBER;

   -- calculate cum_tax_value/pct and assign tax detail results (TAX_DETAIL_TBL) to the corresponding OBJ_TAX_CALC_REC
   cursor C_CUM_TAX is
      select OBJ_TAX_CALC_REC(input.I_item,
                              input.I_pack_ind,
                              input.I_from_entity,
                              input.I_from_entity_type,
                              input.I_to_entity,
                              input.I_to_entity_type,
                              input.I_effective_from_date,
                              input.I_amount,
                              input.I_amount_curr,
                              input.I_amount_tax_incl_ind,
                              input.I_origin_country_id,
                              -- O_cum_tax_pct, converted to I_amount_curr by L10N layer
                              NVL((select sum(NVL(td.tax_rate, 0))
                                     from TABLE(CAST(rslt.tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td
                                    where td.calculation_basis = 'P'), 0),
                              --O_cum_tax_value
                              NVL((select sum(NVL(td.tax_rate, 0))
                                     from TABLE(CAST(rslt.tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td
                                    where td.calculation_basis = 'V'), 0),
                              --O_total_tax_amount
                              NVL((select sum(NVL(td.estimated_tax_value, 0))
                                     from TABLE(CAST(rslt.tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td), 0),
                              --O_total_tax_amount_curr: assume all details have the same currency code
                              (select distinct td.currency_code
                                 from TABLE(CAST(rslt.tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td),
                              NULL,  --O_total_recover_amount
                              NULL,  -- O_total_recover_amount_curr
                              rslt.tax_detail_tbl)
        from TABLE(CAST(L_tax_result as OBJ_TAX_TBL)) rslt,
             TABLE(CAST(IO_tax_calc_tbl as OBJ_TAX_CALC_TBL)) input
       where rslt.item = input.I_item
         and rslt.from_entity = input.I_from_entity
         and rslt.from_entity_type = input.I_from_entity_type
         and NVL(rslt.effective_from_date, to_date('01-01-1900', 'DD-MM-YYYY')) = NVL(input.I_effective_from_date, to_date('01-01-1900', 'DD-MM-YYYY')) and rslt.ORIGIN_COUNTRY_ID = input.I_ORIGIN_COUNTRY_ID; 

   cursor C_RETAIL_TAX is
      select OBJ_TAX_CALC_REC(vt.I_item,
                              vt.I_pack_ind,
                              vt.I_from_entity,         --expect loc
                              vt.I_from_entity_type,    --expect loc type ('ST', 'WH', 'E')
                              vt.I_to_entity,           --NULL
                              vt.I_to_entity_type,      --NULL
                              vt.I_effective_from_date,
                              vt.I_amount,
                              vt.I_amount_curr,
                              vt.I_amount_tax_incl_ind,
                              vt.I_origin_country_id,
                              gi.cum_tax_pct,           --O_cum_tax_pct
                              gi.cum_tax_value,         --O_cum_tax_value
                              NULL,                     --O_total_tax_amount
                              NULL,                     --O_total_tax_amount_curr
                              NULL,                     --O_total_recover_amount
                              NULL,                     --O_total_recover_amount_curr
                              vt.O_tax_detail_tbl)      --expect NULL
        from gtax_item_rollup gi,
             TABLE(CAST(IO_tax_calc_tbl AS OBJ_TAX_CALC_TBL)) vt
       where gi.item = vt.I_item
         and gi.loc_type = vt.I_from_entity_type
         and gi.loc = vt.I_from_entity
         and gi.effective_from_date = (select max(effective_from_date)
                                         from gtax_item_rollup
                                        where item = vt.I_item
                                          and loc = vt.I_from_entity
                                          and loc_type = vt.I_from_entity_type
                                          and effective_from_date <= vt.I_effective_from_date);

BEGIN
   L_input_count := IO_tax_calc_tbl.COUNT;

   if I_call_type in (LP_CALL_TYPE_DEFAULT, LP_CALL_TYPE_RPM_NIL) then
      -- query gtax retail rollup info to object
      open C_RETAIL_TAX;
      fetch C_RETAIL_TAX bulk collect into L_tax_calc_tbl;
      close C_RETAIL_TAX;
   end if;
   --
   if I_call_type = LP_CALL_TYPE_RPM_NIL then
     L_output_count := L_tax_calc_tbl.COUNT;
     if L_input_count != L_output_count then
        L_no_gtax_item_rollup_rec := 'N';
     else 
        IO_tax_calc_tbl.delete();
        IO_tax_calc_tbl := L_tax_calc_tbl;
     end if;
   end if;
   --
   if I_call_type in (LP_CALL_TYPE_NIL, LP_CALL_TYPE_INIT_RETAIL) or
      L_no_gtax_item_rollup_rec = 'N' then  -- New Item Loc or Initial Retail
      -- call tax engine for sales tax
      if GET_ITEM_GTAX(O_error_message,
                       L_tax_result,
                       IO_tax_calc_tbl,
                       L_cost_retail) = FALSE then
         return FALSE;
      end if;

      -- pass tax detail results to the corresponding IO_tax_calc_tbl object and sum up cumulative tax rates and amount
      open C_CUM_TAX;
      IO_tax_calc_tbl.delete();
      fetch C_CUM_TAX bulk collect into IO_tax_calc_tbl;
      close C_CUM_TAX;

      if I_call_type = LP_CALL_TYPE_NIL then
         -- write tax result to gtax_item_rollup
         if WRITE_RETAIL_TAX(O_error_message,
                             L_tax_result) = FALSE then
            return FALSE;
         end if;
         -- write tax breakup at item level to pos_mods_tax_info table for downloading
         -- into POS by nightly batch run taxdnld.pc
         if WRITE_POS_MODS_TAX_INFO (O_error_message,
                                     L_tax_result) = FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   L_output_count := IO_tax_calc_tbl.COUNT;
   if L_input_count != L_output_count then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_OUTPUT',
                                            L_program,
                                            L_input_count,
                                            L_output_count);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END CALC_GTAX_RETAIL_TAX;
------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_GTAX(O_error_message       IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tax_result          IN OUT            OBJ_TAX_TBL,
                       I_tax_calc_tbl        IN OUT   NOCOPY   OBJ_TAX_CALC_TBL,
                       I_cost_retail_ind     IN OUT            VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'TAX_SQL.GET_ITEM_GTAX';

   L_tax_rec          OBJ_TAX_REC := OBJ_TAX_REC();
   L_tax_tbl          OBJ_TAX_TBL := OBJ_TAX_TBL();
   L_l10n_tax_rec     L10N_TAX_REC := L10N_TAX_REC();
   L_missing_addrs    VARCHAR2(1000);
   L_max_array_size   L10N_TAX_OBJECT_CONFIG.TAX_OBJECT_MAX_SIZE%TYPE;

   -- For tax type of Retail (LP_TAX_TYPE_RETAIL), item/locs are passed in the input object. Use from_entity (loc)'s country to determine l10n country.
   -- For tax type of Cost (LP_TAX_TYPE_COST), item/supplier/locs are passed in the input object. Use to_entity (loc)'s country to determine l10n country.

   cursor C_MISSING_ADDR (I_cost_retail VARCHAR2) is
      select DECODE(I_cost_retail, LP_TAX_TYPE_RETAIL, tbl.I_from_entity_type, tbl.I_to_entity_type) entity_type,
             DECODE(I_cost_retail, LP_TAX_TYPE_RETAIL, tbl.I_from_entity, tbl.I_to_entity) entity
        from TABLE(CAST(I_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl,
             -- address is not defined for virtual wh, use physical wh's address.
             (select store vloc,
                     store ploc,
                     'ST' loc_type
                from store
               union all
              select wh vloc,
                     physical_wh ploc,
                     'WH' loc_type
                from wh
               union all
              select TO_NUMBER(partner_id) vloc,
                     TO_NUMBER(partner_id) ploc,
                     'E' loc_type
                from partner
               where partner_type = 'E') location
       where TO_CHAR(location.vloc) = DECODE(I_cost_retail, LP_TAX_TYPE_RETAIL, tbl.I_from_entity, tbl.I_to_entity)
         and location.loc_type = DECODE(I_cost_retail, LP_TAX_TYPE_RETAIL, tbl.I_from_entity_type, tbl.I_to_entity_type)
         and not exists (select 'x'
                           from addr
                          where ((addr.module = 'PTNR'
                                 and  addr.key_value_1 = 'E'
                                 and  addr.key_value_2 = TO_CHAR(location.ploc)) or
                                (addr.module in ('ST','WH','WFST')
                                 and  addr.key_value_1 = TO_CHAR(location.ploc)))
                            and addr_type = DECODE(addr.module ,'WFST','07','01')
                            and primary_addr_ind = 'Y'
                            and rownum = 1);

   cursor C_COUNTRIES(I_cost_retail VARCHAR2) is
      select distinct addr.country_id,
                      tbl.I_effective_from_date
        from TABLE(CAST(I_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl,
             addr,
             -- address is not defined for virtual wh, use physical wh's address.
             (select store vloc,
                     store ploc,
                     'ST' loc_type
                from store
               union all
              select wh vloc,
                     physical_wh ploc,
                     'WH' loc_type
                from wh
               union all
              select TO_NUMBER(partner_id) vloc,
                     TO_NUMBER(partner_id) ploc,
                     'E' loc_type
                from partner
               where partner_type = 'E') location
       where TO_CHAR(location.vloc) = DECODE(I_cost_retail, LP_TAX_TYPE_RETAIL, tbl.I_from_entity, tbl.I_to_entity)
         and location.loc_type = DECODE(I_cost_retail, LP_TAX_TYPE_RETAIL, tbl.I_from_entity_type, tbl.I_to_entity_type)
         and ((addr.module = 'PTNR'
               and  addr.key_value_1 = 'E'
               and  addr.key_value_2 = TO_CHAR(location.ploc)) or
              (addr.module in ('ST','WH','WFST')
               and  addr.key_value_1 = TO_CHAR(location.ploc)))
         and addr_type = DECODE(addr.module ,'WFST','07','01')
         and primary_addr_ind = 'Y';

   cursor C_ENTITIES (I_cost_retail VARCHAR2, I_country_id COUNTRY.COUNTRY_ID%TYPE, I_date DATE) is
      select tbl.I_item,
             tbl.I_pack_ind,
             tbl.I_from_entity,
             tbl.I_from_entity_type,
             tbl.I_to_entity,
             tbl.I_to_entity_type,
             tbl.I_effective_from_date,
             tbl.I_amount,
             tbl.I_amount_curr,
             tbl.I_amount_tax_incl_ind,
             tbl.I_origin_country_id
        from TABLE(CAST(I_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl,
             addr,
             -- address is not defined for virtual wh, use physical wh's address.
             (select store vloc,
                     store ploc,
                     'ST' loc_type
                from store
               union all
              select wh vloc,
                     physical_wh ploc,
                     'WH' loc_type
                from wh
               union all
              select TO_NUMBER(partner_id) vloc,
                     TO_NUMBER(partner_id) ploc,
                     'E' loc_type
                from partner
               where partner_type = 'E') location
       where TO_CHAR(location.vloc) = DECODE(I_cost_retail, LP_TAX_TYPE_RETAIL, tbl.I_from_entity, tbl.I_to_entity)
         and location.loc_type = DECODE(I_cost_retail, LP_TAX_TYPE_RETAIL, tbl.I_from_entity_type, tbl.I_to_entity_type)
         and ((addr.module = 'PTNR'
               and  addr.key_value_1 = 'E'
               and  addr.key_value_2 = to_char(location.ploc)) or
              (addr.module in ('ST','WH','WFST')
               and  addr.key_value_1 = TO_CHAR(location.ploc)))
         and addr_type = DECODE(addr.module ,'WFST','07','01')
         and primary_addr_ind = 'Y'
         and country_id = I_country_id
         and tbl.I_effective_from_date = I_date;


   cursor C_GET_MAX_TBL_SIZE is
      select tax_object_max_size
        from l10n_tax_object_config;

BEGIN

   --Retrieve maximum size for OBJ_TAX_TBL
   open C_GET_MAX_TBL_SIZE;
   fetch C_GET_MAX_TBL_SIZE into L_max_array_size;
   if C_GET_MAX_TBL_SIZE%NOTFOUND then
      close C_GET_MAX_TBL_SIZE;
      O_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                            'L10N_TAX_OBJECT_CONFIG',
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   close C_GET_MAX_TBL_SIZE;

   O_tax_result := OBJ_TAX_TBL();

   --check if address are defined for all entities
   L_missing_addrs := NULL;
   FOR rec in C_MISSING_ADDR(I_cost_retail_ind) LOOP
      L_missing_addrs := L_missing_addrs || ' ' || rec.entity_type || ' ' || rec.entity;
   END LOOP;
   ---
   if L_missing_addrs is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('BUSINESS_ADDR',
                                            L_missing_addrs,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   --Looping for distinct country so that we group the locations under a country.
   FOR rec in C_COUNTRIES(I_cost_retail_ind) LOOP
      FOR cur in C_ENTITIES(I_cost_retail_ind, rec.country_id, rec.I_effective_from_date) LOOP
         -- initialize between loops
         L_tax_rec := OBJ_TAX_REC();
         L_tax_rec.item                := cur.I_item;
         L_tax_rec.pack_ind            := cur.I_pack_ind;
         L_tax_rec.from_entity         := cur.I_from_entity;
         L_tax_rec.from_entity_type    := cur.I_from_entity_type;
         L_tax_rec.to_entity           := cur.I_to_entity;
         L_tax_rec.to_entity_type      := cur.I_to_entity_type;
         L_tax_rec.effective_from_date := cur.I_effective_from_date;
         L_tax_rec.taxable_base        := cur.I_amount;
         L_tax_rec.taxable_base_curr   := cur.I_amount_curr;
         L_tax_rec.taxable_base_tax_incl_ind := cur.I_amount_tax_incl_ind;
         L_tax_rec.origin_country_id   := cur.I_origin_country_id;

         L_tax_tbl.EXTEND;
         L_tax_tbl(L_tax_tbl.COUNT) := L_tax_rec;

         --Determine if the number of records have reached the defined maximum size.
         --Call tax function if maximum size has been reached.
         if L_tax_tbl.COUNT >= L_max_array_size then
            -- initialize between loops
            L_l10n_tax_rec := L10N_TAX_REC();
            L_l10n_tax_rec.procedure_key     := 'LOAD_TAX_OBJECT';
            L_l10n_tax_rec.country_id        := rec.country_id;
            L_l10n_tax_rec.l10n_tax_tbl      := L_tax_tbl;

            -- determine call_type
            if I_cost_retail_ind = LP_TAX_TYPE_RETAIL then
               L_l10n_tax_rec.call_type     := 'S';
            else  -- I_cost_retail_ind = LP_TAX_TYPE_COST
               L_l10n_tax_rec.call_type     := 'P';
            end if;

            --Calling L10N function for dynamic execution.
            if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                      L_l10n_tax_rec) = FALSE then
               return FALSE;
            end if;

            --Output tax detail returned from the tax engine
            if L_l10n_tax_rec.l10n_tax_tbl.COUNT > 0 then
               FOR i in L_l10n_tax_rec.l10n_tax_tbl.FIRST .. L_l10n_tax_rec.l10n_tax_tbl.LAST LOOP
                   O_tax_result.EXTEND;
                   O_tax_result(O_tax_result.COUNT) := L_l10n_tax_rec.l10n_tax_tbl(i);
               END LOOP;
            end if;

            -- Deleting the L_tax_tbl after calling the tax engine for the iteration
            L_tax_tbl.DELETE();
         end if;
      END LOOP;

      -- Handle leftover bit that is less than max size
      if L_tax_tbl.COUNT < L_max_array_size and L_tax_tbl.COUNT > 0 then
         -- initialize between loops
         L_l10n_tax_rec := L10N_TAX_REC();
         L_l10n_tax_rec.procedure_key     := 'LOAD_TAX_OBJECT';
         L_l10n_tax_rec.country_id        := rec.country_id;
         L_l10n_tax_rec.l10n_tax_tbl      := L_tax_tbl;

         -- determine call_type
         if I_cost_retail_ind = LP_TAX_TYPE_RETAIL then
            L_l10n_tax_rec.call_type     := 'S';
         else  -- I_cost_retail_ind = LP_TAX_TYPE_COST
            L_l10n_tax_rec.call_type     := 'P';
         end if;

         --Calling L10N function for dynamic execution.
         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_tax_rec) = FALSE then
            return FALSE;
         end if;

         --Output tax detail returned from the tax engine
         if L_l10n_tax_rec.l10n_tax_tbl.COUNT > 0 then
            FOR i in L_l10n_tax_rec.l10n_tax_tbl.FIRST .. L_l10n_tax_rec.l10n_tax_tbl.LAST LOOP
                O_tax_result.EXTEND;
                O_tax_result(O_tax_result.COUNT) := L_l10n_tax_rec.l10n_tax_tbl(i);
            END LOOP;
         end if;

         -- Deleting the L_tax_tbl after calling the tax engine for the iteration
         L_tax_tbl.DELETE();
      end if;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_ITEM_GTAX;
----------------------------------------------------------------------------------------
FUNCTION WRITE_RETAIL_TAX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tax_tbl       IN OUT OBJ_TAX_TBL)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'TAX_SQL.WRITE_RETAIL_TAX';
   L_user       VARCHAR2(30) := get_user;

BEGIN

   --This function rolls up retail tax percentage and tax value returned from the tax engine call
   --(in I_tax_tbl.tax_detail_tbl) and write to GTAX_ITEM_ROLLUP table.
   merge into gtax_item_rollup gi
      using (select im.item,
                    from_entity,
                    from_entity_type,
                    effective_from_date,
                    im.item_parent,
                    im.item_grandparent,
                    NVL((select sum(NVL(td.tax_rate, 0))
                           from TABLE(CAST(vt.tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td
                      where td.calculation_basis = 'P'), 0) cum_tax_pct,
                    NVL((select sum(NVL(td.tax_rate, 0))
                           from TABLE(CAST(vt.tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td
                      where td.calculation_basis = 'V'), 0) cum_tax_value,
                    -- assume all tax details have the same currency code as taxable base currency
                    taxable_base_curr currency_code
               from item_master im,
                    TABLE(CAST(I_tax_tbl as OBJ_TAX_TBL)) vt
              where im.item = vt.item) tx
         on (gi.item = tx.item and
             gi.loc = tx.from_entity and
             gi.loc_type = tx.from_entity_type and
             gi.effective_from_date = tx.effective_from_date)
       when matched then
          update set gi.item_parent = tx.item_parent,
                     gi.item_grandparent = tx.item_grandparent,
                     gi.cum_tax_pct = tx.cum_tax_pct,
                     gi.cum_tax_value = tx.cum_tax_value,
                     gi.currency_code = tx.currency_code,
                     gi.last_update_datetime = SYSDATE,
                     gi.last_update_id = L_user
       when not matched then
          insert (item,
                  loc,
                  loc_type,
                  effective_from_date,
                  item_parent,
                  item_grandparent,
                  cum_tax_pct,
                  cum_tax_value,
                  currency_code,
                  create_datetime,
                  create_id,
                  last_update_datetime,
                  last_update_id)
          values (tx.item,
                  tx.from_entity,
                  tx.from_entity_type,
                  tx.effective_from_date,
                  tx.item_parent,
                  tx.item_grandparent,
                  tx.cum_tax_pct,
                  tx.cum_tax_value,
                  tx.currency_code,
                  SYSDATE,
                  L_user,
                  SYSDATE,
                  L_user);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_RETAIL_TAX;
-----------------------------------------------------------------------------------------------------
FUNCTION WRITE_POS_MODS_TAX_INFO (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_tax_tbl       IN OUT OBJ_TAX_TBL)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50) := 'TAX_SQL.WRITE_POS_MODS_TAX_INFO';
   L_user       VARCHAR2(30) := get_user;
   L_tax_detail_tbl    OBJ_TAX_DETAIL_TBL  := OBJ_TAX_DETAIL_TBL();
--
BEGIN
   --
   FOR rec in I_tax_tbl.first..I_tax_tbl.last LOOP
      if I_tax_tbl(rec).from_entity_type = 'ST' then

         L_tax_detail_tbl := I_tax_tbl(rec).tax_detail_tbl;

         insert into pos_mods_tax_info
                   (item,
                    store,
                    tax_type,
                    effective_from,
                    tax_code,
                    calculation_basis,
                    tax_rate,
                    estimated_tax_value,
                    create_datetime,
                    create_id)
                select I_tax_tbl(rec).item item,
                       I_tax_tbl(rec).from_entity store,
                       'R' tax_type,
                       I_tax_tbl(rec).effective_from_date effective_from_date,
                       td.tax_code tax_code,
                       td.calculation_basis calculation_basis,
                       td.tax_rate tax_rate,
                       td.estimated_tax_value estimated_tax_value,
                    SYSDATE,
                       L_user
                  from TABLE(CAST(L_tax_detail_tbl as OBJ_TAX_DETAIL_TBL)) td;

      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END WRITE_POS_MODS_TAX_INFO;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_TAX_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_function   VARCHAR2(60) := 'TAX_SQL.DELETE_TAX_ITEM';

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type = LP_GTAX then
      if DELETE_GTAX_ITEM(O_error_message,
                          IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if DELETE_VAT_ITEM(O_error_message,
                         IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            NULL);
      return FALSE;
END DELETE_TAX_ITEM;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_GTAX_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_function   VARCHAR2(60) := 'TAX_SQL.DELETE_GTAX_ITEM';

   L_table                    VARCHAR2(20) := 'GTAX_ITEM_ROLLUP';
   L_item                     GTAX_ITEM_ROLLUP.ITEM%TYPE;

   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_GTAX_ITEM IS
      select 'x'
        from gtax_item_rollup
       where item = L_item
         and item_parent = L_item
         and item_grandparent = L_item
         for update nowait;

BEGIN
   FOR rec in IO_tax_info_tbl.first..IO_tax_info_tbl.last LOOP
      L_item := IO_tax_info_tbl(rec).item;

      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_GTAX_ITEM',
                       'GTAX_ITEM_ROLLUP',
                       NULL);
      open C_LOCK_GTAX_ITEM;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_GTAX_ITEM',
                       'GTAX_ITEM_ROLLUP',
                       NULL);
      close C_LOCK_GTAX_ITEM;
   END LOOP;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'GTAX_ITEM_ROLLUP',
                    'OBJ_TAX_INFO_TBL');

   FORALL i in IO_tax_info_tbl.FIRST..IO_tax_info_tbl.LAST
      DELETE from gtax_item_rollup
         where item = TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).item
            or item_parent = TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).item
            or item_grandparent = TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).item;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            L_item,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            NULL);
      return FALSE;
END DELETE_GTAX_ITEM;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_VAT_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_function   VARCHAR2(60) := 'TAX_SQL.DELETE_VAT_ITEM';

BEGIN
   --Call private function LOCK_VAT_ITEM
   if LOCK_VAT_ITEM(O_error_message,
                    IO_tax_info_tbl) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'VAT_ITEM',
                    'OBJ_TAX_INFO_TBL');
   FORALL i in IO_tax_info_tbl.FIRST..IO_tax_info_tbl.LAST
      DELETE from vat_item
         where item = TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).item
           and vat_type = NVL(TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).cost_retail_ind, vat_type)
           and vat_code = NVL(TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).tax_code, vat_code)
           and vat_region = NVL(TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).from_tax_region, vat_region)
           and active_date = NVL(TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).active_date, active_date);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            NULL);
      return FALSE;
END DELETE_VAT_ITEM;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_TAX_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_function   VARCHAR2(60) := 'TAX_SQL.DELETE_TAX_DEPS';

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in ( LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if DELETE_VAT_DEPS(O_error_message,
                         IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            NULL);
      return FALSE;
END DELETE_TAX_DEPS;
-----------------------------------------------------------------------------------------------------
FUNCTION DELETE_VAT_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_function   VARCHAR2(60) := 'TAX_SQL.DELETE_VAT_DEPS';

BEGIN

   if TAX_SQL.LOCK_VAT_DEPS(O_error_message,
                            IO_tax_info_tbl) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'VAT_DEPS',
                    'OBJ_TAX_INFO_TBL');
   FORALL i in IO_tax_info_tbl.FIRST..IO_tax_info_tbl.LAST
      DELETE from vat_deps
         where dept = NVL(TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).merch_hier_value, dept)
           and vat_type = NVL(TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).cost_retail_ind, vat_type)
           and vat_code = NVL(TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).tax_code, vat_code)
           and vat_region = NVL(TREAT(IO_tax_info_tbl(i) AS obj_tax_info_rec).from_tax_region, vat_region);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            NULL);
      return FALSE;
END DELETE_VAT_DEPS;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_TAX_SKU(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                               O_run_report      IN OUT            BOOLEAN,
                               IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TAX_SQL.INSERT_UPDATE_TAX_SKU';

BEGIN
-- Note: O_run_report is not used. It is always FALSE. It is left in the code due to the number of existing modules that call this function.

   O_run_report := FALSE;

   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 or IO_tax_info_tbl(IO_tax_info_tbl.COUNT).item is NULL then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type = LP_SVAT  then
      if INSERT_UPDATE_VAT_ITEM(O_error_message,
                                IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   elsif LP_system_options_row.default_tax_type = LP_GTAX then
      if INSERT_UPDATE_GTAX_ITEM(O_error_message,
                                 IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END INSERT_UPDATE_TAX_SKU;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_VAT_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TAX_SQL.INSERT_UPDATE_VAT_ITEM';
   L_user              VARCHAR2(30) := get_user;
   L_vdate             DATE         := GET_VDATE;
   L_item              VAT_ITEM.ITEM%TYPE;
   L_vat_region        VAT_ITEM.VAT_REGION%TYPE;
   L_active_date       VAT_ITEM.ACTIVE_DATE%TYPE;

   -- Vat type of Both cannot be present with Cost or Retail tax type.
   cursor C_VAT_TYPE_B_EXISTS is
      select vt.item, 
             vt.from_tax_region,
             vt.active_date
        from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vt
       where exists (select 1
                       from vat_item vi
                      where vi.item = vt.item 
                        and vi.vat_region = vt.from_tax_region 
                        and vi.active_date = vt.active_date
                        and (   (    vi.vat_type = 'B' 
                                 and vt.cost_retail_ind != 'B') 
                             OR (    vi.vat_type != 'B' 
                                 and vt.cost_retail_ind = 'B') ))
         and rownum = 1;
   
BEGIN

   -- If there are valid records, call merge to insert into
   -- vat_item if records do not exists, else update.

   if IO_tax_info_tbl.count != 0 and
      IO_tax_info_tbl is NOT NULL then

      open C_VAT_TYPE_B_EXISTS;
      fetch C_VAT_TYPE_B_EXISTS into L_item, L_vat_region, L_active_date;
      if C_VAT_TYPE_B_EXISTS%FOUND then
         close C_VAT_TYPE_B_EXISTS;
         O_error_message := SQL_LIB.CREATE_MSG('VAT_TYPE_B_EXISTS',
                                               L_item,
                                               L_vat_region,
                                               TO_CHAR(L_active_date));
         return FALSE;
      end if;
      close C_VAT_TYPE_B_EXISTS;
      ---
      if LOCK_VAT_ITEM(O_error_message,
                       IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('MERGE',
                       NULL,
                       'vat_item',
                       'OBJ_TAX_INFO_TBL');



      merge into vat_item vi
      using TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vt
      on    (vi.item = vt.item and
             vi.vat_region = vt.from_tax_region and
             vi.vat_type = vt.cost_retail_ind and
             vi.active_date = vt.active_date)
      when matched then
         update set vi.vat_code = vt.tax_code,
                    vi.vat_rate = vt.tax_rate,
                    vi.reverse_vat_ind = vt.reverse_vat_ind,
                    vi.create_date = L_vdate,
                    vi.create_id = L_user
      when not matched then
         insert (item,
                 vat_region,
                 active_date,
                 vat_code,
                 vat_rate,
                 vat_type,
                 reverse_vat_ind,
                 create_date,
                 create_id,
                 create_datetime,
                 last_update_datetime,
                 last_update_id)
         values (vt.item,
                 vt.from_tax_region,
                 vt.active_date,
                 vt.tax_code,
                 vt.tax_rate,
                 vt.cost_retail_ind,
                 vt.reverse_vat_ind,
                 L_vdate,
                 L_user,
                 SYSDATE,
                 SYSDATE,
                 L_user);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END INSERT_UPDATE_VAT_ITEM;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_GTAX_ITEM(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                 IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'TAX_SQL.INSERT_UPDATE_GTAX_ITEM';
   L_user              VARCHAR2(30) := get_user;

BEGIN

   if LOCK_GTAX_ITEM(O_error_message,
                     IO_tax_info_tbl) = FALSE then
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('MERGE',
                     NULL,
                     'gtax_item_rollup',
                     'OBJ_TAX_INFO_TBL');

   merge into gtax_item_rollup gi
      using TABLE(CAST(IO_tax_info_tbl as OBJ_TAX_INFO_TBL)) vt
      on (gi.item = vt.item and
          gi.loc = to_number(vt.from_entity) and
          gi.loc_type = vt.from_entity_type and
          gi.effective_from_date = vt.active_date)
      when matched then
         update set gi.item_parent = vt.item_parent,
                    gi.item_grandparent = vt.item_grandparent,
                    gi.cum_tax_pct = vt.tax_rate,
                    gi.cum_tax_value = vt.tax_amount,
                    gi.currency_code = vt.currency,
                    gi.last_update_datetime = SYSDATE,
                    gi.last_update_id = L_user
      when not matched then
         insert (item,
                 loc,
                 loc_type,
                 effective_from_date,
                 item_parent,
                 item_grandparent,
                 cum_tax_pct,
                 cum_tax_value,
                 currency_code,
                 create_datetime,
                 create_id,
                 last_update_datetime,
                 last_update_id)
         values (vt.item,
                 to_number(vt.from_entity),
                 vt.from_entity_type,
                 vt.active_date,
                 vt.item_parent,
                 vt.item_grandparent,
                 vt.tax_rate,
                 vt.tax_amount,
                 vt.currency,
                 SYSDATE,
                 L_user,
                 SYSDATE,
                 L_user);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END INSERT_UPDATE_GTAX_ITEM;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_TAX_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64)               := 'TAX_SQL.INSERT_UPDATE_TAX_DEPS';

BEGIN

   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      return TRUE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if INSERT_UPDATE_VAT_DEPS(O_error_message,
                                IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END INSERT_UPDATE_TAX_DEPS;
-----------------------------------------------------------------------------------------------------
FUNCTION INSERT_UPDATE_VAT_DEPS(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(64)               := 'TAX_SQL.INSERT_UPDATE_VAT_DEPS';
   L_tax_ins_upd          OBJ_TAX_INFO_TBL           := OBJ_TAX_INFO_TBL();

BEGIN

   SQL_LIB.SET_MARK('MERGE',
                    NULL,
                    'vat_deps',
                    'OBJ_TAX_INFO_TBL');
   merge into vat_deps vd
      using TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vt
      on    (vd.vat_region = vt.from_tax_region and
             vd.dept = vt.merch_hier_value and
             vd.vat_type = vt.cost_retail_ind)
      when matched then
         update set vd.vat_code = vt.tax_code,
                    vd.reverse_vat_ind = vt.reverse_vat_ind
      when not matched then
         insert (vat_region,
                 dept,
                 vat_type,
                 vat_code,
                 reverse_vat_ind)
         values (vt.from_tax_region,
                 vt.merch_hier_value,
                 vt.cost_retail_ind,
                 vt.tax_code,
                 vt.reverse_vat_ind);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END INSERT_UPDATE_VAT_DEPS;
----------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_DEPS_TAXRATE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'TAX_SQL.DELETE_ITEM_DEPS_TAXRATE';

BEGIN

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if VATRATE_ITEM_DEPS_DELETION(O_error_message,
                                    IO_tax_info_tbl) = FALSE then
          return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END DELETE_ITEM_DEPS_TAXRATE;
----------------------------------------------------------------------------------------
FUNCTION VATRATE_ITEM_DEPS_DELETION(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(60) := 'TAX_SQL.VATRATE_ITEM_DEPS_DELETION';
   L_exists          VARCHAR2(1);

   cursor C_VAT_ITEM_EXISTS is
      select 'x'
        from vat_item vi
       where exists (select 'x'
                       from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vtbl
                      where vi.vat_code = NVL(vtbl.tax_code,vi.vat_code)
                        and vi.vat_rate = NVL(vtbl.tax_rate,vi.vat_rate)
                        and vi.active_date >= vtbl.active_date)
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_VAT_ITEM_EXISTS',
                    'VAT_ITEM, OBJ_TAX_INFO_TBL',
                    NULL);
   open C_VAT_ITEM_EXISTS;

   SQL_LIB.SET_MARK('FETCH',
                    'C_VAT_ITEM_EXISTS',
                    'VAT_ITEM, OBJ_TAX_INFO_TBL',
                    NULL);
   fetch C_VAT_ITEM_EXISTS into L_exists;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_VAT_ITEM_EXISTS',
                    'VAT_ITEM, OBJ_TAX_INFO_TBL',
                    NULL);
   close C_VAT_ITEM_EXISTS;

   if L_exists = 'x' then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_VAT_RATE',
                                            NULL,NULL,NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VATRATE_ITEM_DEPS_DELETION;
------------------------------------------------------------------------------------------
FUNCTION DELETE_ITEM_DEPS_TAXCODE(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
RETURN BOOLEAN  IS

   L_function     VARCHAR2(64) := 'TAX_SQL.DELETE_ITEM_DEPS_TAXCODE';

BEGIN

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if VATCODE_ITEM_DEPS_DELETION(O_error_message,
                                    IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_ITEM_DEPS_TAXCODE;
------------------------------------------------------------------------------------------
FUNCTION VATCODE_ITEM_DEPS_DELETION(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                                    IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)

   RETURN BOOLEAN IS

   L_function     VARCHAR2(64) := 'TAX_SQL.VATCODE_ITEM_DEPS_DELETION';
   L_dummy        VARCHAR2(1);

   cursor C_invc_merch_vat_validate is
      select 'x'
        from invc_merch_vat invc,
             TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vit
       where invc.vat_code = vit.tax_code
         and rownum = 1
   UNION ALL
      select 'x'
        from iif_merch_vat iif,
             TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vit
       where iif.vat_code = vit.tax_code
         and rownum = 1
   UNION ALL
      select 'x'
        from invc_non_merch inm,
             TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vit
       where inm.vat_code = vit.tax_code
         and rownum = 1;

   cursor C_item_deps_vat_validate is
      select 'x'
        from vat_deps,
             TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vit
       where vat_deps.vat_code = vit.tax_code
         and rownum = 1
   UNION ALL
      select 'x'
        from vat_item,
             TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) vit
       where vat_item.vat_code = vit.tax_code
         and rownum = 1;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_invc_merch_vat_validate',
                    'invc_merch_vat, iif_merch_vat, invc_non_merch, IO_tax_info_tbl',
                    IO_tax_info_tbl(IO_tax_info_tbl.count).tax_code);
   open C_invc_merch_vat_validate;

   SQL_LIB.SET_MARK('FETCH',
                    'C_invc_merch_vat_validate',
                    'invc_merch_vat, iif_merch_vat, invc_non_merch, IO_tax_info_tbl',
                    IO_tax_info_tbl(IO_tax_info_tbl.count).tax_code);
   fetch C_invc_merch_vat_validate into L_dummy;

   if L_dummy is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_VAT_INVC',
                                            NULL,
                                            NULL,
                                           NULL);
  SQL_LIB.SET_MARK('CLOSE' ,
                       'C_invc_merch_vat_validate',
                       'invc_merch_vat, iif_merch_vat, invc_non_merch, IO_tax_info_tbl',
                       IO_tax_info_tbl(IO_tax_info_tbl.count).tax_code);
      close C_invc_merch_vat_validate;
      return FALSE;
   end if;

   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_item_deps_vat_validate',
                    'vat_deps, IO_tax_info_tbl',
                    IO_tax_info_tbl(IO_tax_info_tbl.count).tax_code);
   open C_item_deps_vat_validate;

   SQL_LIB.SET_MARK('FETCH',
                    'C_item_deps_vat_validate',
                    'vat_deps, IO_tax_info_tbl',
                    IO_tax_info_tbl(IO_tax_info_tbl.count).tax_code);
   fetch C_item_deps_vat_validate into L_dummy;

   if L_dummy is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_VAT_CODE',
                                            NULL,
                                            NULL,
                                            NULL);

      SQL_LIB.SET_MARK('CLOSE' ,
                       'C_item_deps_vat_validate',
                       'vat_deps, IO_tax_info_tbl',
                       IO_tax_info_tbl(IO_tax_info_tbl.count).tax_code);
      close C_item_deps_vat_validate;
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE' ,
                    'C_item_deps_vat_validate',
                    'vat_deps, IO_tax_info_tbl',
                    IO_tax_info_tbl(IO_tax_info_tbl.count).tax_code);
   close C_item_deps_vat_validate;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END VATCODE_ITEM_DEPS_DELETION;
----------------------------------------------------------------------------------------
FUNCTION POST_TAX_AMOUNT(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program                      VARCHAR2(50)     := 'TAX_SQL.POST_TAX_AMOUNT';

BEGIN
   if IO_tax_info_tbl is NULL or IO_tax_info_tbl.count <= 0 then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'OBJ_TAX_INFO_TBL',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in ( LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if POST_VAT_AMOUNT(O_error_message,
                         IO_tax_info_tbl) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POST_TAX_AMOUNT;
----------------------------------------------------------------------------------------
FUNCTION POST_VAT_AMOUNT(O_error_message   IN OUT            RTK_ERRORS.RTK_TEXT%TYPE,
                         IO_tax_info_tbl   IN OUT   NOCOPY   OBJ_TAX_INFO_TBL)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'TAX_SQL.POST_VAT_AMOUNT';
   L_tax_calc_tbl        OBJ_TAX_CALC_TBL;
   ---
   L_input_count         NUMBER;
   L_output_count        NUMBER;

   L_from_vat_region               VAT_REGION.VAT_REGION%TYPE;
   L_from_vat_calc_type            VAT_REGION.VAT_CALC_TYPE%TYPE;
   L_to_vat_region                 VAT_REGION.VAT_REGION%TYPE;
   L_to_vat_calc_type              VAT_REGION.VAT_CALC_TYPE%TYPE;
   
   cursor C_BUILD_TAX_CALC_OBJ is
      select OBJ_TAX_CALC_REC(input.item,                --I_ITEM
                              input.pack_ind,            --I_PACK_IND
                              input.from_entity,         --I_FROM_ENTITY
                              DECODE(input.from_entity_type, 'S', 'ST', 'W', 'WH', input.from_entity_type),    --I_FROM_ENTITY_TYPE
                              input.to_entity,           --I_TO_ENTITY
                              DECODE(input.to_entity_type, 'S', 'ST', 'W', 'WH', input.to_entity_type),        --I_TO_ENTITY_TYPE
                              input.active_date,         --I_EFFECTIVE_FROM_DATE
                              input.amount,              --I_AMOUNT
                              NULL,                      --I_AMOUNT_CURR, n/a for SVAT, since vat rate is only pct
                              'N',                       --I_AMOUNT_TAX_INCL_IND, default to 'N' for SVAT
                              NULL,                      --I_origin_country_id
                              NULL,                      --O_CUM_TAX_PCT
                              NULL,                      --O_CUM_TAX_VALUE
                              NULL,                      --O_TOTAL_TAX_AMOUNT
                              NULL,                      --O_TOTAL_TAX_AMOUNT_CURR
                              NULL,                      --O_TOTAL_RECOVER_AMOUNT
                              NULL,                      --O_TOTAL_RECOVER_AMOUNT_CURR
                              NULL)                      --O_TAX_DETAIL_TBL
        from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) input;

   cursor C_BUILD_TAX_INFO_OBJ IS
      select OBJ_TAX_INFO_REC(input.item,
                              input.item_parent,
                              input.item_grandparent,
                              input.pack_ind,
                              input.merch_hier_level,
                              input.merch_hier_value,
                              input.from_tax_region,
                              input.from_tax_region_desc,
                              input.to_tax_region,
                              input.from_entity,
                              input.from_entity_type,
                              input.to_entity,
                              input.to_entity_type,
                              rslt.I_amount,
                              input.cost_retail_ind,
                              input.tax_incl_ind,
                              case
                                 when input.tran_code = 24 or
                                      input.tran_code = 83 and rslt.O_total_tax_amount > 0 then
                                    rslt.O_total_tax_amount * (-1)
                                 else
                                    rslt.O_total_tax_amount
                              end,  --tax_amount
                              input.tax_rate,
                              (select tax_code
                                 from TABLE(CAST(rslt.O_tax_detail_tbl AS OBJ_TAX_DETAIL_TBL)) taxdtl
                                where rownum = 1),
                              --input.tax_code,
                              input.tax_code_desc,
                              input.active_date,
                              input.inventory_ind,
                              input.ref_no_1,
                              input.ref_no_2,
                              input.tran_code,
                              input.taxable_base,
                              input.modified_taxable_base,
                              input.calculation_basis,
                              input.recoverable_amount,
                              input.currency,
                              input.reverse_vat_ind)
        from TABLE(CAST(IO_tax_info_tbl AS OBJ_TAX_INFO_TBL)) input,
             TABLE(CAST(L_tax_calc_tbl AS OBJ_TAX_CALC_TBL)) rslt
       where input.item = rslt.I_item
         and input.from_entity = rslt.I_from_entity
         and input.from_entity_type = DECODE(rslt.I_from_entity_type, 'ST', 'S', 'WH', 'W', rslt.I_from_entity_type)
         and input.active_date = rslt.I_effective_from_date;

   cursor C_FROM_VAT_REGION is
      select s.vat_region
        from store s,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where s.store = tbl.I_from_entity
         and tbl.I_from_entity_type  = 'ST'
      union all
      select w.vat_region
        from wh w,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where w.wh = tbl.I_from_entity
         and tbl.I_from_entity_type  = 'WH'
      union all
      select p.vat_region
        from partner p,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where p.partner_id = tbl.I_from_entity
         and p.partner_type = 'E'
         and tbl.I_from_entity_type  = 'E'
      union all
      select sp.vat_region
        from sups sp,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where sp.supplier = tbl.I_from_entity
         and tbl.I_from_entity_type  in ('SP','SU');

   cursor C_TO_VAT_REGION is
      select s.vat_region
        from store s,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where s.store = tbl.I_to_entity
         and tbl.I_to_entity_type  = 'ST'
      union all
      select w.vat_region
        from wh w,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where w.wh = tbl.I_to_entity
         and tbl.I_to_entity_type  = 'WH'
      union all
      select p.vat_region
        from partner p,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where p.partner_id = tbl.I_to_entity
         and p.partner_type = 'E'
         and tbl.I_to_entity_type  = 'E'
      union all
      select sp.vat_region
        from sups sp,
             TABLE(CAST(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tbl
       where sp.supplier = tbl.I_to_entity
         and tbl.I_to_entity_type  in ('SP','SU');
         
BEGIN

   L_input_count := IO_tax_info_tbl.COUNT;
   -- build OBJ_TAX_CALC_REC
   open C_BUILD_TAX_CALC_OBJ;
   fetch C_BUILD_TAX_CALC_OBJ bulk collect into L_tax_calc_tbl;
   close C_BUILD_TAX_CALC_OBJ;

   if L_tax_calc_tbl(L_tax_calc_tbl.count).I_from_entity is not NULL then
      open C_FROM_VAT_REGION;
      fetch C_FROM_VAT_REGION into L_from_vat_region;
      close C_FROM_VAT_REGION;
      
      if TAX_SQL.FETCH_VAT_APPL_TYPE (O_error_message, 
                                      L_from_vat_region,        
                                      L_from_vat_calc_type) = FALSE then     
         return FALSE;
      end if;
   end if;

   if L_tax_calc_tbl(L_tax_calc_tbl.count).I_to_entity is not NULL then   
      open C_TO_VAT_REGION;
      fetch C_TO_VAT_REGION into L_to_vat_region;
      close C_TO_VAT_REGION;
      
      if TAX_SQL.FETCH_VAT_APPL_TYPE (O_error_message, 
                                      L_to_vat_region,        
                                      L_to_vat_calc_type) = FALSE then     
         return FALSE;
      end if;
      
   end if;   
   --Assumption: all records in the collection are either for Cost or for Retail, no mixture
   if IO_tax_info_tbl(1).cost_retail_ind = 'C' then
      ---
      if (L_from_vat_region is not NULL and L_to_vat_region is not NULL ) then
         if L_from_vat_calc_type = 'S' and L_to_vat_calc_type = 'S' then 
            if L_from_vat_region = L_to_vat_region then
               if CALC_VAT_COST_TAX(O_error_message,
                                    L_tax_calc_tbl) = FALSE then
                  return FALSE;
               end if; 
            end if;
            ---
         elsif (L_from_vat_calc_type = 'E' and L_to_vat_calc_type = 'E') then 
            -- For 'E'xempt regions, set tax_exempt_ind = 'Y' 
            for i in L_tax_calc_tbl.first..L_tax_calc_tbl.last LOOP
               L_tax_calc_tbl(i).O_tax_exempt_ind := 'Y';
            END LOOP;
            ---
         elsif (L_from_vat_calc_type = 'C' or L_to_vat_calc_type = 'C') then
            -- Call custom tax_sql function if either vat_calc_type is 'C'ustom
            if CUSTOM_TAX_SQL.CALC_CTAX_COST_TAX(O_error_message, 
                                                 L_tax_calc_tbl) = FALSE then
               return FALSE;                                                 
            end if;
            ---
         end if; --if L_from_vat_calc_type = 'S' and L_to_vat_calc_type = 'S'
      elsif (L_from_vat_region is not NULL and L_to_vat_region is NULL ) then
         if L_from_vat_calc_type = 'S' then
            if CALC_VAT_COST_TAX(O_error_message,
                                 L_tax_calc_tbl) = FALSE then
               return FALSE;
            end if; 
            for i in L_tax_calc_tbl.first..L_tax_calc_tbl.last LOOP
               L_tax_calc_tbl(i).O_tax_exempt_ind := 'N';
            END LOOP;
            ---
         elsif L_from_vat_calc_type = 'E' then
            for i in L_tax_calc_tbl.first..L_tax_calc_tbl.last LOOP
               L_tax_calc_tbl(i).O_tax_exempt_ind := 'Y';
            END LOOP;
            ---
         elsif L_from_vat_calc_type = 'C' then
            if CUSTOM_TAX_SQL.CALC_CTAX_COST_TAX(O_error_message, 
                                                 L_tax_calc_tbl) = FALSE then
               return FALSE;                                                 
            end if;
            ---
         end if;--if L_from_vat_calc_type = 'S' 
      end if;--if (L_from_vat_region is not NULL and L_to_vat_region is not NULL )
      ---
   else  -- 'R'
      ---
      if (L_from_vat_region is not NULL and L_to_vat_region is not NULL ) then
         if L_from_vat_calc_type = 'S' and L_to_vat_calc_type = 'S' then 
            if L_from_vat_region = L_to_vat_region then
               if CALC_VAT_RETAIL_TAX(O_error_message,
                                      L_tax_calc_tbl) = FALSE then
                  return FALSE;
               end if; 
            end if;
            ---
         elsif (L_from_vat_calc_type = 'E' and L_to_vat_calc_type = 'E') then 
            -- For 'E'xempt regions, set tax_exempt_ind = 'Y' 
            for i in L_tax_calc_tbl.first..L_tax_calc_tbl.last LOOP
               L_tax_calc_tbl(i).O_tax_exempt_ind := 'Y';
            END LOOP;
            ---
         elsif (L_from_vat_calc_type = 'C' or L_to_vat_calc_type = 'C') then
            -- Call custom tax_sql function if either vat_calc_type is 'C'ustom
            if CUSTOM_TAX_SQL.CALC_CTAX_RETAIL_TAX(O_error_message, 
                                                   L_tax_calc_tbl) = FALSE then
               return FALSE;                                                 
            end if;
            ---
         end if; --if L_from_vat_calc_type = 'S' and L_to_vat_calc_type = 'S'
      elsif (L_from_vat_region is not NULL and L_to_vat_region is NULL ) then
         if L_from_vat_calc_type = 'S' then
            if CALC_VAT_RETAIL_TAX(O_error_message,
                                   L_tax_calc_tbl) = FALSE then
               return FALSE;
            end if; 
            for i in L_tax_calc_tbl.first..L_tax_calc_tbl.last LOOP
               L_tax_calc_tbl(i).O_tax_exempt_ind := 'N';
            END LOOP;
            ---
         elsif L_from_vat_calc_type = 'E' then
            for i in L_tax_calc_tbl.first..L_tax_calc_tbl.last LOOP
               L_tax_calc_tbl(i).O_tax_exempt_ind := 'Y';
            END LOOP;
            ---
         elsif L_from_vat_calc_type = 'C' then
            if CUSTOM_TAX_SQL.CALC_CTAX_RETAIL_TAX(O_error_message, 
                                                   L_tax_calc_tbl) = FALSE then
               return FALSE;                                                 
            end if;
            ---
         end if;--if L_from_vat_calc_type = 'S' 
      end if;--if (L_from_vat_region is not NULL and L_to_vat_region is not NULL )
      ---
   end if; --if IO_tax_info_tbl(1).cost_retail_ind = 'C'

   -- build OBJ_TAX_INFO_REC to return
   open C_BUILD_TAX_INFO_OBJ;
   IO_tax_info_tbl.delete();
   fetch C_BUILD_TAX_INFO_OBJ bulk collect into IO_tax_info_tbl;
   close C_BUILD_TAX_INFO_OBJ;

   -- check count
   L_output_count := IO_tax_info_tbl.COUNT;
   if L_input_count != L_output_count then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_OUTPUT',
                                            L_program,
                                            L_input_count,
                                            L_output_count);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POST_VAT_AMOUNT;
------------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_TAX_REGION_RATE(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_cum_tax_pct          IN OUT  VAT_ITEM.VAT_RATE%TYPE,
                                     I_item                 IN      VAT_ITEM.ITEM%TYPE,
                                     I_cost_retail_ind      IN      VAT_ITEM.VAT_TYPE%TYPE,
                                     I_effective_from_date  IN      VAT_ITEM.ACTIVE_DATE%TYPE)
   RETURN BOOLEAN IS

      L_program  VARCHAR2(64) :=  'TAX_SQL.GET_DEFAULT_TAX_REGION_RATE';

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_cost_retail_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_retail_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_cost_retail_ind NOT in ('R','C') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_INPUT',
                                            'I_cost_retail_ind: '||I_cost_retail_ind,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   if I_effective_from_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_effective_from_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;

   if LP_system_options_row.default_tax_type in (LP_GTAX, LP_SALES) then
      return TRUE;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      if GET_DEFAULT_VAT_REGION_RATE(O_error_message,
                                     O_cum_tax_pct,
                                     I_item,
                                     I_cost_retail_ind,
                                     I_effective_from_date) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_DEFAULT_TAX_REGION_RATE;
------------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_VAT_REGION_RATE(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_cum_tax_pct          IN OUT  VAT_ITEM.VAT_RATE%TYPE,
                                     I_item                 IN      VAT_ITEM.ITEM%TYPE,
                                     I_cost_retail_ind      IN      VAT_ITEM.VAT_TYPE%TYPE,
                                     I_effective_from_date  IN      VAT_ITEM.ACTIVE_DATE%TYPE)
   RETURN BOOLEAN IS

   L_program                  VARCHAR2(64) :=  'TAX_SQL.GET_DEFAULT_VAT_REGION_RATE';
   L_pack_ind                 ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind             ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind            ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type                ITEM_MASTER.PACK_TYPE%TYPE;
   L_packitem                 ITEM_MASTER.ITEM%TYPE;

   L_packitem_unit_cost       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_pack_cost_in_vat         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := 0;
   L_pack_cost_ex_vat         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE    := 0;
   L_vat_rate                 VAT_ITEM.VAT_RATE%TYPE;

   L_packitem_unit_retail     ITEM_LOC.UNIT_RETAIL%TYPE;
   L_standard_uom_prim        ITEM_MASTER.STANDARD_UOM%TYPE;
   L_selling_unit_retail_prim ITEM_LOC.SELLING_UNIT_RETAIL%TYPE;
   L_selling_uom_prim         ITEM_LOC.SELLING_UOM%TYPE;
   L_class_vat_ind            CLASS.CLASS_VAT_IND%TYPE;
   L_pack_retail_in_vat       ITEM_LOC.UNIT_RETAIL%TYPE           := 0;
   L_pack_retail_ex_vat       ITEM_LOC.UNIT_RETAIL%TYPE           := 0;

   cursor C_PACKSKU is
      select item, qty
        from v_packsku_qty
       where pack_no = I_item;

   cursor C_COST is
      select unit_cost
        from item_supp_country
       where item_supp_country.item = L_packitem
         and item_supp_country.primary_country_ind = 'Y'
         and item_supp_country.primary_supp_ind = 'Y';

   cursor C_VAT_SKU is
      select vat_rate
        from vat_item
       where item = I_item
         and vat_region  = LP_system_options_row.default_vat_region
         and vat_type in (I_cost_retail_ind, LP_TAX_TYPE_BOTH)
         and active_date <= I_effective_from_date
    order by active_date desc;  -- Much faster than then the correlated subquery.

   cursor C_CLASS_VAT is
      select cl.class_vat_ind
        from item_master im,
             class cl
       where im.item  = L_packitem
         and im.dept  = cl.dept
         and im.class = cl.class;

BEGIN

   if LP_system_options_row.default_vat_region is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DEFAULT_VAT_REG',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   --
   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;
   --
   if L_pack_ind = 'Y' and (L_pack_type = 'B' or L_pack_type is NULL) then
      if I_cost_retail_ind = 'C' then
         for rec in C_PACKSKU LOOP
            L_packitem := rec.item;
            open C_COST;
            fetch C_COST into L_packitem_unit_cost;
            close C_COST;
            ---
            L_pack_cost_ex_vat := L_pack_cost_ex_vat
                                  + L_packitem_unit_cost * rec.qty;
            ---
            open  C_VAT_SKU;
            fetch C_VAT_SKU into L_vat_rate;
            if C_VAT_SKU%NOTFOUND then
               close C_VAT_SKU;
               O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                     L_packitem,
                                                     To_char(LP_system_options_row.default_vat_region),
                                                     NULL);
               return FALSE;
            end if;
            close C_VAT_SKU;
            --
            L_pack_cost_in_vat := L_pack_cost_in_vat
                                  + rec.qty * L_packitem_unit_cost * (1 + L_vat_rate /100);
         END LOOP;
         --
         if L_pack_cost_ex_vat > 0 then
            O_cum_tax_pct := ((L_pack_cost_in_vat / L_pack_cost_ex_vat) - 1) * 100;
         else
            O_cum_tax_pct := 0;
         end if;
      else --Retail
         for rec in C_PACKSKU LOOP
            L_packitem := rec.item;
            if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                                    L_packitem_unit_cost,
                                                    L_packitem_unit_retail,
                                                    L_standard_uom_prim,
                                                    L_selling_unit_retail_prim,
                                                    L_selling_uom_prim,
                                                    I_item,
                                                    'R') = FALSE then
               return FALSE;
            end if;
            --
            open  C_VAT_SKU;
            fetch C_VAT_SKU into L_vat_rate;
            if C_VAT_SKU%NOTFOUND then
               close C_VAT_SKU;
               O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                                     L_packitem,
                                                     To_char(LP_system_options_row.default_vat_region),
                                                     NULL);
               return FALSE;
            end if;
            close C_VAT_SKU;
            ---
            open C_CLASS_VAT;
            fetch C_CLASS_VAT into L_class_vat_ind;
            close C_CLASS_VAT;
            ---
            -- If system's class_level_vat_ind is 'N', unit retail in RMS is always tax inclusive.
            -- If system's class_level_vat_ind is 'Y', unit retail in RMS is tax inclusive or exclusive depending on the class.class_vat_ind.
            ---
            if LP_system_options_row.class_level_vat_ind = 'N' or
               LP_system_options_row.class_level_vat_ind = 'Y' and L_class_vat_ind = 'Y' then
               L_pack_retail_in_vat := L_pack_retail_in_vat
                  + L_packitem_unit_retail * rec.qty;
               L_pack_retail_ex_vat := L_pack_retail_ex_vat
                  + rec.qty * L_packitem_unit_retail /(1 + L_vat_rate /100);
            else
               L_pack_retail_ex_vat := L_pack_retail_ex_vat
                  + L_packitem_unit_retail * rec.qty;
               L_pack_retail_in_vat := L_pack_retail_in_vat
                  + rec.qty * L_packitem_unit_retail * (1 + L_vat_rate /100);
            end if;
         END LOOP;
         if L_pack_retail_ex_vat > 0 then
            O_cum_tax_pct := ((L_pack_retail_in_vat / L_pack_retail_ex_vat) - 1) * 100;
         else
            O_cum_tax_pct := 0;
         end if;
      end if;
   else --not a pack
      open  C_VAT_SKU;
      fetch C_VAT_SKU into O_cum_tax_pct;
      if C_VAT_SKU%NOTFOUND then
         close C_VAT_SKU;
         O_error_message := SQL_LIB.CREATE_MSG('VAT_RATE_NOTFOUND_SKU',
                                               I_item,
                                               To_char(LP_system_options_row.default_vat_region),
                                               NULL);
         return FALSE;
      end if;
      close C_VAT_SKU;
   end if; -- pack_item

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_DEFAULT_VAT_REGION_RATE;
-------------------------------------------------------------------------------------
FUNCTION GET_RETAIL_TAX_IND (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_tax_ind         IN OUT   VARCHAR2,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS
   L_program               VARCHAR2(64) :=  'TAX_SQL.GET_RETAIL_TAX_IND';
   L_class_vat_ind         CLASS.CLASS_VAT_IND%TYPE;
   --
   cursor C_GET_CLASS_VAT_IND is
      select c.class_vat_ind
        from class c,
             item_master im
       where im.class = c.class
         and im.dept  = c.dept
         and im.item  = I_item;
BEGIN

   -- default to tax inclusive for retail
   O_tax_ind := 'Y';

   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if LP_system_options_row.default_tax_type = LP_SALES then
      O_tax_ind := 'N';
      return TRUE;
   end if;

   -- Unit retail in RMS is tax always inclusive for GTAX.
   -- Unit retail in RMS is tax always inclusive for SVAT and SALES if system's class_level_vat_ind is 'N'.
   -- For SVAT and SALES, if system's class_level_vat_ind is 'Y', unit retail in RMS is tax inclusive or
   -- tax exclusive depending on the item class's CLASS_VAT_IND.

   if LP_system_options_row.default_tax_type in (LP_SVAT, LP_GTAX) and
      LP_system_options_row.class_level_vat_ind = 'Y'  then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_CLASS_VAT_IND',
                       'ITEM_MASTER'||','||'CLASS',
                       I_item);
      open C_GET_CLASS_VAT_IND;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_CLASS_VAT_IND',
                       'ITEM_MASTER'||','||'CLASS',
                       I_item);
      fetch C_GET_CLASS_VAT_IND into L_class_vat_ind;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_CLASS_VAT_IND',
                       'ITEM_MASTER'||','||'CLASS',
                       I_item);
      close C_GET_CLASS_VAT_IND;
      if L_class_vat_ind = 'N' then
         O_tax_ind := 'N';
      end if;
   end if;
   --
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
   return FALSE;
END GET_RETAIL_TAX_IND;
-------------------------------------------------------------------------------------
FUNCTION ADD_RETAIL_TAX (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_amount              IN OUT   ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_item                IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_ind            IN       ITEM_MASTER.PACK_IND%TYPE,
                         I_dept                IN       ITEM_MASTER.DEPT%TYPE,
                         I_class               IN       ITEM_MASTER.CLASS%TYPE,
                         I_location            IN       ITEM_LOC.LOC%TYPE,
                         I_loc_type            IN       ITEM_LOC.LOC_TYPE%TYPE,
                         I_effective_from_date IN       DATE,
                         I_amount              IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                         I_currency_code       IN       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                         I_qty                 IN       ITEM_LOC_SOH.STOCK_ON_HAND%TYPE DEFAULT 1,
                         I_call_type           IN       VARCHAR2 DEFAULT LP_CALL_TYPE_NIL )
RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'TAX_SQL.ADD_RETAIL_TAX';

   L_tax_calc_tbl  OBJ_TAX_CALC_TBL              := OBJ_TAX_CALC_TBL ();
   L_tax_calc_rec  OBJ_TAX_CALC_REC              := OBJ_TAX_CALC_REC ();
   L_tax_add_tbl   OBJ_TAX_RETAIL_ADD_REMOVE_TBL := OBJ_TAX_RETAIL_ADD_REMOVE_TBL();
   L_tax_add_rec   OBJ_TAX_RETAIL_ADD_REMOVE_REC := OBJ_TAX_RETAIL_ADD_REMOVE_REC();
   --
   I_from_vat_region VAT_REGION.VAT_REGION%TYPE;
   L_vat_calc_type   VAT_REGION.VAT_CALC_TYPE%TYPE;
   
   Cursor C_VAT_REGION is 
     select wh.vat_region
       from wh wh
       where wh = I_location
     union all
     select s.vat_region
      from  store s
      where store = I_location;  

   --

BEGIN
   ---
   if LP_system_options_row.default_tax_type is NULL then
      if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               LP_system_options_row) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   if LP_system_options_row.default_tax_type = LP_SALES  then
      O_amount := I_amount;
   end if;
   --

   if  LP_system_options_row.default_tax_type = LP_GTAX then
      L_tax_calc_rec.I_item                 := I_item;
      L_tax_calc_rec.I_pack_ind             := I_pack_ind;
      L_tax_calc_rec.I_from_entity          := I_location;
      if I_loc_type = 'W' then
         L_tax_calc_rec.I_from_entity_type     := 'WH';
      else
         L_tax_calc_rec.I_from_entity_type     := 'ST';
      end if;
      L_tax_calc_rec.I_effective_from_date  := I_effective_from_date;
      L_tax_calc_rec.I_amount               := I_amount;
      L_tax_calc_rec.I_amount_tax_incl_ind  := 'N';
      L_tax_calc_rec.I_amount_curr          := I_currency_code;

      L_tax_calc_tbl.EXTEND;
      L_tax_calc_tbl(L_tax_calc_tbl.COUNT)   := L_tax_calc_rec;
      --
      if CALC_GTAX_RETAIL_TAX(O_error_message,
                              L_tax_calc_tbl,
                              I_call_type) = FALSE then
         return FALSE;
      end if;
      --
      O_amount  :=  (I_amount/(1-L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_cum_tax_pct/100)) +  L_tax_calc_tbl(L_tax_calc_tbl.COUNT).O_cum_tax_value;
   elsif LP_system_options_row.default_tax_type = LP_SVAT then
      L_tax_add_rec.I_item                 := I_item;
      L_tax_add_rec.I_dept                 := I_dept;
      L_tax_add_rec.I_class                := I_class;
      L_tax_add_rec.I_location             := I_location;
      L_tax_add_rec.I_loc_type             := I_loc_type;
      L_tax_add_rec.I_effective_from_date  := I_effective_from_date;
      L_tax_add_rec.I_amount               := I_amount;
      L_tax_add_rec.I_qty                  := I_qty;

      L_tax_add_tbl.EXTEND;
      L_tax_add_tbl(L_tax_add_tbl.COUNT)   := L_tax_add_rec;

          open C_VAT_REGION;
          fetch C_VAT_REGION into I_from_vat_region;
          close C_VAT_REGION;

         if I_from_vat_region is not null then
           if TAX_SQL.FETCH_VAT_APPL_TYPE(O_error_message,
                                          I_from_vat_region,
                                          L_vat_calc_type) = FALSE then
              return FALSE;
            end if;
         else 
              O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                    'I_from_vat_region',
                                                    L_program,
                                                    to_char(SQLCODE));
         end if;

          if  L_vat_calc_type = 'C' then
              if CUSTOM_TAX_SQL.ADD_CTAX_RETAIL_TAX(O_error_message,
                                                    L_tax_add_tbl) = FALSE then
                  return FALSE;
              end if;     
          elsif L_vat_calc_type = 'S' then
            if ADD_VAT_RETAIL_TAX(O_error_message,
                                  L_tax_add_tbl)= FALSE then
               return FALSE;
            end if;
          end if;
      O_amount := L_tax_add_tbl(1).O_amount;
   end if;

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_RETAIL_TAX;
-------------------------------------------------------------------------------------
FUNCTION DEL_VAT_CODES_EXT (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_vat_code        IN      VAT_CODES.VAT_CODE%TYPE)
RETURN BOOLEAN IS
   L_table           VARCHAR2(30);
   L_program         VARCHAR2(60) := 'TAX_SQL.DEL_VAT_CODES_EXT';

   cursor C_LOCK_VAT_CODES_CFA_EXT is
     select 'x'
       from vat_codes_cfa_ext
      where vat_code = I_vat_code
   for update nowait;
BEGIN

   L_table := 'VAT_CODES_CFA_EXT'
   ;
   open C_LOCK_VAT_CODES_CFA_EXT;
   close C_LOCK_VAT_CODES_CFA_EXT;

   -- delete from vat_codes CFA extension table
   delete from vat_codes_cfa_ext
      where vat_code = I_vat_code;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DEL_VAT_CODES_EXT;
------------------------------------------------------------------------------------------------------
FUNCTION CHECK_DUP_VAT_CODE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists        IN OUT BOOLEAN,
                             I_vat_code      IN     VAT_CODES.VAT_CODE%TYPE)

RETURN BOOLEAN IS
   L_program          VARCHAR2(50) := 'TAX_SQL.CHECK_DUP_VAT_CODE';
   L_vat_code_exists  VARCHAR2(1)  := NULL;

   cursor C_CHECK_VAT_CODE is
      select 'x'
        from vat_codes
       where vat_code = I_vat_code;

BEGIN

   if I_vat_code is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_vat_code',
                                           L_program,
                                           NULL);
      return FALSE;
   end if;

   open C_CHECK_VAT_CODE;
   fetch C_CHECK_VAT_CODE into L_vat_code_exists;
   if (C_CHECK_VAT_CODE%NOTFOUND) then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   close C_CHECK_VAT_CODE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_DUP_VAT_CODE;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_RATE_INFO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_tax_rate        IN OUT   VAT_CODE_RATES.VAT_RATE%TYPE,
                            O_tax_code        IN OUT   VAT_CODES.VAT_CODE%TYPE,
                            O_exempt_ind      IN OUT   VARCHAR2,
                            I_item            IN       ITEM_LOC_SOH.ITEM%TYPE,
                            I_from_loc_type   IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                            I_from_loc        IN       ITEM_LOC_SOH.LOC%TYPE,
                            I_to_loc_type     IN       ITEM_LOC_SOH.LOC_TYPE%TYPE,
                            I_to_loc          IN       ITEM_LOC_SOH.LOC%TYPE,
                            I_cost_retail_ind IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(64)   := 'TAX_SQL.GET_TAX_RATE_INFO';


   L_tax_calc_tbl        OBJ_TAX_CALC_TBL              := OBJ_TAX_CALC_TBL(); 
   L_tax_calc_rec        OBJ_TAX_CALC_REC              := OBJ_TAX_CALC_REC();
   L_from_loc_type       VARCHAR2(2);
   L_to_loc_type         VARCHAR2(2);
   


BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_item',L_program,NULL);
      return FALSE;
   end if;
   if I_from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_from_loc_type',L_program,NULL);
      return FALSE;
   end if;
   if I_from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_from_loc',L_program,NULL);
      return FALSE;
   end if;
   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_from_loc_type',L_program,NULL);
      return FALSE;
   end if;
   if I_to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL','I_to_loc',L_program,NULL);
      return FALSE;
   end if;
   if I_cost_retail_ind is NULL   then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL', 'I_cost_retail_ind',L_program,NULL);
      return FALSE;
   end if;
   if I_cost_retail_ind NOT in ('C', 'R') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_cost_retail_ind',L_program,NULL);
      return FALSE;
   end if; 
   
   if I_from_loc_type = 'W' then
      L_from_loc_type := 'WH';
   elsif I_from_loc_type ='S' then
      L_from_loc_type := 'ST';
   else
      L_from_loc_type := I_from_loc_type;
   end if;
   
   if I_to_loc_type = 'W' then
      L_to_loc_type := 'WH';
   elsif I_to_loc_type ='S' then
      L_to_loc_type := 'ST';
   else
      L_to_loc_type := I_to_loc_type;
   end if;
 

   --Assign the vale to collection
    L_tax_calc_rec.I_item             := I_item;
    L_tax_calc_rec.I_from_entity      := I_from_loc;
    L_tax_calc_rec.I_from_entity_type := L_from_loc_type;
    L_tax_calc_rec.I_to_entity        := I_to_loc;
    L_tax_calc_rec.I_to_entity_type   := L_to_loc_type;
    
    L_tax_calc_tbl.EXTEND();
    L_tax_calc_tbl(L_tax_calc_tbl.count) := L_tax_calc_rec;
  
  if I_cost_retail_ind = 'R' then
      if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                 L_tax_calc_tbl) = FALSE then
         return FALSE;
      end if; 
  elsif I_cost_retail_ind ='C' then
      if TAX_SQL.CALC_COST_TAX(O_error_message,
                               L_tax_calc_tbl) = FALSE then
         return FALSE;
      end if;
  end if; 
    
    if L_tax_calc_tbl.count > 0 and L_tax_calc_tbl.count is not null then
        if L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_exempt_ind = 'Y' then
           O_tax_code    := NULL;
           O_tax_rate   := NULL;
           O_exempt_ind := 'Y';
        else
           O_tax_rate  := L_tax_calc_tbl(L_tax_calc_tbl.count).O_cum_tax_pct;
           if L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl is not null and L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl.count > 0 then
              O_tax_code := L_tax_calc_tbl(L_tax_calc_tbl.count).O_tax_detail_tbl(1).tax_code;
           else    
              O_tax_code := NULL;
           end if;
        end if;
    end if;

   
 
   return TRUE;

EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TAX_RATE_INFO;
------------------------------------------------------------------------------------------------------
FUNCTION FETCH_VAT_APPL_TYPE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_vat_region      IN       STORE.VAT_REGION%TYPE,
                              O_vat_appl_type   OUT   VAT_REGION.VAT_CALC_TYPE%TYPE)
RETURN BOOLEAN IS

  L_program                   VARCHAR2(64)   := 'TAX_SQL.FETCH_VAT_APPL_TYPE';
  L_vat_calc_type             VAT_REGION.VAT_CALC_TYPE%TYPE := NULL;
  
  cursor C_GET_APPLICATION_TYPE is
   select vat_calc_type
     from vat_region
    where vat_region =  I_vat_region;

BEGIN
    
    if I_vat_region is null then
       O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_vat_region',
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
    end if;
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_APPLICATION_TYPE',
                    'vat_region',
                     to_char(I_vat_region));
   open C_GET_APPLICATION_TYPE;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_APPLICATION_TYPE',
                    'vat_region',
                     to_char(I_vat_region));
   fetch C_GET_APPLICATION_TYPE into L_vat_calc_type;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_APPLICATION_TYPE',
                    'vat_region',
                     to_char(I_vat_region));   
    close C_GET_APPLICATION_TYPE;

   if L_vat_calc_type is NULL then
      O_vat_appl_type := NULL;
   else 
      O_vat_appl_type := L_vat_calc_type;
   end if;  

  return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_APPLICATION_TYPE%ISOPEN then
        close C_GET_APPLICATION_TYPE;
      end if;  
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END FETCH_VAT_APPL_TYPE;
------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
FUNCTION CHECK_TAX_INFO_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists           OUT BOOLEAN,
                              I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(50) := 'TAX_SQL.CHECK_TAX_INFO_EXIST';
   L_tax_info_tbl     OBJ_TAX_INFO_TBL   := OBJ_TAX_INFO_TBL();
   L_tax_info_rec     OBJ_TAX_INFO_REC   := OBJ_TAX_INFO_REC();

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --
   L_tax_info_tbl.EXTEND;
   L_tax_info_rec.item := I_item;
   L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
   --
   if GET_TAX_INFO(O_error_message,
                   L_tax_info_tbl) = FALSE then
      return FALSE;
   end if;

   if L_tax_info_tbl.count > 0  then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_TAX_INFO_EXIST;
----------------------------------------------------------------------------------------
FUNCTION GET_AND_INSERT_UPDATE_TAX_SKU(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_item             IN     ITEM_MASTER.ITEM%TYPE,
                                       I_merch_hier_level IN     NUMBER,
                                       I_merch_hier_value IN     NUMBER,
                                       I_cost_retail_ind  IN     VARCHAR2,
                                       I_active_date      IN     DATE)
RETURN BOOLEAN IS
   L_program          VARCHAR2(50) := 'TAX_SQL.GET_AND_INSERT_UPDATE_TAX_SKU';
   L_tax_info_tbl     OBJ_TAX_INFO_TBL   := OBJ_TAX_INFO_TBL();
   L_tax_info_rec     OBJ_TAX_INFO_REC   := OBJ_TAX_INFO_REC();
   L_run_report       BOOLEAN;

BEGIN

   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --
   if I_merch_hier_level is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_merch_hier_level',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --
   if I_merch_hier_value is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_merch_hier_value',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --
   if I_cost_retail_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_retail_ind',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   --
   if I_active_date is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_active_date',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_tax_info_tbl.EXTEND;
   L_tax_info_rec.item                  := I_item;
   L_tax_info_rec.merch_hier_level      := I_merch_hier_level;
   L_tax_info_rec.merch_hier_value      := I_merch_hier_value;
   L_tax_info_rec.cost_retail_ind       := I_cost_retail_ind;
   L_tax_info_rec.active_date           := I_active_date;
   L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
   --
   if GET_TAX_INFO(O_error_message,
                   L_tax_info_tbl) = FALSE then
      return FALSE;
   end if;
   --    
   if TAX_SQL.INSERT_UPDATE_TAX_SKU(O_error_message,
                                    L_run_report,
                                    L_tax_info_tbl) = FALSE then
      return FALSE;
   end if;
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_AND_INSERT_UPDATE_TAX_SKU;
------------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_CODE_DESC_WRP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               IO_tax_info_tbl IN OUT NOCOPY OBJ_TAX_INFO_TBL)
RETURN INTEGER is
   L_program VARCHAR2(60) := 'TAX_SQL.GET_TAX_CODE_DESC_WRP';
BEGIN
   if GET_TAX_CODE_DESC( O_error_message, IO_tax_info_tbl)=false THEN
      return 0;
   else
      return 1;
   end if;
EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      return 0;
END GET_TAX_CODE_DESC_WRP;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_CODE_WRP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_tax_info_tbl IN OUT NOCOPY OBJ_TAX_INFO_TBL)
RETURN INTEGER is
   L_program VARCHAR2(60) := 'TAX_SQL.GET_TAX_CODE_WRP';
BEGIN
   if GET_TAX_CODE( O_error_message, IO_tax_info_tbl)=false THEN
      return 0;
   else
      return 1;
   end if;
EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      return 0;
END GET_TAX_CODE_WRP;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_TAX_RATE_BY_CODE_WRP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  IO_tax_code_rate_tbl    IN OUT   NOCOPY   OBJ_TAX_CODE_RATE_TBL)
  RETURN INTEGER is
   L_program VARCHAR2(60) := 'TAX_SQL.GET_TAX_RATE_BY_CODE_WRP';
BEGIN
   if GET_TAX_RATE_BY_CODE( O_error_message, IO_tax_code_rate_tbl)=false THEN
      return 0;
   else
      return 1;
   end if;
EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, NULL);
      return 0;
END GET_TAX_RATE_BY_CODE_WRP;
------------------------------------------------------------------------------------------------------
END TAX_SQL;
/