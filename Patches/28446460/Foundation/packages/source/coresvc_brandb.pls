CREATE OR REPLACE PACKAGE BODY CORESVC_BRAND AS
----------------------------------------------------------------------------------------------
   cursor C_SVC_BRAND(I_process_id NUMBER,
                      I_chunk_id   NUMBER) is
      select pk_brand.rowid         as pk_brand_rid,
             st.rowid               as st_rid,
             st.brand_description,
             UPPER(st.brand_name)   as brand_name,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)       as action,
             st.process$status,
             pk_brand.create_datetime,
             pk_brand.create_id
        from svc_brand st,
             brand pk_brand,
             dual
       where st.process_id        = I_process_id
         and st.chunk_id          = I_chunk_id
         and UPPER(st.brand_name) = UPPER(pk_brand.brand_name (+));

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type BRAND_TL_tab IS TABLE OF BRAND_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang    LANG.LANG%TYPE;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
   L_program VARCHAR2(64):='CORESVC_BRAND.GET_SHEET_NAME_TRANS';
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );

   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS
   L_sheets        S9T_PKG.names_map_typ;
   brand_cols      S9T_PKG.names_map_typ;
   brand_tl_cols   S9T_PKG.names_map_typ;
BEGIN
   L_sheets                :=S9T_PKG.get_sheet_names(I_file_id);

   brand_cols              :=S9T_PKG.get_col_names(I_file_id,
                                                   BRAND_sheet);
   BRAND$ACTION            := brand_cols('ACTION');
   BRAND$BRAND_NAME        := brand_cols('BRAND_NAME');
   BRAND$BRAND_DESCRIPTION := brand_cols('BRAND_DESCRIPTION');

   brand_tl_cols              :=S9T_PKG.get_col_names(I_file_id,
                                                      BRAND_TL_sheet);
   BRAND_TL$ACTION            := brand_tl_cols('ACTION');
   BRAND_TL$LANG              := brand_tl_cols('LANG');
   BRAND_TL$BRAND_NAME        := brand_tl_cols('BRAND_NAME');
   BRAND_TL$BRAND_DESCRIPTION := brand_tl_cols('BRAND_DESCRIPTION');
END POPULATE_NAMES;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_BRAND( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = BRAND_sheet )
               select s9t_row(s9t_cells(CORESVC_BRAND.action_mod,
                                        brand_name,
                                        brand_description))
                 from brand ;
END POPULATE_BRAND;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_BRAND_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = BRAND_TL_sheet )
               select s9t_row(s9t_cells(CORESVC_BRAND.action_mod,
                                        lang,
                                        brand_name,
                                        brand_description))
                 from brand_tl;
END POPULATE_BRAND_TL;
----------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS

   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(BRAND_sheet);
   L_file.sheets(l_file.get_sheet_index(BRAND_sheet)).column_headers := s9t_cells('ACTION',
                                                                                  'BRAND_NAME',
                                                                                  'BRAND_DESCRIPTION');

   L_file.add_sheet(BRAND_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(BRAND_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                     'LANG',
                                                                                     'BRAND_NAME',
                                                                                     'BRAND_DESCRIPTION');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_BRAND.CREATE_S9T';
   L_file      s9t_file;

BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_BRAND(O_file_id);
      POPULATE_BRAND_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_BRAND( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_BRAND.process_id%TYPE) IS

   TYPE svc_brand_col_typ IS TABLE OF SVC_BRAND%ROWTYPE;
   L_temp_rec SVC_BRAND%ROWTYPE;
   svc_brand_col   svc_brand_col_typ := NEW svc_brand_col_typ();
   L_process_id    SVC_BRAND.PROCESS_ID%TYPE;
   L_error         BOOLEAN := FALSE;
   L_default_rec   SVC_BRAND%ROWTYPE;

   cursor C_MANDATORY_IND is
      select brand_description_mi,
             brand_name_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'BRAND')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('BRAND_DESCRIPTION' as brand_description,
                                       'BRAND_NAME'        as brand_name,
                                       null                as dummy));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_BRAND';
   L_pk_columns    VARCHAR2(255)  := 'Brand Name';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select brand_description_dv,
                      brand_name_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'BRAND')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('BRAND_DESCRIPTION' as brand_description,
                                                'BRAND_NAME'        as brand_name,
                                                NULL                as dummy)))
   LOOP
      BEGIN
         L_default_rec.brand_description := rec.brand_description_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'BRAND ' ,
                            NULL,
                           'BRAND_DESCRIPTION ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.brand_name := rec.brand_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'BRAND ' ,
                            NULL,
                           'BRAND_NAME ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(BRAND$ACTION)                   as ACTION,
                      r.get_cell(BRAND$BRAND_DESCRIPTION)        as brand_description,
                      UPPER(r.get_cell(BRAND$BRAND_NAME))        as brand_name,
                      r.get_row_seq()                            as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(BRAND_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.brand_description := rec.brand_description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_sheet,
                            rec.row_seq,
                            'BRAND_DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.brand_name := rec.brand_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_sheet,
                            rec.row_seq,
                            'BRAND_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_BRAND.action_new then
         L_temp_rec.brand_description := NVL( L_temp_rec.brand_description,L_default_rec.brand_description);
         L_temp_rec.brand_name        := NVL( L_temp_rec.brand_name,L_default_rec.brand_name);
      end if;
      if not (L_temp_rec.brand_name is NOT NULL
         and 1 = 1) then
         WRITE_S9T_ERROR( I_file_id,
                          BRAND_sheet,
                          rec.row_seq,

                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_brand_col.extend();
         svc_brand_col(svc_brand_col.COUNT()) := l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_brand_col.COUNT SAVE EXCEPTIONS
      merge into SVC_BRAND st
      using(select
                  (case
                   when l_mi_rec.brand_description_mi = 'N'
                    and svc_brand_col(i).action = CORESVC_BRAND.action_mod
                    and s1.brand_description IS NULL then
                        mt.brand_description
                   else s1.brand_description
                   end) as brand_description,
                  (case
                   when l_mi_rec.brand_name_mi = 'N'
                    and svc_brand_col(i).action = CORESVC_BRAND.action_mod
                    and s1.brand_name IS NULL then
                        mt.brand_name
                   else s1.brand_name
                   end) as brand_name,
                  null as dummy
              from (select svc_brand_col(i).brand_description as brand_description,
                           svc_brand_col(i).brand_name        as brand_name,
                           null                               as dummy
                      from dual ) s1,
                   BRAND mt
             where mt.brand_name (+) = s1.brand_name
               and 1 = 1 )sq
                on (st.brand_name = sq.brand_name and
                    svc_brand_col(i).ACTION IN (CORESVC_BRAND.action_mod,CORESVC_BRAND.action_del))
      when matched then
      update
         set process_id        = svc_brand_col(i).process_id ,
             chunk_id          = svc_brand_col(i).chunk_id ,
             row_seq           = svc_brand_col(i).row_seq ,
             action            = svc_brand_col(i).action ,
             process$status    = svc_brand_col(i).process$status ,
             brand_description = sq.brand_description ,
             create_id         = svc_brand_col(i).create_id ,
             create_datetime   = svc_brand_col(i).create_datetime ,
             last_upd_id       = svc_brand_col(i).last_upd_id ,
             last_upd_datetime = svc_brand_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             brand_description ,
             brand_name ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_brand_col(i).process_id ,
             svc_brand_col(i).chunk_id ,
             svc_brand_col(i).row_seq ,
             svc_brand_col(i).action ,
             svc_brand_col(i).process$status ,
             sq.brand_description ,
             sq.brand_name ,
             svc_brand_col(i).create_id ,
             svc_brand_col(i).create_datetime ,
             svc_brand_col(i).last_upd_id ,
             svc_brand_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            BRAND_sheet,
                            svc_brand_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_BRAND;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_BRAND_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id   IN   SVC_BRAND_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_BRAND_TL_COL_TYP IS TABLE OF SVC_BRAND_TL%ROWTYPE;
   L_temp_rec         SVC_BRAND_TL%ROWTYPE;
   svc_brand_tl_col   svc_brand_tl_col_typ := NEW svc_brand_tl_col_typ();
   L_process_id       SVC_BRAND_TL.PROCESS_ID%TYPE;
   L_error            BOOLEAN := FALSE;
   L_default_rec      SVC_BRAND_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select brand_description_mi,
             brand_name_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'BRAND_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('BRAND_DESCRIPTION' as brand_description,
                                       'BRAND_NAME'        as brand_name,
                                       'LANG'              as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_BRAND_TL';
   L_pk_columns    VARCHAR2(255)  := 'Brand Name, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select brand_description_dv,
                      brand_name_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'BRAND_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('BRAND_DESCRIPTION' as brand_description,
                                                'BRAND_NAME'        as brand_name,
                                                'LANG'              as lang)))
   LOOP
      BEGIN
         L_default_rec.brand_description := rec.brand_description_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_TL_sheet ,
                            NULL,
                           'BRAND_DESCRIPTION' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.brand_name := rec.brand_name_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           BRAND_TL_sheet ,
                            NULL,
                           'BRAND_NAME' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_TL_sheet ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(BRAND_TL$ACTION)                   as ACTION,
                      r.get_cell(BRAND_TL$BRAND_DESCRIPTION)        as brand_description,
                      UPPER(r.get_cell(BRAND_TL$BRAND_NAME))        as brand_name,
                      UPPER(r.get_cell(BRAND_TL$LANG))              as lang,
                      r.get_row_seq()                               as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(BRAND_TL_sheet))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.brand_description := rec.brand_description;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_TL_sheet,
                            rec.row_seq,
                            'BRAND_DESCRIPTION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.brand_name := rec.brand_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_TL_sheet,
                            rec.row_seq,
                            'BRAND_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BRAND_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_BRAND.action_new then
         L_temp_rec.brand_description := NVL( L_temp_rec.brand_description,L_default_rec.brand_description);
         L_temp_rec.brand_name        := NVL( L_temp_rec.brand_name,L_default_rec.brand_name);
         L_temp_rec.lang              := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.brand_name is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         BRAND_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_brand_tl_col.extend();
         svc_brand_tl_col(svc_brand_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_brand_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_BRAND_TL st
      using(select
                  (case
                   when l_mi_rec.brand_description_mi = 'N'
                    and svc_brand_tl_col(i).action = CORESVC_BRAND.action_mod
                    and s1.brand_description IS NULL then
                        mt.brand_description
                   else s1.brand_description
                   end) as brand_description,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_brand_tl_col(i).action = CORESVC_BRAND.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.brand_name_mi = 'N'
                    and svc_brand_tl_col(i).action = CORESVC_BRAND.action_mod
                    and s1.brand_name IS NULL then
                        mt.brand_name
                   else s1.brand_name
                   end) as brand_name
              from (select svc_brand_tl_col(i).brand_description as brand_description,
                           svc_brand_tl_col(i).brand_name        as brand_name,
                           svc_brand_tl_col(i).lang              as lang
                      from dual) s1,
                   brand_tl mt
             where mt.brand_name (+) = s1.brand_name
               and mt.lang (+)       = s1.lang) sq
                on (st.brand_name = sq.brand_name and
                    st.lang = sq.lang and
                    svc_brand_tl_col(i).ACTION IN (CORESVC_BRAND.action_mod,CORESVC_BRAND.action_del))
      when matched then
      update
         set process_id        = svc_brand_tl_col(i).process_id ,
             chunk_id          = svc_brand_tl_col(i).chunk_id ,
             row_seq           = svc_brand_tl_col(i).row_seq ,
             action            = svc_brand_tl_col(i).action ,
             process$status    = svc_brand_tl_col(i).process$status ,
             brand_description = sq.brand_description
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             brand_description ,
             brand_name ,
             lang)
      values(svc_brand_tl_col(i).process_id ,
             svc_brand_tl_col(i).chunk_id ,
             svc_brand_tl_col(i).row_seq ,
             svc_brand_tl_col(i).action ,
             svc_brand_tl_col(i).process$status ,
             sq.brand_description ,
             sq.brand_name ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            BRAND_TL_sheet,
                            svc_brand_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_BRAND_TL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_BRAND.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	      EXCEPTION;
   PRAGMA	      EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_BRAND(I_file_id,I_process_id);
      PROCESS_S9T_BRAND_TL(I_file_id,I_process_id);
   end if;
   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
         values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION EXEC_BRAND_INS( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_brand_temp_rec   IN       BRAND%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_BRAND.EXEC_BRAND_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BRAND';
BEGIN
   insert into brand
        values I_brand_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_BRAND_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_BRAND_UPD( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_brand_temp_rec   IN       BRAND%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_BRAND.EXEC_BRAND_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BRAND';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_BRAND_UPD is
      select 'x'
        from BRAND
       where brand_name = I_brand_temp_rec.brand_name
         for update nowait;
BEGIN
   open  C_LOCK_BRAND_UPD;
   close C_LOCK_BRAND_UPD;
   update brand
      set brand_description = I_brand_temp_rec.brand_description
    where brand_name = I_brand_temp_rec.brand_name;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_brand_temp_rec.brand_name,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_BRAND_UPD%ISOPEN then
         close C_LOCK_BRAND_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BRAND_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_BRAND_PRE_DEL( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             L_exists           IN OUT   BOOLEAN,
                             L_process_error    IN OUT   BOOLEAN,
                             I_brand_temp_rec   IN       BRAND%ROWTYPE,
                             I_rec              IN       C_SVC_BRAND%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_BRAND.EXEC_BRAND_PRE_DEL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BRAND';
   L_brand     VARCHAR2(1);

   cursor CHK_ITEM_BRAND is
      select 'x'
        from ITEM_MASTER
       where UPPER(brand_name) = UPPER(I_brand_temp_rec.brand_name);
BEGIN
   open CHK_ITEM_BRAND;
   fetch CHK_ITEM_BRAND into L_brand;
   if CHK_ITEM_BRAND%FOUND then
      L_exists := TRUE;
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'BRAND_NAME',
                  'BRAND_IN_ITEM_MASTER');
      L_process_error :=TRUE;
   end if;
   close CHK_ITEM_BRAND;
   return TRUE;

EXCEPTION
   when OTHERS then
      if CHK_ITEM_BRAND%ISOPEN then
         close CHK_ITEM_BRAND;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_BRAND_PRE_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_BRAND_DEL( O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         L_process_error    IN OUT   BOOLEAN,
                         I_brand_temp_rec   IN       BRAND%ROWTYPE,
                         I_rec              IN       C_SVC_BRAND%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_BRAND.EXEC_BRAND_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BRAND';
   L_exist         BOOLEAN                           := FALSE;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_BRAND_DEL is
      select 'x'
        from BRAND
       where brand_name = I_brand_temp_rec.brand_name
         for update nowait;

   cursor C_LOCK_BRAND_TL_DEL is
      select 'x'
        from BRAND_TL
       where brand_name = I_brand_temp_rec.brand_name
         for update nowait;

BEGIN
   open  C_LOCK_BRAND_TL_DEL;
   close C_LOCK_BRAND_TL_DEL;

   open  C_LOCK_BRAND_DEL;
   close C_LOCK_BRAND_DEL;

      if EXEC_BRAND_PRE_DEL( O_error_message,
                             L_exist,
                             L_process_error,
                             I_brand_temp_rec,
                             I_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      L_process_error :=TRUE;
   else
      if NOT L_exist then
         delete from brand_tl
          where brand_name = I_brand_temp_rec.brand_name;

         delete from brand
          where brand_name = I_brand_temp_rec.brand_name;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_brand_temp_rec.brand_name,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_BRAND_TL_DEL%ISOPEN then
         close C_LOCK_BRAND_TL_DEL;
      end if;

      if C_LOCK_BRAND_DEL%ISOPEN then
         close C_LOCK_BRAND_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BRAND_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_BRAND_TL_INS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_brand_tl_ins_tab    IN       BRAND_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_BRAND.EXEC_BRAND_TL_INS';

BEGIN
   if I_brand_tl_ins_tab is NOT NULL and I_brand_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_brand_tl_ins_tab.COUNT()
         insert into brand_tl
              values I_brand_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_BRAND_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_BRAND_TL_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_brand_tl_upd_tab   IN       BRAND_TL_TAB,
                           I_brand_tl_upd_rst   IN       ROW_SEQ_TAB,
                           I_process_id         IN       SVC_BRAND_TL.PROCESS_ID%TYPE,
                           I_chunk_id           IN       SVC_BRAND_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_BRAND_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'BRAND_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_BRAND_TL_UPD(I_brand  BRAND_TL.BRAND_NAME%TYPE,
                              I_lang   BRAND_TL.LANG%TYPE) is
      select 'x'
        from brand_tl
       where brand_name = I_brand
         and lang = I_lang
         for update nowait;

BEGIN
   if I_brand_tl_upd_tab is NOT NULL and I_brand_tl_upd_tab.count > 0 then
      for i in I_brand_tl_upd_tab.FIRST..I_brand_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_brand_tl_upd_tab(i).lang);
            L_key_val2 := 'Brand Name: '||to_char(I_brand_tl_upd_tab(i).brand_name);
            open C_LOCK_BRAND_TL_UPD(I_brand_tl_upd_tab(i).brand_name,
                                     I_brand_tl_upd_tab(i).lang);
            close C_LOCK_BRAND_TL_UPD;
            
            update brand_tl
               set brand_description = I_brand_tl_upd_tab(i).brand_description,
                   last_update_id = I_brand_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_brand_tl_upd_tab(i).last_update_datetime
             where lang = I_brand_tl_upd_tab(i).lang
               and brand_name = I_brand_tl_upd_tab(i).brand_name;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table,
                              I_brand_tl_upd_rst(i),
                              NULL,
                              O_error_message);
         END;
      end loop;

   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_BRAND_TL_UPD%ISOPEN then
         close C_LOCK_BRAND_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BRAND_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_BRAND_TL_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_brand_tl_del_tab   IN       BRAND_TL_TAB,
                           I_brand_tl_del_rst   IN       ROW_SEQ_TAB,
                           I_process_id         IN       SVC_BRAND_TL.PROCESS_ID%TYPE,
                           I_chunk_id           IN       SVC_BRAND_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_BRAND.EXEC_BRAND_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'BRAND_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_BRAND_TL_DEL(I_brand_name  BRAND_TL.BRAND_NAME%TYPE,
                              I_lang        BRAND_TL.LANG%TYPE) is
      select 'x'
        from brand_tl
       where brand_name = I_brand_name
         and lang = I_lang
         for update nowait;

BEGIN
   if I_brand_tl_del_tab is NOT NULL and I_brand_tl_del_tab.count > 0 then
      for i in I_brand_tl_del_tab.FIRST..I_brand_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_brand_tl_del_tab(i).lang);
            L_key_val2 := 'Brand Name: '||to_char(I_brand_tl_del_tab(i).brand_name);
            open C_LOCK_BRAND_TL_DEL(I_brand_tl_del_tab(i).brand_name,
                                     I_brand_tl_del_tab(i).lang);
            close C_LOCK_BRAND_TL_DEL;

            delete brand_tl
             where lang = I_brand_tl_del_tab(i).lang
               and brand_name = I_brand_tl_del_tab(i).brand_name;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table,
                              I_brand_tl_del_rst(i),
                              NULL,
                              O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_BRAND_TL_DEL%ISOPEN then
         close C_LOCK_BRAND_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BRAND_TL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_BRAND( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_BRAND.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_BRAND.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_BRAND.PROCESS_BRAND';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BRAND';
   L_error           BOOLEAN;
   L_process_error   BOOLEAN := FALSE;
   L_error_message   VARCHAR2(600);
   brand_temp_rec    BRAND%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_BRAND(I_process_id,
                          I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.PK_BRAND_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'BRAND_NAME',
                     'DUP_BRAND');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_BRAND_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'BRAND_NAME',
                     'INV_BRAND_NAME');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new, action_mod) then
         if rec.brand_description IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'BRAND_DESCRIPTION',
                        'ENTER_DESC');
            L_error :=TRUE;
         end if;
      end if;
      if NOT L_error then
         brand_temp_rec.brand_description       := rec.brand_description;
         brand_temp_rec.brand_name              := UPPER(rec.brand_name);
         brand_temp_rec.last_update_id          := GET_USER;
         brand_temp_rec.last_update_datetime    := SYSDATE;
         if rec.action in (action_new,action_del) then
            brand_temp_rec.create_id            := GET_USER;
            brand_temp_rec.create_datetime      := SYSDATE;
         else
            brand_temp_rec.create_id            := rec.create_id;
            brand_temp_rec.create_datetime      := rec.create_datetime;
         end if;
         if rec.action = action_new then
            if EXEC_BRAND_INS( L_error_message,
                               brand_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_BRAND_UPD( L_error_message,
                               brand_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_BRAND_DEL( L_error_message,
                               L_process_error,
                               brand_temp_rec,
                               rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_BRAND;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_BRAND_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id      IN       SVC_BRAND_TL.PROCESS_ID%TYPE,
                          I_chunk_id        IN       SVC_BRAND_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_BRAND.PROCESS_BRAND_TL';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BRAND_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'BRAND_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_brand_tl_temp_rec    BRAND_TL%ROWTYPE;

   cursor C_SVC_BRAND_TL(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
      select pk_brand_tl.rowid  as pk_brand_tl_rid,
             fk_brand.rowid     as fk_brand_rid,
             fk_lang.rowid      as fk_lang_rid,
             st.lang,
             st.brand_name,
             st.brand_description,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_brand_tl  st,
             brand         fk_brand,
             brand_tl      pk_brand_tl,
             lang          fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.brand_name  =  fk_brand.brand_name (+)
         and st.lang        =  pk_brand_tl.lang (+)
         and st.brand_name  =  pk_brand_tl.brand_name (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_BRAND_TL is TABLE OF C_SVC_BRAND_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_brand_tl_tab        SVC_BRAND_TL;
   L_brand_tl_upd_rst        ROW_SEQ_TAB;
   L_brand_tl_del_rst        ROW_SEQ_TAB;

   L_brand_tl_ins_tab        BRAND_TL_tab         := NEW BRAND_TL_tab();
   L_brand_tl_upd_tab        BRAND_TL_tab         := NEW BRAND_TL_tab();
   L_brand_tl_del_tab        BRAND_TL_tab         := NEW BRAND_TL_tab();

BEGIN
   if C_SVC_BRAND_TL%ISOPEN then
      close C_SVC_BRAND_TL;
   end if;

   open C_SVC_BRAND_TL(I_process_id,
                       I_chunk_id);
   LOOP
      fetch C_SVC_BRAND_TL bulk collect into L_svc_brand_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_brand_tl_tab.COUNT > 0 then
         FOR i in L_svc_brand_tl_tab.FIRST..L_svc_brand_tl_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_brand_tl_tab(i).action is NULL
               or L_svc_brand_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_brand_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_brand_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_brand_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_brand_tl_tab(i).action = action_new
               and L_svc_brand_tl_tab(i).pk_brand_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_brand_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_brand_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_brand_tl_tab(i).lang is NOT NULL
               and L_svc_brand_tl_tab(i).brand_name is NOT NULL
               and L_svc_brand_tl_tab(i).pk_brand_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_brand_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_brand_tl_tab(i).action = action_new
               and L_svc_brand_tl_tab(i).brand_name is NOT NULL
               and L_svc_brand_tl_tab(i).fk_brand_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_brand_tl_tab(i).row_seq,
                            'BRAND_NAME',
                            'INV_BRAND_NAME');
               L_error :=TRUE;
            end if;

            if L_svc_brand_tl_tab(i).action = action_new
               and L_svc_brand_tl_tab(i).lang is NOT NULL
               and L_svc_brand_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_brand_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_brand_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_brand_tl_tab(i).brand_description is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_brand_tl_tab(i).row_seq,
                              'BRAND_DESCRIPTION',
                              'ENTER_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_brand_tl_tab(i).brand_name is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_brand_tl_tab(i).row_seq,
                           'BRAND_NAME',
                           'ENTER_BRAND_NAME');
               L_error :=TRUE;
            end if;

            if L_svc_brand_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_brand_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_brand_tl_temp_rec.lang := L_svc_brand_tl_tab(i).lang;
               L_brand_tl_temp_rec.brand_name := L_svc_brand_tl_tab(i).brand_name;
               L_brand_tl_temp_rec.brand_description := L_svc_brand_tl_tab(i).brand_description;
               L_brand_tl_temp_rec.create_datetime := SYSDATE;
               L_brand_tl_temp_rec.create_id := GET_USER;
               L_brand_tl_temp_rec.last_update_datetime := SYSDATE;
               L_brand_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_brand_tl_tab(i).action = action_new then
                  L_brand_tl_ins_tab.extend;
                  L_brand_tl_ins_tab(L_brand_tl_ins_tab.count()) := L_brand_tl_temp_rec;
               end if;

               if L_svc_brand_tl_tab(i).action = action_mod then
                  L_brand_tl_upd_tab.extend;
                  L_brand_tl_upd_tab(L_brand_tl_upd_tab.count()) := L_brand_tl_temp_rec;
                  L_brand_tl_upd_rst(L_brand_tl_upd_tab.count()) := L_svc_brand_tl_tab(i).row_seq;
               end if;

               if L_svc_brand_tl_tab(i).action = action_del then
                  L_brand_tl_del_tab.extend;
                  L_brand_tl_del_tab(L_brand_tl_del_tab.count()) := L_brand_tl_temp_rec;
                  L_brand_tl_del_rst(L_brand_tl_del_tab.count()) := L_svc_brand_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_BRAND_TL%NOTFOUND;
   END LOOP;
   close C_SVC_BRAND_TL;

   if EXEC_BRAND_TL_INS(O_error_message,
                        L_brand_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_BRAND_TL_UPD(O_error_message,
                        L_brand_tl_upd_tab,
                        L_brand_tl_upd_rst,
                        I_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_BRAND_TL_DEL(O_error_message,
                        L_brand_tl_del_tab,
                        L_brand_tl_del_rst,
                        I_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_BRAND_TL;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete from svc_brand_tl
         where process_id = I_process_id;

   delete from svc_brand
         where process_id = I_process_id;
END;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER )
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      := 'CORESVC_BRAND.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
	    from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
	    from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_BRAND(O_error_message,
                    I_process_id,
                    I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_BRAND_TL(O_error_message,
                       I_process_id,
                       I_chunk_id) = FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                    when status = 'PE'
                      then 'PE'
                    else L_process_status
                    END),
          action_date = sysdate
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
    CLEAR_STAGING_DATA(I_process_id);
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------
END CORESVC_BRAND;
/
