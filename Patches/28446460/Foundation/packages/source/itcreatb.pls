CREATE OR REPLACE PACKAGE BODY ITEM_CREATE_SQL AS
--------------------------------------------------------------------
FUNCTION TRAN_CHILDREN(O_error_message            IN OUT   VARCHAR2,
                       I_item_master_insert       IN       VARCHAR2,
                       I_auto_approve_child_ind   IN       VARCHAR2,
                       I_svc_izp_exists           IN     VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN IS
   L_program                VARCHAR2(50)                  := 'ITEM_CREATE_SQL.TRAN_CHILDREN';
   L_item                   ITEM_MASTER.ITEM%TYPE;
   L_item_parent            ITEM_TEMP.EXISTING_ITEM_PARENT%TYPE;
   L_item_desc              ITEM_TEMP.ITEM_DESC%TYPE;
   L_elc_ind                SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_import_ind             SYSTEM_OPTIONS.IMPORT_IND%TYPE;
   L_loc                    ITEM_LOC.LOC%TYPE;
   L_item_level             ITEM_MASTER.ITEM%TYPE;
   L_tran_level             ITEM_MASTER.ITEM%TYPE;
   L_parent_status          ITEM_MASTER.STATUS%TYPE;
   L_vdate                  PERIOD.VDATE%TYPE             := GET_VDATE;
   L_item_approved          BOOLEAN                       := FALSE;
   L_children_approved      BOOLEAN                       := FALSE;
   L_item_submitted         BOOLEAN                       := FALSE;
   L_children_submitted     BOOLEAN                       := FALSE;
   L_user                   VARCHAR2(30)                  := GET_USER;
   L_sysdate                DATE                          := SYSDATE;
   L_table                  VARCHAR2(20)                  := 'ITEM_MASTER';
   RECORD_LOCKED            EXCEPTION;
   PRAGMA                   EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_supp_unit_cost         ITEM_SUPP_COUNTRY.UNIT_COST%TYPE         := NULL;
   L_loc_av_cost            ITEM_LOC_SOH.AV_COST%TYPE                := NULL;
   L_loc_unit_cost          ITEM_LOC_SOH.UNIT_COST%TYPE              := NULL;
   L_prim_supp_curr         SUPS.CURRENCY_CODE%TYPE                  := NULL;
   L_default_po_cost        COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE      := NULL;
   L_parent_sellable_ind    ITEM_MASTER.SELLABLE_IND%TYPE            := NULL;
   L_base_cost              ITEM_COST_HEAD.BASE_COST%TYPE            := NULL;
   L_extended_base_cost     ITEM_COST_HEAD.EXTENDED_BASE_COST%TYPE   := NULL;
   L_negotiated_item_cost   ITEM_COST_HEAD.NEGOTIATED_ITEM_COST%TYPE := NULL;
   L_origin_country_id      ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE := NULL;
   L_supplier               ITEM_SUPP_COUNTRY.SUPPLIER%TYPE :=NULL;

   L_nom_weight             ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;
   L_standard_uom           ITEM_MASTER.STANDARD_UOM%TYPE;
   L_catch_weight_ind       ITEM_MASTER.CATCH_WEIGHT_IND%TYPE;
   L_tax_info_tbl           OBJ_TAX_INFO_TBL                         := OBJ_TAX_INFO_TBL();
   L_tax_info_rec           OBJ_TAX_INFO_REC                         := OBJ_TAX_INFO_REC();
   L_run_report             BOOLEAN;

   ---
   TYPE il_item                 IS TABLE OF ITEM_LOC.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_loc                  IS TABLE OF ITEM_LOC.LOC%TYPE   INDEX BY BINARY_INTEGER;
   TYPE il_item_parent          IS TABLE OF ITEM_LOC.ITEM_PARENT%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_loc_type             IS TABLE OF ITEM_LOC.LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_unit_retail          IS TABLE OF ITEM_LOC.UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_regular_unit_retail  IS TABLE OF ITEM_LOC.REGULAR_UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_taxable_ind          IS TABLE OF ITEM_LOC.TAXABLE_IND%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_ti                   IS TABLE OF ITEM_LOC.TI%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_hi                   IS TABLE OF ITEM_LOC.HI%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_store_ord_mult       IS TABLE OF ITEM_LOC.STORE_ORD_MULT%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_status               IS TABLE OF ITEM_LOC.STATUS%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_daily_waste_pct      IS TABLE OF ITEM_LOC.DAILY_WASTE_PCT%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_meas_of_each         IS TABLE OF ITEM_LOC.MEAS_OF_EACH%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_meas_of_price        IS TABLE OF ITEM_LOC.MEAS_OF_PRICE%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_uom_of_price         IS TABLE OF ITEM_LOC.UOM_OF_PRICE%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_primary_variant      IS TABLE OF ITEM_LOC.PRIMARY_VARIANT%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_primary_supp         IS TABLE OF ITEM_LOC.PRIMARY_SUPP%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_primary_cntry        IS TABLE OF ITEM_LOC.PRIMARY_CNTRY%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_selling_unit_retail  IS TABLE OF ITEM_LOC.SELLING_UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_selling_uom          IS TABLE OF ITEM_LOC.SELLING_UOM%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_multi_units          IS TABLE OF ITEM_LOC.MULTI_UNITS%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_multi_unit_retail    IS TABLE OF ITEM_LOC.MULTI_UNIT_RETAIL%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_multi_selling_uom    IS TABLE OF ITEM_LOC.MULTI_SELLING_UOM%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_source_method        IS TABLE OF ITEM_LOC.SOURCE_METHOD%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_source_wh            IS TABLE OF ITEM_LOC.SOURCE_WH%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_store_price_ind      IS TABLE OF ITEM_LOC.STORE_PRICE_IND%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_uin_type             IS TABLE OF ITEM_lOC.UIN_TYPE%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_uin_label            IS TABLE OF ITEM_lOC.UIN_LABEL%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_capture_time         IS TABLE OF ITEM_lOC.CAPTURE_TIME%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_ext_uin_ind          IS TABLE OF ITEM_lOC.EXT_UIN_IND%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_inbound_handling_days IS TABLE OF ITEM_lOC.INBOUND_HANDLING_DAYS%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_ranged_ind           IS TABLE OF ITEM_lOC.RANGED_IND%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_costing_loc          IS TABLE OF ITEM_lOC.COSTING_LOC%TYPE INDEX BY BINARY_INTEGER;
   TYPE il_costing_loc_type     IS TABLE OF ITEM_lOC.COSTING_LOC_TYPE%TYPE INDEX BY BINARY_INTEGER;
   L_previous_parent            ITEM_TEMP.EXISTING_ITEM_PARENT%TYPE := NULL;

   P_il_item                  il_item;
   P_il_loc                   il_loc;
   P_il_item_parent           il_item_parent;
   P_il_loc_type              il_loc_type;
   P_il_unit_retail           il_unit_retail;
   P_il_regular_unit_retail   il_regular_unit_retail;
   P_il_taxable_ind           il_taxable_ind;
   P_il_ti                    il_ti;
   P_il_hi                    il_hi;
   P_il_store_ord_mult        il_store_ord_mult;
   P_il_status                il_status;
   P_il_daily_waste_pct       il_daily_waste_pct;
   P_il_meas_of_each          il_meas_of_each;
   P_il_meas_of_price         il_meas_of_price;
   P_il_uom_of_price          il_uom_of_price;
   P_il_primary_variant       il_primary_variant;
   P_il_primary_supp          il_primary_supp;
   P_il_primary_cntry         il_primary_cntry;
   P_il_selling_unit_retail   il_selling_unit_retail;
   P_il_selling_uom           il_selling_uom;
   P_il_multi_units           il_multi_units;
   P_il_multi_unit_retail     il_multi_unit_retail;
   P_il_multi_selling_uom     il_multi_selling_uom;
   P_il_source_method         il_source_method;
   P_il_source_wh             il_source_wh;
   P_il_store_price_ind       il_store_price_ind;
   P_il_uin_type              il_uin_type;
   P_il_uin_label             il_uin_label;
   P_il_capture_time          il_capture_time;
   P_il_ext_uin_ind           il_ext_uin_ind;
   P_il_inbound_handling_days il_inbound_handling_days;
   P_il_ranged_ind            il_ranged_ind;
   P_il_costing_loc           il_costing_loc;
   P_il_costing_loc_type      il_costing_loc_type;

   P_il_index                 NUMBER := 0;

   -- Define the array to insert the item_loc_soh data into pl/sql table.
   -- using the pl/sql table to insert into item_loc_soh out of the loop
   TYPE ils_item_TBL        IS TABLE OF   ITEM_LOC_SOH.ITEM%TYPE                INDEX BY BINARY_INTEGER;
   TYPE ils_av_cost_TBL     IS TABLE OF   ITEM_LOC_SOH.AV_COST%TYPE             INDEX BY BINARY_INTEGER;
   TYPE ils_unit_cost_TBL   IS TABLE OF   ITEM_LOC_SOH.UNIT_COST%TYPE           INDEX BY BINARY_INTEGER;
   TYPE ils_currency_TBL    IS TABLE OF   SYSTEM_OPTIONS.CURRENCY_CODE%TYPE     INDEX BY BINARY_INTEGER;
   TYPE ils_loc_TBL         IS TABLE OF   ITEM_LOC.LOC%TYPE                     INDEX BY BINARY_INTEGER;

   ---
   P_ils_item          ils_item_TBL;
   P_ils_av_cost       ils_av_cost_TBL;
   P_ils_unit_cost     ils_unit_cost_TBL;
   P_ils_loc           ils_loc_TBL;
   P_ils_currency_cd   ils_currency_TBL;
   ---
   LP_item_loc_soh_table_index NUMBER := 0;
   ---
   TYPE it_item_TBL        IS TABLE OF   ITEM_TEMP.ITEM%TYPE                     INDEX BY BINARY_INTEGER;
   TYPE it_item_parent_TBL IS TABLE OF   ITEM_TEMP.EXISTING_ITEM_PARENT%TYPE     INDEX BY BINARY_INTEGER;
   TYPE it_item_desc_TBL   IS TABLE OF   ITEM_TEMP.ITEM_DESC%TYPE                INDEX BY BINARY_INTEGER;

   P_it_item          it_item_TBL;
   P_it_item_parent   it_item_parent_TBL;
   P_it_item_desc     it_item_desc_TBL;

   CURSOR C_GET_TEMP_ITEMS is
      select item,
             existing_item_parent,
             item_desc
        from item_temp
       order by existing_item_parent;
   ---
   CURSOR C_GET_SYS_OPTS is
      select elc_ind,
             import_ind,
             default_tax_type
        from system_options;
   ---
   CURSOR C_GET_STATUS_SELLABLE_DEPOSIT is
      select status, sellable_ind, deposit_item_type
        from item_master
       where item = L_item_parent;
   ---
   CURSOR C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where item = L_item
         for update of status nowait;
   ---
   CURSOR C_GET_UNIT_COST is
      select isc.unit_cost,
             isc.base_cost,
             isc.extended_base_cost,
             isc.negotiated_item_cost,
             s.currency_code,
             isc.supplier,
             isc.origin_country_id
        from item_supp_country isc,
             sups s
       where isc.item = L_item
         and isc.primary_supp_ind = 'Y'
         and isc.primary_country_ind = 'Y'
         and isc.supplier = s.supplier;
   ---
   CURSOR C_CURRENCY is
      select /*+ INDEX(il) INDEX(s) */ s.currency_code, il.loc
        from store s,
             item_loc il
       where s.store = il.loc
         and il.loc_type = 'S'
         and il.item = L_item
      union
      select /*+ INDEX(il) INDEX(wh) */  wh.currency_code, il.loc
        from wh,
             item_loc il
       where wh.wh = il.loc
         and il.loc_type = 'W'
         and il.item = L_item
      union
       select /*+ INDEX(il) INDEX(p) */  p.currency_code, il.loc
         from partner p,
              item_loc il
        where p.partner_id = to_char(il.loc)
          and p.partner_type = il.loc_type
          and il.loc_type = 'E'
          and il.item = L_item;

   ---  In the following select, the seemingly superfluous joins to the Store and Warehouse table
   ---  are done in order to enforce Location Security, i.e. to ensure that the current user has
   ---  visibility to the Locations they are attempting to utilize.

   CURSOR C_ITEM_LOC_PARENT is
      select i.item, --parent
             i.loc,
             i.item_parent, --grandparent
             i.loc_type,
             i.unit_retail,
             i.regular_unit_retail,
             i.taxable_ind,
             i.ti,
             i.hi,
             i.store_ord_mult,
             i.status,
             i.daily_waste_pct,
             i.meas_of_each,
             i.meas_of_price,
             i.uom_of_price,
             i.primary_variant,
             i.primary_supp,
             i.primary_cntry,
             i.selling_unit_retail,
             i.selling_uom,
             i.multi_units,
             i.multi_unit_retail,
             i.multi_selling_uom,
             i.source_method,
             i.source_wh,
             i.store_price_ind,
             i.uin_type,
             i.uin_label,
             i.capture_time,
             i.ext_uin_ind,
             i.inbound_handling_days,
             i.ranged_ind,
             i.costing_loc,
             i.costing_loc_type
        from item_loc i
       where i.item = L_item_parent;

   CURSOR C_GET_ITEM_AVERAGE_WEIGHT(in_item ITEM_MASTER.ITEM%TYPE) is
      select average_weight
        from item_loc_soh
        where item = in_item
        and ROWNUM = 1;

   CURSOR C_GET_STD_UOM_CW_IND(in_item_parent ITEM_MASTER.ITEM%TYPE) is
      select standard_uom,
             catch_weight_ind
        from item_master
       where item = in_item_parent;

   L_item_deposit_item_type   ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE;
   L_item_average_weight      ITEM_LOC_SOH.AVERAGE_WEIGHT%TYPE;

   L_child_item_table         PM_RETAIL_API_SQL.CHILD_ITEM_PRICING_TABLE;
   L_default_tax_type         SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

BEGIN

   if I_item_master_insert = 'Y' then
      if ITEM_CREATE_SQL.INSERT_ITEM_MASTER(O_error_message,
                                            I_auto_approve_child_ind ) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_SYS_OPTS','SYSTEM_OPTIONS',NULL);
   open C_GET_SYS_OPTS;
   SQL_LIB.SET_MARK('FETCH','C_GET_SYS_OPTS','SYSTEM_OPTIONS',NULL);
   fetch C_GET_SYS_OPTS into L_elc_ind,
                             L_import_ind,
                             L_default_tax_type;
   SQL_LIB.SET_MARK('CLOSE','C_GET_SYS_OPTS','SYSTEM_OPTIONS',NULL);
   close C_GET_SYS_OPTS;
   --
   SQL_LIB.SET_MARK('OPEN','C_GET_TEMP_ITEMS','ITEM_TEMP',NULL);
   open C_GET_TEMP_ITEMS;
   SQL_LIB.SET_MARK('FETCH','C_GET_TEMP_ITEMS','ITEM_TEMP',NULL);
   fetch C_GET_TEMP_ITEMS BULK COLLECT into P_it_item, P_it_item_parent, P_it_item_desc;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TEMP_ITEMS','ITEM_TEMP',NULL);
   close C_GET_TEMP_ITEMS;

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPPLIER',NULL);
   insert into item_supplier(item,
                             supplier,
                             primary_supp_ind,
                             vpn,
                             supp_label,
                             consignment_rate,
                             supp_diff_1,
                             supp_diff_2,
                             supp_diff_3,
                             supp_diff_4,
                             pallet_name,
                             case_name,
                             inner_name,
                             supp_discontinue_date,
                             direct_ship_ind,
                             last_update_datetime,
                             last_update_id,
                             create_datetime,
                             concession_rate,
                             primary_case_size,
                             create_id)
                      select it.item,
                             supplier,
                             primary_supp_ind,
                             NULL,
                             NULL,
                             consignment_rate,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             pallet_name,
                             case_name,
                             inner_name,
                             supp_discontinue_date,
                             direct_ship_ind,
                             L_sysdate,
                             L_user,
                             L_sysdate,
                             concession_rate,
                             primary_case_size,
                             L_user
                        from item_supplier its,
                             item_temp it
                       where its.item = it.existing_item_parent;
   ---

   FOR i IN 1 .. P_it_item.COUNT LOOP
      L_item        := P_it_item(i);
      L_item_parent := P_it_item_parent(i);
      L_item_desc   := P_it_item_desc(i);
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_STD_UOM_CW_IND','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
      open C_GET_STD_UOM_CW_IND(L_item_parent);
      SQL_LIB.SET_MARK('FETCH','C_GET_STD_UOM_CW_IND','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
      fetch C_GET_STD_UOM_CW_IND into L_standard_uom,L_catch_weight_ind;
      SQL_LIB.SET_MARK('CLOSE','C_GET_STD_UOM_CW_IND','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
      close C_GET_STD_UOM_CW_IND;

      if L_item_parent != L_previous_parent OR L_previous_parent is NULL then
         -- Below two cursors are moved here to avoid excessive looping
         -- These will be fetched once initially and the values are used below for
         -- inserts into item_loc_soh and set the child item pricing info
         SQL_LIB.SET_MARK('OPEN','C_GET_ITEM_AVERAGE_WEIGHT','ITEM_LOC_SOH','Item: '||TO_CHAR(L_item_parent));
         open C_GET_ITEM_AVERAGE_WEIGHT(L_item_parent);
         SQL_LIB.SET_MARK('FETCH','C_GET_ITEM_AVERAGE_WEIGHT','ITEM_LOC_SOH','Item: '||TO_CHAR(L_item_parent));
         fetch C_GET_ITEM_AVERAGE_WEIGHT into L_item_average_weight;
         if C_GET_ITEM_AVERAGE_WEIGHT%NOTFOUND then
            L_item_average_weight := NULL;
         end if;
         SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM_AVERAGE_WEIGHT','ITEM_LOC_SOH','Item: '||TO_CHAR(L_item_parent));
         close C_GET_ITEM_AVERAGE_WEIGHT;
         ---
         if  L_standard_uom = 'EA' and L_catch_weight_ind = 'Y' then
            if ITEM_SUPP_COUNTRY_DIM_SQL.GET_NOMINAL_WEIGHT(O_error_message,
                                                            L_nom_weight,
                                                            L_item_parent) = FALSE then
               return FALSE;
            end if;
            L_item_average_weight := L_nom_weight;
         end if;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_STATUS_SELLABLE_DEPOSIT','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
         open C_GET_STATUS_SELLABLE_DEPOSIT;
         SQL_LIB.SET_MARK('FETCH','C_GET_STATUS_SELLABLE_DEPOSIT','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
         fetch C_GET_STATUS_SELLABLE_DEPOSIT into L_parent_status, L_parent_sellable_ind, L_item_deposit_item_type;
         SQL_LIB.SET_MARK('CLOSE','C_GET_STATUS_SELLABLE_DEPOSIT','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
         close C_GET_STATUS_SELLABLE_DEPOSIT;

         SQL_LIB.SET_MARK('OPEN','C_ITEM_LOC_PARENT','ITEM_LOC','Item: '||TO_CHAR(L_item_parent));
         open c_item_loc_parent;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_LOC_PARENT','ITEM_LOC','Item: '||TO_CHAR(L_item_parent));
         fetch c_item_loc_parent BULK COLLECT into P_il_item,P_il_loc,P_il_item_parent,
            P_il_loc_type,P_il_unit_retail,P_il_regular_unit_retail,P_il_taxable_ind,P_il_ti,
            P_il_hi,P_il_store_ord_mult,P_il_status,P_il_daily_waste_pct,P_il_meas_of_each,
            P_il_meas_of_price,P_il_uom_of_price,P_il_primary_variant,P_il_primary_supp,
            P_il_primary_cntry,P_il_selling_unit_retail,P_il_selling_uom,P_il_multi_units,
            P_il_multi_unit_retail,P_il_multi_selling_uom,P_il_source_method,P_il_source_wh,
            P_il_store_price_ind,P_il_uin_type,P_il_uin_label,P_il_capture_time,P_il_ext_uin_ind,P_il_inbound_handling_days,P_il_ranged_ind,P_il_costing_loc,P_il_costing_loc_type;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_LOC_PARENT','ITEM_LOC','Item: '||TO_CHAR(L_item_parent));
         close c_item_loc_parent;

         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC','Item Parent: '||TO_CHAR(L_item_parent));
         FORALL i IN 1..P_il_item.COUNT
            insert into item_loc(item,
                                 loc,
                                 item_parent,
                                 item_grandparent,
                                 loc_type,
                                 unit_retail,
                                 regular_unit_retail,
                                 clear_ind,
                                 taxable_ind,
                                 local_item_desc,
                                 local_short_desc,
                                 ti,
                                 hi,
                                 store_ord_mult,
                                 status,
                                 status_update_date,
                                 daily_waste_pct,
                                 meas_of_each,
                                 meas_of_price,
                                 uom_of_price,
                                 primary_variant,
                                 primary_cost_pack,
                                 primary_supp,
                                 primary_cntry,
                                 selling_unit_retail,
                                 selling_uom,
                                 multi_units,
                                 multi_unit_retail,
                                 multi_selling_uom,
                                 rpm_ind,
                                 store_price_ind,
                                 last_update_datetime,
                                 last_update_id,
                                 create_datetime,
                                 source_method,
                                 source_wh,
                                 uin_type,
                                 uin_label,
                                 capture_time,
                                 ext_uin_ind,
                                 inbound_handling_days,
                                 ranged_ind,
                                 costing_loc,
                                 costing_loc_type,
                                 create_id)
                          select it.item,
                                 P_il_loc(i),
                                 P_il_item(i), --parent
                                 P_il_item_parent(i), --grandparent
                                 P_il_loc_type(i),
                                 P_il_unit_retail(i),
                                 P_il_regular_unit_retail(i),
                                 'N',
                                 P_il_taxable_ind(i),
                                 it.item_desc,
                                 RTRIM(SUBSTRB(it.item_desc,1,120)),
                                 P_il_ti(i),
                                 P_il_hi(i),
                                 P_il_store_ord_mult(i),
                                 P_il_status(i),
                                 L_vdate,
                                 P_il_daily_waste_pct(i),
                                 P_il_meas_of_each(i),
                                 P_il_meas_of_price(i),
                                 P_il_uom_of_price(i),
                                 P_il_primary_variant(i),
                                 NULL,
                                 P_il_primary_supp(i),
                                 P_il_primary_cntry(i),
                                 P_il_selling_unit_retail(i),
                                 P_il_selling_uom(i),
                                 P_il_multi_units(i),
                                 P_il_multi_unit_retail(i),
                                 P_il_multi_selling_uom(i),
                                 'N',
                                 P_il_store_price_ind(i),
                                 L_sysdate,
                                 L_user,
                                 L_sysdate,
                                 P_il_source_method(i),
                                 P_il_source_wh(i),
                                 P_il_uin_type(i),
                                 P_il_uin_label(i),
                                 P_il_capture_time(i),
                                 P_il_ext_uin_ind(i),
                                 P_il_inbound_handling_days(i),
                                 P_il_ranged_ind(i),
                                 P_il_costing_loc(i),
                                 P_il_costing_loc_type(i),
                                 L_user
                            from item_temp it
                           where existing_item_parent = L_item_parent;
         L_previous_parent := L_item_parent;
      end if;
      ---
      if ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                    L_item_level,
                                    L_tran_level,
                                    L_item) = FALSE THEN
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY','Item: '||TO_CHAR(L_item));
      insert into item_supp_country(item,
                                    supplier,
                                    origin_country_id,
                                    unit_cost,
                                    lead_time,
                                    supp_pack_size,
                                    inner_pack_size,
                                    round_lvl,
                                    round_to_inner_pct,
                                    round_to_case_pct,
                                    round_to_layer_pct,
                                    round_to_pallet_pct,
                                    min_order_qty,
                                    max_order_qty,
                                    packing_method,
                                    primary_supp_ind,
                                    primary_country_ind,
                                    default_uop,
                                    ti,
                                    hi,
                                    supp_hier_type_1,
                                    supp_hier_lvl_1,
                                    supp_hier_type_2,
                                    supp_hier_lvl_2,
                                    supp_hier_type_3,
                                    supp_hier_lvl_3,
                                    pickup_lead_time,
                                    last_update_datetime,
                                    last_update_id,
                                    create_datetime,
                                    cost_uom,
                                    tolerance_type,
                                    max_tolerance,
                                    min_tolerance,
                                    negotiated_item_cost,
                                    extended_base_cost,
                                    inclusive_cost,
                                    base_cost,
                                    create_id)
                             select L_item,
                                    supplier,
                                    origin_country_id,
                                    unit_cost,
                                    lead_time,
                                    supp_pack_size,
                                    inner_pack_size,
                                    round_lvl,
                                    round_to_inner_pct,
                                    round_to_case_pct,
                                    round_to_layer_pct,
                                    round_to_pallet_pct,
                                    min_order_qty,
                                    max_order_qty,
                                    packing_method,
                                    primary_supp_ind,
                                    primary_country_ind,
                                    default_uop,
                                    ti,
                                    hi,
                                    supp_hier_type_1,
                                    supp_hier_lvl_1,
                                    supp_hier_type_2,
                                    supp_hier_lvl_2,
                                    supp_hier_type_3,
                                    supp_hier_lvl_3,
                                    pickup_lead_time,
                                    L_sysdate,
                                    L_user,
                                    L_sysdate,
                                    cost_uom,
                                    tolerance_type,
                                    max_tolerance,
                                    min_tolerance,
                                    negotiated_item_cost,
                                    extended_base_cost,
                                    inclusive_cost,
                                    base_cost,
                                    L_user
                               from item_supp_country
                              where item = L_item_parent;

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_MANU_COUNTRY','Item: '||TO_CHAR(L_item));
      insert into item_supp_manu_country(item,
                                         supplier,
                                         manu_country_id,
                                         primary_manu_ctry_ind,
                                         create_id)
                                  select L_item,
                                         supplier,
                                         manu_country_id,
                                         primary_manu_ctry_ind,
                                         L_user
                                    from item_supp_manu_country
                                   where item = L_item_parent;

      ---

      if L_parent_sellable_ind = 'Y' then
         -- Initialise the L_child_item_table before it is populated.
         L_child_item_table(1).child_item := L_item;
         -- call RPM to set the retail
         if NVL(I_svc_izp_exists,'X') != 'Y' then
            if not PM_RETAIL_API_SQL.SET_CHILD_ITEM_PRICING_INFO(O_error_message,
                                                                 L_item_parent,
                                                                 NULL,
                                                                 l_child_item_table) then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      --- item_loc_soh records are only created for transaction level items

      if L_item_level = L_tran_level then
         --- get the unit cost from item_supp_country for the transaction level item
         SQL_LIB.SET_MARK('OPEN','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY, SUPS','Item: '||TO_CHAR(L_item));
         open C_GET_UNIT_COST;
         SQL_LIB.SET_MARK('FETCH','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY, SUPS','Item: '||TO_CHAR(L_item));
         fetch C_GET_UNIT_COST into L_supp_unit_cost,
                                    L_base_cost,
                                    L_extended_base_cost,
                                    L_negotiated_item_cost,
                                    L_prim_supp_curr,
                                    L_supplier,
                                    L_origin_country_id;
         SQL_LIB.SET_MARK('CLOSE','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY, SUPS','Item: '||TO_CHAR(L_item));
         close C_GET_UNIT_COST;

         --- For every currency that this item exists in at location, convert the unit cost
         SQL_LIB.SET_MARK('OPEN','C_CURRENCY','ITEM_LOC, STORE, WH','Item: '||TO_CHAR(L_item));
         FOR rec IN C_CURRENCY LOOP
            --- Get the default PO cost for the location
            if ITEM_COST_SQL.GET_DEFAULT_PO_COST(O_error_message,
                                                 L_default_po_cost,
                                                 NULL,
                                                 rec.loc) = FALSE then
               return FALSE;
            end if;

            --if default tax type is not GTAX then use unit cost from item_supp_country for unit_cost and av_cost calculation of transaction level items
            if L_default_tax_type <> 'GTAX' then
               L_base_cost            := L_supp_unit_cost;
               L_extended_base_cost   := L_supp_unit_cost;
               L_negotiated_item_cost := L_supp_unit_cost;
            end if;

            -- Get the unit cost in local currency
            if L_default_po_cost = 'NIC' then
               -- Use NIC for unit cost in ITEM_LOC_SOH
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_negotiated_item_cost,
                                       L_prim_supp_curr,
                                       rec.currency_code,
                                       L_loc_unit_cost,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
                  return FALSE;
               end if;
            else
               -- Use BC for unit cost in ITEM_LOC_SOH
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_base_cost,
                                       L_prim_supp_curr,
                                       rec.currency_code,
                                       L_loc_unit_cost,
                                       'C',
                                       NULL,
                                       NULL) = FALSE then
                  return FALSE;
               end if;
            end if;
            -- Get the average cost in local currency
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_extended_base_cost,
                                    L_prim_supp_curr,
                                    rec.currency_code,
                                    L_loc_av_cost,
                                    'C',
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
            ---
            LP_item_loc_soh_table_index                      := LP_item_loc_soh_table_index + 1;

            P_ils_item(LP_item_loc_soh_table_index)          := L_item;
            P_ils_loc(LP_item_loc_soh_table_index)           := rec.loc;
            ---
            P_ils_av_cost(LP_item_loc_soh_table_index)       := L_loc_av_cost;
            P_ils_unit_cost(LP_item_loc_soh_table_index)     := L_loc_unit_cost;
            P_ils_currency_cd(LP_item_loc_soh_table_index)   := rec.currency_code;
            --
         END LOOP;
      end if;
      ---
      if Documents_Sql.GET_DEFAULTS(O_error_message,
                                    'IT',
                                    'IT',
                                    L_item_parent,
                                    L_item,
                                    NULL,
                                    NULL)= FALSE then
         return FALSE;
      end if;
      ---
      if Season_Sql.ASSIGN_DEFAULTS(O_error_message,
                                    L_item) = FALSE then
         return FALSE;
      end if;
      ---
      if Timeline_Sql.INSERT_DOWN_PARENT(O_error_message,
                                         L_item_parent) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   ---
   /* Inserting the data from item_loc_soh pl/sql table to item_loc_soh */
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_SOH',NULL);
   FORALL i IN 1..P_ils_item.COUNT
      insert into item_loc_soh(loc,
                               loc_type,
                               item,
                               item_parent,
                               item_grandparent,
                               av_cost,
                               unit_cost,
                               stock_on_hand,
                               soh_update_datetime,
                               last_hist_export_date,
                               in_transit_qty,
                               pack_comp_intran,
                               pack_comp_soh,
                               tsf_reserved_qty,
                               pack_comp_resv,
                               tsf_expected_qty,
                               pack_comp_exp,
                               rtv_qty,
                               non_sellable_qty,
                               customer_resv,
                               customer_backorder,
                               pack_comp_cust_resv,
                               pack_comp_cust_back,
                               pack_comp_non_sellable,
                               last_update_datetime,
                               last_update_id,
                               create_datetime,
                               primary_supp,
                               primary_cntry,
                               average_weight,
                               create_id)
                        select il.loc,
                               il.loc_type,
                               P_ils_item(i),
                               il.item_parent,
                               il.item_grandparent,
                               P_ils_av_cost(i),
                               P_ils_unit_cost(i),
                               0,
                               NULL,
                               NULL,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               L_sysdate,
                               L_user,
                               L_sysdate,
                               il.primary_supp,
                               il.primary_cntry,
                               L_item_average_weight,
                               L_user
                          from item_loc il
                         where il.item = P_ils_item(i)
                           and il.loc = P_ils_loc(i);
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_LOC',NULL);
   FORALL j IN 1 .. p_it_item.COUNT
      insert into item_supp_country_loc(item,
                                        supplier,
                                        origin_country_id,
                                        loc,
                                        loc_type,
                                        primary_loc_ind,
                                        unit_cost,
                                        round_lvl,
                                        round_to_inner_pct,
                                        round_to_case_pct,
                                        round_to_layer_pct,
                                        round_to_pallet_pct,
                                        supp_hier_type_1,
                                        supp_hier_lvl_1,
                                        supp_hier_type_2,
                                        supp_hier_lvl_2,
                                        supp_hier_type_3,
                                        supp_hier_lvl_3,
                                        pickup_lead_time,
                                        last_update_datetime,
                                        last_update_id,
                                        create_datetime,
                                        negotiated_item_cost,
                                        extended_base_cost,
                                        inclusive_cost,
                                        base_cost,
                                        create_id)
                                 select p_it_item(j),
                                        i.supplier,
                                        i.origin_country_id,
                                        i.loc,
                                        i.loc_type,
                                        i.primary_loc_ind,
                                        i.unit_cost,
                                        i.round_lvl,
                                        i.round_to_inner_pct,
                                        i.round_to_case_pct,
                                        i.round_to_layer_pct,
                                        i.round_to_pallet_pct,
                                        i.supp_hier_type_1,
                                        i.supp_hier_lvl_1,
                                        i.supp_hier_type_2,
                                        i.supp_hier_lvl_2,
                                        i.supp_hier_type_3,
                                        i.supp_hier_lvl_3,
                                        i.pickup_lead_time,
                                        L_sysdate,
                                        L_user,
                                        L_sysdate,
                                        i.negotiated_item_cost,
                                        i.extended_base_cost,
                                        i.inclusive_cost,
                                        i.base_cost,
                                        L_user
                                   from item_supp_country_loc i
                                  where item = p_it_item_parent(j);

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_BRACKET_COST',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_supp_country_bracket_cost(item,
                                                 supplier,
                                                 origin_country_id,
                                                 LOCATION,
                                                 bracket_value1,
                                                 loc_type,
                                                 default_bracket_ind,
                                                 unit_cost,
                                                 bracket_value2,
                                                 sup_dept_seq_no,
                                                 create_id)
                                          select p_it_item(i),
                                                 supplier,
                                                 origin_country_id,
                                                 LOCATION,
                                                 bracket_value1,
                                                 loc_type,
                                                 default_bracket_ind,
                                                 unit_cost,
                                                 bracket_value2,
                                                 sup_dept_seq_no,
                                                 L_user
                                            from item_supp_country_bracket_cost
                                           where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_UOM',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_supp_uom(item,
                                supplier,
                                uom,
                                VALUE,
                                last_update_datetime,
                                last_update_id,
                                create_datetime,
                                create_id)
                         select p_it_item(i),
                                supplier,
                                uom,
                                VALUE,
                                L_sysdate,
                                L_user,
                                L_sysdate,
                                L_user
                           from item_supp_uom
                          where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPP_COUNTRY_DIM',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_supp_country_dim(item,
                                        supplier,
                                        origin_country,
                                        dim_object,
                                        presentation_method,
                                        LENGTH,
                                        width,
                                        height,
                                        lwh_uom,
                                        weight,
                                        net_weight,
                                        weight_uom,
                                        liquid_volume,
                                        liquid_volume_uom,
                                        stat_cube,
                                        tare_weight,
                                        tare_type,
                                        last_update_datetime,
                                        last_update_id,
                                        create_datetime,
                                        create_id)
                                 select p_it_item(i),
                                        supplier,
                                        origin_country,
                                        dim_object,
                                        presentation_method,
                                        LENGTH,
                                        width,
                                        height,
                                        lwh_uom,
                                        weight,
                                        net_weight,
                                        weight_uom,
                                        liquid_volume,
                                        liquid_volume_uom,
                                        stat_cube,
                                        tare_weight,
                                        tare_type,
                                        L_sysdate,
                                        L_user,
                                        L_sysdate,
                                        L_user
                                   from item_supp_country_dim
                                  where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_country(item,
                               country_id)
                        select p_it_item(i),
                               country_id
                          from item_country
                         where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY_L10N_EXT',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_country_l10n_ext(item,
                                        country_id,
                                        l10n_country_id,
                                        group_id,
                                        varchar2_1,
                                        varchar2_2,
                                        varchar2_3,
                                        varchar2_4,
                                        varchar2_5,
                                        varchar2_6,
                                        varchar2_7,
                                        varchar2_8,
                                        varchar2_9,
                                        varchar2_10,
                                        number_11,
                                        number_12,
                                        number_13,
                                        number_14,
                                        number_15,
                                        number_16,
                                        number_17,
                                        number_18,
                                        number_19,
                                        number_20,
                                        date_21,
                                        date_22)
                                 select p_it_item(i),
                                        country_id,
                                        l10n_country_id,
                                        group_id,
                                        varchar2_1,
                                        varchar2_2,
                                        varchar2_3,
                                        varchar2_4,
                                        varchar2_5,
                                        varchar2_6,
                                        varchar2_7,
                                        varchar2_8,
                                        varchar2_9,
                                        varchar2_10,
                                        number_11,
                                        number_12,
                                        number_13,
                                        number_14,
                                        number_15,
                                        number_16,
                                        number_17,
                                        number_18,
                                        number_19,
                                        number_20,
                                        date_21,
                                        date_22
                                 from item_country_l10n_ext
                                where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COST_HEAD',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_cost_head(item,
                                 supplier,
                                 origin_country_id,
                                 delivery_country_id,
                                 prim_dlvy_ctry_ind,
                                 nic_static_ind,
                                 base_cost,
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 inclusive_cost)
                          select p_it_item(i),
                                 supplier,
                                 origin_country_id,
                                 delivery_country_id,
                                 prim_dlvy_ctry_ind,
                                 nic_static_ind,
                                 base_cost,
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 inclusive_cost
                            from item_cost_head
                           where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COST_DETAIL',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_cost_detail(item,
                                 supplier,
                                 origin_country_id,
                                 delivery_country_id,
                                 cond_type,
                                 cond_value,
                                 applied_on,
                                 comp_rate,
                                 calculation_basis,
                                 recoverable_amount,
                                 modified_taxable_base)
                          select p_it_item(i),
                                 supplier,
                                 origin_country_id,
                                 delivery_country_id,
                                 cond_type,
                                 cond_value,
                                 applied_on,
                                 comp_rate,
                                 calculation_basis,
                                 recoverable_amount,
                                 modified_taxable_base
                            from item_cost_detail
                           where item = p_it_item_parent(i);

   if L_default_tax_type in ('SVAT','GTAX') then
      FOR i IN 1 .. p_it_item.COUNT LOOP
         L_tax_info_rec.item        := p_it_item(i);
         L_tax_info_rec.item_parent := p_it_item_parent(i);
         ---
         L_tax_info_rec.tran_type       := 'CREATEITEM';
         L_tax_info_rec.tran_date       := L_vdate;
         ---
         L_tax_info_tbl.EXTEND;
         L_tax_info_tbl(L_tax_info_tbl.COUNT) := L_tax_info_rec;
         L_tax_info_rec                       := OBJ_TAX_INFO_REC();
         ---
      end LOOP;
      ---
      if L_tax_info_tbl.COUNT > 0 then
         if TAX_SQL.GET_TAX_INFO(O_error_message,
                                 L_tax_info_tbl) = FALSE then
            return FALSE;
         end if;
         ---
         if TAX_SQL.INSERT_UPDATE_TAX_SKU(O_error_message,
                                          L_run_report,
                                          L_tax_info_tbl) = FALSE then
            return FALSE;
         end if;
         L_tax_info_tbl.DELETE;
      end if;
   end if;

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_TICKET',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_ticket(item,
                              ticket_type_id,
                              po_print_type,
                              print_on_pc_ind,
                              ticket_over_pct,
                              last_update_datetime,
                              last_update_id,
                              create_datetime,
                              create_id)
                       select p_it_item(i),
                              ticket_type_id,
                              po_print_type,
                              print_on_pc_ind,
                              ticket_over_pct,
                              L_sysdate,
                              L_user,
                              L_sysdate,
                              L_user
                         from item_ticket
                        where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_LOV',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into uda_item_lov(item,
                               uda_id,
                               uda_value,
                               last_update_datetime,
                               last_update_id,
                               create_datetime,
                               create_id)
                        select p_it_item(i),
                               uda_id,
                               uda_value,
                               L_sysdate,
                               L_user,
                               L_sysdate,
                               L_user
                          from uda_item_lov
                         where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_FF',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into uda_item_ff(item,
                              uda_id,
                              uda_text,
                              last_update_datetime,
                              last_update_id,
                              create_datetime,
                              create_id)
                       select p_it_item(i),
                              uda_id,
                              uda_text,
                              L_sysdate,
                              L_user,
                              L_sysdate,
                              L_user
                         from uda_item_ff
                        where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'UDA_ITEM_DATE',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into uda_item_date(item,
                                uda_id,
                                uda_date,
                                last_update_datetime,
                                last_update_id,
                                create_datetime,
                                create_id)
                         select p_it_item(i),
                                uda_id,
                                uda_date,
                                L_sysdate,
                                L_user,
                                L_sysdate,
                                L_user
                           from uda_item_date
                          where item = p_it_item_parent(i);

   if L_elc_ind = 'Y' then
      ---
      -- ELC may be set to Yes even if the client is not running RTM.
      -- Need to check the import ind to see if client has RTM and
      -- is therefore using the HTS/Assessments dialogs.
      ---
      if L_import_ind = 'Y' then
         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_HTS',NULL);
         FORALL i IN 1 .. p_it_item.COUNT
            insert into item_hts(item,
                                 hts,
                                 import_country_id,
                                 origin_country_id,
                                 effect_from,
                                 effect_to,
                                 clearing_zone_id,
                                 status,
                                 last_update_datetime,
                                 last_update_id,
                                 create_datetime,
                                 create_id)
                          select p_it_item(i),
                                 hts,
                                 import_country_id,
                                 origin_country_id,
                                 effect_from,
                                 effect_to,
                                 clearing_zone_id,
                                 status,
                                 L_sysdate,
                                 L_user,
                                 L_sysdate,
                                 L_user
                            from item_hts
                           where item = p_it_item_parent(i);

         SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_HTS_ASSESS',NULL);
         FORALL i IN 1 .. p_it_item.COUNT
            insert into item_hts_assess(item,
                                        hts,
                                        import_country_id,
                                        origin_country_id,
                                        effect_from,
                                        effect_to,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order,
                                        last_update_datetime,
                                        last_update_id,
                                        create_datetime,
                                        create_id)
                                 select p_it_item(i),
                                        hts,
                                        import_country_id,
                                        origin_country_id,
                                        effect_from,
                                        effect_to,
                                        comp_id,
                                        cvb_code,
                                        comp_rate,
                                        per_count,
                                        per_count_uom,
                                        est_assess_value,
                                        nom_flag_1,
                                        nom_flag_2,
                                        nom_flag_3,
                                        nom_flag_4,
                                        nom_flag_5,
                                        display_order,
                                        L_sysdate,
                                        L_user,
                                        L_sysdate,
                                        L_user
                                   from item_hts_assess
                                  where item = p_it_item_parent(i);

         SQL_LIB.SET_MARK('INSERT',NULL,'COND_TARIFF_TREATMENT',NULL);
         FORALL i IN 1 .. p_it_item.COUNT
            insert into cond_tariff_treatment(item,
                                              tariff_treatment,
                                              last_update_datetime,
                                              last_update_id,
                                              create_datetime,
                                              create_id)
                                       select p_it_item(i),
                                              tariff_treatment,
                                              L_sysdate,
                                              L_user,
                                              L_sysdate,
                                              L_user
                                         from cond_tariff_treatment
                                        where item = p_it_item_parent(i);
      end if;

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_EXP_HEAD',NULL);
      FORALL i IN 1 .. p_it_item.COUNT
         insert into item_exp_head(item,
                                   supplier,
                                   item_exp_type,
                                   item_exp_seq,
                                   origin_country_id,
                                   zone_id,
                                   lading_port,
                                   discharge_port,
                                   zone_group_id,
                                   base_exp_ind,
                                   last_update_datetime,
                                   last_update_id,
                                   create_datetime,
                                   create_id)
                            select p_it_item(i),
                                   supplier,
                                   item_exp_type,
                                   item_exp_seq,
                                   origin_country_id,
                                   zone_id,
                                   lading_port,
                                   discharge_port,
                                   zone_group_id,
                                   base_exp_ind,
                                   L_sysdate,
                                   L_user,
                                   L_sysdate,
                                   L_user
                              from item_exp_head
                             where item = p_it_item_parent(i);

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_EXP_DETAIL',NULL);
      FORALL i IN 1 .. p_it_item.COUNT
         insert into item_exp_detail(item,
                                     supplier,
                                     item_exp_type,
                                     item_exp_seq,
                                     comp_id,
                                     cvb_code,
                                     comp_rate,
                                     comp_currency,
                                     per_count,
                                     per_count_uom,
                                     est_exp_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     display_order,
                                     last_update_datetime,
                                     last_update_id,
                                     create_datetime,
                                     defaulted_from,
                                     key_value_1,
                                     key_value_2,
                                     create_id)
                              select p_it_item(i),
                                     supplier,
                                     item_exp_type,
                                     item_exp_seq,
                                     comp_id,
                                     cvb_code,
                                     comp_rate,
                                     comp_currency,
                                     per_count,
                                     per_count_uom,
                                     est_exp_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     display_order,
                                     L_sysdate,
                                     L_user,
                                     L_sysdate,
                                     defaulted_from,
                                     key_value_1,
                                     key_value_2,
                                     L_user
                                from item_exp_detail
                               where item = p_it_item_parent(i);

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_CHRG_HEAD',NULL);
      FORALL i IN 1 .. p_it_item.COUNT
         insert into item_chrg_head(item,
                                    from_loc,
                                    to_loc,
                                    from_loc_type,
                                    to_loc_type,
                                    create_id)
                             select p_it_item(i),
                                    from_loc,
                                    to_loc,
                                    from_loc_type,
                                    to_loc_type,
                                    L_user
                               from item_chrg_head
                              where item = p_it_item_parent(i);

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_CHRG_DETAIL',NULL);
      FORALL i IN 1 .. p_it_item.COUNT
         insert into item_chrg_detail(item,
                                      from_loc,
                                      to_loc,
                                      comp_id,
                                      from_loc_type,
                                      to_loc_type,
                                      comp_rate,
                                      per_count,
                                      per_count_uom,
                                      up_chrg_group,
                                      comp_currency,
                                      display_order,
                                      create_id)
                               select /*+ INDEX (item_chrg_detail, pk_item_chrg_detail) */ p_it_item(i),
                                      from_loc,
                                      to_loc,
                                      comp_id,
                                      from_loc_type,
                                      to_loc_type,
                                      comp_rate,
                                      per_count,
                                      per_count_uom,
                                      up_chrg_group,
                                      comp_currency,
                                      display_order,
                                      L_user
                                 from item_chrg_detail
                                where item = p_it_item_parent(i);
   end if;

   if L_import_ind = 'Y' then
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_IMPORT_ATTR',NULL);
      FORALL i IN 1 .. p_it_item.COUNT
         insert into item_import_attr(item,
                                      tooling,
                                      first_order_ind,
                                      amortize_base,
                                      open_balance,
                                      commodity,
                                      import_desc,
                                      create_id)
                               select p_it_item(i),
                                      tooling,
                                      first_order_ind,
                                      amortize_base,
                                      open_balance,
                                      commodity,
                                      import_desc,
                                      L_user
                                 from item_import_attr
                                where item = p_it_item_parent(i);
   end if;

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_IMAGE',NULL);
   FORALL i IN 1 .. p_it_item.COUNT
      insert into item_image(item,
                             image_name,
                             image_addr,
                             image_desc,
                             create_datetime,
                             last_update_datetime,
                             last_update_id,
                             create_id,
                             image_type,
                             primary_ind,
                             display_priority)
                      select p_it_item(i),
                             image_name,
                             image_addr,
                             image_desc,
                             L_sysdate,
                             L_sysdate,
                             L_user,
                             L_user,
                             image_type,
                             primary_ind,
                             display_priority
                        from item_image
                       where item = p_it_item_parent(i);

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_LOC_TRAITS',NULL);
   FORALL j IN 1 .. p_it_item.COUNT
      insert into item_loc_traits(item,
                                  loc,
                                  launch_date,
                                  qty_key_options,
                                  manual_price_entry,
                                  deposit_code,
                                  food_stamp_ind,
                                  wic_ind,
                                  proportional_tare_pct,
                                  fixed_tare_value,
                                  fixed_tare_uom,
                                  reward_eligible_ind,
                                  natl_brand_comp_item,
                                  return_policy,
                                  stop_sale_ind,
                                  elect_mtk_clubs,
                                  report_code,
                                  req_shelf_life_on_selection,
                                  req_shelf_life_on_receipt,
                                  ib_shelf_life,
                                  store_reorderable_ind,
                                  rack_size,
                                  full_pallet_item,
                                  in_store_market_basket,
                                  storage_location,
                                  alt_storage_location,
                                  returnable_ind,
                                  refundable_ind,
                                  back_order_ind,
                                  create_datetime,
                                  last_update_id,
                                  last_update_datetime,
                                  create_id)
                           select p_it_item(j),
                                  i.loc,
                                  i.launch_date,
                                  i.qty_key_options,
                                  i.manual_price_entry,
                                  i.deposit_code,
                                  i.food_stamp_ind,
                                  i.wic_ind,
                                  i.proportional_tare_pct,
                                  i.fixed_tare_value,
                                  i.fixed_tare_uom,
                                  i.reward_eligible_ind,
                                  i.natl_brand_comp_item,
                                  i.return_policy,
                                  i.stop_sale_ind,
                                  i.elect_mtk_clubs,
                                  i.report_code,
                                  i.req_shelf_life_on_selection,
                                  i.req_shelf_life_on_receipt,
                                  i.ib_shelf_life,
                                  i.store_reorderable_ind,
                                  i.rack_size,
                                  i.full_pallet_item,
                                  i.in_store_market_basket,
                                  i.storage_location,
                                  i.alt_storage_location,
                                  i.returnable_ind,
                                  i.refundable_ind,
                                  i.back_order_ind,
                                  L_sysdate,
                                  L_user,
                                  L_sysdate,
                                  L_user
                             from item_loc_traits i
                             where i.item = p_it_item_parent(j);
   ---
   -- If the auto approve indicator is Yes and the parent is already approved
   -- then call approval to validate the item for approval and write the
   -- PRICE_HIST and RPM records.

   L_previous_parent := NULL;

   FOR i IN 1 .. p_it_item.COUNT LOOP
      L_item := P_it_item(i);
      L_item_parent := P_it_item_parent(i);

      if L_item_parent != L_previous_parent or L_previous_parent is NULL then
         SQL_LIB.SET_MARK('OPEN','C_GET_STATUS_SELLABLE_DEPOSIT','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
         open C_GET_STATUS_SELLABLE_DEPOSIT;
         SQL_LIB.SET_MARK('FETCH','C_GET_STATUS_SELLABLE_DEPOSIT','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
         fetch C_GET_STATUS_SELLABLE_DEPOSIT into L_parent_status, L_parent_sellable_ind, L_item_deposit_item_type;
         SQL_LIB.SET_MARK('OPEN','C_GET_STATUS_SELLABLE_DEPOSIT','ITEM_MASTER','Item: '||TO_CHAR(L_item_parent));
         close C_GET_STATUS_SELLABLE_DEPOSIT;

         L_previous_parent := L_item_parent;
      end if;

      if I_auto_approve_child_ind = 'Y' and L_parent_status = 'A'
         and L_item_deposit_item_type is NULL then

         --- Need to submit the item before it can be approved.
         SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_MASTER','ITEM_MASTER','Item: '||TO_CHAR(L_item));
         open C_LOCK_ITEM_MASTER;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_MASTER','ITEM_MASTER','Item: '||TO_CHAR(L_item));
         close C_LOCK_ITEM_MASTER;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_MASTER','Item: '||TO_CHAR(L_item));
         update item_master
            set status = 'S'
           where item  = L_item;
         ---
         if Item_Approval_Sql.APPROVE(O_error_message,
                                      L_item_approved,
                                      L_children_approved,
                                      'N',
                                      L_item) = FALSE then
            return FALSE;
         end if;
         ---
         -- Item passed approval set status to 'A'
         if L_item_approved = TRUE then
            SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_MASTER','ITEM_MASTER','Item: '||TO_CHAR(L_item));
            open C_LOCK_ITEM_MASTER;
            SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_MASTER','ITEM_MASTER','Item: '||TO_CHAR(L_item));
            close C_LOCK_ITEM_MASTER;
            ---
            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_MASTER','Item: '||TO_CHAR(L_item));
            update item_master
               set status = 'A'
             where item   = L_item;
         else
            -- Item failed approval set status back to 'W'
            SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_MASTER','ITEM_MASTER','Item: '||TO_CHAR(L_item));
            open C_LOCK_ITEM_MASTER;
            SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_MASTER','ITEM_MASTER','Item: '||TO_CHAR(L_item));
            close C_LOCK_ITEM_MASTER;
            ---
            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_MASTER','Item: '||TO_CHAR(L_item));
            update item_master
               set status = 'W'
             where item   = L_item;
         end if;
      end if;
   END LOOP;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      if C_GET_TEMP_ITEMS%ISOPEN then
         close C_GET_TEMP_ITEMS;
      end if;

      if C_GET_SYS_OPTS%ISOPEN then
         close C_GET_SYS_OPTS;
      end if;

      if C_GET_STATUS_SELLABLE_DEPOSIT%ISOPEN then
         close C_GET_STATUS_SELLABLE_DEPOSIT;
      end if;

      if C_LOCK_ITEM_MASTER%ISOPEN then
         close C_LOCK_ITEM_MASTER;
      end if;

      if C_GET_UNIT_COST%ISOPEN then
         close C_GET_UNIT_COST;
      end if;

      if C_CURRENCY%ISOPEN then
         close C_CURRENCY;
      end if;

      if C_ITEM_LOC_PARENT%ISOPEN then
         close C_ITEM_LOC_PARENT;
      end if;

      if C_GET_ITEM_AVERAGE_WEIGHT%ISOPEN then
         close C_GET_ITEM_AVERAGE_WEIGHT;
      end if;

      if C_GET_STD_UOM_CW_IND%ISOPEN then
         close C_GET_STD_UOM_CW_IND;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            TO_CHAR(SQLCODE));
      return FALSE;
   WHEN OTHERS THEN

      if C_GET_TEMP_ITEMS%ISOPEN then
         close C_GET_TEMP_ITEMS;
      end if;

      if C_GET_SYS_OPTS%ISOPEN then
         close C_GET_SYS_OPTS;
      end if;

      if C_GET_STATUS_SELLABLE_DEPOSIT%ISOPEN then
         close C_GET_STATUS_SELLABLE_DEPOSIT;
      end if;

      if C_LOCK_ITEM_MASTER%ISOPEN then
         close C_LOCK_ITEM_MASTER;
      end if;

      if C_GET_UNIT_COST%ISOPEN then
         close C_GET_UNIT_COST;
      end if;

      if C_CURRENCY%ISOPEN then
         close C_CURRENCY;
      end if;

      if C_ITEM_LOC_PARENT%ISOPEN then
         close C_ITEM_LOC_PARENT;
      end if;

      if C_GET_ITEM_AVERAGE_WEIGHT%ISOPEN then
         close C_GET_ITEM_AVERAGE_WEIGHT;
      end if;

      if C_GET_STD_UOM_CW_IND%ISOPEN then
         close C_GET_STD_UOM_CW_IND;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
END TRAN_CHILDREN;

--------------------------------------------------------------------
FUNCTION INSERT_ITEM_MASTER(O_error_message            IN OUT   VARCHAR2,
                            I_auto_approve_child_ind   IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program              VARCHAR2(40)                          := 'ITEM_CREATE_SQL.INSERT_ITEM_MASTER';
   L_item                 ITEM_MASTER.ITEM%TYPE                 := NULL;
   L_item_level           ITEM_MASTER.ITEM_LEVEL%TYPE           := NULL;
   L_item_parent          ITEM_MASTER.ITEM_PARENT%TYPE          := NULL;
   L_parent_tran_level    ITEM_MASTER.TRAN_LEVEL%TYPE           := NULL;
   L_user                 VARCHAR2(30)                          := GET_USER;
   L_primary_ref_item_ind ITEM_MASTER.PRIMARY_REF_ITEM_IND%TYPE := NULL;
   L_sysdate              DATE                                  := SYSDATE;
   L_ref_item             item_master.item%TYPE                 := NULL;
   L_table                VARCHAR2(20)                          := NULL;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   CURSOR C_GET_TEMP_ITEMS IS
      SELECT it.item,
             it.item_number_type,
             it.format_id,
             it.prefix,
             it.item_level,
             it.item_desc,
             it.diff_1,
             it.diff_2,
             it.diff_3,
             it.diff_4,
             it.existing_item_parent,
             im.tran_level
        FROM item_temp it,
             item_master im
       WHERE im.item = it.existing_item_parent;
   ---
   CURSOR C_PRIM_REF_ITEM_EXISTS IS
      SELECT 'N'
        FROM item_master im
       WHERE im.primary_ref_item_ind  = 'Y'
         AND im.item_level            > tran_level
         AND im.item_parent           = L_item_parent
         AND ROWNUM                   = 1;
   ---
   CURSOR C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where item = L_ref_item
         for update of primary_ref_item_ind nowait;
   ---
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_TEMP_ITEMS','ITEM_TEMP, ITEM_MASTER',NULL);
   FOR rec IN C_GET_TEMP_ITEMS LOOP
      L_item              := rec.item;
      L_item_level        := rec.item_level;
      L_item_parent       := rec.existing_item_parent;
      L_parent_tran_level := rec.tran_level;
      ---
      IF L_item_level > L_parent_tran_level  THEN
         IF L_ref_item IS NULL THEN
            SQL_LIB.SET_MARK('OPEN','C_PRIM_REF_ITEM_EXISTS','ITEM_MASTER','ITEM: '||L_item_parent);
            OPEN C_PRIM_REF_ITEM_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_PRIM_REF_ITEM_EXISTS','ITEM_MASTER','ITEM: '||L_item_parent);
            FETCH C_PRIM_REF_ITEM_EXISTS INTO L_primary_ref_item_ind;
            IF C_PRIM_REF_ITEM_EXISTS%NOTFOUND THEN
               L_ref_item := L_item;
               SQL_LIB.SET_MARK('CLOSE','C_PRIM_REF_ITEM_EXISTS','ITEM_MASTER','ITEM: '||L_item_parent);
               CLOSE C_PRIM_REF_ITEM_EXISTS;
               EXIT;
            END IF;
            SQL_LIB.SET_MARK('CLOSE','C_PRIM_REF_ITEM_EXISTS','ITEM_MASTER','ITEM: '||L_item_parent);
            CLOSE C_PRIM_REF_ITEM_EXISTS;
         END IF;
      END IF;
      ---
   END LOOP;
   --
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_MASTER',NULL);
   INSERT INTO item_master(item,
                           item_number_type,
                           format_id,
                           prefix,
                           item_parent,
                           item_grandparent,
                           pack_ind,
                           item_level,
                           tran_level,
                           item_aggregate_ind,
                           diff_1,
                           diff_1_aggregate_ind,
                           diff_2,
                           diff_2_aggregate_ind,
                           diff_3,
                           diff_3_aggregate_ind,
                           diff_4,
                           diff_4_aggregate_ind,
                           dept,
                           CLASS,
                           subclass,
                           status,
                           item_desc,
                           item_desc_secondary,
                           short_desc,
                           desc_up,
                           primary_ref_item_ind,
                           cost_zone_group_id,
                           standard_uom,
                           uom_conv_factor,
                           package_size,
                           package_uom,
                           merchandise_ind,
                           store_ord_mult,
                           forecast_ind,
                           original_retail,
                           mfg_rec_retail,
                           retail_label_type,
                           retail_label_value,
                           handling_temp,
                           handling_sensitivity,
                           catch_weight_ind,
                           waste_type,
                           waste_pct,
                           default_waste_pct,
                           const_dimen_ind,
                           simple_pack_ind,
                           contains_inner_ind,
                           sellable_ind,
                           orderable_ind,
                           pack_type,
                           order_as_type,
                           comments,
                           item_service_level,
                           gift_wrap_ind,
                           ship_alone_ind,
                           check_uda_ind,
                           create_datetime,
                           last_update_id,
                           last_update_datetime,
                           item_xform_ind,
                           inventory_ind,
                           order_type,
                           sale_type,
                           deposit_item_type,
                           container_item,
                           deposit_in_price_per_uom,
                           aip_case_type,
                           perishable_ind,
                           notional_pack_ind,
                           soh_inquiry_at_pack_ind,
                           brand_name,
                           product_classification,
                           curr_selling_unit_retail,
                           curr_selling_uom,
                           create_id)
                    SELECT it.item,
                           it.item_number_type,
                           it.format_id,
                           it.prefix,
                           it.existing_item_parent,
                           im.item_parent,
                           im.pack_ind,
                           it.item_level,
                           im.tran_level,
                           'N',
                           it.diff_1,
                           'N',
                           it.diff_2,
                           'N',
                           it.diff_3,
                           'N',
                           it.diff_4,
                           'N',
                           im.dept,
                           im.class,
                           im.subclass,
                           'W',
                           it.item_desc,
                           im.item_desc_secondary,
                           RTRIM(SUBSTRB(it.item_desc,1,120)),
                           UPPER(it.item_desc),
                           'N',
                           im.cost_zone_group_id,
                           im.standard_uom,
                           im.uom_conv_factor,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.package_size
                            end package_size,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.package_uom
                            end package_uom,
                           im.merchandise_ind,
                           im.store_ord_mult,
                           im.forecast_ind,
                           im.original_retail,
                           im.mfg_rec_retail,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.retail_label_type
                            end retail_label_type,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.retail_label_value
                            end retail_label_value,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.handling_temp
                            end handling_temp,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.handling_sensitivity
                           end handling_sensitivity,
                           im.catch_weight_ind,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.waste_type
                            end waste_type,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.waste_pct
                            end waste_pct,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.default_waste_pct
                            end default_waste_pct,
                           im.const_dimen_ind,
                           im.simple_pack_ind,
                           im.contains_inner_ind,
                           im.sellable_ind,
                           im.orderable_ind,
                           im.pack_type,
                           im.order_as_type,
                           im.comments,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.item_service_level
                            end item_service_level,
                           im.gift_wrap_ind,
                           im.ship_alone_ind,
                           im.check_uda_ind,
                           L_sysdate,
                           L_user,
                           L_sysdate,
                           im.item_xform_ind,
                           im.inventory_ind,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.order_type
                            end order_type,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.sale_type
                            end sale_type,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.deposit_item_type
                            end deposit_item_type,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.container_item
                            end container_item,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.deposit_in_price_per_uom
                            end deposit_in_price_per_uom,
                           im.aip_case_type,
                           im.perishable_ind,
                           im.notional_pack_ind,
                           im.soh_inquiry_at_pack_ind,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.brand_name
                            end brand_name,
                           case
                              when it.item_level > im.tran_level then NULL
                              else im.product_classification
                            end product_classification,
                           im.curr_selling_unit_retail,
                           im.curr_selling_uom,
                           L_user
                      FROM item_master im,
                           item_temp it
                     WHERE im.item = it.existing_item_parent;
         ---
   IF L_ref_item IS NOT NULL THEN
      L_table := 'ITEM_MASTER';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_MASTER','ITEM_MASTER','ITEM: '||TO_CHAR(L_ref_item));
      open C_LOCK_ITEM_MASTER;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_MASTER','ITEM_MASTER','ITEM: '||TO_CHAR(L_ref_item));
      close C_LOCK_ITEM_MASTER;

      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_MASTER','ITEM: '||TO_CHAR(L_ref_item));
      update item_master
         set primary_ref_item_ind = 'Y'
       where item = L_ref_item;
   END IF;

         ---
   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_program,
                                             TO_CHAR(L_ref_item));
   if C_GET_TEMP_ITEMS%ISOPEN then
      close C_GET_TEMP_ITEMS;
   end if;

   if C_PRIM_REF_ITEM_EXISTS%ISOPEN then
      close C_PRIM_REF_ITEM_EXISTS;
   end if;

   if C_LOCK_ITEM_MASTER%ISOPEN then
      close C_LOCK_ITEM_MASTER;
   end if;

   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
   if C_GET_TEMP_ITEMS%ISOPEN then
      close C_GET_TEMP_ITEMS;
   end if;

   if C_PRIM_REF_ITEM_EXISTS%ISOPEN then
      close C_PRIM_REF_ITEM_EXISTS;
   end if;

   if C_LOCK_ITEM_MASTER%ISOPEN then
      close C_LOCK_ITEM_MASTER;
   end if;

   RETURN FALSE;
END INSERT_ITEM_MASTER;
-------------------------------------------------------------------
FUNCTION INSERT_ITEM_SUPPLIER(O_error_message   IN OUT   VARCHAR2,
                              I_item            IN       ITEM_MASTER.ITEM%TYPE,
                              I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(40) := 'ITEM_CREATE_SQL.INSERT_ITEM_SUPPLIER';
   L_user    VARCHAR2(30) := GET_USER;

BEGIN
   IF I_item IS NOT NULL AND I_item_parent IS NOT NULL THEN
      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPPLIER','ITEM: '||I_item);
      INSERT INTO item_supplier(item,
                                supplier,
                                primary_supp_ind,
                                vpn,
                                supp_label,
                                consignment_rate,
                                supp_diff_1,
                                supp_diff_2,
                                supp_diff_3,
                                supp_diff_4,
                                pallet_name,
                                case_name,
                                inner_name,
                                supp_discontinue_date,
                                direct_ship_ind,
                                last_update_datetime,
                                last_update_id,
                                create_datetime,
                                concession_rate,
                                primary_case_size,
                                create_id)
                         SELECT I_item,
                                supplier,
                                primary_supp_ind,
                                NULL,
                                NULL,
                                consignment_rate,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                pallet_name,
                                case_name,
                                inner_name,
                                supp_discontinue_date,
                                direct_ship_ind,
                                SYSDATE,
                                L_user,
                                SYSDATE,
                                concession_rate,
                                primary_case_size,
                                L_user
                           FROM item_supplier
                          WHERE item = I_item_parent;
   END IF;
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      RETURN FALSE;
END INSERT_ITEM_SUPPLIER;
-------------------------------------------------------------------
FUNCTION SUB_TRAN_INSERT_ITEM_SUPPLIER(O_error_message   IN OUT   VARCHAR2,
                                       I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(80)                := 'ITEM_CREATE_SQL.SUB_TRAN_INSERT_ITEM_SUPPLIER';
   L_user        VARCHAR2(30)                := GET_USER;
   L_item_parent ITEM_MASTER.ITEM%TYPE       := I_item_parent;

BEGIN
   IF I_item_parent IS NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_parent',
                                             L_program,
                                             NULL);
      RETURN FALSE;
   END IF;

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_SUPPLIER','ITEM: '||NULL);
   INSERT INTO item_supplier(item,
                             supplier,
                             primary_supp_ind,
                             vpn,
                             supp_label,
                             consignment_rate,
                             supp_diff_1,
                             supp_diff_2,
                             supp_diff_3,
                             supp_diff_4,
                             pallet_name,
                             case_name,
                             inner_name,
                             supp_discontinue_date,
                             direct_ship_ind,
                             last_update_datetime,
                             last_update_id,
                             create_datetime,
                             primary_case_size,
                             create_id)
                      SELECT it.item,
                             its.supplier,
                             its.primary_supp_ind,
                             NULL,
                             NULL,
                             its.consignment_rate,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             its.pallet_name,
                             its.case_name,
                             its.inner_name,
                             its.supp_discontinue_date,
                             its.direct_ship_ind,
                             SYSDATE,
                             L_user,
                             SYSDATE,
                             its.primary_case_size,
                             L_user
                        FROM item_supplier its,
                             item_temp it
                       WHERE its.item = I_item_parent;
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      RETURN FALSE;
END SUB_TRAN_INSERT_ITEM_SUPPLIER;
--------------------------------------------------------------------------
FUNCTION SUB_TRAN_INSERT_ITEM_COUNTRY(O_error_message   IN OUT   VARCHAR2,
                                       I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(80)                := 'ITEM_CREATE_SQL.SUB_TRAN_INSERT_ITEM_COUNTRY';
   L_user        VARCHAR2(30)                := GET_USER;
   L_item_parent ITEM_MASTER.ITEM%TYPE       := I_item_parent;

BEGIN
   IF I_item_parent IS NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_parent',
                                             L_program,
                                             NULL);
      RETURN FALSE;
   END IF;

   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY','ITEM: '||NULL);
   INSERT INTO item_country(item,
                            country_id)
                     SELECT it.item,
                            itc.country_id
                       FROM item_country itc,
                            item_temp it
                      WHERE itc.item = I_item_parent;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY_L10N_EXT','ITEM: '||NULL);
   INSERT INTO item_country_l10n_ext(item,
                                     country_id,
                                     l10n_country_id,
                                     group_id,
                                     varchar2_1,
                                     varchar2_2,
                                     varchar2_3,
                                     varchar2_4,
                                     varchar2_5,
                                     varchar2_6,
                                     varchar2_7,
                                     varchar2_8,
                                     varchar2_9,
                                     varchar2_10,
                                     number_11,
                                     number_12,
                                     number_13,
                                     number_14,
                                     number_15,
                                     number_16,
                                     number_17,
                                     number_18,
                                     number_19,
                                     number_20,
                                     date_21,
                                     date_22)
                              select it.item,
                                     itle.country_id,
                                     itle.l10n_country_id,
                                     itle.group_id,
                                     itle.varchar2_1,
                                     itle.varchar2_2,
                                     itle.varchar2_3,
                                     itle.varchar2_4,
                                     itle.varchar2_5,
                                     itle.varchar2_6,
                                     itle.varchar2_7,
                                     itle.varchar2_8,
                                     itle.varchar2_9,
                                     itle.varchar2_10,
                                     itle.number_11,
                                     itle.number_12,
                                     itle.number_13,
                                     itle.number_14,
                                     itle.number_15,
                                     itle.number_16,
                                     itle.number_17,
                                     itle.number_18,
                                     itle.number_19,
                                     itle.number_20,
                                     itle.date_21,
                                     itle.date_22
                                from item_country_l10n_ext itle,
                                     item_temp it
                               where itle.item = I_item_parent;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      RETURN FALSE;
END SUB_TRAN_INSERT_ITEM_COUNTRY;
--------------------------------------------------------------------------
FUNCTION INSERT_ITEM_MASTER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item_rec        IN       ITEM_MASTER%ROWTYPE)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'ITEM_CREATE_SQL.INSERT_ITEM_MASTER';
   L_primary_ref_item_ind  CHAR(1);
   L_table                VARCHAR2(20)  := NULL;
   RECORD_LOCKED          EXCEPTION;
   PRAGMA                 EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   CURSOR C_CHECK_REC_EXISTS IS
      select 'Y'
        from item_master
       where item_parent = I_item_rec.item_parent
         and primary_ref_item_ind = 'Y'
         and rownum = 1;
   ---
   CURSOR C_LOCK_ITEM_MASTER is
      select 'x'
        from item_master
       where item_parent = I_item_rec.item_parent
         for update of primary_ref_item_ind nowait;
   ---
BEGIN

   L_primary_ref_item_ind := I_item_rec.primary_ref_item_ind;
   if I_item_rec.item_level > I_item_rec.tran_level then
      L_table := 'ITEM_MASTER';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ITEM_MASTER','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_rec.item_parent));
      open C_LOCK_ITEM_MASTER;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ITEM_MASTER','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_rec.item_parent));
      close C_LOCK_ITEM_MASTER;

      SQL_LIB.SET_MARK('OPEN','C_CHECK_REC_EXISTS','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_rec.item_parent));
      open C_CHECK_REC_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_REC_EXISTS','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_rec.item_parent));
      fetch C_CHECK_REC_EXISTS into L_primary_ref_item_ind;

      if C_CHECK_REC_EXISTS%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_REC_EXISTS','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_rec.item_parent));
         close C_CHECK_REC_EXISTS;
         L_primary_ref_item_ind := 'Y';
      elsif I_item_rec.primary_ref_item_ind = 'Y' and L_primary_ref_item_ind = 'Y' then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_REC_EXISTS','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_rec.item_parent));
         close C_CHECK_REC_EXISTS;
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_MASTER','Item parent: '||I_item_rec.item_parent);
         update item_master
            set primary_ref_item_ind ='N'
          where item_parent = I_item_rec.item_parent;
      elsif I_item_rec.primary_ref_item_ind = 'N' and L_primary_ref_item_ind = 'Y' then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_REC_EXISTS','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_rec.item_parent));
         close C_CHECK_REC_EXISTS;
         L_primary_ref_item_ind := I_item_rec.primary_ref_item_ind;
      else
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_REC_EXISTS','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_rec.item_parent));
         close C_CHECK_REC_EXISTS;
      end if;
   end if;

   SQL_LIB.SET_MARK('INSERT', NULL, 'ITEM_MASTER','item: '||I_item_rec.item);
   INSERT INTO item_master(item,
                           item_number_type,
                           format_id,
                           prefix,
                           item_parent,
                           item_grandparent,
                           pack_ind,
                           item_level,
                           tran_level,
                           item_aggregate_ind,
                           diff_1,
                           diff_1_aggregate_ind,
                           diff_2,
                           diff_2_aggregate_ind,
                           diff_3,
                           diff_3_aggregate_ind,
                           diff_4,
                           diff_4_aggregate_ind,
                           dept,
                           CLASS,
                           subclass,
                           status,
                           item_desc,
                           item_desc_secondary,
                           short_desc,
                           desc_up,
                           primary_ref_item_ind,
                           cost_zone_group_id,
                           standard_uom,
                           uom_conv_factor,
                           package_size,
                           package_uom,
                           merchandise_ind,
                           store_ord_mult,
                           forecast_ind,
                           original_retail,
                           mfg_rec_retail,
                           retail_label_type,
                           retail_label_value,
                           handling_temp,
                           handling_sensitivity,
                           catch_weight_ind,
                           waste_type,
                           waste_pct,
                           default_waste_pct,
                           const_dimen_ind,
                           simple_pack_ind,
                           contains_inner_ind,
                           sellable_ind,
                           orderable_ind,
                           pack_type,
                           order_as_type,
                           comments,
                           item_service_level,
                           gift_wrap_ind,
                           ship_alone_ind,
                           create_datetime,
                           last_update_id,
                           last_update_datetime,
                           check_uda_ind,
                           item_xform_ind,
                           inventory_ind,
                           order_type,
                           sale_type,
                           deposit_item_type,
                           container_item,
                           deposit_in_price_per_uom,
                           aip_case_type,
                           perishable_ind,
                           notional_pack_ind,
                           soh_inquiry_at_pack_ind,
                           catch_weight_uom,
                           catch_weight_type,
                           brand_name,
                           product_classification,
                           curr_selling_unit_retail,
                           curr_selling_uom,
                           create_id)
                   VALUES( I_item_rec.item,
                           I_item_rec.item_number_type,
                           I_item_rec.format_id,
                           I_item_rec.prefix,
                           I_item_rec.item_parent,
                           I_item_rec.item_grandparent,
                           I_item_rec.pack_ind,
                           I_item_rec.item_level,
                           I_item_rec.tran_level,
                           I_item_rec.item_aggregate_ind,
                           I_item_rec.diff_1,
                           I_item_rec.diff_1_aggregate_ind,
                           I_item_rec.diff_2,
                           I_item_rec.diff_2_aggregate_ind,
                           I_item_rec.diff_3,
                           I_item_rec.diff_3_aggregate_ind,
                           I_item_rec.diff_4,
                           I_item_rec.diff_4_aggregate_ind,
                           I_item_rec.dept,
                           I_item_rec.CLASS,
                           I_item_rec.subclass,
                           'W',
                           I_item_rec.item_desc,
                           I_item_rec.item_desc_secondary,
                           I_item_rec.short_desc,
                           I_item_rec.desc_up,
                           L_primary_ref_item_ind,
                           I_item_rec.cost_zone_group_id,
                           I_item_rec.standard_uom,
                           I_item_rec.uom_conv_factor,
                           I_item_rec.package_size,
                           I_item_rec.package_uom,
                           I_item_rec.merchandise_ind,
                           I_item_rec.store_ord_mult,
                           I_item_rec.forecast_ind,
                           I_item_rec.original_retail,
                           I_item_rec.mfg_rec_retail,
                           I_item_rec.retail_label_type,
                           I_item_rec.retail_label_value,
                           I_item_rec.handling_temp,
                           I_item_rec.handling_sensitivity,
                           I_item_rec.catch_weight_ind,
                           I_item_rec.waste_type,
                           I_item_rec.waste_pct,
                           I_item_rec.default_waste_pct,
                           I_item_rec.const_dimen_ind,
                           I_item_rec.simple_pack_ind,
                           I_item_rec.contains_inner_ind,
                           I_item_rec.sellable_ind,
                           I_item_rec.orderable_ind,
                           I_item_rec.pack_type,
                           I_item_rec.order_as_type,
                           I_item_rec.comments,
                           I_item_rec.item_service_level,
                           I_item_rec.gift_wrap_ind,
                           I_item_rec.ship_alone_ind,
                           I_item_rec.create_datetime,
                           I_item_rec.last_update_id,
                           SYSDATE,
                           I_item_rec.check_uda_ind,
                           I_item_rec.item_xform_ind,
                           I_item_rec.inventory_ind,
                           I_item_rec.order_type,
                           I_item_rec.sale_type,
                           I_item_rec.deposit_item_type,
                           I_item_rec.container_item,
                           I_item_rec.deposit_in_price_per_uom,
                           I_item_rec.aip_case_type,
                           I_item_rec.perishable_ind,
                           NVL(I_item_rec.notional_pack_ind, 'N'),
                           NVL(I_item_rec.soh_inquiry_at_pack_ind, 'N'),
                           I_item_rec.catch_weight_uom,
                           I_item_rec.catch_weight_type,
                           I_item_rec.brand_name,
                           I_item_rec.product_classification,
                           I_item_rec.curr_selling_unit_retail,
                           I_item_rec.curr_selling_uom,
                           I_item_rec.create_id);

   if I_item_rec.status ='A' then
      update item_master
         set status='A'
       where item= I_item_rec.item;
   end if;
   ---

   RETURN TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_program,
                                             to_char(I_item_rec.item_parent));
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;

END INSERT_ITEM_MASTER;
------------------------------------------------------------------
FUNCTION MASS_UPDATE_CHILDREN(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_supplier          IN       SUPS.SUPPLIER%TYPE,
                              I_item_parent       IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                              I_rpm_zone_group_id IN       PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE,
                              I_rpm_zone_id       IN       PM_RETAIL_API_SQL.ZONE_ID%TYPE,
                              I_rpm_currency_code IN       CURRENCIES.CURRENCY_CODE%TYPE,
                              I_diff_group_id     IN       DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                              I_diff_id           IN       DIFF_GROUP_DETAIL.DIFF_ID%TYPE,
                              I_supp_diff_id      IN       DIFF_GROUP_DETAIL.DIFF_ID%TYPE,
                              I_unit_retail       IN       ITEM_LOC.UNIT_RETAIL%TYPE,
                              I_unit_cost         IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                              I_generate_ean      IN       BOOLEAN,
                              I_auto_approve      IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program               VARCHAR2(40) := 'ITEM_CREATE_SQL.MASS_UPDATE_CHILDREN';
   L_supplier_diff_number  ITEM_MASTER.DIFF_1%TYPE;
   L_standard_uom          ITEM_MASTER.STANDARD_UOM%TYPE;
   L_standard_class        UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor           ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_to_value              NUMBER;
   L_retail                NUMBER;
   L_selling_unit_retail   NUMBER;
   L_table                 VARCHAR2(25) := NULL;
   L_child_table_index     NUMBER := 1;
   L_child_item_table      PM_RETAIL_API_SQL.CHILD_ITEM_PRICING_TABLE;
   L_first_ean_exists      BOOLEAN;
   L_ean                   ITEM_MASTER.ITEM%TYPE;
   L_item_number_type      ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE;
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(RECORD_LOCKED, -54);

   L_location_table        OBJ_RPM_LOC_TBL;
   L_prev_item             ITEM_MASTER.ITEM%TYPE;
   L_item                  ITEM_MASTER.ITEM%TYPE;
   L_primary_supplier      VARCHAR2(1) := 'N';
   L_origin_country_id     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   L_zone_group_id         CONSTANT NUMBER(10) := 1;

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   L_negotiated_item_cost   ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE;
   L_extended_base_cost     ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE;
   L_inclusive_cost         ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE;
   L_base_cost              ITEM_SUPP_COUNTRY.BASE_COST%TYPE;

   L_itemloc_table         OBJ_ITEMLOC_TBL := OBJ_ITEMLOC_TBL();
   L_item_table            ITEM_TBL := ITEM_TBL();
   L_comp_item_cost_tbl    OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();
   L_country_attrib_row    COUNTRY_ATTRIB%ROWTYPE := NULL;

   TYPE TABLE1 IS TABLE OF ITEM_LOC.SELLING_UNIT_RETAIL%TYPE
      INDEX BY BINARY_INTEGER;

   TYPE TABLE2 IS TABLE OF PM_RETAIL_API_SQL.ZONE_GROUP_ID%TYPE
      INDEX BY BINARY_INTEGER;

   TYPE TABLE3 IS TABLE OF PM_RETAIL_API_SQL.ZONE_ID%TYPE
      INDEX BY BINARY_INTEGER;

   TYPE TABLE4 IS TABLE OF ITEM_LOC.ITEM%TYPE
      INDEX BY BINARY_INTEGER;

   L_rpm_selling_unit  TABLE1;
   L_rpm_zone_group_id TABLE2;
   L_rpm_zone_id       TABLE3;
   L_rpm_item          TABLE4;
   L_selling_unit      TABLE1;
   L_itm_loc           TABLE3;
   L_itm               TABLE4;
   L_cnt               NUMBER := 0;
   L_tran_level                ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_item_level                ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_child_item_number_type    ITEM_MASTER.ITEM_NUMBER_TYPE%TYPE;
   L_child_exists              BOOLEAN;
   L_child_ind                 VARCHAR2(1) := 'Y';

   CURSOR C_LOCK_ITEM_SUPPLIER IS
      SELECT isupp.supp_diff_1
        FROM item_supplier isupp
       WHERE isupp.supplier=I_supplier
         AND isupp.item IN (SELECT im.item
                              FROM item_master im
                             WHERE im.item_parent=I_item_parent
                               AND I_diff_id IS NULL
                               AND EXISTS (SELECT '1'
                                             FROM diff_group_detail dgd
                                            WHERE dgd.diff_id IN (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                                              AND dgd.diff_group_id=I_diff_group_id
                                              AND ROWNUM = 1)
                             UNION
                            SELECT im.item
                              FROM item_master im
                             WHERE im.item_parent=I_item_parent
                               AND I_diff_id IN (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                               AND I_diff_id IS NOT NULL
                             UNION
                            SELECT im.item
                              FROM item_master im
                             WHERE im.item_parent=I_item_parent
                               AND I_diff_group_id IS NULL
                               AND I_diff_id IS NULL)
      FOR UPDATE OF supp_diff_1 NOWAIT;

   CURSOR C_GET_SUPP_DIFF_NUMBER IS
      SELECT DECODE(I_diff_group_id,diff_1,1,diff_2,2,diff_3,3,diff_4,4)
        FROM item_master
       WHERE item=I_item_parent;

   CURSOR C_LOCK_ITEM_SUPP_COUNTRY IS
      SELECT isc.unit_cost
        FROM item_supp_country isc
       WHERE isc.supplier=I_supplier
         AND isc.primary_country_ind ='Y'
         AND isc.item IN (SELECT im.item
                            FROM item_master im
                           WHERE im.item_parent=I_item_parent
                             AND im.status='W'
                             AND I_diff_id IS NULL
                             AND EXISTS (SELECT '1'
                                           FROM diff_group_detail dgd
                                          WHERE dgd.diff_id IN (im.diff_1, im.diff_2,im.diff_3,im.diff_4)
                                            AND dgd.diff_group_id=I_diff_group_id
                                            AND ROWNUM = 1)
                           UNION
                          SELECT im.item
                            FROM item_master im
                           WHERE im.item_parent=I_item_parent
                             AND im.status='W'
                             AND I_diff_id IN (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                             AND I_diff_id IS NOT NULL
                           UNION
                          SELECT im.item
                            FROM item_master im
                           WHERE im.item_parent=I_item_parent
                             AND im.status='W'
                             AND I_diff_group_id IS NULL
                             AND I_diff_id IS NULL)
      FOR UPDATE OF unit_cost NOWAIT;

   cursor C_LOCK_ITEM_SUPP_COUNTRY_LOC is
      select iscl.unit_cost
        from item_supp_country_loc iscl,
             item_supp_country isc
       where iscl.supplier           = isc.supplier
         and isc.supplier            = I_supplier
         and isc.primary_country_ind ='Y'
         and iscl.origin_country_id  = isc.origin_country_id
         and iscl.item               = isc.item
         and isc.item in (select im.item
                            from item_master im
                           where im.item_parent = I_item_parent
                             and im.status      = 'W'
                             and I_diff_id is NULL
                             and exists (select '1'
                                           from diff_group_detail dgd
                                          where dgd.diff_id in (im.diff_1, im.diff_2,im.diff_3,im.diff_4)
                                            and dgd.diff_group_id = I_diff_group_id
                                            and rownum            = 1)
                           union
                          select im.item
                            from item_master im
                           where im.item_parent = I_item_parent
                             and im.status      = 'W'
                             and I_diff_id in (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                             and I_diff_id is NOT NULL
                           union
                          select im.item
                            from item_master im
                           where im.item_parent = I_item_parent
                             and im.status      = 'W'
                             and I_diff_group_id is NULL
                             and I_diff_id is NULL)
      for update of iscl.unit_cost nowait;

   CURSOR C_ITEM_CHILDREN_INFO IS
      SELECT im.item
        FROM item_master im
       WHERE im.item_parent = I_item_parent
         AND im.status IN ('W','S')
         AND I_diff_id IS NULL
         AND EXISTS (SELECT '1'
                       FROM diff_group_detail dgd
                      WHERE dgd.diff_id IN (im.diff_1, im.diff_2, im.diff_3, im.diff_4)
                        AND dgd.diff_group_id = I_diff_group_id
                        AND rownum = 1)
       UNION
      SELECT im.item
        FROM item_master im
       WHERE im.item_parent = I_item_parent
         AND im.status IN ('W','S')
         AND I_diff_id IN (im.diff_1, im.diff_2, im.diff_3, im.diff_4)
         AND I_diff_id IS NOT NULL
       UNION
      SELECT im.item
        FROM item_master im
       WHERE im.item_parent = I_item_parent
         AND im.status IN ('W','S')
         AND I_diff_group_id IS NULL
         AND I_diff_id IS NULL;

   cursor C_CHECK_PRIMARY_SUPPLIER is
      select 'Y',
             isc.origin_country_id
        from item_supp_country isc
       where isc.supplier            = I_supplier
         and isc.primary_country_ind = 'Y'
         and isc.primary_supp_ind    = 'Y'
         and isc.item                = L_item;

   CURSOR C_LOCK_ITEM_LOC IS
      SELECT il.item,
             il.loc,
             il.unit_retail,
             il.selling_unit_retail,
             il.regular_unit_retail,
             il.selling_uom,
             il.multi_units,
             il.multi_unit_retail,
             il.multi_selling_uom
        FROM item_loc il,
             TABLE(CAST(L_itemloc_table AS OBJ_ITEMLOC_TBL)) itl
       WHERE il.item = itl.item
         AND il.loc = itl.loc
       ORDER BY il.item
     FOR UPDATE OF unit_retail NOWAIT;

   TYPE UPDATE_ITEM_LOC_TYPE IS TABLE OF C_LOCK_ITEM_LOC%ROWTYPE INDEX BY BINARY_INTEGER;
   L_item_loc UPDATE_ITEM_LOC_TYPE;

   CURSOR C_LOCK_ITEM_LOC_RPM_N IS
      SELECT il.item,
             il.loc,
             il.unit_retail,
             il.selling_unit_retail,
             il.regular_unit_retail,
             il.selling_uom,
             il.multi_units,
             il.multi_unit_retail,
             il.multi_selling_uom
        FROM item_loc il,
             TABLE(CAST(L_item_table AS ITEM_TBL)) a
       WHERE il.item = value(a)
       ORDER BY il.item
     FOR UPDATE OF unit_retail NOWAIT;

   TYPE UPDATE_ITEM_LOC_TYPE_RPM_N IS TABLE OF C_LOCK_ITEM_LOC_RPM_N%ROWTYPE INDEX BY BINARY_INTEGER;
   L_item_loc_rpm_n UPDATE_ITEM_LOC_TYPE_RPM_N;
   
   CURSOR C_LOCK_ITEM_MASTER IS
      SELECT i.item
        FROM item_master i,
             TABLE(CAST(L_item_table AS ITEM_TBL)) a
       WHERE i.item = value(a)
       ORDER BY i.item
     FOR UPDATE OF curr_selling_unit_retail NOWAIT;

   CURSOR C_APPLY_RECS IS
      SELECT im1.item,
             im1.item_desc,
             im1.diff_1,
             im1.diff_2,
             im1.diff_3,
             im1.diff_4,
             im1.status
        FROM item_master im1
       WHERE im1.item_parent = I_item_parent
         AND I_diff_id IS NULL
         AND EXISTS (SELECT '1'
                      FROM diff_group_detail dgd
                     WHERE dgd.diff_id IN (im1.diff_1,im1.diff_2,im1.diff_3,im1.diff_4)
                       AND dgd.diff_group_id=I_diff_group_id
                       AND ROWNUM = 1)
       UNION
      SELECT im2.item,
             im2.item_desc,
             im2.diff_1,
             im2.diff_2,
             im2.diff_3,
             im2.diff_4,
             im2.status
        FROM item_master im2
       WHERE im2.item_parent=I_item_parent
         AND I_diff_id IN (im2.diff_1,im2.diff_2,im2.diff_3,im2.diff_4)
         AND I_diff_id IS NOT NULL
       UNION
      SELECT im3.item,
             im3.item_desc,
             im3.diff_1,
             im3.diff_2,
             im3.diff_3,
             im3.diff_4,
             im3.status
        FROM item_master im3
       WHERE im3.item_parent=I_item_parent
         AND I_diff_group_id IS NULL
         AND I_diff_id IS NULL;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   if I_generate_ean = TRUE then
      if ITEM_ATTRIB_SQL.GET_LEVELS(O_ERROR_MESSAGE,
                                    L_item_level,
                                    L_tran_level,
                                    I_item_parent) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN','C_APPLY_RECS','ITEM_MASTER','Item parent: '||TO_CHAR(I_item_parent));
      FOR rec in C_APPLY_RECS LOOP
         L_child_ind := 'Y';
         if (L_item_level + 2) = L_tran_level then
            L_child_exists := TRUE;
            L_child_item_number_type := NULL;
            if ITEM_ATTRIB_SQL.GET_CHILD_ITEM_NUMBER_TYPE(O_error_message,
                                                          L_child_item_number_type,
                                                          L_child_exists,
                                                          rec.item) = FALSE then
               return FALSE;
            end if;
            if not L_child_exists then
               L_child_ind := 'N';
            end if;
         else
            if ITEM_ATTRIB_SQL.GET_FIRST_EAN(O_error_message,
                                             L_first_ean_exists,
                                             L_ean,
                                             L_item_number_type,
                                             rec.item) = FALSE then
               return FALSE;
            end if;
            if L_ean is NULL then
               L_child_ind := 'N';
            end if;
         end if;

         --- Call package to auto generate the next EAN number
         if L_child_ind = 'N' then
            if ITEM_ATTRIB_SQL.NEXT_EAN (O_error_message,
                                         L_ean) = FALSE then
               return FALSE;
            else
               if L_ean is NOT NULL then

                  L_item_number_type := 'EAN13';
                  ---
                  --Create the level 3 EAN items
                  if ITEM_CREATE_SQL.TRAN_CHILDREN_LEVEL3(O_error_message,
                                                          L_ean,
                                                          L_item_number_type,
                                                          3,
                                                          rec.item_desc,
                                                          rec.diff_1,
                                                          rec.diff_2,
                                                          rec.diff_3,
                                                          rec.diff_4,
                                                          rec.item,
                                                          I_auto_approve,
                                                          rec.status) = FALSE then
                     return FALSE;
                  end if;
                  ---
               end if;
               ---
            end if;
            ---
         end if;

      END LOOP;

   end if;

   if I_supp_diff_id is NOT NULL then
      L_table := 'ITEM_SUPPLIER';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_SUPPLIER',
                       'ITEM_SUPPLIER',
                       'Supplier: '||TO_CHAR(I_supplier));
      open C_LOCK_ITEM_SUPPLIER;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_SUPPLIER',
                       'ITEM_SUPPLIER',
                       'Supplier: '||TO_CHAR(I_supplier));
      close C_LOCK_ITEM_SUPPLIER;

      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SUPP_DIFF_NUMBER',
                       'ITEM_MASTER',
                       'Item: '||TO_CHAR(I_item_parent));
      open C_GET_SUPP_DIFF_NUMBER;

      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SUPP_DIFF_NUMBER',
                       'ITEM_MASTER',
                       'Item: '||TO_CHAR(I_item_parent));
      fetch C_GET_SUPP_DIFF_NUMBER into L_supplier_diff_number;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SUPP_DIFF_NUMBER',
                       'ITEM_MASTER',
                       'Item: '||TO_CHAR(I_item_parent));
      close C_GET_SUPP_DIFF_NUMBER;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_SUPPLIER',
                       'Supplier: '||TO_CHAR(I_supplier)||
                       'Supplier diff number: '||TO_CHAR(L_supplier_diff_number));

      update item_supplier
         set supp_diff_1 = DECODE(L_supplier_diff_number,1,I_supp_diff_id,supp_diff_1),
             supp_diff_2 = DECODE(L_supplier_diff_number,2,I_supp_diff_id,supp_diff_2),
             supp_diff_3 = DECODE(L_supplier_diff_number,3,I_supp_diff_id,supp_diff_3),
             supp_diff_4 = DECODE(L_supplier_diff_number,4,I_supp_diff_id,supp_diff_4),
             last_update_datetime = SYSDATE,
             last_update_id       = GET_USER
       where supplier=I_supplier
         and item in (select im.item
                        from item_master im
                       where im.item_parent=I_item_parent
                         and I_diff_id is NULL
                         and exists (select '1'
                                       from diff_group_detail dgd
                                      where dgd.diff_id IN (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                                        and dgd.diff_group_id=I_diff_group_id
                                        and rownum = 1)
                       union
                      select im.item
                        from item_master im
                       where im.item_parent=I_item_parent
                         and I_diff_id in (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                         and I_diff_id is NOT NULL
                       union
                      select im.item
                        from item_master im
                       where im.item_parent=I_item_parent
                         and I_diff_group_id is NULL
                         and I_diff_id is NULL);
   end if;

   if I_unit_cost is NOT NULL then
      L_table := 'ITEM_SUPP_COUNTRY';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_SUPP_COUNTRY',
                       'ITEM_SUPPLIER',
                       'Supplier: '||TO_CHAR(I_supplier));
      open  C_LOCK_ITEM_SUPP_COUNTRY;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_SUPP_COUNTRY',
                       'ITEM_SUPPLIER',
                       'Supplier: '||TO_CHAR(I_supplier));
      close C_LOCK_ITEM_SUPP_COUNTRY;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_SUPP_COUNTRY',
                       'Supplier: '||TO_CHAR(I_supplier)||
                       ', Item parent: '||TO_CHAR(I_item_parent));

      if L_system_options_rec.default_tax_type = 'GTAX' then
         update item_supp_country
            set unit_cost = I_unit_cost
          where supplier = I_supplier
            and primary_country_ind ='Y'
            and item in ( select im.item
                            from item_master im
                           where im.item_parent=I_item_parent
                             and im.status='W'
                             and I_diff_id is NULL
                             and exists (select '1'
                                           from diff_group_detail dgd
                                          where dgd.diff_id in (im.diff_1, im.diff_2,im.diff_3,im.diff_4)
                                            and dgd.diff_group_id=I_diff_group_id
                                            and rownum = 1)
                           union
                          select im.item
                            from item_master im
                           where im.item_parent=I_item_parent
                             and im.status='W'
                             and I_diff_id in (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                             and I_diff_id is NOT NULL
                           union
                          select im.item
                            from item_master im
                           where im.item_parent=I_item_parent
                             and im.status='W'
                             and I_diff_group_id is NULL
                             and I_diff_id is NULL);
      else -- default_tax_type SALES, SVAT or NULL

         update item_supp_country
            set unit_cost = I_unit_cost,
                negotiated_item_cost = NULL,
                extended_base_cost   = NULL,
                inclusive_cost       = NULL,
                base_cost            = NULL
          where supplier = I_supplier
            and primary_country_ind ='Y'
            and item in ( select im.item
                            from item_master im
                           where im.item_parent=I_item_parent
                             and im.status='W'
                             and I_diff_id is NULL
                             and exists (select '1'
                                           from diff_group_detail dgd
                                          where dgd.diff_id in (im.diff_1, im.diff_2,im.diff_3,im.diff_4)
                                            and dgd.diff_group_id=I_diff_group_id
                                            and rownum = 1)
                           union
                          select im.item
                            from item_master im
                           where im.item_parent=I_item_parent
                             and im.status='W'
                             and I_diff_id in (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                             and I_diff_id is NOT NULL
                           union
                          select im.item
                            from item_master im
                           where im.item_parent=I_item_parent
                             and im.status='W'
                             and I_diff_group_id is NULL
                             and I_diff_id is NULL);
      end if;

      L_table := 'ITEM_SUPP_COUNTRY_LOC';
      SQL_LIB.SET_MARK('OPEN',
                       'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                       'ITEM_SUPPLIER',
                       'Supplier: '||TO_CHAR(I_supplier));
      open  C_LOCK_ITEM_SUPP_COUNTRY_LOC;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_LOCK_ITEM_SUPP_COUNTRY_LOC',
                       'ITEM_SUPPLIER',
                       'Supplier: '||TO_CHAR(I_supplier));
      close C_LOCK_ITEM_SUPP_COUNTRY_LOC;

      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'ITEM_SUPPLIER_COUNTRY_LOC',
                       'Supplier: '||TO_CHAR(I_supplier)||
                       ', Item parent: '||TO_CHAR(I_item_parent));

      if L_system_options_rec.default_tax_type = 'GTAX' then

         update item_supp_country_loc iscl
            set unit_cost = (select unit_cost
                               from item_supp_country isc
                              where isc.item                = iscl.item
                                and isc.supplier            = iscl.supplier
                                and isc.origin_country_id   = iscl.origin_country_id
                                and isc.primary_country_ind = 'Y')
          where iscl.supplier = I_supplier
            and exists (select 1
                          from item_supp_country isc1
                         where isc1.item                = iscl.item
                           and isc1.supplier            = iscl.supplier
                           and isc1.origin_country_id   = iscl.origin_country_id
                           and isc1.primary_country_ind = 'Y')
            and iscl.item in (select im.item
                               from item_master im
                              where im.item_parent = I_item_parent
                                and im.status      = 'W'
                                and I_diff_id IS NULL
                                and exists (select '1'
                                              from diff_group_detail dgd
                                             where dgd.diff_id in (im.diff_1, im.diff_2,im.diff_3,im.diff_4)
                                               and dgd.diff_group_id = I_diff_group_id
                                               and rownum = 1)
                              union
                             select im.item
                               from item_master im
                              where im.item_parent = I_item_parent
                                and im.status      = 'W'
                                and I_diff_id in (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                                and I_diff_id is NOT NULL
                              union
                             select im.item
                               from item_master im
                              where im.item_parent = I_item_parent
                                and im.status      = 'W'
                                and I_diff_group_id is NULL
                                and I_diff_id is NULL);
      else -- default_tax_type SALES or NULL

         merge into item_supp_country_loc iscl
            using item_supp_country isc
               on (iscl.item               = isc.item and
                   iscl.supplier           = isc.supplier and
                   iscl.origin_country_id  = isc.origin_country_id)
             when matched then
                update set iscl.unit_cost            = isc.unit_cost,
                           iscl.negotiated_item_cost = NULL,
                           iscl.extended_base_cost   = NULL,
                           iscl.inclusive_cost       = NULL,
                           iscl.base_cost            = NULL
                     where isc.supplier            = I_supplier
                       and isc.primary_country_ind = 'Y'
                       and isc.item in (select im.item
                                          from item_master im
                                         where im.item_parent = I_item_parent
                                           and im.status      = 'W'
                                           and I_diff_id IS NULL
                                           and exists (select '1'
                                                         from diff_group_detail dgd
                                                        where dgd.diff_id in (im.diff_1, im.diff_2,im.diff_3,im.diff_4)
                                                          and dgd.diff_group_id= I_diff_group_id
                                                          and rownum = 1)
                                          union
                                         select im.item
                                           from item_master im
                                          where im.item_parent = I_item_parent
                                            and im.status      = 'W'
                                            and I_diff_id in (im.diff_1,im.diff_2,im.diff_3,im.diff_4)
                                            and I_diff_id is NOT NULL
                                          union
                                         select im.item
                                           from item_master im
                                          where im.item_parent = I_item_parent
                                            and im.status      = 'W'
                                            and I_diff_group_id is NULL
                                            and I_diff_id is NULL);
      end if;

      SQL_LIB.SET_MARK('OPEN',
                       'C_ITEM_CHILDREN_INFO',
                       'ITEM_MASTER',
                       'Item parent: '||TO_CHAR(I_item_parent));
      open C_ITEM_CHILDREN_INFO;

      SQL_LIB.SET_MARK('FETCH',
                       'C_ITEM_CHILDREN_INFO',
                       'ITEM_MASTER',
                       'Item parent: '||TO_CHAR(I_item_parent));
      fetch C_ITEM_CHILDREN_INFO BULK COLLECT into L_item_table;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_CHILDREN_INFO',
                       'ITEM_MASTER',
                       'Item parent: '||TO_CHAR(I_item_parent));
      close C_ITEM_CHILDREN_INFO;

      if L_item_table.COUNT > 0 then
         for i IN L_item_table.FIRST..L_item_table.LAST LOOP
            L_item := L_item_table(i);
            SQL_LIB.SET_MARK('OPEN',
                             'C_CHECK_PRIMARY_SUPPLIER',
                             'ITEM_MASTER',
                             'Item: '||TO_CHAR(L_item)||
                             'Supplier: '||TO_CHAR(I_supplier));
            open C_CHECK_PRIMARY_SUPPLIER;

            SQL_LIB.SET_MARK('FETCH',
                             'C_CHECK_PRIMARY_SUPPLIER',
                             'ITEM_MASTER',
                             'Item: '||TO_CHAR(L_item)||
                             'Supplier: '||TO_CHAR(I_supplier));
            fetch C_CHECK_PRIMARY_SUPPLIER into L_primary_supplier,
                                                L_origin_country_id;
            SQL_LIB.SET_MARK('CLOSE',
                             'C_CHECK_PRIMARY_SUPPLIER',
                             'ITEM_MASTER',
                             'Item: '||TO_CHAR(L_item)||
                             'Supplier: '||TO_CHAR(I_supplier));
            close C_CHECK_PRIMARY_SUPPLIER;

            if L_primary_supplier = 'Y' then
               if ITEM_SUPP_COUNTRY_SQL.FIRST_SUPPLIER(O_error_message,
                                                       L_item,
                                                       I_supplier,
                                                       L_origin_country_id,
                                                       I_unit_cost,
                                                       NULL,
                                                       NULL,
                                                       'Y') = FALSE then
                  return FALSE;
               end if;
            end if;
            L_primary_supplier := 'N';
         end loop;
      end if;
   end if;

   if I_unit_retail is NOT NULL then
      L_child_item_table.DELETE;

      if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                          L_standard_uom,
                                          L_standard_class,
                                          L_conv_factor,
                                          I_item_parent,
                                          'N') = FALSE then
         return FALSE;
      end if;

      -- Select all child items for the retail update.  The child items are selected based
      -- on the diff_group and diff_id filter criteria from the user.
      SQL_LIB.SET_MARK('OPEN',
                       'C_ITEM_CHILDREN_INFO',
                       'ITEM_MASTER',
                       'Item parent: '||TO_CHAR(I_item_parent));
      open C_ITEM_CHILDREN_INFO;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ITEM_CHILDREN_INFO',
                       'ITEM_MASTER',
                       'Item parent: '||TO_CHAR(I_item_parent));
      fetch C_ITEM_CHILDREN_INFO BULK COLLECT into L_item_table;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_CHILDREN_INFO',
                       'ITEM_MASTER',
                       'Item parent: '||TO_CHAR(I_item_parent));
      close C_ITEM_CHILDREN_INFO;

      if L_system_options_rec.rpm_ind = 'Y' then

         -- Retreive all of the locations that RPM knows about.
         PM_RETAIL_API_SQL.GET_ZONE_LOCATIONS(L_location_table,
                                              I_rpm_zone_group_id,
                                              I_rpm_zone_id);

         -- Build a table of item/location combinations which will be used in the ITEM_LOC
         -- retail updates for the child items.  Note that after this block of code, the
         -- L_itemloc_table will only contain item/location combinations, where the location
         -- is known in RPM.  Any location within RMS that has not been communicated to RPM
         -- will be added to the L_itemloc_table later in this function.
         if L_location_table.COUNT > 0 AND L_item_table.COUNT > 0 then
            for b in L_item_table.FIRST..L_item_table.LAST LOOP
               for c in L_location_table.FIRST..L_location_table.LAST LOOP
                   L_itemloc_table.EXTEND;
                   L_itemloc_table(L_itemloc_table.COUNT) := OBJ_ITEMLOC_REC(L_item_table(b),
                                                                             L_location_table(c).location_id);
               end loop;
            end loop;
         end if;

         L_table := 'ITEM_LOC';
         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_ITEM_LOC',
                          'ITEM_LOC',
                          NULL);
         open C_LOCK_ITEM_LOC;

         SQL_LIB.SET_MARK('FETCH',
                          'C_LOCK_ITEM_LOC',
                          'ITEM_LOC',
                          NULL);
         fetch C_LOCK_ITEM_LOC BULK COLLECT into L_item_loc;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_ITEM_LOC',
                          'ITEM_LOC',
                          NULL);
         close C_LOCK_ITEM_LOC;

         if L_item_loc.COUNT > 0 then
            for d IN L_item_loc.FIRST..L_item_loc.LAST LOOP
               if L_standard_uom != L_item_loc(d).selling_uom then
                  if UOM_SQL.CONVERT(O_error_message,            -- error message
                                     L_to_value,                 -- to value
                                     L_standard_uom,             -- to uom
                                     1,                          -- from value
                                     L_item_loc(d).selling_uom,  -- from uom
                                     L_item_loc(d).item,         -- item
                                     NULL,                       -- supplier
                                     NULL) = FALSE then          -- origin country
                     return FALSE;
                  end if;

                  L_selling_unit_retail := I_unit_retail * L_to_value;
               else
                  L_selling_unit_retail := I_unit_retail;
               end If;

               if L_item_loc(d).multi_units is NOT NULL and
                  L_item_loc(d).multi_unit_retail is NOT NULL then

                  if UOM_SQL.CONVERT(O_error_message,                  -- error message
                                     L_to_value,                       -- to value
                                     L_item_loc(d).multi_selling_uom,  -- to uom
                                     L_item_loc(d).multi_unit_retail,  -- from value
                                     L_item_loc(d).selling_uom,        -- from uom
                                     L_item_loc(d).item,               -- item
                                     NULL,                             -- supplier
                                     NULL) = FALSE then                -- origin country
                     return FALSE;
                  end if;

                  if L_to_value <= 0 then
                     O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                           L_item_loc(d).selling_uom,
                                                           L_item_loc(d).multi_selling_uom,
                                                           NULL);
                     return FALSE;
                  end if;

                  L_retail := L_to_value / L_item_loc(d).multi_units;

                  if L_retail > L_selling_unit_retail then
                     O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS',
                                                           NULL,
                                                           NULL,
                                                           NULL);
                     return FALSE;
                  end if;

                  if L_to_value < L_selling_unit_retail then
                     O_error_message := SQL_LIB.CREATE_MSG('MULTI_RETAIL_LESS_SINGLE',
                                                           NULL,
                                                           NULL,
                                                           NULL);
                     return FALSE;
                  end if;
               end if;

               L_cnt := L_cnt + 1;
               L_rpm_selling_unit(L_cnt)  := L_selling_unit_retail;
               L_rpm_zone_group_id(L_cnt) := L_zone_group_id;
               L_rpm_zone_id(L_cnt)       := L_item_loc(d).loc;
               L_rpm_item(L_cnt)          := L_item_loc(d).item;

               if (L_child_table_index = 1 or L_prev_item != L_item_loc(d).item) then
                  L_child_item_table(L_child_table_index).child_item          := L_item_loc(d).item;
                  L_child_item_table(L_child_table_index).unit_retail         := I_unit_retail;
                  L_child_item_table(L_child_table_index).standard_uom        := L_standard_uom;
                  L_child_item_table(L_child_table_index).currency_code       := I_rpm_currency_code;
                  L_child_item_table(L_child_table_index).selling_unit_retail := L_selling_unit_retail;
                  L_child_item_table(L_child_table_index).selling_uom         := L_item_loc(d).selling_uom;
                  L_child_table_index := L_child_table_index + 1;
                  L_prev_item := L_item_loc(d).item;
               end if;
            end loop;

            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_LOC',NULL);
            FORALL i IN 1 .. L_cnt
               update item_loc
                  set unit_retail         = I_unit_retail,
                      selling_unit_retail = L_rpm_selling_unit(i),
                      regular_unit_retail = I_unit_retail
                where loc                 = L_rpm_zone_id(i)
                  and item                = L_rpm_item(i);

         else
            -- No ITEM_LOC records exist, so build the child item table, which is sent to RPM
            -- for the child item retail updates.
            if L_item_table.COUNT > 0 then
               FOR e IN L_item_table.FIRST..L_item_table.LAST LOOP
                   L_child_item_table(L_child_table_index).child_item          := L_item_table(e);
                   L_child_item_table(L_child_table_index).unit_retail         := I_unit_retail;
                   L_child_item_table(L_child_table_index).standard_uom        := L_standard_uom;
                   L_child_item_table(L_child_table_index).currency_code       := I_rpm_currency_code;
                   L_child_item_table(L_child_table_index).selling_unit_retail := I_unit_retail;
                   L_child_item_table(L_child_table_index).selling_uom         := L_standard_uom;
                   L_child_table_index := L_child_table_index + 1;
               END LOOP;
            end if;
         end if;

         if L_child_item_table.COUNT > 0 then
            if NOT PM_RETAIL_API_SQL.SET_CHILD_ITEM_PRICING_INFO(O_error_message,
                                                                 I_item_parent,
                                                                 I_rpm_zone_id,
                                                                 L_child_item_table) then
               return FALSE;
            end if;
         end if;

      else

         open C_LOCK_ITEM_MASTER;
         close C_LOCK_ITEM_MASTER;

         FORALL i IN 1 .. L_item_table.COUNT
            update item_master
               set curr_selling_unit_retail = I_unit_retail
             where item = L_item_table(i);

         SQL_LIB.SET_MARK('OPEN',
                          'C_LOCK_ITEM_LOC_RPM_N',
                          'ITEM_LOC',
                          NULL);
         open C_LOCK_ITEM_LOC_RPM_N;

         SQL_LIB.SET_MARK('FETCH',
                          'C_LOCK_ITEM_LOC_RPM_N',
                          'ITEM_LOC',
                          NULL);
         fetch C_LOCK_ITEM_LOC_RPM_N BULK COLLECT into L_item_loc_rpm_n;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_LOCK_ITEM_LOC_RPM_N',
                          'ITEM_LOC',
                          NULL);
         close C_LOCK_ITEM_LOC_RPM_N;

         if L_item_loc_rpm_n.COUNT > 0 then
            for d IN L_item_loc_rpm_n.FIRST..L_item_loc_rpm_n.LAST LOOP
               if L_standard_uom != L_item_loc_rpm_n(d).selling_uom then
                  if UOM_SQL.CONVERT(O_error_message,            -- error message
                                     L_to_value,                 -- to value
                                     L_standard_uom,             -- to uom
                                     1,                          -- from value
                                     L_item_loc_rpm_n(d).selling_uom,  -- from uom
                                     L_item_loc_rpm_n(d).item,         -- item
                                     NULL,                       -- supplier
                                     NULL) = FALSE then          -- origin country
                     return FALSE;
                  end if;

                  L_selling_unit_retail := I_unit_retail * L_to_value;
               else
                  L_selling_unit_retail := I_unit_retail;
               end If;

               if L_item_loc_rpm_n(d).multi_units is NOT NULL and
                  L_item_loc_rpm_n(d).multi_unit_retail is NOT NULL then

                  if UOM_SQL.CONVERT(O_error_message,                  -- error message
                                     L_to_value,                       -- to value
                                     L_item_loc_rpm_n(d).multi_selling_uom,  -- to uom
                                     L_item_loc_rpm_n(d).multi_unit_retail,  -- from value
                                     L_item_loc_rpm_n(d).selling_uom,        -- from uom
                                     L_item_loc_rpm_n(d).item,               -- item
                                     NULL,                             -- supplier
                                     NULL) = FALSE then                -- origin country
                     return FALSE;
                  end if;

                  if L_to_value <= 0 then
                     O_error_message := SQL_LIB.CREATE_MSG('NO_DIM_CONV',
                                                           L_item_loc_rpm_n(d).selling_uom,
                                                           L_item_loc_rpm_n(d).multi_selling_uom,
                                                           NULL);
                     return FALSE;
                  end if;

                  L_retail := L_to_value / L_item_loc_rpm_n(d).multi_units;

                  if L_retail > L_selling_unit_retail then
                     O_error_message := SQL_LIB.CREATE_MSG('MULTI_UNITS',
                                                           NULL,
                                                           NULL,
                                                           NULL);
                     return FALSE;
                  end if;

                  if L_to_value < L_selling_unit_retail then
                     O_error_message := SQL_LIB.CREATE_MSG('MULTI_RETAIL_LESS_SINGLE',
                                                           NULL,
                                                           NULL,
                                                           NULL);
                     return FALSE;
                  end if;
               end if;

               L_cnt := L_cnt + 1;
               L_selling_unit(L_cnt)     := L_selling_unit_retail;
               L_itm_loc(L_cnt)          := L_item_loc_rpm_n(d).loc;
               L_itm(L_cnt)              := L_item_loc_rpm_n(d).item;
            end loop;

            SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_LOC',NULL);
            FORALL i IN 1 .. L_cnt
               update item_loc
                  set unit_retail         = I_unit_retail,
                      selling_unit_retail = L_selling_unit(i),
                      regular_unit_retail = I_unit_retail
                where loc                 = L_itm_loc(i)
                  and item                = L_itm(i);

         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_program,
                                             NULL);

   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;

END MASS_UPDATE_CHILDREN;
------------------------------------------------------------------
FUNCTION MASS_UPDATE_CHILDREN(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_supplier        IN       SUPS.SUPPLIER%TYPE,
                              I_item            IN       ITEM_MASTER.ITEM_PARENT%TYPE,
                              I_vpn             IN       ITEM_SUPPLIER.VPN%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(40) := 'ITEM_CREATE_SQL.MASS_UPDATE_CHILDREN';
   L_table            VARCHAR2(20) := 'ITEM_SUPPLIER';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

   CURSOR C_LOCK_ITEM_SUPPLIER IS
      SELECT isupp.vpn
        FROM item_supplier isupp
       WHERE isupp.supplier=I_supplier
         AND isupp.item = I_item
  FOR UPDATE OF vpn NOWAIT;

BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_ITEM_SUPPLIER', 'ITEM_SUPPLIER', 'Supplier: '||TO_CHAR(I_supplier));
   OPEN  C_LOCK_ITEM_SUPPLIER;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_ITEM_SUPPLIER', 'ITEM_SUPPLIER', 'Supplier: '||TO_CHAR(I_supplier));
   CLOSE C_LOCK_ITEM_SUPPLIER;

   SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPPLIER','Item: '||TO_CHAR(I_item)||', Supplier: '||TO_CHAR(I_supplier));

   UPDATE item_supplier
      SET vpn                  = I_vpn,
          last_update_datetime = SYSDATE,
          last_update_id       = GET_USER
    WHERE supplier=I_supplier
      AND item = I_item;

   RETURN TRUE;

EXCEPTION

   WHEN RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             L_program,
                                             NULL);
      RETURN FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;

END MASS_UPDATE_CHILDREN;
------------------------------------------------------------------
FUNCTION TRAN_CHILDREN_LEVEL3 (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_item                   IN       ITEM_TEMP.ITEM%TYPE,
                               I_item_number_type       IN       ITEM_TEMP.ITEM_NUMBER_TYPE%TYPE,
                               I_item_level             IN       ITEM_TEMP.ITEM_LEVEL%TYPE,
                               I_item_desc              IN       ITEM_TEMP.ITEM_DESC%TYPE,
                               I_diff_1                 IN       ITEM_TEMP.DIFF_1%TYPE,
                               I_diff_2                 IN       ITEM_TEMP.DIFF_2%TYPE,
                               I_diff_3                 IN       ITEM_TEMP.DIFF_3%TYPE,
                               I_diff_4                 IN       ITEM_TEMP.DIFF_4%TYPE,
                               I_existing_item_parent   IN       ITEM_TEMP.EXISTING_ITEM_PARENT%TYPE,
                               I_auto_approve           IN       VARCHAR2,
                               I_status                 IN       ITEM_MASTER.STATUS%TYPE)
RETURN BOOLEAN IS
   L_program             VARCHAR2(80) := 'ITEM_CREATE_SQL.TRAN_CHILDREN_LEVEL3';
   L_item_approved       BOOLEAN;
   L_children_approved   BOOLEAN;
   L_tran_level          ITEM_MASTER.ITEM%TYPE;

   cursor C_GET_ITEM_TRAN_LEVEL is
      select tran_level
        from item_master
       where item=I_existing_item_parent;

BEGIN

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'ITEM_TEMP',
                    'ITEM: '||TO_CHAR(I_item));

   insert into item_temp (item,
                          item_number_type,
                          item_level,
                          item_desc,
                          diff_1,
                          diff_2,
                          diff_3,
                          diff_4,
                          existing_item_parent)
                  values (I_item,
                          I_item_number_type,
                          I_item_level,
                          I_item_desc,
                          I_diff_1,
                          I_diff_2,
                          I_diff_3,
                          I_diff_4,
                          I_existing_item_parent);

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ITEM_TRAN_LEVEL',
                    'ITEM_MASTER',
                    'Item: '||TO_CHAR(I_existing_item_parent));

   open  C_GET_ITEM_TRAN_LEVEL;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ITEM_TRAN_LEVEL',
                    'ITEM_MASTER',
                    'Item: '||TO_CHAR(I_existing_item_parent));

   fetch C_GET_ITEM_TRAN_LEVEL into L_tran_level;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ITEM_TRAN_LEVEL',
                    'ITEM_MASTER',
                    'Item: '||TO_CHAR(I_existing_item_parent));

   close C_GET_ITEM_TRAN_LEVEL;

   if I_item_level <= L_tran_level then
      if ITEM_CREATE_SQL.TRAN_CHILDREN(O_error_message,
                                       'Y',
                                       I_auto_approve) = FALSE then
         return FALSE;
      end if;
   else
      if ITEM_CREATE_SQL.INSERT_ITEM_MASTER(O_error_message,
                                            I_auto_approve) = FALSE then
         return FALSE;
      end if;
      ---
      if ITEM_CREATE_SQL.SUB_TRAN_INSERT_ITEM_COUNTRY(O_error_message,
                                                       I_existing_item_parent) = FALSE then
         return FALSE;
      end if;
      ---
      if ITEM_CREATE_SQL.INSERT_ITEM_SUPPLIER(O_error_message,
                                              I_item,
                                              I_existing_item_parent) = FALSE then
         return FALSE;
      end if;
      ---
      if I_auto_approve = 'Y' and I_status  = 'A' then
         --- Need to set to submitted status before approval will work
         if ITEM_APPROVAL_SQL.UPDATE_STATUS(O_error_message,
                                           'S',
                                           I_item)= FALSE then
            return FALSE;
         end if;

         if ITEM_APPROVAL_SQL.APPROVE(O_error_message,
                                      L_item_approved,
                                      L_children_approved,
                                      'N', -- don't process children
                                      I_item)= FALSE then
            return FALSE;
         end if;

         if L_item_approved then
            --- Item passed approval check so set status to A
            if ITEM_APPROVAL_SQL.UPDATE_STATUS(O_error_message,
                                               'A',
                                               I_item)= FALSE then
               return FALSE;
            end if;
         else
            --- Item failed approval check so set status to W
            if ITEM_APPROVAL_SQL.UPDATE_STATUS(O_error_message,
                                               'W',
                                               I_item)= FALSE then
               return FALSE;
            end if;
         end if;
      end if;
   end if;

   if DIFF_APPLY_SQL.CLEAR_TEMP_TABLE(O_error_message, 'ALL') =  FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      RETURN FALSE;

END TRAN_CHILDREN_LEVEL3;
------------------------------------------------------------------
FUNCTION INSERT_ITEM_COUNTRY(O_error_message   IN OUT   VARCHAR2,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_item_parent     IN       ITEM_MASTER.ITEM_PARENT%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(40)      := 'ITEM_CREATE_SQL.INSERT_ITEM_COUNTRY';
   L_user               VARCHAR2(30)      := GET_USER;

BEGIN
   if I_item is NOT NULL AND I_item_parent is NOT NULL then

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY','ITEM: '||I_item);
      insert into item_country(item,
                               country_id)
                        select I_item,
                               country_id
                          from item_country
                         where item = I_item_parent;

      SQL_LIB.SET_MARK('INSERT',NULL,'ITEM_COUNTRY_L10N_EXT','ITEM: '||I_item);
      insert into item_country_l10n_ext(item,
                                        country_id,
                                        l10n_country_id,
                                        group_id,
                                        varchar2_1,
                                        varchar2_2,
                                        varchar2_3,
                                        varchar2_4,
                                        varchar2_5,
                                        varchar2_6,
                                        varchar2_7,
                                        varchar2_8,
                                        varchar2_9,
                                        varchar2_10,
                                        number_11,
                                        number_12,
                                        number_13,
                                        number_14,
                                        number_15,
                                        number_16,
                                        number_17,
                                        number_18,
                                        number_19,
                                        number_20,
                                        date_21,
                                        date_22)
                                 select I_item,
                                        country_id,
                                        l10n_country_id,
                                        group_id,
                                        varchar2_1,
                                        varchar2_2,
                                        varchar2_3,
                                        varchar2_4,
                                        varchar2_5,
                                        varchar2_6,
                                        varchar2_7,
                                        varchar2_8,
                                        varchar2_9,
                                        varchar2_10,
                                        number_11,
                                        number_12,
                                        number_13,
                                        number_14,
                                        number_15,
                                        number_16,
                                        number_17,
                                        number_18,
                                        number_19,
                                        number_20,
                                        date_21,
                                        date_22
                                   from item_country_l10n_ext
                                  where item = I_item_parent;
   end if;
   ---
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;

END INSERT_ITEM_COUNTRY;
-------------------------------------------------------------------
END ITEM_CREATE_SQL;
/