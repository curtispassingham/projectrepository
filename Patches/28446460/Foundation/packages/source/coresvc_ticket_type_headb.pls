CREATE OR REPLACE PACKAGE BODY CORESVC_TICKET_TYPE_HEAD as

   cursor C_SVC_TICKET_TYPE_HEAD(I_process_id NUMBER,
                                 I_chunk_id   NUMBER) is
      select pk_ticket_type_head.rowid  as pk_ticket_type_head_rid,
             st.rowid                   as st_rid,
             UPPER(st.ticket_type_id)   as ticket_type_id,
             st.ticket_type_desc,
             st.sel_ind,
             st.filter_org_id,
             st.filter_merch_id,
             st.filter_merch_id_class,
             st.filter_merch_id_subclass,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)           as action,
             st.process$status
        from svc_ticket_type_head st,
             ticket_type_head pk_ticket_type_head
       where st.process_id      = I_process_id
         and st.chunk_id        = I_chunk_id
         and UPPER(st.ticket_type_id)  = pk_ticket_type_head.ticket_type_id (+);

   cursor C_SVC_TICKET_TYPE_DETAIL(I_process_id NUMBER,
                                   I_chunk_id NUMBER) is
      select pk_ticket_type_detail.rowid  as pk_ticket_type_detail_rid,
             st.rowid                     as st_rid,
             ttd_tth_fk.rowid             as ttd_tth_fk_rid,
             UPPER(st.ticket_type_id) as ticket_type_id ,
             st.seq_no,
             st.ticket_item_id,
             st.uda_id,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)             as action,
             st.process$status
        from svc_ticket_type_detail st,
             ticket_type_detail     pk_ticket_type_detail,
             ticket_type_head       ttd_tth_fk
       where st.process_id      = I_process_id
         and st.chunk_id        = I_chunk_id
         and st.seq_no          = pk_ticket_type_detail.seq_no (+)
         and UPPER(st.ticket_type_id)  = UPPER(pk_ticket_type_detail.ticket_type_id (+))
         and UPPER(st.ticket_type_id)  = Upper(ttd_tth_fk.ticket_type_id (+))
         and st.process$status  <> 'P';

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab      errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab  s9t_errors_tab_typ;

   LP_merch_filter_level        SYSTEM_OPTIONS.TICKET_TYPE_MERCH_LEVEL_CODE%TYPE;
   LP_org_filter_level          SYSTEM_OPTIONS.TICKET_TYPE_ORG_LEVEL_CODE%TYPE;

   Type TICKET_TYPE_HEAD_TAB IS TABLE OF TICKET_TYPE_HEAD_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
-----------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
-----------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-----------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
-----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets                    s9t_pkg.names_map_typ;
   TICKET_TYPE_HEAD_cols       s9t_pkg.names_map_typ;
   TICKET_TYPE_DETAIL_cols     s9t_pkg.names_map_typ;
   TICKET_TYPE_HEAD_TL_cols    s9t_pkg.names_map_typ;
BEGIN
   L_sheets                       := s9t_pkg.get_sheet_names(I_file_id);
   TICKET_TYPE_HEAD_cols          := s9t_pkg.get_col_names(I_file_id,TICKET_TYPE_HEAD_sheet);
   TICKET_TYPE_HEAD$Action        := TICKET_TYPE_HEAD_cols('ACTION');
   TICKET_TYPE_HEAD$TKET_ID       := TICKET_TYPE_HEAD_cols('TICKET_TYPE_ID');
   TICKET_TYPE_HEAD$TKET_DESC     := TICKET_TYPE_HEAD_cols('TICKET_TYPE_DESC');
   TICKET_TYPE_HEAD$SEL_IND       := TICKET_TYPE_HEAD_cols('SEL_IND');
   TICKET_TYPE_HEAD$ORG_ID        := TICKET_TYPE_HEAD_cols('FILTER_ORG_ID');
   TICKET_TYPE_HEAD$MERCH_ID      := TICKET_TYPE_HEAD_cols('FILTER_MERCH_ID');
   TICKET_TYPE_HEAD$MERCH_CL      := TICKET_TYPE_HEAD_cols('FILTER_MERCH_ID_CLASS');
   TICKET_TYPE_HEAD$MERCH_SC      := TICKET_TYPE_HEAD_cols('FILTER_MERCH_ID_SUBCLASS');

   TICKET_TYPE_DETAIL_cols        := s9t_pkg.get_col_names(I_file_id,TICKET_TYPE_DETAIL_sheet);
   TICKET_TYPE_DETAIL$Action      := TICKET_TYPE_DETAIL_cols('ACTION');
   TICKET_TYPE_DETAIL$TKET_ID     := TICKET_TYPE_DETAIL_cols('TICKET_TYPE_ID');
   TICKET_TYPE_DETAIL$SEQ_NO      := TICKET_TYPE_DETAIL_cols('SEQ_NO');
   TICKET_TYPE_DETAIL$TKET_ITM_ID := TICKET_TYPE_DETAIL_cols('TICKET_ITEM_ID');
   TICKET_TYPE_DETAIL$UDA_ID      := TICKET_TYPE_DETAIL_cols('UDA_ID');

   TICKET_TYPE_HEAD_TL_cols          := s9t_pkg.get_col_names(I_file_id,TICKET_TYPE_HEAD_TL_SHEET);
   TICKET_TYPE_HEAD_TL$Action        := TICKET_TYPE_HEAD_TL_cols('ACTION');
   TICKET_TYPE_HEAD_TL$LANG          := TICKET_TYPE_HEAD_TL_cols('LANG');
   TICKET_TYPE_HEAD_TL$TKET_ID       := TICKET_TYPE_HEAD_TL_cols('TICKET_TYPE_ID');
   TICKET_TYPE_HEAD_TL$TKET_DESC     := TICKET_TYPE_HEAD_TL_cols('TICKET_TYPE_DESC');

END POPULATE_NAMES;
-----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_TICKET_TYPE_HEAD( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = TICKET_TYPE_HEAD_sheet )
   select s9t_row(s9t_cells(CORESVC_TICKET_TYPE_HEAD.action_mod,
                            ticket_type_id,
                            ticket_type_desc,
                            sel_ind,
                            filter_org_id,
                            filter_merch_id,
                            filter_merch_id_class,
                            filter_merch_id_subclass ))
     from ticket_type_head ;
END POPULATE_TICKET_TYPE_HEAD;
-----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_TICKET_TYPE_HEAD_TL( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = TICKET_TYPE_HEAD_TL_SHEET )
   select s9t_row(s9t_cells(CORESVC_TICKET_TYPE_HEAD.action_mod,
                            lang,
                            ticket_type_id,
                            ticket_type_desc))
     from ticket_type_head_tl ;
END POPULATE_TICKET_TYPE_HEAD_TL;
-----------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_TICKET_TYPE_DETAIL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = TICKET_TYPE_DETAIL_sheet )
   select s9t_row(s9t_cells(NULL,
                            ticket_type_id,
                            seq_no,
                            ticket_item_id,
                            uda_id ))
     from ticket_type_detail ;
END POPULATE_TICKET_TYPE_DETAIL;
-----------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id IN OUT NUMBER) IS
   L_file       s9t_file;
   L_file_name  s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(TICKET_TYPE_HEAD_sheet);
   L_file.sheets(l_file.get_sheet_index(TICKET_TYPE_HEAD_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                             ,'TICKET_TYPE_ID'
                                                                                             ,'TICKET_TYPE_DESC'
                                                                                             ,'SEL_IND'
                                                                                             ,'FILTER_ORG_ID'
                                                                                             ,'FILTER_MERCH_ID'
                                                                                             ,'FILTER_MERCH_ID_CLASS'
                                                                                             ,'FILTER_MERCH_ID_SUBCLASS' );
   L_file.add_sheet(TICKET_TYPE_DETAIL_sheet);
   L_file.sheets(l_file.get_sheet_index(TICKET_TYPE_DETAIL_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                               ,'TICKET_TYPE_ID'
                                                                                               ,'SEQ_NO'
                                                                                               ,'TICKET_ITEM_ID'
                                                                                               ,'UDA_ID' );

   L_file.add_sheet(TICKET_TYPE_HEAD_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(TICKET_TYPE_HEAD_TL_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                                ,'LANG'
                                                                                                ,'TICKET_TYPE_ID'
                                                                                                ,'TICKET_TYPE_DESC' );
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
-----------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT s9t_folder.file_id%TYPE,
                     I_template_only_ind IN     CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program  VARCHAR2(64):='CORESVC_TICKET_TYPE_HEAD.CREATE_S9T';
   L_file     s9t_file;
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE   then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_TICKET_TYPE_HEAD(O_file_id);
      POPULATE_TICKET_TYPE_DETAIL(O_file_id);
      POPULATE_TICKET_TYPE_HEAD_TL(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-----------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TICKET_TYPE_HEAD(I_file_id    IN s9t_folder.file_id%TYPE,
                                       I_process_id IN SVC_TICKET_TYPE_HEAD.process_id%TYPE) IS
   TYPE svc_TICKET_TYPE_HEAD_col_typ IS TABLE OF SVC_TICKET_TYPE_HEAD%ROWTYPE;
   L_temp_rec               SVC_TICKET_TYPE_HEAD%ROWTYPE;
   svc_TICKET_TYPE_HEAD_col svc_TICKET_TYPE_HEAD_col_typ := NEW svc_TICKET_TYPE_HEAD_col_typ();
   L_process_id             SVC_TICKET_TYPE_HEAD.process_id%TYPE;
   L_error                  BOOLEAN                      := FALSE;
   L_default_rec            SVC_TICKET_TYPE_HEAD%ROWTYPE;
   cursor C_MANDATORY_IND is
      select FILTER_MERCH_ID_SUBCLASS_mi,
             FILTER_MERCH_ID_CLASS_mi,
             FILTER_MERCH_ID_mi,
             FILTER_ORG_ID_mi,
             SEL_IND_mi,
             TICKET_TYPE_DESC_mi,
             TICKET_TYPE_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key           = template_key
                 and wksht_key              = 'TICKET_TYPE_HEAD'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('FILTER_MERCH_ID_SUBCLASS' as FILTER_MERCH_ID_SUBCLASS,
                                            'FILTER_MERCH_ID_CLASS'    as FILTER_MERCH_ID_CLASS,
                                            'FILTER_MERCH_ID'          as FILTER_MERCH_ID,
                                            'FILTER_ORG_ID'            as FILTER_ORG_ID,
                                            'SEL_IND'                  as SEL_IND,
                                            'TICKET_TYPE_DESC'         as TICKET_TYPE_DESC,
                                            'TICKET_TYPE_ID'           as TICKET_TYPE_ID,
                                                                  null as dummy));
   l_mi_rec   c_mandatory_ind%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA     EXCEPTION_INIT(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_TICKET_TYPE_HEAD';
   L_pk_columns    VARCHAR2(255)  := 'Ticket Type';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select  FILTER_MERCH_ID_SUBCLASS_dv,
                       FILTER_MERCH_ID_CLASS_dv,
                       FILTER_MERCH_ID_dv,
                       FILTER_ORG_ID_dv,
                       SEL_IND_dv,
                       TICKET_TYPE_DESC_dv,
                       TICKET_TYPE_ID_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key       = template_key
                          and wksht_key          = 'TICKET_TYPE_HEAD'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('FILTER_MERCH_ID_SUBCLASS' as FILTER_MERCH_ID_SUBCLASS,
                                                     'FILTER_MERCH_ID_CLASS'    as FILTER_MERCH_ID_CLASS,
                                                     'FILTER_MERCH_ID'          as FILTER_MERCH_ID,
                                                     'FILTER_ORG_ID'            as FILTER_ORG_ID,
                                                     'SEL_IND'                  as SEL_IND,
                                                     'TICKET_TYPE_DESC'         as TICKET_TYPE_DESC,
                                                     'TICKET_TYPE_ID'           as TICKET_TYPE_ID,
                                                                           NULL as dummy)))
   LOOP
      BEGIN
         L_default_rec.FILTER_MERCH_ID_SUBCLASS := rec.FILTER_MERCH_ID_SUBCLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_HEAD ' ,
                            NULL,
                           'FILTER_MERCH_ID_SUBCLASS ',
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FILTER_MERCH_ID_CLASS := rec.FILTER_MERCH_ID_CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_HEAD ' ,
                            NULL,
                           'FILTER_MERCH_ID_CLASS ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FILTER_MERCH_ID := rec.FILTER_MERCH_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_HEAD ' ,
                            NULL,
                           'FILTER_MERCH_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FILTER_ORG_ID := rec.FILTER_ORG_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_HEAD ' ,
                            NULL,
                           'FILTER_ORG_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SEL_IND := rec.SEL_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_HEAD ' ,
                            NULL,
                           'SEL_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TICKET_TYPE_DESC := rec.TICKET_TYPE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_HEAD ' ,
                            NULL,
                           'TICKET_TYPE_DESC ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TICKET_TYPE_ID := rec.TICKET_TYPE_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_HEAD ' ,
                            NULL,
                           'TICKET_TYPE_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(TICKET_TYPE_HEAD$Action)            as Action,
          r.get_cell(TICKET_TYPE_HEAD$MERCH_SC)          as FILTER_MERCH_ID_SUBCLASS,
          r.get_cell(TICKET_TYPE_HEAD$MERCH_CL)          as FILTER_MERCH_ID_CLASS,
          r.get_cell(TICKET_TYPE_HEAD$MERCH_ID)          as FILTER_MERCH_ID,
          r.get_cell(TICKET_TYPE_HEAD$ORG_ID)            as FILTER_ORG_ID,
          r.get_cell(TICKET_TYPE_HEAD$SEL_IND)           as SEL_IND,
          r.get_cell(TICKET_TYPE_HEAD$TKET_DESC)         as TICKET_TYPE_DESC,
          UPPER(r.get_cell(TICKET_TYPE_HEAD$TKET_ID))    as TICKET_TYPE_ID,
          r.get_row_seq()                                as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(TICKET_TYPE_HEAD_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID_SUBCLASS := rec.FILTER_MERCH_ID_SUBCLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID_SUBCLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID_CLASS := rec.FILTER_MERCH_ID_CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID_CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID := rec.FILTER_MERCH_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_ORG_ID := rec.FILTER_ORG_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            rec.row_seq,
                            'FILTER_ORG_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SEL_IND := rec.SEL_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            rec.row_seq,
                            'SEL_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TICKET_TYPE_DESC := rec.TICKET_TYPE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            rec.row_seq,
                            'TICKET_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TICKET_TYPE_ID := rec.TICKET_TYPE_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            rec.row_seq,
                            'TICKET_TYPE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TICKET_TYPE_HEAD.action_new then
         L_temp_rec.FILTER_MERCH_ID_SUBCLASS := NVL(L_temp_rec.FILTER_MERCH_ID_SUBCLASS,
                                                    L_default_rec.FILTER_MERCH_ID_SUBCLASS);
         L_temp_rec.FILTER_MERCH_ID_CLASS    := NVL(L_temp_rec.FILTER_MERCH_ID_CLASS,
                                                    L_default_rec.FILTER_MERCH_ID_CLASS);
         L_temp_rec.FILTER_MERCH_ID          := NVL(L_temp_rec.FILTER_MERCH_ID,
                                                    L_default_rec.FILTER_MERCH_ID);
         L_temp_rec.FILTER_ORG_ID            := NVL(L_temp_rec.FILTER_ORG_ID,
                                                    L_default_rec.FILTER_ORG_ID);
         L_temp_rec.SEL_IND                  := NVL(L_temp_rec.SEL_IND,
                                                    L_default_rec.SEL_IND);
         L_temp_rec.TICKET_TYPE_DESC         := NVL(L_temp_rec.TICKET_TYPE_DESC,
                                                    L_default_rec.TICKET_TYPE_DESC);
         L_temp_rec.TICKET_TYPE_ID           := NVL(L_temp_rec.TICKET_TYPE_ID,
                                                    L_default_rec.TICKET_TYPE_ID);
      end if;
      if not ( L_temp_rec.TICKET_TYPE_ID is NOT NULL and
               1 = 1 )   then
         WRITE_S9T_ERROR( I_file_id,
                          TICKET_TYPE_HEAD_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_TICKET_TYPE_HEAD_col.extend();
         svc_TICKET_TYPE_HEAD_col(svc_TICKET_TYPE_HEAD_col.COUNT()):=l_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..svc_TICKET_TYPE_HEAD_col.COUNT SAVE EXCEPTIONS
      merge into SVC_TICKET_TYPE_HEAD st
      using(select
                  (case
                   when l_mi_rec.FILTER_MERCH_ID_SUBCLASS_mi    = 'N'
                    and svc_TICKET_TYPE_HEAD_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.FILTER_MERCH_ID_SUBCLASS IS NULL
                   then mt.FILTER_MERCH_ID_SUBCLASS
                   else s1.FILTER_MERCH_ID_SUBCLASS
                   end) AS FILTER_MERCH_ID_SUBCLASS,
                  (case
                   when l_mi_rec.FILTER_MERCH_ID_CLASS_mi    = 'N'
                    and svc_TICKET_TYPE_HEAD_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.FILTER_MERCH_ID_CLASS IS NULL
                   then mt.FILTER_MERCH_ID_CLASS
                   else s1.FILTER_MERCH_ID_CLASS
                   end) AS FILTER_MERCH_ID_CLASS,
                  (case
                   when l_mi_rec.FILTER_MERCH_ID_mi    = 'N'
                    and svc_TICKET_TYPE_HEAD_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.FILTER_MERCH_ID IS NULL
                   then mt.FILTER_MERCH_ID
                   else s1.FILTER_MERCH_ID
                   end) AS FILTER_MERCH_ID,
                  (case
                   when l_mi_rec.FILTER_ORG_ID_mi    = 'N'
                    and svc_TICKET_TYPE_HEAD_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.FILTER_ORG_ID IS NULL
                   then mt.FILTER_ORG_ID
                   else s1.FILTER_ORG_ID
                   end) AS FILTER_ORG_ID,
                  (case
                   when l_mi_rec.SEL_IND_mi    = 'N'
                    and svc_TICKET_TYPE_HEAD_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.SEL_IND IS NULL
                   then mt.SEL_IND
                   else s1.SEL_IND
                   end) AS SEL_IND,
                  (case
                   when l_mi_rec.TICKET_TYPE_DESC_mi    = 'N'
                    and svc_TICKET_TYPE_HEAD_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.TICKET_TYPE_DESC IS NULL
                   then mt.TICKET_TYPE_DESC
                   else s1.TICKET_TYPE_DESC
                   end) AS TICKET_TYPE_DESC,
                  (case
                   when l_mi_rec.TICKET_TYPE_ID_mi    = 'N'
                    and svc_TICKET_TYPE_HEAD_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.TICKET_TYPE_ID IS NULL
                   then mt.TICKET_TYPE_ID
                   else s1.TICKET_TYPE_ID
                   end) AS TICKET_TYPE_ID,
                  null as dummy
              from (select
                          svc_TICKET_TYPE_HEAD_col(i).FILTER_MERCH_ID_SUBCLASS AS FILTER_MERCH_ID_SUBCLASS,
                          svc_TICKET_TYPE_HEAD_col(i).FILTER_MERCH_ID_CLASS AS FILTER_MERCH_ID_CLASS,
                          svc_TICKET_TYPE_HEAD_col(i).FILTER_MERCH_ID AS FILTER_MERCH_ID,
                          svc_TICKET_TYPE_HEAD_col(i).FILTER_ORG_ID AS FILTER_ORG_ID,
                          svc_TICKET_TYPE_HEAD_col(i).SEL_IND AS SEL_IND,
                          svc_TICKET_TYPE_HEAD_col(i).TICKET_TYPE_DESC AS TICKET_TYPE_DESC,
                          svc_TICKET_TYPE_HEAD_col(i).TICKET_TYPE_ID AS TICKET_TYPE_ID,
                          null as dummy
                      from dual ) s1,
            ticket_type_head mt
             where
                  mt.TICKET_TYPE_ID (+)     = s1.TICKET_TYPE_ID   and
                  1 = 1 )sq
                on (
                    st.TICKET_TYPE_ID      = sq.TICKET_TYPE_ID and
                    svc_TICKET_TYPE_HEAD_col(i).ACTION IN (CORESVC_TICKET_TYPE_HEAD.action_mod,CORESVC_TICKET_TYPE_HEAD.action_del))
      when matched then
      update
         set process_id                 = svc_TICKET_TYPE_HEAD_col(i).process_id ,
             chunk_id                   = svc_TICKET_TYPE_HEAD_col(i).chunk_id ,
             row_seq                    = svc_TICKET_TYPE_HEAD_col(i).row_seq ,
             action                     = svc_TICKET_TYPE_HEAD_col(i).action ,
             process$status             = svc_TICKET_TYPE_HEAD_col(i).process$status ,
             ticket_type_desc           = sq.ticket_type_desc ,
             filter_merch_id            = sq.filter_merch_id ,
             filter_merch_id_subclass   = sq.filter_merch_id_subclass ,
             filter_org_id              = sq.filter_org_id ,
             sel_ind                    = sq.sel_ind ,
             filter_merch_id_class      = sq.filter_merch_id_class ,
             create_id                  = svc_TICKET_TYPE_HEAD_col(i).create_id ,
             create_datetime            = svc_TICKET_TYPE_HEAD_col(i).create_datetime ,
             last_upd_id                = svc_TICKET_TYPE_HEAD_col(i).last_upd_id ,
             last_upd_datetime          = svc_TICKET_TYPE_HEAD_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             filter_merch_id_subclass ,
             filter_merch_id_class ,
             filter_merch_id ,
             filter_org_id ,
             sel_ind ,
             ticket_type_desc ,
             ticket_type_id ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_TICKET_TYPE_HEAD_col(i).process_id ,
             svc_TICKET_TYPE_HEAD_col(i).chunk_id ,
             svc_TICKET_TYPE_HEAD_col(i).row_seq ,
             svc_TICKET_TYPE_HEAD_col(i).action ,
             svc_TICKET_TYPE_HEAD_col(i).process$status ,
             sq.filter_merch_id_subclass ,
             sq.filter_merch_id_class ,
             sq.filter_merch_id ,
             sq.filter_org_id ,
             sq.sel_ind ,
             sq.ticket_type_desc ,
             sq.ticket_type_id ,
             svc_TICKET_TYPE_HEAD_col(i).create_id ,
             svc_TICKET_TYPE_HEAD_col(i).create_datetime ,
             svc_TICKET_TYPE_HEAD_col(i).last_upd_id ,
             svc_TICKET_TYPE_HEAD_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            TICKET_TYPE_HEAD_sheet,
                            svc_TICKET_TYPE_HEAD_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TICKET_TYPE_HEAD;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TTH_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_TICKET_TYPE_HEAD_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_TTH_TL_COL_TYP IS TABLE OF SVC_TICKET_TYPE_HEAD_TL%ROWTYPE;
   L_temp_rec                    SVC_TICKET_TYPE_HEAD_TL%ROWTYPE;
   SVC_TICKET_TYPE_HEAD_TL_COL   SVC_TTH_TL_COL_TYP := NEW SVC_TTH_TL_COL_TYP();
   L_process_id                  SVC_TICKET_TYPE_HEAD_TL.PROCESS_ID%TYPE;
   L_error                       BOOLEAN := FALSE;
   L_default_rec                 SVC_TICKET_TYPE_HEAD_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select ticket_type_desc_mi,
             ticket_type_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'TICKET_TYPE_HEAD_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('TICKET_TYPE_DESC' as ticket_type_desc,
                                       'TICKET_TYPE_ID'   as ticket_type_id,
                                       'LANG'             as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_TICKET_TYPE_HEAD_TL';
   L_pk_columns    VARCHAR2(255)  := 'Ticket Type ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select ticket_type_desc_dv,
                      ticket_type_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'TICKET_TYPE_HEAD_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('TICKET_TYPE_DESC' as ticket_type_desc,
                                                'TICKET_TYPE_ID'   as ticket_type_id,
                                                'LANG'             as lang)))
   LOOP
      BEGIN
         L_default_rec.ticket_type_desc := rec.ticket_type_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_TL_SHEET ,
                            NULL,
                           'TICKET_TYPE_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ticket_type_id := rec.ticket_type_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           TICKET_TYPE_HEAD_TL_SHEET ,
                            NULL,
                           'TICKET_TYPE_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(ticket_type_head_tl$action))    as action,
                      r.get_cell(ticket_type_head_tl$tket_desc)        as ticket_type_desc,
                      UPPER(r.get_cell(ticket_type_head_tl$tket_id))   as ticket_type_id,
                      r.get_cell(ticket_type_head_tl$lang)             as lang,
                      r.get_row_seq()                                  as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(TICKET_TYPE_HEAD_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            ticket_type_head_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ticket_type_desc := rec.ticket_type_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_TL_SHEET,
                            rec.row_seq,
                            'TICKET_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ticket_type_id := rec.ticket_type_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_TL_SHEET,
                            rec.row_seq,
                            'TICKET_TYPE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_HEAD_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TICKET_TYPE_HEAD.action_new then
         L_temp_rec.ticket_type_desc := NVL( L_temp_rec.ticket_type_desc,L_default_rec.ticket_type_desc);
         L_temp_rec.ticket_type_id   := NVL( L_temp_rec.ticket_type_id,L_default_rec.ticket_type_id);
         L_temp_rec.lang             := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.ticket_type_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         TICKET_TYPE_HEAD_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_ticket_type_head_tl_col.extend();
         svc_ticket_type_head_tl_col(svc_ticket_type_head_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_ticket_type_head_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_ticket_type_head_TL st
      using(select
                  (case
                   when l_mi_rec.ticket_type_desc_mi = 'N'
                    and svc_ticket_type_head_tl_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.ticket_type_desc IS NULL then
                        mt.ticket_type_desc
                   else s1.ticket_type_desc
                   end) as ticket_type_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_ticket_type_head_tl_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.ticket_type_id_mi = 'N'
                    and svc_ticket_type_head_tl_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.ticket_type_id IS NULL then
                        mt.ticket_type_id
                   else s1.ticket_type_id
                   end) as ticket_type_id
              from (select svc_ticket_type_head_tl_col(i).ticket_type_desc as ticket_type_desc,
                           svc_ticket_type_head_tl_col(i).ticket_type_id        as ticket_type_id,
                           svc_ticket_type_head_tl_col(i).lang              as lang
                      from dual) s1,
                   ticket_type_head_TL mt
             where mt.ticket_type_id (+) = s1.ticket_type_id
               and mt.lang (+)       = s1.lang) sq
                on (st.ticket_type_id = sq.ticket_type_id and
                    st.lang = sq.lang and
                    svc_ticket_type_head_tl_col(i).ACTION IN (CORESVC_TICKET_TYPE_HEAD.action_mod,CORESVC_TICKET_TYPE_HEAD.action_del))
      when matched then
      update
         set process_id        = svc_ticket_type_head_tl_col(i).process_id ,
             chunk_id          = svc_ticket_type_head_tl_col(i).chunk_id ,
             row_seq           = svc_ticket_type_head_tl_col(i).row_seq ,
             action            = svc_ticket_type_head_tl_col(i).action ,
             process$status    = svc_ticket_type_head_tl_col(i).process$status ,
             ticket_type_desc = sq.ticket_type_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             ticket_type_desc ,
             ticket_type_id ,
             lang)
      values(svc_ticket_type_head_tl_col(i).process_id ,
             svc_ticket_type_head_tl_col(i).chunk_id ,
             svc_ticket_type_head_tl_col(i).row_seq ,
             svc_ticket_type_head_tl_col(i).action ,
             svc_ticket_type_head_tl_col(i).process$status ,
             sq.ticket_type_desc ,
             sq.ticket_type_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            TICKET_TYPE_HEAD_TL_SHEET,
                            svc_ticket_type_head_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TTH_TL;
-----------------------------------------------------------------------------------------------------
--ADDED
PROCEDURE PROCESS_S9T_TICKET_TYPE_DETAIL(I_file_id    IN s9t_folder.file_id%TYPE,
                                         I_process_id IN SVC_TICKET_TYPE_DETAIL.process_id%TYPE) IS
   TYPE svc_TICKET_TYPE_DETAIL_col_typ IS TABLE OF SVC_TICKET_TYPE_DETAIL%ROWTYPE;
   L_temp_rec                 SVC_TICKET_TYPE_DETAIL%ROWTYPE;
   svc_TICKET_TYPE_DETAIL_col svc_TICKET_TYPE_DETAIL_col_typ := NEW svc_TICKET_TYPE_DETAIL_col_typ();
   L_process_id               SVC_TICKET_TYPE_DETAIL.process_id%TYPE;
   L_error                    BOOLEAN                        := FALSE;
   L_default_rec              SVC_TICKET_TYPE_DETAIL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select
             TICKET_TYPE_ID_mi,
             SEQ_NO_mi,
             TICKET_ITEM_ID_mi,
             UDA_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'TICKET_TYPE_DETAIL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN (
                                         'TICKET_TYPE_ID' as TICKET_TYPE_ID,
                                         'SEQ_NO'         as SEQ_NO,
                                         'TICKET_ITEM_ID' as TICKET_ITEM_ID,
                                         'UDA_ID'         as UDA_ID,
                                            null          as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_table         VARCHAR2(30)   := 'SVC_TICKET_TYPE_DETAIL';
      L_pk_columns    VARCHAR2(255)  := 'Ticket Type';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select
                       TICKET_TYPE_ID_dv,
                       SEQ_NO_dv,
                       TICKET_ITEM_ID_dv,
                       UDA_ID_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                            = template_key
                          and wksht_key                               = 'TICKET_TYPE_DETAIL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('TICKET_TYPE_ID' as TICKET_TYPE_ID,
                                                     'SEQ_NO'         as SEQ_NO,
                                                     'TICKET_ITEM_ID' as TICKET_ITEM_ID,
                                                     'UDA_ID'         as UDA_ID,
                                                      NULL            as dummy)))
   LOOP
      BEGIN
         L_default_rec.TICKET_TYPE_ID := rec.TICKET_TYPE_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_DETAIL ' ,
                            NULL,
                           'TICKET_TYPE_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SEQ_NO := rec.SEQ_NO_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_DETAIL ' ,
                            NULL,
                           'SEQ_NO ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TICKET_ITEM_ID := rec.TICKET_ITEM_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_DETAIL ' ,
                            NULL,
                           'TICKET_ITEM_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UDA_ID := rec.UDA_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TICKET_TYPE_DETAIL ' ,
                            NULL,
                           'UDA_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(TICKET_TYPE_DETAIL$Action)             as Action,
          UPPER(r.get_cell(TICKET_TYPE_DETAIL$TKET_ID))     as TICKET_TYPE_ID,
          r.get_cell(TICKET_TYPE_DETAIL$SEQ_NO)             as SEQ_NO,
          r.get_cell(TICKET_TYPE_DETAIL$TKET_ITM_ID)        as TICKET_ITEM_ID,
          r.get_cell(TICKET_TYPE_DETAIL$UDA_ID)             as UDA_ID,
          r.get_row_seq()                                   as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id    = I_file_id
       and ss.sheet_name = sheet_name_trans(TICKET_TYPE_DETAIL_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_DETAIL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TICKET_TYPE_ID := rec.TICKET_TYPE_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             TICKET_TYPE_DETAIL_sheet,
                             rec.row_seq,
                            'TICKET_TYPE_ID',
                             SQLCODE,
                             SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SEQ_NO := rec.SEQ_NO;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_DETAIL_sheet,
                            rec.row_seq,
                            'SEQ_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TICKET_ITEM_ID := rec.TICKET_ITEM_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TICKET_TYPE_DETAIL_sheet,
                            rec.row_seq,
                            'TICKET_ITEM_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_ID := rec.UDA_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             TICKET_TYPE_DETAIL_sheet,
                             rec.row_seq,
                            'UDA_ID',
                             SQLCODE,
                             SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TICKET_TYPE_HEAD.action_new then
         L_temp_rec.TICKET_TYPE_ID := NVL(L_temp_rec.TICKET_TYPE_ID,
                                          L_default_rec.TICKET_TYPE_ID);
         L_temp_rec.SEQ_NO         := NVL(L_temp_rec.SEQ_NO,
                                          L_default_rec.SEQ_NO);
         L_temp_rec.TICKET_ITEM_ID := NVL(L_temp_rec.TICKET_ITEM_ID,
                                          L_default_rec.TICKET_ITEM_ID);
         L_temp_rec.UDA_ID         := NVL(L_temp_rec.UDA_ID,
                                          L_default_rec.UDA_ID);
      end if;

      /*Code added as for action new the seq_no can be left as NUll or can be filled*/
      if L_temp_rec.action = action_new
         and L_temp_rec.TICKET_TYPE_ID is NULL   then
          WRITE_S9T_ERROR( I_file_id,
                          TICKET_TYPE_DETAIL_sheet,
                          rec.row_seq,

                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
        L_error := TRUE;
      elsif L_temp_rec.action IN (action_del,action_mod)
            and (L_temp_rec.SEQ_NO is NULL
             or  L_temp_rec.TICKET_TYPE_ID is NULL)   then
            L_pk_columns:='Sequence Number,Ticket Type';
         WRITE_S9T_ERROR( I_file_id,
                          TICKET_TYPE_DETAIL_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
            L_error := TRUE;
      end if;

      if NOT L_error then
         svc_TICKET_TYPE_DETAIL_col.extend();
         svc_TICKET_TYPE_DETAIL_col(svc_TICKET_TYPE_DETAIL_col.COUNT()):=L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..svc_TICKET_TYPE_DETAIL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_TICKET_TYPE_DETAIL st
      using(select
                  (case
                   when l_mi_rec.TICKET_TYPE_ID_mi    = 'N'
                    and svc_TICKET_TYPE_DETAIL_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.TICKET_TYPE_ID IS NULL
                   then mt.TICKET_TYPE_ID
                   else s1.TICKET_TYPE_ID
                   end) as TICKET_TYPE_ID,
                  (case
                   when l_mi_rec.SEQ_NO_mi    = 'N'
                    and svc_TICKET_TYPE_DETAIL_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.SEQ_NO IS NULL
                   then mt.SEQ_NO
                   else s1.SEQ_NO
                   end) as SEQ_NO,
                  (case
                   when l_mi_rec.TICKET_ITEM_ID_mi    = 'N'
                    and svc_TICKET_TYPE_DETAIL_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.TICKET_ITEM_ID IS NULL
                   then mt.TICKET_ITEM_ID
                   else s1.TICKET_ITEM_ID
                   end) as TICKET_ITEM_ID,
                  (case
                   when l_mi_rec.UDA_ID_mi    = 'N'
                    and svc_TICKET_TYPE_DETAIL_col(i).action = CORESVC_TICKET_TYPE_HEAD.action_mod
                    and s1.UDA_ID IS NULL
                   then mt.UDA_ID
                   else s1.UDA_ID
                   end) as UDA_ID,
                  null as dummy
              from (select svc_TICKET_TYPE_DETAIL_col(i).TICKET_TYPE_ID as TICKET_TYPE_ID,
                           svc_TICKET_TYPE_DETAIL_col(i).SEQ_NO         as SEQ_NO,
                           svc_TICKET_TYPE_DETAIL_col(i).TICKET_ITEM_ID as TICKET_ITEM_ID,
                           svc_TICKET_TYPE_DETAIL_col(i).UDA_ID         as UDA_ID,
                           null                                         as dummy
                      from dual ) s1,
            TICKET_TYPE_DETAIL mt
             where mt.SEQ_NO (+)     = s1.SEQ_NO
                   and mt.TICKET_TYPE_ID (+)     = s1.TICKET_TYPE_ID
                   and 1 = 1 )sq
                on ( st.SEQ_NO         = sq.SEQ_NO and
                     st.TICKET_TYPE_ID = sq.TICKET_TYPE_ID and
                     svc_TICKET_TYPE_DETAIL_col(i).ACTION IN (CORESVC_TICKET_TYPE_HEAD.action_mod,CORESVC_TICKET_TYPE_HEAD.action_del))
      when matched then
      update
         set process_id      = svc_TICKET_TYPE_DETAIL_col(i).process_id ,
             chunk_id        = svc_TICKET_TYPE_DETAIL_col(i).chunk_id ,
             row_seq         = svc_TICKET_TYPE_DETAIL_col(i).row_seq ,
             action          = svc_TICKET_TYPE_DETAIL_col(i).action ,
             process$status  = svc_TICKET_TYPE_DETAIL_col(i).process$status ,
             uda_id          = sq.uda_id ,
             ticket_item_id  = sq.ticket_item_id
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             ticket_type_id ,
             seq_no ,
             ticket_item_id ,
             uda_id )
      values(svc_TICKET_TYPE_DETAIL_col(i).process_id ,
             svc_TICKET_TYPE_DETAIL_col(i).chunk_id ,
             svc_TICKET_TYPE_DETAIL_col(i).row_seq ,
             svc_TICKET_TYPE_DETAIL_col(i).action ,
             svc_TICKET_TYPE_DETAIL_col(i).process$status ,
             sq.ticket_type_id ,
             sq.seq_no ,
             sq.ticket_item_id ,
             sq.uda_id);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            TICKET_TYPE_DETAIL_sheet,
                            svc_TICKET_TYPE_DETAIL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TICKET_TYPE_DETAIL;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count   IN OUT NUMBER,
                     I_file_id       IN     s9t_folder.file_id%TYPE,
                     I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_TICKET_TYPE_HEAD.process_s9t';
   L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR         EXCEPTION;
   PRAGMA           EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE    then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false   then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_TICKET_TYPE_HEAD(I_file_id,
                                   I_process_id);

      PROCESS_S9T_TICKET_TYPE_DETAIL(I_file_id,
                                     I_process_id);

      PROCESS_S9T_TTH_TL(I_file_id,
                         I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TICKET_TYPE_HEAD_VAL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_error         IN OUT BOOLEAN,
                                      I_rec           IN OUT C_SVC_TICKET_TYPE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'CORESVC_TICKET_TYPE_HEAD.PROCESS_TICKET_TYPE_HEAD_VAL';
   L_dummy             VARCHAR2(1)  := 'N';
   L_duplicate         BOOLEAN;
   L_valid             BOOLEAN;
   L_classname         CLASS.CLASS_NAME%TYPE;
   L_subclass          SUBCLASS.SUB_NAME%TYPE;
   L_merchid           VARCHAR2(1);
   L_filter_merch_name VARCHAR(255);
   L_filter_org_name   VARCHAR(255);
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TICKET_TYPE_HEAD';
   cursor C_CHECK_DELETE is
         select 'S'
           from item_ticket
          where ticket_type_id = I_rec.ticket_type_id
         union all
         select 'R'
           from ticket_request
          where ticket_type_id = I_rec.ticket_type_id;
BEGIN
   if I_rec.action IN (action_mod,action_new)   then
      if LP_merch_filter_level = 'C'   then
         if I_rec.filter_merch_id_class is NULL   then
            WRITE_ERROR( I_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'FILTER_MERCH_ID_CLASS',
                        'ENTER_CLASS');
            O_error := TRUE;
         else
            if FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS(O_error_message,
                                                      L_valid,
                                                      L_classname,
                                                      I_rec.filter_merch_id,
                                                      I_rec.filter_merch_id_class) = FALSE
               or L_valid = FALSE   then
               WRITE_ERROR( I_rec.process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_rec.chunk_id,
                            L_table,
                            I_rec.row_seq,
                           'FILTER_MERCH_ID_CLASS',
                            O_error_message);
               O_error := TRUE;
            end if;
         end if;
      elsif LP_merch_filter_level = 'S'   then
            if I_rec.filter_merch_id_class is NULL   then
               WRITE_ERROR( I_rec.process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_rec.chunk_id,
                            L_table,
                            I_rec.row_seq,
                           'FILTER_MERCH_ID_CLASS',
                           'ENTER_CLASS');
               O_error := TRUE;
            elsif I_rec.filter_merch_id_subclass is NULL   then
               WRITE_ERROR( I_rec.process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_rec.chunk_id,
                            L_table,
                            I_rec.row_seq,
                           'FILTER_MERCH_ID_SUBCLASS',
                           'ENTER_SUBCLASS');
               O_error := TRUE;
            else
               if FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS(O_error_message,
                                                         L_valid,
                                                         L_classname,
                                                         I_rec.filter_merch_id,
                                                         I_rec.filter_merch_id_class) = FALSE
                  or L_valid = FALSE   then
                  WRITE_ERROR( I_rec.process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               I_rec.chunk_id,
                               L_table,
                               I_rec.row_seq,
                              'FILTER_MERCH_ID_CLASS',
                               O_error_message);
                  O_error := TRUE;
               elsif FILTER_LOV_VALIDATE_SQL.VALIDATE_SUBCLASS(O_error_message,
                                                               L_valid,
                                                               L_subclass,
                                                               I_rec.filter_merch_id,
                                                               I_rec.filter_merch_id_class,
                                                               I_rec.filter_merch_id_subclass) = FALSE
                     or L_valid = FALSE   then
                        WRITE_ERROR( I_rec.process_id,
                                     svc_admin_upld_er_seq.NEXTVAL,
                                     I_rec.chunk_id,
                                     L_table,
                                     I_rec.row_seq,
                                    'FILTER_MERCH_ID_SUBCLASS',
                                     O_error_message);
                        O_error := TRUE;
               end if;
            end if;
      else
         I_rec.filter_merch_id_class    := NULL;
         I_rec.filter_merch_id_subclass := NULL;
      end if;


      if I_rec.filter_org_id is NOT NULL   then
         if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_ORG_LEVEL(O_error_message,
                                                           L_valid,
                                                           L_filter_org_name,
                                                           LP_org_filter_level,
                                                           I_rec.filter_org_id)   then
            WRITE_ERROR( I_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'FILTER_ORG_ID',
                         O_error_message);
            O_error := TRUE;
         elsif L_valid = FALSE then
            L_filter_org_name := NULL;
            WRITE_ERROR( I_rec.process_id,
                         svc_admin_upld_er_seq.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'FILTER_ORG_ID',
                         O_error_message);
            O_error := TRUE;
         end if;
      else
         L_filter_org_name := NULL;
      end if;

      L_merchid := LP_merch_filter_level;
      if LP_merch_filter_level in ('C','S')   then
         L_merchid := 'P';
         I_rec.filter_merch_id_class := NULL;
         if LP_merch_filter_level = 'S'   then
            I_rec.filter_merch_id_subclass := NULL;
         end if;
      end if;

      if I_rec.filter_merch_id is NOT NULL   then
         if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_MERCH_LEVEL(O_error_message,
                                                             L_valid,
                                                             L_filter_merch_name,
                                                             L_merchid,
                                                             I_rec.filter_merch_id,
                                                             I_rec.filter_merch_id_class,
                                                             I_rec.filter_merch_id_subclass)
            or (L_valid = FALSE)   then
                L_filter_merch_name := NULL;
                WRITE_ERROR( I_rec.process_id,
                             svc_admin_upld_er_seq.NEXTVAL,
                             I_rec.chunk_id,
                             L_table,
                             I_rec.row_seq,
                            'FILTER_MERCH_ID',
                             O_error_message);
                O_error := TRUE;
         end if;
      else
         L_filter_merch_name := NULL;
      end if;
   end if;


   if I_rec.action = action_del   then
      open  C_CHECK_DELETE;
      fetch C_CHECK_DELETE into L_dummy;
      close C_CHECK_DELETE;
      if L_dummy = 'S'   then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'TICKET_ITEM_ID',
                     'DEL_TICKET_ITEM');
         O_error := TRUE;
      elsif L_dummy = 'R'   then
         WRITE_ERROR( I_rec.process_id,
                      svc_admin_upld_er_seq.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'TICKET_ITEM_ID',
                     'DEL_TICKET_REQ');
         O_error := TRUE;
      end if;
   end if;


   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CHECK_DELETE%ISOPEN   then
         close C_CHECK_DELETE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TICKET_TYPE_HEAD_VAL;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_TICKET_TYPE_HEAD_INS(O_error_message              IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_ticket_type_head_temp_rec  IN      TICKET_TYPE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TICKET_TYPE_HEAD_INS';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TICKET_TYPE_HEAD';
BEGIN
   insert into ticket_type_head
        values I_ticket_type_head_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TICKET_TYPE_HEAD_INS;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_TICKET_TYPE_HEAD_UPD(O_error_message              IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_ticket_type_head_temp_rec  IN      TICKET_TYPE_HEAD%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TICKET_TYPE_HEAD_UPD';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TICKET_TYPE_HEAD';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);
   cursor C_TICKET_TYPE_HEAD_LOCK is
      select 'X'
        from ticket_type_head
       where ticket_type_id = I_ticket_type_head_temp_rec.ticket_type_id
       for update nowait;
BEGIN
   open C_TICKET_TYPE_HEAD_LOCK;
   close C_TICKET_TYPE_HEAD_LOCK;

   update ticket_type_head
      set row = I_ticket_type_head_temp_rec
    where ticket_type_id = I_ticket_type_head_temp_rec.ticket_type_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'TICKET_TYPE_HEAD',
                                                                I_ticket_type_head_temp_rec.ticket_type_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_TICKET_TYPE_HEAD_LOCK%ISOPEN   then
         close C_TICKET_TYPE_HEAD_LOCK;
      end if;
      return FALSE;
END EXEC_TICKET_TYPE_HEAD_UPD;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_TTD_PRE_DEL(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_ticket_type_detail_temp_rec IN     TICKET_TYPE_HEAD%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TTD_PRE_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TICKET_TYPE_DETAIL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);
   cursor C_TICKET_TYPE_DETAIL_LOCK is
      select 'X'
        from TICKET_TYPE_DETAIL
       where ticket_type_id = I_ticket_type_detail_temp_rec.ticket_type_id
       for update nowait;
BEGIN
   open C_TICKET_TYPE_DETAIL_LOCK;
   close C_TICKET_TYPE_DETAIL_LOCK;

   delete ticket_type_detail
    where ticket_type_id = I_ticket_type_detail_temp_rec.ticket_type_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'TICKET_TYPE_DETAIL',
                                                                I_ticket_type_detail_temp_rec.ticket_type_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_TICKET_TYPE_DETAIL_LOCK%ISOPEN   then
         close C_TICKET_TYPE_DETAIL_LOCK;
      end if;
      return FALSE;
END EXEC_TTD_PRE_DEL;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_TICKET_TYPE_HEAD_DEL(O_error_message              IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_ticket_type_head_temp_rec  IN      TICKET_TYPE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TICKET_TYPE_HEAD_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TICKET_TYPE_HEAD';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_TICKET_TYPE_HEAD_TL_LOCK is
      select 'X'
        from ticket_type_head_tl
       where ticket_type_id = I_ticket_type_head_temp_rec.ticket_type_id
       for update nowait;

   cursor C_TICKET_TYPE_HEAD_LOCK is
      select 'X'
        from ticket_type_head
       where ticket_type_id = I_ticket_type_head_temp_rec.ticket_type_id
       for update nowait;
BEGIN
   open C_TICKET_TYPE_HEAD_TL_LOCK;
   close C_TICKET_TYPE_HEAD_TL_LOCK;

   delete from ticket_type_head_tl
    where ticket_type_id = I_ticket_type_head_temp_rec.ticket_type_id;

   open C_TICKET_TYPE_HEAD_LOCK;
   close C_TICKET_TYPE_HEAD_LOCK;

   if EXEC_TTD_PRE_DEL(O_error_message,
                       I_ticket_type_head_temp_rec) = FALSE   then
      return FALSE;
   end if;

   delete from ticket_type_head
    where ticket_type_id = I_ticket_type_head_temp_rec.ticket_type_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'TICKET_TYPE_HEAD',
                                                                I_ticket_type_head_temp_rec.ticket_type_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_TICKET_TYPE_HEAD_TL_LOCK%ISOPEN then
         close C_TICKET_TYPE_HEAD_TL_LOCK;
      end if;
      if C_TICKET_TYPE_HEAD_LOCK%ISOPEN then
         close C_TICKET_TYPE_HEAD_LOCK;
      end if;
      return FALSE;
END EXEC_TICKET_TYPE_HEAD_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TICKET_TYPE_HEAD_TL_INS(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_ticket_type_head_tl_ins_tab    IN       TICKET_TYPE_HEAD_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TICKET_TYPE_HEAD_TL_INS';

BEGIN
   if I_ticket_type_head_tl_ins_tab is NOT NULL and I_ticket_type_head_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_ticket_type_head_tl_ins_tab.COUNT()
         insert into ticket_type_head_tl
              values I_ticket_type_head_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TICKET_TYPE_HEAD_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TICKET_TYPE_HEAD_TL_UPD(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_ticket_type_head_tl_upd_tab   IN       TICKET_TYPE_HEAD_TAB,
                                      I_ticket_tl_upd_rst             IN       ROW_SEQ_TAB,
                                      I_process_id                    IN       SVC_TICKET_TYPE_HEAD_TL.PROCESS_ID%TYPE,
                                      I_chunk_id                      IN       SVC_TICKET_TYPE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TICKET_TYPE_HEAD_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TICKET_TYPE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_TICKET_TYPE_HEAD_TL_UPD(I_ticket_type_id    TICKET_TYPE_HEAD_TL.TICKET_TYPE_ID%TYPE,
                                         I_lang              TICKET_TYPE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from ticket_type_head_tl
       where ticket_type_id = I_ticket_type_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_ticket_type_head_tl_upd_tab is NOT NULL and I_ticket_type_head_tl_upd_tab.count > 0 then
      for i in I_ticket_type_head_tl_upd_tab.FIRST..I_ticket_type_head_tl_upd_tab.LAST loop
         BEGIN
         L_key_val1 := 'Lang: '||to_char(I_ticket_type_head_tl_upd_tab(i).lang);
         L_key_val2 := 'Ticket Type ID: '||to_char(I_ticket_type_head_tl_upd_tab(i).ticket_type_id);
            open C_LOCK_TICKET_TYPE_HEAD_TL_UPD(I_ticket_type_head_tl_upd_tab(i).ticket_type_id,
                                                I_ticket_type_head_tl_upd_tab(i).lang);
            close C_LOCK_TICKET_TYPE_HEAD_TL_UPD;
            
            update ticket_type_head_tl
               set ticket_type_desc = I_ticket_type_head_tl_upd_tab(i).ticket_type_desc,
                   last_update_id = I_ticket_type_head_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_ticket_type_head_tl_upd_tab(i).last_update_datetime
             where lang = I_ticket_type_head_tl_upd_tab(i).lang
               and ticket_type_id = I_ticket_type_head_tl_upd_tab(i).ticket_type_id;
            
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_TICKET_TYPE_HEAD_TL',
                           I_ticket_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_TICKET_TYPE_HEAD_TL_UPD%ISOPEN then
         close C_LOCK_TICKET_TYPE_HEAD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TICKET_TYPE_HEAD_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TICKET_TYPE_HEAD_TL_DEL(O_error_message                 IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_ticket_type_head_tl_del_tab   IN       TICKET_TYPE_HEAD_TAB,
                                      I_ticket_tl_del_rst             IN       ROW_SEQ_TAB,
                                      I_process_id                    IN       SVC_TICKET_TYPE_HEAD_TL.PROCESS_ID%TYPE,
                                      I_chunk_id                      IN       SVC_TICKET_TYPE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TICKET_TYPE_HEAD_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TICKET_TYPE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_TICKET_TYPE_HEAD_TL_DEL(I_ticket_type_id  TICKET_TYPE_HEAD_TL.TICKET_TYPE_ID%TYPE,
                                         I_lang            TICKET_TYPE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from ticket_type_head_tl
       where ticket_type_id = I_ticket_type_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_ticket_type_head_tl_del_tab is NOT NULL and I_ticket_type_head_tl_del_tab.count > 0 then
      for i in I_ticket_type_head_tl_del_tab.FIRST..I_ticket_type_head_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_ticket_type_head_tl_del_tab(i).lang);
            L_key_val2 := 'Ticket Type ID: '||to_char(I_ticket_type_head_tl_del_tab(i).ticket_type_id);
            open C_LOCK_TICKET_TYPE_HEAD_TL_DEL(I_ticket_type_head_tl_del_tab(i).ticket_type_id,
                                                I_ticket_type_head_tl_del_tab(i).lang);
            close C_LOCK_TICKET_TYPE_HEAD_TL_DEL;
            
            delete ticket_type_head_tl
             where lang = I_ticket_type_head_tl_del_tab(i).lang
               and ticket_type_id = I_ticket_type_head_tl_del_tab(i).ticket_type_id;
               
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_TICKET_TYPE_HEAD_TL',
                           I_ticket_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_TICKET_TYPE_HEAD_TL_DEL%ISOPEN then
         close C_LOCK_TICKET_TYPE_HEAD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TICKET_TYPE_HEAD_TL_DEL;
-----------------------------------------------------------------------------------------------------

FUNCTION PROCESS_TICKET_TYPE_DETAIL_VAL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_error          IN OUT  BOOLEAN,
                                        I_rec            IN OUT  C_SVC_TICKET_TYPE_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)  := 'CORESVC_TICKET_TYPE_HEAD.PROCESS_TICKET_TYPE_DETAIL_VAL';
   L_ticket_item_desc  VARCHAR(64);
   L_duplicate         BOOLEAN;
   L_valid             BOOLEAN;
   L_code_desc         VARCHAR(64);
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TICKET_TYPE_DETAIL';
BEGIN
   if I_rec.action = action_new
      and ((I_rec.ticket_item_id is NOT NULL
      and I_rec.uda_id is NOT NULL)
      or (I_rec.ticket_item_id is NULL
      and I_rec.uda_id is NULL)) then
      WRITE_ERROR( I_rec.process_id,
                   svc_admin_upld_er_seq.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                   'Attribute, User Defined Attribute',
                   'ONE_OPTION_ALLOWED');
      O_error := TRUE;
   elsif I_rec.action = action_new
         and (I_rec.ticket_item_id is NOT NULL
         or I_rec.uda_id is NOT NULL )   then

         if I_rec.action = action_new
            and I_rec.PK_TICKET_TYPE_DETAIL_rid is NULL   then
            if NOT TICKET_SQL.TICKET_ITEM_EXISTS(O_error_message,
                                                 L_duplicate,
                                                 I_rec.ticket_type_id,
                                                 I_rec.ticket_item_id,
                                                 I_rec.uda_id)   then
               WRITE_ERROR( I_rec.process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_rec.chunk_id,
                            L_table,
                            I_rec.row_seq,
                           'TICKET_ITEM_ID',
                            O_error_message);
               O_error := TRUE;
            elsif L_duplicate   then
                  WRITE_ERROR( I_rec.process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               I_rec.chunk_id,
                               L_table,
                               I_rec.row_seq,
                              'TICKET_ITEM_ID',
                              'ATTRIB_DUPLIC');
                  O_error := TRUE;
            end if;
         end if;


         if I_rec.ticket_item_id is NULL
            and I_rec.uda_id is NOT NULL
            and NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_UDA(O_error_message,
                                                        L_valid,
                                                        L_ticket_item_desc,
                                                        I_rec.uda_id)
                  or NOT L_valid   then
                  L_ticket_item_desc := NULL;
                  WRITE_ERROR( I_rec.process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               I_rec.chunk_id,
                               L_table,
                               I_rec.row_seq,
                              'UDA_ID',
                               O_error_message);
                  O_error := TRUE;
         elsif  I_rec.ticket_item_id is NOT NULL
            and I_rec.uda_id is NULL
            and NOT( I_rec.TICKET_ITEM_ID IN ( 'ITEM',
                                               'ITDS',
                                               'ITSD',
                                               'VAR',
                                               'DIF1',
                                               'DIF2',
                                               'DIF3',
                                               'DIF4',
                                               'WGHT',
                                               'DEPT',
                                               'CLSS',
                                               'SBCL',
                                               'RTPC',
                                               'SRTP',
                                               'MUPC',
                                               'SUPR',
                                               'SUP1' ,
                                               'SUP2',
                                               'SUP3',
                                               'SUP4',
                                               'STRE',
                                               'WHSE',
                                               'COOG',
                                               'UOM',
                                               'ITPR',
                                               'IPDS' ,
                                               'EURO',
                                               'NETV',
                                               'DPST',
                                               'DTOT' ))   then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'TICKET_ITEM_ID',
                        'INV_TICKET_ITEM_ID_ATT');
            O_error := TRUE;
         end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TICKET_TYPE_DETAIL_VAL;
---------------------------------------------------------------------------------------------------
FUNCTION EXEC_TICKET_TYPE_DETAIL_INS(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_ticket_type_detail_temp_rec IN     TICKET_TYPE_DETAIL%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TICKET_TYPE_DETAIL_INS';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TICKET_TYPE_DETAIL';
BEGIN
   insert into ticket_type_detail
        values I_ticket_type_detail_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TICKET_TYPE_DETAIL_INS;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_TICKET_TYPE_DETAIL_DEL(O_error_message               IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_ticket_type_detail_temp_rec IN     TICKET_TYPE_DETAIL%ROWTYPE )
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_TICKET_TYPE_HEAD.EXEC_TICKET_TYPE_DETAIL_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TICKET_TYPE_DETAIL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_TICKET_TYPE_DETAIL_LOCK is
      select 'X'
        from TICKET_TYPE_DETAIL
       where seq_no         = I_ticket_type_detail_temp_rec.seq_no
         and ticket_type_id = I_ticket_type_detail_temp_rec.ticket_type_id
       for update nowait;
BEGIN
   open C_TICKET_TYPE_DETAIL_LOCK;
   close C_TICKET_TYPE_DETAIL_LOCK;

   delete from ticket_type_detail
    where seq_no         = I_ticket_type_detail_temp_rec.seq_no
      and ticket_type_id = I_ticket_type_detail_temp_rec.ticket_type_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'TICKET_TYPE_DETAIL',
                                                                I_ticket_type_detail_temp_rec.seq_no,
                                                                I_ticket_type_detail_temp_rec.ticket_type_id);
      return FALSE;

   when OTHERS then
      if C_TICKET_TYPE_DETAIL_LOCK%ISOPEN   then
         close C_TICKET_TYPE_DETAIL_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TICKET_TYPE_DETAIL_DEL;
-----------------------------------------------------------------------------------------------------
FUNCTION UPDATE_DETAIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_ticket_type_detail_id IN     TICKET_TYPE_DETAIL.TICKET_TYPE_ID%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) := 'CORESVC_TICKET_TYPE_HEAD.UPDATE_DETAIL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_SVC_TICKET_TYPE_DETAIL_LOCK is
      select 'X'
        from SVC_TICKET_TYPE_DETAIL
       where ticket_type_id = I_ticket_type_detail_id
       for update nowait;
BEGIN
   open C_SVC_TICKET_TYPE_DETAIL_LOCK;
   close C_SVC_TICKET_TYPE_DETAIL_LOCK;

   update SVC_TICKET_TYPE_DETAIL
      set process$status = 'P'
    where ticket_type_id = I_ticket_type_detail_id
      and action = action_del;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'SVC_TICKET_TYPE_DETAIL',
                                                                NULL,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_SVC_TICKET_TYPE_DETAIL_LOCK%ISOPEN   then
         close C_SVC_TICKET_TYPE_DETAIL_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END UPDATE_DETAIL;
---------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TICKET_TYPE_HEAD( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id    IN     SVC_TICKET_TYPE_HEAD.PROCESS_ID%TYPE,
                                   I_chunk_id      IN     SVC_TICKET_TYPE_HEAD.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                     VARCHAR2(64)  :='CORESVC_TICKET_TYPE_HEAD.PROCESS_TICKET_TYPE_HEAD';
   L_table_head                  VARCHAR2(255) :='SVC_TICKET_TYPE_HEAD';
   L_table_detail                VARCHAR2(255) :='SVC_TICKET_TYPE_DETAIL';
   L_process_error_head          BOOLEAN       := FALSE;
   L_process_error_detail        BOOLEAN       := FALSE;
   L_error_head                  BOOLEAN;
   L_error_detail                BOOLEAN;
   L_ticket_type_head_temp_rec   TICKET_TYPE_HEAD%ROWTYPE;
   L_ticket_type_detail_temp_rec TICKET_TYPE_DETAIL%ROWTYPE;
   L_sys_opt_row                 SYSTEM_OPTIONS%ROWTYPE;

   cursor C_MAX_SEQ is
     select NVL(max(seq_no),0)+1
       from ticket_type_detail;

BEGIN

if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                             L_sys_opt_row)   then
   L_error_head := TRUE;
   return FALSE;
else
   LP_org_filter_level   := L_sys_opt_row.ticket_type_org_level_code;
   LP_merch_filter_level := L_sys_opt_row.ticket_type_merch_level_code;

   FOR rec_head IN C_SVC_TICKET_TYPE_HEAD(I_process_id,
                                          I_chunk_id)
      LOOP
         L_error_head           := FALSE;
         L_process_error_head   := FALSE;

         if rec_head.action is NULL
            or rec_head.action NOT IN (action_new,action_mod,action_del)   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_head,
                         rec_head.row_seq,
                        'ACTION',
                        'INV_ACT');
            L_error_head := TRUE;
         end if;

         if rec_head.action = action_new
            and rec_head.PK_TICKET_TYPE_HEAD_rid is NOT NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_head,
                         rec_head.row_seq,
                        'TICKET_TYPE_ID',
                        'DUP_RECORD');
            L_error_head := TRUE;
         end if;

         if rec_head.action IN (action_mod,action_del)
            and rec_head.PK_TICKET_TYPE_HEAD_rid is NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_head,
                         rec_head.row_seq,
                        'TICKET_TYPE_ID',
                        'NO_RECORD');
            L_error_head := TRUE;
         end if;

         if rec_head.TICKET_TYPE_ID is NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_head,
                         rec_head.row_seq,
                        'TICKET_TYPE_ID',
                        'TICKET_TYPE_REQ');
            L_error_head := TRUE;
         end if;

         if rec_head.action IN (action_mod,action_new)   then
            if rec_head.TICKET_TYPE_DESC is NULL   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table_head,
                            rec_head.row_seq,
                           'TICKET_TYPE_DESC',
                           'TICKET_DESC_REQ');
               L_error_head := TRUE;
            end if;
            if rec_head.SEL_IND is NULL
               or rec_head.SEL_IND NOT IN ( 'Y', 'N' )   then
               WRITE_ERROR( I_process_id,
                            SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                            I_chunk_id,
                            L_table_head,
                            rec_head.row_seq,
                           'SEL_IND',
                           'INV_Y_N_IND');
               L_error_head := TRUE;
            end if;
         end if;

         if PROCESS_TICKET_TYPE_HEAD_VAL(O_error_message,
                                         L_error_head,
                                         rec_head ) = FALSE   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_head,
                         rec_head.row_seq,
                         NULL,
                         O_error_message);
            L_error_head :=TRUE;
         end if;

         if NOT L_error_head   then
            L_ticket_type_head_temp_rec.ticket_type_id           := rec_head.ticket_type_id;
            L_ticket_type_head_temp_rec.ticket_type_desc         := rec_head.ticket_type_desc;
            L_ticket_type_head_temp_rec.sel_ind                  := rec_head.sel_ind;
            L_ticket_type_head_temp_rec.filter_org_id            := rec_head.filter_org_id;
            L_ticket_type_head_temp_rec.filter_merch_id          := rec_head.filter_merch_id;
            L_ticket_type_head_temp_rec.filter_merch_id_class    := rec_head.filter_merch_id_class;
            L_ticket_type_head_temp_rec.filter_merch_id_subclass := rec_head.filter_merch_id_subclass;

            if rec_head.action = action_new   then
               if EXEC_TICKET_TYPE_HEAD_INS(O_error_message,
                                            L_ticket_type_head_temp_rec) = FALSE   then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_head,
                              rec_head.row_seq,
                              NULL,
                              O_error_message);
                  L_process_error_head := TRUE;
               end if;
            end if;

            if rec_head.action = action_mod   then
               if EXEC_TICKET_TYPE_HEAD_UPD(O_error_message,
                                            L_ticket_type_head_temp_rec) = FALSE   then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_head,
                              rec_head.row_seq,
                              NULL,
                              O_error_message);
                  L_process_error_head := TRUE;
               end if;
            end if;

            if rec_head.action = action_del   then
               if EXEC_TICKET_TYPE_HEAD_DEL(O_error_message,
                                            L_ticket_type_head_temp_rec )=FALSE   then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_head,
                              rec_head.row_seq,
                              NULL,
                              O_error_message);
                  L_process_error_head := TRUE;
				else
				  if UPDATE_DETAIL(O_error_message,
                                   L_ticket_type_head_temp_rec.ticket_type_id) = FALSE   then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table_head,
                                 rec_head.row_seq,
                                 NULL,
                                 O_error_message);
                     L_process_error_head := TRUE;
                  end if;
               end if;
            end if;
         end if;
      END LOOP;

      FOR rec_detail IN C_SVC_TICKET_TYPE_DETAIL(I_process_id,
                                                 I_chunk_id)
         LOOP
            L_error_detail         := FALSE;
            L_process_error_detail := FALSE;
            --L_CHK_ID               := NULL;

         if rec_detail.action NOT IN (action_new, action_del)   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_detail,
                         rec_detail.row_seq,
                        'ACTION',
                        'INV_ACT');
            L_error_detail := TRUE;
         end if;

         if rec_detail.action = action_new then
            if rec_detail.ttd_tth_fk_rid IS NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_detail,
                           rec_detail.row_seq,
                           'TICKET_TYPE_ID',
                           'NO_HEAD_RECORD');
               L_error_detail :=TRUE;
			      end if;
		     end if;
         if rec_detail.action = action_del
            and rec_detail.PK_TICKET_TYPE_DETAIL_rid is NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_detail,
                         rec_detail.row_seq,
                        'Ticket Type, Sequence Number',
                        'NO_RECORD');
            L_error_detail :=TRUE;
         end if;

--PK NULL CHECK-----------------------------------------------------------------
         if rec_detail.TICKET_TYPE_ID IS NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_detail,
                         rec_detail.row_seq,
                        'TICKET_TYPE_ID',
                        'TICKET_ID_REQ');
            L_error_detail :=TRUE;
         end if;
         if rec_detail.action = action_del
            and rec_detail.SEQ_NO IS NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_detail,
                         rec_detail.row_seq,
                        'SEQ_NO',
                        'MUST_ENTER_FIELD');
            L_error_detail :=TRUE;
         end if;

         if PROCESS_TICKET_TYPE_DETAIL_VAL(O_error_message,
                                           L_error_detail,
                                           rec_detail ) = FALSE   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table_detail,
                         rec_detail.row_seq,
                         NULL,
                         O_error_message);
            L_error_detail :=TRUE;
         end if;

         if NOT L_error_detail   then

            if rec_detail.action = action_new   then
               open C_MAX_SEQ;
               fetch C_MAX_SEQ into rec_detail.seq_no;
               close C_MAX_SEQ;
            end if;

            L_ticket_type_detail_temp_rec.ticket_type_id := rec_detail.ticket_type_id;
            L_ticket_type_detail_temp_rec.seq_no         := rec_detail.seq_no;
            L_ticket_type_detail_temp_rec.ticket_item_id := rec_detail.ticket_item_id;
            L_ticket_type_detail_temp_rec.uda_id         := rec_detail.uda_id;

            if rec_detail.action = action_new   then
               if EXEC_TICKET_TYPE_DETAIL_INS(O_error_message,
                                              L_ticket_type_detail_temp_rec ) = FALSE   then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_detail,
                              rec_detail.row_seq,
                              NULL,
                              O_error_message);
                  L_process_error_detail := TRUE;
               end if;
            end if;

            if rec_detail.action = action_del   then
               if EXEC_TICKET_TYPE_DETAIL_DEL(O_error_message,
                                              L_ticket_type_detail_temp_rec ) = FALSE   then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_detail,
                              rec_detail.row_seq,
                              NULL,
                              O_error_message);
                  L_process_error_detail := TRUE;
               end if;
            end if;
         end if;

      END LOOP;
end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_MAX_SEQ%ISOPEN   then
         close C_MAX_SEQ;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TICKET_TYPE_HEAD;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TICKET_TYPE_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_process_id      IN       SVC_TICKET_TYPE_HEAD_TL.PROCESS_ID%TYPE,
                                     I_chunk_id        IN       SVC_TICKET_TYPE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                         VARCHAR2(64) := 'CORESVC_TICKET_TYPE_HEAD.PROCESS_TICKET_TYPE_HEAD_TL';
   L_error_message                   RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TICKET_TYPE_HEAD_TL';
   L_base_table                      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TICKET_TYPE_HEAD';
   L_table                           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TICKET_TYPE_HEAD_TL';
   L_error                           BOOLEAN := FALSE;
   L_process_error                   BOOLEAN := FALSE;
   L_ticket_type_head_tl_temp_rec    TICKET_TYPE_HEAD_TL%ROWTYPE;
   L_ticket_tl_upd_rst               ROW_SEQ_TAB;
   L_ticket_tl_del_rst               ROW_SEQ_TAB;

   cursor C_SVC_TICKET_TYPE_HEAD_TL(I_process_id NUMBER,
                                    I_chunk_id NUMBER) is
      select pk_ticket_type_head_tl.rowid  as pk_ticket_type_head_tl_rid,
             fk_ticket_type_head.rowid     as fk_ticket_type_head_rid,
             fk_lang.rowid                 as fk_lang_rid,
             st.lang,
             UPPER(st.ticket_type_id)   as ticket_type_id,
             st.ticket_type_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_ticket_type_head_tl  st,
             ticket_type_head         fk_ticket_type_head,
             ticket_type_head_tl      pk_ticket_type_head_tl,
             lang                     fk_lang
       where st.process_id      =  I_process_id
         and st.chunk_id        =  I_chunk_id
         and st.ticket_type_id  =  fk_ticket_type_head.ticket_type_id (+)
         and st.lang            =  pk_ticket_type_head_tl.lang (+)
         and st.ticket_type_id  =  pk_ticket_type_head_tl.ticket_type_id (+)
         and st.lang            =  fk_lang.lang (+);

   TYPE SVC_TICKET_TYPE_HEAD_TL is TABLE OF C_SVC_TICKET_TYPE_HEAD_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_ticket_type_head_tab        SVC_TICKET_TYPE_HEAD_TL;

   L_ticket_type_head_TL_ins_tab         TICKET_TYPE_HEAD_TAB         := NEW TICKET_TYPE_HEAD_TAB();
   L_ticket_type_head_TL_upd_tab         TICKET_TYPE_HEAD_TAB         := NEW TICKET_TYPE_HEAD_TAB();
   L_ticket_type_head_TL_del_tab         TICKET_TYPE_HEAD_TAB         := NEW TICKET_TYPE_HEAD_TAB();

BEGIN
   if C_SVC_TICKET_TYPE_HEAD_TL%ISOPEN then
      close C_SVC_TICKET_TYPE_HEAD_TL;
   end if;

   open C_SVC_TICKET_TYPE_HEAD_TL(I_process_id,
                                  I_chunk_id);
   LOOP
      fetch C_SVC_TICKET_TYPE_HEAD_TL bulk collect into L_svc_ticket_type_head_tab limit LP_bulk_fetch_limit;
      if L_svc_ticket_type_head_tab.COUNT > 0 then
         FOR i in L_svc_ticket_type_head_tab.FIRST..L_svc_ticket_type_head_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_ticket_type_head_tab(i).action is NULL
               or L_svc_ticket_type_head_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ticket_type_head_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

             --check for primary_lang
            if L_svc_ticket_type_head_tab(i).lang = LP_primary_lang and L_svc_ticket_type_head_tab(i).action = action_new then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ticket_type_head_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_ticket_type_head_tab(i).action = action_new
               and L_svc_ticket_type_head_tab(i).pk_ticket_type_head_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ticket_type_head_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_ticket_type_head_tab(i).action IN (action_mod, action_del)
               and L_svc_ticket_type_head_tab(i).lang is NOT NULL
               and L_svc_ticket_type_head_tab(i).ticket_type_id is NOT NULL
               and L_svc_ticket_type_head_tab(i).pk_ticket_type_head_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_ticket_type_head_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_ticket_type_head_tab(i).action = action_new
               and L_svc_ticket_type_head_tab(i).ticket_type_id is NOT NULL
               and L_svc_ticket_type_head_tab(i).fk_ticket_type_head_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_ticket_type_head_tab(i).row_seq,
                            'TICKET_TYPE_ID',
                           'INV_TCKT_TYPE');
               L_error :=TRUE;
            end if;

            if L_svc_ticket_type_head_tab(i).action = action_new
               and L_svc_ticket_type_head_tab(i).lang is NOT NULL
               and L_svc_ticket_type_head_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ticket_type_head_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_ticket_type_head_tab(i).action in (action_new, action_mod) then
               if L_svc_ticket_type_head_tab(i).ticket_type_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_ticket_type_head_tab(i).row_seq,
                              'TICKET_TYPE_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_ticket_type_head_tab(i).ticket_type_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ticket_type_head_tab(i).row_seq,
                           'TICKET_TYPE_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_ticket_type_head_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_ticket_type_head_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_ticket_type_head_TL_temp_rec.lang := L_svc_ticket_type_head_tab(i).lang;
               L_ticket_type_head_TL_temp_rec.ticket_type_id := L_svc_ticket_type_head_tab(i).ticket_type_id;
               L_ticket_type_head_TL_temp_rec.ticket_type_desc := L_svc_ticket_type_head_tab(i).ticket_type_desc;
               L_ticket_type_head_TL_temp_rec.create_datetime := SYSDATE;
               L_ticket_type_head_TL_temp_rec.create_id := GET_USER;
               L_ticket_type_head_TL_temp_rec.last_update_datetime := SYSDATE;
               L_ticket_type_head_TL_temp_rec.last_update_id := GET_USER;

               if L_svc_ticket_type_head_tab(i).action = action_new then
                  L_ticket_type_head_TL_ins_tab.extend;
                  L_ticket_type_head_TL_ins_tab(L_ticket_type_head_TL_ins_tab.count()) := L_ticket_type_head_TL_temp_rec;
               end if;

               if L_svc_ticket_type_head_tab(i).action = action_mod then
                  L_ticket_type_head_TL_upd_tab.extend;
                  L_ticket_type_head_TL_upd_tab(L_ticket_type_head_TL_upd_tab.count()) := L_ticket_type_head_TL_temp_rec;
                  L_ticket_tl_upd_rst(L_ticket_type_head_TL_upd_tab.count()) := L_svc_ticket_type_head_tab(i).row_seq;
               end if;

               if L_svc_ticket_type_head_tab(i).action = action_del then
                  L_ticket_type_head_TL_del_tab.extend;
                  L_ticket_type_head_TL_del_tab(L_ticket_type_head_TL_del_tab.count()) := L_ticket_type_head_TL_temp_rec;
                  L_ticket_tl_del_rst(L_ticket_type_head_TL_del_tab.count()) := L_svc_ticket_type_head_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_TICKET_TYPE_HEAD_TL%NOTFOUND;
   END LOOP;
   close C_SVC_TICKET_TYPE_HEAD_TL;

   if EXEC_TICKET_TYPE_HEAD_TL_INS(O_error_message,
                                   L_ticket_type_head_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_TICKET_TYPE_HEAD_TL_UPD(O_error_message,
                                   L_ticket_type_head_tl_upd_tab,
                                   L_ticket_tl_upd_rst,
                                   I_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_TICKET_TYPE_HEAD_TL_DEL(O_error_message,
                                   L_ticket_type_head_tl_del_tab,
                                   L_ticket_tl_del_rst,
                                   I_process_id,
                                   I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TICKET_TYPE_HEAD_TL;
-----------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
     from svc_ticket_type_head_tl
    where process_id = I_process_id;

   delete
     from svc_ticket_type_head
    where process_id = I_process_id;

   delete
     from svc_ticket_type_detail
    where process_id = I_process_id;
END;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT NUMBER,
                  I_process_id    IN     NUMBER,
                  I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    :='CORESVC_TICKET_TYPE_HEAD.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_TICKET_TYPE_HEAD(O_error_message,
                               I_process_id,
                               I_chunk_id) = FALSE   then
      return FALSE;
   end if;

   if PROCESS_TICKET_TYPE_HEAD_TL(O_error_message,
                                  I_process_id,
                                  I_chunk_id) = FALSE   then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                       else L_process_status
                        END),
            action_date = SYSDATE
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-----------------------------------------------------------------------------------------------------
END CORESVC_TICKET_TYPE_HEAD;
/