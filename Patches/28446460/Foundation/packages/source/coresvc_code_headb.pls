create or replace PACKAGE BODY CORESVC_CODE_HEAD as
--------------------------------------------------------------------------------
--Global variables
--------------------------------------------------------------------------------
   L_error   BOOLEAN := FALSE;
--------------------------------------------------------------------------------
cursor C_SVC_CODE_HEAD(I_process_id   NUMBER,
                       I_chunk_id     NUMBER) is
   select  t.pk_code_detail_rid,
           t.uk_code_detail_rid,
           t.cdd_cdh_fk_rid,
           UPPER(t.old_required_ind)  as old_required_ind,
           t.stch_rid,
           UPPER(t.stch_action)       as stch_action,
           t.stch_row_seq,
           UPPER(t.stch_code_type)    as stch_code_type,
           LEAD(t.stch_code_type, 1, 0) OVER (ORDER BY t.stch_code_type) as next_code_type,
           t.stch_code_type_desc,
           UPPER(t.stcd_action)       as stcd_action,
           t.stcd_row_seq,
           t.stcd_rid,
           UPPER(t.stcd_code_type)    as stcd_code_type,
           UPPER(t.stcd_code)         as stcd_code,
           t.stcd_code_desc,
           UPPER(t.stcd_required_ind) as stcd_required_ind,
           t.stcd_code_seq,
           t.stcd_process_id,
           t.stch_process_id,
           T.Stch_Chunk_Id,
           t.stcd_chunk_id,
           row_number() over (partition BY t.stch_code_type order by t.stch_code_type) as ch_rank,
           t.old_code_type            as    old_code_type,
           t.old_code_type_desc       as    old_code_type_desc,
           t.old_det_code_type        as    old_det_code_type,
           t.old_code                 as    old_code,
           t.old_code_desc            as    old_code_desc,
           t.old_code_seq             as    old_code_seq
     from
           (select pk_code_detail.rowid               as pk_code_detail_rid,
                   uk_code_detail.rowid               as uk_code_detail_rid,
                   cdd_cdh_fk.rowid                   as cdd_cdh_fk_rid,
                   UPPER(pk_code_detail.required_ind) as old_required_ind,
                   stch.rowid                         as stch_rid,
                   UPPER(stch.action)                 as stch_action,
                   stch.row_seq                       as stch_row_seq,
                   UPPER(stch.code_type)              as stch_code_type,
                   stch.code_type_desc                as stch_code_type_desc,
                   UPPER(stcd.action)                 as stcd_action,
                   stcd.row_seq                       as stcd_row_seq,
                   stcd.rowid                         as stcd_rid,
                   UPPER(stcd.code_type)              as stcd_code_type,
                   UPPER(stcd.code)                   as stcd_code,
                   stcd.code_desc                     as stcd_code_desc,
                   UPPER(stcd.required_ind)           as stcd_required_ind,
                   stcd.code_seq                      as stcd_code_seq,
                   stch.process_id                    as stch_process_id,
                   stcd.process_id                    as stcd_process_id,
                   stch.chunk_id                      as stch_chunk_id,
                   stcd.chunk_id                      as stcd_chunk_id,
                   CDD_CDH_FK.code_type               as old_code_type,
                   CDD_CDH_FK.code_type_desc          as old_code_type_desc,
                   pk_code_detail.code_type           as old_det_code_type,
                   pk_code_detail.code                as old_code,
                   pk_code_detail.code_desc           as old_code_desc,
                   pk_code_detail.code_seq            as old_code_seq
              from svc_code_head stch,
                   svc_code_detail stcd,
                   code_head cdd_cdh_fk,
                   code_detail pk_code_detail,
                   code_detail uk_code_detail
             where stch.process_id                = I_process_id
               and stch.chunk_id                  = I_chunk_id
               and stch.process_id                = stcd.process_id(+)
               and stch.chunk_id                  = stcd.chunk_id(+)
               and UPPER(stch.code_type)          = UPPER(stcd.code_type (+))
               and UPPER(stch.code_type)          = cdd_cdh_fk.code_type (+)
               and UPPER(stcd.code)               = pk_code_detail.code (+)
               and UPPER(stcd.code_type)          = pk_code_detail.code_type (+)
               and UPPER(stcd.code)               = uk_code_detail.code (+)
               and UPPER(stcd.code_type)          = uk_code_detail.code_type (+)
   UNION ALL
      select pk_code_detail.rowid                     as pk_code_detail_rid,
             uk_code_detail.rowid                     as uk_code_detail_rid,
             cdd_cdh_fk.rowid                         as cdd_cdh_fk_rid,
             UPPER(pk_code_detail.required_ind)       as old_required_ind,
             stch.rowid                               as stch_rid,
             UPPER(stch.action)                       as stch_action,
             stch.row_seq                             as stch_row_seq,
             nvl(stch.code_type,CDD_CDH_FK.code_type) as stch_code_type,
             stch.code_type_desc                      as stch_code_type_desc,
             UPPER(stcd.action)                       as stcd_action,
             stcd.row_seq                             as stcd_row_seq,
             stcd.rowid                               as stcd_rid,
             UPPER(stcd.code_type)                    as stcd_code_type,
             UPPER(stcd.code)                         as stcd_code,
             stcd.code_desc                           as stcd_code_desc,
             UPPER(stcd.required_ind)                 as stcd_required_ind,
             stcd.code_seq                            as stcd_code_seq,
             stch.process_id                          as stch_process_id,
             stcd.process_id                          as stcd_process_id,
             stch.chunk_id                            as stch_chunk_id,
             stcd.chunk_id                            as Stcd_Chunk_Id,
             cdd_cdh_fk.code_type                     as old_code_type,
             cdd_cdh_fk.code_type_desc                as old_code_type_desc,
             pk_code_detail.code_type                 as old_det_code_type,
             pk_code_detail.code                      as old_code,
             pk_code_detail.code_desc                 as old_code_desc,
             pk_code_detail.code_seq                  as old_code_seq
        from svc_code_detail stcd,
             svc_code_head stch,
             code_head cdd_cdh_fk,
             code_detail pk_code_detail,
             code_detail uk_code_detail
       where stcd.process_id                = I_process_id
         and stcd.chunk_id                  = I_chunk_id
         and stcd.process_id                = stch.process_id(+)
         and stcd.chunk_id                  = stch.chunk_id(+)
         and UPPER(stcd.code_type)          = UPPER(stch.code_type (+))
         and UPPER(stcd.code_type)          = cdd_cdh_fk.code_type (+)
         and UPPER(stcd.code)               = pk_code_detail.code (+)
         and UPPER(stcd.code_type)          = pk_code_detail.code_type (+)
         and UPPER(stcd.code)               = uk_code_detail.code (+)
         and UPPER(stcd.code_type)          = uk_code_detail.code_type (+)
         and stch.code_type is NULL) t;


   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER %ROWTYPE;
   Lp_errors_tab       errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   Lp_s9t_errors_tab   s9t_errors_tab_typ;


   Type CODE_DTL_TL_TAB IS TABLE OF CODE_DETAIL_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit     CONSTANT NUMBER(12) := 1000;
   LP_primary_lang         LANG.LANG%TYPE;

--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------

PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2)IS
BEGIN
   LP_s9t_errors_tab.extend();
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).ERROR_SEQ_NO         := s9t_errors_seq.nextval;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).ERROR_KEY            :=(
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );

   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.count()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN

   LP_errors_tab.extend();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   LP_errors_tab(LP_errors_tab.COUNT()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id   NUMBER) IS

   L_sheets           s9t_pkg.names_map_typ;
   CODE_HEAD_cols     s9t_pkg.names_map_typ;
   CODE_DETAIL_cols   s9t_pkg.names_map_typ;
   CODE_DTL_TL_cols   s9t_pkg.names_map_typ;

BEGIN

   L_sheets                    := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   CODE_HEAD_cols              := S9T_PKG.GET_COL_NAMES(I_file_id,CODE_HEAD_sheet);
   CODE_DETAIL_cols            := S9T_PKG.GET_COL_NAMES(I_file_id,CODE_DETAIL_sheet);
   CODE_HEAD$ACTION            := CODE_HEAD_cols('ACTION');
   CODE_HEAD$CODE_TYPE_DESC    := CODE_HEAD_cols('CODE_TYPE_DESC');
   CODE_HEAD$CODE_TYPE         := CODE_HEAD_cols('CODE_TYPE');
   CODE_DETAIL$Action          := CODE_DETAIL_cols('ACTION');
   CODE_DETAIL$CODE_TYPE       := CODE_DETAIL_cols('CODE_TYPE');
   CODE_DETAIL$CODE            := CODE_DETAIL_cols('CODE');
   CODE_DETAIL$CODE_DESC       := CODE_DETAIL_cols('CODE_DESC');
   CODE_DETAIL$REQUIRED_IND    := CODE_DETAIL_cols('REQUIRED_IND');
   CODE_DETAIL$CODE_SEQ        := CODE_DETAIL_cols('CODE_SEQ');

   CODE_DTL_TL_cols            := s9t_pkg.get_col_names(I_file_id,CODE_DETAIL_TL_sheet);
   CODE_DETAIL_TL$Action       := CODE_DTL_TL_cols('ACTION');
   CODE_DETAIL_TL$LANG         := CODE_DTL_TL_cols('LANG');
   CODE_DETAIL_TL$CODE_TYPE    := CODE_DTL_TL_cols('CODE_TYPE');
   CODE_DETAIL_TL$CODE         := CODE_DTL_TL_cols('CODE');
   CODE_DETAIL_TL$CODE_DESC    := CODE_DTL_TL_cols('CODE_DESC');

END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_CODE_HEAD(I_file_id IN NUMBER)IS
BEGIN
   insert INTO TABLE
               (select ss.s9t_rows
                  from s9t_folder sf,
                       TABLE(sf.s9t_file_obj.sheets) ss
                 where sf.file_id    = I_file_id
                   and ss.sheet_name = CODE_HEAD_sheet)
   select s9t_row(S9T_CELLS(CORESVC_CODE_HEAD.action_mod,
                            CODE_TYPE,
                            CODE_TYPE_DESC))
     from CODE_HEAD ;
END POPULATE_CODE_HEAD;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_CODE_DETAIL(I_file_id   IN   NUMBER)IS
BEGIN
   insert INTO TABLE
                     (select ss.s9t_rows
                        from s9t_folder sf,
                       table (sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = CODE_DETAIL_sheet)
   select s9t_row(S9T_CELLS(CORESVC_CODE_HEAD.action_mod,
                            CODE_TYPE,
                            CODE,
                            CODE_DESC,
                            REQUIRED_IND,
                            CODE_SEQ))
     from CODE_DETAIL ;
END POPULATE_CODE_DETAIL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_CODE_DTL_TL(I_file_id   IN   NUMBER) IS
BEGIN

   insert INTO TABLE
                     (select ss.s9t_rows
                        from s9t_folder sf,
                       table (sf.s9t_file_obj.sheets) ss
                       where sf.file_id  = I_file_id
                         and ss.sheet_name = CODE_DETAIL_TL_sheet)
   select s9t_row(S9T_CELLS(CORESVC_CODE_HEAD.action_mod,
                            LANG,
                            CODE_TYPE,
                            CODE,
                            CODE_DESC))
     from code_detail_tl
    where lang <> LP_primary_lang;

END POPULATE_CODE_DTL_TL;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS

   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.nextval;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||sysdate||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(CODE_HEAD_sheet);
   L_file.sheets(L_file.get_sheet_index(CODE_HEAD_sheet)).column_headers := s9t_cells('ACTION',
                                                                                      'CODE_TYPE',
                                                                                      'CODE_TYPE_DESC');

   L_file.add_sheet(CODE_DETAIL_sheet);
   L_file.sheets(L_file.get_sheet_index(CODE_DETAIL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                        'CODE_TYPE',
                                                                                        'CODE',
                                                                                        'CODE_DESC',
                                                                                        'REQUIRED_IND',
                                                                                        'CODE_SEQ');

   L_file.add_sheet(CODE_DETAIL_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(CODE_DETAIL_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                           'LANG',
                                                                                           'CODE_TYPE',
                                                                                           'CODE',
                                                                                           'CODE_DESC');
   S9T_PKG.SAVE_OBJ(L_file);

END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_CODE_HEAD.CREATE_S9T';
   L_file      s9t_file;

BEGIN

   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   INIT_S9T(O_file_id);
   --populate column lists
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_CODE_HEAD(O_file_id);
      POPULATE_CODE_DETAIL(O_file_id);
      POPULATE_CODE_DTL_TL(O_file_id);
      commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CODE_HEAD(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id   IN   SVC_CODE_HEAD.PROCESS_ID%TYPE)IS

   TYPE SVC_CODE_HEAD_COL_TYP IS TABLE OF SVC_CODE_HEAD%ROWTYPE;
   L_temp_rec        SVC_CODE_HEAD%ROWTYPE;
   svc_CODE_HEAD_col svc_CODE_HEAD_COL_TYP :=NEW svc_CODE_HEAD_COL_TYp();
   L_process_id      SVC_CODE_HEAD.PROCESS_ID%TYPE;
   L_error           BOOLEAN:=FALSE;
   L_default_rec     SVC_CODE_HEAD%ROWTYPE;
   cursor C_MANDATORY_IND IS
      select  CODE_TYPE_DESC_mi,
              CODE_TYPE_mi,
              1 as dummy
        from  (select column_key,
                      mandatory
                 from s9t_tmpL_cols_def
                where template_key  = CORESVC_CODE_HEAD.template_key
                  and wksht_key     = 'CODE_HEAD')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('CODE_TYPE_DESC' as CODE_TYPE_DESC,
                                                                'CODE_TYPE'      as CODE_TYPE,
                                                                NULL             as dummy));
   L_MI_REC C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CODE_HEAD';
   L_pk_columns    VARCHAR2(255)  := 'Code Type';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR I_rec IN
             (select  CODE_TYPE_DESC_dv,
                      CODE_TYPE_dv,
                      NULL as dummy

                from  (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key  = CORESVC_CODE_HEAD.template_key
                          and wksht_key     = 'CODE_HEAD')

                PIVOT (MAX(default_value) as dv FOR (column_key) IN ('CODE_TYPE_DESC' as CODE_TYPE_DESC,
                                                                     'CODE_TYPE'      as CODE_TYPE,
                                                                     NULL             as dummy)))


   LOOP
   BEGIN
      L_default_rec.CODE_TYPE_DESC := I_rec.CODE_TYPE_DESC_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'CODE_HEAD',
                         NULL,
                         'CODE_TYPE_DESC',
                         'INV_DEFAULT',
                         SQLERRM);
   END;

   BEGIN
      L_default_rec.CODE_TYPE := I_rec.CODE_TYPE_dv;
   EXCEPTION
      when OTHERS then
        WRITE_S9T_ERROR(I_file_id,
                        'CODE_HEAD',
                        NULL,
                        'CODE_TYPE',
                        'INV_DEFAULT',
                        SQLERRM);
   END;
   END LOOP;

 --Get mandatory indicators
    open  C_MANDATORY_IND;
   fetch  C_MANDATORY_IND
    INTO  L_mi_rec;
   close  C_MANDATORY_IND;
   FOR I_rec IN
            (select r.get_cell(CODE_HEAD$ACTION)                    as ACTION,
                    r.get_cell(CODE_HEAD$CODE_TYPE_DESC)            as CODE_TYPE_DESC,
                    UPPER(r.get_cell(CODE_HEAD$CODE_TYPE))          as CODE_TYPE,
                    r.get_row_seq()                                 as row_seq
               from s9t_folder sf,
                    TABLE(sf.s9t_file_obj.sheets) ss,
                    TABLE(ss.s9t_rows) r
              where sf.file_id  = I_file_id
                and ss.sheet_name = SHEET_NAME_TRANS(CODE_HEAD_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := I_rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
   BEGIN
      L_temp_rec.Action := I_rec.Action;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                        CODE_HEAD_sheet,
                        I_rec.row_seq,
                        action_column,
                        SQLCODE,
                        SQLERRM);
         L_error := TRUE;
   END;

   BEGIN
      L_temp_rec.CODE_TYPE_DESC := I_rec.CODE_TYPE_DESC;
   EXCEPTION
       when OTHERS then
          WRITE_S9T_ERROR(I_file_id,
                          CODE_HEAD_sheet,
                          I_rec.row_seq,
                          'CODE_TYPE_DESC',
                          SQLCODE,
                          SQLERRM);
          L_error := TRUE;
   END;
   BEGIN
      L_temp_rec.CODE_TYPE := UPPER(I_rec.CODE_TYPE);
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         CODE_HEAD_sheet,
                         I_rec.row_seq,
                         'CODE_TYPE',
                         SQLCODE,
                         SQLERRM);
          L_error := TRUE;
   END;
   if I_rec.action = CORESVC_CODE_HEAD.action_new then
      L_temp_rec.CODE_TYPE_DESC := NVL( L_temp_rec.CODE_TYPE_DESC,L_default_rec.CODE_TYPE_DESC);
      L_temp_rec.CODE_TYPE      := NVL( L_temp_rec.CODE_TYPE,L_default_rec.CODE_TYPE);
   end if;

   if NOT (L_temp_rec.CODE_TYPE is NOT NULL)then
      WRITE_S9T_ERROR(I_file_id,
                      CODE_HEAD_sheet,
                      I_rec.row_seq,
                      NULL,
                      NULL,
                      SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
      L_error := TRUE;
   end if;

   if NOT L_error then
      svc_CODE_HEAD_col.extend();
      svc_CODE_HEAD_col(svc_CODE_HEAD_col.count()):=L_temp_rec;
   end if;
   END LOOP;
BEGIN
   forall i IN 1..svc_CODE_HEAD_col.count SAVE EXCEPTIONS
   Merge INTO SVC_CODE_HEAD st USING
   (select
            (case
             when L_mi_rec.CODE_TYPE_DESC_mi    = 'N'
             and  svc_CODE_HEAD_col(i).action = coresvc_code_head.action_mod
             and  s1.CODE_TYPE_DESC             IS NULL
             then mt.CODE_TYPE_DESC
             else s1.CODE_TYPE_DESC
             end) as CODE_TYPE_DESC,
            (case
             when L_mi_rec.CODE_TYPE_mi    = 'N'
             and  svc_CODE_HEAD_col(i).action = coresvc_code_head.action_mod
             and  s1.CODE_TYPE             IS NULL
             then mt.CODE_TYPE
             else s1.CODE_TYPE
             end) as CODE_TYPE,
             NULL as dummy
     from  (select  svc_CODE_HEAD_col(i).CODE_TYPE_DESC as CODE_TYPE_DESC,
                    svc_CODE_HEAD_col(i).CODE_TYPE as CODE_TYPE,
                    NULL as dummy
             from   dual) s1,CODE_HEAD mt
    where  mt.CODE_TYPE (+) = s1.CODE_TYPE)
           sq ON (st.CODE_TYPE      = sq.CODE_TYPE and
                  svc_CODE_HEAD_col(i).action in (CORESVC_CODE_HEAD.action_mod,CORESVC_CODE_HEAD.action_del))
   when matched then
      update
         set process_id        = svc_code_head_col(i).process_id ,
             chunk_id          = svc_code_head_col(i).chunk_id ,
             row_seq           = svc_code_head_col(i).row_seq ,
             action            = svc_code_head_col(i).action,
             process$status    = svc_code_head_col(i).process$status ,
             code_type_desc    = sq.code_type_desc ,
             create_id         = svc_code_head_col(i).create_id ,
             create_datetime   = svc_code_head_col(i).create_datetime ,
             last_upd_id       = svc_code_head_col(i).last_upd_id ,
             last_upd_datetime = svc_code_head_col(i).last_upd_datetime
   when NOT matched then
      insert(process_id ,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             code_type_desc ,
             code_type ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values (svc_code_head_col(i).process_id ,
              svc_code_head_col(i).chunk_id ,
              svc_code_head_col(i).row_seq ,
              svc_code_head_col(i).action ,
              svc_code_head_col(i).process$status ,
              sq.code_type_desc ,
              sq.code_type ,
              svc_code_head_col(i).create_id ,
              svc_code_head_col(i).create_datetime ,
              svc_code_head_col(i).last_upd_id ,
              svc_code_head_col(i).last_upd_datetime);

EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.count
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          CODE_HEAD_sheet,
                          svc_CODE_HEAD_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
   END;
END PROCESS_S9T_CODE_HEAD;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CODE_DETAIL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id   IN   SVC_CODE_DETAIL.PROCESS_ID%TYPE) IS
   TYPE svc_CODE_DETAIL_coL_typ IS TABLE OF SVC_CODE_DETAIL%ROWTYPE;
   L_temp_rec          SVC_CODE_DETAIL%ROWTYPE;
   svc_CODE_DETAIL_col svc_CODE_DETAIL_coL_typ :=NEW svc_CODE_DETAIL_coL_typ();
   L_process_id        SVC_CODE_DETAIL.process_id%type;
   L_error             BOOLEAN:=FALSE;
   L_default_rec       SVC_CODE_DETAIL%ROWTYPE;

   cursor C_MANDATORY_IND IS
      select   code_type_mi,
               code_mi,
               code_desc_mi,
               required_ind_mi,
               code_seq_mi,
               1 as dummy

        from   (select column_key,
                       mandatory
                  from s9t_tmpL_cols_def
                 where template_key  = CORESVC_CODE_HEAD.template_key
                   and wksht_key     = 'CODE_DETAIL')
      PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('CODE_TYPE'    as CODE_TYPE,
                                                       'CODE'         as CODE,
                                                       'CODE_DESC'    as CODE_DESC,
                                                       'REQUIRED_IND' as REQUIRED_IND,
                                                       'CODE_SEQ'     as CODE_SEQ,
                                                       NULL as dummy));
      L_mi_rec C_MANDATORY_IND%ROWTYPE;
      DML_ERRORS EXCEPTION;
      PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
      L_table         VARCHAR2(30)   := 'SVC_CODE_DETAIL';
      L_pk_columns    VARCHAR2(255)  := 'Code,Code Type';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN  (select  CODE_TYPE_dv,
                        CODE_dv,
                        CODE_DESC_dv,
                        REQUIRED_IND_dv,
                        CODE_SEQ_dv,
                        NULL as dummy
                 from   (select column_key,
                                default_value
                           from s9t_tmpL_cols_def
                          where template_key  = CORESVC_CODE_HEAD.template_key
                            and wksht_key     = 'CODE_DETAIL')
      PIVOT (MAX(default_value) as dv FOR (column_key) IN ('CODE_TYPE'     as CODE_TYPE,
                                                            'CODE'         as CODE,
                                                            'CODE_DESC'    as CODE_DESC,
                                                            'REQUIRED_IND' as REQUIRED_IND,
                                                            'CODE_SEQ'     as CODE_SEQ,
                                                             NULL          as  dummy)))

   LOOP

   BEGIN
   L_default_rec.CODE_TYPE := UPPER(rec.CODE_TYPE_dv);

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,'CODE_DETAIL',NULL,'CODE_TYPE','INV_DEFAULT',SQLERRM);
   END;


   BEGIN
   L_default_rec.CODE := UPPER(rec.CODE_dv);

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,'CODE_DETAIL',NULL,'CODE','INV_DEFAULT',SQLERRM);
   END;


   BEGIN
   L_default_rec.CODE_DESC := rec.CODE_DESC_dv;

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,'CODE_DETAIL',NULL,'CODE_DESC','INV_DEFAULT',SQLERRM);
   END;


   BEGIN
   L_default_rec.REQUIRED_IND := rec.REQUIRED_IND_dv;

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,'CODE_DETAIL',NULL,'REQUIRED_IND','INV_DEFAULT',SQLERRM);

   END;

   BEGIN
      L_default_rec.CODE_SEQ := rec.CODE_SEQ_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,'CODE_DETAIL',NULL,'CODE_SEQ','INV_DEFAULT',SQLERRM);

   END;
   END LOOP;

   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND
   into  L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
         (select   r.get_cell(CODE_DETAIL$ACTION)                 as ACTION,
                   UPPER(r.get_cell(CODE_DETAIL$CODE_TYPE))       as CODE_TYPE,
                   UPPER(r.get_cell(CODE_DETAIL$CODE))            as CODE,
                   r.get_cell(CODE_DETAIL$CODE_DESC)              as CODE_DESC,
                   r.get_cell(CODE_DETAIL$REQUIRED_IND)           as REQUIRED_IND,
                   r.get_cell(CODE_DETAIL$CODE_SEQ)               as CODE_SEQ,
                   r.get_row_seq()                                as row_seq
            from   s9t_folder sf,
                   TABLE(sf.s9t_file_obj.sheets) ss,
                   TABLE(ss.s9t_rows) r
           where   sf.file_id  = I_file_id
             and   ss.sheet_name = sheet_name_trans(CODE_DETAIL_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := false;

      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      BEGIN
         L_temp_rec.CODE_TYPE := rec.CODE_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_sheet,
                            rec.row_seq,
                            'CODE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      BEGIN
         L_temp_rec.CODE := rec.CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_sheet,
                            rec.row_seq,
                            'CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      BEGIN
         L_temp_rec.CODE_DESC := rec.CODE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_sheet,
                            rec.row_seq,
                            'CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      BEGIN
         L_temp_rec.REQUIRED_IND := rec.REQUIRED_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_sheet,
                            rec.row_seq,
                            'REQUIRED_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      BEGIN
         L_temp_rec.CODE_SEQ := rec.CODE_SEQ;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_sheet,
                            rec.row_seq,
                            'CODE_SEQ',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;


   if rec.action = CORESVC_CODE_HEAD.action_new then
      L_temp_rec.CODE_TYPE    := NVL( L_temp_rec.CODE_TYPE,L_default_rec.CODE_TYPE);
      L_temp_rec.CODE         := NVL( L_temp_rec.CODE,L_default_rec.CODE);
      L_temp_rec.CODE_DESC    := NVL( L_temp_rec.CODE_DESC,L_default_rec.CODE_DESC);
      L_temp_rec.REQUIRED_IND := NVL( L_temp_rec.REQUIRED_IND,L_default_rec.REQUIRED_IND);
      L_temp_rec.CODE_SEQ     := NVL( L_temp_rec.CODE_SEQ,L_default_rec.CODE_SEQ);
   end if;

   if L_temp_rec.CODE is NULL
      or L_temp_rec.CODE_TYPE is NULL then
      WRITE_S9T_ERROR(I_file_id,
                      CODE_DETAIL_sheet,
                      rec.row_seq,
                      NULL,
                      NULL,
                      SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
      L_error := true;
   end if;

   if NOT L_error then
      svc_CODE_DETAIL_col.extend();
      svc_CODE_DETAIL_col(svc_CODE_DETAIL_col.count()):=L_temp_rec;
   end if;
   END LOOP;

   BEGIN
   FORALL i IN 1..svc_CODE_DETAIL_col.count SAVE EXCEPTIONS
      Merge INTO SVC_CODE_DETAIL st USING
      (select (case
               when L_mi_rec.CODE_TYPE_mi    = 'N'
               and  svc_CODE_DETAIL_col(i).action = coresvc_code_head.action_mod
               and  s1.CODE_TYPE             IS NULL
               then mt.CODE_TYPE
               else s1.CODE_TYPE
               end) as CODE_TYPE,
              (case
               when L_mi_rec.CODE_mi    = 'N'
               and  svc_CODE_DETAIL_col(i).action = coresvc_code_head.action_mod
               and  s1.CODE             IS NULL
               then mt.CODE
               else s1.CODE
               end) as CODE,
              (case
               when L_mi_rec.CODE_DESC_mi    = 'N'
               and  svc_CODE_DETAIL_col(i).action = coresvc_code_head.action_mod
               and  s1.CODE_DESC             IS NULL
               then mt.CODE_DESC
               else s1.CODE_DESC
               end) as CODE_DESC,
              (case
               when L_mi_rec.REQUIRED_IND_mi    = 'N'
               and  svc_CODE_DETAIL_col(i).action = coresvc_code_head.action_mod
               and  s1.REQUIRED_IND             IS NULL
               then mt.REQUIRED_IND
               else s1.REQUIRED_IND
               end) as REQUIRED_IND,
              (case
               when L_mi_rec.CODE_SEQ_mi    = 'N'
               and  svc_CODE_DETAIL_col(i).action = coresvc_code_head.action_mod
               and  s1.CODE_SEQ             IS NULL
               then mt.CODE_SEQ
               else s1.CODE_SEQ
               end) as CODE_SEQ,
               NULL as dummy
        from  (select  svc_CODE_DETAIL_col(i).CODE_TYPE    as CODE_TYPE,
                       svc_CODE_DETAIL_col(i).CODE         as CODE,
                       svc_CODE_DETAIL_col(i).CODE_DESC    as CODE_DESC,
                       svc_CODE_DETAIL_col(i).REQUIRED_IND as REQUIRED_IND,
                       svc_CODE_DETAIL_col(i).CODE_SEQ     as CODE_SEQ,
                       NULL as dummy
                 from  dual) s1,CODE_DETAIL mt
                where  mt.CODE (+)         = s1.CODE
                  and  mt.CODE_TYPE (+)    = s1.CODE_TYPE   )
         sq ON (st.CODE           = sq.CODE
           and  st.CODE_TYPE      = sq.CODE_TYPE
           and  svc_CODE_DETAIL_col(i).action in (CORESVC_CODE_HEAD.action_mod,CORESVC_CODE_HEAD.action_del))

   when matched then
      update
      set process_id        = svc_code_detaiL_col(i).process_id ,
          chunk_id          = svc_code_detaiL_col(i).chunk_id ,
          row_seq           = svc_code_detaiL_col(i).row_seq ,
          action            = svc_code_detaiL_col(i).action,
          process$status    = svc_code_detaiL_col(i).process$status ,
          code_seq          = sq.code_seq,
          required_ind      = sq.required_ind ,
          code_desc         = sq.code_desc ,
          create_id         = svc_code_detaiL_col(i).create_id ,
          create_datetime   = svc_code_detaiL_col(i).create_datetime ,
          last_upd_id       = svc_code_detaiL_col(i).last_upd_id ,
          last_upd_datetime = svc_code_detaiL_col(i).last_upd_datetime
   when not matched then
      insert  (process_id,
               chunk_id,
               row_seq,
               action,
               process$status,
               code_type,
               code,
               code_desc,
               required_ind,
               code_seq,
               create_id,
               create_datetime,
               last_upd_id,
               last_upd_datetime)
      values  (svc_code_detaiL_col(i).process_id,
               svc_code_detaiL_col(i).chunk_id,
               svc_code_detaiL_col(i).row_seq,
               svc_code_detaiL_col(i).action,
               svc_code_detaiL_col(i).process$status,
               sq.code_type,
               sq.code,
               sq.code_desc,
               sq.required_ind,
               sq.code_seq,
               svc_code_detaiL_col(i).create_id,
               svc_code_detaiL_col(i).create_datetime,
               svc_code_detaiL_col(i).last_upd_id,
               svc_code_detaiL_col(i).last_upd_datetime);

   EXCEPTION
      when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.count
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          CODE_DETAIL_sheet,
                          svc_CODE_DETAIL_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
      END LOOP;
   END;
END PROCESS_S9T_CODE_DETAIL;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CODE_DTL_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id   IN   SVC_CODE_DETAIL_TL.PROCESS_ID%TYPE) IS

   TYPE svc_CODE_DTL_TL_col_typ IS TABLE OF SVC_CODE_DETAIL_TL%ROWTYPE;
   L_temp_rec            SVC_CODE_DETAIL_TL%ROWTYPE;
   svc_CODE_DTL_TL_col   svc_CODE_DTL_TL_col_typ := NEW svc_CODE_DTL_TL_col_typ();
   L_process_id          SVC_CODE_DETAIL_TL.PROCESS_ID%type;
   L_error               BOOLEAN                 := FALSE;
   L_default_rec         SVC_CODE_DETAIL_TL%ROWTYPE;

   cursor C_MANDATORY_IND IS
      select lang_mi,
             code_type_mi,
             code_mi,
             code_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_CODE_HEAD.template_key
                 and wksht_key     = CODE_DETAIL_TL_sheet)
       PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('LANG'      as lang,
                                                        'CODE_TYPE' as code_type,
                                                        'CODE'      as code,
                                                        'CODE_DESC' as code_desc,
                                                        NULL        as dummy));

      L_mi_rec           C_MANDATORY_IND%ROWTYPE;
      DML_ERRORS         EXCEPTION;
      PRAGMA             EXCEPTION_INIT(DML_ERRORS, -24381);
      L_table            VARCHAR2(30)            := 'SVC_CODE_DETAIL_TL';
      L_pk_columns       VARCHAR2(255)           := 'Code,Code Type, Lang';
      L_error_code       NUMBER;
      L_error_msg        RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN  (select lang_dv,
                       code_type_dv,
                       code_dv,
                       code_desc_dv,
                       NULL as dummy
                 from  (select column_key,
                               default_value
                          from s9t_tmpL_cols_def
                         where template_key  = CORESVC_CODE_HEAD.template_key
                           and wksht_key     = CODE_DETAIL_TL_sheet)
      PIVOT (MAX(default_value) as dv FOR (column_key) IN ('LANG'      as lang,
                                                           'CODE_TYPE' as code_type,
                                                           'CODE'      as code,
                                                           'CODE_DESC' as code_desc,
                                                           NULL        as dummy)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            NULL,
                            'LANG',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.code_type := UPPER(rec.code_type_dv);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            NULL,
                            'CODE_TYPE',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.code := UPPER(rec.code_dv);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            NULL,
                            'CODE',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.code_desc := rec.code_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            NULL,
                            'CODE_DESC',
                            'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;

   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into  L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(code_detail_tl$action)           as action,
                      r.get_cell(code_detail_tl$lang)             as lang,
                      UPPER(r.get_cell(code_detail_tl$code_type)) as code_type,
                      UPPER(r.get_cell(code_detail_tl$code))      as code,
                      r.get_cell(code_detail_tl$code_desc)        as code_desc,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(CODE_DETAIL_TL_sheet))
   LOOP
      L_temp_rec                := NULL;
      L_temp_rec.process_id     := I_process_id;
      L_temp_rec.chunk_id       := 1;
      L_temp_rec.row_seq        := rec.row_seq;
      L_temp_rec.process$status := 'N';
      L_error                   := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.code_type := rec.code_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            rec.row_seq,
                            'CODE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.code := rec.code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            rec.row_seq,
                            'CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.code_desc := rec.code_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            rec.row_seq,
                            'CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_CODE_HEAD.action_new then
         L_temp_rec.lang      := NVL(L_temp_rec.lang,L_default_rec.lang);
         L_temp_rec.code_type := NVL(L_temp_rec.code_type,L_default_rec.code_type);
         L_temp_rec.code      := NVL(L_temp_rec.code,L_default_rec.code);
         L_temp_rec.code_desc := NVL(L_temp_rec.code_desc,L_default_rec.code_desc);
      end if;

      if NOT (L_temp_rec.lang is NOT NULL and L_temp_rec.code_type is NOT NULL and L_temp_rec.code is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         CODE_DETAIL_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_CODE_DTL_TL_col.extend();
         svc_CODE_DTL_TL_col(svc_CODE_DTL_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..svc_CODE_DTL_TL_col.COUNT SAVE EXCEPTIONS
         merge INTO svc_code_detail_tl st
         USING (select (case
                        when L_mi_rec.lang_mi = 'N'
                        and  svc_CODE_DTL_TL_col(i).action = CORESVC_CODE_HEAD.action_mod
                        and  s1.lang is NULL
                        then mt.lang
                        else s1.lang
                        end) as lang,
                       (case
                        when L_mi_rec.code_type_mi = 'N'
                        and  svc_CODE_DTL_TL_col(i).action = CORESVC_CODE_HEAD.action_mod
                        and  s1.code_type is NULL
                        then mt.code_type
                        else s1.code_type
                        end) as code_type,
                       (case
                        when L_mi_rec.code_mi = 'N'
                        and  svc_CODE_DTL_TL_col(i).action = CORESVC_CODE_HEAD.action_mod
                        and  s1.code is NULL
                        then mt.code
                        else s1.code
                        end) as code,
                       (case
                        when L_mi_rec.code_desc_mi = 'N'
                        and  svc_CODE_DTL_TL_col(i).action = CORESVC_CODE_HEAD.action_mod
                        and  s1.code_desc is NULL
                        then mt.code_desc
                        else s1.code_desc
                        end) as code_desc,
                       NULL as dummy
                  from (select svc_CODE_DTL_TL_col(i).lang      as lang,
                               svc_CODE_DTL_TL_col(i).code_type as code_type,
                               svc_CODE_DTL_TL_col(i).code      as code,
                               svc_CODE_DTL_TL_col(i).code_desc as code_desc,
                               NULL as dummy
                          from dual) s1,
                       code_detail_tl mt
                 where mt.lang (+)      = s1.lang
                   and mt.code (+)      = s1.code
                   and mt.code_type (+) = s1.code_type) sq
                   ON (st.lang          = sq.lang
                       and st.code      = sq.code
                       and st.code_type = sq.code_type
                       and svc_CODE_DTL_TL_col(i).action in (CORESVC_CODE_HEAD.action_mod,
                                                             CORESVC_CODE_HEAD.action_del))
         when matched then
            update set process_id     = svc_CODE_DTL_TL_col(i).process_id,
                       chunk_id       = svc_CODE_DTL_TL_col(i).chunk_id,
                       row_seq        = svc_CODE_DTL_TL_col(i).row_seq,
                       action         = svc_CODE_DTL_TL_col(i).action,
                       process$status = svc_CODE_DTL_TL_col(i).process$status,
                       code_desc      = sq.code_desc
         when NOT matched then
            insert  (process_id,
                     chunk_id,
                     row_seq,
                     action,
                     process$status,
                     lang,
                     code_type,
                     code,
                     code_desc)
            values  (svc_CODE_DTL_TL_col(i).process_id,
                     svc_CODE_DTL_TL_col(i).chunk_id,
                     svc_CODE_DTL_TL_col(i).row_seq,
                     svc_CODE_DTL_TL_col(i).action,
                     svc_CODE_DTL_TL_col(i).process$status,
                     sq.lang,
                     sq.code_type,
                     sq.code,
                     sq.code_desc);

   EXCEPTION
      when DML_ERRORS then
         for i in 1..SQL%BULK_EXCEPTIONS.COUNT
         LOOP
            L_error_code := SQL%BULK_EXCEPTIONS(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            CODE_DETAIL_TL_sheet,
                            svc_CODE_DTL_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_CODE_DTL_TL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'CORESVC_CODE_HEAD.PROCESS_S9T';
   L_file             s9t_file;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN

   commit;--to ensure that the record in s9t_folder is commited
   S9T_PKG.ODS2OBJ(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.sheet_trans(L_file.template_key,L_file.user_lang);
      PROCESS_S9T_CODE_HEAD(I_file_id,I_process_id);
      PROCESS_S9T_CODE_DETAIL(I_file_id,I_process_id);
      PROCESS_S9T_CODE_DTL_TL(I_file_id,I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.count();

   forall i in 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
    commit;

   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------

FUNCTION CHECK_DELETE_CODE_HEAD(O_error_message   IN OUT   VARCHAR2,
                                O_exists          IN OUT   BOOLEAN,
                                I_rec             IN       C_SVC_CODE_HEAD%ROWTYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR(64)                       := 'CORESVC_CODE_HEAD.CHECK_DELETE_CODE_HEAD';
   L_error     BOOLEAN;
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_DETAIL';
   L_exists    VARCHAR(1);

   --cursor to check if there is any system required detail for the code_type
   cursor C_DETAIL_EXISTS is
      select 'x'
        from code_detail
       where code_detail.code_type = I_rec.stch_code_type
         and code_detail.required_ind = 'Y';

BEGIN
   open  C_DETAIL_EXISTS;
   fetch C_DETAIL_EXISTS into L_exists;
   if C_DETAIL_EXISTS%FOUND then
      WRITE_ERROR(I_rec.stch_process_id,
                  svc_admin_upld_er_seq.nextval,
                  I_rec.stch_chunk_id,
                  L_table,
                  I_rec.stch_row_seq,
                  'CODE_TYPE',
                  'SYSTEM_REQ_CODE');
      O_exists := TRUE;
      close C_DETAIL_EXISTS;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_DETAIL_EXISTS%ISOPEN then
         close C_DETAIL_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_DELETE_CODE_HEAD;
--------------------------------------------------------------------------------
FUNCTION CHECK_CODE_SEQ_UNIQUE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists             IN OUT BOOLEAN,
                               I_rec                IN     C_SVC_CODE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)  := 'CORESVC_CODE_HEAD.CHECK_CODE_SEQ_UNIQUE';
   L_check_seq_no    VARCHAR2(1)   := 'N';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_CODE_DETAIL';

   cursor C_CHECK_SEQ_NO is
      select 'Y' from code_detail
       where code_detail.code_type = I_rec.stcd_code_type
         and code_detail.code_seq  = I_rec.stcd_code_seq
         and code_detail.code     != I_rec.stcd_code;

BEGIN
   open  C_CHECK_SEQ_NO;
   fetch C_CHECK_SEQ_NO into L_check_seq_no;
   close C_CHECK_SEQ_NO;

   if L_check_seq_no = 'Y' then
      WRITE_ERROR(I_rec.stcd_process_id,
                  svc_admin_upld_er_seq.nextval,
                  I_rec.stcd_chunk_id,
                  L_table,
                  I_rec.stcd_row_seq,
                  'CODE_SEQ',
                  'ENT_SEQ_NUMB');
      O_exists:= TRUE;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_CHECK_SEQ_NO%ISOPEN then
         close C_CHECK_SEQ_NO;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END CHECK_CODE_SEQ_UNIQUE;
--------------------------------------------------------------------------------
FUNCTION EXEC_CODE_HEAD_INS  (O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_head_temp_rec   IN       CODE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_CODE_HEAD.EXEC_CODE_HEAD_INS';
BEGIN
   insert into code_head
        values I_code_head_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END EXEC_CODE_HEAD_INS;

----------------------------------------------------------------------------------------------
FUNCTION EXEC_CODE_HEAD_UPD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_code_head_temp_rec   IN       CODE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_CODE_HEAD.EXEC_CODE_HEAD_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_CODE_HEAD_UPD is
      select 'x'
        from code_head
       where code_type = I_code_head_temp_rec.code_type;

BEGIN
   open  C_LOCK_CODE_HEAD_UPD;
   close C_LOCK_CODE_HEAD_UPD;

   update code_head
      set row = I_code_head_temp_rec
    where code_type = I_code_head_temp_rec.code_type;

return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_code_head_temp_rec.code_type,
                                            NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_CODE_HEAD_UPD%ISOPEN then
         close C_LOCK_CODE_HEAD_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
     return FALSE;

END EXEC_CODE_HEAD_UPD;

--------------------------------------------------------------------------------
FUNCTION EXEC_CODE_HEAD_PRE_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_code_head_temp_rec   IN       CODE_HEAD%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_CODE_HEAD.EXEC_CODE_HEAD_PRE_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_CODEHEAD_PRE_DEL is
      select 'x'
        from code_detail
       where code_detail.code_type = I_code_head_temp_rec.code_type
         and code_detail.required_ind = 'N'
         for update nowait;

   cursor C_LOCK_CODE_DTL_TL_DEL is
      select 'x'
        from code_detail_tl
       where code_type = I_code_head_temp_rec.code_type
         for update nowait;

BEGIN
   L_table := 'CODE_DETAIL_TL';
   open C_LOCK_CODE_DTL_TL_DEL;
   close C_LOCK_CODE_DTL_TL_DEL;

   delete from code_detail_tl
         where code_type = I_code_head_temp_rec.code_type;

   L_table := 'CODE_DETAIL';
   open  C_LOCK_CODEHEAD_PRE_DEL;
   close C_LOCK_CODEHEAD_PRE_DEL;

   delete from code_detail
    where code_detail.code_type= I_code_head_temp_rec.code_type
      and code_detail.required_ind = 'N';

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_CODE_DTL_TL_DEL%ISOPEN then
         close C_LOCK_CODE_DTL_TL_DEL;
      end if;
      if C_LOCK_CODEHEAD_PRE_DEL%ISOPEN then
         close C_LOCK_CODEHEAD_PRE_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_code_head_temp_rec.code_type,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_LOCK_CODE_DTL_TL_DEL%ISOPEN then
         close C_LOCK_CODE_DTL_TL_DEL;
      end if;
      if C_LOCK_CODEHEAD_PRE_DEL%ISOPEN then
         close C_LOCK_CODEHEAD_PRE_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CODE_HEAD_PRE_DEL;

--------------------------------------------------------------------------------
FUNCTION EXEC_CODE_HEAD_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_code_head_temp_rec   IN       CODE_HEAD%ROWTYPE,
                            I_rec                  IN       C_SVC_CODE_HEAD%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_CODE_HEAD.EXEC_CODE_HEAD_DEL';
   L_svc_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CODE_HEAD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_HEAD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_CODE_HEAD_DEL is
      select 'x'
        from code_head
       where code_type = I_code_head_temp_rec.code_type
         for update nowait;

BEGIN
   if EXEC_CODE_HEAD_PRE_DEL(O_error_message,
                             I_code_head_temp_rec) = FALSE then
      return FALSE;
   end if;

   open  C_LOCK_CODE_HEAD_DEL;
   close C_LOCK_CODE_HEAD_DEL;

   delete from code_head
         where code_type = I_code_head_temp_rec.code_type;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_code_head_temp_rec.code_type,
                                            NULL);
      return FALSE;

   when OTHERS then
      if C_LOCK_CODE_HEAD_DEL%ISOPEN then
         close C_LOCK_CODE_HEAD_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CODE_HEAD_DEL;

---------------------------------------------------------------------------------
FUNCTION EXEC_CODE_DETAIL_INS  (O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_code_detail_temp_rec   IN       CODE_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_CODE_HEAD.EXEC_CODE_DETAIL_INS';
BEGIN

   insert into code_detail
        values I_code_detail_temp_rec;
return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END EXEC_CODE_DETAIL_INS;

----------------------------------------------------------------------------------------------
FUNCTION EXEC_CODE_DETAIL_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_detail_temp_rec   IN       CODE_DETAIL%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)  := 'CORESVC_CODE_HEAD.EXEC_CODE_DETAIL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_DETAIL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_CODE_DETAIL_UPD is
      select 'x'
        from code_detail
       where CODE      = I_code_detail_temp_rec.CODE
         and CODE_TYPE = I_code_detail_temp_rec.CODE_TYPE
         for update nowait;

BEGIN
   open  C_LOCK_CODE_DETAIL_UPD;
   close C_LOCK_CODE_DETAIL_UPD;

   update code_detail
      set row       = I_code_detail_temp_rec
    where CODE      = I_code_detail_temp_rec.CODE
      and CODE_TYPE = I_code_detail_temp_rec.CODE_TYPE;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_CODE_DETAIL_UPD%ISOPEN then
         close C_LOCK_CODE_DETAIL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_code_detail_temp_rec.CODE,
                                            I_code_detail_temp_rec.CODE_TYPE);
      return FALSE;
   when OTHERS then
      if C_LOCK_CODE_DETAIL_UPD%ISOPEN then
         close C_LOCK_CODE_DETAIL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END EXEC_CODE_DETAIL_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_CODE_DETAIL_PRE_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_code_detail_temp_rec   IN       CODE_DETAIL%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_CODE_HEAD.EXEC_CODE_DETAIL_PRE_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_DETAIL_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_CODEDET_PRE_DEL is
      select 'x'
        from code_detail_tl
       where code      = I_code_detail_temp_rec.code
         and code_type = I_code_detail_temp_rec.code_type
         for update nowait;

BEGIN
   open  C_LOCK_CODEDET_PRE_DEL;
   close C_LOCK_CODEDET_PRE_DEL;

   delete from code_detail_tl
    where code      = I_code_detail_temp_rec.code
      and code_type = I_code_detail_temp_rec.code_type;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_code_detail_temp_rec.CODE,
                                            I_code_detail_temp_rec.CODE_TYPE);
      return FALSE;

   when OTHERS then
      if C_LOCK_CODEDET_PRE_DEL%ISOPEN then
         close C_LOCK_CODEDET_PRE_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CODE_DETAIL_PRE_DEL;
--------------------------------------------------------------------------------
FUNCTION EXEC_CODE_DETAIL_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_detail_temp_rec   IN       CODE_DETAIL%ROWTYPE,
                              I_rec                    IN       C_SVC_CODE_HEAD%ROWTYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_CODE_HEAD.EXEC_CODE_DETAIL_DEL';
   L_svc_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CODE_DETAIL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_DETAIL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_CODE_DETAIL_DEL is
      select 'x'
        from code_detail
       where code      = I_code_detail_temp_rec.code
         and code_type = I_code_detail_temp_rec.code_type
         for update nowait;

BEGIN
   if EXEC_CODE_DETAIL_PRE_DEL(O_error_message,
                               I_code_detail_temp_rec) = FALSE then
      return FALSE;
   end if;

   open  C_LOCK_CODE_DETAIL_DEL;
   close C_LOCK_CODE_DETAIL_DEL;

   delete from code_detail
    where code      = I_code_detail_temp_rec.code
      and code_type = I_code_detail_temp_rec.code_type;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_LOCK_CODE_DETAIL_DEL%ISOPEN then
         close C_LOCK_CODE_DETAIL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_code_detail_temp_rec.code,
                                            I_code_detail_temp_rec.code_type);
      return FALSE;

   when OTHERS then
      if C_LOCK_CODE_DETAIL_DEL%ISOPEN then
         close C_LOCK_CODE_DETAIL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CODE_DETAIL_DEL;

--------------------------------------------------------------------------------
FUNCTION PROCESS_CODE_HEAD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_CODE_HEAD.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_CODE_HEAD.CHUNK_ID%TYPE )

RETURN BOOLEAN IS
   L_program             VARCHAR2(64)  := 'CORESVC_CODE_HEAD.PROCESS_CODE_HEAD';
   L_check               VARCHAR2(1);
   L_detail_error        BOOLEAN;
   L_process_error       BOOLEAN;
   L_head_process_error  BOOLEAN;
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CODE_HEAD';
   L_detail_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_CODE_DETAIL';
   L_exists              BOOLEAN;
   count_detail          NUMBER;
   L_exists_record       VARCHAR2(1);
   CODE_HEAD_temp_rec    CODE_HEAD%ROWTYPE;
   CODE_DETAIL_temp_rec  CODE_DETAIL%ROWTYPE;
   L_rowseq_count               NUMBER;
   c                            NUMBER      := 1;
   TYPE L_row_seq_tab_type IS TABLE OF SVC_CODE_DETAIL.ROW_SEQ%TYPE;
   L_row_seq_tab                       L_row_seq_tab_type;

   -- Cursor to check that at least one detail exists
   cursor C_DETAIL_ATLEAST(L_detail code_head.code_type%type) is
      select count(code_detail.code) into count_detail
        from code_detail
       where code_type = L_detail;

BEGIN
   L_row_seq_tab := L_row_seq_tab_type();

   FOR I_rec IN C_SVC_CODE_HEAD(I_process_id,I_chunk_id)
   LOOP

      L_detail_error       := FALSE;
      L_process_error      := FALSE;
      L_head_process_error := FALSE;
      L_error              := FALSE;

      --Validate code_head if rank is 1
      if I_rec.ch_rank = 1 then
         L_error := FALSE;
         L_row_seq_tab.DELETE;
         c := 1;

         -- Check if both actions are NULL or code_head action is other than NEW,MOD,DEL
         if (I_rec.stch_action IS NULL and I_rec.stcd_action IS NULL)
             or (I_rec.stch_action NOT IN (action_new,action_mod,action_del))then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.nextval,
                         I_chunk_id,
                         L_table,
                         I_rec.stch_row_seq,
                         'ACTION',
                         'INV_ACT');
            L_error :=TRUE;
         end if;

         if (I_rec.stch_action IS NULL
             and I_rec.stch_rid IS NOT NULL)then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.nextval,
                         I_chunk_id,
                         L_table,
                         I_rec.stch_row_seq,
                         'ACTION',
                         'INV_ACT');
            L_error :=TRUE;
         end if;

         if (I_rec.stcd_action IS NULL and I_rec.PK_CODE_DETAIL_rid IS NOT NULL)then
            WRITE_ERROR( I_process_id,
                         svc_admin_upld_er_seq.nextval,
                         I_chunk_id,
                         L_table,
                         I_rec.stcd_row_seq,
                         'ACTION',
                         'INV_ACT');

            L_error :=TRUE;
         end if;

         -- Check if code_type already exists
         if I_rec.stch_action = action_new and I_rec.CDD_CDH_FK_rid IS NOT NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_table,
                        I_rec.stch_row_seq,
                        'CODE_TYPE',
                        'DUP_CODE_HEAD');
            L_error :=TRUE;
         end if;

         -- Check if the given code_type is missing during action MOD, DEL
         if I_rec.stch_action IN (action_mod,action_del) and I_rec.CDD_CDH_FK_rid IS NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_table,
                        I_rec.stch_row_seq,
                        'CODE_TYPE',
                        'NO_RECORD');
            L_error :=TRUE;
         end if;

         -- Check if code_type_desc is NULL
         if I_rec.stch_action IN (action_new,action_mod) and I_rec.stch_code_type_desc  IS NULL then
              WRITE_ERROR(I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_table,
                          I_rec.stch_row_seq,
                          'CODE_TYPE_DESC',
                          'CODE_TYPE_DESC_REQ');
              L_error :=TRUE;
         end if;

         -- Validation for deleting the code_type from CODE_HEAD
         if I_rec.stch_action = action_del then
            if CHECK_DELETE_CODE_HEAD(O_error_message,
                                      L_error,
                                      I_rec)= FALSE then
              WRITE_ERROR(I_process_id,
                          svc_admin_upld_er_seq.nextval,
                          I_chunk_id,
                          L_table,I_rec.stch_row_seq,
                          NULL,
                          O_error_message);
              L_error := TRUE;
            end if;
         end if;
      end if; -- if rank=1

--------------------------------------------------------------------------------
-- Validations for code_detail table
--------------------------------------------------------------------------------
      -- Check if action other than (NEW, MOD,DEL)
      if I_rec.stcd_action NOT in (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_detail_table,
                     I_rec.stcd_row_seq,
                     'ACTION',
                     'INV_ACT');
         L_detail_error :=true;
      end if;

      -- Check if action is NEW and if code already exists
      if I_rec.stcd_action = action_new and I_rec.PK_CODE_DETAIL_rid IS NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_detail_table,
                     I_rec.stcd_row_seq,
                     'CODE',
                     'DUPL_CODES');
         L_detail_error :=true;
      end if;

      -- Check if action is MOD , DEL and if code doesnt exists
      if I_rec.stcd_action IN (action_mod,action_del) and I_rec.PK_CODE_DETAIL_rid IS NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_detail_table,
                     I_rec.stcd_row_seq,
                     'CODE',
                     'NO_RECORD');
         L_detail_error :=true;
      end if;

      if I_rec.stcd_action = action_new and I_rec.stch_action IS NULL and I_rec.cdd_cdh_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                  svc_admin_upld_er_seq.nextval,
                  I_chunk_id,
                  L_detail_table,
                  I_rec.stcd_row_seq,
                  'CODE_TYPE',
                  'INV_CODETYPE_ENTERED');
         L_detail_error :=true;
      end if;

      if I_rec.stcd_action IN (action_new, action_mod) then
         -- CODE_DESC is NULL
         if NOT(  I_rec.stcd_code_desc  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_detail_table,
                        I_rec.stcd_row_seq,
                        'CODE_DESC',
                        'CODE_DESC_REQ');
             L_detail_error :=true;
         end if;

         -- required_ind other than 'Y', 'N'   or NULL
         if I_rec.stcd_REQUIRED_IND  IS NULL or  I_rec.stcd_REQUIRED_IND NOT IN ( 'Y','N' ) then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_detail_table,
                        I_rec.stcd_row_seq,
                        'REQUIRED_IND',
                        'INV_Y_N_IND'); -- Invalid Required Indicator. Valid Values are 'Y' or 'N'
            L_detail_error :=true;
         end if;
      end if;

      if I_rec.stcd_action = action_new
         or  (I_rec.stcd_action = action_mod and I_rec.stcd_REQUIRED_IND = 'N')then
         if I_rec.stcd_code_seq is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_detail_table,
                        I_rec.stcd_row_seq,
                       'CODE_SEQ',
                       'ENT_CODE_SEQ');
            L_detail_error := TRUE;
         elsif I_rec.stcd_code_seq <= 0 then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_detail_table,
                        I_rec.stcd_row_seq,
                        'CODE_SEQ',
                        'GREATER_0');
            L_detail_error := TRUE;
         elsif CHECK_CODE_SEQ_UNIQUE(O_error_message,
                                  L_detail_error,
                                  I_rec)= FALSE then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_detail_table,
                        I_rec.stcd_row_seq,
                        'CODE_SEQ',
                        O_error_message);
            L_detail_error := TRUE;
         end if;
      end if;

      -- Check if action is MOD and if the required_ind with 'Y' is being changed to 'N'
      if I_rec.stcd_action = action_mod and I_rec.old_required_ind = 'Y' and I_rec.stcd_required_ind = 'N' then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_detail_table,
                     I_rec.stcd_row_seq,
                     'REQUIRED_IND',
                     'REQ_IND_NOT_UPD_TO_N');
         L_detail_error := TRUE;
      end if;

      -- Check if action is MOD and if the required_ind with 'Y' and code sequence  is being changed
      if I_rec.stcd_action = action_mod
         and I_rec.old_required_ind = 'Y'
         and I_rec.stcd_code_seq <> I_rec.old_code_seq then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_detail_table,
                     I_rec.stcd_row_seq,
                     'CODE_SEQ',
                     'CANNOT_MOD_DEL_CODE');
         L_detail_error := TRUE;
      end if;

      -- Check if action is MOD and if the required_ind with 'Y' and desc is being changed
      if I_rec.stcd_action = action_mod
         and I_rec.old_required_ind = 'Y'
         and I_rec.stcd_code_desc <> I_rec.old_code_desc then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_detail_table,
                     I_rec.stcd_row_seq,
                     'CODE_DESC',
                     'CANNOT_MOD_DEL_CODE');
         L_detail_error := TRUE;
      end if;

      --Check if action is DEL and required_ind is 'Y'
      if I_rec.stcd_action = action_del and I_rec.old_required_ind = 'Y' then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.nextval,
                     I_chunk_id,
                     L_detail_table,
                     I_rec.stcd_row_seq,
                     'CODE',
                     'CANNOT_MOD_DEL_CODE');
         L_detail_error := TRUE;
      end if;

      if L_error then
         if I_rec.stcd_row_seq IS NOT NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.nextval,
                        I_chunk_id,
                        L_detail_table,
                        I_rec.stcd_row_seq,
                        'CODE_TYPE',
                        'ERROR_IN_CODE_HEAD');
           L_detail_error := TRUE;
         end if;
      end if;

      ---------------------------------------------------------------------------------
      -- begin SVC_CODE_HEAD PROCESSING
      ---------------------------------------------------------------------------------
      -- if No L_error then do the processing of head
      if NOT L_error then
         if I_rec.ch_rank = 1 then
            SAVEPOINT do_process;
         end if;
         -- process only the first header record for a given CODE_TYPE
         if I_rec.stch_action is NOT NULL and I_rec.ch_rank = 1 then
            CODE_HEAD_temp_rec.CODE_TYPE_DESC := I_rec.stch_code_type_desc;
            CODE_HEAD_temp_rec.CODE_TYPE      := I_rec.stch_CODE_TYPE;

            if I_rec.stch_action = action_new then
               if EXEC_CODE_HEAD_INS(O_error_message,
                                     CODE_HEAD_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.nextval,
                              I_chunk_id,
                              L_table,
                              I_rec.stch_row_seq,
                              NULL,
                              O_error_message);
                  L_head_process_error := TRUE;
               end if;
            end if;

            --Modify only if there is change from old value to new
            if I_rec.stch_action = action_mod and I_rec.stch_code_type_desc <> I_rec.old_code_type_desc then
               if EXEC_CODE_HEAD_UPD(O_error_message,
                                     CODE_HEAD_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.nextval,
                              I_chunk_id,
                              L_table,
                              I_rec.stch_row_seq,
                              NULL,
                              O_error_message);
                  L_head_process_error := TRUE;
               end if;
            end if;

            if I_rec.stch_action = action_del then
               if EXEC_CODE_HEAD_DEL(O_error_message,
                                     CODE_HEAD_temp_rec,
                                     I_rec) = FALSE then
                  if O_error_message like '%PACKAGE_ERROR%' then
                     return FALSE;
                  else
                     WRITE_ERROR(I_process_id,
                                 svc_admin_upld_er_seq.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 I_rec.stch_row_seq,
                                 NULL,
                                 O_error_message);
                     L_head_process_error := TRUE;
                  end if;
               end if;
            end if;

            if L_head_process_error then
               if I_rec.stcd_row_seq IS NOT NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.nextval,
                              I_chunk_id,
                              L_detail_table,
                              I_rec.stcd_row_seq,
                              'CODE_TYPE',
                              'ERROR_IN_CODE_HEAD');
               end if;
            end if;
         end if; -- process only the first header record for a given CODE_TYPE

         if I_rec.stch_action IN (action_new,action_mod)
            or NVL(I_rec.stch_action,'-1') = NVL(NULL,'-1') then
            if I_rec.stcd_action is NOT NULL and NOT L_detail_error and NOT L_head_process_error then
               CODE_DETAIL_temp_rec.CODE_TYPE    := I_rec.stcd_CODE_TYPE;
               CODE_DETAIL_temp_rec.CODE         := I_rec.stcd_CODE;
               CODE_DETAIL_temp_rec.CODE_DESC    := I_rec.stcd_CODE_DESC;
               CODE_DETAIL_temp_rec.REQUIRED_IND := I_rec.stcd_REQUIRED_IND;
               CODE_DETAIL_temp_rec.CODE_SEQ     := I_rec.stcd_CODE_SEQ;

      --------------------------------------------------------------------------------
            -- Begin SVC_CODE_DETAIL processing
      --------------------------------------------------------------------------------
               if I_rec.stcd_action = action_new then
                  if EXEC_CODE_DETAIL_INS(O_error_message,
                                          CODE_DETAIL_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 svc_admin_upld_er_seq.nextval,
                                 I_chunk_id,
                                 L_detail_table,
                                 I_rec.stcd_row_seq,
                                 NULL,
                                 O_error_message);
                     L_process_error := TRUE;
                  end if;
               end if;

               --Modify only if there is change from old value to new
               if I_rec.stcd_action = action_mod
                  and (I_rec.stcd_required_ind <> I_rec.old_required_ind
                       or I_rec.stcd_code_desc <>  I_rec.old_code_desc
                       or I_rec.stcd_code_seq <>  I_rec.old_code_seq) then
                  if EXEC_CODE_DETAIL_UPD(O_error_message,
                                          CODE_DETAIL_temp_rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 svc_admin_upld_er_seq.nextval,
                                 I_chunk_id,
                                 L_detail_table,
                                 I_rec.stcd_row_seq,
                                 NULL,
                                 O_error_message);
                     L_process_error := TRUE;
                  end if;
               end if;

               if I_rec.stcd_action = action_del then
                  if EXEC_CODE_DETAIL_DEL(O_error_message,
                                          CODE_DETAIL_temp_rec,
                                          I_rec) = FALSE then
                     if O_error_message like '%PACKAGE_ERROR%' then
                        return FALSE;
                     else
                        WRITE_ERROR(I_process_id,
                                    svc_admin_upld_er_seq.NEXTVAL,
                                    I_chunk_id,
                                    L_detail_table,
                                    I_rec.stcd_row_seq,
                                    NULL,
                                    O_error_message);
                        L_process_error := TRUE;
                     end if;
                  end if;
               end if;

               if NOT L_process_error then
                  L_row_seq_tab.extend();
                  L_row_seq_tab(c)  := I_rec.stcd_row_seq;
                  c:=c+1;
               end if;
            end if; -- processing code_detail

         else
            if NOT L_head_process_error then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           I_rec.stch_row_seq,
                           'CODE_TYPE',
                           'CODEHD_DEL_REC',
                           'W');
            end if;
         end if; -- code_head  in action_new or mod
         -- Rollback if no detail exists for a code_type or if trying to delete the last detail
      end if; -- if NOT L_error

      if I_rec.stch_code_type <> I_rec.next_code_type then
         -- check if code_head action is del
         if I_rec.stch_action <> action_del or I_rec.stch_action IS NULL then
            open  C_DETAIL_ATLEAST(I_rec.stch_code_type);
            fetch C_DETAIL_ATLEAST into count_detail;
            close C_DETAIL_ATLEAST;

            if count_detail < 1 then
               -- for code_ head action new and no record in detail
               if I_rec.stch_action IN (action_new)
                  and I_rec.stcd_rid is NULL then
                   WRITE_ERROR(I_process_id,
                               svc_admin_upld_er_seq.nextval,
                               I_chunk_id,
                               L_table,
                               I_rec.stch_row_seq,
                               'CODE_TYPE',
                               'ATLEAST_ONE_DETAIL_REQ');
               end if;

               L_rowseq_count := L_row_seq_tab.count();
               if L_rowseq_count > 0 then
                  for i in 1..L_rowseq_count
                  LOOP
                     if I_rec.stcd_action IN (action_del) then
                        WRITE_ERROR(I_process_id,
                                    svc_admin_upld_er_seq.nextval,
                                    I_chunk_id,
                                    L_table,
                                    L_row_seq_tab(i),
                                    'CODE_TYPE',
                                    'ATLEAST_ONE_DETAIL_REQ');
                     end if;
                  END LOOP;
               -- for code_detail action del and last record in code_detail
               end if;

               if I_rec.stch_action IN (action_new)
                  and I_rec.stcd_rid is NOT NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.nextval,
                              I_chunk_id,
                              L_detail_table,
                              I_rec.stcd_row_seq,
                              'CODE_TYPE',
                              'DETAIL_PROC_ER');
               end if;
               ROLLBACK TO do_process;

            end if;
         end if;
      end if; -- Rollback condition
   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_DETAIL_ATLEAST%ISOPEN then
         close C_DETAIL_ATLEAST;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_CODE_HEAD;
---------------------------------------------------------------------------------
FUNCTION EXEC_CODE_DTL_TL_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_dtl_tl_ins_tab   IN       CODE_DTL_TL_TAB)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_CODE_HEAD.EXEC_CODE_DTL_TL_INS';

BEGIN
   if I_code_dtl_tl_ins_tab is NOT NULL and I_code_dtl_tl_ins_tab.COUNT > 0 then
      FORALL i IN 1..I_code_dtl_tl_ins_tab.COUNT()
         insert into code_detail_tl
              values I_code_dtl_tl_ins_tab(i);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CODE_DTL_TL_INS;
---------------------------------------------------------------------------------
FUNCTION EXEC_CODE_DTL_TL_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_dtl_tl_upd_tab   IN       CODE_DTL_TL_TAB,
                              I_cdtl_tl_upd_rst       IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_CODE_DETAIL_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_CODE_DETAIL_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'CORESVC_CODE_HEAD.EXEC_CODE_DTL_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_DETAIL_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;
   L_key_val3      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CD_TL_UPD(I_code_type   CODE_DETAIL_TL.CODE_TYPE%TYPE,
                           I_code        CODE_DETAIL_TL.CODE%TYPE,
                           I_lang        CODE_DETAIL_TL.LANG%TYPE) is
      select 'x'
        from code_detail_tl
       where code_type = I_code_type
         and code = I_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_code_dtl_tl_upd_tab is NOT NULL and I_code_dtl_tl_upd_tab.COUNT > 0 then
      for i in I_code_dtl_tl_upd_tab.FIRST..I_code_dtl_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_code_dtl_tl_upd_tab(i).lang);
            L_key_val2 := 'Code Type: '||I_code_dtl_tl_upd_tab(i).code_type;
            L_key_val3 := 'Code: '||I_code_dtl_tl_upd_tab(i).code;
            L_key_val2 := L_key_val2||' '||L_key_val3;

            open C_LOCK_CD_TL_UPD(I_code_dtl_tl_upd_tab(i).code_type,
                                  I_code_dtl_tl_upd_tab(i).code,
                                  I_code_dtl_tl_upd_tab(i).lang);
            close C_LOCK_CD_TL_UPD;
            
            update code_detail_tl
               set code_desc = I_code_dtl_tl_upd_tab(i).code_desc,
                   last_update_id = I_code_dtl_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_code_dtl_tl_upd_tab(i).last_update_datetime
             where lang = I_code_dtl_tl_upd_tab(i).lang
               and code_type = I_code_dtl_tl_upd_tab(i).code_type
               and code = I_code_dtl_tl_upd_tab(i).code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table,
                              I_cdtl_tl_upd_rst(i),
                              NULL,
                              O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CD_TL_UPD%ISOPEN then
         close C_LOCK_CD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CODE_DTL_TL_UPD;
---------------------------------------------------------------------------------
FUNCTION EXEC_CODE_DTL_TL_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_code_dtl_tl_del_tab   IN       CODE_DTL_TL_TAB,
                              L_cdtl_tl_del_rst       IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_CODE_DETAIL_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_CODE_DETAIL_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_CODE_HEAD.EXEC_CODE_DTL_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_DETAIL_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;
   L_key_val3      VARCHAR2(30) := NULL;

   --Cursor to lock the record
   cursor C_LOCK_CD_TL_DEL(I_code_type   CODE_DETAIL_TL.CODE_TYPE%TYPE,
                           I_code        CODE_DETAIL_TL.CODE%TYPE,
                           I_lang        CODE_DETAIL_TL.LANG%TYPE) is
      select 'x'
        from code_detail_tl
       where code_type = I_code_type
         and code = I_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_code_dtl_tl_del_tab is NOT NULL and I_code_dtl_tl_del_tab.COUNT > 0 then
      for i in I_code_dtl_tl_del_tab.FIRST..I_code_dtl_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_code_dtl_tl_del_tab(i).lang);
            L_key_val2 := 'Code Type: '||I_code_dtl_tl_del_tab(i).code_type;
            L_key_val3 := 'Code: '||I_code_dtl_tl_del_tab(i).code;
            L_key_val2 := L_key_val2||' '||L_key_val3;
            open C_LOCK_CD_TL_DEL(I_code_dtl_tl_del_tab(i).code_type,
                                  I_code_dtl_tl_del_tab(i).code,
                                  I_code_dtl_tl_del_tab(i).lang);
            close C_LOCK_CD_TL_DEL;
            
            delete code_detail_tl
             where lang = I_code_dtl_tl_del_tab(i).lang
               and code_type = I_code_dtl_tl_del_tab(i).code_type
               and code = I_code_dtl_tl_del_tab(i).code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table,
                              L_cdtl_tl_del_rst(i),
                              NULL,
                              O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CD_TL_DEL%ISOPEN then
         close C_LOCK_CD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CODE_DTL_TL_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CODE_DTL_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_CODE_DETAIL_TL.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_CODE_DETAIL_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(64)                      := 'CORESVC_CODE_HEAD.PROCESS_CODE_DTL_TL';
   L_error_message          RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_DETAIL_TL';
   L_base_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CODE_DETAIL';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CODE_DETAIL_TL';
   L_error                  BOOLEAN                           := FALSE;
   L_process_error          BOOLEAN                           := FALSE;
   L_code_dtl_tl_temp_rec   CODE_DETAIL_TL%ROWTYPE;
   L_cdtl_tl_upd_rst        ROW_SEQ_TAB;
   L_cdtl_tl_del_rst        ROW_SEQ_TAB;

   cursor C_SVC_CODE_DTL_TL(I_process_id   NUMBER,
                            I_chunk_id     NUMBER) is
      select pk_cdtl_tl.rowid    as pk_cdtl_tl_rid,
             fk_cdtl.rowid       as fk_cdtl_rid,
             fk_lang.rowid       as fk_lang_rid,
             st.lang,
             UPPER(st.code_type) as code_type,
             UPPER(st.code)      as code,
             st.code_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)    as action,
             st.process$status
        from svc_code_detail_tl st,
             code_detail        fk_cdtl,
             code_detail_tl     pk_cdtl_tl,
             lang               fk_lang
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.code_type  = fk_cdtl.code_type (+)
         and st.code       = fk_cdtl.code (+)
         and st.lang       = pk_cdtl_tl.lang (+)
         and st.code_type  = pk_cdtl_tl.code_type (+)
         and st.code       = pk_cdtl_tl.code (+)
         and st.lang       = fk_lang.lang (+);

   TYPE SVC_CODE_DTL_TL is TABLE OF C_SVC_CODE_DTL_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_code_dtl_tl_tab    SVC_CODE_DTL_TL;
   L_code_dtl_tl_ins_tab    code_dtl_tl_tab                   := NEW code_dtl_tl_tab();
   L_code_dtl_tl_upd_tab    code_dtl_tl_tab                   := NEW code_dtl_tl_tab();
   L_code_dtl_tl_del_tab    code_dtl_tl_tab                   := NEW code_dtl_tl_tab();

BEGIN
   if C_SVC_CODE_DTL_TL%ISOPEN then
      close C_SVC_CODE_DTL_TL;
   end if;

   open C_SVC_CODE_DTL_TL(I_process_id,
                          I_chunk_id);
   LOOP
      fetch C_SVC_CODE_DTL_TL bulk collect into L_svc_code_dtl_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_code_dtl_tl_tab.COUNT > 0 then
         FOR i in L_svc_code_dtl_tl_tab.FIRST..L_svc_code_dtl_tl_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_code_dtl_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_code_dtl_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if action is valid
            if L_svc_code_dtl_tl_tab(i).action is NULL
               or L_svc_code_dtl_tl_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_code_dtl_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_code_dtl_tl_tab(i).action = action_new
               and L_svc_code_dtl_tl_tab(i).pk_cdtl_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_code_dtl_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_code_dtl_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_code_dtl_tl_tab(i).lang is NOT NULL
               and L_svc_code_dtl_tl_tab(i).code_type is NOT NULL
               and L_svc_code_dtl_tl_tab(i).pk_cdtl_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_code_dtl_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            -- check for FK
            if L_svc_code_dtl_tl_tab(i).action = action_new
               and L_svc_code_dtl_tl_tab(i).code_type is NOT NULL
               and L_svc_code_dtl_tl_tab(i).fk_cdtl_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_code_dtl_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_code_dtl_tl_tab(i).action = action_new
               and L_svc_code_dtl_tl_tab(i).lang is NOT NULL
               and L_svc_code_dtl_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_code_dtl_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_code_dtl_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_code_dtl_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if L_svc_code_dtl_tl_tab(i).code_type is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_code_dtl_tl_tab(i).row_seq,
                           'CODE_TYPE',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_code_dtl_tl_tab(i).code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_code_dtl_tl_tab(i).row_seq,
                           'CODE',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_code_dtl_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_code_dtl_tl_tab(i).code_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_code_dtl_tl_tab(i).row_seq,
                              'CODE_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if NOT L_error then
               L_code_dtl_tl_temp_rec.lang := L_svc_code_dtl_tl_tab(i).lang;
               L_code_dtl_tl_temp_rec.code_type := L_svc_code_dtl_tl_tab(i).code_type;
               L_code_dtl_tl_temp_rec.code := L_svc_code_dtl_tl_tab(i).code;
               L_code_dtl_tl_temp_rec.code_desc := L_svc_code_dtl_tl_tab(i).code_desc;
               L_code_dtl_tl_temp_rec.create_datetime := SYSDATE;
               L_code_dtl_tl_temp_rec.create_id := GET_USER;
               L_code_dtl_tl_temp_rec.last_update_datetime := SYSDATE;
               L_code_dtl_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_code_dtl_tl_tab(i).action = action_new then
                  L_code_dtl_tl_ins_tab.extend;
                  L_code_dtl_tl_ins_tab(L_code_dtl_tl_ins_tab.count()) := L_code_dtl_tl_temp_rec;
               end if;

               if L_svc_code_dtl_tl_tab(i).action = action_mod then
                  L_code_dtl_tl_upd_tab.extend;
                  L_code_dtl_tl_upd_tab(L_code_dtl_tl_upd_tab.count()) := L_code_dtl_tl_temp_rec;
                  L_cdtl_tl_upd_rst(L_code_dtl_tl_upd_tab.count()) := L_svc_code_dtl_tl_tab(i).row_seq;
               end if;

               if L_svc_code_dtl_tl_tab(i).action = action_del then
                  L_code_dtl_tl_del_tab.extend;
                  L_code_dtl_tl_del_tab(L_code_dtl_tl_del_tab.count()) := L_code_dtl_tl_temp_rec;
                  L_cdtl_tl_del_rst(L_code_dtl_tl_del_tab.count()) := L_svc_code_dtl_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_CODE_DTL_TL%NOTFOUND;
   END LOOP;
   close C_SVC_CODE_DTL_TL;

   if EXEC_CODE_DTL_TL_INS(O_error_message,
                           L_code_dtl_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_CODE_DTL_TL_UPD(O_error_message,
                           L_code_dtl_tl_upd_tab,
                           L_cdtl_tl_upd_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_CODE_DTL_TL_DEL(O_error_message,
                           L_code_dtl_tl_del_tab,
                           L_cdtl_tl_del_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_CODE_DTL_TL;
---------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN

   delete from svc_code_detail_tl
      where process_id = I_process_id;
   delete from svc_code_detail
      where process_id = I_process_id;
   delete from svc_code_head
      where process_id = I_process_id;

END;
-----------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64) := 'CORESVC_CODE_HEAD.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_CODE_HEAD(O_error_message,
                        I_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;
   if PROCESS_CODE_DTL_TL(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.count();
   forall i in 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();
   open  C_GET_ERR_COUNT;
   fetch C_GET_ERR_COUNT into L_err_count;
   close C_GET_ERR_COUNT;

   open  C_GET_WARN_COUNT;
   fetch C_GET_WARN_COUNT into L_warn_count;
   close C_GET_WARN_COUNT;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;

   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-----------------------------------------------------------------------------------
END CORESVC_CODE_HEAD;
/
