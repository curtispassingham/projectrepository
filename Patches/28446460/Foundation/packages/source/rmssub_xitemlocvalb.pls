CREATE OR REPLACE PACKAGE BODY RMSSUB_XITEMLOC_VALIDATE AS

--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PRIVATE FUNCTION SPECS
--------------------------------------------------------------------------------

FUNCTION CHECK_FIELDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message       IN     "RIB_XItemLocDesc_REC",
                      I_message_type  IN     VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

FUNCTION CHECK_ITEM_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE,
                            I_hier_level      IN       VARCHAR2,
                            I_hier_value      IN       NUMBER)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------

FUNCTION CHECK_MESSAGE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_message       IN     "RIB_XItemLocDesc_REC",
                       I_message_type  IN     VARCHAR2)
RETURN BOOLEAN IS

L_program   VARCHAR2(50)  := 'RMSSUB_XITEMLOC_VALIDATE.CHECK_MESSAGE';

BEGIN

   if I_message is null then
      O_error_message := SQL_LIB.CREATE_MSG('RMSSUB_INV_MESSAGE', null, null,
      null);
      return FALSE;
   end if;

   if I_message.XItemLocDtl_TBL is null then
      O_error_message := SQL_LIB.CREATE_MSG('NO_DTL_REC', null, null, null);
      return FALSE;
   end if;

   if CHECK_FIELDS(O_error_message,
                   I_message,
                   I_message_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
--------------------------------------------------------------------------------

FUNCTION CHECK_FIELDS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_message       IN     "RIB_XItemLocDesc_REC",
                      I_message_type  IN     VARCHAR2)

RETURN BOOLEAN IS

L_program          VARCHAR2(50) := 'RMSSUB_ITEM_LOC_VALIDATE.CHECK_FIELDS';
L_primary_supp     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE;
L_primary_cntry    ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
L_hier_value       NUMBER := null;
L_dummy            VARCHAR2(1) := null;
L_store_type       VARCHAR2(1) := null;
L_stockholding_ind  VARCHAR2(1) := null;
--
L_check_wh         VARCHAR2(1) := null;
L_pack_ind         ITEM_MASTER.PACK_IND%TYPE := null;
L_pack_type        ITEM_MASTER.PACK_TYPE%TYPE := null;
L_order_as_type    ITEM_MASTER.ORDER_AS_TYPE%TYPE := null;
L_finisher_ind     WH.FINISHER_IND%TYPE := null;
L_break_pack_ind   WH.BREAK_PACK_IND%TYPE := null;
L_orderable_ind    ITEM_MASTER.ORDERABLE_IND%TYPE;
L_flag             BOOLEAN;

cursor C_CHK_ISC_EXIST is
select xild.primary_supp,
       xild.primary_cntry
  from TABLE (CAST (I_message.XItemLocDtl_TBL AS "RIB_XItemLocDtl_TBL")) xild
 where not exists (select 'x'
                     from item_supp_country isc
                    where isc.item = I_message.item
                      and isc.supplier = nvl(xild.primary_supp, isc.supplier)
                      and isc.origin_country_id = nvl(xild.primary_cntry, isc.origin_country_id));

cursor C_CHK_LOC_IL is
select xild.hier_value
  from TABLE (CAST (I_message.XItemLocDtl_TBL AS "RIB_XItemLocDtl_TBL")) xild
 where exists (select 'x'
                 from item_loc il
                where il.item = I_message.item
                  and il.loc = xild.hier_value);

cursor C_ITEM is
select im.pack_ind,
       im.pack_type,
       im.order_as_type,
       im.orderable_ind
  from item_master im
 where im.item = I_message.item;

cursor C_WH(I_wh WH.WH%TYPE) is
select w.finisher_ind,
       w.break_pack_ind
  from wh w
 where w.wh = I_wh;

cursor C_CHECK_WH(I_wh WH.WH%TYPE) is
select 'x'
  from wh
 where wh = I_wh
   and wh.finisher_ind = 'N'
   and wh.stockholding_ind = 'Y';

cursor C_CHECK_STORE(I_store store.store%TYPE) is
select store_type,
       stockholding_ind
  from store
 where store = I_store;

cursor C_CHECK_UINT(I_uint code_detail.code%TYPE) is
select 'x'
  from code_detail
 where code_type = 'UINT'
   and code = I_uint;

cursor C_CHECK_ULBL(I_ulbl code_detail.code%TYPE) is
select 'x'
  from code_detail
 where code_type = 'ULBL'
   and code = I_ulbl;

BEGIN

   -- item
   if I_message.item is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item', null,
      null);
      return FALSE;
   else
      open C_ITEM;
      fetch C_ITEM into L_pack_ind,
                        L_pack_type,
                        L_order_as_type,
                        L_orderable_ind;
      close C_ITEM;
      --
      if L_pack_ind is null then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM', null, null, null);
      return FALSE;
      end if;
   end if;

   -- hier_level
   if I_message.hier_level is null then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL',
      'hier_level', null, null);
      return FALSE;
   end if;
   if I_message.hier_level not in ('CH', 'AR', 'RE', 'DI', 'S', 'W') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORG_LEVEL', null, null,
      null);
      return FALSE;
   end if;

   -- XItemLocDtl Fields
   for i in 1 .. I_message.XItemLocDtl_TBL.last loop
      -- hier_value
      if I_message.XItemLocDtl_TBL(i).hier_value is null then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'hier_value',
         null, null);
         return FALSE;
      end if;

      if I_message_type in (RMSSUB_XITEMLOC.ITEM_LOC_UPD) then
         -- item country check only in update as NEW_ITME_LOC_SQL check item country
         if CHECK_ITEM_COUNTRY(O_error_message,
                               I_message.item,
                               I_message.hier_level,
                               I_message.XItemLocDtl_TBL(i).hier_value) = FALSE then
            return FALSE;
         end if;
      end if;

      -- status
      if I_message.XItemLocDtl_TBL(i).status is null then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'status',
         null, null);
         return FALSE;
      end if;

      -- store_ord_mult
      if I_message.XItemLocDtl_TBL(i).store_ord_mult is not null and
      I_message.XItemLocDtl_TBL(i).store_ord_mult not in ('I','C','E') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_ORD_MULT', null,
         null, null);
         return FALSE;
      end if;

       -- source_method
      if I_message.hier_level = 'S' then
         open C_CHECK_STORE(I_message.XItemLocDtl_TBL(i).hier_value);
         fetch C_CHECK_STORE into L_store_type,
                                  L_stockholding_ind;
         close C_CHECK_STORE;
         if L_store_type = 'F' and L_stockholding_ind = 'N' and (I_message.XItemLocDtl_TBL(i).source_method  = 'S' or
            I_message.XItemLocDtl_TBL(i).source_method is null) then
            O_error_message := SQL_LIB.CREATE_MSG('ENT_SOURCE_METHOD', 'source_method','Valid Value W', null);
            return FALSE;
         end if;
      end if;

      -- validation of source_wh and source_method when the hier_level is Warehouse
      if I_message.hier_level = 'W' then
         if I_message.XItemLocDtl_TBL(i).source_wh is NOT NULL then
            O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL', 
                                                  'source_wh',
                                                  NULL,
                                                  NULL);
           return FALSE;
         end if;
        --
         if I_message.XItemLocDtl_TBL(i).source_method is NOT NULL and I_message.XItemLocDtl_TBL(i).source_method != 'S' then
            O_error_message := SQL_LIB.CREATE_MSG('ENT_SOURCE_METHOD',
                                                  'source_method',
                                                   NULL,
                                                   NULL);
              return FALSE;
         end if;
      end if; 

      if I_message.XItemLocDtl_TBL(i).source_method is not null and
      I_message.XItemLocDtl_TBL(i).source_method not in ('W','S') then
         O_error_message := SQL_LIB.CREATE_MSG('ENT_SOURCE_METHOD', 'source_method',
         null, null);
         return FALSE;
      end if;

      if I_message.XItemLocDtl_TBL(i).source_method is null and I_message.XItemLocDtl_TBL(i).source_wh is not null then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'source_method',
         null, null);
         return FALSE;
      end if;

      if I_message.XItemLocDtl_TBL(i).source_method = 'W' and
      I_message.XItemLocDtl_TBL(i).source_wh is null then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'source_wh',
         null, null);
         return FALSE;
      end if;

      if I_message.XItemLocDtl_TBL(i).source_method = 'S' and
      I_message.XItemLocDtl_TBL(i).source_wh is not null then
         O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL', 'source_wh',
         null, null);
         return FALSE;
      end if;

      -- source_wh
      if I_message.XItemLocDtl_TBL(i).source_method = 'W' then
         open C_CHECK_WH(I_message.XItemLocDtl_TBL(i).source_wh);
         fetch C_CHECK_WH into L_dummy;
         close C_CHECK_WH;

         if L_dummy is null then
            O_error_message := SQL_LIB.CREATE_MSG('INV_WH',I_message.XItemLocDtl_TBL(i).source_wh, null, null);
            return FALSE;
         end if;
         
         if I_message.hier_level = 'W' then
            if I_message.XItemLocDtl_TBL(i).source_wh = I_message.XItemLocDtl_TBL(i).hier_value then
               O_error_message := SQL_LIB.CREATE_MSG('WH_TO_WH_SAME',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;
         end if;
         -- item should be ranged to the source warehouse
         if ITEM_LOC_SQL.VALIDATE_WAREHOUSE(O_error_message,
                                            L_flag,
                                            I_message.item,
                                            I_message.XItemLocDtl_TBL(i).source_wh) = FALSE then
            return FALSE;
         end if;
         
         if L_flag = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('SOURCE_WH_NO_EXIST');
            return FALSE;
         end if;
      end if;

      -- wh location validation
      if I_message.hier_level = 'W' then
         open C_CHECK_WH(I_message.XItemLocDtl_TBL(i).hier_value);
         fetch C_CHECK_WH into L_check_wh;
         close C_CHECK_WH;

         if L_check_wh is null then
            O_error_message := SQL_LIB.CREATE_MSG('INV_WH',I_message.XItemLocDtl_TBL(i).hier_value, null, null);
            return FALSE;
         end if;
      end if;

      -- receive_as_type
      if I_message.XItemLocDtl_TBL(i).receive_as_type is not null then
         if L_pack_ind = 'N' or I_message.hier_level != 'W' then
            O_error_message := SQL_LIB.CREATE_MSG('FIELD_MUST_BE_NULL',
            'Receive As Type', null, null);
            return FALSE;
         end if;

         --
         L_finisher_ind := null;

         open C_WH(I_message.XItemLocDtl_TBL(i).hier_value);
         fetch C_WH into L_finisher_ind,
                         L_break_pack_ind;
         close C_WH;

         if L_finisher_ind is null then
            O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_VALUE',
            I_message.XItemLocDtl_TBL(i).hier_value, null, null);
            return FALSE;
         end if;

         if L_finisher_ind = 'Y' and
         I_message.XItemLocDtl_TBL(i).receive_as_type != 'E' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_FINISHER_RCV_AS_TYP',
            null, null, null);
            return FALSE;
         end if;
         --

         if L_pack_type = 'V' then
            if I_message.XItemLocDtl_TBL(i).receive_as_type != 'P' then
               O_error_message := SQL_LIB.CREATE_MSG('INV_VNDR_PACK_RCV_AS_TYP',
               null, null, null);
               return FALSE;
            end if;
         else
            if L_order_as_type = 'P' and L_break_pack_ind = 'N' and
            I_message.XItemLocDtl_TBL(i).receive_as_type != 'P' then
               O_error_message := SQL_LIB.CREATE_MSG('INV_BREAKPACK_RCV_AS_TYP',
               null, null, null);
               return FALSE;
            end if;
         end if;

         if I_message.XItemLocDtl_TBL(i).receive_as_type not in ('E', 'P') then
            O_error_message := SQL_LIB.CREATE_MSG('INV_RECEIVE_AS_TYPE', null,
            null, null);
            return FALSE;
         end if;
      end if;

      -- taxable_ind
      if I_message.XItemLocDtl_TBL(i).taxable_ind is not null and
      I_message.XItemLocDtl_TBL(i).taxable_ind not in ('Y','N') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TAXABLE_IND', null, null,
         null);
         return FALSE;
      end if;

      -- uin_type , uin_label, capture_time, ext_uin_ind
      if I_message.XItemLocDtl_TBL(i).uin_type is not null then
         open C_CHECK_UINT(I_message.XItemLocDtl_TBL(i).uin_type);
         fetch C_CHECK_UINT into L_dummy;
         if C_CHECK_UINT%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_UIN_TYPE', null, null, null);
            close C_CHECK_UINT;
            return FALSE;
         end if;
         close C_CHECK_UINT;

         if I_message.XItemLocDtl_TBL(i).uin_label is null then
            O_error_message := SQL_LIB.CREATE_MSG('UIN_LABEL_REQD', null, null,
            null);
            return FALSE;
         else
            open C_CHECK_ULBL(I_message.XItemLocDtl_TBL(i).uin_label);
            fetch C_CHECK_ULBL into L_dummy;
            if C_CHECK_ULBL%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INV_UIN_LABEL', null, null, null);
               close C_CHECK_ULBL;
               return FALSE;
            end if;
            close C_CHECK_ULBL;
         end if;

         if I_message.XItemLocDtl_TBL(i).capture_time = 'S' and
            I_message.XItemLocDtl_TBL(i).ext_uin_ind != 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_UIN_IND', null, null,
            null);
            return FALSE;
         end if;

      end if;
   end loop;

   --
   if I_message_type in (RMSSUB_XITEMLOC.ITEM_LOC_ADD) then
      for i in 1 .. I_message.XItemLocDtl_TBL.last loop
         -- status
         if I_message.XItemLocDtl_TBL(i).status != 'A' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STATUS', null, null,
            null);
            return FALSE;
         end if;
      end loop;

      -- item-supplier-country check
      open C_CHK_ISC_EXIST;
      fetch C_CHK_ISC_EXIST into L_primary_supp,
                                 L_primary_cntry;
      if C_CHK_ISC_EXIST%FOUND then
         if L_orderable_ind != 'N' then
            close C_CHK_ISC_EXIST;
            ---
            O_error_message := SQL_LIB.CREATE_MSG('NO_ISC_REL', I_message.item,
            L_primary_supp, L_primary_cntry);
            return FALSE;
         end if;
      end if;
      close C_CHK_ISC_EXIST;

      -- item-location check
      if I_message.hier_level in ('S', 'W') then
         L_hier_value := null;

         open C_CHK_LOC_IL;
         fetch C_CHK_LOC_IL into L_hier_value;
         close C_CHK_LOC_IL;

         if L_hier_value is not null then
            O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_EXIST',
            I_message.item, L_hier_value, null);
            return FALSE;
         end if;
      end if;
   elsif I_message_type in (RMSSUB_XITEMLOC.ITEM_LOC_UPD) then
      for i in 1 .. I_message.XItemLocDtl_TBL.last loop
         -- status
         if I_message.XItemLocDtl_TBL(i).status not in ('A', 'I', 'C', 'D') then
            O_error_message := SQL_LIB.CREATE_MSG('INV_STATUS', null, null,
            null);
            return FALSE;
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FIELDS;
--------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_item            IN       ITEM_MASTER.ITEM%TYPE,
                            I_hier_level      IN       VARCHAR2,
                            I_hier_value      IN       NUMBER)
RETURN BOOLEAN is

   L_program     VARCHAR2(50) := 'RMSSUB_ITEM_LOC_VALIDATE.CHECK_ITEM_COUNTRY';
   L_store_tbl   LOC_TBL;
   L_exists      BOOLEAN;

   cursor C_GET_STORE_CH is
      select st.store
        from store st,
             district d,
             area a,
             region r,
             chain c
       where c.chain = I_hier_value
         and c.chain = a.chain
         and a.area = r.area
         and r.region = d.region
         and d.district = st.district;

   cursor C_GET_STORE_AR is
      select st.store
        from store st,
             district d,
             area a,
             region r
       where a.area = I_hier_value
         and a.area = r.area
         and r.region = d.region
         and d.district = st.district;

   cursor C_GET_STORE_RE is
      select st.store
        from store st,
             district d,
             region r
       where r.region = I_hier_value
         and r.region = d.region
         and d.district = st.district;

   cursor C_GET_STORE_DI is
      select st.store
        from store st,
             district d
       where d.district = I_hier_value
         and d.district = st.district;

BEGIN

   if I_hier_level in ('S', 'W') then
      -- check item country ranging
      if ITEM_LOC_SQL.CHECK_ITEM_COUNTRY(O_error_message,
                                         L_exists,
                                         I_item,
                                         I_hier_value,
                                         I_hier_level) = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('CANT_APPLY_LOC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      else
          return TRUE;
      end if;
      ---
   elsif I_hier_level = 'CH' then
      -- explode chain to store level
      open C_GET_STORE_CH;
      fetch C_GET_STORE_CH BULK COLLECT into L_store_tbl;
      close C_GET_STORE_CH;
   elsif I_hier_level = 'AR' then
      -- explode area to store level
      open C_GET_STORE_AR;
      fetch C_GET_STORE_AR BULK COLLECT into L_store_tbl;
      close C_GET_STORE_AR;
   elsif I_hier_level = 'RE' then
      -- explode region to store level
      open C_GET_STORE_RE;
      fetch C_GET_STORE_RE BULK COLLECT into L_store_tbl;
      close C_GET_STORE_RE;
   elsif I_hier_level = 'DI' then
      -- explode district to store level
      open C_GET_STORE_DI;
      fetch C_GET_STORE_DI BULK COLLECT into L_store_tbl;
      close C_GET_STORE_DI;
   end if;

   if L_store_tbl.count < 1 or L_store_tbl is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_VALUE',
                                            I_hier_value,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   -- loop through exploded stores
   for i in L_store_tbl.first..L_store_tbl.last loop
      -- check item country ranging
      if ITEM_LOC_SQL.CHECK_ITEM_COUNTRY(O_error_message,
                                            L_exists,
                                            I_item,
                                            L_store_tbl(i),
                                            'S') = FALSE then
         return FALSE;
      end if;
      ---
      if L_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('CANT_APPLY_LOC',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ITEM_COUNTRY;
--------------------------------------------------------------------------------
END RMSSUB_XITEMLOC_VALIDATE;
/