create or replace PACKAGE BODY CORESVC_UDA AS
----------------------------------------------------------------------------------------------
   cursor C_SVC_UDA(I_process_id   NUMBER,
                    I_chunk_id     NUMBER) is
      select t.pk_uda_values_rid,
             t.pk_uda_rid,
             t.st_udv_rid,
             t.st_uda_rid,
             t.udv_uda_fk_rid,
             LEAD(t.uda_uda_id, 1, 0) OVER (ORDER BY t.uda_uda_id)               as next_uda_id,
             row_number() over (partition BY t.uda_uda_id ORDER BY t.uda_uda_id) as uda_rank,
             t.udv_uda_value_desc,
             DECODE(TRANSLATE(t.udv_uda_value_desc,'0123456789',' '), NULL, 0,1) flag,
             t.udv_uda_value,
             t.udv_uda_id,
             t.udv_process_id,
             t.udv_row_seq,
             t.udv_chunk_id,
             t.udv_action,
             t.udv_process$status,
             t.uda_uda_desc,
             t.uda_uda_id,
             t.uda_filter_merch_id_subclass,
             t.uda_filter_merch_id_class,
             t.uda_filter_merch_id,
             t.uda_filter_org_id,
             t.uda_single_value_ind,
             t.pk_uda_single_value_ind,
             t.uda_data_length,
             t.pk_uda_data_length,
             t.uda_data_type,
             t.pk_uda_data_type,
             t.uda_display_type,
             t.pk_uda_display_type,
             t.uda_process_id,
             t.uda_row_seq,
             t.uda_chunk_id,
             t.uda_action,
             t.uda_process$status
        from (select pk_uda_values.rowid               as pk_uda_values_rid,
                     pk_uda.rowid                      as pk_uda_rid,
                     st_udv.rowid                      as st_udv_rid,
                     st_uda.rowid                      as st_uda_rid,
                     udv_uda_fk.rowid                  as udv_uda_fk_rid,
                     st_udv.uda_value_desc             as udv_uda_value_desc,
                     st_udv.uda_value                  as udv_uda_value,
                     st_udv.uda_id                     as udv_uda_id,
                     st_udv.process_id                 as udv_process_id,
                     st_udv.row_seq                    as udv_row_seq,
                     st_udv.chunk_id                   as udv_chunk_id,
                     UPPER(st_udv.action)              as udv_action,
                     st_udv.process$status             as udv_process$status,
                     st_uda.uda_desc                   as uda_uda_desc,
                     st_uda.uda_id                     as uda_uda_id,
                     st_uda.filter_merch_id_subclass   as uda_filter_merch_id_subclass,
                     st_uda.filter_merch_id_class      as uda_filter_merch_id_class,
                     st_uda.filter_merch_id            as uda_filter_merch_id,
                     st_uda.filter_org_id              as uda_filter_org_id,
                     st_uda.single_value_ind           as uda_single_value_ind,
                     pk_uda.single_value_ind           as pk_uda_single_value_ind,
                     st_uda.data_length                as uda_data_length,
                     pk_uda.data_length                as pk_uda_data_length,
                     st_uda.data_type                  as uda_data_type,
                     pk_uda.data_type                  as pk_uda_data_type,
                     st_uda.display_type               as uda_display_type,
                     pk_uda.display_type               as pk_uda_display_type,
                     st_uda.process_id                 as uda_process_id,
                     st_uda.row_seq                    as uda_row_seq,
                     st_uda.chunk_id                   as uda_chunk_id,
                     UPPER(st_uda.action)              as uda_action,
                     st_uda.process$status             as uda_process$status
                from svc_uda st_uda,
                     svc_uda_values st_udv,
                     uda pk_uda,
                     uda_values pk_uda_values,
                     uda udv_uda_fk
               where st_uda.uda_id     = pk_uda.uda_id (+)
                 and st_uda.process_id = I_process_id
                 and st_uda.chunk_id   = I_chunk_id
                 and st_uda.process_id = st_udv.process_id (+)
                 and st_uda.chunk_id   = st_udv.chunk_id (+)
                 and st_uda.uda_id     = st_udv.uda_id (+)
                 and st_udv.uda_id     = pk_uda_values.uda_id (+)
                 and NVL(st_udv.uda_value,-1)  = NVL(pk_uda_values.uda_value (+),-1)
                 and st_udv.uda_id     = udv_uda_fk.uda_id (+)
            union all
              select pk_uda_values.rowid                          as pk_uda_values_rid,
                     pk_uda.rowid                                 as pk_uda_rid,
                     st_udv.rowid                                 as st_udv_rid,
                     st_uda.rowid                                 as st_uda_rid,
                     udv_uda_fk.rowid                             as udv_uda_fk_rid,
                     st_udv.uda_value_desc                        as udv_uda_value_desc,
                     st_udv.uda_value                             as udv_uda_value,
                     st_udv.uda_id                                as udv_uda_id,
                     st_udv.process_id                            as udv_process_id,
                     st_udv.row_seq                               as udv_row_seq,
                     st_udv.chunk_id                              as udv_chunk_id,
                     UPPER(st_udv.action)                         as udv_action,
                     st_udv.process$status                        as udv_process$status,
                     st_uda.uda_desc                              as uda_uda_desc,
                     NVL(st_uda.uda_id, pk_uda.uda_id)            as uda_uda_id,
                     st_uda.filter_merch_id_subclass              as uda_filter_merch_id_subclass,
                     st_uda.filter_merch_id_class                 as uda_filter_merch_id_class,
                     st_uda.filter_merch_id                       as uda_filter_merch_id,
                     st_uda.filter_org_id                         as uda_filter_org_id,
                     st_uda.single_value_ind                      as uda_single_value_ind,
                     pk_uda.single_value_ind                      as pk_uda_single_value_ind,
                     NVL(st_uda.data_length,pk_uda.data_length)   as uda_data_length,
                     pk_uda.data_length                           as pk_uda_data_length,
                     NVL(st_uda.data_type,pk_uda.data_type)       as uda_data_type,
                     pk_uda.data_type                             as pk_uda_data_type,
                     NVL(st_uda.display_type,pk_uda.display_type) as uda_display_type,
                     pk_uda.display_type                          as pk_uda_display_type,
                     st_uda.process_id                            as uda_process_id,
                     st_uda.row_seq                               as uda_row_seq,
                     st_uda.chunk_id                              as uda_chunk_id,
                     UPPER(st_uda.action)                         as uda_action,
                     st_uda.process$status                        as uda_process$status
                from svc_uda_values st_udv,
                     svc_uda st_uda,
                     uda_values pk_uda_values,
                     uda udv_uda_fk,
                     uda pk_uda
               where st_udv.uda_id     = pk_uda.uda_id (+)
                 and st_udv.process_id = I_process_id
                 and st_udv.chunk_id   = I_chunk_id
                 and st_udv.process_id = st_uda.process_id (+)
                 and st_udv.chunk_id   = st_uda.chunk_id (+)
                 and st_udv.uda_id     = st_uda.uda_id (+)
                 and st_udv.uda_id     = pk_uda_values.uda_id (+)
                 and NVL(st_udv.uda_value,-1)  = NVL(pk_uda_values.uda_value (+),-1)
                 and st_udv.uda_id     = udv_uda_fk.uda_id (+)
                 and st_uda.uda_id IS NULL )t;
                 
   cursor C_SVC_UID(I_process_id   NUMBER,
                    I_chunk_id     NUMBER) is
      select pk_uda_item_defaults.rowid        as pk_uda_item_defaults_rid,
             st.rowid                          as st_rid,
             uid_uda_fk.rowid                  as uid_uda_fk_rid,
             uid_udv_fk.rowid                  as uid_udv_fk_rid,
             st.required_ind,
             pk_uda_item_defaults.required_ind as uid_required_ind,
             st.uda_value,
             st.uda_value_upd,
             pk_uda_item_defaults.uda_value    as uid_uda_value,
             st.subclass,
             pk_uda_item_defaults.subclass     as uid_subclass,
             st.class,
             pk_uda_item_defaults.class        as uid_class,
             st.dept,
             pk_uda_item_defaults.dept         as uid_dept,
             pk_uda_item_defaults.seq_no       as pk_seq_no,
             st.uda_id,
             pk_uda_item_defaults.uda_id       as uid_uda_id,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)                  as action,
             st.process$status
        from svc_uda_item_defaults st,
             uda_item_defaults pk_uda_item_defaults,
             uda uid_uda_fk,
             uda_values uid_udv_fk,
             dual
       where st.process_id                      = I_process_id
         and st.chunk_id                        = I_chunk_id
         and st.uda_id                          = pk_uda_item_defaults.uda_id (+)
         and st.dept                            = pk_uda_item_defaults.dept (+)
         and NVL(st.uda_value,-1)               = NVL(pk_uda_item_defaults.uda_value (+),-1)
         and NVL(st.subclass,-1)                = NVL(pk_uda_item_defaults.subclass (+),-1)
         and NVL(st.class,-1)                   = NVL(pk_uda_item_defaults.class (+),-1)
         and NVL(st.uda_value_upd,st.uda_value) = uid_udv_fk.uda_value (+)
         and st.uda_id                          = uid_udv_fk.uda_id (+)
         and st.uda_id                          = uid_uda_fk.uda_id (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab     errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type UDA_TAB IS TABLE OF UDA_TL%ROWTYPE;
   Type UDA_VALUES_TAB IS TABLE OF uda_values_tl%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
   LP_user            SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;

----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := LP_user;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES (I_file_id   NUMBER) IS
   L_sheets             s9t_pkg.names_map_typ;
   UDA_VALUES_cols      s9t_pkg.names_map_typ;
   uda_values_tl_cols   s9t_pkg.names_map_typ;
   UDA_cols             s9t_pkg.names_map_typ;
   UDA_Tl_cols          s9t_pkg.names_map_typ;
   UID_cols             s9t_pkg.names_map_typ;
BEGIN
   L_sheets                        := s9t_pkg.get_sheet_names(I_file_id);
   UDA_VALUES_cols                 := s9t_pkg.get_coL_names(I_file_id,
                                                            UDA_VALUES_sheet);
   UDA_VALUES$Action               := UDA_VALUES_cols('ACTION');
   UDA_VALUES$UDA_VALUE_DESC       := UDA_VALUES_cols('UDA_VALUE_DESC');
   UDA_VALUES$UDA_VALUE            := UDA_VALUES_cols('UDA_VALUE');
   UDA_VALUES$UDA_ID               := UDA_VALUES_cols('UDA_ID');

   UDA_cols                        := s9t_pkg.get_coL_names(I_file_id,
                                                            UDA_sheet);
   UDA$Action                      := UDA_cols('ACTION');
   UDA$UDA_DESC                    := UDA_cols('UDA_DESC');
   UDA$UDA_ID                      := UDA_cols('UDA_ID');
   UDA$FILTER_MERCH_ID_SUBCLASS    := UDA_cols('FILTER_MERCH_ID_SUBCLASS');
   UDA$FILTER_MERCH_ID_CLASS       := UDA_cols('FILTER_MERCH_ID_CLASS');
   UDA$FILTER_MERCH_ID             := UDA_cols('FILTER_MERCH_ID');
   UDA$FILTER_ORG_ID               := UDA_cols('FILTER_ORG_ID');
   UDA$SINGLE_VALUE_IND            := UDA_cols('SINGLE_VALUE_IND');
   UDA$DATA_LENGTH                 := UDA_cols('DATA_LENGTH');
   UDA$DATA_TYPE                   := UDA_cols('DATA_TYPE');
   UDA$DISPLAY_TYPE                := UDA_cols('DISPLAY_TYPE');

   UDA_TL_COLS                     :=S9T_PKG.GET_COL_NAMES(I_FILE_ID, UDA_TL_SHEET);
   UDA_TL$ACTION                   := UDA_TL_COLS('ACTION');
   UDA_TL$LANG                     := UDA_TL_COLS('LANG');
   UDA_TL$UDA_ID                   := UDA_TL_COLS('UDA_ID');
   UDA_TL$UDA_DESC                 := UDA_TL_COLS('UDA_DESC');

   UDA_VALUES_TL_COLS              := S9T_PKG.GET_COL_NAMES(I_FILE_ID,
                                                            UDA_VALUES_TL_SHEET);
   UDA_VALUES_TL$ACTION            := UDA_VALUES_TL_COLS('ACTION');
   UDA_VALUES_TL$UDA_VALUE_DESC    := UDA_VALUES_TL_COLS('UDA_VALUE_DESC');
   UDA_VALUES_TL$UDA_VALUE         := UDA_VALUES_TL_COLS('UDA_VALUE');
   UDA_VALUES_TL$UDA_ID            := UDA_VALUES_TL_COLS('UDA_ID');
   
   UID_cols          :=s9t_pkg.get_coL_names(I_file_id,
                                             UID_sheet);
   UID$Action        := UID_cols('ACTION');
   UID$REQUIRED_IND  := UID_cols('REQUIRED_IND');
   UID$UDA_VALUE     := UID_cols('UDA_VALUE');
   UID$UDA_VALUE_UPD := UID_cols('UDA_VALUE_UPD');
   UID$SUBCLASS      := UID_cols('SUBCLASS');
   UID$CLASS         := UID_cols('CLASS');
   UID$DEPT          := UID_cols('DEPT');
   UID$UDA_ID        := UID_cols('UDA_ID');
   
END POPULATE_NAMES;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_UDA_VALUES( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = UDA_VALUES_sheet )
               select s9t_row(s9t_cells(CORESVC_UDA.action_mod ,
                                        uda_id,
                                        uda_value,
                                        uda_value_desc))
                 from uda_values ;
END POPULATE_UDA_VALUES;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_UDA_VALUES_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = uda_values_tl_SHEET )
               select s9t_row(s9t_cells(CORESVC_UDA.action_mod ,
                                        lang,
                                        uda_id,
                                        uda_value,
                                        uda_value_desc))
                 from uda_values_tl ;
END POPULATE_UDA_VALUES_TL;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_UDA( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = UDA_sheet )
               select s9t_row(s9t_cells(CORESVC_UDA.action_mod ,
                                        uda_id,
                                        uda_desc,
                                        display_type,
                                        data_type,
                                        data_length,
                                        single_value_ind,
                                        filter_org_id,
                                        filter_merch_id,
                                        filter_merch_id_class,
                                        filter_merch_id_subclass))
                 from uda ;
END POPULATE_UDA;
----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_UDA_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = UDA_TL_SHEET )
               select s9t_row(s9t_cells(CORESVC_UDA.action_mod,
                                        lang,
                                        uda_id,
                                        uda_desc))
                 from uda_tl;
END POPULATE_UDA_TL;
----------------------------------------------------------------------------------
PROCEDURE POPULATE_UID( I_file_id   IN   NUMBER ) IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = UID_sheet )
               select s9t_row(s9t_cells(CORESVC_UDA.action_mod ,
                                        uda_id,
                                        dept,
                                        class,
                                        subclass,
                                        uda_value,
                                        uda_value,
                                        required_ind))
                 from uda_item_defaults ;
END POPULATE_UID;
----------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||LP_user||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(UDA_VALUES_sheet);
   L_file.sheets(L_file.get_sheet_index(UDA_VALUES_sheet)).column_headers := s9t_cells('ACTION',
                                                                                       'UDA_ID',
                                                                                       'UDA_VALUE',
                                                                                       'UDA_VALUE_DESC');

   L_file.add_sheet(UDA_VALUES_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(UDA_VALUES_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                          'LANG',
                                                                                          'UDA_ID',
                                                                                          'UDA_VALUE',
                                                                                          'UDA_VALUE_DESC');
   L_file.add_sheet(UDA_sheet);
   L_file.sheets(L_file.get_sheet_index(UDA_sheet)).column_headers := s9t_cells('ACTION',
                                                                                'UDA_ID',
                                                                                'UDA_DESC',
                                                                                'DISPLAY_TYPE',
                                                                                'DATA_TYPE',
                                                                                'DATA_LENGTH',
                                                                                'SINGLE_VALUE_IND',
                                                                                'FILTER_ORG_ID',
                                                                                'FILTER_MERCH_ID',
                                                                                'FILTER_MERCH_ID_CLASS',
                                                                                'FILTER_MERCH_ID_SUBCLASS');

   L_file.add_sheet(UDA_TL_SHEET);
   L_file.sheets(l_file.get_sheet_index(UDA_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                   'LANG',
                                                                                   'UDA_ID',
                                                                                   'UDA_DESC');
                                                                                   
   L_file.add_sheet(UID_sheet);
   L_file.sheets(L_file.get_sheet_index(UID_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                 'UDA_ID',
                                                                                 'DEPT',
                                                                                 'CLASS',
                                                                                 'SUBCLASS',
                                                                                 'UDA_VALUE',
                                                                                 'UDA_VALUE_UPD',
                                                                                 'REQUIRED_IND');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_UDA.CREATE_S9T';
   L_file      s9t_file;

BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE   then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_UDA_VALUES(O_file_id);
      POPULATE_UDA(O_file_id);
      POPULATE_UDA_TL(O_file_id);
      POPULATE_UDA_VALUES_TL(O_file_id);
      POPULATE_UID(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UDA_VALUES( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id   IN   SVC_UDA_VALUES.process_id%TYPE) IS

   TYPE svc_UDA_VALUES_coL_typ IS TABLE OF SVC_UDA_VALUES%ROWTYPE;
   L_temp_rec           SVC_UDA_VALUES%ROWTYPE;
   svc_UDA_VALUES_col   svc_UDA_VALUES_coL_typ := NEW svc_UDA_VALUES_coL_typ();
   L_process_id         SVC_UDA_VALUES.process_id%TYPE;
   L_error              BOOLEAN := FALSE;
   L_default_rec        SVC_UDA_VALUES%ROWTYPE;

   cursor C_MANDATORY_IND is
      select UDA_VALUE_DESC_mi,
             UDA_VALUE_mi,
             UDA_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key = template_key
                 and wksht_key    = 'UDV')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('UDA_VALUE_DESC' as UDA_VALUE_DESC,
                                       'UDA_VALUE'      as UDA_VALUE,
                                       'UDA_ID'         as UDA_ID,
                                        null            as dummy));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_UDA_VALUES';
   L_pk_columns    VARCHAR2(255)  := 'UDA,UDA Value';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select UDA_VALUE_DESC_dv,
                      UDA_VALUE_dv,
                      UDA_ID_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key = template_key
                          and wksht_key    = 'UDV')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('UDA_VALUE_DESC' as UDA_VALUE_DESC,
                                                'UDA_VALUE'      as UDA_VALUE,
                                                'UDA_ID'         as UDA_ID,
                                                NULL             as dummy)))
   LOOP
      BEGIN
         L_default_rec.UDA_VALUE_DESC := rec.UDA_VALUE_DESC_dv;
  EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA_VALUES ' ,
                            NULL,
                           'UDA_VALUE_DESC ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UDA_VALUE := rec.UDA_VALUE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA_VALUES ' ,
                            NULL,
                           'UDA_VALUE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UDA_ID := rec.UDA_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA_VALUES ' ,
                            NULL,
                           'UDA_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(UDA_VALUES$Action)           as Action,
                      r.get_cell(UDA_VALUES$UDA_VALUE_DESC)   as UDA_VALUE_DESC,
                      r.get_cell(UDA_VALUES$UDA_VALUE)        as UDA_VALUE,
                      r.get_cell(UDA_VALUES$UDA_ID)           as UDA_ID,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(UDA_VALUES_sheet))

   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_VALUES_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_VALUE_DESC := rec.UDA_VALUE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_VALUES_sheet,
                            rec.row_seq,
                            'UDA_VALUE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_VALUE := rec.UDA_VALUE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_VALUES_sheet,
                            rec.row_seq,
                            'UDA_VALUE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_ID := rec.UDA_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_VALUES_sheet,
                            rec.row_seq,
                            'UDA_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_UDA.action_new then
         L_temp_rec.UDA_VALUE_DESC := NVL( L_temp_rec.UDA_VALUE_DESC,L_default_rec.UDA_VALUE_DESC);
         L_temp_rec.UDA_VALUE      := NVL( L_temp_rec.UDA_VALUE,L_default_rec.UDA_VALUE);
         L_temp_rec.UDA_ID         := NVL( L_temp_rec.UDA_ID,L_default_rec.UDA_ID);
      end if;
      if not (L_temp_rec.UDA_VALUE is NOT NULL
              and L_temp_rec.UDA_ID is NOT NULL
         and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         UDA_VALUES_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_UDA_VALUES_col.extend();
         svc_UDA_VALUES_col(svc_UDA_VALUES_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_UDA_VALUES_col.COUNT SAVE EXCEPTIONS
      merge into SVC_UDA_VALUES st
      using(select(case
                   when L_mi_rec.UDA_VALUE_DESC_mi    = 'N'
                    and svc_UDA_VALUES_col(i).action  = CORESVC_UDA.action_mod
                    and s1.UDA_VALUE_DESC IS NULL then
                        mt.UDA_VALUE_DESC
                   else s1.UDA_VALUE_DESC
                   end) as UDA_VALUE_DESC,
                  (case
                   when L_mi_rec.UDA_VALUE_mi         = 'N'
                    and svc_UDA_VALUES_col(i).action  = CORESVC_UDA.action_mod
                    and s1.UDA_VALUE IS NULL then
                        mt.UDA_VALUE
                   else s1.UDA_VALUE
                   end) as UDA_VALUE,
                  (case
                   when L_mi_rec.UDA_ID_mi            = 'N'
                    and svc_UDA_VALUES_col(i).action  = CORESVC_UDA.action_mod
                    and s1.UDA_ID IS NULL then
                        mt.UDA_ID
                   else s1.UDA_ID
                   end) as UDA_ID,
                  null as dummy
             from (select svc_UDA_VALUES_col(i).UDA_VALUE_DESC as UDA_VALUE_DESC,
                          svc_UDA_VALUES_col(i).UDA_VALUE      as UDA_VALUE,
                          svc_UDA_VALUES_col(i).UDA_ID         as UDA_ID,
                          null as dummy
                     from dual ) s1,
                  UDA_VALUES mt
            where mt.UDA_VALUE (+)     = s1.UDA_VALUE
              and mt.UDA_ID (+)        = s1.UDA_ID
              and 1 = 1 )sq
               ON (st.UDA_VALUE        = sq.UDA_VALUE and
                   st.UDA_ID           = sq.UDA_ID    and
                   svc_UDA_VALUES_col(i).ACTION IN (CORESVC_UDA.action_mod,CORESVC_UDA.action_del))
      when matched then
      update
         set process_id      = svc_UDA_VALUES_col(i).process_id ,
             chunk_id        = svc_UDA_VALUES_col(i).chunk_id ,
             row_seq         = svc_UDA_VALUES_col(i).row_seq ,
             action          = svc_UDA_VALUES_col(i).action ,
             process$status  = svc_UDA_VALUES_col(i).process$status ,
             uda_value_desc  = sq.uda_value_desc ,
             create_id       = svc_UDA_VALUES_col(i).create_id ,
             create_datetime = svc_UDA_VALUES_col(i).create_datetime ,
             last_upd_id     = svc_UDA_VALUES_col(i).last_upd_id ,
             last_upd_datetime = svc_UDA_VALUES_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             uda_value_desc ,
             uda_value ,
             uda_id ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_UDA_VALUES_col(i).process_id ,
             svc_UDA_VALUES_col(i).chunk_id ,
             svc_UDA_VALUES_col(i).row_seq ,
             svc_UDA_VALUES_col(i).action ,
             svc_UDA_VALUES_col(i).process$status ,
             sq.uda_value_desc ,
             sq.uda_value ,
             sq.uda_id ,
             svc_UDA_VALUES_col(i).create_id ,
             svc_UDA_VALUES_col(i).create_datetime ,
             svc_UDA_VALUES_col(i).last_upd_id ,
             svc_UDA_VALUES_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             UDA_VALUES_sheet,
                             svc_UDA_VALUES_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_UDA_VALUES;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UDA_VALUES_TL( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   svc_uda_values_tl.PROCESS_ID%TYPE) IS

   TYPE svc_uda_values_tl_COL_TYP IS TABLE OF svc_uda_values_tl%ROWTYPE;
   L_temp_rec              svc_uda_values_tl%ROWTYPE;
   svc_uda_values_tl_COL   svc_uda_values_tl_COL_TYP := NEW svc_uda_values_tl_COL_TYP();
   L_process_id            svc_uda_values_tl.PROCESS_ID%TYPE;
   L_error                 BOOLEAN := FALSE;
   L_default_rec           svc_uda_values_tl%ROWTYPE;

   cursor C_MANDATORY_IND is
      select uda_value_desc_mi,
             uda_value_mi,
             uda_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'uda_values_tl')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('UDA_VALUE_DESC' as uda_value_desc,
                                       'UDA_VALUE'      as uda_value,
                                       'UDA_ID'         as uda_id,
                                       'LANG'           as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_UDA_VALUES_TL';
   L_pk_columns    VARCHAR2(255)  := 'UDA ID, UDA Value, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select uda_value_desc_dv,
                      uda_value_dv,
                      uda_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'uda_values_tl')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('UDA_VALUE_DESC' as uda_value_desc,
                                                'UDA_VALUE'      as uda_value,
                                                'UDA_ID'         as uda_id,
                                                'LANG'           as lang)))
   LOOP
      BEGIN
         L_default_rec.uda_value_desc := rec.uda_value_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_values_tl_SHEET ,
                            NULL,
                           'UDA_VALUE_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.uda_value := rec.uda_value_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_values_tl_SHEET ,
                            NULL,
                           'UDA_VALUE' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.uda_id := rec.uda_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           uda_values_tl_SHEET ,
                            NULL,
                           'UDA_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_values_tl_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(uda_values_tl$action))         as action,
                      r.get_cell(uda_values_tl$uda_value_desc)        as uda_value_desc,
                      r.get_cell(uda_values_tl$uda_value)             as uda_value,
                      r.get_cell(uda_values_tl$uda_id)                as uda_id,
                      r.get_cell(uda_values_tl$lang)                  as lang,
                      r.get_row_seq()                                 as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(uda_values_tl_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_values_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.uda_value_desc := rec.uda_value_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_values_tl_SHEET,
                            rec.row_seq,
                            'UDA_VALUE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.uda_value := rec.uda_value;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_values_tl_SHEET,
                            rec.row_seq,
                            'UDA_VALUE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.uda_id := rec.uda_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_values_tl_SHEET,
                            rec.row_seq,
                            'UDA_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_values_tl_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_UDA.action_new then
         L_temp_rec.uda_value_desc := NVL( L_temp_rec.uda_value_desc,L_default_rec.uda_value_desc);
         L_temp_rec.uda_value      := NVL( L_temp_rec.uda_value,L_default_rec.uda_value);
         L_temp_rec.uda_id         := NVL( L_temp_rec.uda_id,L_default_rec.uda_id);
         L_temp_rec.lang           := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.uda_id is NOT NULL and L_temp_rec.uda_value is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         uda_values_tl_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_uda_values_tl_col.extend();
         svc_uda_values_tl_col(svc_uda_values_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_uda_values_tl_col.COUNT SAVE EXCEPTIONS
      merge into svc_uda_values_tl st
      using(select
                  (case
                   when l_mi_rec.uda_value_desc_mi = 'N'
                    and svc_uda_values_tl_col(i).action = CORESVC_UDA.action_mod
                    and s1.uda_value_desc IS NULL then
                        mt.uda_value_desc
                   else s1.uda_value_desc
                   end) as uda_value_desc,
               (case
                   when l_mi_rec.uda_value_mi = 'N'
                    and svc_uda_values_tl_col(i).action = CORESVC_UDA.action_mod
                    and s1.uda_value IS NULL then
                        mt.uda_value
                   else s1.uda_value
                   end) as uda_value,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_uda_values_tl_col(i).action = CORESVC_UDA.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.uda_id_mi = 'N'
                    and svc_uda_values_tl_col(i).action = CORESVC_UDA.action_mod
                    and s1.uda_id IS NULL then
                        mt.uda_id
                   else s1.uda_id
                   end) as uda_id
              from (select svc_uda_values_tl_col(i).uda_value_desc as uda_value_desc,
                           svc_uda_values_tl_col(i).uda_value      as uda_value,
                           svc_uda_values_tl_col(i).uda_id         as uda_id,
                           svc_uda_values_tl_col(i).lang           as lang
                      from dual) s1,
                   uda_values_tl mt
             where mt.uda_id (+) = s1.uda_id
               and mt.uda_value (+) = s1.uda_value
               and mt.lang (+)       = s1.lang) sq
                on (st.uda_id = sq.uda_id and
                    st.uda_value = sq.uda_value and
                    st.lang = sq.lang and
                    svc_uda_values_tl_col(i).ACTION IN (CORESVC_UDA.action_mod,CORESVC_UDA.action_del))
      when matched then
      update
         set process_id        = svc_uda_values_tl_col(i).process_id ,
             chunk_id          = svc_uda_values_tl_col(i).chunk_id ,
             row_seq           = svc_uda_values_tl_col(i).row_seq ,
             action            = svc_uda_values_tl_col(i).action ,
             process$status    = svc_uda_values_tl_col(i).process$status ,
             uda_value_desc    = sq.uda_value_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             uda_value_desc ,
             uda_value,
             uda_id ,
             lang)
      values(svc_uda_values_tl_col(i).process_id ,
             svc_uda_values_tl_col(i).chunk_id ,
             svc_uda_values_tl_col(i).row_seq ,
             svc_uda_values_tl_col(i).action ,
             svc_uda_values_tl_col(i).process$status ,
             sq.uda_value_desc ,
             sq.uda_value ,
             sq.uda_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            uda_values_tl_SHEET,
                            svc_uda_values_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_UDA_VALUES_TL;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UDA( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                           I_process_id   IN   SVC_UDA.process_id%TYPE) IS

   TYPE svc_UDA_coL_typ IS TABLE OF SVC_UDA%ROWTYPE;
   L_temp_rec      SVC_UDA%ROWTYPE;
   svc_UDA_col     svc_UDA_coL_typ :=NEW svc_UDA_coL_typ();
   L_process_id    SVC_UDA.process_id%TYPE;
   L_error         BOOLEAN:=FALSE;
   L_default_rec   SVC_UDA%ROWTYPE;

   cursor C_MANDATORY_IND is
      select UDA_DESC_mi,
             UDA_ID_mi,
             FILTER_MERCH_ID_SUBCLASS_mi,
             FILTER_MERCH_ID_CLASS_mi,
             FILTER_MERCH_ID_mi,
             FILTER_ORG_ID_mi,
             SINGLE_VALUE_IND_mi,
             DATA_LENGTH_mi,
             DATA_TYPE_mi,
             DISPLAY_TYPE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = template_key
                 and wksht_key = 'UDA')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('UDA_DESC'                 as UDA_DESC,
                                       'UDA_ID'                   as UDA_ID,
                                       'FILTER_MERCH_ID_SUBCLASS' as FILTER_MERCH_ID_SUBCLASS,
                                       'FILTER_MERCH_ID_CLASS'    as FILTER_MERCH_ID_CLASS,
                                       'FILTER_MERCH_ID'          as FILTER_MERCH_ID,
                                       'FILTER_ORG_ID'            as FILTER_ORG_ID,
                                       'SINGLE_VALUE_IND'         as SINGLE_VALUE_IND,
                                       'DATA_LENGTH'              as DATA_LENGTH,
                                       'DATA_TYPE'                as DATA_TYPE,
                                       'DISPLAY_TYPE'             as DISPLAY_TYPE,
                                       null                       as dummy));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_UDA';
   L_pk_columns    VARCHAR2(255)  := 'UDA';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN (select UDA_DESC_dv,
                      UDA_ID_dv,
                      FILTER_MERCH_ID_SUBCLASS_dv,
                      FILTER_MERCH_ID_CLASS_dv,
                      FILTER_MERCH_ID_dv,
                      FILTER_ORG_ID_dv,
                      SINGLE_VALUE_IND_dv,
                      DATA_LENGTH_dv,
                      DATA_TYPE_dv,
                      DISPLAY_TYPE_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key = template_key
                          and wksht_key    = 'UDA')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('UDA_DESC'                 as UDA_DESC,
                                                'UDA_ID'                   as UDA_ID,
                                                'FILTER_MERCH_ID_SUBCLASS' as FILTER_MERCH_ID_SUBCLASS,
                                                'FILTER_MERCH_ID_CLASS'    as FILTER_MERCH_ID_CLASS,
                                                'FILTER_MERCH_ID'          as FILTER_MERCH_ID,
                                                'FILTER_ORG_ID'            as FILTER_ORG_ID,
                                                'SINGLE_VALUE_IND'         as SINGLE_VALUE_IND,
                                                'DATA_LENGTH'              as DATA_LENGTH,
                                                'DATA_TYPE'                as DATA_TYPE,
                                                'DISPLAY_TYPE'             as DISPLAY_TYPE,
                                                NULL                       as dummy)))
   LOOP
      BEGIN
         L_default_rec.UDA_DESC := rec.UDA_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'UDA_DESC ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UDA_ID := rec.UDA_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'UDA_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FILTER_MERCH_ID_SUBCLASS := rec.FILTER_MERCH_ID_SUBCLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'FILTER_MERCH_ID_SUBCLASS ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FILTER_MERCH_ID_CLASS := rec.FILTER_MERCH_ID_CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'FILTER_MERCH_ID_CLASS ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FILTER_MERCH_ID := rec.FILTER_MERCH_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'FILTER_MERCH_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.FILTER_ORG_ID := rec.FILTER_ORG_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'FILTER_ORG_ID ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SINGLE_VALUE_IND := rec.SINGLE_VALUE_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'SINGLE_VALUE_IND ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DATA_LENGTH := rec.DATA_LENGTH_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'DATA_LENGTH ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DATA_TYPE := rec.DATA_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'DATA_TYPE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DISPLAY_TYPE := rec.DISPLAY_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'UDA ' ,
                            NULL,
                           'DISPLAY_TYPE ' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(UDA$Action)                   as Action,
                      r.get_cell(UDA$UDA_DESC)                 as UDA_DESC,
                      r.get_cell(UDA$UDA_ID)                   as UDA_ID,
                      r.get_cell(UDA$FILTER_MERCH_ID_SUBCLASS) as FILTER_MERCH_ID_SUBCLASS,
                      r.get_cell(UDA$FILTER_MERCH_ID_CLASS)    as FILTER_MERCH_ID_CLASS,
                      r.get_cell(UDA$FILTER_MERCH_ID)          as FILTER_MERCH_ID,
                      r.get_cell(UDA$FILTER_ORG_ID)            as FILTER_ORG_ID,
                      r.get_cell(UDA$SINGLE_VALUE_IND)         as SINGLE_VALUE_IND,
                      r.get_cell(UDA$DATA_LENGTH)              as DATA_LENGTH,
                      r.get_cell(UDA$DATA_TYPE)                as DATA_TYPE,
                      r.get_cell(UDA$DISPLAY_TYPE)             as DISPLAY_TYPE,
                      r.get_row_seq()                          as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(UDA_sheet))

   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := LP_user;
      L_temp_rec.last_upd_id       := LP_user;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_DESC := rec.UDA_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'UDA_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_ID := rec.UDA_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'UDA_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID_SUBCLASS := rec.FILTER_MERCH_ID_SUBCLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID_SUBCLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID_CLASS := rec.FILTER_MERCH_ID_CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID_CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_MERCH_ID := rec.FILTER_MERCH_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'FILTER_MERCH_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.FILTER_ORG_ID := rec.FILTER_ORG_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'FILTER_ORG_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SINGLE_VALUE_IND := rec.SINGLE_VALUE_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'SINGLE_VALUE_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DATA_LENGTH := rec.DATA_LENGTH;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'DATA_LENGTH',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DATA_TYPE := rec.DATA_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'DATA_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DISPLAY_TYPE := rec.DISPLAY_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_sheet,
                            rec.row_seq,
                            'DISPLAY_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_UDA.action_new then
         L_temp_rec.UDA_DESC                 := NVL( L_temp_rec.UDA_DESC,
                                                     L_default_rec.UDA_DESC);
         L_temp_rec.UDA_ID                   := NVL( L_temp_rec.UDA_ID,
                                                     L_default_rec.UDA_ID);
         L_temp_rec.FILTER_MERCH_ID_SUBCLASS := NVL( L_temp_rec.FILTER_MERCH_ID_SUBCLASS,
                                                     L_default_rec.FILTER_MERCH_ID_SUBCLASS);
         L_temp_rec.FILTER_MERCH_ID_CLASS    := NVL( L_temp_rec.FILTER_MERCH_ID_CLASS,
                                                     L_default_rec.FILTER_MERCH_ID_CLASS);
         L_temp_rec.FILTER_MERCH_ID          := NVL( L_temp_rec.FILTER_MERCH_ID,
                                                     L_default_rec.FILTER_MERCH_ID);
         L_temp_rec.FILTER_ORG_ID            := NVL( L_temp_rec.FILTER_ORG_ID,
                                                     L_default_rec.FILTER_ORG_ID);
         L_temp_rec.SINGLE_VALUE_IND         := NVL( L_temp_rec.SINGLE_VALUE_IND,
                                                     L_default_rec.SINGLE_VALUE_IND);
         L_temp_rec.DATA_LENGTH              := NVL( L_temp_rec.DATA_LENGTH,
                                                     L_default_rec.DATA_LENGTH);
         L_temp_rec.DATA_TYPE                := NVL( L_temp_rec.DATA_TYPE,
                                                     L_default_rec.DATA_TYPE);
         L_temp_rec.DISPLAY_TYPE             := NVL( L_temp_rec.DISPLAY_TYPE,
                                                     L_default_rec.DISPLAY_TYPE);

      end if;
      if NOT (L_temp_rec.UDA_ID is NOT NULL
         and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         UDA_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_UDA_col.extend();
         svc_UDA_col(svc_UDA_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      forall i IN 1..svc_UDA_col.COUNT SAVE EXCEPTIONS
      merge into SVC_UDA st
      using(select(case
                   when L_mi_rec.UDA_DESC_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.UDA_DESC IS NULL then
                        mt.UDA_DESC
                   else s1.UDA_DESC
                   end) as UDA_DESC,
                  (case
                   when L_mi_rec.UDA_ID_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.UDA_ID IS NULL then
                        mt.UDA_ID
                   else s1.UDA_ID
                   end) as UDA_ID,
                  (case
                   when L_mi_rec.FILTER_MERCH_ID_SUBCLASS_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.FILTER_MERCH_ID_SUBCLASS IS NULL then
                        mt.FILTER_MERCH_ID_SUBCLASS
                   else s1.FILTER_MERCH_ID_SUBCLASS
                   end) as FILTER_MERCH_ID_SUBCLASS,
                  (case
                   when L_mi_rec.FILTER_MERCH_ID_CLASS_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.FILTER_MERCH_ID_CLASS IS NULL then
                        mt.FILTER_MERCH_ID_CLASS
                   else s1.FILTER_MERCH_ID_CLASS
                   end) as FILTER_MERCH_ID_CLASS,
                  (case
                   when L_mi_rec.FILTER_MERCH_ID_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.FILTER_MERCH_ID IS NULL then
                        mt.FILTER_MERCH_ID
                   else s1.FILTER_MERCH_ID
                   end) as FILTER_MERCH_ID,
                  (case
                   when L_mi_rec.FILTER_ORG_ID_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.FILTER_ORG_ID IS NULL then
                        mt.FILTER_ORG_ID
                   else s1.FILTER_ORG_ID
                   end) as FILTER_ORG_ID,
                  (case
                   when L_mi_rec.SINGLE_VALUE_IND_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.SINGLE_VALUE_IND IS NULL then
                        mt.SINGLE_VALUE_IND
                   else s1.SINGLE_VALUE_IND
                   end) as SINGLE_VALUE_IND,
                  (case
                   when L_mi_rec.DATA_LENGTH_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.DATA_LENGTH IS NULL then
                        mt.DATA_LENGTH
                   else s1.DATA_LENGTH
                   end) as DATA_LENGTH,
                  (case
                   when L_mi_rec.DATA_TYPE_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.DATA_TYPE IS NULL then
                        mt.DATA_TYPE
                   else s1.DATA_TYPE
                   end) as DATA_TYPE,
                  (case
                   when L_mi_rec.DISPLAY_TYPE_mi    = 'N'
                    and svc_UDA_col(i).action = CORESVC_UDA.action_mod
                    and s1.DISPLAY_TYPE IS NULL then
                        mt.DISPLAY_TYPE
                   else s1.DISPLAY_TYPE
                   end) as DISPLAY_TYPE,
                  null as dummy
             from (select svc_UDA_col(i).UDA_DESC                 as UDA_DESC,
                          svc_UDA_col(i).UDA_ID                   as UDA_ID,
                          svc_UDA_col(i).FILTER_MERCH_ID_SUBCLASS as FILTER_MERCH_ID_SUBCLASS,
                          svc_UDA_col(i).FILTER_MERCH_ID_CLASS    as FILTER_MERCH_ID_CLASS,
                          svc_UDA_col(i).FILTER_MERCH_ID          as FILTER_MERCH_ID,
                          svc_UDA_col(i).FILTER_ORG_ID            as FILTER_ORG_ID,
                          svc_UDA_col(i).SINGLE_VALUE_IND         as SINGLE_VALUE_IND,
                          svc_UDA_col(i).DATA_LENGTH              as DATA_LENGTH,
                          svc_UDA_col(i).DATA_TYPE                as DATA_TYPE,
                          svc_UDA_col(i).DISPLAY_TYPE             as DISPLAY_TYPE,
                          null                                    as dummy
                     from dual ) s1,
                  UDA mt
            where mt.UDA_ID (+)     = s1.UDA_ID
              and 1 = 1 ) sq
               ON (st.UDA_ID      = sq.UDA_ID and
                   svc_UDA_col(i).ACTION IN (CORESVC_UDA.action_mod,CORESVC_UDA.action_del))
      when matched then
      update
         set process_id               = svc_UDA_col(i).process_id ,
             chunk_id                 = svc_UDA_col(i).chunk_id ,
             row_seq                  = svc_UDA_col(i).row_seq ,
             action                   = svc_UDA_col(i).action ,
             process$status           = svc_UDA_col(i).process$status ,
             display_type             = sq.display_type ,
             filter_merch_id          = sq.filter_merch_id ,
             filter_merch_id_subclass = sq.filter_merch_id_subclass ,
             uda_desc                 = sq.uda_desc ,
             data_type                = sq.data_type ,
             filter_org_id            = sq.filter_org_id ,
             single_value_ind         = sq.single_value_ind ,
             data_length              = sq.data_length ,
             filter_merch_id_class    = sq.filter_merch_id_class ,
             create_id                = svc_UDA_col(i).create_id ,
             create_datetime          = svc_UDA_col(i).create_datetime ,
             last_upd_id              = svc_UDA_col(i).last_upd_id ,
             last_upd_datetime        = svc_UDA_col(i).last_upd_datetime

      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             uda_desc ,
             uda_id ,
             filter_merch_id_subclass ,
             filter_merch_id_class ,
             filter_merch_id ,
             filter_org_id ,
             single_value_ind ,
             data_length ,
             data_type ,
             display_type ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_UDA_col(i).process_id ,
             svc_UDA_col(i).chunk_id ,
             svc_UDA_col(i).row_seq ,
             svc_UDA_col(i).action ,
             svc_UDA_col(i).process$status ,
             sq.uda_desc ,
             sq.uda_id ,
             sq.filter_merch_id_subclass ,
             sq.filter_merch_id_class ,
             sq.filter_merch_id ,
             sq.filter_org_id ,
             sq.single_value_ind ,
             sq.data_length ,
             sq.data_type ,
             sq.display_type ,
             svc_UDA_col(i).create_id ,
             svc_UDA_col(i).create_datetime ,
             svc_UDA_col(i).last_upd_id ,
             svc_UDA_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             UDA_sheet,
                             svc_UDA_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_UDA;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UDA_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_UDA_TL.PROCESS_ID%TYPE) IS

   TYPE svc_uda_tl_col_TYP IS TABLE OF SVC_UDA_TL%ROWTYPE;
   L_temp_rec         SVC_UDA_TL%ROWTYPE;
   svc_uda_tl_col     svc_uda_tl_col_TYP := NEW svc_uda_tl_col_TYP();
   L_process_id       SVC_UDA_TL.PROCESS_ID%TYPE;
   L_error            BOOLEAN := FALSE;
   L_default_rec      SVC_UDA_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select uda_desc_mi,
             uda_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'UDA_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('UDA_DESC' as uda_desc,
                                       'UDA_ID'   as uda_id,
                                       'LANG'     as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_UDA_TL';
   L_pk_columns    VARCHAR2(255)  := 'UDA ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select uda_desc_dv,
                      uda_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'UDA_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('UDA_DESC' as uda_desc,
                                                'UDA_ID'   as uda_id,
                                                'LANG'     as lang)))
   LOOP
      BEGIN
         L_default_rec.uda_desc := rec.uda_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_TL_SHEET ,
                            NULL,
                           'UDA_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.uda_id := rec.uda_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           UDA_TL_SHEET ,
                            NULL,
                           'UDA_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(uda_tl$action)     as action,
                      r.get_cell(uda_tl$uda_desc)   as uda_desc,
                      r.get_cell(uda_tl$uda_id)     as uda_id,
                      r.get_cell(uda_tl$lang)       as lang,
                      r.get_row_seq()               as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(UDA_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            uda_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.uda_desc := rec.uda_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_TL_SHEET,
                            rec.row_seq,
                            'UDA_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.uda_id := rec.uda_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_TL_SHEET,
                            rec.row_seq,
                            'UDA_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UDA_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_UDA.action_new then
         L_temp_rec.uda_desc := NVL( L_temp_rec.uda_desc,L_default_rec.uda_desc);
         L_temp_rec.uda_id   := NVL( L_temp_rec.uda_id,L_default_rec.uda_id);
         L_temp_rec.lang     := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.uda_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         UDA_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_uda_tl_col.extend();
         svc_uda_tl_col(svc_uda_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_uda_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_uda_TL st
      using(select
                  (case
                   when l_mi_rec.uda_desc_mi = 'N'
                    and svc_uda_tl_col(i).action = CORESVC_UDA.action_mod
                    and s1.uda_desc IS NULL then
                        mt.uda_desc
                   else s1.uda_desc
                   end) as uda_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_uda_tl_col(i).action = CORESVC_UDA.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.uda_id_mi = 'N'
                    and svc_uda_tl_col(i).action = CORESVC_UDA.action_mod
                    and s1.uda_id IS NULL then
                        mt.uda_id
                   else s1.uda_id
                   end) as uda_id
              from (select svc_uda_tl_col(i).uda_desc as uda_desc,
                           svc_uda_tl_col(i).uda_id        as uda_id,
                           svc_uda_tl_col(i).lang              as lang
                      from dual) s1,
                   uda_TL mt
             where mt.uda_id (+) = s1.uda_id
               and mt.lang (+)       = s1.lang) sq
                on (st.uda_id = sq.uda_id and
                    st.lang = sq.lang and
                    svc_uda_tl_col(i).ACTION IN (CORESVC_UDA.action_mod,CORESVC_UDA.action_del))
      when matched then
      update
         set process_id        = svc_uda_tl_col(i).process_id ,
             chunk_id          = svc_uda_tl_col(i).chunk_id ,
             row_seq           = svc_uda_tl_col(i).row_seq ,
             action            = svc_uda_tl_col(i).action ,
             process$status    = svc_uda_tl_col(i).process$status ,
             uda_desc = sq.uda_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             uda_desc ,
             uda_id ,
             lang)
      values(svc_uda_tl_col(i).process_id ,
             svc_uda_tl_col(i).chunk_id ,
             svc_uda_tl_col(i).row_seq ,
             svc_uda_tl_col(i).action ,
             svc_uda_tl_col(i).process$status ,
             sq.uda_desc ,
             sq.uda_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            UDA_TL_SHEET,
                            svc_uda_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_UDA_TL;
----------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_UID( I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                           I_process_id   IN   SVC_UDA_ITEM_DEFAULTS.process_id%TYPE) IS
                           
   TYPE svc_UID_coL_typ IS TABLE OF SVC_UDA_ITEM_DEFAULTS%ROWTYPE;
   L_temp_rec      SVC_UDA_ITEM_DEFAULTS%ROWTYPE;
   svc_UID_col     svc_UID_coL_typ :=NEW svc_UID_coL_typ();
   L_process_id    SVC_UDA_ITEM_DEFAULTS.process_id%TYPE;
   L_error         BOOLEAN:=FALSE;
   L_default_rec   SVC_UDA_ITEM_DEFAULTS%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select REQUIRED_IND_mi,
             UDA_VALUE_mi,
             SUBCLASS_mi,
             CLASS_mi,
             DEPT_mi,
             UDA_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key = template_key
                 and wksht_key    = 'UID') 
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('REQUIRED_IND' as REQUIRED_IND,
                                                                'UDA_VALUE'    as UDA_VALUE,
                                                                'SUBCLASS'     as SUBCLASS,
                                                                'CLASS'        as CLASS,
                                                                'DEPT'         as DEPT,
                                                                'UDA_ID'       as UDA_ID,
                                                                 NULL          as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_UDA_ITEM_DEFAULTS';
   L_pk_columns    VARCHAR2(255)  := 'UDA,Department';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;   
      
BEGIN
  -- Get default values.
   FOR rec IN (select REQUIRED_IND_dv,
                      UDA_VALUE_dv,
                      SUBCLASS_dv,
                      CLASS_dv,
                      DEPT_dv,
                      UDA_ID_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key = template_key
                          and wksht_key    = 'UID') 
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('REQUIRED_IND' as REQUIRED_IND,
                                                                             'UDA_VALUE'    as UDA_VALUE,
                                                                             'SUBCLASS'     as SUBCLASS,
                                                                             'CLASS'        as CLASS,
                                                                             'DEPT'         as DEPT,
                                                                             'UDA_ID'       as UDA_ID,
                                                                              NULL          as dummy)))
   LOOP
      BEGIN
         L_default_rec.REQUIRED_IND := rec.REQUIRED_IND_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'UDA_ITEM_DEFAULTS ' ,
                             NULL,
                            'REQUIRED_IND ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UDA_VALUE := rec.UDA_VALUE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'UDA_ITEM_DEFAULTS ' ,
                             NULL,
                            'UDA_VALUE ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SUBCLASS := rec.SUBCLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'UDA_ITEM_DEFAULTS ' ,
                             NULL,
                            'SUBCLASS ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.CLASS := rec.CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'UDA_ITEM_DEFAULTS ' ,
                             NULL,
                            'CLASS ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DEPT := rec.DEPT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'UDA_ITEM_DEFAULTS ' ,
                             NULL,
                            'DEPT ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.UDA_ID := rec.UDA_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'UDA_ITEM_DEFAULTS ' ,
                             NULL,
                            'UDA_ID ' ,
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(UID$Action)        as Action,
                      r.get_cell(UID$REQUIRED_IND)  as REQUIRED_IND,
                      r.get_cell(UID$UDA_VALUE)     as UDA_VALUE,
                      r.get_cell(UID$SUBCLASS)      as SUBCLASS,
                      r.get_cell(UID$CLASS)         as CLASS,
                      r.get_cell(UID$DEPT)          as DEPT,
                      r.get_cell(UID$UDA_VALUE_UPD) as UDA_VALUE_UPD,
                      r.get_cell(UID$UDA_ID)        as UDA_ID,
                      r.get_row_seq()               as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(UID_sheet))
                  
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UID_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.REQUIRED_IND := rec.REQUIRED_IND;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UID_sheet,
                            rec.row_seq,
                            'REQUIRED_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_VALUE := rec.UDA_VALUE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UID_sheet,
                            rec.row_seq,
                            'UDA_VALUE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SUBCLASS := rec.SUBCLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UID_sheet,
                            rec.row_seq,
                            'SUBCLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CLASS := rec.CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UID_sheet,
                            rec.row_seq,
                            'CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEPT := rec.DEPT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UID_sheet,
                            rec.row_seq,
                            'DEPT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_VALUE_UPD := rec.UDA_VALUE_UPD;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UID_sheet,
                            rec.row_seq,
                            'UDA_VALUE_UPD',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.UDA_ID := rec.UDA_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            UID_sheet,
                            rec.row_seq,
                            'UDA_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_UDA.action_new then
         L_temp_rec.REQUIRED_IND := NVL( L_temp_rec.REQUIRED_IND,
                                         L_default_rec.REQUIRED_IND);
         L_temp_rec.UDA_VALUE    := NVL( L_temp_rec.UDA_VALUE,
                                         L_default_rec.UDA_VALUE);
         L_temp_rec.SUBCLASS     := NVL( L_temp_rec.SUBCLASS,
                                         L_default_rec.SUBCLASS);
         L_temp_rec.CLASS        := NVL( L_temp_rec.CLASS,
                                         L_default_rec.CLASS);
         L_temp_rec.DEPT         := NVL( L_temp_rec.DEPT,
                                         L_default_rec.DEPT);
         L_temp_rec.UDA_ID       := NVL( L_temp_rec.UDA_ID,
                                         L_default_rec.UDA_ID);
      end if;
      if NOT (L_temp_rec.DEPT is NOT NULL  
          and L_temp_rec.UDA_ID is NOT NULL 
          and 1 = 1) then
         WRITE_S9T_ERROR( I_file_id,
                          UID_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_UID_col.extend();
         svc_UID_col(svc_UID_col.COUNT()) := L_temp_rec;
      end if;
      
   END LOOP;
   
   BEGIN
      forall i IN 1..svc_UID_col.COUNT SAVE EXCEPTIONS
      merge into SVC_UDA_ITEM_DEFAULTS st
      using(select(case
                   when L_mi_rec.REQUIRED_IND_mi  = 'N'
                    and svc_UID_col(i).action = CORESVC_UDA.action_mod
                    and s1.REQUIRED_IND IS NULL then
                        mt.REQUIRED_IND
                   else s1.REQUIRED_IND
                   end) as REQUIRED_IND,
                  (case
                   when L_mi_rec.UDA_VALUE_mi     = 'N'
                    and svc_UID_col(i).action = CORESVC_UDA.action_mod
                    and s1.UDA_VALUE IS NULL then
                        mt.UDA_VALUE
                   else s1.UDA_VALUE
                   end) as UDA_VALUE,
                  (case
                   when L_mi_rec.SUBCLASS_mi      = 'N'
                    and svc_UID_col(i).action = CORESVC_UDA.action_mod
                    and s1.SUBCLASS IS NULL then
                        mt.SUBCLASS
                   else s1.SUBCLASS
                   end) as SUBCLASS,
                  (case
                   when L_mi_rec.CLASS_mi         = 'N'
                    and svc_UID_col(i).action = CORESVC_UDA.action_mod
                    and s1.CLASS IS NULL then
                        mt.CLASS
                   else s1.CLASS
                   end) as CLASS,
                  (case
                   when L_mi_rec.DEPT_mi          = 'N'
                    and svc_UID_col(i).action = CORESVC_UDA.action_mod
                    and s1.DEPT IS NULL then
                        mt.DEPT
                   else s1.DEPT
                   end) as DEPT,
                  (case
                   when L_mi_rec.UDA_ID_mi        = 'N'
                    and svc_UID_col(i).action = CORESVC_UDA.action_mod
                    and s1.UDA_ID IS NULL then
                        mt.UDA_ID
                   else s1.UDA_ID
                   end) as UDA_ID,
                  null as dummy
             from (select svc_UID_col(i).REQUIRED_IND as REQUIRED_IND,
                          svc_UID_col(i).UDA_VALUE    as UDA_VALUE,
                          svc_UID_col(i).SUBCLASS     as SUBCLASS,
                          svc_UID_col(i).CLASS        as CLASS,
                          svc_UID_col(i).DEPT         as DEPT,
                          svc_UID_col(i).UDA_ID       as UDA_ID,
                          null as dummy
                     from dual ) s1,
                  UDA_ITEM_DEFAULTS mt
             where mt.DEPT (+)              = s1.DEPT    
               and NVL(mt.CLASS (+),-1)     = NVL(s1.CLASS,-1)   
               and NVL(mt.SUBCLASS (+),-1)  = NVL(s1.SUBCLASS,-1)   
               and NVL(mt.UDA_VALUE (+),-1) = NVL(s1.UDA_VALUE,-1)   
               and mt.UDA_ID (+)            = s1.UDA_ID   
               and 1 = 1 )sq
                ON (st.UDA_ID            = sq.UDA_ID and
                    NVL(st.DEPT,-1)      = NVL(sq.DEPT,-1) and
                    NVL(st.CLASS,-1)     = NVL(sq.CLASS,-1) and
                    NVL(st.SUBCLASS,-1)  = NVL(sq.SUBCLASS,-1) and
                    NVL(st.UDA_VALUE,-1) = NVL(sq.UDA_VALUE,-1) and
                    svc_UID_col(i).ACTION IN (CORESVC_UDA.action_mod,CORESVC_UDA.action_del))
      when matched then
      update
         set process_id        = svc_UID_col(i).process_id ,
             chunk_id          = svc_UID_col(i).chunk_id ,
             row_seq           = svc_UID_col(i).row_seq ,
             action            = svc_UID_col(i).action ,
             process$status    = svc_UID_col(i).process$status ,
             required_ind      = sq.required_ind ,
             UDA_VALUE_UPD     = svc_UID_col(i).UDA_VALUE_UPD,
             create_id         = svc_UID_col(i).create_id ,
             create_datetime   = svc_UID_col(i).create_datetime ,
             last_upd_id       = svc_UID_col(i).last_upd_id ,
             last_upd_datetime = svc_UID_col(i).last_upd_datetime
      when NOT matched then
         insert(process_id,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                required_ind ,
                uda_value ,
                uda_value_upd,
                subclass ,
                class ,
                dept ,
                uda_id ,
                create_id ,
                create_datetime ,
                last_upd_id ,
                last_upd_datetime)
         values(svc_UID_col(i).process_id ,
                svc_UID_col(i).chunk_id ,
                svc_UID_col(i).row_seq ,
                svc_UID_col(i).action ,
                svc_UID_col(i).process$status ,
                sq.required_ind ,
                sq.uda_value ,
                svc_UID_col(i).uda_value_upd,
                sq.subclass ,
                sq.class ,
                sq.dept ,
                sq.uda_id ,
                svc_UID_col(i).create_id ,
                svc_UID_col(i).create_datetime ,
                svc_UID_col(i).last_upd_id ,
                svc_UID_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             UID_sheet,
                             svc_UID_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);                            
         END LOOP;
   END;
END PROCESS_S9T_UID;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_UDA.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR       EXCEPTION;
   PRAGMA         EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
  
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file); 

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_UDA(I_file_id,I_process_id);
      PROCESS_S9T_UDA_VALUES(I_file_id,I_process_id);
      PROCESS_S9T_UDA_TL(I_file_id,I_process_id);
      PROCESS_S9T_UDA_VALUES_TL(I_file_id,I_process_id);
      PROCESS_S9T_UID(I_file_id,I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   --Update process$status in svc_process_tracker
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_UDA_VALUES_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error           IN OUT   BOOLEAN,
                                I_rec             IN       C_SVC_UDA%ROWTYPE,
                                I_disp_type       IN       UDA.DISPLAY_TYPE%TYPE ,
                                I_data_type       IN       UDA.DATA_TYPE%TYPE ,
                                I_data_length     IN       UDA.DATA_LENGTH%TYPE)

RETURN BOOLEAN IS
   L_program     VARCHAR2(64) := 'CORESVC_UDA.PROCESS_UDA_VALUES_VAL';
   L_table_udv   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA_VALUES';
   L_exists      BOOLEAN := FALSE;
   L_dup         VARCHAR2(1);

   cursor C_EXISTS IS
      select 'x'
        from uda_values
       where uda_id = I_rec.udv_uda_id
         and uda_value = I_rec.udv_uda_value;

BEGIN
   if (I_disp_type NOT IN ('LV')
      or I_rec.uda_display_type IN ('FF','DT'))then
      WRITE_ERROR(I_rec.udv_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.udv_chunk_id,
                  L_table_udv,
                  I_rec.udv_row_seq,
                  'Display Type',
                  'NOT_LOV_DISPLAY_TYPE');
      O_error :=TRUE;
   end if;

   if (I_rec.uda_data_type = 'ALPHA'
      or I_data_type = 'ALPHA')then
      if I_rec.udv_uda_value_desc is NOT NULL then
         if ITEM_ATTRIB_SQL.CHECK_DESC(O_error_message,
                                       I_rec.udv_uda_value_desc,
                                       L_exists) = FALSE then
            WRITE_ERROR(I_rec.udv_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.udv_chunk_id,
                        L_table_udv,
                        I_rec.udv_row_seq,
                        NULL,
                        O_error_message);
            O_error :=TRUE;
         elsif L_exists = TRUE then
            WRITE_ERROR(I_rec.udv_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.udv_chunk_id,
                        L_table_udv,
                        I_rec.udv_row_seq,
                        'UDA_VALUE_DESC',
                        'NO_SPL_CHR');
            O_error :=TRUE;
         end if;
      end if;
   end if;
   if I_rec.udv_action = action_new
      and I_rec.udv_uda_value is NOT NULL then
      OPEN C_EXISTS;
      FETCH C_EXISTS into L_dup;
      if C_EXISTS%FOUND then
         WRITE_ERROR(I_rec.udv_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.udv_chunk_id,
                     L_table_udv,
                     I_rec.udv_row_seq,
                     'UDA,UDA Value',
                     'NO_DUPL_UDA_VAL');
         O_error :=TRUE;
      end if;
      CLOSE C_EXISTS;
   end if;

   if (I_rec.uda_data_length is not NULL
       or I_data_length is not NULL) then
      if NVL(LENGTHB(I_rec.udv_uda_value_desc), 0) > NVL(I_rec.uda_data_length, I_data_length) then
         WRITE_ERROR(I_rec.udv_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.udv_chunk_id,
                     L_table_udv,
                     I_rec.udv_row_seq,
                     'UDA_VALUE_DESC',
                     'ERR_DESC_EXC_MAX');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_EXISTS%ISOPEN then
         close C_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UDA_VALUES_VAL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_UID_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error             IN OUT   BOOLEAN,
                         I_rec               IN       C_SVC_UID%ROWTYPE,
                         I_hierarchy_value   IN       NUMBER) 
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      :='CORESVC_UDA.PROCESS_UID_VAL';         
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_UDA_ITEM_DEFAULTS';   
   L_dup               VARCHAR2(1)                       := NULL;
   L_dup_check         VARCHAR2(1)                       := NULL;   
   L_valid             BOOLEAN                           := FALSE;   
   L_UDA_HIER_REQUIRED BOOLEAN                           := TRUE; --GET_USER prompted to continue by setting levels to YES/NO in the Form has been Hard coded as 'YES' 
   L_error_message     VARCHAR2(600);
   L_error             BOOLEAN;   
   L_exists            BOOLEAN;
   L_not_required      BOOLEAN;
   L_required          BOOLEAN;   
   L_uda_row           V_UDA%ROWTYPE;
   L_uda_desc          UDA.uda_desc%TYPE;
   L_dept_name         V_DEPS.DEPT_NAME%TYPE;   
   L_display_type      UDA.display_type%TYPE; 
   L_class             V_CLASS.CLASS_NAME%TYPE;
   L_subclass          V_SUBCLASS.SUB_NAME%TYPE;       
   L_single_value_ind  UDA.single_value_ind%TYPE;

-- To check for duplicates while Attaching a New UID
   cursor C_DUP is
      select 'X' into L_dup 
        from uda_item_defaults 
       where uda_id = I_rec.uda_id
         and dept = I_rec.dept
         and NVL(class,-1) = NVL(I_rec.class,-1)
         and NVL(subclass,-1) = NVL(I_rec.subclass,-1)
         and NVL(uda_value,-1) = NVL(I_rec.uda_value,-1)
         and hierarchy_value = I_hierarchy_value;

-- To check for duplicates while Modifying a Record
   cursor C_DUP_UPD is
      select 'X' into L_dup 
        from uda_item_defaults 
       where uda_id = I_rec.uda_id
         and dept = I_rec.dept
         and NVL(class,-1) = NVL(I_rec.class,-1)
         and NVL(subclass,-1) = NVL(I_rec.subclass,-1)
         and NVL(uda_value,-1) = NVL(I_rec.uda_value_upd,-1)
         and hierarchy_value = I_hierarchy_value;

   cursor C_GET_DISPLAY_TYPE is
      select display_type, single_value_ind
        from uda
       where uda_id = I_rec.uda_id;

BEGIN
   if I_rec.action = action_new then
      open C_DUP;
      fetch C_DUP into L_dup_check; 
      close C_DUP;
      if L_dup_check = 'X' then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'UDA,UDA Value,Department,Class,Subclass',
                     'UDA_VALUE_EXISTS');
         O_error :=TRUE;
      end if;   
   end if; 

   if I_rec.action = action_mod                         
      and I_rec.PK_UDA_ITEM_DEFAULTS_rid is NOT NULL 
      and NVL(I_rec.uda_value,-1) <> NVL(I_rec.uda_value_upd,-1) then
      open C_DUP_UPD;
      fetch C_DUP_UPD into L_dup_check; 
      close C_DUP_UPD;
      if L_dup_check = 'X' then
        WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'UDA,UDA Value,Department,Class,Subclass',
                     'UDA_VALUE_EXISTS');
         O_error :=TRUE;
      end if; 
   end if;
   
   if I_rec.action = action_new then 
      open C_GET_DISPLAY_TYPE;
      fetch C_GET_DISPLAY_TYPE into L_display_type, L_single_value_ind;
      close C_GET_DISPLAY_TYPE;
      if NOT ((I_rec.required_ind = 'Y' 
         and I_rec.uda_value IS NULL)
         or(I_rec.required_ind = 'N' 
         and I_rec.uda_value IS NULL 
         and L_display_type IN ('FF','DT') )
         or(I_rec.required_ind = 'N' 
         and L_display_type IN ('LV') 
         and I_rec.uda_id IS NOT NULL)) then 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REQUIRED_IND',
                     'INV_IND_VAL');
         O_error :=TRUE;
      end if;
   end if;
   
   if I_rec.action = action_mod then 
      open C_GET_DISPLAY_TYPE;
      fetch C_GET_DISPLAY_TYPE into L_display_type, L_single_value_ind;
      close C_GET_DISPLAY_TYPE;
      if NOT ((I_rec.required_ind = 'Y' 
         and I_rec.uda_value_upd IS NULL)
         or(I_rec.required_ind = 'N' 
         and I_rec.uda_value_upd IS NULL 
         and L_display_type IN ('FF','DT'))
         or(I_rec.required_ind = 'N' 
         and L_display_type IN ('LV') 
         and I_rec.uda_id IS NOT NULL)) then 
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'REQUIRED_IND',
                     'INV_IND_DISP_UDA_VAL');
         O_error :=TRUE;
      end if;
   end if;   
   
   if I_rec.action = action_new 
      and I_rec.uda_id IS NOT NULL then  
      if UDA_SQL.GET_UDA_DESC(L_error_message,
                              L_uda_desc,
                              I_rec.uda_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     L_error_message);
      end if;
 
      if ((FILTER_LOV_VALIDATE_SQL.VALIDATE_UDA(L_error_message,
                                                L_valid,
                                                L_uda_row,
                                                L_uda_desc,
                                                I_rec.uda_id) = FALSE)
         or(L_valid = FALSE)) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'UDA_ID',
                     L_error_message);
         O_error :=TRUE;
      end if;
   end if; --if I_rec.action = action_new

   if I_rec.required_ind = 'Y' then                              --- UDA required set to YES 
      if UDA_SQL.CHECK_ITEMS_NO_UDA(L_error_message,             --- Checking for items in hierarchy level
                                    L_exists,                    --- without a uda value for this UDA
                                    I_rec.uda_id,
                                    I_rec.dept,
                                    I_rec.class,
                                    I_rec.subclass) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     L_error_message);
         O_error :=TRUE;
      end if;
      if L_exists then                                       --- Items exist without uda value, GET_USER not 
         WRITE_ERROR(I_rec.process_id,                       --- allowed to set required to yes
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'UDA_ID',
                     'NO_UDA_EXISTS');
         O_error :=TRUE;         
      end if;
      ---                                                --- Checking down the hierarchy for levels with 
      if I_rec.subclass is NULL then                     --- required set to NO for this UDA. 
         if UDA_SQL.CHECK_REQUIRED_HIER(L_error_message,
                                        L_not_required, 
                                        I_rec.uda_id,
                                        I_rec.dept,
                                        I_rec.class) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        NULL,
                        L_error_message);
            O_error :=TRUE;
         end if;
         if L_not_required then               --- Level(s) exist 
            if L_UDA_HIER_REQUIRED then       --- GET_USER prompted to continue by setting levels to YES
               if UDA_SQL.UPDATE_REQUIRED_HIER(L_error_message,  --- GET_USER selects OK, levels updated to YES 
                                               I_rec.uda_id,
                                               I_rec.dept,
                                               I_rec.class) = FALSE then
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              I_rec.row_seq,
                              'REQUIRED_IND',
                              L_error_message);
                  O_error :=TRUE;
               end if;
            end if;         
         end if;
      end if; 
   else                                                   --- Required set to NO 
      if (I_rec.action = action_new 
         or (I_rec.action = action_mod 
         and I_rec.required_ind <> I_rec.uid_required_ind))
         and L_single_value_ind = 'Y' then  --- only one uda value allowed
         if UDA_SQL.CHECK_SINGLE_UDA(L_error_message,
                                     L_exists,
                                     I_rec.uda_id,
                                     NULL,               --I_rec.pk_seq_no,
                                     NULL,
                                     I_rec.dept,
                                     I_rec.class,
                                     I_rec.subclass) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        NULL,
                        L_error_message);
            O_error :=TRUE;
         end if;
         if L_exists then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'REQUIRED_IND',
                        'SINGLE_UDA_VAL');
            O_error :=TRUE;
         end if; 
      end if;
                                    --- Must be at Class or lower to check up
      if I_rec.class is not NULL 
         and I_rec.uda_value is NULL then  
         if UDA_SQL.CHECK_REQUIRED_UDA(L_error_message,      --- Checking up the hierarchy for levels set to YES
                                       L_required, 
                                       I_rec.uda_id,
                                       NULL,
                                       I_rec.dept,
                                       I_rec.class,
                                       I_rec.subclass) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        NULL,
                        L_error_message);
            O_error :=TRUE;
         end if;    
         if L_required then                           --- Level(s) exist, USER must set to required YES 
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'REQUIRED_IND',
                        'UDA_REQUIRED_YES');
            O_error :=TRUE;            
         end if;
      end if;  
   end if;
   if I_rec.dept is NOT NULL then
      if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_DEPS(O_error_message,
                                                   L_valid,
                                                   L_dept_name,
                                                   I_rec.dept) 
      or (L_valid = FALSE) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DEPT',
                     O_error_message);
         O_error :=TRUE;
      end if;
 
      if I_rec.class is NOT NULL then
         if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS(O_error_message,
                                                       L_valid,
                                                       L_class,
                                                       I_rec.dept,
                                                       I_rec.class) 
         or (L_valid = FALSE) then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'CLASS',
                        O_error_message);
            O_error :=TRUE;
         end if;

         if I_rec.subclass is NOT NULL then
            if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_SUBCLASS(O_error_message,
                                                             L_valid,
                                                             L_subclass,
                                                             I_rec.dept,
                                                             I_rec.class,
                                                             I_rec.subclass) 
            or (L_valid = FALSE) then   
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'SUBCLASS',
                           O_error_message);
               O_error :=TRUE;
            end if;
         end if;     --if subclass not null
      end if;        --if class not null
   end if;           --if dept not null
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_DUP%ISOPEN then
         close C_DUP;
      end if;   
      if C_DUP_UPD%ISOPEN then
         close C_DUP_UPD;
      end if;   
      if C_GET_DISPLAY_TYPE%ISOPEN then
         close C_GET_DISPLAY_TYPE;
      end if;   
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
     return FALSE;
END PROCESS_UID_VAL;
----------------------------------------------------------------------------------
FUNCTION EXEC_UID_LOCK( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_UID_temp_rec    IN       UDA_ITEM_DEFAULTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_UDA.EXEC_UID_LOCK';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_ITEM_DEFAULTS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_UDA_ITEM_DEFAULTS_DEPT is
      select 'x'
        from uda_item_defaults
       where uda_id = I_UID_temp_rec.uda_id
         and dept   = I_UID_temp_rec.DEPT
         for update nowait;
         
   cursor C_LOCK_UDA_ITEM_DEFAULTS_CLASS is
      select 'x'
        from uda_item_defaults
       where uda_id = I_UID_temp_rec.uda_id
         and dept   = I_UID_temp_rec.DEPT
         and class  = I_UID_temp_rec.CLASS
         for update nowait;
         
   cursor C_LOCK_UDA_ITEM_DEFAULTS_SUB is
      select 'x'
        from uda_item_defaults
       where uda_id   = I_UID_temp_rec.uda_id
         and dept     = I_UID_temp_rec.DEPT
         and class    = I_UID_temp_rec.CLASS
         and subclass = I_UID_temp_rec.SUBCLASS
         for update nowait;         
BEGIN

   if I_UID_temp_rec.SUBCLASS is NOT NULL          ---subclass level
      and I_UID_temp_rec.CLASS is NOT NULL  
      and I_UID_temp_rec.DEPT is NOT NULL then
      L_table := 'UDA_ITEM_DEFAULTS';
      open C_LOCK_UDA_ITEM_DEFAULTS_SUB;
      close C_LOCK_UDA_ITEM_DEFAULTS_SUB;
   
   elsif I_UID_temp_rec.SUBCLASS is NULL           ---class level
      and I_UID_temp_rec.CLASS is NOT NULL  
      and I_UID_temp_rec.DEPT is NOT NULL then
      L_table := 'UDA_ITEM_DEFAULTS';
      open C_LOCK_UDA_ITEM_DEFAULTS_CLASS;
      close C_LOCK_UDA_ITEM_DEFAULTS_CLASS;

   elsif I_UID_temp_rec.SUBCLASS is NULL           ---dept level  
      and I_UID_temp_rec.CLASS is NULL  
      and I_UID_temp_rec.DEPT is NOT NULL then
      L_table := 'UDA_ITEM_DEFAULTS';
      open C_LOCK_UDA_ITEM_DEFAULTS_DEPT;
      close C_LOCK_UDA_ITEM_DEFAULTS_DEPT;
   end if;        
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_UID_temp_rec.uda_id,
                                             I_UID_temp_rec.DEPT);
      return FALSE;
   when OTHERS then
      if C_LOCK_UDA_ITEM_DEFAULTS_DEPT%ISOPEN then
         close C_LOCK_UDA_ITEM_DEFAULTS_DEPT;
      end if;         
      if C_LOCK_UDA_ITEM_DEFAULTS_CLASS%ISOPEN then
         close C_LOCK_UDA_ITEM_DEFAULTS_CLASS;
      end if;
      if C_LOCK_UDA_ITEM_DEFAULTS_SUB%ISOPEN then
         close C_LOCK_UDA_ITEM_DEFAULTS_SUB;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UID_LOCK;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_VALUES_INS( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_uda_values_temp_rec   IN       UDA_VALUES%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_UDA.EXEC_UDA_VALUES_INS';

BEGIN
   insert into uda_values
        values I_uda_values_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UDA_VALUES_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_VALUES_UPD( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_uda_values_temp_rec   IN       UDA_VALUES%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_UDA.EXEC_UDA_VALUES_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_VALUES';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_UDA_VAL_UPD is
      select 'x'
        from uda_values
       where uda_value = I_uda_values_temp_rec.uda_value
         and uda_id = I_uda_values_temp_rec.uda_id
         for update nowait;

BEGIN
   open  C_LOCK_UDA_VAL_UPD;
   close C_LOCK_UDA_VAL_UPD;

   update uda_values
      set row = I_uda_values_temp_rec
    where 1 = 1
      and uda_value = I_uda_values_temp_rec.uda_value
      and uda_id = I_uda_values_temp_rec.uda_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_uda_values_temp_rec.uda_id,
                                             I_uda_values_temp_rec.uda_value);
      return FALSE;
   when OTHERS then
      if C_LOCK_UDA_VAL_UPD%ISOPEN then
         close C_LOCK_UDA_VAL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UDA_VALUES_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_VALUES_PRE_DEL( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rec                   IN       C_SVC_UDA%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_UDA.EXEC_UDA_VALUES_PRE_DEL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_ITEM_DEFAULTS, SKULIST_CRITERIA';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_UDA_ITEM is
      select 'x'
        from uda_item_defaults
       where uda_id = I_rec.udv_uda_id
        and uda_value = I_rec.udv_uda_value
         for update nowait;

   cursor C_LOCK_SKULIST is
      select 'x'
        from skulist_criteria
       where uda_id = I_rec.udv_uda_id
         and uda_value_lov = I_rec.udv_uda_value
         for update nowait;

BEGIN
   open  C_LOCK_UDA_ITEM;
   close C_LOCK_UDA_ITEM;
   open  C_LOCK_SKULIST;
   close C_LOCK_SKULIST;

   delete from uda_item_defaults
      where uda_id = I_rec.udv_uda_id
        and uda_value = I_rec.udv_uda_value;

   delete from skulist_criteria
      where uda_id = I_rec.udv_uda_id
        and uda_value_lov = I_rec.udv_uda_value;
   return TRUE;

EXCEPTION
  when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_rec.udv_uda_id,
                                             I_rec.udv_uda_value);
      return FALSE;
   when OTHERS then
      if C_LOCK_UDA_ITEM%ISOPEN then
         close C_LOCK_UDA_ITEM;
      end if;
      if C_LOCK_SKULIST%ISOPEN then
         close C_LOCK_SKULIST;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
      ROLLBACK TO do_process;
END EXEC_UDA_VALUES_PRE_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_VALUES_DEL( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_udv_process_error     IN OUT   BOOLEAN,
                              I_rec                   IN       C_SVC_UDA%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_UDA.EXEC_UDA_VALUES_DEL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_VALUES';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_UDA_VAL_TL_DEL is
      select 'x'
        from uda_values_tl
       where uda_value = I_rec.udv_uda_value
         and uda_id = I_rec.udv_uda_id
         for update nowait;

   cursor C_LOCK_UDA_VAL_DEL is
      select 'x'
        from uda_values
       where uda_value = I_rec.udv_uda_value
         and uda_id = I_rec.udv_uda_id
         for update nowait;

BEGIN
   if EXEC_UDA_VALUES_PRE_DEL( O_error_message,
                               I_rec) = FALSE then
      WRITE_ERROR(I_rec.udv_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.udv_chunk_id,
                  L_table,
                  I_rec.udv_row_seq,
                  NULL,
                  O_error_message);
      O_udv_process_error := TRUE;
   else
      open  C_LOCK_UDA_VAL_TL_DEL;
      close C_LOCK_UDA_VAL_TL_DEL;

      delete from uda_values_tl
       where uda_value = I_rec.udv_uda_value
         and uda_id = I_rec.udv_uda_id;

      open  C_LOCK_UDA_VAL_DEL;
      close C_LOCK_UDA_VAL_DEL;

      delete from uda_values
       where uda_value = I_rec.udv_uda_value
         and uda_id = I_rec.udv_uda_id;

   end if;
   return TRUE;

EXCEPTION
 when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_rec.udv_uda_id,
                                             I_rec.udv_uda_value);
      return FALSE;
   when OTHERS then
      if C_LOCK_UDA_VAL_TL_DEL%ISOPEN then
         close C_LOCK_UDA_VAL_TL_DEL;
      end if;

      if C_LOCK_UDA_VAL_DEL%ISOPEN then
         close C_LOCK_UDA_VAL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UDA_VALUES_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_VALUES_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_uda_values_tl_ins_tab    IN       UDA_VALUES_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_UDA.EXEC_UDA_VALUES_TL_INS';

BEGIN
   if I_uda_values_tl_ins_tab is NOT NULL and I_uda_values_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_uda_values_tl_ins_tab.COUNT()
         insert into uda_values_tl
              values I_uda_values_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_UDA_VALUES_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_VALUES_TL_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_uda_values_tl_upd_tab   IN       UDA_VALUES_TAB,
                                I_uda_values_tl_upd_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_UDA_VALUES_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_UDA_VALUES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_UDA.EXEC_UDA_VALUES_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_VALUES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_UDA_VALUES_TL_UPD(I_uda_value   UDA_VALUES_TL.UDA_VALUE%TYPE,
                                   I_uda_id      UDA_VALUES_TL.UDA_ID%TYPE,
                                   I_lang        UDA_VALUES_TL.LANG%TYPE) is
      select 'x'
        from uda_values_tl
       where uda_id = I_uda_id
         and uda_value = I_uda_value
         and lang = I_lang
         for update nowait;

BEGIN
   if I_uda_values_tl_upd_tab is NOT NULL and I_uda_values_tl_upd_tab.count > 0 then
      for i in I_uda_values_tl_upd_tab.FIRST..I_uda_values_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_uda_values_tl_upd_tab(i).lang);
            L_key_val2 := 'UDA ID '||to_char(I_uda_values_tl_upd_tab(i).uda_id);
            open C_LOCK_UDA_VALUES_TL_UPD(I_uda_values_tl_upd_tab(i).uda_value,
                                          I_uda_values_tl_upd_tab(i).uda_id,
                                          I_uda_values_tl_upd_tab(i).lang);
            close C_LOCK_UDA_VALUES_TL_UPD;
            
            update uda_values_tl
               set uda_value_desc = I_uda_values_tl_upd_tab(i).uda_value_desc,
                   last_update_id = I_uda_values_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_uda_values_tl_upd_tab(i).last_update_datetime
             where lang = I_uda_values_tl_upd_tab(i).lang
               and uda_id = I_uda_values_tl_upd_tab(i).uda_id
               and uda_value = I_uda_values_tl_upd_tab(i).uda_value;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_UDA_VALUES_TL',
                           I_uda_values_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_UDA_VALUES_TL_UPD%ISOPEN then
         close C_LOCK_UDA_VALUES_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_UDA_VALUES_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_VALUES_TL_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_uda_values_tl_del_tab   IN       uda_values_tab,
                                I_uda_values_tl_del_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_UDA_VALUES_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_UDA_VALUES_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_UDA.EXEC_UDA_VALUES_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_VALUES_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_UDA_VALUES_TL_DEL(I_uda_value   UDA_VALUES_TL.UDA_VALUE%TYPE,
                                   I_uda_id      UDA_VALUES_TL.UDA_ID%TYPE,
                                   I_lang        UDA_VALUES_TL.LANG%TYPE) is
      select 'x'
        from uda_values_tl
       where uda_id = I_uda_id
         and uda_value = I_uda_value
         and lang = I_lang
         for update nowait;

BEGIN
   if I_uda_values_tl_del_tab is NOT NULL and I_uda_values_tl_del_tab.count > 0 then
      for i in I_uda_values_tl_del_tab.FIRST..I_uda_values_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_uda_values_tl_del_tab(i).lang);
            L_key_val2 := 'UDA ID '||to_char(I_uda_values_tl_del_tab(i).uda_id);
            open C_LOCK_UDA_VALUES_TL_DEL(I_uda_values_tl_del_tab(i).uda_value,
                                          I_uda_values_tl_del_tab(i).uda_id,
                                          I_uda_values_tl_del_tab(i).lang);
            close C_LOCK_UDA_VALUES_TL_DEL;
            
            delete uda_values_tl
             where lang = I_uda_values_tl_del_tab(i).lang
               and uda_id = I_uda_values_tl_del_tab(i).uda_id
               and uda_value = I_uda_values_tl_del_tab(i).uda_value;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_UDA_VALUES_TL',
                           I_uda_values_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_UDA_VALUES_TL_DEL%ISOPEN then
         close C_LOCK_UDA_VALUES_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_UDA_VALUES_TL_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_UDA_VAL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error         IN OUT BOOLEAN,
                         I_rec           IN     C_SVC_UDA%ROWTYPE)

RETURN BOOLEAN IS

   L_program        VARCHAR2(64)                      := 'CORESVC_UDA.PROCESS_UDA_VAL';
   L_table_uda      SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA';
   L_valid          BOOLEAN;
   L_class          CLASS.CLASS_NAME%TYPE;
   L_subclass       SUBCLASS.SUB_NAME%TYPE;
   L_merch_desc     VARCHAR2(600);
   L_org_desc       VARCHAR2(600);
   L_sys_opt_row    SYSTEM_OPTIONS%ROWTYPE;
   L_merch_level    SYSTEM_OPTIONS.uda_merch_level_code%TYPE;
   L_org_level      SYSTEM_OPTIONS.uda_org_level_code%TYPE;
   L_exists         BOOLEAN;
   L_merchid        VARCHAR2(1);

BEGIN
   if NOT SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                                L_sys_opt_row) then
      WRITE_ERROR(I_rec.uda_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.uda_chunk_id,
                  L_table_uda,
                  I_rec.uda_row_seq,
                  NULL,
                  O_error_message);
      O_error :=TRUE;
   else
      L_merch_level := L_sys_opt_row.uda_merch_leveL_code;
      L_org_level := L_sys_opt_row.uda_org_leveL_code;
   end if;

   if L_merch_level = 'C'
      and NOT((I_rec.uda_filter_merch_id is NOT NULL
      and I_rec.uda_filter_merch_id_class is NOT NULL)
      or (I_rec.uda_filter_merch_id is NULL
      and I_rec.uda_filter_merch_id_class is NULL))then
      WRITE_ERROR(I_rec.uda_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.uda_chunk_id,
                  L_table_uda,
                  I_rec.uda_row_seq,
                  'Filter Merch ID,Filter Merch ID Class',
                  'INV_DEPT_CLASS');
      O_error :=TRUE;
   end if;

   if L_merch_level = 'S'
      and NOT((I_rec.uda_filter_merch_id is NOT NULL
      and I_rec.uda_filter_merch_id_class is NOT NULL
      and I_rec.uda_filter_merch_id_subclass is NOT NULL)
      or (I_rec.uda_filter_merch_id is NULL
      and I_rec.uda_filter_merch_id_class is NULL
      and I_rec.uda_filter_merch_id_subclass is NULL))then
      WRITE_ERROR(I_rec.uda_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.uda_chunk_id,
                  L_table_uda,
                  I_rec.uda_row_seq,
                  'Filter Merch ID,Filter Merch ID Class,Filter Merch ID Subclass',
                  'INV_DEPT_CLASS_SUB');
      O_error :=TRUE;
   end if;

   if I_rec.uda_uda_desc is NOT NULL then
      if ITEM_ATTRIB_SQL.CHECK_DESC(O_error_message,
                                    I_rec.uda_uda_desc,
                                    L_exists) = FALSE then
         WRITE_ERROR(I_rec.uda_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.uda_chunk_id,
                     L_table_uda,
                     I_rec.uda_row_seq,
                     NULL,
                     O_error_message);
         O_error :=TRUE;
      elsif L_exists = TRUE then
         WRITE_ERROR(I_rec.uda_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.uda_chunk_id,
                     L_table_uda,
                     I_rec.uda_row_seq,
                     'UDA_DESC',
                     'NO_SPL_CHR');
         O_error :=TRUE;
      end if;
   end if;

   L_merchid := L_merch_level;
   if L_merch_level in ('C','S') then
      L_merchid := 'P';
   end if;

   if I_rec.uda_filter_merch_id is NOT NULL then
      if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_MERCH_LEVEL(O_error_message,
                                                          L_valid,
                                                          L_merch_desc,
                                                          L_merchid,
                                                          I_rec.uda_filter_merch_id,
                                                          I_rec.uda_filter_merch_id_class,
                                                          I_rec.uda_filter_merch_id_subclass)
         or (L_valid = FALSE) then
         WRITE_ERROR(I_rec.uda_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.uda_chunk_id,
                     L_table_uda,
                     I_rec.uda_row_seq,
                     'Filter Merch ID,Filter Merch ID Class,Filter Merch ID Subclass',
                     O_error_message);
         O_error :=TRUE;
      end if;
   end if;
   if I_rec.uda_filter_org_id is NOT NULL then
      if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_ORG_LEVEL(O_error_message,
                                                        L_valid,
                                                        L_org_desc,
                                                        L_org_level,
                                                        I_rec.uda_filter_org_id)
         or (L_valid = FALSE)   then
         WRITE_ERROR(I_rec.uda_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.uda_chunk_id,
                     L_table_uda,
                     I_rec.uda_row_seq,
                     'FILTER_ORG_ID',
                     O_error_message);
         O_error :=TRUE;
      end if;
   end if;
   if I_rec.uda_filter_merch_id_class is NOT NULL then
      if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS(O_error_message,
                                                L_valid,
                                                L_class,
                                                I_rec.uda_filter_merch_id,
                                                I_rec.uda_filter_merch_id_class)
      or (L_valid = FALSE) then
         WRITE_ERROR(I_rec.uda_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.uda_chunk_id,
                     L_table_uda,
                     I_rec.uda_row_seq,
                     'FILTER_MERCH_ID_CLASS',
                     O_error_message);
         O_error :=TRUE;
      end if;
   end if;

   if I_rec.uda_filter_merch_id_subclass is NOT NULL then
      if NOT FILTER_LOV_VALIDATE_SQL.VALIDATE_SUBCLASS(O_error_message,
                                                   L_valid,
                                                   L_subclass,
                                                   I_rec.uda_filter_merch_id,
                                                   I_rec.uda_filter_merch_id_class,
                                                   I_rec.uda_filter_merch_id_subclass)
      or (L_valid = FALSE) then
         WRITE_ERROR(I_rec.uda_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.uda_chunk_id,
                     L_table_uda,
                     I_rec.uda_row_seq,
                     'FILTER_MERCH_ID_SUBCLASS',
                     O_error_message);
         O_error :=TRUE;
      end if;
   end if;

   -- validate data length is between 1 and 250
   if I_rec.uda_data_length < 1 or I_rec.uda_data_length > 250  then
      WRITE_ERROR(I_rec.uda_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.uda_chunk_id,
                     L_table_uda,
                     I_rec.uda_row_seq,
                     'DATA_LENGTH',
                     'VALUE_BETWEEN_1_250');
         O_error :=TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UDA_VAL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_INS( O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_uda_temp_rec   IN       UDA%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_UDA.EXEC_UDA_INS';
   L_table_uda SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA';

BEGIN
   insert into uda
        values I_uda_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UDA_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_UPD( O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_uda_temp_rec   IN   UDA%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_UDA.EXEC_UDA_UPD';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_UDA_UPD is
      select 'x'
        from uda
       where uda_id = I_uda_temp_rec.uda_id
         for update nowait;

BEGIN
   open  C_LOCK_UDA_UPD;
   close C_LOCK_UDA_UPD;

   update uda
      set row = I_uda_temp_rec
    where 1 = 1
      and uda_id = I_uda_temp_rec.uda_id;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_uda_temp_rec.uda_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_UDA_UPD%ISOPEN then
         close C_LOCK_UDA_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UDA_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_PRE_DEL( O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_uda_temp_rec   IN       UDA%ROWTYPE )
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                      := 'CORESVC_UDA.EXEC_UDA_PRE_DEL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_ITEM_DEFAULTS,SKULIST_CRITERIA,UDA_VALUES';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_UDA_ITEM_PD is
      select 'x'
        from uda_item_defaults
       where uda_id = I_uda_temp_rec.uda_id
         for update nowait;

   cursor C_LOCK_SKULIST_PD is
      select 'x'
        from skulist_criteria
       where uda_id = I_uda_temp_rec.uda_id
         for update nowait;

   cursor C_LOCK_UDA_VAL_PD is
      select 'x'
        from uda_values
       where uda_id = I_uda_temp_rec.uda_id
         for update nowait;

BEGIN
   open  C_LOCK_UDA_ITEM_PD;
   close C_LOCK_UDA_ITEM_PD;
   open  C_LOCK_SKULIST_PD;
   close C_LOCK_SKULIST_PD;
   open  C_LOCK_UDA_VAL_PD;
   close C_LOCK_UDA_VAL_PD;

   delete from uda_item_defaults
      where uda_id = I_uda_temp_rec.uda_id;

   delete from skulist_criteria
      where uda_id = I_uda_temp_rec.uda_id;
      
   delete from uda_values_tl
      where uda_id = I_uda_temp_rec.uda_id;

   delete from uda_values
      where uda_id = I_uda_temp_rec.uda_id;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_uda_temp_rec.uda_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_UDA_ITEM_PD%ISOPEN then
         close C_LOCK_UDA_ITEM_PD;
      end if;
      if C_LOCK_SKULIST_PD%ISOPEN then
         close C_LOCK_SKULIST_PD;
      end if;
      if C_LOCK_UDA_VAL_PD%ISOPEN then
         close C_LOCK_UDA_VAL_PD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
      ROLLBACK TO do_process;
END EXEC_UDA_PRE_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_DEL( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_process_error         IN OUT   BOOLEAN,
                       I_rec                   IN       C_SVC_UDA%ROWTYPE,
                       I_uda_temp_rec          IN       UDA%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_UDA.EXEC_UDA_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA';
   L_table_udv     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA_VALUES';
   L_exists        BOOLEAN := FALSE;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   --Cursor to lock the record
   cursor C_LOCK_UDA_DEL is
      select 'x'
        from uda
       where uda_id = I_uda_temp_rec.uda_id
         for update nowait;

   cursor C_LOCK_UDA_TL_DEL is
      select 'x'
        from uda_tl
       where uda_id = I_uda_temp_rec.uda_id
         for update nowait;

BEGIN
   if I_rec.uda_display_type = 'LV' then
      if UDA_SQL.RELATION_EXISTS( O_error_message,
                                  L_exists,
                                  I_rec.udv_uda_id,
                                  I_rec.udv_uda_value) = FALSE then
         WRITE_ERROR(I_rec.udv_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.udv_chunk_id,
                     L_table_udv,
                     I_rec.udv_row_seq,
                     NULL,
                     O_error_message);
         O_process_error :=TRUE;
      end if;
      if L_exists then
         WRITE_ERROR(I_rec.udv_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.udv_chunk_id,
                     L_table_udv,
                     I_rec.udv_row_seq,
                     'UDA_VALUE',
                     'UDA_VAL_REL_EXISTS');
         O_process_error :=TRUE;
      else
         if EXEC_UDA_VALUES_DEL( O_error_message,
                                 O_process_error,
                                 I_rec) = FALSE then
            WRITE_ERROR(I_rec.udv_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.udv_chunk_id,
                        L_table_udv,
                        I_rec.udv_row_seq,
                        NULL,
                        O_error_message);
            O_process_error :=TRUE;
         end if;
      end if;
   end if;
   if NOT O_process_error then
      if EXEC_UDA_PRE_DEL( O_error_message,
                           I_uda_temp_rec) = FALSE then
         WRITE_ERROR(I_rec.uda_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.uda_chunk_id,
                     L_table,
                     I_rec.uda_row_seq,
                     NULL,
                     O_error_message);
         O_process_error := TRUE;
      else
         open C_LOCK_UDA_TL_DEL;
         close C_LOCK_UDA_TL_DEL;

         delete from uda_tl
            where uda_id = I_uda_temp_rec.uda_id;

         open C_LOCK_UDA_DEL;
         close C_LOCK_UDA_DEL;

         delete from uda
            where uda_id = I_uda_temp_rec.uda_id;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             L_table,
                                             I_uda_temp_rec.uda_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_UDA_TL_DEL%ISOPEN then
         close C_LOCK_UDA_TL_DEL;
      end if;
      if C_LOCK_UDA_DEL%ISOPEN then
         close C_LOCK_UDA_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UDA_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_TL_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_uda_tl_ins_tab    IN       UDA_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_UDA.EXEC_UDA_TL_INS';

BEGIN
   if I_uda_tl_ins_tab is NOT NULL and I_uda_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_uda_tl_ins_tab.COUNT()
         insert into uda_tl
              values I_uda_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_UDA_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_TL_UPD(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_uda_tl_upd_tab   IN       UDA_TAB,
                         I_uda_tl_upd_rst   IN       ROW_SEQ_TAB,
                         I_process_id       IN       SVC_UDA_TL.PROCESS_ID%TYPE,
                         I_chunk_id         IN       SVC_UDA_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_UDA.EXEC_UDA_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_UDA_TL_UPD(I_uda_id  UDA_TL.UDA_ID%TYPE,
                            I_lang    UDA_TL.LANG%TYPE) is
      select 'x'
        from uda_tl
       where uda_id = I_uda_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_uda_tl_upd_tab is NOT NULL and I_uda_tl_upd_tab.count > 0 then
      for i in I_uda_tl_upd_tab.FIRST..I_uda_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_uda_tl_upd_tab(i).lang);
            L_key_val2 := 'UDA ID: '||to_char(I_uda_tl_upd_tab(i).uda_id);
            open C_LOCK_UDA_TL_UPD(I_uda_tl_upd_tab(i).uda_id,
                                     I_uda_tl_upd_tab(i).lang);
            close C_LOCK_UDA_TL_UPD;
            
            update uda_tl
               set uda_desc = I_uda_tl_upd_tab(i).uda_desc,
                   last_update_id = I_uda_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_uda_tl_upd_tab(i).last_update_datetime
             where lang = I_uda_tl_upd_tab(i).lang
               and uda_id = I_uda_tl_upd_tab(i).uda_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_UDA_TL',
                           I_uda_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_UDA_TL_UPD%ISOPEN then
         close C_LOCK_UDA_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_UDA_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_UDA_TL_DEL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_uda_tl_del_tab   IN       UDA_TAB,
                         I_uda_tl_del_rst   IN       ROW_SEQ_TAB,
                         I_process_id       IN       SVC_UDA_TL.PROCESS_ID%TYPE,
                         I_chunk_id         IN       SVC_UDA_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_UDA.EXEC_UDA_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_UDA_TL_DEL(I_uda_id  UDA_TL.UDA_ID%TYPE,
                            I_lang    UDA_TL.LANG%TYPE) is
      select 'x'
        from uda_tl
       where uda_id = I_uda_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_uda_tl_del_tab is NOT NULL and I_uda_tl_del_tab.count > 0 then
      for i in I_uda_tl_del_tab.FIRST..I_uda_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_uda_tl_del_tab(i).lang);
            L_key_val2 := 'UDA ID: '||to_char(I_uda_tl_del_tab(i).uda_id);
            open C_LOCK_UDA_TL_DEL(I_uda_tl_del_tab(i).uda_id,
                                     I_uda_tl_del_tab(i).lang);
            close C_LOCK_UDA_TL_DEL;
            
            delete uda_tl
             where lang = I_uda_tl_del_tab(i).lang
               and uda_id = I_uda_tl_del_tab(i).uda_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_UDA_TL',
                           I_uda_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_UDA_TL_DEL%ISOPEN then
         close C_LOCK_UDA_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_UDA_TL_DEL;
----------------------------------------------------------------------------------
FUNCTION EXEC_UID_INS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rec             IN       C_SVC_UID%ROWTYPE,
                       I_UID_temp_rec    IN       UDA_ITEM_DEFAULTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_UDA.EXEC_UID_INS';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'svc_uda_item_defaults';
BEGIN
   if EXEC_UID_LOCK( O_error_message,
                     I_UID_temp_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
   else                   
      insert into uda_item_defaults
           values I_UID_temp_rec;
   end if; 
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UID_INS;
----------------------------------------------------------------------------------
FUNCTION EXEC_UID_UPD( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rec             IN       C_SVC_UID%ROWTYPE,
                       I_UID_temp_rec    IN       UDA_ITEM_DEFAULTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_UDA.EXEC_UID_UPD';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA_ITEM_DEFAULTS';
   
BEGIN
   if EXEC_UID_LOCK( O_error_message,
                     I_UID_temp_rec)=FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
   else  
      update uda_item_defaults
         set row = I_UID_temp_rec
       where 1 = 1
         and uda_id     = I_UID_temp_rec.uda_id
         and dept       = I_UID_temp_rec.dept
         and ((class    = I_UID_temp_rec.class) or (class IS NULL and I_UID_temp_rec.class IS NULL))
         and ((subclass = I_UID_temp_rec.subclass) or (subclass IS NULL and I_UID_temp_rec.subclass IS NULL)) 
         and ((uda_value  = I_rec.uda_value) or (uda_value IS NULL and I_rec.uda_value IS NULL));
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UID_UPD;
----------------------------------------------------------------------------------
FUNCTION EXEC_UID_DEL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_rec             IN       C_SVC_UID%ROWTYPE,
                       I_UID_temp_rec    IN       UDA_ITEM_DEFAULTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_UDA.EXEC_UID_DEL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA_ITEM_DEFAULTS';
   
BEGIN
   if EXEC_UID_LOCK( O_error_message,
                     I_UID_temp_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
   else  
      delete from uda_item_defaults
       where 1 = 1
         and uda_id     = I_UID_temp_rec.uda_id
         and dept       = I_UID_temp_rec.dept
         and ((class    = I_UID_temp_rec.class) or (class IS NULL and I_UID_temp_rec.class IS NULL))
         and ((subclass = I_UID_temp_rec.subclass) or (subclass IS NULL and I_UID_temp_rec.subclass IS NULL)) 
         and ((uda_value  = I_UID_temp_rec.uda_value) or (uda_value IS NULL and I_UID_temp_rec.uda_value IS NULL));
   end if;
   return TRUE;
      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_UID_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_UDA( O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id    IN       SVC_UDA.PROCESS_ID%TYPE,
                      I_chunk_id      IN       SVC_UDA.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program              VARCHAR2(64)                      := 'CORESVC_UDA.PROCESS_UDA';
   L_table_uda            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA';
   L_table_udv            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA_VALUES';
   L_uda_process_error    BOOLEAN                           := FALSE;
   L_udv_process_error    BOOLEAN                           := FALSE;
   L_exists               BOOLEAN                           := FALSE;
   L_uda_exists           BOOLEAN                           := FALSE;
   L_udv_error            BOOLEAN ;
   L_count                NUMBER;
   L_error                BOOLEAN                           := FALSE;
   L_uda_error_message    VARCHAR2(600);
   L_uda_temp_rec         UDA%ROWTYPE;
   L_uda_values_temp_rec  UDA_VALUES%ROWTYPE;
   L_data_type            UDA.DATA_TYPE%TYPE := NULL;
   L_data_length          UDA.DATA_LENGTH%TYPE := NULL;
   L_disp                 UDA.DISPLAY_TYPE%TYPE := NULL;
   L_uda_next_seq         UDA.UDA_ID%TYPE;
   L_found                VARCHAR2(1) := NULL;
   L_new_uda_value        UDA_VALUES.UDA_VALUE%TYPE := NULL;
   L_rowseq_count               NUMBER;
   c                            NUMBER      := 1;


   TYPE L_row_seq_tab_type IS TABLE OF SVC_UDA_VALUES.ROW_SEQ%TYPE;
   L_row_seq_tab                       L_row_seq_tab_type;

   cursor C_UDV_COUNT(I_uda UDA_VALUES.UDA_ID%TYPE) is
      select count(UDA_VALUES.UDA_ID) into L_count
        from UDA_VALUES
       where UDA_ID = I_uda;

   cursor C_DISP_TYPE (I_uda UDA.UDA_ID%TYPE) is
      select display_type, data_type, data_length
        from UDA
       where uda_id = I_uda;

BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   FOR rec IN c_svc_UDA(I_process_id,
                        I_chunk_id)
   LOOP
      L_udv_error := FALSE;
      if rec.uda_rank = 1 then
         L_error := FALSE;
         L_uda_next_seq := NULL;
         L_row_seq_tab.DELETE;
         c:=1;
         if rec.uda_action = action_new then
            if rec.uda_data_type is NULL then
               if rec.uda_display_type IN ('LV','FF')then
                  rec.uda_data_type := 'ALPHA';
               elsif rec.uda_display_type = 'DT' then
                  rec.uda_data_type := 'DATE';
               end if;
            end if;
         end if;

--UDA VALIDATIONS START
         if rec.uda_action NOT IN (action_new,action_mod,action_del) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_uda,
                        rec.uda_row_seq,
                        'ACTION',
                        'INV_ACT');
            L_error :=TRUE;
         end if;

         if rec.uda_action IS NULL
            and rec.st_uda_rid IS NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_uda,
                        rec.uda_row_seq,
                        'ACTION',
                        'MUST_ENTER_FIELD');
            L_error :=TRUE;
         end if;

         if rec.uda_action IN (action_mod,action_del)
            and rec.PK_UDA_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_uda,
                        rec.uda_row_seq,
                        'UDA_ID',
                        'NO_RECORD');
            L_error :=TRUE;
         end if;

         if rec.uda_action = action_new then
            if UPPER(rec.uda_SINGLE_VALUE_IND) NOT IN ('Y','N')
            or rec.uda_SINGLE_VALUE_IND is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'SINGLE_VALUE_IND',
                           'INV_Y_N_IND');
               L_error :=TRUE;
            end if;

            if rec.uda_DATA_TYPE NOT IN ('NUM','ALPHA','DATE' ) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'DATA_TYPE',
                           'INV_DATA_TYPE');
               L_error :=TRUE;
            end if;

            if rec.uda_DISPLAY_TYPE NOT IN ('LV','FF','DT') then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'DISPLAY_TYPE',
                           'INV_DISP_TYPE');
               L_error :=TRUE;
            end if;
         end if; --if rec.uda_action = action_new

         if rec.uda_action IN (action_new,action_mod) then
            if rec.uda_UDA_DESC IS NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'UDA_DESC',
                           'UDA_DESC_MUST_BE_ENTERED');
               L_error :=TRUE;
            end if;

            if rec.uda_DISPLAY_TYPE IS NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'DISPLAY_TYPE',
                           'DISP_TYPE_MUST_BE_ENTERED');
               L_error :=TRUE;
            end if;
         end if;

         if rec.uda_action = action_mod
            and rec.uda_DATA_TYPE IS NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'DATA_TYPE',
                           'FIELD_NOT_NULL');
               L_error :=TRUE;
         end if;

         if rec.uda_action = action_mod
            and rec.uda_SINGLE_VALUE_IND IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_uda,
                        rec.uda_row_seq,
                        'SINGLE_VALUE_IND',
                        'FIELD_NOT_NULL');
            L_error :=TRUE;
         end if;

         if rec.uda_action = action_mod
            and rec.PK_UDA_rid is NOT NULL
            and (NVL(rec.pk_uda_single_value_ind,-1) <> NVL(rec.uda_single_value_ind,-1)
                or NVL(rec.pk_uda_data_length,-1)    <> NVL(rec.uda_data_length,-1)
                or NVL(rec.pk_uda_data_type,-1)      <> NVL(rec.uda_data_type,-1)
                or NVL(rec.pk_uda_display_type,-1)   <> NVL(rec.uda_display_type,-1)) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_uda,
                        rec.uda_row_seq,
                        'Display Type,Data Type,Data Length,Single Value?',
                        'FIELD_NO_MOD');
            L_error :=TRUE;
         end if;

         if rec.uda_action IN (action_new,action_mod) then
            if PROCESS_UDA_VAL(O_error_message,
                               L_error,
                               rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           NULL,
                           O_error_message);
               L_error := TRUE;
            end if;
         end if;

         if NOT L_error
            and rec.uda_action = action_new
            and rec.uda_rank   = 1
            and L_uda_next_seq is NULL then
            if UDA_SQL.NEXT_UDA(O_error_message,
                                L_uda_next_seq) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'UDA_ID',
                           O_error_message);
               L_error :=TRUE;
            else
               --sync the uda_id of the records in svc_uda_tl
               update svc_uda_tl
                  set uda_id = L_uda_next_seq
                where uda_id = rec.uda_uda_id
                  and action = action_new
                  and process_id = I_process_id
                  and chunk_id = I_chunk_id;

               --sync the uda_id of the records in svc_uda_values_tl
               update svc_uda_values_tl
                  set uda_id = L_uda_next_seq
                where uda_id = rec.uda_uda_id
                  and action = action_new
                  and process_id = I_process_id
                  and chunk_id = I_chunk_id;
                  
               --sync the uda_id in svc_uda_item_defaults
               update svc_uda_item_defaults
                  set uda_id = L_uda_next_seq
                where uda_id = rec.uda_uda_id
                  and action = action_new
                  and process_id = I_process_id
                  and chunk_id = I_chunk_id;

               rec.uda_uda_id:=L_uda_next_seq;
               rec.udv_uda_id:=L_uda_next_seq;

            end if;
         end if;
      end if; -- if rank=1
      if NOT L_error
         and rec.uda_action = action_new
         and rec.udv_action = action_new
         and rec.uda_rank>1
         and NVL(rec.uda_uda_id,-1)= NVL(rec.udv_uda_id,-1)
         and L_uda_next_seq is NOT NULL then
             rec.udv_uda_id:=L_uda_next_seq;
             rec.uda_uda_id:=L_uda_next_seq;
      end if;
--UDA VALIDATIONS END
-------------------------------------------------------

--UDV VALIDATIONS START
       if rec.udv_action IS NULL
         and rec.st_udv_rid IS NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table_udv,
                     rec.udv_row_seq,
                     'ACTION',
                     'INV_ACT');
         L_udv_error :=TRUE;
      end if;
      L_disp := NULL;
      L_data_type := NULL;
      L_data_length := NULL;

      if rec.st_udv_rid IS NOT NULL then
         if (NOT(rec.uda_action = action_new
            and rec.udv_action = action_new)
         or rec.uda_action IS NULL)
            and rec.udv_action IS NOT NULL then
            OPEN C_DISP_TYPE (rec.udv_uda_id);
            FETCH C_DISP_TYPE into L_disp, L_data_type, L_data_length;
            CLOSE C_DISP_TYPE;
         end if;
         if rec.uda_display_type  =  'LV'
               and rec.uda_data_type IN ('NUM')
               and rec.udv_uda_value is NOT NULL
               and rec.udv_uda_value_desc is NOT NULL
               and rec.flag = 1 then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_udv,
                           rec.udv_row_seq,
                           'UDA_VALUE_DESC',
                           'NON_NUMERIC');
               L_udv_error :=TRUE;
         end if;

         if rec.uda_action = action_new
            and rec.udv_action = action_new then
            rec.udv_uda_id := rec.uda_uda_id;
         end if;

         if rec.udv_action NOT IN (action_new,action_mod,action_del) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_udv,
                        rec.udv_row_seq,
                        'ACTION',
                        'INV_ACT');
            L_udv_error :=TRUE;
         end if;

         if (rec.uda_action <> action_new               -- Because for new-new, the uda_id-uda_value combination
            or rec.uda_action IS NULL)                  -- might already be present in the UDA_VALUES table,
            and rec.udv_action = action_new             -- but we replace the uda_id with the auto generated uda_id
            and rec.PK_UDA_VALUES_rid is NOT NULL then  -- so it becomes a fresh (not duplicate) record.
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_udv,
                        rec.udv_row_seq,
                        'UDA,UDA Value',
                        'DUP_UDA_VALUE');
            L_udv_error :=TRUE;
         end if;
         if rec.udv_action IN (action_mod,action_del)
            and rec.PK_UDA_VALUES_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_udv,
                        rec.udv_row_seq,
                        'UDA,UDA Value',
                        'NO_RECORD');
            L_udv_error :=TRUE;
         end if;

         if (rec.udv_action = action_new
            and (rec.uda_action <> action_new or rec.uda_action IS NULL))
            and rec.udv_uda_fk_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_udv,
                        rec.udv_row_seq,
                        'UDA_ID',
                        'NO_UDA_EXIST');
            L_udv_error :=TRUE;
         end if;

         if rec.udv_action IN (action_new, action_mod) then
            if rec.udv_UDA_VALUE_DESC IS NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_udv,
                           rec.udv_row_seq,
                           'UDA_VALUE_DESC',
                           'VAL_DESC_MUST_BE_ENTERED');
               L_udv_error :=TRUE;
            end if;
         end if;

         if PROCESS_UDA_VALUES_VAL(O_error_message,
                                   L_udv_error,
                                   rec,
                                   L_disp,
                                   L_data_type,
                                   L_data_length) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_udv,
                        rec.udv_row_seq,
                        NULL,
                        O_error_message);
            L_udv_error := TRUE;
         end if;
      end if; -- if rec.st_udv_rid IS NOT NULL then
--UDV VALIDATIONS END
-------------------------------------------------------

--VALIDATIONS INVOLVING UDA and UDV START

       if rec.uda_action = action_new then
         if rec.uda_DISPLAY_TYPE is NOT NULL then
            if NOT((( rec.uda_DISPLAY_TYPE  =  'DT'
               and REC.UDA_DATA_TYPE =  'DATE'
               and REC.UDA_DATA_LENGTH IS NULL
               and REC.UDV_UDA_VALUE IS NULL
               and REC.UDV_UDA_VALUE_DESC IS NULL)
               or
               (REC.UDA_DISPLAY_TYPE  =  'FF'
               and REC.UDA_DATA_TYPE =  'ALPHA'
               and REC.UDV_UDA_VALUE IS NULL
               and REC.UDV_UDA_VALUE_DESC IS NULL)
               or
               (REC.UDA_DISPLAY_TYPE  =  'LV'
               and REC.UDA_DATA_TYPE IN ('NUM','ALPHA')))) THEN
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'Display Type,Data Type,Data Length',
                           'CHK_UDA_DATA_FIELDS');
               L_error := TRUE;
            end if;
         end if;
      end if;
      if rec.uda_action = action_new
         and rec.uda_display_type = 'LV'
         and (rec.udv_action <> action_new
              or rec.udv_action IS NULL) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table_uda,
                     rec.uda_row_seq,
                     'UDA_ID',
                     'ATLEAST_ONE_UDA_VAL');
         L_error := TRUE;
         if rec.st_udv_rid IS NOT NULL then
                WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_udv,
                        rec.udv_row_seq,
                        'UDA_ID',
                        'ATLEAST_ONE_UDA_VAL');
            L_udv_error := TRUE;
         end if;
      end if;

--VALIDATIONS INVOLVING UDA and UDV END
-------------------------------------------------------
      if L_error then
         if rec.st_udv_rid IS NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_udv,
                        rec.udv_row_seq,
                        'UDA_ID',
                        'MASTER_REC_ER');
         end if;
      end if;
-------------------------------------------------------
      if NOT L_error then
         if rec.uda_rank = 1 then
            SAVEPOINT do_process;
      end if;

--PROCESSING UDA START

      if rec.uda_action IS NOT NULL and rec.uda_rank = 1 then
         L_uda_temp_rec.display_type               := rec.uda_display_type;
         L_uda_temp_rec.data_type                  := rec.uda_data_type;
         L_uda_temp_rec.data_length                := rec.uda_data_length;
         L_uda_temp_rec.single_value_ind           := UPPER(rec.uda_single_value_ind);
         L_uda_temp_rec.filter_org_id              := rec.uda_filter_org_id;
         L_uda_temp_rec.filter_merch_id            := rec.uda_filter_merch_id;
         L_uda_temp_rec.filter_merch_id_class      := rec.uda_filter_merch_id_class;
         L_uda_temp_rec.filter_merch_id_subclass   := rec.uda_filter_merch_id_subclass;
         L_uda_temp_rec.uda_id                     := rec.uda_uda_id;
         L_uda_temp_rec.uda_desc                   := rec.uda_uda_desc;
         L_uda_temp_rec.module                     := 'ITEM';
         L_uda_temp_rec.create_id                  := LP_user;
         L_uda_temp_rec.create_datetime            := SYSDATE;
         if rec.uda_action = action_new then
            if EXEC_UDA_INS( O_error_message,
                             L_uda_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           NULL,
                           O_error_message);
               L_uda_process_error :=TRUE;
            end if;
         end if;
         if rec.uda_action = action_mod then
            if EXEC_UDA_UPD( O_error_message,
                             L_uda_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           NULL,
                           O_error_message);
               L_uda_process_error :=TRUE;
            end if;
         end if;
         if rec.uda_action = action_del then
            if UDA_SQL.RELATION_EXISTS(O_error_message,
                                       L_uda_exists,
                                       rec.uda_uda_id,
                                       NULL) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           NULL,
                           O_error_message);
               L_uda_process_error :=TRUE;
            end if;
            if L_uda_exists then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table_uda,
                           rec.uda_row_seq,
                           'UDA_ID',
                           'UDA_REL_EXISTS');
               L_uda_process_error :=TRUE;
            else
               if EXEC_UDA_DEL( L_uda_error_message,
                                L_uda_process_error,
                                rec,
                                L_uda_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_uda,
                              rec.uda_row_seq,
                              NULL,
                              O_error_message);
                  L_uda_process_error :=TRUE;
               end if;
            end if;
         end if;
--PROCESSING UDA END
--------------------------------------------
         if L_uda_process_error then
            if rec.udv_action IS  NOT NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table_udv,
                           rec.udv_row_seq,
                           'UDA_ID',
                           'MASTER_REC_ER');
            end if;
         end if;
      end if; --if rec.uda_rank = 1
--------------------------------------------------------

--PROCESSING UDV START
      if (rec.uda_action IN (action_new, action_mod) --need not process the UDV record when
         or rec.uda_action IS NULL)
         and (L_disp IN ('LV')                       --UDA action is DELETE.
         or rec.uda_display_type = 'LV') then        --To check cases with Action as NEW-NEW.
         if rec.udv_action is NOT NULL
            and NOT L_udv_error
            and NOT L_uda_process_error then
            L_uda_values_temp_rec.uda_id            := rec.udv_uda_id;
            L_uda_values_temp_rec.uda_value         := rec.udv_uda_value;
            L_uda_values_temp_rec.uda_value_desc    := rec.udv_uda_value_desc;
            L_uda_values_temp_rec.create_id         := LP_user;
            L_uda_values_temp_rec.create_datetime   := SYSDATE;
            if rec.udv_action = action_new then
               if EXEC_UDA_VALUES_INS( O_error_message,
                                       L_uda_values_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_udv,
                              rec.udv_row_seq,
                              NULL,
                              O_error_message);
                  L_udv_process_error :=TRUE;
               end if;
            end if;
            if rec.udv_action = action_mod then
               if EXEC_UDA_VALUES_UPD( O_error_message,
                                       L_uda_values_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_udv,
                              rec.udv_row_seq,
                              NULL,
                              O_error_message);
                  L_udv_process_error :=TRUE;
               end if;
            end if;

            if rec.udv_action = action_del then
               if UDA_SQL.RELATION_EXISTS( O_error_message,
                                           L_exists,
                                           rec.udv_uda_id,
                                           rec.udv_uda_value) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_udv,
                              rec.udv_row_seq,
                              NULL,
                              O_error_message);
                  L_udv_process_error :=TRUE;
               end if;
               if L_exists then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table_udv,
                              rec.udv_row_seq,
                              'UDA_VALUE',
                              'UDA_VAL_REL_EXISTS');
                  L_udv_process_error :=TRUE;
               else
                  if EXEC_UDA_VALUES_DEL( O_error_message,
                                          L_udv_process_error,
                                          rec) = FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table_udv,
                                 rec.udv_row_seq,
                                 NULL,
                                 O_error_message);
                     L_udv_process_error :=TRUE;
                  end if;
               end if;
            end if;
            if NOT L_udv_process_error then
               L_row_seq_tab.extend();
               L_row_seq_tab(c)  := rec.udv_row_seq;
               c:=c+1;
            end if;
         end if;
      else
         if rec.uda_action = action_del and rec.udv_action <> action_del then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table_udv,
                        rec.udv_row_seq,
                        'UDA_ID',
                        'UDA_DEL_REC');
            L_udv_process_error :=TRUE;
         end if;
      end if;  --uda_action <> action_del

-- CHECK FOR DATA INTEGRITY
      if rec.uda_uda_id <> rec.next_uda_id then
         if (L_disp = 'LV' or rec.uda_display_type = 'LV')then
            if rec.uda_action in (action_new,action_mod) or rec.uda_action is NULL then
               open C_UDV_COUNT(rec.uda_uda_id);
               fetch C_UDV_COUNT into L_count;
               close C_UDV_COUNT;
               if L_count < 1 then
                  L_rowseq_count := L_row_seq_tab.count();
                  if L_rowseq_count > 0 then
                     for i in 1..L_rowseq_count
                     LOOP
                        if rec.udv_action = action_del then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                       I_chunk_id,
                                       L_table_udv,
                                       L_row_seq_tab(i),
                                       'UDA_ID',
                                       'CANNOT_DEL_LAST_UDA_VAL');
                        end if;
                     END LOOP;
                  end if;
                  if rec.uda_action is NOT NULL then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                       I_chunk_id,
                                       L_table_uda,
                                       rec.uda_row_seq,
                                       'UDA_ID',
                                       'HEADER_REC_NOT_PROCESSED');
                  end if;
                  ROLLBACK TO do_process;

               end if;
            end if; --if (L_disp = 'LV' or rec.uda_display_type = 'LV')
         end if;
      end if; --if rec.uda_uda_id <> rec.next_uda_id
   end if;  --if NOT L_error

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_UDV_COUNT%ISOPEN then
         close C_UDV_COUNT;
      end if;
      if C_DISP_TYPE%ISOPEN then
         close C_DISP_TYPE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UDA;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_UDA_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_UDA_TL.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_UDA_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_UDA.PROCESS_UDA_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_uda_TL_temp_rec      uda_TL%ROWTYPE;
   L_uda_tl_upd_rst       ROW_SEQ_TAB;
   L_uda_tl_del_rst       ROW_SEQ_TAB;

   cursor C_UDA_TL(I_process_id NUMBER,
                   I_chunk_id NUMBER) is
      select pk_uda_tl.rowid  as pk_uda_tl_rid,
             fk_uda.rowid     as fk_uda_rid,
             fk_lang.rowid    as fk_lang_rid,
             st.lang,
             st.uda_id,
             st.uda_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_uda_tl  st,
             uda         fk_uda,
             uda_tl      pk_uda_TL,
             lang        fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.uda_id  =  fk_uda.uda_id (+)
         and st.lang        =  pk_uda_TL.lang (+)
         and st.uda_id  =  pk_uda_TL.uda_id (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_UDA_TL is TABLE OF C_UDA_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_uda_tab        SVC_UDA_TL;

   L_uda_TL_ins_tab         uda_tab         := NEW uda_tab();
   L_uda_TL_upd_tab         uda_tab         := NEW uda_tab();
   L_uda_TL_del_tab         uda_tab         := NEW uda_tab();

BEGIN
   if C_UDA_TL%ISOPEN then
      close C_UDA_TL;
   end if;

   open C_UDA_TL(I_process_id,
                 I_chunk_id);
   LOOP
      fetch C_UDA_TL bulk collect into L_svc_uda_tab limit LP_bulk_fetch_limit;
      if L_svc_uda_tab.COUNT > 0 then
         FOR i in L_svc_uda_tab.FIRST..L_svc_uda_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_uda_tab(i).action is NULL
               or L_svc_uda_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_uda_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_uda_tab(i).action = action_new
               and L_svc_uda_tab(i).pk_uda_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_uda_tab(i).action IN (action_mod, action_del)
               and L_svc_uda_tab(i).lang is NOT NULL
               and L_svc_uda_tab(i).uda_id is NOT NULL
               and L_svc_uda_tab(i).pk_uda_TL_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_uda_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_uda_tab(i).action = action_new
               and L_svc_uda_tab(i).uda_id is NOT NULL
               and L_svc_uda_tab(i).fk_uda_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_uda_tab(i).row_seq,
                            'UDA_ID',
                            'NO_UDA_EXIST');
               L_error :=TRUE;
            end if;

            if L_svc_uda_tab(i).action = action_new
               and L_svc_uda_tab(i).lang is NOT NULL
               and L_svc_uda_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_uda_tab(i).action in (action_new, action_mod) then
               if L_svc_uda_tab(i).uda_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_uda_tab(i).row_seq,
                              'UDA_DESC',
                              'MUST_ENTER_FIELD');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_uda_tab(i).uda_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_tab(i).row_seq,
                           'UDA_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_uda_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_uda_TL_temp_rec.lang := L_svc_uda_tab(i).lang;
               L_uda_TL_temp_rec.uda_id := L_svc_uda_tab(i).uda_id;
               L_uda_TL_temp_rec.uda_desc := L_svc_uda_tab(i).uda_desc;
               L_uda_TL_temp_rec.create_datetime := SYSDATE;
               L_uda_TL_temp_rec.create_id := LP_user;
               L_uda_TL_temp_rec.last_update_datetime := SYSDATE;
               L_uda_TL_temp_rec.last_update_id := LP_user;

               if L_svc_uda_tab(i).action = action_new then
                  L_uda_TL_ins_tab.extend;
                  L_uda_TL_ins_tab(L_uda_TL_ins_tab.count()) := L_uda_TL_temp_rec;
               end if;

               if L_svc_uda_tab(i).action = action_mod then
                  L_uda_TL_upd_tab.extend;
                  L_uda_TL_upd_tab(L_uda_TL_upd_tab.count()) := L_uda_TL_temp_rec;
                  L_uda_tl_upd_rst(L_uda_TL_upd_tab.count()) := L_svc_uda_tab(i).row_seq;
               end if;

               if L_svc_uda_tab(i).action = action_del then
                  L_uda_TL_del_tab.extend;
                  L_uda_TL_del_tab(L_uda_TL_del_tab.count()) := L_uda_TL_temp_rec;
                  L_uda_tl_del_rst(L_uda_TL_del_tab.count()) := L_svc_uda_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_UDA_TL%NOTFOUND;
   END LOOP;
   close C_UDA_TL;

   if EXEC_UDA_TL_INS(O_error_message,
                      L_uda_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_UDA_TL_UPD(O_error_message,
                      L_uda_TL_upd_tab,
                      L_uda_tl_upd_rst,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_UDA_TL_DEL(O_error_message,
                      L_uda_TL_del_tab,
                      L_uda_tl_del_rst,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UDA_TL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_UDA_VALUES_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       svc_uda_values_tl.PROCESS_ID%TYPE,
                               I_chunk_id        IN       svc_uda_values_tl.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                   VARCHAR2(64) := 'CORESVC_UDA_VALUES.PROCESS_UDA_VALUES_TL';
   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_VALUES_TL';
   L_base_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'UDA_VALUES';
   L_table                     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA_VALUES_TL';
   L_error                     BOOLEAN := FALSE;
   L_process_error             BOOLEAN := FALSE;
   L_uda_values_tl_temp_rec    uda_values_tl%ROWTYPE;
   L_uda_values_tl_upd_rst     ROW_SEQ_TAB;
   L_uda_values_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_UDA_VALUES_TL(I_process_id NUMBER,
                              I_chunk_id NUMBER) is
      select pk_uda_values_tl.rowid  as pk_uda_values_tl_rid,
             fk_uda_values.rowid     as fk_uda_values_rid,
             fk_lang.rowid           as fk_lang_rid,
             st.lang,
             st.uda_id,
             st.uda_value,
             st.uda_value_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_uda_values_tl  st,
             uda_values         fk_uda_values,
             uda_values_tl      pk_uda_values_tl,
             lang               fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.uda_id      =  fk_uda_values.uda_id (+)
         and st.uda_value   =  fk_uda_values.uda_value (+)
         and st.lang        =  pk_uda_values_tl.lang (+)
         and st.uda_id      =  pk_uda_values_tl.uda_id (+)
         and st.uda_value   =  pk_uda_values_tl.uda_value (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE svc_uda_values_tl is TABLE OF C_SVC_UDA_VALUES_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_uda_values_tab        svc_uda_values_tl;

   L_uda_values_tl_ins_tab         uda_values_tab         := NEW uda_values_tab();
   L_uda_values_tl_upd_tab         uda_values_tab         := NEW uda_values_tab();
   L_uda_values_tl_del_tab         uda_values_tab         := NEW uda_values_tab();

BEGIN
   if C_SVC_UDA_VALUES_TL%ISOPEN then
      close C_SVC_UDA_VALUES_TL;
   end if;

   open C_SVC_UDA_VALUES_TL(I_process_id,
                       I_chunk_id);
   LOOP
      fetch C_SVC_UDA_VALUES_TL bulk collect into L_svc_uda_values_tab limit LP_bulk_fetch_limit;
      if L_svc_uda_values_tab.COUNT > 0 then
         FOR i in L_svc_uda_values_tab.FIRST..L_svc_uda_values_tab.LAST LOOP
            L_error := FALSE;

            -- check if action is valid
            if L_svc_uda_values_tab(i).action is NULL
               or L_svc_uda_values_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_values_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_uda_values_tab(i).lang = LP_primary_lang and L_svc_uda_values_tab(i).action = action_new then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_values_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG');
               L_error :=TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_uda_values_tab(i).action = action_new
               and L_svc_uda_values_tab(i).pk_uda_values_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_values_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_uda_values_tab(i).action IN (action_mod, action_del)
               and L_svc_uda_values_tab(i).lang is NOT NULL
               and L_svc_uda_values_tab(i).uda_value is NOT NULL
               and L_svc_uda_values_tab(i).uda_id is NOT NULL
               and L_svc_uda_values_tab(i).pk_uda_values_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_uda_values_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_uda_values_tab(i).action = action_new
               and L_svc_uda_values_tab(i).uda_value is NOT NULL
               and L_svc_uda_values_tab(i).uda_id is NOT NULL
               and L_svc_uda_values_tab(i).fk_uda_values_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_uda_values_tab(i).row_seq,
                            NULL,
                           'INV_VAL_UPD');
               L_error :=TRUE;
            end if;

            if L_svc_uda_values_tab(i).action = action_new
               and L_svc_uda_values_tab(i).lang is NOT NULL
               and L_svc_uda_values_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_values_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_uda_values_tab(i).action in (action_new, action_mod) then
               if L_svc_uda_values_tab(i).uda_value_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_uda_values_tab(i).row_seq,
                              'UDA_VALUE_DESC',
                              'UDA_DESC_MUST_BE_ENTERED');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_uda_values_tab(i).uda_value is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_values_tab(i).row_seq,
                           'UDA_VALUE',
                           'UDA_VALUE_MUST_BE_ENTERED');
               L_error :=TRUE;
            end if;

            if L_svc_uda_values_tab(i).uda_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_values_tab(i).row_seq,
                           'UDA_ID',
                           'UDA_ID_REQUIRED');
               L_error :=TRUE;
            end if;

            if L_svc_uda_values_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_uda_values_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_uda_values_tl_temp_rec.lang := L_svc_uda_values_tab(i).lang;
               L_uda_values_tl_temp_rec.uda_id := L_svc_uda_values_tab(i).uda_id;
               L_uda_values_tl_temp_rec.uda_value := L_svc_uda_values_tab(i).uda_value;
               L_uda_values_tl_temp_rec.uda_value_desc := L_svc_uda_values_tab(i).uda_value_desc;
               L_uda_values_tl_temp_rec.create_datetime := SYSDATE;
               L_uda_values_tl_temp_rec.create_id := LP_user;
               L_uda_values_tl_temp_rec.last_update_datetime := SYSDATE;
               L_uda_values_tl_temp_rec.last_update_id := LP_user;

               if L_svc_uda_values_tab(i).action = action_new then
                  L_uda_values_tl_ins_tab.extend;
                  L_uda_values_tl_ins_tab(L_uda_values_tl_ins_tab.count()) := L_uda_values_tl_temp_rec;
               end if;

               if L_svc_uda_values_tab(i).action = action_mod then
                  L_uda_values_tl_upd_tab.extend;
                  L_uda_values_tl_upd_tab(L_uda_values_tl_upd_tab.count()) := L_uda_values_tl_temp_rec;
                  L_uda_values_tl_upd_rst(L_uda_values_tl_upd_tab.count()) := L_svc_uda_values_tab(i).row_seq;
               end if;

               if L_svc_uda_values_tab(i).action = action_del then
                  L_uda_values_tl_del_tab.extend;
                  L_uda_values_tl_del_tab(L_uda_values_tl_del_tab.count()) := L_uda_values_tl_temp_rec;
                  L_uda_values_tl_del_rst(L_uda_values_tl_del_tab.count()) := L_svc_uda_values_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_UDA_VALUES_TL%NOTFOUND;
   END LOOP;
   close C_SVC_UDA_VALUES_TL;

   if EXEC_UDA_VALUES_TL_INS(O_error_message,
                             L_uda_values_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_UDA_VALUES_TL_UPD(O_error_message,
                             L_uda_values_tl_upd_tab,
                             L_uda_values_tl_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_UDA_VALUES_TL_DEL(O_error_message,
                             L_uda_values_tl_del_tab,
                             L_uda_values_tl_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UDA_VALUES_TL;
----------------------------------------------------------------------------------
FUNCTION PROCESS_UID( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_process_id      IN       SVC_UDA_ITEM_DEFAULTS.PROCESS_ID%TYPE,
                      I_chunk_id        IN       SVC_UDA_ITEM_DEFAULTS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_UDA.PROCESS_UID';
   L_table            SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_UDA_ITEM_DEFAULTS';  
   L_error            BOOLEAN;
   L_process_error    BOOLEAN := FALSE;
   L_error_message    VARCHAR2(600);
   L_UID_temp_rec     UDA_ITEM_DEFAULTS%ROWTYPE;
   L_hierarchy_value  NUMBER(1) := 0; 
   L_seq_no           UDA_ITEM_DEFAULTS.seq_no%TYPE;
      
BEGIN
   FOR rec IN C_SVC_UID(I_process_id,
                        I_chunk_id)
   LOOP
      L_error           := FALSE;
      L_process_error   := FALSE;

      if rec.class is NULL then
         L_hierarchy_value := 1;
      elsif rec.subclass is NULL then
         L_hierarchy_value := 2;
      else
         L_hierarchy_value := 3;
      end if;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_UDA_ITEM_DEFAULTS_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UDA,UDA Value,Department,Class,Subclass',
                     'NO_RECORD');
         L_error :=TRUE;
      end if;
      if NOT((rec.SUBCLASS IS NOT NULL and rec.CLASS IS NOT NULL and rec.DEPT IS NOT NULL)  
         or(rec.SUBCLASS IS NULL and rec.CLASS IS NOT NULL and rec.DEPT IS NOT NULL)  
         or(rec.SUBCLASS IS NULL and rec.CLASS IS NULL and rec.DEPT IS NOT NULL)) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Department,Class,Subclass',
                     'INV_HIER');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new, action_del) 
         and rec.uda_value_upd IS NOT NULL then 
         if rec.uda_value <> rec.uda_value_upd then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UDA Value,New User Defined Attribute Value',
                        'UID_INV_VALUE');
            L_error :=TRUE;
         end if;
         if rec.uda_value IS NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UDA Value,New User Defined Attribute Value',
                        'NULL_UDA_VAL');
            L_error :=TRUE;
         end if;         
      end if;                   
      if rec.action = action_mod
         and rec.uid_udv_fk_rid is NULL 
         and rec.uda_value_upd IS NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'UDA_VALUE_UPD',
                     'INV_VAL_UPD');
         L_error :=TRUE;
      end if;
         
      if rec.action = action_new then
         if rec.uda_value IS NOT NULL
            and rec.uid_udv_fk_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UDA_VALUE',
                        'INV_VAL_UPD');
            L_error :=TRUE;
         end if;
      end if;   
      if rec.action IN (action_new, action_mod)then         
         if rec.REQUIRED_IND IS NULL 
            or rec.REQUIRED_IND NOT IN ('Y','N') then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'REQUIRED_IND',
                        'INV_Y_N_IND');
            L_error :=TRUE;
         end if;
     
         if PROCESS_UID_VAL (L_error_message,
                             L_error,
                             rec,
                             L_hierarchy_value) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        L_error_message);
            L_error :=TRUE;
         end if;
      end if;   --if rec.action IN (action_new,action_mod)      
      
      if NOT L_error 
         and rec.action = action_new then
         if UDA_SQL.NEXT_SEQUENCE_NO(L_error_message,
                                     L_seq_no) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        L_error_message);
            L_error :=TRUE;
         end if; 
      end if;
      
      if NOT L_error then
         L_UID_temp_rec.uda_id             := rec.uda_id;
         L_UID_temp_rec.dept               := rec.dept;
         L_UID_temp_rec.class              := rec.class;
         L_UID_temp_rec.subclass           := rec.subclass;
         L_UID_temp_rec.required_ind       := rec.required_ind;      
         if rec.action = action_new then 
            L_UID_temp_rec.seq_no          := L_seq_no;
         else 
            L_UID_temp_rec.seq_no          := rec.pk_seq_no;
         end if;             
         if rec.action = action_mod then
            L_UID_temp_rec.uda_value       := rec.uda_value_upd;
         else
            L_UID_temp_rec.uda_value       := rec.uda_value;
         end if;
         if rec.class is NULL then
            L_UID_temp_rec.hierarchy_value := 1;
         elsif rec.subclass is NULL then
            L_UID_temp_rec.hierarchy_value := 2;
         else
            L_UID_temp_rec.hierarchy_value := 3;
         end if;
         if rec.action = action_new then
            if EXEC_UID_INS( L_error_message,
                             rec,
                             L_UID_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_UID_UPD( L_error_message,
                             rec,
                             L_UID_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_UID_DEL( L_error_message,
                             rec,
                             L_UID_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_UID;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS
BEGIN
   delete from svc_uda_item_defaults
      where process_id = I_process_id;

   delete from svc_uda_tl
      where process_id = I_process_id;

   delete from svc_uda
      where process_id = I_process_id;

   delete from svc_uda_values_tl
      where process_id = I_process_id;

   delete from svc_uda_values
      where process_id = I_process_id;
END;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    :='CORESVC_UDA.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_UDA(O_error_message,
                  I_process_id,
                  I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_UDA_TL(O_error_message,
                     I_process_id,
                     I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_UDA_VALUES_TL(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   if PROCESS_UID(O_error_message,
                  I_process_id,
                  I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
    CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
    CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
----------------------------------------------------------------------------------------------
END CORESVC_UDA;
/

