CREATE OR REPLACE PACKAGE BODY CORESVC_RTK_ROLE_PRIVS AS
   cursor C_SVC_RTK_ROLE_PRIVS(I_process_id   NUMBER,
                               I_chunk_id     NUMBER) is
      select pk_rtk_role_privs.rowid  AS pk_rtk_role_privs_rid,
             st.rowid AS st_rid,
             st.ord_appr_amt,
             st.role,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_rtk_role_privs st,
             rtk_role_privs pk_rtk_role_privs
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.role       = pk_rtk_role_privs.role (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   
   LP_errors_tab errors_tab_typ;
   
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE) IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets              s9t_pkg.names_map_typ;
   RTK_ROLE_PRIVS_cols   s9t_pkg.names_map_typ;
BEGIN
   L_sheets                      := s9t_pkg.get_sheet_names(I_file_id);
   RTK_ROLE_PRIVS_cols           := s9t_pkg.get_col_names(I_file_id,RTK_ROLE_PRIVS_sheet);
   RTK_ROLE_PRIVS$Action         := RTK_ROLE_PRIVS_cols('ACTION');
   RTK_ROLE_PRIVS$ORD_APPR_AMT   := RTK_ROLE_PRIVS_cols('ORD_APPR_AMT');
   RTK_ROLE_PRIVS$ROLE           := RTK_ROLE_PRIVS_cols('ROLE');
   
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_RTK_ROLE_PRIVS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = RTK_ROLE_PRIVS_sheet )
   select s9t_row(s9t_cells(CORESVC_RTK_ROLE_PRIVS.action_mod ,
                            role,
                            ord_appr_amt))
     from rtk_role_privs ;

END POPULATE_RTK_ROLE_PRIVS;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(RTK_ROLE_PRIVS_sheet);
   L_file.sheets(l_file.get_sheet_index(RTK_ROLE_PRIVS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                            'ROLE',
                                                                                            'ORD_APPR_AMT'
                                                                                          );
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id             IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind   IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(64):='CORESVC_RTK_ROLE_PRIVS.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   
   if I_template_only_ind = 'N' then
      POPULATE_RTK_ROLE_PRIVS(O_file_id);
      COMMIT;
   end if;
   
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if; 
   
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_RTK_ROLE_PRIVS( I_file_id      IN   s9t_folder.file_id%TYPE,
                                      I_process_id   IN   SVC_RTK_ROLE_PRIVS.process_id%TYPE) IS
                                      
   TYPE svc_RTK_ROLE_PRIVS_col_typ IS TABLE OF SVC_RTK_ROLE_PRIVS%ROWTYPE;
   L_temp_rec               SVC_RTK_ROLE_PRIVS%ROWTYPE;
   svc_RTK_ROLE_PRIVS_col   svc_RTK_ROLE_PRIVS_col_typ :=NEW svc_RTK_ROLE_PRIVS_col_typ();
   L_process_id             SVC_RTK_ROLE_PRIVS.process_id%TYPE;
   L_error                  BOOLEAN := FALSE;
   L_default_rec            SVC_RTK_ROLE_PRIVS%ROWTYPE;
   
   cursor C_MANDATORY_IND is
      select ORD_APPR_AMT_mi,
             ROLE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key   = CORESVC_RTK_ROLE_PRIVS.template_key
                 and wksht_key      = 'RTK_ROLE_PRIVS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ( 'ORD_APPR_AMT' AS ORD_APPR_AMT,
                                             'ROLE' AS ROLE,
                                             null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors      EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);

      L_pk_columns    VARCHAR2(255)  := 'Role';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  -- Get default values.
   FOR rec IN (select  ORD_APPR_AMT_dv,
                       ROLE_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key   = CORESVC_RTK_ROLE_PRIVS.template_key
                          and wksht_key      = 'RTK_ROLE_PRIVS'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'ORD_APPR_AMT' AS ORD_APPR_AMT,
                                                      'ROLE' AS ROLE,
                                                      NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.ORD_APPR_AMT := rec.ORD_APPR_AMT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'RTK_ROLE_PRIVS ' ,
                            NULL,
                            'ORD_APPR_AMT ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ROLE := rec.ROLE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'RTK_ROLE_PRIVS ' ,
                            NULL,
                            'ROLE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(RTK_ROLE_PRIVS$Action)       AS Action,
                      r.get_cell(RTK_ROLE_PRIVS$ORD_APPR_AMT) AS ORD_APPR_AMT,
                      r.get_cell(RTK_ROLE_PRIVS$ROLE)         AS ROLE,
                      r.get_row_seq()                         AS row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(RTK_ROLE_PRIVS_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ROLE_PRIVS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ORD_APPR_AMT := rec.ORD_APPR_AMT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ROLE_PRIVS_sheet,
                            rec.row_seq,
                            'ORD_APPR_AMT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ROLE := rec.ROLE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            RTK_ROLE_PRIVS_sheet,
                            rec.row_seq,
                            'ROLE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_RTK_ROLE_PRIVS.action_new then
         L_temp_rec.ORD_APPR_AMT := NVL( L_temp_rec.ORD_APPR_AMT,L_default_rec.ORD_APPR_AMT);
         L_temp_rec.ROLE := NVL( L_temp_rec.ROLE,L_default_rec.ROLE);
      end if;
      if not (
            L_temp_rec.ROLE is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         RTK_ROLE_PRIVS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_RTK_ROLE_PRIVS_col.extend();
         svc_RTK_ROLE_PRIVS_col(svc_RTK_ROLE_PRIVS_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_RTK_ROLE_PRIVS_col.COUNT SAVE EXCEPTIONS
      merge into SVC_RTK_ROLE_PRIVS st
      using(select
                  (case
                   when l_mi_rec.ORD_APPR_AMT_mi    = 'N'
                    and svc_RTK_ROLE_PRIVS_col(i).action = CORESVC_RTK_ROLE_PRIVS.action_mod
                    and s1.ORD_APPR_AMT IS NULL
                   then mt.ORD_APPR_AMT
                   else s1.ORD_APPR_AMT
                   end) AS ORD_APPR_AMT,
                  (case
                   when l_mi_rec.ROLE_mi    = 'N'
                    and svc_RTK_ROLE_PRIVS_col(i).action = CORESVC_RTK_ROLE_PRIVS.action_mod
                    and s1.ROLE IS NULL
                   then mt.ROLE
                   else s1.ROLE
                   end) AS ROLE,
                  null as dummy
              from (select
                          svc_RTK_ROLE_PRIVS_col(i).ORD_APPR_AMT AS ORD_APPR_AMT,
                          svc_RTK_ROLE_PRIVS_col(i).ROLE AS ROLE,
                          null as dummy
                      from dual ) s1,
            RTK_ROLE_PRIVS mt
             where
                  mt.ROLE (+)     = s1.ROLE   and
                  1 = 1 )sq
                on (
                    st.ROLE      = sq.ROLE and
                    svc_RTK_ROLE_PRIVS_col(i).ACTION IN (CORESVC_RTK_ROLE_PRIVS.action_mod,CORESVC_RTK_ROLE_PRIVS.action_del))
      when matched then
      update
         set process_id        = svc_RTK_ROLE_PRIVS_col(i).process_id ,
             chunk_id          = svc_RTK_ROLE_PRIVS_col(i).chunk_id ,
             row_seq           = svc_RTK_ROLE_PRIVS_col(i).row_seq ,
             action            = svc_RTK_ROLE_PRIVS_col(i).action ,
             process$status    = svc_RTK_ROLE_PRIVS_col(i).process$status ,
             ord_appr_amt      = sq.ord_appr_amt ,
             create_id         = svc_RTK_ROLE_PRIVS_col(i).create_id ,
             create_datetime   = svc_RTK_ROLE_PRIVS_col(i).create_datetime ,
             last_upd_id       = svc_RTK_ROLE_PRIVS_col(i).last_upd_id ,
             last_upd_datetime = svc_RTK_ROLE_PRIVS_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             ord_appr_amt ,
             role ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_RTK_ROLE_PRIVS_col(i).process_id ,
             svc_RTK_ROLE_PRIVS_col(i).chunk_id ,
             svc_RTK_ROLE_PRIVS_col(i).row_seq ,
             svc_RTK_ROLE_PRIVS_col(i).action ,
             svc_RTK_ROLE_PRIVS_col(i).process$status ,
             sq.ord_appr_amt ,
             sq.role ,
             svc_RTK_ROLE_PRIVS_col(i).create_id ,
             svc_RTK_ROLE_PRIVS_col(i).create_datetime ,
             svc_RTK_ROLE_PRIVS_col(i).last_upd_id ,
             svc_RTK_ROLE_PRIVS_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            RTK_ROLE_PRIVS_sheet,
                            svc_RTK_ROLE_PRIVS_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_RTK_ROLE_PRIVS;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT        OUT   NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file             s9t_file;
   L_sheets           s9t_pkg.names_map_typ;
   L_program          VARCHAR2(64) := 'CORESVC_RTK_ROLE_PRIVS.process_s9t';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   
   s9t_pkg.save_obj(L_file);
   
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_RTK_ROLE_PRIVS(I_file_id,I_process_id);
   end if;
   
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
   
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);

         update svc_process_tracker
            set status       = 'PE',
                file_id      = i_file_id
          where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', 
                                             NULL, 
                                             NULL, 
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
         values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;   
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ROLE_PRIVS_INS(  O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   L_rtk_role_privs_temp_rec   IN   RTK_ROLE_PRIVS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_RTK_ROLE_PRIVS.EXEC_RTK_ROLE_PRIVS_INS';
   L_table     VARCHAR2(64) := 'SVC_RTK_ROLE_PRIVS';
BEGIN
   insert
     into rtk_role_privs
   values L_rtk_role_privs_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_RTK_ROLE_PRIVS_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ROLE_PRIVS_UPD( O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  L_rtk_role_privs_temp_rec   IN       RTK_ROLE_PRIVS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ROLE_PRIVS.EXEC_RTK_ROLE_PRIVS_UPD';
   L_table         VARCHAR2(64) := 'SVC_RTK_ROLE_PRIVS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   CURSOR C_LOCK_RTK_ROLE_PRIVS is
      select 'x'
        from rtk_role_privs
       where role = L_rtk_role_privs_temp_rec.role;
BEGIN
   open C_LOCK_RTK_ROLE_PRIVS;
   close C_LOCK_RTK_ROLE_PRIVS;

   update rtk_role_privs
      set row = L_rtk_role_privs_temp_rec
    where 1 = 1
      and role = L_rtk_role_privs_temp_rec.role;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_rtk_role_privs_temp_rec.role,
                                             NULL);
      close C_LOCK_RTK_ROLE_PRIVS;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_RTK_ROLE_PRIVS%ISOPEN then
         close C_LOCK_RTK_ROLE_PRIVS;
      end if;
      return FALSE;
END EXEC_RTK_ROLE_PRIVS_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_RTK_ROLE_PRIVS_DEL( O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  L_rtk_role_privs_temp_rec   IN   RTK_ROLE_PRIVS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ROLE_PRIVS.EXEC_RTK_ROLE_PRIVS_DEL';
   L_table         VARCHAR2(64) := 'SVC_RTK_ROLE_PRIVS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   
   CURSOR C_LOCK_RTK_ROLE_PRIVS is
      select 'x'
        from rtk_role_privs
       where role = L_rtk_role_privs_temp_rec.role;
BEGIN
   open C_LOCK_RTK_ROLE_PRIVS;
   close C_LOCK_RTK_ROLE_PRIVS;

   delete
     from rtk_role_privs
    where role = L_rtk_role_privs_temp_rec.role;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_rtk_role_privs_temp_rec.role,
                                             NULL);
      close C_LOCK_RTK_ROLE_PRIVS;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_RTK_ROLE_PRIVS%ISOPEN then
         close C_LOCK_RTK_ROLE_PRIVS;
      end if;
      return FALSE;
END EXEC_RTK_ROLE_PRIVS_DEL;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN

   delete from svc_rtk_role_privs
      where process_id = I_process_id;

END CLEAR_STAGING_DATA;
--------------------------------------------------------------------------------
FUNCTION PROCESS_RTK_ROLE_PRIVS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_process_id      IN       SVC_RTK_ROLE_PRIVS.PROCESS_ID%TYPE,
                                 I_chunk_id        IN       SVC_RTK_ROLE_PRIVS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error                     BOOLEAN;
   L_process_error             BOOLEAN := FALSE;
   L_program                   VARCHAR2(64):='CORESVC_RTK_ROLE_PRIVS.PROCESS_RTK_ROLE_PRIVS';
   L_error_message             VARCHAR2(600);
   L_RTK_ROLE_PRIVS_temp_rec   RTK_ROLE_PRIVS%ROWTYPE;
   L_table                     VARCHAR2(64) := 'SVC_RTK_ROLE_PRIVS';
   L_role                      DBA_ROLES.ROLE%TYPE;
   L_base_trans_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'RTK_ROLE_PRIVS';
   L_exists                    VARCHAR2(1)  := 'N';
   
   CURSOR C_CHK_SEC_USER_ROLE(I_role RTK_ROLE_PRIVS.ROLE%TYPE) is
   select 'x'
     from SEC_USER_ROLE
    where ROLE =  I_role;
BEGIN
   FOR rec IN c_svc_RTK_ROLE_PRIVS(I_process_id,
                                   I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.PK_RTK_ROLE_PRIVS_rid is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               L_base_trans_table,
                                               NULL,
                                               NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ROLE',
                     O_error_message);
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_RTK_ROLE_PRIVS_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ROLE',
                     'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      end if;

      if NOT(  rec.ROLE  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ROLE',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;

      if rec.action = action_del then
         open C_CHK_SEC_USER_ROLE(rec.role);
         fetch C_CHK_SEC_USER_ROLE  into L_exists;
         if C_CHK_SEC_USER_ROLE%FOUND then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ROLE',
                        'CHILD_EXISTS');
             L_error := TRUE;
         end if;
         close C_CHK_SEC_USER_ROLE;
      end if;

   if NOT L_error then
      L_rtk_role_privs_temp_rec.role              := rec.role;
      L_rtk_role_privs_temp_rec.ord_appr_amt      := rec.ord_appr_amt;
      L_rtk_role_privs_temp_rec.create_id         := GET_USER;
      L_rtk_role_privs_temp_rec.create_datetime   := SYSDATE;
      
      if rec.action = action_new then
         if EXEC_RTK_ROLE_PRIVS_INS( O_error_message,
                                     L_rtk_role_privs_temp_rec)=FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        rec.action,
                        O_error_message);
            L_process_error :=TRUE;
         end if;
      end if;

      if rec.action = action_mod then
         if EXEC_RTK_ROLE_PRIVS_UPD( O_error_message,
                                     L_rtk_role_privs_temp_rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        rec.action,
                        O_error_message);
            L_process_error :=TRUE;
         end if;
      end if;

      if rec.action = action_del then
         if EXEC_RTK_ROLE_PRIVS_DEL( O_error_message,
                                     L_rtk_role_privs_temp_rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        rec.action,
                        O_error_message);
            L_process_error :=TRUE;
         end if;
      end if;
   end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_RTK_ROLE_PRIVS;
--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_RTK_ROLE_PRIVS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_RTK_ROLE_PRIVS(O_error_message,
                             I_process_id,
                             I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);

   LP_errors_tab := NEW errors_tab_typ();
   
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_RTK_ROLE_PRIVS;
/
