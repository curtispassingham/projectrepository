
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY MERCH_SQL AS
-------------------------------------------------------------------------------------------------------
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_COMPANY
   -- Purpose      : This function will lock the COMPANY table for update.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_COMPANY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_company_id      IN       COMPHEAD.COMPANY%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_DIVISION_TL
   -- Purpose      : This function will lock the DIVISION_TL table for delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_DIVISION_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_division_id     IN       DIVISION.DIVISION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_DIVISION
   -- Purpose      : This function will lock the DIVISION table for update or delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_division_id     IN       DIVISION.DIVISION%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_GROUP_TL
   -- Purpose      : This function will lock the GROUP_TL table for delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_GROUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_group_no        IN       GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_GROUP
   -- Purpose      : This function will lock the GROUP table for update or delete.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_group_no        IN       GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_CLASS
   -- Purpose      : This function will lock the CLASS table for update.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_class           IN       CLASS.CLASS%TYPE,
                    I_dept            IN       CLASS.DEPT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: LOCK_SUBCLASS
   -- Purpose      : This function will lock the SUBCLASS table for update.
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_dept            IN       SUBCLASS.DEPT%TYPE,
                       I_class           IN       SUBCLASS.CLASS%TYPE,
                       I_subclass        IN       SUBCLASS.SUBCLASS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COMPANY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_company_rec     IN       COMPHEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.INSERT_COMPANY';

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'COMPANY',
                    'company: '||I_company_rec.company);

   insert into comphead(company,
                        co_name,
                        co_add1,
                        co_add2,
                        co_add3,
                        co_city,
                        co_state,
                        co_country,
                        co_post)
               values(I_company_rec.company,
                      I_company_rec.co_name,
                      I_company_rec.co_add1,
                      I_company_rec.co_add2,
                      I_company_rec.co_add3,
                      I_company_rec.co_city,
                      I_company_rec.co_state,
                      I_company_rec.co_country,
                      I_company_rec.co_post);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_COMPANY;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_COMPANY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_company_rec     IN       COMPHEAD%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.UPDATE_COMPANY';

BEGIN
   if not LOCK_COMPANY(O_error_message,
                       I_company_rec.company) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'COMPANY',
                    'company: ' || I_company_rec.company);
   update comphead
      set co_name  = NVL(I_company_rec.co_name, co_name),
          co_add1  = NVL(I_company_rec.co_add1,co_add1),
          co_add2  = I_company_rec.co_add2,
          co_add3  = I_company_rec.co_add3,
          co_city  = NVL(I_company_rec.co_city,co_city),
          co_state = I_company_rec.co_state,
          co_country = NVL(I_company_rec.co_country,co_country),
          co_post  = I_company_rec.co_post
    where company  = I_company_rec.company;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_COMPANY;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.INSERT_DIVISION';

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'DIVISION',
                    'division id: '||I_division_rec.division);
   insert into division (division,
                         div_name,
                         buyer,
                         merch,
                         total_market_amt)
                 values (I_division_rec.division,
                         I_division_rec.div_name,
                         I_division_rec.buyer,
                         I_division_rec.merch,
                         I_division_rec.total_market_amt);
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_DIVISION;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.MODIFY_DIVISION';

BEGIN
   if not LOCK_DIVISION(O_error_message,
                        I_division_rec.division) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'DIVISION',
                    'division id: '||I_division_rec.division);
   update division
      set div_name = NVL(I_division_rec.div_name, div_name),
          buyer = I_division_rec.buyer,
          merch = I_division_rec.merch,
          total_market_amt = I_division_rec.total_market_amt
    where division = I_division_rec.division;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_DIVISION;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_division_rec    IN       DIVISION%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.DELETE_DIVISION';

BEGIN
   if not LOCK_DIVISION_TL(O_error_message,
                        I_division_rec.division) then
      return FALSE;
   end if;
   
   if not LOCK_DIVISION(O_error_message,
                        I_division_rec.division) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'DIVISION_TL',
                    'division id: '||I_division_rec.division);
   delete from division_tl
    where division = I_division_rec.division;

   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'DIVISION',
                    'division id: '||I_division_rec.division);
   delete from division
    where division = I_division_rec.division;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DIVISION;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.INSERT_GROUP';

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'GROUPS',
                    'group_no: '||I_group_rec.group_no);
   insert into groups (group_no,
                       group_name,
                       buyer,
                       merch,
                       division)
               values (I_group_rec.group_no,
                       I_group_rec.group_name,
                       I_group_rec.buyer,
                       I_group_rec.merch,
                       I_group_rec.division);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_GROUP;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.MODIFY_GROUP';

BEGIN
   if not LOCK_GROUP(O_error_message,
                     I_group_rec.group_no) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'GROUPS',
                    'group no: ' || I_group_rec.group_no);
   update groups
      set group_name = NVL(I_group_rec.group_name, group_name),
          buyer = I_group_rec.buyer,
          merch = I_group_rec.merch,
          division = NVL(I_group_rec.division, division)
    where group_no = I_group_rec.group_no;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_GROUP;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_rec       IN       GROUPS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.DELETE_GROUP';

BEGIN
   if not LOCK_GROUP_TL(O_error_message,
                     I_group_rec.group_no) then
      return FALSE;
   end if;
   
   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'GROUPS_TL',
                    'group no: '||I_group_rec.group_no);
   delete from groups_tl
    where group_no = I_group_rec.group_no;
    
   ---
    if not LOCK_GROUP(O_error_message,
                     I_group_rec.group_no) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'GROUPS',
                    'group no: '||I_group_rec.group_no);
   delete from groups
    where group_no = I_group_rec.group_no;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_RECORDS');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_GROUP;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_DEPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_dept_rec        IN       MERCH_SQL.DEPT_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.INSERT_DEPT';

BEGIN

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'DEPS',
                    'dept: '||I_dept_rec.dept_row.dept);
   insert into deps(dept,
                    dept_name,
                    buyer,
                    merch,
                    profit_calc_type,
                    purchase_type,
                    group_no,
                    bud_int,
                    bud_mkup,
                    total_market_amt,
                    markup_calc_type,
                    otb_calc_type,
                    max_avg_counter,
                    avg_tolerance_pct,
                    dept_vat_incl_ind)
             values(I_dept_rec.dept_row.dept,
                    I_dept_rec.dept_row.dept_name,
                    I_dept_rec.dept_row.buyer,
                    I_dept_rec.dept_row.merch,
                    I_dept_rec.dept_row.profit_calc_type,
                    I_dept_rec.dept_row.purchase_type,
                    I_dept_rec.dept_row.group_no,
                    I_dept_rec.dept_row.bud_int,
                    I_dept_rec.dept_row.bud_mkup,
                    I_dept_rec.dept_row.total_market_amt,
                    I_dept_rec.dept_row.markup_calc_type,
                    I_dept_rec.dept_row.otb_calc_type,
                    I_dept_rec.dept_row.max_avg_counter,
                    I_dept_rec.dept_row.avg_tolerance_pct,
                    I_dept_rec.dept_row.dept_vat_incl_ind);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_DEPT;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_DEPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_dept_rec        IN       DEPT_REC)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.MODIFY_DEPT';

BEGIN
   if not LOCK_DEPS(O_error_message,
                    I_dept_rec.dept_row.dept) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'DEPS',
                    'dept: ' || I_dept_rec.dept_row.dept);
   update deps
      set dept_name         = NVL(I_dept_rec.dept_row.dept_name, dept_name),
          buyer             = I_dept_rec.dept_row.buyer,
          merch             = I_dept_rec.dept_row.merch,
          group_no          = NVL(I_dept_rec.dept_row.group_no, group_no),
          bud_int           = NVL(I_dept_rec.dept_row.bud_int, bud_int),
          bud_mkup          = NVL(I_dept_rec.dept_row.bud_mkup, bud_mkup),
          total_market_amt  = I_dept_rec.dept_row.total_market_amt,
          markup_calc_type  = NVL(I_dept_rec.dept_row.markup_calc_type, markup_calc_type),
          max_avg_counter   = NVL(I_dept_rec.dept_row.max_avg_counter, max_avg_counter),
          avg_tolerance_pct = NVL(I_dept_rec.dept_row.avg_tolerance_pct, avg_tolerance_pct),
          dept_vat_incl_ind = NVL(I_dept_rec.dept_row.dept_vat_incl_ind, dept_vat_incl_ind)
    where dept     = I_dept_rec.dept_row.dept;

   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_DEPT;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_DEPT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_dept            IN       DEPS.DEPT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)                   := 'MERCH_SQL.DELETE_DEPT';
   L_table        DAILY_PURGE.TABLE_NAME%TYPE    := 'DEPS';
   L_del_order    DAILY_PURGE.DELETE_ORDER%TYPE  := 3;

BEGIN

   if not DAILY_PURGE_SQL.INSERT_RECORD(O_error_message,
                                        I_dept,
                                        L_table,
                                        'D',
                                        L_del_order)then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DEPT;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.INSERT_CLASS';

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'CLASS',
                    'class: '||I_class_rec.class);
   insert into class(dept,
                     class,
                     class_name,
                     class_vat_ind)
              values(I_class_rec.dept,
                     I_class_rec.class,
                     I_class_rec.class_name,
                     I_class_rec.class_vat_ind);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_CLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.MODIFY_CLASS';

BEGIN
   if not LOCK_CLASS(O_error_message,
                     I_class_rec.class,
                     I_class_rec.dept) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'CLASS',
                    'class: ' || I_class_rec.class);
   update class
      set class_name    = I_class_rec.class_name,
          class_vat_ind = I_class_rec.class_vat_ind
    where class = I_class_rec.class
      and dept  = I_class_rec.dept;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_CLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_class_rec       IN       CLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)                   := 'MERCH_SQL.DELETE_CLASS';
   L_table        DAILY_PURGE.TABLE_NAME%TYPE    := 'CLASS';
   L_del_order    DAILY_PURGE.DELETE_ORDER%TYPE  := 2;
   L_key_value    DAILY_PURGE.KEY_VALUE%TYPE;

BEGIN

   L_key_value := substr(to_char(I_class_rec.dept, '0999'),2,4)|| ';' ||
                  substr(to_char(I_class_rec.class, '0999'),2,4);

   if not DAILY_PURGE_SQL.INSERT_RECORD(O_error_message,
                                        L_key_value,
                                        L_table,
                                        'D',
                                        L_del_order)then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_CLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'MERCH_SQL.INSERT_SUBCLASS';

BEGIN
   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'SUBCLASS',
                    'subclass: '||I_subclass_rec.subclass);
   insert into subclass(dept,
                        class,
                        subclass,
                        sub_name)
                 values(I_subclass_rec.dept,
                        I_subclass_rec.class,
                        I_subclass_rec.subclass,
                        I_subclass_rec.sub_name);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_SUBCLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION MODIFY_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec       IN    SUBCLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'MERCH_SQL.MODIFY_SUBCLASS';

BEGIN
   if not LOCK_SUBCLASS(O_error_message,
                        I_subclass_rec.dept,
                        I_subclass_rec.class,
                        I_subclass_rec.subclass) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'SUBCLASS',
                    'subclass: ' || I_subclass_rec.subclass);
   update subclass
      set sub_name = I_subclass_rec.sub_name
    where dept     = I_subclass_rec.dept and
          class    = I_subclass_rec.class and
          subclass = I_subclass_rec.subclass;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC');
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END MODIFY_SUBCLASS;
-------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_subclass_rec    IN       SUBCLASS%ROWTYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'MERCH_SQL.DELETE_SUBCLASS';
   L_table          DAILY_PURGE.TABLE_NAME%TYPE    := 'SUBCLASS';
   L_del_order      DAILY_PURGE.DELETE_ORDER%TYPE  := 1;
   L_key_value      DAILY_PURGE.KEY_VALUE%TYPE;

BEGIN

   L_key_value := substr(to_char(I_subclass_rec.dept, '0999'),2,4)|| ';' ||
                  substr(to_char(I_subclass_rec.class, '0999'),2,4)|| ';' ||
                  substr(to_char(I_subclass_rec.subclass, '0999'),2,4);

   if not DAILY_PURGE_SQL.INSERT_RECORD(O_error_message,
                                        L_key_value,
                                        L_table,
                                        'D',
                                        L_del_order)then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_SUBCLASS;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_COMPANY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_company_id      IN       COMPHEAD.COMPANY%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.LOCK_COMPANY';
   L_table        VARCHAR2(10) := 'COMPHEAD';

   cursor C_LOCK_COMPANY IS
      select 'x'
        from COMPHEAD
       where company = I_company_id
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_COMPANY',
                    'COMPANY',
                    'company: '||I_company_id);
   open C_LOCK_COMPANY;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_COMPANY',
                    'COMPANY',
                    'company: '||I_company_id);
   close C_LOCK_COMPANY;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'company ' || I_company_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_COMPANY;

-------------------------------------------------------------------------------------------------------
FUNCTION LOCK_DIVISION_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_division_id     IN       DIVISION.DIVISION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.LOCK_DIVISION_TL';
   L_table        VARCHAR2(50) := 'DIVISION_TL';

   cursor C_LOCK_DIVISION_TL IS
      select 'x'
        from DIVISION_TL
       where division = I_division_id
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_DIVISION_TL',
                    'DIVISION_TL',
                    'division id: '||I_division_id);
   open C_LOCK_DIVISION_TL;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_DIVISION_TL',
                    'DIVISION_TL',
                    'division id: '||I_division_id);
   close C_LOCK_DIVISION_TL;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'division ' || I_division_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_DIVISION_TL;
---------------------------------------------------------------------
FUNCTION LOCK_DIVISION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_division_id     IN       DIVISION.DIVISION%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'MERCH_SQL.LOCK_DIVISION';
   L_table        VARCHAR2(10) := 'DIVISION';

   cursor C_LOCK_DIVISION IS
      select 'x'
        from DIVISION
       where division = I_division_id
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_DIVISION',
                    'DIVISION',
                    'division id: '||I_division_id);
   open C_LOCK_DIVISION;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_DIVISION',
                    'DIVISION',
                    'division id: '||I_division_id);
   close C_LOCK_DIVISION;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'division ' || I_division_id,
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_DIVISION;
-------------------------------------------------------------------------------------------------------

FUNCTION LOCK_GROUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_group_no        IN       GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'MERCH_SQL.LOCK_GROUP_TL';
   L_table        VARCHAR2(50)  := 'GROUPS_TL';

   cursor C_LOCK_GROUP_TL IS
      select 'x'
        from groups_tl
       where group_no = I_group_no
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_GROUP_TL',
                    'GROUPS_TL',
                    'group_no: '||I_group_no);
   open C_LOCK_GROUP_TL;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_GROUP_TL',
                    'GROUPS_TL',
                    'group_no: '||I_group_no);
   close C_LOCK_GROUP_TL;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'group_no ' || I_group_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_GROUP_TL;
---------------------------------------------------------------------
FUNCTION LOCK_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_group_no        IN       GROUPS.GROUP_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'MERCH_SQL.LOCK_GROUP';
   L_table        VARCHAR2(10)  := 'GROUPS';

   cursor C_LOCK_GROUP IS
      select 'x'
        from groups
       where group_no = I_group_no
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_GROUP',
                    'GROUPS',
                    'group_no: '||I_group_no);
   open C_LOCK_GROUP;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_GROUP',
                    'GROUPS',
                    'group_no: '||I_group_no);
   close C_LOCK_GROUP;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'group_no ' || I_group_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_GROUP;
---------------------------------------------------------------------
FUNCTION LOCK_DEPS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_dept            IN       DEPS.DEPT%TYPE)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'MERCH_SQL.LOCK_DEPS';
   L_table        VARCHAR2(10)  := 'DEPS';

   cursor C_LOCK_DEPS IS
      select 'x'
        from deps
       where dept = I_dept
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_DEPS',
                    'DEPS',
                    'dept: '||I_dept);
   open C_LOCK_DEPS;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_DEPS',
                    'DEPS',
                    'dept: '||I_dept);
   close C_LOCK_DEPS;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'dept' || I_dept,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_DEPS;
-------------------------------------------------------------------------------------------------
FUNCTION LOCK_CLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_class           IN       CLASS.CLASS%TYPE,
                    I_dept            IN       CLASS.DEPT%TYPE)

   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'MERCH_SQL.LOCK_CLASS';
   L_table        VARCHAR2(10)  := 'CLASS';

   cursor C_LOCK_CLASS IS
      select 'x'
        from class
       where class = I_class
         and dept  = I_dept
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_CLASS',
                    'CLASS',
                    'class: '||I_class);
   open C_LOCK_CLASS;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_CLASS',
                    'CLASS',
                    'class: '||I_class);
   close C_LOCK_CLASS;
   ---
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'class ' || I_class,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_CLASS;
-------------------------------------------------------------------------------------------------
FUNCTION LOCK_SUBCLASS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_dept            IN       SUBCLASS.DEPT%TYPE,
                       I_class           IN       SUBCLASS.CLASS%TYPE,
                       I_subclass        IN       SUBCLASS.SUBCLASS%TYPE)

   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)  := 'MERCH_SQL.LOCK_SUBCLASS';
   L_table         VARCHAR2(10)  := 'SUBCLASS';

   cursor C_LOCK_SUBCLASS IS
      select 'x'
        from subclass
       where dept = I_dept
         and class  = I_class
         and subclass = I_subclass
         for update nowait;
BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_SUBCLASS',
                    'SUBCLASS',
                    'subclass: '||I_subclass);
   open C_LOCK_SUBCLASS;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_SUBCLASS',
                    'SUBCLASS',
                    'subclass: '||I_subclass);
   close C_LOCK_SUBCLASS;
   ---
   return TRUE;

   EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            'subclass ' || I_subclass,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END LOCK_SUBCLASS;

-------------------------------------------------------------------------------------------------
FUNCTION NEXT_DEPT_NUMBER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_dept_number     IN OUT   DEPS.DEPT%TYPE)
   RETURN BOOLEAN IS
   L_program                    VARCHAR2(50) := 'MERCH_SQL.NEXT_DEPT_NUMBER';   
   L_dept_sequence              DEPS.DEPT%TYPE;
   L_wrap_sequence_number   DEPS.DEPT%TYPE;
   L_first_time                 VARCHAR2(3)  := 'Yes';
   L_exists                     BOOLEAN;
   cursor C_NEXT_DEPT_SEQ is   
      SELECT dept_seq.NEXTVAL        
        FROM sys.dual;
BEGIN   
   LOOP    
     SQL_LIB.SET_MARK('OPEN', 'C_NEXT_DEPT_SEQ', 'dual', NULL);
     open C_NEXT_DEPT_SEQ;  
     SQL_LIB.SET_MARK('FETCH', 'C_NEXT_DEPT_SEQ', 'dual', NULL);        
     fetch C_NEXT_DEPT_SEQ into L_dept_sequence; 
     SQL_LIB.SET_MARK('CLOSE', 'C_NEXT_DEPT_SEQ', 'dual', NULL);
     close C_NEXT_DEPT_SEQ;    
     if L_first_time = 'Yes' then
        L_wrap_sequence_number := L_dept_sequence;
        L_first_time := 'No';
     elsif (L_dept_sequence = L_wrap_sequence_number) then
        O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                              NULL,
                                              NULL,
                                              NULL);         
        return FALSE;
     end if;      
     if DEPT_VALIDATE_SQL.EXIST(O_error_message,
                                L_dept_sequence,
                                L_exists) = FALSE then
        return FALSE;
     end if;      
     if L_exists = FALSE then        
        O_dept_number := L_dept_sequence;
        return TRUE;
     end if;   
   END LOOP; 
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END NEXT_DEPT_NUMBER;

-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION NEXT_SUBCLASS_NUMBER(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_subclass_number IN OUT  SUBCLASS.SUBCLASS%TYPE,
                              I_dept_number     IN      DEPS.DEPT%TYPE,
                              I_class_number    IN      CLASS.CLASS%TYPE)
   RETURN BOOLEAN IS
   L_program                VARCHAR2(50) := 'MERCH_SQL.NEXT_SUBCLASS_NUMBER';
   L_subclass_sequence      SUBCLASS.SUBCLASS%TYPE;
   L_wrap_sequence_number   SUBCLASS.SUBCLASS%TYPE;
   L_first_time             VARCHAR2(3)  := 'Yes';
   L_exists                 BOOLEAN;

   cursor C_NEXT_SUBCLASS_SEQ is
      select subclass_seq.NEXTVAL
        from sys.dual;
BEGIN

   if I_dept_number is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept_number',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   if I_class_number is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class_number',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   LOOP   
     SQL_LIB.SET_MARK('OPEN', 'C_NEXT_SUBCLASS_SEQ', 'dual', NULL);
     open C_NEXT_SUBCLASS_SEQ;  
     SQL_LIB.SET_MARK('FETCH', 'C_NEXT_SUBCLASS_SEQ', 'dual', NULL);        
     fetch C_NEXT_SUBCLASS_SEQ into L_subclass_sequence;
     SQL_LIB.SET_MARK('CLOSE', 'C_NEXT_SUBCLASS_SEQ', 'dual', NULL);
     close C_NEXT_SUBCLASS_SEQ;      
     if L_first_time = 'Yes' then
        L_wrap_sequence_number := L_subclass_sequence;
        L_first_time := 'No';
     elsif L_subclass_sequence = L_wrap_sequence_number then
        O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                               NULL,
                                               NULL,
                                               NULL);         
        return FALSE;
     end if;
     if SUBCLASS_VALIDATE_SQL.EXIST(O_error_message,                                     
                                    I_dept_number,
                                    I_class_number,
                                    L_subclass_sequence,
                                    L_exists) = FALSE then                           
        return FALSE;
     end if;
     if L_exists = FALSE then        
        O_subclass_number := L_subclass_sequence;
        return TRUE;
     end if;   
   END LOOP;  
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END NEXT_SUBCLASS_NUMBER;

-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
FUNCTION NEXT_CLASS_NUMBER(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_class_number    IN OUT  CLASS.CLASS%TYPE,
                           I_dept_number     IN      DEPS.DEPT%TYPE)
   RETURN BOOLEAN IS            
   L_program                VARCHAR2(50) := 'MERCH_SQL.NEXT_CLASS_NUMBER';
   L_class_sequence         CLASS.CLASS%TYPE;
   L_wrap_sequence_number   CLASS.CLASS%TYPE;
   L_first_time             VARCHAR2(3)  := 'Yes';
   L_exists                 BOOLEAN;

   cursor C_NEXT_CLASS_SEQ is
      select class_seq.NEXTVAL
        from sys.dual;
BEGIN

   if I_dept_number is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_dept_number',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   LOOP
     SQL_LIB.SET_MARK('OPEN', 'C_NEXT_CLASS_SEQ', 'dual', NULL);
     open C_NEXT_CLASS_SEQ;  
     SQL_LIB.SET_MARK('FETCH', 'C_NEXT_CLASS_SEQ', 'dual', NULL);
     fetch C_next_class_seq into L_class_sequence;
     SQL_LIB.SET_MARK('CLOSE', 'C_NEXT_CLASS_SEQ', 'dual', NULL);
     close C_NEXT_CLASS_SEQ;
     if L_first_time = 'Yes' then
        L_wrap_sequence_number := L_class_sequence;
        L_first_time := 'No';
     elsif L_class_sequence = L_wrap_sequence_number then
        O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',
                                               NULL,
                                               NULL,
                                               NULL);        
        return FALSE;
     end if;      
     if CLASS_VALIDATE_SQL.EXIST(O_error_message,                                  
                                 I_dept_number,
                                 L_class_sequence,
                                 L_exists) = FALSE then
        return FALSE;
     end if;     
     if L_exists = FALSE then        
        O_class_number := L_class_sequence;
        return TRUE;
     end if;   
   END LOOP;   

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END NEXT_CLASS_NUMBER;

-------------------------------------------------------------------------------------------------
FUNCTION DELETE_DIVISION_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_division        IN       DIVISION_TL.DIVISION%TYPE)
   RETURN BOOLEAN IS  

   L_program      VARCHAR2(60) := 'MERCH_SQL.DELETE_DIVISION_TL';
  

BEGIN

 if I_division is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_division',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---

   if not LOCK_DIVISION_TL(O_error_message,
                        I_division) then
      return FALSE;
   end if;

   

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'DIVISION_TL',
                    'division id: '||I_division);
   delete from division_tl
    where division = I_division;

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_DIVISION_TL;

-------------------------------------------------------------------------------------------------
FUNCTION DELETE_GROUP_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_group_no           IN       GROUPS_TL.GROUP_NO%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(60) := 'MERCH_SQL.DELETE_GROUP_TL';
   

BEGIN
 
   if not LOCK_GROUP_TL(O_error_message,
                     I_group_no) then
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'GROUPS_TL',
                    'group no: '||I_group_no);
   delete from groups_tl
    where group_no = I_group_no;
    
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_GROUP_TL;
-------------------------------------------------------------------------------------------------
FUNCTION DELETE_SUBCLASS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_dept            IN       SUBCLASS_TL.DEPT%TYPE,
                            I_class           IN       SUBCLASS_TL.CLASS%TYPE,
                            I_subclass        IN       SUBCLASS_TL.SUBCLASS%TYPE )
   RETURN BOOLEAN IS


   L_program       VARCHAR2(60) := 'MERCH_SQL.DELETE_SUBCLASS_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SUBCLASS_TL is
        select 'x'
        from subclass_tl 
       where dept=I_dept
       and class = I_class
       and subclass = I_subclass
         for update nowait;
BEGIN

   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
    if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_class',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
    if I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_subclass',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'SUBCLASS_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_SUBCLASS_TL',
                    L_table,
                    'I_subclass '||I_subclass||' I_class '||I_class||' I_dept '||I_dept);
   open C_LOCK_SUBCLASS_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_SUBCLASS_TL',
                    L_table,
                    'I_subclass '||I_subclass||' I_class '||I_class||' I_dept '||I_dept);
   close C_LOCK_SUBCLASS_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'SUBCLASS_TL',
                    'I_subclass '||I_subclass||' I_class '||I_class||' I_dept '||I_dept);
                    
   delete from SUBCLASS_TL
    where subclass = I_subclass 
    and dept = I_dept
    and class = I_class;
    
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_SUBCLASS_TL;

-------------------------------------------------------------------------------------------------
FUNCTION DELETE_CLASS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_dept            IN CLASS_TL.DEPT%TYPE,
                         I_class           IN  CLASS_TL.CLASS%TYPE)
RETURN BOOLEAN IS


   L_program       VARCHAR2(60) := 'MERCH_SQL.DELETE_CLASS_TL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_CLASS_TL is
      select 'x'
        from class_tl 
        where dept=I_dept
        and class = I_class
        for update nowait;
BEGIN

   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_dept',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   
    if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_class',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   L_table := 'CLASS_TL';
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOCK_CLASS_TL',
                    L_table,
                    'I_class '||I_class||' I_dept '||I_dept);
   open C_LOCK_CLASS_TL;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOCK_CLASS_TL',
                    L_table,
                    'I_class '||I_class||' I_dept '||I_dept);
   close C_LOCK_CLASS_TL;
   ---

   SQL_LIB.SET_MARK('DELETE',
                    NULL,
                    'CLASS_TL',
                    'I_class '||I_class||' I_dept '||I_dept);
                    
   delete from class_tl
    where dept = I_dept 
    and class = I_class;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END DELETE_CLASS_TL;
-------------------------------------------------------------------------------------------------
END MERCH_SQL;
/
