-- File Name : CORESVC_POS_TENDER_TYPE_HEAD_body.pls
CREATE OR REPLACE PACKAGE BODY CORESVC_POS_TENDER_TYPE_HEAD as
   cursor C_SVC_POS_TT_HEAD(I_process_id NUMBER,
                            I_chunk_id   NUMBER) is
      select pk_pos_tender_type_head.rowid             as pk_pos_tender_type_head_rid,
             st.rowid                                  as st_rid,
             pk_pos_tender_type_head.tender_type_group as base_tender_type_group,
             pk_pos_tender_type_head.effective_date    as base_effective_date,
             st.tender_type_id,
             st.effective_date,
             UPPER(st.tender_type_group)               as tender_type_group,
             st.tender_type_desc,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)                          as action,
             st.process$status
        from svc_pos_tender_type_head  st,
             pos_tender_type_head      pk_pos_tender_type_head
       where st.process_id     = I_process_id
         and st.chunk_id       = I_chunk_id
         and st.tender_type_id = pk_pos_tender_type_head.tender_type_id (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab      errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab  s9t_errors_tab_typ;

   Type POS_TENDER_TYPE_HEAD_TAB IS TABLE OF POS_TENDER_TYPE_HEAD_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
-------------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
end GET_SHEET_NAME_TRANS;
----------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
    Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-----------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
------------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets            s9t_pkg.names_map_typ;
   POS_TT_HEAD_cols    s9t_pkg.names_map_typ;
   POS_TT_HEAD_TL_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                      := s9t_pkg.get_sheet_names(I_file_id);
   POS_TT_HEAD_cols              := s9t_pkg.get_col_names(I_file_id,POS_TT_HEAD_sheet);
   POS_TT_HEAD$Action            := POS_TT_HEAD_cols('ACTION');
   POS_TT_HEAD$TENDER_TYPE_ID    := POS_TT_HEAD_cols('TENDER_TYPE_ID');
   POS_TT_HEAD$EFFECTIVE_DATE    := POS_TT_HEAD_cols('EFFECTIVE_DATE');
   POS_TT_HEAD$TENDER_TYPE_GROUP := POS_TT_HEAD_cols('TENDER_TYPE_GROUP');
   POS_TT_HEAD$TENDER_TYPE_DESC  := POS_TT_HEAD_cols('TENDER_TYPE_DESC');

   POS_TT_HEAD_TL_cols           := s9t_pkg.get_col_names(I_file_id,POS_TT_HEAD_TL_sheet);
   POS_TT_HEAD_TL$Action         := POS_TT_HEAD_TL_cols('ACTION');
   POS_TT_HEAD_TL$LANG           := POS_TT_HEAD_TL_cols('LANG');
   POS_TT_HEAD_Tl$TTYPE_ID       := POS_TT_HEAD_TL_cols('TENDER_TYPE_ID');
   POS_TT_HEAD_Tl$TTYPE_DESC     := POS_TT_HEAD_TL_cols('TENDER_TYPE_DESC');
END POPULATE_NAMES;
-----------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_POS_TT_HEAD( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = POS_TT_HEAD_sheet )
   select s9t_row(s9t_cells(CORESVC_POS_TENDER_TYPE_HEAD.action_mod,
                            tender_type_id,
                            tender_type_desc,
                            tender_type_group,
                            effective_date ))
     from pos_tender_type_head;
END POPULATE_POS_TT_HEAD;
-----------------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_POS_TT_HEAD_TL( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = POS_TT_HEAD_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_POS_TENDER_TYPE_HEAD.action_mod,
                            lang,
                            tender_type_id,
                            tender_type_desc))
     from pos_tender_type_head_tl;
END POPULATE_POS_TT_HEAD_TL;
-----------------------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file       s9t_file;
   L_file_name  s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(POS_TT_HEAD_sheet);
   L_file.sheets(L_file.get_sheet_index(POS_TT_HEAD_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                        ,'TENDER_TYPE_ID'
                                                                                        ,'TENDER_TYPE_DESC'
                                                                                        ,'TENDER_TYPE_GROUP'
                                                                                        ,'EFFECTIVE_DATE' );

   L_file.add_sheet(POS_TT_HEAD_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(POS_TT_HEAD_TL_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'LANG'
                                                                                            ,'TENDER_TYPE_ID'
                                                                                            ,'TENDER_TYPE_DESC');
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
------------------------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT s9t_folder.file_id%TYPE,
                     I_template_only_ind IN     CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_program  VARCHAR2(64):='CORESVC_POS_TENDER_TYPE_HEAD.CREATE_S9T';
   L_file     s9t_file;
BEGIN

   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE   then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_POS_TT_HEAD(O_file_id);
      POPULATE_POS_TT_HEAD_TL(O_file_id);
      COMMIT;
   end if;


   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-----------------------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_POS_TT_HEAD(I_file_id    IN s9t_folder.file_id%TYPE,
                                  I_process_id IN SVC_POS_TENDER_TYPE_HEAD.process_id%TYPE) IS
   TYPE svc_POS_TT_HEAD_col_typ IS TABLE OF SVC_POS_TENDER_TYPE_HEAD%ROWTYPE;
   L_temp_rec           SVC_POS_TENDER_TYPE_HEAD%ROWTYPE;
   L_process_id         SVC_POS_TENDER_TYPE_HEAD.process_id%TYPE;
   L_error              BOOLEAN                 := FALSE;
   svc_POS_TT_HEAD_col  svc_POS_TT_HEAD_col_typ := NEW svc_POS_TT_HEAD_col_typ();
   L_default_rec        SVC_POS_TENDER_TYPE_HEAD%ROWTYPE;
   cursor C_MANDATORY_IND is
      select TENDER_TYPE_ID_mi,
             EFFECTIVE_DATE_mi,
             TENDER_TYPE_GROUP_mi,
             TENDER_TYPE_DESC_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key        = template_key
                 and wksht_key           = 'POS_TENDER_TYPE_HEAD' )
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('TENDER_TYPE_ID'    as TENDER_TYPE_ID,
                                                                'EFFECTIVE_DATE'    as EFFECTIVE_DATE,
                                                                'TENDER_TYPE_GROUP' as TENDER_TYPE_GROUP,
                                                                'TENDER_TYPE_DESC'  as TENDER_TYPE_DESC,
                                                                 NULL               as dummy));
   L_mi_rec    c_mandatory_ind%ROWTYPE;
   dml_errors  EXCEPTION;
   PRAGMA      exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_POS_TENDER_TYPE_HEAD';
   L_pk_columns    VARCHAR2(255)  := 'Tender Type ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   -- Get default values.
   FOR rec IN (select TENDER_TYPE_ID_dv,
                      EFFECTIVE_DATE_dv,
                      TENDER_TYPE_GROUP_dv,
                      TENDER_TYPE_DESC_dv,
                      NULL  as  dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key       = template_key
                          and wksht_key          = 'POS_TENDER_TYPE_HEAD')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('TENDER_TYPE_ID'    as TENDER_TYPE_ID,
                                                                             'EFFECTIVE_DATE'    as EFFECTIVE_DATE,
                                                                             'TENDER_TYPE_GROUP' as TENDER_TYPE_GROUP,
                                                                             'TENDER_TYPE_DESC'  as TENDER_TYPE_DESC,
                                                                              NULL               as dummy)))
   LOOP
      BEGIN
         L_default_rec.TENDER_TYPE_ID := rec.TENDER_TYPE_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'POS_TENDER_TYPE_HEAD',
                             NULL,
                            'TENDER_TYPE_ID',
                             NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.EFFECTIVE_DATE := rec.EFFECTIVE_DATE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'POS_TENDER_TYPE_HEAD',
                             NULL,
                            'EFFECTIVE_DATE',
                             NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.TENDER_TYPE_GROUP := rec.TENDER_TYPE_GROUP_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'POS_TENDER_TYPE_HEAD',
                             NULL,
                            'TENDER_TYPE_GROUP',
                             NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.TENDER_TYPE_DESC := rec.TENDER_TYPE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'POS_TENDER_TYPE_HEAD',
                             NULL,
                            'TENDER_TYPE_DESC',
                             NULL,
                            'INV_DEFAULT');
      END;

   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(POS_TT_HEAD$Action)            as Action,
          r.get_cell(POS_TT_HEAD$TENDER_TYPE_ID)    as TENDER_TYPE_ID,
          r.get_cell(POS_TT_HEAD$EFFECTIVE_DATE)    as EFFECTIVE_DATE,
          UPPER(r.get_cell(POS_TT_HEAD$TENDER_TYPE_GROUP)) as TENDER_TYPE_GROUP,
          r.get_cell(POS_TT_HEAD$TENDER_TYPE_DESC)  as TENDER_TYPE_DESC,
          r.get_row_seq()                           as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id    = I_file_id
       and ss.sheet_name = sheet_name_trans(POS_TT_HEAD_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error                      := FALSE;

      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            POS_TT_HEAD_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.TENDER_TYPE_ID := rec.TENDER_TYPE_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             POS_TT_HEAD_sheet,
                             rec.row_seq,
                            'TENDER_TYPE_ID',
                             SQLCODE,
                             SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.EFFECTIVE_DATE := rec.EFFECTIVE_DATE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             POS_TT_HEAD_sheet,
                             rec.row_seq,
                            'EFFECTIVE_DATE',
                             SQLCODE,
                             SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.TENDER_TYPE_GROUP := rec.TENDER_TYPE_GROUP;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             POS_TT_HEAD_sheet,
                             rec.row_seq,
                            'TENDER_TYPE_GROUP',
                             SQLCODE,
                             SQLERRM);
            L_error := TRUE;
      END;

      BEGIN
         L_temp_rec.TENDER_TYPE_DESC := rec.TENDER_TYPE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                             POS_TT_HEAD_sheet,
                             rec.row_seq,
                            'TENDER_TYPE_DESC',
                             SQLCODE,
                             SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_POS_TENDER_TYPE_HEAD.action_new then
         L_temp_rec.TENDER_TYPE_ID    := NVL( L_temp_rec.TENDER_TYPE_ID,
                                              L_default_rec.TENDER_TYPE_ID);
         L_temp_rec.EFFECTIVE_DATE    := NVL( L_temp_rec.EFFECTIVE_DATE,
                                              L_default_rec.EFFECTIVE_DATE);
         L_temp_rec.TENDER_TYPE_GROUP := NVL( L_temp_rec.TENDER_TYPE_GROUP,
                                              L_default_rec.TENDER_TYPE_GROUP);
         L_temp_rec.TENDER_TYPE_DESC  := NVL( L_temp_rec.TENDER_TYPE_DESC,
                                              L_default_rec.TENDER_TYPE_DESC);
      end if;

      if L_temp_rec.ACTION IN (action_mod, action_del)
         and L_temp_rec.TENDER_TYPE_ID is NULL   then
         WRITE_S9T_ERROR( I_file_id,
                          POS_TT_HEAD_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_POS_TT_HEAD_col.extend();
         svc_POS_TT_HEAD_col(svc_POS_TT_HEAD_col.COUNT()):=L_temp_rec;
      end if;

   END LOOP;
   BEGIN
      forall i IN 1..svc_POS_TT_HEAD_col.COUNT SAVE EXCEPTIONS
      merge into SVC_POS_TENDER_TYPE_HEAD st
      using(select
                  (case
                      when l_mi_rec.TENDER_TYPE_ID_mi     = 'N'  and
                           svc_POS_TT_HEAD_col(i).action  = CORESVC_POS_TENDER_TYPE_HEAD.action_mod  and
                           s1.TENDER_TYPE_ID IS NULL   then
                           mt.TENDER_TYPE_ID
                      else s1.TENDER_TYPE_ID
                      end) as TENDER_TYPE_ID,
                  (case
                      when l_mi_rec.EFFECTIVE_DATE_mi     = 'N'  and
                           svc_POS_TT_HEAD_col(i).action  = CORESVC_POS_TENDER_TYPE_HEAD.action_mod  and
                           s1.EFFECTIVE_DATE IS NULL   then
                           mt.EFFECTIVE_DATE
                      else s1.EFFECTIVE_DATE
                      end) as EFFECTIVE_DATE,
                  (case
                      when l_mi_rec.TENDER_TYPE_GROUP_mi  = 'N' and
                           svc_POS_TT_HEAD_col(i).action  = CORESVC_POS_TENDER_TYPE_HEAD.action_mod and
                           s1.TENDER_TYPE_GROUP IS NULL   then
                           mt.TENDER_TYPE_GROUP
                      else s1.TENDER_TYPE_GROUP
                      end) as TENDER_TYPE_GROUP,
                  (case
                      when l_mi_rec.TENDER_TYPE_DESC_mi   = 'N' and
                           svc_POS_TT_HEAD_col(i).action  = CORESVC_POS_TENDER_TYPE_HEAD.action_mod and
                           s1.TENDER_TYPE_DESC IS NULL   then
                           mt.TENDER_TYPE_DESC
                      else s1.TENDER_TYPE_DESC
                      end) as TENDER_TYPE_DESC,
                      null as dummy
              from (select svc_POS_TT_HEAD_col(i).TENDER_TYPE_ID    as TENDER_TYPE_ID,
                           svc_POS_TT_HEAD_col(i).EFFECTIVE_DATE    as EFFECTIVE_DATE,
                           svc_POS_TT_HEAD_col(i).TENDER_TYPE_GROUP as TENDER_TYPE_GROUP,
                           svc_POS_TT_HEAD_col(i).TENDER_TYPE_DESC  as TENDER_TYPE_DESC,
                                                               NULL as dummy
                      from dual )               s1,
                           POS_TENDER_TYPE_HEAD mt
             where mt.TENDER_TYPE_ID (+) = s1.TENDER_TYPE_ID )sq
                on (st.TENDER_TYPE_ID    = sq.TENDER_TYPE_ID and
                    svc_POS_TT_HEAD_col(i).ACTION IN (CORESVC_POS_TENDER_TYPE_HEAD.action_mod,CORESVC_POS_TENDER_TYPE_HEAD.action_del))
      when matched then
      update
         set process_id        = svc_POS_TT_HEAD_col(i).process_id ,
             chunk_id          = svc_POS_TT_HEAD_col(i).chunk_id ,
             row_seq           = svc_POS_TT_HEAD_col(i).row_seq ,
             action            = svc_POS_TT_HEAD_col(i).action ,
             process$status    = svc_POS_TT_HEAD_col(i).process$status ,
             tender_type_group = sq.tender_type_group ,
             tender_type_desc  = sq.tender_type_desc ,
             effective_date    = sq.effective_date
      when NOT matched then
         insert (process_id,
                 chunk_id ,
                 row_seq ,
                 action ,
                 process$status ,
                 tender_type_id ,
                 effective_date ,
                 tender_type_group ,
                 tender_type_desc )
         values (svc_POS_TT_HEAD_col(i).process_id ,
                 svc_POS_TT_HEAD_col(i).chunk_id ,
                 svc_POS_TT_HEAD_col(i).row_seq ,
                 svc_POS_TT_HEAD_col(i).action ,
                 svc_POS_TT_HEAD_col(i).process$status ,
                 sq.tender_type_id ,
                 sq.effective_date ,
                 sq.tender_type_group ,
                 sq.tender_type_desc );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                          POS_TT_HEAD_sheet,
                          svc_POS_TT_HEAD_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_POS_TT_HEAD;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_POS_TT_HEAD_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id   IN   SVC_POS_TENDER_TYPE_HEAD_TL.PROCESS_ID%TYPE) IS

   TYPE svc_pos_tt_head_tl_col_TYP IS TABLE OF SVC_POS_TENDER_TYPE_HEAD_TL%ROWTYPE;
   L_temp_rec               SVC_POS_TENDER_TYPE_HEAD_TL%ROWTYPE;
   svc_pos_tt_head_tl_col   svc_pos_tt_head_tl_col_TYP := NEW svc_pos_tt_head_tl_col_TYP();
   L_process_id             SVC_POS_TENDER_TYPE_HEAD_TL.PROCESS_ID%TYPE;
   L_error                  BOOLEAN := FALSE;
   L_default_rec            SVC_POS_TENDER_TYPE_HEAD_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select tender_type_desc_mi,
             tender_type_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'POS_TENDER_TYPE_HEAD_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('TENDER_TYPE_DESC' as tender_type_desc,
                                       'TENDER_TYPE_ID'   as tender_type_id,
                                       'LANG'             as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_POS_TENDER_TYPE_HEAD_TL';
   L_pk_columns    VARCHAR2(255)  := 'Tender Type ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select tender_type_desc_dv,
                      tender_type_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'POS_TENDER_TYPE_HEAD_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('TENDER_TYPE_DESC' as tender_type_desc,
                                                'TENDER_TYPE_ID'   as tender_type_id,
                                                'LANG'             as lang)))
   LOOP
      BEGIN
         L_default_rec.tender_type_desc := rec.tender_type_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            POS_TT_HEAD_TL_SHEET ,
                            NULL,
                           'TENDER_TYPE_DESC' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tender_type_id := rec.tender_type_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            POS_TT_HEAD_TL_SHEET ,
                            NULL,
                           'TENDER_TYPE_ID' ,
                            NULL,
                           'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            POS_TT_HEAD_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(pos_tt_head_tl$action)) as action,
                      r.get_cell(pos_tt_head_tl$ttype_desc)    as tender_type_desc,
                      r.get_cell(pos_tt_head_tl$ttype_id)      as tender_type_id,
                      r.get_cell(pos_tt_head_tl$lang)          as lang,
                      r.get_row_seq()                          as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(POS_TT_HEAD_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            POS_TT_HEAD_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tender_type_desc := rec.tender_type_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            POS_TT_HEAD_TL_SHEET,
                            rec.row_seq,
                            'TENDER_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tender_type_id := rec.tender_type_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            POS_TT_HEAD_TL_SHEET,
                            rec.row_seq,
                            'TENDER_TYPE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            POS_TT_HEAD_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_POS_TENDER_TYPE_HEAD.action_new then
         L_temp_rec.tender_type_desc := NVL( L_temp_rec.tender_type_desc,L_default_rec.tender_type_desc);
         L_temp_rec.tender_type_id   := NVL( L_temp_rec.tender_type_id,L_default_rec.tender_type_id);
         L_temp_rec.lang             := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.tender_type_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         POS_TT_HEAD_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_pos_tt_head_tl_col.extend();
         svc_pos_tt_head_tl_col(svc_pos_tt_head_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_pos_tt_head_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_pos_tender_type_head_TL st
      using(select
                  (case
                   when l_mi_rec.tender_type_desc_mi = 'N'
                    and svc_pos_tt_head_tl_col(i).action = CORESVC_POS_TENDER_TYPE_HEAD.action_mod
                    and s1.tender_type_desc IS NULL then
                        mt.tender_type_desc
                   else s1.tender_type_desc
                   end) as tender_type_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_pos_tt_head_tl_col(i).action = CORESVC_POS_TENDER_TYPE_HEAD.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.tender_type_id_mi = 'N'
                    and svc_pos_tt_head_tl_col(i).action = CORESVC_POS_TENDER_TYPE_HEAD.action_mod
                    and s1.tender_type_id IS NULL then
                        mt.tender_type_id
                   else s1.tender_type_id
                   end) as tender_type_id
              from (select svc_pos_tt_head_tl_col(i).tender_type_desc as tender_type_desc,
                           svc_pos_tt_head_tl_col(i).tender_type_id        as tender_type_id,
                           svc_pos_tt_head_tl_col(i).lang              as lang
                      from dual) s1,
                   pos_tender_type_head_tl mt
             where mt.tender_type_id (+) = s1.tender_type_id
               and mt.lang (+)       = s1.lang) sq
                on (st.tender_type_id = sq.tender_type_id and
                    st.lang = sq.lang and
                    svc_pos_tt_head_tl_col(i).ACTION IN (CORESVC_POS_TENDER_TYPE_HEAD.action_mod,CORESVC_POS_TENDER_TYPE_HEAD.action_del))
      when matched then
      update
         set process_id        = svc_pos_tt_head_tl_col(i).process_id ,
             chunk_id          = svc_pos_tt_head_tl_col(i).chunk_id ,
             row_seq           = svc_pos_tt_head_tl_col(i).row_seq ,
             action            = svc_pos_tt_head_tl_col(i).action ,
             process$status    = svc_pos_tt_head_tl_col(i).process$status ,
             tender_type_desc = sq.tender_type_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             tender_type_desc ,
             tender_type_id ,
             lang)
      values(svc_pos_tt_head_tl_col(i).process_id ,
             svc_pos_tt_head_tl_col(i).chunk_id ,
             svc_pos_tt_head_tl_col(i).row_seq ,
             svc_pos_tt_head_tl_col(i).action ,
             svc_pos_tt_head_tl_col(i).process$status ,
             sq.tender_type_desc ,
             sq.tender_type_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            POS_TT_HEAD_TL_SHEET,
                            svc_pos_tt_head_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_POS_TT_HEAD_TL;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count   IN OUT NUMBER,
                      I_file_id       IN     s9t_folder.file_id%TYPE,
                      I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_POS_TENDER_TYPE_HEAD.PROCESS_S9T';
   L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR         EXCEPTION;
   PRAGMA           EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE   then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_POS_TT_HEAD(I_file_id,
                              I_process_id);
      PROCESS_S9T_POS_TT_HEAD_TL(I_file_id,
                                 I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
-------------------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
     from svc_pos_tender_type_head_tl
    where process_id = I_process_id;

   delete
     from svc_pos_tender_type_head
    where process_id = I_process_id;
END;
-------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_POS_TT_HEAD_VAL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error         IN OUT BOOLEAN,
                                 I_rec           IN     C_SVC_POS_TT_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                        :='CORESVC_POS_TENDER_TYPE_HEAD.PROCESS_POS_TT_HEAD_VAL';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   :='SVC_POS_TENDER_TYPE_HEAD';
   L_pos_config_status  VARCHAR2(1)                         := NULL;
   L_exists             VARCHAR2(50)                        := NULL;
   L_code_desc          VARCHAR2(50);
   L_lock_ind           BOOLEAN;


BEGIN

   if I_rec.action=action_new
      or (I_rec.action=action_mod)   then
      if I_rec.TENDER_TYPE_GROUP is NULL   then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'TENDER_TYPE_GROUP',
                     'ENTER_TENDER_TYPE_GROUP');
         O_error :=TRUE;
      elsif I_rec.TENDER_TYPE_GROUP NOT IN ('CASH',
                                            'CHECK',
                                            'CCARD',
                                            'COUPON',
                                            'LOTTRY',
                                            'FSTAMP',
                                            'DCARD',
                                            'VOUCH',
                                            'MORDER',
                                            'ERR',
                                            'SOCASS',
                                            'TERM',
                                            'EBS',
                                            'DRIVEO',
                                            'FONCOT',
                                            'PAYPAL',
                                            'OTHERS')   then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'TENDER_TYPE_GROUP',
                     'INV_TENDER_TYPE_GROUP');
         O_error :=TRUE;
      end if;
   end if;

   --Checking if the record to be edited is locked or not.
   if I_rec.action = action_del   then
      if POS_CONFIG_LOCK_SQL.LOCK_POS_TENDER_TYPE_HEAD(O_error_message,
                                                       L_lock_ind,
                                                       I_rec.tender_type_id) = FALSE   then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'TENDER_TYPE_ID',
                      O_error_message);
         O_error :=TRUE;
      elsif L_lock_ind = FALSE   then  --Record is locked
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                         'TENDER_TYPE_ID',
                         O_error_message);
            O_error :=TRUE;
      end if;
   end if;
-- Checking if the group_type field is updated or not: It should not be updated.
   if (I_rec.action=action_mod)
      and I_rec.PK_POS_TENDER_TYPE_HEAD_rid is NOT NULL
      and I_rec.tender_type_group <> I_rec.base_tender_type_group   then
      WRITE_ERROR( I_rec.process_id,
                   SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                   I_rec.chunk_id,
                   L_table,
                   I_rec.row_seq,
                  'TENDER_TYPE_GROUP',
                  'CANT_UPD_GRP_TYP');
      O_error :=TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_POS_TT_HEAD_VAL;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_POS_TT_HEAD_INS( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_pos_tt_head_temp_rec IN     POS_TENDER_TYPE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) :='CORESVC_POS_TENDER_TYPE_HEAD.EXEC_POS_TT_HEAD_INS';
BEGIN

   insert into pos_tender_type_head
        values I_pos_tt_head_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_POS_TT_HEAD_INS;
-----------------------------------------------------------------------------------------------------
FUNCTION EXEC_POS_TT_HEAD_UPD( O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_pos_tt_head_temp_rec IN     POS_TENDER_TYPE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) :='CORESVC_POS_TENDER_TYPE_HEAD.EXEC_POS_TT_HEAD_UPD';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);
   cursor C_POS_TT_HEAD_LOCK is
      select 'X'
        from POS_TENDER_TYPE_HEAD
       where tender_type_id = I_pos_tt_head_temp_rec.tender_type_id
       for update nowait;
BEGIN

   open C_POS_TT_HEAD_LOCK;
   close C_POS_TT_HEAD_LOCK;

   update pos_tender_type_head
      set row = I_pos_tt_head_temp_rec
    where tender_type_id = I_pos_tt_head_temp_rec.tender_type_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'POS_TENDER_TYPE_HEAD',
                                                                I_pos_tt_head_temp_rec.tender_type_id,
                                                                NULL);

      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_POS_TT_HEAD_LOCK%ISOPEN   then
         close C_POS_TT_HEAD_LOCK;
      end if;
      return FALSE;
END EXEC_POS_TT_HEAD_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_POS_TT_HEAD_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_pos_tt_head_temp_rec IN       POS_TENDER_TYPE_HEAD%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64) :='CORESVC_POS_TENDER_TYPE_HEAD.EXEC_POS_TT_HEAD_DEL';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);
   
   cursor C_POS_TT_HEAD_LOCK is
      select 'X'
        from POS_TENDER_TYPE_HEAD
       where tender_type_id = I_pos_tt_head_temp_rec.tender_type_id
       for update nowait;

   cursor C_POS_TT_HEAD_TL_DEL_LOCK is
      select 'x'
        from pos_tender_type_head_tl
       where tender_type_id = I_pos_tt_head_temp_rec.tender_type_id
         for update nowait; 
 
   cursor C_SVC_POS_TT_HEAD_TL_DEL_LOCK is
      select 'x'
        from svc_pos_tender_type_head_tl
       where tender_type_id = I_pos_tt_head_temp_rec.tender_type_id
         for update nowait; 
BEGIN
   open  C_POS_TT_HEAD_TL_DEL_LOCK;
   close C_POS_TT_HEAD_TL_DEL_LOCK;
   
   delete
     from pos_tender_type_head_tl
    where tender_type_id = I_pos_tt_head_temp_rec.tender_type_id;

   open C_POS_TT_HEAD_LOCK;
   close C_POS_TT_HEAD_LOCK;

   delete pos_tender_type_head
   where tender_type_id = I_pos_tt_head_temp_rec.tender_type_id;
   
   open C_SVC_POS_TT_HEAD_TL_DEL_LOCK;
   close C_SVC_POS_TT_HEAD_TL_DEL_LOCK;

   delete svc_pos_tender_type_head_tl
   where tender_type_id = I_pos_tt_head_temp_rec.tender_type_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'POS_TENDER_TYPE_HEAD',
                                                                I_pos_tt_head_temp_rec.tender_type_id,
                                                                NULL);

      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_POS_TT_HEAD_LOCK%ISOPEN   then
         close C_POS_TT_HEAD_LOCK;
      end if;
      return FALSE;
END EXEC_POS_TT_HEAD_DEL;
----------------------------------------------------------------------------------------------------------
FUNCTION EXEC_POS_TT_HEAD_TL_INS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_pos_tt_head_tl_ins_tab    IN       POS_TENDER_TYPE_HEAD_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_POS_TENDER_TYPE_HEAD.EXEC_POS_TT_HEAD_TL_INS';

BEGIN
   if I_pos_tt_head_tl_ins_tab is NOT NULL and I_pos_tt_head_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_pos_tt_head_tl_ins_tab.COUNT()
         insert into pos_tender_type_head_TL
              values I_pos_tt_head_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_POS_TT_HEAD_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_POS_TT_HEAD_TL_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_pos_tt_head_tl_upd_tab   IN       POS_TENDER_TYPE_HEAD_TAB,
                                 I_pos_tt_head_tl_upd_rst   IN       ROW_SEQ_TAB,
                                 I_process_id               IN       SVC_POS_TENDER_TYPE_HEAD_TL.PROCESS_ID%TYPE,
                                 I_chunk_id                 IN       SVC_POS_TENDER_TYPE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_POS_TENDER_TYPE_HEAD.EXEC_POS_TT_HEAD_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'POS_TENDER_TYPE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_POS_TT_HEAD_TL_UPD(I_pos_tender_type_id    POS_TENDER_TYPE_HEAD_TL.TENDER_TYPE_ID%TYPE,
                                    I_lang                  POS_TENDER_TYPE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from pos_tender_type_head_tl
       where tender_type_id = I_pos_tender_type_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_pos_tt_head_tl_upd_tab is NOT NULL and I_pos_tt_head_tl_upd_tab.count > 0 then
      for i in I_pos_tt_head_tl_upd_tab.FIRST..I_pos_tt_head_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_pos_tt_head_tl_upd_tab(i).lang);
            L_key_val2 := 'Tender Type ID: '||to_char(I_pos_tt_head_tl_upd_tab(i).tender_type_id);
            open C_LOCK_POS_TT_HEAD_TL_UPD(I_pos_tt_head_tl_upd_tab(i).tender_type_id,
                                           I_pos_tt_head_tl_upd_tab(i).lang);
            close C_LOCK_POS_TT_HEAD_TL_UPD;
            update pos_tender_type_head_tl
               set tender_type_desc = I_pos_tt_head_tl_upd_tab(i).tender_type_desc,
                   last_update_id = I_pos_tt_head_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_pos_tt_head_tl_upd_tab(i).last_update_datetime
             where lang = I_pos_tt_head_tl_upd_tab(i).lang
               and tender_type_id = I_pos_tt_head_tl_upd_tab(i).tender_type_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_POS_TENDER_TYPE_HEAD_TL',
                           I_pos_tt_head_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_POS_TT_HEAD_TL_UPD%ISOPEN then
         close C_LOCK_POS_TT_HEAD_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_POS_TT_HEAD_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_POS_TT_HEAD_TL_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_pos_tt_head_tl_del_tab   IN       POS_TENDER_TYPE_HEAD_TAB,
                                 I_pos_tt_head_tl_del_rst   IN       ROW_SEQ_TAB,
                                 I_process_id               IN       SVC_POS_TENDER_TYPE_HEAD_TL.PROCESS_ID%TYPE,
                                 I_chunk_id                 IN       SVC_POS_TENDER_TYPE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_POS_TENDER_TYPE_HEAD.EXEC_POS_TT_HEAD_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'POS_TENDER_TYPE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_POS_TT_HEAD_TL_DEL(I_pos_tender_type_id  POS_TENDER_TYPE_HEAD_TL.TENDER_TYPE_ID%TYPE,
                                    I_lang                POS_TENDER_TYPE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from pos_tender_type_head_tl
       where tender_type_id = I_pos_tender_type_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_pos_tt_head_tl_del_tab is NOT NULL and I_pos_tt_head_tl_del_tab.count > 0 then
      for i in I_pos_tt_head_tl_del_tab.FIRST..I_pos_tt_head_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_pos_tt_head_tl_del_tab(i).lang);
            L_key_val2 := 'Tender Type ID: '||to_char(I_pos_tt_head_tl_del_tab(i).tender_type_id);
            open C_LOCK_POS_TT_HEAD_TL_DEL(I_pos_tt_head_tl_del_tab(i).tender_type_id,
                                           I_pos_tt_head_tl_del_tab(i).lang);
            close C_LOCK_POS_TT_HEAD_TL_DEL;
            
            delete pos_tender_type_head_tl
             where lang = I_pos_tt_head_tl_del_tab(i).lang
               and tender_type_id = I_pos_tt_head_tl_del_tab(i).tender_type_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_POS_TENDER_TYPE_HEAD_TL',
                           I_pos_tt_head_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_POS_TT_HEAD_TL_DEL%ISOPEN then
         close C_LOCK_POS_TT_HEAD_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_POS_TT_HEAD_TL_DEL;
-------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_POS_TT_HEAD(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id    IN     SVC_POS_TENDER_TYPE_HEAD.PROCESS_ID%TYPE,
                             I_chunk_id      IN     SVC_POS_TENDER_TYPE_HEAD.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program               VARCHAR2(64)                             :='CORESVC_POS_TENDER_TYPE_HEAD.PROCESS_POS_TT_HEAD';
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE        :='SVC_POS_TENDER_TYPE_HEAD';
   L_calling_dialogue      VARCHAR2(4)                              :='TTYP';
   L_date                  POS_TENDER_TYPE_HEAD.EFFECTIVE_DATE%TYPE := GET_VDATE;
   L_process_error         BOOLEAN                                  := FALSE;
   L_error                 BOOLEAN;
   L_pos_tt_head_temp_rec  POS_TENDER_TYPE_HEAD%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_POS_TT_HEAD(I_process_id,
                                I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.PK_POS_TENDER_TYPE_HEAD_rid is NOT NULL then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'TENDER_TYPE_ID',
                     'DUP_RECORD');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)   then
         if rec.PK_POS_TENDER_TYPE_HEAD_rid is NULL   then
            WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'TENDER_TYPE_ID',
                     'NO_RECORD');
            L_error :=TRUE;
         end if;
         --Primary Key validation is already validated during spreadsheet upload.
         if rec.TENDER_TYPE_ID is NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'TENDER_TYPE_ID',
                        'MUST_ENTER_FIELD');
            L_error :=TRUE;
         end if;
      end if;

      if rec.action=action_new 
		   or (rec.action=action_mod)   then
         if rec.TENDER_TYPE_DESC is NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'TENDER_TYPE_DESC',
                        'POS_ENTER_TTYP_DESC');
            L_error :=TRUE;
         end if;

         if rec.EFFECTIVE_DATE is NULL   then
            WRITE_ERROR( I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                        'EFFECTIVE_DATE',
                        'EFFECTIVE_DATE_REQ');
            L_error :=TRUE;
         --Effective Date must not be greater than the current date.
         elsif (rec.action = action_mod
                and rec.effective_date <> rec.base_effective_date
                and rec.effective_date < L_date)
                or (rec.action = action_new
                and rec.effective_date < L_date)   then
                WRITE_ERROR( rec.process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             rec.chunk_id,
                             L_table,
                             rec.row_seq,
                            'EFFECTIVE_DATE',
                            'EFFECT_DATE_NOT_B4_TODAY');
                L_error :=TRUE;
         end if;
      end if;

      if PROCESS_POS_TT_HEAD_VAL(O_error_message,
                                 L_error,
                                 rec) = FALSE   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error   then
         L_pos_tt_head_temp_rec.tender_type_desc  := rec.tender_type_desc;
         L_pos_tt_head_temp_rec.tender_type_group := rec.tender_type_group;
         L_pos_tt_head_temp_rec.effective_date    := rec.effective_date;
         --When the record is successfully validated and modified
         --[here even if there is no actual modification i.e. no flag for checking the update]
         if rec.action = action_mod   then
            L_pos_tt_head_temp_rec.modify_date    := GET_VDATE;
            L_pos_tt_head_temp_rec.modify_id      := GET_USER;
         end if;

         if rec.action = action_new and rec.TENDER_TYPE_ID is NULL   then
           if POS_NEXT_GENERATED_NUMBER(O_error_message,
                                        L_pos_tt_head_temp_rec.tender_type_id,
                                        L_calling_dialogue) = FALSE then
              WRITE_ERROR( I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
              L_error :=TRUE;
           end if;
         else
            L_pos_tt_head_temp_rec.tender_type_id := rec.tender_type_id;
         end if;

---Default Value Setting Code: Sets default value (Y) to few columns that are part of the functional sunset.
         L_pos_tt_head_temp_rec.create_date                := GET_VDATE;
         L_pos_tt_head_temp_rec.create_id                  := GET_USER;

--Default Value Setting Code

         if rec.action = action_new   then
            if EXEC_POS_TT_HEAD_INS( O_error_message,
                                     L_pos_tt_head_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod   then
            if EXEC_POS_TT_HEAD_UPD( O_error_message,
                                     L_pos_tt_head_temp_rec ) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_POS_TT_HEAD_DEL(O_error_message,
                                    L_pos_tt_head_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if; 
      end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_POS_TT_HEAD;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_POS_TT_HEAD_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id      IN       SVC_POS_TENDER_TYPE_HEAD_TL.PROCESS_ID%TYPE,
                                I_chunk_id        IN       SVC_POS_TENDER_TYPE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_POS_TENDER_TYPE_HEAD.PROCESS_POS_TT_HEAD_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'POS_TENDER_TYPE_HEAD_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'POS_TENDER_TYPE_HEAD';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_POS_TENDER_TYPE_HEAD_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_pos_tt_head_TL_temp_rec    POS_TENDER_TYPE_HEAD_TL%ROWTYPE;
   L_pos_tt_head_tl_upd_rst     ROW_SEQ_TAB;
   L_pos_tt_head_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_POS_TT_HEAD_TL(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
      select pk_pos_tender_type_head_tl.rowid  as pk_pos_tender_type_head_TL_rid,
             fk_pos_tender_type_head.rowid     as fk_pos_tender_type_head_rid,
             fk_lang.rowid                     as fk_lang_rid,
             st.lang,
             st.tender_type_id,
             st.tender_type_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_pos_tender_type_head_tl  st,
             pos_tender_type_head         fk_pos_tender_type_head,
             pos_tender_type_head_tl      pk_pos_tender_type_head_tl,
             lang                         fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.tender_type_id  =  fk_pos_tender_type_head.tender_type_id (+)
         and st.lang        =  pk_pos_tender_type_head_tl.lang (+)
         and st.tender_type_id  =  pk_pos_tender_type_head_tl.tender_type_id (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_POS_TENDER_TYPE_HEAD_TL is TABLE OF C_SVC_POS_TT_HEAD_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_pos_tender_type_head_tab        SVC_POS_TENDER_TYPE_HEAD_TL;

   L_pos_tt_head_tl_ins_tab         pos_tender_type_head_tab         := NEW pos_tender_type_head_tab();
   L_pos_tt_head_tl_upd_tab         pos_tender_type_head_tab         := NEW pos_tender_type_head_tab();
   L_pos_tt_head_tl_del_tab         pos_tender_type_head_tab         := NEW pos_tender_type_head_tab();

BEGIN
   if C_SVC_POS_TT_HEAD_TL%ISOPEN then
      close C_SVC_POS_TT_HEAD_TL;
   end if;

   open C_SVC_POS_TT_HEAD_TL(I_process_id,
                             I_chunk_id);
   LOOP
      fetch C_SVC_POS_TT_HEAD_TL bulk collect into L_svc_pos_tender_type_head_tab limit LP_bulk_fetch_limit;
      if L_svc_pos_tender_type_head_tab.COUNT > 0 then
         FOR i in L_svc_pos_tender_type_head_tab.FIRST..L_svc_pos_tender_type_head_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_pos_tender_type_head_tab(i).action is NULL
               or L_svc_pos_tender_type_head_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_pos_tender_type_head_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_pos_tender_type_head_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_pos_tender_type_head_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_pos_tender_type_head_tab(i).action = action_new
               and L_svc_pos_tender_type_head_tab(i).pk_pos_tender_type_head_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_pos_tender_type_head_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_pos_tender_type_head_tab(i).action IN (action_mod, action_del)
               and L_svc_pos_tender_type_head_tab(i).lang is NOT NULL
               and L_svc_pos_tender_type_head_tab(i).tender_type_id is NOT NULL
               and L_svc_pos_tender_type_head_tab(i).pk_pos_tender_type_head_TL_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_pos_tender_type_head_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_pos_tender_type_head_tab(i).action = action_new
               and L_svc_pos_tender_type_head_tab(i).tender_type_id is NOT NULL
               and L_svc_pos_tender_type_head_tab(i).fk_pos_tender_type_head_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_pos_tender_type_head_tab(i).row_seq,
                            'TENDER_TYPE_ID',
                            'POS_INV_TTYP_ID');
               L_error :=TRUE;
            end if;

            if L_svc_pos_tender_type_head_tab(i).action = action_new
               and L_svc_pos_tender_type_head_tab(i).fk_lang_rid is NULL
               and L_svc_pos_tender_type_head_tab(i).lang is NOT NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_pos_tender_type_head_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_pos_tender_type_head_tab(i).action in (action_new, action_mod) then
               if L_svc_pos_tender_type_head_tab(i).tender_type_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_pos_tender_type_head_tab(i).row_seq,
                              'TENDER_TYPE_DESC',
                              'POS_ENTER_TTYP_DESC');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_pos_tender_type_head_tab(i).tender_type_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_pos_tender_type_head_tab(i).row_seq,
                           'TENDER_TYPE_ID',
                           'ENTER_TENDER_TYPE_ID');
               L_error :=TRUE;
            end if;

            if L_svc_pos_tender_type_head_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_pos_tender_type_head_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_pos_tt_head_TL_temp_rec.lang := L_svc_pos_tender_type_head_tab(i).lang;
               L_pos_tt_head_TL_temp_rec.tender_type_id := L_svc_pos_tender_type_head_tab(i).tender_type_id;
               L_pos_tt_head_TL_temp_rec.tender_type_desc := L_svc_pos_tender_type_head_tab(i).tender_type_desc;
               L_pos_tt_head_TL_temp_rec.create_datetime := SYSDATE;
               L_pos_tt_head_TL_temp_rec.create_id := GET_USER;
               L_pos_tt_head_TL_temp_rec.last_update_datetime := SYSDATE;
               L_pos_tt_head_TL_temp_rec.last_update_id := GET_USER;

               if L_svc_pos_tender_type_head_tab(i).action = action_new then
                  L_pos_tt_head_tl_ins_tab.extend;
                  L_pos_tt_head_tl_ins_tab(L_pos_tt_head_tl_ins_tab.count()) := L_pos_tt_head_TL_temp_rec;
               end if;

               if L_svc_pos_tender_type_head_tab(i).action = action_mod then
                  L_pos_tt_head_tl_upd_tab.extend;
                  L_pos_tt_head_tl_upd_tab(L_pos_tt_head_tl_upd_tab.count()) := L_pos_tt_head_TL_temp_rec;
                  L_pos_tt_head_tl_upd_rst(L_pos_tt_head_tl_upd_tab.count()) := L_svc_pos_tender_type_head_tab(i).row_seq;
               end if;

               if L_svc_pos_tender_type_head_tab(i).action = action_del then
                  L_pos_tt_head_tl_del_tab.extend;
                  L_pos_tt_head_tl_del_tab(L_pos_tt_head_tl_del_tab.count()) := L_pos_tt_head_TL_temp_rec;
                  L_pos_tt_head_tl_del_rst(L_pos_tt_head_tl_del_tab.count()) := L_svc_pos_tender_type_head_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_POS_TT_HEAD_TL%NOTFOUND;
   END LOOP;
   close C_SVC_POS_TT_HEAD_TL;

   if EXEC_POS_TT_HEAD_TL_INS(O_error_message,
                              L_pos_tt_head_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_POS_TT_HEAD_TL_UPD(O_error_message,
                              L_pos_tt_head_tl_upd_tab,
                              L_pos_tt_head_tl_upd_rst,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_POS_TT_HEAD_TL_DEL(O_error_message,
                              L_pos_tt_head_tl_del_tab,
                              L_pos_tt_head_tl_del_rst,
                              I_process_id,
                              I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_POS_TT_HEAD_TL;
-----------------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count   IN OUT NUMBER,
                  I_process_id    IN     NUMBER,
                  I_chunk_id      IN     NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    :='CORESVC_POS_TENDER_TYPE_HEAD.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();

   if PROCESS_POS_TT_HEAD(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE   then
      return FALSE;
   end if;

   if PROCESS_POS_TT_HEAD_TL(O_error_message,
                             I_process_id,
                             I_chunk_id) = FALSE   then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                      when status = 'PE' then
                          'PE'
                      else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-------------------------------------------------------------------------------------------------------------
END CORESVC_POS_TENDER_TYPE_HEAD;
/
