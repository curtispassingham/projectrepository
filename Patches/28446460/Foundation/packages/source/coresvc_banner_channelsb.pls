CREATE OR REPLACE PACKAGE BODY CORESVC_BANNER_CHANNELS AS
----------------------------------------------------------------------------------------------
   cursor C_SVC_BANNER(I_process_id   NUMBER,
                       I_chunk_id     NUMBER) is
      select pk_banner.rowid    as pk_banner_rid,
             st.rowid           as st_rowid,
             st.banner_name,
             st.banner_id,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)   as action,
             st.process$status
        from svc_banner st,
             banner pk_banner,
             dual
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.banner_id  = pk_banner.banner_id (+);

   cursor C_SVC_CHANNELS(I_process_id NUMBER,
                         I_chunk_id NUMBER) is
      select pk_channels.rowid   as pk_channels_rid,
             fk_banner.rowid     as fk_banner_rid,
             cd_chty.rowid       as cd_rid,
             st.rowid            as st_rid,
             st.banner_id,
             st.channel_type,
             st.channel_name,
             st.channel_id,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)    as action,
             st.process$status
        from svc_channels st,
             channels pk_channels,
             banner fk_banner,
             code_detail cd_chty
       where st.process_id         = I_process_id
         and st.chunk_id           = I_chunk_id
         and st.channel_id         = pk_channels.channel_id (+)
         and st.banner_id          = fk_banner.banner_id (+)
         and st.channel_type       = cd_chty.code (+)
         and cd_chty.code_type (+) = 'CHTY'
         and (st.process$status = 'N' or st.process$status  is NULL);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   Lp_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   Lp_s9t_errors_tab s9t_errors_tab_typ;

   Type BANNER_TL_tab IS TABLE OF BANNER_TL%ROWTYPE;
   Type CHANNELS_TL_tab IS TABLE OF CHANNELS_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   
   LP_primary_lang   LANG.LANG%TYPE;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );

   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID             := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME       := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID        := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME  := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES (I_file_id   NUMBER) IS
   L_sheets         s9t_pkg.names_map_typ;
   BANNER_cols      s9t_pkg.names_map_typ;
   BANNER_TL_cols   s9t_pkg.names_map_typ;
   CHANNELS_cols   S9T_PKG.NAMES_map_typ;
   CHANNELS_TL_cols   S9T_PKG.NAMES_map_typ;
BEGIN
   L_sheets           := s9t_pkg.get_sheet_names(I_file_id);
   BANNER_cols        := s9t_pkg.get_col_names(I_file_id,
                                               BANNER_sheet);
   BANNER$action      := BANNER_cols('ACTION');
   BANNER$banner_id   := BANNER_cols('BANNER_ID');
   BANNER$banner_name := BANNER_cols('BANNER_NAME');

   BANNER_TL_cols        := s9t_pkg.get_col_names(I_file_id,
                                                  BANNER_TL_sheet);
   BANNER_TL$action      := BANNER_TL_cols('ACTION');
   BANNER_TL$lang        := BANNER_TL_cols('LANG');
   BANNER_TL$banner_id   := BANNER_TL_cols('BANNER_ID');
   BANNER_TL$banner_name := BANNER_TL_cols('BANNER_NAME');

   CHANNELS_cols         := S9T_PKG.get_col_names(I_file_id,
                                                  CHANNELS_sheet);
   CHANNELS$ACTION       := CHANNELS_cols('ACTION');
   CHANNELS$CHANNEL_ID   := CHANNELS_cols('CHANNEL_ID');
   CHANNELS$BANNER_ID    := CHANNELS_cols('BANNER_ID');
   CHANNELS$CHANNEL_TYPE := CHANNELS_cols('CHANNEL_TYPE');
   CHANNELS$CHANNEL_NAME := CHANNELS_cols('CHANNEL_NAME');

   CHANNELS_TL_cols         := S9T_PKG.get_col_names(I_file_id,
                                                     CHANNELS_TL_sheet);
   CHANNELS_TL$ACTION       := CHANNELS_TL_cols('ACTION');
   CHANNELS_TL$LANG         := CHANNELS_TL_cols('LANG');
   CHANNELS_TL$CHANNEL_ID   := CHANNELS_TL_cols('CHANNEL_ID');
   CHANNELS_TL$CHANNEL_NAME := CHANNELS_TL_cols('CHANNEL_NAME');
   
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_BANNER(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = BANNER_sheet)
               select s9t_row(s9t_cells(CORESVC_BANNER_CHANNELS.action_mod,
                                        banner_id,
                                        banner_name))
                 from BANNER;
END POPULATE_BANNER;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_BANNER_TL(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = BANNER_TL_sheet)
               select s9t_row(s9t_cells(CORESVC_BANNER_CHANNELS.action_mod,
                                        lang,
                                        banner_id,
                                        banner_name))
                 from BANNER_TL;
END POPULATE_BANNER_TL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_CHANNELS(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE
     (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id    = I_file_id
         and ss.sheet_name = CHANNELS_sheet)
      select s9t_row(s9t_cells(CORESVC_BANNER_CHANNELS.action_mod,
                               channel_id,
                               banner_id,
                               channel_type,
                               channel_name))
        from channels;
END POPULATE_CHANNELS;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_CHANNELS_TL(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE
     (select ss.s9t_rows
        from s9t_folder sf,
             TABLE(sf.s9t_file_obj.sheets) ss
       where sf.file_id    = I_file_id
         and ss.sheet_name = CHANNELS_TL_sheet)
      select s9t_row(s9t_cells(CORESVC_BANNER_CHANNELS.action_mod,
                               lang,
                               channel_id,
                               channel_name))
        from channels_tl;
END POPULATE_CHANNELS_TL;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS

   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(BANNER_sheet);
   L_file.sheets(L_file.get_sheet_index(BANNER_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                    'BANNER_ID',
                                                                                    'BANNER_NAME');

   L_file.add_sheet(BANNER_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(BANNER_TL_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                       'LANG',
                                                                                       'BANNER_ID',
                                                                                       'BANNER_NAME');

   L_file.add_sheet(CHANNELS_sheet);
   L_file.sheets(L_file.get_sheet_index(CHANNELS_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                      'CHANNEL_ID',
                                                                                      'BANNER_ID',
                                                                                      'CHANNEL_TYPE',
                                                                                      'CHANNEL_NAME'
                                                                                    );

   L_file.add_sheet(CHANNELS_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(CHANNELS_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                        'LANG',
                                                                                        'CHANNEL_ID',
                                                                                        'CHANNEL_NAME'
                                                                                        );

   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.CREATE_S9T';
   L_file      s9t_file;

BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_BANNER(O_file_id);
      POPULATE_BANNER_TL(O_file_id);
      POPULATE_CHANNELS(O_file_id);
      POPULATE_CHANNELS_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_BANNER(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_BANNER.process_id%TYPE)IS

   TYPE svc_BANNER_col_typ IS TABLE OF SVC_BANNER%ROWTYPE;
   L_temp_rec       SVC_BANNER%ROWTYPE;
   svc_BANNER_col   svc_BANNER_col_typ := NEW svc_BANNER_col_typ();
   L_process_id     SVC_BANNER.process_id%TYPE;
   L_error          BOOLEAN := FALSE;
   L_default_rec    SVC_BANNER%ROWTYPE;

   cursor C_MANDATORY_IND is
      select BANNER_NAME_mi,
             BANNER_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_BANNER_CHANNELS.template_key
                 and wksht_key = 'BANNER')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('BANNER_NAME'   as   banner_name,
                                       'BANNER_ID'     as   banner_id,
                                       NULL            as   dummy));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_BANNER';
   L_pk_columns    VARCHAR2(255)  := 'Banner ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;


BEGIN
   FOR rec in (select BANNER_NAME_dv,
                      BANNER_ID_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_BANNER_CHANNELS.template_key
                          and wksht_key = 'BANNER')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('BANNER_NAME'   as   banner_name,
                                                'BANNER_ID'     as   banner_id,
                                                NULL            as   dummy)))

   LOOP
      BEGIN
         L_default_rec.banner_name := rec.BANNER_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'BANNER',
                             NULL,
                            'BANNER_NAME',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.banner_id := rec.BANNER_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'BANNER',
                             NULL,
                            'BANNER_ID',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec in (select r.get_cell(BANNER$action)        as   action,
                      r.get_cell(BANNER$banner_name)   as   banner_name,
                      r.get_cell(BANNER$banner_id)     as   banner_id,
                      r.get_row_seq()                  as   row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(BANNER_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.banner_name := rec.banner_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_sheet,
                            rec.row_seq,
                            'BANNER_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.banner_id := rec.banner_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_sheet,
                            rec.row_seq,
                            'BANNER_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_BANNER_CHANNELS.action_new then
        L_temp_rec.banner_name   :=    NVL(L_temp_rec.banner_name,
                                           L_default_rec.banner_name);
        L_temp_rec.banner_id     :=    NVL(L_temp_rec.banner_id,
                                           L_default_rec.banner_id);
      end if;
      if not (L_temp_rec.banner_id is NOT NULL) then
         WRITE_S9T_ERROR( I_file_id,
                          BANNER_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_BANNER_col.extend();
         svc_BANNER_col(svc_BANNER_col.count()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_BANNER_col.count SAVE EXCEPTIONS
      merge into SVC_BANNER st
      using (select (case
                     when L_mi_rec.BANNER_NAME_mi = 'N'
                      and svc_BANNER_col(i).action = CORESVC_BANNER_CHANNELS.action_mod
                      and s1.banner_name IS NULL
                     then mt.banner_name
                     else s1.banner_name
                     end) as banner_name,
                    (case
                     when L_mi_rec.BANNER_ID_mi = 'N'
                      and svc_BANNER_col(i).action = CORESVC_BANNER_CHANNELS.action_mod
                      and s1.banner_id IS NULL
                     then mt.banner_id
                     else s1.banner_id
                     end) as banner_id,
                     NULL as dummy
               from (select svc_BANNER_col(i).banner_name   as   banner_name,
                            svc_BANNER_col(i).banner_id     as   banner_id,
                            NULL                            as   dummy
                       from dual) s1,
                            BANNER mt
                      where mt.banner_id (+) = s1.banner_id) sq
                         ON (st.banner_id = sq.banner_id and
                             svc_BANNER_col(i).action in (CORESVC_BANNER_CHANNELS.action_mod,CORESVC_BANNER_CHANNELS.action_del))
      when matched then
      update
         set process_id        = svc_BANNER_col(i).process_id ,
             chunk_id          = svc_BANNER_col(i).chunk_id ,
             row_seq           = svc_BANNER_col(i).row_seq ,
             action            = svc_BANNER_col(i).ACTION,
             process$status    = svc_BANNER_col(i).process$status ,
             banner_name       = sq.banner_name ,
             CREATE_ID         = svc_BANNER_col(i).CREATE_ID ,
             CREATE_DATETIME   = svc_BANNER_col(i).CREATE_DATETIME ,
             LAST_UPD_ID       = svc_BANNER_col(i).LAST_UPD_ID ,
             LAST_UPD_DATETIME = svc_BANNER_col(i).LAST_UPD_DATETIME
      when NOT matched then
      insert(process_id ,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             banner_name ,
             banner_id ,
             CREATE_ID ,
             CREATE_DATETIME ,
             LAST_UPD_ID ,
             LAST_UPD_DATETIME)
      values(svc_BANNER_col(i).process_id ,
             svc_BANNER_col(i).chunk_id ,
             svc_BANNER_col(i).row_seq ,
             svc_BANNER_col(i).action ,
             svc_BANNER_col(i).process$status ,
             sq.banner_name ,
             sq.banner_id ,
             svc_BANNER_col(i).CREATE_ID ,
             svc_BANNER_col(i).CREATE_DATETIME ,
             svc_BANNER_col(i).LAST_UPD_ID ,
             svc_BANNER_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            BANNER_sheet,
                            svc_banner_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_BANNER;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_BANNER_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                I_process_id   IN   SVC_BANNER_TL.PROCESS_ID%TYPE)IS

   TYPE SVC_BANNER_TL_COL_TYP IS TABLE OF SVC_BANNER_TL%ROWTYPE;
   L_temp_rec          SVC_BANNER_TL%ROWTYPE;
   SVC_BANNER_TL_COL   SVC_BANNER_TL_COL_TYP := NEW SVC_BANNER_TL_COL_TYP();
   L_process_id        SVC_BANNER_TL.PROCESS_ID%TYPE;
   L_error             BOOLEAN := FALSE;
   L_default_rec       SVC_BANNER_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             banner_id_mi,
             banner_name_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_BANNER_CHANNELS.TEMPLATE_KEY
                 and wksht_key = 'BANNER_TL' )
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('LANG'          as lang,
                                       'BANNER_ID'     as banner_id,
                                       'BANNER_NAME'   as banner_name));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_BANNER_TL';
   L_pk_columns    VARCHAR2(255)  := 'Banner ID, Language';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;


BEGIN
   FOR rec in (select LANG_dv,
                      BANNER_ID_dv,
                      BANNER_NAME_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_BANNER_CHANNELS.TEMPLATE_KEY
                          and wksht_key = 'BANNER_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('LANG'        as   lang,
                                                'BANNER_ID'   as   banner_id,
                                                'BANNER_NAME' as   banner_name)))

   LOOP
      BEGIN
         L_default_rec.lang := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_TL_sheet,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.banner_id := rec.BANNER_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_TL_sheet,
                            NULL,
                            'BANNER_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.banner_name := rec.BANNER_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_TL_sheet,
                            NULL,
                            'BANNER_NAME',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec in (select r.get_cell(BANNER_TL$action)        as   action,
                      r.get_cell(BANNER_TL$lang)          as   lang,
                      r.get_cell(BANNER_TL$banner_id)     as   banner_id,
                      r.get_cell(BANNER_TL$banner_name)   as   banner_name,
                      r.get_row_seq()                     as   row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(BANNER_TL_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.banner_id := rec.banner_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_TL_sheet,
                            rec.row_seq,
                            'BANNER_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.banner_name := rec.banner_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            BANNER_TL_sheet,
                            rec.row_seq,
                            'BANNER_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_BANNER_CHANNELS.action_new then
        L_temp_rec.lang          :=    NVL(L_temp_rec.lang,
                                           L_default_rec.lang);
        L_temp_rec.banner_id     :=    NVL(L_temp_rec.banner_id,
                                           L_default_rec.banner_id);
        L_temp_rec.banner_name   :=    NVL(L_temp_rec.banner_name,
                                           L_default_rec.banner_name);
      end if;
      if not (L_temp_rec.banner_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR( I_file_id,
                          BANNER_TL_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_BANNER_TL_col.extend();
         svc_BANNER_TL_col(svc_BANNER_TL_col.count()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_BANNER_TL_col.count SAVE EXCEPTIONS
      merge into SVC_BANNER_TL st
      using (select (case
                     when L_mi_rec.BANNER_NAME_mi = 'N'
                      and svc_BANNER_tl_col(i).action = CORESVC_BANNER_CHANNELS.action_mod
                      and s1.banner_name IS NULL
                     then mt.banner_name
                     else s1.banner_name
                     end) as banner_name,
                    (case
                     when L_mi_rec.BANNER_ID_mi = 'N'
                      and svc_BANNER_tl_col(i).action = CORESVC_BANNER_CHANNELS.action_mod
                      and s1.banner_id IS NULL
                     then mt.banner_id
                     else s1.banner_id
                     end) as banner_id,
                    (case
                     when L_mi_rec.LANG_mi = 'N'
                      and svc_BANNER_TL_col(i).action = CORESVC_BANNER_CHANNELS.action_mod
                      and s1.lang IS NULL
                     then mt.lang
                     else s1.lang
                     end) as lang
               from (select svc_BANNER_TL_col(i).banner_name   as   banner_name,
                            svc_BANNER_TL_col(i).banner_id     as   banner_id,
                            svc_BANNER_TL_col(i).lang          as   lang
                       from dual) s1,
                            BANNER_TL mt
                      where mt.banner_id (+) = s1.banner_id
                        and mt.lang (+) = s1.lang) sq
                         ON (st.banner_id = sq.banner_id and
                             st.lang = sq.lang and
                             svc_BANNER_TL_col(i).action in (CORESVC_BANNER_CHANNELS.action_mod,CORESVC_BANNER_CHANNELS.action_del))
      when matched then
      update
         set process_id        = svc_BANNER_TL_col(i).process_id ,
             chunk_id          = svc_BANNER_TL_col(i).chunk_id ,
             row_seq           = svc_BANNER_TL_col(i).row_seq ,
             action            = svc_BANNER_TL_col(i).ACTION,
             process$status    = svc_BANNER_TL_col(i).process$status ,
             banner_name       = sq.banner_name
      when NOT matched then
      insert(process_id ,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             banner_name ,
             banner_id ,
             lang)
      values(svc_BANNER_TL_col(i).process_id ,
             svc_BANNER_TL_col(i).chunk_id ,
             svc_BANNER_TL_col(i).row_seq ,
             svc_BANNER_TL_col(i).action ,
             svc_BANNER_TL_col(i).process$status ,
             sq.banner_name ,
             sq.banner_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            BANNER_TL_sheet,
                            svc_BANNER_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_BANNER_TL;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CHANNELS(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                               I_process_id IN SVC_CHANNELS.PROCESS_ID%TYPE)
IS
   TYPE svc_CHANNELS_col_typ IS TABLE OF SVC_CHANNELS%ROWTYPE;
   L_temp_rec                            SVC_CHANNELS%ROWTYPE;
   svc_CHANNELS_col                      svc_CHANNELS_col_typ                 :=NEW svc_CHANNELS_col_typ();
   L_process_id                          SVC_CHANNELS.PROCESS_ID%TYPE;
   L_error                               BOOLEAN                              :=FALSE;
   L_default_rec                         SVC_CHANNELS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select banner_id_mi,
             channel_type_mi,
             channel_name_mi,
             channel_id_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_BANNER_CHANNELS.template_key
                 and wksht_key     = 'CHANNELS')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('channel_id'    as banner_id,
                                                                'banner_id' as channel_type,
                                                                'channel_type' as channel_name,
                                                                'channel_name'   as channel_id,
                                                                 NULL           as dummy));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CHANNELS';
   L_pk_columns    VARCHAR2(255)  := 'Channel ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec IN
    (select BANNER_ID_dv,
            CHANNEL_TYPE_dv,
            CHANNEL_NAME_dv,
            CHANNEL_ID_dv,
            NULL as dummy
       from (select column_key,
                    default_value
               from s9t_tmpl_cols_def
              where template_key  = CORESVC_BANNER_CHANNELS.template_key
                and wksht_key     = 'CHANNELS')
              PIVOT (MAX(default_value) as dv FOR (column_key) IN ('channel_id'    as banner_id,
                                                                   'banner_id' as channel_type,
                                                                   'channel_type' as channel_name,
                                                                   'channel_name'   as channel_id,
                                                                    NULL           as dummy)))LOOP

   BEGIN
   L_default_rec.BANNER_ID := rec.BANNER_ID_dv;

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'CHANNELS',
                          NULL,
                         'BANNER_ID',
                          NULL,
                         'INV_DEFAULT');

   END;

   BEGIN
   L_default_rec.CHANNEL_TYPE := rec.CHANNEL_TYPE_dv;

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'CHANNELS',
                          NULL,
                         'CHANNEL_TYPE',
                          NULL,
                         'INV_DEFAULT');

   END;

   BEGIN
   L_default_rec.CHANNEL_NAME := rec.CHANNEL_NAME_dv;

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'CHANNELS',
                          NULL,
                         'CHANNEL_NAME',
                          NULL,
                         'INV_DEFAULT');

   END;

   BEGIN
   L_default_rec.CHANNEL_ID := rec.CHANNEL_ID_dv;

   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'CHANNELS',
                          NULL,
                         'CHANNEL_ID',
                          NULL,
                         'INV_DEFAULT');

   END;

END LOOP;

   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
     FOR rec IN (select r.GET_CELL(CHANNELS$ACTION)        as action,
                        r.GET_CELL(CHANNELS$BANNER_ID)     as banner_id,
                        r.GET_CELL(CHANNELS$CHANNEL_TYPE)  as channel_type,
                        r.GET_CELL(CHANNELS$CHANNEL_NAME)  as channel_name,
                        r.GET_CELL(CHANNELS$CHANNEL_ID)    as channel_id,
                        r.GET_ROW_SEQ()                    as row_seq
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss,
                        TABLE(ss.s9t_rows) r
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = GET_SHEET_NAME_TRANS(CHANNELS_sheet)) LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
     BEGIN
        L_temp_rec.action := rec.action;
     EXCEPTION
        when OTHERS then
           if c_mandatory_ind%ISOPEN then
              close c_mandatory_ind;
           end if;

           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_sheet,
                           rec.row_seq,
                           action_column,
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.banner_id := rec.banner_id;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_sheet,
                           rec.row_seq,
                           'BANNER_ID',
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.channel_type := rec.channel_type;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_sheet,
                           rec.row_seq,
                           'CHANNEL_TYPE',
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.channel_name := rec.channel_name;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_sheet,
                           rec.row_seq,
                           'CHANNEL_NAME',
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.channel_id := rec.channel_id;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_sheet,
                           rec.row_seq,
                           'CHANNEL_ID',
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     if rec.action = CORESVC_BANNER_CHANNELS.action_new then
        L_temp_rec.BANNER_ID    := NVL( L_temp_rec.BANNER_ID,L_default_rec.BANNER_ID);
        L_temp_rec.CHANNEL_TYPE := NVL( L_temp_rec.CHANNEL_TYPE,L_default_rec.CHANNEL_TYPE);
        L_temp_rec.CHANNEL_NAME := NVL( L_temp_rec.CHANNEL_NAME,L_default_rec.CHANNEL_NAME);
        L_temp_rec.CHANNEL_ID   := NVL( L_temp_rec.CHANNEL_ID,L_default_rec.CHANNEL_ID);
     end if;
     if NOT(L_temp_rec.channel_id is NOT NULL)then
         WRITE_S9T_ERROR( I_file_id,
                          CHANNELS_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
     end if;

     if NOT L_error then
            svc_CHANNELS_col.EXTEND();
            svc_CHANNELS_col(svc_CHANNELS_col.COUNT()):=L_temp_rec;
     end if;
END LOOP;

BEGIN
   forall i IN 1..svc_CHANNELS_col.COUNT SAVE EXCEPTIONS
      merge into SVC_CHANNELS st
      using (select (case
                        when L_mi_rec.BANNER_ID_mi    = 'N'and
                             svc_CHANNELS_col(i).action = CORESVC_BANNER_CHANNELS.action_mod and
                             s1.banner_id               is NULL then
                             mt.banner_id
                        else s1.banner_id
                        end) as banner_id,

                    (case
                        when L_mi_rec.CHANNEL_TYPE_mi    = 'N' and
                             svc_CHANNELS_col(i).action    = CORESVC_BANNER_CHANNELS.action_mod and
                             s1.channel_type               is NULL then
                             mt.channel_type
                        else s1.channel_type
                        end) as channel_type,

                    (case
                        when L_mi_rec.CHANNEL_NAME_mi    = 'N' and
                             svc_CHANNELS_col(i).action    = CORESVC_BANNER_CHANNELS.action_mod and
                             s1.channel_name               is NULL then
                             mt.channel_name
                        else s1.channel_name
                        end) as channel_name,

                    (case
                        when L_mi_rec.CHANNEL_ID_mi      = 'N' and
                             svc_CHANNELS_col(i).action    = CORESVC_BANNER_CHANNELS.action_mod and
                             s1.channel_id                 is NULL then
                             mt.channel_id
                        else s1.channel_id
                        end) as channel_id,
                        null as dummy
               from (select svc_CHANNELS_col(i).banner_id    as banner_id,
                            svc_CHANNELS_col(i).channel_type as channel_type,
                            svc_CHANNELS_col(i).channel_name as channel_name,
                            svc_CHANNELS_col(i).channel_id   as channel_id,
                            NULL                             as dummy
                       from dual) s1,
                            CHANNELS mt
                      where mt.channel_id (+) = s1.channel_id
                        and 1 = 1) sq
         on (st.channel_id = sq.channel_id
             and svc_CHANNELS_col(i).action in (CORESVC_BANNER_CHANNELS.action_mod,
                                                CORESVC_BANNER_CHANNELS.action_del))
         when matched then
            update
               set process_id        = svc_CHANNELS_col(i).process_id ,
                   chunk_id          = svc_CHANNELS_col(i).chunk_id ,
                   row_seq           = svc_CHANNELS_col(i).row_seq ,
                   action            = svc_CHANNELS_col(i).action,
                   process$status    = svc_CHANNELS_col(i).process$status ,
                   banner_id         = sq.banner_id ,
                   channel_name      = sq.channel_name ,
                   channel_type      = sq.channel_type ,
                   create_id         = svc_CHANNELS_col(i).create_id ,
                   create_datetime   = svc_CHANNELS_col(i).create_datetime ,
                   last_upd_id       = svc_CHANNELS_col(i).last_upd_id ,
                   last_upd_datetime = svc_CHANNELS_col(i).last_upd_datetime
         when NOT matched then
            insert(process_id ,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   banner_id ,
                   channel_type ,
                   channel_name ,
                   channel_id ,
                   create_id ,
                   create_datetime ,
                   last_upd_id ,
                   last_upd_datetime)
            values(svc_CHANNELS_col(i).process_id ,
                   svc_CHANNELS_col(i).chunk_id ,
                   svc_CHANNELS_col(i).row_seq ,
                   svc_CHANNELS_col(i).action ,
                   svc_CHANNELS_col(i).process$status ,
                   sq.banner_id ,
                   sq.channel_type ,
                   sq.channel_name ,
                   sq.channel_id ,
                   svc_CHANNELS_col(i).create_id ,
                   svc_CHANNELS_col(i).create_datetime ,
                   svc_CHANNELS_col(i).last_upd_id ,
                   svc_CHANNELS_col(i).last_upd_datetime);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            CHANNELS_sheet,
                            svc_channels_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_CHANNELS;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_CHANNELS_TL(I_file_id    IN S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id IN SVC_CHANNELS_TL.PROCESS_ID%TYPE)
IS
   TYPE SVC_CHANNELS_TL_COL_TYP IS TABLE OF SVC_CHANNELS_TL%ROWTYPE;
   L_temp_rec                            SVC_CHANNELS_TL%ROWTYPE;
   svc_channels_tl_col                   SVC_CHANNELS_TL_COL_TYP              := NEW SVC_CHANNELS_TL_COL_TYP();
   L_process_id                          SVC_CHANNELS_TL.PROCESS_ID%TYPE;
   L_error                               BOOLEAN                              :=FALSE;
   L_default_rec                         SVC_CHANNELS_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select channel_name_mi,
             channel_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_BANNER_CHANNELS.template_key
                 and wksht_key     = 'CHANNELS_TL')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('channel_name'    as channel_name,
                                                                'channel_id'      as channel_id,
                                                                'lang'            as lang));
   L_mi_rec c_mandatory_ind%ROWTYPE;
   dmL_errors EXCEPTION;
   PRAGMA exception_init(dmL_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_CHANNELS_TL';
   L_pk_columns    VARCHAR2(255)  := 'Channel ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FOR rec IN
    (select CHANNEL_NAME_dv,
            CHANNEL_ID_dv,
            LANG_dv
       from (select column_key,
                    default_value
               from s9t_tmpl_cols_def
              where template_key  = CORESVC_BANNER_CHANNELS.template_key
                and wksht_key     = 'CHANNELS_TL')
              PIVOT (MAX(default_value) as dv FOR (column_key) IN ('channel_name' as channel_name,
                                                                   'channel_id'   as channel_id,
                                                                   'lang'         as lang)))LOOP

      BEGIN
         L_default_rec.CHANNEL_NAME := rec.CHANNEL_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CHANNELS_TL_sheet,
                            NULL,
                            'CHANNEL_NAME',
                            NULL,
                            'INV_DEFAULT');

      END;
      BEGIN
         L_default_rec.CHANNEL_ID := rec.CHANNEL_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CHANNELS_TL_sheet,
                            NULL,
                            'CHANNEL_ID',
                            NULL,
                            'INV_DEFAULT');

      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            CHANNELS_TL_sheet,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');

      END;
   END LOOP;

   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
     FOR rec IN (select r.GET_CELL(CHANNELS_TL$ACTION)        as action,
                        r.GET_CELL(CHANNELS_TL$LANG)          as lang,
                        r.GET_CELL(CHANNELS_TL$CHANNEL_ID)    as channel_id,
                        r.GET_CELL(CHANNELS_TL$CHANNEL_NAME)  as channel_name,
                        r.GET_ROW_SEQ()                       as row_seq
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss,
                        TABLE(ss.s9t_rows) r
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = GET_SHEET_NAME_TRANS(CHANNELS_TL_sheet)) LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
     BEGIN
        L_temp_rec.action := rec.action;
     EXCEPTION
        when OTHERS then
           if c_mandatory_ind%ISOPEN then
              close c_mandatory_ind;
           end if;

           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_TL_sheet,
                           rec.row_seq,
                           action_column,
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.channel_name := rec.channel_name;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_TL_sheet,
                           rec.row_seq,
                           'CHANNEL_NAME',
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.channel_id := rec.channel_id;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_TL_sheet,
                           rec.row_seq,
                           'CHANNEL_ID',
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     BEGIN
        L_temp_rec.lang := rec.lang;
     EXCEPTION
        when OTHERS then
           WRITE_S9T_ERROR(I_file_id,
                           CHANNELS_TL_sheet,
                           rec.row_seq,
                           'LANG',
                           SQLCODE,
                           SQLERRM);
           L_error := TRUE;
     END;

     if rec.action = CORESVC_BANNER_CHANNELS.action_new then
        L_temp_rec.LANG         := NVL( L_temp_rec.LANG,L_default_rec.LANG);
        L_temp_rec.CHANNEL_NAME := NVL( L_temp_rec.CHANNEL_NAME,L_default_rec.CHANNEL_NAME);
        L_temp_rec.CHANNEL_ID   := NVL( L_temp_rec.CHANNEL_ID,L_default_rec.CHANNEL_ID);
     end if;
     if NOT(L_temp_rec.channel_id is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         CHANNELS_tl_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
     end if;

     if NOT L_error then
            svc_channels_tl_col.EXTEND();
            svc_channels_tl_col(svc_channels_tl_col.COUNT()) := L_temp_rec;
     end if;
END LOOP;

BEGIN
   forall i IN 1..svc_channels_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_CHANNELS_TL st
      using (select (case
                        when L_mi_rec.LANG_mi    = 'N'and
                             svc_channels_tl_col(i).action = CORESVC_BANNER_CHANNELS.action_mod and
                             s1.lang               is NULL then
                             mt.lang
                        else s1.lang
                        end) as lang,
                    (case
                        when L_mi_rec.CHANNEL_NAME_mi    = 'N' and
                             svc_channels_tl_col(i).action    = CORESVC_BANNER_CHANNELS.action_mod and
                             s1.channel_name               is NULL then
                             mt.channel_name
                        else s1.channel_name
                        end) as channel_name,
                    (case
                        when L_mi_rec.CHANNEL_ID_mi      = 'N' and
                             svc_channels_tl_col(i).action    = CORESVC_BANNER_CHANNELS.action_mod and
                             s1.channel_id                 is NULL then
                             mt.channel_id
                        else s1.channel_id
                        end) as channel_id,
                        null as dummy
               from (select svc_channels_tl_col(i).lang         as lang,
                            svc_channels_tl_col(i).channel_name as channel_name,
                            svc_channels_tl_col(i).channel_id   as channel_id
                       from dual) s1,
                            CHANNELS_TL mt
                      where mt.channel_id (+) = s1.channel_id
                        and mt.lang (+)       = s1.lang) sq
         on (st.channel_id = sq.channel_id
             and st.lang = sq.lang
             and svc_channels_tl_col(i).action in (CORESVC_BANNER_CHANNELS.action_mod,
                                                   CORESVC_BANNER_CHANNELS.action_del))
         when matched then
            update
               set process_id        = svc_channels_tl_col(i).process_id ,
                   chunk_id          = svc_channels_tl_col(i).chunk_id ,
                   row_seq           = svc_channels_tl_col(i).row_seq ,
                   action            = svc_channels_tl_col(i).action,
                   process$status    = svc_channels_tl_col(i).process$status ,
                   channel_name      = sq.channel_name
         when NOT matched then
            insert(process_id ,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   lang ,
                   channel_name ,
                   channel_id)
            values(svc_channels_tl_col(i).process_id ,
                   svc_channels_tl_col(i).chunk_id ,
                   svc_channels_tl_col(i).row_seq ,
                   svc_channels_tl_col(i).action ,
                   svc_channels_tl_col(i).process$status ,
                   sq.lang ,
                   sq.channel_name ,
                   sq.channel_id);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            CHANNELS_TL_sheet,
                            svc_channels_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
      END LOOP;
END;
END PROCESS_S9T_CHANNELS_TL;
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR       EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_BANNER(I_file_id,I_process_id);
      PROCESS_S9T_BANNER_TL(I_file_id,I_process_id);
      PROCESS_S9T_CHANNELS(I_file_id,I_process_id);
      PROCESS_S9T_CHANNELS_TL(I_file_id,I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
  insert into s9t_errors
       values Lp_s9t_errors_tab(i);

      update svc_process_tracker
  set status = 'PE',
      file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------
FUNCTION PROCESS_BANNER_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error           IN OUT   BOOLEAN,
                            I_rec             IN       C_SVC_BANNER%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.PROCESS_BANNER_VAL';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BANNER';
   L_exists    BOOLEAN;

BEGIN

   if I_rec.action = action_del then
      if BANNER_SQL.DELETE_BANNER(O_error_message,
                                  L_exists,
                                  I_rec.banner_id)= FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'BANNER_ID',
                     O_error_message);
      elsif L_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'BANNER_ID',
                     'CANT_DELETE_BANNER');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
     return FALSE;
END PROCESS_BANNER_VAL;
--------------------------------------------------------------------------------
FUNCTION EXEC_BANNER_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_banner_temp_rec   IN       BANNER%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64):= 'CORESVC_BANNER_CHANNELS.EXEC_BANNER_INS';

BEGIN
   insert into banner
        values I_banner_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BANNER_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_BANNER_UPD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_banner_temp_rec   IN       BANNER%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.EXEC_BANNER_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BANNER';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_BANNER_UPD is
      select 'x'
        from banner
       where banner_id = I_banner_temp_rec.banner_id
         for update nowait;

BEGIN
   open  C_LOCK_BANNER_UPD;
   close C_LOCK_BANNER_UPD;

   update banner
      set row = I_banner_temp_rec
    where banner_id = I_banner_temp_rec.banner_id;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_banner_temp_rec.banner_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_BANNER_UPD%ISOPEN then
         close C_LOCK_BANNER_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BANNER_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_BANNER_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_banner_temp_rec   IN       BANNER%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.EXEC_BANNER_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BANNER';
   L_exists  BOOLEAN;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    cursor C_CHK_CHANNELS_DEL is
       select rowid rid,
              action,
              channel_id,
              banner_id
         from svc_channels
    where banner_id = I_banner_temp_rec.banner_id;
    
   cursor C_LOCK_BANNER_DEL is
      select 'x'
        from banner
       where banner_id = I_banner_temp_rec.banner_id
         for update nowait;

   cursor C_LOCK_BANNER_TL_DEL is
      select 'x'
        from banner_tl
       where banner_id = I_banner_temp_rec.banner_id
         for update nowait;

   cursor C_CHANNELS_LOCK(CP_channel_id CHANNELS.CHANNEL_ID%TYPE) is
      select 'X'
        from channels
       where channel_id = CP_channel_id
         for update nowait;

   cursor C_CHANNELS_TL_LOCK(CP_channel_id CHANNELS.CHANNEL_ID%TYPE) is
      select 'X'
        from channels_tl
       where channel_id = CP_channel_id
         for update nowait;

BEGIN

   for rec in C_CHK_CHANNELS_DEL loop
      if rec.action = action_del then
         if (CHANNEL_SQL.DELETE_CHANNEL(O_error_message,
                                        L_exists,
                                        rec.channel_id))= FALSE then
            return FALSE;
         end if;

         if L_exists then
            O_error_message := 'ATT_CHANN_ST_WH';
            return FALSE;
         end if;

         open  C_CHANNELS_TL_LOCK(rec.channel_id);
         close C_CHANNELS_TL_LOCK;

         open  C_CHANNELS_LOCK(rec.channel_id);
         close C_CHANNELS_LOCK;

         delete
           from channels_tl
          where channel_id = rec.channel_id;

         delete
           from channels
          where channel_id = rec.channel_id;

         update svc_channels set process$status ='P'
          where rowid = rec.rid;
         
         update svc_channels_tl set process$status ='P'
          where channel_id = rec.channel_id;
      end if;
   end loop;

   if BANNER_SQL.DELETE_BANNER(O_error_message,
                               L_exists,
                               I_banner_temp_rec.banner_id)= FALSE then
      return FALSE;
   end if;

   if L_exists then
      O_error_message := 'CANT_DELETE_BANNER';
      return FALSE;
   end if;
   
   open  C_LOCK_BANNER_TL_DEL;
   close C_LOCK_BANNER_TL_DEL;

   open  C_LOCK_BANNER_DEL;
   close C_LOCK_BANNER_DEL;

   delete from banner_tl
    where banner_id = I_banner_temp_rec.banner_id;

   delete from banner
    where banner_id = I_banner_temp_rec.banner_id;

   update svc_banner_tl set process$status ='P'
    where banner_id = I_banner_temp_rec.banner_id;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_banner_temp_rec.banner_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_BANNER_TL_DEL%ISOPEN then
         close C_LOCK_BANNER_TL_DEL;
      end if;
      if C_LOCK_BANNER_DEL%ISOPEN then
         close C_LOCK_BANNER_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BANNER_DEL;
--------------------------------------------------------------------------------
FUNCTION EXEC_BANNER_TL_INS(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_banner_tl_ins_tab    IN       BANNER_TL_tab)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64):= 'CORESVC_BANNER_CHANNELS.EXEC_BANNER_TL_INS';

BEGIN
   if I_banner_tl_ins_tab is NOT NULL and I_banner_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_banner_tl_ins_tab.COUNT()
         insert into banner_tl
              values I_banner_tl_ins_tab(i);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BANNER_TL_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_BANNER_TL_UPD(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_banner_tl_upd_tab   IN       BANNER_TL_TAB,
                            I_banner_tl_upd_rst   IN       ROW_SEQ_TAB,
                            I_process_id          IN       SVC_BANNER_TL.PROCESS_ID%TYPE,
                            I_chunk_id            IN       SVC_BANNER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.EXEC_BANNER_TL_UPD';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'BANNER_TL';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1        VARCHAR2(30) := NULL;
   L_key_val2        VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_BANNER_TL_UPD(I_banner_id  BANNER_TL.BANNER_ID%TYPE,
                               I_lang       BANNER_TL.LANG%TYPE) is
      select 'x'
        from banner_tl
       where banner_id = I_banner_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_banner_tl_upd_tab is NOT NULL and I_banner_tl_upd_tab.count > 0 then
      for i in I_banner_tl_upd_tab.FIRST..I_banner_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_banner_tl_upd_tab(i).lang);
            L_key_val2 := 'Banner ID: '||to_char(I_banner_tl_upd_tab(i).banner_id);
            open C_LOCK_BANNER_TL_UPD(I_banner_tl_upd_tab(i).banner_id,
                                          I_banner_tl_upd_tab(i).lang);
            close C_LOCK_BANNER_TL_UPD;
            
            update banner_tl
               set banner_name = I_banner_tl_upd_tab(i).banner_name,
                   last_update_id = I_banner_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_banner_tl_upd_tab(i).last_update_datetime
             where lang = I_banner_tl_upd_tab(i).lang
               and banner_id = I_banner_tl_upd_tab(i).banner_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table,
                              I_banner_tl_upd_rst(i),
                              NULL,
                              O_error_message);
         END;
      END LOOP;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_BANNER_TL_UPD%ISOPEN then
         close C_LOCK_BANNER_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BANNER_TL_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_BANNER_TL_DEL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_banner_tl_del_tab   IN       BANNER_TL_TAB,
                            I_banner_tl_del_rst   IN       ROW_SEQ_TAB,
                            I_process_id          IN       SVC_BANNER_TL.PROCESS_ID%TYPE,
                            I_chunk_id            IN       SVC_BANNER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.EXEC_BANNER_TL_DEL';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BANNER_TL';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1        VARCHAR2(30) := NULL;
   L_key_val2        VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_BANNER_TL_DEL(I_banner_id  BANNER.BANNER_ID%TYPE,
                               I_lang       BANNER_TL.LANG%TYPE) is
      select 'x'
        from banner_tl
       where banner_id = I_banner_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_banner_tl_del_tab is NOT NULL and I_banner_tl_del_tab.count > 0 then
      for i in I_banner_tl_del_tab.FIRST..I_banner_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_banner_tl_del_tab(i).lang);
            L_key_val2 := 'Banner ID: '||to_char(I_banner_tl_del_tab(i).banner_id);
            open C_LOCK_BANNER_TL_DEL(I_banner_tl_del_tab(i).banner_id,
                                      I_banner_tl_del_tab(i).lang);
            close C_LOCK_BANNER_TL_DEL;
            delete banner_tl
             where lang = I_banner_tl_del_tab(i).lang
               and banner_id = I_banner_tl_del_tab(i).banner_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_banner_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_BANNER_TL_DEL%ISOPEN then
         close C_LOCK_BANNER_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_BANNER_TL_DEL;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id        IN     SVC_BANNER.PROCESS_ID%TYPE) IS

BEGIN
   delete from svc_banner_tl
         where process_id= I_process_id;

   delete from svc_banner
         where process_id= I_process_id;

   delete from svc_channels_tl
    where process_id= I_process_id;

   delete from svc_channels
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;

--------------------------------------------------------------------------------
FUNCTION PROCESS_BANNER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_BANNER.process_id%TYPE,
                        I_chunk_id        IN       SVC_BANNER.chunk_id%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.PROCESS_BANNER';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BANNER';
   L_error             BOOLEAN := FALSE;
   L_process_error     BOOLEAN := FALSE;
   L_banner_temp_rec   BANNER%ROWTYPE;

BEGIN
   FOR rec IN C_SVC_BANNER(I_process_id,
                           I_chunk_id)
   LOOP
      L_error := FALSE;
      if rec.action IS NULL
         or rec.action NOT in (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
        and rec.PK_BANNER_rid is NOT NULL then
        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.nextval,
                    I_chunk_id,
                    L_table,
                    rec.row_seq,
                    'BANNER_ID',
                    'BAN_EXISTS');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod, action_del)
        and rec.PK_BANNER_rid IS NULL
        and rec.banner_id is NOT NULL then
        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.nextval,
                    I_chunk_id,
                    L_table,
                    rec.row_seq,
                    'BANNER_ID',
                    'INV_BANNER');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new, action_mod)
        and rec.banner_name IS NULL then
        WRITE_ERROR(I_process_id,
                    SVC_ADMIN_UPLD_ER_SEQ.nextval,
                    I_chunk_id,
                    L_table,
                    rec.row_seq,
                    'BANNER_NAME',
                    'ENT_BANNER_NAME');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.banner_id < 1 then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'BANNER_ID',
                     'GREATER_0');
        L_error :=TRUE;
      end if;

      /*if rec.action IN (action_new, action_del) then
        if PROCESS_BANNER_VAL(O_error_message,
                              L_error,
                              rec) = FALSE then
            WRITE_ERROR(rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        rec.chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error := TRUE;
        end if;
      end if;*/
      if NOT L_error then
         L_banner_temp_rec.banner_name     := rec.banner_name;
         L_banner_temp_rec.banner_id       := rec.banner_id;
         L_banner_temp_rec.CREATE_ID       := GET_USER;
         L_banner_temp_rec.CREATE_DATETIME := SYSDATE;
         if rec.action = action_new then
            if EXEC_BANNER_INS(O_error_message,
                               L_banner_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_BANNER_UPD(O_error_message,
                               L_banner_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_BANNER_DEL(O_error_message,
                               L_banner_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;

   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_BANNER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_BANNER_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_BANNER_TL.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_BANNER_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.PROCESS_BANNER_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_BANNER_TL';
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'BANNER_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'BANNER';
   L_error                BOOLEAN := FALSE;
   L_banner_tl_temp_rec   BANNER_TL%ROWTYPE;

   cursor C_SVC_BANNER_TL(I_process_id NUMBER,
                          I_chunk_id NUMBER) is
      select pk_banner_tl.rowid as pk_banner_tl_rid,
             fk_banner.rowid    as fk_banner_rid,
             fk_lang.rowid      as fk_lang_rid,
             st.lang,
             st.banner_id,
             st.banner_name,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_banner_tl  st,
             banner         fk_banner,
             banner_tl      pk_banner_tl,
             lang           fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.banner_id   =  fk_banner.banner_id (+)
         and st.lang        =  pk_banner_tl.lang (+)
         and st.banner_id   =  pk_banner_tl.banner_id (+)
         and st.lang        =  fk_lang.lang (+)
         and (st.process$status = 'N' or st.process$status  is NULL);

   TYPE SVC_BANNER_TL is TABLE OF C_SVC_BANNER_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_banner_tl_tab         SVC_BANNER_TL;
   L_banner_tl_upd_rst         ROW_SEQ_TAB;
   L_banner_tl_del_rst         ROW_SEQ_TAB;

   L_banner_tl_ins_tab         BANNER_TL_tab         := NEW BANNER_TL_tab();
   L_banner_tl_upd_tab         BANNER_TL_tab         := NEW BANNER_TL_tab();
   L_banner_tl_del_tab         BANNER_TL_tab         := NEW BANNER_TL_tab();


BEGIN
   if C_SVC_BANNER_TL%ISOPEN then
      close C_SVC_BANNER_TL;
   end if;

   open C_SVC_BANNER_TL(I_process_id,
                        I_chunk_id);
   LOOP
      fetch C_SVC_BANNER_TL bulk collect into L_svc_banner_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_banner_tl_tab.COUNT > 0 then
         FOR i in L_svc_banner_tl_tab.FIRST..L_svc_banner_tl_tab.LAST LOOP
            L_error := FALSE;

            --check for primary_lang
            if L_svc_banner_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_banner_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if action is valid
            if L_svc_banner_tl_tab(i).action is NULL
               or L_svc_banner_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_banner_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            -- check if primary key exists
            if L_svc_banner_tl_tab(i).action = action_new
               and L_svc_banner_tl_tab(i).pk_banner_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_banner_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_banner_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_banner_tl_tab(i).lang is NOT NULL
               and L_svc_banner_tl_tab(i).banner_id is NOT NULL
               and L_svc_banner_tl_tab(i).pk_banner_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_banner_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_banner_tl_tab(i).action = action_new
               and L_svc_banner_tl_tab(i).banner_id is NOT NULL
               and L_svc_banner_tl_tab(i).fk_banner_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_banner_tl_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_banner_tl_tab(i).action = action_new
               and L_svc_banner_tl_tab(i).lang is NOT NULL
               and L_svc_banner_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_banner_tl_tab(i).row_seq,
                            'LANG',
                            'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_banner_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_banner_tl_tab(i).banner_name is NULL then
                  WRITE_ERROR( I_process_id,
                               svc_admin_upld_er_seq.NEXTVAL,
                               I_chunk_id,
                               L_table,
                               L_svc_banner_tl_tab(i).row_seq,
                              'BANNER_NAME',
                              'ENT_BANNER_NAME');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_banner_tl_tab(i).banner_id is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_banner_tl_tab(i).row_seq,
                           'BANNER_ID',
                           'ENT_BANNER_ID');
               L_error :=TRUE;
            end if;

            if L_svc_banner_tl_tab(i).lang is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_banner_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_banner_tl_temp_rec.lang := L_svc_banner_tl_tab(i).lang;
               L_banner_tl_temp_rec.banner_id := L_svc_banner_tl_tab(i).banner_id;
               L_banner_tl_temp_rec.banner_name := L_svc_banner_tl_tab(i).banner_name;
               L_banner_tl_temp_rec.create_datetime := SYSDATE;
               L_banner_tl_temp_rec.create_id := GET_USER;
               L_banner_tl_temp_rec.last_update_datetime := SYSDATE;
               L_banner_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_banner_tl_tab(i).action = action_new then
                  L_banner_tl_ins_tab.extend;
                  L_banner_tl_ins_tab(L_banner_tl_ins_tab.count()) := L_banner_tl_temp_rec;
               end if;

               if L_svc_banner_tl_tab(i).action = action_mod then
                  L_banner_tl_upd_tab.extend;
                  L_banner_tl_upd_tab(L_banner_tl_upd_tab.count()) := L_banner_tl_temp_rec;
                  L_banner_tl_upd_rst(L_banner_tl_upd_tab.count()) := L_svc_banner_tl_tab(i).row_seq;
               end if;

               if L_svc_banner_tl_tab(i).action = action_del then
                  L_banner_tl_del_tab.extend;
                  L_banner_tl_del_tab(L_banner_tl_del_tab.count()) := L_banner_tl_temp_rec;
                  L_banner_tl_del_rst(L_banner_tl_del_tab.count()) := L_svc_banner_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_BANNER_TL%NOTFOUND;
   END LOOP;
   close C_SVC_BANNER_TL;

   if EXEC_BANNER_TL_INS(O_error_message,
                         L_banner_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_BANNER_TL_UPD(O_error_message,
                         L_banner_tl_upd_tab,
                         L_banner_tl_upd_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_BANNER_TL_DEL(O_error_message,
                         L_banner_tl_del_tab,
                         L_banner_tl_del_rst,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_BANNER_TL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_CHANNELS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_error             IN OUT BOOLEAN,
                              I_rec               IN     C_SVC_CHANNELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                       := 'CORESVC_BANNER_CHANNELS.PROCESS_CHANNELS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_CHANNELS';
   L_exists  BOOLEAN;
BEGIN
   if I_rec.action = action_new and I_rec.channel_id < 1 then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'CHANNEL_ID',
                  'CH_ID_NOT_IN_RANGE');
      O_error :=TRUE;
   end if;

   if I_rec.action = action_del then
      if (CHANNEL_SQL.DELETE_CHANNEL(O_error_message,
                                     L_exists,
                                     I_rec.channel_id))= FALSE then
          WRITE_ERROR(I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'CHANNEL_ID',
                      O_error_message);
          O_error :=TRUE;
      end if;
      if L_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CHANNEL_ID',
                     'ATT_CHANN_ST_WH');
         O_error :=TRUE;

      end if;

   end if;

   if I_rec.action IN (action_new,action_mod)
      and I_rec.banner_id is NOT NULL
      and I_rec.fk_banner_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'BANNER_ID',
                  'INV_BANNER');
      O_error :=TRUE;
   end if;

   if I_rec.action IN (action_new,action_mod)
      and I_rec.channel_type is NOT NULL
      and I_rec.cd_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'CHANNEL_TYPE',
                  'INV_CHTY');
      O_error :=TRUE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));

      return FALSE;
END PROCESS_VAL_CHANNELS;
--------------------------------------------------------------------------------
FUNCTION EXEC_CHANNELS_INS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_channels_temp_rec IN     CHANNELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_BANNER_CHANNELS.EXEC_CHANNELS_INS';

BEGIN
   insert into channels
        values I_channels_temp_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;

END EXEC_CHANNELS_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_CHANNELS_UPD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_channels_temp_rec IN     CHANNELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64):='CORESVC_BANNER_CHANNELS.EXEC_CHANNELS_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_CHANNELS_LOCK is
      select 'X'
        from channels
       where channel_id = I_channels_temp_rec.channel_id
         for update nowait;
BEGIN
   open  C_CHANNELS_LOCK;
   close C_CHANNELS_LOCK;

   update channels
      set row = I_channels_temp_rec
    where channel_id = I_channels_temp_rec.channel_id;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'CHANNELS',
                                                                I_channels_temp_rec.channel_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_CHANNELS_LOCK%ISOPEN then
         close C_CHANNELS_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;

END EXEC_CHANNELS_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_CHANNELS_DEL(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_channels_temp_rec IN     CHANNELS%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64):='CORESVC_BANNER_CHANNELS.EXEC_CHANNELS_DEL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_CHANNELS_LOCK is
      select 'X'
        from channels
       where channel_id = I_channels_temp_rec.channel_id
         for update nowait;

   cursor C_CHANNELS_TL_LOCK is
      select 'X'
        from channels_tl
       where channel_id = I_channels_temp_rec.channel_id
         for update nowait;
BEGIN
   open  C_CHANNELS_TL_LOCK;
   close C_CHANNELS_TL_LOCK;

   open  C_CHANNELS_LOCK;
   close C_CHANNELS_LOCK;

   delete
     from channels_tl
    where channel_id = I_channels_temp_rec.channel_id;

   delete
     from channels
    where channel_id = I_channels_temp_rec.channel_id;

   update svc_channels_tl set process$status ='P'
      where channel_id = I_channels_temp_rec.channel_id;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'CHANNELS',
                                                                I_channels_temp_rec.channel_id,
                                                                NULL);
      return FALSE;

   when OTHERS then
      if C_CHANNELS_LOCK%ISOPEN then
         close C_CHANNELS_LOCK;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;

END EXEC_CHANNELS_DEL;
--------------------------------------------------------------------------------
FUNCTION EXEC_CHANNELS_TL_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_channels_tl_ins_tab    IN       CHANNELS_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.EXEC_CHANNELS_TL_INS';

BEGIN
   if I_channels_tl_ins_tab is NOT NULL and I_channels_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_channels_tl_ins_tab.COUNT()
         insert into channels_tl
              values I_channels_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_CHANNELS_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CHANNELS_TL_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_channels_tl_upd_tab   IN       CHANNELS_TL_TAB,
                              I_channels_tl_upd_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_CHANNELS_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_CHANNELS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_RTK_ERRORS.EXEC_CHANNELS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CHANNELS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CHANNELS_TL_UPD(I_channel  CHANNELS_TL.CHANNEL_ID%TYPE,
                                 I_lang     CHANNELS_TL.LANG%TYPE) is
      select 'x'
        from channels_tl
       where channel_id = I_channel
         and lang = I_lang
         for update nowait;

BEGIN
   if I_channels_tl_upd_tab is NOT NULL and I_channels_tl_upd_tab.count > 0 then
      for i in I_channels_tl_upd_tab.FIRST..I_channels_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_channels_tl_upd_tab(i).lang);
            L_key_val2 := 'Channel ID: '||to_char(I_channels_tl_upd_tab(i).channel_id);
            open C_LOCK_CHANNELS_TL_UPD(I_channels_tl_upd_tab(i).channel_id,
                                        I_channels_tl_upd_tab(i).lang);
            close C_LOCK_CHANNELS_TL_UPD;
            
            update channels_tl
               set channel_name = I_channels_tl_upd_tab(i).channel_name,
                   last_update_id = I_channels_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_channels_tl_upd_tab(i).last_update_datetime
             where lang = I_channels_tl_upd_tab(i).lang
               and channel_id = I_channels_tl_upd_tab(i).channel_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table,
                              I_channels_tl_upd_rst(i),
                              NULL,
                              O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CHANNELS_TL_UPD%ISOPEN then
         close C_LOCK_CHANNELS_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CHANNELS_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_CHANNELS_TL_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_channels_tl_del_tab   IN       CHANNELS_TL_TAB,
                              L_channels_tl_del_rst   IN       ROW_SEQ_TAB,
                              I_process_id            IN       SVC_CHANNELS_TL.PROCESS_ID%TYPE,
                              I_chunk_id              IN       SVC_CHANNELS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.EXEC_CHANNELS_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CHANNELS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_CHANNELS_TL_DEL(I_channel_id  CHANNELS_TL.CHANNEL_ID%TYPE,
                                I_lang        CHANNELS_TL.LANG%TYPE) is
      select 'x'
        from channels_tl
       where channel_id = I_channel_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_channels_tl_del_tab is NOT NULL and I_channels_tl_del_tab.count > 0 then
      for i in I_channels_tl_del_tab.FIRST..I_channels_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_channels_tl_del_tab(i).lang);
            L_key_val2 := 'Channel ID: '||to_char(I_channels_tl_del_tab(i).channel_id);
            open C_LOCK_CHANNELS_TL_DEL(I_channels_tl_del_tab(i).channel_id,
                                        I_channels_tl_del_tab(i).lang);
            close C_LOCK_CHANNELS_TL_DEL;
            
         delete channels_tl
          where lang = I_channels_tl_del_tab(i).lang
            and channel_id = I_channels_tl_del_tab(i).channel_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table,
                              L_channels_tl_del_rst(i),
                              NULL,
                              O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_CHANNELS_TL_DEL%ISOPEN then
         close C_LOCK_CHANNELS_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_CHANNELS_TL_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CHANNELS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_process_id        IN     SVC_CHANNELS.PROCESS_ID%TYPE,
                          I_chunk_id          IN     SVC_CHANNELS.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                       := 'CORESVC_BANNER_CHANNELS.PROCESS_CHANNELS';
   L_error             BOOLEAN;
   L_channels_temp_rec CHANNELS%ROWTYPE;
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_CHANNELS';
   L_process_error     BOOLEAN                            := FALSE;

BEGIN
   FOR rec in C_SVC_CHANNELS(I_process_id,
                             I_chunk_id)
   LOOP
      L_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,
                               action_mod,
                               action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action = action_new
         and rec.channel_id is NOT NULL
         and rec.pk_channels_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CHANNEL_ID',
                     'CHAN_EXISTS');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.channel_id is NOT NULL
         and rec.PK_channels_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CHANNEL_ID',
                     'INV_CHANNEL');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
         and rec.banner_id is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'BANNER_ID',
                     'ENT_BANNER_ID');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
         and rec.channel_type is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CHANNEL_TYPE',
                     'ENT_CHAN_TYPE');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_new,action_mod)
         and rec.channel_name is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CHANNEL_NAME',
                     'ENT_CHAN_NAME');
         L_error :=TRUE;
      end if;

      if PROCESS_VAL_CHANNELS(O_error_message,
                              L_error,
                              rec)=FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error:=TRUE;
      end if;

     if NOT L_error  then
        L_channels_temp_rec.banner_id                 := rec.banner_id;
        L_channels_temp_rec.channel_type              := rec.channel_type;
        L_channels_temp_rec.channel_name              := rec.channel_name;
        L_channels_temp_rec.channel_id                := rec.channel_id;
        L_channels_temp_rec.create_id                 := GET_USER;
        L_channels_temp_rec.create_datetime           := SYSDATE;
        if rec.action IN (action_new) then
           if EXEC_CHANNELS_INS(O_error_message,
                                L_channels_temp_rec)=FALSE then
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                          NULL,
                          O_error_message);

           L_process_error:=TRUE;
           end if;
        end if;
        if rec.action IN (action_mod) then
           if EXEC_CHANNELS_UPD(O_error_message,
                                L_channels_temp_rec)=FALSE then
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                          NULL,
                          O_error_message);

              L_process_error:=TRUE;
           end if;
        end if;

        if rec.action IN (action_del) then

           if EXEC_CHANNELS_DEL(O_error_message,
                                L_channels_temp_rec)= FALSE then
              WRITE_ERROR(I_process_id,
                          SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                          I_chunk_id,
                          L_table,
                          rec.row_seq,
                          NULL,
                          O_error_message);

              L_process_error:=TRUE;
           end if;
        end if;

     end if;

   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_CHANNELS%ISOPEN then
         close C_SVC_CHANNELS;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return FALSE;
END PROCESS_CHANNELS;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CHANNELS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_CHANNELS_TL.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_CHANNELS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64) := 'CORESVC_BANNER_CHANNELS.PROCESS_CHANNELS_TL';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_CHANNELS_TL';
   L_error_message          RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'CHANNELS_TL';
   L_error                  BOOLEAN := FALSE;
   L_channels_tl_temp_rec   CHANNELS_TL%ROWTYPE;

   cursor C_SVC_CHANNELS_TL(I_process_id NUMBER,
                           I_chunk_id NUMBER) is
      select pk_channels_tl.rowid  as pk_channels_tl_rid,
             fk_channels.rowid     as fk_channels_rid,
             fk_lang.rowid         as fk_lang_rid,
             st.lang,
             st.channel_id,
             st.channel_name,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_channels_tl  st,
             channels         fk_channels,
             channels_tl      pk_channels_tl,
             lang             fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.channel_id  =  fk_channels.channel_id (+)
         and st.lang        =  pk_channels_tl.lang (+)
         and st.channel_id  =  pk_channels_tl.channel_id (+)
         and st.lang        =  fk_lang.lang (+)
         and (st.process$status = 'N' or st.process$status  is NULL);


   TYPE SVC_CHANNELS_TL is TABLE OF C_SVC_CHANNELS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_channels_tl_tab         SVC_CHANNELS_TL;
   L_channels_tl_upd_rst         ROW_SEQ_TAB;
   L_channels_tl_del_rst         ROW_SEQ_TAB;

   L_CHANNELS_TL_ins_tab         CHANNELS_TL_tab         := NEW CHANNELS_TL_tab();
   L_CHANNELS_TL_upd_tab         CHANNELS_TL_tab         := NEW CHANNELS_TL_tab();
   L_CHANNELS_TL_del_tab         CHANNELS_TL_tab         := NEW CHANNELS_TL_tab();

BEGIN
   if C_SVC_CHANNELS_TL%ISOPEN then
      close C_SVC_CHANNELS_TL;
   end if;

   open C_SVC_CHANNELS_TL(I_process_id,
                          I_chunk_id);
   LOOP
      fetch C_SVC_CHANNELS_TL bulk collect into L_svc_channels_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_channels_tl_tab.COUNT > 0 then
         FOR i in L_svc_channels_tl_tab.FIRST..L_svc_channels_tl_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_channels_tl_tab(i).action is NULL
               or L_svc_channels_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_channels_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_channels_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_channels_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_channels_tl_tab(i).action = action_new
               and L_svc_channels_tl_tab(i).pk_channels_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_channels_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_channels_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_channels_tl_tab(i).lang is NOT NULL
               and L_svc_channels_tl_tab(i).channel_id is NOT NULL
               and L_svc_channels_tl_tab(i).pk_channels_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_channels_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_channels_tl_tab(i).action = action_new
               and L_svc_channels_tl_tab(i).channel_id is NULL
               and L_svc_channels_tl_tab(i).fk_channels_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_channels_tl_tab(i).row_seq,
                            'CHANNEL_ID',
                            'INV_CHANNEL');
               L_error :=TRUE;
            end if;

            if L_svc_channels_tl_tab(i).action = action_new
               and L_svc_channels_tl_tab(i).lang is NOT NULL
               and L_svc_channels_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_channels_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_channels_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_channels_tl_tab(i).channel_name is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_channels_tl_tab(i).row_seq,
                              'CHANNEL_NAME',
                              'ENT_CHAN_NAME');
                  L_error :=TRUE;
               end if;
             end if;

            if L_svc_channels_tl_tab(i).channel_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_channels_tl_tab(i).row_seq,
                           'CHANNEL_ID',
                           'ENT_CHANNEL_ID');
               L_error :=TRUE;
            end if;

            if L_svc_channels_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_SVC_CHANNELS_TL_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_channels_tl_temp_rec.lang := L_svc_channels_tl_tab(i).lang;
               L_channels_tl_temp_rec.channel_id := L_svc_channels_tl_tab(i).channel_id;
               L_channels_tl_temp_rec.channel_name := L_svc_channels_tl_tab(i).channel_name;
               L_channels_tl_temp_rec.create_datetime := SYSDATE;
               L_channels_tl_temp_rec.create_id := GET_USER;
               L_channels_tl_temp_rec.last_update_datetime := SYSDATE;
               L_channels_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_channels_tl_tab(i).action = action_new then
                  L_channels_tl_ins_tab.extend;
                  L_channels_tl_ins_tab(L_channels_tl_ins_tab.count()) := L_channels_tl_temp_rec;
               end if;

               if L_svc_channels_tl_tab(i).action = action_mod then
                  L_channels_tl_upd_tab.extend;
                  L_channels_tl_upd_tab(L_channels_tl_upd_tab.count()) := L_channels_tl_temp_rec;
                  L_channels_tl_upd_rst(L_channels_tl_upd_tab.count()) := L_svc_channels_tl_tab(i).row_seq;
               end if;

               if L_svc_channels_tl_tab(i).action = action_del then
                  L_channels_tl_del_tab.extend;
                  L_channels_tl_del_tab(L_channels_tl_del_tab.count()) := L_channels_tl_temp_rec;
                  L_channels_tl_del_rst(L_channels_tl_del_tab.count()) := L_svc_channels_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_CHANNELS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_CHANNELS_TL;

   if EXEC_CHANNELS_TL_INS(O_error_message,
                           L_channels_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_CHANNELS_TL_UPD(O_error_message,
                           L_channels_tl_upd_tab,
                           L_channels_tl_upd_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_CHANNELS_TL_DEL(O_error_message,
                           L_channels_tl_del_tab,
                           L_channels_tl_del_rst,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_CHANNELS_TL;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR(64) := 'CORESVC_BANNER_CHANNELS.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
     from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
     from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   Lp_errors_tab := NEW errors_tab_typ();
   if PROCESS_BANNER(O_error_message,
                     I_process_id,
                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_BANNER_TL(O_error_message,
                        I_process_id,
                        I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_CHANNELS(O_error_message,
                       I_process_id,
                       I_chunk_id)=FALSE then
       return FALSE;
   end if;

   if PROCESS_CHANNELS_TL(O_error_message,
                          I_process_id,
                          I_chunk_id)=FALSE then
       return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;

EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_BANNER_CHANNELS;
/