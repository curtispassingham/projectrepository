-- File Name : CORESVC_NON_MERCH_CODE_body.pls
create or replace PACKAGE BODY CORESVC_NON_MERCH_CODE AS
   cursor C_SVC_NON_MER_COMP(I_process_id NUMBER,I_chunk_id NUMBER) is
      select pk_non_merch_code_comp.rowid   as pk_non_merch_code_comp_rid,
             mmc_nmc_fk.rowid               as mmc_nmc_fk_rid,
             pk_non_merch_code_comp.comp_id as old_comp_id,
             upper(st.comp_id) as comp_id,
             upper(st.non_merch_code) as non_merch_code,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)               as action,
             st.process$status
        from svc_non_merch_code_comp  st,
             non_merch_code_comp      pk_non_merch_code_comp,
             non_merch_code_head      mmc_nmc_fk
       where st.process_id             = I_process_id
         and st.chunk_id               = I_chunk_id
         and upper(st.comp_id)                = pk_non_merch_code_comp.comp_id (+)
         and upper(st.non_merch_code)         = pk_non_merch_code_comp.non_merch_code (+)
         and upper(st.non_merch_code)         = mmc_nmc_fk.non_merch_code (+);

   cursor C_SVC_NON_MER_TL(I_process_id NUMBER,I_chunk_id NUMBER) is
      select pk_non_merch_code_head_tl.rowid  as pk_non_merch_code_head_tl_rid,
             nmcht_nmc_fk.rowid               as nmcht_nmc_fk_rid,
             st.non_merch_code_desc,
             upper(st.non_merch_code) as non_merch_code,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)                 as action,
             st.process$status
        from svc_non_merch_code_head_tl st,
             non_merch_code_head_tl     pk_non_merch_code_head_tl,
             non_merch_code_head        nmcht_nmc_fk
       where st.process_id            = I_process_id
         and st.chunk_id              = I_chunk_id
         and upper(st.non_merch_code) = pk_non_merch_code_head_tl.non_merch_code (+)
         and upper(st.non_merch_code) = nmcht_nmc_fk.non_merch_code (+);

   LP_comp_desc          ELC_COMP.COMP_DESC%TYPE := 'code';
   TYPE errors_tab_typ is TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab         ERRORS_TAB_TYP;
   TYPE s9t_errors_tab_typ is TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab     S9T_ERRORS_TAB_TYP;

   Type NON_MER_LANG_TAB IS TABLE OF NON_MERCH_CODE_HEAD_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang       LANG.LANG%TYPE;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
  RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
-------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.extend();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(CASE
                                                                        WHEN I_sqlcode IS NULL THEN
                                                                             I_sqlerrm
                                                                        ELSE 'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                        END);
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
----------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                       I_error_type   IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
---------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets            S9T_PKG.NAMES_MAP_TYP;
   non_mer_comp_cols   S9T_PKG.NAMES_MAP_TYP;
   non_mer_tl_cols     S9T_PKG.NAMES_MAP_TYP;
   non_mer_lang_cols   S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                           := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   non_mer_tl_cols                    := S9T_PKG.GET_COL_NAMES(I_file_id,non_mer_tl_sheet);
   non_mer_tl$action                  := non_mer_tl_cols('ACTION');
   non_mer_tl$non_merch_code          := non_mer_tl_cols('NON_MERCH_CODE');
   non_mer_tl$non_merch_code_desc     := non_mer_tl_cols('NON_MERCH_CODE_DESC');

   non_mer_comp_cols                  := S9T_PKG.GET_COL_NAMES(I_file_id,non_mer_comp_sheet);
   non_mer_comp$action                := non_mer_comp_cols('ACTION');
   non_mer_comp$non_merch_code        := non_mer_comp_cols('NON_MERCH_CODE');
   non_mer_comp$comp_id               := non_mer_comp_cols('COMP_ID');

   non_mer_lang_cols                  := S9T_PKG.GET_COL_NAMES(I_file_id,non_mer_lang_sheet);
   non_mer_lang$action                := non_mer_lang_cols('ACTION');
   non_mer_lang$lang                  := non_mer_lang_cols('LANG');
   non_mer_lang$non_merch_code        := non_mer_lang_cols('NON_MERCH_CODE');
   non_mer_lang$nmc_desc              := non_mer_lang_cols('NON_MERCH_CODE_DESC');

END POPULATE_NAMES;
--------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NON_MER_COMP( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = NON_MER_COMP_sheet )
   select s9t_row(s9t_cells( NULL,
                             NON_MERCH_CODE,
                             COMP_ID
                           ))
     from non_merch_code_comp ;
END POPULATE_NON_MER_COMP;
--------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NON_MER_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = NON_MER_TL_sheet )
   select s9t_row(s9t_cells( CORESVC_NON_MERCH_CODE.action_mod,
                             NON_MERCH_CODE,
                             NON_MERCH_CODE_DESC
                           ))
     from non_merch_code_head_tl tl
    where tl.lang = LP_primary_lang;
END POPULATE_NON_MER_TL;
--------------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NON_MER_LANG( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id    = I_file_id
                    and ss.sheet_name = non_mer_lang_sheet)
   select s9t_row(s9t_cells( CORESVC_NON_MERCH_CODE.action_mod,
                             lang,
                             non_merch_code,
                             non_merch_code_desc
                           ))
     from non_merch_code_head_tl tl
    where tl.lang <> LP_primary_lang;

END POPULATE_NON_MER_LANG;
------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file      S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(non_mer_tl_sheet);
   L_file.sheets(L_file.get_sheet_index(non_mer_tl_sheet)).column_headers := s9t_cells('ACTION',
                                                                                       'NON_MERCH_CODE',
                                                                                       'NON_MERCH_CODE_DESC'
                                                                                      );

   L_file.add_sheet(non_mer_comp_sheet);
   L_file.sheets(L_file.get_sheet_index(non_mer_comp_sheet)).column_headers := s9t_cells('ACTION',
                                                                                         'NON_MERCH_CODE',
                                                                                         'COMP_ID'
                                                                                        );

   L_file.add_sheet(non_mer_lang_sheet);
   L_file.sheets(L_file.get_sheet_index(non_mer_lang_sheet)).column_headers := s9t_cells('ACTION',
                                                                                         'LANG',
                                                                                         'NON_MERCH_CODE',
                                                                                         'NON_MERCH_CODE_DESC'
                                                                                        );

   S9T_PKG.SAVE_OBJ(L_file);

END INIT_S9T;
--------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file    S9T_FILE;
   L_program VARCHAR2(64):='CORESVC_NON_MERCH_CODE.CREATE_S9T';
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_NON_MER_TL(O_file_id);
      POPULATE_NON_MER_COMP(O_file_id);
      POPULATE_NON_MER_LANG(O_file_id);
      COMMIT;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_NON_MER_COMP( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id IN   SVC_NON_MERCH_CODE_COMP.PROCESS_ID%TYPE) IS

   TYPE svc_NON_MER_COMP_col_typ is TABLE OF SVC_NON_MERCH_CODE_COMP%ROWTYPE;
   L_temp_rec           SVC_NON_MERCH_CODE_COMP%ROWTYPE;
   svc_NON_MER_COMP_col SVC_NON_MER_COMP_COL_TYP                  :=NEW svc_NON_MER_COMP_col_typ();
   L_process_id         SVC_NON_MERCH_CODE_COMP.PROCESS_ID%TYPE;
   L_error              BOOLEAN                                   :=FALSE;
   L_default_rec        SVC_NON_MERCH_CODE_COMP%ROWTYPE;
   cursor C_MANDATORY_IND is
      select non_merch_code_mi,
             comp_id_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'NON_MERCH_CODE_COMP'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('NON_MERCH_CODE' as non_merch_code,
                                            'COMP_ID'        as comp_id,
                                             NULL            as dummy));
   L_mi_rec   C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA     exception_init(dml_errors, -24381);
    L_table         VARCHAR2(30)   := 'SVC_NON_MERCH_CODE_COMP';
   L_pk_columns    VARCHAR2(255)  := 'Component ID,Non Merchandise Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   FOR rec IN (select non_merch_code_dv,
                      comp_id_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  =  template_key
                          and wksht_key                                     = 'NON_MERCH_CODE_COMP'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('NON_MERCH_CODE' as non_merch_code,
                                                     'COMP_ID'        as comp_id,
                                                      NULL            as dummy)))
   LOOP
      BEGIN
         L_default_rec.comp_id := rec.comp_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'NON_MERCH_CODE_COMP',
                             NULL,
                            'COMP_ID',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.non_merch_code := rec.non_merch_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR( I_file_id,
                            'NON_MERCH_CODE_COMP',
                             NULL,
                            'NON_MERCH_CODE',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(non_mer_comp$action)                as action,
                     UPPER(r.get_cell(non_mer_comp$non_merch_code))        as non_merch_code,
                     UPPER(r.get_cell(non_mer_comp$comp_id))               as comp_id,
                     r.get_row_seq()                                as row_seq
               from s9t_folder sf,
                    TABLE(sf.s9t_file_obj.sheets) ss,
                    TABLE(ss.s9t_rows) r
              where sf.file_id    = I_file_id
                and ss.sheet_name = GET_SHEET_NAME_TRANS(non_mer_comp_sheet)
             )
   LOOP
       L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := false;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_COMP_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.comp_id := rec.comp_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_COMP_sheet,
                            rec.row_seq,
                            'COMP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
         L_temp_rec.non_merch_code := rec.non_merch_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_COMP_sheet,
                            rec.row_seq,
                            'NON_MERCH_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      if rec.action = CORESVC_NON_MERCH_CODE.action_new then
         L_temp_rec.non_merch_codE := NVL( L_temp_rec.non_merch_code,L_default_rec.non_merch_code);
         L_temp_rec.comp_id        := NVL( L_temp_rec.comp_id,L_default_rec.comp_id);
      end if;
      if not (L_temp_rec.comp_id is not NULL
              and L_temp_rec.non_merch_code is not NULL
              and 1 = 1 )then
         WRITE_S9T_ERROR( I_file_id,
                          NON_MER_COMP_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := true;
      end if;
      if NOT L_error then
         svc_non_mer_comp_col.extend();
         svc_non_mer_comp_col(svc_non_mer_comp_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      FORALL i IN 1..svc_non_mer_comp_col.COUNT SAVE EXCEPTIONS
         merge into svc_non_merch_code_comp st
         using(select (case
                       when L_mi_rec.non_merch_code_mi     = 'N'
                       and svc_non_mer_comp_col(i).action = CORESVC_NON_MERCH_CODE.action_mod
                       and s1.non_merch_code             IS NULL
                      then mt.non_merch_code
                      else s1.non_merch_code
                       end) as non_merch_code,
                      (case
                      when L_mi_rec.comp_id_mi            = 'N'
                       and svc_non_mer_comp_col(i).action = CORESVC_NON_MERCH_CODE.action_mod
                       and s1.comp_id             IS NULL
                      then mt.comp_id
                      else s1.comp_id
                       end) as comp_id,
                       null as dummy
                 from (select svc_non_mer_comp_col(i).non_merch_code as non_merch_code,
                              svc_non_mer_comp_col(i).comp_id as comp_id,
                              null as dummy
                         from dual
                      ) s1,
                        non_merch_code_comp mt
                where mt.comp_id(+)             = s1.comp_id
                  and mt.non_merch_code(+)      = s1.non_merch_code
                  and 1 = 1) sq on (st.comp_id                 = sq.comp_id
                                    and st.non_merch_code      = sq.non_merch_code
                                    and svc_non_mer_comp_col(i).action IN(
                                                             CORESVC_NON_MERCH_CODE.action_del)
                                   )
         when matched then
            update
               set process_id        = svc_non_mer_comp_col(i).process_id ,
                    chunk_id          = svc_non_mer_comp_col(i).chunk_id ,
                    row_seq           = svc_non_mer_comp_col(i).row_seq ,
                    action            = svc_non_mer_comp_col(i).action,
                    process$status    = svc_non_mer_comp_col(i).process$status ,
                    create_id         = svc_non_mer_comp_col(i).create_id ,
                    create_datetime   = svc_non_mer_comp_col(i).create_datetime ,
                    last_upd_id       = svc_non_mer_comp_col(i).last_upd_id ,
                    last_upd_datetime = svc_non_mer_comp_col(i).last_upd_datetime
        when NOT matched then
           insert(process_id ,
                  chunk_id ,
                  row_seq ,
                  action ,
                  process$status ,
                  non_merch_code ,
                  comp_id ,
                  create_id ,
                  create_datetime ,
                  last_upd_id ,
                  last_upd_datetime)
           values(svc_non_mer_comp_col(i).process_id ,
                  svc_non_mer_comp_col(i).chunk_id ,
                  svc_non_mer_comp_col(i).row_seq ,
                  svc_non_mer_comp_col(i).action ,
                  svc_non_mer_comp_col(i).process$status ,
                  sq.non_merch_code ,
                  sq.comp_id ,
                  svc_non_mer_comp_col(i).create_id ,
                  svc_non_mer_comp_col(i).create_datetime ,
                  svc_non_mer_comp_col(i).last_upd_id ,
                  svc_non_mer_comp_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
            LOOP
                   L_error_code:=sql%bulk_exceptions(i).error_code;
                   if L_error_code=1 then
                      L_error_code:=NULL;
                      L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
                   end if;
               WRITE_S9T_ERROR( I_file_id,
                                NON_MER_COMP_sheet,
                                svc_NON_MER_COMP_col(sql%bulk_exceptions(i).error_index).row_seq,
                                NULL,
                                L_error_code,
                                L_error_msg);
            END LOOP;
   END;
END PROCESS_S9T_NON_MER_COMP;
-------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_NON_MER_TL( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id IN   SVC_NON_MERCH_CODE_HEAD_TL.PROCESS_ID%TYPE) IS

   TYPE svc_non_mer_tl_col_typ is TABLE OF SVC_NON_MERCH_CODE_HEAD_TL%ROWTYPE;
   L_temp_rec         SVC_NON_MERCH_CODE_HEAD_TL%ROWTYPE;
   svc_non_mer_tl_col svc_non_mer_tl_col_typ                      :=NEW svc_non_mer_tl_col_typ();
   L_process_id       SVC_NON_MERCH_CODE_HEAD_TL.PROCESS_ID%TYPE;
   L_error            BOOLEAN                                     :=FALSE;
   L_default_rec      SVC_NON_MERCH_CODE_HEAD_TL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select non_merch_code_mi,
             non_merch_code_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                                =  template_key
                 and wksht_key                                   = 'NON_MERCH_CODE_HEAD_TL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('NON_MERCH_CODE'      as non_merch_code,
                                            'NON_MERCH_CODE_DESC' as non_merch_code_desc,
                                             NULL                 as dummy));
   L_mi_rec   C_MANDATORY_IND%ROWTYPE;
   dml_errors EXCEPTION;
   PRAGMA     exception_init(dml_errors, -24381);
    L_table         VARCHAR2(30)   := 'SVC_NON_MERCH_CODE_HEAD_TL';
   L_pk_columns    VARCHAR2(255)  := 'Non-Merchandise Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
   FOR rec IN (select non_merch_code_dv,
                      non_merch_code_desc_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                           =  template_key
                          and wksht_key                              = 'NON_MERCH_CODE_HEAD_TL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'NON_MERCH_CODE'      as non_merch_code,
                                                      'NON_MERCH_CODE_DESC' as non_merch_code_desc,
                                                       NULL                 as dummy)))
   LOOP
      BEGIN
         L_default_rec.non_merch_code_desc := rec.non_merch_code_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'NON_MERCH_CODE_HEAD_TL',
                            NULL,
                            'NON_MERCH_CODE_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.non_merch_code := rec.non_merch_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'NON_MERCH_CODE_HEAD_TL',
                            NULL,
                            'NON_MERCH_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(non_mer_tl$action)                  as action,
                     UPPER(r.get_cell(non_mer_tl$non_merch_code))          as non_merch_code,
                     r.get_cell(non_mer_tl$non_merch_code_desc)     as non_merch_code_desc,
                     r.get_row_seq()                                as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(non_mer_tl_sheet)
             )
   LOOP
     L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
     L_temp_rec.chunk_id          := 1;
     L_temp_rec.row_seq           := rec.row_seq;
     L_temp_rec.process$status    := 'N';
     L_temp_rec.create_id         := GET_USER;
     L_temp_rec.last_upd_id       := GET_USER;
     L_temp_rec.create_datetime   := SYSDATE;
     L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := false;
      BEGIN
        L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.non_merch_code_desc := rec.non_merch_code_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_TL_sheet,
                            rec.row_seq,
                            'NON_MERCH_CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.non_merch_code := rec.non_merch_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_TL_sheet,
                            rec.row_seq,
                            'NON_MERCH_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      if rec.action = CORESVC_NON_MERCH_CODE.action_new then
        L_temp_rec.non_merch_code_desc := NVL(L_temp_rec.non_merch_code_desc,
                                                L_default_rec.non_merch_code_desc);
        L_temp_rec.non_merch_code      := NVL(L_temp_rec.non_merch_code,L_default_rec.non_merch_code);
      end if;
      if NOT (L_temp_rec.non_merch_code is not NULL
              and 1 = 1)then
         WRITE_S9T_ERROR( I_file_id,
                          NON_MER_TL_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := true;
      end if;
      if NOT L_error then
         svc_non_mer_tl_col.extend();
         svc_non_mer_tl_col(svc_non_mer_tl_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_non_mer_tl_col.COUNT SAVE EXCEPTIONS
         Merge into SVC_NON_MERCH_CODE_HEAD_TL st
         USING(select (case
                       when L_mi_rec.non_merch_code_mi    = 'N'
                       and svc_non_mer_tl_col(i).action   = CORESVC_NON_MERCH_CODE.action_mod
                          and s1.non_merch_code             IS NULL
                       then mt.non_merch_code
                       else s1.non_merch_code
                       end) as non_merch_code,
                      (case
                       when L_mi_rec.non_merch_code_desc_mi = 'N'
                       and svc_non_mer_tl_col(i).action     = CORESVC_NON_MERCH_CODE.action_mod
                       and s1.non_merch_code_desc             IS NULL
                       then mt.non_merch_code_desc
                       else s1.non_merch_code_desc
                        end) as non_merch_code_desc
                 from (select svc_non_mer_tl_col(i).non_merch_code as non_merch_code,
                              svc_non_mer_tl_col(i).non_merch_code_desc as non_merch_code_desc
                         from dual
                       )s1,
                       non_merch_code_head_tl mt
                where mt.non_merch_code (+)     = s1.non_merch_code
                  and mt.lang (+)               = LP_primary_lang) sq on (st.non_merch_code      = sq.non_merch_code
                  and svc_non_mer_tl_col(i).ACTION IN (CORESVC_NON_MERCH_CODE.action_mod,
                                                       CORESVC_NON_MERCH_CODE.action_del))
         when matched then
            update
               set process_id             = svc_non_mer_tl_col(i).process_id ,
                   chunk_id               = svc_non_mer_tl_col(i).chunk_id ,
                   row_seq                = svc_non_mer_tl_col(i).row_seq ,
                   action                 = svc_non_mer_tl_col(i).action,
                   process$status         = svc_non_mer_tl_col(i).process$status ,
                   non_merch_code_desc    = sq.non_merch_code_desc ,
                   create_id              = svc_non_mer_tl_col(i).create_id ,
                   create_datetime        = svc_non_mer_tl_col(i).create_datetime ,
                   last_upd_id            = svc_non_mer_tl_col(i).last_upd_id ,
                   last_upd_datetime      = svc_non_mer_tl_col(i).last_upd_datetime
         when NOT matched then
            insert(process_id ,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   non_merch_code ,
                   non_merch_code_desc ,
                   create_id ,
                   create_datetime ,
                   last_upd_id ,
                   last_upd_datetime)
            values(svc_non_mer_tl_col(i).process_id ,
                   svc_non_mer_tl_col(i).chunk_id ,
                   svc_non_mer_tl_col(i).row_seq ,
                   svc_non_mer_tl_col(i).action ,
                   svc_non_mer_tl_col(i).process$status ,
                   sq.non_merch_code ,
                   sq.non_merch_code_desc ,
                   svc_non_mer_tl_col(i).create_id ,
                   svc_non_mer_tl_col(i).create_datetime ,
                   svc_non_mer_tl_col(i).last_upd_id ,
                   svc_non_mer_tl_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
            LOOP
               L_error_code:=sql%bulk_exceptions(i).error_code;
                   if L_error_code=1 then
                      L_error_code:=NULL;
                      L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
                   end if;
               WRITE_S9T_ERROR( I_file_id,
                                non_mer_tl_sheet,
                                svc_non_mer_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                                NULL,
                                L_error_code,
                                L_error_msg);
            END LOOP;
   END;
END PROCESS_S9T_NON_MER_TL;
-------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_NON_MER_LANG(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_NONMERCH_CODE_HEAD_LTL.PROCESS_ID%TYPE) IS

   TYPE svc_non_mer_lang_col_typ is TABLE OF SVC_NONMERCH_CODE_HEAD_LTL%ROWTYPE;
   L_temp_rec             SVC_NONMERCH_CODE_HEAD_LTL%ROWTYPE;
   svc_non_mer_lang_col   svc_non_mer_lang_col_typ                      :=NEW svc_non_mer_lang_col_typ();
   L_process_id           SVC_NONMERCH_CODE_HEAD_LTL.PROCESS_ID%TYPE;
   L_error                BOOLEAN                                     :=FALSE;
   L_default_rec          SVC_NONMERCH_CODE_HEAD_LTL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select lang_mi,
             non_merch_code_mi,
             non_merch_code_desc_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                                =  template_key
                 and wksht_key                                   = 'NON_MERCH_CODE_HEAD_LTL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('LANG'                as lang,
                                            'NON_MERCH_CODE'      as non_merch_code,
                                            'NON_MERCH_CODE_DESC' as non_merch_code_desc));

   L_mi_rec               C_MANDATORY_IND%ROWTYPE;
   dml_errors             EXCEPTION;
   PRAGMA                 exception_init(dml_errors, -24381);
   L_table                VARCHAR2(30)   := 'SVC_NONMERCH_CODE_HEAD_LTL';
   L_pk_columns           VARCHAR2(255)  := 'Non-Merchandise Code, Lang';
   L_error_code           NUMBER;
   L_error_msg            RTK_ERRORS.RTK_TEXT%type;

BEGIN
   FOR rec IN (select lang_dv,
                      non_merch_code_dv,
                      non_merch_code_desc_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                           =  template_key
                          and wksht_key                              = 'NON_MERCH_CODE_HEAD_LTL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'LANG'                as lang,
                                                      'NON_MERCH_CODE'      as non_merch_code,
                                                      'NON_MERCH_CODE_DESC' as non_merch_code_desc)))
   LOOP
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_LANG_sheet,
                            NULL,
                            'LANG',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.non_merch_code_desc := rec.non_merch_code_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_LANG_sheet,
                            NULL,
                            'NON_MERCH_CODE_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.non_merch_code := rec.non_merch_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            NON_MER_LANG_sheet,
                            NULL,
                            'NON_MERCH_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.get_cell(non_mer_lang$action)                  as action,
                     r.get_cell(non_mer_lang$lang)                    as lang,
                     UPPER(r.get_cell(non_mer_lang$non_merch_code))   as non_merch_code,
                     r.get_cell(non_mer_lang$nmc_desc)                as non_merch_code_desc,
                     r.get_row_seq()                                  as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id    = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(non_mer_lang_sheet)
             )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := false;

      BEGIN
        L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            non_mer_lang_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            non_mer_lang_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.non_merch_code_desc := rec.non_merch_code_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            non_mer_lang_sheet,
                            rec.row_seq,
                            'NON_MERCH_CODE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;
      BEGIN
        L_temp_rec.non_merch_code := rec.non_merch_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            non_mer_lang_sheet,
                            rec.row_seq,
                            'NON_MERCH_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := true;
      END;

      if rec.action = CORESVC_NON_MERCH_CODE.action_new then
        L_temp_rec.non_merch_code_desc := NVL(L_temp_rec.non_merch_code_desc, L_default_rec.non_merch_code_desc);
        L_temp_rec.non_merch_code      := NVL(L_temp_rec.non_merch_code,L_default_rec.non_merch_code);
        L_temp_rec.lang                := NVL(L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.non_merch_code is NOT NULL and L_temp_rec.lang is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         non_mer_lang_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_non_mer_lang_col.extend();
         svc_non_mer_lang_col(svc_non_mer_lang_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_non_mer_lang_col.COUNT SAVE EXCEPTIONS
         Merge into SVC_NONMERCH_CODE_HEAD_LTL st
         USING(select (case
                       when L_mi_rec.non_merch_code_mi    = 'N'
                       and svc_non_mer_lang_col(i).action   = CORESVC_NON_MERCH_CODE.action_mod
                          and s1.non_merch_code             IS NULL
                       then mt.non_merch_code
                       else s1.non_merch_code
                       end) as non_merch_code,
                      (case
                       when L_mi_rec.lang_mi    = 'N'
                       and svc_non_mer_lang_col(i).action   = CORESVC_NON_MERCH_CODE.action_mod
                          and s1.lang             IS NULL
                       then mt.lang
                       else s1.lang
                       end) as lang,
                      (case
                       when L_mi_rec.non_merch_code_desc_mi = 'N'
                       and svc_non_mer_lang_col(i).action     = CORESVC_NON_MERCH_CODE.action_mod
                       and s1.non_merch_code_desc             IS NULL
                       then mt.non_merch_code_desc
                       else s1.non_merch_code_desc
                        end) as non_merch_code_desc,
                        null as dummy
                 from (select svc_non_mer_lang_col(i).lang as lang,
                              svc_non_mer_lang_col(i).non_merch_code as non_merch_code,
                              svc_non_mer_lang_col(i).non_merch_code_desc as non_merch_code_desc,
                              null as dummy
                         from dual) s1,
                       NON_MERCH_CODE_HEAD_TL mt
                where mt.lang (+)               = s1.lang
                  and mt.non_merch_code (+)     = s1.non_merch_code) sq
                  ON (st.non_merch_code = sq.non_merch_code
                      and st.lang       = sq.lang
                      and svc_non_mer_lang_col(i).action IN (CORESVC_NON_MERCH_CODE.action_mod,
                                                             CORESVC_NON_MERCH_CODE.action_del))
         when matched then
            update
               set process_id             = svc_non_mer_lang_col(i).process_id ,
                   chunk_id               = svc_non_mer_lang_col(i).chunk_id ,
                   row_seq                = svc_non_mer_lang_col(i).row_seq ,
                   action                 = svc_non_mer_lang_col(i).action,
                   process$status         = svc_non_mer_lang_col(i).process$status ,
                   non_merch_code_desc    = sq.non_merch_code_desc
         when NOT matched then
            insert(process_id ,
                   chunk_id ,
                   row_seq ,
                   action ,
                   process$status ,
                   lang,
                   non_merch_code ,
                   non_merch_code_desc)
            values(svc_non_mer_lang_col(i).process_id ,
                   svc_non_mer_lang_col(i).chunk_id ,
                   svc_non_mer_lang_col(i).row_seq ,
                   svc_non_mer_lang_col(i).action ,
                   svc_non_mer_lang_col(i).process$status ,
                   sq.lang,
                   sq.non_merch_code ,
                   sq.non_merch_code_desc);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
            LOOP
               L_error_code:=sql%bulk_exceptions(i).error_code;
                   if L_error_code=1 then
                      L_error_code := NULL;
                      L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
                   end if;
               WRITE_S9T_ERROR(I_file_id,
                               non_mer_lang_sheet,
                               svc_non_mer_lang_col(sql%bulk_exceptions(i).error_index).row_seq,
                               NULL,
                               L_error_code,
                               L_error_msg);
            END LOOP;
   END;
END PROCESS_S9T_NON_MER_LANG;
--------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_COUNT   IN OUT NUMBER,
                     I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN     NUMBER)
  RETURN BOOLEAN IS

   L_file           S9T_FILE;
   l_sheets         S9T_PKG.NAMES_MAP_TYP;
   L_program        VARCHAR2(64):='CORESVC_NON_MERCH_CODE.PROCESS_S9T';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR     EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_NON_MER_TL(I_file_id,I_process_id);
      PROCESS_S9T_NON_MER_COMP(I_file_id,I_process_id);
      PROCESS_S9T_NON_MER_LANG(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();

   FORALL i in 1..O_error_count
      insert into s9t_errors
           values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   COMMIT;
   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      FORALL i in 1..O_error_count
         insert into s9t_errors
              values lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;

      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
  RETURN FALSE;
END PROCESS_S9T;
----------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_NON_MER_COMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error         IN OUT BOOLEAN ,
                                  I_rec           IN     C_SVC_NON_MER_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_assoc_exists BOOLEAN                              := FALSE;
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_NON_MERCH_CODE_COMP';
BEGIN
   L_assoc_exists := TRUE;
   if I_rec.action=action_new
      and I_rec.comp_id is NOT NULL then
      if ELC_SQL.GET_COMP_DESC(O_error_message,
                               L_assoc_exists,
                               LP_comp_desc,
                               I_rec.comp_id)= FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_ID',
                     O_error_message);
         O_error :=TRUE;
      end if;
      if NOT L_assoc_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'COMP_ID',
                     'COMP_ID_NOT_EXIST');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_VAL_NON_MER_COMP;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_NON_MER_TL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error         IN OUT BOOLEAN ,
                                I_rec           IN     C_SVC_NON_MER_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_assoc_exists BOOLEAN                              := FALSE;
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_NON_MERCH_CODE_HEAD_TL';
BEGIN
   if I_rec.action=action_del
      and I_rec.non_merch_code is NOT NULL then
      if INVC_VALIDATE_SQL.INVC_NON_MERCH_EXIST(O_error_message,
                                                L_assoc_exists,
                                                I_rec.non_merch_code
                                                )= FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'NON_MERCH_CODE',
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_assoc_exists then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'NON_MERCH_CODE',
                     'NO_DEL_INVC_CHILD');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             NULL,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_VAL_NON_MER_TL;
--------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_COMP_INS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_non_mer_comp_temp_rec   IN       NON_MERCH_CODE_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_COMP_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_NON_MERCH_CODE_COMP';
BEGIN
   insert
     into non_merch_code_comp
   values I_non_mer_comp_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_NON_MER_COMP_INS;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_COMP_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_non_mer_comp_temp_rec   IN       NON_MERCH_CODE_COMP%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                      := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_COMP_DEL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_NON_MERCH_CODE_COMP';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor c_non_merch_code_comp is
      select 'x'
        from non_merch_code_comp
       where comp_id        = I_non_mer_comp_temp_rec.comp_id
         and non_merch_code = I_non_mer_comp_temp_rec.non_merch_code
         for update nowait;

BEGIN
   open  C_NON_MERCH_CODE_COMP;
   close C_NON_MERCH_CODE_COMP;
   delete
     from non_merch_code_comp
    where comp_id        = I_non_mer_comp_temp_rec.comp_id
      and non_merch_code = I_non_mer_comp_temp_rec.non_merch_code;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_non_mer_comp_temp_rec.comp_id,
                                             I_non_mer_comp_temp_rec.non_merch_code);
      close C_NON_MERCH_CODE_COMP;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_NON_MERCH_CODE_COMP%ISOPEN then
         close C_NON_MERCH_CODE_COMP;
      end if;
      return FALSE;
END EXEC_NON_MER_COMP_DEL;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_NON_MER_COMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id    IN     SVC_NON_MERCH_CODE_COMP.PROCESS_ID%TYPE,
                              I_chunk_id      IN     SVC_NON_MERCH_CODE_COMP.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_error                 BOOLEAN;
   L_process_error         BOOLEAN;
   L_program               VARCHAR2(64)                    :='CORESVC_NON_MERCH_CODE.PROCESS_NON_MER_COMP';
   L_non_mer_comp_temp_rec NON_MERCH_CODE_COMP%ROWTYPE;
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:='SVC_NON_MERCH_CODE_COMP';
BEGIN
   FOR rec IN C_SVC_NON_MER_COMP(I_process_id,
                                 I_chunk_id)
   LOOP
      L_error       := FALSE;
      if rec.action is NOT NULL
         and rec.action NOT IN (action_new,action_del) then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;

      if rec.action = action_new
         and rec.pk_non_merch_code_comp_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Component ID,Non Merchandise Code',
                     'COMP_ID_EXIST');
         L_error :=TRUE;
      end if;

      if rec.action =action_new
         and rec.non_merch_code is NOT NULL
         and rec.mmc_nmc_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'NON_MERCH_CODE',
                     'NON_MER_CODE_NOT_EXIST');
         L_error :=TRUE;
      end if;


      if PROCESS_VAL_NON_MER_COMP(O_error_message,
                                  L_error,
                                  rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error :=TRUE;
      end if;

      if rec.action = action_del
         and rec.non_merch_code is NOT NULL
         and rec.comp_id is NOT NULL
         and rec.pk_non_merch_code_comp_rid is NULL then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Component ID,Non Merchandise Code',
                     'COMP_ID_NOT_EXIST');
         L_error :=TRUE;
      end if;


      if NOT L_error then
         L_non_mer_comp_temp_rec.non_merch_code       := rec.non_merch_code;
         L_non_mer_comp_temp_rec.comp_id              := rec.comp_id;
         if rec.action = action_new then
            if EXEC_NON_MER_COMP_INS(O_error_message,
                                     L_non_mer_comp_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_NON_MER_COMP_DEL(O_error_message,
                                     L_non_mer_comp_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_NON_MER_COMP;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_PRE_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_non_mer_tl_temp_rec   IN       NON_MERCH_CODE_HEAD_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                     := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_PRE_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'SVC_NON_MERCH_CODE_HEAD_TL';
BEGIN
   insert
     into non_merch_code_head (non_merch_code,service_ind)
   values (I_non_mer_tl_temp_rec.non_merch_code,'N');
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_NON_MER_PRE_INS;
----------------------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_TL_INS(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_non_mer_tl_temp_rec  IN      NON_MERCH_CODE_HEAD_TL%ROWTYPE,
                             I_rec                  IN      C_SVC_NON_MER_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                     := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_TL_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'SVC_NON_MERCH_CODE_HEAD_TL';
BEGIN
   if EXEC_NON_MER_PRE_INS(O_error_message,
                           I_non_mer_tl_temp_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      return FALSE;
   end if;
   if NON_MERCH_CODE_SQL.MERGE_NON_MERCH_CODE_HEAD_TL(O_error_message,
                                                      I_non_mer_tl_temp_rec.non_merch_code,
                                                      LP_primary_lang,
                                                      I_non_mer_tl_temp_rec.non_merch_code_desc) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_NON_MER_TL_INS;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_TL_UPD(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_non_mer_tl_temp_rec  IN      NON_MERCH_CODE_HEAD_TL%ROWTYPE,
                             I_rec                  IN      C_SVC_NON_MER_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                     := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_TL_UPD';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'SVC_NON_MERCH_CODE_HEAD_TL';
BEGIN
   if NON_MERCH_CODE_SQL.MERGE_NON_MERCH_CODE_HEAD_TL(O_error_message,
                                                      I_non_mer_tl_temp_rec.non_merch_code,
                                                      LP_primary_lang,
                                                      I_non_mer_tl_temp_rec.non_merch_code_desc) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_program,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_NON_MER_TL_UPD;
---------------------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_POST_DEL(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_non_mer_tl_temp_rec  IN      NON_MERCH_CODE_HEAD_TL%ROWTYPE,
                               I_rec                  IN      C_SVC_NON_MER_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)                     := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_TL_DEL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'SVC_NON_MERCH_CODE_HEAD_TL';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);

   cursor C_NON_MERCH is
      select 'x'
        from non_merch_code_head
       where non_merch_code = I_non_mer_tl_temp_rec.non_merch_code
         for update nowait;

BEGIN
   if NON_MERCH_CODE_SQL.DEL_NON_MERCH_CODE_COMP( O_error_message,
                                                  I_non_mer_tl_temp_rec.non_merch_code)=FALSE then
      WRITE_ERROR(I_rec.process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_rec.chunk_id,
                  L_program,
                  I_rec.row_seq,
                  'NON_MERCH_CODE',
                  O_error_message);
      return FALSE;
   end if;
   open  C_NON_MERCH;
   close C_NON_MERCH;
   delete from non_merch_code_head
    where non_merch_code = I_non_mer_tl_temp_rec.non_merch_code;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_non_mer_tl_temp_rec.non_merch_code,
                                             NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_NON_MERCH%ISOPEN then
         close C_NON_MERCH;
      end if;
      return FALSE;
END EXEC_NON_MER_POST_DEL;
------------------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_TL_DEL(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_non_mer_tl_temp_rec  IN      NON_MERCH_CODE_HEAD_TL%ROWTYPE,
                             I_rec                  IN      C_SVC_NON_MER_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                     := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_TL_DEL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'SVC_NON_MERCH_CODE_HEAD_TL';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked, -54);

   cursor C_NON_MERCH_TL is
      select 'x'
        from non_merch_code_head_tl
       where non_merch_code = I_non_mer_tl_temp_rec.non_merch_code
         for update nowait;

BEGIN
   open  C_NON_MERCH_TL;
   close C_NON_MERCH_TL;
   delete
     from non_merch_code_head_tl
    where non_merch_code = I_non_mer_tl_temp_rec.non_merch_code;

   if EXEC_NON_MER_POST_DEL(O_error_message,
                            I_non_mer_tl_temp_rec,
                            I_rec)= FALSE then
      return FALSE;

   end if;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_non_mer_tl_temp_rec.non_merch_code,
                                             NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_NON_MERCH_TL%ISOPEN then
         close C_NON_MERCH_TL;
      end if;
      return FALSE;
END EXEC_NON_MER_TL_DEL;
-----------------------------------------------------------------------------------------------------
FUNCTION PROCESS_NON_MER_TL(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id    IN      SVC_NON_MERCH_CODE_HEAD_TL.PROCESS_ID%TYPE,
                            I_chunk_id      IN      SVC_NON_MERCH_CODE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_error               BOOLEAN;
   L_validations         BOOLEAN;
   L_process_error       BOOLEAN;
   L_program             VARCHAR2(64)                     :='CORESVC_NON_MERCH_CODE.PROCESS_NON_MER_TL';
   L_non_mer_tl_temp_rec NON_MERCH_CODE_HEAD_TL%ROWTYPE;
   L_table               SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:='SVC_NON_MERCH_CODE_HEAD_TL';
BEGIN
   FOR rec IN C_SVC_NON_MER_TL(I_process_id,
                               I_chunk_id)
   LOOP
      SAVEPOINT processing;
      L_error       := FALSE;
      L_validations :=FALSE;
      L_process_error := FALSE;
      if rec.action IN (action_mod,action_del)
         and rec.PK_NON_MERCH_CODE_HEAD_TL_rid is NOT NULL
         and rec.non_merch_code IN ('T','F','E') then
         WRITE_ERROR(I_process_id,
                     svc_admin_upld_er_seq.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'NON_MERCH_CODE',
                     'NON_MER_MOD_DEL_NO_PER');
          L_validations := TRUE;
      end if;

      if L_validations = FALSE then
         if rec.action is NULL
            or rec.action NOT IN (action_new,action_mod,action_del) then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'ACTION',
                        'INV_ACT');
            L_error :=true;
         end if;

         if rec.action = action_new
            and rec.nmcht_nmc_fk_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NON_MERCH_CODE',
                        'NON_MERCH_CODE_EXIST');
            L_error :=TRUE;
         end if;

         if rec.action IN (action_mod,action_del)
            and rec.non_merch_code is NOT NULL
            and rec.PK_NON_MERCH_CODE_HEAD_TL_rid is NULL then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NON_MERCH_CODE',
                        'NON_MER_CODE_NOT_EXIST');
            L_error :=TRUE;
         end if;

         if rec.action IN (action_new,action_mod)
            and NOT(  rec.NON_MERCH_CODE_DESC  IS NOT NULL ) then
            WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'NON_MERCH_CODE_DESC',
                        'ENTER_NON_MERCH_CODE_DESC');
            L_error :=TRUE;
         end if;

         if PROCESS_VAL_NON_MER_TL(O_error_message,
                                   L_error,
                                   rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error :=TRUE;
         end if;


         if NOT L_error then
            L_non_mer_tl_temp_rec.non_merch_code              := rec.non_merch_code;
            L_non_mer_tl_temp_rec.non_merch_code_desc         := rec.non_merch_code_desc;
            L_non_mer_tl_temp_rec.create_id                   := GET_USER;
            L_non_mer_tl_temp_rec.create_datetime             := SYSDATE;
            L_non_mer_tl_temp_rec.last_update_datetime        := SYSDATE;
            if rec.action = action_new then
               if EXEC_NON_MER_TL_INS(O_error_message,
                                      L_non_mer_tl_temp_rec,
                                       rec)=FALSE then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              NULL,
                              O_error_message);
                  ROLLBACK to processing;
                  L_process_error :=TRUE;
               end if;
            end if;
            if rec.action = action_mod then
               if EXEC_NON_MER_TL_UPD(O_error_message,
                                      L_non_mer_tl_temp_rec,
                                      rec)=FALSE then
                  WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
                  L_process_error :=TRUE;
               end if;
            end if;
            if rec.action = action_del then
               if EXEC_NON_MER_TL_DEL(O_error_message,
                                      L_non_mer_tl_temp_rec,
                                      rec)=FALSE then
                  WRITE_ERROR(I_process_id,
                        svc_admin_upld_er_seq.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
                  ROLLBACK to processing;
                  L_process_error :=TRUE;
               end if;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_SVC_NON_MER_TL%ISOPEN then
         close C_SVC_NON_MER_TL;
      end if;
      return FALSE;
END PROCESS_NON_MER_TL;
--------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_LANG_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_non_mer_lang_ins_tab   IN       NON_MER_LANG_TAB)
RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_LANG_INS';

BEGIN
   if I_non_mer_lang_ins_tab is NOT NULL and I_non_mer_lang_ins_tab.COUNT > 0 then
      FORALL i IN 1..I_non_mer_lang_ins_tab.COUNT()
         insert into non_merch_code_head_tl
              values I_non_mer_lang_ins_tab(i);


      FORALL i IN 1..I_non_mer_lang_ins_tab.COUNT()
         update non_merch_code_head_tl
            set reviewed_ind = 'Y'
          where reviewed_ind = 'N'
            and non_merch_code = I_non_mer_lang_ins_tab(i).non_merch_code
            and lang = LP_primary_lang;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_NON_MER_LANG_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_LANG_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_non_mer_lang_upd_tab   IN       NON_MER_LANG_TAB,
                               I_non_mer_tl_upd_rst     IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_NON_MERCH_CODE_HEAD_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_NON_MERCH_CODE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_LANG_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'NON_MERCH_CODE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_NON_MER_LANG_UPD(I_non_merch_code   NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE%TYPE,
                                  I_lang             NON_MERCH_CODE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from non_merch_code_head_tl
       where non_merch_code = I_non_merch_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_non_mer_lang_upd_tab is NOT NULL and I_non_mer_lang_upd_tab.count > 0 then
      for i in I_non_mer_lang_upd_tab.FIRST..I_non_mer_lang_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_non_mer_lang_upd_tab(i).lang);
            L_key_val2 := 'NON_MERCH_CODE: '||to_char(I_non_mer_lang_upd_tab(i).non_merch_code);
            open C_LOCK_NON_MER_LANG_UPD(I_non_mer_lang_upd_tab(i).non_merch_code,
                                         I_non_mer_lang_upd_tab(i).lang);
            close C_LOCK_NON_MER_LANG_UPD;
            
            update non_merch_code_head_tl
               set non_merch_code_desc = I_non_mer_lang_upd_tab(i).non_merch_code_desc,
                   last_update_id = I_non_mer_lang_upd_tab(i).last_update_id,
                   last_update_datetime = I_non_mer_lang_upd_tab(i).last_update_datetime
             where lang = I_non_mer_lang_upd_tab(i).lang
               and non_merch_code = I_non_mer_lang_upd_tab(i).non_merch_code;
            
            update non_merch_code_head_tl
               set reviewed_ind = 'Y'
             where reviewed_ind = 'N'
               and non_merch_code = I_non_mer_lang_upd_tab(i).non_merch_code
               and lang = LP_primary_lang;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_NON_MERCH_CODE_HEAD_TL',
                           I_non_mer_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_NON_MER_LANG_UPD%ISOPEN then
         close C_LOCK_NON_MER_LANG_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_NON_MER_LANG_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_NON_MER_LANG_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_non_mer_lang_del_tab   IN       NON_MER_LANG_TAB,
                               I_non_mer_tl_del_rst     IN       ROW_SEQ_TAB,
                               I_process_id             IN       SVC_NON_MERCH_CODE_HEAD_TL.PROCESS_ID%TYPE,
                               I_chunk_id               IN       SVC_NON_MERCH_CODE_HEAD_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64)                      := 'CORESVC_NON_MERCH_CODE.EXEC_NON_MER_LANG_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'NON_MERCH_CODE_HEAD_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30)                      := NULL;
   L_key_val2      VARCHAR2(30)                      := NULL;

    --Cursor to lock the record
   cursor C_LOCK_NON_MER_LANG_DEL(I_non_merch_code   NON_MERCH_CODE_HEAD_TL.NON_MERCH_CODE%TYPE,
                                  I_lang             NON_MERCH_CODE_HEAD_TL.LANG%TYPE) is
      select 'x'
        from non_merch_code_head_tl
       where non_merch_code = I_non_merch_code
         and lang = I_lang
         for update nowait;

BEGIN
   if I_non_mer_lang_del_tab is NOT NULL and I_non_mer_lang_del_tab.COUNT > 0 then
      for i in I_non_mer_lang_del_tab.FIRST..I_non_mer_lang_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_non_mer_lang_del_tab(i).lang);
            L_key_val2 := 'NON_MERCH_CODE: '||to_char(I_non_mer_lang_del_tab(i).non_merch_code);
            open C_LOCK_NON_MER_LANG_DEL(I_non_mer_lang_del_tab(i).non_merch_code,
                                         I_non_mer_lang_del_tab(i).lang);
            close C_LOCK_NON_MER_LANG_DEL;
            
            delete non_merch_code_head_tl
             where lang = I_non_mer_lang_del_tab(i).lang
               and non_merch_code = I_non_mer_lang_del_tab(i).non_merch_code;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_NON_MERCH_CODE_HEAD_TL',
                           I_non_mer_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_NON_MER_LANG_DEL%ISOPEN then
         close C_LOCK_NON_MER_LANG_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_NON_MER_LANG_DEL;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_NON_MER_LANG(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_NONMERCH_CODE_HEAD_LTL.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_NONMERCH_CODE_HEAD_LTL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(64)                      := 'CORESVC_NON_MERCH_CODE.PROCESS_NON_MER_LANG';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'NON_MERCH_CODE_HEAD_TL';
   L_base_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'NON_MERCH_CODE_HEAD';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_NONMERCH_CODE_HEAD_LTL';
   L_error                   BOOLEAN                           := FALSE;
   L_process_error           BOOLEAN                           := FALSE;
   L_non_mer_lang_temp_rec   NON_MERCH_CODE_HEAD_TL%ROWTYPE;
   L_non_mer_tl_upd_rst      ROW_SEQ_TAB;
   L_non_mer_tl_del_rst      ROW_SEQ_TAB;

   cursor C_SVC_NMC_TL(I_process_id NUMBER,
                       I_chunk_id NUMBER) is
      select pk_non_mer_tl.rowid          as pk_non_mer_tl_rid,
             fk_non_mer.rowid             as fk_non_mer_rid,
             fk_lang.rowid                as fk_lang_rid,
             pk_nm_tl_prim.non_merch_code as pk_nm_tl_prim_ncm,
             st.lang,
             upper(st.non_merch_code)     as non_merch_code,
             st.non_merch_code_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)             as action,
             st.process$status
        from svc_nonmerch_code_head_ltl st,
             non_merch_code_head        fk_non_mer,
             non_merch_code_head_tl     pk_non_mer_tl,
             (select non_merch_code
                from non_merch_code_head_tl
               where lang = LP_primary_lang) pk_nm_tl_prim,
             lang                       fk_lang
       where st.process_id     = I_process_id
         and st.chunk_id       = I_chunk_id
         and st.non_merch_code = fk_non_mer.non_merch_code (+)
         and st.lang           = pk_non_mer_tl.lang (+)
         and st.non_merch_code = pk_non_mer_tl.non_merch_code (+)
         and st.non_merch_code = pk_nm_tl_prim.non_merch_code (+)
         and st.lang           = fk_lang.lang (+);

   TYPE SVC_NMC_TL is TABLE OF C_SVC_NMC_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_non_mer_lang_tab    SVC_NMC_TL;

   L_non_mer_lang_ins_tab    non_mer_lang_tab                  := NEW non_mer_lang_tab();
   L_non_mer_lang_upd_tab    non_mer_lang_tab                  := NEW non_mer_lang_tab();
   L_non_mer_lang_del_tab    non_mer_lang_tab                  := NEW non_mer_lang_tab();

BEGIN
   if C_SVC_NMC_TL%ISOPEN then
      close C_SVC_NMC_TL;
   end if;

   open C_SVC_NMC_TL(I_process_id,
                     I_chunk_id);
   LOOP
      fetch C_SVC_NMC_TL bulk collect into L_svc_non_mer_lang_tab limit LP_bulk_fetch_limit;
      if L_svc_non_mer_lang_tab.COUNT > 0 then
         FOR i in L_svc_non_mer_lang_tab.FIRST..L_svc_non_mer_lang_tab.LAST LOOP
         L_error := FALSE;

            --check for primary_lang
            if L_svc_non_mer_lang_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_non_mer_lang_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;
            if L_svc_non_mer_lang_tab(i).action = action_new and L_svc_non_mer_lang_tab(i).lang <> LP_primary_lang and L_svc_non_mer_lang_tab(i).pk_non_mer_tl_rid is NULL then
               if L_svc_non_mer_lang_tab(i).pk_nm_tl_prim_ncm is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_non_mer_lang_tab(i).row_seq,
                              'LANG',
                              'PRIMARY_LANG_REQ');
                  L_error := TRUE;
               end if;
            end if;

            -- check if action is valid
            if L_svc_non_mer_lang_tab(i).action is NULL
               or L_svc_non_mer_lang_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_non_mer_lang_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            -- check if primary key values already exist
            if L_svc_non_mer_lang_tab(i).action = action_new
               and L_svc_non_mer_lang_tab(i).pk_non_mer_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_non_mer_lang_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_non_mer_lang_tab(i).action IN (action_mod, action_del)
               and L_svc_non_mer_lang_tab(i).lang is NOT NULL
               and L_svc_non_mer_lang_tab(i).non_merch_code is NOT NULL
               and L_svc_non_mer_lang_tab(i).pk_non_mer_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_non_mer_lang_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            -- check for FK
            if L_svc_non_mer_lang_tab(i).action = action_new
               and L_svc_non_mer_lang_tab(i).non_merch_code is NOT NULL
               and L_svc_non_mer_lang_tab(i).fk_non_mer_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_non_mer_lang_tab(i).row_seq,
                            NULL,
                            L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_non_mer_lang_tab(i).action = action_new
               and L_svc_non_mer_lang_tab(i).lang is NOT NULL
               and L_svc_non_mer_lang_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_non_mer_lang_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check for required fields
            if L_svc_non_mer_lang_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_non_mer_lang_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error := TRUE;
            end if;

            if L_svc_non_mer_lang_tab(i).non_merch_code is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_non_mer_lang_tab(i).row_seq,
                           'NON_MERCH_CODE',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_non_mer_lang_tab(i).action in (action_new, action_mod) then
               if L_svc_non_mer_lang_tab(i).non_merch_code_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_non_mer_lang_tab(i).row_seq,
                              'NON_MERCH_CODE_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if NOT L_error then
               L_non_mer_lang_temp_rec.lang := L_svc_non_mer_lang_tab(i).lang;
               L_non_mer_lang_temp_rec.non_merch_code := L_svc_non_mer_lang_tab(i).non_merch_code;
               L_non_mer_lang_temp_rec.non_merch_code_desc := L_svc_non_mer_lang_tab(i).non_merch_code_desc;
               L_non_mer_lang_temp_rec.orig_lang_ind := 'N';
               L_non_mer_lang_temp_rec.reviewed_ind := 'Y';
               L_non_mer_lang_temp_rec.create_datetime := SYSDATE;
               L_non_mer_lang_temp_rec.create_id := GET_USER;
               L_non_mer_lang_temp_rec.last_update_datetime := SYSDATE;
               L_non_mer_lang_temp_rec.last_update_id := GET_USER;

               if L_svc_non_mer_lang_tab(i).action = action_new then
                  L_non_mer_lang_ins_tab.extend;
                  L_non_mer_lang_ins_tab(L_non_mer_lang_ins_tab.count()) := L_non_mer_lang_temp_rec;
               end if;

               if L_svc_non_mer_lang_tab(i).action = action_mod then
                  L_non_mer_lang_upd_tab.extend;
                  L_non_mer_lang_upd_tab(L_non_mer_lang_upd_tab.count()) := L_non_mer_lang_temp_rec;
                  L_non_mer_tl_upd_rst(L_non_mer_lang_upd_tab.count()) := L_svc_non_mer_lang_tab(i).row_seq;
               end if;

               if L_svc_non_mer_lang_tab(i).action = action_del then
                  L_non_mer_lang_del_tab.extend;
                  L_non_mer_lang_del_tab(L_non_mer_lang_del_tab.count()) := L_non_mer_lang_temp_rec;
                  L_non_mer_tl_del_rst(L_non_mer_lang_del_tab.count()) := L_svc_non_mer_lang_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_NMC_TL%NOTFOUND;
   END LOOP;
   close C_SVC_NMC_TL;

   if EXEC_NON_MER_LANG_INS(O_error_message,
                            L_non_mer_lang_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_NON_MER_LANG_UPD(O_error_message,
                            L_non_mer_lang_upd_tab,
                            L_non_mer_tl_upd_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_NON_MER_LANG_DEL(O_error_message,
                            L_non_mer_lang_del_tab,
                            L_non_mer_tl_del_rst,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_NON_MER_LANG;
--------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete
     from svc_nonmerch_code_head_ltl
    where process_id= I_process_id;

   Delete
     from svc_non_merch_code_comp
    where process_id= I_process_id;

   Delete
     from svc_non_merch_code_head_tl
    where process_id= I_process_id;

END CLEAR_STAGING_DATA;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS

   L_program          VARCHAR2(64)                    := 'CORESVC_NON_MERCH_CODE.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_error            BOOLEAN;
   L_exists           VARCHAR2(1) := NULL;
   L_import_ind            SYSTEM_OPTIONS.IMPORT_IND%TYPE;
   L_rtm_simplified_ind    SYSTEM_OPTIONS.RTM_SIMPLIFIED_IND%TYPE;
   L_system_options        SYSTEM_OPTIONS%ROWTYPE;
   L_table                 SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:='SVC_NON_MERCH_CODE_COMP';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

   cursor C_GET_COMP_COUNT is
      select 'x'
        from svc_non_merch_code_comp
       where process_id = I_process_id
         and chunk_id = I_chunk_id;

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      WRITE_ERROR(I_process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  1,
                  NULL,
                  O_error_message);
      return FALSE;
   end if;
   L_import_ind             := L_system_options.import_ind;
   L_rtm_simplified_ind     := L_system_options.rtm_simplified_ind;


   open  C_GET_COMP_COUNT;
   fetch C_GET_COMP_COUNT into L_exists;
   close C_GET_COMP_COUNT;
   L_error       := FALSE;

   if (L_rtm_simplified_ind = 'Y' or L_import_ind = 'N') and L_exists is NOT NULL then
      WRITE_ERROR(I_process_id,
                  svc_admin_upld_er_seq.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  1,
                  NULL,
                  'NO_PER_ADD_COST_COMP');
      L_error     := TRUE;
   end if;

if not L_error then
   if PROCESS_NON_MER_TL(O_error_message,
                         I_process_id,
                         I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_NON_MER_COMP(O_error_message,
                           I_process_id,
                           I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_NON_MER_LANG(O_error_message,
                           I_process_id,
                           I_chunk_id)=FALSE then
      return FALSE;
   end if;
end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

    CLEAR_STAGING_DATA(I_process_id);

   return TRUE;

EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS;
--------------------------------------------------------------------------------------------------
END CORESVC_NON_MERCH_CODE;
/