create or replace PACKAGE BODY CORESVC_DEAL_COMP_TYPE AS
   cursor C_SVC_DEAL_COMP_TYPE_TL(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_deal_comp_type_tl.rowid  AS pk_deal_comp_type_tl_rid,
             st.rowid AS st_rid,
             dctt_dct_fk.rowid           AS dctt_dct_fk_rid,
             st.lang,
             st.deal_comp_type_desc,
             cd_lang.rowid as cd_lang_rid,
             st.deal_comp_type,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_deal_comp_type_tl st,
             deal_comp_type_tl pk_deal_comp_type_tl,
             deal_comp_type dctt_dct_fk,
			 code_detail cd_lang,
             dual
       where st.process_id             = I_process_id
         and st.chunk_id               = I_chunk_id
         and st.deal_comp_type         = pk_deal_comp_type_tl.deal_comp_type (+)
         and st.lang                   = pk_deal_comp_type_tl.lang (+)  
         and st.deal_comp_type         = dctt_dct_fk.deal_comp_type (+)
         and st.lang                   = cd_lang.code (+)
         and cd_lang.code_type (+)     = 'LANG';

   cursor C_SVC_DEAL_COMP_TYPE(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             pk_deal_comp_type.rowid  AS pk_deal_comp_type_rid,
             st.rowid AS st_rid,
             st.deal_comp_type,
			 st.deal_comp_type_desc,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_deal_comp_type st,
             deal_comp_type pk_deal_comp_type,
             dual
       where st.process_id             = I_process_id
         and st.chunk_id               = I_chunk_id
         and st.deal_comp_type         = pk_deal_comp_type.deal_comp_type (+)
;
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   LP_primary_lang    LANG.LANG%TYPE :=  LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;   
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE,
					   I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS

BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
   LP_errors_tab(LP_errors_tab.COUNT()).error_type  := I_error_type;

END WRITE_ERROR;
---------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   DEAL_COMP_TYPE_TL_cols s9t_pkg.names_map_typ;
   DEAL_COMP_TYPE_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                                     :=s9t_pkg.get_sheet_names(I_file_id);
   DEAL_COMP_TYPE_TL_cols                       :=s9t_pkg.get_col_names(I_file_id,DEAL_COMP_TYPE_TL_sheet);
   DEAL_COMP_TYPE_TL$Action                     := DEAL_COMP_TYPE_TL_cols('ACTION');
   DEAL_COMP_TYPE_TL$DL_CP_TP_DSC               := DEAL_COMP_TYPE_TL_cols('DEAL_COMP_TYPE_DESC');
   DEAL_COMP_TYPE_TL$LANG                       := DEAL_COMP_TYPE_TL_cols('LANG');
   DEAL_COMP_TYPE_TL$DL_CMP_TP                  := DEAL_COMP_TYPE_TL_cols('DEAL_COMP_TYPE');
   DEAL_COMP_TYPE_cols                          :=s9t_pkg.get_col_names(I_file_id,DEAL_COMP_TYPE_sheet);
   DEAL_COMP_TYPE$Action                        := DEAL_COMP_TYPE_cols('ACTION');
   DEAL_COMP_TYPE$DEAL_COMP_TYPE                := DEAL_COMP_TYPE_cols('DEAL_COMP_TYPE');
   DEAL_COMP_TYPE$DL_COMP_TP_DESC               := DEAL_COMP_TYPE_cols('DEAL_COMP_TYPE_DESC');
END POPULATE_NAMES;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_DEAL_COMP_TYPE_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = DEAL_COMP_TYPE_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_DEAL_COMP_TYPE.action_mod ,
                            deal_comp_type,
							lang,
							deal_comp_type_desc
                           ))
     from deal_comp_type_tl 
	where lang != LP_primary_lang;
END POPULATE_DEAL_COMP_TYPE_TL;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_DEAL_COMP_TYPE( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = DEAL_COMP_TYPE_sheet )
   select s9t_row(s9t_cells(CORESVC_DEAL_COMP_TYPE.action_mod ,
                           d.deal_comp_type,
						               d1.deal_comp_type_desc
                           ))
     from deal_comp_type d,
	      deal_comp_type_tl d1
	where d.deal_comp_type = d1.deal_comp_type
      and d1.lang = LP_primary_lang;	  
END POPULATE_DEAL_COMP_TYPE;
-----------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(DEAL_COMP_TYPE_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(DEAL_COMP_TYPE_TL_sheet)).column_headers := s9t_cells( 'ACTION','DEAL_COMP_TYPE','LANG','DEAL_COMP_TYPE_DESC');
                                                                                            
                                                                                           
   L_file.add_sheet(DEAL_COMP_TYPE_sheet);
   L_file.sheets(l_file.get_sheet_index(DEAL_COMP_TYPE_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                            ,'DEAL_COMP_TYPE',
																							'DEAL_COMP_TYPE_DESC'
);
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_DEAL_COMP_TYPE.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_DEAL_COMP_TYPE_TL(O_file_id);
      POPULATE_DEAL_COMP_TYPE(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
  
   return FALSE;
END CREATE_S9T;
---------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DEAL_COMP_TYPE_TL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                         I_process_id IN   SVC_DEAL_COMP_TYPE_TL.process_id%TYPE) IS
   TYPE svc_DEAL_COMP_TYPE_TL_col_typ IS TABLE OF SVC_DEAL_COMP_TYPE_TL%ROWTYPE;
   L_temp_rec SVC_DEAL_COMP_TYPE_TL%ROWTYPE;
   svc_DEAL_COMP_TYPE_TL_col svc_DEAL_COMP_TYPE_TL_col_typ :=NEW svc_DEAL_COMP_TYPE_TL_col_typ();
   L_process_id SVC_DEAL_COMP_TYPE_TL.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_DEAL_COMP_TYPE_TL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select
             DEAL_COMP_TYPE_DESC_mi,
             LANG_mi,
             DEAL_COMP_TYPE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'DEAL_COMP_TYPE_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'DEAL_COMP_TYPE_DESC' AS DEAL_COMP_TYPE_DESC,
                                         'LANG' AS LANG,
                                         'DEAL_COMP_TYPE' AS DEAL_COMP_TYPE,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns    VARCHAR2(255)  := 'Deal Component Type, Lang';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select
                       DEAL_COMP_TYPE_DESC_dv,
                       LANG_dv,
                       DEAL_COMP_TYPE_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'DEAL_COMP_TYPE_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'DEAL_COMP_TYPE_DESC' AS DEAL_COMP_TYPE_DESC,
                                                      'LANG' AS LANG,
                                                      'DEAL_COMP_TYPE' AS DEAL_COMP_TYPE,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.DEAL_COMP_TYPE_DESC := rec.DEAL_COMP_TYPE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_COMP_TYPE_TL ' ,
                            NULL,
                           'DEAL_COMP_TYPE_DESC ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_COMP_TYPE_TL ' ,
                            NULL,
                           'LANG ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DEAL_COMP_TYPE := rec.DEAL_COMP_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_COMP_TYPE_TL ' ,
                            NULL,
                           'DEAL_COMP_TYPE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(DEAL_COMP_TYPE_TL$Action)                           AS Action,
          r.get_cell(DEAL_COMP_TYPE_TL$DL_CP_TP_DSC )                    AS DEAL_COMP_TYPE_DESC,
          r.get_cell(DEAL_COMP_TYPE_TL$LANG)                             AS LANG,
          r.get_cell(DEAL_COMP_TYPE_TL$DL_CMP_TP)                        AS DEAL_COMP_TYPE,
          r.get_row_seq()                                                AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(DEAL_COMP_TYPE_TL_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
DEAL_COMP_TYPE_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEAL_COMP_TYPE_DESC := rec.DEAL_COMP_TYPE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
DEAL_COMP_TYPE_TL_sheet,
                            rec.row_seq,
                            'DEAL_COMP_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
DEAL_COMP_TYPE_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEAL_COMP_TYPE := rec.DEAL_COMP_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
DEAL_COMP_TYPE_TL_sheet,
                            rec.row_seq,
                            'DEAL_COMP_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_DEAL_COMP_TYPE.action_new then
         L_temp_rec.DEAL_COMP_TYPE_DESC := NVL( L_temp_rec.DEAL_COMP_TYPE_DESC,L_default_rec.DEAL_COMP_TYPE_DESC);
         L_temp_rec.LANG := NVL( L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.DEAL_COMP_TYPE := NVL( L_temp_rec.DEAL_COMP_TYPE,L_default_rec.DEAL_COMP_TYPE);
      end if;
      if not (
            L_temp_rec.DEAL_COMP_TYPE is NOT NULL and
            L_temp_rec.LANG is NOT NULL and
            1 = 1
            )then
         WRITE_S9T_ERROR(I_file_id,
                         DEAL_COMP_TYPE_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_DEAL_COMP_TYPE_TL_col.extend();
         svc_DEAL_COMP_TYPE_TL_col(svc_DEAL_COMP_TYPE_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_DEAL_COMP_TYPE_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_DEAL_COMP_TYPE_TL st
      using(select
                  (case
                   when l_mi_rec.DEAL_COMP_TYPE_DESC_mi    = 'N'
                    and svc_DEAL_COMP_TYPE_TL_col(i).action = CORESVC_DEAL_COMP_TYPE.action_mod
                    and s1.DEAL_COMP_TYPE_DESC IS NULL
                   then mt.DEAL_COMP_TYPE_DESC
                   else s1.DEAL_COMP_TYPE_DESC
                   end) AS DEAL_COMP_TYPE_DESC,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_DEAL_COMP_TYPE_TL_col(i).action = CORESVC_DEAL_COMP_TYPE.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  (case
                   when l_mi_rec.DEAL_COMP_TYPE_mi    = 'N'
                    and svc_DEAL_COMP_TYPE_TL_col(i).action = CORESVC_DEAL_COMP_TYPE.action_mod
                    and s1.DEAL_COMP_TYPE IS NULL
                   then mt.DEAL_COMP_TYPE
                   else s1.DEAL_COMP_TYPE
                   end) AS DEAL_COMP_TYPE,
                  null as dummy
              from (select
                          svc_DEAL_COMP_TYPE_TL_col(i).DEAL_COMP_TYPE_DESC AS DEAL_COMP_TYPE_DESC,
                          svc_DEAL_COMP_TYPE_TL_col(i).LANG AS LANG,
                          svc_DEAL_COMP_TYPE_TL_col(i).DEAL_COMP_TYPE AS DEAL_COMP_TYPE,
                          null as dummy
                      from dual ) s1,
            DEAL_COMP_TYPE_TL mt
             where
                  mt.DEAL_COMP_TYPE (+)     = s1.DEAL_COMP_TYPE   and
                  mt.LANG (+)     = s1.LANG   and
                  1 = 1 )sq
                on (
                    st.DEAL_COMP_TYPE      = sq.DEAL_COMP_TYPE and
                    st.LANG      = sq.LANG and
                    svc_DEAL_COMP_TYPE_TL_col(i).ACTION IN (CORESVC_DEAL_COMP_TYPE.action_mod,CORESVC_DEAL_COMP_TYPE.action_del))
      when matched then
      update
         set process_id      = svc_DEAL_COMP_TYPE_TL_col(i).process_id ,
             chunk_id        = svc_DEAL_COMP_TYPE_TL_col(i).chunk_id ,
             row_seq         = svc_DEAL_COMP_TYPE_TL_col(i).row_seq ,
             action          = svc_DEAL_COMP_TYPE_TL_col(i).action ,
             process$status  = svc_DEAL_COMP_TYPE_TL_col(i).process$status ,
             deal_comp_type_desc              = sq.deal_comp_type_desc ,
             create_id       = svc_DEAL_COMP_TYPE_TL_col(i).create_id ,
             create_datetime = svc_DEAL_COMP_TYPE_TL_col(i).create_datetime ,
             last_upd_id     = svc_DEAL_COMP_TYPE_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_DEAL_COMP_TYPE_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             deal_comp_type_desc ,
             lang ,
             deal_comp_type ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_DEAL_COMP_TYPE_TL_col(i).process_id ,
             svc_DEAL_COMP_TYPE_TL_col(i).chunk_id ,
             svc_DEAL_COMP_TYPE_TL_col(i).row_seq ,
             svc_DEAL_COMP_TYPE_TL_col(i).action ,
             svc_DEAL_COMP_TYPE_TL_col(i).process$status ,
             sq.deal_comp_type_desc ,
             sq.lang ,
             sq.deal_comp_type ,
             svc_DEAL_COMP_TYPE_TL_col(i).create_id ,
             svc_DEAL_COMP_TYPE_TL_col(i).create_datetime ,
             svc_DEAL_COMP_TYPE_TL_col(i).last_upd_id ,
             svc_DEAL_COMP_TYPE_TL_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            DEAL_COMP_TYPE_TL_sheet,
                            svc_DEAL_COMP_TYPE_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;
END PROCESS_S9T_DEAL_COMP_TYPE_TL;
----------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DEAL_COMP_TYPE( I_file_id    IN   s9t_folder.file_id%TYPE,
                                      I_process_id IN   SVC_DEAL_COMP_TYPE.process_id%TYPE) IS
   TYPE svc_DEAL_COMP_TYPE_col_typ IS TABLE OF SVC_DEAL_COMP_TYPE%ROWTYPE;
   L_temp_rec SVC_DEAL_COMP_TYPE%ROWTYPE;
   svc_DEAL_COMP_TYPE_col svc_DEAL_COMP_TYPE_col_typ :=NEW svc_DEAL_COMP_TYPE_col_typ();
   L_process_id SVC_DEAL_COMP_TYPE.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_DEAL_COMP_TYPE%ROWTYPE;
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             DEAL_COMP_TYPE_mi,
			 DEAL_COMP_TYPE_DESC_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'DEAL_COMP_TYPE'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'DEAL_COMP_TYPE' AS DEAL_COMP_TYPE,
										 'DEAL_COMP_TYPE_DESC' AS DEAL_COMP_TYPE_DESC,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_pk_columns   VARCHAR2(255)  := 'Deal Component Type';
	BEGIN
  -- Get default values.
   FOR rec IN (select
                       DEAL_COMP_TYPE_dv,
					   DEAL_COMP_TYPE_DESC_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'DEAL_COMP_TYPE'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'DEAL_COMP_TYPE' AS DEAL_COMP_TYPE,
													  'DEAL_COMP_TYPE_DESC' AS DEAL_COMP_TYPE_DESC,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.DEAL_COMP_TYPE := rec.DEAL_COMP_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_COMP_TYPE ' ,
                            NULL,
                           'DEAL_COMP_TYPE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.DEAL_COMP_TYPE_DESC := rec.DEAL_COMP_TYPE_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_COMP_TYPE ' ,
                            NULL,
                           'DEAL_COMP_TYPE_DESC' ,
                            NULL,
                            'INV_DEFAULT');
      END;

	END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(DEAL_COMP_TYPE$Action)                           AS Action,
          r.get_cell(DEAL_COMP_TYPE$DEAL_COMP_TYPE)                   AS DEAL_COMP_TYPE,
		  r.get_cell(DEAL_COMP_TYPE$DL_COMP_TP_DESC)              AS DEAL_COMP_TYPE_DESC,
          r.get_row_seq()                                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(DEAL_COMP_TYPE_sheet)
  )
   LOOP
      L_temp_rec:=null;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_COMP_TYPE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEAL_COMP_TYPE := rec.DEAL_COMP_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_COMP_TYPE_sheet,
                            rec.row_seq,
                            'DEAL_COMP_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEAL_COMP_TYPE_DESC := rec.DEAL_COMP_TYPE_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_COMP_TYPE_sheet,
                            rec.row_seq,
                            'DEAL_COMP_TYPE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_DEAL_COMP_TYPE.action_new then
         L_temp_rec.DEAL_COMP_TYPE := NVL( L_temp_rec.DEAL_COMP_TYPE,L_default_rec.DEAL_COMP_TYPE);
		 L_temp_rec.DEAL_COMP_TYPE_DESC := NVL( L_temp_rec.DEAL_COMP_TYPE_DESC,L_default_rec.DEAL_COMP_TYPE_DESC);
      end if;
      if not (
            L_temp_rec.DEAL_COMP_TYPE is NOT NULL and
            1 = 1
            )then
          WRITE_S9T_ERROR(I_file_id,
                         DEAL_COMP_TYPE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_DEAL_COMP_TYPE_col.extend();
         svc_DEAL_COMP_TYPE_col(svc_DEAL_COMP_TYPE_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_DEAL_COMP_TYPE_col.COUNT SAVE EXCEPTIONS
      merge into SVC_DEAL_COMP_TYPE st
      using(select
                  (case
                   when l_mi_rec.DEAL_COMP_TYPE_mi    = 'N'
                    and svc_DEAL_COMP_TYPE_col(i).action = CORESVC_DEAL_COMP_TYPE.action_mod
                    and s1.DEAL_COMP_TYPE IS NULL
                   then mt.DEAL_COMP_TYPE
                   else s1.DEAL_COMP_TYPE
                   end) AS DEAL_COMP_TYPE,
				   (case
                   when l_mi_rec.DEAL_COMP_TYPE_DESC_mi    = 'N'
                    and svc_DEAL_COMP_TYPE_col(i).action = CORESVC_DEAL_COMP_TYPE.action_mod
                    and s1.DEAL_COMP_TYPE_DESC IS NULL
                   then mt.DEAL_COMP_TYPE_DESC
                   else s1.DEAL_COMP_TYPE_DESC
                   end) AS DEAL_COMP_TYPE_DESC,
                  null as dummy
              from (select
                          svc_DEAL_COMP_TYPE_col(i).DEAL_COMP_TYPE AS DEAL_COMP_TYPE,
						  svc_DEAL_COMP_TYPE_col(i).DEAL_COMP_TYPE_DESC AS DEAL_COMP_TYPE_DESC,
                          null as dummy
                      from dual ) s1,
                      (select dt.*, dtl.deal_comp_type_desc
                         from DEAL_COMP_TYPE dt, DEAL_COMP_TYPE_TL dtl
                        where dt.deal_comp_type = dtl.deal_comp_type
                          and dtl.lang = LP_primary_lang) mt
             where
                  mt.DEAL_COMP_TYPE (+)     = s1.DEAL_COMP_TYPE   and
                  1 = 1 )sq
                on (
                    st.DEAL_COMP_TYPE      = sq.DEAL_COMP_TYPE and
                    svc_DEAL_COMP_TYPE_col(i).ACTION IN (CORESVC_DEAL_COMP_TYPE.action_mod,CORESVC_DEAL_COMP_TYPE.action_del))
      when matched then
      update
         set process_id      = svc_DEAL_COMP_TYPE_col(i).process_id ,
             chunk_id        = svc_DEAL_COMP_TYPE_col(i).chunk_id ,
             row_seq         = svc_DEAL_COMP_TYPE_col(i).row_seq ,
             action          = svc_DEAL_COMP_TYPE_col(i).action ,
             process$status  = svc_DEAL_COMP_TYPE_col(i).process$status ,
             create_id       = svc_DEAL_COMP_TYPE_col(i).create_id ,
             create_datetime = svc_DEAL_COMP_TYPE_col(i).create_datetime ,
             last_upd_id     = svc_DEAL_COMP_TYPE_col(i).last_upd_id ,
             last_upd_datetime = svc_DEAL_COMP_TYPE_col(i).last_upd_datetime,
			 deal_comp_type_desc = svc_DEAL_COMP_TYPE_col(i).deal_comp_type_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             deal_comp_type ,
			 deal_comp_type_desc,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_DEAL_COMP_TYPE_col(i).process_id ,
             svc_DEAL_COMP_TYPE_col(i).chunk_id ,
             svc_DEAL_COMP_TYPE_col(i).row_seq ,
             svc_DEAL_COMP_TYPE_col(i).action ,
             svc_DEAL_COMP_TYPE_col(i).process$status ,
             sq.deal_comp_type ,
			 sq.deal_comp_type_desc,
             svc_DEAL_COMP_TYPE_col(i).create_id ,
             svc_DEAL_COMP_TYPE_col(i).create_datetime ,
             svc_DEAL_COMP_TYPE_col(i).last_upd_id ,
             svc_DEAL_COMP_TYPE_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            DEAL_COMP_TYPE_sheet,
                            svc_DEAL_COMP_TYPE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	 END;
END PROCESS_S9T_DEAL_COMP_TYPE;
-----------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER
                      )
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_DEAL_COMP_TYPE.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_DEAL_COMP_TYPE_TL(I_file_id,I_process_id);
      PROCESS_S9T_DEAL_COMP_TYPE(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
-----------------------------------------------------------------------------
FUNCTION EXEC_DEAL_COMP_TYPE_TL_INS(  L_deal_comp_type_tl_temp_rec   IN   DEAL_COMP_TYPE_TL%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_DEAL_COMP_TYPE.EXEC_DEAL_COMP_TYPE_TL_INS';
   L_table   VARCHAR2(255):= 'SVC_DEAL_COMP_TYPE_TL';
BEGIN
   insert
     into deal_comp_type_tl
   values L_deal_comp_type_tl_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DEAL_COMP_TYPE_TL_INS;
------------------------------------------------------------------------------
FUNCTION EXEC_DEAL_COMP_TYPE_TL_UPD( L_deal_comp_type_tl_temp_rec   IN   DEAL_COMP_TYPE_TL%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_DEAL_COMP_TYPE.EXEC_DEAL_COMP_TYPE_TL_UPD';
   L_table   VARCHAR2(255):= 'SVC_DEAL_COMP_TYPE_TL';
BEGIN
 
   update deal_comp_type_tl
      set deal_comp_type_desc = L_deal_comp_type_tl_temp_rec.deal_comp_type_desc,
          last_update_datetime = sysdate,
          last_update_id = GET_USER      
    where 1 = 1
      and deal_comp_type = L_deal_comp_type_tl_temp_rec.deal_comp_type
      and lang = L_deal_comp_type_tl_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DEAL_COMP_TYPE_TL_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_DEAL_COMP_TYPE_TL_DEL(  L_deal_comp_type_tl_temp_rec   IN   DEAL_COMP_TYPE_TL%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_DEAL_COMP_TYPE.EXEC_DEAL_COMP_TYPE_TL_DEL';
   L_table   VARCHAR2(255):= 'SVC_DEAL_COMP_TYPE_TL';
BEGIN
   delete
     from deal_comp_type_tl
    where 1 = 1
      and deal_comp_type = L_deal_comp_type_tl_temp_rec.deal_comp_type
      and lang = L_deal_comp_type_tl_temp_rec.lang
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DEAL_COMP_TYPE_TL_DEL;
---------------------------------------------------------------------------
FUNCTION PROCESS_DEAL_COMP_TYPE_TL( I_process_id   IN   SVC_DEAL_COMP_TYPE_TL.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_DEAL_COMP_TYPE_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_DEAL_COMP_TYPE.PROCESS_DEAL_COMP_TYPE_TL';
   L_error_message VARCHAR2(600);
   L_DEAL_COMP_TYPE_TL_temp_rec DEAL_COMP_TYPE_TL%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_DEAL_COMP_TYPE_TL';
BEGIN
   FOR rec IN c_svc_DEAL_COMP_TYPE_TL(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
            
      if rec.action = action_new
         and rec.PK_DEAL_COMP_TYPE_TL_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'DEAL_COMP_TYPE_TL',
                                               NULL,
                                               NULL);         
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'DEAL_COMP_TYPE,LANG',
                    L_error_message);
         L_error :=TRUE;
      end if;
 
       
      if rec.action IN (action_mod,action_del)
         and rec.lang is NOT NULL
         and rec.deal_comp_type is NOT NULL
         and rec.PK_DEAL_COMP_TYPE_TL_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                    'DEAL_COMP_TYPE,LANG',
                    'NO_RECORD_UPD_DEL');
         L_error :=TRUE;
      else
         if rec.action = action_del
            and rec.lang = LP_primary_lang then
			           WRITE_ERROR(I_process_id,
                             SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                             I_chunk_id,
                             L_table,
                             rec.row_seq,
                            'LANG',
                            'CAN_NOT_DEL_PRIM_LANG');
                 L_error :=TRUE;		    
	     end if;			 	  
	  end if;
      
      if rec.action in (action_new)
	     and rec.lang is NOT NULL
         and rec.cd_lang_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'LANG',
                     'LANG_EXIST');
         L_error :=TRUE;
      end if;
      
      if rec.deal_comp_type is not null
         and rec.dctt_dct_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEAL_COMP_TYPE',
                     'INV_DEAL_COMP_TYPE');
         L_error :=TRUE;
      end if;
      
      if rec.action in (action_new,action_mod)
       and NOT(  rec.DEAL_COMP_TYPE_DESC  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEAL_COMP_TYPE_DESC',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
      
      
          
      if NOT L_error then
         L_deal_comp_type_tl_temp_rec.deal_comp_type                   := rec.deal_comp_type;
         L_deal_comp_type_tl_temp_rec.lang                             := rec.lang;
         L_deal_comp_type_tl_temp_rec.orig_lang_ind                    := 'N';
         L_deal_comp_type_tl_temp_rec.reviewed_ind                     := 'Y';
         L_deal_comp_type_tl_temp_rec.deal_comp_type_desc              := rec.deal_comp_type_desc;
         L_deal_comp_type_tl_temp_rec.create_id                        := USER;
         L_deal_comp_type_tl_temp_rec.create_datetime                  := SYSDATE;
         L_deal_comp_type_tl_temp_rec.last_update_id                   := USER;
         L_deal_comp_type_tl_temp_rec.last_update_datetime             := SYSDATE;
         if rec.action = action_new then
    
            if EXEC_DEAL_COMP_TYPE_TL_INS(   L_deal_comp_type_tl_temp_rec,
                                             L_error_message)=FALSE then
                                          
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      
         if rec.action = action_mod then
            if EXEC_DEAL_COMP_TYPE_TL_UPD( L_deal_comp_type_tl_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            
            end if;
         end if;
      
         if rec.action = action_del then
            if EXEC_DEAL_COMP_TYPE_TL_DEL( L_deal_comp_type_tl_temp_rec,
                                           L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
                       
               L_process_error :=TRUE;
            end if;
         end if;
  
      else
         update svc_deal_comp_type_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_deal_comp_type_tl st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_deal_comp_type_tl st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_DEAL_COMP_TYPE_TL%ISOPEN then
         close C_SVC_DEAL_COMP_TYPE_TL;
      end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DEAL_COMP_TYPE_TL;
---------------------------------------------------------------------------------------------
FUNCTION MERGE_DEAL_COMP_TYPE_TL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_deal_comp_type           IN       DEAL_COMP_TYPE_TL.DEAL_COMP_TYPE%TYPE,
                                 I_deal_comp_type_desc      IN       DEAL_COMP_TYPE_TL.DEAL_COMP_TYPE_DESC%TYPE,
                                 I_lang                     IN       DEAL_COMP_TYPE_TL.LANG%TYPE)
   RETURN BOOLEAN is
   L_program       VARCHAR2(61) := 'MERGE_DEAL_COMP_TYPE_TL';
   L_table         VARCHAR2(30) := 'DEAL_COMP_TYPE_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_deal_comp_type_exists    VARCHAR2(1)  := NULL;
   L_orig_lang_ind            VARCHAR2(1)  := 'N';
   L_reviewed_ind             VARCHAR2(1)  := 'N';

   cursor C_DEAL_COMP_TYPE_EXIST is
      select 'x'
        from deal_comp_type_tl
       where deal_comp_type = I_deal_comp_type
         and rownum = 1;

   cursor C_LOCK_DEAL_COMP_TYPE_TL is
      select 'x'
        from deal_comp_type_tl
       where deal_comp_type = I_deal_comp_type
         and lang   = I_lang
         for update nowait;

BEGIN

   -- Check first if the deal_comp_type (regardless of language) already exists in the table.
   -- If it already exists, set orig_lang_ind = 'N'. Otherwise, set orig_lang_ind = 'Y'.
   -- Reviewed_ind is only set to 'N' for entries in the original language to indicate
   -- if the original description has changed and translation should be reviewed for accuracy.
   -- Both flags are only used for inserts, not updates.

   open C_DEAL_COMP_TYPE_EXIST;
   fetch C_DEAL_COMP_TYPE_EXIST into L_deal_comp_type_exists;
   close C_DEAL_COMP_TYPE_EXIST;

   if L_deal_comp_type_exists is NULL then
      L_orig_lang_ind := 'Y';
      L_reviewed_ind  := 'N';
   else
      L_orig_lang_ind := 'N';
      L_reviewed_ind  := 'Y';
   end if;

   open C_LOCK_DEAL_COMP_TYPE_TL;
   close C_LOCK_DEAL_COMP_TYPE_TL;

   merge into deal_comp_type_tl dctt
      using (select I_deal_comp_type deal_comp_type,
                    I_lang lang,
                    I_deal_comp_type_desc deal_comp_type_desc,
                    L_orig_lang_ind orig_lang_ind, --used for inserts only
                    L_reviewed_ind reviewed_ind,   --used for inserts only
                    user create_id,
                    sysdate create_datetime,
                    user last_update_id,
                    sysdate last_update_datetime
               from dual) use_this
         on (dctt.deal_comp_type = use_this.deal_comp_type and
             dctt.lang  = use_this.lang)
   when matched then
      update
         set dctt.deal_comp_type_desc = use_this.deal_comp_type_desc,
             dctt.reviewed_ind = decode(dctt.orig_lang_ind, 'Y', 'N', reviewed_ind), --when description is changed for the original language, set the entry for translation review
             dctt.last_update_id = use_this.last_update_id,
             dctt.last_update_datetime = use_this.last_update_datetime
   when NOT matched then
      insert (deal_comp_type,
              lang,
              deal_comp_type_desc,
              orig_lang_ind,
              reviewed_ind,
              create_id,
              create_datetime,
              last_update_id,
              last_update_datetime)
      values (use_this.deal_comp_type,
              use_this.lang,
              use_this.deal_comp_type_desc,
              use_this.orig_lang_ind,
              use_this.reviewed_ind,
              use_this.create_id,
              use_this.create_datetime,
              use_this.last_update_id,
              use_this.last_update_datetime);

   return TRUE;
EXCEPTION
   when RECORD_LOCKED THEN
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                            L_table,
                                            I_deal_comp_type,
                                            I_LANG);
      return FALSE;
   when OTHERS then
      if C_DEAL_COMP_TYPE_EXIST%ISOPEN then
         close C_DEAL_COMP_TYPE_EXIST;
      end if;
      if C_LOCK_DEAL_COMP_TYPE_TL%ISOPEN then
         close C_LOCK_DEAL_COMP_TYPE_TL;
      end if;
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_DEAL_COMP_TYPE_TL;
--------------------------------------------------------------------------------
FUNCTION EXEC_DEAL_COMP_TYPE_INS(  L_deal_comp_type_temp_rec   IN       DEAL_COMP_TYPE%ROWTYPE,
                                   O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_rec                       IN       C_SVC_DEAL_COMP_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_DEAL_COMP_TYPE.EXEC_DEAL_COMP_TYPE_INS';
   L_table   VARCHAR2(255):= 'SVC_DEAL_COMP_TYPE';
BEGIN
   SAVEPOINT DEAL_COMP_TYPE_INS;

   insert
     into deal_comp_type
   values L_deal_comp_type_temp_rec;

   if MERGE_DEAL_COMP_TYPE_TL(O_error_message,
                              I_rec.deal_comp_type,
                              I_rec.deal_comp_type_desc,
                              LP_primary_lang) = FALSE then
      ROLLBACK TO SAVEPOINT DEAL_COMP_TYPE_INS;
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DEAL_COMP_TYPE_INS;
------------------------------------------------------------------------------------
FUNCTION EXEC_DEAL_COMP_TYPE_UPD( L_deal_comp_type_temp_rec   IN       DEAL_COMP_TYPE%ROWTYPE,
                                  O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_rec                       IN       C_SVC_DEAL_COMP_TYPE%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_DEAL_COMP_TYPE.EXEC_DEAL_COMP_TYPE_UPD';
   L_table   VARCHAR2(255):= 'SVC_DEAL_COMP_TYPE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DEAL_COMP_TYPE_UPD is
      select 'x'
        from DEAL_COMP_TYPE
       where DEAL_COMP_TYPE = L_deal_comp_type_temp_rec.DEAL_COMP_TYPE
         for update nowait;
BEGIN

   open C_LOCK_DEAL_COMP_TYPE_UPD;
   close C_LOCK_DEAL_COMP_TYPE_UPD;

   if MERGE_DEAL_COMP_TYPE_TL(O_error_message,
                              I_rec.deal_comp_type,
                              I_rec.deal_comp_type_desc,
                              LP_primary_lang) = FALSE then
      return FALSE;
   end if;

   update deal_comp_type
      set row = L_deal_comp_type_temp_rec
    where 1 = 1
      and deal_comp_type = L_deal_comp_type_temp_rec.deal_comp_type
;
   return TRUE;
EXCEPTION
   when OTHERS then
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_LOCK_DEAL_COMP_TYPE_UPD%ISOPEN then
         close C_LOCK_DEAL_COMP_TYPE_UPD;
      end if;
	  return FALSE;
END EXEC_DEAL_COMP_TYPE_UPD;
-----------------------------------------------------------------------------------------
FUNCTION EXEC_DEAL_COMP_TYPE_DEL(  L_deal_comp_type_temp_rec   IN   DEAL_COMP_TYPE%ROWTYPE ,
                                   O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_DEAL_COMP_TYPE.EXEC_DEAL_COMP_TYPE_DEL';
   L_table   VARCHAR2(255):= 'SVC_DEAL_COMP_TYPE';

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DEAL_COMP_TYPE_DEL is
      select 'x'
        from deal_comp_type
       where deal_comp_type = L_deal_comp_type_temp_rec.deal_comp_type
         for update nowait;

   cursor C_LOCK_DEAL_COMP_TYPE_TL is
      select 'x'
        from DEAL_COMP_TYPE_TL
       where DEAL_COMP_TYPE = L_deal_comp_type_temp_rec.deal_comp_type
         for update nowait;

BEGIN

   open C_LOCK_DEAL_COMP_TYPE_TL;
   close C_LOCK_DEAL_COMP_TYPE_TL;

   open C_LOCK_DEAL_COMP_TYPE_DEL;
   close C_LOCK_DEAL_COMP_TYPE_DEL;

   delete
     from deal_comp_type_tl
    where 1 = 1
      and deal_comp_type = L_deal_comp_type_temp_rec.deal_comp_type
;
   
   delete
     from deal_comp_type
    where 1 = 1
      and deal_comp_type = L_deal_comp_type_temp_rec.deal_comp_type
;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_DEAL_COMP_TYPE_DEL%ISOPEN then
         close C_LOCK_DEAL_COMP_TYPE_DEL;
      end if;
      
      if C_LOCK_DEAL_COMP_TYPE_TL%ISOPEN then
         close C_LOCK_DEAL_COMP_TYPE_TL;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DEAL_COMP_TYPE_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_DEAL_COMP_TYPE( I_process_id   IN   SVC_DEAL_COMP_TYPE.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_DEAL_COMP_TYPE.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_DEAL_COMP_TYPE.PROCESS_DEAL_COMP_TYPE';
   L_error_message VARCHAR2(600);
   L_DEAL_COMP_TYPE_temp_rec DEAL_COMP_TYPE%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_DEAL_COMP_TYPE';
   L_exists varchar2(1);
   cursor c_child_deal_comp_type_exists(I_deal_comp_type SVC_DEAL_COMP_TYPE.DEAL_COMP_TYPE%TYPE) is
      select 'x'
	    from deal_detail
	   where deal_comp_type=I_deal_comp_type;
BEGIN
   FOR rec IN c_svc_DEAL_COMP_TYPE(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
 
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
        
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
   
      if rec.action = action_new
         and rec.PK_DEAL_COMP_TYPE_rid is NOT NULL then
   
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEAL_COMP_TYPE',
                    'REC_EXIST');
      
         L_error :=TRUE;
      end if;
          
      if rec.action IN (action_mod,action_del)
         and rec.PK_DEAL_COMP_TYPE_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEAL_COMP_TYPE',
                    'NO_RECORD_UPD_DEL');
        
         L_error :=TRUE;
      end if;

      if rec.action in (action_new,action_mod)
       and NOT(  rec.DEAL_COMP_TYPE_DESC  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEAL_COMP_TYPE_DESC',
                    'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
	  
      if rec.action IN (action_del) then
         open c_child_deal_comp_type_exists(rec.deal_comp_type);
		 fetch c_child_deal_comp_type_exists into L_exists;
		 close c_child_deal_comp_type_exists;
         if L_exists = 'x' then
		 L_error_message := SQL_LIB.CREATE_MSG('CANNOT_DEL_REC',
                                                'DEAL_DETAIL',
                                                NULL,
                                                NULL);
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEAL_COMP_TYPE',
                     L_error_message);
         L_error :=TRUE;  
         end if;		 
      end if;
           
      if NOT L_error then
         L_deal_comp_type_temp_rec.deal_comp_type              := rec.deal_comp_type;
         L_deal_comp_type_temp_rec.create_id                   := USER;
         L_deal_comp_type_temp_rec.create_datetime             := SYSDATE;
         if rec.action = action_new then
            if EXEC_DEAL_COMP_TYPE_INS(   L_deal_comp_type_temp_rec,
                                          L_error_message,
                                          rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
             
            end if;
         end if;
       
         if rec.action = action_mod then
            if EXEC_DEAL_COMP_TYPE_UPD( L_deal_comp_type_temp_rec,
                                             L_error_message,
                                             rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
               
            end if;
         end if;
       
         if rec.action = action_del then
            if EXEC_DEAL_COMP_TYPE_DEL( L_deal_comp_type_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
             
            end if;
         end if;
      
      else
         update svc_deal_comp_type st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_deal_comp_type st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_deal_comp_type st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_SVC_DEAL_COMP_TYPE%ISOPEN then
         close C_SVC_DEAL_COMP_TYPE;
      end if;
      
      if c_child_deal_comp_type_exists%ISOPEN then
         close c_child_deal_comp_type_exists;
      end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DEAL_COMP_TYPE;
-------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete 
     from svc_deal_comp_type_tl 
    where process_id=I_process_id;

   delete 
     from svc_deal_comp_type
    where process_id=I_process_id;
END;
------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER
                  )
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_DEAL_COMP_TYPE.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();


   if PROCESS_DEAL_COMP_TYPE(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   commit;
   if PROCESS_DEAL_COMP_TYPE_TL(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_WARN_COUNT%ISOPEN then
         close C_GET_WARN_COUNT;
      end if;
      
      if C_GET_ERR_COUNT%ISOPEN then
         close C_GET_ERR_COUNT;
      end if;
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_DEAL_COMP_TYPE;
/