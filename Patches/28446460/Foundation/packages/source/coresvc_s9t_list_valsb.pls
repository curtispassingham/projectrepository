create or replace PACKAGE BODY CORESVC_S9T_LIST_VALS AS
   cursor C_SVC_S9T_LIST_VALS(I_process_id NUMBER,
                                  I_chunk_id NUMBER) is
      select
             s9t_list_vals_pk.rowid  AS s9t_list_vals_pk_rid,
             st.rowid AS st_rid,
             cd_fk.rowid As cd_rid,
             s9twf_fk.rowid As s9twf_rid,
             s9tcf_fk.rowid As s9tcf_rid,
             st.code,
             st.column_name,
             st.sheet_name, 
             st.template_category,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action) AS action,
             st.process$status
        from svc_s9t_list_vals st,
             s9t_list_vals s9t_list_vals_pk,
             code_detail cd_fk,
             S9T_TMPL_WKSHT_DEF s9twf_fk,
             S9T_TMPL_COLS_DEF s9tcf_fk,
             dual
       where st.process_id                = I_process_id
         and st.chunk_id                  = I_chunk_id
         and st.template_category         = s9t_list_vals_pk.template_category (+)
         and st.sheet_name                = s9t_list_vals_pk.sheet_name (+)
         and st.column_name               = s9t_list_vals_pk.column_name (+)
         and st.template_category         = cd_fk.code(+)
         and cd_fk.code_type (+)          ='S9LV'  
         and st.sheet_name                = s9twf_fk.wksht_key(+) 
         and st.column_name               = s9tcf_fk.column_key(+)
         and st.sheet_name                = s9tcf_fk.wksht_key(+)

;
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   S9T_LIST_VALS_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                               :=s9t_pkg.get_sheet_names(I_file_id);
   S9T_LIST_VALS_cols                     :=s9t_pkg.get_col_names(I_file_id,S9T_LIST_VALS_sheet);
   S9T_LIST_VALS$Action                   := S9T_LIST_VALS_cols('ACTION');
   S9T_LIST_VALS$CODE                     := S9T_LIST_VALS_cols('CODE');
   S9T_LIST_VALS$COLUMN_NAME              := S9T_LIST_VALS_cols('COLUMN_NAME');
   S9T_LIST_VALS$SHEET_NAME               := S9T_LIST_VALS_cols('SHEET_NAME');
   S9T_LIST_VALS$TMPL_CATEGORY            := S9T_LIST_VALS_cols('TEMPLATE_CATEGORY');
END POPULATE_NAMES;
-----------------------------------------------------------------------------
PROCEDURE POPULATE_S9T_LIST_VALS( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = S9T_LIST_VALS_sheet )
   select s9t_row(s9t_cells(CORESVC_S9T_LIST_VALS.action_mod ,template_category,sheet_name,column_name,code
                           ))
     from s9t_list_vals ;
END POPULATE_S9T_LIST_VALS;
-----------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
   cursor C_USER_LANG is
      select lang
        from user_attrib
       where user_id = get_user;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   SQL_LIB.SET_MARK('OPEN','C_USER_LANG','USER_ATTRIB',NULL);
   open C_USER_LANG;
   SQL_LIB.SET_MARK('FETCH','C_USER_LANG','USER_ATTRIB',NULL);
   fetch C_USER_LANG into l_file.user_lang;
   SQL_LIB.SET_MARK('CLOSE','C_USER_LANG','USER_ATTRIB',NULL);
   close C_USER_LANG;
   if l_file.user_lang IS NULL then
      L_file.user_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   end if;
   L_file.add_sheet(S9T_LIST_VALS_sheet);
   L_file.sheets(l_file.get_sheet_index(S9T_LIST_VALS_sheet)).column_headers := s9t_cells( 'ACTION','TEMPLATE_CATEGORY','SHEET_NAME','COLUMN_NAME','CODE'
                                                                                         );
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(255):='CORESVC_S9T_LIST_VALS.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;   
   if I_template_only_ind = 'N' then
      POPULATE_S9T_LIST_VALS(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_S9T_LIST_VALS( I_file_id    IN   s9t_folder.file_id%TYPE,
                                               I_process_id IN   SVC_S9T_LIST_VALS.process_id%TYPE) IS
   TYPE svc_S9T_LIST_VALS_col_typ IS TABLE OF SVC_S9T_LIST_VALS%ROWTYPE;
   L_temp_rec SVC_S9T_LIST_VALS%ROWTYPE;
   svc_S9T_LIST_VALS_col svc_S9T_LIST_VALS_col_typ :=NEW svc_S9T_LIST_VALS_col_typ();
   L_process_id SVC_S9T_LIST_VALS.process_id%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_S9T_LIST_VALS%ROWTYPE;
      L_pk_columns    VARCHAR2(255)  := 'Template Category , Sheet name, Column Name';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

   cursor C_MANDATORY_IND is
      select
             CODE_mi,
             COLUMN_NAME_mi,
             SHEET_NAME_mi,
             TEMPLATE_CATEGORY_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'S9T_LIST_VALS'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN (
                                         'CODE' AS CODE,
                                         'COLUMN_NAME' AS COLUMN_NAME,
                                         'SHEET_NAME' AS SHEET_NAME,
                                         'TEMPLATE_CATEGORY' AS TEMPLATE_CATEGORY,
                                            null as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       CODE_dv,
                       COLUMN_NAME_dv,
                       SHEET_NAME_dv,
                       TEMPLATE_CATEGORY_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                       = 'S9T_LIST_VALS'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'CODE' AS CODE,
                                                      'COLUMN_NAME' AS COLUMN_NAME,
                                                      'SHEET_NAME' AS SHEET_NAME,
                                                      'TEMPLATE_CATEGORY' AS TEMPLATE_CATEGORY,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.CODE := rec.CODE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'S9T_LIST_VALS ' ,
                            NULL,
                           'CODE ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.COLUMN_NAME := rec.COLUMN_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'S9T_LIST_VALS ' ,
                            NULL,
                           'COLUMN_NAME ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.SHEET_NAME := rec.SHEET_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'S9T_LIST_VALS ' ,
                            NULL,
                           'SHEET_NAME ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TEMPLATE_CATEGORY := rec.TEMPLATE_CATEGORY_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'S9T_LIST_VALS ' ,
                            NULL,
                           'TEMPLATE_CATEGORY ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(S9T_LIST_VALS$Action)      AS Action,
          r.get_cell(S9T_LIST_VALS$CODE)              AS CODE,
          r.get_cell(S9T_LIST_VALS$COLUMN_NAME)              AS COLUMN_NAME,
          r.get_cell(S9T_LIST_VALS$SHEET_NAME)              AS SHEET_NAME,
          r.get_cell(S9T_LIST_VALS$TMPL_CATEGORY)              AS TEMPLATE_CATEGORY,
          r.get_row_seq()                             AS row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(S9T_LIST_VALS_sheet)
  )
   LOOP
      L_temp_rec:=NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
S9T_LIST_VALS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CODE := rec.CODE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
S9T_LIST_VALS_sheet,
                            rec.row_seq,
                            'CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COLUMN_NAME := rec.COLUMN_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
S9T_LIST_VALS_sheet,
                            rec.row_seq,
                            'COLUMN_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SHEET_NAME := rec.SHEET_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
S9T_LIST_VALS_sheet,
                            rec.row_seq,
                            'SHEET_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TEMPLATE_CATEGORY := rec.TEMPLATE_CATEGORY;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
S9T_LIST_VALS_sheet,
                            rec.row_seq,
                            'TEMPLATE_CATEGORY',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_S9T_LIST_VALS.action_new then
         L_temp_rec.CODE := NVL( L_temp_rec.CODE,L_default_rec.CODE);
         L_temp_rec.COLUMN_NAME := NVL( L_temp_rec.COLUMN_NAME,L_default_rec.COLUMN_NAME);
         L_temp_rec.SHEET_NAME := NVL( L_temp_rec.SHEET_NAME,L_default_rec.SHEET_NAME);
         L_temp_rec.TEMPLATE_CATEGORY := NVL( L_temp_rec.TEMPLATE_CATEGORY,L_default_rec.TEMPLATE_CATEGORY);
      end if;
      if not (
            L_temp_rec.TEMPLATE_CATEGORY is NOT NULL and
            L_temp_rec.SHEET_NAME is NOT NULL and
            L_temp_rec.COLUMN_NAME is NOT NULL and
            1 = 1
            )then
			WRITE_S9T_ERROR(I_file_id,
						 S9T_LIST_VALS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
     if NOT L_error then
         svc_S9T_LIST_VALS_col.extend();
         svc_S9T_LIST_VALS_col(svc_S9T_LIST_VALS_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_S9T_LIST_VALS_col.COUNT SAVE EXCEPTIONS
      merge into SVC_S9T_LIST_VALS st
      using(select
                  (case
                   when l_mi_rec.CODE_mi    = 'N'
                    and svc_S9T_LIST_VALS_col(i).action = CORESVC_S9T_LIST_VALS.action_mod
                    and s1.CODE IS NULL
                   then mt.CODE
                   else s1.CODE
                   end) AS CODE,
                  (case
                   when l_mi_rec.COLUMN_NAME_mi    = 'N'
                    and svc_S9T_LIST_VALS_col(i).action = CORESVC_S9T_LIST_VALS.action_mod
                    and s1.COLUMN_NAME IS NULL
                   then mt.COLUMN_NAME
                   else s1.COLUMN_NAME
                   end) AS COLUMN_NAME,
                  (case
                   when l_mi_rec.SHEET_NAME_mi    = 'N'
                    and svc_S9T_LIST_VALS_col(i).action = CORESVC_S9T_LIST_VALS.action_mod
                    and s1.SHEET_NAME IS NULL
                   then mt.SHEET_NAME
                   else s1.SHEET_NAME
                   end) AS SHEET_NAME,
                  (case
                   when l_mi_rec.TEMPLATE_CATEGORY_mi    = 'N'
                    and svc_S9T_LIST_VALS_col(i).action = CORESVC_S9T_LIST_VALS.action_mod
                    and s1.TEMPLATE_CATEGORY IS NULL
                   then mt.TEMPLATE_CATEGORY
                   else s1.TEMPLATE_CATEGORY
                   end) AS TEMPLATE_CATEGORY,
                  null as dummy
              from (select
                          svc_S9T_LIST_VALS_col(i).CODE AS CODE,
                          svc_S9T_LIST_VALS_col(i).COLUMN_NAME AS COLUMN_NAME,
                          svc_S9T_LIST_VALS_col(i).SHEET_NAME AS SHEET_NAME,
                          svc_S9T_LIST_VALS_col(i).TEMPLATE_CATEGORY AS TEMPLATE_CATEGORY,
                          null as dummy
                      from dual ) s1,
            S9T_LIST_VALS mt
             where
                  mt.TEMPLATE_CATEGORY (+)     = s1.TEMPLATE_CATEGORY   and
                  mt.SHEET_NAME (+)     = s1.SHEET_NAME   and
                  mt.COLUMN_NAME (+)     = s1.COLUMN_NAME   and
                  1 = 1 )sq
                on (
                    st.TEMPLATE_CATEGORY      = sq.TEMPLATE_CATEGORY and
                    st.SHEET_NAME      = sq.SHEET_NAME and
                    st.COLUMN_NAME      = sq.COLUMN_NAME and
                    svc_S9T_LIST_VALS_col(i).ACTION IN (CORESVC_S9T_LIST_VALS.action_mod,CORESVC_S9T_LIST_VALS.action_del))
      when matched then
      update
         set process_id      = svc_S9T_LIST_VALS_col(i).process_id ,
             chunk_id        = svc_S9T_LIST_VALS_col(i).chunk_id ,
             row_seq         = svc_S9T_LIST_VALS_col(i).row_seq ,
             action          = svc_S9T_LIST_VALS_col(i).action ,
             process$status  = svc_S9T_LIST_VALS_col(i).process$status ,
             code              = sq.code ,
             create_id       = svc_S9T_LIST_VALS_col(i).create_id ,
             create_datetime = svc_S9T_LIST_VALS_col(i).create_datetime ,
             last_upd_id     = svc_S9T_LIST_VALS_col(i).last_upd_id ,
             last_upd_datetime = svc_S9T_LIST_VALS_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             code ,
             column_name ,
             sheet_name ,
             template_category ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_S9T_LIST_VALS_col(i).process_id ,
             svc_S9T_LIST_VALS_col(i).chunk_id ,
             svc_S9T_LIST_VALS_col(i).row_seq ,
             svc_S9T_LIST_VALS_col(i).action ,
             svc_S9T_LIST_VALS_col(i).process$status ,
             sq.code ,
             sq.column_name ,
             sq.sheet_name ,
             sq.template_category ,
             svc_S9T_LIST_VALS_col(i).create_id ,
             svc_S9T_LIST_VALS_col(i).create_datetime ,
             svc_S9T_LIST_VALS_col(i).last_upd_id ,
             svc_S9T_LIST_VALS_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR(I_file_id,
                            S9T_LIST_VALS_sheet,
                            svc_S9T_LIST_VALS_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
	END;

END PROCESS_S9T_S9T_LIST_VALS;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT        OUT   NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_program VARCHAR2(255):='CORESVC_S9T_LIST_VALS.process_s9t';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);

BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_S9T_LIST_VALS(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.count();
      forall i in 1..O_error_count
         insert into s9t_errors
              values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      commit;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION EXEC_S9T_LIST_VALS_INS(  L_s9t_list_vals_temp_rec   IN   S9T_LIST_VALS%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_S9T_LIST_VALS.EXEC_S9T_LIST_VALS_INS';
   L_table   VARCHAR2(255):= 'SVC_S9T_LIST_VALS';
BEGIN
   insert
     into s9t_list_vals
   values L_s9t_list_vals_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_S9T_LIST_VALS_INS;
---------------------------------------------------------------------------------------------
FUNCTION EXEC_S9T_LIST_VALS_UPD( L_s9t_list_vals_temp_rec   IN   S9T_LIST_VALS%ROWTYPE,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_S9T_LIST_VALS.EXEC_S9T_LIST_VALS_UPD';
   L_table   VARCHAR2(255):= 'SVC_S9T_LIST_VALS';
BEGIN
   update s9t_list_vals
      set row = L_s9t_list_vals_temp_rec
    where 1 = 1
      and template_category = L_s9t_list_vals_temp_rec.template_category
      and sheet_name = L_s9t_list_vals_temp_rec.sheet_name
      and column_name = L_s9t_list_vals_temp_rec.column_name
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_S9T_LIST_VALS_UPD;
-------------------------------------------------------------------------------------------
FUNCTION EXEC_S9T_LIST_VALS_DEL(  L_s9t_list_vals_temp_rec   IN   S9T_LIST_VALS%ROWTYPE ,
                                     O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_S9T_LIST_VALS.EXEC_S9T_LIST_VALS_DEL';
   L_table   VARCHAR2(255):= 'SVC_S9T_LIST_VALS';
BEGIN
   delete
     from s9t_list_vals
    where 1 = 1
      and template_category = L_s9t_list_vals_temp_rec.template_category
      and sheet_name = L_s9t_list_vals_temp_rec.sheet_name
      and column_name = L_s9t_list_vals_temp_rec.column_name
;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_S9T_LIST_VALS_DEL;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T_LIST_VALS( I_process_id   IN   SVC_S9T_LIST_VALS.PROCESS_ID%TYPE,
                                   I_chunk_id     IN SVC_S9T_LIST_VALS.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_program   VARCHAR2(255):='CORESVC_S9T_LIST_VALS.PROCESS_S9T_LIST_VALS';
   L_error_message VARCHAR2(600);
   L_S9T_LIST_VALS_temp_rec S9T_LIST_VALS%ROWTYPE;
   L_table VARCHAR2(255)    :='SVC_S9T_LIST_VALS';
BEGIN
   FOR rec IN c_svc_S9T_LIST_VALS(I_process_id,
                                     I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=true;
      end if;
      
      if rec.action = action_new
         and rec.cd_rid is null then
         L_error_message := SQL_LIB.CREATE_MSG('INV_CODE',
                                               UPPER(rec.template_category),
                                               'S9LV',
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CODE_TYPE,TEMPLATE_CATEGORY,COLUMN_NAME',
                     L_error_message);
         L_error :=TRUE;     
      end if;
 
     if rec.action = action_new
         and rec.s9twf_rid is null then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'S9T_TMPL_WKSHT_DEF',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SHEET_NAME',
                     L_error_message);
         L_error :=TRUE;     
      end if;

     if rec.action = action_new
         and rec.s9twf_rid is null then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'S9T_TMPL_COLS_DEF',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'COLUMN_NAME',
                     L_error_message);
         L_error :=TRUE;     
      end if;
       
      if rec.action = action_new
         and rec.S9T_LIST_VALS_PK_rid is NOT NULL then
         L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                               'S9T_LIST_VALS',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SHEET_NAME,TEMPLATE_CATEGORY,COLUMN_NAME',
                     L_error_message);
         L_error :=TRUE;
      end if;
	  
      if rec.action IN (action_mod,action_del)
         and rec.S9T_LIST_VALS_PK_rid is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('NOT_FOUND_RECORD',
                                               'S9T_LIST_VALS',
                                                NULL,
                                                NULL); 

         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'SHEET_NAME,TEMPLATE_CATEGORY,COLUMN_NAME',
                    L_error_message);
         L_error :=TRUE;
      end if;
      
    if rec.action in (action_new,action_mod)
	   and NOT(  rec.CODE  IS NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'CODE',
                    'ENT_CODE_REQ');
         L_error :=TRUE;
      end if;
      if NOT L_error then
         L_s9t_list_vals_temp_rec.template_category              := rec.template_category;
         L_s9t_list_vals_temp_rec.sheet_name                     := rec.sheet_name;
         L_s9t_list_vals_temp_rec.column_name                    := rec.column_name;
         L_s9t_list_vals_temp_rec.code                           := rec.code;
         if rec.action = action_new then
            if EXEC_S9T_LIST_VALS_INS(   L_s9t_list_vals_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_S9T_LIST_VALS_UPD( L_s9t_list_vals_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_S9T_LIST_VALS_DEL( L_s9t_list_vals_temp_rec,
                                             L_error_message)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
      else
         update svc_s9t_list_vals st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
      if NOT L_process_error then
         update svc_s9t_list_vals st
            set process$status ='P'
          where rowid = rec.st_rid
            and st.process$status != 'E';
      else
         update svc_s9t_list_vals st
            set process$status ='E'
          where rowid = rec.st_rid;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if c_svc_S9T_LIST_VALS%isopen then
         close c_svc_S9T_LIST_VALS;
      end if;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T_LIST_VALS;
-------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id      IN       NUMBER)
IS
BEGIN
   delete 
     from svc_s9t_list_vals 
    where process_id=I_process_id;
END;
-------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_S9T_LIST_VALS.PROCESS';
   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';

BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_S9T_LIST_VALS(I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
      
   O_error_count := LP_errors_tab.COUNT();
   
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   
   LP_errors_tab := NEW errors_tab_typ();
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status =(case
                   when status='PE'
                   then 'PE'
                   else L_process_status
                   end),
          action_date=sysdate
    where process_id=I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;


   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      if c_get_err_count%isopen then
         close c_get_err_count;
      end if;
      if c_get_warn_count%isopen then
         close c_get_warn_count;
      end if;
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_S9T_LIST_VALS;
/