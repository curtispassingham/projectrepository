CREATE OR REPLACE PACKAGE BODY NEW_ITEM_LOC_SQL AS
--------------------------------------------------------------------------------
--   GLOBULE VARIABLES
--------------------------------------------------------------------------------
LP_system_options        SYSTEM_OPTIONS%ROWTYPE;
LP_date                  DATE                   := GET_VDATE;
LP_user_id               VARCHAR2(30)           := NVL(SYS_CONTEXT('RETAIL_CTX', 'APP_USER_ID'),  USER);
LP_from_itemloc_online   VARCHAR2(1)            := 'N';
LP_called_from_PO        VARCHAR2(1)            := 'N';
--------------------------------------------------------------------------------
--   PRIVATE  FUNCTIONS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION POPULATE_WORKING_TABLE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_input            IN       NIL_INPUT_TBL,
                                I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE,
                                I_apply_sec_ind    IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION VALIDATE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE,
                  I_apply_sec_ind    IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXPLODE_LOCS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE,
                      I_apply_sec_ind    IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_SOURCE_METHOD(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEMS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_AV_WEIGHT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION COMPLETE_ITEM_LOC_REC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_RETAIL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CONVERT_CURRENCIES(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION COMPUTE_ITEM_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_comp_item_cost_tbl   IN OUT   OBJ_COMP_ITEM_COST_TBL,
                           I_nil_process_id        IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_TAX(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 IO_comp_item_cost_tbl   IN OUT   OBJ_COMP_ITEM_COST_TBL,
                 I_nil_process_id        IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PERSIST_ITEM_LOC_TRAITS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION PERSIST_ITEM_SUPP_COUNTRY_LOC(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       IO_comp_item_cost_tbl   IN OUT   NOCOPY OBJ_COMP_ITEM_COST_TBL,
                                       I_nil_process_id        IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CLEAR_WORKING_TABLE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION CHECK_SOURCE_WH(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_nil_process_id IN     NIL_INPUT_WORKING.PROCESS_ID%TYPE)               
RETURN BOOLEAN;
--------------------------------------------------------------------------------
--   PUBLIC  FUNCTIONS
--------------------------------------------------------------------------------
FUNCTION NEW_ITEM_LOC_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_input           IN       WRP_NIL_INPUT_TBL,
                          I_apply_sec_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN NUMBER IS
L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.NEW_ITEM_LOC_WRP';
L_input_tbl     NIL_INPUT_TBL;
L_input_rec     NIL_INPUT_RECORD;
BEGIN
   
   --copy the data in database type IO_sup_inv_mgmt_tbl to PLSQL type L_sup_inv_mgmt_tbl
   for i in I_input.FIRST .. I_input.last loop
      L_input_rec.ITEM                    := I_input(i).ITEM;
      L_input_rec.ITEM_PARENT             := I_input(i).ITEM_PARENT;
      L_input_rec.ITEM_GRANDPARENT        := I_input(i).ITEM_GRANDPARENT;
      L_input_rec.ITEM_SHORT_DESC         := I_input(i).ITEM_SHORT_DESC;
      L_input_rec.DEPT                    := I_input(i).DEPT;
      L_input_rec.ITEM_CLASS              := I_input(i).ITEM_CLASS;
      L_input_rec.SUBCLASS                := I_input(i).SUBCLASS;
      L_input_rec.ITEM_LEVEL              := I_input(i).ITEM_LEVEL;
      L_input_rec.TRAN_LEVEL              := I_input(i).TRAN_LEVEL;
      L_input_rec.ITEM_STATUS             := I_input(i).ITEM_STATUS;
      L_input_rec.WASTE_TYPE              := I_input(i).WASTE_TYPE;
      L_input_rec.SELLABLE_IND            := I_input(i).SELLABLE_IND;
      L_input_rec.ORDERABLE_IND           := I_input(i).ORDERABLE_IND;
      L_input_rec.PACK_IND                := I_input(i).PACK_IND;
      L_input_rec.PACK_TYPE               := I_input(i).PACK_TYPE;
      L_input_rec.ITEM_DESC               := I_input(i).ITEM_DESC;
      L_input_rec.DIFF_1                  := I_input(i).DIFF_1;
      L_input_rec.DIFF_2                  := I_input(i).DIFF_2;
      L_input_rec.DIFF_3                  := I_input(i).DIFF_3;
      L_input_rec.DIFF_4                  := I_input(i).DIFF_4;
      L_input_rec.LOC                     := I_input(i).LOC;
      L_input_rec.LOC_TYPE                := I_input(i).LOC_TYPE;
      L_input_rec.DAILY_WASTE_PCT         := I_input(i).DAILY_WASTE_PCT;
      L_input_rec.UNIT_COST_LOC           := I_input(i).UNIT_COST_LOC;
      L_input_rec.UNIT_RETAIL_LOC         := I_input(i).UNIT_RETAIL_LOC;
      L_input_rec.SELLING_RETAIL_LOC      := I_input(i).SELLING_RETAIL_LOC;
      L_input_rec.SELLING_UOM             := I_input(i).SELLING_UOM;
      L_input_rec.MULTI_UNITS             := I_input(i).MULTI_UNITS;
      L_input_rec.MULTI_UNIT_RETAIL       := I_input(i).MULTI_UNIT_RETAIL;
      L_input_rec.MULTI_SELLING_UOM       := I_input(i).MULTI_SELLING_UOM;
      L_input_rec.ITEM_LOC_STATUS         := I_input(i).ITEM_LOC_STATUS;
      L_input_rec.TAXABLE_IND             := I_input(i).TAXABLE_IND;
      L_input_rec.TI                      := I_input(i).TI;
      L_input_rec.HI                      := I_input(i).HI;
      L_input_rec.STORE_ORD_MULT          := I_input(i).STORE_ORD_MULT;
      L_input_rec.MEAS_OF_EACH            := I_input(i).MEAS_OF_EACH;
      L_input_rec.MEAS_OF_PRICE           := I_input(i).MEAS_OF_PRICE;
      L_input_rec.UOM_OF_PRICE            := I_input(i).UOM_OF_PRICE;
      L_input_rec.PRIMARY_VARIANT         := I_input(i).PRIMARY_VARIANT;
      L_input_rec.PRIMARY_SUPP            := I_input(i).PRIMARY_SUPP;
      L_input_rec.PRIMARY_CNTRY           := I_input(i).PRIMARY_CNTRY;
      L_input_rec.LOCAL_ITEM_DESC         := I_input(i).LOCAL_ITEM_DESC;
      L_input_rec.LOCAL_SHORT_DESC        := I_input(i).LOCAL_SHORT_DESC;
      L_input_rec.PRIMARY_COST_PACK       := I_input(i).PRIMARY_COST_PACK;
      L_input_rec.RECEIVE_AS_TYPE         := I_input(i).RECEIVE_AS_TYPE;
      L_input_rec.STORE_PRICE_IND         := I_input(i).STORE_PRICE_IND;
      L_input_rec.UIN_TYPE                := I_input(i).UIN_TYPE;
      L_input_rec.UIN_LABEL               := I_input(i).UIN_LABEL;
      L_input_rec.CAPTURE_TIME            := I_input(i).CAPTURE_TIME;
      L_input_rec.EXT_UIN_IND             := I_input(i).EXT_UIN_IND;
      L_input_rec.SOURCE_METHOD           := I_input(i).SOURCE_METHOD;
      L_input_rec.SOURCE_WH               := I_input(i).SOURCE_WH;
      L_input_rec.INBOUND_HANDLING_DAYS   := I_input(i).INBOUND_HANDLING_DAYS;
      L_input_rec.CURRENCY_CODE           := I_input(i).CURRENCY_CODE;
      L_input_rec.LIKE_STORE              := I_input(i).LIKE_STORE;
      L_input_rec.DEFAULT_TO_CHILDREN_IND := I_input(i).DEFAULT_TO_CHILDREN_IND;
      L_input_rec.CLASS_VAT_IND           := I_input(i).CLASS_VAT_IND;
      L_input_rec.HIER_LEVEL              := I_input(i).HIER_LEVEL;
      L_input_rec.HIER_NUM_VALUE          := I_input(i).HIER_NUM_VALUE;
      L_input_rec.HIER_CHAR_VALUE         := I_input(i).HIER_CHAR_VALUE;
      L_input_rec.COSTING_LOC             := I_input(i).COSTING_LOC;
      L_input_rec.COSTING_LOC_TYPE        := I_input(i).COSTING_LOC_TYPE;
      L_input_rec.RANGED_IND              := I_input(i).RANGED_IND;
      L_input_rec.DEFAULT_WH              := I_input(i).DEFAULT_WH;
      L_input_rec.ITEM_LOC_IND            := I_input(i).ITEM_LOC_IND;
   
      L_input_tbl(i) := L_input_rec;
   end loop;

   if NEW_ITEM_LOC_SQL.NEW_ITEM_LOC (O_error_message,
                                     L_input_tbl,
                                     I_apply_sec_ind) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;

END NEW_ITEM_LOC_WRP;

---------------------------------------------------------------------------------

FUNCTION NEW_ITEM_LOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_input           IN       NIL_INPUT_TBL,
                      I_apply_sec_ind   IN       VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.NEW_ITEM_LOC';

   L_cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_nil_process_id          NIL_INPUT_WORKING.PROCESS_ID%TYPE;

   L_fc_item_loc_table    OBJ_ITEMLOC_TBL        := OBJ_ITEMLOC_TBL();
   L_comp_item_cost_tbl   OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();

   L_input_rec_exists  VARCHAR2(1)  := NULL;
   L_owner             VARCHAR2(50) := NULL;
   L_called_name       VARCHAR2(50) := NULL;
   L_line              NUMBER       := NULL;
   L_caller_type       VARCHAR2(50) := NULL;
   
   exp_wf_default_wh   EXCEPTION;
   CURSOR C_INPUT_REC IS
      SELECT 'x'
        FROM nil_input_working
       WHERE process_id = L_nil_process_id
         AND rownum = 1;

BEGIN
   --
   IF SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   ---
   SYS.OWA_UTIL.who_called_me(L_owner,
                              L_called_name,
                              L_line,
                              L_caller_type);
   ---
   IF L_called_name IS NULL OR L_called_name = 'NEW_ITEM_LOC' THEN
      L_called_name := GV_who_called_me;
   END IF;
   ---
   IF upper(L_called_name) IN ('CREATE_ORDER_SQL', 'ORDER_RCV_SQL', 'ORDER_SETUP_SQL', 'RMSSUB_XORDER_VALIDATE', 'RMSSUB_DSRCPT_SQL', 'CNTRORDB', 'EDIUPACK') THEN
      -- If new item loc relationship is being created from any order package(PO or DSD or externally generated order),
      -- then source method = 'S'upplier (which provides inventory) AND Source_wh will be NULL.
      LP_called_from_PO := 'Y';
   END IF;
   ---
   LP_from_itemloc_online := I_apply_sec_ind;
   ---
   SELECT NIL_PROCESS_ID_SEQ.nextval
     INTO L_nil_process_id
     FROM dual;
   ---
   
   IF POPULATE_WORKING_TABLE(O_error_message,
                             I_input,
                             L_nil_process_id,
                             I_apply_sec_ind) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF EXPLODE_LOCS(O_error_message,
                   L_nil_process_id,
                   I_apply_sec_ind) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF GET_SOURCE_METHOD(O_error_message,
                        L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF CHECK_SOURCE_WH(O_error_message,L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF EXPLODE_ITEMS(O_error_message,
                    L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   -- Check any records left to process
   OPEN C_INPUT_REC;
   FETCH C_INPUT_REC INTO L_input_rec_exists;
   CLOSE C_INPUT_REC;
   ---
   IF L_input_rec_exists IS NULL THEN
      RETURN TRUE;
   END IF;
   ---
   IF LP_system_options.default_tax_type = 'GTAX' THEN
      IF CHECK_ITEM_COUNTRY(O_error_message,
                            L_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
   END IF;
   ---
   IF GET_SUPPLIER(O_error_message,
                   L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF CHECK_SUPPLIER(O_error_message,
                     L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF GET_AV_WEIGHT(O_error_message,
                    L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF COMPLETE_ITEM_LOC_REC(O_error_message,
                            L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF COMPUTE_ITEM_COST(O_error_message,
                        L_comp_item_cost_tbl,
                        L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF CALC_RETAIL_TAX(O_error_message,
                      L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF CONVERT_CURRENCIES(O_error_message,
                         L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   IF PERSIST(O_error_message,
              L_comp_item_cost_tbl,
              L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   SELECT OBJ_ITEMLOC_REC( item
                          ,loc)
     BULK COLLECT INTO L_fc_item_loc_table
     FROM nil_input_working
    WHERE process_id   = L_nil_process_id
      AND item_level   = tran_level
      AND item_status  = 'A'
      AND loc_type    != 'E';
   ---
   IF L_fc_item_loc_table IS NOT NULL AND L_fc_item_loc_table.COUNT > 0 THEN
      ---
      IF FUTURE_COST_EVENT_SQL.ADD_NIL(O_error_message,
                                       L_cost_event_process_id,
                                       L_fc_item_loc_table,
                                       USER) = FALSE THEN
         RETURN FALSE;
      END IF;
   END IF;
   ---
   IF CLEAR_WORKING_TABLE(O_error_message,
                          L_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---

   RETURN TRUE;

EXCEPTION
   WHEN exp_wf_default_wh THEN
      RAISE_APPLICATION_ERROR(-20001,'Error: wf_default_wh is Null in system_options');
      ---
   WHEN OTHERS THEN
      ---
      IF C_INPUT_REC%ISOPEN THEN
         CLOSE C_INPUT_REC;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,L_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END NEW_ITEM_LOC;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--   PRIVATE  FUNCTIONS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION POPULATE_WORKING_TABLE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_input            IN       NIL_INPUT_TBL,
                                I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE,
                                I_apply_sec_ind    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'NEW_ITEM_LOC_SQL.POPULATE_WORKING_TABLE';

BEGIN

   -- Insert input collection into working table.
   FORALL I_indx IN I_input.FIRST .. I_input.LAST
      INSERT INTO nil_input_working (process_id
                                    ,item
                                    ,item_parent
                                    ,item_grandparent
                                    ,item_short_desc
                                    ,dept
                                    ,class
                                    ,subclass
                                    ,item_level
                                    ,tran_level
                                    ,item_status
                                    ,waste_type
                                    ,sellable_ind
                                    ,orderable_ind
                                    ,pack_ind
                                    ,pack_type
                                    ,item_desc
                                    ,diff_1
                                    ,diff_2
                                    ,diff_3
                                    ,diff_4
                                    ,loc
                                    ,loc_type
                                    ,daily_waste_pct
                                    ,unit_cost_loc
                                    ,unit_retail_loc
                                    ,selling_unit_retail
                                    ,selling_uom
                                    ,multi_units
                                    ,multi_unit_retail
                                    ,multi_selling_uom
                                    ,item_loc_status
                                    ,taxable_ind
                                    ,ti
                                    ,hi
                                    ,store_ord_mult
                                    ,meas_of_each
                                    ,meas_of_price
                                    ,uom_of_price
                                    ,primary_variant
                                    ,primary_supp
                                    ,primary_cntry
                                    ,local_item_desc
                                    ,local_short_desc
                                    ,primary_cost_pack
                                    ,receive_as_type
                                    ,store_price_ind
                                    ,uin_type
                                    ,uin_label
                                    ,capture_time
                                    ,ext_uin_ind
                                    ,source_method
                                    ,source_wh
                                    ,inbound_handling_days
                                    ,currency_code
                                    ,like_store
                                    ,default_to_children_ind
                                    ,class_vat_ind
                                    ,hier_level
                                    ,hier_num_value
                                    ,hier_char_value
                                    ,costing_loc
                                    ,costing_loc_type
                                    ,ranged_ind
                                    ,default_wh
                                    ,item_loc_ind)
                             VALUES (I_nil_process_id
                                    ,I_input(I_indx).item
                                    ,I_input(I_indx).item_parent
                                    ,I_input(I_indx).item_grandparent
                                    ,I_input(I_indx).item_short_desc
                                    ,I_input(I_indx).dept
                                    ,I_input(I_indx).item_class
                                    ,I_input(I_indx).subclass
                                    ,I_input(I_indx).item_level
                                    ,I_input(I_indx).tran_level
                                    ,I_input(I_indx).item_status
                                    ,I_input(I_indx).waste_type
                                    ,I_input(I_indx).sellable_ind
                                    ,I_input(I_indx).orderable_ind
                                    ,I_input(I_indx).pack_ind
                                    ,I_input(I_indx).pack_type
                                    ,I_input(I_indx).item_desc
                                    ,I_input(I_indx).diff_1
                                    ,I_input(I_indx).diff_2
                                    ,I_input(I_indx).diff_3
                                    ,I_input(I_indx).diff_4
                                    ,I_input(I_indx).loc
                                    ,I_input(I_indx).loc_type
                                    ,I_input(I_indx).daily_waste_pct
                                    ,I_input(I_indx).unit_cost_loc
                                    ,I_input(I_indx).unit_retail_loc
                                    ,I_input(I_indx).selling_retail_loc
                                    ,I_input(I_indx).selling_uom
                                    ,I_input(I_indx).multi_units
                                    ,I_input(I_indx).multi_unit_retail
                                    ,I_input(I_indx).multi_selling_uom
                                    ,I_input(I_indx).item_loc_status
                                    ,I_input(I_indx).taxable_ind
                                    ,I_input(I_indx).ti
                                    ,I_input(I_indx).hi
                                    ,I_input(I_indx).store_ord_mult
                                    ,I_input(I_indx).meas_of_each
                                    ,I_input(I_indx).meas_of_price
                                    ,I_input(I_indx).uom_of_price
                                    ,I_input(I_indx).primary_variant
                                    ,I_input(I_indx).primary_supp
                                    ,I_input(I_indx).primary_cntry
                                    ,I_input(I_indx).local_item_desc
                                    ,I_input(I_indx).local_short_desc
                                    ,I_input(I_indx).primary_cost_pack
                                    ,I_input(I_indx).receive_as_type
                                    ,I_input(I_indx).store_price_ind
                                    ,I_input(I_indx).uin_type
                                    ,I_input(I_indx).uin_label
                                    ,I_input(I_indx).capture_time
                                    ,I_input(I_indx).ext_uin_ind
                                    ,I_input(I_indx).source_method
                                    ,I_input(I_indx).source_wh
                                    ,I_input(I_indx).inbound_handling_days
                                    ,I_input(I_indx).currency_code
                                    ,I_input(I_indx).like_store
                                    ,I_input(I_indx).default_to_children_ind
                                    ,I_input(I_indx).class_vat_ind
                                    ,I_input(I_indx).hier_level
                                    ,I_input(I_indx).hier_num_value
                                    ,I_input(I_indx).hier_char_value
                                    ,I_input(I_indx).costing_loc
                                    ,I_input(I_indx).costing_loc_type
                                    ,I_input(I_indx).ranged_ind
                                    ,I_input(I_indx).default_wh
                                    ,I_input(I_indx).item_loc_ind);

   -- Retrieve item properties from ITEM_MASTER and update the working table.
   MERGE INTO nil_input_working niw
      USING (
             SELECT niw_in.process_id
                   ,niw_in.item
                   ,niw_in.loc
                   ,niw_in.loc_type
                   ,niw_in.hier_num_value
                   ,niw_in.hier_char_value
                   ,NVL(niw_in.item_parent,      im.item_parent)      item_parent
                   ,NVL(niw_in.item_grandparent, im.item_grandparent) item_grandparent
                   ,NVL(niw_in.item_desc,        im.item_desc)        item_desc
                   ,NVL(niw_in.item_short_desc,  im.short_desc)       item_short_desc
                   ,NVL(niw_in.dept,             im.dept)             dept
                   ,NVL(niw_in.class,            im.class)            class
                   ,NVL(niw_in.subclass,         im.subclass)         subclass
                   ,NVL(niw_in.item_level,       im.item_level)       item_level
                   ,NVL(niw_in.tran_level,       im.tran_level)       tran_level
                   ,NVL(niw_in.item_status,      im.status)           item_status
                   ,NVL(niw_in.waste_type,       im.waste_type)       waste_type
                   ,NVL(niw_in.sellable_ind,     im.sellable_ind)     sellable_ind
                   ,NVL(niw_in.orderable_ind,    im.orderable_ind)    orderable_ind
                   ,NVL(niw_in.pack_ind,         im.pack_ind)         pack_ind
                   ,NVL(niw_in.pack_type,        im.pack_type)        pack_type
                   ,NVL(niw_in.diff_1,           im.diff_1)           diff_1
                   ,NVL(niw_in.diff_2,           im.diff_2)           diff_2
                   ,NVL(niw_in.diff_3,           im.diff_3)           diff_3
                   ,NVL(niw_in.diff_4,           im.diff_4)           diff_4
                   ,NVL(niw_in.store_ord_mult,   im.store_ord_mult)   store_ord_mult
                   ,CASE
                       WHEN     im.pack_ind             = 'Y'
                            AND niw_in.loc_type        != 'S'
                            AND niw_in.receive_as_type IS NULL
                            AND im.pack_type            = 'V'
                       THEN 'P'
                       ELSE im.order_as_type
                    END                                               order_as_type
                   ,im.item_number_type
                   ,im.format_id
                   ,im.prefix
                   ,im.waste_pct
                   ,im.default_waste_pct
                   ,im.simple_pack_ind
                   ,im.contains_inner_ind
                   ,im.catch_weight_ind
                   ,im.catch_weight_uom
                   ,im.sale_type
                   ,im.container_item
                   ,im.deposit_item_type
                   ,im.item_xform_ind
                   ,im.standard_uom
                   ,im.uom_conv_factor
                   ,ROW_NUMBER() OVER (PARTITION BY niw_in.process_id
                                                   ,niw_in.item
                                                   ,niw_in.loc
                                                   ,niw_in.loc_type
                                                   ,niw_in.hier_num_value
                                                   ,niw_in.hier_char_value
                                           ORDER BY niw_in.process_id
                                                   ,niw_in.item
                                                   ,niw_in.loc
                                                   ,niw_in.loc_type
                                                   ,niw_in.hier_num_value
                                                   ,niw_in.hier_char_value) AS itemloc_rank
               FROM nil_input_working niw_in
                   ,item_master       im
              WHERE niw_in.process_id = I_nil_process_id
                AND im.item           = niw_in.item
             ) input
         ON (    niw.process_id                   = I_nil_process_id
             AND niw.item                         = input.item
             AND NVL(niw.loc, -999)               = NVL(input.loc, -999)
             AND NVL(niw.loc_type, -999)          = NVL(input.loc_type, -999)
             AND NVL(niw.hier_num_value, -999)    = NVL(input.hier_num_value, -999)
             AND NVL(niw.hier_char_value, '-999') = NVL(input.hier_char_value, '-999')
             AND input.itemloc_rank               = 1)
      WHEN MATCHED THEN
         UPDATE
            SET niw.item_parent        = input.item_parent
               ,niw.item_grandparent   = input.item_grandparent
               ,niw.item_desc          = input.item_desc
               ,niw.item_short_desc    = input.item_short_desc
               ,niw.item_number_type   = input.item_number_type
               ,niw.format_id          = input.format_id
               ,niw.prefix             = input.prefix
               ,niw.dept               = input.dept
               ,niw.class              = input.class
               ,niw.subclass           = input.subclass
               ,niw.item_level         = input.item_level
               ,niw.tran_level         = input.tran_level
               ,niw.item_status        = input.item_status
               ,niw.waste_type         = input.waste_type
               ,niw.waste_pct          = input.waste_pct
               ,niw.default_waste_pct  = input.default_waste_pct
               ,niw.sellable_ind       = input.sellable_ind
               ,niw.orderable_ind      = input.orderable_ind
               ,niw.pack_ind           = input.pack_ind
               ,niw.pack_type          = input.pack_type
               ,niw.simple_pack_ind    = input.simple_pack_ind
               ,niw.diff_1             = input.diff_1
               ,niw.diff_2             = input.diff_2
               ,niw.diff_3             = input.diff_3
               ,niw.diff_4             = input.diff_4
               ,niw.order_as_type      = input.order_as_type
               ,niw.contains_inner_ind = input.contains_inner_ind
               ,niw.store_ord_mult     = input.store_ord_mult
               ,niw.catch_weight_ind   = input.catch_weight_ind
               ,niw.catch_weight_uom   = input.catch_weight_uom
               ,niw.sale_type          = input.sale_type
               ,niw.container_item     = input.container_item
               ,niw.deposit_item_type  = input.deposit_item_type
               ,niw.item_xform_ind     = input.item_xform_ind
               ,niw.standard_uom       = input.standard_uom
               ,niw.uom_conv_factor    = input.uom_conv_factor;
   ---
   IF VALIDATE(O_error_message,
               I_nil_process_id,
               I_apply_sec_ind) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END POPULATE_WORKING_TABLE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE,
                  I_apply_sec_ind    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(30) := 'NEW_ITEM_LOC_SQL.VALIDATE';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

   CURSOR C_CHECK_INPUT IS
      SELECT CASE
                WHEN item IS NULL
                THEN SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'item',
                                        L_program,
                                        NULL)
                WHEN loc IS NULL AND loc_type IS NULL AND hier_level IS NULL
                THEN SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'loc_type OR hier_level',
                                        L_program,
                                        NULL)
                WHEN loc_type IS NULL AND hier_level NOT IN ('A', 'AE', 'AI', 'AL', 'AR', 'AS', 'AW', 'C', 'CH', 'D', 'DI', 'DW', 'L', 'LLS', 'LLW', 'PW', 'R', 'RE', 'T')
                THEN SQL_LIB.CREATE_MSG('INV_ORG_LEVEL',
                                        L_program,
                                        'Item :' || item || ' Hier Level: ' || hier_level,
                                        NULL)
                WHEN loc IS NULL AND hier_level NOT IN ('C', 'AL', 'AS', 'AW', 'AI', 'AE') AND hier_num_value IS NULL
                THEN SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'hier_num_value',
                                        L_program,
                                        NULL)
                WHEN loc IS NULL AND hier_level = 'C' AND hier_char_value IS NULL
                THEN SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                        'hier_char_value',
                                        L_program,
                                        NULL)
                WHEN item_level > tran_level
                THEN SQL_LIB.CREATE_MSG('ITEM_TRAN_LVL_OR_ABOVE',
                                        'Program :' || L_program,
                                        'Item :' || item,
                                        'Item Level :' || item_level || ' Tran Level :' || tran_level)
                WHEN hier_level IN ('DW', 'W', 'PW', 'I') AND inbound_handling_days IS NULL
                THEN SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                        L_program,
                                        'inbound_handling_days',
                                        'NULL')
                WHEN hier_level NOT IN ('DW', 'W', 'PW', 'I') AND inbound_handling_days IS NOT NULL
                THEN SQL_LIB.CREATE_MSG('INV_PARM_PROG',
                                        L_program,
                                        'inbound_handling_days',
                                        'Note NULL')
                ELSE NULL
             END err_msg
        FROM nil_input_working input
       WHERE process_id = I_nil_process_id
       ORDER BY err_msg;

BEGIN

   OPEN  C_CHECK_INPUT;
   FETCH C_CHECK_INPUT INTO L_error_message;
   CLOSE C_CHECK_INPUT;

   IF L_error_message IS NOT NULL THEN
      O_error_message := L_error_message;
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
   END IF;
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      IF C_CHECK_INPUT%ISOPEN THEN
         CLOSE C_CHECK_INPUT;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END VALIDATE;
--------------------------------------------------------------------------------
FUNCTION EXPLODE_LOCS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE,
                      I_apply_sec_ind    IN       VARCHAR2)
  RETURN BOOLEAN IS

   L_program   VARCHAR2(40) := 'NEW_ITEM_LOC_SQL.EXPLODE_LOCS';

   L_hier_num_value    NIL_INPUT_WORKING.HIER_NUM_VALUE%TYPE  := NULL;
   L_hier_char_value   NIL_INPUT_WORKING.HIER_CHAR_VALUE%TYPE := NULL;

   -- Check if there are any hier which did not explode to any location
   CURSOR C_GET_INV_HIER IS
      SELECT hier_num_value
            ,hier_char_value
        FROM nil_input_working nil1
       WHERE process_id  = I_nil_process_id
         AND loc        IS NULL
         AND loc_type   IS NULL
         AND NOT EXISTS (
                         SELECT 'x'
                           FROM nil_input_working nil2
                          WHERE nil2.process_id                    = I_nil_process_id
                            AND nil2.loc                          IS NOT NULL
                            AND nil2.loc_type                     IS NOT NULL
                            AND nil2.item                          = nil1.item
                            AND nil2.hier_level                    = nil1.hier_level
                            AND NVL(nil2.hier_num_value,  -999)    = NVL(nil1.hier_num_value,  -999)
                            AND NVL(nil2.hier_char_value, '-999')  = NVL(nil1.hier_char_value, '-999')
                         );

BEGIN
   -- Retrieve Location info for hierarchy defined at the location level
   MERGE INTO nil_input_working nil
      USING (
             -- For Stores
             SELECT niw.item
                   ,s.store                                                   loc
                   ,'S'                                                       loc_type
                   ,s.org_unit_id
                   ,s.currency_code
                   ,s.lang
                   ,s.store_type
                   ,niw.inbound_handling_days
                   ,NVL(s.default_wh,        LP_system_options.wf_default_wh) default_wh
               FROM nil_input_working niw
                   ,store             s
              WHERE niw.process_id         = I_nil_process_id
                AND niw.loc                = s.store
                AND NVL(niw.loc_type, 'S') = 'S'
             UNION ALL
             -- For WH and Internal Finishers
             SELECT niw.item
                   ,w.wh                                                      loc
                   ,'W'                                                       loc_type
                   ,w.org_unit_id
                   ,w.currency_code
                   ,NULL                                                      lang
                   ,NULL                                                      store_type
                   ,w.inbound_handling_days
                   ,NULL                                                      default_wh
               FROM nil_input_working niw
                   ,wh w
              WHERE niw.process_id          = I_nil_process_id
                AND niw.loc                 = w.wh
                AND NVL(niw.loc_type, 'W')  IN ('W', 'I')
             UNION ALL
             -- For External Finishers
             SELECT niw.item
                   ,TO_NUMBER(ef.partner_id)                                  loc
                   ,'E'                                                       loc_type
                   ,ef.org_unit_id
                   ,ef.currency_code
                   ,NULL                                                      lang
                   ,NULL                                                      store_type
                   ,NULL                                                      inbound_handling_days
                   ,NULL                                                      default_wh
               FROM nil_input_working niw,
                    partner ef
              WHERE niw.process_id         = I_nil_process_id
                AND ef.partner_type        = 'E'
                AND niw.loc                = TO_NUMBER(ef.partner_id)
                AND NVL(niw.loc_type, 'E') = 'E'
             ) input
         ON (    nil.process_id = I_nil_process_id
             AND nil.item       = input.item
             AND nil.loc        = input.loc)
       WHEN MATCHED THEN
          UPDATE
             SET nil.loc_type              = input.loc_type
                ,nil.store_type            = input.store_type
                ,nil.org_unit_id           = input.org_unit_id
                ,nil.currency_code         = input.currency_code
                ,nil.lang                  = input.lang
                ,nil.default_wh            = input.default_wh
                ,nil.inbound_handling_days = input.inbound_handling_days
                ,nil.costing_loc           = CASE input.store_type
                                                WHEN 'F'
                                                THEN NVL(nil.costing_loc, input.default_wh)
                                                ELSE NULL
                                             END
                ,nil.costing_loc_type      = CASE input.store_type
                                                WHEN 'F'
                                                THEN NVL(nil.costing_loc_type, 'W')
                                                ELSE NULL
                                             END;

   -- Explode the hier level and retrieve the location info
   INSERT INTO nil_input_working (process_id
                                 ,item
                                 ,item_parent
                                 ,item_grandparent
                                 ,item_desc
                                 ,item_short_desc
                                 ,item_number_type
                                 ,format_id
                                 ,prefix
                                 ,dept
                                 ,class
                                 ,subclass
                                 ,item_level
                                 ,tran_level
                                 ,item_status
                                 ,waste_type
                                 ,waste_pct
                                 ,default_waste_pct
                                 ,sellable_ind
                                 ,orderable_ind
                                 ,pack_ind
                                 ,pack_type
                                 ,simple_pack_ind
                                 ,diff_1
                                 ,diff_2
                                 ,diff_3
                                 ,diff_4
                                 ,order_as_type
                                 ,contains_inner_ind
                                 ,store_ord_mult
                                 ,catch_weight_ind
                                 ,catch_weight_uom
                                 ,sale_type
                                 ,container_item
                                 ,item_xform_ind
                                 ,standard_uom
                                 ,uom_conv_factor
                                 ,loc
                                 ,loc_type
                                 ,daily_waste_pct
                                 ,unit_cost_loc
                                 ,unit_retail_loc
                                 ,selling_unit_retail
                                 ,selling_uom
                                 ,multi_units
                                 ,multi_unit_retail
                                 ,multi_selling_uom
                                 ,item_loc_status
                                 ,taxable_ind
                                 ,ti
                                 ,hi
                                 ,meas_of_each
                                 ,meas_of_price
                                 ,uom_of_price
                                 ,primary_variant
                                 ,primary_supp
                                 ,primary_cntry
                                 ,primary_cost_pack
                                 ,local_item_desc
                                 ,local_short_desc
                                 ,receive_as_type
                                 ,store_price_ind
                                 ,uin_type
                                 ,uin_label
                                 ,ext_uin_ind
                                 ,capture_time
                                 ,source_method
                                 ,source_wh
                                 ,inbound_handling_days
                                 ,currency_code
                                 ,like_store
                                 ,default_to_children_ind
                                 ,class_vat_ind
                                 ,hier_level
                                 ,hier_num_value
                                 ,hier_char_value
                                 ,org_unit_id
                                 ,lang
                                 ,default_wh
                                 ,store_type
                                 ,ranged_ind
                                 ,costing_loc
                                 ,costing_loc_type)
                            -- First select for Stores
                           SELECT input.process_id
                                 ,input.item
                                 ,input.item_parent
                                 ,input.item_grandparent
                                 ,input.item_desc
                                 ,input.item_short_desc
                                 ,input.item_number_type
                                 ,input.format_id
                                 ,input.prefix
                                 ,input.dept
                                 ,input.class
                                 ,input.subclass
                                 ,input.item_level
                                 ,input.tran_level
                                 ,input.item_status
                                 ,input.waste_type
                                 ,input.waste_pct
                                 ,input.default_waste_pct
                                 ,input.sellable_ind
                                 ,input.orderable_ind
                                 ,input.pack_ind
                                 ,input.pack_type
                                 ,input.simple_pack_ind
                                 ,input.diff_1
                                 ,input.diff_2
                                 ,input.diff_3
                                 ,input.diff_4
                                 ,input.order_as_type
                                 ,input.contains_inner_ind
                                 ,input.store_ord_mult
                                 ,input.catch_weight_ind
                                 ,input.catch_weight_uom
                                 ,input.sale_type
                                 ,input.container_item
                                 ,input.item_xform_ind
                                 ,input.standard_uom
                                 ,input.uom_conv_factor
                                 ,st.store                                             loc
                                 ,'S'                                                  loc_type
                                 ,input.daily_waste_pct
                                 ,input.unit_cost_loc
                                 ,input.unit_retail_loc
                                 ,input.selling_unit_retail
                                 ,input.selling_uom
                                 ,input.multi_units
                                 ,input.multi_unit_retail
                                 ,input.multi_selling_uom
                                 ,input.item_loc_status
                                 ,input.taxable_ind
                                 ,input.ti
                                 ,input.hi
                                 ,input.meas_of_each
                                 ,input.meas_of_price
                                 ,input.uom_of_price
                                 ,input.primary_variant
                                 ,input.primary_supp
                                 ,input.primary_cntry
                                 ,input.primary_cost_pack
                                 ,input.local_item_desc
                                 ,input.local_short_desc
                                 ,input.receive_as_type
                                 ,input.store_price_ind
                                 ,input.uin_type
                                 ,input.uin_label
                                 ,input.ext_uin_ind
                                 ,input.capture_time
                                 ,input.source_method
                                 ,input.source_wh
                                 ,input.inbound_handling_days
                                 ,st.currency_code
                                 ,input.like_store
                                 ,input.default_to_children_ind
                                 ,input.class_vat_ind
                                 ,input.hier_level
                                 ,input.hier_num_value
                                 ,input.hier_char_value
                                 ,st.org_unit_id
                                 ,st.lang
                                 ,NVL(st.default_wh, LP_system_options.wf_default_wh) default_wh
                                 ,st.store_type
                                 ,input.ranged_ind
                                 ,CASE st.store_type
                                     WHEN 'F'
                                     THEN NVL(input.costing_loc, st.default_wh)
                                     ELSE NULL
                                  END                                                  costing_loc
                                 ,CASE st.store_type
                                     WHEN 'F'
                                     THEN NVL(input.costing_loc_type, 'W')
                                     ELSE NULL
                                  END
                             FROM (
                                   SELECT st.store
                                         ,v.chain
                                         ,v.area
                                         ,v.region
                                         ,v.district
                                         ,st.currency_code
                                         ,st.org_unit_id
                                         ,st.store_class
                                         ,st.transfer_zone
                                         ,st.lang
                                         ,st.default_wh
                                         ,st.store_type
                                     FROM v_store v
                                         ,store   st
                                    WHERE I_apply_sec_ind = 'Y'
                                      AND v.store         = st.store
                                   UNION ALL
                                   SELECT st.store
                                         ,sh.chain
                                         ,sh.area
                                         ,sh.region
                                         ,sh.district
                                         ,st.currency_code
                                         ,st.org_unit_id
                                         ,st.store_class
                                         ,st.transfer_zone
                                         ,st.lang
                                         ,st.default_wh
                                         ,st.store_type
                                     FROM store_hierarchy sh
                                         ,store           st
                                    WHERE I_apply_sec_ind = 'N'
                                      AND sh.store        = st.store
                                   )                st
                                 ,nil_input_working input
                            WHERE input.process_id               = I_nil_process_id
                              AND st.currency_code               = NVL(input.currency_code, st.currency_code)
                              AND NVL(input.hier_num_value,-999) = CASE
                                                                      WHEN input.hier_level IN ('AL', 'AS')
                                                                      THEN -999
                                                                      WHEN input.hier_level = 'CH'
                                                                      THEN st.chain
                                                                      WHEN input.hier_level IN ('A', 'AR')
                                                                      THEN st.area
                                                                      WHEN input.hier_level IN ('R', 'RE')
                                                                      THEN st.region
                                                                      WHEN input.hier_level IN ('D', 'DI')
                                                                      THEN st.district
                                                                      WHEN input.hier_level = 'T'
                                                                      THEN st.transfer_zone
                                                                      WHEN input.hier_level = 'C' AND input.hier_char_value = st.store_class
                                                                      THEN -999
                                                                      WHEN input.hier_level = 'DW'
                                                                      THEN st.default_wh
                                                                      WHEN input.hier_level = 'L'
                                                                      THEN (
                                                                            SELECT l.loc_trait
                                                                              FROM loc_traits_matrix l
                                                                             WHERE l.store     = st.store
                                                                               AND l.loc_trait = input.hier_num_value
                                                                            )
                                                                      WHEN input.hier_level = 'LLS'
                                                                      THEN (
                                                                            SELECT l.loc_list
                                                                              FROM loc_list_detail l
                                                                             WHERE l.location = st.store
                                                                               AND l.loc_list = input.hier_num_value
                                                                               AND l.loc_type = 'S'
                                                                            )
                                                                      ELSE 0
                                                                   END
                           UNION ALL
                           -- Second select for Warehouse
                           SELECT input.process_id
                                 ,input.item
                                 ,input.item_parent
                                 ,input.item_grandparent
                                 ,input.item_desc
                                 ,input.item_short_desc
                                 ,input.item_number_type
                                 ,input.format_id
                                 ,input.prefix
                                 ,input.dept
                                 ,input.class
                                 ,input.subclass
                                 ,input.item_level
                                 ,input.tran_level
                                 ,input.item_status
                                 ,input.waste_type
                                 ,input.waste_pct
                                 ,input.default_waste_pct
                                 ,input.sellable_ind
                                 ,input.orderable_ind
                                 ,input.pack_ind
                                 ,input.pack_type
                                 ,input.simple_pack_ind
                                 ,input.diff_1
                                 ,input.diff_2
                                 ,input.diff_3
                                 ,input.diff_4
                                 ,CASE
                                     WHEN input.pack_ind = 'Y' AND input.receive_as_type IS NULL AND input.pack_type = 'V'
                                     THEN 'P'
                                     ELSE input.order_as_type
                                  END                              order_as_type
                                 ,input.contains_inner_ind
                                 ,input.store_ord_mult
                                 ,input.catch_weight_ind
                                 ,input.catch_weight_uom
                                 ,input.sale_type
                                 ,input.container_item
                                 ,input.item_xform_ind
                                 ,input.standard_uom
                                 ,input.uom_conv_factor
                                 ,wh.wh                            loc
                                 ,'W'                              loc_type
                                 ,input.daily_waste_pct
                                 ,input.unit_cost_loc
                                 ,input.unit_retail_loc
                                 ,input.selling_unit_retail
                                 ,input.selling_uom
                                 ,input.multi_units
                                 ,input.multi_unit_retail
                                 ,input.multi_selling_uom
                                 ,input.item_loc_status
                                 ,input.taxable_ind
                                 ,input.ti
                                 ,input.hi
                                 ,input.meas_of_each
                                 ,input.meas_of_price
                                 ,input.uom_of_price
                                 ,input.primary_variant
                                 ,input.primary_supp
                                 ,input.primary_cntry
                                 ,input.primary_cost_pack
                                 ,input.local_item_desc
                                 ,input.local_short_desc
                                 ,input.receive_as_type
                                 ,input.store_price_ind
                                 ,input.uin_type
                                 ,input.uin_label
                                 ,input.ext_uin_ind
                                 ,input.capture_time
                                 ,input.source_method
                                 ,input.source_wh
                                 ,wh.inbound_handling_days
                                 ,wh.currency_code
                                 ,input.like_store
                                 ,input.default_to_children_ind
                                 ,input.class_vat_ind
                                 ,input.hier_level
                                 ,input.hier_num_value
                                 ,input.hier_char_value
                                 ,wh.org_unit_id
                                 ,NULL                             lang
                                 ,NULL                             default_wh
                                 ,NULL                             store_type
                                 ,input.ranged_ind
                                 ,NULL                             costing_loc
                                 ,NULL                             costing_loc_type
                             FROM (
                                   SELECT vw.wh
                                         ,vw.physical_wh
                                         ,wh.inbound_handling_days
                                         ,vw.currency_code
                                         ,vw.org_unit_id
                                         ,vw.stockholding_ind
                                     FROM v_wh vw
                                         ,wh
                                    WHERE I_apply_sec_ind = 'Y'
                                      AND wh.wh           = vw.wh
                                   UNION ALL
                                   SELECT wh
                                         ,physical_wh
                                         ,inbound_handling_days
                                         ,currency_code
                                         ,org_unit_id
                                         ,stockholding_ind
                                     FROM wh
                                    WHERE I_apply_sec_ind = 'N'
                                   )                wh
                                 ,nil_input_working input
                            WHERE input.process_id               = I_nil_process_id
                              AND wh.currency_code               = NVL(input.currency_code, wh.currency_code)
                              AND wh.stockholding_ind            = 'Y'
                              AND NVL(input.hier_num_value,-999) = CASE
                                                                      WHEN input.hier_level IN ('AL', 'AW')
                                                                      THEN -999
                                                                      WHEN input.hier_level = 'PW'
                                                                      THEN wh.physical_wh
                                                                      WHEN input.hier_level = 'LLW'
                                                                      THEN (
                                                                            SELECT l.loc_list
                                                                              FROM loc_list_detail l
                                                                             WHERE (   l.location     = wh.wh
                                                                                    OR wh.physical_wh  = l.location)
                                                                               AND l.loc_list         = input.hier_num_value
                                                                               AND l.loc_type         = 'W'
                                                                            )
                                                                      ELSE 0
                                                                   END
                              AND NOT EXISTS (
                                              SELECT wh
                                                FROM wh_add
                                               WHERE wh = wh.wh
                                              )
                           UNION ALL
                           -- Select for Internal Finishers
                           SELECT input.process_id
                                 ,input.item
                                 ,input.item_parent
                                 ,input.item_grandparent
                                 ,input.item_desc
                                 ,input.item_short_desc
                                 ,input.item_number_type
                                 ,input.format_id
                                 ,input.prefix
                                 ,input.dept
                                 ,input.class
                                 ,input.subclass
                                 ,input.item_level
                                 ,input.tran_level
                                 ,input.item_status
                                 ,input.waste_type
                                 ,input.waste_pct
                                 ,input.default_waste_pct
                                 ,input.sellable_ind
                                 ,input.orderable_ind
                                 ,input.pack_ind
                                 ,input.pack_type
                                 ,input.simple_pack_ind
                                 ,input.diff_1
                                 ,input.diff_2
                                 ,input.diff_3
                                 ,input.diff_4
                                 ,CASE
                                     WHEN input.pack_ind = 'Y' AND input.receive_as_type IS NULL AND input.pack_type = 'V'
                                     THEN 'P'
                                     ELSE input.order_as_type
                                  END                              order_as_type
                                 ,input.contains_inner_ind
                                 ,input.store_ord_mult
                                 ,input.catch_weight_ind
                                 ,input.catch_weight_uom
                                 ,input.sale_type
                                 ,input.container_item
                                 ,input.item_xform_ind
                                 ,input.standard_uom
                                 ,input.uom_conv_factor
                                 ,wh.wh                            loc
                                 ,'W'                              loc_type
                                 ,input.daily_waste_pct
                                 ,input.unit_cost_loc
                                 ,input.unit_retail_loc
                                 ,input.selling_unit_retail
                                 ,input.selling_uom
                                 ,input.multi_units
                                 ,input.multi_unit_retail
                                 ,input.multi_selling_uom
                                 ,input.item_loc_status
                                 ,input.taxable_ind
                                 ,input.ti
                                 ,input.hi
                                 ,input.meas_of_each
                                 ,input.meas_of_price
                                 ,input.uom_of_price
                                 ,input.primary_variant
                                 ,input.primary_supp
                                 ,input.primary_cntry
                                 ,input.primary_cost_pack
                                 ,input.local_item_desc
                                 ,input.local_short_desc
                                 ,input.receive_as_type
                                 ,input.store_price_ind
                                 ,input.uin_type
                                 ,input.uin_label
                                 ,input.ext_uin_ind
                                 ,input.capture_time
                                 ,input.source_method
                                 ,input.source_wh
                                 ,wh.inbound_handling_days
                                 ,wh.currency_code
                                 ,input.like_store
                                 ,input.default_to_children_ind
                                 ,input.class_vat_ind
                                 ,input.hier_level
                                 ,input.hier_num_value
                                 ,input.hier_char_value
                                 ,wh.org_unit_id
                                 ,NULL                             lang
                                 ,NULL                             default_wh
                                 ,NULL                             store_type
                                 ,input.ranged_ind
                                 ,NULL                             costing_loc
                                 ,NULL                             costing_loc_type
                             FROM (
                                   SELECT vif.finisher_id wh
                                         ,vif.physical_wh
                                         ,wh.inbound_handling_days
                                         ,vif.currency_code
                                         ,vif.org_unit_id
                                     FROM v_internal_finisher vif
                                         ,wh
                                    WHERE I_apply_sec_ind     = 'Y'
                                      AND wh.wh               = vif.finisher_id
                                      AND wh.finisher_ind     = 'Y'
                                      and wh.stockholding_ind = 'Y'
                                   UNION ALL
                                   SELECT wh
                                         ,physical_wh
                                         ,inbound_handling_days
                                         ,currency_code
                                         ,org_unit_id
                                     FROM wh
                                    WHERE I_apply_sec_ind     = 'N'
                                      AND finisher_ind        = 'Y'
                                      and stockholding_ind    = 'Y'
                                   )                wh
                                 ,nil_input_working input
                            WHERE input.process_id        = I_nil_process_id
                              AND wh.currency_code        = NVL(input.currency_code, wh.currency_code)
                              AND input.hier_level        = 'AI'
                              AND NOT EXISTS (
                                              SELECT wh
                                                FROM wh_add
                                               WHERE wh = wh.wh
                                              )
                           UNION ALL
                           -- Last select is for External Finishers
                           SELECT input.process_id
                                 ,input.item
                                 ,input.item_parent
                                 ,input.item_grandparent
                                 ,input.item_desc
                                 ,input.item_short_desc
                                 ,input.item_number_type
                                 ,input.format_id
                                 ,input.prefix
                                 ,input.dept
                                 ,input.class
                                 ,input.subclass
                                 ,input.item_level
                                 ,input.tran_level
                                 ,input.item_status
                                 ,input.waste_type
                                 ,input.waste_pct
                                 ,input.default_waste_pct
                                 ,input.sellable_ind
                                 ,input.orderable_ind
                                 ,input.pack_ind
                                 ,input.pack_type
                                 ,input.simple_pack_ind
                                 ,input.diff_1
                                 ,input.diff_2
                                 ,input.diff_3
                                 ,input.diff_4
                                 ,CASE
                                     WHEN input.pack_ind = 'Y' AND input.receive_as_type IS NULL AND input.pack_type = 'V'
                                     THEN 'P'
                                     ELSE input.order_as_type
                                  END                             order_as_type
                                 ,input.contains_inner_ind
                                 ,input.store_ord_mult
                                 ,input.catch_weight_ind
                                 ,input.catch_weight_uom
                                 ,input.sale_type
                                 ,input.container_item
                                 ,input.item_xform_ind
                                 ,input.standard_uom
                                 ,input.uom_conv_factor
                                 ,ef.finisher_id                  loc
                                 ,'E'                             loc_type
                                 ,input.daily_waste_pct
                                 ,input.unit_cost_loc
                                 ,input.unit_retail_loc
                                 ,input.selling_unit_retail
                                 ,input.selling_uom
                                 ,input.multi_units
                                 ,input.multi_unit_retail
                                 ,input.multi_selling_uom
                                 ,input.item_loc_status
                                 ,input.taxable_ind
                                 ,input.ti
                                 ,input.hi
                                 ,input.meas_of_each
                                 ,input.meas_of_price
                                 ,input.uom_of_price
                                 ,input.primary_variant
                                 ,input.primary_supp
                                 ,input.primary_cntry
                                 ,input.primary_cost_pack
                                 ,input.local_item_desc
                                 ,input.local_short_desc
                                 ,input.receive_as_type
                                 ,input.store_price_ind
                                 ,input.uin_type
                                 ,input.uin_label
                                 ,input.ext_uin_ind
                                 ,input.capture_time
                                 ,input.source_method
                                 ,input.source_wh
                                 ,input.inbound_handling_days
                                 ,ef.currency_code
                                 ,input.like_store
                                 ,input.default_to_children_ind
                                 ,input.class_vat_ind
                                 ,input.hier_level
                                 ,input.hier_num_value
                                 ,input.hier_char_value
                                 ,ef.org_unit_id
                                 ,NULL                            lang
                                 ,NULL                            default_wh
                                 ,NULL                            store_type
                                 ,input.ranged_ind
                                 ,NULL                            costing_loc
                                 ,NULL                            costing_loc_type
                             FROM (
                                   SELECT finisher_id
                                         ,currency_code
                                         ,org_unit_id
                                     FROM v_external_finisher
                                    WHERE I_apply_sec_ind = 'Y'
                                   UNION ALL
                                   SELECT TO_NUMBER(partner_id) finisher_id
                                         ,currency_code
                                         ,org_unit_id
                                     FROM partner
                                    WHERE I_apply_sec_ind = 'N'
                                      AND partner_type    = 'E'
                                   )                ef
                                 ,nil_input_working input
                            WHERE input.process_id = I_nil_process_id
                              AND ef.currency_code = NVL(input.currency_code, ef.currency_code)
                              AND input.hier_level = 'AE';

   IF LP_from_itemloc_online = 'N' THEN
      -- Range the Costing Loc and Default WH. Only autorange if not called from itemloc form.
      MERGE INTO nil_input_working input
         USING (
                SELECT nil.process_id
                      ,nil.item
                      ,nil.item_parent
                      ,nil.item_grandparent
                      ,nil.item_desc
                      ,nil.item_short_desc
                      ,nil.item_number_type
                      ,nil.format_id
                      ,nil.prefix
                      ,nil.dept
                      ,nil.class
                      ,nil.subclass
                      ,nil.item_level
                      ,nil.tran_level
                      ,nil.item_status
                      ,nil.waste_type
                      ,nil.waste_pct
                      ,nil.default_waste_pct
                      ,nil.sellable_ind
                      ,nil.orderable_ind
                      ,nil.pack_ind
                      ,nil.pack_type
                      ,nil.simple_pack_ind
                      ,nil.diff_1
                      ,nil.diff_2
                      ,nil.diff_3
                      ,nil.diff_4
                      ,nil.order_as_type
                      ,nil.contains_inner_ind
                      ,nil.store_ord_mult
                      ,nil.catch_weight_ind
                      ,nil.catch_weight_uom
                      ,nil.sale_type
                      ,nil.container_item
                      ,nil.item_xform_ind
                      ,nil.standard_uom
                      ,nil.uom_conv_factor
                      ,nil.daily_waste_pct
                      ,nil.unit_cost_loc
                      ,nil.unit_retail_loc
                      ,nil.selling_unit_retail
                      ,nil.selling_uom
                      ,nil.multi_units
                      ,nil.multi_unit_retail
                      ,nil.multi_selling_uom
                      ,nil.item_loc_status
                      ,nil.taxable_ind
                      ,nil.ti
                      ,nil.hi
                      ,nil.meas_of_each
                      ,nil.meas_of_price
                      ,nil.uom_of_price
                      ,nil.primary_variant
                      ,nil.primary_supp
                      ,nil.primary_cntry
                      ,nil.primary_cost_pack
                      ,nil.local_item_desc
                      ,nil.local_short_desc
                      ,nil.receive_as_type
                      ,nil.store_price_ind
                      ,nil.uin_type
                      ,nil.uin_label
                      ,nil.ext_uin_ind
                      ,nil.capture_time
                      ,nil.source_wh
                      ,v.inbound_handling_days
                      ,nil.like_store
                      ,nil.default_to_children_ind
                      ,nil.class_vat_ind
                      ,nil.hier_level
                      ,nil.hier_num_value
                      ,nil.hier_char_value
                      ,nil.ranged_ind
                      ,v.loc
                      ,v.loc_type
                      ,'S' source_method
                      ,v.currency_code
                      ,v.store_type
                      ,v.org_unit_id
                      ,v.lang
                      ,nil.deposit_item_type
                  FROM (
                        SELECT nilw.*,
                               def.def_loc
                          FROM nil_input_working nilw
                              ,(
                                SELECT NVL(niw.costing_loc, niw.default_wh) def_loc
                                     ,niw.item
                                     ,MIN(niw.loc)                          loc
                                 FROM nil_input_working niw,
                              deps d
                                WHERE niw.process_id   = I_nil_process_id
                          AND niw.dept         = d.dept
                          AND d.purchase_type  = 0
                                  AND NVL(niw.costing_loc, niw.default_wh) IS NOT NULL
                                GROUP BY niw.item
                                        ,NVL(niw.costing_loc, niw.default_wh)
                                )                def
                         WHERE nilw.process_id = I_nil_process_id
                           AND nilw.item       = def.item
                           AND nilw.loc        = def.loc
                        ) nil
                      ,(
                        SELECT s.store loc,
                               s.store_type,
                               s.lang,
                               s.org_unit_id,
                               s.currency_code,
                               'S'                          loc_type,
                               NULL                         inbound_handling_days
                          FROM store s
                         UNION ALL
                        SELECT wh.wh loc,
                               NULL                         store_type,
                               NULL                         lang,
                               wh.org_unit_id,
                               wh.currency_code,
                               'W'                          loc_type,
                               wh.inbound_handling_days
                          FROM wh
                        ) v
                 WHERE v.loc          = nil.def_loc
                   AND nil.process_id = I_nil_process_id
                ) nil2
            ON (    input.process_id = I_nil_process_id
                AND input.loc        = nil2.loc
                AND input.item       = nil2.item)
          WHEN NOT MATCHED THEN
             INSERT (process_id
                    ,item
                    ,item_parent
                    ,item_grandparent
                    ,item_desc
                    ,item_short_desc
                    ,item_number_type
                    ,format_id
                    ,prefix
                    ,dept
                    ,class
                    ,subclass
                    ,item_level
                    ,tran_level
                    ,item_status
                    ,waste_type
                    ,waste_pct
                    ,default_waste_pct
                    ,sellable_ind
                    ,orderable_ind
                    ,pack_ind
                    ,pack_type
                    ,simple_pack_ind
                    ,diff_1
                    ,diff_2
                    ,diff_3
                    ,diff_4
                    ,order_as_type
                    ,contains_inner_ind
                    ,store_ord_mult
                    ,catch_weight_ind
                    ,catch_weight_uom
                    ,sale_type
                    ,container_item
                    ,item_xform_ind
                    ,standard_uom
                    ,uom_conv_factor
                    ,loc
                    ,loc_type
                    ,daily_waste_pct
                    ,unit_cost_loc
                    ,unit_retail_loc
                    ,selling_unit_retail
                    ,selling_uom
                    ,multi_units
                    ,multi_unit_retail
                    ,multi_selling_uom
                    ,item_loc_status
                    ,taxable_ind
                    ,ti
                    ,hi
                    ,meas_of_each
                    ,meas_of_price
                    ,uom_of_price
                    ,primary_variant
                    ,primary_supp
                    ,primary_cntry
                    ,primary_cost_pack
                    ,local_item_desc
                    ,local_short_desc
                    ,receive_as_type
                    ,store_price_ind
                    ,uin_type
                    ,uin_label
                    ,ext_uin_ind
                    ,capture_time
                    ,source_method
                    ,source_wh
                    ,inbound_handling_days
                    ,currency_code
                    ,like_store
                    ,default_to_children_ind
                    ,class_vat_ind
                    ,hier_level
                    ,hier_num_value
                    ,hier_char_value
                    ,org_unit_id
                    ,ranged_ind
                    ,lang
                    ,store_type
                    ,deposit_item_type)
             VALUES (nil2.process_id
                    ,nil2.item
                    ,nil2.item_parent
                    ,nil2.item_grandparent
                    ,nil2.item_desc
                    ,nil2.item_short_desc
                    ,nil2.item_number_type
                    ,nil2.format_id
                    ,nil2.prefix
                    ,nil2.dept
                    ,nil2.class
                    ,nil2.subclass
                    ,nil2.item_level
                    ,nil2.tran_level
                    ,nil2.item_status
                    ,nil2.waste_type
                    ,nil2.waste_pct
                    ,nil2.default_waste_pct
                    ,nil2.sellable_ind
                    ,nil2.orderable_ind
                    ,nil2.pack_ind
                    ,nil2.pack_type
                    ,nil2.simple_pack_ind
                    ,nil2.diff_1
                    ,nil2.diff_2
                    ,nil2.diff_3
                    ,nil2.diff_4
                    ,nil2.order_as_type
                    ,nil2.contains_inner_ind
                    ,nil2.store_ord_mult
                    ,nil2.catch_weight_ind
                    ,nil2.catch_weight_uom
                    ,nil2.sale_type
                    ,nil2.container_item
                    ,nil2.item_xform_ind
                    ,nil2.standard_uom
                    ,nil2.uom_conv_factor
                    ,nil2.loc
                    ,nil2.loc_type
                    ,nil2.daily_waste_pct
                    ,nil2.unit_cost_loc
                    ,decode(nil2.loc_type,'S',nil2.unit_retail_loc, NULL)
                    ,decode(nil2.loc_type,'S',nil2.selling_unit_retail, NULL)
                    ,decode(nil2.loc_type,'S',nil2.selling_uom, NULL)
                    ,decode(nil2.loc_type,'S',nil2.multi_units, NULL)
                    ,decode(nil2.loc_type,'S',nil2.multi_unit_retail, NULL)
                    ,decode(nil2.loc_type,'S',nil2.multi_selling_uom, NULL)
                    ,nil2.item_loc_status
                    ,nil2.taxable_ind
                    ,nil2.ti
                    ,nil2.hi
                    ,nil2.meas_of_each
                    ,nil2.meas_of_price
                    ,nil2.uom_of_price
                    ,nil2.primary_variant
                    ,nil2.primary_supp
                    ,nil2.primary_cntry
                    ,nil2.primary_cost_pack
                    ,nil2.local_item_desc
                    ,nil2.local_short_desc
                    ,nil2.receive_as_type
                    ,nil2.store_price_ind
                    ,nil2.uin_type
                    ,nil2.uin_label
                    ,nil2.ext_uin_ind
                    ,nil2.capture_time
                    ,'S' -- source_method
                    ,null
                    ,nil2.inbound_handling_days
                    ,nil2.currency_code
                    ,nil2.like_store
                    ,nil2.default_to_children_ind
                    ,nil2.class_vat_ind
                    ,nil2.hier_level
                    ,nil2.hier_num_value
                    ,nil2.hier_char_value
                    ,nil2.org_unit_id
                    ,nil2.ranged_ind
                    ,nil2.lang
                    ,nil2.store_type
                    ,nil2.deposit_item_type);
   END IF;

   ---
   -- Validate if all hier value is exploded
   OPEN C_GET_INV_HIER;
   FETCH C_GET_INV_HIER INTO L_hier_num_value,
                             L_hier_char_value;
   CLOSE C_GET_INV_HIER;
   ---
   IF L_hier_num_value IS NOT NULL OR L_hier_char_value IS NOT NULL THEN
      O_error_message := SQL_LIB.CREATE_MSG('INV_HIER_VALUE',
                                            NVL(L_hier_num_value, L_hier_char_value));
      RETURN FALSE;
   END IF;
   ---
   -- Remove hier records that were exploded
   DELETE
     FROM nil_input_working
    WHERE process_id  = I_nil_process_id
      AND loc        IS NULL
      AND loc_type   IS NULL;
   ---
   -- Get loc delivery country
   MERGE INTO nil_input_working nil
      USING (
             SELECT loc
                   ,loc_type
                   ,country_id
               FROM mv_loc_prim_addr
             ) input
         ON (    nil.process_id = I_nil_process_id
             AND nil.loc        = input.loc
             AND nil.loc_type   = input.loc_type)
       WHEN MATCHED THEN
          UPDATE
             SET nil.delivery_country_id = input.country_id;
   ---
   IF LP_system_options.default_tax_type IN ('SVAT', 'GTAX') THEN
      MERGE INTO nil_input_working nil
         USING (
                SELECT nilw.process_id
                      ,nilw.item
                      ,nilw.loc
                      ,nilw.loc_type
                      ,ca.localized_ind
                      ,NVL(ca.default_po_cost, 'BC') default_po_cost
                      ,CASE
                          WHEN ca.default_loc = nilw.loc
                          THEN 'Y'
                          ELSE 'N'
                       END                           default_loc_ind
                  FROM nil_input_working nilw
                      ,country_attrib ca
                 WHERE nilw.process_id          = I_nil_process_id
                   AND nilw.delivery_country_id = ca.country_id
                ) input
            ON (    nil.process_id = I_nil_process_id
                AND nil.item       = input.item
                AND nil.loc        = input.loc
                AND nil.loc_type   = input.loc_type)
          WHEN MATCHED THEN
             UPDATE
                SET nil.delivery_country_localized_ind = input.localized_ind
                   ,nil.default_loc_ind                = input.default_loc_ind
                   ,nil.default_po_cost                = input.default_po_cost;
   END IF;
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      IF C_GET_INV_HIER%ISOPEN THEN
         CLOSE C_GET_INV_HIER;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END EXPLODE_LOCS;
--------------------------------------------------------------------------------
FUNCTION GET_SOURCE_METHOD(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.GET_SOURCE_METHOD';

BEGIN

   -- Update first source_method if NULL.
   -- If called from online set the source_method to 'S'.
   -- If called from PO, set source_method to 'S' if location is 'W'.
   -- For stores, check if default WH exists then set to 'W'. If none, then
   -- set source_method to 'S'.
   IF LP_from_itemloc_online = 'Y' THEN
      UPDATE nil_input_working
         SET source_method = 'S'
       WHERE process_id     = I_nil_process_id
         AND source_method IS NULL;
   ELSIF LP_called_from_PO = 'Y' THEN
      UPDATE nil_input_working nil
         SET source_method  = CASE loc_type
                                 WHEN 'S'
                                 THEN
                                    CASE
                                       WHEN default_wh IS NULL
                                       THEN 'S'
                                       ELSE 'W'
                                    END
                                 ELSE 'S'
                              END
            ,source_wh      = CASE loc_type
                                 WHEN 'S'
                                 THEN default_wh
                                 ELSE NULL
                              END
       WHERE process_id     = I_nil_process_id
         AND source_method IS NULL;
   ELSE
      -- After the above update, source_method will be NULL only if it is not called from
      -- a PO or online. Populate the source_method depending on the WH.
      -- If the source_method is still NULL, then the location is a store.
      -- Get first the WH with the same entity. If none, get WH belonging to the same channel,
      -- if none, then get the first stockholding WH ranged or to be ranged. If a source WH is
      -- retrieved, then set the source_method to 'W'. Else then set to 'S'.
      UPDATE nil_input_working nil
         SET source_method  = 'S'
            ,source_wh      = NULL
       WHERE process_id     = I_nil_process_id
         AND ((source_method IS NULL
         AND loc_type       = 'W')
       OR  (EXISTS (SELECT 1 
                     FROM deps d
                  WHERE d.dept = nil.dept
                  AND d.purchase_type in (1,2))));
                  
      ---
      MERGE INTO nil_input_working nil
         USING (
                WITH nilw AS
                   (
                    SELECT process_id
                          ,item
                          ,loc
                          ,loc_type
                          ,source_method
                      FROM nil_input_working
                     WHERE process_id = I_nil_process_id
                    )
                SELECT process_id
                      ,nilw.item
                      ,nilw.loc
                      ,wh.wh                                source_wh
                      ,ROW_NUMBER() OVER (PARTITION BY nilw.item
                                                      ,nilw.loc
                                              ORDER BY CASE
                                                          WHEN wh.wh = st.default_wh
                                                          THEN 1 -- Store default wh is exist.
                                                          WHEN (    wh.channel_id    = st.channel_id
                                                                AND wh.tsf_entity_id = st.tsf_entity_id)
                                                          THEN 2 -- Same entity and channel
                                                          WHEN wh.channel_id = st.channel_id
                                                          THEN 3 -- Same Channel
                                                          ELSE 4 -- Any ranged WH or NULL
                                                       END) priority
                  FROM nilw
                      ,(
                        SELECT il1.item
                              ,il1.loc
                          FROM item_loc      il1
                              ,(SELECT DISTINCT item
                                  FROM nilw) v
                         WHERE il1.item      = v.item
                           AND il1.loc_type  = 'W'
                         UNION ALL
                        SELECT item
                              ,loc
                          FROM nilw
                         WHERE loc_type = 'W'
                        )                il
                      ,wh                wh
                      ,store             st
                 WHERE nilw.source_method     IS NULL
                   AND nilw.item               = il.item(+)
                   AND il.loc                  = wh.wh(+)
                   AND nilw.loc                = st.store
                   AND wh.stockholding_ind(+)  = 'Y'
                   AND wh.finisher_ind(+)      = 'N'
                ) input
            ON (    nil.process_id = input.process_id
                AND nil.item       = input.item
                AND nil.loc        = input.loc
                AND input.priority = 1)
          WHEN MATCHED THEN
             UPDATE
                SET nil.source_wh     = DECODE(nil.source_method, 'S', NULL, input.source_wh)
                   ,nil.source_method = DECODE(input.source_wh, NULL, 'S', 'W');
   END IF;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END GET_SOURCE_METHOD;
--------------------------------------------------------------------------------
-- This function will check if the Company store ranged to the item has a source wh
-- asscociated . if not associated, the default_wh of company store will be source wh. If default
-- wh is NULL, then wf_default_wh from SYSTEM_OPTIONS will be source wh.
-- Also checks if the source wh of company store is already ranged to the item or not.
--------------------------------------------------------------------------------
FUNCTION CHECK_SOURCE_WH(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_nil_process_id IN     NIL_INPUT_WORKING.PROCESS_ID%TYPE)               
RETURN BOOLEAN IS
   
   L_program                  VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.CHECK_SOURCE_WH';
   
BEGIN
  
      MERGE INTO nil_input_working input
         USING (
                SELECT nil.process_id
                      ,nil.item
                      ,nil.item_parent
                      ,nil.item_grandparent
                      ,nil.item_desc
                      ,nil.item_short_desc
                      ,nil.item_number_type
                      ,nil.format_id
                      ,nil.prefix
                      ,nil.dept
                      ,nil.class
                      ,nil.subclass
                      ,nil.item_level
                      ,nil.tran_level
                      ,nil.item_status
                      ,nil.waste_type
                      ,nil.waste_pct
                      ,nil.default_waste_pct
                      ,nil.sellable_ind
                      ,nil.orderable_ind
                      ,nil.pack_ind
                      ,nil.pack_type
                      ,nil.simple_pack_ind
                      ,nil.diff_1
                      ,nil.diff_2
                      ,nil.diff_3
                      ,nil.diff_4
                      ,nil.order_as_type
                      ,nil.contains_inner_ind
                      ,nil.store_ord_mult
                      ,nil.catch_weight_ind
                      ,nil.catch_weight_uom
                      ,nil.sale_type
                      ,nil.container_item
                      ,nil.item_xform_ind
                      ,nil.standard_uom
                      ,nil.uom_conv_factor
                      ,nil.daily_waste_pct
                      ,nil.unit_cost_loc
                      ,NULL unit_retail_loc
                      ,NULL selling_unit_retail
                      ,nil.selling_uom
                      ,nil.multi_units
                      ,nil.multi_unit_retail
                      ,nil.multi_selling_uom
                      ,nil.item_loc_status
                      ,nil.taxable_ind
                      ,nil.ti
                      ,nil.hi
                      ,nil.meas_of_each
                      ,nil.meas_of_price
                      ,nil.uom_of_price
                      ,nil.primary_variant
                      ,nil.primary_supp
                      ,nil.primary_cntry
                      ,nil.primary_cost_pack
                      ,nil.local_item_desc
                      ,nil.local_short_desc
                      ,CASE
                         WHEN nil.pack_ind = 'Y' AND nil.pack_type = 'V'
                         THEN 'P'
                         ELSE nil.order_as_type
                       END                               receive_as_type                      
                      ,nil.store_price_ind
                      ,nil.uin_type
                      ,nil.uin_label
                      ,nil.ext_uin_ind
                      ,nil.capture_time
                      ,nil.source_wh
                      ,v.inbound_handling_days
                      ,nil.like_store
                      ,nil.default_to_children_ind
                      ,nil.class_vat_ind
                      ,nil.hier_level
                      ,nil.hier_num_value
                      ,nil.hier_char_value
                      ,v.loc
                      ,v.loc_type
                      ,'S' source_method
                      ,v.currency_code
                      ,v.store_type
                      ,v.org_unit_id
                      ,v.lang
                  FROM (
                        SELECT nilw.*,
                               def.def_loc
                          FROM nil_input_working nilw
                              ,(
                                SELECT NVL(niw.source_wh, niw.default_wh) def_loc
                                     ,niw.item
                                     ,MIN(niw.loc)                          loc
                                 FROM nil_input_working niw
                                WHERE niw.process_id                        = I_nil_process_id
                                  AND NVL(niw.source_wh, niw.default_wh) IS NOT NULL
                                GROUP BY niw.item
                                        ,NVL(niw.source_wh, niw.default_wh)
                                )                def
                         WHERE nilw.process_id    = I_nil_process_id
                           AND nilw.item          = def.item
                           AND nilw.loc           = def.loc
                           AND nilw.source_method = 'W'
                        ) nil
                      ,(
                        SELECT wh.wh loc,
                               NULL                         store_type,
                               NULL                         lang,
                               wh.org_unit_id,
                               wh.currency_code,
                               'W'                          loc_type,
                               wh.inbound_handling_days
                          FROM wh
                        ) v
                 WHERE v.loc          = nil.def_loc
                   AND nil.process_id = I_nil_process_id
                ) nil2
            ON (    input.process_id = I_nil_process_id
                AND input.loc        = nil2.loc
                AND input.item       = nil2.item)
          WHEN NOT MATCHED THEN
             INSERT (process_id
                    ,item
                    ,item_parent
                    ,item_grandparent
                    ,item_desc
                    ,item_short_desc
                    ,item_number_type
                    ,format_id
                    ,prefix
                    ,dept
                    ,class
                    ,subclass
                    ,item_level
                    ,tran_level
                    ,item_status
                    ,waste_type
                    ,waste_pct
                    ,default_waste_pct
                    ,sellable_ind
                    ,orderable_ind
                    ,pack_ind
                    ,pack_type
                    ,simple_pack_ind
                    ,diff_1
                    ,diff_2
                    ,diff_3
                    ,diff_4
                    ,order_as_type
                    ,contains_inner_ind
                    ,store_ord_mult
                    ,catch_weight_ind
                    ,catch_weight_uom
                    ,sale_type
                    ,container_item
                    ,item_xform_ind
                    ,standard_uom
                    ,uom_conv_factor
                    ,loc
                    ,loc_type
                    ,daily_waste_pct
                    ,unit_cost_loc
                    ,unit_retail_loc
                    ,selling_unit_retail
                    ,selling_uom
                    ,multi_units
                    ,multi_unit_retail
                    ,multi_selling_uom
                    ,item_loc_status
                    ,taxable_ind
                    ,ti
                    ,hi
                    ,meas_of_each
                    ,meas_of_price
                    ,uom_of_price
                    ,primary_variant
                    ,primary_supp
                    ,primary_cntry
                    ,primary_cost_pack
                    ,local_item_desc
                    ,local_short_desc
                    ,receive_as_type
                    ,store_price_ind
                    ,uin_type
                    ,uin_label
                    ,ext_uin_ind
                    ,capture_time
                    ,source_method
                    ,source_wh
                    ,inbound_handling_days
                    ,currency_code
                    ,like_store
                    ,default_to_children_ind
                    ,class_vat_ind
                    ,hier_level
                    ,hier_num_value
                    ,hier_char_value
                    ,org_unit_id
                    ,lang
                    ,store_type)
             VALUES (nil2.process_id
                    ,nil2.item
                    ,nil2.item_parent
                    ,nil2.item_grandparent
                    ,nil2.item_desc
                    ,nil2.item_short_desc
                    ,nil2.item_number_type
                    ,nil2.format_id
                    ,nil2.prefix
                    ,nil2.dept
                    ,nil2.class
                    ,nil2.subclass
                    ,nil2.item_level
                    ,nil2.tran_level
                    ,nil2.item_status
                    ,nil2.waste_type
                    ,nil2.waste_pct
                    ,nil2.default_waste_pct
                    ,nil2.sellable_ind
                    ,nil2.orderable_ind
                    ,nil2.pack_ind
                    ,nil2.pack_type
                    ,nil2.simple_pack_ind
                    ,nil2.diff_1
                    ,nil2.diff_2
                    ,nil2.diff_3
                    ,nil2.diff_4
                    ,nil2.order_as_type
                    ,nil2.contains_inner_ind
                    ,nil2.store_ord_mult
                    ,nil2.catch_weight_ind
                    ,nil2.catch_weight_uom
                    ,nil2.sale_type
                    ,nil2.container_item
                    ,nil2.item_xform_ind
                    ,nil2.standard_uom
                    ,nil2.uom_conv_factor
                    ,nvl(nil2.loc,LP_system_options.wf_default_wh)
                    ,'W'                    --nil2.loc_type
                    ,nil2.daily_waste_pct
                    ,nil2.unit_cost_loc
                    ,nil2.unit_retail_loc
                    ,nil2.selling_unit_retail
                    ,nil2.selling_uom
                    ,nil2.multi_units
                    ,nil2.multi_unit_retail
                    ,nil2.multi_selling_uom
                    ,nil2.item_loc_status
                    ,nil2.taxable_ind
                    ,nil2.ti
                    ,nil2.hi
                    ,nil2.meas_of_each
                    ,nil2.meas_of_price
                    ,nil2.uom_of_price
                    ,nil2.primary_variant
                    ,nil2.primary_supp
                    ,nil2.primary_cntry
                    ,nil2.primary_cost_pack
                    ,nil2.local_item_desc
                    ,nil2.local_short_desc
                    ,nil2.receive_as_type
                    ,nil2.store_price_ind
                    ,nil2.uin_type
                    ,nil2.uin_label
                    ,nil2.ext_uin_ind
                    ,nil2.capture_time
                    ,'S'   -- source_method
                    ,NULL  --nil2.source_wh
                    ,nil2.inbound_handling_days
                    ,nil2.currency_code
                    ,nil2.like_store
                    ,nil2.default_to_children_ind
                    ,nil2.class_vat_ind
                    ,NULL                --nil2.hier_level
                    ,NULL                --nil2.hier_num_value
                    ,NULL                --nil2.hier_char_value
                    ,nil2.org_unit_id
                    ,nil2.lang
                    ,nil2.store_type);   
      ---
      -- Get loc delivery country
      MERGE INTO nil_input_working nil
         USING (
                SELECT loc
                      ,loc_type
                      ,country_id
                  FROM mv_loc_prim_addr
                ) input
            ON (    nil.process_id = I_nil_process_id
                AND nil.loc        = input.loc
                AND nil.loc_type   = input.loc_type)
          WHEN MATCHED THEN
             UPDATE
                SET nil.delivery_country_id = input.country_id;
      ---
      IF LP_system_options.default_tax_type IN ('SVAT', 'GTAX') THEN
         MERGE INTO nil_input_working nil
            USING (
                   SELECT nilw.process_id
                         ,nilw.item
                         ,nilw.loc
                         ,nilw.loc_type
                         ,ca.localized_ind
                         ,NVL(ca.default_po_cost, 'BC') default_po_cost
                         ,CASE
                             WHEN ca.default_loc = nilw.loc
                             THEN 'Y'
                             ELSE 'N'
                          END                           default_loc_ind
                     FROM nil_input_working nilw
                         ,country_attrib ca
                    WHERE nilw.process_id          = I_nil_process_id
                      AND nilw.delivery_country_id = ca.country_id
                   ) input
               ON (    nil.process_id = I_nil_process_id
                   AND nil.item       = input.item
                   AND nil.loc        = input.loc
                   AND nil.loc_type   = input.loc_type)
             WHEN MATCHED THEN
                UPDATE
                   SET nil.delivery_country_localized_ind = input.localized_ind
                      ,nil.default_loc_ind                = input.default_loc_ind
                      ,nil.default_po_cost                = input.default_po_cost;
      END IF;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHECK_SOURCE_WH; 
--------------------------------------------------------------------------------
FUNCTION GET_SUPPLIER(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.GET_SUPPLIER';

BEGIN
   --
   -- Get MSOB_SUPP
   -- MSOB_SUPP will contain 1 supplier per Location
   -- The value may be as folllows
   --     1. NULL - This is possible for
   --                    1. Non-Company stores and
   --                    2. For all locations when MSOB is OFF
   --
   --     2. A valid Supplier - This is possible only when MSOB is ON and
   --                           for those locations WHERE atleast one Supplier
   --                           supplies to their Org Unit.
   --                           The supplier passed by user has the highest priority,
   --                           followed by Primary supplier

   ---
   -- For WH/Finisher, take supplier with the same org_unit or Primary.
   -- For Company Stores, if the source_method is 'S', take the DSD supplier. If no DSD supplier, Primary Supplier has the highest priority.
   -- Else we just take the primary supplier. We will populate for Franchise stores later when we have retrieved the primary_supplier for
   -- all locations in the working table.
   IF LP_system_options.org_unit_ind = 'Y' THEN
      MERGE INTO nil_input_working nil
         USING (
                SELECT nilw.item
                      ,nilw.loc
                      ,isp.supplier
                      ,ROW_NUMBER() OVER (PARTITION BY nilw.item
                                                      ,nilw.loc
                                              ORDER BY CASE nilw.store_type
                                                       WHEN 'C'
                                                       THEN
                                                          CASE nilw.source_method
                                                             WHEN 'S'
                                                             THEN
                                                                CASE sup.dsd_ind
                                                                   WHEN 'Y'
                                                                   THEN 1
                                                                   ELSE 2
                                                                END
                                                             ELSE
                                                                CASE isp.primary_supp_ind
                                                                   WHEN 'Y'
                                                                   THEN 1
                                                                   ELSE 2
                                                                END
                                                          END
                                                       ELSE
                                                          CASE isp.primary_supp_ind
                                                             WHEN 'Y'
                                                             THEN 1
                                                             ELSE 2
                                                          END
                                                    END)    row_num
                  FROM item_supplier     isp
                      ,partner_org_unit  pou
                      ,nil_input_working nilw
                      ,sups_imp_exp      sie
                      ,sups              sup
                 WHERE nilw.process_id            = I_nil_process_id
                   AND NVL(nilw.store_type, 'C')  = 'C'
                   AND nilw.primary_supp         IS NULL
                   AND (
                           nilw.pack_ind = 'N'
                        OR (
                                nilw.pack_ind      = 'Y'
                            AND nilw.orderable_ind = 'Y'
                            )
                        )
                   AND isp.item                   = nilw.item
                   AND pou.partner                = isp.supplier
                   AND pou.partner_type          IN ('S', 'U')
                   AND sie.supplier(+)            = isp.supplier
                   AND (
                           (    pou.org_unit_id  = nilw.org_unit_id
                            AND sie.supplier    IS NULL
                            )
                        OR sie.supplier IS NOT NULL
                        )
                   AND sup.supplier               = isp.supplier
                ) input
            ON (    nil.process_id = I_nil_process_id
                AND nil.item       = input.item
                AND nil.loc        = input.loc
                AND input.row_num  = 1)
          WHEN MATCHED THEN
             UPDATE
                SET nil.primary_supp  = input.supplier;
   END IF;

   -- For those not updated by the above query (Org_Unit_ind = 'N' or no matching criteria), update with primary supplier.
   MERGE INTO nil_input_working nil
      USING (
             SELECT nilw.item
                   ,nilw.loc
                   ,isp.supplier
                   ,ROW_NUMBER() OVER (PARTITION BY nilw.item
                                                   ,nilw.loc
                                           ORDER BY CASE nilw.store_type
                                                       WHEN 'C'
                                                       THEN
                                                          CASE nilw.source_method
                                                             WHEN 'S'
                                                             THEN
                                                                CASE sup.dsd_ind
                                                                   WHEN 'Y'
                                                                   THEN 1
                                                                   ELSE 2
                                                                END
                                                             ELSE
                                                                CASE isp.primary_supp_ind
                                                                   WHEN 'Y'
                                                                   THEN 1
                                                                   ELSE 2
                                                                END
                                                          END
                                                       ELSE
                                                          CASE isp.primary_supp_ind
                                                             WHEN 'Y'
                                                             THEN 1
                                                             ELSE 2
                                                          END
                                                    END) row_num
               FROM item_supplier     isp
                   ,nil_input_working nilw
                   ,sups              sup
              WHERE nilw.process_id           = I_nil_process_id
                AND NVL(nilw.store_type, 'C') = 'C'
                AND nilw.primary_supp        IS NULL
                AND (
                        nilw.pack_ind = 'N'
                     OR (
                             nilw.pack_ind      = 'Y'
                         AND nilw.orderable_ind = 'Y'
                         )
                     )
                AND isp.item                  = nilw.item
                AND sup.supplier              = isp.supplier
             ) input
         ON (    nil.process_id = I_nil_process_id
             AND nil.item       = input.item
             AND nil.loc        = input.loc
             AND input.row_num  = 1)
       WHEN MATCHED THEN
          UPDATE
             SET nil.primary_supp  = input.supplier;

   -- Update for Franchise Stores. We just take the primary_supplier and country of the costing loc.
   MERGE INTO nil_input_working nil
      USING (
             SELECT nilw1.item
                   ,nilw1.loc
                   ,NVL(il.primary_supp,  nilw2.primary_supp)  supplier
                   ,NVL(il.primary_cntry, nilw2.primary_cntry) cntry
               FROM nil_input_working nilw1
                   ,nil_input_working nilw2
                   ,item_loc          il
              WHERE nilw1.process_id   = I_nil_process_id
                AND nilw1.item         = il.item(+)
                AND nilw1.costing_loc  = il.loc(+)
                AND nilw1.item         = nilw2.item(+)
                AND nilw1.costing_loc  = nilw2.loc(+)
                AND nilw1.process_id   = nilw2.process_id(+)
                AND nilw1.store_type  != 'C'
                AND (
                        nilw1.pack_ind = 'N'
                     OR (
                             nilw1.pack_ind       = 'Y'
                         AND nilw1.orderable_ind  = 'Y'
                         )
                     )
             ) input
         ON (    nil.process_id = I_nil_process_id
             AND nil.item       = input.item
             AND nil.loc        = input.loc)
       WHEN MATCHED THEN
          UPDATE
             SET nil.primary_supp  = input.supplier
                ,nil.primary_cntry = input.cntry;

   ---
   -- Get Supplier Currency
   MERGE INTO nil_input_working nil
      USING (SELECT sups.supplier,
                    sups.currency_code
               FROM sups) input
         ON (    nil.process_id = I_nil_process_id
             AND input.supplier = nil.primary_supp)
       WHEN MATCHED THEN
          UPDATE
             SET nil.supp_currency_code  = input.currency_code;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END GET_SUPPLIER;
--------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEMS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.EXPLODE_ITEMS';

BEGIN

   -- Get children and Container Item
   INSERT INTO nil_input_working (process_id
                                 ,item
                                 ,item_parent
                                 ,item_grandparent
                                 ,item_desc
                                 ,item_short_desc
                                 ,dept
                                 ,class
                                 ,subclass
                                 ,item_level
                                 ,tran_level
                                 ,item_status
                                 ,waste_type
                                 ,waste_pct
                                 ,default_waste_pct
                                 ,sellable_ind
                                 ,orderable_ind
                                 ,pack_ind
                                 ,pack_type
                                 ,simple_pack_ind
                                 ,diff_1
                                 ,diff_2
                                 ,diff_3
                                 ,diff_4
                                 ,order_as_type
                                 ,contains_inner_ind
                                 ,store_ord_mult
                                 ,catch_weight_ind
                                 ,catch_weight_uom
                                 ,sale_type
                                 ,container_item
                                 ,item_xform_ind
                                 ,standard_uom
                                 ,uom_conv_factor
                                 ,loc
                                 ,loc_type
                                 ,daily_waste_pct
                                 ,unit_cost_loc
                                 ,unit_retail_loc
                                 ,selling_unit_retail
                                 ,selling_uom
                                 ,multi_units
                                 ,multi_unit_retail
                                 ,multi_selling_uom
                                 ,item_loc_status
                                 ,taxable_ind
                                 ,ti
                                 ,hi
                                 ,meas_of_each
                                 ,meas_of_price
                                 ,uom_of_price
                                 ,primary_variant
                                 ,primary_supp
                                 ,primary_cntry
                                 ,primary_cost_pack
                                 ,local_item_desc
                                 ,local_short_desc
                                 ,receive_as_type
                                 ,store_price_ind
                                 ,uin_type
                                 ,uin_label
                                 ,ext_uin_ind
                                 ,capture_time
                                 ,source_method
                                 ,source_wh
                                 ,inbound_handling_days
                                 ,currency_code
                                 ,like_store
                                 ,default_to_children_ind
                                 ,class_vat_ind
                                 ,hier_level
                                 ,hier_num_value
                                 ,hier_char_value
                                 ,store_type
                                 ,org_unit_id
                                 ,source_wh_supp
                                 ,source_wh_org_unit_id
                                 ,unit_cost_sup
                                 ,child_from_input
                                 ,item_from_input_pack
                                 ,delivery_country_id
                                 ,lang
                                 ,delivery_country_localized_ind
                                 ,default_loc_ind
                                 ,default_po_cost
                                 ,ranged_ind
                                 ,costing_loc
                                 ,costing_loc_type)
                           SELECT input.process_id
                                 ,im.item
                                 ,im.item_parent
                                 ,im.item_grandparent
                                 ,im.item_desc
                                 ,im.short_desc
                                 ,im.dept
                                 ,im.class
                                 ,im.subclass
                                 ,im.item_level
                                 ,im.tran_level
                                 ,im.status
                                 ,im.waste_type
                                 ,im.waste_pct
                                 ,im.default_waste_pct
                                 ,im.sellable_ind
                                 ,im.orderable_ind
                                 ,im.pack_ind
                                 ,im.pack_type
                                 ,im.simple_pack_ind
                                 ,im.diff_1
                                 ,im.diff_2
                                 ,im.diff_3
                                 ,im.diff_4
                                 ,CASE
                                     WHEN im.pack_ind = 'Y' AND input.loc_type != 'S' AND input.receive_as_type IS NULL AND im.pack_type = 'V'
                                     THEN 'P'
                                     ELSE im.order_as_type
                                  END order_as_type
                                 ,im.contains_inner_ind
                                 ,im.store_ord_mult
                                 ,im.catch_weight_ind
                                 ,im.catch_weight_uom
                                 ,im.sale_type
                                 ,im.container_item
                                 ,im.item_xform_ind
                                 ,im.standard_uom
                                 ,im.uom_conv_factor
                                 ,input.loc
                                 ,input.loc_type
                                 ,input.daily_waste_pct
                                 ,input.unit_cost_loc
                                 ,NULL unit_retail_loc
                                 ,NULL selling_unit_retail
                                 ,NULL selling_uom
                                 ,NULL multi_units
                                 ,NULL multi_unit_retail
                                 ,NULL multi_selling_uom
                                 ,input.item_loc_status
                                 ,input.taxable_ind
                                 ,input.ti
                                 ,input.hi
                                 ,input.meas_of_each
                                 ,input.meas_of_price
                                 ,input.uom_of_price
                                 ,input.primary_variant
                                 ,input.primary_supp
                                 ,input.primary_cntry
                                 ,input.primary_cost_pack
                                 ,im.item_desc  --local_item_desc
                                 ,im.short_desc --local_short_desc
                                 ,input.receive_as_type
                                 ,input.store_price_ind
                                 ,input.uin_type
                                 ,input.uin_label
                                 ,input.ext_uin_ind
                                 ,input.capture_time
                                 ,input.source_method
                                 ,input.source_wh
                                 ,input.inbound_handling_days
                                 ,input.currency_code
                                 ,input.like_store
                                 ,'N'                         --default_to_children_ind
                                 ,input.class_vat_ind
                                 ,input.hier_level
                                 ,input.hier_num_value
                                 ,input.hier_char_value
                                 ,input.store_type
                                 ,input.org_unit_id
                                 ,input.source_wh_supp
                                 ,input.source_wh_org_unit_id
                                 ,input.unit_cost_sup
                                 ,'Y'                         --child_from_input
                                 ,'N'                         --item_from_input_pack
                                 ,input.delivery_country_id
                                 ,input.lang
                                 ,input.delivery_country_localized_ind
                                 ,input.default_loc_ind
                                 ,input.default_po_cost
                                 ,input.ranged_ind
                                 ,input.costing_loc
                                 ,input.costing_loc_type
                             FROM nil_input_working input
                                 ,item_master       im
                            WHERE input.process_id  = I_nil_process_id
                              AND (
                                      (
                                            input.default_to_children_ind  = 'Y'
                                       AND  im.item_level                 <= im.tran_level
                                       AND (   im.item_parent      = input.item
                                            OR im.item_grandparent = input.item
                                            )
                                       )
                                   OR (
                                           input.like_store        IS NULL
                                       AND input.deposit_item_type  = 'E'
                                       AND im.item                  = input.container_item
                                       )
                                   );
   ---
   -- Get Pack SKUs
   INSERT INTO nil_input_working (process_id
                                 ,item
                                 ,item_parent
                                 ,item_grandparent
                                 ,item_desc
                                 ,item_short_desc
                                 ,dept
                                 ,class
                                 ,subclass
                                 ,item_level
                                 ,tran_level
                                 ,item_status
                                 ,waste_type
                                 ,waste_pct
                                 ,default_waste_pct
                                 ,sellable_ind
                                 ,orderable_ind
                                 ,pack_ind
                                 ,pack_type
                                 ,simple_pack_ind
                                 ,diff_1
                                 ,diff_2
                                 ,diff_3
                                 ,diff_4
                                 ,order_as_type
                                 ,contains_inner_ind
                                 ,store_ord_mult
                                 ,catch_weight_ind
                                 ,catch_weight_uom
                                 ,sale_type
                                 ,container_item
                                 ,item_xform_ind
                                 ,standard_uom
                                 ,uom_conv_factor
                                 ,loc
                                 ,loc_type
                                 ,daily_waste_pct
                                 ,unit_cost_loc
                                 ,unit_retail_loc
                                 ,selling_unit_retail
                                 ,selling_uom
                                 ,multi_units
                                 ,multi_unit_retail
                                 ,multi_selling_uom
                                 ,item_loc_status
                                 ,taxable_ind
                                 ,ti
                                 ,hi
                                 ,meas_of_each
                                 ,meas_of_price
                                 ,uom_of_price
                                 ,primary_variant
                                 ,primary_supp
                                 ,primary_cntry
                                 ,primary_cost_pack
                                 ,local_item_desc
                                 ,local_short_desc
                                 ,receive_as_type
                                 ,store_price_ind
                                 ,uin_type
                                 ,uin_label
                                 ,ext_uin_ind
                                 ,capture_time
                                 ,source_method
                                 ,source_wh
                                 ,inbound_handling_days
                                 ,currency_code
                                 ,like_store
                                 ,default_to_children_ind
                                 ,class_vat_ind
                                 ,hier_level
                                 ,hier_num_value
                                 ,hier_char_value
                                 ,store_type
                                 ,org_unit_id
                                 ,source_wh_supp
                                 ,source_wh_org_unit_id
                                 ,unit_cost_sup
                                 ,child_from_input
                                 ,item_from_input_pack
                                 ,delivery_country_id
                                 ,lang
                                 ,delivery_country_localized_ind
                                 ,default_loc_ind
                                 ,default_po_cost
                                 ,ranged_ind
                                 ,costing_loc
                                 ,costing_loc_type)
                           SELECT DISTINCT
                                  input.process_id
                                 ,im.item
                                 ,im.item_parent
                                 ,im.item_grandparent
                                 ,im.item_desc
                                 ,im.short_desc
                                 ,im.dept
                                 ,im.class
                                 ,im.subclass
                                 ,im.item_level
                                 ,im.tran_level
                                 ,im.status
                                 ,im.waste_type
                                 ,im.waste_pct
                                 ,im.default_waste_pct
                                 ,im.sellable_ind
                                 ,im.orderable_ind
                                 ,im.pack_ind
                                 ,im.pack_type
                                 ,im.simple_pack_ind
                                 ,im.diff_1
                                 ,im.diff_2
                                 ,im.diff_3
                                 ,im.diff_4
                                 ,CASE
                                     WHEN im.pack_ind = 'Y' AND input.loc_type != 'S' AND input.receive_as_type IS NULL AND im.pack_type = 'V'
                                     THEN 'P'
                                     ELSE im.order_as_type
                                  END order_as_type
                                 ,im.contains_inner_ind
                                 ,im.store_ord_mult
                                 ,im.catch_weight_ind
                                 ,im.catch_weight_uom
                                 ,im.sale_type
                                 ,im.container_item
                                 ,im.item_xform_ind
                                 ,im.standard_uom
                                 ,im.uom_conv_factor
                                 ,input.loc
                                 ,input.loc_type
                                 ,input.daily_waste_pct
                                 ,input.unit_cost_loc
                                 ,NULL unit_retail_loc
                                 ,NULL selling_unit_retail
                                 ,NULL selling_uom
                                 ,NULL multi_units
                                 ,NULL multi_unit_retail
                                 ,NULL multi_selling_uom
                                 ,input.item_loc_status
                                 ,input.taxable_ind
                                 ,input.ti
                                 ,input.hi
                                 ,input.meas_of_each
                                 ,input.meas_of_price
                                 ,input.uom_of_price
                                 ,input.primary_variant
                                 ,input.primary_supp
                                 ,NULL                      --primary_cntry
                                 ,input.primary_cost_pack
                                 ,im.item_desc              --local_item_desc
                                 ,im.short_desc             --local_short_desc
                                 ,input.receive_as_type
                                 ,input.store_price_ind
                                 ,input.uin_type
                                 ,input.uin_label
                                 ,input.ext_uin_ind
                                 ,input.capture_time
                                 ,input.source_method
                                 ,input.source_wh
                                 ,input.inbound_handling_days
                                 ,input.currency_code
                                 ,input.like_store
                                 ,'N'                       --default_to_children_ind
                                 ,input.class_vat_ind
                                 ,input.hier_level
                                 ,input.hier_num_value
                                 ,input.hier_char_value
                                 ,input.store_type
                                 ,input.org_unit_id
                                 ,input.source_wh_supp
                                 ,input.source_wh_org_unit_id
                                 ,input.unit_cost_sup
                                 ,'N'                       --child_from_input
                                 ,'Y'                       --item_from_input_pack
                                 ,input.delivery_country_id
                                 ,input.lang
                                 ,input.delivery_country_localized_ind
                                 ,input.default_loc_ind
                                 ,input.default_po_cost
                                 ,input.ranged_ind
                                 ,input.costing_loc
                                 ,input.costing_loc_type
                             FROM nil_input_working input
                                 ,item_master       im
                                 ,packitem_breakout pib
                            WHERE input.process_id                  = I_nil_process_id
                              AND input.pack_ind                    = 'Y'
                              AND NVL(input.child_from_input, 'N')  = 'N'
                              AND input.like_store                 IS NULL
                              AND pib.pack_no                       = input.item
                              AND pib.item                          = im.item;
   ---
   --Get Pack in Pack
   INSERT INTO nil_input_working (process_id
                                 ,item
                                 ,item_parent
                                 ,item_grandparent
                                 ,item_desc
                                 ,item_short_desc
                                 ,dept
                                 ,class
                                 ,subclass
                                 ,item_level
                                 ,tran_level
                                 ,item_status
                                 ,waste_type
                                 ,waste_pct
                                 ,default_waste_pct
                                 ,sellable_ind
                                 ,orderable_ind
                                 ,pack_ind
                                 ,pack_type
                                 ,simple_pack_ind
                                 ,diff_1
                                 ,diff_2
                                 ,diff_3
                                 ,diff_4
                                 ,order_as_type
                                 ,contains_inner_ind
                                 ,store_ord_mult
                                 ,catch_weight_ind
                                 ,catch_weight_uom
                                 ,sale_type
                                 ,container_item
                                 ,item_xform_ind
                                 ,standard_uom
                                 ,uom_conv_factor
                                 ,loc
                                 ,loc_type
                                 ,daily_waste_pct
                                 ,unit_cost_loc
                                 ,unit_retail_loc
                                 ,selling_unit_retail
                                 ,selling_uom
                                 ,multi_units
                                 ,multi_unit_retail
                                 ,multi_selling_uom
                                 ,item_loc_status
                                 ,taxable_ind
                                 ,ti
                                 ,hi
                                 ,meas_of_each
                                 ,meas_of_price
                                 ,uom_of_price
                                 ,primary_variant
                                 ,primary_supp
                                 ,primary_cntry
                                 ,primary_cost_pack
                                 ,local_item_desc
                                 ,local_short_desc
                                 ,receive_as_type
                                 ,store_price_ind
                                 ,uin_type
                                 ,uin_label
                                 ,ext_uin_ind
                                 ,capture_time
                                 ,source_method
                                 ,source_wh
                                 ,inbound_handling_days
                                 ,currency_code
                                 ,like_store
                                 ,default_to_children_ind
                                 ,class_vat_ind
                                 ,hier_level
                                 ,hier_num_value
                                 ,hier_char_value
                                 ,store_type
                                 ,org_unit_id
                                 ,source_wh_supp
                                 ,source_wh_org_unit_id
                                 ,unit_cost_sup
                                 ,child_from_input
                                 ,item_from_input_pack
                                 ,delivery_country_id
                                 ,lang
                                 ,delivery_country_localized_ind
                                 ,default_loc_ind
                                 ,default_po_cost
                                 ,ranged_ind
                                 ,costing_loc
                                 ,costing_loc_type)
                           SELECT input.process_id
                                 ,im.item
                                 ,im.item_parent
                                 ,im.item_grandparent
                                 ,im.item_desc
                                 ,im.short_desc
                                 ,im.dept
                                 ,im.class
                                 ,im.subclass
                                 ,im.item_level
                                 ,im.tran_level
                                 ,im.status
                                 ,im.waste_type
                                 ,im.waste_pct
                                 ,im.default_waste_pct
                                 ,im.sellable_ind
                                 ,im.orderable_ind
                                 ,im.pack_ind
                                 ,im.pack_type
                                 ,im.simple_pack_ind
                                 ,im.diff_1
                                 ,im.diff_2
                                 ,im.diff_3
                                 ,im.diff_4
                                 ,CASE
                                     WHEN im.pack_ind = 'Y' AND input.loc_type != 'S' AND input.receive_as_type IS NULL AND im.pack_type = 'V'
                                     THEN 'P'
                                     ELSE im.order_as_type
                                  END order_as_type
                                 ,im.contains_inner_ind
                                 ,im.store_ord_mult
                                 ,im.catch_weight_ind
                                 ,im.catch_weight_uom
                                 ,im.sale_type
                                 ,im.container_item
                                 ,im.item_xform_ind
                                 ,im.standard_uom
                                 ,im.uom_conv_factor
                                 ,input.loc
                                 ,input.loc_type
                                 ,input.daily_waste_pct
                                 ,input.unit_cost_loc
                                 ,NULL unit_retail_loc
                                 ,NULL selling_unit_retail
                                 ,NULL selling_uom
                                 ,NULL multi_units
                                 ,NULL multi_unit_retail
                                 ,NULL multi_selling_uom
                                 ,input.item_loc_status
                                 ,input.taxable_ind
                                 ,input.ti
                                 ,input.hi
                                 ,input.meas_of_each
                                 ,input.meas_of_price
                                 ,input.uom_of_price
                                 ,input.primary_variant
                                 ,input.primary_supp
                                 ,NULL                      --primary_cntry
                                 ,input.primary_cost_pack
                                 ,im.item_desc              --local_item_desc
                                 ,im.short_desc             --local_short_desc
                                 ,input.receive_as_type
                                 ,input.store_price_ind
                                 ,input.uin_type
                                 ,input.uin_label
                                 ,input.ext_uin_ind
                                 ,input.capture_time
                                 ,input.source_method
                                 ,input.source_wh
                                 ,input.inbound_handling_days
                                 ,input.currency_code
                                 ,input.like_store
                                 ,'N'                       --default_to_children_ind
                                 ,input.class_vat_ind
                                 ,input.hier_level
                                 ,input.hier_num_value
                                 ,input.hier_char_value
                                 ,input.store_type
                                 ,input.org_unit_id
                                 ,input.source_wh_supp
                                 ,input.source_wh_org_unit_id
                                 ,input.unit_cost_sup
                                 ,'N'                       --child_from_input
                                 ,'Y'                       --item_from_input_pack
                                 ,input.delivery_country_id
                                 ,input.lang
                                 ,input.delivery_country_localized_ind
                                 ,input.default_loc_ind
                                 ,input.default_po_cost
                                 ,input.ranged_ind
                                 ,input.costing_loc
                                 ,input.costing_loc_type
                             FROM nil_input_working input
                                 ,item_master       im
                                 ,packitem          pi
                            WHERE input.process_id                      = I_nil_process_id
                              AND input.pack_ind                        = 'Y'
                              AND NVL(input.child_from_input, 'N')      = 'N'
                              AND input.LIKE_STORE                     IS NULL
                              AND NVL(input.item_from_input_pack, 'N')  = 'N'
                              AND pi.pack_no                            = input.item
                              AND pi.item                               = im.item
                              AND im.pack_ind                           = 'Y';
   ---
   -- Remove duplicate entries. This is possible if a pack item and component is ranged on PO.
   MERGE INTO nil_input_working input
      USING (
             SELECT item
                   ,loc
                   ,ROW_NUMBER() OVER (PARTITION BY item
                                                   ,loc
                                           ORDER BY item_from_input_pack) keep
               FROM nil_input_working
              WHERE process_id = I_nil_process_id
             ) rec
         ON (    input.process_id = I_nil_process_id
             AND input.item       = rec.item
             AND input.loc        = rec.loc
             AND input.rowid      = rec.rowid
             AND rec.keep         > 1)
       WHEN MATCHED THEN
          UPDATE
             SET input.loc_type = 'X'
          DELETE
           WHERE input.loc_type = 'X';

   -- Remove records that already exists in item_loc
   DELETE
     FROM nil_input_working input
    WHERE input.process_id  = I_nil_process_id
      AND EXISTS (SELECT 'x'
                    FROM item_loc il
                   WHERE il.loc  = input.loc
                     AND il.item = input.item);

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END EXPLODE_ITEMS;
--------------------------------------------------------------------------------
FUNCTION CHECK_ITEM_COUNTRY(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.CHECK_ITEM_COUNTRY';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

   CURSOR C_CHECK_ITEM_COUNTRY IS
      SELECT SQL_LIB.CREATE_MSG('MISSING_ITM_COUNTRY_REC',
                                iw.item,
                                iw.delivery_country_id,
                                NULL)
        FROM nil_input_working iw
       WHERE iw.process_id                     = I_nil_process_id
         AND iw.delivery_country_localized_ind = 'Y'
         AND NOT EXISTS (SELECT 'x'
                           FROM item_country ic
                          WHERE ic.country_id = iw.delivery_country_id
                            AND ic.item       = iw.item);
BEGIN
   ---
   IF LP_system_options.default_tax_type NOT IN ('SVAT') THEN
      RETURN TRUE;
   END IF;
   ---
   OPEN  C_CHECK_ITEM_COUNTRY;
   FETCH C_CHECK_ITEM_COUNTRY INTO L_error_message;
   CLOSE C_CHECK_ITEM_COUNTRY;
   ---
   IF L_error_message IS NOT NULL THEN
      O_error_message := L_error_message;
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
   END IF;

   -- Add an item-country relationship for each unique delivery country
   MERGE INTO item_country ic
      USING (
             SELECT DISTINCT
                    item
                   ,delivery_country_id
               FROM nil_input_working
              WHERE process_id = I_nil_process_id
             ) input
         ON (    ic.item       = input.item
             AND ic.country_id = input.delivery_country_id)
       WHEN NOT MATCHED THEN
          INSERT (item
                 ,country_id)
          VALUES (input.item
                 ,input.delivery_country_id);
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      IF C_CHECK_ITEM_COUNTRY%ISOPEN THEN
         CLOSE C_CHECK_ITEM_COUNTRY;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END CHECK_ITEM_COUNTRY;
--------------------------------------------------------------------------------
FUNCTION CHECK_SUPPLIER(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_nil_process_id IN     NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.CHECK_SUPPLIER';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

   CURSOR CHECK_SUPPLIER IS
      SELECT CASE
                WHEN  item_status!='W' AND primary_supp IS NULL AND orderable_ind = 'Y'
                THEN SQL_LIB.CREATE_MSG('NO_PRIM_SUPP_ITEMLOC',
                                        input.item,
                                        loc,
                                        NULL)
                WHEN source_wh IS NULL AND source_method = 'W'
                THEN SQL_LIB.CREATE_MSG('SOURCE_WH_IS_NULL',
                                        loc,
                                        input.item,
                                        NULL)

                WHEN source_method = 'S' AND loc_type = 'S' AND primary_supp in (SELECT supplier
                                                                                   FROM sups
                                                                                  WHERE dsd_ind = 'N')
                     AND EXISTS (SELECT 1
                                   FROM deps d 
                                  WHERE d.dept = input.dept 
                                    AND d.purchase_type = 0)                                                              
                THEN SQL_LIB.CREATE_MSG('SUPP_SOURCE_NO_DSD',
                                        item,
                                        loc,
                                        NULL)
                ELSE NULL
             END err_msg
        FROM nil_input_working input
       WHERE input.process_id = I_nil_process_id
       ORDER BY err_msg;
BEGIN
   ---
   OPEN  CHECK_SUPPLIER;
   FETCH CHECK_SUPPLIER INTO L_error_message;
   CLOSE CHECK_SUPPLIER;
   ---
   IF L_error_message IS NOT NULL THEN
      O_error_message := L_error_message;
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
   END IF;
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      IF CHECK_SUPPLIER%ISOPEN THEN
         CLOSE CHECK_SUPPLIER;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END CHECK_SUPPLIER;
--------------------------------------------------------------------------------
FUNCTION GET_AV_WEIGHT(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.GET_AV_WEIGHT';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

   CURSOR C_CHECK IS
      SELECT CASE
                WHEN nilw.av_weight IS NULL
                THEN SQL_LIB.CREATE_MSG('NOMINAL_WGT_NOT_FOUND',
                                        nilw.item,
                                        NULL,
                                        NULL)
                WHEN 'MASS' != (
                                SELECT uom_class
                                  FROM uom_class
                                 WHERE uom = nilw.catch_weight_uom
                                )
                THEN SQL_LIB.CREATE_MSG('INV_CUOM_CLASS',
                                        nilw.catch_weight_uom,
                                        nilw.item,
                                        NULL)
             END err_msg
        FROM nil_input_working nilw
       WHERE nilw.process_id       = I_nil_process_id
         AND nilw.item_level       = nilw.tran_level
         AND nilw.standard_uom     = 'EA'
         AND nilw.catch_weight_ind = 'Y'
       ORDER BY err_msg;

BEGIN
   ---
   MERGE INTO nil_input_working nil
      USING (
             SELECT nilw.item
                   ,nilw.loc
                   ,(iscd.net_weight/isc.supp_pack_size) av_weight
                   ,iscd.weight_uom                      uom_iscd
                   ,CASE
                       WHEN nilw.catch_weight_uom IS NULL
                       THEN iscd.weight_uom
                       ELSE nilw.catch_weight_uom
                    END                                  catch_weight_uom
               FROM item_supp_country_dim iscd
                   ,item_supp_country     isc
                   ,nil_input_working     nilw
              WHERE nilw.process_id         = I_nil_process_id
                AND isc.item                = iscd.item
                AND isc.supplier            = iscd.supplier
                AND isc.origin_country_id   = iscd.origin_country
                AND iscd.dim_object         = 'CA'
                AND iscd.item               = nilw.item
                AND isc.primary_supp_ind    = 'Y'
                AND isc.primary_country_ind = 'Y'
                AND nilw.item_level         = nilw.tran_level
                AND nilw.standard_uom       = 'EA'
                AND nilw.catch_weight_ind   = 'Y'
             ) input
         ON (    nil.process_id = I_nil_process_id
             AND nil.item       = input.item
             AND nil.loc        = input.loc)
       WHEN MATCHED THEN
          UPDATE
             SET nil.av_weight        = input.av_weight
                ,nil.uom_iscd         = input.uom_iscd
                ,nil.catch_weight_uom = input.catch_weight_uom;
   ---
   --Check for issues
   OPEN  C_CHECK;
   FETCH C_CHECK INTO L_error_message;
   CLOSE C_CHECK;
   ---
   IF L_error_message IS NOT NULL THEN
      O_error_message := L_error_message;
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
   END IF;
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      IF C_CHECK%ISOPEN THEN
         CLOSE C_CHECK;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END GET_AV_WEIGHT;
--------------------------------------------------------------------------------
FUNCTION COMPLETE_ITEM_LOC_REC(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.COMPLETE_ITEM_LOC_REC';

BEGIN
   -- Get details from item_supp_country for missing info (country, ti, hi, costs)
   MERGE INTO nil_input_working nil
      USING (
             SELECT nilw.item
                   ,nilw.loc
                   ,isc.origin_country_id
                   ,NVL(nilw.ti, isc.ti)   ti
                   ,NVL(nilw.hi, isc.hi)   hi
                   ,isc.extended_base_cost
                   ,CASE
                       WHEN (
                                 (
                                     nilw.unit_cost_loc        IS NULL
                                  OR nilw.item_from_input_pack  = 'Y'
                                  )
                             AND (
                                     nilw.unit_cost_sup IS NULL
                                  OR nilw.store_type    != 'C'
                                  )
                             )
                       THEN isc.unit_cost
                       ELSE nilw.unit_cost_sup
                    END                    unit_cost_sup
                   ,CASE
                       WHEN nilw.store_type != 'C'
                       THEN isc.extended_base_cost
                       ELSE NULL
                    END                    wh_unit_cost
               FROM nil_input_working nilw
                   ,item_supp_country isc
              WHERE nilw.process_id = I_nil_process_id
                AND isc.item        = nilw.item
                AND isc.supplier    = nilw.primary_supp
                AND (
                        isc.origin_country_id = nilw.primary_cntry
                     OR (
                             isc.primary_country_ind  = 'Y'
                         AND nilw.primary_cntry      IS NULL
                         )
                     )
                AND (
                        nilw.pack_ind = 'N'
                     OR (
                             nilw.pack_ind      = 'Y'
                         AND nilw.orderable_ind = 'Y'
                         )
                     )
             ) input
         ON (    nil.process_id = I_nil_process_id
             AND nil.item       = input.item
             AND nil.loc        = input.loc)
       WHEN MATCHED THEN
          UPDATE
             SET nil.av_cost       = input.extended_base_cost
                ,nil.unit_cost_sup = input.unit_cost_sup
                ,nil.wh_unit_cost  = input.wh_unit_cost
                ,nil.primary_cntry = input.origin_country_id
                ,nil.ti            = input.ti
                ,nil.hi            = input.hi;

   -- Get item/loc info for items of packs or item/loc with missing information from a parent item
   MERGE INTO nil_input_working nil
      USING (
             SELECT nilw.item
                   ,nilw.loc
                   ,CASE
                       WHEN nilw.item_from_input_pack != 'Y' AND nilw.unit_retail_loc IS NULL
                       THEN il.unit_retail
                       ELSE nilw.unit_retail_loc
                    END unit_retail
                   ,CASE
                       WHEN nilw.item_from_input_pack != 'Y' AND nilw.selling_unit_retail IS NULL
                       THEN il.selling_unit_retail
                       ELSE nilw.selling_unit_retail
                    END selling_unit_retail
                   ,il.selling_uom
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.taxable_ind IS NOT NULL
                       THEN nilw.taxable_ind
                       ELSE il.taxable_ind
                    END taxable_ind
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.ti IS NOT NULL
                       THEN nilw.ti
                       ELSE il.ti
                    END ti
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.hi IS NOT NULL
                       THEN nilw.hi
                       ELSE il.hi
                    END hi
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.store_ord_mult IS NOT NULL
                       THEN nilw.store_ord_mult
                       ELSE il.store_ord_mult
                    END store_ord_mult
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.item_loc_status IS NOT NULL
                       THEN nilw.item_loc_status
                       ELSE il.status
                    END item_loc_status
                   ,il.daily_waste_pct
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.meas_of_each IS NOT NULL
                       THEN nilw.meas_of_each
                       ELSE il.meas_of_each
                    END meas_of_each
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.meas_of_price IS NOT NULL
                       THEN nilw.meas_of_price
                       ELSE il.meas_of_price
                    END meas_of_price
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.uom_of_price IS NOT NULL
                       THEN nilw.uom_of_price
                       ELSE il.uom_of_price
                    END uom_of_price
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.primary_variant IS NOT NULL
                       THEN nilw.primary_variant
                       ELSE il.primary_variant
                    END primary_variant
                   ,il.primary_cost_pack
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.source_method IS NOT NULL
                       THEN nilw.source_method
                       ELSE il.source_method
                    END source_method
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.source_wh IS NOT NULL
                       THEN nilw.source_wh
                       ELSE il.source_wh
                    END source_wh
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.uin_type IS NOT NULL
                       THEN nilw.uin_type
                       ELSE il.uin_type
                    END uin_type
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.uin_label IS NOT NULL
                       THEN nilw.uin_label
                       ELSE il.uin_label
                    END uin_label
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.capture_time IS NOT NULL
                       THEN nilw.capture_time
                       ELSE il.capture_time
                    END capture_time
                   ,CASE
                       WHEN nilw.item_from_input_pack = 'N' AND nilw.ext_uin_ind IS NOT NULL
                       THEN nilw.ext_uin_ind
                       ELSE il.ext_uin_ind
                    END ext_uin_ind
               FROM nil_input_working nilw
                   ,item_loc          il
              WHERE nilw.process_id   = I_nil_process_id
                AND nilw.item_parent IS NOT NULL
                AND il.item           = nilw.item_parent
                AND il.loc            = nilw.loc
                AND (
                        (
                             (
                                  nilw.like_store IS NULL
                              AND (
                                      nilw.ti              IS NULL
                                   OR nilw.hi              IS NULL
                                   OR nilw.store_ord_mult  IS NULL
                                   OR nilw.meas_of_each    IS NULL
                                   OR nilw.meas_of_price   IS NULL
                                   OR nilw.uom_of_price    IS NULL
                                   OR nilw.primary_variant IS NULL
                                   OR nilw.primary_supp    IS NULL
                                   OR nilw.primary_cntry   IS NULL
                                   OR (
                                           nilw.waste_type       = 'SP'
                                       AND nilw.daily_waste_pct IS NULL
                                       )
                                   )
                              )
                         AND nilw.item_level >= 2
                         )
                     OR nilw.item_from_input_pack = 'Y'
                     )
             ) input
         ON (    nil.process_id = I_nil_process_id
             AND nil.item       = input.item
             AND nil.loc        = input.loc)
       WHEN MATCHED THEN
          UPDATE
             SET nil.taxable_ind         = input.taxable_ind
                ,nil.ti                  = input.ti
                ,nil.hi                  = input.hi
                ,nil.store_ord_mult      = input.store_ord_mult
                ,nil.item_loc_status     = input.item_loc_status
                ,nil.daily_waste_pct     = input.daily_waste_pct
                ,nil.meas_of_each        = input.meas_of_each
                ,nil.meas_of_price       = input.meas_of_price
                ,nil.uom_of_price        = input.uom_of_price
                ,nil.primary_variant     = input.primary_variant
                ,nil.primary_cost_pack   = input.primary_cost_pack
                ,nil.source_method       = input.source_method
                ,nil.source_wh           = input.source_wh
                ,nil.uin_type            = input.uin_type
                ,nil.uin_label           = input.uin_label
                ,nil.capture_time        = input.capture_time
                ,nil.ext_uin_ind         = input.ext_uin_ind;

   -- If there are still fields that need defaulting, do it here.
   UPDATE nil_input_working input
      SET taxable_ind       = NVL(taxable_ind, DECODE(loc_type, 'S', 'Y', 'N'))
         ,item_loc_status   = NVL(item_loc_status, 'A')
         ,local_item_desc   = NVL(local_item_desc, item_desc)
         ,local_short_desc  = NVL(local_short_desc, item_short_desc)
         ,receive_as_type   = CASE
                                 WHEN input.pack_ind = 'N' OR input.loc_type = 'S'
                                 THEN NULL
                                 WHEN input.receive_as_type IS NOT NULL
                                 THEN input.receive_as_type
                                 ELSE NVL(  (
                                             SELECT i.receive_as_type
                                               FROM item_loc i
                                                   ,wh       w1
                                                   ,wh       w2
                                              WHERE i.item         = input.item
                                                AND i.loc          = w1.wh
                                                AND w2.wh          = input.loc
                                                AND w1.physical_wh = w2.physical_wh
                                                AND rownum         = 1
                                             )
                                          , input.order_as_type
                                          )
                              END
         ,default_waste_pct = CASE
                                 WHEN loc_type = 'S' AND waste_type = 'SP'
                                 THEN CASE
                                         WHEN daily_waste_pct IS NOT NULL AND item_from_input_pack != 'Y'
                                         THEN daily_waste_pct
                                         ELSE default_waste_pct
                                      END
                                 ELSE NULL
                              END
         ,primary_variant   = CASE
                                 WHEN item_level = tran_level
                                 THEN NULL
                                 ELSE primary_variant
                              END
         ,primary_cost_pack = CASE
                                 WHEN item_level != tran_level
                                 THEN NULL
                                 ELSE primary_cost_pack
                              END
         ,uin_type          = CASE
                                 WHEN loc_type = 'S'
                                 THEN uin_type
                                 ELSE NULL
                              END
         ,uin_label         = CASE
                                 WHEN loc_type = 'S'
                                 THEN uin_label
                                 ELSE NULL
                              END
         ,capture_time      = CASE
                                 WHEN loc_type = 'S'
                                 THEN capture_time
                                 ELSE NULL
                              END
         ,ext_uin_ind       = CASE
                                 WHEN loc_type = 'S'
                                 THEN NVL(ext_uin_ind, 'N')
                                 ELSE 'N'
                              END
    WHERE process_id = I_nil_process_id;
   ---
   IF GET_RETAIL(O_error_message,
                 I_nil_process_id) = FALSE THEN
      RETURN FALSE;
   END IF;
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END COMPLETE_ITEM_LOC_REC;
--------------------------------------------------------------------------------
FUNCTION GET_RETAIL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.GET_RETAIL';

   L_unit_cost_sup           ITEM_LOC_SOH.UNIT_COST%TYPE       := NULL;
   L_unit_retail_loc         ITEM_LOC.UNIT_RETAIL%TYPE         := NULL;
   L_selling_retail_loc      ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := NULL;
   L_selling_uom             ITEM_LOC.SELLING_UOM%TYPE         := NULL;
   L_multi_units             ITEM_LOC.MULTI_UNITS%TYPE         := NULL;
   L_multi_unit_retail_loc   ITEM_LOC.MULTI_UNIT_RETAIL%TYPE   := NULL;
   L_multi_selling_uom       ITEM_LOC.MULTI_SELLING_UOM%TYPE   := NULL;

   CURSOR C_XFORM_SUP_COST IS
      SELECT item
            ,loc
        FROM nil_input_working input
       WHERE input.process_id      = I_nil_process_id
         AND (
                 input.item_from_input_pack = 'Y'
              OR input.unit_retail_loc     IS NULL
              OR input.selling_unit_retail IS NULL
              OR input.selling_uom         IS NULL
              )
         AND input.sellable_ind    = 'Y'
         AND input.loc_type       IN ('S', 'W', 'I')
         AND input.item_xform_ind  = 'Y';

   CURSOR C_CALC_RETAIL IS
              SELECT input.item
                    ,input.item_parent
                    ,input.loc
                    ,input.loc_type
                    ,input.dept
                    ,input.class
                    ,input.subclass
                    ,input.standard_uom
                    ,input.pack_ind
                    ,input.sellable_ind
                    ,input.unit_cost_sup
                    ,input.currency_code
                    ,input.supp_currency_code
                    ,input.primary_supp
                    ,input.primary_cntry
                    ,input.unit_retail_loc
                    ,input.selling_unit_retail
                    ,input.selling_uom
                    ,input.multi_units
                    ,input.multi_unit_retail
                    ,input.multi_selling_uom
                    ,input.item_from_input_pack
                FROM nil_input_working input
               WHERE input.process_id     = I_nil_process_id
                 AND (
                         input.unit_retail_loc     IS NULL
                      OR input.selling_unit_retail IS NULL
                      OR input.selling_uom         IS NULL
                      OR input.item_from_input_pack = 'Y'
                      )
                  AND input.loc_type     IN ('S', 'W', 'I')
                  AND input.sellable_ind  = 'Y';

BEGIN
   ---
   FOR xfrom_rec IN C_XFORM_SUP_COST LOOP
      ---
      IF ITEM_XFORM_SQL.CALCULATE_COST(O_error_message,
                                       xfrom_rec.item,
                                       xfrom_rec.loc,
                                       L_unit_cost_sup) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      UPDATE nil_input_working
         SET unit_cost_sup = L_unit_cost_sup
       WHERE process_id = I_nil_process_id
         AND item       = xfrom_rec.item
         AND loc        = xfrom_rec.loc;
      ---
   END LOOP;
   ---
   FOR get_retail_rec IN C_CALC_RETAIL LOOP
      -- The GET_NEW_ITEM_LOC_RETAIL function only fetches the unit cost if the Currency is NULL too.
      IF get_retail_rec.unit_cost_sup IS NULL THEN
         ---
         IF PM_RETAIL_API_SQL.GET_NEW_ITEM_LOC_RETAIL(O_error_message,
                                                      L_selling_retail_loc,
                                                      L_selling_uom,
                                                      L_multi_units,
                                                      L_multi_unit_retail_loc,
                                                      L_multi_selling_uom,
                                                      get_retail_rec.item,
                                                      get_retail_rec.dept,
                                                      get_retail_rec.class,
                                                      get_retail_rec.subclass,
                                                      get_retail_rec.loc,
                                                      get_retail_rec.loc_type,
                                                      get_retail_rec.unit_cost_sup,
                                                      NULL) = FALSE THEN
            RETURN FALSE;
         END IF;
      ELSE
         ---
         IF PM_RETAIL_API_SQL.GET_NEW_ITEM_LOC_RETAIL(O_error_message,
                                                      L_selling_retail_loc,
                                                      L_selling_uom,
                                                      L_multi_units,
                                                      L_multi_unit_retail_loc,
                                                      L_multi_selling_uom,
                                                      get_retail_rec.item,
                                                      get_retail_rec.dept,
                                                      get_retail_rec.class,
                                                      get_retail_rec.subclass,
                                                      get_retail_rec.loc,
                                                      get_retail_rec.loc_type,
                                                      get_retail_rec.unit_cost_sup,
                                                      NVL(get_retail_rec.supp_currency_code, get_retail_rec.currency_code)
                                                      ) = FALSE THEN
            RETURN FALSE;
         END IF;
      END IF;
      ---
      IF L_selling_uom IS NOT NULL THEN
         ---
         IF get_retail_rec.standard_uom = L_selling_uom THEN
            L_unit_retail_loc := L_selling_retail_loc;
         ELSE
            ---
            IF UOM_SQL.CONVERT(O_error_message,
                               L_unit_retail_loc,
                               L_selling_uom,
                               L_selling_retail_loc,
                               get_retail_rec.standard_uom,
                               get_retail_rec.item,
                               get_retail_rec.primary_supp,
                               get_retail_rec.primary_cntry) = FALSE THEN
                RETURN FALSE;
             END IF;
             ---
         END IF;
      ELSE -- selling uom IS NULL
         L_unit_retail_loc    := 0.00;
         L_selling_retail_loc := 0.00;
         L_selling_uom        := get_retail_rec.standard_uom;
      END IF;
      ---
      IF get_retail_rec.item_from_input_pack != 'Y' THEN
         --
         IF get_retail_rec.selling_uom IS NOT NULL THEN
            L_selling_uom := get_retail_rec.selling_uom;
         END IF;
      END IF;
      ---
      L_multi_units           := NVL(get_retail_rec.multi_units, L_multi_units);
      L_multi_unit_retail_loc := NVL(get_retail_rec.multi_unit_retail, L_multi_unit_retail_loc);
      L_multi_selling_uom     := NVL(get_retail_rec.multi_selling_uom, L_multi_selling_uom);
      ---
      UPDATE nil_input_working
         SET unit_retail_loc     = L_unit_retail_loc
            ,selling_unit_retail = L_selling_retail_loc
            ,selling_uom         = L_selling_uom
            ,multi_units         = L_multi_units
            ,multi_unit_retail   = L_multi_unit_retail_loc
            ,multi_selling_uom   = L_multi_selling_uom
       WHERE process_id                      = I_nil_process_id
         AND item                            = get_retail_rec.item
         AND loc                             = get_retail_rec.loc
         AND loc_type                        = get_retail_rec.loc_type
         AND sellable_ind                    = 'Y';
      ---
   END LOOP;
   ---
   UPDATE nil_input_working
      SET unit_retail_loc     = DECODE(pack_ind, 'Y', NULL, unit_retail_loc)
         ,selling_unit_retail = NULL
         ,selling_uom         = NULL
         ,multi_units         = NULL
         ,multi_unit_retail   = NULL
         ,multi_selling_uom   = NULL
    WHERE process_id   = I_nil_process_id
      AND sellable_ind = 'N';
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      IF C_XFORM_SUP_COST%ISOPEN THEN
         CLOSE C_XFORM_SUP_COST;
      END iF;
      ---
      IF C_CALC_RETAIL%ISOPEN THEN
         CLOSE C_CALC_RETAIL;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END GET_RETAIL;
------------------------------------------------------------------------
FUNCTION CONVERT_CURRENCIES(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.CONVERT_CURRENCIES';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

   L_exchange_type   CURRENCY_RATES.EXCHANGE_TYPE%TYPE := NULL;

   CURSOR C_GET_EXG_ERROR_LOC_TO_SUP IS
      SELECT SQL_LIB.CREATE_MSG('EXCHANGE_RATE_NOT_EXIST',
                                'From: '|| nilw.currency_code,
                                ' To: '|| nilw.supp_currency_code,
                                NULL) errmsg
        FROM nil_input_working nilw
       WHERE nilw.process_id     = I_nil_process_id
         AND nilw.item_level     = nilw.tran_level
         AND nilw.currency_code != nilw.supp_currency_code
         AND nilw.primary_supp  IS NOT NULL
         AND nilw.unit_cost_sup IS NULL
         AND (
                 nilw.pack_ind = 'N'
              OR (
                      nilw.pack_ind      = 'Y'
                  AND nilw.orderable_ind = 'Y'
                  )
              )
         AND NOT EXISTS (SELECT 'x'
                           FROM mv_currency_conversion_rates mccr
                          WHERE mccr.from_currency   = nilw.currency_code
                            AND mccr.to_currency     = nilw.supp_currency_code
                            AND mccr.exchange_type   = L_exchange_type
                            AND mccr.effective_date <= LP_date);
   ---
   CURSOR C_GET_EXG_ERROR_SUP_TO_LOC IS
      SELECT SQL_LIB.CREATE_MSG('EXCHANGE_RATE_NOT_EXIST',
                                'From: '|| nilw.supp_currency_code,
                                ' To: '|| nilw.currency_code,
                                NULL) errmsg
        FROM nil_input_working nilw
       WHERE nilw.process_id     = I_nil_process_id
         AND nilw.item_level     = nilw.tran_level
         AND nilw.currency_code != nilw.supp_currency_code
         AND nilw.primary_supp  IS NOT NULL
         AND (
                 nilw.pack_ind = 'N'
              OR (
                      nilw.pack_ind      = 'Y'
                  AND nilw.orderable_ind = 'Y'
                  )
              )
         AND NOT EXISTS (SELECT 'x'
                           FROM mv_currency_conversion_rates mccr
                          WHERE mccr.from_currency   = nilw.supp_currency_code
                            AND mccr.to_currency     = nilw.currency_code
                            AND mccr.exchange_type   = L_exchange_type
                            AND mccr.effective_date <= LP_date);

BEGIN
   ---
   IF LP_system_options.consolidation_ind = 'Y' THEN
      L_exchange_type := 'C';
   ELSE
      L_exchange_type := 'O';
   END IF;
   ---
   OPEN C_GET_EXG_ERROR_LOC_TO_SUP;
   FETCH C_GET_EXG_ERROR_LOC_TO_SUP INTO L_error_message;
   CLOSE C_GET_EXG_ERROR_LOC_TO_SUP;
   ---
   IF L_error_message IS NOT NULL THEN
      O_error_message := L_error_message;
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
   END IF;
   ---
   MERGE INTO nil_input_working nil
      USING (
             SELECT nilw.item
                   ,nilw.loc
                   ,nilw.loc_type
                   ,ROUND( (nilw.unit_cost_loc * mccr.exchange_rate), c.currency_cost_dec) unit_cost
                   ,ROW_NUMBER() OVER (PARTITION BY nilw.item
                                                   ,nilw.loc
                                                   ,nilw.loc_type
                                           ORDER BY mccr.effective_date DESC)              row_num
               FROM nil_input_working            nilw
                   ,mv_currency_conversion_rates mccr
                   ,currencies c
              WHERE nilw.process_id          = I_nil_process_id
                AND nilw.item_level          = nilw.tran_level
                AND nilw.currency_code      != nilw.supp_currency_code
                AND nilw.primary_supp        IS NOT NULL
                AND nilw.unit_cost_sup       IS NULL
                AND (
                        nilw.pack_ind = 'N'
                     OR (    nilw.pack_ind      = 'Y'
                         AND nilw.orderable_ind = 'Y'
                         )
                     )
                AND mccr.from_currency       = nilw.currency_code
                AND mccr.to_currency         = nilw.supp_currency_code
                AND mccr.exchange_type       = L_exchange_type
                AND mccr.effective_date     <= LP_date
                AND nilw.supp_currency_code  = c.currency_code) input
         ON (    nil.process_id = I_nil_process_id
             AND input.item     = nil.item
             AND input.loc      = nil.loc
             AND input.loc_type = nil.loc_type
             AND input.row_num  = 1)
       WHEN MATCHED THEN
          UPDATE
             SET nil.unit_cost_sup = input.unit_cost;
   ---
   OPEN C_GET_EXG_ERROR_SUP_TO_LOC;
   FETCH C_GET_EXG_ERROR_SUP_TO_LOC INTO L_error_message;
   CLOSE C_GET_EXG_ERROR_SUP_TO_LOC;

   IF L_error_message IS NOT NULL THEN
      O_error_message := L_error_message;
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
   END IF;
   ---
   MERGE INTO nil_input_working nil
      USING (SELECT nilw.item
                   ,nilw.loc
                   ,nilw.loc_type
                   ,CASE
                       WHEN LP_system_options.default_tax_type IN ('GTAX', 'SVAT')
                       THEN ROUND( (nilw.unit_cost_loc * mccr.exchange_rate), c.currency_cost_dec)
                       WHEN nilw.unit_cost_loc IS NULL OR nilw.item_from_input_pack = 'Y'
                       THEN ROUND( (nilw.unit_cost_sup * mccr.exchange_rate), c.currency_cost_dec)
                       ELSE nilw.unit_cost_loc
                    END                                                              unit_cost
                   ,ROUND( (nilw.av_cost * mccr.exchange_rate), c.currency_cost_dec) av_cost
                   ,ROW_NUMBER() OVER (PARTITION BY nilw.item
                                                   ,nilw.loc
                                                   ,nilw.loc_type
                                           ORDER BY mccr.effective_date DESC)        row_num
               FROM nil_input_working            nilw
                   ,mv_currency_conversion_rates mccr
                   ,currencies                   c
              WHERE nilw.process_id      = I_nil_process_id
                AND nilw.item_level      = nilw.tran_level
                AND nilw.currency_code  != nilw.supp_currency_code
                AND nilw.primary_supp   IS NOT NULL
                AND (
                        nilw.pack_ind = 'N'
                     OR (
                             nilw.pack_ind       = 'Y'
                         AND nilw.orderable_ind  = 'Y'
                         )
                     )
                AND mccr.from_currency   = nilw.supp_currency_code
                AND mccr.to_currency     = nilw.currency_code
                AND mccr.exchange_type   = L_exchange_type
                AND mccr.effective_date <= LP_date
                AND nilw.currency_code   = c.currency_code) input
         ON (    nil.process_id = I_nil_process_id
             AND input.item     = nil.item
             AND input.loc      = nil.loc
             AND input.loc_type = nil.loc_type
             AND input.row_num  = 1)
       WHEN MATCHED THEN
          UPDATE
             SET nil.unit_cost_loc = input.unit_cost
                ,nil.av_cost       = input.av_cost;
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      IF C_GET_EXG_ERROR_LOC_TO_SUP%ISOPEN THEN
         CLOSE C_GET_EXG_ERROR_LOC_TO_SUP;
      END iF;
      ---
      IF C_GET_EXG_ERROR_SUP_TO_LOC%ISOPEN THEN
         CLOSE C_GET_EXG_ERROR_SUP_TO_LOC;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END CONVERT_CURRENCIES;
--------------------------------------------------------------------------------
FUNCTION COMPUTE_ITEM_COST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           IO_comp_item_cost_tbl   IN OUT   OBJ_COMP_ITEM_COST_TBL,
                           I_nil_process_id        IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.COMPUTE_ITEM_COST';

BEGIN
   ---
   SELECT OBJ_COMP_ITEM_COST_REC( nil.item                         -- I_item
                                 ,NVL(ich.nic_static_ind, 'N')     -- I_nic_static_ind
                                 ,isc.supplier                     -- I_supplier
                                 ,nil.loc                          -- I_location
                                 ,nil.loc_type                     -- I_loc_type
                                 ,LP_date                          -- I_effective_date
                                 ,'ITEMSUPPCTRYLOC'                -- I_calling_form
                                 ,isc.origin_country_id            -- I_origin_country_id
                                 ,nil.delivery_country_id          -- I_delivery_country_id
                                 ,NVL(ich.prim_dlvy_ctry_ind, 'N') -- I_prim_dlvy_ctry_ind
                                 ,CASE
                                     WHEN LP_system_options.default_tax_type = 'GTAX' 
                                     THEN nil.DEFAULT_LOC_IND
                                     ELSE 'Y'
                                 END                               -- I_update_itemcost_ind
                                 ,'N'                              -- I_update_itemcost_child_ind
                                 ,NVL(ich.nic_static_ind, 'N')     -- I_item_cost_tax_incl_ind,
                                 ,CASE
                                     WHEN LP_system_options.default_tax_type = 'GTAX'
                                     THEN ich.base_cost
                                     ELSE NULL
                                  END                              -- O_base_cost
                                 ,CASE
                                     WHEN LP_system_options.default_tax_type = 'GTAX'
                                     THEN ich.extended_base_cost
                                     ELSE NULL
                                  END                              -- O_extended_base_cost
                                 ,CASE
                                     WHEN LP_system_options.default_tax_type = 'GTAX'
                                     THEN ich.inclusive_cost
                                     ELSE NULL
                                  END                              -- O_inclusive_cost
                                 ,CASE
                                     WHEN LP_system_options.default_tax_type = 'GTAX'
                                     THEN ich.negotiated_item_cost
                                     ELSE NULL
                                  END                              -- O_negotiated_item_cost
                                 ,NULL                             -- svat_tax_rate
                                 ,NULL                             -- tax_loc_type
                                 ,nil.pack_ind                     -- pack_ind
                                 ,nil.pack_type                    -- pack_type
                                 ,nil.dept                         -- dept
                                 ,NULL                             -- prim_supp_currency_code
                                 ,NULL                             -- loc_prim_country
                                 ,NULL                             -- loc_prim_country_tax_incl_ind
                                 ,NULL                             -- gtax_total_tax_amount
                                 ,NULL                             -- gtax_total_tax_amount_nic
                                 ,NULL)                            -- gtax_total_recover_amount
     BULK COLLECT INTO IO_comp_item_cost_tbl
     FROM item_cost_head    ich
         ,item_supp_country isc
         ,nil_input_working nil
    WHERE nil.process_id           = I_nil_process_id
      AND nil.item                 = isc.item
      AND isc.item                 = ich.item(+)
      AND isc.supplier             = ich.supplier(+)
      AND isc.origin_country_id    = ich.origin_country_id(+)
      AND nil.delivery_country_id  = NVL(ich.delivery_country_id(+), nil.delivery_country_id)
      AND nil.primary_supp        IS NOT NULL
      AND (
              nil.pack_ind = 'N'
           OR (
                   nil.pack_ind      = 'Y'
               AND nil.orderable_ind = 'Y'
               )
           );
   ---
   IF LP_system_options.default_tax_type = 'GTAX' AND IO_comp_item_cost_tbl IS NOT NULL AND IO_comp_item_cost_tbl.COUNT > 0 THEN
      ---
      IF ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK(O_error_message,
                                              IO_comp_item_cost_tbl) = FALSE THEN
         RETURN FALSE;
      END IF;

      -- Update cost
      MERGE INTO nil_input_working nil
      USING (
             SELECT nilw.item
                   ,nilw.loc
                   ,nilw.loc_type
                   ,CASE
                       WHEN nilw.pack_ind = 'Y'
                       THEN NULL
                       ELSE
                          CASE
                             WHEN LP_system_options.default_tax_type IN ('GTAX')
                             THEN
                                 input_cost_tbl.O_extended_base_cost
                             ELSE nilw.av_cost
                          END
                    END av_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type IN ('GTAX')
                       THEN DECODE(nilw.default_po_cost, 'BC' , NVL(input_cost_tbl.O_base_cost, 0),
                                                         'NIC', NVL(input_cost_tbl.O_negotiated_item_cost, 0))
                       ELSE nilw.unit_cost_loc
                    END unit_cost
               FROM nil_input_working                                       nilw
                   ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) input_cost_tbl
              WHERE nilw.process_id    = I_nil_process_id
                AND nilw.item          = input_cost_tbl.I_item
                AND nilw.loc           = input_cost_tbl.I_location
                AND nilw.primary_supp  = input_cost_tbl.I_supplier
                AND nilw.primary_cntry = input_cost_tbl.I_origin_country_id
             ) input
         ON (    nil.process_id = I_nil_process_id
             AND input.item     = nil.item
             AND input.loc      = nil.loc
             AND input.loc_type = nil.loc_type)
       WHEN MATCHED THEN
          UPDATE
             SET nil.unit_cost_loc = input.unit_cost
                ,nil.av_cost       = input.av_cost;
    
   else 
        -- update the unit_cost and avg_cost for SVAT and SALES
        MERGE INTO nil_input_working nil
        USING
             ( SELECT nilw.item
                     ,nilw.loc
                     ,nilw.loc_type
                     ,CASE
                         WHEN nilw.pack_ind = 'Y'
                         THEN NULL
                         ELSE isc.unit_cost
                      END avg_cost
                     ,isc.unit_cost
                FROM nil_input_working nilw,
                     item_supp_country isc
               WHERE nilw.process_id    = I_nil_process_id
                 AND nilw.item          = isc.item
                 AND nilw.primary_supp  = isc.supplier
                 AND nilw.primary_cntry = isc.origin_country_id
              ) input
        ON (    nil.process_id = I_nil_process_id
            AND input.item     = nil.item
            AND input.loc      = nil.loc
            AND input.loc_type = nil.loc_type )
        WHEN MATCHED THEN 
            UPDATE 
              SET nil.unit_cost_loc = input.unit_cost
                 ,nil.av_cost       = input.avg_cost;

   END IF;
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END COMPUTE_ITEM_COST;
--------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_TAX(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.CALC_RETAIL_TAX';

   L_tax_calc_tbl   OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();

BEGIN
   ---
   SELECT OBJ_TAX_CALC_REC( nil.item           -- I_item
                           ,nil.pack_ind        -- I_pack_ind
                           ,nil.loc             -- I_from_entity
                           ,CASE
                               WHEN nil.loc_type IN ('W', 'I')
                               THEN 'WH'
                               ELSE 'ST'
                            END                 -- I_from_entity_type
                           ,NULL                -- I_to_entity
                           ,NULL                -- I_to_entity_type
                           ,LP_date             -- I_effective_from_date
                           ,nil.unit_retail_loc -- I_amount
                           ,nil.currency_code   -- I_amount_curr
                           ,NULL                -- I_amount_tax_incl_ind
                           ,NULL                -- I_origin_country_id
                           ,NULL                -- O_cum_tax_pct
                           ,NULL                -- O_cum_tax_value
                           ,NULL                -- O_total_tax_amount
                           ,NULL                -- O_total_tax_amount_curr
                           ,NULL                -- O_total_recover_amount
                           ,NULL                -- O_total_recover_amount_curr
                           ,NULL                -- O_tax_detail_tbl
                           ,'ADDITEM'           -- I_tran_type
                           ,get_vdate()         -- I_tran_date
                           ,I_nil_process_id    -- I_tran_id
                           ,'R')                -- I_cost_retail_ind
     BULK COLLECT INTO L_tax_calc_tbl
     FROM nil_input_working nil
    WHERE nil.process_id    = I_nil_process_id
      AND nil.sellable_ind  = 'Y'
      AND nil.loc_type     IN ('W', 'I', 'S');

   IF L_tax_calc_tbl IS NOT NULL AND L_tax_calc_tbl.COUNT > 0 THEN
      ---
      IF TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                                 L_tax_calc_tbl,
                                 TAX_SQL.LP_CALL_TYPE_NIL) = FALSE THEN
         RETURN FALSE;
      END IF;
   END IF;
   ---

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END CALC_RETAIL_TAX;
--------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 IO_comp_item_cost_tbl   IN OUT   OBJ_COMP_ITEM_COST_TBL,
                 I_nil_process_id        IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.PERSIST';

BEGIN

   INSERT INTO item_loc (item
                        ,loc
                        ,item_parent
                        ,item_grandparent
                        ,loc_type
                        ,unit_retail
                        ,regular_unit_retail
                        ,multi_units
                        ,multi_unit_retail
                        ,multi_selling_uom
                        ,selling_unit_retail
                        ,selling_uom
                        ,promo_retail
                        ,promo_selling_retail
                        ,promo_selling_uom
                        ,clear_ind
                        ,taxable_ind
                        ,local_item_desc
                        ,local_short_desc
                        ,ti
                        ,hi
                        ,store_ord_mult
                        ,status
                        ,status_update_date
                        ,daily_waste_pct
                        ,meas_of_each
                        ,meas_of_price
                        ,uom_of_price
                        ,primary_variant
                        ,primary_supp
                        ,primary_cntry
                        ,primary_cost_pack
                        ,receive_as_type
                        ,create_datetime
                        ,last_update_datetime
                        ,last_update_id
                        ,inbound_handling_days
                        ,store_price_ind
                        ,source_method
                        ,source_wh
                        ,rpm_ind
                        ,uin_type
                        ,uin_label
                        ,capture_time
                        ,ext_uin_ind
                        ,ranged_ind
                        ,costing_loc
                        ,costing_loc_type
                        ,create_id)
                 SELECT  input.item
                        ,input.loc
                        ,input.item_parent
                        ,input.item_grandparent
                        ,input.loc_type
                        ,input.unit_retail_loc
                        ,input.unit_retail_loc
                        ,input.multi_units
                        ,input.multi_unit_retail
                        ,input.multi_selling_uom
                        ,input.selling_unit_retail
                        ,input.selling_uom
                        ,NULL
                        ,NULL
                        ,NULL
                        ,'N'
                        ,input.taxable_ind
                        ,input.local_item_desc
                        ,input.local_short_desc
                        ,input.ti
                        ,input.hi
                        ,input.store_ord_mult
                        ,input.item_loc_status
                        ,LP_date
                        ,input.default_waste_pct
                        ,input.meas_of_each
                        ,input.meas_of_price
                        ,input.uom_of_price
                        ,input.primary_variant
                        ,input.primary_supp
                        ,input.primary_cntry
                        ,input.primary_cost_pack
                        ,input.receive_as_type
                        ,SYSDATE
                        ,SYSDATE
                        ,LP_user_id
                        ,input.inbound_handling_days
                        ,NVL(input.store_price_ind, 'N')
                        ,input.source_method
                        ,input.source_wh
                        ,'N'
                        ,input.uin_type
                        ,input.uin_label
                        ,input.capture_time
                        ,input.ext_uin_ind
                        ,NVL(input.ranged_ind, 'N')
                        ,input.costing_loc
                        ,input.costing_loc_type
                        ,LP_user_id
                    FROM nil_input_working input
                   WHERE input.process_id = I_nil_process_id
                     and not exists(select 'x'
                                      from item_loc il
                                     where il.item = input.item
                                       and il.loc  = input.loc
                                       and rownum = 1);
   ---
   IF sql%rowcount >0 THEN
      INSERT INTO item_loc_soh (item
                               ,item_parent
                               ,item_grandparent
                               ,loc
                               ,loc_type
                               ,av_cost
                               ,unit_cost
                               ,stock_on_hand
                               ,soh_update_datetime
                               ,last_hist_export_date
                               ,in_transit_qty
                               ,pack_comp_intran
                               ,pack_comp_soh
                               ,tsf_reserved_qty
                               ,pack_comp_resv
                               ,tsf_expected_qty
                               ,pack_comp_exp
                               ,rtv_qty
                               ,non_sellable_qty
                               ,customer_resv
                               ,customer_backorder
                               ,pack_comp_cust_resv
                               ,pack_comp_cust_back
                               ,create_datetime
                               ,last_update_datetime
                               ,last_update_id
                               ,first_received
                               ,last_received
                               ,qty_received
                               ,first_sold
                               ,last_sold
                               ,qty_sold
                               ,primary_supp
                               ,primary_cntry
                               ,average_weight
                               ,pack_comp_non_sellable
                               ,create_id)
                         SELECT input.item
                               ,input.item_parent
                               ,input.item_grandparent
                               ,input.loc
                               ,input.loc_type
                               ,input.av_cost
                               ,input.unit_cost_loc
                               ,0
                               ,NULL
                               ,NULL
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,0
                               ,SYSDATE
                               ,SYSDATE
                               ,LP_user_id
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,input.primary_supp
                               ,input.primary_cntry
                               ,input.av_weight
                               ,0
                               ,LP_user_id
                          FROM nil_input_working input
                         WHERE input.process_id = I_nil_process_id
                           AND input.item_level = input.tran_level;
      ---
      INSERT INTO price_hist (tran_type
                             ,reason
                             ,event
                             ,item
                             ,loc
                             ,loc_type
                             ,unit_cost
                             ,unit_retail
                             ,selling_unit_retail
                             ,selling_uom
                             ,action_date
                             ,multi_units
                             ,multi_unit_retail
                             ,multi_selling_uom
                             ,post_date)
                       SELECT 0
                             ,0
                             ,NULL
                             ,input.item
                             ,input.loc
                             ,input.loc_type
                             ,input.unit_cost_loc
                             ,input.unit_retail_loc
                             ,input.selling_unit_retail
                             ,input.selling_uom
                             ,LP_date
                             ,input.multi_units
                             ,input.multi_unit_retail
                             ,input.multi_selling_uom
                             ,SYSDATE
                        FROM nil_input_working input
                       WHERE input.process_id  = I_nil_process_id
                         AND input.item_status = 'A';
      ---
      IF PERSIST_ITEM_LOC_TRAITS(O_error_message,
                                 I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      IF IO_comp_item_cost_tbl IS NOT NULL AND IO_comp_item_cost_tbl.COUNT > 0 THEN
         ---
         IF PERSIST_ITEM_SUPP_COUNTRY_LOC(O_error_message,
                                          IO_comp_item_cost_tbl,
                                          I_nil_process_id) = FALSE THEN
            RETURN FALSE;
         END IF;
      END IF;
      ---
   END If; --rowcount
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END PERSIST;
------------------------------------------------------------------------
FUNCTION PERSIST_ITEM_LOC_TRAITS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.PERSIST_ITEM_LOC_TRAITS';

BEGIN
   --
   INSERT INTO item_loc_traits (item
                              ,loc
                              ,launch_date
                              ,qty_key_options
                              ,manual_price_entry
                              ,deposit_code
                              ,food_stamp_ind
                              ,wic_ind
                              ,proportional_tare_pct
                              ,fixed_tare_value
                              ,fixed_tare_uom
                              ,reward_eligible_ind
                              ,natl_brand_comp_item
                              ,return_policy
                              ,stop_sale_ind
                              ,elect_mtk_clubs
                              ,report_code
                              ,req_shelf_life_on_SELECTion
                              ,req_shelf_life_on_receipt
                              ,ib_shelf_life
                              ,store_reorderable_ind
                              ,rack_size
                              ,full_pallet_item
                              ,in_store_market_basket
                              ,storage_location
                              ,alt_storage_location
                              ,returnable_ind
                              ,refundable_ind
                              ,back_order_ind
                              ,create_datetime
                              ,last_update_id
                              ,last_update_datetime
                              ,create_id)
                        SELECT input.item
                              ,input.loc
                              ,launch_date
                              ,qty_key_options
                              ,manual_price_entry
                              ,deposit_code
                              ,food_stamp_ind
                              ,wic_ind
                              ,proportional_tare_pct
                              ,fixed_tare_value
                              ,fixed_tare_uom
                              ,reward_eligible_ind
                              ,natl_brand_comp_item
                              ,return_policy
                              ,stop_sale_ind
                              ,elect_mtk_clubs
                              ,report_code
                              ,req_shelf_life_on_SELECTion
                              ,req_shelf_life_on_receipt
                              ,ib_shelf_life
                              ,store_reorderable_ind
                              ,rack_size
                              ,full_pallet_item
                              ,in_store_market_basket
                              ,storage_location
                              ,alt_storage_location
                              ,returnable_ind
                              ,refundable_ind
                              ,back_order_ind
                              ,SYSDATE
                              ,LP_user_id
                              ,SYSDATE
                              ,LP_user_id
                          FROM item_loc_traits   ilt
                              ,nil_input_working input
                         WHERE input.process_id    = I_nil_process_id
                           AND input.item          = ilt.item
                           AND input.like_store    = ilt.loc
                           AND input.loc_type      = 'S'
                           AND input.item_status   = 'A'
                           AND (   input.sellable_ind = 'Y'
                                OR input.pack_ind     = 'N');
   ---
   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END PERSIST_ITEM_LOC_TRAITS;
------------------------------------------------------------------------
FUNCTION PERSIST_ITEM_SUPP_COUNTRY_LOC(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                       IO_comp_item_cost_tbl   IN OUT   NOCOPY OBJ_COMP_ITEM_COST_TBL,
                                       I_nil_process_id        IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(64) := 'NEW_ITEM_LOC_SQL.PERSIST_ITEM_SUPP_COUNTRY_LOC';
   L_exists        BOOLEAN;

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   CURSOR C_UPDATE_BUYER_PACK IS
      SELECT 'x'
        FROM item_supp_country_loc                                   isl
            ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
       WHERE isl.loc               = iscl_table.I_location
         AND isl.item              = iscl_table.I_item
         AND isl.supplier          = iscl_table.I_supplier
         AND isl.origin_country_id = iscl_table.I_origin_country_id
         FOR UPDATE OF isl.unit_cost NOWAIT;

   CURSOR C_LOCK_ISCBC IS
      SELECT 'x'
        FROM item_supp_country_bracket_cost                          iscbc
            ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
       WHERE iscl_table.I_item               = iscbc.item
         AND iscl_table.I_supplier           = iscbc.supplier
         AND iscl_table.I_origin_country_id  = iscbc.origin_country_id
         AND iscl_table.I_location           = iscbc.location
         FOR UPDATE OF iscbc.unit_cost NOWAIT;

   CURSOR C_LOCK_ISCL IS
      SELECT 'x'
        FROM item_supp_country_loc                                   iscl
            ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
       WHERE item                = iscl_table.I_item
         AND supplier            = iscl_table.I_supplier
         AND origin_country_id   = iscl_table.I_origin_country_id
         AND loc                 = iscl_table.I_location
         FOR UPDATE OF iscl.unit_cost
                      ,iscl.negotiated_item_cost
                      ,iscl.extended_base_cost
                      ,iscl.inclusive_cost
                      ,iscl.base_cost
                      ,iscl.last_update_id
                      ,iscl.last_update_datetime NOWAIT;

   CURSOR C_LOCK_PRIM_LOC IS
       SELECT 'x'
         FROM item_supp_country                                       isc
             ,item_supp_country_loc                                   iscl
             ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
        WHERE iscl.item              = iscl_table.I_item
          AND iscl.supplier          = iscl_table.I_supplier
          AND iscl.origin_country_id = iscl_table.I_origin_country_id
          AND iscl.loc               = iscl_table.I_location
          AND iscl.primary_loc_ind   = 'Y'
          AND iscl_table.I_loc_type  = 'W'
          AND NVL(iscl_table.pack_type,'N')  != 'B'
          AND iscl.item              = isc.item
          AND iscl.supplier          = isc.supplier
          AND iscl.origin_country_id = isc.origin_country_id
          FOR UPDATE OF isc.unit_cost
                       ,isc.inclusive_cost
                       ,isc.extended_base_cost
                       ,isc.base_cost
                       ,isc.negotiated_item_cost
                       ,isc.last_update_datetime
                       ,isc.last_update_id NOWAIT;

   CURSOR C_GET_ISCL_ROW IS
      SELECT iscl.item,
             iscl.supplier,
             iscl.origin_country_id,
             iscl.loc
        FROM item_supp_country_loc                                   iscl
            ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
       WHERE item                = iscl_table.I_item
         AND supplier            = iscl_table.I_supplier
         AND origin_country_id   = iscl_table.I_origin_country_id
         AND loc                 = iscl_table.I_location;
         
   L_get_iscl_row  c_get_iscl_row%ROWTYPE;

BEGIN

   SQL_LIB.SET_MARK('INSERT',
                    NULL,
                    'ITEM_SUPP_COUNTRY_LOC',
                    NULL);
   -- insert records into item_supp_country_loc for records on IO_comp_item_cost_tbl
   MERGE INTO item_supp_country_loc iscl
      USING (
             SELECT iscl_table.I_item              item
                   ,iscl_table.I_supplier          supplier
                   ,iscl_table.I_origin_country_id origin_country_id
                   ,iscl_table.I_location          loc
                   ,iscl_table.I_loc_type          loc_type
                   ,CASE NVL(ca.default_po_cost, 'BC')
                       WHEN 'BC'
                       THEN NVL(iscl_table.O_base_cost, isc.unit_cost)
                       WHEN 'NIC'
                       THEN NVL(iscl_table.O_negotiated_item_cost, isc.unit_cost)
                       ELSE isc.unit_cost
                    END                            unit_cost
                   ,isc.round_lvl
                   ,isc.round_to_inner_pct
                   ,isc.round_to_case_pct
                   ,isc.round_to_layer_pct
                   ,isc.round_to_pallet_pct
                   ,isc.supp_hier_type_1
                   ,isc.supp_hier_lvl_1
                   ,isc.supp_hier_type_2
                   ,isc.supp_hier_lvl_2
                   ,isc.supp_hier_type_3
                   ,isc.supp_hier_lvl_3
                   ,isc.pickup_lead_time
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_negotiated_item_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_negotiated_item_cost
                            END
                       ELSE NULL
                    END                            negotiated_item_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_extended_base_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_extended_base_cost
                            END
                       ELSE NULL
                    END                            extended_base_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_inclusive_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_inclusive_cost
                            END
                       ELSE NULL
                    END                            inclusive_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_base_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_base_cost
                            END
                       ELSE NULL
                    END                            base_cost
               FROM sups                                                    s
                   ,item_supp_country                                       isc
                   ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                   ,country_attrib                                          ca
              WHERE isc.item              = iscl_table.I_item
                AND isc.supplier          = iscl_table.I_supplier
                AND isc.origin_country_id = iscl_table.I_origin_country_id
                AND ca.country_id         = iscl_table.I_delivery_country_id
                AND s.supplier            = isc.supplier
                AND (   iscl_table.I_loc_type IN ('S', 'E')
                     OR s.inv_mgmt_lvl IN ('S', 'D'))
              UNION ALL
             SELECT iscl_table.I_item                                        item
                   ,iscl_table.I_supplier                                    supplier
                   ,iscl_table.I_origin_country_id                           origin_country_id
                   ,iscl_table.I_location                                    loc
                   ,iscl_table.I_loc_type                                    loc_type
                   ,CASE NVL(ca.default_po_cost, 'BC')
                       WHEN 'BC'
                       THEN NVL(iscl_table.O_base_cost, isc.unit_cost)
                       WHEN 'NIC'
                       THEN NVL(iscl_table.O_negotiated_item_cost, isc.unit_cost)
                       ELSE isc.unit_cost
                    END                            unit_cost
                   ,NVL(sim_wh.round_lvl, isc.round_lvl)                     round_lvl
                   ,NVL(sim_wh.round_to_inner_pct, isc.round_to_inner_pct)   round_to_inner_pct
                   ,NVL(sim_wh.round_to_case_pct, isc.round_to_case_pct)     round_to_case_pct
                   ,NVL(sim_wh.round_to_layer_pct, isc.round_to_layer_pct)   round_to_layer_pct
                   ,NVL(sim_wh.round_to_pallet_pct, isc.round_to_pallet_pct) round_to_pallet_pct
                   ,isc.supp_hier_type_1
                   ,isc.supp_hier_lvl_1
                   ,isc.supp_hier_type_2
                   ,isc.supp_hier_lvl_2
                   ,isc.supp_hier_type_3
                   ,isc.supp_hier_lvl_3
                   ,isc.pickup_lead_time
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_negotiated_item_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_negotiated_item_cost
                            END
                       ELSE NULL
                    END                                                      negotiated_item_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_extended_base_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_extended_base_cost
                            END
                       ELSE NULL
                    END                                                      extended_base_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_inclusive_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_inclusive_cost
                            END
                       ELSE NULL
                    END                                                      inclusive_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_base_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_base_cost
                            END
                       ELSE NULL
                    END                                                      base_cost
               FROM sups                                                    s
                   ,item_supp_country                                       isc
                   ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                   ,country_attrib                                          ca
                   ,(
                     SELECT sim.supplier,
                            sim.location,
                            w.wh,
                            sim.dept,
                            sim.round_lvl,
                            sim.round_to_inner_pct,
                            sim.round_to_case_pct,
                            sim.round_to_layer_pct,
                            sim.round_to_pallet_pct
                       FROM sup_inv_mgmt sim,
                            wh w
                      WHERE w.physical_wh = sim.location
                     )                                                      sim_wh
              WHERE isc.item               = iscl_table.I_item
                AND isc.supplier           = iscl_table.I_supplier
                AND isc.origin_country_id  = iscl_table.I_origin_country_id
                AND ca.country_id          = iscl_table.I_delivery_country_id
                AND s.supplier             = isc.supplier
                AND s.inv_mgmt_lvl         = 'A'
                AND iscl_table.I_loc_type IN ('W', 'I')
                AND iscl_table.I_location  = sim_wh.wh(+)
                AND iscl_table.I_supplier  = sim_wh.supplier(+)
                AND iscl_table.dept        = sim_wh.dept(+)
              UNION ALL
             SELECT iscl_table.I_item                                        item
                   ,iscl_table.I_supplier                                    supplier
                   ,iscl_table.I_origin_country_id                           origin_country_id
                   ,iscl_table.I_location                                    loc
                   ,iscl_table.I_loc_type                                    loc_type
                   ,CASE NVL(ca.default_po_cost, 'BC')
                       WHEN 'BC'
                       THEN NVL(iscl_table.O_base_cost, isc.unit_cost)
                       WHEN 'NIC'
                       THEN NVL(iscl_table.O_negotiated_item_cost, isc.unit_cost)
                       ELSE isc.unit_cost
                    END                                                      unit_cost
                   ,NVL(sim_wh.round_lvl, isc.round_lvl)                     round_lvl
                   ,NVL(sim_wh.round_to_inner_pct, isc.round_to_inner_pct)   round_to_inner_pct
                   ,NVL(sim_wh.round_to_case_pct, isc.round_to_case_pct)     round_to_case_pct
                   ,NVL(sim_wh.round_to_layer_pct, isc.round_to_layer_pct)   round_to_layer_pct
                   ,NVL(sim_wh.round_to_pallet_pct, isc.round_to_pallet_pct) round_to_pallet_pct
                   ,isc.supp_hier_type_1
                   ,isc.supp_hier_lvl_1
                   ,isc.supp_hier_type_2
                   ,isc.supp_hier_lvl_2
                   ,isc.supp_hier_type_3
                   ,isc.supp_hier_lvl_3
                   ,isc.pickup_lead_time
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_negotiated_item_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_negotiated_item_cost
                            END
                       ELSE NULL
                    END                                                      negotiated_item_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_extended_base_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_extended_base_cost
                            END
                       ELSE NULL
                    END                                                      extended_base_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_inclusive_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_inclusive_cost
                            END
                       ELSE NULL
                    END                                                      inclusive_cost
                   ,CASE
                       WHEN LP_system_options.default_tax_type = 'GTAX'
                       THEN CASE
                               WHEN iscl_table.O_base_cost = 0
                               THEN NULL
                               ELSE iscl_table.O_base_cost
                            END
                       ELSE NULL
                    END                                                      base_cost
               FROM sups                                                    s
                   ,item_supp_country                                       isc
                   ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                   ,country_attrib                                          ca
                   ,(
                     SELECT sim.supplier
                           ,sim.location
                           ,w.wh
                           ,sim.dept
                           ,sim.round_lvl
                           ,sim.round_to_inner_pct
                           ,sim.round_to_case_pct
                           ,sim.round_to_layer_pct
                           ,sim.round_to_pallet_pct
                       FROM sup_inv_mgmt sim
                           ,wh           w
                      WHERE w.physical_wh = sim.location
                     )                                                      sim_wh
              WHERE isc.item               = iscl_table.I_item
                AND isc.supplier           = iscl_table.I_supplier
                AND isc.origin_country_id  = iscl_table.I_origin_country_id
                AND ca.country_id          = iscl_table.I_delivery_country_id
                AND s.supplier             = isc.supplier
                AND s.inv_mgmt_lvl         = 'L'
                AND iscl_table.I_loc_type IN ('W', 'I')
                AND iscl_table.I_location  = sim_wh.wh(+)
                AND iscl_table.I_supplier  = sim_wh.supplier(+)
                AND sim_wh.dept     IS NULL
             ) input
         ON (    input.item              = iscl.item
             AND input.supplier          = iscl.supplier
             AND input.origin_country_id = iscl.origin_country_id
             AND input.loc               = iscl.loc
             AND input.loc_type          = iscl.loc_type)
       WHEN NOT MATCHED THEN
          INSERT (item
                 ,supplier
                 ,origin_country_id
                 ,loc
                 ,loc_type
                 ,primary_loc_ind
                 ,unit_cost
                 ,round_lvl
                 ,round_to_inner_pct
                 ,round_to_case_pct
                 ,round_to_layer_pct
                 ,round_to_pallet_pct
                 ,supp_hier_type_1
                 ,supp_hier_lvl_1
                 ,supp_hier_type_2
                 ,supp_hier_lvl_2
                 ,supp_hier_type_3
                 ,supp_hier_lvl_3
                 ,pickup_lead_time
                 ,create_datetime
                 ,last_update_datetime
                 ,last_update_id
                 ,negotiated_item_cost
                 ,extended_base_cost
                 ,inclusive_cost
                 ,base_cost
                 ,create_id)
          VALUES (input.item
                 ,input.supplier
                 ,input.origin_country_id
                 ,input.loc
                 ,input.loc_type
                 ,'N'
                 ,input.unit_cost
                 ,input.round_lvl
                 ,input.round_to_inner_pct
                 ,input.round_to_case_pct
                 ,input.round_to_layer_pct
                 ,input.round_to_pallet_pct
                 ,input.supp_hier_type_1
                 ,input.supp_hier_lvl_1
                 ,input.supp_hier_type_2
                 ,input.supp_hier_lvl_2
                 ,input.supp_hier_type_3
                 ,input.supp_hier_lvl_3
                 ,input.pickup_lead_time
                 ,SYSDATE
                 ,SYSDATE
                 ,LP_user_id
                 ,input.negotiated_item_cost
                 ,input.extended_base_cost
                 ,input.inclusive_cost
                 ,input.base_cost
                 ,LP_user_id);

   SQL_LIB.SET_MARK('UPDATE',
                    NULL,
                    'ITEM_SUPP_COUNTRY_LOC',
                    NULL);

   -- Select a primary location in item_supp_country_loc for records on IO_comp_item_cost_tbl
   -- if no primary exists yet.
   MERGE INTO item_supp_country_loc iscl
      USING (
             SELECT I_item
                   ,I_supplier
                   ,I_origin_country_id
                   ,MIN(I_location) I_location
               FROM TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl))
              GROUP BY I_item
                      ,I_supplier
                      ,I_origin_country_id
             ) iscl_tbl
         ON (    iscl.item              = iscl_tbl.I_item
             AND iscl.supplier          = iscl_tbl.I_supplier
             AND iscl.origin_country_id = iscl_tbl.I_origin_country_id
             AND iscl.loc               = iscl_tbl.I_location)
       WHEN MATCHED THEN
          UPDATE
             SET iscl.primary_loc_ind = 'Y'
           WHERE NOT EXISTS (SELECT 'x'
                               FROM item_supp_country_loc iscl_in
                              WHERE iscl_in.item              = iscl.item
                                AND iscl_in.supplier          = iscl.supplier
                                AND iscl_in.origin_country_id = iscl.origin_country_id
                                AND iscl_in.primary_loc_ind   = 'Y');
   ---
   OPEN C_UPDATE_BUYER_PACK;
   CLOSE C_UPDATE_BUYER_PACK;
   ---

   -- update the unit cost based on components for buyer packs
   MERGE INTO item_supp_country_loc iscl
      USING (
             SELECT /*+ cardinality(iscl_table, 10) */
                    iscl_table.I_item
                   ,iscl_table.I_supplier
                   ,iscl_table.I_origin_country_id
                   ,iscl_table.I_location
                   ,NVL(SUM(isl.unit_cost * vpq.pack_item_qty), 0) unit_cost
               FROM item_supp_country_loc                                   isl
                   ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                   ,item_master                                             im
                   ,packitem_breakout                                       vpq
              WHERE isl.item               = vpq.item
                AND vpq.pack_no            = iscl_table.I_item
                AND isl.supplier           = iscl_table.I_supplier
                AND isl.origin_country_id  = iscl_table.I_origin_country_id
                AND isl.loc                = iscl_table.I_location
                AND im.item                = iscl_table.I_item
                AND NVL(im.pack_type, 'N') = 'B'
              GROUP BY iscl_table.I_item
                       ,iscl_table.I_supplier
                       ,iscl_table.I_origin_country_id
                       ,iscl_table.I_location
             ) update_set
         ON (    iscl.item              = update_set.I_item
             AND iscl.supplier          = update_set.I_supplier
             AND iscl.origin_country_id = update_set.I_origin_country_id
             AND iscl.loc               = update_set.I_location)
       WHEN MATCHED THEN
          UPDATE
             SET iscl.unit_cost = update_set.unit_cost;

   -- bracket costing functionality
   IF LP_system_options.bracket_costing_ind = 'Y' THEN
      ---
      MERGE INTO item_supp_country_bracket_cost iscbc
         USING (
                SELECT distinct item,
                       supplier,
                       origin_country_id,
                       loc,
                       default_bracket_ind,
                       bracket_value1,
                       unit_cost,
                       bracket_value2,
                       sup_dept_seq_no
                  FROM (
                        SELECT iscl_table.I_item              item
                              ,iscl_table.I_supplier          supplier
                              ,iscl_table.I_origin_country_id origin_country_id
                              ,iscl_table.I_location          loc
                              ,iscbc.default_bracket_ind
                              ,iscbc.bracket_value1
                              ,iscbc.unit_cost
                              ,iscbc.bracket_value2
                              ,iscbc.sup_dept_seq_no
                              ,1                              loc_priority
                              ,1                              dept_priority
                          FROM item_supp_country_bracket_cost                          iscbc
                              ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                              ,sups                                                    s
                         WHERE iscbc.location          IS NULL
                           AND iscl_table.I_location   IS NULL
                           AND iscbc.item               = iscl_table.I_item
                           AND iscbc.supplier           = iscl_table.I_supplier
                           AND iscbc.origin_country_id  = iscl_table.I_origin_country_id
                           AND iscl_table.I_loc_type    = 'W'
                           AND nvl(iscl_table.pack_type,'N')   != 'B'
                           AND iscl_table.I_supplier    = s.supplier
                           AND s.inv_mgmt_lvl          IN ('S', 'D') 
                         UNION ALL
                        SELECT iscl_table.I_item              item
                              ,iscl_table.I_supplier          supplier
                              ,iscl_table.I_origin_country_id origin_country_id
                              ,iscl_table.I_location          loc
                              ,sbc.default_bracket_ind
                              ,sbc.bracket_value1
                              ,iscl.unit_cost
                              ,sbc.bracket_value2
                              ,sbc.sup_dept_seq_no
                              ,1                              loc_priority
                              ,1                              dept_priority
                          FROM item_supp_country_loc                                   iscl
                              ,sup_bracket_cost                                        sbc
                              ,wh
                              ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                              ,sups                                                    s
                         WHERE iscl.item              = iscl_table.I_item
                           AND iscl.supplier          = iscl_table.I_supplier
                           AND iscl.origin_country_id = iscl_table.I_origin_country_id
                           AND iscl.loc               = iscl_table.I_location
                           AND iscl_table.I_loc_type  = 'W'
                           AND nvl(iscl_table.pack_type,'N')   != 'B'
                           AND iscl.supplier          = sbc.supplier
                           AND iscl.loc               = wh.wh
                           AND wh.physical_wh         = sbc.location
                           AND (
                                   (    s.inv_mgmt_lvl = 'A'
                                    AND sbc.dept       = iscl_table.dept
                                    )
                                OR (    s.inv_mgmt_lvl  = 'L'
                                    AND sbc.dept       IS NULL
                                    )
                                )
                         UNION ALL
                        SELECT iscl_table.I_item              item
                              ,iscl_table.I_supplier          supplier
                              ,iscl_table.I_origin_country_id origin_country_id
                              ,iscl_table.I_location          loc
                              ,sbc.default_bracket_ind
                              ,sbc.bracket_value1
                              ,iscbc.unit_cost
                              ,sbc.bracket_value2
                              ,sbc.sup_dept_seq_no
                              ,2                              loc_priority
                              ,CASE
                                  WHEN s.inv_mgmt_lvl IN ('S', 'D', 'A') AND sbc.dept IS NULL
                                  THEN 3
                                  ELSE 2
                               END                            dept_priority
                          FROM item_supp_country_loc                                   iscl
                              ,sup_bracket_cost                                        sbc
                              ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                              ,sups                                                    s
                              ,item_supp_country_bracket_cost                          iscbc
                          WHERE iscl.item               = iscl_table.I_item
                            AND iscl.supplier           = iscl_table.I_supplier
                            AND iscl.origin_country_id  = iscl_table.I_origin_country_id
                            AND iscl.loc                = iscl_table.I_location
                            AND iscbc.item              = iscl.item
                            AND iscbc.supplier          = iscl.supplier
                            AND iscbc.origin_country_id = iscl.origin_country_id
                            AND iscl_table.I_loc_type   = 'W'
                            AND nvl(iscl_table.pack_type,'N')   != 'B'
                            AND iscl_table.I_supplier   = s.supplier
                            AND iscl.supplier           = sbc.supplier
                            AND iscbc.supplier          = sbc.supplier
                            AND iscbc.bracket_value1    = sbc.bracket_value1
                            AND (
                                    (
                                         s.inv_mgmt_lvl                IN ('A', 'S', 'D')
                                     AND NVL(sbc.dept,iscl_table.dept)  = iscl_table.dept
                                     )
                                 OR (    s.inv_mgmt_lvl = 'L'
                                     AND sbc.dept      IS NULL
                                     )
                                 )
                            AND NOT EXISTS(SELECT 'x' 
                                             FROM item_supp_country_bracket_cost iscbc2
                                            WHERE iscbc2.item              = iscbc.item
                                              AND iscbc2.supplier          = iscbc.supplier
                                              AND iscbc2.origin_country_id = iscbc.origin_country_id
                                              AND iscbc2.location          = iscbc.location
                                              AND iscbc2.bracket_value1    = iscbc.bracket_value1
                                              AND rownum                   = 1)
                        )
                ) input
            ON (    input.item              = iscbc.item
                AND input.supplier          = iscbc.supplier
                AND input.origin_country_id = iscbc.origin_country_id
                AND input.loc               = iscbc.location
                AND input.bracket_value1    = iscbc.bracket_value1)
          WHEN NOT MATCHED THEN
             INSERT (item
                    ,supplier
                    ,origin_country_id
                    ,location
                    ,loc_type
                    ,default_bracket_ind
                    ,bracket_value1
                    ,unit_cost
                    ,bracket_value2
                    ,sup_dept_seq_no)
             VALUES (input.item
                    ,input.supplier
                    ,input.origin_country_id
                    ,input.loc
                    ,'W'
                    ,input.default_bracket_ind
                    ,input.bracket_value1
                    ,input.unit_cost
                    ,input.bracket_value2
                    ,input.sup_dept_seq_no);
      ---
      OPEN c_get_iscl_row;
      FETCH c_get_iscl_row INTO L_get_iscl_row;      
      ---
      if not ITEM_BRACKET_COST_SQL.BRACKETS_EXIST(O_error_message,
                            L_exists,
                            L_get_iscl_row.item,
                            L_get_iscl_row.supplier,
                            L_get_iscl_row.origin_country_id,
                            L_get_iscl_row.loc) then
         return FALSE;
      end if;
      ---
      CLOSE c_get_iscl_row;
      if L_exists = FALSE then
      
         MERGE INTO item_supp_country_bracket_cost iscbc
            USING (
                   SELECT distinct item,
                          supplier,
                          origin_country_id,
                          loc,
                          default_bracket_ind,
                          bracket_value1,
                          unit_cost,
                          bracket_value2,
                          sup_dept_seq_no
                     FROM (
                           SELECT iscl_table.I_item              item
                                 ,iscl_table.I_supplier          supplier
                                 ,iscl_table.I_origin_country_id origin_country_id
                                 ,iscl_table.I_location          loc
                                 ,sbc.default_bracket_ind
                                 ,sbc.bracket_value1
                                 ,iscl.unit_cost
                                 ,sbc.bracket_value2
                                 ,sbc.sup_dept_seq_no
                                 ,2                              loc_priority
                                 ,CASE
                                     WHEN s.inv_mgmt_lvl IN ('S', 'D', 'A') AND sbc.dept IS NULL
                                     THEN 3
                                     ELSE 2
                                  END                            dept_priority
                             FROM item_supp_country_loc                                   iscl
                                 ,sup_bracket_cost                                        sbc
                                 ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                                 ,sups                                                    s
                            WHERE iscl.item               = iscl_table.I_item
                              AND iscl.supplier           = iscl_table.I_supplier
                              AND iscl.origin_country_id  = iscl_table.I_origin_country_id
                              AND iscl.loc                = iscl_table.I_location
                              AND iscl_table.I_loc_type   = 'W'
                              AND nvl(iscl_table.pack_type,'N')   != 'B'
                              AND iscl_table.I_supplier   = s.supplier
                              AND iscl.supplier           = sbc.supplier
                              AND (
                                      (
                                           s.inv_mgmt_lvl                IN ('A', 'S', 'D')
                                       AND NVL(sbc.dept,iscl_table.dept)  = iscl_table.dept
                                       )
                                   OR (    s.inv_mgmt_lvl = 'L'
                                       AND sbc.dept      IS NULL
                                       )
                                  )
                              AND NOT EXISTS(SELECT 'x' 
                                               FROM item_supp_country_bracket_cost iscbc2
                                              WHERE iscbc2.item              = iscl_table.I_item
                                                AND iscbc2.supplier          = iscl_table.I_supplier
                                                AND iscbc2.origin_country_id = iscl_table.I_origin_country_id
                                                AND iscbc2.location          = iscl_table.I_location
                                                AND iscbc2.bracket_value1    = sbc.bracket_value1
                                                AND rownum                   = 1)
                           )

                   ) input
               ON (    input.item              = iscbc.item
                   AND input.supplier          = iscbc.supplier
                   AND input.origin_country_id = iscbc.origin_country_id
                   AND input.loc               = iscbc.location
                   AND input.bracket_value1    = iscbc.bracket_value1)
             WHEN NOT MATCHED THEN
                INSERT (item
                       ,supplier
                       ,origin_country_id
                       ,location
                       ,loc_type
                       ,default_bracket_ind
                       ,bracket_value1
                       ,unit_cost
                       ,bracket_value2
                       ,sup_dept_seq_no)
                VALUES (input.item
                       ,input.supplier
                       ,input.origin_country_id
                       ,input.loc
                       ,'W'
                       ,input.default_bracket_ind
                       ,input.bracket_value1
                       ,input.unit_cost
                       ,input.bracket_value2
                       ,input.sup_dept_seq_no);
      
      end if;
      ---
      OPEN C_LOCK_ISCBC;
      CLOSE C_LOCK_ISCBC;
      ---

      MERGE INTO item_supp_country_bracket_cost iscbc
         USING (
                SELECT iscl_table.I_item
                      ,iscl_table.I_supplier
                      ,iscl_table.I_origin_country_id
                      ,iscl_table.I_location
                      ,wh2.wh                                  existing_wh
                      ,iscbc.bracket_value1
                      ,iscbc.unit_cost
                      ,ROW_NUMBER() OVER (PARTITION BY iscl_table.I_item
                                                      ,iscl_table.I_location
                                                      ,iscl_table.I_supplier
                                                      ,iscl_table.I_origin_country_id
                                                      ,iscbc.bracket_value1
                                              ORDER BY wh2.wh) row_num
                  FROM wh                                                      wh1
                      ,wh                                                      wh2
                      ,TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                      ,item_supp_country_bracket_cost                          iscbc
                 WHERE wh1.physical_wh          = wh2.physical_wh
                   AND wh1.wh                   = iscl_table.I_location
                   AND wh2.wh                  != iscl_table.I_location
                   AND iscbc.item               = iscl_table.I_item
                   AND iscbc.supplier           = iscl_table.I_supplier
                   AND iscbc.origin_country_id  = iscl_table.I_origin_country_id
                   AND iscl_table.I_loc_type    = 'W'
                   AND nvl(iscl_table.pack_type,'N')   != 'B'
                   AND iscbc.location           = wh2.wh
                ) input
            ON (    input.I_item               = iscbc.item
                AND input.I_supplier           = iscbc.supplier
                AND input.I_origin_country_id  = iscbc.origin_country_id
                AND input.I_location           = iscbc.location
                AND input.bracket_value1       = iscbc.bracket_value1
                AND input.row_num              = 1)
          WHEN MATCHED THEN
             UPDATE
                SET iscbc.unit_cost = input.unit_cost;
      ---
      OPEN C_LOCK_ISCL;
      CLOSE C_LOCK_ISCL;
      ---

      MERGE INTO item_supp_country_loc iscl
         USING (
                SELECT iscl_table.I_item
                      ,iscl_table.I_supplier
                      ,iscl_table.I_origin_country_id
                      ,iscl_table.I_location
                      ,iscbc.unit_cost
                  FROM TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                      ,item_supp_country_bracket_cost                          iscbc
                 WHERE iscbc.item                 = iscl_table.I_item
                   AND iscbc.supplier             = iscl_table.I_supplier
                   AND iscbc.origin_country_id    = iscl_table.I_origin_country_id
                   AND iscbc.location             = iscl_table.I_location
                   AND iscbc.default_bracket_ind  = 'Y'
                   AND iscl_table.I_loc_type      = 'W'
                   AND nvl(iscl_table.pack_type,'N')   != 'B'
                ) input
            ON (    input.I_item              = iscl.item
                AND input.I_supplier          = iscl.supplier
                AND input.I_origin_country_id = iscl.origin_country_id
                AND input.I_location          = iscl.loc)
          WHEN MATCHED THEN
             UPDATE
                SET iscl.unit_cost            = input.unit_cost
                   ,iscl.negotiated_item_cost = DECODE(LP_system_options.default_tax_type, 'GTAX', iscl.negotiated_item_cost, NULL)
                   ,iscl.extended_base_cost   = DECODE(LP_system_options.default_tax_type, 'GTAX', iscl.extended_base_cost, NULL)
                   ,iscl.inclusive_cost       = DECODE(LP_system_options.default_tax_type, 'GTAX', iscl.inclusive_cost, NULL)
                   ,iscl.base_cost            = DECODE(LP_system_options.default_tax_type, 'GTAX', iscl.base_cost, NULL)
                   ,iscl.last_update_id       = LP_user_id
                   ,iscl.last_update_datetime = SYSDATE;
      ---
      OPEN C_LOCK_PRIM_LOC;
      CLOSE C_LOCK_PRIM_LOC;
      ---

      MERGE INTO item_supp_country isc
         USING (
                SELECT iscl_table.I_item
                      ,iscl_table.I_supplier
                      ,iscl_table.I_origin_country_id
                      ,iscl_table.I_location
                      ,iscl.unit_cost
                      ,iscl.inclusive_cost
                  FROM TABLE(OBJ_COMP_ITEM_COST_RETURN(IO_comp_item_cost_tbl)) iscl_table
                      ,item_supp_country_loc                                   iscl
                 WHERE iscl.item               = iscl_table.I_item
                   AND iscl.supplier           = iscl_table.I_supplier
                   AND iscl.origin_country_id  = iscl_table.I_origin_country_id
                   AND iscl.loc                = iscl_table.I_location
                   AND iscl.primary_loc_ind    = 'Y'
                   AND iscl_table.I_loc_type   = 'W'
                   AND nvl(iscl_table.pack_type,'N')   != 'B'
                ) input
            ON (    input.I_item              = isc.item
                AND input.I_supplier          = isc.supplier
                AND input.I_origin_country_id = isc.origin_country_id)
          WHEN MATCHED THEN
             UPDATE
                SET isc.unit_cost            = input.unit_cost
                   ,isc.negotiated_item_cost = DECODE(LP_system_options.default_tax_type, 'GTAX', isc.negotiated_item_cost,input.unit_cost)
                   ,isc.extended_base_cost   = DECODE(LP_system_options.default_tax_type, 'GTAX', isc.extended_base_cost,  input.unit_cost)
                   ,isc.inclusive_cost       = DECODE(LP_system_options.default_tax_type, 'GTAX', isc.inclusive_cost,      input.inclusive_cost)
                   ,isc.base_cost            = DECODE(LP_system_options.default_tax_type, 'GTAX', isc.base_cost,           input.unit_cost)
                   ,isc.last_update_id       = LP_user_id
                   ,isc.last_update_datetime = SYSDATE;
      ---
   END IF; --LP_system_options.bracket_costing_ind = 'Y'

   RETURN TRUE;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      ---
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                             NULL,
                                             NULL,
                                             L_program);
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
   WHEN OTHERS THEN
      ---
      IF C_UPDATE_BUYER_PACK%ISOPEN THEN
         CLOSE C_UPDATE_BUYER_PACK;
      END iF;
      ---
      IF C_LOCK_ISCBC%ISOPEN THEN
         CLOSE C_LOCK_ISCBC;
      END iF;
      ---
      IF C_LOCK_ISCL%ISOPEN THEN
         CLOSE C_LOCK_ISCL;
      END iF;
      ---
      IF C_LOCK_PRIM_LOC%ISOPEN THEN
         CLOSE C_LOCK_PRIM_LOC;
      END iF;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      ---
      IF CLEAR_WORKING_TABLE(O_error_message,
                             I_nil_process_id) = FALSE THEN
         RETURN FALSE;
      END IF;
      ---
      RETURN FALSE;
END PERSIST_ITEM_SUPP_COUNTRY_LOC;
--------------------------------------------------------------------------------
FUNCTION CLEAR_WORKING_TABLE(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_nil_process_id   IN       NIL_INPUT_WORKING.PROCESS_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'NEW_ITEM_LOC_SQL.CLEAR_WORKING_TABLE';

BEGIN

   DELETE
     FROM nil_input_working
    WHERE process_id = I_nil_process_id;

   RETURN TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END CLEAR_WORKING_TABLE;
--------------------------------------------------------------------------------
END NEW_ITEM_LOC_SQL;
/
