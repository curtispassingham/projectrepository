CREATE OR REPLACE PACKAGE BODY RMSMFM_WH AS
--------------------------------------------------------------------------------
-- PACKAGE GLOBALS
--------------------------------------------------------------------------------

TYPE ROWID_TBL        IS TABLE OF ROWID INDEX BY BINARY_INTEGER;
--
LP_error_status VARCHAR2(1) := null;


--------------------------------------------------------------------------------
-- PRIVATE PROTOTYPES
--------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK(O_error_msg       OUT VARCHAR2,
                        O_queue_locked    OUT BOOLEAN,
                        I_wh_key_rec   IN     WH_KEY_REC)
RETURN BOOLEAN;

FUNCTION PROCESS_QUEUE_RECORD(O_error_msg        OUT VARCHAR2,
                              O_message       IN OUT NOCOPY RIB_OBJECT,
                              O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id    OUT    RIB_BUSOBJID_TBL,
                              O_message_type  IN OUT WH_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_wh_key_rec    IN     WH_KEY_REC,
                              I_hdr_published IN     WH_PUB_INFO.PUBLISHED%TYPE,
                              I_rowid         IN     ROWID)
RETURN BOOLEAN;

FUNCTION DELETE_QUEUE_REC(O_error_msg    OUT VARCHAR2,
                          I_rowid     IN     ROWID)
RETURN BOOLEAN;

FUNCTION MAKE_CREATE(O_error_msg       OUT VARCHAR2,
                     O_message      IN OUT NOCOPY RIB_OBJECT,
                     O_routing_info IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                     I_wh_key_rec   IN     WH_KEY_REC,
                     I_rowid        IN     ROWID)
RETURN BOOLEAN;

FUNCTION BUILD_HEADER_OBJECT(O_error_msg         OUT VARCHAR2,
                             O_routing_info   IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_rib_whdesc_rec    OUT NOCOPY "RIB_WHDesc_REC",
                             I_wh_key_rec     IN     WH_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_HEADER_OBJECT(O_error_msg        OUT VARCHAR2,
                             O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_rib_whref_rec    OUT NOCOPY "RIB_WHRef_REC",
                             I_wh_key_rec    IN     WH_KEY_REC)
RETURN BOOLEAN;

PROCEDURE HANDLE_ERRORS(O_status_code  IN OUT VARCHAR2,
                        O_error_msg    IN OUT VARCHAR2,
                        O_message      IN OUT NOCOPY RIB_OBJECT,
                        O_bus_obj_id   IN OUT NOCOPY RIB_BUSOBJID_TBL,
                        O_routing_info IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                        O_message_type IN OUT WH_MFQUEUE.MESSAGE_TYPE%TYPE,
                        I_seq_no       IN     WH_MFQUEUE.SEQ_NO%TYPE,
                        I_wh_key_rec   IN     WH_KEY_REC);

FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg            IN OUT VARCHAR2,
                              O_rib_whaddrdtl_tbl    IN OUT NOCOPY "RIB_AddrDesc_TBL",
                              O_wh_mfqueue_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_wh_mfqueue_size      IN OUT BINARY_INTEGER,
                              O_wh_addrdtl_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_wh_addrdtl_size      IN OUT BINARY_INTEGER,
                              O_delete_row_ind       IN OUT VARCHAR2,
                              I_message_type         IN     WH_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_wh_key_rec           IN     WH_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_SINGLE_DETAIL(O_error_msg             IN OUT VARCHAR2,
                             O_rib_whaddrdtl_tbl     IN OUT NOCOPY "RIB_AddrDesc_TBL",
                             O_rib_whaddrdtl_rec     IN OUT NOCOPY "RIB_AddrDesc_REC",
                             I_state_name            IN     STATE.DESCRIPTION%TYPE,
                             I_country_name          IN     COUNTRY.COUNTRY_DESC%TYPE,
                             I_addr_key              IN     ADDR.ADDR_KEY%TYPE,
                             I_addr_type             IN     ADDR.ADDR_TYPE%TYPE,
                             I_primary_ind           IN     ADD_TYPE_MODULE.PRIMARY_IND%TYPE,
                             I_primary_addr_ind      IN     ADDR.PRIMARY_ADDR_IND%TYPE,
                             I_add_1                 IN     ADDR.ADD_1%TYPE,
                             I_add_2                 IN     ADDR.ADD_2%TYPE,
                             I_add_3                 IN     ADDR.ADD_3%TYPE,
                             I_city                  IN     ADDR.CITY%TYPE,
                             I_state                 IN     ADDR.STATE%TYPE,
                             I_country_id            IN     ADDR.COUNTRY_ID%TYPE,
                             I_post                  IN     ADDR.POST%TYPE,
                             I_jurs_code             IN     ADDR.JURISDICTION_CODE%TYPE,
                             I_contact_name          IN     ADDR.CONTACT_NAME%TYPE,
                             I_contact_phone         IN     ADDR.CONTACT_PHONE%TYPE,
                             I_contact_telex         IN     ADDR.CONTACT_TELEX%TYPE,
                             I_contact_fax           IN     ADDR.CONTACT_FAX%TYPE,
                             I_contact_email         IN     ADDR.CONTACT_EMAIL%TYPE,
                             I_oracle_vendor_site_id IN     ADDR.ORACLE_VENDOR_SITE_ID%TYPE,
                             I_county                IN     ADDR.COUNTY%TYPE)
RETURN BOOLEAN;

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg         OUT VARCHAR2,
                                     O_rib_whdesc_rec IN OUT NOCOPY "RIB_WHDesc_REC",
                                     I_message_type   IN     WH_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_wh_key_rec     IN     WH_KEY_REC)
RETURN BOOLEAN;

FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg        OUT VARCHAR2,
                                     O_rib_whref_rec IN OUT NOCOPY "RIB_WHRef_REC",
                                     I_wh_key_rec    IN     WH_KEY_REC)
RETURN BOOLEAN;


--------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
--------------------------------------------------------------------------------

FUNCTION ADDTOQ(O_error_msg             OUT   VARCHAR2,
                I_message_type       IN       VARCHAR2,
                I_wh_key_rec         IN       WH_KEY_REC,
                I_addr_publish_ind   IN       ADDR.PUBLISH_IND%TYPE)
RETURN BOOLEAN IS

   L_wh             WH.WH%TYPE                 := NULL;
   L_pub            WH_PUB_INFO.PUBLISHED%TYPE := 'N';
   L_message_type   VARCHAR2(15)               := I_message_type;

   cursor C_WH_PUB_INFO is
      select wpi.wh,
             wpi.published
        from wh_pub_info wpi
       where wpi.wh = I_wh_key_rec.wh
         for update;

BEGIN

   if L_message_type != HDR_ADD then
      -- get wh pub info
      L_wh := NULL;

      open C_WH_PUB_INFO;
      fetch C_WH_PUB_INFO into L_wh,
                               L_pub;
      close C_WH_PUB_INFO;

      if L_wh is NULL then
         O_error_msg := SQL_LIB.CREATE_MSG('PUB_INFO_NOT_FOUND',
                                           'wh_pub_info',
                                           I_wh_key_rec.wh,
                                           NULL);
         return FALSE;
      end if;

      -- delete any prior messages on the queue that will be
      -- redundant once we add our new message to the queue
      if L_message_type = DTL_UPD then
         delete from wh_mfqueue
          where wh = I_wh_key_rec.wh
            and addr_key = I_wh_key_rec.addr_key
            and message_type = DTL_UPD;
      elsif L_message_type = DTL_DEL then
         delete from wh_mfqueue
          where wh = I_wh_key_rec.wh
            and addr_key = I_wh_key_rec.addr_key;
      elsif L_message_type = HDR_UPD then
         delete from wh_mfqueue
          where wh = I_wh_key_rec.wh
            and message_type = HDR_UPD;
      elsif L_message_type = HDR_DEL then
         delete from wh_mfqueue
          where wh = I_wh_key_rec.wh;
      end if;

   elsif L_message_type = HDR_ADD then

      open C_WH_PUB_INFO;
      fetch C_WH_PUB_INFO into L_wh,
                               L_pub;
      close C_WH_PUB_INFO;

      if L_wh is NULL then
         insert into wh_pub_info(wh,
                                 published,
                                 wh_type,
                                 pricing_loc,
                                 pricing_loc_curr)
                          values(I_wh_key_rec.wh,
                                 'N',
                                 I_wh_key_rec.wh_type,
                                 NULL,                      -- pricing_loc
                                 NULL);                     -- pricing_loc_curr
      end if;
   end if;
   --
   if I_addr_publish_ind = 'Y' or L_message_type != DTL_DEL then
      if L_message_type in (WHA_ADD, WHA_UPD) then
         update wh_pub_info
            set pricing_loc = I_wh_key_rec.pricing_loc,
                pricing_loc_curr = I_wh_key_rec.pricing_loc_curr
          where wh = I_wh_key_rec.wh;
         --
         L_message_type := HDR_UPD;

         if (NVL(L_pub,'N') = 'N') then
            L_message_type := NULL;
         end if;
      end if;

      if L_message_type is not NULL then
         insert into wh_mfqueue(seq_no,
                                wh,
                                addr_key,
                                message_type,
                                family,
                                custom_message_type,
                                pub_status,
                                transaction_number,
                                transaction_time_stamp)
                         values(wh_mfsequence.NEXTVAL,
                                I_wh_key_rec.wh,
                                I_wh_key_rec.addr_key,
                                L_message_type,
                                RMSMFM_WH.FAMILY,
                                null,
                                'U',
                                I_wh_key_rec.wh,
                                SYSDATE);
      end if;
   end if;

   LP_error_status := API_CODES.SUCCESS;
   return TRUE;

EXCEPTION
   when OTHERS then
      API_LIBRARY.HANDLE_ERRORS(LP_error_status,
                                O_error_msg,
                                API_LIBRARY.FATAL_ERROR,
                                'RMSMFM_WH.ADDTOQ');
      return FALSE;

END ADDTOQ;
--------------------------------------------------------------------------------

PROCEDURE GETNXT(O_status_code     OUT VARCHAR2,
                 O_error_msg       OUT VARCHAR2,
                 O_message_type    OUT VARCHAR2,
                 O_message         OUT RIB_OBJECT,
                 O_bus_obj_id      OUT RIB_BUSOBJID_TBL,
                 O_routing_info    OUT RIB_ROUTINGINFO_TBL,
                 I_num_threads  IN     NUMBER DEFAULT 1,
                 I_thread_val   IN     NUMBER DEFAULT 1) IS

L_wh_queue_rec    WH_MFQUEUE%ROWTYPE;
L_wh              WH.WH%TYPE := null;
L_wh_key_rec      RMSMFM_WH.WH_KEY_REC := null;
L_pub_status      WH_MFQUEUE.PUB_STATUS%TYPE:= null;
L_rowid           ROWID := null;
L_hdr_published   WH_PUB_INFO.PUBLISHED%TYPE:= null;
L_hosp            VARCHAR2(1) :='N';
L_exists          VARCHAR2(1) := null;
L_keep_queue      BOOLEAN :=FALSE;
L_queue_locked    BOOLEAN :=FALSE;

cursor C_QUEUE is
  select seq_no,
         wh,
         addr_key,
         message_type,
         pub_status,
         rowid
    from wh_mfqueue
   where pub_status = 'U'
     and not exists (select 'x'
                       from daily_purge dp
                      where dp.table_name = 'WH'
                        and dp.key_value = wh_mfqueue.wh
                        and rownum = 1)
order by seq_no;

cursor C_HOSP is
select 'Y'
  from wh_mfqueue
 where wh = L_wh
   and pub_status = API_CODES.HOSPITAL;

cursor C_HDR_PUB is
select published,
       wh_type,
       pricing_loc,
       pricing_loc_curr
  from wh_pub_info
 where wh = L_wh;

cursor C_ADDR_EXISTS is
  select 'X'
    from addr a,
         add_type_module atm,
         wh_mfqueue wq
   where a.module = atm.module
     and a.addr_type = atm.address_type
     and a.module = 'WH'
     and atm.primary_ind = 'Y'
     and a.key_value_1 = wq.wh
     and a.addr_key = wq.addr_key(+)
     and a.key_value_1 = to_char(L_wh);

BEGIN

   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   loop
      O_message := null;
      open C_QUEUE;
      fetch C_QUEUE into L_wh_queue_rec.seq_no,
                         L_wh_queue_rec.wh,
                         L_wh_queue_rec.addr_key,
                         L_wh_queue_rec.message_type,
                         L_wh_queue_rec.pub_status,
                         L_rowid;
      if C_QUEUE%NOTFOUND then
         close C_QUEUE;
         O_status_code := API_CODES.NO_MSG;
         return;
      end if;
      close C_QUEUE;

      O_message_type := L_wh_queue_rec.message_type;
      L_wh := L_wh_queue_rec.wh;
      L_wh_key_rec.wh := L_wh_queue_rec.wh;
      L_wh_key_rec.addr_key := L_wh_queue_rec.addr_key;


      open  C_HDR_PUB;
      fetch C_HDR_PUB into L_hdr_published,
                           L_wh_key_rec.wh_type,
                           L_wh_key_rec.pricing_loc,
                           L_wh_key_rec.pricing_loc_curr;
      close C_HDR_PUB;

      if O_message_type = HDR_ADD then
         -- make sure address record exist before publishing a whcre message
         open C_ADDR_EXISTS;
         fetch C_ADDR_EXISTS into L_exists;
         close C_ADDR_EXISTS;
      end if;

      if O_message_type = HDR_ADD AND
         L_exists is NULL         AND
         L_wh_key_rec.wh_type = 'P' then
         -- do not publish a physical wh without addresses
         update wh_mfqueue 
            set pub_status = 'H' 
          where wh = L_wh_key_rec.wh;
         exit;
      end if;

      if LOCK_THE_BLOCK(O_error_msg,
                        L_queue_locked,
                        L_wh_key_rec) = FALSE then
         raise PROGRAM_ERROR;
      end if;

      if O_message_type = DTL_ADD THEN
         delete from wh_mfqueue 
               where wh=L_wh_key_rec.wh 
                 and addr_key is null 
                 and pub_status = API_CODES.HOSPITAL;
      end if;      
      if L_queue_locked = FALSE then
         open  C_HOSP;
         fetch C_HOSP into L_hosp;
         close C_HOSP;
         if L_hosp = 'Y' then
            O_error_msg := SQL_LIB.CREATE_MSG('SEND_TO_HOSP',null, null, null);
            raise PROGRAM_ERROR;
         end if;

         if PROCESS_QUEUE_RECORD(O_error_msg,
                                 O_message,
                                 O_routing_info,
                                 O_bus_obj_id,
                                 O_message_type,
                                 L_wh_key_rec,
                                 L_hdr_published,
                                 L_rowid) = FALSE then
            raise PROGRAM_ERROR;
         end if;
         if L_wh_queue_rec.message_type = HDR_DEL and
            O_message is null then
            null;  -- skip the record if not sending a HDR_DEL message
         else
            exit;  -- Break out of loop after processing record
         end if;
      end if;
   end loop;

   if O_message IS null then
      O_status_code := API_CODES.NO_MSG;
   else
      O_status_code := API_CODES.NEW_MSG;
      O_bus_obj_id := RIB_BUSOBJID_TBL(L_wh);
   end if;

EXCEPTION

   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    O_message_type,
                    L_wh_queue_rec.seq_no,
                    L_wh_key_rec);

END GETNXT;
--------------------------------------------------------------------------------

PROCEDURE PUB_RETRY(O_status_code     OUT VARCHAR2,
                    O_error_msg       OUT VARCHAR2,
                    O_message_type IN OUT VARCHAR2,
                    O_message         OUT RIB_OBJECT,
                    O_bus_obj_id   IN OUT RIB_BUSOBJID_TBL,
                    O_routing_info IN OUT RIB_ROUTINGINFO_TBL,
                    I_ref_object   IN     RIB_OBJECT) IS

L_seq_no           WH_MFQUEUE.SEQ_NO%TYPE := null;
L_hdr_published    WH_PUB_INFO.PUBLISHED%TYPE := null;
L_rowid            ROWID := null;
L_wh_key_rec       WH_KEY_REC := null;
L_wh_queue_rec     WH_MFQUEUE%ROWTYPE;
L_keep_queue       BOOLEAN := FALSE;
L_queue_locked     BOOLEAN := FALSE;

cursor C_RETRY_QUEUE is
select wq.seq_no,
       wq.wh,
       wpi.published hdr_published,
       wpi.wh_type,
       wq.addr_key,
       wq.message_type,
       wq.pub_status,
       wpi.pricing_loc,
       wpi.pricing_loc_curr,
       wq.rowid
  from wh_mfqueue wq,
       wh_pub_info wpi
 where wq.seq_no = L_seq_no
   and wq.wh = wpi.wh;

BEGIN
   -- status of 'H'ostipal
   LP_error_status := API_CODES.HOSPITAL;

   --get info from routing info
   ---assuming the only thing in the routing info is the seq_no
   L_seq_no := O_routing_info(1).value;
   O_message := null;

   --get info from queue table
   open C_RETRY_QUEUE;
   fetch C_RETRY_QUEUE into L_wh_queue_rec.seq_no,
                            L_wh_queue_rec.wh,
                            L_hdr_published,
                            L_wh_key_rec.wh_type,
                            L_wh_queue_rec.addr_key,
                            L_wh_queue_rec.message_type,
                            L_wh_queue_rec.pub_status,
                            L_wh_key_rec.pricing_loc,
                            L_wh_key_rec.pricing_loc_curr,
                            L_rowid;
   close C_RETRY_QUEUE;

   if L_wh_queue_rec.wh is null then
      O_status_code := API_CODES.NO_MSG;
      return;
   end if;
   L_wh_key_rec.wh := L_wh_queue_rec.wh;
   L_wh_key_rec.addr_key := L_wh_queue_rec.addr_key;

   if LOCK_THE_BLOCK(O_error_msg,
                     L_queue_locked,
                     L_wh_key_rec) = FALSE then
      raise PROGRAM_ERROR;
   end if;

   if L_queue_locked = FALSE then
      if PROCESS_QUEUE_RECORD(O_error_msg,
                              O_message,
                              O_routing_info,
                              O_bus_obj_id,
                              L_wh_queue_rec.message_type,
                              L_wh_key_rec,
                              L_hdr_published,
                              L_rowid) = FALSE then
         raise PROGRAM_ERROR;
      end if;
      O_message_type := L_wh_queue_rec.message_type;

      if O_message IS null then
         O_status_code := API_CODES.NO_MSG;
      else
         O_status_code := API_CODES.NEW_MSG;
         O_bus_obj_id := RIB_BUSOBJID_TBL(L_wh_queue_rec.wh);
      end if;
   else
      O_status_code := API_CODES.HOSPITAL;
   end if;

EXCEPTION

   when OTHERS then
      HANDLE_ERRORS(O_status_code,
                    O_error_msg,
                    O_message,
                    O_bus_obj_id,
                    O_routing_info,
                    L_wh_queue_rec.message_type,
                    L_wh_queue_rec.seq_no,
                    L_wh_key_rec);

END PUB_RETRY;


--------------------------------------------------------------------------------
-- PRIVATE PROCEDURES
--------------------------------------------------------------------------------

FUNCTION LOCK_THE_BLOCK(O_error_msg       OUT VARCHAR2,
                        O_queue_locked    OUT BOOLEAN,
                        I_wh_key_rec   IN     WH_KEY_REC)
RETURN BOOLEAN IS

L_table         VARCHAR2(30) := 'wh_mfqueue';
L_key1          VARCHAR2(100) := I_wh_key_rec.wh;
L_key2          VARCHAR2(100) := I_wh_key_rec.addr_key;
RECORD_LOCKED   EXCEPTION;
PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

cursor C_LOCK_QUEUE_KEY1 is
select 'x'
  from wh_mfqueue
 where wh = L_key1
   for update nowait;

cursor C_LOCK_QUEUE_KEY2 is
select 'x'
  from wh_mfqueue
 where wh = L_key1
   and addr_key = L_key2
   for update nowait;

BEGIN

   O_queue_locked := FALSE;

   if L_key2 is null then
      open C_LOCK_QUEUE_KEY1;
      close C_LOCK_QUEUE_KEY1;
   else
      open C_LOCK_QUEUE_KEY2;
      close C_LOCK_QUEUE_KEY2;
   end if;

   return TRUE;

EXCEPTION

   when RECORD_LOCKED then
      O_queue_locked := TRUE;
      return TRUE;

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.LOCK_THE_BLOCK',
                                        to_char(SQLCODE));
      return FALSE;

END LOCK_THE_BLOCK;
--------------------------------------------------------------------------------

FUNCTION PROCESS_QUEUE_RECORD(O_error_msg        OUT VARCHAR2,
                              O_message       IN OUT NOCOPY RIB_OBJECT,
                              O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                              O_bus_obj_id    OUT    RIB_BUSOBJID_TBL,
                              O_message_type  IN OUT WH_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_wh_key_rec    IN     WH_KEY_REC,
                              I_hdr_published IN     WH_PUB_INFO.PUBLISHED%TYPE,
                              I_rowid         IN     ROWID)
RETURN BOOLEAN IS

L_rib_whdesc_rec   "RIB_WHDesc_REC" := null;
L_rib_whref_rec    "RIB_WHRef_REC" := null;
L_wh               WH_PUB_INFO.WH%TYPE := null;

cursor C_WH_PUB_INFO is
select wpi.wh
  from wh_pub_info wpi
 where wpi.wh = I_wh_key_rec.wh
   for update;

BEGIN

   if O_message_type = HDR_DEL then
      if I_hdr_published = 'Y' then

         if BUILD_HEADER_OBJECT(O_error_msg,
                                O_routing_info,
                                L_rib_whref_rec,
                                I_wh_key_rec) = FALSE then
            return FALSE;
         end if;
         O_message := L_rib_whref_rec;
      end if;

      if DELETE_QUEUE_REC(O_error_msg,
                          I_rowid) = FALSE then
         return FALSE;
      end if;

      open C_WH_PUB_INFO;
      fetch C_WH_PUB_INFO into L_wh;
      close C_WH_PUB_INFO;

      delete from wh_pub_info wpi
       where not exists (select 'x'
                           from wh_mfqueue wm
                          where wm.wh = wpi.wh)
         and wpi.wh = I_wh_key_rec.wh;

   elsif I_hdr_published in ('N','I') then
      if I_hdr_published = 'N' then
         O_message_type := HDR_ADD;
      else
         O_message_type := DTL_ADD;
      end if;

      if MAKE_CREATE(O_error_msg,
                     O_message,
                     O_routing_info,
                     I_wh_key_rec,
                     I_rowid) = FALSE then
         return FALSE;
      end if;

   elsif O_message_type in(HDR_UPD, DTL_ADD, DTL_UPD) then
      if BUILD_HEADER_OBJECT(O_error_msg,
                             O_routing_info,
                             L_rib_whdesc_rec,
                             I_wh_key_rec) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg,
                                     L_rib_whdesc_rec,
                                     O_message_type,
                                     I_wh_key_rec) = FALSE then
         return FALSE;
      end if;

      if O_message_type = HDR_UPD then
         delete from wh_mfqueue
          where rowid = I_rowid;
      end if;

      O_message := L_rib_whdesc_rec;

   elsif O_message_type = DTL_DEL then
      if BUILD_HEADER_OBJECT(O_error_msg,
                             O_routing_info,
                             L_rib_whref_rec,
                             I_wh_key_rec) = FALSE then
         return FALSE;
      end if;

      if BUILD_DETAIL_DELETE_OBJECTS(O_error_msg,
                                     L_rib_whref_rec,
                                     I_wh_key_rec) = FALSE then
         return FALSE;
      end if;

      O_message := L_rib_whref_rec;
   else
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.PROCESS_QUEUE_RECORD',
                                        to_char(SQLCODE));
      return FALSE;

END PROCESS_QUEUE_RECORD;
--------------------------------------------------------------------------------

FUNCTION DELETE_QUEUE_REC(O_error_msg    OUT VARCHAR2,
                          I_rowid     IN     ROWID)
RETURN BOOLEAN IS

BEGIN

   delete from wh_mfqueue
    where rowid = I_rowid;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.DELETE_QUEUE_REC',
                                        to_char(SQLCODE));
      return FALSE;

END DELETE_QUEUE_REC;
--------------------------------------------------------------------------------

FUNCTION MAKE_CREATE(O_error_msg       OUT VARCHAR2,
                     O_message      IN OUT NOCOPY RIB_OBJECT,
                     O_routing_info IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                     I_wh_key_rec   IN     WH_KEY_REC,
                     I_rowid        IN     ROWID)
RETURN BOOLEAN IS

L_rib_whdesc_rec         "RIB_WHDesc_REC" := null;
L_rib_whaddrdtl_tbl      "RIB_AddrDesc_TBL" := null;
L_wh_mfqueue_rowid_tbl   ROWID_TBL;
L_wh_mfqueue_size        BINARY_INTEGER := 0;
L_wh_addrdtl_rowid_tbl   ROWID_TBL;
L_wh_addrdtl_size        BINARY_INTEGER := 0;
L_delete_rowid_ind       VARCHAR2(1) := 'Y';

BEGIN

   if BUILD_HEADER_OBJECT(O_error_msg,
                          O_routing_info,
                          L_rib_whdesc_rec,
                          I_wh_key_rec) = FALSE then
      return FALSE;
   end if;

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           L_rib_whaddrdtl_tbl,
                           L_wh_mfqueue_rowid_tbl,
                           L_wh_mfqueue_size,
                           L_wh_addrdtl_rowid_tbl,
                           L_wh_addrdtl_size,
                           L_delete_rowid_ind,
                           null,
                           I_wh_key_rec) = FALSE then
      return FALSE;
   end if;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   --- if flag from BUILD_DETAIL_OBJECTS is TRUE, add rowid to L_wh_mfqueue_rowid table;
   if L_delete_rowid_ind = 'Y' then
      L_wh_mfqueue_size := L_wh_mfqueue_size + 1;
      L_wh_mfqueue_rowid_tbl(L_wh_mfqueue_size) := I_rowid;
      L_rib_whdesc_rec.AddrDesc_TBL := null;
   end if;

   --- Update wh_pub_info;
   update wh_pub_info
      set published = decode(L_delete_rowid_ind, 'Y', 'Y', 'I')
    where wh = I_wh_key_rec.wh;

   --- Add the detail to the header;
   L_rib_whdesc_rec.AddrDesc_TBL := L_rib_whaddrdtl_tbl;

   --- Delete from wh_mfqueue where rowid = I_rowid;
   if L_wh_mfqueue_size > 0 then
      for i in 1..L_wh_mfqueue_size loop
         if DELETE_QUEUE_REC(O_error_msg,
                             L_wh_mfqueue_rowid_tbl(i)) = FALSE then
            return FALSE;
         end if;
      end loop;
   end if;

   --- Update wh_addr_dtl_info;
   if L_wh_addrdtl_size > 0 then
      forall i in 1..L_wh_addrdtl_size
      update addr
         set publish_ind = 'Y'
       where rowid = L_wh_addrdtl_rowid_tbl(i);
   end if;

   O_message := L_rib_whdesc_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.MAKE_CREATE',
                                        to_char(SQLCODE));
      return FALSE;

END MAKE_CREATE;
--------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT(O_error_msg         OUT VARCHAR2,
                             O_routing_info   IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_rib_whdesc_rec    OUT NOCOPY "RIB_WHDesc_REC",
                             I_wh_key_rec     IN     WH_KEY_REC)
RETURN BOOLEAN IS

L_rib_routing_rec   RIB_ROUTINGINFO_REC := null;
L_wh_type           VARCHAR2(1) := null;

cursor C_WH is
select wh,
       wh_name,
       email,
       currency_code,
       physical_wh,
       channel_id,
       stockholding_ind,
       break_pack_ind,
       redist_wh_ind,
       delivery_policy,
       duns_number,
       duns_loc,
       org_unit_id
  from wh
 where wh = I_wh_key_rec.wh;

L_wh_rec   C_WH%ROWTYPE := null;

BEGIN

   OPEN C_WH;
   FETCH C_WH into L_wh_rec;
   CLOSE C_WH;

   if L_wh_rec.wh is null then
      O_error_msg := SQL_LIB.CREATE_MSG('NO_WH_HEADER_PUB', I_wh_key_rec.wh, null, null);
      return FALSE;
   end if;

   O_rib_whdesc_rec := "RIB_WHDesc_REC"(0,
                                      L_wh_rec.wh,
                                      L_wh_rec.wh_name,
                                      null,                        --Address1
                                      null,                        --Address2
                                      null,                        --City
                                      null,                        --County
                                      null,                        --state
                                      null,                        --country_id
                                      null,                        --wh_pcode
                                      L_wh_rec.email,
                                      L_wh_rec.stockholding_ind,
                                      L_wh_rec.channel_id,
                                      L_wh_rec.currency_code,
                                      L_wh_rec.duns_number,
                                      L_wh_rec.duns_loc,
                                      L_wh_rec.physical_wh,
                                      L_wh_rec.break_pack_ind,
                                      L_wh_rec.redist_wh_ind,
                                      L_wh_rec.delivery_policy,
                                      null,                        --contact_person
                                      null,                        --dest_fax
                                      null,                        --phone_nbr
                                      null,                        --default_route
                                      null,                        --default_carrier_code
                                      null,                        --default_service_code
                                      null,                        --expedite_route
                                      null,                        --expedite_carrier_code
                                      null,                        --expedite_service_code
                                      null,                        --bol_upload_type
                                      null,                        --bol_print_type
                                      0,                           --lead_time
                                      0,                           --distance_to_dest
                                      null,                        --drop_trailers_accepted_flag
                                      null,                        --rcv_doc_available_flag
                                      null,                        --container_type
                                      null,                        --mld_default_route
                                      null,                        --unit_pick_container_type
                                      0,                           --dest_seq_nbr
                                      null,                        --owning dc
                                      null,                        --Address detail Rec
                                      I_wh_key_rec.pricing_loc,
                                      I_wh_key_rec.pricing_loc_curr,
                                      L_wh_rec.org_unit_id);       --Org unit id

   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('dest_id',
                                            I_wh_key_rec.wh,
                                            'loc_type',
                                            I_wh_key_rec.wh_type,
                                            null,
                                            null);
   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.BUILD_HEADER_OBJECT',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;
--------------------------------------------------------------------------------

FUNCTION BUILD_HEADER_OBJECT(O_error_msg        OUT VARCHAR2,
                             O_routing_info  IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                             O_rib_whref_rec    OUT NOCOPY "RIB_WHRef_REC",
                             I_wh_key_rec    IN     WH_KEY_REC)
RETURN BOOLEAN IS

L_rib_routing_rec   RIB_ROUTINGINFO_REC := null;

BEGIN

   O_rib_whref_rec := "RIB_WHRef_REC"(0,
                                    I_wh_key_rec.wh,
                                    null);            -- Address detail Rec

   O_routing_info := RIB_ROUTINGINFO_TBL();
   L_rib_routing_rec := RIB_ROUTINGINFO_REC('dest_id',
                                            I_wh_key_rec.wh,
                                            'loc_type',I_wh_key_rec.wh_type,
                                            null,
                                            null);

   O_routing_info.EXTEND;
   O_routing_info(O_routing_info.COUNT) := L_rib_routing_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.BUILD_HEADER_OBJECT',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_HEADER_OBJECT;
--------------------------------------------------------------------------------

PROCEDURE HANDLE_ERRORS(O_status_code  IN OUT VARCHAR2,
                        O_error_msg    IN OUT VARCHAR2,
                        O_message      IN OUT NOCOPY RIB_OBJECT,
                        O_bus_obj_id   IN OUT NOCOPY RIB_BUSOBJID_TBL,
                        O_routing_info IN OUT NOCOPY RIB_ROUTINGINFO_TBL,
                        O_message_type IN OUT WH_MFQUEUE.MESSAGE_TYPE%TYPE,
                        I_seq_no       IN     WH_MFQUEUE.SEQ_NO%TYPE,
                        I_wh_key_rec   IN     WH_KEY_REC) IS

L_rib_whdtlref_rec   "RIB_AddrRef_REC";
L_rib_whdtlref_tbl   "RIB_AddrRef_TBL";
L_rib_whref_rec      "RIB_WHRef_REC";
L_error_type         VARCHAR2(5) := null;

BEGIN

   O_status_code := LP_error_status;

   if LP_error_status = API_CODES.HOSPITAL then
      O_bus_obj_id := RIB_BUSOBJID_TBL(I_wh_key_rec.wh);
      O_routing_info := RIB_ROUTINGINFO_TBL(RIB_ROUTINGINFO_REC('sequence_no',
                                                                I_seq_no,
                                                                null,
                                                                null,
                                                                null,
                                                                null));

      L_rib_whdtlref_rec := "RIB_AddrRef_REC"(0,I_wh_key_rec.addr_key);
      L_rib_whdtlref_tbl := "RIB_AddrRef_TBL"(L_rib_whdtlref_rec);
      L_rib_whref_rec := "RIB_WHRef_REC"(0,
                                       I_wh_key_rec.wh,
                                       L_rib_whdtlref_tbl);
      O_message := L_rib_whref_rec;

      update wh_mfqueue
         set pub_status = LP_error_status
       where seq_no = I_seq_no;

   end if;

   /* Pass out parsed error message */
   SQL_LIB.API_MSG(L_error_type, O_error_msg);

EXCEPTION

   when OTHERS then
      O_status_code := API_CODES.UNHANDLED_ERROR;
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.HANDLE_ERRORS',
                                        to_char(SQLCODE));

END HANDLE_ERRORS;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_OBJECTS(O_error_msg            IN OUT VARCHAR2,
                              O_rib_whaddrdtl_tbl    IN OUT NOCOPY "RIB_AddrDesc_TBL",
                              O_wh_mfqueue_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_wh_mfqueue_size      IN OUT BINARY_INTEGER,
                              O_wh_addrdtl_rowid_tbl IN OUT NOCOPY ROWID_TBL,
                              O_wh_addrdtl_size      IN OUT BINARY_INTEGER,
                              O_delete_row_ind       IN OUT VARCHAR2,
                              I_message_type         IN     WH_MFQUEUE.MESSAGE_TYPE%TYPE,
                              I_wh_key_rec           IN     WH_KEY_REC)
RETURN BOOLEAN IS

L_rib_whaddrdtl_tbl   "RIB_AddrDesc_TBL" := null;
L_rib_whaddrdtl_rec   "RIB_AddrDesc_REC" := null;
L_records_found       BOOLEAN := FALSE;
L_max_details         RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 12;
L_num_threads         RIB_SETTINGS.NUM_THREADS%TYPE := null;
L_min_time_lag        RIB_SETTINGS.MINUTES_TIME_LAG%TYPE := null;
L_status_code         VARCHAR2(1) := null;
L_details_processed   RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 0;

CURSOR C_ADDR_MC IS
  select s.description,
         c.country_desc,
         a.addr_key,
         a.addr_type,
         atm.primary_ind,
         a.primary_addr_ind,
         a.add_1,
         a.add_2,
         a.add_3,
         a.city,
         a.state,
         a.country_id,
         a.post,
         a.jurisdiction_code,
         a.contact_name,
         a.contact_phone,
         a.contact_telex,
         a.contact_fax,
         a.contact_email,
         a.oracle_vendor_site_id,
         a.county,
         a.rowid addr_rowid,
         wq.rowid wq_rowid
    from addr a,
         add_type_module atm,
         wh_mfqueue wq,
         state s,
         country c
   where a.module = atm.module
     and a.addr_type = atm.address_type
     and a.module = 'WH'
     and a.publish_ind = 'N'
     and a.state = s.state(+)
     and a.country_id = c.country_id
     and a.country_id =s.country_id(+)
     and a.key_value_1 = wq.wh (+)
     and a.addr_key = wq.addr_key (+)
     and a.key_value_1 = to_char(I_wh_key_rec.wh)
     and (a.jurisdiction_code is NULL
          or exists (select 'x'
                       from country_tax_jurisdiction ctj
                      where ctj.jurisdiction_code = a.jurisdiction_code
                        and ctj.state = a.state
                        and ctj.country_id = a.country_id))
order by atm.primary_ind desc, a.primary_addr_ind desc
     for update of a.publish_ind nowait;

CURSOR C_ADDR IS
select s.description,
       c.country_desc,
       a.addr_key,
       a.addr_type,
       atm.primary_ind,
       a.primary_addr_ind,
       a.add_1,
       a.add_2,
       a.add_3,
       a.city,
       a.state,
       a.country_id,
       a.post,
       a.jurisdiction_code,
       a.contact_name,
       a.contact_phone,
       a.contact_telex,
       a.contact_fax,
       a.contact_email,
       a.oracle_vendor_site_id,
       a.county,
       a.rowid addr_rowid,
       wq.rowid wq_rowid
  from addr a,
       add_type_module atm,
       wh_mfqueue wq,
       state s,
       country c
 where a.module = atm.module
   and a.addr_type = atm.address_type
   and a.module = 'WH'
   and a.state = s.state(+)
   and a.country_id = c.country_id
   and a.country_id =s.country_id(+)
   and a.addr_key = wq.addr_key(+)
   and a.addr_key = I_wh_key_rec.addr_key
   and wq.message_type(+) = I_message_type
   and (a.jurisdiction_code is NULL
        or exists (select 'x'
                     from country_tax_jurisdiction ctj
                    where ctj.jurisdiction_code = a.jurisdiction_code
                      and ctj.state = a.state
                      and ctj.country_id = a.country_id))
   for update of a.publish_ind nowait;

CURSOR C_ADDR_HDR_UPD IS
select s.description,
       c.country_desc,
       a.addr_key,
       a.addr_type,
       atm.primary_ind,
       a.primary_addr_ind,
       a.add_1,
       a.add_2,
       a.add_3,
       a.city,
       a.state,
       a.country_id,
       a.post,
       a.jurisdiction_code,
       a.contact_name,
       a.contact_phone,
       a.contact_telex,
       a.contact_fax,
       a.contact_email,
       a.oracle_vendor_site_id,
       a.county,
       a.rowid addr_rowid
  from addr a,
       add_type_module atm,
       state s,
       country c
 where a.module = atm.module
   and a.addr_type = atm.address_type
   and a.module = 'WH'
   and a.key_value_1 = to_char(I_wh_key_rec.wh)
   and atm.primary_ind = 'Y'
   and a.primary_addr_ind = 'Y'
   and a.state = s.state(+)
   and a.country_id = c.country_id
   and a.country_id =s.country_id(+)
   and (a.jurisdiction_code is NULL
     or exists (select 'x'
                  from country_tax_jurisdiction ctj
                 where ctj.jurisdiction_code = a.jurisdiction_code
                   and ctj.state = a.state
                   and ctj.country_id = a.country_id));

BEGIN

   if O_rib_whaddrdtl_tbl is null then
      L_rib_whaddrdtl_tbl := "RIB_AddrDesc_TBL"();
   else
      L_rib_whaddrdtl_tbl := O_rib_whaddrdtl_tbl;
   end if;

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_WH.FAMILY);

   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   if I_message_type is null then
      for rec in C_ADDR_MC loop
         L_records_found := TRUE;
         if L_details_processed >= L_max_details then
            O_delete_row_ind := 'N';
            exit;
         end if;

         if BUILD_SINGLE_DETAIL(O_error_msg,
                                 L_rib_whaddrdtl_tbl,
                                 L_rib_whaddrdtl_rec,
                                 rec.description,
                                 rec.country_desc,
                                 rec.addr_key,
                                 rec.addr_type,
                                 rec.primary_ind,
                                 rec.primary_addr_ind,
                                 rec.add_1,
                                 rec.add_2,
                                 rec.add_3,
                                 rec.city,
                                 rec.state,
                                 rec.country_id,
                                 rec.post,
                                 rec.jurisdiction_code,
                                 rec.contact_name,
                                 rec.contact_phone,
                                 rec.contact_telex,
                                 rec.contact_fax,
                                 rec.contact_email,
                                 rec.oracle_vendor_site_id,
                                 rec.county) = FALSE then
            return FALSE;
         end if;

         O_wh_addrdtl_size := O_wh_addrdtl_size + 1;
         O_wh_addrdtl_rowid_tbl(O_wh_addrdtl_size) := rec.addr_rowid;

         if rec.wq_rowid is not null then
            O_wh_mfqueue_size := O_wh_mfqueue_size + 1;
            O_wh_mfqueue_rowid_tbl(O_wh_mfqueue_size) := rec.wq_rowid;
         end if;

         L_details_processed := L_details_processed + 1;
      end loop;
   elsif I_message_type = HDR_UPD then
      for rec in C_ADDR_HDR_UPD loop
         L_records_found := TRUE;

         if BUILD_SINGLE_DETAIL(O_error_msg,
                                L_rib_whaddrdtl_tbl,
                                L_rib_whaddrdtl_rec,
                                rec.description,
                                rec.country_desc,
                                rec.addr_key,
                                rec.addr_type,
                                rec.primary_ind,
                                rec.primary_addr_ind,
                                rec.add_1,
                                rec.add_2,
                                rec.add_3,
                                rec.city,
                                rec.state,
                                rec.country_id,
                                rec.post,
                                rec.jurisdiction_code,
                                rec.contact_name,
                                rec.contact_phone,
                                rec.contact_telex,
                                rec.contact_fax,
                                rec.contact_email,
                                rec.oracle_vendor_site_id,
                                rec.county) = FALSE then
            return FALSE;
         end if;

         O_wh_addrdtl_size := O_wh_addrdtl_size + 1;
         O_wh_addrdtl_rowid_tbl(O_wh_addrdtl_size) := rec.addr_rowid;

         L_details_processed := L_details_processed + 1;
         exit;
      end loop;
   else
      for rec in C_ADDR loop
         L_records_found := TRUE;
         if L_details_processed >= L_max_details then
            O_delete_row_ind := 'N';
            exit;
         end if;

         if BUILD_SINGLE_DETAIL(O_error_msg,
                                L_rib_whaddrdtl_tbl,
                                L_rib_whaddrdtl_rec,
                                rec.description,
                                rec.country_desc,
                                rec.addr_key,
                                rec.addr_type,
                                rec.primary_ind,
                                rec.primary_addr_ind,
                                rec.add_1,
                                rec.add_2,
                                rec.add_3,
                                rec.city,
                                rec.state,
                                rec.country_id,
                                rec.post,
                                rec.jurisdiction_code,
                                rec.contact_name,
                                rec.contact_phone,
                                rec.contact_telex,
                                rec.contact_fax,
                                rec.contact_email,
                                rec.oracle_vendor_site_id,
                                rec.county) = FALSE then
            return FALSE;
         end if;

         O_wh_addrdtl_size := O_wh_addrdtl_size + 1;
         O_wh_addrdtl_rowid_tbl(O_wh_addrdtl_size) := rec.addr_rowid;

         if rec.wq_rowid is not null then
            O_wh_mfqueue_size := O_wh_mfqueue_size + 1;
            O_wh_mfqueue_rowid_tbl(O_wh_mfqueue_size) := rec.wq_rowid;
         end if;

         L_details_processed := L_details_processed + 1;
      end loop;

      -- if no data found in cursor, raise error
      if not L_records_found then
         O_error_msg := SQL_LIB.CREATE_MSG('NO_ADDR_DETAIL_PUB', I_wh_key_rec.wh, null, null);
         return FALSE;
      end if;
   end if;

   if L_rib_whaddrdtl_tbl.COUNT > 0 then
      O_rib_whaddrdtl_tbl := L_rib_whaddrdtl_tbl;
   else
      O_rib_whaddrdtl_tbl := null;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.BUILD_DETAIL_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_OBJECTS;
--------------------------------------------------------------------------------

FUNCTION BUILD_SINGLE_DETAIL(O_error_msg             IN OUT VARCHAR2,
                             O_rib_whaddrdtl_tbl     IN OUT NOCOPY "RIB_AddrDesc_TBL",
                             O_rib_whaddrdtl_rec     IN OUT NOCOPY "RIB_AddrDesc_REC",
                             I_state_name            IN     STATE.DESCRIPTION%TYPE,
                             I_country_name          IN     COUNTRY.COUNTRY_DESC%TYPE,
                             I_addr_key              IN     ADDR.ADDR_KEY%TYPE,
                             I_addr_type             IN     ADDR.ADDR_TYPE%TYPE,
                             I_primary_ind           IN     ADD_TYPE_MODULE.PRIMARY_IND%TYPE,
                             I_primary_addr_ind      IN     ADDR.PRIMARY_ADDR_IND%TYPE,
                             I_add_1                 IN     ADDR.ADD_1%TYPE,
                             I_add_2                 IN     ADDR.ADD_2%TYPE,
                             I_add_3                 IN     ADDR.ADD_3%TYPE,
                             I_city                  IN     ADDR.CITY%TYPE,
                             I_state                 IN     ADDR.STATE%TYPE,
                             I_country_id            IN     ADDR.COUNTRY_ID%TYPE,
                             I_post                  IN     ADDR.POST%TYPE,
                             I_jurs_code             IN     ADDR.JURISDICTION_CODE%TYPE,
                             I_contact_name          IN     ADDR.CONTACT_NAME%TYPE,
                             I_contact_phone         IN     ADDR.CONTACT_PHONE%TYPE,
                             I_contact_telex         IN     ADDR.CONTACT_TELEX%TYPE,
                             I_contact_fax           IN     ADDR.CONTACT_FAX%TYPE,
                             I_contact_email         IN     ADDR.CONTACT_EMAIL%TYPE,
                             I_oracle_vendor_site_id IN     ADDR.ORACLE_VENDOR_SITE_ID%TYPE,
                             I_county                IN     ADDR.COUNTY%TYPE)
RETURN BOOLEAN IS

BEGIN

   O_rib_whaddrdtl_rec := "RIB_AddrDesc_REC"(0,
                                             NULL,
                                             I_state_name,
                                             I_country_name,
                                             I_addr_key,
                                             I_addr_type,
                                             I_primary_ind,
                                             I_primary_addr_ind,
                                             I_add_1,
                                             I_add_2,
                                             I_add_3,
                                             I_city,
                                             I_state,
                                             I_country_id,
                                             I_post,
                                             I_contact_name,
                                             I_contact_phone,
                                             I_contact_telex,
                                             I_contact_fax,
                                             I_contact_email,
                                             I_oracle_vendor_site_id,
                                             I_county,
                                             I_jurs_code);

   O_rib_whaddrdtl_tbl.EXTEND;
   O_rib_whaddrdtl_tbl(O_rib_whaddrdtl_tbl.count) := O_rib_whaddrdtl_rec;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.BUILD_SINGLE_DETAIL',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_SINGLE_DETAIL;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_CHANGE_OBJECTS(O_error_msg         OUT VARCHAR2,
                                     O_rib_whdesc_rec IN OUT NOCOPY "RIB_WHDesc_REC",
                                     I_message_type   IN     WH_MFQUEUE.MESSAGE_TYPE%TYPE,
                                     I_wh_key_rec     IN     WH_KEY_REC)
RETURN BOOLEAN IS

L_wh_mfqueue_rowid_tbl   ROWID_TBL;
L_wh_mfqueue_size        BINARY_INTEGER := 0;
L_wh_addrdtl_rowid_tbl   ROWID_TBL;
L_wh_addrdtl_size        BINARY_INTEGER := 0;
L_delete_row_ind         VARCHAR2(1) := 'Y';

BEGIN

   if BUILD_DETAIL_OBJECTS(O_error_msg,
                           O_rib_whdesc_rec.AddrDesc_tbl,
                           L_wh_mfqueue_rowid_tbl,
                           L_wh_mfqueue_size,
                           L_wh_addrdtl_rowid_tbl,
                           L_wh_addrdtl_size,
                           L_delete_row_ind,
                           I_message_type,
                           I_wh_key_rec) = FALSE then
      return FALSE;
   end if;

   if L_wh_addrdtl_size > 0 then
      forall i in 1..L_wh_addrdtl_size
      update addr
         set publish_ind = 'Y'
       where rowid = L_wh_addrdtl_rowid_tbl(i);
   end if;

   if L_wh_mfqueue_size > 0 then
      for i in 1..L_wh_mfqueue_size loop
         if DELETE_QUEUE_REC(O_error_msg,
                             L_wh_mfqueue_rowid_tbl(i)) = FALSE then
            return FALSE;
         end if;
      end loop;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.BUILD_DETAIL_CHANGE_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_CHANGE_OBJECTS;
--------------------------------------------------------------------------------

FUNCTION BUILD_DETAIL_DELETE_OBJECTS(O_error_msg        OUT VARCHAR2,
                                     O_rib_whref_rec IN OUT NOCOPY "RIB_WHRef_REC",
                                     I_wh_key_rec    IN     WH_KEY_REC)
RETURN BOOLEAN IS

L_whaddr_rowid_tbl       ROWID_TBL;
L_whaddr_size            BINARY_INTEGER := 0;
L_wh_mfqueue_rowid_tbl   ROWID_TBL;
L_wh_mfqueue_size        BINARY_INTEGER := 0;
L_rib_whaddrref_rec      "RIB_AddrRef_REC" := null;
L_rib_whaddrref_tbl      "RIB_AddrRef_TBL" := null;
L_max_details            RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE := 12;
L_num_threads            RIB_SETTINGS.NUM_THREADS%TYPE := null;
L_min_time_lag           RIB_SETTINGS.MINUTES_TIME_LAG%TYPE := null;
L_status_code            VARCHAR2(1) := null;
L_details_processed      RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE :=0;

cursor C_ADDR_DELETE is
select wq.addr_key,
       wq.rowid wq_rowid
  from wh_mfqueue wq
 where wq.wh = I_wh_key_rec.wh
   and wq.message_type = DTL_DEL
   and rownum <= L_max_details;

BEGIN

   API_LIBRARY.GET_RIB_SETTINGS(L_status_code,
                                O_error_msg,
                                L_max_details,
                                L_num_threads,
                                L_min_time_lag,
                                RMSMFM_WH.FAMILY);

   if L_status_code in (API_CODES.UNHANDLED_ERROR) then
      return FALSE;
   end if;

   L_rib_whaddrref_tbl := "RIB_AddrRef_TBL"();

   for rec in C_ADDR_DELETE loop
      if rec.wq_rowid IS NOT null then
         L_rib_whaddrref_rec := "RIB_AddrRef_REC"(0,rec.addr_key);
         L_rib_whaddrref_tbl.EXTEND;
         L_rib_whaddrref_tbl(L_rib_whaddrref_tbl.count) := L_rib_whaddrref_rec;

         L_wh_mfqueue_size := L_wh_mfqueue_size + 1;
         L_wh_mfqueue_rowid_tbl(L_wh_mfqueue_size) := rec.wq_rowid;
      end if;
   end loop;

   LP_error_status := API_CODES.UNHANDLED_ERROR;

   if L_wh_mfqueue_size > 0 then
      for i in 1..L_wh_mfqueue_size loop
         if DELETE_QUEUE_REC(O_error_msg,
                             L_wh_mfqueue_rowid_tbl(i)) = FALSE then
           return FALSE;
         end if;
      end loop;
   end if;

   if L_rib_whaddrref_tbl.COUNT > 0 then
      O_rib_whref_rec.addrref_tbl := L_rib_whaddrref_tbl;
   else
      O_rib_whref_rec := null;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RMSMFM_WH.BUILD_DETAIL_DELETE_OBJECTS',
                                        to_char(SQLCODE));
      return FALSE;

END BUILD_DETAIL_DELETE_OBJECTS;
--------------------------------------------------------------------------------
END RMSMFM_WH;
/
