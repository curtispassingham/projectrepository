CREATE OR REPLACE PACKAGE BODY RMSAIASUB_SUPPLIER_VALIDATE AS
------------------------------------------------------------------------------------
--- Declare private procedure and functions.
------------------------------------------------------------------------------------
--- Private Function Name : INSERT_SUPPLIER_SITES
--- Purpose               : Checks the record reference is present.
------------------------------------------------------------------------------------
FUNCTION CHECK_FKEYS (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable  IN      VARCHAR2,
                      I_fkey_column      IN      VARCHAR2,
                      I_fkey_table       IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : CHECK_NULLS
--- Purpose               : Cehcks for NOT NULL.
------------------------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable  IN      VARCHAR2,
                      I_record_name      IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : CHECK_CODES
--- Purpose               : Checks against maintained codes.
------------------------------------------------------------------------------------
FUNCTION CHECK_CODES (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable  IN      VARCHAR2,
                      I_code_value       IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : CHECK_ORPHAN
--- Purpose               : Checks for orphan child nodes.
---                         This function checks that supplier-xref-key exists for supplier site.
---                         Also, org-unit validation checks that org-unit records are
---                         tied to sup-site-xref-key and same is for address_records.
------------------------------------------------------------------------------------
FUNCTION CHECK_ORPHAN(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_supp_xref_key       IN      VARCHAR2,
                      I_supp_site_xref_key  IN      VARCHAR2,
                      I_addr_xref_key       IN      VARCHAR2,
                      I_supplier_id         IN      SUPS.SUPPLIER%TYPE,
                      I_supp_site_id        IN      SUPS.SUPPLIER%TYPE,
                      I_addr_key            IN      ADDR.ADDR_KEY%TYPE,
                      I_inputobject_type    IN      VARCHAR2,
                      I_check_ind           IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : VALIDATE_RECORD
--- Purpose               : Validates entire supplier record.
------------------------------------------------------------------------------------
FUNCTION VALIDATE_RECORD (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_inputobject       IN      "RIB_SupplierColDesc_REC",
                          I_inputobject_type  IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : VALIDATE_SUPP_ATTR
--- Purpose               : Validates supplier attributes.
------------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPP_ATTR (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_supp_rec          IN      "RIB_SupAttr_REC",
                             I_inputobject_type  IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : VALIDATE_ADDRESS_RECORD
--- Purpose               : Validates address attributes.
------------------------------------------------------------------------------------
FUNCTION VALIDATE_ADDRESS_RECORD  (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_address_record    IN      "RIB_Addr_REC",
                                   I_inputobject_type  IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : VALIDATE_ORG_UNIT_RECORD
--- Purpose               : Validates org unit id.
------------------------------------------------------------------------------------
FUNCTION VALIDATE_ORG_UNIT_RECORD (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_org_unit_id    IN      ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : POPULATE_RECORD
--- Purpose               : IPopulates address data in SUPP_REC.
------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_supplier_record  IN OUT  SUPP_REC,
                          O_ref_message      IN OUT  "RIB_SupplierColRef_REC",
                          I_inputobject      IN      "RIB_SupplierColDesc_REC",
                          I_inputobject_type IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : POPULATE_SUPPLIER_DATA
--- Purpose               : Populates address data in SUPP_REC.SUPP_ATTR_TBL.
------------------------------------------------------------------------------------
FUNCTION POPULATE_SUPPLIER_DATA(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_inputobject          IN       "RIB_SupAttr_REC",
                                I_input_addr           IN       "RIB_Addr_REC",
                                O_supp_attr_tbl        IN OUT   SUPP_ATTR_TBL,
                                I_supp_xref_key        IN       VARCHAR2,
                                I_supp_site_xref_key   IN       VARCHAR2,
                                I_supplier_id          IN       SUPS.SUPPLIER%TYPE,
                                I_supplier_parent      IN       SUPS.SUPPLIER_PARENT%TYPE,
                                I_supp_parent_status   IN       SUPS.SUP_STATUS%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : POPULATE_ADDR_DATA
--- Purpose               : Populates address data in SUPP_REC.UPP_SITE_ADDR_TBL.
------------------------------------------------------------------------------------
FUNCTION POPULATE_ADDR_DATA(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_inputobject         IN      "RIB_Addr_REC",
                            O_supp_addr_tbl       IN OUT  SUPP_SITE_ADDR_TBL,
                            I_supplier_site_id    IN      SUPS.SUPPLIER%TYPE,
                            I_supp_site_xref_key  IN      VARCHAR2,
                            I_addr_key            IN      ADDR.ADDR_KEY%TYPE,
                            I_addr_xref_key       IN      VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : GET_NXT_SUPPLIER
--- Purpose               : Get the next supplier from SUPPLIER_SEQ.
------------------------------------------------------------------------------------
FUNCTION GET_NXT_SUPPLIER (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_supplier       IN OUT  SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : GET_NXT_ADDR_KEY
--- Purpose               : Get the next addr_key from ADDR_SEQ.
------------------------------------------------------------------------------------
FUNCTION GET_NXT_ADDR_KEY (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_addr_key       IN OUT  ADDR.ADDR_KEY%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : VALIDATE_LOCALIZED_ADDR
--- Purpose               : Validates if the country of the primary business address
---                         of the supplier is localized or not.
------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCALIZED_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_localized_ind   IN OUT   COUNTRY_ATTRIB.LOCALIZED_IND%TYPE,
                                 I_input_addr      IN       "RIB_Addr_REC")
RETURN BOOLEAN;
------------------------------------------------------------------------------------
--- Private Function Name : CHK_LOCALIZED_ATTR
--- Purpose               : Checks if localized attribut node is populated.
------------------------------------------------------------------------------------
FUNCTION CHK_LOCALIZED_ATTR(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_localized_attr_fnd   IN OUT   VARCHAR2,
                            I_inputobject          IN       "RIB_SupAttr_REC")
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
--- Private Function Name : VALIDATE_REQUIRED_ADDR_TYPES 
--- Purpose               : Validates if there are any missing addresses that are 
---                         mandatory. If yes, then existing address values are 
---                         copied into the mandatory addresses.
------------------------------------------------------------------------------------
FUNCTION VALIDATE_REQUIRED_ADDR_TYPES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_supplier      IN     SUPS.SUPPLIER%TYPE,
                                      IO_address_tbl  IN OUT SUPP_SITE_ADDR_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------
LP_system_options     SYSTEM_OPTIONS%ROWTYPE := NULL;

-------------------------------------------------------------------------------------
FUNCTION PROCESS_SUPPLIER_RECORD(O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_supplier_object   IN OUT  SUPP_REC,
                                 O_ref_outputobject  IN OUT  "RIB_SupplierColRef_REC",
                                 I_inputobject       IN      "RIB_SupplierColDesc_REC",
                                 I_inputobject_type  IN      VARCHAR2)
return BOOLEAN IS

   L_program VARCHAR2(64):= 'RMSSUB_SUPPLIER_VAL.PROCESS_SUPPLIER_RECORD';

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options) = FALSE then
      return FALSE;
   end if;

   -- Validates the incoming supplier data
   if VALIDATE_RECORD (O_error_message,
                       I_inputobject,
                       I_inputobject_type) = FALSE then
      return FALSE;
   end if;

   -- Populates the local record groups used for populating RMS tables.
   if POPULATE_RECORD (O_error_message,
                       O_supplier_object,
                       O_ref_outputobject,
                       I_inputobject,
                       I_inputobject_type) = FALSE then
      return FALSE;
   end if;

   -- Call localization layer.
   if not L10N_FLEX_API_SQL.VALIDATE_L10N_ATTRIB(O_error_message,
                                                 I_inputobject,
                                                 I_inputobject_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_SUPPLIER_RECORD;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_RECORD (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_inputobject       IN      "RIB_SupplierColDesc_REC",
                          I_inputobject_type  IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64):= 'RMSSUB_SUPPLIER_VAL.VALIDATE_RECORD';

   cursor C_CHECK_DUP_SUP_XREF_ID is
     SELECT SQL_LIB.CREATE_MSG('DUPLICATE_RECORD',sup_xref_key, NULL,NULL)
       FROM TABLE(CAST(I_inputobject.SupplierDesc_TBL AS "RIB_SupplierDesc_TBL"))
      WHERE sup_xref_key IS NOT NULL
      GROUP BY sup_xref_key
     HAVING count(sup_xref_key) > 1;

   ----
   cursor C_CHECK_DUP_SUPSITE_XREF_ID(C_index NUMBER) is
     SELECT SQL_LIB.CREATE_MSG('DUPLICATE_RECORD',supsite_xref_key, NULL,NULL)
       FROM TABLE(CAST(I_inputobject.SupplierDesc_TBL(C_index).SupSite_TBL AS "RIB_SupSite_TBL"))
      WHERE supsite_xref_key IS NOT NULL
      GROUP BY supsite_xref_key
     HAVING count(supsite_xref_key) > 1;

BEGIN

   if I_inputobject.SupplierDesc_TBL is not NULL and I_inputobject.SupplierDesc_TBL.COUNT > 0 then
      OPEN C_CHECK_DUP_SUP_XREF_ID;
      FETCH C_CHECK_DUP_SUP_XREF_ID INTO O_error_message;
      CLOSE C_CHECK_DUP_SUP_XREF_ID;

      if O_error_message IS NOT NULL then
         return FALSE;
      end if;
      --
      for s_index in I_inputobject.SupplierDesc_TBL.FIRST..I_inputobject.SupplierDesc_TBL.LAST
      loop
         if CHECK_ORPHAN(O_error_message,
                         I_inputobject.SupplierDesc_TBL(s_index).sup_xref_key,
                         NULL,
                         NULL,
                         I_inputobject.SupplierDesc_TBL(s_index).supplier_id,
                         NULL,
                         NULL,
                         I_inputobject_type,
                         'S') = FALSE then
            return FALSE;
         end if;

         if VALIDATE_SUPP_ATTR (O_error_message,
                                I_inputobject.SupplierDesc_TBL(s_index).SupAttr,
                                I_inputobject_type) = FALSE then
            return FALSE;
         end if;

         -- Supplier Site Loop
         if I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL is not NULL and I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL.COUNT > 0 then
            OPEN C_CHECK_DUP_SUPSITE_XREF_ID(s_index);
            FETCH C_CHECK_DUP_SUPSITE_XREF_ID INTO O_error_message;
            CLOSE C_CHECK_DUP_SUPSITE_XREF_ID;

            if O_error_message IS NOT NULL then
               return FALSE;
            end if;
            --
            for ss_index in I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL.FIRST..I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL.LAST
            loop
               if CHECK_ORPHAN(O_error_message,
                               I_inputobject.SupplierDesc_TBL(s_index).sup_xref_key,
                               I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).supsite_xref_key,
                               NULL,
                               I_inputobject.SupplierDesc_TBL(s_index).supplier_id,
                               I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).supplier_site_id,
                               NULL,
                               I_inputobject_type,
                               'U') = FALSE then
                  return FALSE;
               end if;

               if VALIDATE_SUPP_ATTR (O_error_message,
                                      I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupAttr,
                                      I_inputobject_type) = FALSE then
                  return FALSE;
               end if;

               -- Supplier Site org unit loop
               if I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL is not NULL and I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL.COUNT > 0 then
                  for ou_index in I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL.FIRST..I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL.LAST
                  loop
                     if VALIDATE_ORG_UNIT_RECORD (O_error_message,
                                                  I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL(ou_index).org_unit_id) = FALSE then
                        return FALSE;
                     end if;
                  end loop; -- End ofSupSiteOrgUnit_TBL loop.
               end if;

               -- Supplier Site Address loop
               if I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL is not NULL and I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL.COUNT > 0 then
                  for ad_index in I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL.FIRST..I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL.LAST
                  loop
                     if CHECK_ORPHAN(O_error_message,
                                     I_inputobject.SupplierDesc_TBL(s_index).sup_xref_key,
                                     I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).supsite_xref_key,
                                     I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).addr_xref_key,
                                     I_inputobject.SupplierDesc_TBL(s_index).supplier_id,
                                     I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).supplier_site_id,
                                     I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).addr_key,
                                     I_inputobject_type,
                                     'A') = FALSE then
                        return FALSE;
                     end if;
                     if VALIDATE_ADDRESS_RECORD(O_error_message,
                                                I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).Addr,
                                                I_inputobject_type) = FALSE then
                        return FALSE;
                     end if;
                  end loop;    -- End of SupSiteAddr_TBL loop
               end if;
            end loop;       -- End of SupSite_TBL loop
         end if;
      end loop;          -- End of supplier collection loop
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALIDATE_RECORD;
--------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD (O_error_message     IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_supplier_record   IN OUT  SUPP_REC,
                          O_ref_message       IN OUT  "RIB_SupplierColRef_REC",
                          I_inputobject       IN      "RIB_SupplierColDesc_REC",
                          I_inputobject_type  IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64):= 'RMSSUB_SUPPLIER_VAL.POPULATE_RECORD';

   L_supp_attr_tbl            SUPP_ATTR_TBL      := SUPP_ATTR_TBL();
   L_supp_site_attr_tbl       SUPP_ATTR_TBL      := SUPP_ATTR_TBL();
   L_supp_site_addr_tbl       SUPP_SITE_ADDR_TBL := SUPP_SITE_ADDR_TBL();
   L_supp_site_ou_tbl         SUPP_SITE_OU_TBL   := SUPP_SITE_OU_TBL();

   L_SupplierRef_REC            "RIB_SupplierRef_REC" := NULL;
   L_SupplierSite_REC           "RIB_SupplierSite_REC" := NULL;
   L_SupplierSiteAddress_REC    "RIB_SupplierSiteAddr_REC" := NULL;

   L_SupplierRef_TBL            "RIB_SupplierRef_TBL"         := "RIB_SupplierRef_TBL"();
   L_SupplierSite_TBL           "RIB_SupplierSite_TBL"        := "RIB_SupplierSite_TBL"();
   L_SupplierSiteAddress_TBL    "RIB_SupplierSiteAddr_TBL" := "RIB_SupplierSiteAddr_TBL"();

   L_supplier        SUPS.SUPPLIER%TYPE;
   L_supplier_site   SUPS.SUPPLIER%TYPE;
   L_addr_key        ADDR.ADDR_KEY%TYPE;
   L_input_addr_key  ADDR.ADDR_KEY%TYPE;   

   L_supp_xref_key     VARCHAR2(32);
   L_suppsite_xref_key VARCHAR2(32);
   L_addr_xref_key     VARCHAR2(32);
   L_collection_size   NUMBER := 0;
   L_supp_addr_pop_ind NUMBER := 0;
   L_loop_counter      NUMBER := 0;
   L_dumby_sups_id     NUMBER := -999;
   L_supp_add_type_ind VARCHAR2(1);
   L_supplier_status   SUPS.SUP_STATUS%TYPE;

   cursor C_CHECK_ADDR_KEY(C_supplier NUMBER, C_addr_type VARCHAR2, C_addr_ref_id VARCHAR2) is
    SELECT addr_key
      FROM addr
     WHERE key_value_1 = TO_CHAR(C_supplier)
       AND addr_type = C_addr_type
       AND module = 'SUPP'
    AND NVL(external_ref_id,'-999') = NVL(C_addr_ref_id,'-999')
       order by addr_key;

   cursor C_GET_SUP_ID(C_SUP VARCHAR2, C_SUP_PARENT NUMBER) is
      SELECT SUPPLIER
        FROM SUPS
       WHERE EXTERNAL_REF_ID                       = C_SUP
         AND NVL(SUPPLIER_PARENT, L_dumby_sups_id) = C_SUP_PARENT;

   cursor C_CHECK_SUP_ADDR(C_supplier NUMBER, C_addr_type VARCHAR2) is   
      SELECT 'Y'
        FROM addr
       WHERE key_value_1 = TO_CHAR(C_supplier)
         AND addr_type   = C_addr_type
         AND module      = 'SUPP' ;
     
BEGIN
   -- Supplier Loop
   for s_index in I_inputobject.SupplierDesc_TBL.FIRST..I_inputobject.SupplierDesc_TBL.LAST
   loop
      L_supplier := NULL;
      L_supp_xref_key := NULL;
      L_supp_addr_pop_ind := 0;
      ---
      L_supp_attr_tbl.EXTEND();
      if I_inputobject.SupplierDesc_TBL(s_index).supplier_id is NULL then
         if I_inputobject.SupplierDesc_TBL(s_index).sup_xref_key is NOT NULL then
            open C_GET_SUP_ID(I_inputobject.SupplierDesc_TBL(s_index).sup_xref_key, L_dumby_sups_id);
            fetch C_GET_SUP_ID into L_supplier;
            close C_GET_SUP_ID;
            --
         end if;
         --
         if L_supplier is NULL then
            if GET_NXT_SUPPLIER (O_error_message,
                                 L_supplier) = FALSE then
               return FALSE;
            end if;
         end if;
      else
         L_supplier := I_inputobject.SupplierDesc_TBL(s_index).supplier_id;
      end if;

      L_supp_xref_key := I_inputobject.SupplierDesc_TBL(s_index).sup_xref_key;
      L_supplier_status := I_inputobject.SupplierDesc_TBL(s_index).SupAttr.sup_status;

      if POPULATE_SUPPLIER_DATA(O_error_message,
                                I_inputobject.SupplierDesc_TBL(s_index).SupAttr,
                                NULL,
                                L_supp_attr_tbl,
                                L_supp_xref_key,
                                NULL,
                                L_supplier,
                                NULL,
                                L_supplier_status) = FALSE then
         return FALSE;
      end if;

      if I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL is not NULL and I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL.COUNT > 0 then
         -- Supplier Site Loop
         for ss_index in I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL.FIRST..I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL.LAST
         loop
            ---
            L_supplier_site := NULL;
            L_suppsite_xref_key := NULL;
            ---
            L_supp_site_attr_tbl.EXTEND();

            if I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).supplier_site_id is NULL then
               if I_inputobject.SupplierDesc_TBL(s_index).sup_xref_key is NOT NULL then
                  open C_GET_SUP_ID(I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).supsite_xref_key, L_supplier);
                  fetch C_GET_SUP_ID into L_supplier_site;
                  close C_GET_SUP_ID;
               end if;
                   --
               if L_supplier_site is NULL then
                  if GET_NXT_SUPPLIER (O_error_message,
                                       L_supplier_site) = FALSE then
                     return FALSE;
                  end if;
               end if;
            else
               L_supplier_site := I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).supplier_site_id;
            end if;

            L_suppsite_xref_key := I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).supsite_xref_key;
            
            L_supplier_status := L_supp_attr_tbl(s_index).sup_status;
            if POPULATE_SUPPLIER_DATA(O_error_message,
                                       I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupAttr,
                                       NULL,
                                       L_supp_site_attr_tbl,
                                       L_supp_xref_key,
                                       L_suppsite_xref_key,
                                       L_supplier_site,
                                       L_supplier,
                                       L_supplier_status) = FALSE then
               return FALSE;
            end if;

            if I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL is not NULL and I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL.COUNT > 0 then
               --- Supplier Site org unit loop
               for ou_index in I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL.FIRST..I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL.LAST
               loop

                  L_supp_site_ou_tbl.EXTEND();

                  L_supp_site_ou_tbl(L_supp_site_ou_tbl.COUNT)
                  := SUPP_SITE_OU_REC(L_suppsite_xref_key,
                                      L_supplier_site,
                                      I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL(ou_index).org_unit_id,
                                      I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteOrgUnit_TBL(ou_index).primary_pay_site);

               end loop; -- End ofSupSiteOrgUnit_TBL loop.
            end if;

            if (I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL is not NULL and I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL.COUNT > 0) then
               -- Supplier site address attribute loop
               for ad_index in I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL.FIRST..I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL.LAST
               loop
                  L_addr_key := NULL;
                  L_addr_xref_key := NULL;
                  L_input_addr_key := NULL;

                  L_supp_site_addr_tbl.EXTEND();

                  L_addr_xref_key := I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).addr_xref_key;
                  OPEN  C_CHECK_ADDR_KEY (L_supplier_site, I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).addr.addr_type,L_addr_xref_key);
                  FETCH C_CHECK_ADDR_KEY INTO L_input_addr_key;
                  CLOSE C_CHECK_ADDR_KEY ;

                  if L_input_addr_key is NOT NULL then
                     L_addr_key := L_input_addr_key;
                  else
                     if GET_NXT_ADDR_KEY(O_error_message,
                                         L_addr_key) = FALSE then
                        return FALSE;
                     end if;
                  end if;

                  if POPULATE_ADDR_DATA(O_error_message,
                                         I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).addr,
                                         L_supp_site_addr_tbl,
                                         L_supplier_site,
                                         L_suppsite_xref_key,
                                         L_addr_key,
                                         L_addr_xref_key) = FALSE then
                     return FALSE;
                  end if;

                  if (I_inputobject_type = RMSAIASUB_SUPPLIER.LP_sup_add and
                      I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).addr.addr_type = '01' and
                      I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).addr.primary_addr_ind = 'Y') then

                     if POPULATE_SUPPLIER_DATA(O_error_message,
                                               I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupAttr,
                                               I_inputobject.SupplierDesc_TBL(s_index).SupSite_TBL(ss_index).SupSiteAddr_TBL(ad_index).addr,
                                               L_supp_site_attr_tbl,
                                               L_supp_xref_key,
                                               L_suppsite_xref_key,
                                               L_supplier_site,
                                               L_supplier,
                                               L_supplier_status) = FALSE then
                         return FALSE;
                     end if;
                  end if;

                  L_SupplierSiteAddress_REC := "RIB_SupplierSiteAddr_REC"(0,
                                                                             nvl(L_addr_xref_key, 'NULL_FROM_AIA'),
                                                                             L_addr_key);

                  L_SupplierSiteAddress_TBL.EXTEND();

                  L_SupplierSiteAddress_TBL(ad_index) := L_SupplierSiteAddress_REC;
               end loop;    -- End of SupSiteAddr_TBL loop

               -- filling details for the missing mandatory addresses for the supplier site
               if VALIDATE_REQUIRED_ADDR_TYPES(O_error_message,
                                               L_supplier_site,
                                               L_supp_site_addr_tbl) = FALSE then
                  raise PROGRAM_ERROR;
               end if;

               if L_supp_addr_pop_ind != 1 and
                  I_inputobject_type = RMSAIASUB_SUPPLIER.LP_sup_add then

                   L_loop_counter := L_supp_site_addr_tbl.COUNT;

                   FOR i in 1 .. L_loop_counter LOOP
                      L_supp_add_type_ind := 'N';
                      OPEN  C_CHECK_SUP_ADDR(L_supplier, L_supp_site_addr_tbl(i).ADDR_TYPE);
                      FETCH C_CHECK_SUP_ADDR INTO L_supp_add_type_ind ;
                      CLOSE C_CHECK_SUP_ADDR ;     
                      --EBS always calls with LP_sup_add thus makes sure to update the existing addresses
                      if L_supp_add_type_ind ='N' then
                         L_addr_key := NULL;
                         OPEN  C_CHECK_ADDR_KEY (L_supplier, L_supp_site_addr_tbl(i).ADDR_TYPE,L_supp_site_addr_tbl(i).ADDR_XREF_KEY);
                         FETCH C_CHECK_ADDR_KEY INTO L_input_addr_key;
                         CLOSE C_CHECK_ADDR_KEY ;

                         if L_input_addr_key is NOT NULL then
                              L_addr_key := L_input_addr_key;
                         else
                             if GET_NXT_ADDR_KEY(O_error_message,
                                             L_addr_key) = FALSE then
                             return FALSE;
                             end if;
                         end if;
                        
                         L_supp_site_addr_tbl.EXTEND();
                         L_supp_site_addr_tbl(L_supp_site_addr_tbl.COUNT) := L_supp_site_addr_tbl(i);
                         L_supp_site_addr_tbl(L_supp_site_addr_tbl.COUNT).key_value_1 := L_supplier;
                         L_supp_site_addr_tbl(L_supp_site_addr_tbl.COUNT).addr_key := L_addr_key;
                       end if;
                   END LOOP;
                   L_supp_addr_pop_ind := 1;
                end if;
             end if;

            L_SupplierSite_REC := "RIB_SupplierSite_REC"(0,
                                                         L_suppsite_xref_key,
                                                         L_supplier_site,
                                                         L_SupplierSiteAddress_TBL);
            L_SupplierSite_TBL.EXTEND();
            L_SupplierSite_TBL(ss_index) := L_SupplierSite_REC;
            L_SupplierSiteAddress_TBL := "RIB_SupplierSiteAddr_TBL"();
         end loop;       -- End of SupSite_TBL loop
      end if;
      L_SupplierRef_REC := "RIB_SupplierRef_REC"(0,
                                                   L_supp_xref_key,
                                                   L_supplier,
                                                   L_SupplierSite_TBL);

      L_SupplierRef_TBL.EXTEND();
      L_SupplierRef_TBL(s_index) := L_SupplierRef_REC;
      L_SupplierSite_TBL := "RIB_SupplierSite_TBL"();
      L_collection_size := L_collection_size + 1;
   end loop;          -- End of supplier collection loop

   O_supplier_record := SUPP_REC(L_supp_attr_tbl,
                                  L_supp_site_attr_tbl,
                                  L_supp_site_addr_tbl,
                                  L_supp_site_ou_tbl);

   O_ref_message := "RIB_SupplierColRef_REC"(0,
                                             L_collection_size,
                                             L_SupplierRef_TBL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END POPULATE_RECORD;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_REQUIRED_ADDR_TYPES(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_supplier      IN     SUPS.SUPPLIER%TYPE,
                                      IO_address_tbl  IN OUT SUPP_SITE_ADDR_TBL)
return BOOLEAN IS

   CURSOR c_get_missing_addr_type IS
 SELECT  atm.address_type
       FROM add_type_module atm,
            add_type at
      WHERE atm.module = 'SUPP'
        AND atm.address_type = at.address_type
        AND ((atm.mandatory_ind = 'Y' 
        AND not exists (SELECT 'x'
                          FROM addr
                         WHERE key_value_1 = TO_CHAR(I_supplier)
                           AND addr_type   = atm.address_type
                           AND module      = 'SUPP'))
            OR at.external_addr_ind = 'Y')
       MINUS
     SELECT addr_type
       FROM TABLE(CAST(IO_address_tbl AS SUPP_SITE_ADDR_TBL)) SAT
      WHERE key_value_1 = to_char(I_supplier);

   cursor C_CHECK_ADDR_KEY(C_supplier NUMBER, C_addr_type VARCHAR2) is
    SELECT min(addr_key)
      FROM addr
     WHERE key_value_1 = TO_CHAR(C_supplier)
       AND addr_type   = C_addr_type
       AND module      = 'SUPP';

   L_missing_type ADDRESS_TYPE_DATA;
   L_address_tbl_index NUMBER := 0;
   L_default_address_index NUMBER := -1;
   L_addr_key      ADDR.ADDR_KEY%TYPE;

BEGIN

   L_addr_key := NULL;
   OPEN c_get_missing_addr_type;
   FETCH c_get_missing_addr_type BULK COLLECT INTO L_missing_type;
   CLOSE c_get_missing_addr_type;

   if L_missing_type.COUNT > 0 then   
      -- Find the address index to default from if missing addresses are found.  If an Ordering
      -- address has been passed, default addresses from the Ordering address.  If an Ordering
      -- address has not been passed, and a Remittance address has, default addresses from the
      -- Remittance address.  If neither Ordering nor Remittance address has been passed, default
      -- addresses from the first address sent.
      -- Modified for HPQC 79
      FOR a IN IO_address_tbl.FIRST .. IO_address_tbl.LAST LOOP 
         if (IO_address_tbl(a).addr_type = '04' and IO_address_tbl(a).key_value_1 = to_char(I_supplier)) or 
            (L_default_address_index = -1 and IO_address_tbl(a).addr_type = '06' and IO_address_tbl(a).key_value_1 = to_char(I_supplier)) then
            L_default_address_index := a;
         end if;
      END LOOP;

      if L_default_address_index = -1 then
         L_default_address_index := 1;
      end if;

      -- Check to see if any missing address type has just been passed into this API
      FOR b IN L_missing_type.FIRST .. L_missing_type.LAST LOOP
         
            if IO_address_tbl.COUNT >= 1 then
               IO_address_tbl.EXTEND();
               L_address_tbl_index := IO_address_tbl.COUNT;
               
               IO_address_tbl(L_address_tbl_index) := IO_address_tbl(L_default_address_index);

               L_addr_key := NULL;
               OPEN  C_CHECK_ADDR_KEY (I_supplier, L_missing_type(b));
               FETCH C_CHECK_ADDR_KEY INTO L_addr_key;
               CLOSE C_CHECK_ADDR_KEY ;
               
               if L_addr_key is NULL then
                  if GET_NXT_ADDR_KEY(O_error_message,
                                      L_addr_key) = FALSE then
                     return FALSE;
                  end if;
               end if;

               IO_address_tbl(L_address_tbl_index).addr_key := L_addr_key;
               IO_address_tbl(L_address_tbl_index).addr_type := L_missing_type(b);
            end if;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'RMSSUB_SUPPLIER.VALIDATE_REQUIRED_ADDR_TYPES',
                                             to_char(SQLCODE));
      return FALSE;

END VALIDATE_REQUIRED_ADDR_TYPES;
--------------------------------------------------------------------------------------
FUNCTION VALIDATE_ADDRESS_RECORD  (O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_address_record    IN     "RIB_Addr_REC",
                                   I_inputobject_type  IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64):= 'RMSSUB_SUPPLIER_VAL.VALIDATE_ADDRESS_RECORD';
   L_address_record  "RIB_Addr_REC" := I_address_record;

BEGIN

   if CHECK_NULLS(O_error_message,
                  L_address_record.addr_type,
                  'ADDR_TYPE') = FALSE then
       return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  L_address_record.add_1,
                  'ADD_1') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  L_address_record.city,
                  'CITY') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  L_address_record.country_id,
                  'COUNTRY_ID') = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_CODES(O_error_message,
                  L_address_record.primary_addr_ind,
                  'YSNO') = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_FKEYS(O_error_message,
                  L_address_record.addr_type,
                  'address_type',
                  'add_type') = FALSE  then
      return FALSE;
   end if;

   if CHECK_FKEYS(O_error_message,
                  L_address_record.state,
                  'state',
                  'state') = FALSE then
      return FALSE;
   end if;

   if CHECK_FKEYS(O_error_message,
                  L_address_record.country_id,
                  'country_id',
                  'country') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END VALIDATE_ADDRESS_RECORD;
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_SUPP_ATTR (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_supp_rec          IN "RIB_SupAttr_REC",
                             I_inputobject_type  IN VARCHAR2)
RETURN BOOLEAN IS

   L_program               VARCHAR2(64):= 'RMSSUB_SUPPLIER.VALIDATE_SUPP_ATTR';

BEGIN

   if CHECK_NULLS(O_error_message,
                  I_supp_rec.sup_name,
                  'SUP_NAME') = FALSE then
      return FALSE;
   end if;

   if CHECK_CODES(O_error_message,
                  I_supp_rec.sup_status,
                 'SPST') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  I_supp_rec.contact_name,
                 'CONTACT_NAME') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  I_supp_rec.contact_phone,
                  'CONTACT_PHONE') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  I_supp_rec.currency_code,
                  'CURRENCY_CODE') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  I_supp_rec.terms,
                  'TERMS') = FALSE then
      return FALSE;
   end if;

   if CHECK_NULLS(O_error_message,
                  I_supp_rec.freight_terms,
                  'FREIGHT_TERMS') = FALSE then
      return FALSE;
   end if;
   ---
   if CHECK_FKEYS(O_error_message,
                  I_supp_rec.freight_terms,
                  'freight_terms',
                  'freight_terms') = FALSE then
      return FALSE;
   end if;

   if CHECK_FKEYS(O_error_message,
                  I_supp_rec.currency_code,
                  'currency_code',
                  'currencies') = FALSE  then
      return FALSE;
   end if;

   if CHECK_FKEYS(O_error_message,
                  I_supp_rec.lang,
                  'lang',
                  'lang') = FALSE then
      return FALSE;
   end if;

   if CHECK_FKEYS(O_error_message,
                  I_supp_rec.terms,
                  'terms',
                  'terms') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_SUPP_ATTR;
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_ORG_UNIT_RECORD (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_org_unit_id    IN      ORG_UNIT.ORG_UNIT_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSSUB_SUPPLIER_VAL.VALIDATE_ORG_UNIT_RECORD';

BEGIN

   if CHECK_NULLS(O_error_message,
                     I_org_unit_id,
                     'ORG_UNIT_ID') = FALSE then
      return FALSE;
   end if;

   if CHECK_FKEYS(O_error_message,
                  I_org_unit_id,
                  'ORG_UNIT_ID',
                  'ORG_UNIT') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_ORG_UNIT_RECORD;
-------------------------------------------------------------------------------------
FUNCTION CHECK_FKEYS (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable  IN      VARCHAR2,
                      I_fkey_column      IN      VARCHAR2,
                      I_fkey_table       IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64):= 'RMSAIASUB_SUPPLIER_VALIDATE.CHECK_FKEYS';
   V_block_string VARCHAR2(255) := NULL;
   V_cursor INTEGER := NULL;

BEGIN
   if I_record_variable is NOT NULL then
      V_block_string := 'select ''x'' from '
                                 || sys.DBMS_ASSERT.SQL_OBJECT_NAME(I_fkey_table)
                                 || ' where '
                                 || sys.DBMS_ASSERT.SIMPLE_SQL_NAME(I_fkey_column )
                                 || ' = :l_record_variable';
      V_cursor := DBMS_SQL.OPEN_CURSOR;

      DBMS_SQL.PARSE(V_cursor, V_block_string, DBMS_SQL.V7);
      DBMS_SQL.BIND_VARIABLE(V_cursor, 'l_record_variable', I_record_variable);
      if DBMS_SQL.EXECUTE_AND_FETCH(V_cursor) = 0 then
         DBMS_SQL.CLOSE_CURSOR(V_cursor);
            O_error_message := sql_lib.create_msg('INVALID_FKEY',
                                                  I_record_variable,
                                                  I_fkey_column,
                                                  I_fkey_table);
            return FALSE;
      else
         DBMS_SQL.CLOSE_CURSOR(V_cursor);
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FKEYS;
-------------------------------------------------------------------------------------
FUNCTION CHECK_NULLS (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable  IN      VARCHAR2,
                      I_record_name      IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.CHECK_NULLS';

BEGIN

   if I_record_variable is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_record_name,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_NULLS;
-------------------------------------------------------------------------------------
FUNCTION CHECK_CODES (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_record_variable  IN      VARCHAR2,
                      I_code_value       IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.CHECK_CODES';
   L_exists VARCHAR2(1) := NULL;

   cursor C_CODES is
      select 'x'
        from code_detail
       where code_type = I_code_value
         and code = I_record_variable;

BEGIN

   if I_record_variable is NOT NULL then
      open C_CODES;
      fetch C_CODES into L_exists;
         if C_CODES%NOTFOUND then
            close C_CODES;
            O_error_message := sql_lib.create_msg('INV_CODE',
                                                  I_record_variable,
                                                  I_code_value,
                                                  L_program);
            return FALSE;
         end if;
      close C_CODES;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_CODES;
-------------------------------------------------------------------------------------
FUNCTION CHECK_ORPHAN(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_supp_xref_key       IN      VARCHAR2,
                      I_supp_site_xref_key  IN      VARCHAR2,
                      I_addr_xref_key       IN      VARCHAR2,
                      I_supplier_id         IN      SUPS.SUPPLIER%TYPE,
                      I_supp_site_id        IN      SUPS.SUPPLIER%TYPE,
                      I_addr_key            IN      ADDR.ADDR_KEY%TYPE,
                      I_inputobject_type    IN      VARCHAR2,
                      I_check_ind           IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.CHECK_ORPHAN';

BEGIN

   if I_inputobject_type = RMSAIASUB_SUPPLIER.LP_sup_add then
      if I_check_ind = 'S' then
         if I_supp_xref_key is NULL then
             O_error_message := SQL_LIB.CREATE_MSG('NULL_SUPPLIER_XREF',
                                                   'Sup_xref_key',
                                                   NULL,
                                                   NULL);
             return FALSE;
         end if;
      elsif I_check_ind = 'U' then
         if (I_supp_xref_key is NULL or I_supp_site_xref_key is NULL) then
            O_error_message := SQL_LIB.CREATE_MSG('NULL_SUPPLIER_XREF',
                                                  'Sup_xref_key/Supsite_xref_key',
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      elsif I_check_ind = 'A' then
         if (I_supp_site_xref_key is NULL) then
            O_error_message := SQL_LIB.CREATE_MSG('NULL_SUPPLIER_XREF',
                                                  'Supsite_xref_key',
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;
   else
      if I_supplier_id is NULL and
         I_supp_xref_key is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('NULL_SUPPLIER',
                                                  'Supplier_id',
                                                  NULL,
                                                  NULL);
            return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_ORPHAN;
-------------------------------------------------------------------------------------
FUNCTION GET_NXT_SUPPLIER (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_supplier       IN OUT  SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.GET_NXT_SUPPLIER';

   cursor C_GET_NXT_SUPP IS
      select SUPPLIER_SEQ.NEXTVAL
        from dual;

BEGIN

   open C_GET_NXT_SUPP;
   fetch C_GET_NXT_SUPP into O_supplier;
   close C_GET_NXT_SUPP;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END GET_NXT_SUPPLIER;
-------------------------------------------------------------------------------------
FUNCTION GET_NXT_ADDR_KEY (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_addr_key       IN OUT  ADDR.ADDR_KEY%TYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.GET_NXT_ADDR_KEY';

   cursor C_GET_NXT_ADDR_KEY IS
      select ADDR_SEQUENCE.NEXTVAL
        from dual;

BEGIN

   open C_GET_NXT_ADDR_KEY;
   fetch C_GET_NXT_ADDR_KEY into O_addr_key;
   close C_GET_NXT_ADDR_KEY;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NXT_ADDR_KEY;
-------------------------------------------------------------------------------------
FUNCTION POPULATE_SUPPLIER_DATA(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_inputobject          IN       "RIB_SupAttr_REC",
                                I_input_addr           IN       "RIB_Addr_REC",
                                O_supp_attr_tbl        IN OUT   SUPP_ATTR_TBL,
                                I_supp_xref_key        IN       VARCHAR2,
                                I_supp_site_xref_key   IN       VARCHAR2,
                                I_supplier_id          IN       SUPS.SUPPLIER%TYPE,
                                I_supplier_parent      IN       SUPS.SUPPLIER_PARENT%TYPE,
                                I_supp_parent_status   IN       SUPS.SUP_STATUS%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.POPULATE_SUPPLIER_DATA';

   L_vat_region         VAT_REGION.VAT_REGION%TYPE                      := NULL;
   L_vat_region_sups    SUPS.VAT_REGION%TYPE                            := NULL;
   L_dept_lvl_ord       PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE := NULL;
   L_inv_mgmt_lvl       SUPS.INV_MGMT_LVL%TYPE                          := NULL;
   L_localized_ind      COUNTRY_ATTRIB.LOCALIZED_IND%TYPE               := NULL;
   L_localized_attr_fnd VARCHAR2(1)                         := 'N';
   L_sup_status         SUPS.SUP_STATUS%TYPE                := NULL;
   L_inactive           SUPS.SUP_STATUS%TYPE                := 'I';
   L_active             SUPS.SUP_STATUS%TYPE                := 'A';
   L_mandatory_ind      ADD_TYPE_MODULE.MANDATORY_IND%TYPE  := NULL;
   L_supplier_rec       "RIB_SupAttr_REC"    := i_inputobject;

   cursor C_SUPPLIER is
      select DECODE(NVL(status_upd_by_rms,'N'),'Y',sup_status,NVL(I_inputobject.sup_status,'A')),vat_region
        from sups
       where supplier = I_supplier_id;

   cursor C_VAT_REGION(I_vat_region VAT_REGION.VAT_REGION%TYPE) is
      select vat_region
        from vat_region
       where vat_region = I_vat_region;

   cursor C_GET_DEPT_LVL_ORD is
      select dept_level_orders
        from procurement_unit_options;
   
   cursor C_ADDR_MAND_IND is
      select mandatory_ind 
        from add_type_module 
       where address_type=03 
         and module='SUPP';

BEGIN

   if LP_system_options.default_tax_type = 'SVAT' then
      L_vat_region := I_inputobject.vat_region;

      open C_VAT_REGION(L_vat_region);
      fetch C_VAT_REGION into L_vat_region;

      if C_VAT_REGION%NOTFOUND then
         L_vat_region := NULL;
      end if;

      close C_VAT_REGION;
   else ---for Default_tax_type 'SALES' and 'GTAX'
      L_vat_region := NULL;
   end if;

   open C_GET_DEPT_LVL_ORD;
   fetch C_GET_DEPT_LVL_ORD into L_dept_lvl_ord;
   close C_GET_DEPT_LVL_ORD;
   ---
   if L_dept_lvl_ord = 'Y' then
      L_inv_mgmt_lvl := 'D';
   else
      L_inv_mgmt_lvl := 'S';
   end if;

   open C_SUPPLIER;
   fetch C_SUPPLIER into L_supplier_rec.sup_status,L_vat_region_sups;
   close C_SUPPLIER;
   
   if LP_system_options.default_tax_type = 'SVAT'
   and L_vat_region_sups is NULL
   and L_vat_region is NULL then
       L_supplier_rec.sup_status := L_inactive;
   end if;
   
   open C_ADDR_MAND_IND;
   fetch C_ADDR_MAND_IND into L_mandatory_ind;
   close C_ADDR_MAND_IND;


   if I_input_addr is NOT NULL then
      -- check localized_ind of address
      if VALIDATE_LOCALIZED_ADDR(O_error_message,
                                 L_localized_ind,
                                 I_input_addr) = FALSE then
         return FALSE;
      end if;
      -- check if localised node is populated
         if CHK_LOCALIZED_ATTR(O_error_message,
                               L_localized_attr_fnd,
                               I_inputobject) = FALSE then
            return FALSE;
         end if;
      -- update supplier status based on the localized_ind
      if L_localized_ind      = 'Y' and 
         L_localized_attr_fnd = 'N' then
         L_supplier_rec.sup_status := L_inactive;
      end if;
   end if;
   --If the supplier parent status is Inactive then set all supplier sites to inactive
   if I_supp_parent_status     = L_inactive and
      I_inputobject.sup_status = L_active then
      L_supplier_rec.sup_status := L_inactive;
   end if;
   if I_input_addr.addr_type = 03 or L_mandatory_ind ='Y' then
      L_supplier_rec.ret_allow_ind := 'Y';
   else
      L_supplier_rec.ret_allow_ind := 'N';
   end if;
   O_supp_attr_tbl(O_supp_attr_tbl.COUNT) := SUPP_ATTR_REC(I_supp_xref_key,
                                                           I_supp_site_xref_key,
                                                           to_number(I_supplier_id),
                                                           I_supplier_parent,
                                                           I_inputobject.sup_name,
                                                           I_inputobject.sup_name_secondary,
                                                           NVL(I_inputobject.contact_name,'UNKNOWN'),
                                                           NVL(I_inputobject.contact_phone,1),
                                                           I_inputobject.contact_fax,
                                                           I_inputobject.contact_pager,
                                                           L_supplier_rec.sup_status,
                                                           NVL(I_inputobject.qc_ind, 'N'),
                                                           I_inputobject.qc_pct,
                                                           I_inputobject.qc_freq,
                                                           NVL(I_inputobject.vc_ind,'N'),
                                                           I_inputobject.vc_pct,
                                                           I_inputobject.vc_freq,
                                                           I_inputobject.currency_code,
                                                           I_inputobject.lang,
                                                           I_inputobject.terms,
                                                           I_inputobject.freight_terms,
                                                           NVL(I_inputobject.ret_allow_ind,L_supplier_rec.ret_allow_ind),
                                                           NVL(I_inputobject.ret_auth_req,'N'),
                                                           I_inputobject.ret_min_dol_amt,
                                                           I_inputobject.ret_courier,
                                                           I_inputobject.handling_pct,
                                                           NVL(I_inputobject.edi_po_ind,'N'),
                                                           NVL(I_inputobject.edi_po_chg,'N'),
                                                           NVL(I_inputobject.edi_po_confirm,'N'),
                                                           NVL(I_inputobject.edi_asn,'N'),
                                                           CASE WHEN NVL(I_inputobject.edi_sales_rpt_freq,'N') IN ('D','W') THEN
                                                                I_inputobject.edi_sales_rpt_freq
                                                           ELSE
                                                                NULL
                                                           END ,     -- edi_sales_rpt_freq
                                                           NVL(I_inputobject.edi_supp_available_ind,'N'),
                                                           NVL(I_inputobject.edi_contract_ind,'N'),
                                                           NVL(I_inputobject.edi_invc_ind,'N'),
                                                           I_inputobject.edi_channel_ind,
                                                           I_inputobject.cost_chg_pct_var,
                                                           I_inputobject.cost_chg_amt_var,
                                                           NVL(I_inputobject.replen_approval_ind,'N'),
                                                           I_inputobject.ship_method,
                                                           I_inputobject.payment_method,
                                                           I_inputobject.contact_telex,
                                                           I_inputobject.contact_email,
                                                           NVL(I_inputobject.settlement_code,'N'),
                                                           NVL(I_inputobject.pre_mark_ind,'N'),
                                                           NVL(I_inputobject.auto_appr_invc_ind,'N'),      -- auto_appr_invc_ind
                                                           NVL(I_inputobject.dbt_memo_code,'N'),           -- dbt_memo_code
                                                           NVL(I_inputobject.freight_charge_ind,'N'),      -- freight_charge_ind
                                                           NVL(I_inputobject.auto_appr_dbt_memo_ind,'N'),  -- auto_appr_dbt_memo_ind
                                                           NVL(I_inputobject.prepay_invc_ind,'N'),         -- prepay_invc_ind
                                                           NVL(I_inputobject.backorder_ind,'N'),           -- backorder_ind
                                                           L_vat_region,
                                                           L_inv_mgmt_lvl,
                                                           NVL(I_inputobject.service_perf_req_ind,'N'),    -- service_perf_req_ind
                                                           I_inputobject.invc_pay_loc,
                                                           I_inputobject.invc_receive_loc,
                                                           NVL(I_inputobject.addinvc_gross_net,'N'),       -- addinvc_gross_net
                                                           NVL(I_inputobject.delivery_policy,'NEXT'),      -- delivery_policy
                                                           I_inputobject.comment_desc,
                                                           I_inputobject.default_item_lead_time,
                                                           I_inputobject.duns_number,
                                                           I_inputobject.duns_loc,
                                                           NVL(I_inputobject.bracket_costing_ind,'N'),     -- bracket_costing_ind
                                                           I_inputobject.vmi_order_status,                 -- vmi order status
                                                           NVL(I_inputobject.dsd_ind,'N'),                 -- dsd ind
                                                           NVL(I_inputobject.scale_aip_orders,'N'),
                                                           NVL(I_inputobject.sup_qty_level, 'EA'));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_SUPPLIER_DATA;
-------------------------------------------------------------------------------------
FUNCTION POPULATE_ADDR_DATA(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            I_inputobject         IN      "RIB_Addr_REC",
                            O_supp_addr_tbl       IN OUT  SUPP_SITE_ADDR_TBL,
                            I_supplier_site_id    IN      SUPS.SUPPLIER%TYPE,
                            I_supp_site_xref_key  IN      VARCHAR2,
                            I_addr_key            IN      ADDR.ADDR_KEY%TYPE,
                            I_addr_xref_key       IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.POPULATE_ADDR_DATA';

   L_primary_addr_ind  ADDR.PRIMARY_ADDR_IND%TYPE := NULL;

   cursor C_LOCK_PRIMARY_ADDR is
      select 'x'
        from addr
       where key_value_1 = to_char(I_supplier_site_id)
         and addr_type   = I_inputobject.addr_type
         and module      = 'SUPP'
         for update nowait;

BEGIN

   if I_inputobject.primary_addr_ind = 'Y' then
      open C_LOCK_PRIMARY_ADDR;
      close C_LOCK_PRIMARY_ADDR;
      ---
      update addr
         set primary_addr_ind = 'N'
       where key_value_1 = to_char(I_supplier_site_id)
         and addr_type   = I_inputobject.addr_type
         and module      = 'SUPP';
   end if;

   L_primary_addr_ind := I_inputobject.primary_addr_ind;

   if I_inputobject.primary_addr_ind IS NULL then
      L_primary_addr_ind := 'N';
   end if;

   O_supp_addr_tbl(O_supp_addr_tbl.COUNT) := ADDR_TYPE_REC(I_supp_site_xref_key,
                                                           I_supplier_site_id,
                                                           I_addr_xref_key,
                                                           I_addr_key,
                                                           I_inputobject.addr_type,
                                                           L_primary_addr_ind,
                                                           I_inputobject.add_1,
                                                           I_inputobject.add_2,
                                                           I_inputobject.add_3,
                                                           I_inputobject.city,
                                                           I_inputobject.state,
                                                           I_inputobject.country_id,
                                                           I_inputobject.jurisdiction_code,
                                                           I_inputobject.post,
                                                           I_inputobject.contact_name,
                                                           I_inputobject.contact_phone,
                                                           I_inputobject.contact_fax,
                                                           I_inputobject.contact_email);
   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END POPULATE_ADDR_DATA;
-------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCALIZED_ADDR(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_localized_ind   IN OUT   COUNTRY_ATTRIB.LOCALIZED_IND%TYPE,
                                 I_input_addr      IN       "RIB_Addr_REC")
RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.VALIDATE_LOCALIZED_ADDR';

   L_addr_country   "RIB_Addr_REC" := I_input_addr;

   cursor C_GET_LOCALIZED_IND is
      select localized_ind
        from country_attrib ca
       where L_addr_country.country_id = ca.country_id
         and L_addr_country.addr_type = '01'
         and L_addr_country.primary_addr_ind = 'Y';

BEGIN

   L_addr_country := I_input_addr;

   open C_GET_LOCALIZED_IND;
   fetch C_GET_LOCALIZED_IND into O_localized_ind;
   close C_GET_LOCALIZED_IND;

   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALIDATE_LOCALIZED_ADDR;
-------------------------------------------------------------------------------------
FUNCTION CHK_LOCALIZED_ATTR(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_localized_attr_fnd   IN OUT   VARCHAR2,
                            I_inputobject          IN       "RIB_SupAttr_REC")
RETURN BOOLEAN IS
                            
   L_program VARCHAR2(64) := 'RMSAIASUB_SUPPLIER_VALIDATE.CHK_LOCALIZED_ATTR';

BEGIN
   if I_inputobject.LocOfSupAttr_TBL is not NULL and I_inputobject.LocOfSupAttr_TBL.COUNT >0 then
      if I_inputobject.LocOfSupAttr_TBL(I_inputobject.LocOfSupAttr_TBL.FIRST).InSupAttr_TBL is not NULL or
         I_inputobject.LocOfSupAttr_TBL(I_inputobject.LocOfSupAttr_TBL.FIRST).BrSupAttr_TBL is not NULL then
         O_localized_attr_fnd := 'Y';
      else
         O_localized_attr_fnd := 'N';
      end if;
   else 
      O_localized_attr_fnd := 'N';
   end if;
      
   return TRUE;

EXCEPTION
    when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHK_LOCALIZED_ATTR;
-------------------------------------------------------------------------------------
END RMSAIASUB_SUPPLIER_VALIDATE;
/