create or replace PACKAGE BODY PM_API_SQL IS
--------------------------------------------------------------------------------
-- Local Package Variables
--------------------------------------------------------------------------------
LP_rpm_system_options_rec        PM_API_SQL.RPM_SYSTEM_OPTIONS_REC;
LP_rpm_so_rec_populated          BOOLEAN := FALSE;
LP_temp_tables_populated         BOOLEAN := FALSE;

--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(61) := 'PM_API_SQL.POPULATE_GTT';
   L_exists_promo          VARCHAR2(1)  := 'N';
   L_exists_promo_comp     VARCHAR2(1)  := 'N';
   L_success               VARCHAR2(1);
   L_obj_promo_table       OBJ_PROMO_TBL;
   L_obj_promo_comp_table  OBJ_PROMO_COMP_TBL;

   cursor C_GTT_PROMO is
      select 'Y'
        from gtt_promo_temp
       where rownum = 1;

   cursor C_GTT_PROMO_COMP is
      select 'Y'
        from gtt_promo_comp_temp
       where rownum = 1;

BEGIN

   open  C_GTT_PROMO;
   fetch C_GTT_PROMO into L_exists_promo;
   close C_GTT_PROMO;

   open  C_GTT_PROMO_COMP;
   fetch C_GTT_PROMO_COMP into L_exists_promo_comp;
   close C_GTT_PROMO_COMP;

   LP_temp_tables_populated := (L_exists_promo = 'Y');

   if LP_temp_tables_populated = FALSE then

      --- Call pricing modules to get objects
      MERCH_API_SQL.GET_PROMOS(O_error_message,
                               L_success,
                               L_obj_promo_table);
      if L_success = 'N' then
         return FALSE;
      end if;
      ---

      --- Move stuff from pricing objects into the global temp tables
      insert into gtt_promo_temp
           select *
             from TABLE(cast(L_obj_promo_table as OBJ_PROMO_TBL));
      ---

      LP_temp_tables_populated := TRUE;

   end if;

   LP_temp_tables_populated := (L_exists_promo_comp = 'Y');

   if LP_temp_tables_populated = FALSE then

      MERCH_API_SQL.GET_PROMO_COMPS(O_error_message,
                                    L_success,
                                    L_obj_promo_comp_table);
      if L_success = 'N' then
         return FALSE;
      end if;
      ---
      insert into gtt_promo_comp_temp
           select *
             from TABLE(cast(L_obj_promo_comp_table as OBJ_PROMO_COMP_TBL));

      LP_temp_tables_populated := TRUE;

    end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END POPULATE_GTT;

--------------------------------------------------------------------------------
FUNCTION GET_RPM_SYSTEM_OPTIONS(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_rpm_system_options_rec  IN OUT  PM_API_SQL.RPM_SYSTEM_OPTIONS_REC)
RETURN BOOLEAN IS

   L_program                     VARCHAR2(61) := 'PM_API_SQL.GET_RPM_SYSTEM_OPTIONS';
   L_success                     VARCHAR2(1);
   L_obj_rpm_system_options_rec  OBJ_RPM_SYSTEM_OPTIONS_REC;

BEGIN

   if LP_rpm_so_rec_populated = FALSE then
      MERCH_API_SQL.GET_RPM_SYSTEM_OPTIONS(O_error_message,
                                           L_success,
                                           L_obj_rpm_system_options_rec);
      if L_success = 'N' then
         return FALSE;
      end if;

      -- Copy all RPM system options values into output record
      LP_rpm_system_options_rec.complex_promo_allowed_ind := L_obj_rpm_system_options_rec.complex_promo_allowed_ind;

      LP_rpm_so_rec_populated := TRUE;
   end if;

   O_rpm_system_options_rec := LP_rpm_system_options_rec;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RPM_SYSTEM_OPTIONS;

--------------------------------------------------------------------------------
FUNCTION GET_PROMOTION(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_promo_display_id IN      GTT_PROMO_TEMP.PROMO_DISPLAY_ID%TYPE,
                       O_promo_id         IN OUT  SA_TRAN_DISC.PROMOTION%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.GET_PROMOTION';

   cursor C_PROMOTION is
      select promo_id 
        from gtt_promo_temp
       where promo_display_id = I_promo_display_id;

BEGIN
   if I_promo_display_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_promo_display_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   
   open C_PROMOTION;
   fetch C_PROMOTION into O_promo_id;
   if C_PROMOTION%NOTFOUND then
      close C_PROMOTION;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PROMO');
      return FALSE;
   else
      close C_PROMOTION;
      return TRUE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PROMOTION;

--------------------------------------------------------------------------------
FUNCTION GET_PROMO_DISPLAY_ID(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_promo_id         IN      GTT_PROMO_TEMP.PROMO_ID%TYPE,
                              O_promo_display_id IN OUT  SA_TRAN_DISC.PROMOTION%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.GET_PROMO_DISPLAY_ID';
      
   cursor C_PROMO_DISPLAY_ID is
      select promo_display_id 
        from gtt_promo_temp
       where promo_id = I_promo_id;

BEGIN
   if I_promo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_promo_id',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   
   open C_PROMO_DISPLAY_ID;
   fetch C_PROMO_DISPLAY_ID into O_promo_display_id;
   if C_PROMO_DISPLAY_ID%NOTFOUND then
      close C_PROMO_DISPLAY_ID;
      O_error_message := SQL_LIB.CREATE_MSG('INV_PROMO');
      return FALSE;
   else
      close C_PROMO_DISPLAY_ID;
      return TRUE;
   end if;
    
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PROMO_DISPLAY_ID;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMOTION(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid          IN OUT  VARCHAR2,
                            O_promo_rec      IN OUT  PM_API_SQL.PROMOTION_REC,
                            I_check_status   IN      VARCHAR2,
                            I_promo_id       IN      SA_TRAN_DISC.PROMOTION%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.VALIDATE_PROMO';
   L_invalid_param  VARCHAR2(30);
   L_success        VARCHAR2(1);
   L_obj_promo_rec  OBJ_PROMO_REC;

   cursor C_VALIDATE is
      select 'Y',
             promo_id,
             promo_display_id,
             state,
             name,
             currency_code,
             promo_event_id,
             start_date,
             end_date
        from gtt_promo_temp
       where promo_id = I_promo_id
         and ((end_date >= (select vdate
                              from period))
          or (end_date is NULL))
         and rownum = 1;

BEGIN

   --- Initialize variables
   O_valid         := 'N';
   O_promo_rec := NULL;

   --- Validate parameters
   if I_promo_id is NULL then
      L_invalid_param := 'I_promotion';
   elsif I_check_status is NULL then
      L_invalid_param := 'I_check_status';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if LP_temp_tables_populated then
      open  C_VALIDATE;
      fetch C_VALIDATE into O_valid,
                            O_promo_rec.promo_id,
                            O_promo_rec.promo_display_id,
                            O_promo_rec.state,
                            O_promo_rec.name,
                            O_promo_rec.currency_code,
                            O_promo_rec.promo_event_id,
                            O_promo_rec.start_date,
                            O_promo_rec.end_date;
      close C_VALIDATE;
   else
      MERCH_API_SQL.VALIDATE_PROMO(O_error_message,
                                   L_success,
                                   O_valid,
                                   L_obj_promo_rec,
                                   I_check_status,
                                   I_promo_id);
      if L_success = 'N' then
         return FALSE;
      end if;
      ---
      if O_valid = 'Y' then
         O_promo_rec.promo_id         := L_obj_promo_rec.promo_id;
         O_promo_rec.promo_display_id := L_obj_promo_rec.promo_display_id;
         O_promo_rec.state            := L_obj_promo_rec.state;
         O_promo_rec.name             := L_obj_promo_rec.name;
         O_promo_rec.currency_code    := L_obj_promo_rec.currency_code;
         O_promo_rec.promo_event_id   := L_obj_promo_rec.promo_event_id;
         O_promo_rec.start_date       := L_obj_promo_rec.start_date;
         O_promo_rec.end_date         := L_obj_promo_rec.end_date;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_PROMOTION;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMOTION_WRP(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid          IN OUT  VARCHAR2,
                                O_promo_rec      IN OUT  OBJ_PROMO_REC,
                                I_check_status   IN      VARCHAR2,
                                I_promo_id       IN      SA_TRAN_DISC.PROMOTION%TYPE)
RETURN NUMBER IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.VALIDATE_PROMOTION_WRP';
   L_obj_promo_rec  PM_API_SQL.PROMOTION_REC;
BEGIN
   
   O_promo_rec := OBJ_PROMO_REC(null, null, null, null, null, null, null, null);

   if VALIDATE_PROMOTION(O_error_message,
                         O_valid,
                         L_obj_promo_rec,
                         I_check_status,
                         I_promo_id) = false then
      return 0;
   end if;
   
   O_promo_rec.promo_id         := L_obj_promo_rec.promo_id;
   O_promo_rec.promo_display_id := L_obj_promo_rec.promo_display_id;
   O_promo_rec.state            := L_obj_promo_rec.state;
   O_promo_rec.name             := L_obj_promo_rec.name;
   O_promo_rec.currency_code    := L_obj_promo_rec.currency_code;
   O_promo_rec.promo_event_id   := L_obj_promo_rec.promo_event_id;
   O_promo_rec.start_date       := L_obj_promo_rec.start_date;
   O_promo_rec.end_date         := L_obj_promo_rec.end_date;
   
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END VALIDATE_PROMOTION_WRP;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMO_COMP(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_valid           IN OUT  VARCHAR2,
                             O_promo_comp_rec  IN OUT  PM_API_SQL.COMPONENT_REC,
                             I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                             I_promo_comp_id   IN      SA_TRAN_DISC.PROMO_COMP%TYPE)
RETURN BOOLEAN IS

   L_program             VARCHAR2(61) := 'PM_API_SQL.VALIDATE_PROMO_COMP';
   L_success             VARCHAR2(1);
   L_obj_promo_comp_rec  OBJ_PROMO_COMP_REC;

   cursor C_VALIDATE is
      select 'Y',
             promo_comp_id,
             comp_display_id,
             promo_id,
             name
        from gtt_promo_comp_temp
       where promo_comp_id = I_promo_comp_id
         and (I_promo_id is NULL or promo_id = I_promo_id);

BEGIN

   --- Initialize output variables
   O_valid          := 'N';
   O_promo_comp_rec := NULL;

   --- Validate parameters
   if I_promo_comp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_promo_comp_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if LP_temp_tables_populated then
      open  C_VALIDATE;
      fetch C_VALIDATE into O_valid,
                            O_promo_comp_rec.promo_comp_id,
                            O_promo_comp_rec.comp_display_id,
                            O_promo_comp_rec.promo_id,
                            O_promo_comp_rec.name;
      close C_VALIDATE;
   else
      MERCH_API_SQL.VALIDATE_PROMO_COMP(O_error_message,
                                        L_success,
                                        O_valid,
                                        L_obj_promo_comp_rec,
                                        I_promo_id,
                                        I_promo_comp_id);
      if L_success = 'N' then
         return FALSE;
      end if;
      ---
      if O_valid = 'Y' then
         O_promo_comp_rec.promo_comp_id   := L_obj_promo_comp_rec.promo_comp_id;
         O_promo_comp_rec.comp_display_id := L_obj_promo_comp_rec.comp_display_id;
         O_promo_comp_rec.promo_id        := L_obj_promo_comp_rec.promo_id;
         O_promo_comp_rec.name            := L_obj_promo_comp_rec.name;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_PROMO_COMP;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMO_COMP_WRP(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_valid           IN OUT  VARCHAR2,
                                 O_promo_comp_rec  IN OUT  OBJ_PROMO_COMP_REC,
                                 I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                 I_promo_comp_id   IN      SA_TRAN_DISC.PROMO_COMP%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(61)                 := 'PM_API_SQL.VALIDATE_PROMO_COMP_WRP';
   L_obj_promo_comp_rec  PM_API_SQL.COMPONENT_REC;

BEGIN
   O_promo_comp_rec := OBJ_PROMO_COMP_REC (null, null, null, null);

   if VALIDATE_PROMO_COMP(O_error_message,
                          O_valid,
                          L_obj_promo_comp_rec,
                          I_promo_id,
                          I_promo_comp_id) = false then
      return 0;
   end if;
   
   O_promo_comp_rec.promo_comp_id   := L_obj_promo_comp_rec.promo_comp_id;
   O_promo_comp_rec.comp_display_id := L_obj_promo_comp_rec.comp_display_id;
   O_promo_comp_rec.promo_id        := L_obj_promo_comp_rec.promo_id;
   O_promo_comp_rec.name            := L_obj_promo_comp_rec.name;
   
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END VALIDATE_PROMO_COMP_WRP;
--------------------------------------------------------------------------------
FUNCTION DEAL_PROMO_EXISTS(I_deal_id  IN  DEAL_HEAD.DEAL_ID%TYPE)
RETURN VARCHAR2 IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.DEAL_PROMO_EXISTS';
   L_error_message  RTK_ERRORS.RTK_TEXT%TYPE;
   L_success        VARCHAR2(1);
   L_exists         VARCHAR2(1);
   L_rpm_ind        VARCHAR2(1):=NULL;
   cursor C_RPM_IND  is 
      select RPM_IND 
	     from system_options;


BEGIN

   -- TODO: error handling.  Since this procedure is called from within a cursor,
   -- we cannot use SQL_LIB to handle errors.  The PRAGMA declared in the spec
   -- says that we can't write any database state, and SQL_LIB violates that
   -- pragma.  Use raise_application_error?

   open C_RPM_IND;
   fetch C_RPM_IND into L_rpm_ind;
   close C_RPM_IND;
   --
   if L_rpm_ind = 'Y' then                            
      MERCH_API_SQL.DEAL_PROMO_EXISTS(L_error_message,
                                      L_success,
                                      L_exists,
                                      I_deal_id);   
      if L_success = 'N' then
         -- TODO: error handling
         return 'ERROR';
      end if;
   else                                              
      L_exists := 'N';
      return L_exists;
   end if;	                                    
   return L_exists;

EXCEPTION
   when OTHERS then
      -- TODO: error handling
      return 'ERROR';
END DEAL_PROMO_EXISTS;

--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMOTION_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_valid           IN OUT   BOOLEAN,
                                  O_promo_name         OUT   GTT_PROMO_TEMP.NAME%TYPE,
                                  I_promo_id        IN       GTT_PROMO_TEMP.PROMO_ID%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.VALIDATE_PROMOTION_EXIST';
   L_promo_id       GTT_PROMO_TEMP.PROMO_ID%TYPE;

   cursor C_VALIDATE is
      select promo_id,
             name
        from gtt_promo_temp
       where promo_id = I_promo_id;

BEGIN

   --- Initialize variables
   O_valid         := FALSE;

   --- Validate parameters
   if I_promo_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_promo_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if LP_temp_tables_populated then
      SQL_LIB.SET_MARK('OPEN',
                       'C_VALIDATE',
                       'GTT_PROMO_TEMP',
                       'PROMO_ID:' || I_promo_id);
      open  C_VALIDATE;

      SQL_LIB.SET_MARK('FETCH',
                       'C_VALIDATE',
                       'GTT_PROMO_TEMP',
                       'PROMO_ID:' || I_promo_id);
      fetch C_VALIDATE into L_promo_id,
                            O_promo_name;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_VALIDATE',
                       'GTT_PROMO_TEMP',
                       'PROMO_ID:' || I_promo_id);
      close C_VALIDATE;

      if L_promo_id is NOT NULL then
         O_valid := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_PROMOTION_EXIST;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_ITEMLIST_ACTIVE(O_error_message            OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_itemlist_active_ind      OUT   BOOLEAN,
                                  I_itemlist_id           IN       SKULIST_HEAD.SKULIST%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'PM_API_SQL.VALIDATE_ITEMLIST_ACTIVE';
   L_itemlist_active_ind   NUMBER       := 0;
   L_rpm_ind               VARCHAR2(1):=NULL;
   cursor C_RPM_IND  is 
      select RPM_IND 
	     from 
	    system_options;

BEGIN
   -- 
   open c_rpm_ind;
   fetch c_rpm_ind into L_rpm_ind;
   close c_rpm_ind;
   --
   
   if L_rpm_ind = 'Y' then 
      if MERCH_API_SQL.CHECK_ITEMLIST_ACTIVE(O_error_message,
                                             L_itemlist_active_ind,
                                             I_itemlist_id) = 0 then
         return FALSE;
      end if;

      if L_itemlist_active_ind = 1 then
         O_itemlist_active_ind := TRUE;
      else
         O_itemlist_active_ind := FALSE;
      end if;
   else    
      O_itemlist_active_ind := FALSE;
   end if ;

   return TRUE;

EXCEPTION
   when OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_ITEMLIST_ACTIVE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMOTION_FROM_HIST (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_valid          IN OUT  VARCHAR2,
                                       O_promo_rec      IN OUT  PM_API_SQL.PROMOTION_REC,
                                       I_check_status   IN      VARCHAR2,
                                       I_promo_id       IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                       I_need_hist      IN      NUMBER)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.VALIDATE_PROMOTION_FROM_HIST';
   L_invalid_param  VARCHAR2(30);
   L_success        VARCHAR2(1);
   L_obj_promo_rec  OBJ_PROMO_REC;

BEGIN

   --- Initialize variables
   O_valid         := 'N';
   O_promo_rec := NULL;

   --- Validate parameters
   if I_promo_id is NULL then
      L_invalid_param := 'I_promotion';
   elsif I_check_status is NULL then
      L_invalid_param := 'I_check_status';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   MERCH_API_SQL.VALIDATE_PROMO(O_error_message,
                                L_success,
                                O_valid,
                                L_obj_promo_rec,
                                I_check_status,
                                I_promo_id,
                                I_need_hist);
   if L_success = 'N' then
      return FALSE;
   end if;
   ---
   if O_valid = 'Y' then
      O_promo_rec.promo_id         := L_obj_promo_rec.promo_id;
      O_promo_rec.promo_display_id := L_obj_promo_rec.promo_display_id;
      O_promo_rec.state            := L_obj_promo_rec.state;
      O_promo_rec.name             := L_obj_promo_rec.name;
      O_promo_rec.currency_code    := L_obj_promo_rec.currency_code;
      O_promo_rec.promo_event_id   := L_obj_promo_rec.promo_event_id;
      O_promo_rec.start_date       := L_obj_promo_rec.start_date;
      O_promo_rec.end_date         := L_obj_promo_rec.end_date;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_PROMOTION_FROM_HIST;
--------------------------------------------------------------------------------
FUNCTION VAL_PROMOTION_FROM_HIST_WRP (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_valid          IN OUT  VARCHAR2,
                                      O_promo_rec      IN OUT  OBJ_PROMO_REC,
                                      I_check_status   IN      VARCHAR2,
                                      I_promo_id       IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                      I_need_hist      IN      NUMBER)
RETURN NUMBER IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.VAL_PROMOTION_FROM_HIST_WRP';
   L_obj_promo_rec  PM_API_SQL.PROMOTION_REC;

BEGIN

   O_promo_rec := OBJ_PROMO_REC(null, null, null, null, null, null, null, null);
   
   if VALIDATE_PROMOTION_FROM_HIST (O_error_message,
                                    O_valid,
                                    L_obj_promo_rec,
                                    I_check_status,
                                    I_promo_id,
                                    I_need_hist) = FALSE then
      return 0;
   end if;
   
   O_promo_rec.promo_id         := L_obj_promo_rec.promo_id;
   O_promo_rec.promo_display_id := L_obj_promo_rec.promo_display_id;
   O_promo_rec.state            := L_obj_promo_rec.state;
   O_promo_rec.name             := L_obj_promo_rec.name;
   O_promo_rec.currency_code    := L_obj_promo_rec.currency_code;
   O_promo_rec.promo_event_id   := L_obj_promo_rec.promo_event_id;
   O_promo_rec.start_date       := L_obj_promo_rec.start_date;
   O_promo_rec.end_date         := L_obj_promo_rec.end_date;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END VAL_PROMOTION_FROM_HIST_WRP;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PROMO_COMP_FROM_HIST (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_valid           IN OUT  VARCHAR2,
                                        O_promo_comp_rec  IN OUT  PM_API_SQL.COMPONENT_REC,
                                        I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                        I_promo_comp_id   IN      SA_TRAN_DISC.PROMO_COMP%TYPE,
                                        I_need_hist       IN      NUMBER)
RETURN BOOLEAN IS

   L_program             VARCHAR2(61) := 'PM_API_SQL.VALIDATE_PROMO_COMP_FROM_HIST';
   L_success             VARCHAR2(1);
   L_obj_promo_comp_rec  OBJ_PROMO_COMP_REC;

BEGIN

   --- Initialize output variables
   O_valid          := 'N';
   O_promo_comp_rec := NULL;

   --- Validate parameters
   if I_promo_comp_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_promo_comp_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   MERCH_API_SQL.VALIDATE_PROMO_COMP(O_error_message,
                                     L_success,
                                     O_valid,
                                     L_obj_promo_comp_rec,
                                     I_promo_id,
                                     I_promo_comp_id,
                                     I_need_hist);
   if L_success = 'N' then
      return FALSE;
   end if;
   ---
   if O_valid = 'Y' then
      O_promo_comp_rec.promo_comp_id   := L_obj_promo_comp_rec.promo_comp_id;
      O_promo_comp_rec.comp_display_id := L_obj_promo_comp_rec.comp_display_id;
      O_promo_comp_rec.promo_id        := L_obj_promo_comp_rec.promo_id;
      O_promo_comp_rec.name            := L_obj_promo_comp_rec.name;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_PROMO_COMP_FROM_HIST;
--------------------------------------------------------------------------------
FUNCTION VAL_PROMO_COMP_FROM_HIST_WRP (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_valid           IN OUT  VARCHAR2,
                                       O_promo_comp_rec  IN OUT  OBJ_PROMO_COMP_REC,
                                       I_promo_id        IN      SA_TRAN_DISC.PROMOTION%TYPE,
                                       I_promo_comp_id   IN      SA_TRAN_DISC.PROMO_COMP%TYPE,
                                       I_need_hist       IN      NUMBER)
RETURN NUMBER IS

   L_program             VARCHAR2(61) := 'PM_API_SQL.VAL_PROMO_COMP_FROM_HIST_WRP';
   L_obj_promo_comp_rec  PM_API_SQL.COMPONENT_REC;

BEGIN
   O_promo_comp_rec := OBJ_PROMO_COMP_REC (null, null, null, null);
   if VALIDATE_PROMO_COMP_FROM_HIST (O_error_message,
                                     O_valid,
                                     L_obj_promo_comp_rec,
                                     I_promo_id,
                                     I_promo_comp_id,
                                     I_need_hist) = FALSE then
      return 0;
   end if;                                        

   O_promo_comp_rec.promo_comp_id   := L_obj_promo_comp_rec.promo_comp_id;
   O_promo_comp_rec.comp_display_id := L_obj_promo_comp_rec.comp_display_id;
   O_promo_comp_rec.promo_id        := L_obj_promo_comp_rec.promo_id;
   O_promo_comp_rec.name            := L_obj_promo_comp_rec.name;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END VAL_PROMO_COMP_FROM_HIST_WRP;
--------------------------------------------------------------------------------
FUNCTION GET_PROMO_DETAILS (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid            IN OUT  VARCHAR2,
                            O_promo_display_id IN OUT  VARCHAR2,
                            O_promo_name       IN OUT  VARCHAR2,
                            O_currency_code    IN OUT  VARCHAR2,
                            O_start_date       IN OUT  DATE,
                            O_end_date         IN OUT  DATE,
                            I_promo_id         IN      SA_TRAN_DISC.PROMOTION%TYPE,
                            I_check_status     IN      VARCHAR2,
                            I_need_hist        IN      NUMBER)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.GET_PROMO_DETAILS';
   L_invalid_param  VARCHAR2(30);
   L_success        VARCHAR2(1);
   L_obj_promo_rec  OBJ_PROMO_REC;

BEGIN

   --- Initialize variables
   O_valid         := 'N';

   --- Validate parameters
   if I_promo_id is NULL then
      L_invalid_param := 'I_promotion';
   elsif I_check_status is NULL then
      L_invalid_param := 'I_check_status';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   MERCH_API_SQL.VALIDATE_PROMO(O_error_message,
                                L_success,
                                O_valid,
                                L_obj_promo_rec,
                                I_check_status,
                                I_promo_id,
                                I_need_hist); 
                              
   if L_success = 'N' then
      return FALSE;
   end if;
   ---
   if O_valid = 'Y' then
      O_promo_display_id           := L_obj_promo_rec.promo_display_id;
      O_promo_name                 := L_obj_promo_rec.name;
      O_currency_code              := L_obj_promo_rec.currency_code;
      O_start_date                 := L_obj_promo_rec.start_date;
      O_end_date                   := L_obj_promo_rec.end_date;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PROMO_DETAILS;
--------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_ZONE_GROUP_LOC(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_location         IN OUT  RPM_ZONE_LOCATION.LOCATION%TYPE,
                                    I_item             IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(61) := 'PM_API_SQL.GET_PRIMARY_ZONE_GROUP_LOC';
   
   cursor C_get_item_ranged_location is
   select rzl.location 
    from item_master im,
         rpm_merch_retail_def_expl rmrde,
         rpm_zone rz,
         rpm_zone_location rzl,
         item_loc il
   where im.item = I_item
     and im.dept = rmrde.dept
     and im.class = rmrde.class
     and im.subclass = rmrde.subclass
     and rmrde.regular_zone_group = rz.zone_group_id
     and rz.zone_id = rzl.zone_id
     and il.item = I_item
     and il.loc = rzl.location
     and rownum = 1;
BEGIN
   
   open C_get_item_ranged_location;
   fetch C_get_item_ranged_location into O_location;
   close C_get_item_ranged_location;   
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PRIMARY_ZONE_GROUP_LOC;
--------------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_RETAIL(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_unit_retail        IN OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                            O_currency_code      IN OUT CURRENCIES.CURRENCY_CODE%TYPE,
                            I_item               IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                            I_location           IN     RPM_FUTURE_RETAIL.LOCATION%TYPE,
                            I_effective_date     IN     DATE DEFAULT NULL)
RETURN BOOLEAN IS
   
   L_program        VARCHAR2(61) := 'PM_API_SQL.GET_ITEMLOC_RETAIL';    

   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_selling_uom    RPM_FUTURE_RETAIL.SELLING_UOM%TYPE;
   L_qty            NUMBER;
   L_standard_uom   UOM_CLASS.UOM%TYPE;
   L_item           RPM_FUTURE_RETAIL.ITEM%TYPE;

   cursor c_get_item_loc_retail is
     with loc_currency as (select store loc,
                                  'S' loc_type,
                                  currency_code
                             from store
                            union all
                           select wh loc,
                                  'W' loc_type,
                                  currency_code
                             from wh
                            union all
                           select to_number(partner_id) loc,
                                  'E' loc_type,
                                  currency_code
                             from partner
                            where partner_type = 'E')
   select distinct avg(il.unit_retail) over (partition by I_item) as avg_unit_retail,
          c.currency_code
     from item_loc il,
          item_master im,
          loc_currency c
    where il.item = im.item
      and im.item_level = im.tran_level
      and (im.item = I_item
       or im.item_parent = I_item
       or im.item_grandparent = I_item)
      and il.loc = I_location
      and il.loc = c.loc
      and il.loc_type = c.loc_type;

BEGIN

   L_item := I_item;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   if L_system_options_row.rpm_ind = 'Y' then
      if MERCH_API_SQL.GET_ITEMLOC_RETAIL(O_error_message,
                                          O_unit_retail,
                                          L_selling_uom,
                                          O_currency_code,
                                          L_item,
                                          I_location,
                                          I_effective_date) = 0 then
         return FALSE;
      end if;

      if O_unit_retail is NOT NULL then
         select standard_uom into L_standard_uom
           from item_master
          where item = I_item;

         L_qty := 0;

         if L_selling_uom <> L_standard_uom then
            if UOM_SQL.CONVERT(O_error_message,
                               L_qty,
                               L_standard_uom,
                               1,
                               L_selling_uom,
                               I_item,
                               NULL,
                               NULL) = FALSE then
               return FALSE;
            end if;
            --- Convert the selling unit retail to standard UOM
            if L_qty <> 0 then
               O_unit_retail := O_unit_retail / L_qty;
            end if;
         end if;
      else
         open c_get_item_loc_retail;
         fetch c_get_item_loc_retail into O_unit_retail, O_currency_code;
         close c_get_item_loc_retail;
      end if;
   else
      open c_get_item_loc_retail;
      fetch c_get_item_loc_retail into O_unit_retail, O_currency_code;
      close c_get_item_loc_retail;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ITEMLOC_RETAIL;
--------------------------------------------------------------------------------
END PM_API_SQL;
/