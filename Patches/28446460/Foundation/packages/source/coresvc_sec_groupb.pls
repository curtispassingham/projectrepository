CREATE OR REPLACE PACKAGE BODY CORESVC_SEC_GROUP AS

    cursor C_SVC_SEC_GROUP(I_process_id   NUMBER,
                           I_chunk_id     NUMBER) is
      select pk_sec_group.rowid          as pk_sec_group_rid,
             st.rowid                    as st_rid,
             cd_role.rowid               as cd_rid,
             st.comments,
             UPPER(st.role)              as role,
             st.group_name,
             st.group_id,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(pk_sec_group.role)    as old_role,
             UPPER(st.action)            as action,
             st.process$status
        from svc_sec_group st,
             sec_group pk_sec_group,
             code_detail cd_role
       where st.process_id              = I_process_id
         and st.chunk_id                = I_chunk_id
         and st.group_id                = pk_sec_group.group_id (+)
         and UPPER(st.role)             = cd_role.code(+)
         and cd_role.code_type(+)       = 'ROLE';
        
   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
   
   Type SEC_GROUP_TL_tab IS TABLE OF SEC_GROUP_TL%ROWTYPE;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   LP_primary_lang       LANG.LANG%TYPE;   
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
end GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   SEC_GROUP_cols s9t_pkg.names_map_typ;
   SEC_GROUP_TL_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                := s9t_pkg.get_sheet_names(I_file_id);
   SEC_GROUP_cols          := s9t_pkg.get_col_names(I_file_id,SEC_GROUP_sheet);
   
   SEC_GROUP$Action        := SEC_GROUP_cols('ACTION');
   SEC_GROUP$GROUP_ID      := SEC_GROUP_cols('GROUP_ID');
   SEC_GROUP$GROUP_NAME    := SEC_GROUP_cols('GROUP_NAME');
   SEC_GROUP$ROLE          := SEC_GROUP_cols('ROLE');
   SEC_GROUP$COMMENTS      := SEC_GROUP_cols('COMMENTS');
   
   SEC_GROUP_TL_cols       := s9t_pkg.get_col_names(I_file_id,SEC_GROUP_TL_sheet);
   SEC_GROUP_TL$Action     := SEC_GROUP_TL_cols('ACTION');
   SEC_GROUP_TL$LANG       := SEC_GROUP_TL_cols('LANG');
   SEC_GROUP_TL$GROUP_ID   := SEC_GROUP_TL_cols('GROUP_ID');
   SEC_GROUP_TL$GROUP_NAME := SEC_GROUP_TL_cols('GROUP_NAME');   
                          
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_SEC_GROUP( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SEC_GROUP_sheet )
   select s9t_row(s9t_cells(CORESVC_SEC_GROUP.action_mod,
                            group_id,
                            group_name,
                            role,
                            comments))
     from sec_group;
END POPULATE_SEC_GROUP;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_SEC_GROUP_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = SEC_GROUP_TL_sheet )
   select s9t_row(s9t_cells(CORESVC_SEC_GROUP.action_mod,
                            lang,
                            group_id,
                            group_name))
     from SEC_GROUP_TL ;
END POPULATE_SEC_GROUP_TL;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name s9t_folder.file_name%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   
   L_file.add_sheet(SEC_GROUP_sheet);
   L_file.sheets(l_file.get_sheet_index(SEC_GROUP_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                       ,'GROUP_ID'
                                                                                       ,'GROUP_NAME'
                                                                                       ,'ROLE'
                                                                                       ,'COMMENTS');
                                                                                            
   L_file.add_sheet(SEC_GROUP_TL_sheet);
   L_file.sheets(l_file.get_sheet_index(SEC_GROUP_TL_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                          ,'LANG'
                                                                                          ,'GROUP_ID'
                                                                                          ,'GROUP_NAME');
                                                                                            
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_file s9t_file;
   L_program VARCHAR2(64):='CORESVC_SEC_GROUP.CREATE_S9T';
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_SEC_GROUP(O_file_id);
      POPULATE_SEC_GROUP_TL(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SEC_GROUP( I_file_id    IN   s9t_folder.file_id%TYPE,
                                 I_process_id IN   SVC_SEC_GROUP.process_id%TYPE) IS
   TYPE svc_SEC_GROUP_col_typ IS TABLE OF SVC_SEC_GROUP%ROWTYPE;
   L_temp_rec        SVC_SEC_GROUP%ROWTYPE;
   svc_SEC_GROUP_col svc_SEC_GROUP_col_typ :=NEW svc_SEC_GROUP_col_typ();
   L_process_id      SVC_SEC_GROUP.process_id%TYPE;
   L_error           BOOLEAN:=FALSE;
   L_default_rec     SVC_SEC_GROUP%ROWTYPE;
   L_pk_columns      VARCHAR2(255)  := 'Group ID';
   L_error_code      NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
   cursor C_MANDATORY_IND is
      select
             COMMENTS_mi,
             ROLE_mi,
             GROUP_NAME_mi,
             GROUP_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_SEC_GROUP.template_key
                 and wksht_key     = 'SEC_GROUP'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('GROUP_ID' AS GROUP_ID,
                                            'GROUP_NAME' AS GROUP_NAME,
                                            'ROLE' AS ROLE,
                                            'COMMENTS' AS COMMENTS,
                                            NULL as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       COMMENTS_dv,
                       ROLE_dv,
                       GROUP_NAME_dv,
                       GROUP_ID_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = CORESVC_SEC_GROUP.template_key
                          and wksht_key    = 'SEC_GROUP'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ('GROUP_ID' AS GROUP_ID,
                                                     'GROUP_NAME' AS GROUP_NAME,
                                                     'ROLE' AS ROLE,
                                                     'COMMENTS' AS COMMENTS,
                                                     NULL AS dummy)))
   LOOP
      BEGIN
         L_default_rec.COMMENTS := rec.COMMENTS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SEC_GROUP',
                            NULL,
                            'COMMENTS',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ROLE := rec.ROLE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SEC_GROUP',
                            NULL,
                            'ROLE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.GROUP_NAME := rec.GROUP_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SEC_GROUP',
                            NULL,
                            'GROUP_NAME',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.GROUP_ID := rec.GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SEC_GROUP',
                            NULL,
                            'GROUP_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(SEC_GROUP$Action)      as action,
          r.get_cell(SEC_GROUP$GROUP_ID)    as group_id,
          r.get_cell(SEC_GROUP$GROUP_NAME)  as group_name,
          r.get_cell(SEC_GROUP$ROLE)        as role,
          r.get_cell(SEC_GROUP$COMMENTS)    as comments,
          r.get_row_seq()                   as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id    = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(SEC_GROUP_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := USER;
      L_temp_rec.last_upd_id       := USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comments := rec.comments;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_sheet,
                            rec.row_seq,
                            'COMMENTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.role := rec.role;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_sheet,
                            rec.row_seq,
                            'ROLE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.group_name := rec.group_name;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_sheet,
                            rec.row_seq,
                            'GROUP_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_ID := rec.GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_sheet,
                            rec.row_seq,
                            'GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SEC_GROUP.action_new then
         L_temp_rec.group_id   := NVL( L_temp_rec.group_id,
                                       L_default_rec.group_id);
         L_temp_rec.group_name := NVL( L_temp_rec.group_name,
                                       L_default_rec.group_name);
         L_temp_rec.role       := NVL( L_temp_rec.role,
                                       L_default_rec.role);
         L_temp_rec.comments   := NVL( L_temp_rec.comments,
                                       L_default_rec.comments);
      end if;
      if not ( L_temp_rec.GROUP_ID is NOT NULL)then
         WRITE_S9T_ERROR(I_file_id,
                         SEC_GROUP_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_SEC_GROUP_col.extend();
         svc_SEC_GROUP_col(svc_SEC_GROUP_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_SEC_GROUP_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SEC_GROUP st
      using(select
                  (case
                   when l_mi_rec.GROUP_ID_mi    = 'N'
                    and svc_SEC_GROUP_col(i).action = CORESVC_SEC_GROUP.action_mod
                    and s1.GROUP_ID IS NULL
                   then mt.GROUP_ID
                   else s1.GROUP_ID
                   end) AS GROUP_ID,
                  (case
                   when l_mi_rec.GROUP_NAME_mi    = 'N'
                    and svc_SEC_GROUP_col(i).action = CORESVC_SEC_GROUP.action_mod
                    and s1.GROUP_NAME IS NULL
                   then mt.GROUP_NAME
                   else s1.GROUP_NAME
                   end) AS GROUP_NAME,                  
                  (case
                   when l_mi_rec.ROLE_mi    = 'N'
                    and svc_SEC_GROUP_col(i).action = CORESVC_SEC_GROUP.action_mod
                    and s1.ROLE IS NULL
                   then mt.ROLE
                   else s1.ROLE
                   end) AS ROLE,                  
                  (case
                   when l_mi_rec.COMMENTS_mi    = 'N'
                    and svc_SEC_GROUP_col(i).action = CORESVC_SEC_GROUP.action_mod
                    and s1.COMMENTS IS NULL
                   then mt.COMMENTS
                   else s1.COMMENTS
                   end) AS COMMENTS,
                  null as dummy
              from (select svc_SEC_GROUP_col(i).group_id   as group_id,
                           svc_SEC_GROUP_col(i).group_name as group_name,
                           svc_SEC_GROUP_col(i).role       as role,
                           svc_SEC_GROUP_col(i).comments   as comments,
                           NULL                            as dummy
                      from dual ) s1,
                           SEC_GROUP mt
                     where mt.GROUP_ID (+)     = s1.group_id
                       and 1 = 1 )sq
                on (st.group_id      = sq.group_id 
                    and svc_SEC_GROUP_col(i).action in (CORESVC_SEC_GROUP.action_mod,
                                                        CORESVC_SEC_GROUP.action_del))
      when matched then
      update
         set process_id        = svc_SEC_GROUP_col(i).process_id ,
             chunk_id          = svc_SEC_GROUP_col(i).chunk_id ,
             row_seq           = svc_SEC_GROUP_col(i).row_seq ,
             action            = svc_SEC_GROUP_col(i).action ,
             process$status    = svc_SEC_GROUP_col(i).process$status ,
             comments          = sq.comments ,
             role              = sq.role ,
             group_name        = sq.group_name ,
             create_id         = svc_SEC_GROUP_col(i).create_id ,
             create_datetime   = svc_SEC_GROUP_col(i).create_datetime ,
             last_upd_id       = svc_SEC_GROUP_col(i).last_upd_id ,
             last_upd_datetime = svc_SEC_GROUP_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             comments ,
             role ,
             group_name ,
             group_id ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_SEC_GROUP_col(i).process_id ,
             svc_SEC_GROUP_col(i).chunk_id ,
             svc_SEC_GROUP_col(i).row_seq ,
             svc_SEC_GROUP_col(i).action ,
             svc_SEC_GROUP_col(i).process$status ,
             sq.comments ,
             sq.role ,
             sq.group_name ,
             sq.group_id ,
             svc_SEC_GROUP_col(i).create_id ,
             svc_SEC_GROUP_col(i).create_datetime ,
             svc_SEC_GROUP_col(i).last_upd_id ,
             svc_SEC_GROUP_col(i).last_upd_datetime );
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_sheet,
                            svc_SEC_GROUP_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            sql%bulk_exceptions(i).error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_SEC_GROUP;
------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_SEC_GROUP_TL( I_file_id    IN   s9t_folder.file_id%TYPE,
                                    I_process_id IN   SVC_SEC_GROUP_TL.process_id%TYPE) IS
   TYPE svc_SEC_GROUP_TL_col_typ IS TABLE OF SVC_SEC_GROUP_TL%ROWTYPE;
   L_temp_rec           SVC_SEC_GROUP_TL%ROWTYPE;
   svc_SEC_GROUP_TL_col svc_SEC_GROUP_TL_col_typ :=NEW svc_SEC_GROUP_TL_col_typ();
   L_process_id         SVC_SEC_GROUP_TL.process_id%TYPE;
   L_error              BOOLEAN:=FALSE;
   L_default_rec        SVC_SEC_GROUP_TL%ROWTYPE;
   L_pk_columns         VARCHAR2(255)  := 'Group ID, Lang';
   L_error_code         NUMBER;
   L_error_msg          RTK_ERRORS.RTK_TEXT%TYPE;
   cursor C_MANDATORY_IND is
      select
             GROUP_NAME_mi,
             GROUP_ID_mi,
             LANG_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key    = CORESVC_SEC_GROUP.template_key
                 and wksht_key       = 'SEC_GROUP_TL'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('LANG'       as lang,
                                            'GROUP_ID'   as group_id,
                                            'GROUP_NAME' as group_name,
                                             null        as dummy));
      l_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
BEGIN
  -- Get default values.
   FOR rec IN (select
                       GROUP_NAME_dv,
                       GROUP_ID_dv,
                       LANG_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                = CORESVC_SEC_GROUP.template_key
                          and wksht_key                   = 'SEC_GROUP_TL'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ( 'LANG'       as lang,
                                                      'GROUP_ID'   as group_id,
                                                      'GROUP_NAME' as group_name,
                                                       NULL        as dummy)))
   LOOP
      BEGIN
         L_default_rec.GROUP_NAME := rec.GROUP_NAME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SEC_GROUP_TL ' ,
                            NULL,
                            'GROUP_NAME ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.GROUP_ID := rec.GROUP_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SEC_GROUP_TL ' ,
                            NULL,
                            'GROUP_ID ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'SEC_GROUP_TL ' ,
                            NULL,
                            'LANG ' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(SEC_GROUP_TL$Action)      as action,
          r.get_cell(SEC_GROUP_TL$LANG)        as lang,
          r.get_cell(SEC_GROUP_TL$GROUP_ID)    as group_id,
          r.get_cell(SEC_GROUP_TL$GROUP_NAME)  as group_name,
          r.get_row_seq()                      as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = GET_SHEET_NAME_TRANS(SEC_GROUP_TL_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_NAME := rec.GROUP_NAME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_TL_sheet,
                            rec.row_seq,
                            'GROUP_NAME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.GROUP_ID := rec.GROUP_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_TL_sheet,
                            rec.row_seq,
                            'GROUP_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LANG := rec.LANG;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            SEC_GROUP_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_SEC_GROUP.action_new then
         L_temp_rec.LANG       := NVL( L_temp_rec.LANG,L_default_rec.LANG);
         L_temp_rec.GROUP_ID   := NVL( L_temp_rec.GROUP_ID,L_default_rec.GROUP_ID);         
         L_temp_rec.GROUP_NAME := NVL( L_temp_rec.GROUP_NAME,L_default_rec.GROUP_NAME);         
      end if;
      if not (L_temp_rec.GROUP_ID is NOT NULL and
              L_temp_rec.LANG is NOT NULL and
              1 = 1)then
         WRITE_S9T_ERROR(I_file_id,
                         SEC_GROUP_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                        SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_SEC_GROUP_TL_col.extend();
         svc_SEC_GROUP_TL_col(svc_SEC_GROUP_TL_col.COUNT()):=l_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_SEC_GROUP_TL_col.COUNT SAVE EXCEPTIONS
      merge into SVC_SEC_GROUP_TL st
      using(select
                  (case
                   when l_mi_rec.GROUP_NAME_mi    = 'N'
                    and svc_SEC_GROUP_TL_col(i).action = CORESVC_SEC_GROUP.action_mod
                    and s1.GROUP_NAME IS NULL
                   then mt.GROUP_NAME
                   else s1.GROUP_NAME
                   end) AS GROUP_NAME,
                  (case
                   when l_mi_rec.GROUP_ID_mi    = 'N'
                    and svc_SEC_GROUP_TL_col(i).action = CORESVC_SEC_GROUP.action_mod
                    and s1.GROUP_ID IS NULL
                   then mt.GROUP_ID
                   else s1.GROUP_ID
                   end) AS GROUP_ID,
                  (case
                   when l_mi_rec.LANG_mi    = 'N'
                    and svc_SEC_GROUP_TL_col(i).action = CORESVC_SEC_GROUP.action_mod
                    and s1.LANG IS NULL
                   then mt.LANG
                   else s1.LANG
                   end) AS LANG,
                  null as dummy
              from (select
                          svc_SEC_GROUP_TL_col(i).GROUP_NAME AS GROUP_NAME,
                          svc_SEC_GROUP_TL_col(i).GROUP_ID AS GROUP_ID,
                          svc_SEC_GROUP_TL_col(i).LANG AS LANG,
                          null as dummy
                      from dual ) s1,
            SEC_GROUP_TL mt
             where
                  mt.GROUP_ID (+)     = s1.GROUP_ID   and
                  mt.LANG (+)     = s1.LANG   and
                  1 = 1 )sq
                on (
                    st.GROUP_ID      = sq.GROUP_ID and
                    st.LANG      = sq.LANG and
                    svc_SEC_GROUP_TL_col(i).ACTION IN (CORESVC_SEC_GROUP.action_mod,CORESVC_SEC_GROUP.action_del))
      when matched then
      update
         set process_id      = svc_SEC_GROUP_TL_col(i).process_id ,
             chunk_id        = svc_SEC_GROUP_TL_col(i).chunk_id ,
             row_seq         = svc_SEC_GROUP_TL_col(i).row_seq ,
             action          = svc_SEC_GROUP_TL_col(i).action ,
             process$status  = svc_SEC_GROUP_TL_col(i).process$status ,
             group_name      = sq.group_name ,
             create_id       = svc_SEC_GROUP_TL_col(i).create_id ,
             create_datetime = svc_SEC_GROUP_TL_col(i).create_datetime ,
             last_upd_id     = svc_SEC_GROUP_TL_col(i).last_upd_id ,
             last_upd_datetime = svc_SEC_GROUP_TL_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             group_name ,
             group_id ,
             lang ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_SEC_GROUP_TL_col(i).process_id ,
             svc_SEC_GROUP_TL_col(i).chunk_id ,
             svc_SEC_GROUP_TL_col(i).row_seq ,
             svc_SEC_GROUP_TL_col(i).action ,
             svc_SEC_GROUP_TL_col(i).process$status ,
             sq.group_name ,
             sq.group_id ,
             sq.lang ,
             svc_SEC_GROUP_TL_col(i).create_id ,
             svc_SEC_GROUP_TL_col(i).create_datetime ,
             svc_SEC_GROUP_TL_col(i).last_upd_id ,
             svc_SEC_GROUP_TL_col(i).last_upd_datetime );
   EXCEPTION
      
      when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
         end if;

         WRITE_S9T_ERROR( I_file_id,
                         SEC_GROUP_TL_sheet,
                         SVC_SEC_GROUP_TL_COL(sql%bulk_exceptions(i).error_index).row_seq,
                         NULL,
                         L_error_code,
                         L_error_msg);
      END LOOP;         
   END;
END PROCESS_S9T_SEC_GROUP_TL;
-------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_COUNT     OUT      NUMBER,
                      I_file_id         IN       s9t_folder.file_id%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_file             s9t_file;
   L_sheets           s9t_pkg.names_map_typ;
   L_program          VARCHAR2(64):='CORESVC_SEC_GROUP.PROCESS_S9T';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = false then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_SEC_GROUP(I_file_id,I_process_id);
      PROCESS_S9T_SEC_GROUP_TL(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
      insert into s9t_errors
           values Lp_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
-------------------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_INS( O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_sec_group_temp_rec   IN       SEC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_SEC_GROUP.EXEC_SEC_GROUP_INS';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_SEC_GROUP';
BEGIN
   insert into sec_group 
        values I_sec_group_temp_rec;
   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('SEC_GROUP_EXISTS',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                             'CORESVC_SEC_GROUP.EXEC_SEC_GROUP_INS',
                                              TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_INS;
-------------------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_UPD(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sec_group_temp_rec   IN       SEC_GROUP%ROWTYPE,
                            I_rec                  IN       C_SVC_SEC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_SEC_GROUP.EXEC_SEC_GROUP_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_SEC_GROUP';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   cursor C_SEC_GRP_LOCK is
      select 'X'
        from sec_group
       where group_id = I_sec_group_temp_rec.group_id
         for update nowait;   
BEGIN
   open C_SEC_GRP_LOCK;
   close C_SEC_GRP_LOCK;
   update sec_group 
      set row      = I_sec_group_temp_rec 
    where group_id = I_sec_group_temp_rec.group_id;
   return TRUE;
EXCEPTION
  when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sec_group_temp_rec.group_id,
                                             NULL);  
   when OTHERS then
      if C_SEC_GRP_LOCK%ISOPEN then
         close C_SEC_GRP_LOCK;
      end if;         
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_UPD;
-------------------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_DEL(O_error_message        IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_sec_group_temp_rec   IN       SEC_GROUP%ROWTYPE,
                            I_rec                  IN       C_SVC_SEC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_SEC_GROUP.EXEC_SEC_GROUP_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_SEC_GROUP';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   cursor C_LOCK_SEC_GRP_DEL is
      select 'X'
        from sec_group
       where group_id = I_sec_group_temp_rec.group_id
         for update nowait;
         
   cursor C_LOCK_SEC_GRP_TL_DEL is
      select 'x'
        from sec_group_tl
       where group_id = I_sec_group_temp_rec.group_id
         for update nowait;   
BEGIN
      
   open  C_LOCK_SEC_GRP_TL_DEL;
   close C_LOCK_SEC_GRP_TL_DEL;
    
   open C_LOCK_SEC_GRP_DEL;
   close C_LOCK_SEC_GRP_DEL;
   
   if LOC_PROD_SECURITY_SQL.DELETE_GRP_SEC(O_error_message,
                                           I_sec_group_temp_rec.group_id) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
   else
      delete from sec_group_tl 
       where group_id = I_sec_group_temp_rec.group_id;
    
      delete from sec_group 
       where group_id = I_sec_group_temp_rec.group_id;
   end if;
   return TRUE;

EXCEPTION
  when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_sec_group_temp_rec.group_id,
                                             NULL);  
   when OTHERS then
      if C_LOCK_SEC_GRP_TL_DEL%ISOPEN then
         close C_LOCK_SEC_GRP_TL_DEL;
      end if;      
      if C_LOCK_SEC_GRP_DEL%ISOPEN then
         close C_LOCK_SEC_GRP_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_DEL;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SEC_GROUP_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_error           IN OUT   BOOLEAN,                               
                               I_rec             IN       C_SVC_SEC_GROUP%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_SEC_GROUP.PROCESS_SEC_GROUP_VAL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_SEC_GROUP';
   L_merch_exist  BOOLEAN ;
   L_org_exist    BOOLEAN;
   L_orig_role    SEC_GROUP.ROLE%TYPE;
BEGIN
   if I_rec.action IN (action_new, action_mod)
      and I_rec.role is NOT NULL
      and I_rec.cd_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'ROLE',
                  'ROLE_INVALID');
         O_error := TRUE;
   end if;
   -- This validation checks to see if the group has any merch children attached
   if I_rec.action = action_del 
      and I_rec.group_id is NOT NULL then
      if FILTER_GROUP_HIER_SQL.CHECK_GROUP_MERCH(O_error_message,
                                                 L_merch_exist,
                                                 I_rec.group_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'GROUP_ID',
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_merch_exist = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'GROUP_ID',
                     'CANNOT_DELETE_GROUP');
         O_error :=TRUE;
      end if;
      
      -- this validation checks if the group has any org childern attached
      if FILTER_GROUP_HIER_SQL.CHECK_GROUP_ORG(O_error_message,
                                               L_org_exist,
                                               I_rec.group_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'GROUP_ID',
                     O_error_message);
         O_error :=TRUE;
      end if;
      if L_org_exist = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'GROUP_ID',
                     'CANNOT_DELETE_GROUP');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;                                      
END PROCESS_SEC_GROUP_VAL;
--------------------------------------------------------------------------------------------------
FUNCTION PROCESS_SEC_GROUP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_process_id      IN       SVC_SEC_GROUP.PROCESS_ID%TYPE,
                           I_chunk_id        IN       SVC_SEC_GROUP.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                         := 'CORESVC_SEC_GROUP.PROCESS_SEC_GROUP';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    := 'SVC_SEC_GROUP';
   L_error              BOOLEAN;
   L_process_error      BOOLEAN;
   L_proceed_update     BOOLEAN;
   L_proceed_delete     BOOLEAN;
   L_sec_group_temp_rec SEC_GROUP%ROWTYPE;
   L_next_group_id      SEC_GROUP.GROUP_ID%TYPE := NULL;
BEGIN
   FOR rec IN C_SVC_SEC_GROUP(I_process_id,
                              I_chunk_id)
   LOOP
   SAVEPOINT successful_sec_group;
      L_error          := FALSE;
      L_process_error  := FALSE;
      L_proceed_update := TRUE;
      L_proceed_delete := TRUE;
      if rec.action is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      IF rec.action is NOT NULL 
         and rec.action NOT IN (action_new,action_mod,action_del) THEN
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;
      if rec.action IN (action_mod,action_del) 
         and rec.pk_sec_group_rid is NULL THEN
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'GROUP_ID',
                     'INV_VALUE');
         L_error :=TRUE;
      end if;
      if NOT( rec.group_id is NOT NULL ) 
         and rec.action IN (action_mod, action_del,action_new) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'GROUP_ID',
                     'FIELD_NOT_NULL');
         L_error :=TRUE;
      end if;
      if NOT( rec.group_name is NOT NULL ) 
         and rec.action IN (action_mod, action_new)then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'GROUP_NAME',
                     'ENTER_SEC_GROUP_NAME');
         L_error :=TRUE;
      end if;
      --Other validations present in the forms
      if PROCESS_SEC_GROUP_VAL(O_error_message,
                               L_error,
                               rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'GROUP_ID',
                     O_error_message);
         L_error := TRUE;
      end if;
      if NOT L_error 
	     and rec.action = action_new then
         if LOC_PROD_SECURITY_SQL.NEXT_GROUP_NUMBER(O_error_message,
                                                    L_next_group_id) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'GROUP_ID',
                        O_error_message);
            L_error := TRUE;
         else
            L_sec_group_temp_rec.group_id := L_next_group_id;
            update svc_sec_group_tl
               set group_id   = L_next_group_id
             where process_id = I_process_id
                and chunk_id  = I_chunk_id
                and action    = action_new
                and group_id  = rec.group_id;
            L_next_group_id := NULL; 
         end if;
      end if;
      if NOT L_error then
         
         if rec.action IN (action_mod,action_del)then
            L_sec_group_temp_rec.group_id                := rec.group_id;
         end if;         
         L_sec_group_temp_rec.group_name              := rec.group_name;
         L_sec_group_temp_rec.role                    := rec.role;
         L_sec_group_temp_rec.comments                := rec.comments;
         L_sec_group_temp_rec.CREATE_ID               := GET_USER;
         L_sec_group_temp_rec.CREATE_DATETIME         := SYSDATE;
         
         if rec.action = action_new then
            if EXEC_SEC_GROUP_INS(O_error_message,
                                  L_sec_group_temp_rec) = FALSE then               
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
		   if rec.action = action_mod then
            if EXEC_SEC_GROUP_UPD(O_error_message,
                                  L_sec_group_temp_rec,               
                                  rec) = FALSE then
               ROLLBACK TO successful_sec_group;
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           rec.action,
                           O_error_message);      
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if LOC_PROD_SECURITY_SQL.LOCK_GRP_SEC(O_error_message,
                                                  rec.group_id) = FALSE then
               WRITE_ERROR(rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           rec.chunk_id,
                           L_table, 
                           rec.row_seq,
                           'GROUP_ID',
                           O_error_message);
                L_proceed_delete := FALSE;
            end if;
            if L_proceed_delete then
               if EXEC_SEC_GROUP_DEL(O_error_message,
                                     L_sec_group_temp_rec,
                                     rec) = FALSE then               
                  ROLLBACK TO successful_sec_group;
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              rec.row_seq,
                              rec.action,
                              O_error_message);
                  L_process_error := TRUE;
               end if;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
  
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SEC_GROUP;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_TL_INS( O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sec_group_tl_temp_rec  IN       SEC_GROUP_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_GROUP.EXEC_SEC_GROUP_TL_INS';
   L_table   VARCHAR2(255):= 'SVC_SEC_GROUP_TL';
BEGIN
   
   insert
     into sec_group_tl
   values I_sec_group_tl_temp_rec;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_TL_INS;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_TL_UPD( O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sec_group_tl_temp_rec   IN       SEC_GROUP_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_GROUP.EXEC_SEC_GROUP_TL_UPD';
   L_table   VARCHAR2(255):= 'SEC_GROUP_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;
   
   cursor C_LOCK_SEC_GROUP_TL_UPD is
      select 'x'
        from sec_group_tl
       where group_id = I_sec_group_tl_temp_rec.group_id
         and lang = I_sec_group_tl_temp_rec.lang
         for update nowait;
         
BEGIN
   
   L_key_val1 := 'Lang: '||to_char(I_sec_group_tl_temp_rec.lang);
   L_key_val2 := 'Group ID: '||to_char(I_sec_group_tl_temp_rec.group_id);
   
   open C_LOCK_SEC_GROUP_TL_UPD;
   close C_LOCK_SEC_GROUP_TL_UPD;
   
   
   update sec_group_tl
      set group_name = I_sec_group_tl_temp_rec.group_name,
          last_update_id = I_sec_group_tl_temp_rec.last_update_id,
          last_update_datetime = I_sec_group_tl_temp_rec.last_update_datetime
    where lang = I_sec_group_tl_temp_rec.lang
      and group_id = I_sec_group_tl_temp_rec.group_id;
   
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_SEC_GROUP_TL_UPD%ISOPEN then
         close C_LOCK_SEC_GROUP_TL_UPD;
      end if;      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_TL_UPD;
--------------------------------------------------------------------------------------
FUNCTION EXEC_SEC_GROUP_TL_DEL( O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_sec_group_tl_temp_rec  IN   SEC_GROUP_TL%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):= 'CORESVC_SEC_GROUP.EXEC_SEC_GROUP_TL_DEL';
   L_table   VARCHAR2(255):= 'SEC_GROUP_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;
   
   cursor C_LOCK_SEC_GROUP_TL_DEL is
      select 'x'
        from sec_group_tl
       where group_id = I_sec_group_tl_temp_rec.group_id
         and lang = I_sec_group_tl_temp_rec.lang
         for update nowait;
 
BEGIN
   
   L_key_val1 := 'Lang: '||to_char(I_sec_group_tl_temp_rec.lang);
   L_key_val2 := 'Group ID: '||to_char(I_sec_group_tl_temp_rec.group_id);
   
   
   open C_LOCK_SEC_GROUP_TL_DEL;
   close C_LOCK_SEC_GROUP_TL_DEL;
   
   delete sec_group_tl
    where lang     = I_sec_group_tl_temp_rec.lang
      and group_id = I_sec_group_tl_temp_rec.group_id;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             L_key_val1,
                                             L_key_val2);
      return FALSE;
   when OTHERS then
      if C_LOCK_SEC_GROUP_TL_DEL%ISOPEN then
         close C_LOCK_SEC_GROUP_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_SEC_GROUP_TL_DEL;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_SEC_GROUP_TL( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_SEC_GROUP_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_SEC_GROUP_TL.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)     := 'CORESVC_SEC_GROUP.PROCESS_SEC_GROUP_TL';
   L_table                    VARCHAR2(255)    := 'SVC_SEC_GROUP_TL';
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SEC_GROUP_TL';
   L_error                    BOOLEAN          := FALSE;
   L_sec_group_tl_temp_rec    SEC_GROUP_TL%ROWTYPE;
   

   cursor C_SVC_SEC_GROUP_TL(I_process_id NUMBER,
                             I_chunk_id NUMBER) is
      select pk_sec_group_tl.rowid  as pk_sec_group_tl_rid,
             fk_sec_group.rowid     as fk_sec_group_rid,
             st.rowid               as st_rid,
             fk_lang.rowid          as fk_lang_rid,
             st.lang,
             st.group_id,
             st.group_name,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             upper(st.action)       as action,
             st.process$status
        from svc_sec_group_tl st,
             sec_group_tl pk_sec_group_tl,
             sec_group fk_sec_group,
             lang fk_lang
       where st.process_id = I_process_id
         and st.chunk_id   = I_chunk_id
         and st.lang       = pk_sec_group_tl.lang (+)
         and st.group_id   = pk_sec_group_tl.group_id (+)
         and st.lang       = fk_lang.lang (+)
         and st.group_id   = fk_sec_group.group_id (+);
         
   TYPE SVC_SEC_GROUP_TL is TABLE OF C_SVC_SEC_GROUP_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_sec_group_tl_tab     SVC_SEC_GROUP_TL;

   L_sec_group_tl_ins_tab     SEC_GROUP_TL_tab         := NEW SEC_GROUP_TL_tab();
   L_sec_group_tl_upd_tab     SEC_GROUP_TL_tab         := NEW SEC_GROUP_TL_tab();
   L_sec_group_tl_del_tab     SEC_GROUP_TL_tab         := NEW SEC_GROUP_TL_tab();            
BEGIN
   
   if C_SVC_SEC_GROUP_TL%ISOPEN then
      close C_SVC_SEC_GROUP_TL;
   end if;
   
   open C_SVC_SEC_GROUP_TL(I_process_id,
                           I_chunk_id);
   
   LOOP
      fetch C_SVC_SEC_GROUP_TL bulk collect into L_svc_sec_group_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_sec_group_tl_tab.COUNT > 0 then
         FOR i in L_svc_sec_group_tl_tab.FIRST..L_svc_sec_group_tl_tab.LAST
         LOOP
            L_error               := FALSE;
            
            -- check if action is valid
            if L_svc_sec_group_tl_tab(i).action is NULL
               or L_svc_sec_group_tl_tab(i).action NOT IN (action_new,action_mod,action_del) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sec_group_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;
            
            --check for primary_lang
            if L_svc_sec_group_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sec_group_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;
            
            -- check if primary key values already exist
            if L_svc_sec_group_tl_tab(i).action = action_new
               and L_svc_sec_group_tl_tab(i).pk_sec_group_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sec_group_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;
            
            if L_svc_sec_group_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_sec_group_tl_tab(i).lang is NOT NULL
               and L_svc_sec_group_tl_tab(i).group_id is NOT NULL
               and L_svc_sec_group_tl_tab(i).pk_sec_group_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_sec_group_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;
            
            -- check for FK
            if L_svc_sec_group_tl_tab(i).action = action_new
               and L_svc_sec_group_tl_tab(i).group_id is NOT NULL
               and L_svc_sec_group_tl_tab(i).fk_sec_group_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_sec_group_tl_tab(i).row_seq,
                            'GROUP_ID',
                            'INV_VALUE');
               L_error :=TRUE;
            end if;
            
            if L_svc_sec_group_tl_tab(i).action = action_new
               and L_svc_sec_group_tl_tab(i).lang is NOT NULL
               and L_svc_sec_group_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sec_group_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;
            
            --check for required fields
            if L_svc_sec_group_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_sec_group_tl_tab(i).group_name is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_sec_group_tl_tab(i).row_seq,
                              'GROUP_NAME',
                              'ENTER_SEC_GROUP_NAME');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_sec_group_tl_tab(i).group_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sec_group_tl_tab(i).row_seq,
                           'GROUP_ID',
                           'FIELD_NOT_NULL');
               L_error :=TRUE;
            end if;

            if L_svc_sec_group_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_sec_group_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;
            if NOT L_error then
               L_sec_group_tl_temp_rec.lang                  := L_svc_sec_group_tl_tab(i).lang;
               L_sec_group_tl_temp_rec.group_id              := L_svc_sec_group_tl_tab(i).group_id;
               L_sec_group_tl_temp_rec.group_name            := L_svc_sec_group_tl_tab(i).group_name;
               L_sec_group_tl_temp_rec.create_id             := GET_USER;
               L_sec_group_tl_temp_rec.create_datetime       := SYSDATE;
               L_sec_group_tl_temp_rec.last_update_id        := GET_USER;
               L_sec_group_tl_temp_rec.last_update_datetime  := SYSDATE;
               if L_svc_sec_group_tl_tab(i).action = action_new then
                  if EXEC_SEC_GROUP_TL_INS(L_error_message,
                                           L_sec_group_tl_temp_rec )=FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_sec_group_tl_tab(i).row_seq,
                                 L_svc_sec_group_tl_tab(i).action,
                                 L_error_message);
                  end if;
               end if;
               if L_svc_sec_group_tl_tab(i).action = action_mod then
                  if EXEC_SEC_GROUP_TL_UPD( L_error_message,
                                            L_sec_group_tl_temp_rec )=FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_sec_group_tl_tab(i).row_seq,
                                 L_svc_sec_group_tl_tab(i).action,
                                 L_error_message);
                  end if;
               end if;
               if L_svc_sec_group_tl_tab(i).action = action_del then
                  if EXEC_SEC_GROUP_TL_DEL( L_error_message,
                                            L_sec_group_tl_temp_rec )=FALSE then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_svc_sec_group_tl_tab(i).row_seq,
                                 L_svc_sec_group_tl_tab(i).action,
                                 L_error_message);
                  end if;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_SEC_GROUP_TL%NOTFOUND;      
   END LOOP;
   close C_SVC_SEC_GROUP_TL;   
   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_SEC_GROUP_TL;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN SVC_DOC.PROCESS_ID%TYPE) IS
BEGIN           
   delete
     from svc_sec_group_tl
    where process_id = I_process_id;

   delete
     from svc_sec_group
    where process_id = I_process_id;
END CLEAR_STAGING_DATA;

------------------------------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count        OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_SEC_GROUP.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_SEC_GROUP(O_error_message,
                        I_process_id,
                        I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_SEC_GROUP_TL(O_error_message,
                           I_process_id,
                           I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   
   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                    when status = 'PE'
                    then 'PE'
                    else L_process_status
                    END),
          action_date = sysdate
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_SEC_GROUP;
/
