--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'GTT_FC_ITEM_HTS_ASSESS'
ALTER TABLE GTT_FC_ITEM_HTS_ASSESS MODIFY HTS VARCHAR (25 )
/

COMMENT ON COLUMN GTT_FC_ITEM_HTS_ASSESS.HTS is 'This column stores the harmonized tariff number whenever assessments at various levels are fetched for update.'
/

