--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ITEM_MASTER'

PROMPT Creating Index 'SVC_ITEM_MASTER_I1'
CREATE INDEX SVC_ITEM_MASTER_I1 on SVC_ITEM_MASTER
  (CHUNK_ID,
   PROCESS_ID,
   ITEM_PARENT,
   ITEM_GRANDPARENT
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

