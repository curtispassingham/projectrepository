CREATE OR REPLACE PACKAGE BODY CORESVC_HTS_DEFINITION AS

   LP_hts_heading_length     HTS_IMPORT_COUNTRY_SETUP.HTS_HEADING_LENGTH%TYPE := 0;
   LP_hts_format_mask        HTS_IMPORT_COUNTRY_SETUP.HTS_FORMAT_MASK%TYPE;
   LP_clearing_zone_exists   BOOLEAN := FALSE;

   cursor C_SVC_HTS(I_process_id NUMBER,
                    I_chunk_id   NUMBER) is
      select pk_hts.rowid                as pk_hts_rid,
             st.rowid                    as st_rid,
             hts_qca_fk.rowid            as hts_qca_fk_rid,
             hts_umc_fk3.rowid           as hts_umc_fk3_rid,
             hts_umc_fk.rowid            as hts_umc_fk_rid,
             hts_umc_fk2.rowid           as hts_umc_fk2_rid,
             cntry.rowid                 as cntry_rid,
             cd_duty.rowid               as cd_rid,
             st.cvd_ind,
             st.ad_ind,
             st.quota_ind,
             UPPER(st.quota_cat)         as quota_cat,
             st.more_hts_ind,
             UPPER(st.duty_comp_code)    as duty_comp_code,
             UPPER(st.units_3)           as units_3,
             UPPER(st.units_2)           as units_2,
             UPPER(st.units_1)           as units_1,
             st.units,
             st.HTS_DESC,
             st.effect_to,
             st.effect_from,
             UPPER(st.import_country_id) as import_country_id,
             st.hts,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action) as action,
             st.process$status
        from svc_hts st,
             hts pk_hts,
             quota_category hts_qca_fk,
             uom_class hts_umc_fk3,
             uom_class hts_umc_fk,
             uom_class hts_umc_fk2,
             code_detail cd_duty,
             country cntry,
             dual
       where st.process_id                = I_process_id
         and st.chunk_id                  = I_chunk_id
         and st.effect_to                 = pk_hts.effect_to (+)
         and st.effect_from               = pk_hts.effect_from (+)
         and UPPER(st.import_country_id)  = pk_hts.import_country_id (+)
         and st.hts                       = pk_hts.hts (+)
         and UPPER(st.units_3)            = hts_umc_fk3.uom (+)
         and UPPER(st.units_2)            = hts_umc_fk2.uom (+)
         and UPPER(st.units_1)            = hts_umc_fk.uom (+)
         and UPPER(st.quota_cat)          = hts_qca_fk.quota_cat (+)
         and UPPER(st.import_country_id)  = hts_qca_fk.import_country_id (+)
         and UPPER(st.import_country_id)  = cntry.country_id (+)
         and UPPER(st.duty_comp_code)     = cd_duty.code(+)
         and cd_duty.code_type(+)         = 'DCMP';

   cursor C_SVC_HTS_AD(I_process_id NUMBER,
                       I_chunk_id   NUMBER) IS
     select pk_hts_ad.rowid             as pk_hts_ad_rid,
            hta_hts_fk.rowid            as hta_hts_fk_rid,
            hta_sup_fk.supplier         as hta_sup_fk_rid,
            hta_cnt_fk.rowid            as hta_cnt_fk_rid,
            st.mfg_id,
            st.effect_to,
            st.effect_from,
            UPPER(st.import_country_id) as import_country_id,
            st.hts,
            st.effective_export_date,
            st.effective_entry_date,
            st.related_case_no,
            st.rate,
            st.supplier,
            st.shipper_id,
            st.case_no,
            UPPER(st.origin_country_id) as origin_country_id,
            st.process_id,
            st.chunk_id,
            st.row_seq,
            UPPER(st.action)            as action,
            st.process$status,
            st.rowid as st_rid
       from svc_hts_ad st,
            hts_ad pk_hts_ad,
            hts hta_hts_fk,
            v_sups hta_sup_fk,
            country hta_cnt_fk,
            dual
      where st.process_id                = I_process_id
        and st.chunk_id                  = I_chunk_id
        and st.mfg_id                    = pk_hts_ad.mfg_id (+)
        and st.effect_to                 = pk_hts_ad.effect_to (+)
        and st.effect_from               = pk_hts_ad.effect_from (+)
        and UPPER(st.import_country_id)  = pk_hts_ad.import_country_id (+)
        and st.hts                       = pk_hts_ad.hts (+)
        and UPPER(st.origin_country_id)  = pk_hts_ad.origin_country_id (+)
        and st.supplier                  = hta_sup_fk.supplier (+)
        and st.effect_to                 = hta_hts_fk.effect_to (+)
        and st.effect_from               = hta_hts_fk.effect_from (+)
        and UPPER(st.import_country_id)  = hta_hts_fk.import_country_id (+)
        and st.hts                       = hta_hts_fk.hts (+)
        and UPPER(st.origin_country_id)  = hta_cnt_fk.country_id (+);

   cursor C_SVC_HTS_CVD(I_process_id NUMBER,
                        I_chunk_id   NUMBER) IS
      select pk_hts_cvd.rowid            as pk_hts_cvd_rid,
             hcv_hts_fk.rowid            as hcv_hts_fk_rid,
             hcv_sup_fk.supplier         as hcv_sup_fk_rid,
             hcv_cnt_fk.rowid            as hcv_cnt_fk_rid,
             st.effective_export_date,
             st.effective_entry_date,
             st.related_case_no,
             st.rate,
             st.supplier,
             st.shipper_id,
             st.mfg_id,
             st.case_no,
             UPPER(st.origin_country_id) as origin_country_id,
             st.effect_to,
             st.effect_from,
             UPPER(st.import_country_id) as import_country_id,
             st.hts,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)            as action,
             st.process$status,
             st.rowid as st_rid
        from svc_hts_cvd st,
             hts_cvd pk_hts_cvd,
             hts hcv_hts_fk,
             v_sups hcv_sup_fk,
             country hcv_cnt_fk,
             dual
       where st.process_id                 = I_process_id
         and st.chunk_id                   = I_chunk_id
         and UPPER(st.origin_country_id)   = pk_hts_cvd.origin_country_id (+)
         and st.effect_to                  = pk_hts_cvd.effect_to (+)
         and st.effect_from                = pk_hts_cvd.effect_from (+)
         and UPPER(st.import_country_id)   = pk_hts_cvd.import_country_id (+)
         and st.hts                        = pk_hts_cvd.hts (+)
         and st.supplier                   = hcv_sup_fk.supplier (+)
         and st.effect_to                  = hcv_hts_fk.effect_to (+)
         and st.effect_from                = hcv_hts_fk.effect_from (+)
         and UPPER(st.import_country_id)   = hcv_hts_fk.import_country_id (+)
         and st.hts                        = hcv_hts_fk.hts (+)
         and UPPER(st.origin_country_id)   = hcv_cnt_fk.country_id (+);

   cursor C_SVC_HTS_FEE(I_process_id NUMBER,
                        I_chunk_id   NUMBER) is
      select pk_hts_fee.rowid               as pk_hts_fee_rid,
             htf_hts_fk.rowid               as htf_hts_fk_rid,
             st.fee_av_rate,
             st.fee_specific_rate,
             upper(st.fee_comp_code)        as fee_comp_code,
             upper(st.fee_type)             as fee_type,
             st.effect_to,
             st.effect_from,
             upper(st.import_country_id)    as import_country_id,
             st.hts,
             pk_hts_fee.fee_type            as old_fee_type,
             pk_hts_fee.fee_specific_rate   as old_fee_specific_rate,
             pk_hts_fee.fee_av_rate         as old_fee_av_rate,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)               as action,
             st.rowid                       as st_rid,
             st.process$status
        from svc_hts_fee st,
             hts_fee pk_hts_fee,
             hts htf_hts_fk
       where st.process_id               = I_process_id
         and st.chunk_id                 = I_chunk_id
         and upper(st.fee_type)          = pk_hts_fee.fee_type (+)
         and st.effect_to                = pk_hts_fee.effect_to (+)
         and st.effect_from              = pk_hts_fee.effect_from (+)
         and upper(st.import_country_id) = pk_hts_fee.import_country_id (+)
         and st.hts                      = pk_hts_fee.hts (+)
         and st.effect_to                = htf_hts_fk.effect_to (+)
         and st.effect_from              = htf_hts_fk.effect_from (+)
         and upper(st.import_country_id) = htf_hts_fk.import_country_id (+)
         and st.hts                      = htf_hts_fk.hts (+);

   cursor C_SVC_HTS_FEE_ZONE(I_process_id NUMBER,
                             I_chunk_id   NUMBER) is
      select pk_hts_fee_zone.rowid              as pk_hts_fee_zone_rid,
             hfz_htf_fk.rowid                   as hfz_htf_fk_rid,
             outl.rowid                         as outl_fk_rid,
             st.hts,
             upper(st.import_country_id)        as import_country_id,
             st.effect_from,
             st.effect_to,
             upper(st.fee_type)                 as fee_type,
             upper(st.clearing_zone_id)         as clearing_zone_id,
             st.fee_specific_rate,
             st.fee_av_rate,
             pk_hts_fee_zone.clearing_zone_id   as old_clearing_zone_id,
             st.rowid                           as st_rid,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             outl.outloc_id,
             outl.outloc_desc,
             outl.outloc_type,
             UPPER(st.action)                   as action,
             st.process$status
        from svc_hts_fee_zone st,
             hts_fee_zone pk_hts_fee_zone,
             hts_fee hfz_htf_fk,
             outloc outl
       where st.process_id               = I_process_id
         and st.chunk_id                 = I_chunk_id
         and upper(st.clearing_zone_id)  = pk_hts_fee_zone.clearing_zone_id (+)
         and upper(st.fee_type)          = pk_hts_fee_zone.fee_type (+)
         and st.effect_to                = pk_hts_fee_zone.effect_to (+)
         and st.effect_from              = pk_hts_fee_zone.effect_from (+)
         and upper(st.import_country_id) = pk_hts_fee_zone.import_country_id (+)
         and st.hts                      = pk_hts_fee_zone.hts (+)
         and upper(st.fee_type)          = hfz_htf_fk.fee_type (+)
         and st.effect_to                = hfz_htf_fk.effect_to (+)
         and st.effect_from              = hfz_htf_fk.effect_from (+)
         and upper(st.import_country_id) = hfz_htf_fk.import_country_id (+)
         and st.hts                      = hfz_htf_fk.hts (+)
         and upper(st.clearing_zone_id)  = outl.outloc_id (+);

   cursor C_SVC_HTS_OGA(I_process_id NUMBER,
                        I_chunk_id   NUMBER) is
      select pk_hts_oga.rowid            as pk_hts_oga_rid,
             hto_hts_fk.rowid            as hto_hts_fk_rid,
             hto_oga_fk.rowid            as hto_oga_fk_rid,
             st.rowid                    as st_rid,
             st.comments,
             st.reference_id,
             UPPER(st.oga_code)          as oga_code,
             st.effect_to,
             st.effect_from,
             UPPER(st.import_country_id) as import_country_id,
             st.hts,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)            as action,
             st.process$status
        from svc_hts_oga st,
             hts_oga pk_hts_oga,
             hts hto_hts_fk,
             oga hto_oga_fk
       where st.process_id               = I_process_id
         and st.chunk_id                 = I_chunk_id
         and UPPER(st.oga_code)          = pk_hts_oga.oga_code (+)
         and st.effect_to                = pk_hts_oga.effect_to (+)
         and st.effect_from              = pk_hts_oga.effect_from (+)
         and UPPER(st.import_country_id) = pk_hts_oga.import_country_id (+)
         and st.hts                      = pk_hts_oga.hts (+)
         and UPPER(st.oga_code)          = hto_oga_fk.oga_code (+)
         and st.effect_to                = hto_hts_fk.effect_to (+)
         and st.effect_from              = hto_hts_fk.effect_from (+)
         and UPPER(st.import_country_id) = hto_hts_fk.import_country_id (+)
         and st.hts                      = hto_hts_fk.hts (+);

   cursor C_SVC_HTS_REF(I_process_id NUMBER,
                        I_chunk_id   NUMBER) is
      select pk_hts_reference.rowid               as pk_hts_reference_rid,
             htr_hts_fk.rowid                     as htr_hts_fk_rid,
             st.rowid                             as st_rid,
             st.reference_desc,
             UPPER(st.reference_id)               as reference_id,
             TO_DATE(st.effect_to,'DD-MON-YY')    as effect_to,
             TO_DATE(st.effect_from,'DD-MON-YY')  as effect_from,
             UPPER(st.import_country_id)          as import_country_id,
             st.hts,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)                     as action,
             st.process$status
        from svc_hts_ref   st,
             hts_reference pk_hts_reference,
             hts           htr_hts_fk
       where st.process_id                       = I_process_id
         and st.chunk_id                         = I_chunk_id
         and UPPER(st.reference_id)              = pk_hts_reference.reference_id (+)
         and TO_DATE(st.effect_to,'DD-MON-YY')   = pk_hts_reference.effect_to (+)
         and TO_DATE(st.effect_from,'DD-MON-YY') = pk_hts_reference.effect_from (+)
         and UPPER(st.import_country_id)         = pk_hts_reference.import_country_id (+)
         and st.hts                              = pk_hts_reference.hts (+)
         and TO_DATE(st.effect_to,'DD-MON-YY')   = htr_hts_fk.effect_to (+)
         and TO_DATE(st.effect_from,'DD-MON-YY') = htr_hts_fk.effect_from (+)
         and UPPER(st.import_country_id)         = htr_hts_fk.import_country_id (+)
         and st.hts                              = htr_hts_fk.hts (+);

   cursor C_SVC_HTT(I_process_id NUMBER,
                    I_chunk_id   NUMBER)   is
      select pk_hts_tariff_treatment.rowid         as pk_hts_tariff_treatment_rid,
             htt_hts_fk.rowid                      as htt_hts_fk_rid,
             htt_ttt_fk.rowid                      as htt_ttt_fk_rid,
             st.other_rate,
             st.av_rate,
             st.specific_rate,
             UPPER(st.tariff_treatment)            as tariff_treatment,
             st.effect_to,
             st.effect_from,
             UPPER(st.import_country_id)           as import_country_id,
             pk_hts_tariff_treatment.specific_rate as old_specific_rate,
             pk_hts_tariff_treatment.av_rate       as old_av_rate,
             pk_hts_tariff_treatment.other_rate    as old_other_rate,
             st.hts,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             htt_ttt_fk.tariff_treatment_desc,
             UPPER(st.action)                      as action,
             st.rowid                              as st_rid,
             st.process$status
        from svc_hts_tariff_treatment st,
             hts_tariff_treatment   pk_hts_tariff_treatment,
             hts                    htt_hts_fk,
             tariff_treatment       htt_ttt_fk
       where st.process_id               = I_process_id
         and st.chunk_id                 = I_chunk_id
         and UPPER(st.tariff_treatment)  = pk_hts_tariff_treatment.tariff_treatment (+)
         and st.effect_to                = pk_hts_tariff_treatment.effect_to (+)
         and st.effect_from              = pk_hts_tariff_treatment.effect_from (+)
         and UPPER(st.import_country_id) = pk_hts_tariff_treatment.import_country_id (+)
         and st.hts                      = pk_hts_tariff_treatment.hts (+)
         and UPPER(st.tariff_treatment)  = htt_ttt_fk.tariff_treatment (+)
         and st.effect_to                = htt_hts_fk.effect_to (+)
         and st.effect_from              = htt_hts_fk.effect_from (+)
         and UPPER(st.import_country_id) = htt_hts_fk.import_country_id (+)
         and st.hts                      = htt_hts_fk.hts (+);

   cursor C_SVC_HTS_TT_EXCLUSIONS(I_process_id NUMBER,
                                  I_chunk_id   NUMBER) is
      select pk_hts_tt_exclusions.rowid  as pk_hts_tt_exclusions_rid,
             hte_htt_fk.rowid            as hte_htt_fk_rid,
             hte_cnt_fk.rowid            as hte_cnt_fk_rid,
             hte_ttt_fk.rowid            as hte_ttt_fk_rid,
             UPPER(st.origin_country_id) as origin_country_id ,
             UPPER(st.tariff_treatment)  as tariff_treatment,
             st.effect_to,
             st.effect_from,
             UPPER(st.import_country_id) as import_country_id,
             st.hts,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             hte_cnt_fk.country_desc,
             UPPER(st.action)            as action,
             st.rowid                    as st_rid,
             st.process$status
        from svc_hts_tt_exclusions st,
             hts_tt_exclusions       pk_hts_tt_exclusions,
             hts_tariff_treatment    hte_htt_fk,
             country                 hte_cnt_fk,
             tariff_treatment        hte_ttt_fk
       where st.process_id                = I_process_id
         and st.chunk_id                  = I_chunk_id
         and UPPER(st.origin_country_id)  = pk_hts_tt_exclusions.origin_country_id (+)
         and UPPER(st.tariff_treatment)   = pk_hts_tt_exclusions.tariff_treatment (+)
         and st.effect_to                 = pk_hts_tt_exclusions.effect_to (+)
         and st.effect_from               = pk_hts_tt_exclusions.effect_from (+)
         and UPPER(st.import_country_id)  = pk_hts_tt_exclusions.import_country_id (+)
         and st.hts                       = pk_hts_tt_exclusions.hts (+)
         and UPPER(st.tariff_treatment)   = hte_ttt_fk.tariff_treatment (+)
         and UPPER(st.tariff_treatment)   = hte_htt_fk.tariff_treatment (+)
         and st.effect_to                 = hte_htt_fk.effect_to (+)
         and st.effect_from               = hte_htt_fk.effect_from (+)
         and UPPER(st.import_country_id)  = hte_htt_fk.import_country_id (+)
         and st.hts                       = hte_htt_fk.hts (+)
         and UPPER(st.origin_country_id)  = hte_cnt_fk.country_id (+);

   cursor C_SVC_HTS_TT_ZONE(I_process_id NUMBER,
                            I_chunk_id NUMBER) is
      select pk_hts_tariff_treatment_zone.rowid            as pk_hts_tariff_treatment_zo_rid,
             haz_htt_fk.rowid                              as haz_htt_fk_rid,
             haz_ttt_fk.rowid                              as haz_ttt_fk_rid,
             st.other_rate,
             st.av_rate,
             st.specific_rate,
             UPPER(st.clearing_zone_id)                    as clearing_zone_id,
             UPPER(st.tariff_treatment)                    as tariff_treatment,
             st.effect_to,
             st.effect_from,
             pk_hts_tariff_treatment_zone.clearing_zone_id as old_clearing_zone_id,
             UPPER(st.import_country_id)                   as import_country_id,
             st.hts,
             outl.outloc_id,
             outl.outloc_desc,
             outl.outloc_type,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)                              as action,
             st.rowid                                      as st_rid,
             st.process$status
        from svc_hts_tariff_treatment_zone st,
             hts_tariff_treatment_zone     pk_hts_tariff_treatment_zone,
             hts_tariff_treatment          haz_htt_fk,
             tariff_treatment              haz_ttt_fk,
             outloc                        outl
       where st.process_id               = I_process_id
         and st.chunk_id                 = I_chunk_id
         and UPPER(st.clearing_zone_id)  = pk_hts_tariff_treatment_zone.clearing_zone_id (+)
         and UPPER(st.tariff_treatment)  = pk_hts_tariff_treatment_zone.tariff_treatment (+)
         and st.effect_to                = pk_hts_tariff_treatment_zone.effect_to (+)
         and st.effect_from              = pk_hts_tariff_treatment_zone.effect_from (+)
         and UPPER(st.import_country_id) = pk_hts_tariff_treatment_zone.import_country_id (+)
         and st.hts                      = pk_hts_tariff_treatment_zone.hts (+)
         and UPPER(st.tariff_treatment)  = haz_ttt_fk.tariff_treatment (+)
         and UPPER(st.tariff_treatment)  = haz_htt_fk.tariff_treatment (+)
         and st.effect_to                = haz_htt_fk.effect_to (+)
         and st.effect_from              = haz_htt_fk.effect_from (+)
         and UPPER(st.import_country_id) = haz_htt_fk.import_country_id (+)
         and st.hts                      = haz_htt_fk.hts (+)
         and UPPER(st.clearing_zone_id)  = outl.outloc_id (+)
         and outl.outloc_type (+)        = 'CZ';

   cursor C_SVC_HTS_TAX(I_process_id NUMBER,
                        I_chunk_id   NUMBER) is
      select pk_hts_tax.rowid            as pk_hts_tax_rid,
             htx_hts_fk.rowid            as htx_hts_fk_rid,
             st.hts,
             UPPER(st.import_country_id) as import_country_id,
             st.effect_from,
             st.effect_to,
             UPPER(st.tax_type)          as tax_type,
             UPPER(st.tax_comp_code)     as tax_comp_code,
             st.tax_specific_rate,
             st.tax_av_rate,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)            as action,
             st.process$status,
             st.rowid  as  st_rid
        from svc_hts_tax st,
             hts_tax pk_hts_tax,
             hts htx_hts_fk
       where st.process_id                = I_process_id
         and st.chunk_id                  = I_chunk_id
         and UPPER(st.tax_type)           = pk_hts_tax.tax_type (+)
         and st.effect_to                 = pk_hts_tax.effect_to (+)
         and st.effect_from               = pk_hts_tax.effect_from (+)
         and UPPER(st.import_country_id)  = pk_hts_tax.import_country_id (+)
         and st.hts                       = pk_hts_tax.hts (+)
         and st.effect_to                 = htx_hts_fk.effect_to (+)
         and st.effect_from               = htx_hts_fk.effect_from (+)
         and UPPER(st.import_country_id)  = htx_hts_fk.import_country_id (+)
         and st.hts                       = htx_hts_fk.hts (+);

   cursor C_SVC_HTS_TAX_ZONE(I_process_id NUMBER,
                             I_chunk_id   NUMBER) IS
      select pk_hts_tax_zone.rowid       as pk_hts_tax_zone_rid,
             htz_htx_fk.rowid            as htz_htx_fk_rid,
             st.tax_av_rate,
             st.tax_specific_rate,
             UPPER(st.clearing_zone_id)  as clearing_zone_id,
             UPPER(st.tax_type)          as tax_type,
             st.effect_to,
             st.effect_from,
             UPPER(st.import_country_id) as import_country_id,
             st.hts,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)            as action,
             st.process$status,
             outl.outloc_id,
             outl.outloc_desc,
             outl.outloc_type,
             st.rowid                    as st_rid
        from svc_hts_tax_zone  st,
             hts_tax_zone      pk_hts_tax_zone,
             hts_tax           htz_htx_fk,
             outloc            outl,
             dual
       where st.process_id               = I_process_id
         and st.chunk_id                 = I_chunk_id
         and UPPER(st.clearing_zone_id)  = pk_hts_tax_zone.clearing_zone_id (+)
         and UPPER(st.tax_type)          = pk_hts_tax_zone.tax_type (+)
         and st.effect_to                = pk_hts_tax_zone.effect_to (+)
         and st.effect_from              = pk_hts_tax_zone.effect_from(+)
         and UPPER(st.import_country_id) = pk_hts_tax_zone.import_country_id (+)
         and st.hts                      = pk_hts_tax_zone.hts (+)
         and UPPER(st.tax_type)          = htz_htx_fk.tax_type (+)
         and st.effect_to                = htz_htx_fk.effect_to (+)
         and st.effect_from              = htz_htx_fk.effect_from(+)
         and UPPER(st.import_country_id) = htz_htx_fk.import_country_id (+)
         and st.hts                      = htz_htx_fk.hts (+)
         and UPPER(st.clearing_zone_id)  = outl.outloc_id (+);

   TYPE LP_errors_tab_typ is TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab LP_errors_tab_typ;
   TYPE LP_s9t_errors_tab_typ is TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab LP_s9t_errors_tab_typ;

   TYPE HTS_TL_TAB is TABLE OF HTS_TL%ROWTYPE;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;
   
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;

   LP_primary_lang    LANG.LANG%TYPE;
-----------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if SHEET_NAME_TRANS.EXISTS(I_sheet_name) then
      return SHEET_NAME_TRANS(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
-----------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).file_id              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_seq_no         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).template_key         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).wksht_key            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).column_key           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).error_key            := (
                                                                        CASE
                                                                           WHEN I_sqlcode is NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_id            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).create_datetime      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_id       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).last_update_datetime := SYSDATE;
END WRITE_S9T_ERROR;
-----------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   LP_errors_tab.extend();
   LP_errors_tab(LP_errors_tab.count()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.count()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.count()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.count()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.count()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.count()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.count()).error_msg   := I_error_msg;
   LP_errors_tab(LP_errors_tab.count()).error_type  := I_error_type;
END WRITE_ERROR;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets            S9T_PKG.NAMES_MAP_TYP;
   HTS_cols            S9T_PKG.NAMES_MAP_TYP;
   HTS_TL_cols         S9T_PKG.NAMES_MAP_TYP;
   HTT_cols            S9T_PKG.NAMES_MAP_TYP;
   HTS_TT_EXC_cols     S9T_PKG.NAMES_MAP_TYP;
   HTS_TT_ZONE_cols    S9T_PKG.NAMES_MAP_TYP;
   HTS_FEE_cols        S9T_PKG.NAMES_MAP_TYP;
   HTS_FEE_ZONE_cols   S9T_PKG.NAMES_MAP_TYP;
   HTS_TAX_cols        S9T_PKG.NAMES_MAP_TYP;
   HTS_TAX_ZONE_cols   S9T_PKG.NAMES_MAP_TYP;
   HTS_AD_cols         S9T_PKG.NAMES_MAP_TYP;
   HTS_CVD_cols        S9T_PKG.NAMES_MAP_TYP;
   HTS_REF_cols        S9T_PKG.NAMES_MAP_TYP;
   HTS_OGA_cols        S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                       := S9T_PKG.GET_SHEET_NAMES(I_file_id);
   HTS_cols                       := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_sheet);
   HTS$Action                     := HTS_cols('ACTION');
   HTS$HTS                        := HTS_cols('HTS');
   HTS$IMPORT_COUNTRY_ID          := HTS_cols('IMPORT_COUNTRY_ID');
   HTS$EFFECT_FROM                := HTS_cols('EFFECT_FROM');
   HTS$EFFECT_TO                  := HTS_cols('EFFECT_TO');
   HTS$HTS_DESC                   := HTS_cols('HTS_DESC');
   HTS$UNITS                      := HTS_cols('UNITS');
   HTS$UNITS_1                    := HTS_cols('UNITS_1');
   HTS$UNITS_2                    := HTS_cols('UNITS_2');
   HTS$UNITS_3                    := HTS_cols('UNITS_3');
   HTS$DUTY_COMP_CODE             := HTS_cols('DUTY_COMP_CODE');
   HTS$MORE_HTS_IND               := HTS_cols('MORE_HTS_IND');
   HTS$QUOTA_CAT                  := HTS_cols('QUOTA_CAT');
   HTS$QUOTA_IND                  := HTS_cols('QUOTA_IND');
   HTS$AD_IND                     := HTS_cols('AD_IND');
   HTS$CVD_IND                    := HTS_cols('CVD_IND');

   HTS_TL_cols                    := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_TL_sheet);
   HTS_TL$ACTION                  := HTS_TL_cols('ACTION');
   HTS_TL$LANG                    := HTS_TL_cols('LANG');
   HTS_TL$HTS                     := HTS_TL_cols('HTS');
   HTS_TL$IMPORT_COUNTRY_ID       := HTS_TL_cols('IMPORT_COUNTRY_ID');
   HTS_TL$EFFECT_FROM             := HTS_TL_cols('EFFECT_FROM');
   HTS_TL$EFFECT_TO               := HTS_TL_cols('EFFECT_TO');
   HTS_TL$HTS_DESC                := hts_TL_cols('HTS_DESC');

   HTT_cols                       := S9T_PKG.GET_COL_NAMES(I_file_id,HTT_sheet);
   HTT$ACTION                     := HTT_cols('ACTION');
   HTT$HTS                        := HTT_cols('HTS');
   HTT$IMPORT_COUNTRY_ID          := HTT_cols('IMPORT_COUNTRY_ID');
   HTT$EFFECT_FROM                := HTT_cols('EFFECT_FROM');
   HTT$EFFECT_TO                  := HTT_cols('EFFECT_TO');
   HTT$TARIFF_TREATMENT           := HTT_cols('TARIFF_TREATMENT');
   HTT$SPECIFIC_RATE              := HTT_cols('SPECIFIC_RATE');
   HTT$AV_RATE                    := HTT_cols('AV_RATE');
   HTT$OTHER_RATE                 := HTT_cols('OTHER_RATE');

   HTS_TT_EXC_cols                := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_TT_EXC_sheet);
   HTS_TT_EXC$ACTION              := HTS_TT_EXC_cols('ACTION');
   HTS_TT_EXC$HTS                 := HTS_TT_EXC_cols('HTS');
   HTS_TT_EXC$IMPORT_COUNTRY_ID   := HTS_TT_EXC_cols('IMPORT_COUNTRY_ID');
   HTS_TT_EXC$EFFECT_FROM         := HTS_TT_EXC_cols('EFFECT_FROM');
   HTS_TT_EXC$EFFECT_TO           := HTS_TT_EXC_cols('EFFECT_TO');
   HTS_TT_EXC$TARIFF_TREATMENT    := HTS_TT_EXC_cols('TARIFF_TREATMENT');
   HTS_TT_EXC$ORIGIN_COUNTRY_ID   := HTS_TT_EXC_cols('ORIGIN_COUNTRY_ID');

   HTS_TT_ZONE_cols               := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_TT_ZONE_sheet);
   HTS_TT_ZONE$ACTION             := HTS_TT_ZONE_cols('ACTION');
   HTS_TT_ZONE$HTS                := HTS_TT_ZONE_cols('HTS');
   HTS_TT_ZONE$IMPORT_COUNTRY_ID  := HTS_TT_ZONE_cols('IMPORT_COUNTRY_ID');
   HTS_TT_ZONE$EFFECT_FROM        := HTS_TT_ZONE_cols('EFFECT_FROM');
   HTS_TT_ZONE$EFFECT_TO          := HTS_TT_ZONE_cols('EFFECT_TO');
   HTS_TT_ZONE$TARIFF_TREATMENT   := HTS_TT_ZONE_cols('TARIFF_TREATMENT');
   HTS_TT_ZONE$CLEARING_ZONE_ID   := HTS_TT_ZONE_cols('CLEARING_ZONE_ID');
   HTS_TT_ZONE$SPECIFIC_RATE      := HTS_TT_ZONE_cols('SPECIFIC_RATE');
   HTS_TT_ZONE$AV_RATE            := HTS_TT_ZONE_cols('AV_RATE');
   HTS_TT_ZONE$OTHER_RATE         := HTS_TT_ZONE_cols('OTHER_RATE');
   
   HTS_FEE_cols                   := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_FEE_sheet);
   HTS_FEE$ACTION                 := HTS_FEE_cols('ACTION');
   HTS_FEE$HTS                    := HTS_FEE_cols('HTS');
   HTS_FEE$IMPORT_COUNTRY_ID      := HTS_FEE_cols('IMPORT_COUNTRY_ID');
   HTS_FEE$EFFECT_FROM            := HTS_FEE_cols('EFFECT_FROM');
   HTS_FEE$EFFECT_TO              := HTS_FEE_cols('EFFECT_TO');
   HTS_FEE$FEE_TYPE               := HTS_FEE_cols('FEE_TYPE');
   HTS_FEE$FEE_COMP_CODE          := HTS_FEE_cols('FEE_COMP_CODE');
   HTS_FEE$FEE_SPECIFIC_RATE      := HTS_FEE_cols('FEE_SPECIFIC_RATE');
   HTS_FEE$FEE_AV_RATE            := HTS_FEE_cols('FEE_AV_RATE');

   HTS_FEE_ZONE_cols              := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_FEE_ZONE_sheet);
   HTS_FEE_ZONE$ACTION            := HTS_FEE_ZONE_cols('ACTION');
   HTS_FEE_ZONE$HTS               := HTS_FEE_ZONE_cols('HTS');
   HTS_FEE_ZONE$IMPORT_COUNTRY_ID := HTS_FEE_ZONE_cols('IMPORT_COUNTRY_ID');
   HTS_FEE_ZONE$EFFECT_FROM       := HTS_FEE_ZONE_cols('EFFECT_FROM');
   HTS_FEE_ZONE$EFFECT_TO         := HTS_FEE_ZONE_cols('EFFECT_TO');
   HTS_FEE_ZONE$FEE_TYPE          := HTS_FEE_ZONE_cols('FEE_TYPE');
   HTS_FEE_ZONE$CLEARING_ZONE_ID  := HTS_FEE_ZONE_cols('CLEARING_ZONE_ID');
   HTS_FEE_ZONE$FEE_SPECIFIC_RATE := HTS_FEE_ZONE_cols('FEE_SPECIFIC_RATE');
   HTS_FEE_ZONE$FEE_AV_RATE       := HTS_FEE_ZONE_cols('FEE_AV_RATE');

   HTS_TAX_cols                   := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_TAX_sheet);
   HTS_TAX$Action                 := HTS_TAX_cols('ACTION');
   HTS_TAX$HTS                    := HTS_TAX_cols('HTS');
   HTS_TAX$IMPORT_COUNTRY_ID      := HTS_TAX_cols('IMPORT_COUNTRY_ID');
   HTS_TAX$EFFECT_FROM            := HTS_TAX_cols('EFFECT_FROM');
   HTS_TAX$EFFECT_TO              := HTS_TAX_cols('EFFECT_TO');
   HTS_TAX$TAX_TYPE               := HTS_TAX_cols('TAX_TYPE');
   HTS_TAX$TAX_COMP_CODE          := HTS_TAX_cols('TAX_COMP_CODE');
   HTS_TAX$TAX_SPECIFIC_RATE      := HTS_TAX_cols('TAX_SPECIFIC_RATE');
   HTS_TAX$TAX_AV_RATE            := HTS_TAX_cols('TAX_AV_RATE');

   HTS_TAX_ZONE_COLS              := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_TAX_ZONE_sheet);
   HTS_TAX_ZONE$ACTION            := HTS_TAX_ZONE_COLS('ACTION');
   HTS_TAX_ZONE$TAX_AV_RATE       := HTS_TAX_ZONE_COLS('TAX_AV_RATE');
   HTS_TAX_ZONE$TAX_SPECIFIC_RATE := HTS_TAX_ZONE_COLS('TAX_SPECIFIC_RATE');
   HTS_TAX_ZONE$CLEARING_ZONE_ID  := HTS_TAX_ZONE_COLS('CLEARING_ZONE_ID');
   HTS_TAX_ZONE$TAX_TYPE          := HTS_TAX_ZONE_COLS('TAX_TYPE');
   HTS_TAX_ZONE$EFFECT_TO         := HTS_TAX_ZONE_COLS('EFFECT_TO');
   HTS_TAX_ZONE$EFFECT_FROM       := HTS_TAX_ZONE_COLS('EFFECT_FROM');
   HTS_TAX_ZONE$IMPORT_COUNTRY_ID := HTS_TAX_ZONE_COLS('IMPORT_COUNTRY_ID');
   HTS_TAX_ZONE$HTS               := HTS_TAX_ZONE_COLS('HTS');

   HTS_AD_cols                    := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_AD_sheet);
   HTS_AD$ACTION                  := HTS_AD_COLS('ACTION');
   HTS_AD$MFG_ID                  := HTS_AD_COLS('MFG_ID');
   HTS_AD$EFFECT_TO               := HTS_AD_COLS('EFFECT_TO');
   HTS_AD$EFFECT_FROM             := HTS_AD_COLS('EFFECT_FROM');
   HTS_AD$IMPORT_COUNTRY_ID       := HTS_AD_COLS('IMPORT_COUNTRY_ID');
   HTS_AD$HTS                     := HTS_AD_COLS('HTS');
   HTS_AD$EFFECTIVE_EXPORT_DATE   := HTS_AD_COLS('EFFECTIVE_EXPORT_DATE');
   HTS_AD$EFFECTIVE_ENTRY_DATE    := HTS_AD_COLS('EFFECTIVE_ENTRY_DATE');
   HTS_AD$RELATED_CASE_NO         := HTS_AD_COLS('RELATED_CASE_NO');
   HTS_AD$RATE                    := HTS_AD_COLS('RATE');
   HTS_AD$SUPPLIER                := HTS_AD_COLS('SUPPLIER');
   HTS_AD$SHIPPER_ID              := HTS_AD_COLS('SHIPPER_ID');
   HTS_AD$CASE_NO                 := HTS_AD_COLS('CASE_NO');
   HTS_AD$ORIGIN_COUNTRY_ID       := HTS_AD_COLS('ORIGIN_COUNTRY_ID');

   HTS_CVD_cols                   := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_CVD_sheet);
   HTS_CVD$ACTION                 := HTS_CVD_COLS('ACTION');
   HTS_CVD$EFFECTIVE_EXPORT_DATE  := HTS_CVD_COLS('EFFECTIVE_EXPORT_DATE');
   HTS_CVD$EFFECTIVE_ENTRY_DATE   := HTS_CVD_COLS('EFFECTIVE_ENTRY_DATE');
   HTS_CVD$RELATED_CASE_NO        := HTS_CVD_COLS('RELATED_CASE_NO');
   HTS_CVD$RATE                   := HTS_CVD_COLS('RATE');
   HTS_CVD$SUPPLIER               := HTS_CVD_COLS('SUPPLIER');
   HTS_CVD$SHIPPER_ID             := HTS_CVD_COLS('SHIPPER_ID');
   HTS_CVD$MFG_ID                 := HTS_CVD_COLS('MFG_ID');
   HTS_CVD$CASE_NO                := HTS_CVD_COLS('CASE_NO');
   HTS_CVD$ORIGIN_COUNTRY_ID      := HTS_CVD_COLS('ORIGIN_COUNTRY_ID');
   HTS_CVD$EFFECT_TO              := HTS_CVD_COLS('EFFECT_TO');
   HTS_CVD$EFFECT_FROM            := HTS_CVD_COLS('EFFECT_FROM');
   HTS_CVD$IMPORT_COUNTRY_ID      := HTS_CVD_COLS('IMPORT_COUNTRY_ID');
   HTS_CVD$HTS                    := HTS_CVD_COLS('HTS');

   HTS_REF_cols                   := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_REF_sheet);
   HTS_REF$ACTION                 := HTS_REF_cols('ACTION');
   HTS_REF$HTS                    := HTS_REF_cols('HTS');
   HTS_REF$IMPORT_COUNTRY_ID      := HTS_REF_cols('IMPORT_COUNTRY_ID');
   HTS_REF$EFFECT_FROM            := HTS_REF_cols('EFFECT_FROM');
   HTS_REF$EFFECT_TO              := HTS_REF_cols('EFFECT_TO');
   HTS_REF$reference_id           := HTS_REF_cols('REFERENCE_ID');
   HTS_REF$REFERENCE_DESC         := HTS_REF_cols('REFERENCE_DESC');

   HTS_OGA_cols                   := S9T_PKG.GET_COL_NAMES(I_file_id,HTS_OGA_sheet);
   HTS_OGA$Action                 := HTS_OGA_cols('ACTION');
   HTS_OGA$HTS                    := HTS_OGA_cols('HTS');
   HTS_OGA$IMPORT_COUNTRY_ID      := HTS_OGA_cols('IMPORT_COUNTRY_ID');
   HTS_OGA$EFFECT_FROM            := HTS_OGA_cols('EFFECT_FROM');
   HTS_OGA$EFFECT_TO              := HTS_OGA_cols('EFFECT_TO');
   HTS_OGA$OGA_CODE               := HTS_OGA_cols('OGA_CODE');
   HTS_OGA$REFERENCE_ID           := HTS_OGA_cols('REFERENCE_ID');
   HTS_OGA$COMMENTS               := HTS_OGA_cols('COMMENTS');
END POPULATE_NAMES;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_sheet)
      select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               HTS_DESC,
                               units,
                               units_1,
                               units_2,
                               units_3,
                               duty_comp_code,
                               more_hts_ind,
                               quota_cat,
                               quota_ind,
                               ad_ind,
                               cvd_ind))
        from hts;
END POPULATE_HTS;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_TL(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_TL_sheet)
      select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                               lang,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               hts_desc))
        from hts_tl;
END POPULATE_HTS_TL;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_AD(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_AD_sheet)
      select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               mfg_id,
                               origin_country_id,
                               case_no,
                               shipper_id,
                               supplier,
                               rate,
                               related_case_no,
                               effective_entry_date,
                               effective_export_date))
        from hts_ad;
END POPULATE_HTS_AD;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_CVD(I_file_id IN NUMBER)IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_CVD_sheet)
      select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               origin_country_id,
                               case_no,
                               mfg_id,
                               shipper_id,
                               supplier,
                               rate,
                               related_case_no,
                               effective_entry_date,
                               effective_export_date))
        from hts_cvd;
END POPULATE_HTS_CVD;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_FEE(I_file_id IN NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_FEE_sheet)
      select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               fee_type,
                               fee_comp_code,
                               fee_specific_rate,
                               fee_av_rate))
        from hts_fee;
END POPULATE_HTS_FEE;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_FEE_ZONE(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                      from s9t_folder sf,
                           TABLE(sf.s9t_file_obj.sheets) ss
                     where sf.file_id  = I_file_id
                       and ss.sheet_name = HTS_FEE_ZONE_sheet)
      select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               fee_type,
                               clearing_zone_id,
                               fee_specific_rate,
                               fee_av_rate))
        from hts_fee_zone;
END POPULATE_HTS_FEE_ZONE;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_OGA(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_OGA_sheet)
      select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               oga_code,
                               reference_id,
                               comments))
        from hts_oga;
END POPULATE_HTS_OGA;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_REF(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = HTS_REF_sheet)
   select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                            hts,
                            import_country_id,
                            effect_from,
                            effect_to,
                            reference_id,
                            reference_desc))
     from hts_reference;
END POPULATE_HTS_REF;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTT(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTT_sheet)
      select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               tariff_treatment,
                               specific_rate,
                               av_rate,
                               other_rate))
        from hts_tariff_treatment;
END POPULATE_HTT;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_TT_EXCLUSIONS(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id    = I_file_id
                        and ss.sheet_name = HTS_TT_EXC_sheet)
      select s9t_row(s9t_cells(NULL,
                               hts,
                               import_country_id,
                               effect_from,
                               effect_to,
                               tariff_treatment,
                               origin_country_id))
        from hts_tt_exclusions;
END POPULATE_HTS_TT_EXCLUSIONS;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_TT_ZONE(I_file_id   IN   NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_TT_ZONE_sheet)
   select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                            hts,
                            import_country_id,
                            effect_from,
                            effect_to,
                            tariff_treatment,
                            clearing_zone_id,
                            specific_rate,
                            av_rate,
                            other_rate))
     from hts_tariff_treatment_zone;
END POPULATE_HTS_TT_ZONE;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_TAX(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                      table (sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_TAX_sheet)
   select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                            hts,
                            import_country_id,
                            effect_from,
                            effect_to,
                            tax_type,
                            tax_comp_code,
                            tax_specific_rate,
                            tax_av_rate))
     from hts_tax;
END POPULATE_HTS_TAX;
-----------------------------------------------------------------------------------------------
PROCEDURE POPULATE_HTS_TAX_ZONE(I_file_id IN NUMBER) IS
BEGIN
   insert into TABLE(select ss.s9t_rows
                       from s9t_folder sf,
                            TABLE(sf.s9t_file_obj.sheets) ss
                      where sf.file_id  = I_file_id
                        and ss.sheet_name = HTS_TAX_ZONE_sheet)
   select s9t_row(s9t_cells(CORESVC_HTS_DEFINITION.action_mod,
                            hts,
                            import_country_id,
                            effect_from,
                            effect_to,
                            tax_type ,
                            clearing_zone_id,
                            tax_specific_rate,
                            tax_av_rate))
     from hts_tax_zone;
END POPULATE_HTS_TAX_ZONE;
-----------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS
   L_file        S9T_FILE;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(HTS_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_sheet)).column_headers := s9t_cells('ACTION',
                                                                                'HTS',
                                                                                'IMPORT_COUNTRY_ID',
                                                                                'EFFECT_FROM',
                                                                                'EFFECT_TO',
                                                                                'HTS_DESC',
                                                                                'UNITS',
                                                                                'UNITS_1',
                                                                                'UNITS_2',
                                                                                'UNITS_3',
                                                                                'DUTY_COMP_CODE',
                                                                                'MORE_HTS_IND',
                                                                                'QUOTA_CAT',
                                                                                'QUOTA_IND',
                                                                                'AD_IND',
                                                                                'CVD_IND');

   L_file.add_sheet(HTS_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                   'LANG',
                                                                                   'HTS',
                                                                                   'IMPORT_COUNTRY_ID',
                                                                                   'EFFECT_FROM',
                                                                                   'EFFECT_TO',
                                                                                   'HTS_DESC');

   L_file.add_sheet(HTS_AD_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_AD_sheet)).column_headers := s9t_cells('ACTION',
                                                                                   'HTS',  
                                                                                   'IMPORT_COUNTRY_ID', 
                                                                                   'EFFECT_FROM',
                                                                                   'EFFECT_TO',  
                                                                                   'MFG_ID',
                                                                                   'ORIGIN_COUNTRY_ID',
                                                                                   'CASE_NO',
                                                                                   'SHIPPER_ID', 
                                                                                   'SUPPLIER',
                                                                                   'RATE',
                                                                                   'RELATED_CASE_NO',
                                                                                   'EFFECTIVE_ENTRY_DATE',
                                                                                   'EFFECTIVE_EXPORT_DATE');

   L_file.add_sheet(HTS_CVD_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_CVD_sheet)).column_headers := s9t_cells('ACTION',
                                                                                    'HTS',
                                                                                    'IMPORT_COUNTRY_ID',
                                                                                    'EFFECT_FROM',
                                                                                    'EFFECT_TO',
                                                                                    'ORIGIN_COUNTRY_ID',
                                                                                    'CASE_NO',
                                                                                    'MFG_ID',
                                                                                    'SHIPPER_ID',
                                                                                    'SUPPLIER',
                                                                                    'RATE',
                                                                                    'RELATED_CASE_NO',
                                                                                    'EFFECTIVE_ENTRY_DATE',
                                                                                    'EFFECTIVE_EXPORT_DATE');

   L_file.add_sheet(HTS_FEE_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_FEE_sheet)).column_headers := s9t_cells('ACTION',
                                                                                    'HTS',
                                                                                    'IMPORT_COUNTRY_ID',
                                                                                    'EFFECT_FROM',
                                                                                    'EFFECT_TO',
                                                                                    'FEE_TYPE',
                                                                                    'FEE_COMP_CODE',
                                                                                    'FEE_SPECIFIC_RATE',
                                                                                    'FEE_AV_RATE');

   L_file.add_sheet(HTS_FEE_ZONE_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_FEE_ZONE_sheet)).column_headers := s9t_cells('ACTION',
                                                                                         'HTS',
                                                                                         'IMPORT_COUNTRY_ID',
                                                                                         'EFFECT_FROM',
                                                                                         'EFFECT_TO',
                                                                                         'FEE_TYPE',
                                                                                         'CLEARING_ZONE_ID',
                                                                                         'FEE_SPECIFIC_RATE',
                                                                                         'FEE_AV_RATE');

   L_file.add_sheet(HTS_OGA_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_OGA_sheet)).column_headers := s9t_cells('ACTION',
                                                                                    'HTS',
                                                                                    'IMPORT_COUNTRY_ID',
                                                                                    'EFFECT_FROM',
                                                                                    'EFFECT_TO',
                                                                                    'OGA_CODE',
                                                                                    'REFERENCE_ID',
                                                                                    'COMMENTS');
   L_file.add_sheet(HTS_REF_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_REF_sheet)).column_headers := s9t_cells('ACTION',
                                                                                    'HTS',
                                                                                    'IMPORT_COUNTRY_ID',
                                                                                    'EFFECT_FROM',
                                                                                    'EFFECT_TO',
                                                                                    'REFERENCE_ID',
                                                                                    'REFERENCE_DESC');

   L_file.add_sheet(HTT_sheet);
   L_file.sheets(L_file.get_sheet_index(HTT_sheet)).column_headers := s9t_cells('ACTION',
                                                                                'HTS',
                                                                                'IMPORT_COUNTRY_ID',
                                                                                'EFFECT_FROM',
                                                                                'EFFECT_TO',
                                                                                'TARIFF_TREATMENT',
                                                                                'SPECIFIC_RATE',
                                                                                'AV_RATE',
                                                                                'OTHER_RATE');

   L_file.add_sheet(HTS_TT_EXC_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_TT_EXC_sheet)).column_headers := s9t_cells('ACTION',
                                                                                       'HTS',
                                                                                       'IMPORT_COUNTRY_ID',
                                                                                       'EFFECT_FROM',
                                                                                       'EFFECT_TO',
                                                                                       'TARIFF_TREATMENT',
                                                                                       'ORIGIN_COUNTRY_ID');

   L_file.add_sheet(HTS_TT_ZONE_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_TT_ZONE_sheet)).column_headers := s9t_cells('ACTION',
                                                                                        'HTS',
                                                                                        'IMPORT_COUNTRY_ID',
                                                                                        'EFFECT_FROM',
                                                                                        'EFFECT_TO',
                                                                                        'TARIFF_TREATMENT',
                                                                                        'CLEARING_ZONE_ID',
                                                                                        'SPECIFIC_RATE',
                                                                                        'AV_RATE',
                                                                                        'OTHER_RATE');

   L_file.add_sheet(HTS_TAX_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_TAX_sheet)).column_headers := s9t_cells('ACTION',
                                                                                    'HTS',
                                                                                    'IMPORT_COUNTRY_ID',
                                                                                    'EFFECT_FROM',
                                                                                    'EFFECT_TO',
                                                                                    'TAX_TYPE',
                                                                                    'TAX_COMP_CODE',
                                                                                    'TAX_SPECIFIC_RATE',
                                                                                    'TAX_AV_RATE');

   L_file.add_sheet(HTS_TAX_ZONE_sheet);
   L_file.sheets(L_file.get_sheet_index(HTS_TAX_ZONE_sheet)).column_headers := s9t_cells('ACTION',
                                                                                         'HTS',
                                                                                         'IMPORT_COUNTRY_ID',
                                                                                         'EFFECT_FROM',
                                                                                         'EFFECT_TO',
                                                                                         'TAX_TYPE',
                                                                                         'CLEARING_ZONE_ID',
                                                                                         'TAX_SPECIFIC_RATE',
                                                                                         'TAX_AV_RATE');

   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
-----------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.CREATE_S9T';
   L_file      S9T_FILE;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_HTS(O_file_id);
      POPULATE_HTS_TL(O_file_id);
      POPULATE_HTT(O_file_id);
      POPULATE_HTS_TT_EXCLUSIONS(O_file_id);
      POPULATE_HTS_TT_ZONE(O_file_id);
      POPULATE_HTS_FEE(O_file_id);
      POPULATE_HTS_FEE_ZONE(O_file_id);
      POPULATE_HTS_TAX(O_file_id);
      POPULATE_HTS_TAX_ZONE(O_file_id);
      POPULATE_HTS_AD(O_file_id);
      POPULATE_HTS_CVD(O_file_id);
      POPULATE_HTS_REF(O_file_id);
      POPULATE_HTS_OGA(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_HTS.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_COL_TYP is TABLE OF SVC_HTS%ROWTYPE;
   L_temp_rec      SVC_HTS%ROWTYPE;
   SVC_HTS_col     SVC_HTS_COL_TYP         := NEW SVC_HTS_COL_TYP();
   L_process_id    SVC_HTS.PROCESS_ID%TYPE;
   L_error         BOOLEAN                 := FALSE;
   L_default_rec   SVC_HTS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select hts_mi,
             import_country_id_mi,
             effect_from_mi,
             effect_to_mi,
             HTS_DESC_mi,
             units_mi,
             units_1_mi,
             units_2_mi,
             units_3_mi,
             duty_comp_code_mi,
             more_hts_ind_mi,
             quota_cat_mi,
             quota_ind_mi,
             ad_ind_mi,
             cvd_ind_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'HTS'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('HTS'               as hts,
                                            'IMPORT_COUNTRY_ID' as import_country_id,
                                            'EFFECT_FROM'       as effect_from,
                                            'EFFECT_TO'         as effect_to,
                                            'HTS_DESC'          as HTS_DESC,
                                            'UNITS'             as units,
                                            'UNITS_1'           as units_1,
                                            'UNITS_2'           as units_2,
                                            'UNITS_3'           as units_3,
                                            'DUTY_COMP_CODE'    as duty_comp_code,
                                            'MORE_HTS_IND'      as more_hts_ind,
                                            'QUOTA_CAT'         as quota_cat,
                                            'QUOTA_IND'         as quota_ind,
                                            'AD_IND'            as ad_ind,
                                            'CVD_IND'           as cvd_ind,
                                             null               as dummy));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)   := 'SVC_HTS';
   L_pk_columns   VARCHAR2(255)  := 'HTS,Importing Country,Effect From,Effect to';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%type;
BEGIN
   --Get default values.
   FOR rec IN (select  hts_dv,
                       import_country_id_dv,
                       effect_from_dv,
                       effect_to_dv,
                       HTS_DESC_dv,
                       units_dv,
                       units_1_dv,
                       units_2_dv,
                       units_3_dv,
                       duty_comp_code_dv,
                       more_hts_ind_dv,
                       quota_cat_dv,
                       quota_ind_dv,
                       ad_ind_dv,
                       cvd_ind_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                    = template_key
                          and wksht_key                                       = 'HTS'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('HTS'               as hts,
                                                     'IMPORT_COUNTRY_ID' as import_country_id,
                                                     'EFFECT_FROM'       as effect_from,
                                                     'EFFECT_TO'         as effect_to,
                                                     'HTS_DESC'          as HTS_DESC,
                                                     'UNITS'             as units,
                                                     'UNITS_1'           as units_1,
                                                     'UNITS_2'           as units_2,
                                                     'UNITS_3'           as units_3,
                                                     'DUTY_COMP_CODE'    as duty_comp_code,
                                                     'MORE_HTS_IND'      as more_hts_ind,
                                                     'QUOTA_CAT'         as quota_cat,
                                                     'QUOTA_IND'         as quota_ind,
                                                     'AD_IND'            as ad_ind,
                                                     'CVD_IND'           as cvd_ind,
                                                     NULL                as dummy)))
   LOOP
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts_desc := rec.hts_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'HTS_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.units := rec.units_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'UNITS',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.units_1 := rec.units_1_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'UNITS_1',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.units_2 := rec.units_2_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'UNITS_2',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.units_3 := rec.units_3_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'UNITS_3',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.duty_comp_code := rec.duty_comp_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'DUTY_COMP_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.more_hts_ind := rec.more_hts_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'MORE_HTS_IND',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.quota_cat := rec.quota_cat_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS',
                            NULL,
                            'QUOTA_CAT',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.quota_ind := rec.quota_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS ',
                            NULL,
                            'QUOTA_IND',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.ad_ind := rec.ad_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS ',
                            NULL,
                            'AD_IND',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.cvd_ind := rec.cvd_ind_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS ',
                            NULL,
                            'CVD_IND',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(HTS$ACTION))              as action,
                      r.get_cell(HTS$HTS)                        as hts,
                      UPPER(r.get_cell(HTS$IMPORT_COUNTRY_ID))   as import_country_id,
                      r.get_cell(HTS$EFFECT_FROM)                as effect_from,
                      r.get_cell(HTS$EFFECT_TO)                  as effect_to,
                      r.get_cell(HTS$HTS_DESC)                   as hts_desc,
                      r.get_cell(HTS$UNITS)                      as units,
                      UPPER(r.get_cell(HTS$UNITS_1))             as units_1,
                      UPPER(r.get_cell(HTS$UNITS_2))             as units_2,
                      UPPER(r.get_cell(HTS$UNITS_3))             as units_3,
                      UPPER(r.get_cell(HTS$DUTY_COMP_CODE))      as duty_comp_code,
                      r.get_cell(HTS$MORE_HTS_IND)               as more_hts_ind,
                      UPPER(r.get_cell(HTS$QUOTA_CAT))           as quota_cat,
                      r.get_cell(HTS$QUOTA_IND)                  as quota_ind,
                      r.get_cell(HTS$AD_IND)                     as ad_ind,
                      r.get_cell(HTS$CVD_IND)                    as cvd_ind,
                      r.get_row_seq()                            as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTS_sheet,
                             rec.row_seq,
                             'HTS',
                             SQLCODE,
                             SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts_desc := rec.hts_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'HTS_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.units := rec.units;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'UNITS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.units_1 := rec.units_1;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'UNITS_1',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.units_2 := rec.units_2;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'UNITS_2',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.units_3 := rec.units_3;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'UNITS_3',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.duty_comp_code := rec.duty_comp_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'DUTY_COMP_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.more_hts_ind := rec.more_hts_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'MORE_HTS_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.quota_cat := rec.quota_cat;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'QUOTA_CAT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.quota_ind := rec.quota_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'QUOTA_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ad_ind := rec.ad_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'AD_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.cvd_ind := rec.cvd_ind;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            rec.row_seq,
                            'CVD_IND',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
         L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.hts_desc          := NVL(L_temp_rec.hts_desc,L_default_rec.hts_desc);
         L_temp_rec.units             := NVL(L_temp_rec.units,L_default_rec.units);
         L_temp_rec.units_1           := NVL(L_temp_rec.units_1,L_default_rec.units_1);
         L_temp_rec.units_2           := NVL(L_temp_rec.units_2,L_default_rec.units_2);
         L_temp_rec.units_3           := NVL(L_temp_rec.units_3,L_default_rec.units_3);
         L_temp_rec.duty_comp_code    := NVL(L_temp_rec.duty_comp_code,L_default_rec.duty_comp_code);
         L_temp_rec.more_hts_ind      := NVL(L_temp_rec.more_hts_ind,L_default_rec.more_hts_ind);
         L_temp_rec.quota_cat         := NVL(L_temp_rec.quota_cat,L_default_rec.quota_cat);
         L_temp_rec.quota_ind         := NVL(L_temp_rec.quota_ind,L_default_rec.quota_ind);
         L_temp_rec.ad_ind            := NVL(L_temp_rec.ad_ind,L_default_rec.ad_ind);
         L_temp_rec.cvd_ind           := NVL(L_temp_rec.cvd_ind,L_default_rec.cvd_ind);
      end if;

      if L_temp_rec.effect_to is NULL
         or L_temp_rec.effect_from is NULL
         or L_temp_rec.import_country_id is NULL
         or L_temp_rec.hts is NULL then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_HTS_col.extend();
         SVC_HTS_col(SVC_HTS_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts st
         using(select
                     (case
                      when L_mi_rec.hts_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.hts is NULL
                      then mt.hts
                      else s1.hts
                      end) as hts,
                     (case
                      when L_mi_rec.import_country_id_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.import_country_id is NULL
                      then mt.import_country_id
                      else s1.import_country_id
                      end) as import_country_id,
                     (case
                      when L_mi_rec.effect_from_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.effect_from is NULL
                      then mt.effect_from
                      else s1.effect_from
                      end) as effect_from,
                     (case
                      when L_mi_rec.effect_to_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.effect_to is NULL
                      then mt.effect_to
                      else s1.effect_to
                      end) as effect_to,
                     (case
                      when L_mi_rec.HTS_DESC_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.HTS_DESC is NULL
                      then mt.HTS_DESC
                      else s1.HTS_DESC
                      end) as HTS_DESC,
                     (case
                      when L_mi_rec.units_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.units is NULL
                      then mt.units
                      else s1.units
                      end) as units,
                     (case
                      when L_mi_rec.units_1_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.units_1 is NULL
                      then mt.units_1
                      else s1.units_1
                      end) as units_1,
                     (case
                      when L_mi_rec.units_2_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.units_2 is NULL
                      then mt.units_2
                      else s1.units_2
                      end) as units_2,
                     (case
                      when L_mi_rec.units_3_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.units_3 is NULL
                      then mt.units_3
                      else s1.units_3
                      end) as units_3,
                     (case
                      when L_mi_rec.duty_comp_code_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.duty_comp_code is NULL
                      then mt.duty_comp_code
                      else s1.duty_comp_code
                      end) as duty_comp_code,
                     (case
                      when L_mi_rec.more_hts_ind_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.more_hts_ind is NULL
                      then mt.more_hts_ind
                      else s1.more_hts_ind
                      end) as more_hts_ind,
                     (case
                      when L_mi_rec.quota_cat_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.quota_cat is NULL
                      then mt.quota_cat
                      else s1.quota_cat
                      end) as quota_cat,
                     (case
                      when L_mi_rec.quota_ind_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.quota_ind is NULL
                      then mt.quota_ind
                      else s1.quota_ind
                      end) as quota_ind,
                     (case
                      when L_mi_rec.ad_ind_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.ad_ind is NULL
                      then mt.ad_ind
                      else s1.ad_ind
                      end) as ad_ind,
                     (case
                      when L_mi_rec.cvd_ind_mi    = 'N'
                       and SVC_HTS_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.cvd_ind is NULL
                      then mt.cvd_ind
                      else s1.cvd_ind
                      end) as cvd_ind,
                      null as dummy
                from (select SVC_HTS_col(i).hts               as hts,
                             SVC_HTS_col(i).import_country_id as import_country_id,
                             SVC_HTS_col(i).effect_from       as effect_from,
                             SVC_HTS_col(i).effect_to         as effect_to,
                             SVC_HTS_col(i).hts_desc          as hts_desc,
                             SVC_HTS_col(i).units             as units,
                             SVC_HTS_col(i).units_1           as units_1,
                             SVC_HTS_col(i).units_2           as units_2,
                             SVC_HTS_col(i).units_3           as units_3,
                             SVC_HTS_col(i).duty_comp_code    as duty_comp_code,
                             SVC_HTS_col(i).more_hts_ind      as more_hts_ind,
                             SVC_HTS_col(i).quota_cat         as quota_cat,
                             SVC_HTS_col(i).quota_ind         as quota_ind,
                             SVC_HTS_col(i).ad_ind            as ad_ind,
                             SVC_HTS_col(i).cvd_ind           as cvd_ind,
                             null                             as dummy
                        from dual) s1,
                     hts mt
               where mt.effect_to (+)             = s1.effect_to
                 and mt.effect_from (+)           = s1.effect_from
                 and mt.import_country_id (+)     = s1.import_country_id
                 and mt.hts (+)                   = s1.hts
                 and 1 = 1)sq
           on (st.effect_to            = sq.effect_to         and
               st.effect_from          = sq.effect_from       and
               st.import_country_id    = sq.import_country_id and
               st.hts                  = sq.hts               and
               SVC_HTS_col(i).ACTION IN (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
         when matched then
            update
               set process_id        = SVC_HTS_col(i).process_id,
                   chunk_id          = SVC_HTS_col(i).chunk_id,
                   row_seq           = SVC_HTS_col(i).row_seq,
                   action            = SVC_HTS_col(i).action,
                   process$status    = SVC_HTS_col(i).process$status,
                   hts_desc          = sq.hts_desc,
                   quota_cat         = sq.quota_cat,
                   duty_comp_code    = sq.duty_comp_code,
                   units             = sq.units,
                   cvd_ind           = sq.cvd_ind,
                   units_3           = sq.units_3,
                   units_2           = sq.units_2,
                   quota_ind         = sq.quota_ind,
                   more_hts_ind      = sq.more_hts_ind,
                   ad_ind            = sq.ad_ind,
                   units_1           = sq.units_1,
                   create_id         = SVC_HTS_col(i).create_id,
                   create_datetime   = SVC_HTS_col(i).create_datetime,
                   last_upd_id       = SVC_HTS_col(i).last_upd_id,
                   last_upd_datetime = SVC_HTS_col(i).last_upd_datetime
         when NOT matched then
            insert(process_id,
                   chunk_id,
                   row_seq,
                   action,
                   process$status,
                   hts,
                   import_country_id,
                   effect_from,
                   effect_to,
                   hts_desc,
                   units,
                   units_1,
                   units_2,
                   units_3,
                   duty_comp_code,
                   more_hts_ind,
                   quota_cat,
                   quota_ind,
                   ad_ind,
                   cvd_ind,
                   create_id,
                   create_datetime,
                   last_upd_id,
                   last_upd_datetime)
            values(SVC_HTS_col(i).process_id,
                   SVC_HTS_col(i).chunk_id,
                   SVC_HTS_col(i).row_seq,
                   SVC_HTS_col(i).action,
                   SVC_HTS_col(i).process$status,
                   sq.hts,
                   sq.import_country_id,
                   sq.effect_from,
                   sq.effect_to,
                   sq.hts_desc,
                   sq.units,
                   sq.units_1,
                   sq.units_2,
                   sq.units_3,
                   sq.duty_comp_code,
                   sq.more_hts_ind,
                   sq.quota_cat,
                   sq.quota_ind,
                   sq.ad_ind,
                   sq.cvd_ind,
                   SVC_HTS_col(i).create_id,
                   SVC_HTS_col(i).create_datetime,
                   SVC_HTS_col(i).last_upd_id,
                   SVC_HTS_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_sheet,
                            SVC_HTS_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_TL(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_HTS_TL.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_TL_COL_TYP is TABLE OF SVC_HTS_TL%ROWTYPE;
   L_temp_rec       SVC_HTS_TL%ROWTYPE;
   SVC_HTS_TL_col   SVC_HTS_TL_COL_TYP := NEW SVC_HTS_TL_COL_TYP();
   L_process_id     SVC_HTS_TL.PROCESS_ID%TYPE;
   L_error          BOOLEAN := FALSE;
   L_default_rec    SVC_HTS_TL%ROWTYPE;

   cursor C_MANDATORY_IND is
      select hts_desc_mi,
             effect_to_mi,
             effect_from_mi,
             import_country_id_mi,
             hts_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'HTS_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('HTS_DESC'            as hts_desc,
                                       'EFFECT_TO'           as effect_to,
                                       'EFFECT_FROM'         as effect_from,
                                       'IMPORT_COUNTRY_ID'   as import_country_id,
                                       'HTS'                 as hts,
                                       'LANG'                as lang));

   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HTS_TL';
   L_pk_columns   VARCHAR2(255) := 'HTS, Import Country ID, Effect From, Effect To, Lang';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   --Get default values.
   FOR rec IN (select hts_desc_dv,
                      effect_to_dv,
                      effect_from_dv,
                      import_country_id_dv,
                      hts_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'HTS_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('HTS_DESC'            as hts_desc,
                                                'EFFECT_TO'           as effect_to,
                                                'EFFECT_FROM'         as effect_from,
                                                'IMPORT_COUNTRY_ID'   as import_country_id,
                                                'HTS'                 as hts,
                                                'LANG'                as lang)))
   LOOP
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            NULL,
                            'EFFECT_TO_MI',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            NULL,
                            'EFFECT_FROM_MI',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts_desc := rec.hts_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            NULL,
                            'HTS_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            NULL,
                            'LANG' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(HTS_TL$ACTION))              as action,
                      r.get_cell(HTS_TL$LANG)                       as lang,
                      r.get_cell(HTS_TL$HTS)                        as hts,
                      UPPER(r.get_cell(HTS_TL$IMPORT_COUNTRY_ID))   as import_country_id,
                      r.get_cell(HTS_TL$EFFECT_FROM)                as effect_from,
                      r.get_cell(HTS_TL$EFFECT_TO)                  as effect_to,
                      r.get_cell(HTS_TL$HTS_DESC)                   as hts_desc,
                      r.get_row_seq()                               as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_TL_sheet))
   LOOP
      L_temp_rec                := NULL;
      L_temp_rec.process_id     := I_process_id;
      L_temp_rec.chunk_id       := 1;
      L_temp_rec.row_seq        := rec.row_seq;
      L_temp_rec.process$status := 'N';
      L_error                   := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts_desc := rec.hts_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            rec.row_seq,
                            'HTS_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.hts_desc          := NVL(L_temp_rec.hts_desc,L_default_rec.hts_desc);
         L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
         L_temp_rec.lang              := NVL(L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.hts is NOT NULL and L_temp_rec.import_country_id is NOT NULL
         and L_temp_rec.lang is NOT NULL and L_temp_rec.effect_from is NOT NULL and L_temp_rec.effect_to is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_HTS_TL_col.extend();
         SVC_HTS_TL_col(SVC_HTS_TL_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_TL_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts_tl st
         using(select
                     (case
                      when L_mi_rec.hts_desc_mi = 'N'
                       and svc_hts_tl_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.hts_desc is NULL then
                           mt.hts_desc
                      else s1.hts_desc
                      end) as hts_desc,
                     (case
                      when L_mi_rec.import_country_id_mi = 'N'
                       and svc_hts_tl_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.import_country_id is NULL then
                           mt.import_country_id
                      else s1.import_country_id
                      end) as import_country_id,
                     (case
                      when L_mi_rec.effect_from_mi = 'N'
                       and svc_hts_tl_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.effect_from is NULL then
                           mt.effect_from
                      else s1.effect_from
                      end) as effect_from,
                     (case
                      when L_mi_rec.effect_to_mi = 'N'
                       and svc_hts_tl_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.effect_to is NULL then
                           mt.effect_to
                      else s1.effect_to
                      end) as effect_to,
                     (case
                      when L_mi_rec.lang_mi = 'N'
                       and svc_hts_tl_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.lang is NULL then
                           mt.lang
                      else s1.lang
                      end) as lang,
                     (case
                      when L_mi_rec.hts_mi = 'N'
                       and svc_hts_tl_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                       and s1.hts is NULL then
                           mt.hts
                      else s1.hts
                      end) as hts
                 from (select svc_hts_tl_col(i).hts_desc          as hts_desc,
                              svc_hts_tl_col(i).effect_from       as effect_from,
                              svc_hts_tl_col(i).effect_to         as effect_to,
                              svc_hts_tl_col(i).import_country_id as import_country_id,
                              svc_hts_tl_col(i).hts               as hts,
                              svc_hts_tl_col(i).lang              as lang
                         from dual) s1,
                      hts_tl mt
                where mt.import_country_id (+)  = s1.import_country_id
                  and mt.effect_from (+)        = s1.effect_from
                  and mt.effect_to (+)          = s1.effect_to
                  and mt.hts (+)                = s1.hts
                  and mt.lang (+)               = s1.lang) sq
                   on (st.import_country_id   = sq.import_country_id and
                       st.effect_from         = sq.effect_from and
                       st.effect_to           = sq.effect_to and
                       st.hts                 = sq.hts and
                       st.lang                = sq.lang and
                       svc_hts_tl_col(i).action in (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
         when matched then
            update
               set process_id        = svc_hts_tl_col(i).process_id,
                   chunk_id          = svc_hts_tl_col(i).chunk_id,
                   row_seq           = svc_hts_tl_col(i).row_seq,
                   action            = svc_hts_tl_col(i).action,
                   process$status    = svc_hts_tl_col(i).process$status,
                   hts_desc          = sq.hts_desc
         when NOT matched then
            insert(process_id,
                   chunk_id,
                   row_seq,
                   action,
                   process$status,
                   hts_desc,
                   effect_from,
                   effect_to,
                   import_country_id,
                   hts,
                   lang)
            values(svc_hts_tl_col(i).process_id,
                   svc_hts_tl_col(i).chunk_id,
                   svc_hts_tl_col(i).row_seq,
                   svc_hts_tl_col(i).action,
                   svc_hts_tl_col(i).process$status,
                   sq.hts_desc,
                   sq.effect_from,
                   sq.effect_to,
                   sq.import_country_id,
                   sq.hts,
                   sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TL_SHEET,
                            SVC_HTS_TL_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_TL;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_AD(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                             I_process_id   IN   SVC_HTS_AD.PROCESS_ID%TYPE)IS

   TYPE SVC_HTS_AD_COL_TYP is TABLE OF SVC_HTS_AD%ROWTYPE;
   L_temp_rec       SVC_HTS_AD%ROWTYPE;
   SVC_HTS_AD_col   SVC_HTS_AD_COL_TYP := NEW SVC_HTS_AD_COL_TYP();
   L_process_id     SVC_HTS_AD.PROCESS_ID%TYPE;
   L_error          BOOLEAN := FALSE;
   L_default_rec    SVC_HTS_AD%ROWTYPE;

   cursor C_MANDATORY_IND IS
      select MFG_ID_mi,
             EFFECT_TO_mi,
             EFFECT_FROM_mi,
             IMPORT_COUNTRY_ID_mi,
             HTS_mi,
             EFFECTIVE_EXPORT_DATE_mi,
             EFFECTIVE_ENTRY_DATE_mi,
             RELATED_CASE_NO_mi,
             RATE_mi,
             SUPPLIER_mi,
             SHIPPER_ID_mi,
             CASE_NO_mi,
             ORIGIN_COUNTRY_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_HTS_DEFINITION.template_key
                 and wksht_key     = 'HTS_AD') 
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('MFG_ID'                 as MFG_ID,
                                                                'EFFECT_TO'              as EFFECT_TO,
                                                                'EFFECT_FROM'            as EFFECT_FROM,
                                                                'IMPORT_COUNTRY_ID'      as IMPORT_COUNTRY_ID,
                                                                'HTS'                    as HTS,
                                                                'EFFECTIVE_EXPORT_DATE'  as EFFECTIVE_EXPORT_DATE,
                                                                'EFFECTIVE_ENTRY_DATE'   as EFFECTIVE_ENTRY_DATE,
                                                                'RELATED_CASE_NO'        as RELATED_CASE_NO,
                                                                'RATE'                   as RATE,
                                                                'SUPPLIER'               as SUPPLIER,
                                                                'SHIPPER_ID'             as SHIPPER_ID,
                                                                'CASE_NO'                as CASE_NO,
                                                                'ORIGIN_COUNTRY_ID'      as ORIGIN_COUNTRY_ID,
                                                                 NULL                    as dummy));
                                                              
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HTS_AD';
   L_pk_columns   VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect To,Mfg. ID,Country Of Sourcing';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   --Get default values.
   FOR rec IN (select MFG_ID_dv,
                      EFFECT_TO_dv,
                      EFFECT_FROM_dv,
                      IMPORT_COUNTRY_ID_dv,
                      HTS_dv,
                      EFFECTIVE_EXPORT_DATE_dv,
                      EFFECTIVE_ENTRY_DATE_dv,
                      RELATED_CASE_NO_dv,
                      RATE_dv,
                      SUPPLIER_dv,
                      SHIPPER_ID_dv,
                      CASE_NO_dv,
                      ORIGIN_COUNTRY_ID_dv,
                      NULL as dummy
                 from
                    (select column_key,
                              default_value
                       from s9t_tmpl_cols_def
                      where template_key  = CORESVC_HTS_DEFINITION.template_key
                        and wksht_key     = 'HTS_AD') 
                      PIVOT (MAX(default_value) as dv FOR (column_key) IN ('MFG_ID'               as MFG_ID,
                                                                           'EFFECT_TO'            as EFFECT_TO,
                                                                           'EFFECT_FROM'          as EFFECT_FROM,
                                                                           'IMPORT_COUNTRY_ID'    as IMPORT_COUNTRY_ID,
                                                                           'HTS'                  as HTS,
                                                                           'EFFECTIVE_EXPORT_DATE'as EFFECTIVE_EXPORT_DATE,
                                                                           'EFFECTIVE_ENTRY_DATE' as EFFECTIVE_ENTRY_DATE,
                                                                           'RELATED_CASE_NO'      as RELATED_CASE_NO,
                                                                           'RATE'                 as RATE,
                                                                           'SUPPLIER'             as SUPPLIER,
                                                                           'SHIPPER_ID'           as SHIPPER_ID,
                                                                           'CASE_NO'              as CASE_NO,
                                                                           'ORIGIN_COUNTRY_ID'    as ORIGIN_COUNTRY_ID,
                                                                            NULL                  as dummy)))
   LOOP
      BEGIN
         L_default_rec.mfg_id := rec.mfg_id_dv;
      EXCEPTION  
         when OTHERS then  
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'MFG_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effective_export_date := rec.effective_export_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'EFFECTIVE_EXPORT_DATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effective_entry_date := rec.effective_entry_date_dv;
      EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'HTS_AD',
                         NULL,
                         'EFFECTIVE_ENTRY_DATE',
                         NULL,
                         'INV_DEFAULT');  
      END;
      BEGIN
         L_default_rec.related_case_no := rec.related_case_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'RELATED_CASE_NO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.rate := rec.rate_dv;
      EXCEPTION
         when OTHERS then  
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.supplier := rec.supplier_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'SUPPLIER',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.shipper_id := rec.shipper_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'SHIPPER_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.case_no := rec.case_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'CASE_NO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.origin_country_id := rec.origin_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_AD',
                            NULL,
                            'ORIGIN_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.GET_CELL(HTS_AD$ACTION))            as ACTION,
                      r.GET_CELL(HTS_AD$MFG_ID)                   as MFG_ID,
                      r.GET_CELL(HTS_AD$EFFECT_TO)                as EFFECT_TO,
                      r.GET_CELL(HTS_AD$EFFECT_FROM)              as EFFECT_FROM,
                      UPPER(r.GET_CELL(HTS_AD$IMPORT_COUNTRY_ID)) as IMPORT_COUNTRY_ID,
                      r.GET_CELL(HTS_AD$HTS)                      as HTS,
                      r.GET_CELL(HTS_AD$EFFECTIVE_EXPORT_DATE)    as EFFECTIVE_EXPORT_DATE,
                      r.GET_CELL(HTS_AD$EFFECTIVE_ENTRY_DATE)     as EFFECTIVE_ENTRY_DATE,
                      r.GET_CELL(HTS_AD$RELATED_CASE_NO)          as RELATED_CASE_NO,
                      r.GET_CELL(HTS_AD$RATE)                     as RATE,
                      r.GET_CELL(HTS_AD$SUPPLIER)                 as SUPPLIER,
                      r.GET_CELL(HTS_AD$SHIPPER_ID)               as SHIPPER_ID,
                      r.GET_CELL(HTS_AD$CASE_NO)                  as CASE_NO,
                      UPPER(r.GET_CELL(HTS_AD$ORIGIN_COUNTRY_ID)) as ORIGIN_COUNTRY_ID,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = SHEET_NAME_TRANS(HTS_AD_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.mfg_id := rec.mfg_id;
      EXCEPTION
         when OTHERS then  
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'MFG_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
        L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effective_export_date := rec.effective_export_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'EFFECTIVE_EXPORT_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effective_entry_date := rec.effective_entry_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'EFFECTIVE_ENTRY_DATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.related_case_no := rec.related_case_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'RELATED_CASE_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.rate := rec.rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supplier := rec.supplier;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'SUPPLIER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.shipper_id := rec.shipper_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'SHIPPER_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.case_no := rec.case_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'CASE_NO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.origin_country_id := rec.origin_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            rec.row_seq,
                            'ORIGIN_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.mfg_id                := NVL(L_temp_rec.mfg_id,L_default_rec.mfg_id);
         L_temp_rec.effect_to             := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.effect_from           := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.import_country_id     := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts                   := NVL(L_temp_rec.hts,L_default_rec.hts);
         L_temp_rec.effective_export_date := NVL(L_temp_rec.effective_export_date,L_default_rec.effective_export_date);
         L_temp_rec.effective_entry_date  := NVL(L_temp_rec.effective_entry_date,L_default_rec.effective_entry_date);
         L_temp_rec.related_case_no       := NVL(L_temp_rec.related_case_no,L_default_rec.related_case_no);
         L_temp_rec.rate                  := NVL(L_temp_rec.rate,L_default_rec.rate);
         L_temp_rec.supplier              := NVL(L_temp_rec.supplier,L_default_rec.supplier);
         L_temp_rec.shipper_id            := NVL(L_temp_rec.shipper_id,L_default_rec.shipper_id);
         L_temp_rec.case_no               := NVL(L_temp_rec.case_no,L_default_rec.case_no);
         L_temp_rec.origin_country_id     := NVL(L_temp_rec.origin_country_id,L_default_rec.origin_country_id);
      end if;
      if NOT (L_temp_rec.mfg_id            is NOT NULL and
              L_temp_rec.effect_to         is NOT NULL and
              L_temp_rec.effect_from       is NOT NULL and
              L_temp_rec.import_country_id is NOT NULL and
              L_temp_rec.hts               is NOT NULL and
              L_temp_rec.origin_country_id is NOT NULL and
              1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_AD_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_HTS_AD_col.EXTEND();
         SVC_HTS_AD_col(SVC_HTS_AD_col.count()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_AD_col.count SAVE EXCEPTIONS
         merge into svc_hts_ad st
         using (select (case
                           when L_mi_rec.MFG_ID_mi       = 'N' and
                                SVC_HTS_AD_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.MFG_ID is NULL        then
                                mt.MFG_ID
                           else s1.MFG_ID
                           end) as MFG_ID,
                       (case
                           when L_mi_rec.EFFECT_TO_mi    = 'N' and
                                SVC_HTS_AD_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_TO is NULL     then
                                mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi  = 'N' and
                                SVC_HTS_AD_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_FROM is NULL   then
                                mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi = 'N' and
                                SVC_HTS_AD_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.IMPORT_COUNTRY_ID is NULL  then
                                mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.HTS_mi          = 'N' and
                                SVC_HTS_AD_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.HTS is NULL           then
                                mt.HTS
                           else s1.HTS
                           end) as HTS,
                       (case
                           when L_mi_rec.EFFECTIVE_EXPORT_DATE_mi = 'N' and
                                SVC_HTS_AD_col(i).action          = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECTIVE_EXPORT_DATE is NULL then
                                mt.EFFECTIVE_EXPORT_DATE
                           else s1.EFFECTIVE_EXPORT_DATE
                           end) as EFFECTIVE_EXPORT_DATE,
                       (case
                           when L_mi_rec.EFFECTIVE_ENTRY_DATE_mi  = 'N' and
                                SVC_HTS_AD_col(i).action          = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECTIVE_ENTRY_DATE is NULL  then
                                mt.EFFECTIVE_ENTRY_DATE
                           else s1.EFFECTIVE_ENTRY_DATE
                           end) as EFFECTIVE_ENTRY_DATE,
                       (case
                           when L_mi_rec.RELATED_CASE_NO_mi = 'N' and
                                SVC_HTS_AD_col(i).action    = CORESVC_HTS_DEFINITION.action_mod and
                                s1.RELATED_CASE_NO is NULL  then
                                mt.RELATED_CASE_NO
                           else s1.RELATED_CASE_NO
                           end) as RELATED_CASE_NO,
                       (case
                           when L_mi_rec.RATE_mi         = 'N' and
                                SVC_HTS_AD_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.RATE is NULL          then
                                mt.RATE
                           else s1.RATE
                           end) as RATE,
                       (case
                           when L_mi_rec.SUPPLIER_mi     = 'N' and
                                SVC_HTS_AD_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.SUPPLIER is NULL      then
                                mt.SUPPLIER
                           else s1.SUPPLIER
                           end) as SUPPLIER,
                       (case
                           when L_mi_rec.SHIPPER_ID_mi   = 'N' and
                                SVC_HTS_AD_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.SHIPPER_ID is NULL    then
                                mt.SHIPPER_ID
                           else s1.SHIPPER_ID
                           end) as SHIPPER_ID,
                       (case
                           when L_mi_rec.CASE_NO_mi      = 'N' and 
                                SVC_HTS_AD_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.CASE_NO is NULL       then
                                mt.CASE_NO
                           else s1.CASE_NO
                           end) as CASE_NO,
                       (case
                           when L_mi_rec.ORIGIN_COUNTRY_ID_mi = 'N' and 
                                SVC_HTS_AD_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.ORIGIN_COUNTRY_ID is NULL  then 
                                mt.ORIGIN_COUNTRY_ID
                           else s1.ORIGIN_COUNTRY_ID
                           end) as ORIGIN_COUNTRY_ID,
                           null as dummy
                  from (select SVC_HTS_AD_col(i).MFG_ID                as MFG_ID,
                               SVC_HTS_AD_col(i).EFFECT_TO             as EFFECT_TO,
                               SVC_HTS_AD_col(i).EFFECT_FROM           as EFFECT_FROM,
                               SVC_HTS_AD_col(i).IMPORT_COUNTRY_ID     as IMPORT_COUNTRY_ID,
                               SVC_HTS_AD_col(i).HTS                   as HTS,
                               SVC_HTS_AD_col(i).EFFECTIVE_EXPORT_DATE as EFFECTIVE_EXPORT_DATE,
                               SVC_HTS_AD_col(i).EFFECTIVE_ENTRY_DATE  as EFFECTIVE_ENTRY_DATE,
                               SVC_HTS_AD_col(i).RELATED_CASE_NO       as RELATED_CASE_NO,
                               SVC_HTS_AD_col(i).RATE                  as RATE,
                               SVC_HTS_AD_col(i).SUPPLIER              as SUPPLIER,
                               SVC_HTS_AD_col(i).SHIPPER_ID            as SHIPPER_ID,
                               SVC_HTS_AD_col(i).CASE_NO               as CASE_NO,
                               SVC_HTS_AD_col(i).ORIGIN_COUNTRY_ID     as ORIGIN_COUNTRY_ID,
                               null                                    as dummy
                          from dual)   s1,
                       hts_ad mt
                 where mt.MFG_ID (+)            = s1.MFG_ID
                   and mt.EFFECT_TO (+)         = s1.EFFECT_TO
                   and mt.EFFECT_FROM (+)       = s1.EFFECT_FROM
                   and mt.IMPORT_COUNTRY_ID (+) = s1.IMPORT_COUNTRY_ID
                   and mt.HTS (+)               = s1.HTS
                   and mt.ORIGIN_COUNTRY_ID (+) = s1.ORIGIN_COUNTRY_ID
                   and 1 = 1) sq 
            on (    st.MFG_ID            = sq.MFG_ID
                and st.EFFECT_TO         = sq.EFFECT_TO
                and st.EFFECT_FROM       = sq.EFFECT_FROM
                and st.IMPORT_COUNTRY_ID = sq.IMPORT_COUNTRY_ID
                and st.HTS               = sq.HTS
                and st.ORIGIN_COUNTRY_ID = sq.ORIGIN_COUNTRY_ID
                and SVC_HTS_AD_col(i).action in (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
               update
                  set process_id             = SVC_HTS_AD_col(i).process_id,
                      chunk_id               = SVC_HTS_AD_col(i).chunk_id,
                      row_seq                = SVC_HTS_AD_col(i).row_seq,
                      action                 = SVC_HTS_AD_col(i).action,
                      process$status         = SVC_HTS_AD_col(i).process$status,
                      case_no                = sq.case_no,
                      effective_export_date  = sq.effective_export_date,
                      supplier               = sq.supplier,
                      effective_entry_date   = sq.effective_entry_date,
                      related_case_no        = sq.related_case_no,
                      rate                   = sq.rate,
                      shipper_id             = sq.shipper_id,
                      create_id              = SVC_HTS_AD_col(i).create_id,
                      create_datetime        = SVC_HTS_AD_col(i).create_datetime,
                      last_upd_id            = SVC_HTS_AD_col(i).last_upd_id,
                      last_upd_datetime      = SVC_HTS_AD_col(i).last_upd_datetime
            when NOT matched then
               insert (process_id,
                       chunk_id,
                       row_seq,
                       action,
                       process$status,
                       mfg_id,
                       effect_to,
                       effect_from,
                       import_country_id,
                       hts,
                       effective_export_date,
                       effective_entry_date,
                       related_case_no,
                       rate,
                       supplier,
                       shipper_id,
                       case_no,
                       origin_country_id,
                       create_id,
                       create_datetime ,
                       last_upd_id ,
                       last_upd_datetime)
               values (SVC_HTS_AD_col(i).process_id,
                       SVC_HTS_AD_col(i).chunk_id,
                       SVC_HTS_AD_col(i).row_seq,
                       SVC_HTS_AD_col(i).action,
                       SVC_HTS_AD_col(i).process$status,
                       sq.mfg_id,
                       sq.effect_to,
                       sq.effect_from,
                       sq.import_country_id,
                       sq.hts,
                       sq.effective_export_date,
                       sq.effective_entry_date,
                       sq.related_case_no,
                       sq.rate,
                       sq.supplier,
                       sq.shipper_id,
                       sq.case_no,
                       sq.origin_country_id,
                       SVC_HTS_AD_col(i).create_id,
                       SVC_HTS_AD_col(i).create_datetime,
                       SVC_HTS_AD_col(i).last_upd_id,
                       SVC_HTS_AD_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_AD_sheet,
                            SVC_HTS_AD_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_AD;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_CVD(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id   IN   SVC_HTS_CVD.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_CVD_COL_TYPE is TABLE OF SVC_HTS_CVD%ROWTYPE;
   L_temp_rec        SVC_HTS_CVD%ROWTYPE;
   SVC_HTS_CVD_col   SVC_HTS_CVD_COL_TYPE := NEW SVC_HTS_CVD_COL_TYPE();
   L_process_id      SVC_HTS_CVD.PROCESS_ID%TYPE;
   L_error           BOOLEAN := FALSE;
   L_default_rec     SVC_HTS_CVD%ROWTYPE;

   cursor C_MANDATORY_IND IS
      select effective_export_date_mi,
             effective_entry_date_mi,
             related_case_no_mi,
             rate_mi,
             supplier_mi,
             shipper_id_mi,
             mfg_id_mi,
             case_no_mi,
             origin_country_id_mi,
             effect_to_mi,
             effect_from_mi,
             import_country_id_mi,
             hts_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_HTS_DEFINITION.template_key
                 and wksht_key     = 'HTS_CVD') 
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('EFFECTIVE_EXPORT_DATE'     as EFFECTIVE_EXPORT_DATE,
                                                                'EFFECTIVE_ENTRY_DATE'      as EFFECTIVE_ENTRY_DATE,
                                                                'RELATED_CASE_NO'           as RELATED_CASE_NO,
                                                                'RATE'                      as RATE,
                                                                'SUPPLIER'                  as SUPPLIER,
                                                                'SHIPPER_ID'                as SHIPPER_ID,
                                                                'MFG_ID'                    as MFG_ID,
                                                                'CASE_NO'                   as CASE_NO,
                                                                'ORIGIN_COUNTRY_ID'         as ORIGIN_COUNTRY_ID,
                                                                'EFFECT_TO'                 as EFFECT_TO,
                                                                'EFFECT_FROM'               as EFFECT_FROM,
                                                                'IMPORT_COUNTRY_ID'         as IMPORT_COUNTRY_ID,
                                                                'HTS'                       as HTS,
                                                                 NULL                       as dummy));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HTS_CVD';
   L_pk_columns   VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect To,Country Of Sourcing';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select EFFECTIVE_EXPORT_DATE_DV,
                      EFFECTIVE_ENTRY_DATE_DV,
                      RELATED_CASE_NO_DV,
                      RATE_DV,
                      SUPPLIER_DV,
                      SHIPPER_ID_DV,
                      MFG_ID_DV,
                      CASE_NO_DV,
                      ORIGIN_COUNTRY_ID_DV,
                      EFFECT_TO_DV,
                      EFFECT_FROM_DV,
                      IMPORT_COUNTRY_ID_DV,
                      HTS_DV,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key  = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key     = 'HTS_CVD')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('EFFECTIVE_EXPORT_DATE'  as EFFECTIVE_EXPORT_DATE,
                                                                             'EFFECTIVE_ENTRY_DATE'   as EFFECTIVE_ENTRY_DATE,
                                                                             'RELATED_CASE_NO'        as RELATED_CASE_NO,
                                                                             'RATE'                   as RATE,
                                                                             'SUPPLIER'               as SUPPLIER,
                                                                             'SHIPPER_ID'             as SHIPPER_ID,
                                                                             'MFG_ID'                 as MFG_ID,
                                                                             'CASE_NO'                as CASE_NO,
                                                                             'ORIGIN_COUNTRY_ID'      as ORIGIN_COUNTRY_ID,
                                                                             'EFFECT_TO'              as EFFECT_TO,
                                                                             'EFFECT_FROM'            as EFFECT_FROM,
                                                                             'IMPORT_COUNTRY_ID'      as IMPORT_COUNTRY_ID,
                                                                             'HTS'                    as HTS,
                                                                              NULL                    as dummy)))
   LOOP
      BEGIN
         L_default_rec.effective_export_date := rec.effective_export_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'EFFECTIVE_EXPORT_DATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effective_entry_date := rec.effective_entry_date_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'EFFECTIVE_ENTRY_DATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.related_case_no := rec.related_case_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'RELATED_case_NO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.rate := rec.rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.supplier := rec.supplier_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'SUPPLIER',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.shipper_id := rec.shipper_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'SHIPPER_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN  
         L_default_rec.mfg_id := rec.mfg_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'MFG_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.case_no := rec.case_no_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'CASE_NO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN  
         L_default_rec.origin_country_id := rec.origin_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'ORIGIN_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_CVD',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN  (select UPPER(r.GET_CELL(HTS_CVD$ACTION))                  as ACTION,
                       r.GET_CELL(HTS_CVD$EFFECTIVE_EXPORT_DATE)          as EFFECTIVE_EXPORT_DATE,
                       r.GET_CELL(HTS_CVD$EFFECTIVE_ENTRY_DATE)           as EFFECTIVE_ENTRY_DATE,
                       r.GET_CELL(HTS_CVD$RELATED_CASE_NO)                as RELATED_CASE_NO,
                       r.GET_CELL(HTS_CVD$RATE)                           as RATE,
                       r.GET_CELL(HTS_CVD$SUPPLIER)                       as SUPPLIER,
                       r.GET_CELL(HTS_CVD$SHIPPER_ID)                     as SHIPPER_ID,
                       r.GET_CELL(HTS_CVD$MFG_ID)                         as MFG_ID,
                       r.GET_CELL(HTS_CVD$CASE_NO)                        as CASE_NO,
                       UPPER(r.GET_CELL(HTS_CVD$ORIGIN_COUNTRY_ID))       as ORIGIN_COUNTRY_ID,
                       r.GET_CELL(HTS_CVD$EFFECT_TO)                      as EFFECT_TO,
                       r.GET_CELL(HTS_CVD$EFFECT_FROM)                    as EFFECT_FROM,
                       UPPER(r.GET_CELL(HTS_CVD$IMPORT_COUNTRY_ID))       as IMPORT_COUNTRY_ID,
                       r.GET_CELL(HTS_CVD$HTS)                            as HTS,
                       r.get_row_seq()                                    as row_seq
                  from s9t_folder sf,
                       TABLE(sf.s9t_file_obj.sheets) ss,
                       TABLE(ss.s9t_rows) r
                 where sf.file_id  = I_file_id
                   and ss.sheet_name = sheet_name_trans(HTS_CVD_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effective_export_date := rec.effective_export_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'EFFECTIVE_EXPORT_DATE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effective_entry_date := rec.effective_entry_date;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'EFFECTIVE_ENTRY_DATE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.related_case_no := rec.related_case_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'RELATED_CASE_NO',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.rate := rec.rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'RATE',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supplier := rec.supplier;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'SUPPLIER',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.shipper_id := rec.shipper_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'SHIPPER_ID',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.mfg_id := rec.mfg_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'MFG_ID',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.case_no := rec.case_no;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'CASE_NO',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.origin_country_id := UPPER(rec.origin_country_id);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'ORIGIN_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := UPPER(rec.import_country_id);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
         L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.effective_export_date := NVL(L_temp_rec.effective_export_date,L_default_rec.effective_export_date);
         L_temp_rec.effective_entry_date  := NVL(L_temp_rec.effective_entry_date,L_default_rec.effective_entry_date);
         L_temp_rec.related_case_no       := NVL(L_temp_rec.related_case_no,L_default_rec.related_case_no);
         L_temp_rec.rate                  := NVL(L_temp_rec.rate,L_default_rec.rate);
         L_temp_rec.supplier              := NVL(L_temp_rec.supplier,L_default_rec.supplier);
         L_temp_rec.shipper_id            := NVL(L_temp_rec.shipper_id,L_default_rec.shipper_id);
         L_temp_rec.mfg_id                := NVL(L_temp_rec.mfg_id,L_default_rec.mfg_id);
         L_temp_rec.case_no               := NVL(L_temp_rec.case_no,L_default_rec.case_no);
         L_temp_rec.origin_country_id     := NVL(L_temp_rec.origin_country_id,L_default_rec.origin_country_id);
         L_temp_rec.effect_to             := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.effect_from           := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.import_country_id     := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts                   := NVL(L_temp_rec.hts,L_default_rec.hts);
      end if;
      if NOT (L_temp_rec.origin_country_id is NOT NULL and
              L_temp_rec.effect_to         is NOT NULL and
              L_temp_rec.effect_from       is NOT NULL and
              L_temp_rec.import_country_id is NOT NULL and
              L_temp_rec.hts               is NOT NULL and
              1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_CVD_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_HTS_CVD_col.extend();
         SVC_HTS_CVD_col(SVC_HTS_CVD_col.count()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_CVD_col.count SAVE EXCEPTIONS 
         merge into svc_hts_cvd st
         using (select (case
                           when L_mi_rec.EFFECTIVE_EXPORT_DATE_mi = 'N'
                            and  SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and  s1.EFFECTIVE_EXPORT_DATE is NULL
                            then mt.EFFECTIVE_EXPORT_DATE
                            else s1.EFFECTIVE_EXPORT_DATE
                            end) as EFFECTIVE_EXPORT_DATE,
                       (case
                           when L_mi_rec.EFFECTIVE_ENTRY_DATE_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.EFFECTIVE_ENTRY_DATE   is NULL
                            then mt.EFFECTIVE_ENTRY_DATE
                            else s1.EFFECTIVE_ENTRY_DATE
                            end) as EFFECTIVE_ENTRY_DATE,
                       (case
                           when L_mi_rec.RELATED_case_NO_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.RELATED_case_NO        is NULL
                            then mt.RELATED_case_NO
                            else s1.RELATED_case_NO
                            end) as RELATED_case_NO,
                       (case
                           when L_mi_rec.RATE_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.RATE                   is NULL
                           then mt.RATE
                           else s1.RATE
                           end) as RATE,
                       (case
                           when L_mi_rec.SUPPLIER_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.SUPPLIER               is NULL
                           then mt.SUPPLIER
                           else s1.SUPPLIER
                           end) as SUPPLIER,
                       (case
                           when L_mi_rec.SHIPPER_ID_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.SHIPPER_ID             is NULL
                           then mt.SHIPPER_ID
                           else s1.SHIPPER_ID
                           end) as SHIPPER_ID,
                       (case
                           when L_mi_rec.MFG_ID_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.MFG_ID                 is NULL
                           then mt.MFG_ID
                           else s1.MFG_ID
                           end) as MFG_ID,
                       (case
                           when L_mi_rec.case_NO_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.case_NO                is NULL
                           then mt.case_NO
                           else s1.case_NO
                           end) as case_NO,
                       (case
                           when L_mi_rec.ORIGIN_COUNTRY_ID_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.ORIGIN_COUNTRY_ID      is NULL
                           then mt.ORIGIN_COUNTRY_ID
                           else s1.ORIGIN_COUNTRY_ID
                           end) as ORIGIN_COUNTRY_ID,
                       (case
                           when L_mi_rec.EFFECT_TO_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.EFFECT_TO              is NULL
                           then mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.EFFECT_FROM            is NULL
                           then mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.IMPORT_COUNTRY_ID      is NULL
                           then mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.HTS_mi = 'N'
                            and SVC_HTS_CVD_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.HTS                    is NULL
                           then mt.HTS
                           else s1.HTS
                           end) as HTS,
                           null as dummy
                  from (select SVC_HTS_CVD_col(i).EFFECTIVE_EXPORT_DATE  as EFFECTIVE_EXPORT_DATE,
                               SVC_HTS_CVD_col(i).EFFECTIVE_ENTRY_DATE   as EFFECTIVE_ENTRY_DATE,
                               SVC_HTS_CVD_col(i).RELATED_case_NO        as RELATED_case_NO,
                               SVC_HTS_CVD_col(i).RATE                   as RATE,
                               SVC_HTS_CVD_col(i).SUPPLIER               as SUPPLIER,
                               SVC_HTS_CVD_col(i).SHIPPER_ID             as SHIPPER_ID,
                               SVC_HTS_CVD_col(i).MFG_ID                 as MFG_ID,
                               SVC_HTS_CVD_col(i).CASE_NO                as CASE_NO,
                               SVC_HTS_CVD_col(i).ORIGIN_COUNTRY_ID      as ORIGIN_COUNTRY_ID,
                               SVC_HTS_CVD_col(i).EFFECT_TO              as EFFECT_TO,
                               SVC_HTS_CVD_col(i).EFFECT_FROM            as EFFECT_FROM,
                               SVC_HTS_CVD_col(i).IMPORT_COUNTRY_ID      as IMPORT_COUNTRY_ID,
                               SVC_HTS_CVD_col(i).HTS                    as HTS,
                               null                                      as dummy
                          from dual)    s1,
                          hts_cvd  mt
                   where mt.ORIGIN_COUNTRY_ID (+)   = s1.ORIGIN_COUNTRY_ID
                     and mt.EFFECT_TO (+)           = s1.EFFECT_TO
                     and mt.EFFECT_FROM (+)         = s1.EFFECT_FROM
                     and mt.IMPORT_COUNTRY_ID (+)   = s1.IMPORT_COUNTRY_ID
                     and mt.HTS (+)                 = s1.HTS  and 1 = 1)  sq
            on (st.ORIGIN_COUNTRY_ID = sq.ORIGIN_COUNTRY_ID
            and st.EFFECT_TO         = sq.EFFECT_TO
            and st.EFFECT_FROM       = sq.EFFECT_FROM
            and st.IMPORT_COUNTRY_ID = sq.IMPORT_COUNTRY_ID
            and st.HTS               = sq.HTS
            and SVC_HTS_CVD_col(i).action IN (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
               update
                  set process_id              = SVC_HTS_CVD_col(i).process_id,
                      chunk_id                = SVC_HTS_CVD_col(i).chunk_id,
                      row_seq                 = SVC_HTS_CVD_col(i).row_seq,
                      action                  = SVC_HTS_CVD_col(i).action,
                      process$status          = SVC_HTS_CVD_col(i).process$status,
                      case_no                 = sq.case_no,
                      effective_export_date   = sq.effective_export_date,
                      supplier                = sq.supplier,
                      effective_entry_date    = sq.effective_entry_date,
                      related_case_no         = sq.related_case_no,
                      rate                    = sq.rate,
                      mfg_id                  = sq.mfg_id,
                      shipper_id              = sq.shipper_id,
                      create_id               = SVC_HTS_CVD_col(i).create_id,
                      create_datetime         = SVC_HTS_CVD_col(i).create_datetime,
                      last_upd_id             = SVC_HTS_CVD_col(i).last_upd_id,
                      last_upd_datetime       = SVC_HTS_CVD_col(i).last_upd_datetime 
            when NOT matched then
               insert (process_id,
                       chunk_id,
                       row_seq,
                       action,
                       process$status,
                       effective_export_date,
                       effective_entry_date,
                       related_case_no,
                       rate,
                       supplier,
                       shipper_id,
                       mfg_id,
                       case_no,
                       origin_country_id,
                       effect_to,
                       effect_from,
                       import_country_id,
                       hts,
                       create_id,
                       create_datetime,
                       last_upd_id,
                       last_upd_datetime)
               values (SVC_HTS_CVD_col(i).process_id,
                       SVC_HTS_CVD_col(i).chunk_id,
                       SVC_HTS_CVD_col(i).row_seq,
                       SVC_HTS_CVD_col(i).action,
                       SVC_HTS_CVD_col(i).process$status,
                       sq.effective_export_date,
                       sq.effective_entry_date,
                       sq.related_case_no,
                       sq.rate,
                       sq.supplier,
                       sq.shipper_id,
                       sq.mfg_id,
                       sq.case_no,
                       sq.origin_country_id,
                       sq.effect_to,
                       sq.effect_from,
                       sq.import_country_id,
                       sq.hts,
                       SVC_HTS_CVD_col(i).create_id,
                       SVC_HTS_CVD_col(i).create_datetime,
                       SVC_HTS_CVD_col(i).last_upd_id,
                       SVC_HTS_CVD_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_CVD_sheet,
                            SVC_HTS_CVD_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_CVD;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_FEE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id   IN   SVC_HTS_FEE.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_FEE_COL_TYP is TABLE OF SVC_HTS_FEE%ROWTYPE;
   L_temp_rec        SVC_HTS_FEE%ROWTYPE;
   SVC_HTS_FEE_col   SVC_HTS_FEE_COL_TYP := NEW SVC_HTS_FEE_COL_TYP();
   L_process_id      SVC_HTS_FEE.PROCESS_ID%TYPE;
   L_error           BOOLEAN := FALSE;
   L_default_rec     SVC_HTS_FEE%ROWTYPE;

   cursor C_MANDATORY_IND is
      select FEE_AV_RATE_mi,
             FEE_SPECIFIC_RATE_mi,
             FEE_COMP_CODE_mi,
             FEE_TYPE_mi,
             EFFECT_TO_mi,
             EFFECT_FROM_mi,
             IMPORT_COUNTRY_ID_mi,
             HTS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
       where template_key  = CORESVC_HTS_DEFINITION.template_key
         and wksht_key = 'HTS_FEE')
       PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('FEE_AV_RATE'       as FEE_AV_RATE,
                                                        'FEE_SPECIFIC_RATE' as FEE_SPECIFIC_RATE,
                                                        'FEE_COMP_CODE'     as FEE_COMP_CODE,
                                                        'FEE_TYPE'          as FEE_TYPE,	
                                                        'EFFECT_TO'         as EFFECT_TO,
                                                        'EFFECT_FROM'       as EFFECT_FROM,
                                                        'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                        'HTS'               as HTS,
                                                         NULL               as dummy));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HST_FEE';
   L_pk_columns   VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect To,Fee Type';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select FEE_AV_RATE_dv,
                      FEE_SPECIFIC_RATE_dv,
                      FEE_COMP_CODE_dv,
                      FEE_TYPE_dv,
                      EFFECT_TO_dv,
                      EFFECT_FROM_dv,
                      IMPORT_COUNTRY_ID_dv,
                      HTS_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key = 'HTS_FEE')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('FEE_AV_RATE'       as FEE_AV_RATE,
                                                                             'FEE_SPECIFIC_RATE' as FEE_SPECIFIC_RATE,
                                                                             'FEE_COMP_CODE'     as FEE_COMP_CODE,
                                                                             'FEE_TYPE'          as FEE_TYPE,
                                                                             'EFFECT_TO'         as EFFECT_TO,
                                                                             'EFFECT_FROM'       as EFFECT_FROM,
                                                                             'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                             'HTS'               as HTS,
                                                                              NULL               as dummy)))
   LOOP
      BEGIN
         L_default_rec.fee_av_rate := rec.fee_av_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE',
                            NULL,
                            'FEE_AV_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.fee_specific_rate := rec.fee_specific_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE',
                            NULL,
                            'FEE_SPECIFIC_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.fee_comp_code := rec.fee_comp_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE',
                            NULL,
                            'FEE_COMP_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.fee_type := rec.fee_type_dv;
      EXCEPTION
         when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'HTS_FEE',
                         NULL,
                         'FEE_TYPE',
                         NULL,
                         'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_mandatory_ind;
   fetch C_mandatory_ind into L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN (select r.get_cell(HTS_FEE$Action)                   as ACTION,
                      r.get_cell(HTS_FEE$FEE_AV_RATE)              as FEE_AV_RATE,
                      r.get_cell(HTS_FEE$FEE_SPECIFIC_RATE)        as FEE_SPECIFIC_RATE,
                      upper(r.get_cell(HTS_FEE$FEE_COMP_CODE))     as FEE_COMP_CODE,
                      upper(r.get_cell(HTS_FEE$FEE_TYPE))          as FEE_TYPE,
                      r.get_cell(HTS_FEE$EFFECT_TO)                as EFFECT_TO,
                      r.get_cell(HTS_FEE$EFFECT_FROM)              as EFFECT_FROM,
                      upper(r.get_cell(HTS_FEE$IMPORT_COUNTRY_ID)) as IMPORT_COUNTRY_ID,
                      r.get_cell(HTS_FEE$HTS)                      as HTS,
                      r.get_row_seq()                              as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_FEE_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            if C_MANDATORY_IND%ISOPEN then
               close c_mandatory_ind;
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fee_av_rate := rec.fee_av_rate;
      EXCEPTION
         when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_FEE_sheet,
                         rec.row_seq,
                         'FEE_AV_RATE',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fee_specific_rate := rec.fee_specific_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            rec.row_seq,
                            'FEE_SPECIFIC_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fee_comp_code := rec.fee_comp_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            rec.row_seq,
                            'FEE_COMP_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fee_type := rec.fee_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            rec.row_seq,
                            'FEE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.fee_av_rate        := NVL(L_temp_rec.fee_av_rate,L_default_rec.fee_av_rate);
         L_temp_rec.fee_specific_rate  := NVL(L_temp_rec.fee_specific_rate,L_default_rec.fee_specific_rate);
         L_temp_rec.fee_comp_code      := NVL(L_temp_rec.fee_comp_code,L_default_rec.fee_comp_code);
         L_temp_rec.fee_type           := NVL(L_temp_rec.fee_type,L_default_rec.fee_type);
         L_temp_rec.effect_to          := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.effect_from        := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.import_country_id  := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts                := NVL(L_temp_rec.hts,L_default_rec.hts);
      end if;

      if NOT (L_temp_rec.fee_type              is NOT NULL
              and L_temp_rec.effect_to         is NOT NULL
              and L_temp_rec.effect_from       is NOT NULL
              and L_temp_rec.import_country_id is NOT NULL
              and L_temp_rec.hts               is NOT NULL
              and 1 = 1)   then
              WRITE_S9T_ERROR(I_file_id,
                              HTS_FEE_sheet,
                              rec.row_seq,
                              NULL,
                              NULL,
                              SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
              L_error := TRUE;
      end if;
      
      if NOT L_error then
         SVC_HTS_FEE_col.EXTEND();
         SVC_HTS_FEE_col(SVC_HTS_FEE_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..svc_HTS_FEE_col.COUNT SAVE EXCEPTIONS 
         merge into svc_hts_fee st
         using (select (case
                           when L_mi_rec.FEE_AV_RATE_mi        = 'N'   and
                                svc_HTS_FEE_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and 
                                s1.FEE_AV_RATE is NULL          then 
                                mt.FEE_AV_RATE
                           else s1.FEE_AV_RATE
                           end) as FEE_AV_RATE,
                       (case
                           when L_mi_rec.FEE_SPECIFIC_RATE_mi  = 'N'   and
                                svc_HTS_FEE_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.FEE_SPECIFIC_RATE is NULL    then
                                mt.FEE_SPECIFIC_RATE
                           else s1.FEE_SPECIFIC_RATE
                           end) as FEE_SPECIFIC_RATE,
                       (case
                           when L_mi_rec.FEE_COMP_CODE_mi      = 'N'   and
                                svc_HTS_FEE_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.FEE_COMP_CODE is NULL        then
                                mt.FEE_COMP_CODE
                           else s1.FEE_COMP_CODE
                           end) as FEE_COMP_CODE,
                       (case
                           when L_mi_rec.FEE_TYPE_mi           = 'N'   and
                                svc_HTS_FEE_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.FEE_TYPE is NULL             then
                                mt.FEE_TYPE
                           else s1.FEE_TYPE
                           end) as FEE_TYPE,
                       (case
                           when L_mi_rec.EFFECT_TO_mi          = 'N'   and
                                svc_HTS_FEE_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_TO is NULL            then
                                mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,		
                       (case
                           when L_mi_rec.EFFECT_FROM_mi        = 'N'   and
                                svc_HTS_FEE_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_FROM is NULL          then
                                mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi  = 'N'   and
                                svc_HTS_FEE_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.IMPORT_COUNTRY_ID is NULL    then
                                mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.HTS_mi                = 'N'   and
                                svc_HTS_FEE_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.HTS is NULL                  then
                                mt.HTS
                           else s1.HTS
                           end) as HTS,
                           null as dummy
                  from (select svc_HTS_FEE_col(i).FEE_AV_RATE       as FEE_AV_RATE,
                               svc_HTS_FEE_col(i).FEE_SPECIFIC_RATE as FEE_SPECIFIC_RATE,
                               svc_HTS_FEE_col(i).FEE_COMP_CODE     as FEE_COMP_CODE,
                               svc_HTS_FEE_col(i).FEE_TYPE          as FEE_TYPE,
                               svc_HTS_FEE_col(i).EFFECT_TO         as EFFECT_TO,
                               svc_HTS_FEE_col(i).EFFECT_FROM       as EFFECT_FROM,
                               svc_HTS_FEE_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                               svc_HTS_FEE_col(i).HTS               as HTS,
                               null                                 as dummy
                        from dual)   s1,
                       hts_fee mt
                 where mt.FEE_TYPE (+)          = s1.FEE_TYPE          and
                       mt.EFFECT_TO (+)         = s1.EFFECT_TO         and
                       mt.EFFECT_FROM (+)       = s1.EFFECT_FROM       and
                       mt.IMPORT_COUNTRY_ID (+) = s1.IMPORT_COUNTRY_ID and
                       mt.HTS (+)               = s1.HTS               and
                       1 = 1) sq 
            on (st.FEE_TYPE          = sq.FEE_TYPE
            and st.EFFECT_TO         = sq.EFFECT_TO
            and st.EFFECT_FROM       = sq.EFFECT_FROM
            and st.IMPORT_COUNTRY_ID = sq.IMPORT_COUNTRY_ID
            and st.HTS               = sq.HTS
            and svc_HTS_FEE_col(i).action IN (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
               update
                  set process_id        = svc_HTS_FEE_col(i).process_id,
                      chunk_id          = svc_HTS_FEE_col(i).chunk_id,
                      row_seq           = svc_HTS_FEE_col(i).row_seq,
                      process$status    = svc_HTS_FEE_col(i).process$status,
                      fee_specific_rate = sq.fee_specific_rate,
                      fee_comp_code     = sq.fee_comp_code,
                      fee_av_rate       = sq.fee_av_rate,
                      create_id         = svc_HTS_FEE_col(i).create_id,
                      create_datetime   = svc_HTS_FEE_col(i).create_datetime,
                      last_upd_id       = svc_HTS_FEE_col(i).last_upd_id,
                      last_upd_datetime = svc_HTS_FEE_col(i).last_upd_datetime
            when NOT matched then
               insert(process_id,
                      chunk_id,
                      row_seq,
                      action,
                      process$status,
                      fee_av_rate,
                      fee_specific_rate,
                      fee_comp_code,
                      fee_type,
                      effect_to,
                      effect_from,
                      import_country_id,
                      hts,
                      create_id,
                      create_datetime,
                      last_upd_id,
                      last_upd_datetime)
               values(svc_HTS_FEE_col(i).process_id,
                      svc_HTS_FEE_col(i).chunk_id,
                      svc_HTS_FEE_col(i).row_seq,
                      svc_HTS_FEE_col(i).action,
                      svc_HTS_FEE_col(i).process$status,
                      sq.fee_av_rate,
                      sq.fee_specific_rate,
                      sq.fee_comp_code,
                      sq.fee_type,
                      sq.effect_to,
                      sq.effect_from,
                      sq.import_country_id,
                      sq.hts,
                      svc_HTS_FEE_col(i).create_id,
                      svc_HTS_FEE_col(i).create_datetime,
                      svc_HTS_FEE_col(i).last_upd_id,
                      svc_HTS_FEE_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_sheet,
                            svc_HTS_FEE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_FEE;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_FEE_ZONE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_HTS_FEE_ZONE.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_FEE_ZONE_COL_TYP is TABLE OF SVC_HTS_FEE_ZONE%ROWTYPE;
   L_temp_rec             SVC_HTS_FEE_ZONE%ROWTYPE;
   SVC_HTS_FEE_ZONE_col   SVC_HTS_FEE_ZONE_COL_TYP := NEW SVC_HTS_FEE_ZONE_COL_TYP();
   L_process_id           SVC_HTS_FEE_ZONE.PROCESS_ID%TYPE;
   L_error                BOOLEAN := FALSE;
   L_default_rec          SVC_HTS_FEE_ZONE%ROWTYPE;

   cursor C_MANDATORY_IND is
      select HTS_mi,
             IMPORT_COUNTRY_ID_mi,
             EFFECT_FROM_mi,
             EFFECT_TO_mi,
             FEE_TYPE_mi,
             CLEARING_ZONE_ID_mi,
             FEE_SPECIFIC_RATE_mi,
             FEE_AV_RATE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_HTS_DEFINITION.template_key
                 and wksht_key     = 'HTS_FEE_ZONE')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('HTS'               as HTS,
                                                                'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                'EFFECT_FROM'       as EFFECT_FROM,
                                                                'EFFECT_TO'         as EFFECT_TO,
                                                                'FEE_TYPE'          as FEE_TYPE,
                                                                'CLEARING_ZONE_ID'  as CLEARING_ZONE_ID,
                                                                'FEE_SPECIFIC_RATE' as FEE_SPECIFIC_RATE,
                                                                'FEE_AV_RATE'       as FEE_AV_RATE,
                                                                 NULL               as dummy));
                                     
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HTS_FEE_ZONE';
   L_pk_columns   VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect To,Fee Type,Clearing Zone';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select HTS_dv,
                      IMPORT_COUNTRY_ID_dv,
                      EFFECT_FROM_dv,
                      EFFECT_TO_dv,
                      FEE_TYPE_dv,
                      CLEARING_ZONE_ID_dv,
                      FEE_SPECIFIC_RATE_dv,
                      FEE_AV_RATE_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key     = 'HTS_FEE_ZONE')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('HTS'               as HTS,
                                                                             'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                             'EFFECT_FROM'       as EFFECT_FROM,
                                                                             'EFFECT_TO'         as EFFECT_TO,
                                                                             'FEE_TYPE'          as FEE_TYPE,
                                                                             'CLEARING_ZONE_ID'  as CLEARING_ZONE_ID,
                                                                             'FEE_SPECIFIC_RATE' as FEE_SPECIFIC_RATE,
                                                                             'FEE_AV_RATE'       as FEE_AV_RATE,
                                                                              NULL               as dummy)))
   LOOP
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE_ZONE',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE_ZONE',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE_ZONE',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE_ZONE',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.fee_type := rec.fee_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE_ZONE',
                            NULL,
                            'FEE_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.clearing_zone_id := rec.clearing_zone_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE_ZONE',
                            NULL,
                            'CLEARING_ZONE_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.fee_specific_rate := rec.fee_specific_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE_ZONE',
                            NULL,
                            'FEE_SPECIFIC_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.fee_av_rate := rec.fee_av_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_FEE_ZONE',
                            NULL,
                            'FEE_AV_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(HTS_FEE_ZONE$Action)                   as ACTION,
                      r.get_cell(HTS_FEE_ZONE$HTS)                      as HTS,
                      upper(r.get_cell(HTS_FEE_ZONE$IMPORT_COUNTRY_ID)) as IMPORT_COUNTRY_ID,
                      r.get_cell(HTS_FEE_ZONE$EFFECT_FROM)              as EFFECT_FROM,
                      r.get_cell(HTS_FEE_ZONE$EFFECT_TO)                as EFFECT_TO,
                      upper(r.get_cell(HTS_FEE_ZONE$FEE_TYPE))          as FEE_TYPE,
                      upper(r.get_cell(HTS_FEE_ZONE$CLEARING_ZONE_ID))  as CLEARING_ZONE_ID,
                      r.get_cell(HTS_FEE_ZONE$FEE_SPECIFIC_RATE)        as FEE_SPECIFIC_RATE,
                      r.get_cell(HTS_FEE_ZONE$FEE_AV_RATE)              as FEE_AV_RATE,
                      r.get_row_seq()                                   as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_FEE_ZONE_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fee_type := rec.fee_type;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            'FEE_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.clearing_zone_id := rec.clearing_zone_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            'CLEARING_ZONE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fee_specific_rate := rec.fee_specific_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            'FEE_SPECIFIC_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.fee_av_rate := rec.fee_av_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            rec.row_seq,
                            'FEE_AV_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
         L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.fee_type          := NVL(L_temp_rec.fee_type,L_default_rec.fee_type);
         L_temp_rec.clearing_zone_id  := NVL(L_temp_rec.clearing_zone_id,L_default_rec.clearing_zone_id);
         L_temp_rec.fee_specific_rate := NVL(L_temp_rec.fee_specific_rate,L_default_rec.fee_specific_rate);
         L_temp_rec.fee_av_rate       := NVL(L_temp_rec.fee_av_rate,L_default_rec.fee_av_rate);
      end if;

      if NOT (L_temp_rec.clearing_zone_id is NOT NULL
         and L_temp_rec.fee_type          is NOT NULL
         and L_temp_rec.effect_to         is NOT NULL
         and L_temp_rec.effect_from       is NOT NULL
         and L_temp_rec.import_country_id is NOT NULL
         and L_temp_rec.hts               is NOT NULL
         and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_FEE_ZONE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         SVC_HTS_FEE_ZONE_col.EXTEND();
         SVC_HTS_FEE_ZONE_col(SVC_HTS_FEE_ZONE_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_FEE_ZONE_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts_fee_zone st
         using (select (case
                           when L_mi_rec.HTS_mi = 'N'
                            and SVC_HTS_FEE_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.HTS is NULL then
                                mt.HTS
                           else s1.HTS
                           end) as HTS,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi = 'N'
                            and SVC_HTS_FEE_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.IMPORT_COUNTRY_ID is NULL then
                                mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi = 'N'
                            and SVC_HTS_FEE_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.EFFECT_FROM is NULL then
                                mt.EFFECT_FROM
                           else s1.EFFECT_FROM	
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.EFFECT_TO_mi = 'N'
                            and SVC_HTS_FEE_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.EFFECT_TO is NULL then
                                mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.FEE_TYPE_mi = 'N'
                            and SVC_HTS_FEE_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.FEE_TYPE is NULL then
                                mt.FEE_TYPE
                           else s1.FEE_TYPE
                           end) as FEE_TYPE,
                       (case
                           when L_mi_rec.CLEARING_ZONE_ID_mi = 'N'
                            and SVC_HTS_FEE_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.CLEARING_ZONE_ID is NULL then
                                mt.CLEARING_ZONE_ID
                           else s1.CLEARING_ZONE_ID
                           end) as CLEARING_ZONE_ID,
                       (case
                           when L_mi_rec.FEE_SPECIFIC_RATE_mi = 'N'
                            and SVC_HTS_FEE_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.FEE_SPECIFIC_RATE is NULL then
                                mt.FEE_SPECIFIC_RATE
                           else s1.FEE_SPECIFIC_RATE
                           end) as FEE_SPECIFIC_RATE,
                       (case
                           when L_mi_rec.FEE_AV_RATE_mi = 'N'
                            and SVC_HTS_FEE_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.FEE_AV_RATE  is NULL then
                                mt.FEE_AV_RATE
                           else s1.FEE_AV_RATE
                           end) as FEE_AV_RATE,
                           null as dummy
                  from (select SVC_HTS_FEE_ZONE_col(i).HTS               as HTS,
                               SVC_HTS_FEE_ZONE_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                               SVC_HTS_FEE_ZONE_col(i).EFFECT_FROM       as EFFECT_FROM,
                               SVC_HTS_FEE_ZONE_col(i).EFFECT_TO         as EFFECT_TO,
                               SVC_HTS_FEE_ZONE_col(i).FEE_TYPE          as FEE_TYPE,
                               SVC_HTS_FEE_ZONE_col(i).CLEARING_ZONE_ID  as CLEARING_ZONE_ID,
                               SVC_HTS_FEE_ZONE_col(i).FEE_SPECIFIC_RATE as FEE_SPECIFIC_RATE,
                               SVC_HTS_FEE_ZONE_col(i).FEE_AV_RATE       as FEE_AV_RATE,
                               NULL                                      as dummy
                          from dual)        s1,
                       hts_fee_zone mt
                 where mt.CLEARING_ZONE_ID (+)  = s1.CLEARING_ZONE_ID
                   and mt.FEE_TYPE (+)          = s1.FEE_TYPE
                   and mt.EFFECT_TO (+)         = s1.EFFECT_TO
                   and mt.EFFECT_FROM (+)       = s1.EFFECT_FROM
                   and mt.IMPORT_COUNTRY_ID (+) = s1.IMPORT_COUNTRY_ID
                   and mt.HTS (+)               = s1.HTS
                   and 1 = 1) sq
            on (st.CLEARING_ZONE_ID  = sq.CLEARING_ZONE_ID  and
                st.FEE_TYPE          = sq.FEE_TYPE          and
                st.EFFECT_TO         = sq.EFFECT_TO         and
                st.EFFECT_FROM       = sq.EFFECT_FROM       and
                st.IMPORT_COUNTRY_ID = sq.IMPORT_COUNTRY_ID and
                st.HTS               = sq.HTS               and
                SVC_HTS_FEE_ZONE_col(i).action in (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
            update
               set process_id        = SVC_HTS_FEE_ZONE_col(i).process_id,
                   chunk_id          = SVC_HTS_FEE_ZONE_col(i).chunk_id,
                   action            = SVC_HTS_FEE_ZONE_col(i).action,
                   row_seq           = SVC_HTS_FEE_ZONE_col(i).row_seq,
                   process$status    = SVC_HTS_FEE_ZONE_col(i).process$status,
                   fee_specific_rate = sq.fee_specific_rate,
                   fee_av_rate       = sq.fee_av_rate,
                   create_id         = SVC_HTS_FEE_ZONE_col(i).create_id,
                   create_datetime   = SVC_HTS_FEE_ZONE_col(i).create_datetime,
                   last_upd_id       = SVC_HTS_FEE_ZONE_col(i).last_upd_id,
                   last_upd_datetime = SVC_HTS_FEE_ZONE_col(i).last_upd_datetime
            when NOT matched then
               insert (process_id,
                       chunk_id,
                       row_seq,
                       action,
                       process$status,
                       hts,
                       import_country_id,
                       effect_from,
                       effect_to,
                       fee_type,
                       clearing_zone_id,
                       fee_specific_rate,
                       fee_av_rate,
                       create_id,
                       create_datetime,
                       last_upd_id,
                       last_upd_datetime)
               values (SVC_HTS_FEE_ZONE_col(i).process_id,
                       SVC_HTS_FEE_ZONE_col(i).chunk_id,
                       SVC_HTS_FEE_ZONE_col(i).row_seq,
                       SVC_HTS_FEE_ZONE_col(i).action,
                       SVC_HTS_FEE_ZONE_col(i).process$status,
                       sq.hts,
                       sq.import_country_id,
                       sq.effect_from,
                       sq.effect_to,	
                       sq.fee_type,
                       sq.clearing_zone_id,
                       sq.fee_specific_rate,
                       sq.fee_av_rate,
                       SVC_HTS_FEE_ZONE_col(i).create_id,
                       SVC_HTS_FEE_ZONE_col(i).create_datetime,
                       SVC_HTS_FEE_ZONE_col(i).last_upd_id,
                       SVC_HTS_FEE_ZONE_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_FEE_ZONE_sheet,
                            SVC_HTS_FEE_ZONE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_FEE_ZONE;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_OGA(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id   IN   SVC_HTS_OGA.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_OGA_COL_TYP is TABLE OF SVC_HTS_OGA%ROWTYPE;
   L_temp_rec        SVC_HTS_OGA%ROWTYPE;
   SVC_HTS_OGA_col   SVC_HTS_OGA_COL_TYP := NEW SVC_HTS_OGA_COL_TYP();
   L_process_id      SVC_HTS_OGA.PROCESS_ID%TYPE;
   L_error           BOOLEAN := FALSE;
   L_default_rec     SVC_HTS_OGA%ROWTYPE;

   cursor C_MANDATORY_IND is
      select COMMENTS_mi,
             REFERENCE_ID_mi,
             OGA_CODE_mi,
             EFFECT_TO_mi,
             EFFECT_FROM_mi,
             IMPORT_COUNTRY_ID_mi,
             HTS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key               = CORESVC_HTS_DEFINITION.template_key
                 and wksht_key                  = 'HTS_OGA')
               PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('COMMENTS'          as COMMENTS,
                                            'REFERENCE_ID'      as REFERENCE_ID,
                                            'OGA_CODE'          as OGA_CODE,
                                            'EFFECT_TO'         as EFFECT_TO,
                                            'EFFECT_FROM'       as EFFECT_FROM,
                                            'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                            'HTS'               as HTS,
                                             NULL               as dummy));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)  := 'SVC_HTS_OGA';
   L_pk_columns    VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect to,OGA Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   --Get default values.
   FOR rec IN (select  COMMENTS_dv,
                       REFERENCE_ID_dv,
                       OGA_CODE_dv,
                       EFFECT_TO_dv,
                       EFFECT_FROM_dv,
                       IMPORT_COUNTRY_ID_dv,
                       HTS_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key      = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key         = 'HTS_OGA')
                        PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ('COMMENTS'          as COMMENTS,
                                                     'REFERENCE_ID'      as REFERENCE_ID,
                                                     'OGA_CODE'          as OGA_CODE,
                                                     'EFFECT_TO'         as EFFECT_TO,
                                                     'EFFECT_FROM'       as EFFECT_FROM,
                                                     'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                     'HTS'               as HTS,
                                                      NULL               as dummy)))
   LOOP
      BEGIN
         L_default_rec.comments := rec.comments_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_OGA',
                            NULL,
                            'COMMENTS',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.reference_id := rec.reference_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_OGA',
                            NULL,
                            'REFERENCE_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.oga_code := rec.oga_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_OGA',
                            NULL,
                            'OGA_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_OGA',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_OGA',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_OGA',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_OGA',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(HTS_OGA$Action))             as ACTION,
                      r.get_cell(HTS_OGA$COMMENTS)                  as COMMENTS,
                      r.get_cell(HTS_OGA$REFERENCE_ID)              as REFERENCE_ID,
                      UPPER(r.get_cell(HTS_OGA$OGA_CODE))           as OGA_CODE,
                      r.get_cell(HTS_OGA$EFFECT_TO)                 as EFFECT_TO,
                      r.get_cell(HTS_OGA$EFFECT_FROM)               as EFFECT_FROM,
                      UPPER(r.get_cell(HTS_OGA$IMPORT_COUNTRY_ID))  as IMPORT_COUNTRY_ID,
                      r.get_cell(HTS_OGA$HTS)                       as HTS,
                      r.get_row_seq()                               as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_OGA_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.comments := rec.comments;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            rec.row_seq,
                            'COMMENTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reference_id := rec.reference_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            rec.row_seq,
                            'REFERENCE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.oga_code := rec.oga_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            rec.row_seq,
                            'OGA_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.comments          := NVL(L_temp_rec.comments,L_default_rec.comments);
         L_temp_rec.reference_id      := NVL(L_temp_rec.reference_id,L_default_rec.reference_id);
         L_temp_rec.oga_code          := NVL(L_temp_rec.oga_code,L_default_rec.oga_code);
         L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
      end if;
      if NOT (L_temp_rec.oga_code          is NOT NULL
          and L_temp_rec.effect_to         is NOT NULL
          and L_temp_rec.effect_from       is NOT NULL
          and L_temp_rec.import_country_id is NOT NULL
          and L_temp_rec.hts is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_OGA_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_HTS_OGA_col.extend();
         SVC_HTS_OGA_col(SVC_HTS_OGA_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_OGA_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts_oga st
         using (select (case
                           when L_mi_rec.COMMENTS_mi    = 'N'
                            and SVC_HTS_OGA_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.COMMENTS             is NULL
                           then mt.COMMENTS
                           else s1.COMMENTS
                           end) as COMMENTS,
                       (case
                           when L_mi_rec.REFERENCE_ID_mi    = 'N'
                            and SVC_HTS_OGA_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.REFERENCE_ID             is NULL
                           then mt.REFERENCE_ID
                           else s1.REFERENCE_ID
                           end) as REFERENCE_ID,
                       (case
                           when L_mi_rec.OGA_CODE_mi    = 'N'
                            and SVC_HTS_OGA_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.OGA_CODE             is NULL
                           then mt.OGA_CODE
                           else s1.OGA_CODE
                           end) as OGA_CODE,
                       (case
                           when L_mi_rec.EFFECT_TO_mi    = 'N'
                            and SVC_HTS_OGA_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.EFFECT_TO             is NULL
                           then mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi    = 'N'
                            and SVC_HTS_OGA_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.EFFECT_FROM             is NULL
                           then mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi    = 'N'
                            and SVC_HTS_OGA_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.IMPORT_COUNTRY_ID             is NULL
                           then mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.HTS_mi    = 'N'
                            and SVC_HTS_OGA_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.HTS             is NULL
                           then mt.HTS
                           else s1.HTS
                           end) as HTS,
                           null as dummy
                  from (select SVC_HTS_OGA_col(i).COMMENTS          as COMMENTS,
                               SVC_HTS_OGA_col(i).REFERENCE_ID      as REFERENCE_ID,
                               SVC_HTS_OGA_col(i).OGA_CODE          as OGA_CODE,
                               SVC_HTS_OGA_col(i).EFFECT_TO         as EFFECT_TO,
                               SVC_HTS_OGA_col(i).EFFECT_FROM       as EFFECT_FROM,
                               SVC_HTS_OGA_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                               SVC_HTS_OGA_col(i).HTS               as HTS,
                               null as dummy
                          from dual) s1,
                       hts_oga mt
                 where mt.OGA_CODE (+)              = s1.OGA_CODE
                   and mt.EFFECT_TO (+)             = s1.EFFECT_TO
                   and mt.EFFECT_FROM (+)           = s1.EFFECT_FROM
                   and mt.IMPORT_COUNTRY_ID (+)     = s1.IMPORT_COUNTRY_ID
                   and mt.HTS (+)                   = s1.HTS
                   and 1 = 1) sq
              on (st.OGA_CODE               = sq.OGA_CODE and
                  st.EFFECT_TO              = sq.EFFECT_TO and
                  st.EFFECT_FROM            = sq.EFFECT_FROM and
                  st.IMPORT_COUNTRY_ID      = sq.IMPORT_COUNTRY_ID and
                  st.HTS                    = sq.HTS and
                  SVC_HTS_OGA_col(i).action IN (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
               update
                  set process_id        = SVC_HTS_OGA_col(i).process_id,
                      chunk_id          = SVC_HTS_OGA_col(i).chunk_id,
                      row_seq           = SVC_HTS_OGA_col(i).row_seq,
                      action            = SVC_HTS_OGA_col(i).action,
                      process$status    = SVC_HTS_OGA_col(i).process$status,
                      comments          = sq.comments,
                      reference_id      = sq.reference_id,
                      create_id         = SVC_HTS_OGA_col(i).create_id,
                      create_datetime   = SVC_HTS_OGA_col(i).create_datetime,
                      last_upd_id       = SVC_HTS_OGA_col(i).last_upd_id,
                      last_upd_datetime = SVC_HTS_OGA_col(i).last_upd_datetime
            when NOT matched then
               insert (process_id,
                       chunk_id,
                       row_seq,
                       action,
                       process$status,
                       comments,
                       reference_id,
                       oga_code,
                       effect_to,
                       effect_from,
                       import_country_id,
                       hts,
                       create_id,
                       create_datetime,
                       last_upd_id,
                       last_upd_datetime)
               values (SVC_HTS_OGA_col(i).process_id,
                       SVC_HTS_OGA_col(i).chunk_id,
                       SVC_HTS_OGA_col(i).row_seq,
                       SVC_HTS_OGA_col(i).action,
                       SVC_HTS_OGA_col(i).process$status,
                       sq.comments,
                       sq.reference_id,
                       sq.oga_code,
                       sq.effect_to,
                       sq.effect_from,
                       sq.import_country_id,
                       sq.hts,
                       SVC_HTS_OGA_col(i).create_id,
                       SVC_HTS_OGA_col(i).create_datetime,
                       SVC_HTS_OGA_col(i).last_upd_id,
                       SVC_HTS_OGA_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_OGA_sheet,
                            SVC_HTS_OGA_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_OGA;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_REF(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id   IN   SVC_HTS_REF.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_REF_COL_TYP is TABLE OF SVC_HTS_REF%ROWTYPE;
   L_temp_rec        SVC_HTS_REF%ROWTYPE;
   SVC_HTS_REF_col   SVC_HTS_REF_COL_TYP         := NEW SVC_HTS_REF_COL_TYP();
   L_error           BOOLEAN                     := FALSE;
   L_process_id      SVC_HTS_REF.PROCESS_ID%TYPE;
   L_default_rec     SVC_HTS_REF%ROWTYPE;

   cursor C_MANDATORY_IND is
      select reference_desc_mi,
             reference_id_mi,
             effect_to_mi,
             effect_from_mi,
             import_country_id_mi,
             hts_mi,
             1 as dummy
        from (SELECT column_key,
                     mandatory
                FROM s9t_tmpl_cols_def
               WHERE template_key                                = CORESVC_HTS_DEFINITION.template_key
                 AND wksht_key                                   = 'HTS_REFERENCE')
               PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('REFERENCE_DESC'    as reference_desc,
                                            'REFERENCE_ID'      as reference_id,
                                            'EFFECT_TO'         as effect_to,
                                            'EFFECT_FROM'       as effect_from,
                                            'IMPORT_COUNTRY_ID' as import_country_id,
                                            'HTS'               as hts,
                                            NULL                as dummy));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)   := 'SVC_HTS_REF';
   L_pk_columns   VARCHAR2(255)  := 'HTS,Importing Country,Effect From,Effect To,Reference ID';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
   
BEGIN
   FOR rec IN (select reference_desc_dv,
                      reference_id_dv,
                      effect_to_dv,
                      effect_from_dv,
                      import_country_id_dv,
                      hts_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                    = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key                                       = 'HTS_REFERENCE'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ('REFERENCE_DESC'    as reference_desc,
                                                     'REFERENCE_ID'      as reference_id,
                                                     'EFFECT_TO'         as effect_to,
                                                     'EFFECT_FROM'       as effect_from,
                                                     'IMPORT_COUNTRY_ID' as import_country_id,
                                                     'HTS'               as hts,
                                                      NULL               as dummy)))
   LOOP
      BEGIN
         L_default_rec.reference_desc := rec.reference_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_REFERENCE',
                            NULL,
                            'REFERENCE_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.reference_id := rec.reference_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_REFERENCE',
                            NULL,
                            'REFERENCE_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_REFERENCE',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_REFERENCE',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_REFERENCE',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_REFERENCE',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(HTS_REF$ACTION))                  as action,
                      r.get_cell(HTS_REF$REFERENCE_DESC)                 as reference_desc,
                      UPPER(r.get_cell(HTS_REF$REFERENCE_ID))            as reference_id,
                      r.get_cell(HTS_REF$EFFECT_TO)                      as effect_to,
                      r.get_cell(HTS_REF$EFFECT_FROM)                    as effect_from,
                      UPPER(r.get_cell(HTS_REF$IMPORT_COUNTRY_ID))       as import_country_id,
                      r.get_cell(HTS_REF$HTS)                            as hts,
                      r.get_row_seq()                                    as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_REF_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_REF_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reference_desc := rec.reference_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_REF_sheet,
                            rec.row_seq,
                            'REFERENCE_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.reference_id := rec.reference_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_REF_sheet,
                            rec.row_seq,
                            'REFERENCE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YYYY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_REF_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YYYY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_REF_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_REF_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_REF_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.reference_desc    := NVL(L_temp_rec.reference_desc,L_default_rec.reference_desc);
         L_temp_rec.reference_id      := NVL(L_temp_rec.reference_id,L_default_rec.reference_id);
         L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
      end if;
      if NOT (L_temp_rec.reference_id is NOT NULL
              and L_temp_rec.effect_to is NOT NULL
              and L_temp_rec.effect_from is NOT NULL
              and L_temp_rec.import_country_id is NOT NULL
              and L_temp_rec.hts is NOT NULL
              and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_REF_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      END IF;
      if NOT L_error then
         SVC_HTS_REF_col.extend();
         SVC_HTS_REF_col(SVC_HTS_REF_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_REF_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts_ref st
         using (select (case
                           when L_mi_rec.reference_desc_mi    = 'N'
                            and SVC_HTS_REF_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.reference_desc  is NULL
                           then mt.reference_desc
                           else s1.reference_desc
                           end) as reference_desc,
                       (case
                           when L_mi_rec.reference_id_mi    = 'N'
                            and SVC_HTS_REF_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.reference_id       is NULL
                           then mt.reference_id
                           else s1.reference_id
                           end) as reference_id,
                       (case
                           when L_mi_rec.effect_to_mi    = 'N'
                            and SVC_HTS_REF_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.effect_to          is NULL
                           then mt.effect_to
                           else s1.effect_to
                           end) as effect_to,
                       (case
                           when L_mi_rec.effect_from_mi    = 'N'
                            and SVC_HTS_REF_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.effect_from        is NULL
                           then mt.effect_from
                           else s1.effect_from
                           end) as effect_from,
                       (case
                           when L_mi_rec.import_country_id_mi    = 'N'
                            and SVC_HTS_REF_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.import_country_id  is NULL
                           then mt.import_country_id
                           else s1.import_country_id
                           end) as import_country_id,
                       (case
                           when L_mi_rec.hts_mi    = 'N'
                            and SVC_HTS_REF_col(i).action = CORESVC_HTS_DEFINITION.action_mod
                            and s1.hts                is NULL
                           then mt.hts
                           else s1.hts
                           end) as hts,
                           null as dummy
                 from (select SVC_HTS_REF_col(i).hts                as hts,
                              SVC_HTS_REF_col(i).import_country_id  as import_country_id,
                              SVC_HTS_REF_col(i).effect_from        as effect_from,
                              SVC_HTS_REF_col(i).effect_to          as effect_to,
                              SVC_HTS_REF_col(i).reference_id       as reference_id,
                              SVC_HTS_REF_col(i).reference_desc     as reference_desc,
                              null as dummy
                         from dual) s1,
                      hts_reference mt
                where mt.reference_id (+)          = s1.reference_id
                  and mt.effect_to (+)             = s1.effect_to
                  and mt.effect_from (+)           = s1.effect_from
                  and mt.import_country_id (+)     = s1.import_country_id
                  and mt.hts (+)                   = s1.hts
                  and 1 = 1) sq
              on (st.reference_id           = sq.reference_id      and
                  st.effect_to              = sq.effect_to         and
                  st.effect_from            = sq.effect_from       and
                  st.import_country_id      = sq.import_country_id and
                  st.hts                    = sq.hts               and
                  SVC_HTS_REF_col(i).action IN (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
              when matched then
                 update
                    set process_id        = SVC_HTS_REF_col(i).process_id,
                        chunk_id          = SVC_HTS_REF_col(i).chunk_id,
                        row_seq           = SVC_HTS_REF_col(i).row_seq,
                        action            = SVC_HTS_REF_col(i).action,
                        process$status    = SVC_HTS_REF_col(i).process$status,
                        reference_desc    = sq.reference_desc,
                        create_id         = SVC_HTS_REF_col(i).create_id,
                        create_datetime   = SVC_HTS_REF_col(i).create_datetime,
                        last_upd_id       = SVC_HTS_REF_col(i).last_upd_id,
                        last_upd_datetime = SVC_HTS_REF_col(i).last_upd_datetime
              when NOT matched then
                 insert (process_id,
                         chunk_id,
                         row_seq,
                         action,
                         process$status,
                         reference_desc,
                         reference_id,
                         effect_to,
                         effect_from,
                         import_country_id,
                         hts,
                         create_id,
                         create_datetime,
                         last_upd_id,
                         last_upd_datetime)
                 values (SVC_HTS_REF_col(i).process_id,
                         SVC_HTS_REF_col(i).chunk_id,
                         SVC_HTS_REF_col(i).row_seq,
                         SVC_HTS_REF_col(i).action,
                         SVC_HTS_REF_col(i).process$status,
                         sq.reference_desc,
                         sq.reference_id,
                         sq.effect_to,
                         sq.effect_from,
                         sq.import_country_id,
                         sq.hts,
                         SVC_HTS_REF_col(i).create_id,
                         SVC_HTS_REF_col(i).create_datetime,
                         SVC_HTS_REF_col(i).last_upd_id,
                         SVC_HTS_REF_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_REF_sheet,
                            SVC_HTS_REF_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_REF;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTT(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_HTS_TARIFF_TREATMENT.PROCESS_ID%TYPE) IS

   TYPE SVC_HTT_COL_TYP is TABLE OF SVC_HTS_TARIFF_TREATMENT%ROWTYPE;
   L_error       BOOLEAN := FALSE;
   L_temp_rec    SVC_HTS_TARIFF_TREATMENT%ROWTYPE;
   SVC_HTT_col   SVC_HTT_COL_TYP := NEW SVC_HTT_COL_TYP();
   L_process_id  SVC_HTS_TARIFF_TREATMENT.PROCESS_ID%TYPE;
   L_default_rec SVC_HTS_TARIFF_TREATMENT%ROWTYPE;

   cursor C_MANDATORY_IND is
      select OTHER_RATE_mi,
             AV_RATE_mi,
             SPECIFIC_RATE_mi,
             TARIFF_TREATMENT_mi,
             EFFECT_TO_mi,
             EFFECT_FROM_mi,
             IMPORT_COUNTRY_ID_mi,
             HTS_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_HTS_DEFINITION.template_key
                 and wksht_key     = 'HTS_TARIFF_TREATMENT')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('OTHER_RATE'        as OTHER_RATE,
                                                                'AV_RATE'           as AV_RATE,
                                                                'SPECIFIC_RATE'     as SPECIFIC_RATE,
                                                                'TARIFF_TREATMENT'  as TARIFF_TREATMENT,
                                                                'EFFECT_TO'         as EFFECT_TO,
                                                                'EFFECT_FROM'       as EFFECT_FROM,
                                                                'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                'HTS'               as HTS,
                                                                 NULL               as DUMMY));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HTS_TARIFF_TREATMENT';
   L_pk_columns   VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect To,Tariff Treatment';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select OTHER_RATE_dv,
                      AV_RATE_dv,
                      SPECIFIC_RATE_dv,
                      TARIFF_TREATMENT_dv,
                      EFFECT_TO_dv,
                      EFFECT_FROM_dv,
                      IMPORT_COUNTRY_ID_dv,
                      HTS_dv,
                      null as DUMMY
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key     = 'HTS_TARIFF_TREATMENT')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('OTHER_RATE'        as OTHER_RATE,
                                                                             'AV_RATE'           as AV_RATE,
                                                                             'SPECIFIC_RATE'     as SPECIFIC_RATE,
                                                                             'TARIFF_TREATMENT'  as TARIFF_TREATMENT,
                                                                             'EFFECT_TO'         as EFFECT_TO,
                                                                             'EFFECT_FROM'       as EFFECT_FROM,
                                                                             'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                             'HTS'               as HTS,
                                                                              NULL               as DUMMY)))
   LOOP
      BEGIN
         L_default_rec.other_rate := rec.other_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT',
                            NULL,
                            'OTHER_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.av_rate := rec.av_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT',
                            NULL,
                            'AV_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.specific_rate := rec.specific_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT',
                            NULL,
                            'SPECIFIC_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tariff_treatment := rec.tariff_treatment_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT',
                            NULL,
                            'TARIFF_TREATMENT',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN  (select r.get_cell(HTT$Action)                    as  ACTION,
                       r.get_cell(HTT$OTHER_RATE)                as  OTHER_RATE,
                       r.get_cell(HTT$AV_RATE)                   as  AV_RATE,
                       r.get_cell(HTT$SPECIFIC_RATE)             as  SPECIFIC_RATE,
                       UPPER(r.get_cell(HTT$TARIFF_TREATMENT))   as  TARIFF_TREATMENT,
                       r.get_cell(HTT$EFFECT_TO)                 as  EFFECT_TO,
                       r.get_cell(HTT$EFFECT_FROM)               as  EFFECT_FROM,
                       UPPER(r.get_cell(HTT$IMPORT_COUNTRY_ID))  as  IMPORT_COUNTRY_ID,
                       r.get_cell(HTT$HTS)                       as  HTS,
                       r.get_row_seq()                           as  ROW_SEQ
                  from s9t_folder sf,
                       TABLE(sf.s9t_file_obj.sheets) ss,
                       TABLE(ss.s9t_rows) r
                 where sf.file_id  = I_file_id
                   and ss.sheet_name = GET_SHEET_NAME_TRANS(HTT_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
       BEGIN
          L_temp_rec.action := rec.action;
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             action_column,
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
       END;
       BEGIN
         L_temp_rec.other_rate := rec.other_rate;
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             'OTHER_RATE',
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
       END;
       BEGIN
         L_temp_rec.av_rate := rec.av_rate;
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             'AV_RATE',
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
       END;
       BEGIN
          L_temp_rec.specific_rate := rec.specific_rate;
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             'SPECIFIC_RATE',
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
       END;
       BEGIN
         L_temp_rec.tariff_treatment := rec.tariff_treatment;
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             'TARIFF_TREATMENT',
                             SQLCODE,
                             SQLERRM);
          L_error := TRUE;
       END;
       BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             'EFFECT_TO',
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
       END;
       BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             'EFFECT_FROM',
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
       END;
       BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             'IMPORT_COUNTRY_ID',
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
       END;
       BEGIN
         L_temp_rec.hts := rec.hts;
       EXCEPTION
          when OTHERS then
             WRITE_S9T_ERROR(I_file_id,
                             HTT_sheet,
                             rec.row_seq,
                             'HTS',
                             SQLCODE,
                             SQLERRM);
             L_error := TRUE;
       END;

       if rec.action = CORESVC_HTS_DEFINITION.action_new then
          L_temp_rec.other_rate        := NVL(L_temp_rec.other_rate,L_default_rec.other_rate);
          L_temp_rec.av_rate           := NVL(L_temp_rec.av_rate,L_default_rec.av_rate);
          L_temp_rec.specific_rate     := NVL(L_temp_rec.specific_rate,L_default_rec.specific_rate);
          L_temp_rec.tariff_treatment  := NVL(L_temp_rec.tariff_treatment,L_default_rec.tariff_treatment);
          L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
          L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
          L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
          L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
       end if;

       if NOT (L_temp_rec.TARIFF_TREATMENT  is NOT NULL
           and L_temp_rec.EFFECT_TO         is NOT NULL
           and L_temp_rec.EFFECT_FROM       is NOT NULL
           and L_temp_rec.IMPORT_COUNTRY_ID is NOT NULL
           and L_temp_rec.HTS               is NOT NULL
           and 1 = 1) then
           WRITE_S9T_ERROR(I_file_id,
                           HTT_sheet,
                           rec.row_seq,
                           NULL,
                           NULL,
                           SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
           L_error := TRUE;
       end if;

       if NOT L_error then
          SVC_HTT_col.EXTEND();
          SVC_HTT_col(SVC_HTT_col.COUNT()) := L_temp_rec;
       end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTT_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts_tariff_treatment st
         using (select (case
                           when L_mi_rec.OTHER_RATE_mi = 'N' and
                                SVC_HTT_col(i).action  = CORESVC_HTS_DEFINITION.action_mod and
                                s1.OTHER_RATE is NULL then 
                                mt.OTHER_RATE
                           else s1.OTHER_RATE
                           end) as OTHER_RATE,
                       (case
                           when L_mi_rec.AV_RATE_mi    = 'N' and
                                SVC_HTT_col(i).action  = CORESVC_HTS_DEFINITION.action_mod and
                                s1.AV_RATE is NULL then
                                mt.AV_RATE
                           else s1.AV_RATE
                           end) as AV_RATE,
                       (case
                           when L_mi_rec.SPECIFIC_RATE_mi = 'N' and
                                SVC_HTT_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.SPECIFIC_RATE is NULL then
                                mt.SPECIFIC_RATE
                           else s1.SPECIFIC_RATE
                           end) as SPECIFIC_RATE,
                       (case
                           when L_mi_rec.TARIFF_TREATMENT_mi = 'N' and
                                SVC_HTT_col(i).action      = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TARIFF_TREATMENT is NULL then
                                mt.TARIFF_TREATMENT
                           else s1.TARIFF_TREATMENT
                           end) as TARIFF_TREATMENT,
                       (case
                           when L_mi_rec.EFFECT_TO_mi = 'N' and
                                SVC_HTT_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_TO is NULL then
                                mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi = 'N' and
                                SVC_HTT_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_FROM is NULL then
                                mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi = 'N' and
                                SVC_HTT_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.IMPORT_COUNTRY_ID is NULL then
                                mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.HTS_mi = 'N' and
                                SVC_HTT_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.HTS is NULL then
                                mt.HTS
                           else s1.HTS
                           end) as HTS,
                           null as dummy
                  from (select SVC_HTT_col(i).OTHER_RATE        as OTHER_RATE,
                               SVC_HTT_col(i).AV_RATE           as AV_RATE,
                               SVC_HTT_col(i).SPECIFIC_RATE     as SPECIFIC_RATE,
                               SVC_HTT_col(i).TARIFF_TREATMENT  as TARIFF_TREATMENT,
                               SVC_HTT_col(i).EFFECT_TO         as EFFECT_TO,
                               SVC_HTT_col(i).EFFECT_FROM       as EFFECT_FROM,
                               SVC_HTT_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                               SVC_HTT_col(i).HTS               as HTS,
                               null                             as DUMMY
                          from dual)                s1,
                       hts_tariff_treatment mt
                 where mt.TARIFF_TREATMENT (+)  = s1.TARIFF_TREATMENT
                   and mt.EFFECT_TO (+)         = s1.EFFECT_TO
                   and mt.EFFECT_FROM (+)       = s1.EFFECT_FROM
                   and mt.IMPORT_COUNTRY_ID (+) = s1.IMPORT_COUNTRY_ID
                   and mt.HTS (+)               = s1.HTS 
                   and 1 = 1) sq
             on (st.TARIFF_TREATMENT  = sq.TARIFF_TREATMENT
             and st.EFFECT_TO         = sq.EFFECT_TO
             and st.EFFECT_FROM       = sq.EFFECT_FROM
             and st.IMPORT_COUNTRY_ID = sq.IMPORT_COUNTRY_ID
             and st.HTS               = sq.HTS
             and SVC_HTT_col(i).action in (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
               update
                  set process_id        = SVC_HTT_col(i).process_id,
                      chunk_id          = SVC_HTT_col(i).chunk_id,
                      action            = SVC_HTT_col(i).action,
                      row_seq           = SVC_HTT_col(i).row_seq,
                      process$status    = SVC_HTT_col(i).process$status,
                      other_rate        = sq.other_rate,
                      av_rate           = sq.av_rate,
                      specific_rate     = sq.specific_rate,
                      create_id         = SVC_HTT_col(i).create_id,
                      create_datetime   = SVC_HTT_col(i).create_datetime,
                      last_upd_id       = SVC_HTT_col(i).last_upd_id,
                      last_upd_datetime = SVC_HTT_col(i).last_upd_datetime
            when NOT matched then
               insert (process_id,
                       chunk_id,
                       row_seq,
                       action,
                       process$status,
                       other_rate,
                       av_rate,
                       specific_rate,
                       tariff_treatment,
                       effect_to,
                       effect_from,
                       import_country_id,
                       hts,
                       create_id,
                       create_datetime,
                       last_upd_id,
                       last_upd_datetime)
               values (SVC_HTT_col(i).process_id,
                       SVC_HTT_col(i).chunk_id,
                       SVC_HTT_col(i).row_seq,
                       SVC_HTT_col(i).action,
                       SVC_HTT_col(i).process$status,
                       sq.other_rate,
                       sq.av_rate,
                       sq.specific_rate,
                       sq.tariff_treatment,
                       sq.effect_to,
                       sq.effect_from,
                       sq.import_country_id,
                       sq.hts,
                       SVC_HTT_col(i).create_id,
                       SVC_HTT_col(i).create_datetime,
                       SVC_HTT_col(i).last_upd_id,
                       SVC_HTT_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTT_sheet,
                            SVC_HTT_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTT;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_TT_EXCLUSIONS(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                        I_process_id   IN   SVC_HTS_TT_EXCLUSIONS.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_TT_EXC_COL_TYP is TABLE OF SVC_HTS_TT_EXCLUSIONS%ROWTYPE;
   L_error             BOOLEAN := FALSE;
   L_temp_rec          SVC_HTS_TT_EXCLUSIONS%ROWTYPE;
   SVC_HTS_TT_EXC_col  SVC_HTS_TT_EXC_COL_TYP := NEW SVC_HTS_TT_EXC_COL_TYP();
   L_process_id        SVC_HTS_TT_EXCLUSIONS.PROCESS_ID%TYPE;
   L_default_rec       SVC_HTS_TT_EXCLUSIONS%ROWTYPE;

   cursor C_MANDATORY_IND is
      select origin_country_id_mi,
             tariff_treatment_mi,
             effect_to_mi,
             effect_from_mi,
             import_country_id_mi,
             hts_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_HTS_DEFINITION.template_key
                 and wksht_key     = 'HTS_TT_EXCLUSIONS')
               PIVOT (MAX(mandatory) as mi FOR (column_key) in ('ORIGIN_COUNTRY_ID' as ORIGIN_COUNTRY_ID,
                                                                'TARIFF_TREATMENT'  as TARIFF_TREATMENT,
                                                                'EFFECT_TO'         as EFFECT_TO,
                                                                'EFFECT_FROM'       as EFFECT_FROM,
                                                                'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                'HTS'               as HTS,
                                                                NULL                as DUMMY));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HTS_TT_EXCLUSIONS';
   L_pk_columns   VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect To,Tariff Treatment,Country Of Sourcing';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select origin_country_id_dv,
                      tariff_treatment_dv,
                      effect_to_dv,
                      effect_from_dv,
                      import_country_id_dv,
                      hts_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key     = 'HTS_TT_EXCLUSIONS')
                        PIVOT (MAX(default_value) as dv FOR (column_key) in ('ORIGIN_COUNTRY_ID'  as ORIGIN_COUNTRY_ID,
                                                                             'TARIFF_TREATMENT'   as TARIFF_TREATMENT,
                                                                             'EFFECT_TO'          as EFFECT_TO,
                                                                             'EFFECT_FROM'        as EFFECT_FROM,
                                                                             'IMPORT_COUNTRY_ID'  as IMPORT_COUNTRY_ID,
                                                                             'HTS'                as HTS,
                                                                              NULL                as DUMMY)))
   LOOP
      BEGIN
         L_default_rec.origin_country_id := rec.origin_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TT_EXCLUSIONS',
                             NULL,
                            'ORIGIN_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tariff_treatment := rec.tariff_treatment_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TT_EXCLUSIONS',
                             NULL,
                            'TARIFF_TREATMENT',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TT_EXCLUSIONS',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TT_EXCLUSIONS',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TT_EXCLUSIONS',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TT_EXCLUSIONS',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(HTS_TT_EXC$Action)                   as ACTION,
                      UPPER(r.get_cell(HTS_TT_EXC$ORIGIN_COUNTRY_ID)) as ORIGIN_COUNTRY_ID,
                      UPPER(r.get_cell(HTS_TT_EXC$TARIFF_TREATMENT))  as TARIFF_TREATMENT,
                      r.get_cell(HTS_TT_EXC$EFFECT_TO)                as EFFECT_TO,
                      r.get_cell(HTS_TT_EXC$EFFECT_FROM)              as EFFECT_FROM,
                      UPPER(r.get_cell(HTS_TT_EXC$IMPORT_COUNTRY_ID)) as IMPORT_COUNTRY_ID,
                      r.get_cell(HTS_TT_EXC$HTS)                      as HTS,
                      r.get_row_seq()   	                          as ROW_SEQ
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(HTS_TT_EXC_sheet)) 
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_EXC_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.origin_country_id := rec.origin_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_EXC_sheet,
                            rec.row_seq,
                            'ORIGIN_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tariff_treatment := rec.tariff_treatment;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_EXC_sheet,
                            rec.row_seq,
                            'TARIFF_TREATMENT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_EXC_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_EXC_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_EXC_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_EXC_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      
      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.origin_country_id := NVL(L_temp_rec.origin_country_id,L_default_rec.origin_country_id);
         L_temp_rec.tariff_treatment  := NVL(L_temp_rec.tariff_treatment,L_default_rec.tariff_treatment);
         L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
      end if;
      
      if NOT (L_temp_rec.ORIGIN_COUNTRY_ID is NOT NULL and
              L_temp_rec.TARIFF_TREATMENT  is NOT NULL and
              L_temp_rec.EFFECT_TO         is NOT NULL and
              L_temp_rec.EFFECT_FROM       is NOT NULL and
              L_temp_rec.IMPORT_COUNTRY_ID is NOT NULL and
              L_temp_rec.HTS               is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_TT_EXC_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      
      if NOT L_error then
         SVC_HTS_TT_EXC_col.EXTEND();
         SVC_HTS_TT_EXC_col(SVC_HTS_TT_EXC_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_TT_EXC_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts_tt_exclusions st
         using (select (case
                           when L_mi_rec.ORIGIN_COUNTRY_ID_mi = 'N'   and
                                SVC_HTS_TT_EXC_col(i).action  = CORESVC_HTS_DEFINITION.action_mod and
                                s1.ORIGIN_COUNTRY_ID is NULL  then
                                mt.ORIGIN_COUNTRY_ID
                           else s1.ORIGIN_COUNTRY_ID
                           end) as ORIGIN_COUNTRY_ID,
                       (case
                           when L_mi_rec.TARIFF_TREATMENT_mi    = 'N' and
                                SVC_HTS_TT_EXC_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TARIFF_TREATMENT is NULL   then
                                mt.TARIFF_TREATMENT
                           else s1.TARIFF_TREATMENT
                           end) as TARIFF_TREATMENT,
                       (case
                           when L_mi_rec.EFFECT_TO_mi    = 'N'        and
                                SVC_HTS_TT_EXC_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_TO is NULL          then
                                mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi  = 'N'        and
                                SVC_HTS_TT_EXC_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_FROM is NULL        then
                                mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi   = 'N' and
                                SVC_HTS_TT_EXC_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.IMPORT_COUNTRY_ID is NULL  then
                                mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.HTS_mi = 'N'                 and
                                SVC_HTS_TT_EXC_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.HTS is NULL                then
                                mt.HTS
                           else s1.HTS
                           end) as HTS,
                           null  as dummy
                  from (select SVC_HTS_TT_EXC_col(i).ORIGIN_COUNTRY_ID as ORIGIN_COUNTRY_ID,
                               SVC_HTS_TT_EXC_col(i).TARIFF_TREATMENT  as TARIFF_TREATMENT,
                               SVC_HTS_TT_EXC_col(i).EFFECT_TO         as EFFECT_TO,
                               SVC_HTS_TT_EXC_col(i).EFFECT_FROM       as EFFECT_FROM,
                               SVC_HTS_TT_EXC_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                               SVC_HTS_TT_EXC_col(i).HTS               as HTS,
                               null                                    as DUMMY
                          from dual)             s1,
                       hts_tt_exclusions mt
                 where mt.ORIGIN_COUNTRY_ID (+) = s1.ORIGIN_COUNTRY_ID
                   and mt.TARIFF_TREATMENT (+)  = s1.TARIFF_TREATMENT
                   and mt.EFFECT_TO (+)         = s1.EFFECT_TO
                   and mt.EFFECT_FROM (+)       = s1.EFFECT_FROM
                   and mt.IMPORT_COUNTRY_ID (+) = s1.IMPORT_COUNTRY_ID
                   and mt.HTS (+)               = s1.HTS
                   and 1 = 1) sq
             on ( st.ORIGIN_COUNTRY_ID = sq.ORIGIN_COUNTRY_ID
             and st.TARIFF_TREATMENT   = sq.TARIFF_TREATMENT
             and st.EFFECT_TO          = sq.EFFECT_TO
             and st.EFFECT_FROM        = sq.EFFECT_FROM
             and st.IMPORT_COUNTRY_ID  = sq.IMPORT_COUNTRY_ID
             and st.HTS                = sq.HTS
             and SVC_HTS_TT_EXC_col(i).action in (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
               update
                  set process_id        = SVC_HTS_TT_EXC_col(i).process_id,
                      chunk_id          = SVC_HTS_TT_EXC_col(i).chunk_id,
                      action            = SVC_HTS_TT_EXC_col(i).action,
                      row_seq           = SVC_HTS_TT_EXC_col(i).row_seq,
                      process$status    = SVC_HTS_TT_EXC_col(i).process$status,
                      create_id         = SVC_HTS_TT_EXC_col(i).create_id,
                      create_datetime   = SVC_HTS_TT_EXC_col(i).create_datetime,
                      last_upd_id       = SVC_HTS_TT_EXC_col(i).last_upd_id,
                      last_upd_datetime = SVC_HTS_TT_EXC_col(i).last_upd_datetime
            when NOT matched then
               insert (process_id,
                       chunk_id,
                       row_seq,
                       action ,
                       process$status,
                       origin_country_id,		
                       tariff_treatment,
                       effect_to,
                       effect_from,
                       import_country_id,
                       hts,
                       create_id,
                       create_datetime,
                       last_upd_id,
                       last_upd_datetime)
               values (SVC_HTS_TT_EXC_col(i).process_id,
                       SVC_HTS_TT_EXC_col(i).chunk_id,
                       SVC_HTS_TT_EXC_col(i).row_seq,
                       SVC_HTS_TT_EXC_col(i).action,
                       SVC_HTS_TT_EXC_col(i).process$status,
                       sq.origin_country_id,
                       sq.tariff_treatment,
                       sq.effect_to,
                       sq.effect_from,
                       sq.import_country_id,
                       sq.hts,
                       SVC_HTS_TT_EXC_col(i).create_id,
                       SVC_HTS_TT_EXC_col(i).create_datetime,
                       SVC_HTS_TT_EXC_col(i).last_upd_id,
                       SVC_HTS_TT_EXC_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                  L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_EXC_sheet,
                            SVC_HTS_TT_EXC_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_TT_EXCLUSIONS;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_TT_ZONE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                  I_process_id   IN   SVC_HTS_TARIFF_TREATMENT_ZONE.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_TT_ZONE_COL_TYP is TABLE OF SVC_HTS_TARIFF_TREATMENT_ZONE%ROWTYPE;
   SVC_HTS_TT_ZONE_col   SVC_HTS_TT_ZONE_COL_TYP := NEW SVC_HTS_TT_ZONE_COL_TYP();
   L_error               BOOLEAN                 := FALSE;
   L_temp_rec            SVC_HTS_TARIFF_TREATMENT_ZONE%ROWTYPE;
   L_process_id          SVC_HTS_TARIFF_TREATMENT_ZONE.PROCESS_ID%TYPE;
   L_default_rec         SVC_HTS_TARIFF_TREATMENT_ZONE%ROWTYPE;

   cursor C_MANDATORY_IND is
      select other_rate_mi,
             av_rate_mi,
             specific_rate_mi,
             clearing_zone_id_mi,
             tariff_treatment_mi,
             effect_to_mi,
             effect_from_mi,
             import_country_id_mi,
             hts_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key  = CORESVC_HTS_DEFINITION.template_key
                 and wksht_key = 'HTS_TARIFF_TREATMENT_ZONE')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('OTHER_RATE'        as OTHER_RATE,
                                                                'AV_RATE'           as AV_RATE,
                                                                'SPECIFIC_RATE'     as SPECIFIC_RATE,
                                                                'CLEARING_ZONE_ID'  as CLEARING_ZONE_ID,
                                                                'TARIFF_TREATMENT'  as TARIFF_TREATMENT,
                                                                'EFFECT_TO'         as EFFECT_TO,
                                                                'EFFECT_FROM'       as EFFECT_FROM,
                                                                'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                'HTS'               as HTS,
                                                                 NULL               as DUMMY));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)   := 'SVC_HTS_TARIFF_TREATMENT_ZONE';
   L_pk_columns   VARCHAR2(255)  := 'HTS,Importing Country,Effect From,Effect To,Tariff Treatment,Clearing Zone';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
   --Get default values.
   FOR rec IN (select other_rate_dv,
                      av_rate_dv,
                      specific_rate_dv,
                      clearing_zone_id_dv,
                      tariff_treatment_dv,
                      effect_to_dv,
                      effect_from_dv,
                      import_country_id_dv,
                      hts_dv,
                      NULL as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key  = 'HTS_TARIFF_TREATMENT_ZONE')
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('OTHER_RATE'        as OTHER_RATE,
                                                                             'AV_RATE'           as AV_RATE,
                                                                             'SPECIFIC_RATE'     as SPECIFIC_RATE,
                                                                             'CLEARING_ZONE_ID'  as CLEARING_ZONE_ID,
                                                                             'TARIFF_TREATMENT'  as TARIFF_TREATMENT,
                                                                             'EFFECT_TO'         as EFFECT_TO,
                                                                             'EFFECT_FROM'       as EFFECT_FROM,
                                                                             'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                             'HTS'               as HTS,
                                                                              NULL               as DUMMY)))
   LOOP
      BEGIN
         L_default_rec.other_rate := rec.other_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                            NULL,
                            'OTHER_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.av_rate := rec.av_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                            NULL,
                            'AV_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.specific_rate := rec.specific_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                            NULL,
                            'SPECIFIC_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.clearing_zone_id := rec.clearing_zone_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                            NULL,
                            'CLEARING_ZONE_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tariff_treatment := rec.tariff_treatment_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                            NULL,
                            'TARIFF_TREATMENT',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TARIFF_TREATMENT_ZONE',
                             NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(HTS_TT_ZONE$Action)                   as ACTION,
                      r.get_cell(HTS_TT_ZONE$OTHER_RATE)               as OTHER_RATE,
                      r.get_cell(HTS_TT_ZONE$AV_RATE)                  as AV_RATE,
                      r.get_cell(HTS_TT_ZONE$SPECIFIC_RATE)            as SPECIFIC_RATE,
                      UPPER(r.get_cell(HTS_TT_ZONE$CLEARING_ZONE_ID))  as CLEARING_ZONE_ID,
                      UPPER(r.get_cell(HTS_TT_ZONE$TARIFF_TREATMENT))  as TARIFF_TREATMENT,
                      r.get_cell(HTS_TT_ZONE$EFFECT_TO)                as EFFECT_TO,
                      r.get_cell(HTS_TT_ZONE$EFFECT_FROM)              as EFFECT_FROM,
                      UPPER(r.get_cell(HTS_TT_ZONE$IMPORT_COUNTRY_ID)) as IMPORT_COUNTRY_ID,
                      r.get_cell(HTS_TT_ZONE$HTS)                      as HTS,
                      r.get_row_seq()                                  as ROW_SEQ
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id    = I_file_id
                  and ss.sheet_name = SHEET_NAME_TRANS(HTS_TT_ZONE_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.other_rate := rec.other_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'OTHER_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.av_rate := rec.av_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'AV_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.specific_rate := rec.specific_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'SPECIFIC_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.clearing_zone_id := rec.clearing_zone_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'CLEARING_ZONE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tariff_treatment := rec.tariff_treatment;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'TARIFF_TREATMENT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := rec.import_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.other_rate        := NVL(L_temp_rec.other_rate,L_default_rec.other_rate);
         L_temp_rec.av_rate           := NVL(L_temp_rec.av_rate,L_default_rec.av_rate);
         L_temp_rec.specific_rate     := NVL(L_temp_rec.specific_rate,L_default_rec.specific_rate);
         L_temp_rec.clearing_zone_id  := NVL(L_temp_rec.clearing_zone_id,L_default_rec.clearing_zone_id);
         L_temp_rec.tariff_treatment  := NVL(L_temp_rec.tariff_treatment,L_default_rec.tariff_treatment);
         L_temp_rec.effect_to         := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.effect_from       := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.import_country_id := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts               := NVL(L_temp_rec.hts,L_default_rec.hts);
      end if;

      if NOT (L_temp_rec.clearing_zone_id  is NOT NULL and
              L_temp_rec.tariff_treatment  is NOT NULL and
              L_temp_rec.effect_to         is NOT NULL and
              L_temp_rec.effect_from       is NOT NULL and
              L_temp_rec.import_country_id is NOT NULL and
              L_temp_rec.hts               is NOT NULL and
              1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_TT_ZONE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      
      if NOT L_error then
         SVC_HTS_TT_ZONE_col.EXTEND();
         SVC_HTS_TT_ZONE_col(SVC_HTS_TT_ZONE_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_TT_ZONE_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts_tariff_treatment_zone st
         using (select (case
                           when L_mi_rec.OTHER_RATE_mi        = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.OTHER_RATE        is NULL      then
                                mt.OTHER_RATE
                           else s1.OTHER_RATE
                           end) as OTHER_RATE,
                       (case
                           when L_mi_rec.AV_RATE_mi           = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.AV_RATE           is NULL      then
                                mt.AV_RATE
                           else s1.AV_RATE
                           end) as AV_RATE,
                       (case 
                           when L_mi_rec.SPECIFIC_RATE_mi     = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.SPECIFIC_RATE     is NULL      then
                                mt.SPECIFIC_RATE
                           else s1.SPECIFIC_RATE
                           end) as SPECIFIC_RATE,
                       (case
                           when L_mi_rec.CLEARING_ZONE_ID_mi  = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.CLEARING_ZONE_ID  is NULL      then
                                mt.CLEARING_ZONE_ID
                           else s1.CLEARING_ZONE_ID
                           end) as CLEARING_ZONE_ID,
                       (case
                           when L_mi_rec.TARIFF_TREATMENT_mi  = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TARIFF_TREATMENT  is NULL      then
                                mt.TARIFF_TREATMENT
                           else s1.TARIFF_TREATMENT
                           end) as TARIFF_TREATMENT,
                       (case
                           when L_mi_rec.EFFECT_TO_mi         = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_TO         is NULL      then
                                mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi       = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_FROM       is NULL      then
                                mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.IMPORT_COUNTRY_ID is NULL     then
                                mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.HTS_mi               = 'N' and
                                SVC_HTS_TT_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.HTS               is NULL     then
                                mt.HTS
                           else s1.HTS
                           end) as HTS,
                           null as dummy
                  from (select SVC_HTS_TT_ZONE_col(i).OTHER_RATE        as OTHER_RATE,
                               SVC_HTS_TT_ZONE_col(i).AV_RATE           as AV_RATE,
                               SVC_HTS_TT_ZONE_col(i).SPECIFIC_RATE     as SPECIFIC_RATE,
                               SVC_HTS_TT_ZONE_col(i).CLEARING_ZONE_ID  as CLEARING_ZONE_ID,
                               SVC_HTS_TT_ZONE_col(i).TARIFF_TREATMENT  as TARIFF_TREATMENT,
                               SVC_HTS_TT_ZONE_col(i).EFFECT_TO         as EFFECT_TO,
                               SVC_HTS_TT_ZONE_col(i).EFFECT_FROM       as EFFECT_FROM,
                               SVC_HTS_TT_ZONE_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                               SVC_HTS_TT_ZONE_col(i).HTS               as HTS,
                               null                                     as DUMMY
                          from dual) s1,
                         hts_tariff_treatment_zone mt
                   where mt.CLEARING_ZONE_ID (+)   = s1.CLEARING_ZONE_ID
                     and mt.TARIFF_TREATMENT (+)   = s1.TARIFF_TREATMENT
                     and mt.EFFECT_TO (+)          = s1.EFFECT_TO
                     and mt.EFFECT_FROM (+)        = s1.EFFECT_FROM
                     and mt.IMPORT_COUNTRY_ID (+)  = s1.IMPORT_COUNTRY_ID
                     and mt.HTS (+)                = s1.HTS
                     and 1 = 1) sq
            on (st.CLEARING_ZONE_ID  = sq.CLEARING_ZONE_ID
            and st.TARIFF_TREATMENT  = sq.TARIFF_TREATMENT
            and st.EFFECT_TO         = sq.EFFECT_TO
            and st.EFFECT_FROM       = sq.EFFECT_FROM
            and st.IMPORT_COUNTRY_ID = sq.IMPORT_COUNTRY_ID 
            and st.HTS               = sq.HTS
            and SVC_HTS_TT_ZONE_col(i).action in (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
               update
                  set process_id        = SVC_HTS_TT_ZONE_col(i).process_id,
                      chunk_id          = SVC_HTS_TT_ZONE_col(i).chunk_id,
                      row_seq           = SVC_HTS_TT_ZONE_col(i).row_seq,
                      action            = SVC_HTS_TT_ZONE_col(i).action,
                      process$status    = SVC_HTS_TT_ZONE_col(i).process$status,
                      other_rate        = sq.other_rate,
                      av_rate           = sq.av_rate,
                      specific_rate     = sq.specific_rate,
                      create_id         = SVC_HTS_TT_ZONE_col(i).create_id,
                      create_datetime   = SVC_HTS_TT_ZONE_col(i).create_datetime,
                      last_upd_id       = SVC_HTS_TT_ZONE_col(i).last_upd_id,
                      last_upd_datetime = SVC_HTS_TT_ZONE_col(i).last_upd_datetime
            when NOT matched then
               insert(process_id,
                      chunk_id,
                      row_seq,
                      action,	
                      process$status,
                      other_rate,
                      av_rate,
                      specific_rate,
                      clearing_zone_id,
                      tariff_treatment,
                      effect_to,
                      effect_from,
                      import_country_id,
                      hts,
                      create_id,
                      create_datetime,
                      last_upd_id,
                      last_upd_datetime)
               values(SVC_HTS_TT_ZONE_col(i).process_id,
                      SVC_HTS_TT_ZONE_col(i).chunk_id,
                      SVC_HTS_TT_ZONE_col(i).row_seq,
                      SVC_HTS_TT_ZONE_col(i).action,
                      SVC_HTS_TT_ZONE_col(i).process$status,
                      sq.other_rate,
                      sq.av_rate,
                      sq.specific_rate,
                      sq.clearing_zone_id,
                      sq.tariff_treatment,
                      sq.effect_to,
                      sq.effect_from,
                      sq.import_country_id,
                      sq.hts,
                      SVC_HTS_TT_ZONE_col(i).create_id,
                      SVC_HTS_TT_ZONE_col(i).create_datetime,
                      SVC_HTS_TT_ZONE_col(i).last_upd_id,
                      SVC_HTS_TT_ZONE_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg  := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TT_ZONE_sheet,
                            SVC_HTS_TT_ZONE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_TT_ZONE;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_TAX(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                              I_process_id   IN   SVC_HTS_TAX.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_TAX_COL_TYP is TABLE OF SVC_HTS_TAX%ROWTYPE;
   L_temp_rec        SVC_HTS_TAX%ROWTYPE;
   SVC_HTS_TAX_col   SVC_HTS_TAX_COL_TYP := NEW SVC_HTS_TAX_COL_TYP();
   L_process_id      SVC_HTS_TAX.PROCESS_ID%TYPE;
   L_error           BOOLEAN := FALSE;
   L_default_rec     SVC_HTS_TAX%ROWTYPE;

   cursor C_MANDATORY_IND IS
      select HTS_mi,
             IMPORT_COUNTRY_ID_mi,
             EFFECT_FROM_mi,
             EFFECT_TO_mi,
             TAX_TYPE_mi,
             TAX_COMP_CODE_mi,
             TAX_SPECIFIC_RATE_mi,
             TAX_AV_RATE_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key       = CORESVC_HTS_DEFINITION.template_key
                 and wksht_key          = 'HTS_TAX')
               PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('HTS'               as HTS,
                                                                'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                'EFFECT_FROM'       as EFFECT_FROM,
                                                                'EFFECT_TO'         as EFFECT_TO,
                                                                'TAX_TYPE'          as TAX_TYPE,
                                                                'TAX_COMP_CODE'     as TAX_COMP_CODE,
                                                                'TAX_SPECIFIC_RATE' as TAX_SPECIFIC_RATE,
                                                                'TAX_AV_RATE'       as TAX_AV_RATE,
                                                                 NULL               as dummy));
   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HTS_TAX';
   L_pk_columns   VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect To,Tax Type';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
   
BEGIN
   --Get default values.
   FOR rec IN (select HTS_dv,
                      IMPORT_COUNTRY_ID_dv,
                      EFFECT_FROM_dv,
                      EFFECT_TO_dv,
                      TAX_TYPE_dv,
                      TAX_COMP_CODE_dv,
                      TAX_SPECIFIC_RATE_dv,
                      TAX_AV_RATE_dv,
                      null as dummy
                from (select column_key,
                             default_value
                        from s9t_tmpL_cols_def
                       where template_key         = CORESVC_HTS_DEFINITION.template_key
                         and wksht_key            = 'HTS_TAX') 
                PIVOT (MAX(default_value) AS dv FOR (column_key) IN('HTS'                as HTS,
                                                                    'IMPORT_COUNTRY_ID'  as IMPORT_COUNTRY_ID,
                                                                    'EFFECT_FROM'        as EFFECT_FROM,
                                                                    'EFFECT_TO'          as EFFECT_TO,
                                                                    'TAX_TYPE'           as TAX_TYPE,
                                                                    'TAX_COMP_CODE'      as TAX_COMP_CODE,
                                                                    'TAX_SPECIFIC_RATE'  as TAX_SPECIFIC_RATE,
                                                                    'TAX_AV_RATE'        as TAX_AV_RATE,
                                                                     NULL                as dummy)))
   LOOP
      BEGIN
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from := rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tax_type := rec.tax_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX',
                            NULL,
                            'TAX_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tax_comp_code := rec.tax_comp_code_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX',
                            NULL,
                            'TAX_COMP_CODE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tax_specific_rate := rec.tax_specific_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX',
                            NULL,
                            'TAX_SPECIFIC_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tax_av_rate := rec.tax_av_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX',
                            NULL,
                            'TAX_AV_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select UPPER(r.get_cell(HTS_TAX$Action))             as ACTION,
                     r.get_cell(HTS_TAX$HTS)                       as HTS,
                     UPPER(r.get_cell(HTS_TAX$IMPORT_COUNTRY_ID))  as IMPORT_COUNTRY_ID,
                     r.get_cell(HTS_TAX$EFFECT_FROM)               as EFFECT_FROM,
                     r.get_cell(HTS_TAX$EFFECT_TO)                 as EFFECT_TO,
                     UPPER(r.get_cell(HTS_TAX$TAX_TYPE))           as TAX_TYPE,
                     UPPER(r.get_cell(HTS_TAX$TAX_COMP_CODE))      as TAX_COMP_CODE,
                     r.get_cell(HTS_TAX$TAX_SPECIFIC_RATE)         as TAX_SPECIFIC_RATE,
                     r.get_cell(HTS_TAX$TAX_AV_RATE)               as TAX_AV_RATE,
                     r.get_row_seq()                               as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(HTS_TAX_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := UPPER(rec.import_country_id);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from := TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tax_type := UPPER(rec.tax_type);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            'TAX_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tax_comp_code := rec.tax_comp_code;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            'TAX_COMP_CODE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tax_specific_rate := rec.tax_specific_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            'TAX_SPECIFIC_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tax_av_rate := rec.tax_av_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            rec.row_seq,
                            'TAX_AV_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.hts                    := NVL(L_temp_rec.hts,L_default_rec.hts);
         L_temp_rec.import_country_id      := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.effect_from            := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.effect_to              := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.tax_type               := NVL(L_temp_rec.tax_type,L_default_rec.tax_type);
         L_temp_rec.tax_comp_code          := NVL(L_temp_rec.tax_comp_code,L_default_rec.tax_comp_code);
         L_temp_rec.tax_specific_rate      := NVL(L_temp_rec.tax_specific_rate,L_default_rec.tax_specific_rate);
         L_temp_rec.tax_av_rate            := NVL(L_temp_rec.tax_av_rate,L_default_rec.tax_av_rate);
      end if;

      if NOT ( L_temp_rec.tax_type          is NOT NULL
           and L_temp_rec.effect_to         is NOT NULL
           and L_temp_rec.effect_from       is NOT NULL
           and L_temp_rec.import_country_id is NOT NULL
           and L_temp_rec.hts               is NOT NULL
           and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_TAX_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         SVC_HTS_TAX_col.EXTEND();
         SVC_HTS_TAX_col(SVC_HTS_TAX_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_TAX_col.count SAVE EXCEPTIONS 
         merge into svc_hts_tax st
         using (select (case
                           when L_mi_rec.HTS_mi = 'N'               and
                                SVC_HTS_TAX_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.HTS is NULL                then
                                mt.HTS
                           else s1.HTS
                           end) as HTS,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi = 'N' and
                                SVC_HTS_TAX_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.IMPORT_COUNTRY_ID is NULL  then
                                mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi = 'N'       and
                                SVC_HTS_TAX_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_FROM is NULL        then
                                mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.EFFECT_TO_mi = 'N'         and
                                SVC_HTS_TAX_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_TO is NULL          then
                                mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.TAX_TYPE_mi = 'N'          and
                                SVC_HTS_TAX_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TAX_TYPE is NULL           then
                                mt.TAX_TYPE
                           else s1.TAX_TYPE
                           end) as TAX_TYPE,
                       (case
                           when L_mi_rec.TAX_COMP_CODE_mi = 'N'     and
                                SVC_HTS_TAX_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TAX_COMP_CODE is NULL      then
                                mt.TAX_COMP_CODE
                           else s1.TAX_COMP_CODE
                           end) as TAX_COMP_CODE,
                       (case
                           when L_mi_rec.TAX_SPECIFIC_RATE_mi = 'N' and
                                SVC_HTS_TAX_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TAX_SPECIFIC_RATE is NULL  then
                                mt.TAX_SPECIFIC_RATE
                           else s1.TAX_SPECIFIC_RATE
                           end) as TAX_SPECIFIC_RATE,
                       (case
                           when L_mi_rec.TAX_AV_RATE_mi = 'N'       and
                                SVC_HTS_TAX_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TAX_AV_RATE is NULL        then
                                mt.TAX_AV_RATE
                           else s1.TAX_AV_RATE
                           end) as TAX_AV_RATE,
                           null as dummy
                  from (select svc_hts_tax_col(i).hts               as hts,
                               svc_hts_tax_col(i).import_country_id as import_country_id,
                               svc_hts_tax_col(i).effect_from       as effect_from,
                               svc_hts_tax_col(i).effect_to         as effect_to,
                               svc_hts_tax_col(i).tax_type          as tax_type,
                               svc_hts_tax_col(i).tax_comp_code     as tax_comp_code,
                               svc_hts_tax_col(i).tax_specific_rate as tax_specific_rate,
                               svc_hts_tax_col(i).tax_av_rate       as tax_av_rate,
                               NULL                                 as dummy
                          from dual)     s1,
                        hts_tax   mt
                 where mt.TAX_TYPE (+)          = s1.TAX_TYPE
                   and mt.EFFECT_TO (+)         = s1.EFFECT_TO
                   and mt.EFFECT_FROM (+)       = s1.EFFECT_FROM
                   and mt.IMPORT_COUNTRY_ID (+) = s1.IMPORT_COUNTRY_ID
                   and mt.HTS (+)               = s1.HTS
                   and 1 = 1)  sq
            on (st.TAX_TYPE          = sq.TAX_TYPE
            and st.EFFECT_TO         = sq.EFFECT_TO
            and st.EFFECT_FROM       = sq.EFFECT_FROM
            and st.IMPORT_COUNTRY_ID = sq.IMPORT_COUNTRY_ID
            and st.HTS               = sq.HTS 
            and SVC_HTS_TAX_col(i).action in (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
            when matched then
               update
                  set process_id        = SVC_HTS_TAX_col(i).process_id,
                      chunk_id          = SVC_HTS_TAX_col(i).chunk_id,
                      row_seq           = SVC_HTS_TAX_col(i).row_seq,
                      action            = SVC_HTS_TAX_col(i).action,
                      process$status    = SVC_HTS_TAX_col(i).process$status,
                      tax_comp_code     = sq.tax_comp_code,
                      tax_av_rate       = sq.tax_av_rate,
                      tax_specific_rate = sq.tax_specific_rate,
                      create_id         = SVC_HTS_TAX_col(i).create_id,
                      create_datetime   = SVC_HTS_TAX_col(i).create_datetime,
                      last_upd_id       = SVC_HTS_TAX_col(i).last_upd_id,
                      last_upd_datetime = SVC_HTS_TAX_col(i).last_upd_datetime
            when NOT matched then
               insert (process_id,
                       chunk_id,
                       row_seq,
                       action,
                       process$status,
                       hts,
                       import_country_id,
                       effect_from,
                       effect_to,
                       tax_type,
                       tax_comp_code,
                       tax_specific_rate,
                       tax_av_rate,
                       create_id,
                       create_datetime,
                       last_upd_id,
                       last_upd_datetime)
               values (SVC_HTS_TAX_col(i).process_id,
                       SVC_HTS_TAX_col(i).chunk_id,
                       SVC_HTS_TAX_col(i).row_seq,
                       SVC_HTS_TAX_col(i).action,
                       SVC_HTS_TAX_col(i).process$status,
                       sq.hts,
                       sq.import_country_id,
                       sq.effect_from,
                       sq.effect_to,
                       sq.tax_type,
                       sq.tax_comp_code,
                       sq.tax_specific_rate,
                       sq.tax_av_rate,
                       SVC_HTS_TAX_col(i).create_id,
                       SVC_HTS_TAX_col(i).create_datetime,
                       SVC_HTS_TAX_col(i).last_upd_id,
                       SVC_HTS_TAX_col(i).last_upd_datetime);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.count LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_sheet,
                            SVC_HTS_TAX_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_TAX;
-----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_HTS_TAX_ZONE(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                   I_process_id   IN   SVC_HTS_TAX_ZONE.PROCESS_ID%TYPE) IS

   TYPE SVC_HTS_TAX_ZONE_COL_TYP is TABLE OF SVC_HTS_TAX_ZONE%ROWTYPE;
   L_temp_rec             SVC_HTS_TAX_ZONE%ROWTYPE;
   SVC_HTS_TAX_ZONE_col   SVC_HTS_TAX_ZONE_COL_TYP := NEW SVC_HTS_TAX_ZONE_COL_TYP();
   L_process_id           SVC_HTS_TAX_ZONE.PROCESS_ID%TYPE;
   L_error                BOOLEAN := FALSE;
   L_default_rec          SVC_HTS_TAX_ZONE%ROWTYPE;

   cursor C_MANDATORY_IND IS
      select tax_av_rate_mi,
             tax_specific_rate_mi,
             clearing_zone_id_mi,
             tax_type_mi,
             effect_to_mi,
             effect_from_mi,
             import_country_id_mi,
             hts_mi,
             1 as dummy
       from (select column_key,
                    mandatory
               from s9t_tmpl_cols_def
              where template_key  = CORESVC_HTS_DEFINITION.template_key
                and wksht_key     = 'HTS_TAX_ZONE')
              PIVOT (MAX(mandatory) as mi FOR (column_key) IN ('TAX_AV_RATE'       as TAX_AV_RATE,
                                                               'TAX_SPECIFIC_RATE' as TAX_SPECIFIC_RATE,
                                                               'CLEARING_ZONE_ID'  as CLEARING_ZONE_ID,
                                                               'TAX_TYPE'          as TAX_TYPE,
                                                               'EFFECT_TO'         as EFFECT_TO,
                                                               'EFFECT_FROM'       as EFFECT_FROM,
                                                               'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                               'HTS'               as HTS,
                                                                NULL               as dummy));

   L_mi_rec       C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS     EXCEPTION;
   PRAGMA         EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table        VARCHAR2(30)  := 'SVC_HTS_TAX_ZONE';
   L_pk_columns   VARCHAR2(255) := 'HTS,Importing Country,Effect From,Effect To,Tax Type,Clearing Zone';
   L_error_code   NUMBER;
   L_error_msg    RTK_ERRORS.RTK_TEXT%TYPE;
   
BEGIN
   --Get default values.
   FOR rec IN (select TAX_AV_RATE_dv,
                      TAX_SPECIFIC_RATE_dv,
                      CLEARING_ZONE_ID_dv,
                      TAX_TYPE_dv,
                      EFFECT_TO_dv,
                      EFFECT_FROM_dv,
                      IMPORT_COUNTRY_ID_dv,
                      HTS_dv,
                      NULL as dummy
                 from (select column_key,
                                 default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_HTS_DEFINITION.template_key
                          and wksht_key     = 'HTS_TAX_ZONE') 
                        PIVOT (MAX(default_value) as dv FOR (column_key) IN ('TAX_AV_RATE'       as TAX_AV_RATE,
                                                                             'TAX_SPECIFIC_RATE' as TAX_SPECIFIC_RATE,
                                                                             'CLEARING_ZONE_ID'  as CLEARING_ZONE_ID,
                                                                             'TAX_TYPE'          as TAX_TYPE,
                                                                             'EFFECT_TO'         as EFFECT_TO,
                                                                             'EFFECT_FROM'       as EFFECT_FROM,
                                                                             'IMPORT_COUNTRY_ID' as IMPORT_COUNTRY_ID,
                                                                             'HTS'               as HTS,
                                                                              NULL               as dummy)))
   LOOP
      BEGIN
         L_default_rec.tax_av_rate := rec.tax_av_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX_ZONE',
                            NULL,
                            'TAX_AV_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tax_specific_rate := rec.tax_specific_rate_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX_ZONE',
                            NULL,
                            'TAX_SPECIFIC_RATE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.clearing_zone_id := rec.clearing_zone_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX_ZONE',
                            NULL,
                            'CLEARING_ZONE_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tax_type := rec.tax_type_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX_ZONE',
                            NULL,
                            'TAX_TYPE',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_to := rec.effect_to_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX_ZONE',
                            NULL,
                            'EFFECT_TO',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.effect_from:= rec.effect_from_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX_ZONE',
                            NULL,
                            'EFFECT_FROM',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.import_country_id := rec.import_country_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX_ZONE',
                            NULL,
                            'IMPORT_COUNTRY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN  
         L_default_rec.hts := rec.hts_dv;
      EXCEPTION  
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'HTS_TAX_ZONE',
                            NULL,
                            'HTS',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into  L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(HTS_TAX_ZONE$Action))            as ACTION,
                      r.get_cell(HTS_TAX_ZONE$TAX_AV_RATE)              as TAX_AV_RATE,
                      r.get_cell(HTS_TAX_ZONE$TAX_SPECIFIC_RATE)        as TAX_SPECIFIC_RATE,
                      UPPER(r.get_cell(HTS_TAX_ZONE$CLEARING_ZONE_ID))  as CLEARING_ZONE_ID,
                      UPPER(r.get_cell(HTS_TAX_ZONE$TAX_TYPE))          as TAX_TYPE,
                      r.get_cell(HTS_TAX_ZONE$EFFECT_TO)                as EFFECT_TO,
                      r.get_cell(HTS_TAX_ZONE$EFFECT_FROM)              as EFFECT_FROM,
                      UPPER(r.get_cell(HTS_TAX_ZONE$IMPORT_COUNTRY_ID)) as IMPORT_COUNTRY_ID,
                      r.get_cell(HTS_TAX_ZONE$HTS)                      as HTS,
                      r.get_row_seq()                                   as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(HTS_TAX_ZONE_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;

      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tax_av_rate := rec.tax_av_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            'TAX_AV_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tax_specific_rate := rec.tax_specific_rate;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            'TAX_SPECIFIC_RATE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.clearing_zone_id := UPPER(rec.clearing_zone_id);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            'CLEARING_ZONE_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tax_type := UPPER(rec.tax_type);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            'TAX_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_to := TO_DATE(rec.effect_to,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            'EFFECT_TO',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.effect_from:= TO_DATE(rec.effect_from,'DD-MM-YY');
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            'EFFECT_FROM',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.import_country_id := UPPER(rec.import_country_id);
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            'IMPORT_COUNTRY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.hts := rec.hts;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            rec.row_seq,
                            'HTS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_HTS_DEFINITION.action_new then
         L_temp_rec.tax_av_rate         := NVL(L_temp_rec.tax_av_rate,L_default_rec.tax_av_rate);
         L_temp_rec.tax_specific_rate   := NVL(L_temp_rec.tax_specific_rate,L_default_rec.tax_specific_rate);
         L_temp_rec.clearing_zone_id    := NVL(L_temp_rec.clearing_zone_id,L_default_rec.clearing_zone_id);
         L_temp_rec.tax_type            := NVL(L_temp_rec.tax_type,L_default_rec.tax_type);
         L_temp_rec.effect_to           := NVL(L_temp_rec.effect_to,L_default_rec.effect_to);
         L_temp_rec.effect_from         := NVL(L_temp_rec.effect_from,L_default_rec.effect_from);
         L_temp_rec.import_country_id   := NVL(L_temp_rec.import_country_id,L_default_rec.import_country_id);
         L_temp_rec.hts                 := NVL(L_temp_rec.hts,L_default_rec.hts);
      end if;

      if NOT (L_temp_rec.CLEARING_ZONE_ID  is NOT NULL
          and L_temp_rec.TAX_TYPE          is NOT NULL
          and L_temp_rec.EFFECT_TO         is NOT NULL
          and L_temp_rec.EFFECT_FROM       is NOT NULL
          and L_temp_rec.IMPORT_COUNTRY_ID is NOT NULL
          and L_temp_rec.HTS               is NOT NULL
          and 1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         HTS_TAX_ZONE_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         SVC_HTS_TAX_ZONE_col.EXTEND();
         SVC_HTS_TAX_ZONE_col(SVC_HTS_TAX_ZONE_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..SVC_HTS_TAX_ZONE_col.COUNT SAVE EXCEPTIONS
         merge into svc_hts_tax_zone st
         using (select (case
                           when L_mi_rec.TAX_AV_RATE_mi = 'N' and
                                SVC_HTS_TAX_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TAX_AV_RATE is NULL  then
                                mt.TAX_AV_RATE
                           else s1.TAX_AV_RATE
                           end) as TAX_AV_RATE,
                       (case
                           when L_mi_rec.TAX_SPECIFIC_RATE_mi = 'N'  and
                                SVC_HTS_TAX_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TAX_SPECIFIC_RATE is NULL then
                                mt.TAX_SPECIFIC_RATE
                           else s1.TAX_SPECIFIC_RATE
                           end) as TAX_SPECIFIC_RATE,
                       (case
                           when L_mi_rec.CLEARING_ZONE_ID_mi = 'N'  and
                                SVC_HTS_TAX_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.CLEARING_ZONE_ID is NULL then
                                mt.CLEARING_ZONE_ID
                           else s1.CLEARING_ZONE_ID
                           end) as CLEARING_ZONE_ID,
                       (case
                           when L_mi_rec.TAX_TYPE_mi = 'N' and
                                SVC_HTS_TAX_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.TAX_TYPE is NULL then
                                mt.TAX_TYPE
                           else s1.TAX_TYPE
                           end) as TAX_TYPE,
                       (case
                           when L_mi_rec.EFFECT_TO_mi = 'N' and
                                SVC_HTS_TAX_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_TO is NULL then
                                mt.EFFECT_TO
                           else s1.EFFECT_TO
                           end) as EFFECT_TO,
                       (case
                           when L_mi_rec.EFFECT_FROM_mi = 'N' and
                                SVC_HTS_TAX_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.EFFECT_FROM is NULL then
                                mt.EFFECT_FROM
                           else s1.EFFECT_FROM
                           end) as EFFECT_FROM,
                       (case
                           when L_mi_rec.IMPORT_COUNTRY_ID_mi = 'N' and
                                SVC_HTS_TAX_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.IMPORT_COUNTRY_ID is NULL then
                                mt.IMPORT_COUNTRY_ID
                           else s1.IMPORT_COUNTRY_ID
                           end) as IMPORT_COUNTRY_ID,
                       (case
                           when L_mi_rec.HTS_mi = 'N' and
                                SVC_HTS_TAX_ZONE_col(i).action = CORESVC_HTS_DEFINITION.action_mod and
                                s1.HTS is NULL then
                                mt.HTS
                           else s1.HTS
                           end) as HTS,
                         NULL as dummy
                  from (select SVC_HTS_TAX_ZONE_col(i).TAX_AV_RATE       as TAX_AV_RATE,
                               SVC_HTS_TAX_ZONE_col(i).TAX_SPECIFIC_RATE as TAX_SPECIFIC_RATE,
                               SVC_HTS_TAX_ZONE_col(i).CLEARING_ZONE_ID  as CLEARING_ZONE_ID,
                               SVC_HTS_TAX_ZONE_col(i).TAX_TYPE          as TAX_TYPE,
                               SVC_HTS_TAX_ZONE_col(i).EFFECT_TO         as EFFECT_TO,
                               SVC_HTS_TAX_ZONE_col(i).EFFECT_FROM       as EFFECT_FROM,
                               SVC_HTS_TAX_ZONE_col(i).IMPORT_COUNTRY_ID as IMPORT_COUNTRY_ID,
                               SVC_HTS_TAX_ZONE_col(i).HTS               as HTS,
                               NULL as dummy
                          from dual) s1,
                        hts_tax_zone mt
                 where mt.CLEARING_ZONE_ID (+)     = s1.CLEARING_ZONE_ID
                   and mt.TAX_TYPE (+)             = s1.TAX_TYPE
                   and mt.EFFECT_TO (+)            = s1.EFFECT_TO
                   and mt.EFFECT_FROM(+)           = s1.EFFECT_FROM
                   and mt.IMPORT_COUNTRY_ID (+)    = s1.IMPORT_COUNTRY_ID
                   and mt.HTS (+) = s1.HTS
                   and 1 = 1) sq
               on (st.CLEARING_ZONE_ID   = sq.CLEARING_ZONE_ID and
                   st.TAX_TYPE           = sq.TAX_TYPE and
                   st.EFFECT_TO          = sq.EFFECT_TO and
                   st.EFFECT_FROM        = sq.EFFECT_FROM and
                   st.IMPORT_COUNTRY_ID  = sq.IMPORT_COUNTRY_ID and
                   st.HTS                = sq.HTS and
                   SVC_HTS_TAX_ZONE_col(i).action in (CORESVC_HTS_DEFINITION.action_mod,CORESVC_HTS_DEFINITION.action_del))
         when matched then
            update
               set process_id        = SVC_HTS_TAX_ZONE_col(i).process_id,
                   chunk_id          = SVC_HTS_TAX_ZONE_col(i).chunk_id,
                   row_seq           = SVC_HTS_TAX_ZONE_col(i).row_seq,
                   action            = SVC_HTS_TAX_ZONE_col(i).action,
                   process$status    = SVC_HTS_TAX_ZONE_col(i).process$status,
                   tax_av_rate       = sq.tax_av_rate,
                   tax_specific_rate = sq.tax_specific_rate,
                   create_id         = SVC_HTS_TAX_ZONE_col(i).create_id,
                   create_datetime   = SVC_HTS_TAX_ZONE_col(i).create_datetime,
                   last_upd_id       = SVC_HTS_TAX_ZONE_col(i).last_upd_id,
                   last_upd_datetime = SVC_HTS_TAX_ZONE_col(i).last_upd_datetime
         when NOT matched then
            insert (process_id,
                    chunk_id,
                    row_seq,
                    action,
                    process$status,
                    tax_av_rate,
                    tax_specific_rate,
                    clearing_zone_id,
                    tax_type,
                    effect_to,
                    effect_from,
                    import_country_id,
                    hts,
                    create_id,
                    create_datetime,
                    last_upd_id,
                    last_upd_datetime)
            values (SVC_HTS_TAX_ZONE_col(i).process_id,
                    SVC_HTS_TAX_ZONE_col(i).chunk_id,
                    SVC_HTS_TAX_ZONE_col(i).row_seq,
                    SVC_HTS_TAX_ZONE_col(i).action,
                    SVC_HTS_TAX_ZONE_col(i).process$status,
                    sq.tax_av_rate,
                    sq.tax_specific_rate,
                    sq.clearing_zone_id,
                    sq.tax_type,
                    sq.effect_to,
                    sq.effect_from,
                    sq.import_country_id,
                    sq.hts,
                    SVC_HTS_TAX_ZONE_col(i).create_id,
                    SVC_HTS_TAX_ZONE_col(i).create_datetime,
                    SVC_HTS_TAX_ZONE_col(i).last_upd_id,
                    SVC_HTS_TAX_ZONE_col(i).last_upd_datetime);
   EXCEPTION
      when DML_errors then
         FOR i IN 1..sql%bulk_exceptions.COUNT LOOP
            L_error_code := sql%bulk_exceptions(i).error_code;
            if L_error_code = 1 then
               L_error_code := NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR(I_file_id,
                            HTS_TAX_ZONE_sheet,
                            SVC_HTS_TAX_ZONE_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_HTS_TAX_ZONE;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_COUNT     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.NAMES_MAP_TYP;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR           EXCEPTION;
   PRAGMA             EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_HTS(I_file_id,I_process_id);
      PROCESS_S9T_HTS_TL(I_file_id,I_process_id);
      PROCESS_S9T_HTT(I_file_id,I_process_id);
      PROCESS_S9T_HTS_TT_EXCLUSIONS(I_file_id,I_process_id);
      PROCESS_S9T_HTS_TT_ZONE(I_file_id,I_process_id);
      PROCESS_S9T_HTS_FEE(I_file_id,I_process_id);
      PROCESS_S9T_HTS_FEE_ZONE(I_file_id,I_process_id);
      PROCESS_S9T_HTS_TAX(I_file_id,I_process_id);
      PROCESS_S9T_HTS_TAX_ZONE(I_file_id,I_process_id);
      PROCESS_S9T_HTS_AD(I_file_id,I_process_id);
      PROCESS_S9T_HTS_CVD(I_file_id,I_process_id);
      PROCESS_S9T_HTS_REF(I_file_id,I_process_id);
      PROCESS_S9T_HTS_OGA(I_file_id,I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.COUNT();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status     = 'PE',
             file_id    = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR',
                                            NULL,
                                            NULL,
                                            NULL);
      LP_s9t_errors_tab := NEW LP_s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'EXCEEDS_4000_CHAR');
      O_error_count := LP_s9t_errors_tab.COUNT();
      FORALL i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status = 'PE',
             file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
-----------------------------------------------------------------------------------------------
--This function checks that the input data range (effect_from to effect_to) do not conflict 
--with the existing date ranges.
FUNCTION VALIDATE_DATE_RANGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exists          IN OUT   BOOLEAN,
                             I_rec             IN       C_SVC_HTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.VALIDATE_DATE_RANGE';
   L_exists    VARCHAR2(1);

   cursor C_DATE_RANGE is
      select 'x'
        from dual
       where exists(select 'x'
                      from hts
                     where hts.import_country_id = I_rec.import_country_id
                       and hts.hts = I_rec.hts
                       and (I_rec.effect_from between hts.effect_from and hts.effect_to
                            or I_rec.effect_to between hts.effect_from and hts.effect_to
                            or (hts.effect_from > I_rec.effect_from and I_rec.effect_to > hts.effect_to)));

BEGIN
   open C_DATE_RANGE;
   fetch C_DATE_RANGE into L_exists;
   if C_DATE_RANGE%FOUND then
      close C_DATE_RANGE;
      O_exists := FALSE;
      O_error_message := 'DATE_RANGE_CONFLICT';
   else
      close C_DATE_RANGE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS THEN
      if C_DATE_RANGE%ISOPEN then
         close C_DATE_RANGE;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_DATE_RANGE;
-----------------------------------------------------------------------------------------------
--This function checks that the hts follows the format mask specified in the
--HTS_IMPORT_COUNTRY_SETUP for the input import country.
FUNCTION VALIDATE_MASK(O_error_message     IN OUT   RTK_ERRORS.RTK_KEY%TYPE,
                       O_format            IN OUT   BOOLEAN,
                       I_rec               IN       C_SVC_HTS%ROWTYPE,
                       I_hts_format_mask   IN       HTS_IMPORT_COUNTRY_SETUP.HTS_FORMAT_MASK%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR(64)  := 'CORESVC_HTS_DEFINITION.VALIDATE_MASK';
   L_fmt_length   NUMBER(2)    := 0;
   L_hts          HTS.HTS%TYPE;
   L_position     NUMBER(2)    := 0;
   L_count        NUMBER(2)    := 0;
   L_hts_length   NUMBER(2)    := 0;

BEGIN
   L_fmt_length := LENGTH(I_hts_format_mask);
   L_hts := I_rec.hts;
   LOOP
      if SUBSTR(I_hts_format_mask,
                L_position,
                1) IN ('X') then
         L_count := L_count+1;
      end if;
      L_position := L_position + 1;
      if L_position > L_fmt_length then
         EXIT;
      end if;
   END LOOP;
   L_hts_length := LENGTH(L_hts);
   if NOT (L_hts_length <= L_count) then
      O_format := FALSE;
      O_error_message := 'HTS_INV_FRMT';
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_MASK;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_error           IN OUT   BOOLEAN,
                         O_chapter         IN OUT   HTS.CHAPTER%TYPE,
                         I_rec             IN OUT   C_SVC_HTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_VAL';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS';
   L_mode               VARCHAR2(4)                       := 'EDIT';
   L_hts                HTS.HTS%TYPE                      := I_rec.hts;
   L_date_range_exists  BOOLEAN                           := TRUE;
   L_format             BOOLEAN                           := TRUE;
   L_country_desc       COUNTRY.COUNTRY_DESC%TYPE         := NULL;
   L_hts_chapter_desc   HTS_CHAPTER.CHAPTER_DESC%TYPE;
   L_exists             BOOLEAN;

BEGIN
   --checks that the import country is valid
   if I_rec.action IN (action_new, action_mod, action_del) and
      I_rec.import_country_id is NOT NULL  and
      I_rec.cntry_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'IMPORT_COUNTRY_ID',
                  'INV_COUNTRY');
      O_error := TRUE;
   end if;
   --check that the hts follows the format mask as specified in the
   --HTS_IMPORT_COUNTRY_SETUP for the input import country
   if I_rec.action = action_new
      and I_rec.cntry_rid is NOT NULL then
       --check for mask here
      if LP_hts_format_mask is NOT NULL then
         if VALIDATE_MASK(O_error_message,
                          L_format,
                          I_rec,
                          LP_hts_format_mask) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'HTS',
                        O_error_message);
            O_error := TRUE;
         end if;
         if NOT L_format then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'HTS',
                        O_error_message) ;
            O_error := TRUE;
         end if;
      end if;
   end if;

   --checks that the chapter is present for the input import country and HTS
   if I_rec.action = action_new
      and I_rec.cntry_rid is NOT NULL then
      if LP_hts_heading_length = 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'NO_HTS_HEADING_LEN_SETUP');
         O_error := TRUE;
      else
         --checks that the input hts contains only numeric characters
         if I_rec.hts is NOT NULL then
            if TRANSLATE(I_rec.hts,'A1234567890', 'A') is NOT NULL then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'HTS',
                           'NON_NUMERIC');
               O_error := TRUE;
            else
               --checks that the chapter present in HTS is valid
               O_chapter := SUBSTR(L_hts,1,LP_hts_heading_length);
               if HTS_SQL.GET_HTS_CHAPTER_DESC(O_error_message,
                                               L_hts_chapter_desc,
                                               I_rec.import_country_id,
                                               O_chapter) = FALSE then
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              I_rec.row_seq,
                              'IMPORT_COUNTRY_ID',
                              O_error_message);
                  O_error := TRUE;
               end if;
            end if;
         end if;
      end if;
   end if;

   --checks that the input units is either (0,1,2,3)
   if I_rec.action IN (action_new, action_mod) then
      if I_rec.units is NOT NULL then
         if I_rec.units NOT IN (0,1,2,3) then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'UNITS',
                        'INV_VAL_0123');
            O_error := TRUE;
         end if;
      end if;
   end if;

   --checks that all the input units are unique
   if I_rec.action IN (action_new, action_mod) then
      if I_rec.units = 0 then
         I_rec.units_1 := 'X';
         I_rec.units_2 := NULL;
         I_rec.units_3 := NULL;
      elsif I_rec.units = 1 then
         I_rec.units_2 := NULL;
         I_rec.units_3 := NULL;
      elsif I_rec.units =2 then
         I_rec.units_3 := NULL;
      elsif I_rec.units = 3 then
         NULL;
      end if;

      if (I_rec.hts_umc_fk3_rid is NOT NULL and I_rec.units_3 is NOT NULL)
         or (I_rec.hts_umc_fk2_rid is NOT NULL and I_rec.units_2 is NOT NULL)
         or (I_rec.hts_umc_fk_rid is NOT NULL and I_rec.units_1 is NOT NULL) then
         if I_rec.units_1 = I_rec.units_2 or
            I_rec.units_2 = I_rec.units_3 or
            I_rec.units_3 = I_rec.units_1 then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Unit 1 UOM,Unit 2 UOM,Unit 3 UOM',
                        'UOM_UNIQUE');
            O_error := TRUE;
         end if;
      end if;
   end if;

   --checks that effect_from is earlier than effect_to
   if I_rec.action = action_new and I_rec.PK_HTS_rid is NULL then
      if I_rec.effect_from is NOT NULL and
         I_rec.effect_to is NOT NULL then
         if I_rec.effect_from >= I_rec.effect_to then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Effect From,Effect to',
                        'TO_DATE_AFTER_FROM_DATE');
            O_error := TRUE;
         end if;

         --checks that the input date range (effect_from to effect_to) do not conflict with the existing date ranges
         if VALIDATE_DATE_RANGE(O_error_message,
                                L_date_range_exists,
                                I_rec)= FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Effect From,Effect to',
                        O_error_message);
            O_error := TRUE;
         end if;
         if L_date_range_exists = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'Effect From,Effect to',
                        O_error_message);
            O_error := TRUE;
         end if;
      end if;
   end if;
   --checks that the input duty_comp_code is present in the CODE_DETAIL table
   --with code_type as 'DCMP'
   if I_rec.action IN (action_new,action_mod) and
      I_rec.duty_comp_code is NOT NULL and
      I_rec.cd_rid is NULL then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'DUTY_COMP_CODE',
                  'HTS_INV_DUTY_CODE');
      O_error := TRUE;
   end if;
   if I_rec.action = action_del then
      if HTS_SQL.CHECK_LOCK_HTS(O_error_message,
                                L_exists,
                                L_mode,
                                I_rec.hts,
                                I_rec.import_country_id,
                                I_rec.effect_from,
                                I_rec.effect_to) = FALSE then
          WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
          O_error := TRUE;
      elsif L_exists = TRUE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_VAL;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_INS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_hts_temp_rec    IN       HTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_INS';

BEGIN
   insert into hts
        values I_hts_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_UPD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_hts_temp_rec    IN       HTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_HTS_DEFINITION.EXEC_HTS_UPD';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'HTS';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_HTS_LOCK is
      select 'x'
        from hts
       where effect_to         = I_hts_temp_rec.effect_to
         and effect_from       = I_hts_temp_rec.effect_from
         and import_country_id = I_hts_temp_rec.import_country_id
         and hts               = I_hts_temp_rec.hts
         for update nowait;
BEGIN
   open  C_HTS_LOCK;
   close C_HTS_LOCK;

   update hts
      set row = I_hts_temp_rec
    where 1 = 1
      and effect_to         = I_hts_temp_rec.effect_to
      and effect_from       = I_hts_temp_rec.effect_from
      and import_country_id = I_hts_temp_rec.import_country_id
      and hts               = I_hts_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_HTS_LOCK;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_temp_rec.hts,
                                            I_hts_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      if C_HTS_LOCK%ISOPEN then
         close C_HTS_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_hts_temp_rec    IN       HTS%ROWTYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                       := 'CORESVC_HTS_DEFINITION.EXEC_HTS_DEL';
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'HTS';
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_HTS_LOCK is
      select 'x'
        from hts
       where effect_to         = I_hts_temp_rec.effect_to
         and effect_from       = I_hts_temp_rec.effect_from
         and import_country_id = I_hts_temp_rec.import_country_id
         and hts               = I_hts_temp_rec.hts
         for update nowait;

   cursor C_HTS_TL_LOCK is
      select 'x'
        from hts_tl
       where effect_to         = I_hts_temp_rec.effect_to
         and effect_from       = I_hts_temp_rec.effect_from
         and import_country_id = I_hts_temp_rec.import_country_id
         and hts               = I_hts_temp_rec.hts
         for update nowait;
BEGIN
   open  C_HTS_TL_LOCK;
   close C_HTS_TL_LOCK;

   delete from hts_tl
    where effect_to         = I_hts_temp_rec.effect_to
      and effect_from       = I_hts_temp_rec.effect_from
      and import_country_id = I_hts_temp_rec.import_country_id
      and hts               = I_hts_temp_rec.hts;

   if HTS_SQL.DELETE_HTS(O_error_message,
                         I_hts_temp_rec.hts,
                         I_hts_temp_rec.import_country_id,
                         I_hts_temp_rec.effect_from,
                         I_hts_temp_rec.effect_to) = FALSE then
      return FALSE;
   end if;

   open  C_HTS_LOCK;
   close C_HTS_LOCK;

   delete from hts
    where 1 = 1
      and effect_to         = I_hts_temp_rec.effect_to
      and effect_from       = I_hts_temp_rec.effect_from
      and import_country_id = I_hts_temp_rec.import_country_id
      and hts               = I_hts_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_HTS_LOCK;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_temp_rec.hts,
                                            I_hts_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      if C_HTS_LOCK%ISOPEN   then
         close C_HTS_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TL_INS(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_tl_ins_tab   IN       HTS_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TL_INS';

BEGIN
   if I_hts_tl_ins_tab is NOT NULL and I_hts_tl_ins_tab.COUNT > 0 then
      FORALL i IN 1..I_hts_tl_ins_tab.COUNT()
         insert into hts_tl
              values I_hts_tl_ins_tab(i);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_TL_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TL_UPD(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_tl_upd_tab   IN       HTS_TL_TAB,
                         I_hts_tl_upd_rst   IN       ROW_SEQ_TAB,
                         I_process_id       IN       SVC_HTS_TL.PROCESS_ID%TYPE,
                         I_chunk_id         IN       SVC_HTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_HTS_TL_UPD(I_hts                 HTS_TL.HTS%TYPE,
                            I_lang                HTS_TL.LANG%TYPE,
                            I_import_country_id   HTS_TL.IMPORT_COUNTRY_ID%TYPE,
                            I_effect_from         HTS_TL.EFFECT_FROM%TYPE,
                            I_effect_to           HTS_TL.EFFECT_TO%TYPE) is
      select 'x'
        from hts_tl
       where hts = I_hts
         and lang = I_lang
         and import_country_id = I_import_country_id
         and effect_from = I_effect_from
         and effect_to = I_effect_to
         for update nowait;
BEGIN
   if I_hts_tl_upd_tab is NOT NULL and I_hts_tl_upd_tab.COUNT > 0 then
      FOR i in I_hts_tl_upd_tab.FIRST..I_hts_tl_upd_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_hts_tl_upd_tab(i).lang);
            L_key_val2 := 'HTS: '||to_char(I_hts_tl_upd_tab(i).hts);
            open C_LOCK_HTS_TL_UPD(I_hts_tl_upd_tab(i).hts,
                                   I_hts_tl_upd_tab(i).lang,
                                   I_hts_tl_upd_tab(i).import_country_id,
                                   I_hts_tl_upd_tab(i).effect_from,
                                   I_hts_tl_upd_tab(i).effect_to);
            close C_LOCK_HTS_TL_UPD;

            update hts_tl
               set hts_desc = I_hts_tl_upd_tab(i).hts_desc,
                   last_update_id = I_hts_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_hts_tl_upd_tab(i).last_update_datetime
             where lang = I_hts_tl_upd_tab(i).lang
               and hts = I_hts_tl_upd_tab(i).hts
               and import_country_id = I_hts_tl_upd_tab(i).import_country_id
               and effect_from = I_hts_tl_upd_tab(i).effect_from
               and effect_to = I_hts_tl_upd_tab(i).effect_to;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                     L_table,
                                                     L_key_val1,
                                                     L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           L_table,
                           I_hts_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END; 
      END LOOP;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_HTS_TL_UPD%ISOPEN then
         close C_LOCK_HTS_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_TL_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TL_DEL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_tl_del_tab   IN       HTS_TL_TAB,
                         I_hts_tl_del_rst   IN       ROW_SEQ_TAB,
                         I_process_id       IN       SVC_HTS_TL.PROCESS_ID%TYPE,
                         I_chunk_id         IN       SVC_HTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

   cursor C_LOCK_HTS_TL_DEL(I_hts                 HTS_TL.HTS%TYPE,
                            I_lang                HTS_TL.LANG%TYPE,
                            I_import_country_id   HTS_TL.IMPORT_COUNTRY_ID%TYPE,
                            I_effect_from         HTS_TL.EFFECT_FROM%TYPE,
                            I_effect_to           HTS_TL.EFFECT_TO%TYPE) is
      select 'x'
        from hts_tl
       where hts = I_hts
         and lang = I_lang
         and import_country_id = I_import_country_id
         and effect_from = I_effect_from
         and effect_to = I_effect_to
         for update nowait;
BEGIN
   if I_hts_tl_del_tab is NOT NULL and I_hts_tl_del_tab.COUNT > 0 then
      FOR i in I_hts_tl_del_tab.FIRST..I_hts_tl_del_tab.LAST LOOP
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_hts_tl_del_tab(i).lang);
            L_key_val2 := 'HTS: '||to_char(I_hts_tl_del_tab(i).hts);
            open C_LOCK_HTS_TL_DEL(I_hts_tl_del_tab(i).hts,
                                   I_hts_tl_del_tab(i).lang,
                                   I_hts_tl_del_tab(i).import_country_id,
                                   I_hts_tl_del_tab(i).effect_from,
                                   I_hts_tl_del_tab(i).effect_to);
            close C_LOCK_HTS_TL_DEL;
            
            delete hts_tl
             where lang = I_hts_tl_del_tab(i).lang
               and hts = I_hts_tl_del_tab(i).hts
               and import_country_id = I_hts_tl_del_tab(i).import_country_id
               and effect_from = I_hts_tl_del_tab(i).effect_from
               and effect_to = I_hts_tl_del_tab(i).effect_to;
         EXCEPTION
               when RECORD_LOCKED then
                  O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                        L_table,
                                                        L_key_val1,
                                                        L_key_val2);
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table,
                              I_hts_tl_del_rst(i),
                              NULL,
                              O_error_message);
         END;
      END LOOP;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      if C_LOCK_HTS_TL_DEL%ISOPEN then
         close C_LOCK_HTS_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_HTS_TL_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id      IN       SVC_HTS.PROCESS_ID%TYPE,
                     I_chunk_id        IN       SVC_HTS.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS';
   L_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS';
   L_error           BOOLEAN;
   L_chapter         HTS.CHAPTER%TYPE;
   L_process_error   BOOLEAN := FALSE;
   L_exists          BOOLEAN;
   L_hts_temp_rec    HTS%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_HTS(I_process_id,
                        I_chunk_id) LOOP
      --fetch the heading length and format mask
      if HTS_SQL.GET_FORMAT_MASK_HEADING_LENGTH(O_error_message,
                                                LP_hts_format_mask,
                                                LP_hts_heading_length,
                                                rec.import_country_id) = FALSE then
         WRITE_ERROR(rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     rec.chunk_id,
                     L_table,
                     rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     O_error_message);
         L_error := TRUE;
      end if;
      SAVEPOINT successful_hts;
      L_error         := FALSE;
      L_process_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL
         and rec.PK_HTS_rid        is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect to',
                     'HTS_CODE_EXISTS');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_HTS_rid        is NULL
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.cntry_rid         is NOT NULL
         and rec.effect_to         is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect to',
                     'INV_HTS');
         L_error := TRUE;
      end if;
      if rec.action IN (action_new, action_mod) then
         if rec.hts_qca_fk_rid is NULL
            and rec.cntry_rid is NOT NULL
            and rec.quota_cat is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'QUOTA_CAT',
                        'INV_QUOTA_CATEGORY');
            L_error := TRUE;
         end if;
         if rec.hts_umc_fk3_rid is NULL
            and rec.units_3 is NOT NULL
            and rec.units = 3 then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UNITS_3',
                        'INVALID_UOM');
            L_error := TRUE;
         end if;
         if rec.hts_umc_fk_rid is NULL
            and rec.units_1 is NOT NULL
            and rec.units IN (1,2,3) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UNITS_1',
                        'INVALID_UOM');
            L_error := TRUE;
         end if;
         if rec.hts_umc_fk2_rid is NULL
            and rec.units_2 is NOT NULL
            and rec.units IN (2,3) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UNITS_2',
                        'INVALID_UOM');
            L_error := TRUE;
         end if;
         if NOT(rec.hts_desc is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'HTS_DESC',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if NOT(rec.units is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UNITS',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if NOT(rec.units_1 is NOT NULL)
            and rec.units <> 0 then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UNITS_1',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if rec.units = 2
            and rec.units_2 is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'UNITS_2',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if rec.units = 3 then
            if rec.units_2 is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'UNITS_2',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;
            if rec.units_3 is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           'UNITS_3',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;
         end if;
         if NOT(rec.duty_comp_code is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'DUTY_COMP_CODE',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if NOT(rec.more_hts_ind is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'MORE_HTS_IND',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if NOT(rec.quota_ind is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'QUOTA_IND',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if NOT(rec.ad_ind is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'AD_IND',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if NOT(rec.cvd_ind is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CVD_IND',
                        'MUST_ENTER_FIELD');
            L_error := TRUE;
         end if;
         if NOT(rec.more_hts_ind IN ('Y','N')) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'MORE_HTS_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
         if NOT(rec.cvd_ind IN ('Y','N')) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CVD_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
         if NOT(rec.ad_ind IN ('Y','N')) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'AD_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
         if NOT(rec.quota_ind IN ('Y','N')) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'QUOTA_IND',
                        'INV_Y_N_IND');
            L_error := TRUE;
         end if;
      end if;
      if PROCESS_HTS_VAL(O_error_message,
                         L_error,
                         L_chapter,
                         rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_temp_rec.cvd_ind        := rec.cvd_ind;
         L_hts_temp_rec.ad_ind         := rec.ad_ind;
         L_hts_temp_rec.quota_ind      := rec.quota_ind;
         L_hts_temp_rec.quota_cat      := rec.quota_cat;
         L_hts_temp_rec.more_hts_ind   := rec.more_hts_ind;
         L_hts_temp_rec.duty_comp_code := rec.duty_comp_code;
         L_hts_temp_rec.units          := rec.units;
         if rec.units = 0 then
            rec.units_1 := 'X';
            rec.units_2 := NULL;
            rec.units_3 := NULL;
         elsif rec.units = 1 then
            if rec.units_2 is NOT NULL then
               rec.units_2 := NULL;
            end if;
            if rec.units_3 is NOT NULL then
               rec.units_3 := NULL;
            end if;
         elsif rec.units =2 then
            if rec.units_3 is NOT NULL then
               rec.units_3 := NULL;
            end if;
         elsif rec.units = 3 then
               null;
         end if;
         L_hts_temp_rec.units_3                 := rec.units_3;
         L_hts_temp_rec.units_2                 := rec.units_2;
         L_hts_temp_rec.units_1                 := rec.units_1;
         if rec.action = action_mod then
            L_chapter := SUBSTR(rec.hts,1,LP_hts_heading_length);
         end if;
         L_hts_temp_rec.chapter                 := L_chapter;
         L_hts_temp_rec.hts_desc                := rec.hts_desc;
         L_hts_temp_rec.effect_to               := rec.effect_to;
         L_hts_temp_rec.effect_from             := rec.effect_from;
         L_hts_temp_rec.import_country_id       := rec.import_country_id;
         L_hts_temp_rec.hts                     := rec.hts;

         if rec.action = action_new then
            if EXEC_HTS_INS(O_error_message,
                            L_hts_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_mod then
            if EXEC_HTS_UPD(O_error_message,
                            L_hts_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         if rec.action = action_del then
            if EXEC_HTS_DEL(O_error_message,
                            L_hts_temp_rec) = FALSE then
               ROLLBACK TO successful_hts;
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_HTS_TL.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_HTS_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_TL';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TL';
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TL';
   L_base_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS';
   L_error              BOOLEAN := FALSE;
   L_process_error      BOOLEAN := FALSE;
   L_hts_TL_temp_rec    HTS_TL%ROWTYPE;
   L_hts_TL_upd_rst     ROW_SEQ_TAB;
   L_hts_TL_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_HTS_TL(I_process_id NUMBER,
                       I_chunk_id NUMBER) is
      select pk_hts_tl.rowid       as pk_hts_TL_rid,
             fk_hts.rowid          as fk_hts_rid,
             fk_lang.rowid         as fk_lang_rid,
             st.lang,
             st.hts,
             st.import_country_id,
             st.effect_from,
             st.effect_to,
             st.hts_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_hts_tl       st,
             hts              fk_hts,
             hts_tl           pk_hts_tl,
             lang             fk_lang
       where st.process_id        =  I_process_id
         and st.chunk_id          =  I_chunk_id
         and st.hts               =  fk_hts.hts (+)
         and st.import_country_id =  fk_hts.import_country_id (+)
         and st.effect_from       =  fk_hts.effect_from (+)
         and st.effect_to         =  fk_hts.effect_to (+)
         and st.lang              =  pk_hts_TL.lang (+)
         and st.hts               =  pk_hts_TL.hts (+)
         and st.import_country_id =  pk_hts_TL.import_country_id (+)
         and st.effect_from       =  pk_hts_TL.effect_from (+)
         and st.effect_to         =  pk_hts_TL.effect_to (+)
         and st.lang              =  fk_lang.lang (+);

   TYPE SVC_HTS_TL_TAB is TABLE OF C_SVC_HTS_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_hts_tl_tab   SVC_HTS_TL_TAB;

   L_hts_tl_ins_tab   HTS_TL_TAB         := NEW HTS_TL_TAB();
   L_hts_tl_upd_tab   HTS_TL_TAB         := NEW HTS_TL_TAB();
   L_hts_tl_del_tab   HTS_TL_TAB         := NEW HTS_TL_TAB();

BEGIN
   if C_SVC_HTS_TL%ISOPEN then
      close C_SVC_HTS_TL;
   end if;

   open C_SVC_HTS_TL(I_process_id,
                     I_chunk_id);
   LOOP
      fetch C_SVC_HTS_TL bulk collect into L_svc_hts_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_hts_tl_tab.COUNT > 0 then
         FOR i in L_svc_hts_tl_tab.FIRST..L_svc_hts_tl_tab.LAST LOOP
            L_error := FALSE;

            --check if action is valid
            if L_svc_hts_tl_tab(i).action is NULL
               or L_svc_hts_tl_tab(i).action NOT IN (action_new, action_mod, action_del) then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error := TRUE;
            end if;

            --check for primary_lang
            if L_svc_hts_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            --check if primary key values already exist
            if L_svc_hts_tl_tab(i).action = action_new
               and L_svc_hts_tl_tab(i).pk_hts_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_hts_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_hts_tl_tab(i).lang is NOT NULL
               and L_svc_hts_tl_tab(i).hts is NOT NULL
               and L_svc_hts_tl_tab(i).import_country_id is NOT NULL
               and L_svc_hts_tl_tab(i).effect_from is NOT NULL
               and L_svc_hts_tl_tab(i).effect_to is NOT NULL
               and L_svc_hts_tl_tab(i).pk_hts_tl_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           NULL,
                           'NO_RECORD_UPD_DEL');
               L_error := TRUE;
            end if;

            --check for FK
            if L_svc_hts_tl_tab(i).action = action_new
               and L_svc_hts_tl_tab(i).hts is NOT NULL
               and L_svc_hts_tl_tab(i).import_country_id is NOT NULL
               and L_svc_hts_tl_tab(i).effect_from is NOT NULL
               and L_svc_hts_tl_tab(i).effect_to is NOT NULL
               and L_svc_hts_tl_tab(i).fk_hts_rid is NULL then
               L_error_message := SQL_LIB.CREATE_MSG('PARENT_REC_NOT_EXIST',
                                                     L_base_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error := TRUE;
            end if;

            if L_svc_hts_tl_tab(i).action = action_new
               and L_svc_hts_tl_tab(i).lang is NOT NULL
               and L_svc_hts_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error := TRUE;
            end if;

            --check required fields
            if L_svc_hts_tl_tab(i).action in (action_new, action_mod) then
               if L_svc_hts_tl_tab(i).hts_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_hts_tl_tab(i).row_seq,
                              'HTS_DESC',
                              'MUST_ENTER_FIELD');
                  L_error := TRUE;
               end if;
            end if;

            if L_svc_hts_tl_tab(i).effect_to is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           'EFFECT_TO',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_hts_tl_tab(i).effect_from is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           'EFFECT_FROM',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_hts_tl_tab(i).import_country_id is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           'IMPORT_COUNTRY_ID',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_hts_tl_tab(i).hts is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           'HTS',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if L_svc_hts_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_hts_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_FIELD');
               L_error := TRUE;
            end if;

            if NOT L_error then
               L_hts_TL_temp_rec.lang                 := L_svc_hts_tl_tab(i).lang;
               L_hts_TL_temp_rec.hts                  := L_svc_hts_tl_tab(i).hts;
               L_hts_TL_temp_rec.import_country_id    := L_svc_hts_tl_tab(i).import_country_id;
               L_hts_TL_temp_rec.effect_from          := L_svc_hts_tl_tab(i).effect_from;
               L_hts_TL_temp_rec.effect_to            := L_svc_hts_tl_tab(i).effect_to;
               L_hts_TL_temp_rec.hts_desc             := L_svc_hts_tl_tab(i).hts_desc;
               L_hts_TL_temp_rec.create_datetime      := SYSDATE;
               L_hts_TL_temp_rec.create_id            := GET_USER;
               L_hts_TL_temp_rec.last_update_datetime := SYSDATE;
               L_hts_TL_temp_rec.last_update_id       := GET_USER;

               if L_svc_hts_tl_tab(i).action = action_new then
                  L_hts_TL_ins_tab.extend;
                  L_hts_TL_ins_tab(L_hts_TL_ins_tab.COUNT()) := L_hts_TL_temp_rec;
               end if;

               if L_svc_hts_tl_tab(i).action = action_mod then
                  L_hts_TL_upd_tab.extend;
                  L_hts_TL_upd_tab(L_hts_TL_upd_tab.COUNT()) := L_hts_TL_temp_rec;
                  L_hts_TL_upd_rst(L_hts_TL_upd_tab.count()) := L_svc_hts_tl_tab(i).row_seq;
               end if;

               if L_svc_hts_tl_tab(i).action = action_del then
                  L_hts_TL_del_tab.extend;
                  L_hts_TL_del_tab(L_hts_TL_del_tab.COUNT()) := L_hts_TL_temp_rec;
                  L_hts_TL_del_rst(L_hts_TL_del_tab.count()) := L_svc_hts_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_HTS_TL%NOTFOUND;
   END LOOP;
   close C_SVC_HTS_TL;

   if EXEC_HTS_TL_INS(O_error_message,
                      L_hts_TL_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_HTS_TL_UPD(O_error_message,
                      L_hts_TL_upd_tab,
                      L_hts_TL_upd_rst,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_HTS_TL_DEL(O_error_message,
                      L_hts_TL_del_tab,
                      L_hts_TL_del_rst,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_TL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_AD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error           IN OUT   BOOLEAN,
                            I_rec             IN       C_SVC_HTS_AD%ROWTYPE) 
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_VAL_HTS_AD';
   L_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_AD';
   L_exists                   BOOLEAN;
   L_supplier_rec             V_SUPS%ROWTYPE;
   L_supplier_access_exists   BOOLEAN := TRUE;
   L_supplier_desc            SUPS.SUP_NAME%TYPE;
   L_supplier_sites_ind       SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE;
   LP_system_options_row      SYSTEM_OPTIONS%ROWTYPE;
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_supplier_sites_ind := LP_system_options_row.supplier_sites_ind;

   --check if rate is valid
   if I_rec.action IN (action_new,action_mod) then
      if I_rec.rate is NOT NULL and I_rec.rate < 0.0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'RATE',
                     'GREATER_0');
         O_error := TRUE;
      end if;
   end if;

   --check if entry date is earlier than export date
   if (I_rec.effective_entry_date is NOT NULL) then
      if I_rec.effective_entry_date < I_rec.effective_export_date then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'EFFECTIVE_ENTRY_DATE',
                     'EXPORT_BEFORE_IMPORT');
         O_error := TRUE;
      end if;  
   end if;

   --check if supplier is NULL
   if I_rec.action IN (action_new,action_mod) then
      if NOT(I_rec.supplier is NOT NULL) then
         if L_supplier_sites_ind = 'Y' then 
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SUPPLIER',
                        'ENTER_SUPPLIER_SITE');
            O_error := TRUE;
         else
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SUPPLIER',
                        'ENTER_SUPPLIER');
            O_error := TRUE;
         end if;
      end if;
   end if;
   
   --validations for supplier and supplier site
   if I_rec.action IN (action_new, action_mod) then
      if I_rec.supplier is NOT NULL then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(O_error_message,
                                                      L_supplier_access_exists,
                                                      L_supplier_rec,
                                                      I_rec.supplier) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        NULL,
                        O_error_message);
            O_error := TRUE;
         end if;
         --security check
         if L_supplier_access_exists = FALSE then
            if L_supplier_sites_ind = 'Y' then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'SUPPLIER',
                           'INV_SUPPLIER_SITE');
               O_error := TRUE;
            else
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           NULL,
                           O_error_message);
               O_error := TRUE;
            end if;    
         end if;

         if SUPP_ATTRIB_SQL.CHECK_SUPPLIER_SITES(O_error_message,
                                                 L_exists,
                                                 I_rec.supplier) = TRUE then
            if L_supplier_sites_ind = 'N' 
               and  L_exists = TRUE   then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'SUPPLIER',
                           'SUPP_SITE_NOT_ALLOW');
               O_error := TRUE;
            end if;
         end if; 
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program, 
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_VAL_HTS_AD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_AD_INS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_ad_temp_rec   IN       HTS_AD%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_AD_INS';
BEGIN
   insert into hts_ad
        values I_hts_ad_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_AD_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_AD_UPD(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_ad_temp_rec   IN       HTS_AD%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_AD_UPD';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_AD';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_HTS_AD_UPD is
      select 'x'
        from hts_ad
       where mfg_id            = I_hts_ad_temp_rec.mfg_id
         and effect_to         = I_hts_ad_temp_rec.effect_to
         and effect_from       = I_hts_ad_temp_rec.effect_from
         and import_country_id = I_hts_ad_temp_rec.import_country_id
         and hts               = I_hts_ad_temp_rec.hts
         and origin_country_id = I_hts_ad_temp_rec.origin_country_id
         for update nowait;
BEGIN 
   open  C_LOCK_HTS_AD_UPD;
   close C_LOCK_HTS_AD_UPD;

   update hts_ad 
      set row = I_hts_ad_temp_rec 
    where 1 = 1
      and mfg_id            = I_hts_ad_temp_rec.mfg_id
      and effect_to         = I_hts_ad_temp_rec.effect_to
      and effect_from       = I_hts_ad_temp_rec.effect_from
      and import_country_id = I_hts_ad_temp_rec.import_country_id
      and hts               = I_hts_ad_temp_rec.hts
      and origin_country_id = I_hts_ad_temp_rec.origin_country_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED   then
      close C_LOCK_HTS_AD_UPD;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_ad_temp_rec.mfg_id,
                                            I_hts_ad_temp_rec.origin_country_id);
      return FALSE;
   when OTHERS then
      if C_LOCK_HTS_AD_UPD%ISOPEN   then
         close C_LOCK_HTS_AD_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_AD_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_AD_DEL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_hts_ad_temp_rec   IN       HTS_AD%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_AD_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_AD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_HTS_AD_DEL is
      select 'x'
        from hts_ad
       where mfg_id            = I_hts_ad_temp_rec.mfg_id
         and effect_to         = I_hts_ad_temp_rec.effect_to
         and effect_from       = I_hts_ad_temp_rec.effect_from
         and import_country_id = I_hts_ad_temp_rec.import_country_id
         and hts               = I_hts_ad_temp_rec.hts
         and origin_country_id = I_hts_ad_temp_rec.origin_country_id
         for update nowait;
BEGIN 
   open  C_LOCK_HTS_AD_DEL;
   close C_LOCK_HTS_AD_DEL;

   delete from hts_ad 
    where 1 = 1
      and mfg_id            = I_hts_ad_temp_rec.mfg_id
      and effect_to         = I_hts_ad_temp_rec.effect_to
      and effect_from       = I_hts_ad_temp_rec.effect_from
      and import_country_id = I_hts_ad_temp_rec.import_country_id
      and hts               = I_hts_ad_temp_rec.hts
      and origin_country_id = I_hts_ad_temp_rec.origin_country_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_LOCK_HTS_AD_DEL;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_ad_temp_rec.mfg_id,
                                            I_hts_ad_temp_rec.origin_country_id);
      return FALSE; 
   when OTHERS then
      if C_LOCK_HTS_AD_DEL%ISOPEN then
         close C_LOCK_HTS_AD_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_AD_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_AD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_process_id      IN       SVC_HTS_AD.PROCESS_ID%TYPE,
                        I_chunk_id        IN       SVC_HTS_AD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program           VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_AD';
   L_table             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_AD';
   L_hts_ad_temp_rec   HTS_AD%ROWTYPE;
   L_error             BOOLEAN;
   L_process_error     BOOLEAN;

BEGIN
   FOR rec IN C_SVC_HTS_AD(I_process_id,
                           I_chunk_id) LOOP
      L_error := FALSE;
      --check if action is NULL or other than new,mod,del
      if rec.action is NULL 
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      --check if record already exists
      if rec.action = action_new 
         and rec.PK_HTS_AD_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Mfg. ID,Country Of Sourcing',
                     'HTS_AD_EXISTS');
         L_error := TRUE;
      end if;

      --check if action is mod,del and record does not exist
      if rec.action IN (action_mod,action_del) 
         and rec.PK_HTS_AD_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Mfg. ID,Country Of Sourcing',
                     'NO_HTS_AD_DETL');
         L_error := TRUE;
      end if;

      --check whether HTS exists in HTS table
      if rec.HTA_HTS_FK_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Mfg. ID,Country Of Sourcing',
                     'INV_HTS');
         L_error := TRUE;
      end if;

      --check whether country exists in COUNTRY table
      if rec.HTA_CNT_FK_rid is NULL 
         and rec.origin_country_id is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ORIGIN_COUNTRY_ID',
                     'INV_COUNTRY');
         L_error := TRUE;
      end if;

      --check if case_no is NULL
      if rec.action IN (action_new,action_mod) then
        if NOT(rec.CASE_NO is NOT NULL) then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'CASE_NO',
                       'ENTER_CASE_NO');
           L_error := TRUE;
        end if;
      end if;

      --check if shipper_id is NULL
      if rec.action IN (action_new,action_mod) then
        if NOT(rec.SHIPPER_ID is NOT NULL) then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'SHIPPER_ID',
                       'ENTER_SHIPPER_ID');
           L_error := TRUE;
        end if;
      end if;

      --check if rate is NULL
      if rec.action IN (action_new,action_mod) then
        if NOT(rec.rate is NOT NULL) then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       'RATE',
                       'ENTER_RATE');
           L_error := TRUE;
        end if;
      end if;

      --call validation function
      if PROCESS_VAL_HTS_AD(O_error_message,
                            L_error,
                            rec) = FALSE then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       NULL,
                       O_error_message);
           L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_ad_temp_rec.mfg_id                := rec.mfg_id;
         L_hts_ad_temp_rec.effect_to             := rec.effect_to;
         L_hts_ad_temp_rec.effect_from           := rec.effect_from;
         L_hts_ad_temp_rec.import_country_id     := rec.import_country_id;
         L_hts_ad_temp_rec.hts                   := rec.hts;
         L_hts_ad_temp_rec.effective_export_date := rec.effective_export_date;
         L_hts_ad_temp_rec.effective_entry_date  := rec.effective_entry_date;
         L_hts_ad_temp_rec.related_case_no       := rec.related_case_no;
         L_hts_ad_temp_rec.rate                  := rec.rate;
         L_hts_ad_temp_rec.supplier              := rec.supplier;
         L_hts_ad_temp_rec.shipper_id            := rec.shipper_id;
         L_hts_ad_temp_rec.case_no               := rec.case_no;
         L_hts_ad_temp_rec.origin_country_id     := rec.origin_country_id;

         if rec.action = action_new then
            if EXEC_HTS_AD_INS(O_error_message,
                               L_hts_ad_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if; 
         end if;

         if rec.action = action_mod then
            if EXEC_HTS_AD_UPD(O_error_message,
                               L_hts_ad_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_HTS_AD_DEL(O_error_message,
                               L_hts_ad_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_AD;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_CVD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error           IN OUT   BOOLEAN,
                             I_rec             IN       C_SVC_HTS_CVD%ROWTYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_VAL_HTS_CVD';
   L_table                    SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_CVD';
   L_supplier_access_exists   BOOLEAN                           := TRUE;
   L_exists                   BOOLEAN;
   L_supplier_desc            SUPS.SUP_NAME%TYPE;
   L_supplier_rec             V_SUPS%ROWTYPE;
   L_supplier_sites_ind       SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE;
   L_system_options_row       SYSTEM_OPTIONS%ROWTYPE;
BEGIN
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;
   L_supplier_sites_ind := L_system_options_row.supplier_sites_ind;

   --check if rate is invalid
   if I_rec.action IN (action_new,action_mod) then
      if I_rec.rate  is NOT NULL
         and I_rec.rate < 0.0   then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'RATE',
                     'POSITIVE_VALUE');
         O_error := TRUE;
      end if;
   end if;

   --check if entry is before export
   if (I_rec.effective_entry_date is NOT NULL) then
      if I_rec.effective_entry_date < I_rec.effective_export_date then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'EFFECTIVE_ENTRY_DATE',
                     'EXPORT_BEFORE_IMPORT');
         O_error := TRUE;
      end if;
   end if;

   --check if supplier is NULL
   if I_rec.action IN (action_new,action_mod) then
      if NOT(I_rec.supplier is NOT NULL) then
         if L_supplier_sites_ind = 'Y' then 
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SUPPLIER',
                        'ENTER_SUPPLIER_SITE');
            O_error := TRUE;
         else
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SUPPLIER',
                        'ENTER_SUPPLIER');
            O_error := TRUE;
         end if;
      end if;
   end if;

   --validations for supplier and supplier site
   if I_rec.action IN (action_new, action_mod) then
      if I_rec.supplier is NOT NULL then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(O_error_message,
                                                      L_supplier_access_exists,
                                                      L_supplier_rec,
                                                      I_rec.supplier) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SUPPLIER',
                        O_error_message);
            O_error := TRUE;
         end if;
         --security check
         if L_supplier_access_exists = FALSE then
            if L_supplier_sites_ind = 'Y' then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'SUPPLIER',
                           'INV_SUPPLIER_SITE');
               O_error := TRUE;
            else
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'SUPPLIER',
                            O_error_message);
               O_error := TRUE;
            end if;
         else
            if SUPP_ATTRIB_SQL.CHECK_SUPPLIER_SITES(O_error_message,
                                                    L_exists,
                                                    I_rec.supplier) = TRUE then
               if L_supplier_sites_ind = 'N'
                  and  L_exists = TRUE then
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              I_rec.row_seq,
                              'SUPPLIER',
                              'SUPP_SITE_NOT_ALLOW');
                  O_error := TRUE;
               end if;
               if L_supplier_sites_ind = 'Y'
                  and L_exists = FALSE then
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              I_rec.row_seq,
                              'SUPPLIER',
                              'INV_SUPPLIER_SITE');
                  O_error := TRUE;
               end if;
            end if;
         end if; 
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_HTS_CVD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CVD_INS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_cvd_temp_rec   IN       HTS_CVD%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_CVD_INS';
BEGIN
   insert into hts_cvd
       values I_hts_cvd_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_CVD_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CVD_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_cvd_temp_rec   IN       HTS_CVD%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_CVD_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CVD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_CVD_LOCK is
      select 'x'
        from hts_cvd
       where origin_country_id = I_hts_cvd_temp_rec.origin_country_id
         and effect_to         = I_hts_cvd_temp_rec.effect_to
         and effect_from       = I_hts_cvd_temp_rec.effect_from
         and import_country_id = I_hts_cvd_temp_rec.import_country_id
         and hts               = I_hts_cvd_temp_rec.hts
         for update nowait;
BEGIN
   open  C_HTS_CVD_LOCK;
   close C_HTS_CVD_LOCK;

   update hts_cvd
      set row = I_hts_cvd_temp_rec
    where 1 = 1
      and origin_country_id = I_hts_cvd_temp_rec.origin_country_id
      and effect_to         = I_hts_cvd_temp_rec.effect_to
      and effect_from       = I_hts_cvd_temp_rec.effect_from
      and import_country_id = I_hts_cvd_temp_rec.import_country_id
      and hts               = I_hts_cvd_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_HTS_CVD_LOCK;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_cvd_temp_rec.hts,
                                                               I_hts_cvd_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_CVD_LOCK%ISOPEN then
         close C_HTS_CVD_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_CVD_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_CVD_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_cvd_temp_rec   IN       HTS_CVD%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_CVD_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_CVD';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_CVD_LOCK is
      select 'x'
        from hts_cvd
       where origin_country_id = I_hts_cvd_temp_rec.origin_country_id
         and effect_to         = I_hts_cvd_temp_rec.effect_to
         and effect_from       = I_hts_cvd_temp_rec.effect_from
         and import_country_id = I_hts_cvd_temp_rec.import_country_id
         and hts               = I_hts_cvd_temp_rec.hts
         for update nowait;
BEGIN
   open  C_HTS_CVD_LOCK;
   close C_HTS_CVD_LOCK;

   delete from hts_cvd
    where 1 = 1
      and origin_country_id  = I_hts_cvd_temp_rec.origin_country_id
      and effect_to          = I_hts_cvd_temp_rec.effect_to
      and effect_from        = I_hts_cvd_temp_rec.effect_from
      and import_country_id  = I_hts_cvd_temp_rec.import_country_id
      and hts                = I_hts_cvd_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_HTS_CVD_LOCK;
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_cvd_temp_rec.hts,
                                                               I_hts_cvd_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      if C_HTS_CVD_LOCK%ISOPEN then
         close C_HTS_CVD_LOCK;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_CVD_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_CVD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_HTS_CVD.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_HTS_CVD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      :='CORESVC_HTS_DEFINITION.PROCESS_HTS_CVD';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_HTS_CVD';
   L_hts_cvd_temp_rec   HTS_CVD%ROWTYPE;
   L_process_error      BOOLEAN;
   L_error              BOOLEAN;
BEGIN
   FOR rec IN C_SVC_HTS_CVD(I_process_id,
                            I_chunk_id) LOOP
      L_error := FALSE;
      --check if action is NULL or other than new,mod,del
      if rec.action is NULL 
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      --check if record already exists
      if rec.action = action_new 
         and rec.PK_HTS_CVD_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Country Of Sourcing',
                     'HTS_CVD_EXISTS');
         L_error := TRUE;
      end if;

      --check if action is mod, del and if record is missing
      if rec.action IN (action_mod,action_del)
         and rec.PK_HTS_CVD_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Country Of Sourcing',
                     'NO_RECORD');
         L_error := TRUE;
      end if;

      --check if HTS is missing from HTS table
      if rec.HCV_HTS_FK_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS',
                     'INV_HTS');
         L_error := TRUE;
      end if;

      --check if country is missing from COUNTRY table
      if rec.HCV_CNT_FK_rid is NULL
         and rec.origin_country_id is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ORIGIN_COUNTRY_ID',
                     'INV_COUNTRY');
         L_error := TRUE;
      end if;

      --check if case_no is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.case_no is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'CASE_NO',
                        'ENTER_CASE_NO');
            L_error := TRUE;
         end if;
      end if;

      --check if mfg_id is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.mfg_id is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'MFG_ID',
                        'ENTER_MFG_ID');
            L_error := TRUE;
         end if;
      end if;

      --check if shipper_id is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.shipper_id is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'SHIPPER_ID',
                        'ENTER_SHIPPER_ID');
            L_error := TRUE;
         end if;
      end if;

      --check if rate is NULL
      if rec.action IN (action_new,action_mod) then
         if NOT(rec.rate is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'RATE',
                        'ENTER_RATE');
            L_error := TRUE;
         end if;
      end if;

      --call validation function
      if PROCESS_VAL_HTS_CVD(O_error_message,
                             L_error,
                             rec) = FALSE then
           WRITE_ERROR(I_process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_chunk_id,
                       L_table,
                       rec.row_seq,
                       NULL,
                       O_error_message);
           L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_cvd_temp_rec.effective_export_date  := rec.effective_export_date;
         L_hts_cvd_temp_rec.effective_entry_date   := rec.effective_entry_date;
         L_hts_cvd_temp_rec.related_case_no        := rec.related_case_no;
         L_hts_cvd_temp_rec.rate                   := rec.rate;
         L_hts_cvd_temp_rec.supplier               := rec.supplier;
         L_hts_cvd_temp_rec.shipper_id             := rec.shipper_id;
         L_hts_cvd_temp_rec.mfg_id                 := rec.mfg_id;
         L_hts_cvd_temp_rec.case_no                := rec.case_no;
         L_hts_cvd_temp_rec.origin_country_id      := rec.origin_country_id;
         L_hts_cvd_temp_rec.effect_to              := rec.effect_to;
         L_hts_cvd_temp_rec.effect_from            := rec.effect_from;
         L_hts_cvd_temp_rec.import_country_id      := rec.import_country_id;
         L_hts_cvd_temp_rec.hts                    := rec.hts;

         --if no error then do the processing
         if rec.action = action_new then
            if EXEC_HTS_CVD_INS(O_error_message,
                                L_hts_cvd_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if; 
         end if;  
  
         if rec.action = action_mod then
            if EXEC_HTS_CVD_UPD(O_error_message,
                                L_hts_cvd_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if; 
         end if;  

         if rec.action = action_del then
            if EXEC_HTS_CVD_DEL(O_error_message,
                                L_hts_cvd_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_CVD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_FEE_INS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_fee_temp_rec   IN       HTS_FEE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_FEE_INS';
BEGIN
   insert into hts_fee
      values I_hts_fee_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
    return FALSE;
END EXEC_HTS_FEE_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_FEE_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_fee_temp_rec   IN       HTS_FEE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_FEE_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_FEE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_FEE_LOCK is
      select 'x'
        from hts_fee
       where fee_type          = I_hts_fee_temp_rec.fee_type
         and effect_to         = I_hts_fee_temp_rec.effect_to
         and effect_from       = I_hts_fee_temp_rec.effect_from
         and import_country_id = I_hts_fee_temp_rec.import_country_id
         and hts               = I_hts_fee_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_FEE_LOCK;
   close C_HTS_FEE_LOCK;

   update hts_fee
      set row = I_hts_fee_temp_rec
    where 1 = 1
      and fee_type          = I_hts_fee_temp_rec.fee_type
      and effect_to         = I_hts_fee_temp_rec.effect_to
      and effect_from       = I_hts_fee_temp_rec.effect_from
      and import_country_id = I_hts_fee_temp_rec.import_country_id
      and hts               = I_hts_fee_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_fee_temp_rec.hts,
                                                               I_hts_fee_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_FEE_LOCK%ISOPEN then
         close C_HTS_FEE_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_FEE_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_FEE_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_fee_temp_rec   IN       HTS_FEE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_FEE_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_FEE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_FEE_LOCK is
      select 'x'
        from hts_fee
       where fee_type          = I_hts_fee_temp_rec.fee_type
         and effect_to         = I_hts_fee_temp_rec.effect_to
         and effect_from       = I_hts_fee_temp_rec.effect_from
         and import_country_id = I_hts_fee_temp_rec.import_country_id
         and hts               = I_hts_fee_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_FEE_LOCK;
   close C_HTS_FEE_LOCK;

   delete from hts_fee
    where 1 = 1
      and fee_type          = I_hts_fee_temp_rec.fee_type
      and effect_to         = I_hts_fee_temp_rec.effect_to
      and effect_from       = I_hts_fee_temp_rec.effect_from
      and import_country_id = I_hts_fee_temp_rec.import_country_id
      and hts               = I_hts_fee_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_fee_temp_rec.hts,
                                                               I_hts_fee_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_FEE_LOCK%ISOPEN then
         close C_HTS_FEE_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_FEE_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_FEE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error           IN OUT   BOOLEAN,
                             I_rec             IN       C_SVC_HTS_FEE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_VAL_HTS_FEE';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_FEE';
   L_exist     BOOLEAN;
BEGIN
   if I_rec.action in (action_new,action_mod) then
      if I_rec.fee_specific_rate < 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FEE_SPECIFIC_RATE',
                     'NOT_NEG');
         O_error := TRUE;
      end if;

      if I_rec.fee_av_rate < 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FEE_AV_RATE',
                     'NOT_NEG');
         O_error := TRUE;
      end if;

      if I_rec.import_country_id is NOT NULL then
         if OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message,
                                                               LP_clearing_zone_exists,
                                                               I_rec.import_country_id) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'IMPORT_COUNTRY_ID',
                        O_error_message);
            O_error := TRUE;
         end if;
      end if;

      if (LP_clearing_zone_exists = FALSE)
         and I_rec.fee_specific_rate is NULL
         and I_rec.fee_av_rate is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Specific Rate,AV Rate',
                     'ENT_ATLEAST_1_RATE_HTSFEE');
         O_error := TRUE;
      end if;
   end if;

   if (I_rec.action = action_mod) then
      if (LP_clearing_zone_exists = TRUE)
         and (NVL(I_rec.fee_specific_rate,-1) <> I_rec.old_fee_specific_rate) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FEE_SPECIFIC_RATE',
                     'CANT_UPD_FEE_SPCF_RATE');
         O_error := TRUE;
      end if;
      if (LP_clearing_zone_exists = TRUE)
         and (NVL(I_rec.fee_av_rate,-1) <> I_rec.old_fee_av_rate) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'FEE_AV_RATE',
                     'CANT_UPD_FEE_AV_RATE');
         O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_HTS_FEE;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_FEE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_HTS_FEE.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_HTS_FEE.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_FEE';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_FEE';
   L_process_error      BOOLEAN                           := FALSE;
   L_error              BOOLEAN;
   L_exist              BOOLEAN;
   L_hts_fee_temp_rec   HTS_FEE%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_HTS_FEE(I_process_id,
                           I_chunk_id) LOOP
      L_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.action = action_new 
         and rec.pk_hts_fee_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Fee Type',
                     'PK_HTS_FEE');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.fee_type          is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL
         and rec.hts               is NOT NULL
         and rec.pk_hts_fee_rid    is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Fee Type',
                     'NO_HTS_FEE_DETL');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.htf_hts_fk_rid    is NULL
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Fee Type',
                     'HTF_HTS_FK');
         L_error := TRUE;
      end if;

      if rec.action in (action_new,action_mod)
         and rec.fee_comp_code is NULL then
             WRITE_ERROR(I_process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_chunk_id,
                         L_table,
                         rec.row_seq,
                         'FEE_COMP_CODE',
                         'NEED_FEE_COMP_CODE');
             L_error := TRUE;
      end if;

      LP_clearing_zone_exists := FALSE;
      if PROCESS_VAL_HTS_FEE(O_error_message,
                             L_error,
                             rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_fee_temp_rec.fee_av_rate        := NVL(rec.fee_av_rate,0);
         L_hts_fee_temp_rec.fee_specific_rate  := NVL(rec.fee_specific_rate,0);
         L_hts_fee_temp_rec.fee_comp_code      := rec.fee_comp_code;
         L_hts_fee_temp_rec.fee_type           := rec.fee_type;
         L_hts_fee_temp_rec.effect_to          := rec.effect_to;
         L_hts_fee_temp_rec.effect_from        := rec.effect_from;
         L_hts_fee_temp_rec.import_country_id  := rec.import_country_id;
         L_hts_fee_temp_rec.hts                := rec.hts;
         SAVEPOINT successful_hts_fee;
         
         if rec.action = action_new then
            if(LP_clearing_zone_exists = TRUE) then
                L_hts_fee_temp_rec.fee_specific_rate := 0.0;
                L_hts_fee_temp_rec.fee_av_rate       := 0.0;
            end if;
            if EXEC_HTS_FEE_INS(O_error_message,
                                L_hts_fee_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         
         if rec.action = action_mod then
            if EXEC_HTS_FEE_UPD(O_error_message,
                                L_hts_fee_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         
         if rec.action = action_del then
            if HTS_CLEAR_ZONE_SQL.DELETE_FEE_ZONE(O_error_message,
                                                  rec.hts,
                                                  rec.import_country_id,
                                                  rec.effect_from,
                                                  rec.effect_to,
                                                  rec.fee_type) = FALSE then
               WRITE_ERROR(rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           rec.chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
               ROLLBACK TO SAVEPOINT successful_hts_fee;
            end if;
         
            if NOT L_process_error
               and EXEC_HTS_FEE_DEL(O_error_message,
                                    L_hts_fee_temp_rec) = FALSE then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table,
                               rec.row_seq,
                               NULL,
                               O_error_message);
                   L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;
END PROCESS_HTS_FEE;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_FEE_ZONE_INS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_fee_zone_temp_rec   IN       HTS_FEE_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_FEE_ZONE_INS';
BEGIN
   insert into hts_fee_zone
       values I_hts_fee_zone_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_FEE_ZONE_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_FEE_ZONE_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_fee_zone_temp_rec   IN       HTS_FEE_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_FEE_ZONE_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_FEE_ZONE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_FEE_ZONE_UPD is
      select 'x'
        from hts_fee_zone
       where clearing_zone_id  = I_hts_fee_zone_temp_rec.clearing_zone_id
         and fee_type          = I_hts_fee_zone_temp_rec.fee_type
         and effect_to         = I_hts_fee_zone_temp_rec.effect_to
         and effect_from       = I_hts_fee_zone_temp_rec.effect_from
         and import_country_id = I_hts_fee_zone_temp_rec.import_country_id
         and hts               = I_hts_fee_zone_temp_rec.hts
         for update nowait;
BEGIN
   open C_LOCK_FEE_ZONE_UPD;
   close C_LOCK_FEE_ZONE_UPD;

   update hts_fee_zone
      set row = I_hts_fee_zone_temp_rec
    where 1 = 1
      and clearing_zone_id  = I_hts_fee_zone_temp_rec.clearing_zone_id
      and fee_type          = I_hts_fee_zone_temp_rec.fee_type
      and effect_to         = I_hts_fee_zone_temp_rec.effect_to
      and effect_from       = I_hts_fee_zone_temp_rec.effect_from
      and import_country_id = I_hts_fee_zone_temp_rec.import_country_id
      and hts               = I_hts_fee_zone_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_fee_zone_temp_rec.clearing_zone_id,
                                            I_hts_fee_zone_temp_rec.hts);
      return FALSE;
   when OTHERS then
      if C_LOCK_FEE_ZONE_UPD%ISOPEN then
         close C_LOCK_FEE_ZONE_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_FEE_ZONE_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_FEE_ZONE_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_fee_zone_temp_rec   IN       HTS_FEE_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_FEE_ZONE_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_FEE_ZONE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_FEE_ZONE_DEL is
      select 'x'
        from hts_fee_zone
       where clearing_zone_id  = I_hts_fee_zone_temp_rec.clearing_zone_id
         and fee_type          = I_hts_fee_zone_temp_rec.fee_type
         and effect_to         = I_hts_fee_zone_temp_rec.effect_to
         and effect_from       = I_hts_fee_zone_temp_rec.effect_from
         and import_country_id = I_hts_fee_zone_temp_rec.import_country_id
         and hts               = I_hts_fee_zone_temp_rec.hts
         for update nowait;
BEGIN
   open C_LOCK_FEE_ZONE_DEL;
   close C_LOCK_FEE_ZONE_DEL;

   delete from hts_fee_zone
    where 1 = 1
      and clearing_zone_id  = I_hts_fee_zone_temp_rec.clearing_zone_id
      and fee_type          = I_hts_fee_zone_temp_rec.fee_type
      and effect_to         = I_hts_fee_zone_temp_rec.effect_to
      and effect_from       = I_hts_fee_zone_temp_rec.effect_from
      and import_country_id = I_hts_fee_zone_temp_rec.import_country_id
      and hts               = I_hts_fee_zone_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_fee_zone_temp_rec.clearing_zone_id,
                                            I_hts_fee_zone_temp_rec.hts);
      return FALSE;
   when OTHERS then
      if C_LOCK_FEE_ZONE_DEL%ISOPEN then
         close C_LOCK_FEE_ZONE_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_FEE_ZONE_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_FEE_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error           IN OUT   BOOLEAN,
                                  I_rec             IN       C_SVC_HTS_FEE_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_VAL_HTS_FEE_ZONE';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_FEE_ZONE';
   L_out_desc               OUTLOC.OUTLOC_DESC%TYPE           := I_rec.outloc_desc;
   L_exist                  BOOLEAN;
   L_clearing_zone_exists   BOOLEAN                           := FALSE;
BEGIN
   --check if description for clearing_zone_id exists
   if I_rec.action = action_new
      and I_rec.clearing_zone_id  is NOT NULL
      and I_rec.import_country_id is NOT NULL then
      if OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message,
                                                            L_clearing_zone_exists,
                                                            I_rec.import_country_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      elsif L_clearing_zone_exists = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     'CANNOT_CREATE_ZONE_RATES');
         O_error := TRUE;
      elsif OUTSIDE_LOCATION_SQL.GET_DESC(O_error_message,
                                          L_out_desc,
                                          I_rec.clearing_zone_id,
                                          'CZ') = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'CLEARING_ZONE_ID',
                     'NO_DESC_CLEARING_ZONE');
         O_error := TRUE;
      end if;
   end if;

   if I_rec.action in (action_new,action_mod) then
      if (I_rec.fee_specific_rate is NULL and I_rec.fee_av_rate is NULL) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Specific Rate,AV Rate',
                     'ANYONE_RATE_NEEDED');
         O_error := TRUE;
      end if;
   end if;

   if HTS_CLEAR_ZONE_SQL.LOCK_HTS_FEE(O_error_message,
                                      I_rec.hts,
                                      I_rec.import_country_id,
                                      I_rec.effect_from,
                                      I_rec.effect_to,
                                      I_rec.fee_type) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error := TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_HTS_FEE_ZONE;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_FEE_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_HTS_FEE_ZONE.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_HTS_FEE_ZONE.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_FEE_ZONE';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_FEE_ZONE';
   L_process_error           BOOLEAN                           := FALSE;
   L_error                   BOOLEAN;
   L_hts_fee_zone_temp_rec   HTS_FEE_ZONE%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_HTS_FEE_ZONE(I_process_id,
                                 I_chunk_id) LOOP
      L_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.PK_HTS_FEE_ZONE_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Fee Type,Clearing Zone',
                     'PK_HTS_FEE_ZONE');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.PK_HTS_FEE_ZONE_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Fee Type,Clearing Zone',
                     'INV_CLEARING_ZONE');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.hfz_htf_fk_rid    is NULL
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL
         and rec.fee_type          is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Fee Type',
                     'HFZ_HTF_FK');
         L_error := TRUE;
      end if;

      if PROCESS_VAL_HTS_FEE_ZONE(O_error_message,
                                  L_error,
                                  rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
                     L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_fee_zone_temp_rec.hts               := rec.hts;
         L_hts_fee_zone_temp_rec.import_country_id := rec.import_country_id;
         L_hts_fee_zone_temp_rec.effect_from       := rec.effect_from;
         L_hts_fee_zone_temp_rec.effect_to         := rec.effect_to;
         L_hts_fee_zone_temp_rec.fee_type          := rec.fee_type;
         L_hts_fee_zone_temp_rec.clearing_zone_id  := rec.clearing_zone_id;
         L_hts_fee_zone_temp_rec.fee_specific_rate := NVL(rec.fee_specific_rate,0);
         L_hts_fee_zone_temp_rec.fee_av_rate       := NVL(rec.fee_av_rate,0);

         if rec.action = action_new then
            if EXEC_HTS_FEE_ZONE_INS(O_error_message,
                                     L_hts_fee_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_HTS_FEE_ZONE_UPD(O_error_message,
                                     L_hts_fee_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_HTS_FEE_ZONE_DEL(O_error_message,
                                     L_hts_fee_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_FEE_ZONE;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_OGA_INS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_oga_temp_rec   IN       HTS_OGA%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_OGA_INS';
BEGIN
   insert into hts_oga
        values I_hts_oga_temp_rec;

   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('HTS_OGA_EXISTS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_OGA_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_OGA_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_oga_temp_rec   IN       HTS_OGA%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_OGA_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_OGA';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_OGA_LOCK is
      select 'x'
        from hts_oga
       where oga_code          = I_hts_oga_temp_rec.oga_code
         and effect_to         = I_hts_oga_temp_rec.effect_to
         and effect_from       = I_hts_oga_temp_rec.effect_from
         and import_country_id = I_hts_oga_temp_rec.import_country_id
         and hts               = I_hts_oga_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_OGA_LOCK;
   close C_HTS_OGA_LOCK;

   update hts_oga
      set row = I_hts_oga_temp_rec
    where oga_code          = I_hts_oga_temp_rec.oga_code
      and effect_to         = I_hts_oga_temp_rec.effect_to
      and effect_from       = I_hts_oga_temp_rec.effect_from
      and import_country_id = I_hts_oga_temp_rec.import_country_id
      and hts               = I_hts_oga_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_oga_temp_rec.hts,
                                                               I_hts_oga_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_OGA_LOCK%ISOPEN then
         close C_HTS_OGA_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_OGA_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_OGA_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_oga_temp_rec   IN       HTS_OGA%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                        := 'CORESVC_HTS_DEFINITION.EXEC_HTS_OGA_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE   := 'HTS_OGA';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_OGA_LOCK is
      select 'x'
        from hts_oga
       where oga_code          = I_hts_oga_temp_rec.oga_code
         and effect_to         = I_hts_oga_temp_rec.effect_to
         and effect_from       = I_hts_oga_temp_rec.effect_from
         and import_country_id = I_hts_oga_temp_rec.import_country_id
         and hts               = I_hts_oga_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_OGA_LOCK;
   close C_HTS_OGA_LOCK;

   delete from hts_oga
    where oga_code          = I_hts_oga_temp_rec.oga_code
      and effect_to         = I_hts_oga_temp_rec.effect_to
      and effect_from       = I_hts_oga_temp_rec.effect_from
      and import_country_id = I_hts_oga_temp_rec.import_country_id
      and hts               = I_hts_oga_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_oga_temp_rec.hts,
                                                               I_hts_oga_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_OGA_LOCK%ISOPEN   then
         close C_HTS_OGA_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_OGA_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_OGA(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_HTS_OGA.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_HTS_OGA.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      :='CORESVC_HTS_DEFINITION.PROCESS_HTS_OGA';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_HTS_OGA';
   L_process_error      BOOLEAN                           := FALSE;
   L_error              BOOLEAN;
   L_hts_oga_temp_rec   HTS_OGA%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_HTS_OGA(I_process_id,
                            I_chunk_id) LOOP
      L_error          := FALSE;
      L_process_error  := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,
                               action_mod,
                               action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL
         and rec.oga_code          is NOT NULL
         and rec.pk_hts_oga_rid    is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect to,OGA Code',
                     'DUP_RECORD');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL
         and rec.oga_code          is NOT NULL
         and rec.pk_hts_oga_rid    is NULL
         and rec.hto_oga_fk_rid    is NOT NULL
         and rec.hto_hts_fk_rid    is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect to,OGA Code',
                     'HTSO_MISSING');
         L_error := TRUE;
      end if;
      if rec.hto_hts_fk_rid        is NULL
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS',
                     'INV_HTS');
         L_error := TRUE;
      end if;
      if rec.hto_oga_fk_rid is NULL
         and rec.oga_code   is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id, 
                     L_table,
                     rec.row_seq,
                     'OGA_CODE',
                     'OGA_CODE_NOTFOUND');
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_oga_temp_rec.comments           := rec.comments;
         L_hts_oga_temp_rec.reference_id       := rec.reference_id;
         L_hts_oga_temp_rec.oga_code           := rec.oga_code;
         L_hts_oga_temp_rec.effect_to          := rec.effect_to;
         L_hts_oga_temp_rec.effect_from        := rec.effect_from;
         L_hts_oga_temp_rec.import_country_id  := rec.import_country_id;
         L_hts_oga_temp_rec.hts                := rec.hts;

         if rec.action = action_new then
            if EXEC_HTS_OGA_INS(O_error_message,
                                L_hts_oga_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_HTS_OGA_UPD(O_error_message,
                                L_hts_oga_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_HTS_OGA_DEL(O_error_message,
                                L_hts_oga_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_OGA;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_REF_INS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_ref_temp_rec   IN       HTS_REFERENCE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_REF_INS';
BEGIN
   insert into hts_reference
        values I_hts_ref_temp_rec;

   return TRUE;
EXCEPTION
   when DUP_VAL_ON_INDEX then
      O_error_message := SQL_LIB.CREATE_MSG('HTS_REFERENCE_EXISTS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_REF_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_REF_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_ref_temp_rec   IN       HTS_REFERENCE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_REF_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_REFERENCE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_HTS_LOCK is
      select 'x' 
        from hts_reference
       where reference_id      = I_hts_ref_temp_rec.reference_id
         and effect_to         = I_hts_ref_temp_rec.effect_to
         and effect_from       = I_hts_ref_temp_rec.effect_from
         and import_country_id = I_hts_ref_temp_rec.import_country_id
         and hts               = I_hts_ref_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_LOCK;
   close C_HTS_LOCK;

   update hts_reference
      set row = I_hts_ref_temp_rec
    where 1 = 1
      and reference_id      = I_hts_ref_temp_rec.reference_id
      and effect_to         = I_hts_ref_temp_rec.effect_to
      and effect_from       = I_hts_ref_temp_rec.effect_from
      and import_country_id = I_hts_ref_temp_rec.import_country_id
      and hts               = I_hts_ref_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_ref_temp_rec.reference_id,
                                            I_hts_ref_temp_rec.hts);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_LOCK%ISOPEN   then
         close C_HTS_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_REF_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_REF_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_ref_temp_rec   IN       HTS_REFERENCE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_REF_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_REFERENCE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor HTS_LOCK is
      select 'x'
        from hts_reference
       where reference_id      = I_hts_ref_temp_rec.reference_id
         and effect_to         = I_hts_ref_temp_rec.effect_to
         and effect_from       = I_hts_ref_temp_rec.effect_from
         and import_country_id = I_hts_ref_temp_rec.import_country_id
         and hts               = I_hts_ref_temp_rec.hts
         for update nowait;
BEGIN
   open  HTS_LOCK;
   close HTS_LOCK;

   delete from hts_reference
    where 1 = 1
      and reference_id      = I_hts_ref_temp_rec.reference_id
      and effect_to         = I_hts_ref_temp_rec.effect_to
      and effect_from       = I_hts_ref_temp_rec.effect_from
      and import_country_id = I_hts_ref_temp_rec.import_country_id
      and hts               = I_hts_ref_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_ref_temp_rec.reference_id,
                                            I_hts_ref_temp_rec.hts);
      
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if HTS_LOCK%ISOPEN   then
         close HTS_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_REF_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_REF(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id     IN       SVC_HTS_REF.PROCESS_ID%TYPE,
                         I_chunk_id       IN       SVC_HTS_REF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                       :='CORESVC_HTS_DEFINITION.PROCESS_HTS_REF';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  :='SVC_HTS_REF';
   L_process_error      BOOLEAN                            := FALSE;
   L_hts_ref_temp_rec   HTS_REFERENCE%ROWTYPE;
   L_error              BOOLEAN;
BEGIN
   FOR rec IN C_SVC_HTS_REF(I_process_id,
                            I_chunk_id) LOOP
      L_error         := FALSE;
      L_process_error := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;
      if rec.action = action_new
         and rec.pk_hts_reference_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Reference ID',
                     'DUPLICATE_RECORD');
         L_error := TRUE;
      end if;
      if rec.action IN (action_mod,action_del)
         and rec.pk_hts_reference_rid is NULL
         and rec.hts                  is NOT NULL
         and rec.import_country_id    is NOT NULL
         and rec.effect_from          is NOT NULL
         and rec.effect_to            is NOT NULL
         and rec.reference_id         is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'REFERENCE_ID',
                     'HTSR_INV');
         L_error := TRUE;
      end if;
      if rec.htr_hts_fk_rid        is NULL
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS',
                     'INV_HTS');
         L_error := TRUE;
      end if;
      if rec.action IN (action_new,action_mod)
         and rec.reference_desc is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'REFERENCE_DESC',
                     'NEED_REFERENCE');
         L_error := TRUE;
      end if;
      if NOT L_error then
         L_hts_ref_temp_rec.hts                 := rec.hts;
         L_hts_ref_temp_rec.import_country_id   := rec.import_country_id;
         L_hts_ref_temp_rec.effect_from         := rec.effect_from;
         L_hts_ref_temp_rec.effect_to           := rec.effect_to;
         L_hts_ref_temp_rec.reference_id        := rec.reference_id;
         L_hts_ref_temp_rec.reference_desc      := rec.reference_desc;
         if rec.action = action_new then
            if EXEC_HTS_REF_INS(O_error_message,
                                L_hts_ref_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_HTS_REF_UPD(O_error_message,
                                L_hts_ref_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_HTS_REF_DEL(O_error_message,
                                L_hts_ref_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_REF;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTT_INS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_htt_temp_rec    IN       HTS_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTT_INS';
BEGIN
   insert into hts_tariff_treatment
        values I_htt_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTT_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TT_EXCLUSIONS_INS(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_hts_tt_exclusions_temp_rec   IN       HTS_TT_EXCLUSIONS%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TT_EXCLUSIONS_INS';
BEGIN
   insert into hts_tt_exclusions
        values I_hts_tt_exclusions_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_TT_EXCLUSIONS_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTT_UPD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_htt_temp_rec    IN       HTS_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTT_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TARIFF_TREATMENT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_TARIFF_TREATMENT_LOCK is
      select 'x'
        from hts_tariff_treatment
       where tariff_treatment   = I_HTT_temp_rec.tariff_treatment
         and effect_to          = I_HTT_temp_rec.effect_to
         and effect_from        = I_HTT_temp_rec.effect_from
         and import_country_id  = I_HTT_temp_rec.import_country_id
         and hts                = I_HTT_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_TARIFF_TREATMENT_LOCK;
   close C_HTS_TARIFF_TREATMENT_LOCK;

   update hts_tariff_treatment
      set row = I_htt_temp_rec
    where tariff_treatment  = I_HTT_temp_rec.tariff_treatment
      and effect_to         = I_HTT_temp_rec.effect_to
      and effect_from       = I_HTT_temp_rec.effect_from
      and import_country_id = I_HTT_temp_rec.import_country_id
      and hts               = I_HTT_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_HTT_temp_rec.hts,
                                                               I_HTT_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_HTS_TARIFF_TREATMENT_LOCK%ISOPEN then
         close C_HTS_TARIFF_TREATMENT_LOCK;
      end if;
      return FALSE;
END EXEC_HTT_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTT_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_htt_temp_rec    IN       HTS_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTT_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TARIFF_TREATMENT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_TARIFF_TREATMENT_LOCK is
      select 'x'
        from hts_tariff_treatment
       where tariff_treatment  = I_htt_temp_rec.tariff_treatment
         and effect_to         = I_htt_temp_rec.effect_to
         and effect_from       = I_htt_temp_rec.effect_from
         and import_country_id = I_htt_temp_rec.import_country_id
         and hts               = I_htt_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_TARIFF_TREATMENT_LOCK;
   close C_HTS_TARIFF_TREATMENT_LOCK;

   delete from hts_tariff_treatment
    where tariff_treatment  = I_htt_temp_rec.tariff_treatment
      and effect_to         = I_htt_temp_rec.effect_to
      and effect_from       = I_htt_temp_rec.effect_from
      and import_country_id = I_htt_temp_rec.import_country_id
      and hts               = I_htt_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_HTT_temp_rec.hts,
                                                               I_HTT_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_TARIFF_TREATMENT_LOCK%ISOPEN then
         close C_HTS_TARIFF_TREATMENT_LOCK;
      end if;
      return FALSE;
END EXEC_HTT_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TT_EXCLUSIONS_DEL(O_error_message                IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_hts_tt_exclusions_temp_rec   IN       HTS_TT_EXCLUSIONS%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TT_EXCLUSIONS_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TT_EXCLUSIONS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_TT_EXCLUSIONS_LOCK is
      select 'x'
        from hts_tt_exclusions
       where origin_country_id = I_hts_tt_exclusions_temp_rec.origin_country_id
         and tariff_treatment  = I_hts_tt_exclusions_temp_rec.tariff_treatment
         and effect_to         = I_hts_tt_exclusions_temp_rec.effect_to
         and effect_from       = I_hts_tt_exclusions_temp_rec.effect_from
         and import_country_id = I_hts_tt_exclusions_temp_rec.import_country_id
         and hts               = I_hts_tt_exclusions_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_TT_EXCLUSIONS_LOCK;
   close C_HTS_TT_EXCLUSIONS_LOCK;

   delete from hts_tt_exclusions
    where origin_country_id = I_hts_tt_exclusions_temp_rec.origin_country_id
      and tariff_treatment  = I_hts_tt_exclusions_temp_rec.tariff_treatment
      and effect_to         = I_hts_tt_exclusions_temp_rec.effect_to
      and effect_from       = I_hts_tt_exclusions_temp_rec.effect_from
      and import_country_id = I_hts_tt_exclusions_temp_rec.import_country_id
      and hts               = I_hts_tt_exclusions_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_tt_exclusions_temp_rec.hts,
                                                               I_hts_tt_exclusions_temp_rec.import_country_id);
      
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_TT_EXCLUSIONS_LOCK%ISOPEN   then
         close C_HTS_TT_EXCLUSIONS_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_TT_EXCLUSIONS_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TT_EXCLUSIONS_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_htt_temp_rec    IN       HTS_TARIFF_TREATMENT%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TT_EXCLUSIONS_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TT_EXCLUSIONS';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_TT_EXCLUSIONS_LOCK is
      select 'x'
        from hts_tt_exclusions
       where tariff_treatment  = I_htt_temp_rec.tariff_treatment
         and effect_to         = I_htt_temp_rec.effect_to
         and effect_from       = I_htt_temp_rec.effect_from
         and import_country_id = I_htt_temp_rec.import_country_id
         and hts               = I_htt_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_TT_EXCLUSIONS_LOCK;
   close C_HTS_TT_EXCLUSIONS_LOCK;

   delete from hts_tt_exclusions
    where tariff_treatment  = I_htt_temp_rec.tariff_treatment
      and effect_to         = I_htt_temp_rec.effect_to
      and effect_from       = I_htt_temp_rec.effect_from
      and import_country_id = I_htt_temp_rec.import_country_id
      and hts               = I_htt_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_htt_temp_rec.hts,
                                                               I_htt_temp_rec.import_country_id);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_TT_EXCLUSIONS_LOCK%ISOPEN   then
         close C_HTS_TT_EXCLUSIONS_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_TT_EXCLUSIONS_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_TT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error           IN OUT   BOOLEAN,
                            I_rec             IN OUT   C_SVC_HTT%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_VAL_HTS_TT';
   L_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TARIFF_TREATMENT';
   L_exists    BOOLEAN;

BEGIN
   if I_rec.action IN (action_new,action_mod) then
      if I_rec.specific_rate < 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SPECIFIC_RATE',
                     'NOT_NEG');
         O_error := TRUE;
      end if;

      if I_rec.av_rate < 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'AV_RATE',
                     'NOT_NEG');
         O_error := TRUE;
      end if;

      if I_rec.other_rate < 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'OTHER_RATE',
                     'NOT_NEG');
         O_error := TRUE;
      end if;

      --checking for clearing_zone_id
      if I_rec.import_country_id is NOT NULL then
         if OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message,
                                                               LP_clearing_zone_exists,
                                                               I_rec.import_country_id) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'IMPORT_COUNTRY_ID',
                        O_error_message);
            O_error := TRUE;
         end if;
      end if;

      --validation for modifying rates when zone rate is present
      if I_rec.action = action_mod then
         if LP_clearing_zone_exists = TRUE
            and (NVL(I_rec.specific_rate,-1) <> I_rec.old_specific_rate) then --or directly can compare to 0.0
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SPECIFIC_RATE',
                        'CANNOT_UPD_SPECIFIC_RATE');
             O_error := TRUE;
         end if;

         if LP_clearing_zone_exists = TRUE
            and (NVL(I_rec.av_rate,-1) <> I_rec.old_av_rate) then --or directly can compare to 0.0
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'AV_RATE',
                        'CANNOT_UPD_AV_RATE');
            O_error := TRUE;
         end if;

         if LP_clearing_zone_exists = TRUE
            and (NVL(I_rec.other_rate,-1) <> I_rec.old_other_rate) then --or directly can compare to 0.0
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'OTHER_RATE',
                        'CANNOT_UPD_OTHER_RATE');
            O_error := TRUE;
         end if;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_HTS_TT;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_id      IN       SVC_HTS_TARIFF_TREATMENT.PROCESS_ID%TYPE,
                     I_chunk_id        IN       SVC_HTS_TARIFF_TREATMENT.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                      VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTT';
   L_table                        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TARIFF_TREATMENT';
   L_process_error                BOOLEAN                           := FALSE;
   L_error                        BOOLEAN;
   L_htt_temp_rec                 HTS_TARIFF_TREATMENT%ROWTYPE;
   L_hts_tt_exclusions_temp_rec   HTS_TT_EXCLUSIONS%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_HTT(I_process_id,
                        I_chunk_id) LOOP
      L_error         := FALSE;
      L_process_error := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.pk_hts_tariff_treatment_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment',
                     'DUP_HTS_TARIFF_TREATMENT');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.pk_hts_tariff_treatment_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment',
                     'INV_HTS_TARIFF_TREATMENT');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.htt_hts_fk_rid    is NULL
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment',
                     'HTF_HTS_FK');
         L_error := TRUE;
      end if;

      if rec.htt_ttt_fk_rid is NULL
         and rec.tariff_treatment is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TARIFF_TREATMENT',
                     'INV_HTS_TARIFF_TREATMENT');
         L_error := TRUE;
      end if;

      LP_clearing_zone_exists := FALSE;
      if PROCESS_VAL_HTS_TT(O_error_message,
                            L_error,
                            rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_htt_temp_rec.hts               := rec.hts;
         L_htt_temp_rec.import_country_id := rec.import_country_id;
         L_htt_temp_rec.effect_from       := rec.effect_from;
         L_htt_temp_rec.effect_to         := rec.effect_to;
         L_htt_temp_rec.tariff_treatment  := rec.tariff_treatment;
         L_htt_temp_rec.specific_rate     := NVL(rec.specific_rate,0);
         L_htt_temp_rec.av_rate           := NVL(rec.av_rate,0);
         L_htt_temp_rec.other_rate        := NVL(rec.other_rate,0);
         SAVEPOINT successful_tariff_trt;

         if rec.action = action_new   then
            if LP_clearing_zone_exists = TRUE then
               L_htt_temp_rec.specific_rate := 0.0;
               L_htt_temp_rec.av_rate       := 0.0;
               L_htt_temp_rec.other_rate    := 0.0;
            end if;
         
            if EXEC_HTT_INS(O_error_message,
                         L_htt_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         
         if rec.action = action_mod then
            if EXEC_HTT_UPD(O_error_message,
                            L_htt_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
         
         if rec.action = action_del then
            if EXEC_HTS_TT_EXCLUSIONS_DEL(O_error_message,
                                          L_htt_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
               ROLLBACK TO SAVEPOINT successful_tariff_trt;
            end if;
         
            if NOT L_process_error
               and HTS_CLEAR_ZONE_SQL.DELETE_TARIFF_TREATMENT_ZONE(O_error_message,
                                                                   rec.hts,
                                                                   rec.import_country_id,
                                                                   rec.effect_from,
                                                                   rec.effect_to,
                                                                   rec.tariff_treatment) = FALSE then
               WRITE_ERROR(rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           rec.chunk_id,
                           L_table,
                           rec.row_seq,
                           'HTS,Importing Country,Effect From,Effect To,Tariff Treatment',
                           O_error_message);
               L_process_error := TRUE;
               ROLLBACK TO SAVEPOINT successful_tariff_trt;
            end if;
            
            if NOT L_process_error
               and EXEC_HTT_DEL(O_error_message,
                                L_htt_temp_rec)= FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTT;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_TT_EXCLUSIONS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_process_id      IN       SVC_HTS_TT_EXCLUSIONS.PROCESS_ID%TYPE,
                                   I_chunk_id        IN       SVC_HTS_TT_EXCLUSIONS.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                      VARCHAR2(64)  := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_TT_EXCLUSIONS';
   L_table                        VARCHAR2(255) := 'SVC_HTS_TT_EXCLUSIONS';
   L_process_error                BOOLEAN       := FALSE;
   L_error                        BOOLEAN;
   L_hts_tt_exclusions_temp_rec   HTS_TT_EXCLUSIONS%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_HTS_TT_EXCLUSIONS(I_process_id,
                                      I_chunk_id) LOOP
      L_error         := FALSE;
      L_process_error := FALSE;
      if rec.action NOT IN (action_new,action_del)
         and rec.action is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_HTS_TT_EXCLUSIONS_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment,Country Of Sourcing',
                     'DUP_HTS_TT_EXCLUSIONS');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_HTS_TT_EXCLUSIONS_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment,Country Of Sourcing',
                     'INV_HTS_TT_EXCLUSIONS');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.hte_htt_fk_rid    is NULL
         and rec.hts               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL
         and rec.tariff_treatment  is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment',
                     'INV_HTS_TARIFF_TREATMENT');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.hte_cnt_fk_rid is NULL
         and rec.origin_country_id is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ORIGIN_COUNTRY_ID',
                     'INV_COUNTRY');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.hte_ttt_fk_rid is NULL
         and rec.tariff_treatment is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'TARIFF_TREATMENT',
                     'TARIFF_TREATMENT_MISSING');
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_tt_exclusions_temp_rec.hts               := rec.hts;
         L_hts_tt_exclusions_temp_rec.import_country_id := rec.import_country_id;
         L_hts_tt_exclusions_temp_rec.effect_from       := rec.effect_from;
         L_hts_tt_exclusions_temp_rec.effect_to         := rec.effect_to;
         L_hts_tt_exclusions_temp_rec.tariff_treatment  := rec.tariff_treatment;
         L_hts_tt_exclusions_temp_rec.origin_country_id := rec.origin_country_id;

         if rec.action = action_new   then
            if EXEC_HTS_TT_EXCLUSIONS_INS(O_error_message,
                                          L_hts_tt_exclusions_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_HTS_TT_EXCLUSIONS_DEL(O_error_message,
                                          L_hts_tt_exclusions_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_TT_EXCLUSIONS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TT_ZONE_INS(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hts_tt_zone_temp_rec   IN       HTS_TARIFF_TREATMENT_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TT_ZONE_INS';
BEGIN
   insert into hts_tariff_treatment_zone
       values I_hts_tt_zone_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_TT_ZONE_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TT_ZONE_UPD(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hts_tt_zone_temp_rec   IN       HTS_TARIFF_TREATMENT_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TT_ZONE_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TARIFF_TREATMENT_ZONE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_TT_ZONE_LOCK is
      select 'x'
        from hts_tariff_treatment_zone
       where clearing_zone_id   = I_hts_tt_zone_temp_rec.clearing_zone_id
         and tariff_treatment   = I_hts_tt_zone_temp_rec.tariff_treatment
         and effect_to          = I_hts_tt_zone_temp_rec.effect_to
         and effect_from        = I_hts_tt_zone_temp_rec.effect_from
         and import_country_id  = I_hts_tt_zone_temp_rec.import_country_id
         and hts                = I_hts_tt_zone_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_TT_ZONE_LOCK;
   close C_HTS_TT_ZONE_LOCK;

   update hts_tariff_treatment_zone
      set row = I_hts_tt_zone_temp_rec
    where 1 = 1
      and clearing_zone_id  = I_hts_tt_zone_temp_rec.clearing_zone_id
      and tariff_treatment  = I_hts_tt_zone_temp_rec.tariff_treatment
      and effect_to         = I_hts_tt_zone_temp_rec.effect_to
      and effect_from       = I_hts_tt_zone_temp_rec.effect_from
      and import_country_id = I_hts_tt_zone_temp_rec.import_country_id
      and hts               = I_hts_tt_zone_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_tt_zone_temp_rec.hts,
                                                               I_hts_tt_zone_temp_rec.tariff_treatment);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_HTS_TT_ZONE_LOCK%ISOPEN then
         close C_HTS_TT_ZONE_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_TT_ZONE_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TT_ZONE_DEL(O_error_message          IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_hts_tt_zone_temp_rec   IN       HTS_TARIFF_TREATMENT_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TT_ZONE_DEL';
   L_table        SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TARIFF_TREATMENT_ZONE';
   RECORD_LOCKED  EXCEPTION;
   PRAGMA         EXCEPTION_INIT(Record_Locked,-54);

   cursor C_HTS_TT_ZONE_LOCK is
      select 'x'
        from hts_tariff_treatment_zone
       where clearing_zone_id   = I_hts_tt_zone_temp_rec.clearing_zone_id
         and tariff_treatment   = I_hts_tt_zone_temp_rec.tariff_treatment
         and effect_to          = I_hts_tt_zone_temp_rec.effect_to
         and effect_from        = I_hts_tt_zone_temp_rec.effect_from
         and import_country_id  = I_hts_tt_zone_temp_rec.import_country_id
         and hts                = I_hts_tt_zone_temp_rec.hts
         for update nowait;
BEGIN
   open C_HTS_TT_ZONE_LOCK;
   close C_HTS_TT_ZONE_LOCK;

   delete from hts_tariff_treatment_zone
    where 1 = 1
      and clearing_zone_id  = I_hts_tt_zone_temp_rec.clearing_zone_id
      and tariff_treatment  = I_hts_tt_zone_temp_rec.tariff_treatment
      and effect_to         = I_hts_tt_zone_temp_rec.effect_to
      and effect_from       = I_hts_tt_zone_temp_rec.effect_from
      and import_country_id = I_hts_tt_zone_temp_rec.import_country_id
      and hts               = I_hts_tt_zone_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_hts_tt_zone_temp_rec.hts,
                                                               I_hts_tt_zone_temp_rec.tariff_treatment);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_HTS_TT_ZONE_LOCK%ISOPEN   then
         close C_HTS_TT_ZONE_LOCK;
      end if;
      return FALSE;
END EXEC_HTS_TT_ZONE_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_TT_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_error           IN OUT   BOOLEAN,
                                 O_rec             IN OUT   C_SVC_HTS_TT_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_VAL_HTS_TT_ZONE';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TARIFF_TREATMENT_ZONE';
   L_exist                  BOOLEAN;
   L_clearing_zone_exists   BOOLEAN                           := FALSE;

BEGIN
   if O_rec.action = action_new
      and O_rec.clearing_zone_id  is NOT NULL
      and O_rec.import_country_id is NOT NULL then
      if OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message,
                                                            L_clearing_zone_exists,
                                                            O_rec.import_country_id) = FALSE then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      elsif L_clearing_zone_exists = FALSE then
            WRITE_ERROR(O_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        O_rec.chunk_id,
                        L_table,
                        O_rec.row_seq,
                        'IMPORT_COUNTRY_ID',
                        'CANNOT_CREATE_ZONE_RATES');
            O_error := TRUE;
      elsif OUTSIDE_LOCATION_SQL.GET_DESC(O_error_message,
                                          O_rec.outloc_desc,
                                          O_rec.clearing_zone_id,
                                          'CZ') = FALSE then
            WRITE_ERROR(O_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        O_rec.chunk_id,
                        L_table,
                        O_rec.row_seq,
                        'CLEARING_ZONE_ID',
                        'NO_DESC_CLEARING_ZONE');
            O_error := TRUE;
      end if;
   end if;

   if O_rec.action in (action_new,action_mod) then
      if O_rec.specific_rate is NULL then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'SPECIFIC_RATE',
                     'ENTER_SPECIFIC_RATE');
         O_error := TRUE;
      end if;
   end if;

   if O_rec.action in (action_new,action_mod) then
      if O_rec.av_rate is NULL then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'AV_RATE',
                     'ENTER_AV_RATE');
         O_error := TRUE;
      end if;
   end if;

   if O_rec.action in (action_new,action_mod) then
      if O_rec.other_rate is NULL then
         WRITE_ERROR(O_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     O_rec.chunk_id,
                     L_table,
                     O_rec.row_seq,
                     'OTHER_RATE',
                     'ENTER_OTHER_RATE');
         O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_HTS_TT_ZONE;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_TT_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_HTS_TARIFF_TREATMENT_ZONE.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_HTS_TARIFF_TREATMENT_ZONE.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_TT_ZONE';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TARIFF_TREATMENT_ZONE';
   L_process_error        BOOLEAN                           := FALSE;
   L_error                BOOLEAN;
   L_hts_tt_zone_temp_rec HTS_TARIFF_TREATMENT_ZONE%ROWTYPE;

BEGIN
   FOR rec IN C_SVC_HTS_TT_ZONE(I_process_id,
                                I_chunk_id)LOOP
      L_error       := FALSE;
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.action = action_new
         and rec.pk_hts_tariff_treatment_zo_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment,Clearing Zone',
                     'DUP_HTS_TTZ');
         L_error := TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.pk_hts_tariff_treatment_zo_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment,Clearing Zone',
                     'INV_HTS_TTZ');
         L_error := TRUE;
      end if;

      if rec.action = action_new 
         and rec.haz_htt_fk_rid    is NULL
         and rec.HTS               is NOT NULL
         and rec.import_country_id is NOT NULL
         and rec.effect_from       is NOT NULL
         and rec.effect_to         is NOT NULL
         and rec.tariff_treatment  is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tariff Treatment',
                     'INV_HTS_TTZ');
         L_error := TRUE;
      end if;

      if PROCESS_VAL_HTS_TT_ZONE(O_error_message,
                                 L_error,
                                 rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_tt_zone_temp_rec.hts               := rec.hts;
         L_hts_tt_zone_temp_rec.import_country_id := rec.import_country_id;
         L_hts_tt_zone_temp_rec.effect_from       := rec.effect_from;
         L_hts_tt_zone_temp_rec.effect_to         := rec.effect_to;
         L_hts_tt_zone_temp_rec.tariff_treatment  := rec.tariff_treatment;
         L_hts_tt_zone_temp_rec.clearing_zone_id  := rec.clearing_zone_id;
         L_hts_tt_zone_temp_rec.specific_rate     := rec.specific_rate;
         L_hts_tt_zone_temp_rec.av_rate           := rec.av_rate;
         L_hts_tt_zone_temp_rec.other_rate        := rec.other_rate;

         if rec.action = action_new then
            if EXEC_HTS_TT_ZONE_INS(O_error_message,
                                    L_hts_tt_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_HTS_TT_ZONE_UPD(O_error_message,
                                    L_hts_tt_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_HTS_TT_ZONE_DEL(O_error_message,
                                    L_hts_tt_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_TT_ZONE;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_TAX(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error           IN OUT   BOOLEAN,
                             I_rec             IN OUT   C_SVC_HTS_TAX%ROWTYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_VAL_HTS_TAX';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TAX';
   L_exist                  BOOLEAN;
   L_clearing_zone_exists   BOOLEAN                           := FALSE;
BEGIN
   --check if action is new, mod and no clearing zone exists then both rates cannot be NULL
   if I_rec.action IN (action_new,action_mod) then
      if I_rec.tax_specific_rate < 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TAX_SPECIFIC_RATE',
                     'NOT_NEG');
         O_error := TRUE;
      end if;

      if I_rec.tax_av_rate < 0 then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TAX_AV_RATE',
                     'NOT_NEG');
         O_error := TRUE;
      end if;

      if I_rec.import_country_id is NOT NULL then
         if OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message,
                                                               L_clearing_zone_exists,
                                                               I_rec.import_country_id) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'IMPORT_COUNTRY_ID',
                        O_error_message);
            O_error := TRUE;
         end if;
      end if;

      if L_clearing_zone_exists = FALSE
         and (I_rec.tax_specific_rate is NULL
         and I_rec.tax_av_rate is NULL) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Specific rate,Av rate',
                     'ENT_ATLEAST_ONE_RATE');
         O_error := TRUE;
      end if;
   end if;

   --check if action is new, mod and clearing zone exists then both rates must be set to 0.0
   if I_rec.action = action_new then
      if OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message,
                                                            L_clearing_zone_exists,
                                                            I_rec.import_country_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     O_error_message);
         O_error := TRUE;
      end if;
      if L_clearing_zone_exists = TRUE then
         I_rec.tax_specific_rate:=0.0;
         I_rec.tax_av_rate:=0.0;
      end if;
   end if;

   --check if action is mod and clearing zone exists then rates cannot be updated to any value other than 0.0
   if I_rec.action = action_mod then
      if OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message,
                                                            L_clearing_zone_exists,
                                                            I_rec.import_country_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'IMPORT_COUNTRY_ID',
                     O_error_message);
         O_error := TRUE;
      end if;

      if L_clearing_zone_exists = TRUE
         and (I_rec.tax_specific_rate <> 0.0) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TAX_SPECIFIC_RATE',
                     'CANNOT_UPD_TAXSPEC_RATE');
         O_error := TRUE;
      end if;

      if L_clearing_zone_exists = TRUE
         and (I_rec.tax_av_rate <> 0.0) then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'TAX_AV_RATE',
                     'CANNOT_UPD_TAX_AV_RATE');
         O_error := TRUE;
      end if;
   end if;

   --check if action is del, then delete records from HTS_TAX_ZONE
   if I_rec.action = action_del then
      if HTS_CLEAR_ZONE_SQL.DELETE_TAX_ZONE(O_error_message,
                                            I_rec.hts,
                                            I_rec.import_country_id,
                                            I_rec.effect_from,
                                            I_rec.effect_to,
                                            I_rec.tax_type) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_HTS_TAX;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TAX_INS(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_tax_temp_rec   IN       HTS_TAX%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TAX_INS';
BEGIN
   insert into hts_tax
      values I_hts_tax_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_TAX_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TAX_UPD(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_tax_temp_rec   IN       HTS_TAX%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TAX_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TAX';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_HTS_TAX_UPD is
      select 'x'
        from hts_tax
       where tax_type          = I_hts_tax_temp_rec.tax_type
         and effect_to         = I_hts_tax_temp_rec.effect_to
         and effect_from       = I_hts_tax_temp_rec.effect_from
         and import_country_id = I_hts_tax_temp_rec.import_country_id
         and hts               = I_hts_tax_temp_rec.hts
         for update nowait;
BEGIN
   open  C_LOCK_HTS_TAX_UPD;
   close C_LOCK_HTS_TAX_UPD;

   update hts_tax
      set row = I_hts_tax_temp_rec
    where 1 = 1
      and tax_type          = I_hts_tax_temp_rec.tax_type
      and effect_to         = I_hts_tax_temp_rec.effect_to
      and effect_from       = I_hts_tax_temp_rec.effect_from
      and import_country_id = I_hts_tax_temp_rec.import_country_id
      and hts               = I_hts_tax_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_tax_temp_rec.hts,
                                            I_hts_tax_temp_rec.tax_type);
      close C_LOCK_HTS_TAX_UPD;
      return FALSE;
   when OTHERS then
      if C_LOCK_HTS_TAX_UPD%ISOPEN   then
         close C_LOCK_HTS_TAX_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_TAX_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TAX_DEL(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_hts_tax_temp_rec   IN       HTS_TAX%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TAX_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TAX';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_HTS_TAX_DEL is
      select 'x'
        from hts_tax
       where tax_type          = I_hts_tax_temp_rec.tax_type
         and effect_to         = I_hts_tax_temp_rec.effect_to
         and effect_from       = I_hts_tax_temp_rec.effect_from
         and import_country_id = I_hts_tax_temp_rec.import_country_id
         and hts               = I_hts_tax_temp_rec.hts
         for update nowait;
BEGIN 
   open  C_LOCK_HTS_TAX_DEL;
   close C_LOCK_HTS_TAX_DEL;

   delete from hts_tax
    where 1 = 1
      and tax_type          = I_hts_tax_temp_rec.tax_type
      and effect_to         = I_hts_tax_temp_rec.effect_to
      and effect_from       = I_hts_tax_temp_rec.effect_from
      and import_country_id = I_hts_tax_temp_rec.import_country_id
      and hts               = I_hts_tax_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_tax_temp_rec.hts,
                                            I_hts_tax_temp_rec.tax_type);
      close C_LOCK_HTS_TAX_DEL;
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      if C_LOCK_HTS_TAX_DEL%ISOPEN   then
         close C_LOCK_HTS_TAX_DEL;
      end if;
      return FALSE;
END EXEC_HTS_TAX_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_TAX(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_HTS_TAX.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_HTS_TAX.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_TAX';
   L_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TAX';
   L_process_error      BOOLEAN                           := FALSE;
   L_error              BOOLEAN;
   L_exist              BOOLEAN;
   L_hts_tax_temp_rec   HTS_TAX%ROWTYPE;
BEGIN
   FOR rec IN C_SVC_HTS_TAX(I_process_id,
                            I_chunk_id) LOOP
      L_error := FALSE;
      --check if action is NULL or other than new,mod,del
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
          WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      'ACTION',
                      'INV_ACT');
          L_error := TRUE;
       end if;

       --check if action is new and if tax_type already exists for the HTS
      if rec.action = action_new
         and rec.PK_HTS_TAX_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tax Type',
                     'HTS_TAX_EXISTS');
         L_error := TRUE;
      end if;

      --check if action is mod/del and if tax_type does not exist
      if rec.action IN (action_mod,action_del)
         and rec.PK_HTS_TAX_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tax Type',
                     'HTS_TAX_MISSING');
         L_error := TRUE;
      end if;

      --check if HTS does not exist for new/mod/del
      if rec.HTX_HTS_FK_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS',
                     'INV_HTS');
         L_error := TRUE;
      end if;

      --check if tax_comp_code is NULL
      if rec.action NOT IN (action_del) then
         if NOT(rec.tax_comp_code is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        'TAX_COMP_CODE',
                        'NEED_TAX_COMP_CODE');
            L_error := TRUE;
         end if;
      end if;

      if PROCESS_VAL_HTS_TAX(O_error_message,
                             L_error,
                             rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_tax_temp_rec.hts                := rec.hts;
         L_hts_tax_temp_rec.import_country_id  := rec.import_country_id;
         L_hts_tax_temp_rec.effect_from        := rec.effect_from;
         L_hts_tax_temp_rec.effect_to          := rec.effect_to;
         L_hts_tax_temp_rec.tax_type           := rec.tax_type;
         L_hts_tax_temp_rec.tax_comp_code      := rec.tax_comp_code;
         L_hts_tax_temp_rec.tax_av_rate        := NVL(rec.tax_av_rate,0);
         L_hts_tax_temp_rec.tax_specific_rate  := NVL(rec.tax_specific_rate,0);

         if rec.action = action_new then
            if EXEC_HTS_TAX_INS(O_error_message,
                                L_hts_tax_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
                L_process_error := TRUE;
            end if; 
         end if;

         if rec.action = action_mod then
            if EXEC_HTS_TAX_UPD(O_error_message,
                                L_hts_tax_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if; 
         end if;

         if rec.action = action_del then
            if EXEC_HTS_TAX_DEL(O_error_message,
                                L_hts_tax_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
                L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_HTS_TAX;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_HTS_TAX_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error           IN OUT   BOOLEAN,
                                  I_rec             IN OUT   C_SVC_HTS_TAX_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_VAL_HTS_TAX_ZONE';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TAX_ZONE';
   L_exist                  BOOLEAN;
   L_clearing_zone_exists   BOOLEAN                           := FALSE;

BEGIN
   --check if description for clearing_zone_id exists
   if I_rec.action = action_new
      and I_rec.clearing_zone_id  is NOT NULL
      and I_rec.import_country_id is NOT NULL then
      if OUTSIDE_LOCATION_SQL.CLEAR_ZONE_IMP_COUNTRY_EXISTS(O_error_message,
                                                            L_clearing_zone_exists,
                                                            I_rec.import_country_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      elsif L_clearing_zone_exists = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'IMPORT_COUNTRY_ID',
                        'CANNOT_CREATE_ZONE_RATES');
            O_error := TRUE;
      elsif OUTSIDE_LOCATION_SQL.GET_DESC(O_error_message,
                                          I_rec.outloc_desc,
                                          I_rec.clearing_zone_id,
                                          'CZ') = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'CLEARING_ZONE_ID',
                        'NO_DESC_CLEARING_ZONE');
            O_error := TRUE;
      end if;
   end if;

   --check if both rates are NULL
   if I_rec.action in (action_new,action_mod) then
      if I_rec.tax_specific_rate is NULL
         and I_rec.tax_av_rate is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Specific rate,AV rate',
                     'ENT_ATLEAST_ONE_RATE');
         O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_VAL_HTS_TAX_ZONE;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TAX_ZONE_INS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_tax_zone_temp_rec   IN       HTS_TAX_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TAX_ZONE_INS';
BEGIN
   insert into hts_tax_zone
       values I_hts_tax_zone_temp_rec;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_TAX_ZONE_INS;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TAX_ZONE_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_tax_zone_temp_rec   IN       HTS_TAX_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TAX_ZONE_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TAX_ZONE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_HTS_TZ_UPD is
      select 'x'
        from hts_tax_zone
       where clearing_zone_id   = I_hts_tax_zone_temp_rec.clearing_zone_id
         and tax_type           = I_hts_tax_zone_temp_rec.tax_type
         and effect_to          = I_hts_tax_zone_temp_rec.effect_to
         and effect_from        = I_hts_tax_zone_temp_rec.effect_from
         and import_country_id  = I_hts_tax_zone_temp_rec.import_country_id
         and hts                = I_hts_tax_zone_temp_rec.hts
         for update nowait;
BEGIN
   open  C_LOCK_HTS_TZ_UPD;
   close C_LOCK_HTS_TZ_UPD;

   update hts_tax_zone
      set row = I_hts_tax_zone_temp_rec
    where 1 = 1
      and clearing_zone_id   = I_hts_tax_zone_temp_rec.clearing_zone_id
      and tax_type           = I_hts_tax_zone_temp_rec.tax_type
      and effect_to          = I_hts_tax_zone_temp_rec.effect_to
      and effect_from        = I_hts_tax_zone_temp_rec.effect_from
      and import_country_id  = I_hts_tax_zone_temp_rec.import_country_id
      and hts                = I_hts_tax_zone_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_LOCK_HTS_TZ_UPD;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_tax_zone_temp_rec.clearing_zone_id,
                                            I_hts_tax_zone_temp_rec.tax_type);
      return FALSE;
   when OTHERS then
      if C_LOCK_HTS_TZ_UPD%ISOPEN then
         close C_LOCK_HTS_TZ_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_TAX_ZONE_UPD;
-----------------------------------------------------------------------------------------------
FUNCTION EXEC_HTS_TAX_ZONE_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_hts_tax_zone_temp_rec   IN       HTS_TAX_ZONE%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.EXEC_HTS_TAX_ZONE_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'HTS_TAX_ZONE';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_HTS_TZ_DEL is
      select 'x'
        from hts_tax_zone
       where clearing_zone_id   = I_hts_tax_zone_temp_rec.clearing_zone_id
         and tax_type           = I_hts_tax_zone_temp_rec.tax_type
         and effect_to          = I_hts_tax_zone_temp_rec.effect_to
         and effect_from        = I_hts_tax_zone_temp_rec.effect_from
         and import_country_id  = I_hts_tax_zone_temp_rec.import_country_id
         and hts                = I_hts_tax_zone_temp_rec.hts
         for update nowait;
BEGIN 
   open C_LOCK_HTS_TZ_DEL;
   close C_LOCK_HTS_TZ_DEL;

   delete from hts_tax_zone
    where 1 = 1
      and clearing_zone_id   = I_hts_tax_zone_temp_rec.clearing_zone_id
      and tax_type           = I_hts_tax_zone_temp_rec.tax_type
      and effect_to          = I_hts_tax_zone_temp_rec.effect_to
      and effect_from        = I_hts_tax_zone_temp_rec.effect_from
      and import_country_id  = I_hts_tax_zone_temp_rec.import_country_id
      and hts                = I_hts_tax_zone_temp_rec.hts;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      close C_LOCK_HTS_TZ_DEL;
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_hts_tax_zone_temp_rec.clearing_zone_id,
                                            I_hts_tax_zone_temp_rec.tax_type);
      return FALSE;
   when OTHERS then
      if C_LOCK_HTS_TZ_DEL%ISOPEN then
         close C_LOCK_HTS_TZ_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_HTS_TAX_ZONE_DEL;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS_HTS_TAX_ZONE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_HTS_TAX_ZONE.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_HTS_TAX_ZONE.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64)                      := 'CORESVC_HTS_DEFINITION.PROCESS_HTS_TAX_ZONE';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_HTS_TAX_ZONE';
   L_process_error           BOOLEAN                           := FALSE;
   L_error                   BOOLEAN;
   L_hts_tax_zone_temp_rec   HTS_TAX_ZONE%ROWTYPE;

BEGIN
   FOR rec IN C_SVC_HTS_TAX_ZONE(I_process_id,
                                 I_chunk_id) LOOP
      L_error := FALSE;
      --check if action is NULL or other than new, mod,del
      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
          WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      'ACTION',
                      'INV_ACT');
          L_error := TRUE;
      end if;

      --check if clearing_zone for HTS_TAX already exists
      if rec.action = action_new
         and rec.PK_HTS_TAX_ZONE_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tax Type,Clearing Zone',
                     'HTS_TAX_ZONE_EXIST');
         L_error := TRUE;
      end if;

      --check if action is mod,del and if record does NOT exist
      if rec.action IN (action_mod,action_del)
         and rec.PK_HTS_TAX_ZONE_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tax Type,Clearing Zone',
                     'HTS_TAX_ZONE_MISSING');
         L_error := TRUE;
      end if;

      --check if HTS_TAX exists
      if rec.HTZ_HTX_FK_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'HTS,Importing Country,Effect From,Effect To,Tax Type',
                     'HTS_TAX_MISSING');
         L_error := TRUE;
      end if;

      if PROCESS_VAL_HTS_TAX_ZONE(O_error_message,
                                  L_error,
                                  rec) = FALSE then
          WRITE_ERROR(I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
          L_error := TRUE;
      end if;

      if NOT L_error then
         L_hts_tax_zone_temp_rec.hts                := rec.hts;
         L_hts_tax_zone_temp_rec.import_country_id  := rec.import_country_id;
         L_hts_tax_zone_temp_rec.effect_from        := rec.effect_from;
         L_hts_tax_zone_temp_rec.effect_to          := rec.effect_to;
         L_hts_tax_zone_temp_rec.tax_type           := rec.tax_type;
         L_hts_tax_zone_temp_rec.clearing_zone_id   := rec.clearing_zone_id;
         L_hts_tax_zone_temp_rec.tax_specific_rate  := NVL(rec.tax_specific_rate,0);
         L_hts_tax_zone_temp_rec.tax_av_rate        := NVL(rec.tax_av_rate,0);
      
         if rec.action = action_new then
            if EXEC_HTS_TAX_ZONE_INS(O_error_message,
                                     L_hts_tax_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
                L_process_error := TRUE;
            end if; 
         end if;
      
         if rec.action = action_mod then
            if EXEC_HTS_TAX_ZONE_UPD(O_error_message,
                                     L_hts_tax_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if; 
         end if;
      
         if rec.action = action_del then
            if EXEC_HTS_TAX_ZONE_DEL(O_error_message,
                                     L_hts_tax_zone_temp_rec) = FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
      end if;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_HTS_TAX_ZONE;
-----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS
BEGIN

   delete
     from svc_hts_tl
    where process_id = I_process_id;

   delete
     from svc_hts
    where process_id = I_process_id;
    
   delete
     from svc_hts_ad
    where process_id = I_process_id;
   
   delete
     from svc_hts_cvd
    where process_id = I_process_id;
   
   delete
     from svc_hts_fee
    where process_id = I_process_id;

   delete
     from svc_hts_fee_zone
    where process_id = I_process_id;

   delete
     from svc_hts_oga
    where process_id = I_process_id;

   delete
     from svc_hts_ref
    where process_id = I_process_id;

   delete
     from svc_hts_tariff_treatment
    where process_id = I_process_id;

   delete
     from svc_hts_tt_exclusions
    where process_id = I_process_id;

   delete
     from svc_hts_tariff_treatment_zone
    where process_id = I_process_id;

   delete
     from svc_hts_tax
    where process_id = I_process_id;

   delete
     from svc_hts_tax_zone
    where process_id = I_process_id;
END;
-----------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64)                    := 'CORESVC_HTS_DEFINITION.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW LP_errors_tab_typ();

   if PROCESS_HTS(O_error_message,
                  I_process_id,
                  I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_TL(O_error_message,
                     I_process_id,
                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTT(O_error_message,
                  I_process_id,
                  I_chunk_id) and PROCESS_HTS_TT_EXCLUSIONS(O_error_message,
                                                            I_process_id,
                                                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_TT_ZONE(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_FEE(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
       return FALSE;
   end if;

   if PROCESS_HTS_FEE_ZONE(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_TAX(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_TAX_ZONE(O_error_message,
                           I_process_id,
                           I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_AD(O_error_message,
                     I_process_id,
                     I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_CVD(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_REF(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if PROCESS_HTS_OGA(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW LP_errors_tab_typ();

   open  C_GET_ERR_COUNT;
   fetch C_GET_ERR_COUNT into L_err_count;
   close C_GET_ERR_COUNT;

   open  C_GET_WARN_COUNT;
   fetch C_GET_WARN_COUNT into L_warn_count;
   close C_GET_WARN_COUNT;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = sysdate
    where process_id = I_process_id;

   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
-----------------------------------------------------------------------------------------------
END CORESVC_HTS_DEFINITION;
/