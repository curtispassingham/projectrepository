
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE CE_ALLOC_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------
FUNCTION ALLOC_CE(O_error_message         IN OUT VARCHAR2,
                  I_obligation_key        IN     OBLIGATION.OBLIGATION_KEY%TYPE,
                  I_obligation_level      IN     OBLIGATION.OBLIGATION_LEVEL%TYPE,
                  I_entry_no              IN     CE_HEAD.ENTRY_NO%TYPE,
                  I_comp_id               IN     ELC_COMP.COMP_ID%TYPE,
                  I_alloc_basis_uom       IN     UOM_CLASS.UOM%TYPE,
                  I_qty                   IN     OBLIGATION_COMP.QTY%TYPE,
                  I_amt_prim              IN     OBLIGATION_COMP.AMT%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------
FUNCTION ALLOC_CE_DETAIL(O_error_message IN OUT VARCHAR2,
                         I_ce_id         IN     CE_HEAD.CE_ID%TYPE,
                         I_entry_no      IN     CE_HEAD.ENTRY_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------
FUNCTION CE_INVC_NON_MERCH ( O_error_message     IN OUT   VARCHAR2,
                             I_invc_id           IN       INVC_HEAD.INVC_ID%TYPE  ,
                             I_ce_id             IN       CE_HEAD.CE_ID%TYPE,
                             I_Loc               IN       ORDLOC.LOCATION%TYPE
                           )
      RETURN BOOLEAN ;

END CE_ALLOC_SQL;
/
