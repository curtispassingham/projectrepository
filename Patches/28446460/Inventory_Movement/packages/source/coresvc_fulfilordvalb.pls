CREATE OR REPLACE PACKAGE BODY CORESVC_FULFILORD_VALIDATE AS

   LP_user   SVCPROV_CONTEXT.USER_NAME%TYPE   := GET_USER;
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_FULFILORD
-- Purpose: This is a private function that performs basic validation of staged header fulfillment
--          records on SVC_FULFILORD. It should process all messages on the staging table for a
--          given I_process_id and I_chunk_id in 'N'ot processed status.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_FULFILORDDTL
-- Purpose: This is a private function that performs basic validation of staged fulfillment item detail
--          records on SVC_FULFILORDDTL. It should process all messages on the staging table for a
--          given I_process_id and I_chunk_id in 'N'ot processed status.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORDDTL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_FULFILORDDTL.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORDDTL.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_FULFILORDCUST
-- Purpose: This is a private function that performs basic validation of staged fulfillment customer
--          detail records on SVC_FULFILORDCUST. It should process all messages on the staging table
--          for a given I_process_id and I_chunk_id in 'N'ot processed status.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORDCUST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDCUST.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDCUST.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_FULFILORDREF
-- Purpose: This is a private function that performs basic validation of header information related
--          to staged fulfillment cancellation requests on SVC_FULFILORDREF.  It should process all
--          messages on the staging table for a given I_process_id and I_chunk_id in 'N'ot processed status.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORDREF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Name   : CHECK_FULFILORDDTLREF
-- Purpose: This is a private function that performs basic validation of item details related to
--          staged fulfillment cancel requests on SVC_FULFILORDDTLREF. It should process all messages
--          on the staging table for a given I_process_id and I_chunk_id in 'N'ot processed status.
---------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORDDTLREF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_FULFILORDDTLREF.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_FULFILORDDTLREF.CHUNK_ID%TYPE)
RETURN BOOLEAN;

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE_CREATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'CORESVC_FULFILORD_VALIDATE.CHECK_MESSAGE_CREATE';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_l10n_obj      L10N_OBJ := L10N_OBJ();

   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and process_status <> CORESVC_FULFILORD.PROCESS_STATUS_ERROR
         for update nowait;

   cursor C_LOCALIZED_COUNTRY is
      select country_id
        from country_attrib
       where localized_ind = 'Y';

BEGIN
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chunk_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if CHECK_FULFILORD(O_error_message,
                      I_process_id,
                      I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if CHECK_FULFILORDDTL(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if CHECK_FULFILORDCUST(O_error_message,
                          I_process_id,
                          I_chunk_id) = FALSE then
      return FALSE;
   end if;

   FOR rec in C_LOCALIZED_COUNTRY LOOP
      L_l10n_obj.procedure_key := 'VALIDATE_SVC_FULFILORD_L10N';
      L_l10n_obj.country_id := rec.country_id;
      L_l10n_obj.process_id := I_process_id;
      L_l10n_obj.chunk_id := I_chunk_id;

      if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                L_l10n_obj) = FALSE then
         return FALSE;
      end if;
   END LOOP;

   L_table := 'SVC_FULFILORD';
   open C_LOCK_SVC_FULFILORD;
   close C_LOCK_SVC_FULFILORD;

   update svc_fulfilord
      set process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED,
          last_update_id = LP_user,
          last_update_datetime = sysdate
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status <> CORESVC_FULFILORD.PROCESS_STATUS_ERROR;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE_CREATE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE_CANCEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                              I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50) := 'CORESVC_FULFILORD_VALIDATE.CHECK_MESSAGE_CANCEL';
   L_table         VARCHAR2(30);
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_FULFILORDREF is
      select 'x'
        from svc_fulfilordref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and process_status <> CORESVC_FULFILORD.PROCESS_STATUS_ERROR
         for update nowait;

BEGIN
   if I_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_process_id',
                                            L_program,
                                            NULL);
      return FALSE;
   elsif I_chunk_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_chunk_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if CHECK_FULFILORDREF(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;


   if CHECK_FULFILORDDTLREF(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   L_table := 'SVC_FULFILORDREF';
   open C_LOCK_SVC_FULFILORDREF;
   close C_LOCK_SVC_FULFILORDREF;

   update svc_fulfilordref
      set process_status = CORESVC_FULFILORD.PROCESS_STATUS_VALIDATED,
          last_update_id = LP_user,
          last_update_datetime = sysdate
    where process_id = I_process_id
      and chunk_id = I_chunk_id
      and process_status <> CORESVC_FULFILORD.PROCESS_STATUS_ERROR;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE_CANCEL;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       SVC_FULFILORD.PROCESS_ID%TYPE,
                         I_chunk_id        IN       SVC_FULFILORD.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'CORESVC_FULFILORD_VALIDATE.CHECK_FULFILORD';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_vdate           PERIOD.VDATE%TYPE := GET_VDATE;

BEGIN
   merge into svc_fulfilord sfo
   using (select *
            from (select svfo.process_id,
                         svfo.chunk_id,
                         svfo.fulfilord_id,
                         -- validate that fulfill_loc_type is 'S' (physical store) or 'V' (virtual store)
                         case
                            when svfo.fulfill_loc_type NOT in ('S', 'V') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_FULFILL_LOC_TYPE', svfo.fulfill_loc_type, svfo.customer_order_no, svfo.fulfill_order_no)||';'
                         end ||
                         -- validate that partial_delivery_ind is 'Y' or 'N'
                         case
                            when svfo.partial_delivery_ind NOT in ('Y', 'N') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_PARTIAL_DELIVERY_IND', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that delivery_type is 'S' (ship direct) or 'C' (customer pickup)
                         case
                            when svfo.delivery_type NOT in ('S', 'C') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_DELIVERY_TYPE', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that customer details exists
                         case
                            when NOT exists (select 'x'
                                                                             from svc_fulfilordcust sfd
                                                                            where sfd.fulfilord_id = svfo.fulfilord_id
                                                                              and sfd.process_id = svfo.process_id
                                                                              and sfd.chunk_id = svfo.chunk_id) then
                               SQL_LIB.GET_MESSAGE_TEXT('CUST_DETAILS_MISSING', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||                                                                              
                 -- validate that if fulfill_loc_type is 'S', fulfill_loc_id is a valid customer orderable company/franchise store with stockholding_ind = 'Y'
                         -- if fulfill_loc_type is 'V', fulfill_loc_id is a valid customer orderable company store with stockholding_ind = 'N'
                         case
                            when svfo.fulfill_loc_type = 'S' and NVL(fulfill_loc_tbl.stockholding_ind, '-999') <> 'Y' then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_FULFILL_LOC_ID_S', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                            when svfo.fulfill_loc_type = 'V' and (NVL(fulfill_loc_tbl.stockholding_ind, '-999') <> 'N' or NVL(fulfill_loc_tbl.store_type, '-999') <> 'C') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_FULFILL_LOC_ID_V', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that source_loc_type and source_loc_id are populated or both are NULL
                         case
                            when svfo.source_loc_type is NULL and svfo.source_loc_id is NOT NULL or
                                 svfo.source_loc_type is NOT NULL and svfo.source_loc_id is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('SOURCE_LOC_TYPE_LOC_ID', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that source_loc_id and fulfill_loc_id are not the same
                         -- supplier ID can overlap with store or warehouse ids, so exclude that
                         case
                            when svfo.source_loc_type != 'SU' and svfo.source_loc_id = svfo.fulfill_loc_id then
                               SQL_LIB.GET_MESSAGE_TEXT('SRC_FULFILL_LOC_NOT_SAME', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that the source_loc_type is 'SU' or 'ST' or 'WH' if defined
                         case
                            when svfo.source_loc_type is NOT NULL and svfo.source_loc_type NOT in ('SU', 'ST', 'WH') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_SOURCE_LOC_TYPE', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that the source_loc_id is a valid supplier_id on SUPS table if source_loc_type is 'SU'
                         -- source_loc_id is a valid stockholding customer orderable company/franchise store on STORE table if source_loc_type is 'ST'
                         -- source_loc_id is a valid physical wh on WH table if source_loc_type is 'WH'
                         case
                            when svfo.source_loc_id is NOT NULL and source_loc_tbl.phys_loc is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_SOURCE_LOC_ID', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that delivery_charges and delivery_charges_curr are either both defined or both NULL
                         case
                            when svfo.delivery_charges is NULL and svfo.delivery_charges_curr is NOT NULL or
                                 svfo.delivery_charges is NOT NULL and svfo.delivery_charges_curr is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('DELIV_CHARGES_CURR', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that delivery_charge is greater than zero
                         case
                            when svfo.delivery_charges <= 0 then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_DELIVERY_CHARGE', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate the currency_code
                         case
                            when svfo.delivery_charges_curr is NOT NULL and cur.currency_code is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_DELIV_CURR_CODE', svfo.delivery_charges_curr, svfo.customer_order_no, svfo.fulfill_order_no)||';'
                         end ||
                         -- validate that consumer_delivery_date and consumer_delivery_time are not both populated at the same time
                         case
                            when svfo.consumer_delivery_date is NOT NULL and svfo.consumer_delivery_time is NOT NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('DELIV_DATE_TIME_NOT_NULL', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that carrier_service_code is a valid code on code_detail for code_type = 'CSVC'
                         case
                            when svfo.carrier_service_code is NOT NULL and cd.code is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_CARRIER_SVC_CODE', svfo.carrier_service_code, svfo.customer_order_no, svfo.fulfill_order_no)||';'
                         end ||
                         -- validate that at least one detail exists in svc_fulfilorddtl for a given svc_fulfilord header record
                         case
                            when NOT exists (select 'x'
                                               from svc_fulfilorddtl sfd
                                              where sfd.fulfilord_id = svfo.fulfilord_id
                                                and sfd.process_id = svfo.process_id
                                                and sfd.chunk_id = svfo.chunk_id
                                                and rownum = 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('DETAIL_RECORD_REQ', svfo.customer_order_no, svfo.fulfill_order_no)||';'
                         end ||
                         -- validate that the records in the detail table, svc_fulfilorddtl, have unique process_id/chunk_id/fulfilord_id/item combinations
                         case
                            when exists (select 'x'
                                           from svc_fulfilorddtl sfd
                                          where sfd.fulfilord_id = svfo.fulfilord_id
                                            and sfd.process_id = svfo.process_id
                                            and sfd.chunk_id = svfo.chunk_id
                                          group by sfd.process_id,
                                                sfd.chunk_id,
                                                sfd.fulfilord_id,
                                                sfd.item
                                         having count(sfd.item) > 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('UNIQUE_ITEM_PER_CO', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that for each customer order, there is at most one corresponding customer record
                         case
                            when (select count(sfc.fulfilord_id)
                                    from svc_fulfilordcust sfc
                                   where sfc.fulfilord_id = svfo.fulfilord_id
                                     and sfc.process_id = svfo.process_id
                                     and sfc.chunk_id = svfo.chunk_id) > 1 then
                               SQL_LIB.GET_MESSAGE_TEXT('MAX_ONE_CUSTOMER_REC', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- when source_loc_type is 'WH' or 'SU' and fulfill_loc_type = 'V' one svc_fulfilordcust
                         -- record should exist for the same process_id/chunk_id/fulfilord_id
                         case
                            when svfo.source_loc_type in ('WH','SU') and
                                 svfo.fulfill_loc_type = 'V' and
                                (select count(sfc.fulfilord_id)
                                   from svc_fulfilordcust sfc
                                  where sfc.fulfilord_id = svfo.fulfilord_id
                                    and sfc.process_id = svfo.process_id
                                    and sfc.chunk_id = svfo.chunk_id) < 1 then
                               SQL_LIB.GET_MESSAGE_TEXT('CUSTOMER_RECORD_REQ', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- customer order no/fulfill order no/source loc id/fulfill loc id combination must be unique within the staging table
                         case
                            when count(svfo.fulfilord_id) over (partition by svfo.customer_order_no,
                                                                             svfo.fulfill_order_no,
                                                                             svfo.source_loc_type,
                                                                             svfo.source_loc_id,
                                                                             svfo.fulfill_loc_type,
                                                                             svfo.fulfill_loc_id) > 1 then
                               SQL_LIB.GET_MESSAGE_TEXT('DUPLICATE_CO_RECORD', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end ||
                         -- customer order no/fulfill order no/source loc id/fulfill loc id combination must not exist in ordcust
                         case
                            when exists (select 'x'
                                           from ordcust oc
                                          where oc.customer_order_no = svfo.customer_order_no
                                            and oc.fulfill_order_no = svfo.fulfill_order_no
                                            and NVL(oc.source_loc_type, '-999') = NVL(svfo.source_loc_type, '-999')
                                            and NVL(oc.source_loc_id, -999) = NVL(svfo.source_loc_id, -999)
                                            and oc.fulfill_loc_type = svfo.fulfill_loc_type
                                            and oc.fulfill_loc_id = svfo.fulfill_loc_id
                                            and rownum = 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('ORDCUST_REC_EXISTS_CO', svfo.customer_order_no, svfo.fulfill_order_no, NULL)||';'
                         end error_msg
                    from svc_fulfilord svfo,
                         (-- stockholding company/franchise store that is customer orderable
                          select 'ST' source_loc_type,
                                 store phys_loc
                            from store
                           where stockholding_ind = 'Y'
                             and customer_order_loc_ind = 'Y'
                           union all
                          -- physical warehouse
                          select 'WH' source_loc_type,
                                 wh phys_loc
                            from wh
                           union all
                          -- supplier
                          select 'SU' source_loc_type,
                                 supplier phys_loc
                            from sups) source_loc_tbl,
                         (select store,
                                 store_type,
                                 stockholding_ind
                            from store
                           where customer_order_loc_ind = 'Y') fulfill_loc_tbl,
                         currencies cur,
                         code_detail cd
                   where svfo.process_id = I_process_id
                     and svfo.chunk_id = I_chunk_id
                     and svfo.fulfill_loc_id = fulfill_loc_tbl.store(+)
                     and svfo.source_loc_id = source_loc_tbl.phys_loc(+)
                     and svfo.source_loc_type = source_loc_tbl.source_loc_type(+)
                     and svfo.delivery_charges_curr = cur.currency_code(+)
                     and svfo.carrier_service_code = cd.code(+)
                     and cd.code_type(+) = 'CSVC'
                     and svfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW)
           where error_msg is NOT NULL) inner
   on (sfo.process_id = inner.process_id
       and sfo.chunk_id = inner.chunk_id
       and sfo.fulfilord_id = inner.fulfilord_id)
   when matched then
      update set sfo.error_msg = substr(sfo.error_msg || inner.error_msg, 1, 2000),
                 sfo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
                 sfo.last_update_id = LP_user,
                 sfo.last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      select substr(substr(error_msg, 1, instr(error_msg, ';')-1), 1, 255)
        into L_error_message
        from svc_fulfilord
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FULFILORD;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORDDTL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_FULFILORDDTL.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORDDTL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'CORESVC_FULFILORD_VALIDATE.CHECK_FULFILORDDTL';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord sf
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtl sd
                      where sd.error_msg is NOT NULL
                        and sd.process_id = sf.process_id
                        and sd.chunk_id = sf.chunk_id
                        and sd.fulfilord_id = sf.fulfilord_id
                        and rownum = 1)
         for update nowait;

BEGIN
   merge into svc_fulfilorddtl sfd
   using (select *
            from (select svfd.process_id,
                         svfd.chunk_id,
                         svfd.fulfilord_id,
                         svfd.item,
                         -- validate that item is valid in RMS
                         case
                            when itm.item is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_FULFILL_ITEM', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is at transaction level
                         case
                            when itm.item_level <> itm.tran_level then
                               SQL_LIB.GET_MESSAGE_TEXT('ITEM_NOT_TRAN_LEVEL', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is approved
                         case
                            when itm.status <> 'A' then
                               SQL_LIB.GET_MESSAGE_TEXT('FULFILL_ITEM_NOT_APPROVED', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is sellable
                         case
                            when itm.sellable_ind = 'N' then
                               SQL_LIB.GET_MESSAGE_TEXT('FULFILL_ITEM_NOT_SELLABLE', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is inventoried
                         case
                            when itm.inventory_ind = 'N' then
                               SQL_LIB.GET_MESSAGE_TEXT('ITEM_NOT_INVENTORY', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is not a deposit container
                         case
                            when itm.deposit_item_type = 'A' then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_DEPOSIT_CONTAINER', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is not a deposit crate
                         case
                            when itm.deposit_item_type = 'Z' then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_DEPOSIT_CRATE', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is not a transformable item
                         case
                            when itm.item_xform_ind = 'Y' then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_TRANSFORMED_ITEMS', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is not a catch weight item
                         case
                            when itm.catch_weight_ind = 'Y' then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_CATCH_WEIGHT_ITEMS', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the item is not a concession/consignment item
                         case
                            when exists (select 'x'
                                           from deps d
                                          where d.dept = itm.dept
                                            and d.purchase_type in (1,2)
                                            and rownum = 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('NO_CONCESSION_CONSIGNMENT', itm.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that ref item is valid in RMS
                         case
                            when svfd.ref_item is NOT NULL and itm2.item is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_FULFILL_ITEM', svfd.ref_item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the ref item is at sub-transaction level
                         case
                            when svfd.ref_item is NOT NULL and itm2.item_level <= itm2.tran_level then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_FULFIL_REF_ITEM_LEVEL', svfd.ref_item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that the ref item is approved
                         case
                            when svfd.ref_item is NOT NULL and itm2.status <> 'A' then
                               SQL_LIB.GET_MESSAGE_TEXT('FULFILL_ITEM_NOT_APPROVED', svfd.ref_item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that transaction_uom is a valid UOM on UOM_CLASS table
                         case
                            when um.uom is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_TRAN_UOM', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that standard_uom is the item's standard_uom on ITEM_MASTER
                         case
                            when svfd.standard_uom <> itm.standard_uom then
                               SQL_LIB.GET_MESSAGE_TEXT('UOM_NOT_ITM_UOM', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that order_qty_suom is > 0
                         case
                            when svfd.order_qty_suom <= 0 then
                               SQL_LIB.GET_MESSAGE_TEXT('ORDER_QTY_POSITIVE', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that order_qty_suom is an integer if standard_uom is 'EA'
                         case
                            when svfd.standard_uom = 'EA' and NOT regexp_like(svfd.order_qty_suom,'^[0-9]+$') then
                               SQL_LIB.GET_MESSAGE_TEXT('ORDER_QTY_INTEGER', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that unit_retail and unit_retail_curr are both defined or both NULL
                         case
                            when svfd.unit_retail is NOT NULL and svfd.retail_curr is NULL or
                                 svfd.unit_retail is NULL and svfd.retail_curr is NOT NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('UNIT_RETAIL_CURR', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that unit_retail > 0 if defined
                         case
                            when svfd.unit_retail is NOT NULL and svfd.unit_retail <= 0 then
                               SQL_LIB.GET_MESSAGE_TEXT('UNIT_RETAIL_ZERO', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that retail_curr is a valid currency code
                         case
                            when svfd.retail_curr is NOT NULL and curr.currency_code is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_RETAIL_CURR_CODE', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate that substitute_ind is 'Y' or 'N'
                         case
                            when svfd.substitute_ind NOT in ('Y','N') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_SUBSTITUTE_IND', svfd.item, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end error_msg
                    from svc_fulfilorddtl svfd,
                         item_master itm,
                         item_master itm2,
                         uom_class um,
                         currencies curr,
                         svc_fulfilord svo
                   where svfd.process_id = I_process_id
                     and svfd.chunk_id = I_chunk_id
                     and svfd.item = itm.item(+)
                     and svfd.ref_item = itm2.item(+)
                     and svfd.transaction_uom = um.uom(+)
                     and svfd.retail_curr = curr.currency_code(+)
                     and svfd.fulfilord_id = svo.fulfilord_id
                     and svfd.process_id = svo.process_id
                     and svfd.chunk_id = svo.chunk_id
                     and svo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW)
           where error_msg is NOT NULL) inner
   on (sfd.process_id = inner.process_id
       and sfd.chunk_id = inner.chunk_id
       and sfd.fulfilord_id = inner.fulfilord_id
       and sfd.item = inner.item)
   when matched then
      update set sfd.error_msg = substr(sfd.error_msg || inner.error_msg, 1, 2000),
                 sfd.last_update_id = LP_user,
                 sfd.last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      L_table := 'SVC_FULFILORD';
      open C_LOCK_SVC_FULFILORD;
      close C_LOCK_SVC_FULFILORD;

      update svc_fulfilord sf
         set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sf.error_msg = substr(sf.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_DETAIL', sf.customer_order_no, sf.fulfill_order_no, NULL)||';', 1, 2000),
             sf.last_update_id = LP_user,
             sf.last_update_datetime = sysdate
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtl sd
                      where sd.error_msg is NOT NULL
                        and sd.process_id = sf.process_id
                        and sd.chunk_id = sf.chunk_id
                        and sd.fulfilord_id = sf.fulfilord_id
                        and rownum = 1);

      select substr(substr(error_msg, 1, instr(error_msg, ';')-1), 1, 255)
        into L_error_message
        from svc_fulfilorddtl
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FULFILORDDTL;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORDCUST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_process_id      IN       SVC_FULFILORDCUST.PROCESS_ID%TYPE,
                             I_chunk_id        IN       SVC_FULFILORDCUST.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'CORESVC_FULFILORD_VALIDATE.CHECK_FULFILORDCUST';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_FULFILORD is
      select 'x'
        from svc_fulfilord sf
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilordcust sc
                      where sc.error_msg is NOT NULL
                        and sc.process_id = sf.process_id
                        and sc.chunk_id = sf.chunk_id
                        and sc.fulfilord_id = sf.fulfilord_id
                        and rownum = 1)
         for update nowait;

BEGIN
   merge into svc_fulfilordcust sfc
   using (select *
            from (select svfc.process_id,
                         svfc.chunk_id,
                         svfc.fulfilord_id,
                         -- if deliver_state is populated, deliver_country_id must be populated or both should be NULL
                         -- if bill_state is populated, bill_country_id must be populated or both should be NULL
                         case
                            when svfc.deliver_state is NULL and svfc.deliver_country_id is NOT NULL or
                               svfc.deliver_state is NOT NULL and svfc.deliver_country_id is NULL or
                               svfc.bill_state is NULL and svfc.bill_country_id is NOT NULL or
                               svfc.bill_state is NOT NULL and svfc.bill_country_id is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('DEFINE_COUNTRY_STATE', svo.customer_order_no, svo.fulfill_order_no, NULL)||';'
                         end ||
                         -- if deliver_jurisdiction is populated, validate it agains the country_tax_jurisdiction table
                         -- and verify that the country and state are not NULL.
                         case
                            when svfc.deliver_jurisdiction is NOT NULL and
                                 (svfc.deliver_state      is NULL or
                                  svfc.deliver_country_id is NULL) then
                               SQL_LIB.GET_MESSAGE_TEXT('NULL_CNTRY_ST_JURIS', svo.customer_order_no, svo.fulfill_order_no, NULL)||';'
                            when svfc.deliver_jurisdiction is NOT NULL and
                                 NOT exists (select 'x'
                                               from country_tax_jurisdiction
                                              where jurisdiction_code = svfc.deliver_jurisdiction
                                                and state             = svfc.deliver_state
                                                and country_id        = svfc.deliver_country_id
                                                and rownum            = 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_CNTRY_ST_JURIS', svo.customer_order_no, svo.fulfill_order_no, NULL)||';'
                         end ||
                         -- if bill_jurisdiction is populated, validate it agains the country_tax_jurisdiction table
                         -- and verify that the country and state are not NULL.
                         case
                            when svfc.bill_jurisdiction is NOT NULL and
                                 (svfc.bill_state      is NULL or
                                  svfc.bill_country_id is NULL) then
                               SQL_LIB.GET_MESSAGE_TEXT('NULL_CNTRY_ST_JURIS', svo.customer_order_no, svo.fulfill_order_no, NULL)||';'
                            when svfc.bill_jurisdiction is NOT NULL and
                                 NOT exists (select 'x'
                                               from country_tax_jurisdiction
                                              where jurisdiction_code = svfc.bill_jurisdiction
                                                and state             = svfc.bill_state
                                                and country_id        = svfc.bill_country_id
                                                and rownum            = 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_CNTRY_ST_JURIS', svo.customer_order_no, svo.fulfill_order_no, NULL)||';'
                         end ||
                         -- if defined, validate that deliver_state and deliver_country_id combination is valid on the STATE table
                         case
                            when svfc.deliver_state is NOT NULL and
                                 svfc.deliver_country_id is NOT NULL and
                                 NOT exists (select 'x'
                                               from state s
                                              where svfc.deliver_state = s.state
                                                and svfc.deliver_country_id = s.country_id
                                                and rownum = 1) then
                                  SQL_LIB.GET_MESSAGE_TEXT('INV_COUNTRY_STATE', svfc.deliver_country_id||'/'||svfc.deliver_state, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- if defined, validate that bill_state and bill_country_id combination is valid on the STATE table
                         case
                            when svfc.bill_state is NOT NULL and
                                 svfc.bill_country_id is NOT NULL and
                                 NOT exists (select 'x'
                                               from state s
                                              where svfc.bill_state = s.state
                                                and svfc.bill_country_id = s.country_id
                                                and rownum = 1) then
                                  SQL_LIB.GET_MESSAGE_TEXT('INV_COUNTRY_STATE', svfc.bill_country_id||'/'||svfc.bill_state, svo.customer_order_no, svo.fulfill_order_no)||';'
                         end ||
                         -- validate required fields - deliver_first_name, deliver_last_name, deliver_add1, deliver_city, deliver_state,
                         -- deliver_country_id, deliver_phone, bill_first_name, bill_last_name, bill_add1, bill_city, bill_state, bill_country_id
                         -- if source_loc_type is 'WH' or 'SU' and fulfill_loc_type is 'V' on SVC_FULFILORD
                         case
                            when svo.source_loc_type in ('WH', 'SU') and svo.fulfill_loc_type = 'V' and
                               (svfc.deliver_first_name is NULL or
                                svfc.deliver_last_name is NULL or
                                svfc.deliver_add1 is NULL or
                                svfc.deliver_city is NULL or
                                svfc.deliver_state is NULL or
                                svfc.deliver_country_id is NULL or
                                svfc.deliver_phone is NULL or
                                svfc.bill_first_name is NULL or
                                svfc.bill_last_name is NULL or
                                svfc.bill_add1 is NULL or
                                svfc.bill_city is NULL or
                                svfc.bill_state is NULL or
                                svfc.bill_country_id is NULL) then
                                SQL_LIB.GET_MESSAGE_TEXT('DELIVER_BILL_TO_INFO_REQ', svo.customer_order_no, svo.fulfill_order_no, NULL)||';'
                         end error_msg
                    from svc_fulfilordcust svfc,
                         svc_fulfilord svo
                   where svfc.process_id = I_process_id
                     and svfc.chunk_id = I_chunk_id
                     and svfc.fulfilord_id = svo.fulfilord_id
                     and svfc.process_id = svo.process_id
                     and svfc.chunk_id = svo.chunk_id
                     and svo.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW)
           where error_msg is NOT NULL) inner
   on (sfc.process_id = inner.process_id
       and sfc.chunk_id = inner.chunk_id
       and sfc.fulfilord_id = inner.fulfilord_id)
   when matched then
      update set sfc.error_msg = substr(sfc.error_msg || inner.error_msg, 1, 2000),
                 sfc.last_update_id = LP_user,
                 sfc.last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      L_table := 'SVC_FULFILORD';
      open C_LOCK_SVC_FULFILORD;
      close C_LOCK_SVC_FULFILORD;

      update svc_fulfilord sf
         set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sf.error_msg = substr(sf.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_CUST', sf.customer_order_no, sf.fulfill_order_no, NULL)||';', 1, 2000),
             sf.last_update_id = LP_user,
             sf.last_update_datetime = sysdate
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilordcust sc
                      where sc.error_msg is NOT NULL
                        and sc.process_id = sf.process_id
                        and sc.chunk_id = sf.chunk_id
                        and sc.fulfilord_id = sf.fulfilord_id
                        and rownum = 1);

      select substr(substr(error_msg, 1, instr(error_msg, ';')-1), 1, 255)
        into L_error_message
        from svc_fulfilordcust
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FULFILORDCUST;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORDREF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_FULFILORDREF.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_FULFILORDREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'CORESVC_FULFILORD_VALIDATE.CHECK_FULFILORDREF';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   merge into svc_fulfilordref sfr
   using (select *
            from (select svfr.process_id,
                         svfr.chunk_id,
                         svfr.fulfilordref_id,
                         -- validate that source_loc_type and source_loc_id are both populated or both NULL
                         case
                            when svfr.source_loc_type is NULL and svfr.source_loc_id is NOT NULL or
                                 svfr.source_loc_type is NOT NULL and svfr.source_loc_id is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('SOURCE_LOC_TYPE_LOC_ID', svfr.customer_order_no, svfr.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that the source_loc_type is 'SU' or 'ST' or 'WH' if defined
                         case
                            when svfr.source_loc_type is NOT NULL and svfr.source_loc_type NOT in ('SU', 'ST', 'WH') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_SOURCE_LOC_TYPE', svfr.customer_order_no, svfr.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that fulfill_loc_type is 'S' or 'V'
                         case
                            when svfr.fulfill_loc_type NOT in ('S', 'V') then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_FULFILL_LOC_TYPE', svfr.fulfill_loc_type, svfr.customer_order_no, svfr.fulfill_order_no)||';'
                         end ||
                         -- validate that customer_order_no/fulfill_order_no/source loc/fulfill loc combination exists on ORDCUST
                         case
                            when NOT exists (select 'x'
                                               from ordcust od
                                              where svfr.customer_order_no = od.customer_order_no
                                                and svfr.fulfill_order_no = od.fulfill_order_no
                                                and NVL(svfr.source_loc_type, '-999') = NVL(od.source_loc_type, '-999')
                                                and NVL(svfr.source_loc_id, -999) = NVL(od.source_loc_id, -999)
                                                and svfr.fulfill_loc_type = od.fulfill_loc_type
                                                and svfr.fulfill_loc_id = od.fulfill_loc_id
                                                and rownum = 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('ORDCUST_REC_NOT_FOUND', svfr.customer_order_no, svfr.fulfill_order_no, NULL)||';'
                         end ||
                         -- validate that at least one SVC_FULFILORDDTLREF exists for the corresponding SVC_FULFILORDREF
                         case
                            when NOT exists (select 'x'
                                              from svc_fulfilorddtlref svdr
                                             where svfr.process_id = svdr.process_id
                                               and svfr.chunk_id = svdr.chunk_id
                                               and svfr.fulfilordref_id = svdr.fulfilordref_id
                                               and rownum = 1) then
                               SQL_LIB.GET_MESSAGE_TEXT('DETAIL_REF_RECORD_REQ', svfr.customer_order_no, svfr.fulfill_order_no)||';'
                         end error_msg
                    from svc_fulfilordref svfr
                   where svfr.process_id = I_process_id
                     and svfr.chunk_id = I_chunk_id
                     and svfr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW)
           where error_msg is NOT NULL) inner
   on (sfr.process_id = inner.process_id
       and sfr.chunk_id = inner.chunk_id
       and sfr.fulfilordref_id = inner.fulfilordref_id)
   when matched then
      update set sfr.error_msg = substr(sfr.error_msg || inner.error_msg, 1, 2000),
                 process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
                 last_update_id = LP_user,
                 last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      select substr(substr(error_msg, 1, instr(error_msg, ';')-1), 1, 255)
        into L_error_message
        from svc_fulfilordref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FULFILORDREF;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_FULFILORDDTLREF(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_FULFILORDDTLREF.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_FULFILORDDTLREF.CHUNK_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'CORESVC_FULFILORD_VALIDATE.CHECK_FULFILORDDTLREF';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_table           VARCHAR2(30);
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_SVC_FULFILORDREF is
      select 'x'
        from svc_fulfilordref sf
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtlref sr
                      where sr.error_msg is NOT NULL
                        and sr.process_id = sf.process_id
                        and sr.chunk_id = sf.chunk_id
                        and sr.fulfilordref_id = sf.fulfilordref_id
                        and rownum = 1)
         for update nowait;

BEGIN
   merge into svc_fulfilorddtlref sfdr
   using (select *
            from (select svoc.process_id,
                         svoc.chunk_id,
                         svoc.fulfilordref_id,
                         svoc.item,
                         -- validate that the item exists in ordcust_detail
                         case
                            when od.item is NULL then
                               SQL_LIB.GET_MESSAGE_TEXT('ITEM_NOT_EXIST_ORDDET', svoc.item, svoc.customer_order_no, svoc.fulfill_order_no)||';'
                         end ||
                         -- if defined, validate that the ref_item exists in ordcust_detail
                         case
                            when od.item is NOT NULL and NVL(svoc.ref_item, '-999') <> NVL(od.ref_item, '-999') then
                               SQL_LIB.GET_MESSAGE_TEXT('REFITM_NOT_EXIST_ORDDET', svoc.item, svoc.customer_order_no, svoc.fulfill_order_no)||';'
                         end ||
                         --prevent cancelling the same item on the same order multiple times in a single call.
                         --i.e. customer_order_no/fulfillment_order_no/source_loc/fulfill_loc/item combination is unique
                         case
                            when count(svoc.item) over (partition by svoc.customer_order_no,
                                                                     svoc.fulfill_order_no,
                                                                     svoc.source_loc_type,
                                                                     svoc.source_loc_id,
                                                                     svoc.fulfill_loc_type,
                                                                     svoc.fulfill_loc_id,
                                                                     svoc.item) > 1 then
                               SQL_LIB.GET_MESSAGE_TEXT('UNIQUE_ITEM_PER_CO', svoc.customer_order_no, svoc.fulfill_order_no,NULL)||';'
                         end ||
                         -- validate that standard_uom match that of the item on ORDCUST_DETAIL
                         case
                            when svoc.standard_uom <> od.standard_uom then
                               SQL_LIB.GET_MESSAGE_TEXT('SUOM_NOT_MATCH_ORDDET', svoc.item, svoc.customer_order_no, svoc.fulfill_order_no)||';'
                         end ||
                         -- validate that transaction_uom match that of the item on ORDCUST_DETAIL
                         case
                            when svoc.transaction_uom <> od.transaction_uom then
                               SQL_LIB.GET_MESSAGE_TEXT('TUOM_NOT_MATCH_ORDDET', svoc.item, svoc.customer_order_no, svoc.fulfill_order_no)||';'
                         end ||
                         -- validate that the cancel_qty_suom is > 0 but <= qty_ordered_suom - qty_cancelled_suom on ORDCUST_DETAIL for the item
                         case
                            when svoc.cancel_qty_suom <= 0 or svoc.cancel_qty_suom > od.qty_ordered_suom - NVL(od.qty_cancelled_suom, 0) then
                               SQL_LIB.GET_MESSAGE_TEXT('INV_FULFILL_CANCEL_QTY', svoc.item, svoc.customer_order_no, svoc.fulfill_order_no)||';'
                         end ||
                         -- validate that cancel_qty_suom is an integer if standard_uom is 'EA'
                         case
                            when svoc.standard_uom = 'EA' and NOT regexp_like(svoc.cancel_qty_suom,'^[0-9]+$') then
                               SQL_LIB.GET_MESSAGE_TEXT('ORDER_QTY_INTEGER', svoc.item, svoc.customer_order_no, svoc.fulfill_order_no)||';'
                         end error_msg
                    from (select ordcust_no,
                                 item,
                                 MAX(ref_item) ref_item,
                                 MAX(standard_uom) standard_uom,
                                 MAX(transaction_uom) transaction_uom,
                                 SUM(qty_ordered_suom) qty_ordered_suom,
                                 SUM(NVL(qty_cancelled_suom,0)) qty_cancelled_suom
                            from ordcust_detail
                          group by ordcust_no,
                                   item) od,
                         (select svdr.process_id,
                                 svdr.chunk_id,
                                 svdr.fulfilordref_id,
                                 svdr.item,
                                 svdr.ref_item,
                                 svdr.standard_uom,
                                 svdr.transaction_uom,
                                 svdr.cancel_qty_suom,
                                 svfr.customer_order_no,
                                 svfr.fulfill_order_no,
                                 svfr.source_loc_type,
                                 svfr.source_loc_id,
                                 svfr.fulfill_loc_type,
                                 svfr.fulfill_loc_id,
                                 oc.ordcust_no
                            from ordcust oc,
                                 svc_fulfilordref svfr,
                                 svc_fulfilorddtlref svdr
                           where svfr.customer_order_no = oc.customer_order_no
                             and svfr.fulfill_order_no = oc.fulfill_order_no
                             and NVL(svfr.source_loc_type, '-999') = NVL(oc.source_loc_type, '-999')
                             and NVL(svfr.source_loc_id, -999) = NVL(oc.source_loc_id, -999)
                             and svfr.fulfill_loc_type = oc.fulfill_loc_type
                             and svfr.fulfill_loc_id = oc.fulfill_loc_id
                             and svfr.process_id = I_process_id
                             and svfr.chunk_id = I_chunk_id
                             and svfr.process_id = svdr.process_id
                             and svfr.chunk_id = svdr.chunk_id
                             and svfr.fulfilordref_id = svdr.fulfilordref_id
                             and svfr.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW) svoc
                   where svoc.ordcust_no = od.ordcust_no(+)
                     and svoc.item = od.item(+))
           where error_msg is NOT NULL) inner
   on (sfdr.process_id = inner.process_id
       and sfdr.chunk_id = inner.chunk_id
       and sfdr.fulfilordref_id = inner.fulfilordref_id
       and sfdr.item = inner.item)
   when matched then
      update set sfdr.error_msg = substr(sfdr.error_msg || inner.error_msg, 1, 2000),
                 last_update_id = LP_user,
                 last_update_datetime = sysdate;

   if SQL%ROWCOUNT > 0 then
      L_table := 'SVC_FULFILORDREF';
      open C_LOCK_SVC_FULFILORDREF;
      close C_LOCK_SVC_FULFILORDREF;

      update svc_fulfilordref sf
         set sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_ERROR,
             sf.error_msg = substr(sf.error_msg || SQL_LIB.GET_MESSAGE_TEXT('ERROR_SVC_DETAIL', sf.customer_order_no, sf.fulfill_order_no, NULL)||';', 1, 2000),
             sf.last_update_id = LP_user,
             sf.last_update_datetime = sysdate
       where sf.process_status = CORESVC_FULFILORD.PROCESS_STATUS_NEW
         and sf.process_id = I_process_id
         and sf.chunk_id = I_chunk_id
         and exists (select 'x'
                       from svc_fulfilorddtlref sd
                      where sd.error_msg is NOT NULL
                        and sf.fulfilordref_id = sd.fulfilordref_id
                        and sf.process_id = sd.process_id
                        and sf.chunk_id = sd.chunk_id
                        and rownum = 1);

      select substr(substr(error_msg, 1, instr(error_msg, ';')-1), 1, 255)
        into L_error_message
        from svc_fulfilorddtlref
       where process_id = I_process_id
         and chunk_id = I_chunk_id
         and error_msg is NOT NULL
         and rownum = 1;

      O_error_message := L_error_message;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_process_id,
                                            I_chunk_id);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_FULFILORDDTLREF;
-------------------------------------------------------------------------------------------------------
END CORESVC_FULFILORD_VALIDATE;
/
