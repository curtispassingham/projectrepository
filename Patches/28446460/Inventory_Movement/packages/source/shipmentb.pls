CREATE OR REPLACE PACKAGE BODY SHIPMENT_SQL AS
------------------------------------------------------------------------------------
-- Private Function Declaration
------------------------------------------------------------------------------------
FUNCTION PUB_SHIPMENT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_to_loc_type   IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                      I_shipment      IN     SHIPMENT.SHIPMENT%TYPE,
                      I_wf_ship_ind   IN     BOOLEAN DEFAULT FALSE)
RETURN BOOLEAN;

------------------------------------------------------------------------------------
-- Public Functions
------------------------------------------------------------------------------------
FUNCTION SHIP_TSF(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                  I_tsf_no        IN      TSFHEAD.TSF_NO%TYPE,
                  I_pub_ind       IN      VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN IS

   L_program               VARCHAR2(50) := 'SHIPMENT_SQL.SHIP_TSF';
   L_from_loc              TSFHEAD.FROM_LOC%TYPE;
   L_to_loc                TSFHEAD.TO_LOC%TYPE;
   L_invalid_param         VARCHAR2(30) := NULL;
   L_tsf_items_tbl         BOL_SQL.BOL_SHIPSKU_TBL;
   L_item_tbl              BOL_SQL.BOL_SHIPSKU_TBL;
   L_date                  PERIOD.VDATE%TYPE := GET_VDATE;
   L_inv_status            INV_STATUS_CODES.INV_STATUS%TYPE;
   L_fin_inv_status        INV_STATUS_CODES.INV_STATUS%TYPE;
   L_shipment              SHIPMENT.SHIPMENT%TYPE;
   L_finisher              BOOLEAN := FALSE;
   L_finisher_name         PARTNER.PARTNER_DESC%TYPE := NULL;
   L_tsf_type              TSFHEAD.TSF_TYPE%TYPE;
   L_del_type              ORDCUST.DELIVERY_TYPE%TYPE;
   L_finisher_loc_ind      VARCHAR2(1);
   L_finisher_entity_ind   VARCHAR2(1);
   L_bol_exist             BOOLEAN;
   L_count                 NUMBER;
   L_bol_no                SHIPMENT.BOL_NO%TYPE;
   L_pub_ship              BOOLEAN;
   L_stockholding_ind      STORE.STOCKHOLDING_IND%TYPE;
   L_store_type            STORE.STORE_TYPE%TYPE;
   L_detail_created        BOOLEAN;

   cursor C_NEXT_BOL_NO is
      select bill_of_lading_sequence.NEXTVAL
        from dual;

   cursor C_GET_TSF_HEAD is
      select tsf_no,
             tsf_parent_no,
             to_loc,
             to_loc_type,
             from_loc,
             from_loc_type,
             tsf_type,
             wf_order_no,
             rma_no
        from tsfhead
       where tsf_no = I_tsf_no;

   cursor C_GET_TSF_REC is
      select  item,
              tsf_qty,
              inv_status
        from  tsfdetail
       where  tsf_no = I_tsf_no;
   
   --To support partial shipment and multiple shipments of a MLD transfer
   --from the 1st leg (e.g. warehouse to external finisher), use the current 
   --difference of ship_qty between the 1st and 2nd leg to determine the 
   --auto ship qty for the 2nd leg from an external finisher. 
   cursor C_GET_TSF_PARENT_REC (I_tsf_parent_no TSFHEAD.TSF_NO%TYPE) is
      select ptd.item,
             ptd.ship_qty - NVL(td.ship_qty, 0)   tsf_qty,
             ptd.inv_status
        from tsfdetail ptd,
             tsfdetail td
       where ptd.tsf_no = I_tsf_parent_no
         and td.tsf_no(+) = I_tsf_no
         and td.item (+) = ptd.item
         and ptd.ship_qty > 0;

   cursor C_SHIPMENT IS
      select shipment
        from shipment
       where bol_no = L_bol_no;

    cursor C_FROM_STORE is
      select store_type,
             stockholding_ind
        from store
       where store = L_from_loc;

   TYPE tsf_rec IS RECORD(tsf_no         SHIPSKU.DISTRO_NO%TYPE,
                          tsf_type       TSFHEAD.TSF_TYPE%TYPE,
                          tsf_item       SHIPSKU.ITEM%TYPE);
   TYPE tsf_rec_item_tbl IS TABLE of tsf_rec index by binary_integer;
   L_ils_item_tbl      tsf_rec_item_tbl;
   
   TYPE tsf_det_tbl IS TABLE of c_get_tsf_rec%rowtype index by binary_integer;
   L_tsf_det_tbl TSF_DET_TBL;

   L_tsf_head_rec C_GET_TSF_HEAD%ROWTYPE;
   L_shipment_rec C_SHIPMENT%ROWTYPE;

BEGIN
   --- Validate parameters
   if I_tsf_no is NULL then
      L_invalid_param := 'I_tsf_no';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_TSF_HEAD;
   fetch C_GET_TSF_HEAD into L_tsf_head_rec;
   if C_GET_TSF_HEAD%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_TSF_ITEMS',
                                            I_tsf_no,
                                            L_program,
                                            NULL);
      close C_GET_TSF_HEAD;
      return FALSE;
   end if;
   close C_GET_TSF_HEAD;

   if L_tsf_head_rec.from_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_from_loc,
                                       L_tsf_head_rec.from_loc) = FALSE then
         return FALSE;
      end if;

      if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                      L_finisher,
                                      L_finisher_name,
                                      L_tsf_head_rec.from_loc) = FALSE then
         return FALSE;
      end if;

      if L_finisher = TRUE then
         if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                      L_fin_inv_status,
                                      'ATS') = FALSE then
            return FALSE;
         end if;
      else
         L_inv_status := NULL;
      end if;
   else
      L_from_loc := L_tsf_head_rec.from_loc;
   end if;


   if L_tsf_head_rec.to_loc_type = 'W' then
      if WH_ATTRIB_SQL.GET_PWH_FOR_VWH(O_error_message,
                                       L_to_loc,
                                       L_tsf_head_rec.to_loc) = FALSE then
         return FALSE;
      end if;
   elsif  L_tsf_head_rec.to_loc_type = 'S' then
      L_to_loc := L_tsf_head_rec.to_loc;
   end if;
   ---
   if L_tsf_head_rec.from_loc_type != 'E' then
      open C_GET_TSF_REC;
      fetch C_GET_TSF_REC BULK COLLECT into L_tsf_det_tbl;
      close C_GET_TSF_REC;
      if L_tsf_det_tbl.first is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_REC',
                                               NULL,
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
   else
      open C_GET_TSF_PARENT_REC (L_tsf_head_rec.tsf_parent_no);
      fetch C_GET_TSF_PARENT_REC BULK COLLECT INTO L_tsf_det_tbl;
      close C_GET_TSF_PARENT_REC;
      
      if L_tsf_det_tbl.FIRST is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_REC',
                                               NULL,
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
   end if;
   ---
   open C_NEXT_BOL_NO;
   fetch C_NEXT_BOL_NO into L_bol_no;
   close C_NEXT_BOL_NO;

   if BOL_SQL.PUT_BOL(O_error_message,
                      L_bol_exist,
                      L_bol_no,
                      L_from_loc,
                      L_to_loc,
                      L_date,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL) = FALSE then
      return FALSE;
   end if;

   if L_bol_exist = TRUE then
     O_error_message := SQL_LIB.CREATE_MSG('DUP_BOL',
                                            L_bol_no,
                                            L_program,
                                            NULL);
     return FALSE;
   end if;

   for i in L_tsf_det_tbl.FIRST..L_tsf_det_tbl.LAST LOOP
      dbms_output.put_line(L_tsf_det_tbl(i).item);
      dbms_output.put_line(L_tsf_det_tbl(i).tsf_qty);
      if L_tsf_det_tbl(i).tsf_qty = 0 then
         O_error_message := SQL_LIB.CREATE_MSG('ENT_TRANSF_GREAT_0',
                                               L_tsf_det_tbl(i).item,
                                               L_program,
                                               NULL);
         return FALSE;
      end if;
      L_count := L_tsf_items_tbl.COUNT + 1;
      L_tsf_items_tbl(L_count).distro_no              := I_tsf_no;
      L_tsf_items_tbl(L_count).item                   := L_tsf_det_tbl(i).item;
      L_tsf_items_tbl(L_count).carton                 := NULL;
      L_tsf_items_tbl(L_count).ship_qty               := L_tsf_det_tbl(i).tsf_qty;
      L_tsf_items_tbl(L_count).weight                 := NULL;
      L_tsf_items_tbl(L_count).weight_uom             := NULL;
      L_tsf_items_tbl(L_count).distro_type            := 'T';
      L_tsf_items_tbl(L_count).inv_status             := NVL(L_fin_inv_status, L_tsf_det_tbl(i).inv_status);

   END LOOP;

   if BOL_SQL.PROCESS_ITEM(O_error_message,
                           L_item_tbl,  -- output table
                           L_tsf_items_tbl,
                           L_from_loc,
                           L_to_loc) = FALSE then
      return FALSE;
   end if;
   L_tsf_type := L_tsf_head_rec.tsf_type;

   if BOL_SQL.PUT_TSF(O_error_message,
                      L_tsf_head_rec.to_loc,
                      L_tsf_head_rec.from_loc,
                      L_tsf_type,
                      I_tsf_no,
                      L_from_loc,
                      L_tsf_head_rec.from_loc_type,
                      L_to_loc,
                      L_tsf_head_rec.to_loc_type,
                      L_date,
                      NULL) = FALSE then
      return FALSE;
   end if;

   if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                     L_finisher_loc_ind,
                                     L_finisher_entity_ind,
                                     I_tsf_no)= FALSE THEN
     return FALSE;
   end if;

   for k in L_item_tbl.first..L_item_tbl.last LOOP
      if BOL_SQL.PUT_TSF_ITEM(O_error_message,
                              L_detail_created,
                              L_item_tbl(k).distro_no,
                              L_item_tbl(k).item,
                              L_item_tbl(k).carton,
                              L_item_tbl(k).ship_qty,
                              L_item_tbl(k).weight,
                              L_item_tbl(k).weight_uom,
                              L_item_tbl(k).inv_status,
                              L_from_loc,
                              L_tsf_head_rec.from_loc_type,
                              L_to_loc,
                              L_tsf_head_rec.to_loc_type,
                              L_tsf_head_rec.to_loc,
                              L_tsf_head_rec.from_loc,
                              L_tsf_type,
                              'Y') = FALSE then
         return FALSE;
      end if;

      if L_finisher = FALSE then
         L_count := L_ils_item_tbl.COUNT + 1;
         L_ils_item_tbl(L_count).tsf_no := I_tsf_no;
         L_ils_item_tbl(L_count).tsf_type := L_tsf_type;
         L_ils_item_tbl(L_count).tsf_item := L_item_tbl(k).item;
      end if;
   END LOOP;

   if BOL_SQL.PROCESS_TSF(O_error_message) = FALSE then
      return FALSE;
   end if;

   if L_tsf_head_rec.wf_order_no is NOT NULL then
      if WF_BOL_SQL.UPDATE_SHIPPED_WF_ORDER(O_error_message,
                                            I_tsf_no,
                                            'T',
                                            NULL,
                                            NULL) = FALSE then
         return FALSE;
      end if;
   elsif L_tsf_head_rec.rma_no is NOT NULL then
      if WF_BOL_SQL.UPDATE_SHIPPED_WF_RETURN(O_error_message,
                                             I_tsf_no) = FALSE then
         return FALSE;
      end if;
   end if;

   if BOL_SQL.FLUSH_BOL_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;

   open C_SHIPMENT;
   fetch C_SHIPMENT into L_shipment;
   close C_SHIPMENT;
   ---
   if L_ils_item_tbl.first is NOT NULL then
      if L_finisher_loc_ind is NOT NULL then
         FOR i IN 1..L_ils_item_tbl.COUNT LOOP
            if BOL_SQL.PUT_ILS_AV_RETAIL(O_error_message,
                                         L_tsf_head_rec.to_loc,
                                         L_tsf_head_rec.to_loc_type,
                                         L_ils_item_tbl(i).tsf_item,
                                         L_shipment,
                                         I_tsf_no,
                                         L_ils_item_tbl(i).tsf_type,
                                         NULL) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;
   end if;

   if I_pub_ind = 'Y' then
      L_pub_ship := TRUE;
   else
      L_pub_ship := FALSE;

      if L_tsf_head_rec.from_loc_type = 'S' then
        open C_FROM_STORE;
        fetch C_FROM_STORE into L_store_type, L_stockholding_ind;
        close C_FROM_STORE;

         if L_store_type = 'F' and L_stockholding_ind ='N' then
            L_pub_ship := TRUE;
         end if;
      end if;
   end if;

   if PUB_SHIPMENT(O_error_message,
                   L_tsf_head_rec.to_loc_type,
                   L_shipment,
                   L_pub_ship) = FALSE then
      return FALSE;
   end if;

   L_ils_item_tbl.delete;
   L_item_tbl.delete;
   L_tsf_items_tbl.delete;

   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SHIP_TSF;
------------------------------------------------------------------------------------
FUNCTION SETUP_BOL_DISTRO_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_bol_no          IN       BOL_SHIPSKU.BOL_NO%TYPE,
                                I_distro_no       IN       BOL_DISTRO_ITEMS_TEMP.DISTRO_NO%TYPE,
                                I_distro_type     IN       BOL_DISTRO_ITEMS_TEMP.DISTRO_TYPE%TYPE,
                                I_select_all      IN       BOL_DISTRO_ITEMS_TEMP.SELECT_IND%TYPE,
                                I_from_loc        IN       TSFHEAD.FROM_LOC%TYPE,
                                I_to_loc          IN       TSFHEAD.TO_LOC%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(50) := 'SHIPMENT_SQL.SETUP_BOL_DISTRO_ITEMS';
   L_invalid_param  VARCHAR2(30) := NULL;

BEGIN
   --- Validate parameters
   if I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
   elsif I_distro_no is NULL then
      L_invalid_param := 'I_distro_no';
   elsif I_distro_type is NULL then
      L_invalid_param := 'I_distro_type';
   elsif I_select_all is NULL then
      L_invalid_param := 'I_select_all';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_distro_type = 'T' then
      merge into bol_distro_items_temp bol
         using (select tdt.tsf_no record_num,
                       (tdt.tsf_qty - NVL(tdt.ship_qty,0)) record_qty,
                       tdt.item record_item,
                       case
                          when I_select_all = 'Y' then 'Y'
                          when bsk.item is NOT NULL then 'Y'
                          else 'N'
                       end select_ind
                  from bol_shipsku bsk,
                       tsfdetail tdt,
                       v_tsfhead vt
                 where bsk.bol_no(+) = I_bol_no
                   and bsk.item(+) = tdt.item
                   and bsk.distro_no(+)= tdt.tsf_no
                   and bsk.distro_type(+) = I_distro_type
                   and vt.tsf_no = tdt.tsf_no
                   and NVL(vt.overall_status,'X') in ('A','N')
                   and tdt.tsf_no = I_distro_no
                   and tdt.tsf_qty - NVL(tdt.ship_qty,0) > 0
                 union
                select vt.tsf_no record_num,
                       (tdt.tsf_qty - NVL(tdt.ship_qty,0)) record_qty,
                       tdt.item record_item,
                       case
                          when I_select_all = 'Y' then 'Y'
                          when bsk.item is NOT NULL then 'Y'
                          else 'N'
                       end select_ind
                  from bol_shipsku bsk,
                       tsfdetail tdt,
                       v_tsfhead vt
                 where bsk.bol_no(+) = I_bol_no
                   and bsk.item(+) = tdt.item
                   and bsk.distro_no(+)= tdt.tsf_no
                   and bsk.distro_type(+) = I_distro_type
                   and vt.tsf_no = I_distro_no
                   and vt.child_tsf_no = tdt.tsf_no
                   and vt.leg_1_status = 'C'
                   and vt.leg_2_status in ('A','S')
                   and NVL(vt.overall_status,'X') = 'N'
                   and tdt.tsf_qty - NVL(tdt.ship_qty,0) > 0) rec
            on ( bol.bol_no = I_bol_no
                 and bol.distro_type = I_distro_type
                 and bol.distro_no = rec.record_num                 
                 and bol.item = rec.record_item)
          when matched then
             update set select_ind = rec.select_ind
          when NOT matched then
             insert(bol_no,
                    distro_no,
                    distro_type,
                    item,
                    distro_qty,
                    select_ind)
             values(I_bol_no,
                    rec.record_num,
                    I_distro_type,
                    rec.record_item,
                    rec.record_qty,
                    rec.select_ind);
   elsif I_distro_type = 'A' then
      merge into bol_distro_items_temp bol
        using (select distinct 
                      alloc_det.alloc_no record_num,
                      alloc_head.item record_item,
                      SUM(alloc_det.qty_allocated - NVL(alloc_det.qty_transferred,0)) over (partition by alloc_det.alloc_no, loc.p_loc) record_qty,
                      case
                         when I_select_all = 'Y' then 'Y'
                         when bol.item is NOT NULL then 'Y'
                         else 'N'
                      end as select_ind
                 from bol_shipsku bol,
                      alloc_detail alloc_det,
                      alloc_header alloc_head,
                      wh w,
                      (select physical_wh p_loc,
                              wh location
                         from wh
                        where physical_wh = I_to_loc
                        union
                       select store p_loc,
                              store location
                         from store
                        where store = I_to_loc) loc
                 where bol.bol_no(+) = I_bol_no
                   and bol.item(+) = alloc_head.item
                   and bol.distro_no(+)= alloc_head.alloc_no
                   and bol.distro_type(+) = I_distro_type
                   and alloc_head.alloc_no = alloc_det.alloc_no
                   and alloc_det.alloc_no = I_distro_no
                   and alloc_head.wh = w.wh
                   and w.physical_wh = I_from_loc
                   and alloc_det.to_loc = loc.location
                   and alloc_det.qty_allocated - NVL(alloc_det.qty_transferred,0) > 0) rec
           on ( bol.bol_no = I_bol_no
                and bol.distro_type = I_distro_type
                and bol.distro_no = rec.record_num                
                and bol.item = rec.record_item)
         when matched then
            update set select_ind = 'Y'
         when NOT matched then
            insert(bol_no,
                   distro_no,
                   distro_type,
                   item,
                   distro_qty,
                   select_ind)
            values(I_bol_no,
                   rec.record_num,
                   I_distro_type,
                   rec.record_item,
                   rec.record_qty,
                   rec.select_ind);
   end if;

   return TRUE;

EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'SHIPMENT_SQL.SETUP_BOL_DISTRO_ITEMS',
                                               TO_CHAR(SQLCODE));
         return FALSE;
END SETUP_BOL_DISTRO_ITEMS;
------------------------------------------------------------------------------------
FUNCTION SETUP_ORDER_SHIPMENT (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no        IN       V_ORDHEAD.ORDER_NO%TYPE,
                               I_location        IN       V_ORDLOC.LOCATION%TYPE,
                               I_loc_type        IN       V_ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program                   VARCHAR2(50) := 'SHIPMENT_SQL.SETUP_ORDER_SHIPMENT';
   L_invalid_param             VARCHAR(30) := NULL;

BEGIN
   --- Validate parameters
   if I_order_no is NULL then
      L_invalid_param := 'I_order_no';
   elsif I_location is NULL then
      L_invalid_param := 'I_location';
   elsif I_loc_type is NULL then
      L_invalid_param := 'I_loc_type';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_loc_type = 'S' then
      insert into order_shipment_temp (order_no,
                                       ship_ind,
                                       item,
                                       item_desc,
                                       carton,
                                       qty_shipped,
                                       qty_received,
                                       qty_ordered)
                                select I_order_no order_no,
                                       'N' ship_ind,
                                       vo.item,
                                       iem.item_desc,
                                       NULL carton,
                                       NULL qty_shipped,
                                       NVL(SUM(ssi.qty_expected), 0),  -- used to be vo.qty_received,
                                       vo.qty_ordered
                                  from v_ordloc vo,
                                       item_master iem,
                                       (select s.order_no,
                                               s.asn,
                                               s.to_loc,
                                               ss.item,
                                               CASE 
                                                 WHEN s.status_code != 'C' 
                                                 THEN ss.qty_expected 
                                                 WHEN s.status_code = 'C' and NVL(ss.qty_received,0) > 0
                                                 THEN LEAST(ss.qty_received,ss.qty_expected)
                                               END qty_expected
                                          from shipment s,
                                               shipsku ss
                                         where s.order_no is NOT NULL
                                           and s.asn is NOT NULL
                                           and s.to_loc_type = 'S'
                                           and s.shipment = ss.shipment) ssi
                                 where vo.order_no = I_order_no
                                   and vo.location = I_location
                                   and iem.item = vo.item
                                   and NVL(iem.deposit_item_type,'X') != 'A'
                                   and vo.order_no = ssi.order_no (+)
                                   and vo.location = ssi.to_loc (+)
                                   and vo.item = ssi.item (+)
                                 group by vo.item,
                                          iem.item_desc,
                                          vo.qty_ordered;
   elsif I_loc_type = 'W' then
      insert into order_shipment_temp (order_no,
                                       ship_ind,
                                       item,
                                       item_desc,
                                       carton,
                                       qty_shipped,
                                       qty_received,
                                       qty_ordered)
                                select voi.order_no,
                                       'N' ship_ind,
                                        voi.item,
                                        voi.item_desc,
                                        NULL carton,
                                        NULL qty_shipped,
                                        SUM(nvl(ssi.qty_expected, 0)),
                                        voi.qty_ordered
                                   from (select vo.order_no,
                                                vo.item,
                                                iem.item_desc,
                                                wh.physical_wh,
                                                SUM(vo.qty_ordered) qty_ordered
                                           from v_ordloc vo,
                                                item_master iem,
                                                wh
                                          where vo.order_no = I_order_no
                                            and wh.physical_wh = I_location
                                            and vo.location = wh.wh
                                            and iem.item = vo.item
                                            and NVL(iem.deposit_item_type,'X') != 'A'
                                          group by vo.order_no,
                                                   vo.item,
                                                   iem.item_desc,
                                                   wh.physical_wh) voi,
                                        (select s.order_no,
                                                s.asn,
                                                s.to_loc,
                                                ss.item,
                                                ss.qty_expected
                                           from shipment s,
                                                shipsku ss
                                          where s.order_no is NOT NULL
                                            and s.asn is NOT NULL
                                            and s.to_loc_type = 'W'
                                            and s.shipment = ss.shipment
                                            and s.status_code != 'C') ssi
                                      where voi.order_no = ssi.order_no(+)
                                        and voi.physical_wh = ssi.to_loc(+)
                                        and voi.item = ssi.item(+)
                                   group by  voi.order_no,
                                             voi.item,
                                             voi.item_desc,
                                             voi.qty_ordered;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_ORDER_SHIPMENT;
------------------------------------------------------------------------------------
FUNCTION DELETE_SHIPSKU(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_bol_no          IN       BOL_SHIPSKU.BOL_NO%TYPE,
                        I_distro_type     IN       BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                        I_distro_no       IN       BOL_SHIPSKU.DISTRO_NO%TYPE,
                        I_item            IN       BOL_SHIPSKU.ITEM%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_program        VARCHAR2(50) := 'SHIPMENT_SQL.DELETE_SHIPSKU';
   L_invalid_param  VARCHAR2(30) := NULL;
   L_table          VARCHAR2(30) := NULL;

   cursor C_LOCK_BOL_SHIPSKU is
      select 'x'
        from bol_shipsku
       where bol_no      = I_bol_no
         and distro_type = I_distro_type
         and distro_no   = I_distro_no
         and item        = I_item
         for update nowait;

BEGIN

   if I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
   elsif I_distro_no is NULL then
      L_invalid_param := 'I_distro_no';
   elsif I_distro_type is NULL then
      L_invalid_param := 'I_distro_type';
   elsif I_item is NULL then
      L_invalid_param := 'I_item';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_table := 'BOL_SHIPSKU';
   open C_LOCK_BOL_SHIPSKU;
   close C_LOCK_BOL_SHIPSKU;

   delete bol_shipsku
    where bol_no      = I_bol_no
      and distro_type = I_distro_type
      and distro_no   = I_distro_no
      and item        = I_item;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'I_bol_no: '||I_bol_no,
                                            'I_item: '||I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_SHIPSKU;
------------------------------------------------------------------------------------
FUNCTION DELETE_DISTRO_ITEMS_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_bol_no          IN       BOL_SHIPSKU.BOL_NO%TYPE,
                                  I_distro_type     IN       BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                                  I_distro_no       IN       BOL_SHIPSKU.DISTRO_NO%TYPE,
                                  I_item            IN       BOL_SHIPSKU.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'SHIPMENT_SQL.DELETE_DISTRO_ITEMS_TEMP';
   L_invalid_param   VARCHAR2(30) := NULL;

   L_table           VARCHAR2(30) := NULL;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_DISTRO_ITEMS_TEMP is
      select 'x'
        from bol_distro_items_temp
       where bol_no      = I_bol_no
         and distro_type = I_distro_type
         and distro_no   = I_distro_no
         and item        = I_item
         for update nowait;

BEGIN

   if I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
   elsif I_distro_no is NULL then
      L_invalid_param := 'I_distro_no';
   elsif I_distro_type is NULL then
      L_invalid_param := 'I_distro_type';
   elsif I_item is NULL then
      L_invalid_param := 'I_item';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_table := 'BOL_DISTRO_ITEMS_TEMP';
   open C_LOCK_DISTRO_ITEMS_TEMP;
   close C_LOCK_DISTRO_ITEMS_TEMP;

   delete bol_distro_items_temp
    where bol_no      = I_bol_no
      and distro_type = I_distro_type
      and distro_no   = I_distro_no
      and item        = I_item;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            'I_bol_no: '||I_bol_no,
                                            'I_item: '||I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END DELETE_DISTRO_ITEMS_TEMP;
------------------------------------------------------------------------------------
FUNCTION DELETE_BOL_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_bol_no          IN       BOL_SHIPSKU.BOL_NO%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);
   L_program        VARCHAR2(50) := 'SHIPMENT_SQL.DELETE_BOL_SHIPMENT';
   L_invalid_param  VARCHAR2(30) := NULL;
   L_table_name     VARCHAR2(30) := NULL;

   cursor C_LOCK_BOL_SHIPSKU is
      select 'x'
        from bol_shipsku
       where bol_no = I_bol_no
         for update nowait;

   cursor C_LOCK_BOL_SHIPMENT is
      select 'x'
       from bol_shipment
      where bol_no = I_bol_no
         for update nowait;

BEGIN
   if I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_table_name := 'BOL_SHIPSKU';
   open C_LOCK_BOL_SHIPMENT;
   close C_LOCK_BOL_SHIPMENT;

   delete bol_shipsku
   where bol_no = I_bol_no;

   L_table_name := 'BOL_SHIPMENT';
   open C_LOCK_BOL_SHIPMENT;
   close C_LOCK_BOL_SHIPMENT;

   delete bol_shipment
     where bol_no = I_bol_no;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table_name,
                                            'BOL No: '||I_bol_no);
      return FALSE;
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
     return FALSE;
END DELETE_BOL_SHIPMENT;
-----------------------------------------------------------------------------------
FUNCTION APPLY_BOL_SHIPSKU(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_bol_no             IN       BOL_SHIPSKU.BOL_NO%TYPE,
                           I_distro_type        IN       BOL_SHIPSKU.DISTRO_TYPE%TYPE,
                           I_distro_no          IN       BOL_SHIPSKU.DISTRO_NO%TYPE,
                           I_apply_all          IN       VARCHAR2)
  RETURN BOOLEAN IS

  L_program        VARCHAR2(50) := 'SHIPMENT_SQL.APPLY_BOL_SHIPSKU';
  L_invalid_param  VARCHAR2(30) := NULL;

BEGIN
   if I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
   elsif I_distro_type is NULL then
      L_invalid_param := 'I_distro_type';
   elsif I_distro_no is NULL then
      L_invalid_param := 'I_distro_no';
   elsif I_apply_all is NULL then
      L_invalid_param := 'I_apply_all';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   insert into bol_shipsku(bol_no,
                           distro_type,
                           distro_no,
                           item,
                           ref_item,
                           carton,
                           ship_qty,
                           weight_expected,
                           weight_expected_uom,
                           last_update_datetime)
                    select I_bol_no,
                           bdit.distro_type,
                           bdit.distro_no,
                           bdit.item,
                           DECODE(im.primary_ref_item_ind, 'Y', im.item_parent, NULL),
                           NULL,
                           DECODE(I_apply_all, 'Y', bdit.distro_qty, NULL),
                           NULL,
                           NULL,
                           SYSDATE
                      from item_master im,
                           bol_distro_items_temp bdit
                     where im.item          = bdit.item
                       and bdit.bol_no      = I_bol_no
                       and bdit.distro_type = I_distro_type
                       and bdit.distro_no   = I_distro_no
                       and bdit.select_ind  = 'Y'
                       and not exists(select 'x'
                                        from bol_shipsku bs
                                       where im.item        = bs.item
                                         and bs.bol_no      = I_bol_no
                                         and bs.distro_no   = I_distro_no
                                         and bs.distro_type = I_distro_type
                                         and rownum         = 1);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
    return FALSE;
END APPLY_BOL_SHIPSKU;
-----------------------------------------------------------------------------------
FUNCTION SHIP_PO (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_order_no        IN       V_ORDLOC.ORDER_NO%TYPE,
                  I_location        IN       SHIPMENT.TO_LOC%TYPE,
                  I_asn             IN       SHIPMENT.ASN%TYPE,
                  I_shipment_date   IN       SHIPMENT.SHIP_DATE%TYPE,
                  I_courier         IN       SHIPMENT.COURIER%TYPE,
                  I_no_boxes        IN       SHIPMENT.NO_BOXES%TYPE,
                  I_comments        IN       SHIPMENT.COMMENTS%TYPE)

RETURN BOOLEAN IS

   RECORD_LOCKED                      EXCEPTION;
   PRAGMA                             EXCEPTION_INIT(Record_Locked, -54);

   L_program                          VARCHAR2(50) := 'SHIPMENT_SQL.SHIP_PO';
   L_loc_type                         SHIPMENT.TO_LOC_TYPE%TYPE;
   L_ctn_ind                          VARCHAR2(1) := NULL;
   L_order_no                         ORDHEAD.ORDER_NO%TYPE;
   L_premark_ind                      ORDHEAD.PRE_MARK_IND%TYPE;
   L_shipment                         SHIPMENT.SHIPMENT%TYPE;
   L_supplier                         ORDHEAD.SUPPLIER%TYPE;
   L_ship_match                       BOOLEAN := FALSE;
   L_exists                           BOOLEAN;
   L_ship_pay_method                  V_ORDHEAD.SHIP_PAY_METHOD%TYPE;
   L_not_after_date                   V_ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_invalid_param                    VARCHAR2(30) := NULL;
   L_auto_rcv_store                   VARCHAR2(1);
   L_receive_item_table               RECEIVE_SQL.ITEM_RECV_TABLE;
   L_distro_contents                  VARCHAR2(1);

   cursor C_ORDER_RECS is
      select (case 
             when qty_shipped>0 then 'Y'
             else 'N' 
             end)ship_ind,
             item,
             carton,
             qty_shipped,
             qty_received,
             qty_ordered
        from order_shipment_temp
       where order_no = I_order_no
       order by carton,
                item;

   cursor C_GET_ORDER_INFO is
      select supplier,
             ship_pay_method,
             not_after_date
        from v_ordhead
       where order_no = I_order_no;

   cursor C_GET_SYSTEM_OPTIONS is
      select auto_rcv_store
        from system_options;

   TYPE asn_po_tbl IS TABLE of c_order_recs%rowtype index by binary_integer;
   L_asn_po_tbl           asn_po_tbl;

BEGIN

   if I_order_no is NULL then
      L_invalid_param := 'I_order_no';
   elsif  I_location is NULL then
      L_invalid_param := 'I_location';
   elsif I_asn is NULL then
      L_invalid_param := 'I_asn';
   elsif I_shipment_date is NULL then
      L_invalid_param := 'I_shipment_date';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_ORDER_INFO;
   fetch C_GET_ORDER_INFO into L_supplier,
                               L_ship_pay_method,
                               L_not_after_date;
   if C_GET_ORDER_INFO%NOTFOUND then
      close C_GET_ORDER_INFO;
      return FALSE;
   end if;
   close C_GET_ORDER_INFO;

   open C_ORDER_RECS;
   fetch C_ORDER_RECS BULK COLLECT into L_asn_po_tbl;
   close C_ORDER_RECS;

   if ASN_SQL.RESET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if ASN_SQL.VALIDATE_LOCATION(O_error_message,
                                L_loc_type,
                                I_location) = FALSE then
      return FALSE;
   end if;
   ---
   if ASN_SQL.PROCESS_ORDER_ONLINE(O_error_message,
                                   L_order_no,
                                   L_premark_ind,
                                   L_shipment,
                                   I_asn,
                                   I_order_no,
                                   I_location,
                                   L_loc_type,
                                   L_ship_pay_method,   -- I_ship_pay_method
                                   L_not_after_date,    -- I_not_after_date
                                   I_shipment_date,
                                   NULL,                -- I_est_arr_date
                                   I_courier,
                                   I_no_boxes,
                                   I_comments,
                                   NULL,                -- I_inbound_bol
                                   L_supplier,
                                   L_ctn_ind) = FALSE then
      return FALSE;
   end if;
   ---
   if L_asn_po_tbl.first is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC_TO_PROCESS',
                                            NULL,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   for rec_carton in L_asn_po_tbl.first..L_asn_po_tbl.last LOOP
      if L_asn_po_tbl(rec_carton).carton is NOT NULL and L_asn_po_tbl(rec_carton).ship_ind = 'Y' then
         if ASN_SQL.VALIDATE_CARTON(O_error_message,
                                    L_asn_po_tbl(rec_carton).carton,
                                    I_location) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;
   ---
   for rec_carton in L_asn_po_tbl.first..L_asn_po_tbl.last LOOP
      if L_asn_po_tbl(rec_carton).ship_ind = 'Y' then
         if ASN_SQL.CHECK_ITEM(O_error_message,
                               L_shipment,
                               L_supplier,
                               I_asn,
                               L_order_no,
                               I_location,
                               NULL,
                               L_asn_po_tbl(rec_carton).item,
                               NULL,
                               NULL,
                               L_asn_po_tbl(rec_carton).carton,
                               L_premark_ind,
                               L_asn_po_tbl(rec_carton).qty_shipped,
                               L_ship_match,
                               L_loc_type) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;

   if ASN_SQL.DO_SHIPSKU_INSERTS(O_error_message) = FALSE then
      return FALSE;
   end if;

   if NOT SHIPMENT_ATTRIB_SQL.CHECK_SHIPSKU(O_error_message,
                                            L_exists,
                                            L_shipment) then
      return FALSE;
   end if;

   if NOT L_exists then
      O_error_message := SQL_LIB.CREATE_MSG('NO_SHIPSKU_RECORDS',
                                            L_shipment,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    NULL);
   open C_GET_SYSTEM_OPTIONS;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    NULL);
   fetch C_GET_SYSTEM_OPTIONS into L_auto_rcv_store;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_SYSTEM_OPTIONS',
                    'SYSTEM_OPTIONS',
                    NULL);
   close C_GET_SYSTEM_OPTIONS;

   if L_auto_rcv_store = 'Y' and L_loc_type = 'S' then
      if RECEIVE_SQL.LOCK_RECV_SHIPMENT(O_error_message,
                                        L_shipment) = FALSE then
         return FALSE;
      end if;
      ---
      RECEIVE_SQL.FETCH_ITEM_BULK(L_receive_item_table,
                                  L_distro_contents,
                                  L_shipment);
      ---
      if L_receive_item_table(1).return_code = 'FALSE' then
         O_error_message := L_receive_item_table(1).error_message;
         return FALSE;
      end if;
      ---
      FOR i in L_receive_item_table.FIRST..L_receive_item_table.LAST LOOP
         L_receive_item_table(i).qty_received := L_receive_item_table(i).qty_expected;
      END LOOP;
      ---
      RECEIVE_SQL.RECEIVE_ITEMS(L_receive_item_table,
                                L_shipment,
                                L_distro_contents,
                                I_location,
                                I_asn,
                                L_order_no,
                                I_shipment_date,
                                'ATS');    --Disposition value
      if L_receive_item_table(1).return_code = 'FALSE' then
         O_error_message := L_receive_item_table(1).error_message;
         return FALSE;
      end if;
      ---
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'SHIPMENT',
                                            'L_shipment: '||L_shipment);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END SHIP_PO;
-----------------------------------------------------------------------------------
FUNCTION SHIP_DISTROS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_bol_no          IN       BOL_SHIPMENT.BOL_NO%TYPE)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'SHIPMENT_SQL.SHIP_DISTROS';
   L_from_loc             BOL_SHIPMENT.FROM_LOC%TYPE;
   L_to_loc               BOL_SHIPMENT.TO_LOC%TYPE;
   L_from_loc_orig        BOL_SHIPMENT.FROM_LOC%TYPE;
   L_to_loc_orig          BOL_SHIPMENT.TO_LOC%TYPE;
   L_bol_items_tbl        BOL_SQL.BOL_SHIPSKU_TBL;
   L_item_tbl             BOL_SQL.BOL_SHIPSKU_TBL;
   L_invalid_param        VARCHAR2(30) := NULL;
   L_date                 PERIOD.VDATE%TYPE := GET_VDATE;
   L_next_distro_no       BOL_SHIPSKU.DISTRO_NO%TYPE := -1;
   L_next_distro_type     BOL_SHIPSKU.DISTRO_TYPE%TYPE := NULL;
   L_fin_inv_status       INV_STATUS_CODES.INV_STATUS%TYPE;
   L_shipment             SHIPMENT.SHIPMENT%TYPE;
   L_finisher             BOOLEAN := FALSE;
   L_finisher_name        PARTNER.PARTNER_DESC%TYPE := NULL;
   L_tsf_type             TSFHEAD.TSF_TYPE%TYPE;
   L_count                NUMBER;
   L_distro_no            BOL_SHIPSKU.DISTRO_NO%TYPE;
   L_distro_type          BOL_SHIPSKU.DISTRO_TYPE%TYPE;
   L_del_type             ORDCUST.DELIVERY_TYPE%TYPE;
   L_finisher_loc_ind     VARCHAR2(1);
   L_finisher_entity_ind  VARCHAR2(1);
   L_bol_exist            BOOLEAN;
   L_ctr                  NUMBER;
   L_new_distro           VARCHAR2(1);
   L_tsfhead_info         V_TSFHEAD%ROWTYPE;
   L_stockholding_ind     STORE.STOCKHOLDING_IND%TYPE;
   L_store_type           STORE.STORE_TYPE%TYPE;
   L_pub_shipment         BOOLEAN;
   L_detail_created       BOOLEAN;
   L_franchise_ordret_ind VARCHAR2(1);

   cursor C_GET_BOL_SHIPMENT is
      select to_loc,
             to_loc_type,
             from_loc,
             from_loc_type,
             ship_date,
             no_boxes,
             courier,
             comments
        from bol_shipment bol_sh
       where bol_sh.bol_no = I_bol_no;

   cursor C_GET_BOL_SHIP_SKU is
      select bol_sku.distro_no,
             thead.tsf_no check_distro,
             bol_sku.distro_type,
             bol_sku.carton,
             bol_sku.item,
             tsf.item check_item,
             bol_sku.ship_qty,
             tsf.tsf_qty - NVL(tsf.ship_qty, 0) available_qty,
             bol_sku.weight_expected,
             bol_sku.weight_expected_uom,
             NVL(tsf.inv_status,-1) inv_status,
             thead.tsf_type,
             thead.from_loc,
             thead.to_loc
        from bol_shipsku bol_sku,
             tsfdetail tsf,
             tsfhead thead
       where bol_sku.bol_no = I_bol_no
         and tsf.tsf_no(+) = bol_sku.distro_no
         and tsf.item(+) = bol_sku.item
         and tsf.tsf_qty > 0
         and thead.tsf_no (+) = bol_sku.distro_no
         and bol_sku.distro_type = 'T'
         and not exists (select 'x'
                           from v_tsfhead v
                          where v.tsf_no = thead.tsf_no
                            and v.child_tsf_no is NOT NULL
                            and v.leg_1_status = 'C'
                            and v.leg_2_status in ('A','S')
                            and v.finisher_type = 'I')
       union all
      select thead.child_tsf_no,
             thead.child_tsf_no check_distro,
             bol_sku.distro_type,
             bol_sku.carton,
             bol_sku.item,
             tsf.item check_item,
             bol_sku.ship_qty,
             tsf.tsf_qty - NVL(tsf.ship_qty, 0) available_qty,
             bol_sku.weight_expected,
             bol_sku.weight_expected_uom,
             NVL(tsf.inv_status,-1) inv_status,
             thead.tsf_type,
             thead.finisher,
             thead.to_loc
        from bol_shipsku bol_sku,
             tsfdetail tsf,
             v_tsfhead thead
       where bol_sku.bol_no = I_bol_no
         and tsf.item(+) = bol_sku.item
         and tsf.tsf_qty > 0
         and thead.tsf_no (+) = bol_sku.distro_no
         and thead.child_tsf_no = tsf.tsf_no
         and bol_sku.distro_type = 'T'
         and thead.leg_1_status = 'C'
         and thead.leg_2_status in ('A','S')
         and thead.finisher_type = 'I'
       union all
      select bol_sku.distro_no,
             ah.alloc_no check_distro,
             bol_sku.distro_type,
             bol_sku.carton,
             bol_sku.item ,
             decode(ah.alloc_no, ad.alloc_no, ah.item, NULL) check_item,
             bol_sku.ship_qty,
             ad.available_qty,
             bol_sku.weight_expected,
             bol_sku.weight_expected_uom,
             -1,
             NULL,
             NULL,
             NULL
        from (select bs.from_loc,
                     bs.to_loc,
                     bk.*
                from bol_shipment bs,
                     bol_shipsku bk
               where bs.bol_no = I_bol_no
                 and bs.bol_no = bk.bol_no
                 and bk.distro_type = 'A') bol_sku,
             (select distinct ad.alloc_no,
                     SUM(ad.qty_allocated - NVL(ad.qty_transferred,0)) over (partition by ad.alloc_no, bs.to_loc) available_qty
                from bol_shipment bs,
                     bol_shipsku bk,
                     wh,
                     alloc_detail ad
               where bs.bol_no = I_bol_no
                 and bk.bol_no = bs.bol_no
                 and bk.distro_no = ad.alloc_no
                 and bk.distro_type = 'A'
                 and bs.to_loc = wh.physical_wh
                 and wh.wh = ad.to_loc
                 and ad.to_loc_type = 'W'
               union all
              select ad.alloc_no,
                     ad.qty_allocated - NVL(ad.qty_transferred,0) available_qty
                from bol_shipment bs,
                     bol_shipsku bk,
                     alloc_detail ad
               where bs.bol_no = I_bol_no
                 and bk.bol_no = bs.bol_no
                 and bk.distro_no = ad.alloc_no
                 and bk.distro_type = 'A'
                 and bs.to_loc = ad.to_loc
                 and ad.to_loc_type = 'S') ad,
             (select ah.alloc_no,
                     ah.item,
                     wh.physical_wh wh
                from bol_shipment bs,
                     bol_shipsku bk,
                     wh,
                     alloc_header ah
               where bs.bol_no = I_bol_no
                 and bs.bol_no = bk.bol_no
                 and bs.from_loc = wh.physical_wh
                 and bk.distro_no = ah.alloc_no
                 and bk.distro_type = 'A'
                 and bk.item = ah.item
                 and wh.wh = ah.wh) ah
       where bol_sku.distro_no = ah.alloc_no (+)
         and bol_sku.distro_no = ad.alloc_no (+)
       order by 3, 1; --distro_type, distro_no

   cursor C_SHIPMENT is
      select shipment
        from shipment
       where bol_no = I_bol_no;

   cursor C_STOCKHOLDING_IND is
      select store_type,
             stockholding_ind
        from store
       where store = L_to_loc;

   TYPE tsf_rec IS RECORD(tsf_no         SHIPSKU.DISTRO_NO%TYPE,
                          tsf_type       TSFHEAD.TSF_TYPE%TYPE,
                          tsf_item       SHIPSKU.ITEM%TYPE);
   TYPE tsf_rec_item_tbl IS TABLE of tsf_rec index by binary_integer;
   L_ils_item_tbl      tsf_rec_item_tbl;

   TYPE bol_shipsku_tbl IS TABLE of c_get_bol_ship_sku%rowtype index by binary_integer;
   L_bol_shipsku_tbl   bol_shipsku_tbl;

   L_bol_shipment_rec  C_GET_BOL_SHIPMENT%ROWTYPE;

BEGIN
   --- Validate parameters
   if I_bol_no is NULL then
      L_invalid_param := 'I_bol_no';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   open C_GET_BOL_SHIPMENT;
   fetch C_GET_BOL_SHIPMENT INTO L_bol_shipment_rec;

   if C_GET_BOL_SHIPMENT%NOTFOUND then
      close C_GET_BOL_SHIPMENT;
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC',
                                            NULL,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   close C_GET_BOL_SHIPMENT;

   if (L_bol_shipment_rec.ship_date < L_date) then
      O_error_message := SQL_LIB.CREATE_MSG('SHIP_DATE_PASSED',
                                            SQLERRM,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   L_from_loc := L_bol_shipment_rec.from_loc;
   L_to_loc := L_bol_shipment_rec.to_loc;

   open C_GET_BOL_SHIP_SKU;
   fetch C_GET_BOL_SHIP_SKU BULK COLLECT into L_bol_shipsku_tbl;
   close C_GET_BOL_SHIP_SKU;

   if L_bol_shipsku_tbl.first is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_REC',
                                            NULL,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if BOL_SQL.PUT_BOL(O_error_message,
                      L_bol_exist,
                      I_bol_no,
                      L_from_loc,
                      L_to_loc,
                      L_bol_shipment_rec.ship_date,
                      NULL,
                      L_bol_shipment_rec.no_boxes,
                      L_bol_shipment_rec.courier,
                      NULL,
                      L_bol_shipment_rec.comments) = FALSE then

      return FALSE;
   end if;

   if L_bol_exist = TRUE then
      O_error_message := SQL_LIB.CREATE_MSG('DUP_BOL',
                                            I_bol_no,
                                            L_program,
                                            NULL);
        return FALSE;
   end if;
   ---
   L_from_loc_orig := L_from_loc;
   L_to_loc_orig := L_to_loc;
   ---
   if WF_TRANSFER_SQL.IS_FRANCHISE_ORDER_RETURN(O_error_message,
                                                L_franchise_ordret_ind,
                                                L_bol_shipment_rec.from_loc,
                                                L_bol_shipment_rec.from_loc_type,
                                                L_bol_shipment_rec.to_loc,
                                                L_bol_shipment_rec.to_loc_type) = FALSE then
      return FALSE;
   end if;
   ---
   for a in L_bol_shipsku_tbl.first..L_bol_shipsku_tbl.last loop

      if L_bol_shipsku_tbl(a).check_distro is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DISTRO_TSF_ALLOC',
                                               L_bol_shipsku_tbl(a).distro_no,
                                               L_program,
                                               NULL);
         return FALSE;
      end if;

      if L_bol_shipsku_tbl(a).check_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_ALLOC_ITEM',
                                               L_bol_shipsku_tbl(a).item,
                                               L_bol_shipsku_tbl(a).distro_no,
                                               NULL);
         return FALSE;
      end if;

      if L_bol_shipsku_tbl(a).ship_qty > L_bol_shipsku_tbl(a).available_qty then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_ALLOC_QTY',
                                               L_bol_shipsku_tbl(a).distro_no,
                                               L_bol_shipsku_tbl(a).item,
                                               NULL);
         return FALSE;
      end if;
      ---
      L_from_loc := L_from_loc_orig;
      L_to_loc := L_to_loc_orig;
      ---
      if L_bol_shipsku_tbl(a).distro_type = 'T' and
         L_bol_shipment_rec.from_loc_type = 'W' and
         L_new_distro = 'Y' then
         if WH_ATTRIB_SQL.CHECK_FINISHER(O_error_message,
                                         L_finisher,
                                         L_finisher_name,
                                         L_bol_shipsku_tbl(a).from_loc) = FALSE then
            return FALSE;
         end if;

         if L_finisher = TRUE then
            if INVADJ_SQL.GET_INV_STATUS(O_error_message,
                                         L_fin_inv_status,
                                         'ATS') = FALSE THEN
               return FALSE;
            end if;
         else
            L_fin_inv_status := NULL;
         end if;
      else
         L_fin_inv_status := NULL;
      end if;

      L_new_distro := 'N';
      L_count := L_bol_items_tbl.COUNT + 1;
      L_bol_items_tbl(L_count).distro_no              := L_bol_shipsku_tbl(a).distro_no;
      L_bol_items_tbl(L_count).item                   := L_bol_shipsku_tbl(a).item;
      L_bol_items_tbl(L_count).carton                 := L_bol_shipsku_tbl(a).carton;
      L_bol_items_tbl(L_count).ship_qty               := L_bol_shipsku_tbl(a).ship_qty;
      L_bol_items_tbl(L_count).weight                 := L_bol_shipsku_tbl(a).weight_expected;
      L_bol_items_tbl(L_count).weight_uom             := L_bol_shipsku_tbl(a).weight_expected_uom;
      L_bol_items_tbl(L_count).distro_type            := L_bol_shipsku_tbl(a).distro_type;
      L_bol_items_tbl(L_count).inv_status             := NVL(L_fin_inv_status, L_bol_shipsku_tbl(a).inv_status);

      if a < L_bol_shipsku_tbl.last then
         L_next_distro_no   := L_bol_shipsku_tbl(a + 1).distro_no;
         L_next_distro_type := L_bol_shipsku_tbl(a + 1).distro_type;
      end if;

      if a = L_bol_shipsku_tbl.last or 
         L_bol_shipsku_tbl(a).distro_type != L_next_distro_type or
         L_bol_shipsku_tbl(a).distro_no != L_next_distro_no then 

         L_new_distro := 'Y';
         --Pass the L_sellable table to BOL_SQL.PROCESS_ITEM
         if BOL_SQL.PROCESS_ITEM(O_error_message,
                                 L_item_tbl,  -- output table
                                 L_bol_items_tbl,
                                 L_from_loc,
                                 L_to_loc) = FALSE then
            return FALSE;
         end if;

         L_distro_no := L_item_tbl(1).distro_no;
         L_distro_type := L_item_tbl(1).distro_type;

         if L_distro_type = 'T' then
            if TRANSFER_SQL.GET_TSFHEAD_INFO(O_error_message,
                                             L_tsfhead_info,
                                             L_distro_no) = FALSE then
               return FALSE;
            end if;

            if L_tsfhead_info.finisher_type = 'I' and
               L_tsfhead_info.leg_1_status = 'C' and
               L_tsfhead_info.leg_2_status in ('A','S') then
               L_distro_no := L_tsfhead_info.child_tsf_no;
            end if;

            L_tsf_type := L_bol_shipsku_tbl(a).tsf_type;
            if BOL_SQL.PUT_TSF(O_error_message,
                               L_bol_shipment_rec.to_loc,
                               L_bol_shipment_rec.from_loc,
                               L_tsf_type,
                               L_distro_no,
                               L_from_loc,
                               L_bol_shipment_rec.from_loc_type,
                               L_to_loc,
                               L_bol_shipment_rec.to_loc_type,
                               L_date,
                               NULL) = FALSE then
              return FALSE;
            end if;

            if TRANSFER_SQL.GET_FINISHER_INFO(O_error_message,
                                              L_finisher_loc_ind,
                                              L_finisher_entity_ind,
                                              L_distro_no)= FALSE THEN
               return FALSE;
            end if;

            for k in L_item_tbl.first..L_item_tbl.last LOOP
               if BOL_SQL.PUT_TSF_ITEM(O_error_message,
                                       L_detail_created,
                                       L_item_tbl(k).distro_no,
                                       L_item_tbl(k).item,
                                       L_item_tbl(k).carton,
                                       L_item_tbl(k).ship_qty,
                                       L_item_tbl(k).weight,
                                       L_item_tbl(k).weight_uom,
                                       L_item_tbl(k).inv_status,
                                       L_from_loc,
                                       L_bol_shipment_rec.from_loc_type,
                                       L_to_loc,
                                       L_bol_shipment_rec.to_loc_type,
                                       L_bol_shipment_rec.to_loc,
                                       L_bol_shipment_rec.from_loc,
                                       L_tsf_type,
                                       'Y') = FALSE then
                  return FALSE;
               end if;

               if L_finisher = FALSE then
                  L_ctr := L_ils_item_tbl.COUNT + 1;
                  L_ils_item_tbl(L_ctr).tsf_no   := L_distro_no;
                  L_ils_item_tbl(L_ctr).tsf_type := L_tsf_type;
                  L_ils_item_tbl(L_ctr).tsf_item := L_item_tbl(k).item;
               end if;
            END LOOP;

            if BOL_SQL.PROCESS_TSF(O_error_message) = FALSE then
               return FALSE;
            end if;

            if L_franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
               if WF_BOL_SQL.UPDATE_SHIPPED_WF_ORDER(O_error_message,
                                                     L_distro_no,
                                                     'T',
                                                     NULL,
                                                     NULL) = FALSE then
                  return FALSE;
               end if;
            elsif L_franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_RETURN then
               if WF_BOL_SQL.UPDATE_SHIPPED_WF_RETURN(O_error_message,
                                                      L_distro_no) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;

         if L_distro_type = 'A' then
            for k in L_item_tbl.first..L_item_tbl.last LOOP
               if BOL_SQL.PUT_ALLOC(O_error_message,
                                     L_item_tbl(k).item,
                                     L_bol_shipment_rec.from_loc,
                                     L_distro_no,
                                     L_from_loc, --physical location
                                     L_item_tbl(k).item) = FALSE then
                  return FALSE;
               end if;

               if BOL_SQL.PUT_ALLOC_ITEM(O_error_message,
                                         L_detail_created,
                                         L_item_tbl(k).distro_no,
                                         L_item_tbl(k).item,
                                         L_item_tbl(k).carton,
                                         L_item_tbl(k).ship_qty,
                                         L_item_tbl(k).weight,
                                         L_item_tbl(k).weight_uom,
                                         L_item_tbl(k).inv_status,
                                         L_to_loc,                         -- physical location
                                         L_bol_shipment_rec.to_loc_type,
                                         L_from_loc) = FALSE then          -- physical location
                  return FALSE;
               end if;
            END LOOP;

            if BOL_SQL.PROCESS_ALLOC(O_error_message) = FALSE then
               return FALSE;
            end if;

            if L_franchise_ordret_ind = WF_TRANSFER_SQL.ACTION_CREATE_WF_ORDER then
               if WF_BOL_SQL.UPDATE_SHIPPED_WF_ORDER(O_error_message,
                                                     L_distro_no,
                                                     'A',
                                                     L_to_loc,
                                                     L_bol_shipment_rec.to_loc_type) = FALSE then
                  return FALSE;
               end if;
            end if;
         end if;

         L_item_tbl.delete;
         L_bol_items_tbl.delete;

      end if;
   END LOOP;

   if BOL_SQL.FLUSH_BOL_PROCESS(O_error_message) = FALSE then
      return FALSE;
   end if;

   open C_SHIPMENT;
   fetch C_SHIPMENT into L_shipment;
   close C_SHIPMENT;
   ---
   if L_ils_item_tbl.first is NOT NULL then
      if L_finisher_loc_ind is NOT NULL then
         for i in L_ils_item_tbl.first..L_ils_item_tbl.last LOOP
            if BOL_SQL.PUT_ILS_AV_RETAIL(O_error_message,
                                         L_bol_shipment_rec.to_loc,
                                         L_bol_shipment_rec.to_loc_type,
                                         L_ils_item_tbl(i).tsf_item,
                                         L_shipment,
                                         L_distro_no,
                                         L_ils_item_tbl(i).tsf_type,
                                         NULL) = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;
   end if;

   L_pub_shipment := TRUE;

   if L_bol_shipment_rec.to_loc_type ='S' then
      open C_STOCKHOLDING_IND;
      fetch C_STOCKHOLDING_IND into L_store_type,
                                    L_stockholding_ind;
      close C_STOCKHOLDING_IND;

      if L_store_type = 'F' and L_stockholding_ind = 'N' then
         L_pub_shipment := FALSE;
      end if;
   end if;

   if L_pub_shipment = TRUE then
      if PUB_SHIPMENT(O_error_message,
                      L_bol_shipment_rec.to_loc_type,
                      L_shipment) = FALSE then
         return FALSE;
      end if;
   end if;
   L_ils_item_tbl.delete;
   L_bol_shipsku_tbl.delete;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END SHIP_DISTROS;
--------------------------------------------------------------------------------
FUNCTION LOCK_ORDER_SHIPMENT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_program       VARCHAR2(60) := 'SHIPMENT_SQL.LOCK_ORDER_SHIPMENT';

   cursor C_LOCK_ORDER is
      select 'x'
        from ordhead_lock oh,
             ordloc ol
       where oh.order_no = ol.order_no
         and oh.order_no = I_order_no
         for update nowait;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            I_order_no,
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_LOCK_ORDER;
   close C_LOCK_ORDER;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDHEAD_REC_LOC',
                                             I_order_no,
                                             NULL,
                                             NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_ORDER_SHIPMENT;
----------------------------------------------------------------------------
FUNCTION DELETE_CARTON(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_shipment      IN     SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(64) := 'SHIPMENT_SQL.DELETE_CARTON';

BEGIN
  
  delete from carton where carton in(select sk.carton
                                       from shipsku sk,
                                            shipment sh
                                      where sk.shipment = I_shipment
                                        and sh.status_code = 'I'
                                        and sk.shipment = sh.shipment);
  
  return TRUE;
  
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;
END DELETE_CARTON;
----------------------------------------------------------------------------
----------PRIVATE FUNCTION--------------------------------------------------
----------------------------------------------------------------------------

FUNCTION PUB_SHIPMENT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_to_loc_type   IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                      I_shipment      IN     SHIPMENT.SHIPMENT%TYPE,
                      I_wf_ship_ind   IN     BOOLEAN DEFAULT FALSE)
RETURN BOOLEAN IS

   L_system_options_row   SYSTEM_OPTIONS%ROWTYPE;
   L_program              VARCHAR2(64) := 'SHIPMENT_SQL.PUB_SHIPMENT';
   L_to_loc               NUMBER;

BEGIN

   if I_shipment is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_shipment',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;

   if I_to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_to_loc_type',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;


   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_row) = FALSE then
      return FALSE;
   end if;

   -- Need to publish shipments in following scenarios
   -- 1. Destination is store, ship_rcv_store = 'N' and ship_rcv_wh = 'Y'
   -- 2. Destination is a wh, ship_rcv_wh = 'N' and ship_rcv_store = 'Y'
   -- These indicate that the shipment is to a store and store receiving is happening
   -- in a separate store system (e.g. SIM), or the shipment is to a warehouse and
   -- warehouse receiving is happening in a separate wh system (e.g. RWMS).
   -- Only in these cases, the shipment needs to be published to inform
   -- the receiving location of the coming shipment.
   -- 3. In case of Franchise Returns, only for non stockholding Franchise stores shipments will to be published irrespctive of ship_rcv_wh_ind
   --    and ship_rcv_store ind . Incase of stock holding franchise store, ship_rcv_store = 'N' and ship_rcv_wh = 'Y'.


   if ((L_system_options_row.ship_rcv_store = 'N' and I_to_loc_type = 'S') and L_system_options_row.ship_rcv_wh    = 'Y') or
      ((L_system_options_row.ship_rcv_wh    = 'N' and I_to_loc_type = 'W') and L_system_options_row.ship_rcv_store = 'Y') or
      I_wf_ship_ind = TRUE then
      ---
      insert into shipment_pub_temp(shipment)
                             values(I_shipment);
      ---
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PUB_SHIPMENT;
----------------------------------------------------------------------------

END SHIPMENT_SQL;
/
