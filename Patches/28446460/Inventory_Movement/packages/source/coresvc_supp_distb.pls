create or replace 
PACKAGE BODY CORESVC_SUPP_DIST AS
--------------------------------------------------------------------------------
   cursor C_SVC_REPL_ITEM_LOC_SUPP_DIST(I_process_id NUMBER,
                                        I_chunk_id   NUMBER) is
      select pk_repl_item_loc_supp_dist.rowid  as pk_repl_item_loc_supp_dist_rid,
             rid_isl_fk.rowid                  as rid_isl_fk_rid,
             st.rowid                          as st_rid,
             st.dist_pct,
             UPPER(st.origin_country_id)       as origin_country_id,
             st.supplier,
             st.location,
             UPPER(st.item)                    as item,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action) as action,
             st.process$status,
             row_number() over (partition BY st.item,st.location order by st.item) as item_loc_rank ,
             LEAD(st.item, 1, 0) over (order by st.item,st.location)               as next_item,
             LEAD(st.location, 1, 0) over (order by st.item,st.location)               as next_loc
        from svc_repl_item_loc_supp_dist st,
             repl_item_loc_supp_dist pk_repl_item_loc_supp_dist,
             item_supp_country_loc rid_isl_fk,
             dual
       where st.process_id                = I_process_id
         and st.chunk_id                  = I_chunk_id
         and UPPER(st.origin_country_id)  = pk_repl_item_loc_supp_dist.origin_country_id (+)
         and st.supplier                  = pk_repl_item_loc_supp_dist.supplier (+)
         and st.location                  = pk_repl_item_loc_supp_dist.location (+)
         and UPPER(st.item)               = pk_repl_item_loc_supp_dist.item (+)
         and UPPER(st.origin_country_id)  = rid_isl_fk.origin_country_id (+)
         and st.supplier                  = rid_isl_fk.supplier (+)
         and st.location                  = rid_isl_fk.loc (+)
         and UPPER(st.item)               = rid_isl_fk.item (+);

   TYPE errors_tab_typ is TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ is TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;
--------------------------------------------------------------------------------

FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------

PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------

PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
--------------------------------------------------------------------------------

PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets S9T_PKG.NAMES_MAP_TYP;
   REPL_ITEM_LOC_SUPP_DIST_cols S9T_PKG.NAMES_MAP_TYP;
BEGIN
   L_sheets                       := s9t_pkg.GET_SHEET_NAMES(I_file_id);
   REPL_ITEM_LOC_SUPP_DIST_cols   := s9t_pkg.GET_COL_NAMES(I_file_id,
                                                           REPL_ITEM_LOC_SUPP_DIST_sheet);
   RILSD$Action                   := REPL_ITEM_LOC_SUPP_DIST_cols('ACTION');
   RILSD$DIST_PCT                 := REPL_ITEM_LOC_SUPP_DIST_cols('DIST_PCT');
   RILSD$ORIGIN_COUNTRY_ID        := REPL_ITEM_LOC_SUPP_DIST_cols('ORIGIN_COUNTRY_ID');
   RILSD$SUPPLIER                 := REPL_ITEM_LOC_SUPP_DIST_cols('SUPPLIER');
   RILSD$LOCATION                 := REPL_ITEM_LOC_SUPP_DIST_cols('LOCATION');
   RILSD$ITEM                     := REPL_ITEM_LOC_SUPP_DIST_cols('ITEM');
END POPULATE_NAMES;
--------------------------------------------------------------------------------

PROCEDURE POPULATE_RILSD( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from S9T_FOLDER sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = REPL_ITEM_LOC_SUPP_DIST_sheet )
   select s9t_row(s9t_cells(CORESVC_SUPP_DIST.action_mod,
                            item,
                            location,
                            supplier,
                            origin_country_id,
                            dist_pct))
     from repl_item_loc_supp_dist ;
END POPULATE_RILSD;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(REPL_ITEM_LOC_SUPP_DIST_sheet);
   L_file.sheets(l_file.GET_SHEET_INDEX(REPL_ITEM_LOC_SUPP_DIST_sheet)).column_headers := s9t_cells('ACTION',
                                                                                                    'ITEM',
                                                                                                    'LOCATION',
                                                                                                    'SUPPLIER',
                                                                                                    'ORIGIN_COUNTRY_ID',
                                                                                                    'DIST_PCT');

   s9t_pkg.SAVE_OBJ(l_file);

END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_SUPP_DIST.CREATE_S9T';
   L_file S9T_FILE;

BEGIN
   INIT_S9T(O_file_id);

   --populate column lists
	if S9T_PKG.POPULATE_LISTS(O_error_message,
                              O_file_id,
                              template_category,
							  template_key)= FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_RILSD(O_file_id);
      commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);

   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);

   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file)= FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);

   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------

PROCEDURE PROCESS_S9T_RILSD(I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                            I_process_id IN   SVC_REPL_ITEM_LOC_SUPP_DIST.process_id%TYPE) IS
   TYPE svc_rilsd_col_typ is TABLE OF SVC_REPL_ITEM_LOC_SUPP_DIST%ROWTYPE;
   L_temp_rec    SVC_REPL_ITEM_LOC_SUPP_DIST%ROWTYPE;
   svc_RILSD_col svc_rilsd_col_typ := NEW svc_rilsd_col_typ();
   L_process_id  SVC_REPL_ITEM_LOC_SUPP_DIST.process_id%TYPE;
   L_error       BOOLEAN:= FALSE;
   L_default_rec SVC_REPL_ITEM_LOC_SUPP_DIST%ROWTYPE;
   cursor C_MANDATORY_IND is
      select dist_pct_mi,
             origin_country_id_mi,
             supplier_mi,
             location_mi,
             item_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = CORESVC_SUPP_DIST.template_key
                 and wksht_key         = 'REPL_ITEM_LOC_SUPP_DIST'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ('DIST_PCT'          as dist_pct,
                                            'ORIGIN_COUNTRY_ID' as origin_country_id,
                                            'SUPPLIER'          as supplier,
                                            'LOCATION'          as location,
                                            'ITEM'              as item));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_REPL_ITEM_LOC_SUPP_DIST';
   L_pk_columns    VARCHAR2(255)  := 'Item,Location,Supplier,Country Of Sourcing';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN (select  dist_pct_dv,
                       origin_country_id_dv,
                       supplier_dv,
                       location_dv,
                       item_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key       = CORESVC_SUPP_DIST.template_key
                          and wksht_key          = 'REPL_ITEM_LOC_SUPP_DIST')
                       PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'DIST_PCT'          as dist_pct,
                                                      'ORIGIN_COUNTRY_ID' as origin_country_id,
                                                      'SUPPLIER'          as supplier,
                                                      'LOCATION'          as location,
                                                      'ITEM'              as item)))
   LOOP
   BEGIN
      L_default_rec.dist_pct := rec.dist_pct_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'REPL_ITEM_LOC_SUPP_DIST',
                         NULL,
                         'DIST_PCT',
                         'INV_DEFAULT',
                         SQLERRM);
   END;
   BEGIN
      L_default_rec.origin_country_id := rec.origin_country_id_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'REPL_ITEM_LOC_SUPP_DIST',
                         NULL,
                         'ORIGIN_COUNTRY_ID',
                         'INV_DEFAULT',
                         SQLERRM);
   END;
   BEGIN
      L_default_rec.supplier := rec.supplier_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'REPL_ITEM_LOC_SUPP_DIST',
                         NULL,
                         'SUPPLIER',
                         'INV_DEFAULT',
                         SQLERRM);
   END;
   BEGIN
      L_default_rec.location := rec.location_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'REPL_ITEM_LOC_SUPP_DIST',
                         NULL,
                         'LOCATION',
                         'INV_DEFAULT',
                         SQLERRM);
   END;
   BEGIN
      L_default_rec.item := rec.item_dv;
   EXCEPTION
      when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         'REPL_ITEM_LOC_SUPP_DIST',
                         NULL,
                         'ITEM',
                         'INV_DEFAULT',
                         SQLERRM);
   END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN(select r.GET_CELL(RILSD$ACTION)                   as action,
                     r.GET_CELL(RILSD$DIST_PCT)                 as dist_pct,
                     UPPER(r.GET_CELL(RILSD$ORIGIN_COUNTRY_ID)) as origin_country_id,
                     r.GET_CELL(RILSD$SUPPLIER)                 as supplier,
                     r.GET_CELL(RILSD$LOCATION)                 as location,
                     UPPER(r.GET_CELL(RILSD$ITEM))              as item,
                     r.GET_ROW_SEQ()                            as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = GET_SHEET_NAME_TRANS(REPL_ITEM_LOC_SUPP_DIST_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            REPL_ITEM_LOC_SUPP_DIST_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.dist_pct := rec.dist_pct;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            REPL_ITEM_LOC_SUPP_DIST_sheet,
                            rec.row_seq,
                            'DIST_PCT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.origin_country_id := rec.origin_country_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            REPL_ITEM_LOC_SUPP_DIST_sheet,
                               rec.row_seq,
                               'ORIGIN_COUNTRY_ID',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.supplier := rec.supplier;
      EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               REPL_ITEM_LOC_SUPP_DIST_sheet,
                               rec.row_seq,
                               'SUPPLIER',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.location := rec.location;
      EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               REPL_ITEM_LOC_SUPP_DIST_sheet,
                               rec.row_seq,
                               'LOCATION',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.item := rec.item;
      EXCEPTION
            when OTHERS then
               WRITE_S9T_ERROR(I_file_id,
                               REPL_ITEM_LOC_SUPP_DIST_sheet,
                               rec.row_seq,
                               'ITEM',
                               SQLCODE,
                               SQLERRM);
               L_error := TRUE;
      END;
      if rec.action = CORESVC_SUPP_DIST.action_new then
         L_temp_rec.dist_pct          := NVL( L_temp_rec.dist_pct,L_default_rec.dist_pct);
         L_temp_rec.origin_country_id := NVL( L_temp_rec.origin_country_id,L_default_rec.origin_country_id);
         L_temp_rec.supplier          := NVL( L_temp_rec.supplier,L_default_rec.supplier);
         L_temp_rec.location          := NVL( L_temp_rec.location,L_default_rec.location);
         L_temp_rec.item              := NVL( L_temp_rec.item,L_default_rec.item);
      end if;
      if NOT (L_temp_rec.origin_country_id is NOT NULL and
              L_temp_rec.supplier          is NOT NULL and
              L_temp_rec.location          is NOT NULL and
              L_temp_rec.item              is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         REPL_ITEM_LOC_SUPP_DIST_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_RILSD_col.EXTEND();
         svc_RILSD_col(svc_RILSD_col.COUNT()):=L_temp_rec;
      end if;

   END LOOP;

   BEGIN
   FORALL i IN 1..svc_RILSD_col.COUNT SAVE EXCEPTIONS
   Merge into SVC_REPL_ITEM_LOC_SUPP_DIST st USING
      (select (case
                  when L_mi_rec.dist_pct_mi    = 'N'
                  and svc_RILSD_col(i).action = CORESVC_SUPP_DIST.action_mod
                  and s1.dist_pct             is NULL
                  then mt.dist_pct
                  else s1.dist_pct
              end) as dist_pct,
             (case
                 when L_mi_rec.origin_country_id_mi    = 'N'
                  and svc_RILSD_col(i).action = CORESVC_SUPP_DIST.action_mod
                  and s1.origin_country_id    is NULL
                 then mt.origin_country_id
                 else s1.origin_country_id
             end) as origin_country_id,
             (case
                 when L_mi_rec.supplier_mi    = 'N'
                  and svc_RILSD_col(i).action = CORESVC_SUPP_DIST.action_mod
                  and s1.supplier             is NULL
                 then mt.supplier
                 else s1.supplier
             end) as supplier,
             (case
                 when L_mi_rec.location_mi    = 'N'
                  and svc_RILSD_col(i).action = CORESVC_SUPP_DIST.action_mod
                  and s1.location             is NULL
                 then mt.location
                 else s1.location
             end) as location,
             (case
                 when L_mi_rec.item_mi    = 'N'
                  and svc_RILSD_col(i).action = CORESVC_SUPP_DIST.action_mod
                  and s1.item             is NULL
                 then mt.item
                 else s1.item
             end) as item,
             null as dummy
      from (select svc_RILSD_col(i).dist_pct as dist_pct,
                   svc_RILSD_col(i).origin_country_id as origin_country_id,
                   svc_RILSD_col(i).supplier as supplier,
                   svc_RILSD_col(i).location as location,
                   svc_RILSD_col(i).item as item
              from dual ) s1,
            repl_item_loc_supp_dist mt
      where mt.origin_country_id (+)     = s1.origin_country_id   and
            mt.supplier (+)              = s1.supplier   and
            mt.location (+)              = s1.location   and
            mt.item (+)                  = s1.item) sq
       ON (st.origin_country_id   = sq.origin_country_id and
           st.supplier            = sq.supplier and
           st.location            = sq.location and
           st.item                = sq.item and
           svc_RILSD_col(i).ACTION in (CORESVC_SUPP_DIST.action_mod,CORESVC_SUPP_DIST.action_del))
      when matched then
         update set   process_id        = svc_rilsd_col(i).process_id ,
                      chunk_id          = svc_rilsd_col(i).chunk_id ,
                      row_seq           = svc_rilsd_col(i).row_seq ,
                      action            = svc_rilsd_col(i).action,
                      process$status    = svc_rilsd_col(i).process$status ,
                      dist_pct          = sq.dist_pct ,
                      create_id         = svc_rilsd_col(i).create_id ,
                      create_datetime   = svc_rilsd_col(i).create_datetime ,
                      last_upd_id       = svc_rilsd_col(i).last_upd_id ,
                      last_upd_datetime = svc_rilsd_col(i).last_upd_datetime
      when NOT matched then
         insert ( process_id ,
                  chunk_id ,
                  row_seq ,
                  action ,
                  process$status ,
                  dist_pct ,
                  origin_country_id ,
                  supplier ,
                  location ,
                  item ,
                  create_id ,
                  create_datetime ,
                  last_upd_id ,
                  last_upd_datetime)
         values ( svc_rilsd_col(i).process_id ,
                  svc_rilsd_col(i).chunk_id ,
                  svc_rilsd_col(i).row_seq ,
                  svc_rilsd_col(i).action ,
                  svc_rilsd_col(i).process$status ,
                  sq.dist_pct ,
                  sq.origin_country_id ,
                  sq.supplier ,
                  sq.location ,
                  sq.item ,
                  svc_rilsd_col(i).create_id ,
                  svc_rilsd_col(i).create_datetime ,
                  svc_rilsd_col(i).last_upd_id ,
                  svc_rilsd_col(i).last_upd_datetime);
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT
      LOOP
      L_error_code:=sql%bulk_exceptions(i).error_code;
      if L_error_code=1 then
         L_error_code:=NULL;
         L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                           L_pk_columns);
      end if;
      WRITE_S9T_ERROR( I_file_id,
                       REPL_ITEM_LOC_SUPP_DIST_sheet,
                       svc_RILSD_col(sql%bulk_exceptions(i).error_index).row_seq,
                       NULL,
                       L_error_code,
                       L_error_msg);
      END LOOP;
   END;

END PROCESS_S9T_RILSD;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count   IN OUT NUMBER,
                     I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_SUPP_DIST.PROCESS_S9T';
   L_file           S9T_FILE;
   L_sheets         S9T_PKG.NAMES_MAP_TYP;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	    EXCEPTION;
   PRAGMA	    EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN

   commit;--to ensure that the record in s9t_folder is commited
   s9t_pkg.ODS2OBJ(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE)= FALSE then
      return FALSE;
   end if;
   s9t_pkg.SAVE_OBJ(l_file);
   if s9t_pkg.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(l_file.template_key,l_file.user_lang);
      PROCESS_S9T_RILSD(I_file_id,I_process_id);
   end if;

   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);

   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   -- Update SVC_PROCESS_TRACKER
   if O_error_count = 0 then
 	   L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
	       file_id    = I_file_id
	 where process_id = I_process_id;
	 commit;

   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values lp_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_RILSD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error             IN OUT BOOLEAN,
                           I_rec               IN     C_SVC_REPL_ITEM_LOC_SUPP_DIST%ROWTYPE)
RETURN BOOLEAN IS
   L_program                     VARCHAR2(64)                      := 'CORESVC_SUPP_DIST.PROCESS_VAL_RILSD';
   LP_system_options_row         SYSTEM_OPTIONS%ROWTYPE;
   L_table                       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_REPL_ITEM_LOC_SUPP_DIST';
   L_supplier_sites_ind          SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE;
   L_supplier_rec                V_SUPS%ROWTYPE;
   L_supplier_access_exists      BOOLEAN;
   L_valid                       BOOLEAN;
   L_exists                      BOOLEAN;
   L_item_master                 V_ITEM_MASTER%ROWTYPE;
   L_wh                          V_WH%ROWTYPE;
   L_store                       V_STORE%ROWTYPE;
   L_country_desc                COUNTRY.COUNTRY_DESC%TYPE;

BEGIN
   -- Validate ITEM
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_ITEM_MASTER(O_error_message,
                                                   L_valid,
                                                   L_item_master,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   I_rec.item) = FALSE or
      L_valid = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'ITEM',
                  O_error_message);
      O_error := TRUE;
   end if;

   -- Validate if the ITEM is active
   /*if SUP_DIST_SQL.CHECK_ITEM_LOC(O_error_message,
                                  L_exists,
                                  I_rec.item,
                                  NULL) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error := TRUE;
   else
      if L_exists = FALSE then
         WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'ITEM',
                  'INA_REPL_ITEM_ANY_LOC');
         O_error := TRUE;
      end if;
   end if;*/

   -- Validate ITEM if its active for the current location
    if SUP_DIST_SQL.CHECK_ITEM_LOC(O_error_message,
                                   L_valid,
                                   I_rec.item,
                                   I_rec.location) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error := TRUE;
   else
      if L_valid = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Item,Location',
                     'INA_REPL_ITEM_CURR_LOC');
         O_error := TRUE;
      end if;
   end if;

   --Validation for the origin Country ID
   if COUNTRY_VALIDATE_SQL.GET_NAME(O_error_message,
                                    I_rec.origin_country_id,
                                    L_country_desc) = FALSE then
      	  WRITE_ERROR(I_rec.process_id,
                       SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                       I_rec.chunk_id,
                       L_table,
                       I_rec.row_seq,
                       'ORIGIN_COUNTRY_ID',
                       O_error_message);
         O_error := TRUE;
      end if;


   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error := TRUE;
   end if;

   L_supplier_sites_ind := LP_system_options_row.supplier_sites_ind;
   -- Check if user has access to Supplier
   if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(O_error_message,
                                                L_supplier_access_exists,
                                                L_supplier_rec,
                                                I_rec.supplier) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error :=TRUE;
   end if;

   if L_supplier_access_exists = FALSE then
                  WRITE_ERROR(I_rec.process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                              I_rec.chunk_id,
                              L_table,
                              I_rec.row_seq,
                              'SUPPLIER',
                              O_error_message);
                  O_error := TRUE;
   else
      if L_supplier_sites_ind = 'Y'
	     and L_supplier_rec.supplier_parent is NULL then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'SUPPLIER',
                     'INV_SUPPLIER_SITE');
         O_error := TRUE;
      elsif L_supplier_sites_ind = 'N'
	        and L_supplier_rec.supplier_parent is NOT NULL then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'SUPPLIER',
                        'INV_SUPPLIER');
            O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
  return FALSE;

END PROCESS_VAL_RILSD;
--------------------------------------------------------------------------------
FUNCTION PROCESS_RILSD(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_process_id        IN     SVC_REPL_ITEM_LOC_SUPP_DIST.PROCESS_ID%TYPE,
                       I_chunk_id          IN     SVC_REPL_ITEM_LOC_SUPP_DIST.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program   VARCHAR2(64)                     := 'CORESVC_SUPP_DIST.PROCESS_RILSD';
   L_table SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    := 'SVC_REPL_ITEM_LOC_SUPP_DIST';
   L_error BOOLEAN;
   L_process_error BOOLEAN  := FALSE;
   L_rilsd_temp_rec REPL_ITEM_LOC_SUPP_DIST%ROWTYPE;
   L_sum NUMBER;
   L_supp_dist_rec_tab  SUP_DIST_SQL.REPL_SUPP_DIST_TBL_TYPE;
   L_supp_dist_rec  SUP_DIST_SQL.REPL_SUPP_DIST_REC;
   L_write_error_flag BOOLEAN := FALSE;

	L_rowseq_count               NUMBER;
   c                            NUMBER      := 1;
	TYPE L_row_seq_tab_type IS TABLE OF SVC_REPL_ITEM_LOC_SUPP_DIST.ROW_SEQ%TYPE;
   L_row_seq_tab                       L_row_seq_tab_type;
   cursor C_GET_DISTPCT_TOTAL(I_item     SVC_REPL_ITEM_LOC_SUPP_DIST.ITEM%TYPE,
                              I_location SVC_REPL_ITEM_LOC_SUPP_DIST.LOCATION%TYPE) is
      select SUM(dist_pct)
        from repl_item_loc_supp_dist rilsd
       where rilsd.item = I_item
         and rilsd.location = I_location;

--    L_supp_dist_tbl_rec L_supp_dist_tbl := NEW L_supp_dist_tbl();
BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   FOR rec IN C_SVC_REPL_ITEM_LOC_SUPP_DIST(I_process_id,
                                            I_chunk_id)
   LOOP
      L_error            := FALSE;
      L_sum              := 0;
      L_process_error    := FALSE;

      if rec.item_loc_rank = 1 then
         L_row_seq_tab.DELETE;
         c:=1;
			SAVEPOINT process_rilsd_savepoint;
      end if;

      if rec.action is NULL
         or rec.action NOT IN (action_mod) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error := TRUE;
      end if;

      if rec.dist_pct  is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIST_PCT',
                     'DIST_PCT_IS_NULL');
         L_error :=TRUE;
      end if;
		if  rec.dist_pct < 0
          or rec.dist_pct > 100 then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DIST_PCT',
                     'VALID_PERCENT_RANGE');
         L_error :=TRUE;
      end if;

      if rec.dist_pct  is NOT NULL
         and rec.origin_country_id  is NOT NULL
         and rec.supplier  is NOT NULL
         and rec.location  is NOT NULL
         and rec.item  is NOT NULL
         and rec.rid_isl_fk_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'Item,Location,Supplier,Country Of Sourcing,Percent',
                     'RILSD_NO_RECORDS_EXIST');
         L_error :=TRUE;
      end if;

      if rec.supplier  is NOT NULL then
         if PROCESS_VAL_RILSD(O_error_message,
                              L_error,
                              rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        O_error_message);
            L_error := TRUE;
         end if;
      end if;

      if NOT L_error then
         L_rilsd_temp_rec.item                  := rec.item;
         L_rilsd_temp_rec.location              := rec.location;
         L_rilsd_temp_rec.supplier              := rec.supplier;
         L_rilsd_temp_rec.origin_country_id     := rec.origin_country_id;
         L_rilsd_temp_rec.dist_pct              := rec.dist_pct;
         L_rilsd_temp_rec.last_update_datetime  := SYSDATE;
		     L_supp_dist_rec.dist_pct                := rec.dist_pct;
         L_supp_dist_rec.supplier               := rec.supplier;
         L_supp_dist_rec.origin_country_id      := rec.origin_country_id;

         L_supp_dist_rec_tab(1) := L_supp_dist_rec;

         SUP_DIST_SQL.LOCK_PROCEDURE (L_supp_dist_rec_tab,
                                      rec.item,
                                      rec.location);
         if L_supp_dist_rec_tab(1).return_code = 'FALSE' then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_chunk_id,
                        L_table,
                        rec.row_seq,
                        NULL,
                        L_supp_dist_rec_tab(1).error_message);

           L_process_error    := TRUE;
         else
			   SUP_DIST_SQL.UPDATE_PROCEDURE(L_supp_dist_rec_tab,
                                          rec.item,
                                          rec.location);
            if L_supp_dist_rec_tab(1).return_code = 'FALSE' then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_supp_dist_rec_tab(1).error_message);
              L_process_error    := TRUE;
            else
            
				   if NOT L_process_error then
                        L_row_seq_tab.extend();
                        L_row_seq_tab(c)  := rec.row_seq;
                        c:=c+1;
               end if;
               
               if (rec.item <> rec.next_item 
                   or rec.location <> rec.next_loc) then
                  open C_GET_DISTPCT_TOTAL(rec.item,rec.location);
                  fetch C_GET_DISTPCT_TOTAL into L_sum;
                  close C_GET_DISTPCT_TOTAL;
               
						       if ROUND(NVL(L_sum,0)) != 100 then
                     L_rowseq_count := L_row_seq_tab.count();
                     if L_rowseq_count > 0 then
                        for i in 1..L_rowseq_count
                        LOOP
							      WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                                 I_chunk_id,
                                 L_table,
                                 L_row_seq_tab(i),
                                 'DIST_PCT',
                                 'INV_TOT_SUP_DIST_PCT');
                        END LOOP;
							end if;
							L_process_error    := TRUE;
                     ROLLBACK TO process_rilsd_savepoint;
                  end if;
               end if;
            end if;
         end if;
      end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      if C_GET_DISTPCT_TOTAL%ISOPEN then
         close C_GET_DISTPCT_TOTAL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_RILSD;
------------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
	 from svc_repl_item_loc_supp_dist
	where process_id = I_process_id;
END;
--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_SUPP_DIST.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_RILSD(O_error_message,
                    I_process_id,
                    I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
	    L_process_status := 'PE';
   end if;

   update svc_process_tracker
	    set status = (CASE
		                when status = 'PE'
		                then 'PE'
                    else L_process_status
                    END),
		  action_date = sysdate
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
	  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_SUPP_DIST;
/