CREATE OR REPLACE PACKAGE BODY RMSSUB_XTSF_VALIDATE AS

TYPE inv_status_types_TBL is table of inv_status_types%ROWTYPE;

LP_inv_status_types       inv_status_types_TBL;

-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the create and modify messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XTsfDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_REQUIRED_FIELDS
   -- Purpose      : This function will check all required fields for the delete messages
   --                to ensure that they are not null.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XTsfRef_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: VALIDATE_MESSAGE
   -- Purpose      : This function will validate any data in the message against the system.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_XTsfDesc_REC",
                          I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_XTsfRef_REC",
                          I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: VALIDATE_LOCATION
   -- Purpose      : This function will validate a location.
-------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_physical_wh         OUT  WH.WH%TYPE,
                           I_loc              IN      WH.WH%TYPE,
                           I_loc_type         IN      ITEM_LOC.LOC_TYPE%TYPE,
                           I_tsf_type         IN      TSFHEAD.TSF_TYPE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: CHECK_TSF_EXISTS
   -- Purpose      : This function will verify if the TSF record already exists.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TSF_EXISTS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no            IN       TSFHEAD.TSF_NO%TYPE,
                          I_message_type      IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB TSF object into a TSF record defined in
   --                the MERCH_SQL package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                         O_tsf_rec         IN OUT    NOCOPY RMSSUB_XTSF.TSF_REC,
                         I_message         IN               "RIB_XTsfDesc_REC",
                         I_message_type    IN               VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: POPULATE_RECORD
   -- Purpose      : This function will convert the RIB TSF object into a TSF record defined in
   --                the MERCH_SQL package.
-------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_tsf_rec         OUT    NOCOPY   RMSSUB_XTSF.TSF_REC,
                         I_message         IN              "RIB_XTsfRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name: GET_INV_STATUS_TYPES
-- Purpose      : Checks for the existence of the input inv_status
--                in the table inv_status_types.  The database table is cached in the global
--                variable LP_inv_status_types, since it is a small table that could
--                potentially be queried many times.
-------------------------------------------------------------------------------------------------------
-- PRIVATE FUNCTION
FUNCTION GET_INV_STATUS_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
FUNCTION GET_INV_STATUS_TYPES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              I_inv_status      IN       INV_ADJ.INV_STATUS%TYPE)
   RETURN BOOLEAN IS

   INV_INV_STATUS        EXCEPTION;

   cursor C_INV_STATUS_TYPES is
      select *
        from inv_status_types;
BEGIN

   if LP_inv_status_types is NULL then
      open C_INV_STATUS_TYPES;
      fetch C_INV_STATUS_TYPES BULK COLLECT into LP_inv_status_types;
      close C_INV_STATUS_TYPES;
   end if;

   FOR i IN 1..LP_inv_status_types.COUNT LOOP
      if LP_inv_status_types(i).inv_status = I_inv_status then
         return TRUE;
      end if;
   END LOOP;

   raise INV_INV_STATUS;

EXCEPTION
   when INV_INV_STATUS then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INV_STATUS',
                                            I_inv_status,
                                            NULL,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RMSSUB_XTSF_VALIDATE.GET_INV_STATUS',
                                            to_char(SQLCODE));
      return FALSE;
END GET_INV_STATUS_TYPES;
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tsf_rec            OUT NOCOPY   RMSSUB_XTSF.TSF_REC,
                       I_message         IN              "RIB_XTsfDesc_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XTSF_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;

   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if not VALIDATE_MESSAGE(O_error_message,
                           I_message,
                           I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_tsf_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                       O_tsf_rec         OUT    NOCOPY   RMSSUB_XTSF.TSF_REC,
                       I_message         IN              "RIB_XTsfRef_REC",
                       I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XTSF_VALIDATE.CHECK_MESSAGE';

BEGIN

   if not CHECK_REQUIRED_FIELDS(O_error_message,
                                I_message,
                                I_message_type) then
      return FALSE;
   end if;

   if not SYSTEM_OPTIONS_SQL.POPULATE_SYSTEM_OPTIONS(O_error_message) then
      return FALSE;
   end if;

   if not VALIDATE_MESSAGE(O_error_message,
                           I_message,
                           I_message_type) then
      return FALSE;
   end if;

   if not POPULATE_RECORD(O_error_message,
                          O_tsf_rec,
                          I_message,
                          I_message_type) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_MESSAGE;

---------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XTsfDesc_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50)  := 'RMSSUB_XTSF_VALIDATE.CHECK_REQUIRED_FIELDS';
   L_item_uom_class   UOM_CLASS.UOM_CLASS%TYPE;

   cursor C_GET_UOM_CLASS (cv_item ITEM_MASTER.ITEM%TYPE) is
      select uc.uom_class
        from uom_class uc, item_master im
       where im.item         = cv_item
         and im.standard_uom = uc.uom;

BEGIN

   if I_message.tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'tsf_no');
      return FALSE;
   end if;

   if I_message.from_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'from_loc');
      return FALSE;
   end if;

   if I_message.from_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'from_loc_type');
      return FALSE;
   end if;

   if I_message.from_loc_type not in ('S', 'W') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'from_loc_type',
                                            I_message.from_loc_type,
                                            'S or W');
      return FALSE;
   end if;

   if I_message.to_loc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'to_loc');
      return FALSE;
   end if;

   if I_message.to_loc_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'to_loc_type');
      return FALSE;
   end if;

   if I_message.to_loc_type not in ('S', 'W', 'E') then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'to_loc_type',
                                            I_message.to_loc_type,
                                            'S or W');
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XTSF.LP_mod_type then --head mod only

      if I_message.routing_code is NULL and
         I_message.delivery_date is NULL then
         ---
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'routing_code or delivery_date');
         return FALSE;
      end if;

      if I_message.XTsfDtl_TBL is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DTLS_HDR_MOD_DEL', null, null, null);
         return FALSE;
      end if;

   end if;

   --Delivery date is required for non-RAC/EG/SIM transfers when AIP_IND is Y
   if SYSTEM_OPTIONS_SQL.GP_system_options_row.aip_ind = 'Y' and
      I_message_type in (RMSSUB_XTSF.LP_cre_type, RMSSUB_XTSF.LP_mod_type) then
      if I_message.tsf_type not in ('SIM', 'EG', 'RAC') and I_message.delivery_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'delivery_date');
         return FALSE;
      end if;
   end if;

   if (I_message_type in (RMSSUB_XTSF.LP_dtl_mod_type, RMSSUB_XTSF.LP_dtl_cre_type) or
       (I_message_type = RMSSUB_XTSF.LP_cre_type and
        (I_message.status is NULL or
         I_message.status = 'A'))) and
      (I_message.XTsfDtl_TBL is NULL or
       I_message.XTsfDtl_TBL.COUNT < 1) then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('DTLS_MUST_EXIST', null, null, null);
      return FALSE;
   end if;

   if I_message.XTsfDtl_TBL is NOT NULL then
      for i in I_message.XTsfDtl_TBL.first..I_message.XTsfDtl_TBL.last loop

         if I_message.XTsfDtl_TBL(i).item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item', null, null);
            return FALSE;
         end if;

         if I_message.XTsfDtl_TBL(i).tsf_qty is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'tsf_qty', null, null);
            return FALSE;
         end if;

         if (I_message.XTsfDtl_TBL(i).tsf_qty <= 0 and
             I_message_type != RMSSUB_XTSF.LP_dtl_mod_type) or
            (I_message.XTsfDtl_TBL(i).tsf_qty < 0 and
             I_message_type = RMSSUB_XTSF.LP_dtl_mod_type) then
            O_error_message := SQL_LIB.CREATE_MSG('QTY_GREATER_ZERO', null, null, null);
            return FALSE;
         end if;

         open C_GET_UOM_CLASS (I_message.XTsfDtl_TBL(i).item);
         fetch C_GET_UOM_CLASS into L_item_uom_class;

         if L_item_uom_class = 'QTY' then
            if I_message.XTsfDtl_TBL(i).tsf_qty != ROUND(I_message.XTsfDtl_TBL(i).tsf_qty,0) then
               O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_INTEGER', NULL, NULL, NULL);
               close C_GET_UOM_CLASS;
               return FALSE;
            end if;
         end if;

         close C_GET_UOM_CLASS;

if (I_message.XTsfDtl_TBL(i).adjustment_type is NULL and
            I_message.XTsfDtl_TBL(i).adjustment_value is NOT NULL) then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'adjustment_type', null, null);
            return FALSE;
         end if;

         if (I_message.XTsfDtl_TBL(i).adjustment_value is NULL and
            I_message.XTsfDtl_TBL(i).adjustment_type is NOT NULL) then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'adjustment_value', null, null);
            return FALSE;
         end if;

         if I_message.XTsfDtl_TBL(i).adjustment_type NOT IN ('DA', 'DP', 'IA', 'IP', 'S') then
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PRC_ADJ_TYPE', null, null, null);
            return FALSE;
         end if;

         if I_message.XTsfDtl_TBL(i).adjustment_type IN ('IA', 'IP') and SYSTEM_OPTIONS_SQL.GP_system_options_row.tsf_price_exceed_wac_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_PRC_ADJ_TYPE_WAC_IND', null, null, null);
            return FALSE;
         end if;

         if I_message.XTsfDtl_TBL(i).adjustment_value < 0 then
            O_error_message := SQL_LIB.CREATE_MSG('POS_VALUE', 'Adjustment Value', null, null);
            return FALSE;
         end if;
      end loop;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_REQUIRED_FIELDS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_message         IN       "RIB_XTsfRef_REC",
                               I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'RMSSUB_XTSF_VALIDATE.CHECK_REQUIRED_FIELDS';

BEGIN

   if I_message.tsf_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'tsf_no');
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XTSF.LP_del_type and
      I_message.XTsfDtlRef_TBL is NOT NULL then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('NO_DTLS_HEADER_MOD_DEL', NULL, NULL, NULL);
      return FALSE;
   end if;

   if I_message_type = RMSSUB_XTSF.LP_dtl_del_type and
      (I_message.XTsfDtlRef_TBL is NULL or
       I_message.XTSFDtlRef_TBL.count < 1) then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('DTLS_MUST_EXIST', NULL, NULL, NULL);
      return FALSE;
   end if;

   if I_message.XTsfDtlRef_TBL is NOT NULL then

      for i in I_message.XTsfDtlRef_TBL.first..I_message.XTsfDtlRef_TBL.last loop
         ---
         if I_message.XTsfDtlRef_TBL(i).item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQ_FIELD_NULL', 'item');
            return FALSE;
         end if;
         ---
      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_REQUIRED_FIELDS;
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_XTsfDesc_REC",
                          I_message_type    IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50)     := 'RMSSUB_XTSF_VALIDATE.VALIDATE_MESSAGE';

   L_item                 ITEM_MASTER.ITEM%TYPE;
   L_receive_as_type      ITEM_LOC.RECEIVE_AS_TYPE%TYPE;
   L_pack_ind             ITEM_MASTER.PACK_IND%TYPE;
   L_inventory_ind        ITEM_MASTER.INVENTORY_IND%TYPE;
   L_status               ITEM_MASTER.STATUS%TYPE;
   L_tran_level           ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_item_level           ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_dept                 ITEM_MASTER.DEPT%TYPE;
   L_exists               BOOLEAN := FALSE;
   L_avail_qty            ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;
   L_current_tsf_qty      TSFDETAIL.TSF_QTY%TYPE;
   L_qty_diff             TSFDETAIL.TSF_QTY%TYPE;

   L_dummy                VARCHAR2(1);
   L_flag                 BOOLEAN  DEFAULT FALSE;
   L_dummy_qty            ITEM_LOC_SOH.STOCK_ON_HAND%TYPE;

   L_sku_exist            BOOLEAN := FALSE;

   L_inv_status           TSFDETAIL.INV_STATUS%TYPE;
   L_greatest_qty         TSFDETAIL.SHIP_QTY%TYPE;
   L_tsf_inventory_type   TSFHEAD.INVENTORY_TYPE%TYPE;

   L_store                     STORE.STORE%TYPE;
   L_from_store                STORE.STORE%TYPE;
   L_from_store_type           STORE.STORE_TYPE%TYPE := 'X';
   L_from_stockholding_ind     STORE.STOCKHOLDING_IND%TYPE;
   L_from_finisher_ind         WH.FINISHER_IND%TYPE;
   L_from_pwh                  WH.WH%TYPE;

   L_to_store                  STORE.STORE%TYPE;
   L_to_store_type             STORE.STORE_TYPE%TYPE := 'X';
   L_to_stockholding_ind       STORE.STOCKHOLDING_IND%TYPE;
   L_to_finisher_ind           WH.FINISHER_IND%TYPE;
   L_to_pwh                    WH.WH%TYPE;
   L_wh                        WH.WH%TYPE;
   L_intercompany              BOOLEAN := FALSE;

   L_pack_exists               VARCHAR2(1) := 'N';
   L_tsf_status                TSFHEAD.STATUS%TYPE;
   L_open_close_ind            VARCHAR2(1) := null;
   L_store_in_zone             BOOLEAN;
   L_from_store_zone           STORE.TRANSFER_ZONE%TYPE;
   L_to_store_zone             STORE.TRANSFER_ZONE%TYPE;
   L_il_exists                 BOOLEAN := FALSE;
   L_il_status                 ITEM_LOC.STATUS%TYPE;

   TYPE item_loc_rec is RECORD (receive_as_type item_loc.receive_as_type%TYPE,
                                pack_ind        item_master.pack_ind%TYPE,
                                inventory_ind   item_master.inventory_ind%TYPE,
                                status          item_master.status%TYPE,
                                tran_level      item_master.tran_level%TYPE,
                                item_level      item_master.item_level%TYPE,
                                dept            item_master.dept%TYPE,
                                tsf_qty         tsfdetail.tsf_qty%TYPE);

   TYPE item_loc_tbl is TABLE OF item_loc_rec index by BINARY_INTEGER;
   L_item_loc_tbl item_loc_tbl;

   cursor C_CHECK_TSF_DETAIL is
      select greatest(NVL(ABS(distro_qty), 0), NVL(selected_qty, 0), NVL(ship_qty, 0))
        from tsfdetail
       where tsf_no     = I_message.tsf_no
         and item       = L_item
         and (inv_status = L_inv_status
              or
             (inv_status is NULL
              and L_inv_status is NULL));

   cursor C_GET_ITEM_INFO_WH is
      select nvl(il.receive_as_type, 'P'),
             im.pack_ind,
             im.inventory_ind,
             im.status,
             im.tran_level,
             im.item_level,
             im.dept,
             nvl(td.tsf_qty, 0)
        from item_loc il,
             item_master im,
             tsfdetail td
       where im.item = L_item
         and im.item = il.item
         and (il.loc = I_message.from_loc
             or (il.loc <> I_message.from_loc
             and exists (select 'x'
                           from wh w
                          where w.wh = il.loc
                            and w.physical_wh = I_message.from_loc)))
         and il.loc_type = I_message.from_loc_type
         and td.item(+) = il.item
         and td.inv_status(+) = L_inv_status
         and td.tsf_no(+) = I_message.tsf_no;

   cursor C_GET_ITEM_INFO_ST is
      select 'P',  -- don't care about receive_as_type for stores
             im.pack_ind,
             im.inventory_ind,
             im.status,
             im.tran_level,
             im.item_level,
             im.dept,
             nvl(td.tsf_qty, 0)
        from item_master im,
             tsfdetail td
       where im.item = L_item
         and td.item(+) = im.item
         and td.inv_status(+) = L_inv_status
         and td.tsf_no(+) = I_message.tsf_no;

   cursor C_IL_STATUS is
      select status
        from item_loc
       where item = L_item
         and loc = I_message.from_loc;

   cursor C_CHECK_FREIGHT_CODE is
      select 'x'
        from tsfhead
       where freight_code = 'E'
         and tsf_no = I_message.tsf_no;

   cursor C_GET_INVENTORY_TYPE is
      select inventory_type
        from tsfhead
       where tsf_no = I_message.tsf_no;

   cursor C_GET_STORE_TYPE is
      select store_type,
             stockholding_ind
        from store
       where store = L_store;

   cursor C_GET_FINISHER_IND is
      select finisher_ind
        from wh
       where wh = L_wh;

   cursor C_GET_STATUS is
      select status
        from tsfhead
       where tsf_no = I_message.tsf_no;

   cursor C_GET_PACK_IND is
      select 'Y'
        from TABLE(CAST(I_message.XTsfDtl_TBL as "RIB_XTsfDtl_TBL")) x
       where exists (select 'x'
                       from item_master im
                      where im.item = x.item
                        and im.pack_ind = 'Y'
                        and rownum = 1)
         and rownum = 1;

BEGIN

   if not VALIDATE_LOCATION(O_error_message,
                            L_from_pwh,
                            I_message.from_loc,
                            I_message.from_loc_type,
                            I_message.tsf_type) then
      return FALSE;
   end if;

   if not VALIDATE_LOCATION(O_error_message,
                            L_to_pwh,
                            I_message.to_loc,
                            I_message.to_loc_type,
                            I_message.tsf_type) then
      return FALSE;
   end if;

   -- Validate that both Stores belong to the same transfer zone.
   if I_message.to_loc_type = 'S' and I_message.from_loc_type = 'S' then
      if STORE_ATTRIB_SQL.GET_TSF_ZONE(O_error_message,
                                       L_from_store_zone,
                                       L_store_in_zone,
                                       I_message.from_loc) = FALSE then
         return false;
      end if;

      if STORE_ATTRIB_SQL.GET_TSF_ZONE(O_error_message,
                                       L_to_store_zone,
                                       L_store_in_zone,
                                       I_message.to_loc) = FALSE then
         return false;
      end if;

      if L_from_store_zone != L_to_store_zone then
         O_error_message := SQL_LIB.CREATE_MSG('TF_STR_TRAN_ZONE',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;

   end if;

   -- Ensure that the to_loc Store is not Closed and still active.
   if I_message.status != 'I' and  I_message.to_loc_type = 'S' then
      if STORE_ATTRIB_SQL.ACTIVE(I_message.to_loc,
                                 GET_VDATE,
                                 L_open_close_ind,
                                 O_error_message) = FALSE then
         return FALSE;
      end if;

      if  L_open_close_ind = 'C' then
         return FALSE;
      end if;
   end if;

   if I_message.tsf_type is NOT NULL then
      --validate that Book Transfer must be between warehouses in the same physical wh
      if I_message.tsf_type = 'BT' then

         if I_message.from_loc_type != 'W' or
            I_message.to_loc_type != 'W' then
            O_error_message := SQL_LIB.CREATE_MSG('BOOK_TSF_BTN_WH',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;

         if L_from_pwh != L_to_pwh then
            O_error_message := SQL_LIB.CREATE_MSG('SAME_PWH_FOR_BT',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      else
         --validate that non-Book Transfers cannot be between locations in the same physical wh.
         if I_message.from_loc_type = 'W' and I_message.to_loc_type = 'W' and L_from_pwh = L_to_pwh then
            O_error_message := SQL_LIB.CREATE_MSG('SAME_PWH_TSF_NO_ALLOW',
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
         end if;
      end if;

      -- This check will only be necessary for the following transfer types.
      -- Ensure that these transfer types have locations that belong to the same entity.
      if I_message.tsf_type in ('AD', 'CF', 'MR','RAC','RV', 'BT') then
         if TRANSFER_SQL.IS_INTERCOMPANY(O_error_message,
                                         L_intercompany ,
                                         'T',                -- Distro Type
                                         I_message.tsf_type, --I_tsf_type
                                         I_message.from_loc,
                                         I_message.from_loc_type,
                                         I_message.to_loc,
                                         I_message.to_loc_type) = FALSE then
            return FALSE;
         end if;
         --
         if L_intercompany then
            O_error_message := SQL_LIB.CREATE_MSG('MUST_BE_INTRACOMPANY_TSF',
                                                   I_message.tsf_type,
                                                   NULL,
                                                   NULL);
            return FALSE;
         end if;
      end if;
   end if;

   -- Check if To and From location are identical
   if I_message.from_loc_type = I_message.to_loc_type and
      I_message.from_loc = I_message.to_loc then
      if I_message.from_loc_type = 'S' then
         O_error_message := SQL_LIB.CREATE_MSG('TF_STR_NOT_SAME',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      elsif I_message.from_loc_type = 'W' then
         O_error_message := SQL_LIB.CREATE_MSG('TF_WH_NOT_SAME',
                                               NULL,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
   end if;

   if I_message.from_loc_type = 'S' then
      L_store := I_message.from_loc;

      open C_GET_STORE_TYPE;
      fetch C_GET_STORE_TYPE into L_from_store_type, L_from_stockholding_ind;
      close C_GET_STORE_TYPE;
   end if;

   open C_GET_PACK_IND;
   fetch C_GET_PACK_IND into L_pack_exists;
   close C_GET_PACK_IND;

   if I_message.to_loc_type = 'S' then
      L_store := I_message.to_loc;

      open C_GET_STORE_TYPE;
      fetch C_GET_STORE_TYPE into L_to_store_type, L_to_stockholding_ind;
      close C_GET_STORE_TYPE;
   end if;

   if I_message.from_loc_type = 'W' then
      L_wh := I_message.from_loc;

      open C_GET_FINISHER_IND;
      fetch C_GET_FINISHER_IND into L_from_finisher_ind;
      close C_GET_FINISHER_IND;
   end if;

   if I_message.to_loc_type = 'W' then
      L_wh := I_message.to_loc;

      open C_GET_FINISHER_IND;
      fetch C_GET_FINISHER_IND into L_to_finisher_ind;
      close C_GET_FINISHER_IND;
   end if;

   -- In case from/to loc is a F store, the other location should not be a Internal/External Finisher.
   if (L_from_store_type = 'F' and I_message.to_loc_type = 'W' and L_to_finisher_ind = 'Y')
      or (L_to_store_type = 'F' and I_message.from_loc_type = 'W' and L_from_finisher_ind = 'Y')
      or (L_from_store_type = 'F' and I_message.to_loc_type not in ('W','S'))
      or (L_to_store_type = 'F' and I_message.from_loc_type not in ('W','S')) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_FST_FINWH', null, null, null);
      return FALSE;
   end if;

   -- AIP transfers can be between stockholding stores only.
   if (NVL(I_message.tsf_type,'XX')='AIP') and
       ((I_message.to_loc_type = 'S' and L_to_stockholding_ind = 'N') or (I_message.from_loc_type = 'S' and L_from_stockholding_ind = 'N')) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_AIP_NONSTK', null, null, null);
      return FALSE;
   end if;

   -- In case from/to loc is a non-stockholding F store, the other location should be a Warehouse only.
   if ((L_from_store_type = 'F' and L_from_stockholding_ind = 'N' and not (I_message.to_loc_type = 'W'))
      or (L_to_store_type = 'F' and L_to_stockholding_ind = 'N' and not (I_message.from_loc_type = 'W' ))) then
      O_error_message := SQL_LIB.CREATE_MSG('INV_NONSTKF_WH_NONSTKF', null, null, null);
      return FALSE;
   end if;

   -- Pack items are not allowed on Store sourced transactions except for transfer types AIP,SIM and EG
   if L_pack_exists = 'Y' and I_message.from_loc_type = 'S' and NVL(I_message.tsf_type,'XX') not in ('AIP', 'SIM', 'EG') then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PACK_ITM_TSF_TYPE', null, null, null);
      return FALSE;
   end if;

   if not CHECK_TSF_EXISTS(O_error_message,
                           I_message.tsf_no,
                           I_message_type) then
      return FALSE;
   end if;

   if I_message.status is NOT NULL and
      I_message.status not in ('I', 'A') then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'status',
                                            I_message.status,
                                            'I or A');
      return FALSE;
   end if;

   -- Transfers generated from 'AIP','SIM','EG' cannot be in 'I' status.
   if I_message.status = 'I' and I_message.tsf_type in ('AIP', 'SIM', 'EG') then
      O_error_message := SQL_LIB.CREATE_MSG('TSF_STA_NO_I', null, null,null);
      return FALSE;
   end if;

   -- The status of transfer cannot be changed from 'A'pproved to 'I'nput.
   if I_message_type = RMSSUB_XTSF.LP_mod_type then
      open  C_GET_STATUS;
      fetch C_GET_STATUS into L_tsf_status;
      close C_GET_STATUS;

      if L_tsf_status = 'A' and I_message.status = 'I' then
         O_error_message := SQL_LIB.CREATE_MSG('TSF_STA_NO_A_TO_I',null,null,null);
         return FALSE;
      end if;
   end if;


   if I_message_type in (RMSSUB_XTSF.LP_cre_type, RMSSUB_XTSF.LP_mod_type, RMSSUB_XTSF.LP_dtl_cre_type, RMSSUB_XTSF.LP_dtl_mod_type) then

      if I_message.delivery_date is NOT NULL and
         I_message.delivery_date < GET_VDATE then
         ---
         O_error_message := SQL_LIB.CREATE_MSG('DELIVER_DATE_AFTER', GET_VDATE, null, null);
         return FALSE;
      end if;

      if I_message_type = RMSSUB_XTSF.LP_cre_type then

         if I_message.dept is NOT NULL then

            if SYSTEM_OPTIONS_SQL.GP_system_options_row.dept_level_transfers = 'N' and
               I_message.tsf_type not in ('SIM', 'AIP', 'EG') then
               ---
               O_error_message := SQL_LIB.CREATE_MSG('DEPT_MUST_BE_NULL', NULL, NULL, NULL);
               return FALSE;
            end if;

            if not DEPT_VALIDATE_SQL.EXIST(O_error_message,
                                           I_message.dept,
                                           L_exists) then
               return FALSE;
            end if;

            if L_exists = FALSE then
               O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT', NULL, NULL, NULL);
               return FALSE;
            end if;

         end if;

         if I_message.freight_code is NOT NULL then

            if I_message.freight_code not in ('N', 'E', 'H') then
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                     'freight_code',
                                                     I_message.freight_code,
                                                     'N, E, or H');
               return FALSE;
            end if;

            if I_message.freight_code != 'E' and
               I_message.routing_code is NOT NULL then
               ---
               O_error_message := SQL_LIB.CREATE_MSG('ROUTING_CODE_NULL', null, null, null);
               return FALSE;
            end if;

         end if;

      elsif I_message_type = RMSSUB_XTSF.LP_mod_type then

         if I_message.routing_code is NOT NULL then
            open C_CHECK_FREIGHT_CODE;
            fetch C_CHECK_FREIGHT_CODE into L_dummy;

            if C_CHECK_FREIGHT_CODE%NOTFOUND then
               close C_CHECK_FREIGHT_CODE;
               O_error_message := SQL_LIB.CREATE_MSG('ROUTING_CODE_NULL', null, null, null);
               return FALSE;
            end if;

            close C_CHECK_FREIGHT_CODE;

         end if;
      end if;

      -- No need to check these for RMSSUB_XTSF.LP_dtl_mod_type types
      if I_message_type <> RMSSUB_XTSF.LP_dtl_mod_type then
         if I_message.routing_code is NOT NULL and
            I_message.routing_code not in ('1', '2', '3') then
            ---
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                  'routing_code',
                                                  I_message.routing_code,
                                                  '1, 2 or 3');
            return FALSE;
         end if;
      end if;

      --In addition to 'AIP', 'SIM', 'EG' transfers that can be created in RMS through XTsf,
      --XTsf API is used by RMS dashboard to create transfers. Valid transfer types that can be
      --created through dashboard are: 'AD', 'CF', 'RV', 'RAC', 'BT', 'MR' and 'IC'.
      --Among these, 'IC' transfers must be intercompany; all other transfers must be intra-company.
      --'BT', 'RV', 'RAC' transfers cannot be created in or updated to Approved status because
      --approval of these transfer types require special handling which is not currently covered in XTsf.
      --XTsf API only supports single-leg.
      if I_message_type NOT in (RMSSUB_XTSF.LP_dtl_mod_type,RMSSUB_XTSF.LP_dtl_cre_type) then
         if I_message.tsf_type is NOT NULL and
            I_message.tsf_type not in ('AIP', 'SIM', 'EG', 'AD', 'CF', 'MR', 'RV', 'RAC', 'IC', 'BT') then
            ---
            O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                                  'tsf_type',
                                                  I_message.tsf_type,
                                                  'AIP or SIM or EG or AD or CF or MR or RV or RAC or IC or BT');
            return FALSE;
         end if;

         if I_message.tsf_type in ('BT', 'RV', 'RAC') and I_message.status = 'A' then
            O_error_message := SQL_LIB.CREATE_MSG('INV_TSF_TYPE_STATUS', null, null,null);
            return FALSE;
         end if;
      end if;

      if I_message.XTsfDtl_TBL is NOT NULL then
         open C_GET_INVENTORY_TYPE;
         fetch C_GET_INVENTORY_TYPE into L_tsf_inventory_type;
         close C_GET_INVENTORY_TYPE;

         --- Check if all the items are of the same type.
         L_inv_status := I_message.XTsfDtl_TBL(1).inv_status;

         FOR i in I_message.XTsfDtl_TBL.first..I_message.XTsfDtl_TBL.last LOOP
            if (NVL(L_inv_status, -1) = -1) and (NVL(I_message.XTsfDtl_TBL(i).inv_status,-1) = -1) then
               L_inv_status := I_message.XTsfDtl_TBL(i).inv_status;
            elsif (NVL(L_inv_status,-1) != -1) and (NVL(I_message.XTsfDtl_TBL(i).inv_status,-1) != -1) then
               L_inv_status := I_message.XTsfDtl_TBL(i).inv_status;
            else
               O_error_message := SQL_LIB.CREATE_MSG('INVALID_STATUS',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               return FALSE;
            end if;

           if L_inv_status is NOT NULL then
              if GET_INV_STATUS_TYPES(O_error_message,
                                      L_inv_status) = FALSE then
                 return FALSE;
              end if;
           end if;

         END LOOP;

         if (L_tsf_inventory_type = 'A' and L_inv_status is NOT NULL) or (L_tsf_inventory_type = 'U' and L_inv_status is NULL) then
             O_error_message := SQL_LIB.CREATE_MSG('INV_TYPE_NOT_MATCH',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return FALSE;
         end if;

         --- check detail level values
         for i in I_message.XTsfDtl_TBL.first..I_message.XTsfDtl_TBL.last loop
            ---
            L_item := I_message.XTsfDtl_TBL(i).item;
            L_inv_status := I_message.XTsfDtl_TBL(i).inv_status;
            if I_message_type = RMSSUB_XTSF.LP_dtl_mod_type then
               -- loop through the details that came in the mod message to make
               -- sure that all items exist for this tsf already
               ---
               open C_CHECK_TSF_DETAIL;
               fetch C_CHECK_TSF_DETAIL into L_greatest_qty;

               if C_CHECK_TSF_DETAIL%NOTFOUND then
                  O_error_message := SQL_LIB.CREATE_MSG('ITEM_NOT_ON_TSF',
                                                        L_item,
                                                        L_inv_status,
                                                        I_message.tsf_no);
                  close C_CHECK_TSF_DETAIL;
                  return FALSE;
               end if;

               close C_CHECK_TSF_DETAIL;

               if I_message.XTsfDtl_TBL(i).tsf_qty < L_greatest_qty then
                  O_error_message := SQL_LIB.CREATE_MSG('QTY_LESS_THAN_PROCESSED', L_greatest_qty, L_item, NULL);
                  return FALSE;
               end if;
            end if;

            if I_message_type = RMSSUB_XTSF.LP_dtl_cre_type then
               -- Check if item already exists in the tsfdetail table
               if TRANSFER_SQL.TSF_ITEM_EXIST(O_error_message,
                                              L_sku_exist,
                                              I_message.tsf_no,
                                              L_item,
                                              L_inv_status) = FALSE then
                  return FALSE;
               end if;

               if L_sku_exist = TRUE then
                  O_error_message := SQL_LIB.CREATE_MSG('SKU_EXIST_TRAN',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;

            end if;

            -- Item and To Location validation
            -- If To Location and Item exists in item_loc table, verify that the Item Location status is valid.
            if ITEMLOC_ATTRIB_SQL.ITEM_LOC_EXIST(O_error_message,
                                                 L_item,
                                                 I_message.to_loc,
                                                 L_il_exists) = FALSE then
               return FALSE;
            end if;

            if L_il_exists = TRUE then
               if ITEMLOC_ATTRIB_SQL.ITEM_STATUS(O_error_message,
                                                 L_item,
                                                 I_message.to_loc,
                                                 L_il_status) = FALSE then
                  return FALSE;
               end if;
               ---
               if L_il_status in ('D','I') then
                  O_error_message := SQL_LIB.CREATE_MSG('ITEM_STATUS_TSF',
                                                        NULL,
                                                        NULL,
                                                        NULL);
                  return FALSE;
               end if;
            end if;

            -- For RWMS, item/from_loc must be ranged when transfers are created.
            -- However, SIM wants to allow creating transfers for an unranged from location
            -- to accommodate possible business scenarioes. Transfers created in SIM will only
            -- have from_loc of store not warehouse.

            -- Allowing transfers with non-ranged item/from_loc means we have to remove available
            -- inventory check. However, this will automatically result in negative stock qty on
            -- ITEM_LOC_SOH in RMS. Based on the discussions between SIM, RWMS and RMS teams,
            -- negative and/or imbalance stock qty between SIM and RMS is not ideal but acceptable.
            -- As a result, RMS will always have imbalanced qty with SIM until SIM
            -- performs a cycle count or user manually do an inventory adjustment in RMS.

            if I_message.from_loc_type = 'W' then
               -- For a transfer created from WMS, the item/from_loc must be ranged in RMS already.
               open C_GET_ITEM_INFO_WH;
               fetch C_GET_ITEM_INFO_WH BULK COLLECT INTO L_item_loc_tbl;

               if L_item_loc_tbl.COUNT = 0 then
                   O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_LOC', L_item, I_message.from_loc, null);
                   close C_GET_ITEM_INFO_WH;
                  return FALSE;
               end if;
               close C_GET_ITEM_INFO_WH;
            else  -- from_loc_type = 'S'
               open C_GET_ITEM_INFO_ST;
               fetch C_GET_ITEM_INFO_ST BULK COLLECT INTO L_item_loc_tbl;
               if L_item_loc_tbl.COUNT = 0 then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_ITEM', L_item, null, null);
                  close C_GET_ITEM_INFO_ST;
                  return FALSE;
               end if;
               close C_GET_ITEM_INFO_ST;
            end if;

            --if item/from-loc is ranged, it cannot be in 'D'eleted status
            L_il_status := '';
            open C_IL_STATUS;
            fetch C_IL_STATUS into L_il_status;
            close C_IL_STATUS;        

            if L_il_status = 'D' then
               O_error_message := SQL_LIB.CREATE_MSG('ITEM_LOC_DELETE', 
                                                     L_item, 
                                                     to_char(I_message.from_loc),
                                                     NULL);
               return FALSE;
            end if;

            for j in L_item_loc_tbl.FIRST..L_item_loc_tbl.LAST loop

               L_receive_as_type := L_item_loc_tbl(j).receive_as_type;
               L_pack_ind := L_item_loc_tbl(j).pack_ind;
               L_inventory_ind := L_item_loc_tbl(j).inventory_ind;
               L_status := L_item_loc_tbl(j).status;
               L_tran_level := L_item_loc_tbl(j).tran_level;
               L_item_level := L_item_loc_tbl(j).item_level;
               L_dept := L_item_loc_tbl(j).dept;
               L_current_tsf_qty := L_item_loc_tbl(j).tsf_qty;

               if L_inventory_ind = 'N' then
                  O_error_message := SQL_LIB.CREATE_MSG('CANNOT_SELECT_NON_INV', L_item, null, null);
                  return FALSE;
               end if;

               if L_status != 'A' then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_NOT_APPROVED', L_item, null, null);
                  return FALSE;
               end if;

               if L_tran_level != L_item_level then
                  O_error_message := SQL_LIB.CREATE_MSG('NOT_SKU', L_item, null, null);
                  return FALSE;
               end if;

               if L_pack_ind = 'Y' and
                  L_receive_as_type != 'P' and
                  I_message.from_loc_type = 'W' then
                  ---
                  O_error_message := SQL_LIB.CREATE_MSG('INV_RECEIVE_AS_TYPE',null,null,null);
                  return FALSE;
               end if;

               if I_message.dept is NOT NULL and
                  I_message.dept != L_dept then
                  ---
                  O_error_message := SQL_LIB.CREATE_MSG('ITEM_NOT_IN_DEPT', L_item, I_message.dept, null);
                  return FALSE;
               end if;

               if I_message.XTsfDtl_TBL(i).supp_pack_size is NOT NULL and
                  I_message.XTsfDtl_TBL(i).supp_pack_size <= 0 then
                  ---
                  O_error_message := SQL_LIB.CREATE_MSG('NO_NEG_SUPP_PACK', null, null, null);
                  return FALSE;
               end if;
               if I_message.from_loc_type = 'W' then
                  if WH_ATTRIB_SQL.CHECK_PWH(O_error_message,
                                             L_flag,
                                             I_message.from_loc) = FALSE then
                     return FALSE;
                  end if;
               end if;

               if I_message.from_loc_type = 'W' and L_flag = TRUE and I_message.tsf_type = 'EG' and NVL(L_inv_status,-1) = -1 then

                  if ITEMLOC_QUANTITY_SQL.GET_ITEM_GROUP_QTYS(O_error_message,
                                                              L_avail_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_dummy_qty,
                                                              L_item,
                                                              I_message.from_loc,
                                                              'PW') = FALSE then
                     return FALSE;
                  end if;
               elsif I_message.from_loc_type != 'S' and NVL(L_inv_status,-1) = -1 then

                  if ITEMLOC_QUANTITY_SQL.GET_LOC_CURRENT_AVAIL(O_error_message,
                                                                L_avail_qty,
                                                                L_item,
                                                                I_message.from_loc,
                                                                I_message.from_loc_type) = FALSE then
                     return FALSE;
                  end if;
               elsif I_message.from_loc_type != 'S' and NVL(L_inv_status,-1) != -1 then
                  if TRANSFER_SQL.QUANTITY_AVAIL(O_error_message,
                                                 L_avail_qty,
                                                 L_item,
                                                 L_inv_status,
                                                 I_message.from_loc_type,
                                                 I_message.from_loc) = FALSE then
                     return FALSE;
                  end if;
               end if;
               ---
               -- If we have a create or a detail create message, we need to check
               -- the entire requested qty against the available qty.  A detail mod
               -- message when the tsf has not yet been approved should also be
               -- validated in the same fashion.
               ---
               if I_message.tsf_type <> 'EG' then
                  if (I_message_type in (RMSSUB_XTSF.LP_cre_type, RMSSUB_XTSF.LP_dtl_cre_type) or
                     (I_message_type = RMSSUB_XTSF.LP_dtl_mod_type and
                      I_message.status != 'A')) and I_message.from_loc_type != 'S' then
                     ---
                      if L_avail_qty < I_message.XTsfDtl_TBL(i).tsf_qty then
                         O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_ST_TSF',
                                                               L_item,
                                                               I_message.from_loc,
                                                               L_avail_qty);
                         return FALSE;
                      end if;
                     ---
                  -- If the message is for a detail modification and the tsf has already
                  -- been approved, we need to only validate that there is enough available
                  -- qty for the extra qty being added (if the difference between the new
                  -- requested qty and the original qty is greater than zero).
                  ---
                  elsif I_message_type = RMSSUB_XTSF.LP_dtl_mod_type and
                        I_message.status = 'A' and I_message.from_loc_type != 'S' then
                     ---
                     L_qty_diff := I_message.XTsfDtl_TBL(i).tsf_qty - L_current_tsf_qty;

                     if L_avail_qty < L_qty_diff and
                        L_qty_diff > 0 then
                        ---
                        O_error_message := SQL_LIB.CREATE_MSG('QTY_AVAIL_ST_TSF',
                                                              I_message.XTsfDtl_TBL(i).item,
                                                              I_message.from_loc,
                                                              L_avail_qty);
                        return FALSE;
                     end if;
                  end if;
               end if;
            end loop;
         end loop;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_MESSAGE;
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_physical_wh         OUT  WH.WH%TYPE,
                           I_loc              IN      WH.WH%TYPE,
                           I_loc_type         IN      ITEM_LOC.LOC_TYPE%TYPE,
                           I_tsf_type         IN      TSFHEAD.TSF_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XTSF_VALIDATE.VALIDATE_LOCATION';
   L_dummy        VARCHAR2(1)  := NULL;

   cursor C_VERIFY_WH is
      select physical_wh
        from wh
       where wh = I_loc;

   cursor C_CHECK_VWH is
      select physical_wh
        from wh
       where wh = I_loc
         and wh != physical_wh;

   cursor C_VERIFY_STORE is
      select 'x'
        from store
       where store = I_loc;

   cursor C_CHECK_EXTERNAL_FINISHER is
      select 'x'
        from partner
       where partner_id = to_char(I_loc);

BEGIN

   if I_loc_type = 'W' then

      if I_tsf_type <> 'EG' or I_tsf_type is null then -- null handles 'MR','IC' then

         open C_CHECK_VWH;
         fetch C_CHECK_VWH into O_physical_wh;

         if C_CHECK_VWH%NOTFOUND then
            close C_CHECK_VWH;
            O_error_message := SQL_LIB.CREATE_MSG('INV_STOCKHOLDING_WH', I_loc, null, null);
            return FALSE;
         end if;

         close C_CHECK_VWH;

      else

         open C_VERIFY_WH;
         fetch C_VERIFY_WH into O_physical_wh;

         if C_VERIFY_WH%NOTFOUND then
            close C_VERIFY_WH;
            O_error_message := SQL_LIB.CREATE_MSG('INV_WH',I_loc,null,null);
            return FALSE;
         end if;

         close C_VERIFY_WH;

      end if;
   elsif I_loc_type = 'E' then

      open C_CHECK_EXTERNAL_FINISHER;
      fetch C_CHECK_EXTERNAL_FINISHER into L_dummy;

      if C_CHECK_EXTERNAL_FINISHER%NOTFOUND then
         close C_CHECK_EXTERNAL_FINISHER;
         O_error_message := SQL_LIB.CREATE_MSG('INV_EXT_FINISHER', I_loc, null, null);
         return FALSE;
      end if;

      close C_CHECK_EXTERNAL_FINISHER;

   else -- I_loc_type = 'S'

      open C_VERIFY_STORE;
      fetch C_VERIFY_STORE into L_dummy;

      if C_VERIFY_STORE%NOTFOUND then
         close C_VERIFY_STORE;
         O_error_message := SQL_LIB.CREATE_MSG('INV_STORE_STORE', I_loc, null, null);
         return FALSE;
      end if;

      close C_VERIFY_STORE;

   end if; -- end I_loc_type check

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_LOCATION;
---------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_MESSAGE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_message         IN       "RIB_XTsfRef_REC",
                          I_message_type    IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR2(50)               := 'RMSSUB_XTSF_VALIDATE.VALIDATE_MESSAGE';
   L_message_item  TSFDETAIL.ITEM%TYPE;
   L_tsf_item      TSFDETAIL.ITEM%TYPE;
   L_qty_sum       NUMBER;
   L_status        TSFHEAD.STATUS%TYPE;

   cursor C_CHECK_TSF_STATUS is
      select status
        from tsfhead
       where tsf_no = I_message.tsf_no;

   cursor C_CHECK_DETAIL is
      select item,
             NVL(ABS(distro_qty), 0) +
             NVL(selected_qty, 0) +
             NVL(received_qty, 0) +
             NVL(ship_qty, 0) qty_sum
        from tsfdetail
       where tsf_no = I_message.tsf_no
         and item = NVL(L_message_item, item);

BEGIN
   open C_CHECK_TSF_STATUS;
   fetch C_CHECK_TSF_STATUS into L_status;
   close C_CHECK_TSF_STATUS;

   if L_status in ('D', 'C') then
      O_error_message := SQL_LIB.CREATE_MSG('TSF_DEL_CLOSED',
                                            I_message.tsf_no,
                                            NULL,
                                            NULL);
         return FALSE;
   end if;

   if not CHECK_TSF_EXISTS(O_error_message,
                           I_message.tsf_no,
                           I_message_type) then
      return FALSE;
   end if;

   if I_message.XTsfDtlRef_TBL is NOT NULL then
      for i in I_message.XTsfDtlRef_TBL.first..I_message.XTsfDtlRef_TBL.last LOOP

         L_message_item := I_message.XTsfDtlRef_TBL(i).item;

         open C_CHECK_DETAIL;
         fetch C_CHECK_DETAIL into L_tsf_item,
                                   L_qty_sum;

         if C_CHECK_DETAIL%NOTFOUND then
            close C_CHECK_DETAIL;
            O_error_message := SQL_LIB.CREATE_MSG('ITEM_NOT_ON_TSF',
                                                  L_message_item,
                                                  NULL,
                                                  I_message.tsf_no);
            return FALSE;
         end if;

         close C_CHECK_DETAIL;

         if L_qty_sum != 0 then
            O_error_message := SQL_LIB.CREATE_MSG('TSF_DTL_IN_PROG_NO_DEL',
                                                  I_message.tsf_no,
                                                  L_message_item,
                                                  NULL);
            return FALSE;
         end if;

      END LOOP;

   else -- there are no details, thus this is a header delete

      L_message_item := NULL;

      for rec in C_CHECK_DETAIL loop

         if rec.qty_sum != 0 then
            O_error_message := SQL_LIB.CREATE_MSG('TSF_DTL_IN_PROG_NO_DEL',
                                                  I_message.tsf_no,
                                                  rec.item,
                                                  NULL);
            return FALSE;
         end if;

      end loop;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END VALIDATE_MESSAGE;
---------------------------------------------------------------------------------------------------------
FUNCTION CHECK_TSF_EXISTS(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_tsf_no            IN       TSFHEAD.TSF_NO%TYPE,
                          I_message_type      IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)               := 'RMSSUB_XTSF_VALIDATE.CHECK_TSF_EXISTS';
   L_exist         BOOLEAN                    := FALSE;
   L_dummy         VARCHAR2(1);
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE   := NULL;

BEGIN
   if not TSF_VALIDATE_SQL.EXIST(L_error_message,
                                 I_tsf_no,
                                 L_exist) then
      O_error_message := L_error_message;
      return FALSE;
   end if;

   if I_message_type != RMSSUB_XTSF.LP_cre_type then
      if NOT L_exist then
         O_error_message := SQL_LIB.CREATE_MSG('INV_TSF',NULL,NULL,NULL);
         return FALSE;
      end if;

   else -- this is a create message

      if L_exist then
         O_error_message := SQL_LIB.CREATE_MSG('TRAN_NUM_EXISTS',NULL,NULL,NULL);
         return FALSE;
      end if;
end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END CHECK_TSF_EXISTS;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT           RTK_ERRORS.RTK_TEXT%TYPE,
                         O_tsf_rec         IN OUT    NOCOPY RMSSUB_XTSF.TSF_REC,
                         I_message         IN               "RIB_XTsfDesc_REC",
                         I_message_type    IN               VARCHAR2)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'RMSSUB_XTSF_VALIDATE.POPULATE_RECORD';
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(Record_Locked, -54);
   L_item             ITEM_MASTER.ITEM%TYPE;
   L_index            NUMBER;
   L_pack_ind         ITEM_MASTER.PACK_IND%TYPE;
   L_pack_type        ITEM_MASTER.PACK_TYPE%TYPE;
   L_orderable_ind    ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_dummy            VARCHAR2(1);
   L_existing_qty     TSFDETAIL.TSF_QTY%TYPE;
   L_cancelled_qty    TSFDETAIL.CANCELLED_QTY%TYPE;
   L_tsf_seq_num      TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_tsf_seq_no       TSFDETAIL.TSF_SEQ_NO%TYPE;
   L_existing_status  TSFHEAD.STATUS%TYPE;
   L_change           TSFDETAIL.CANCELLED_QTY%TYPE;
   L_details_exist    VARCHAR2(1) := 'N';
   L_intercompany     BOOLEAN := FALSE;
   L_cost             TSFDETAIL.TSF_COST%TYPE;
   L_avg_cost         ITEM_LOC_SOH.AV_COST%TYPE := NULL;
   L_value            TSFDETAIL.TSF_PRICE%TYPE;
   L_tsf_price        TSFDETAIL.TSF_PRICE%TYPE;
   L_dummy_num        NUMBER;
   L_dummy_var        VARCHAR2(20);
   L_ship_exists      VARCHAR2(1);
   L_ship_flag        VARCHAR2(1)                := 'N';
   L_is_physical_wh   VARCHAR2(1) := 'N';
   L_item_supp_cost   ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := NULL;
   L_supp_currency    STORE.CURRENCY_CODE%TYPE := NULL;
   L_local_currency   STORE.CURRENCY_CODE%TYPE := NULL;
   L_prim_supplier    ITEM_SUPP_COUNTRY.SUPPLIER%TYPE;
   L_loc_exists       VARCHAR2(1) := 'N';

   cursor C_IS_PHYSICAL_WH(I_loc WH.WH%TYPE) is
      select 'Y'
        from wh
       where wh = I_loc
         and wh = physical_wh;

   cursor C_CHECK_ITEM_TO_LOC is
      select 'x'
        from item_loc_soh
       where item = L_item
         and loc = I_message.to_loc;

   cursor C_CHECK_ITEM_FROM_LOC is
      select 'x'
        from item_loc_soh
       where item = L_item
         and loc = I_message.from_loc;

   cursor C_GET_PACK_IND is
      select pack_ind,
             pack_type,
             orderable_ind
        from item_master
       where item = L_item;

   cursor C_GET_DTL_QTY is
      select greatest(tsf_qty,NVL(ship_qty,0)),
          cancelled_qty,
             tsf_seq_no
        from tsfdetail
       where tsf_no = I_message.tsf_no
         and item = L_item;

   cursor C_GET_MAX_SEQ_NO is
      select NVL(max(tsf_seq_no), 0)
        from tsfdetail
       where tsf_no = I_message.tsf_no;

   cursor C_GET_HEADER is
      select status
        from tsfhead
       where tsf_no = I_message.tsf_no;

   cursor C_CHECK_DETAILS is
      select 'Y'
        from tsfdetail
       where tsf_no = I_message.tsf_no
         and rownum = 1;

   cursor C_CHECK_SHIP_EXISTS is
         select 'Y'
           from shipsku
          where distro_no = I_message.tsf_no
            and rownum = 1;

   cursor C_GET_TSFDETAIL_INFO is
      select item,
             tsf_qty,
             inv_status,
             supp_pack_size,
             tsf_seq_no,
          tsf_price
        from tsfdetail
       where tsf_no = I_message.tsf_no;

   cursor C_ITEM_SUPP_COUNTRY_COST is
       select unit_cost,supplier
         from item_supp_country
        where item = L_item
          and primary_supp_ind ='Y'
          and primary_country_ind ='Y';

     cursor C_CHECK_ITEM_LOC_CURRENCY is
        select st.currency_code
          from store st
         where st.store = I_message.from_loc
           and I_message.from_loc_type ='S';

      cursor C_CHECK_ITEM_LOC is
       select 'Y'
         from item_loc
        where item = L_item
          and loc = I_message.from_loc
          and I_message.from_loc_type ='S';

   TYPE TSF_DETAIL_DATA is TABLE OF C_GET_TSFDETAIL_INFO%ROWTYPE
      INDEX BY BINARY_INTEGER;

   L_tsf_detail_data     TSF_DETAIL_DATA;

BEGIN

   open C_GET_HEADER;
   fetch C_GET_HEADER into L_existing_status;
   close C_GET_HEADER;

   open C_CHECK_DETAILS;
   fetch C_CHECK_DETAILS into L_details_exist;
   close C_CHECK_DETAILS;

   if I_message_type = RMSSUB_XTSF.LP_mod_type and
      I_message.status is NOT NULL and
      I_message.status = 'A' and
      L_existing_status != 'A' and
      L_details_exist = 'N' then
      ---
      O_error_message := SQL_LIB.CREATE_MSG('DTLS_MUST_EXIST', NULL, NULL, NULL);
      return FALSE;
   end if;

   O_tsf_rec.header.tsf_no         := I_message.tsf_no;
   O_tsf_rec.header.delivery_date  := TRUNC(I_message.delivery_date);
   O_tsf_rec.header.to_loc         := I_message.to_loc;
   O_tsf_rec.header.to_loc_type    := I_message.to_loc_type;
   O_tsf_rec.header.from_loc       := I_message.from_loc;
   O_tsf_rec.header.from_loc_type  := I_message.from_loc_type;
   O_tsf_rec.header.dept           := I_message.dept;
   O_tsf_rec.header.routing_code   := I_message.routing_code;
   O_tsf_rec.header.comment_desc   := I_message.comment_desc;
   O_tsf_rec.header.context_type   := I_message.context_type;
   O_tsf_rec.header.context_value  := I_message.context_value;

   if I_message.freight_code is NOT NULL then
      O_tsf_rec.header.freight_code := I_message.freight_code;
   else
      O_tsf_rec.header.freight_code := RMSSUB_XTSF_DEFAULT.DEFAULT_FREIGHT_CODE;
   end if;

   if I_message.tsf_type is NOT NULL then
      O_tsf_rec.header.tsf_type := I_message.tsf_type;
   else
      if TRANSFER_SQL.IS_INTERCOMPANY (O_error_message,
                                       L_intercompany ,
                                       'T',                -- Distro Type
                                       I_message.tsf_type, --I_tsf_type
                                       I_message.from_loc,
                                       I_message.from_loc_type,
                                       I_message.to_loc,
                                       I_message.to_loc_type) = FALSE then
         return FALSE;
      end if;
      --
      if L_intercompany then
         O_tsf_rec.header.tsf_type := 'IC';
      else
         O_tsf_rec.header.tsf_type := RMSSUB_XTSF_DEFAULT.DEFAULT_TSF_TYPE;
      end if;
   end if;

   if I_message.status is NOT NULL then
      O_tsf_rec.header.status := I_message.status;
   else
      O_tsf_rec.header.status := RMSSUB_XTSF_DEFAULT.DEFAULT_STATUS;
   end if;

   if I_message.user_id is NOT NULL then
      O_tsf_rec.header.create_id := I_message.user_id;
   else
      O_tsf_rec.header.create_id := GET_USER;
   end if;

   open C_GET_MAX_SEQ_NO;
   fetch C_GET_MAX_SEQ_NO into L_tsf_seq_no;
   close C_GET_MAX_SEQ_NO;

   -- add one to the max seq_no, so that we are beyond
   -- what is already on tsfdetail or beyond the 0 that
   -- would be returned if this is a new tsf being created.
   L_tsf_seq_no := L_tsf_seq_no + 1;

   -- initialize collection
   O_tsf_rec.tsf_detail.items := ITEM_TBL();
   O_tsf_rec.tsf_detail.seq_nos := RMSSUB_XTSF.SEQ_TBL();
   O_tsf_rec.tsf_detail.qtys := QTY_TBL();
   O_tsf_rec.tsf_detail.existing_qtys := QTY_TBL();
   O_tsf_rec.tsf_detail.supp_pack_sizes := RMSSUB_XTSF.SUPP_PACK_TBL();
   O_tsf_rec.tsf_detail.inv_statuses := RMSSUB_XTSF.INV_TBL();
   O_tsf_rec.tsf_detail.pack_inds := INDICATOR_TBL();
   O_tsf_rec.tsf_detail.pack_types := INDICATOR_TBL();
   O_tsf_rec.tsf_detail.cancelled_qtys := QTY_TBL();
   O_tsf_rec.tsf_detail.selected_qtys   := QTY_TBL();
   O_tsf_rec.tsf_detail.tsf_price := RMSSUB_XTSF.PRICE_TBL();
   O_tsf_rec.new_itemloc.items := ITEM_TBL();
   O_tsf_rec.new_itemloc.pack_inds := INDICATOR_TBL();
   O_tsf_rec.new_itemloc.pack_types := INDICATOR_TBL();
   O_tsf_rec.new_itemloc.locs := LOC_TBL();
   O_tsf_rec.new_itemloc.loc_types := LOC_TYPE_TBL();

   if I_message_type in (RMSSUB_XTSF.LP_cre_type, RMSSUB_XTSF.LP_dtl_cre_type) then

      if I_message.XtsfDtl_TBL is NOT NULL then

         FOR i in 1..I_message.XTsfDtl_TBL.COUNT LOOP

            L_item := I_message.XTsfDtl_TBL(i).item;
            L_avg_cost := NULL;
            O_tsf_rec.tsf_detail.items.EXTEND;
            O_tsf_rec.tsf_detail.seq_nos.EXTEND;
            O_tsf_rec.tsf_detail.qtys.EXTEND;
            O_tsf_rec.tsf_detail.existing_qtys.EXTEND;
            O_tsf_rec.tsf_detail.supp_pack_sizes.EXTEND;
            O_tsf_rec.tsf_detail.inv_statuses.EXTEND;
            O_tsf_rec.tsf_detail.pack_inds.EXTEND;
            O_tsf_rec.tsf_detail.pack_types.EXTEND;
            O_tsf_rec.tsf_detail.tsf_price.EXTEND;
            O_tsf_rec.tsf_detail.items(i)        := L_item;
            O_tsf_rec.tsf_detail.seq_nos(i)      := L_tsf_seq_no;
            O_tsf_rec.tsf_detail.inv_statuses(i) := I_message.XTsfDtl_TBL(i).inv_status;

            open C_GET_PACK_IND;
            fetch C_GET_PACK_IND into L_pack_ind,
                                      L_pack_type,
                                      L_orderable_ind;
            close C_GET_PACK_IND;

            if I_message.from_loc_type='S' then
                open C_CHECK_ITEM_LOC;
                fetch C_CHECK_ITEM_LOC into L_loc_exists;
                close C_CHECK_ITEM_LOC;
                if L_loc_exists = 'N' then
                   open  C_CHECK_ITEM_LOC_CURRENCY;
                   fetch C_CHECK_ITEM_LOC_CURRENCY into L_local_currency;
                   close C_CHECK_ITEM_LOC_CURRENCY;

                   if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type in ('SVAT','SALES') then
                      open C_ITEM_SUPP_COUNTRY_COST;
                      fetch C_ITEM_SUPP_COUNTRY_COST into L_item_supp_cost,L_prim_supplier;
                      close C_ITEM_SUPP_COUNTRY_COST;

                      if not SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                                               L_supp_currency,
                                                               L_prim_supplier) then
                        return FALSE;
                      end if;

                      if L_local_currency != L_supp_currency then
                         if not CURRENCY_SQL.CONVERT(O_error_message,
                                                     L_item_supp_cost,
                                                     L_supp_currency,
                                                     L_local_currency,
                                                     L_avg_cost,
                                                     'C',
                                                     NULL,
                                                     NULL) then
                            return FALSE;
                         end if;
                      else
                          L_avg_cost := L_item_supp_cost;
                      end if;
                   else
                      if SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type in('GTAX') then
                         O_error_message:= SQL_LIB.CREATE_MSG('NO_ITEM_LOC', NULL,
                                                     NULL, NULL);
                         return FALSE;
                      end if;
                   end if;    -- end if of SYSTEM_OPTIONS_SQL.GP_system_options_row.default_tax_type in ('SVAT','SALES')
                end if; -- end if of C_CHECK_ITEM_LOC%NOTFOUND
            end if;

            if O_tsf_rec.header.tsf_type != 'IC' and (I_message.XTsfDtl_TBL(i).adjustment_type is NOT NULL or I_message.XTsfDtl_TBL(i).adjustment_value is NOT NULL) then
               O_error_message := SQL_LIB.CREATE_MSG('ADJ_REQUIRED_IC', null, null, null);
               return FALSE;
            end if;

            if O_tsf_rec.header.tsf_type = 'IC' then
               if L_avg_cost IS NULL then
                  if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                              L_item,
                                                              I_message.from_loc,
                                                              I_message.from_loc_type,
                                                              'Y',
                                                              L_avg_cost,
                                                              L_dummy_num,
                                                              L_dummy_num,
                                                              L_dummy_num,
                                                              L_dummy_var) = FALSE then
                     return FALSE;
                  end if;
               end if;

               L_cost := ROUND(L_avg_cost,4);

               if I_message.XTsfDtl_TBL(i).adjustment_type IS NOT NULL then
                  if I_message.XTsfDtl_TBL(i).adjustment_type = 'DA' then
                     if L_cost >= I_message.XTsfDtl_TBL(i).adjustment_value then
                        L_value := L_cost - I_message.XTsfDtl_TBL(i).adjustment_value;
                     else
                        O_error_message := SQL_LIB.CREATE_MSG('TSF_PRICE_POS', null, null, null);
                        return FALSE;
                     end if;

                  elsif I_message.XTsfDtl_TBL(i).adjustment_type = 'DP' then
                     if I_message.XTsfDtl_TBL(i).adjustment_value <= 100 then
                        L_value := L_cost - (L_cost * (I_message.XTsfDtl_TBL(i).adjustment_value/100));
                     else
                        O_error_message := SQL_LIB.CREATE_MSG('VALID_PERCENT_RANGE', null, null, null);
                        return FALSE;
                     end if;

                  elsif I_message.XTsfDtl_TBL(i).adjustment_type = 'IA' then
                     L_value := L_cost + I_message.XTsfDtl_TBL(i).adjustment_value;

                  elsif I_message.XTsfDtl_TBL(i).adjustment_type = 'IP' then
                     L_value := L_cost + (L_cost * (I_message.XTsfDtl_TBL(i).adjustment_value/100));

                  elsif I_message.XTsfDtl_TBL(i).adjustment_type = 'S' then
                     if SYSTEM_OPTIONS_SQL.GP_system_options_row.tsf_price_exceed_wac_ind = 'N' and L_cost < I_message.XTsfDtl_TBL(i).adjustment_value then
                        O_error_message := SQL_LIB.CREATE_MSG('TSF_PRICE_NO_EXCEED_WAC', null, null, null);
                        return FALSE;
                     Else
                        L_value := I_message.XTsfDtl_TBL(i).adjustment_value;
                     end if;
                  end if;
               else
                  L_value := L_cost;
               end if;

               L_tsf_price := L_value;
            end if;
            if I_message.XTsfDtl_TBL(i).supp_pack_size is NOT NULL then
               O_tsf_rec.tsf_detail.supp_pack_sizes(i) := I_message.XTsfDtl_TBL(i).supp_pack_size;
            else
               if L_orderable_ind = 'N' then
                  O_tsf_rec.tsf_detail.supp_pack_sizes(i) := 1;
               else
                  if RMSSUB_XTSF_DEFAULT.DEFAULT_SUPP_PACK_SIZE(O_error_message,
                                                                O_tsf_rec.tsf_detail.supp_pack_sizes(i),
                                                                L_item) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;

            O_tsf_rec.tsf_detail.pack_inds(i)     := L_pack_ind;
            O_tsf_rec.tsf_detail.pack_types(i)    := L_pack_type;
            O_tsf_rec.tsf_detail.qtys(i)          := I_message.XTsfDtl_TBL(i).tsf_qty;
            O_tsf_rec.tsf_detail.existing_qtys(i) := 0;
            O_tsf_rec.tsf_detail.tsf_price(i)     := L_tsf_price;
            --Range item/to-loc.
            --In case of unranged physical wh, do NOT range item-physical_wh. Its virtual whs will be ranged through rmssub_xtsf_sql.UPD_PHYSICAL_WHS.
            if I_message.to_loc_type = 'W' then
               open C_IS_PHYSICAL_WH(I_message.to_loc);
               fetch C_IS_PHYSICAL_WH into L_is_physical_wh;
               if C_IS_PHYSICAL_WH%NOTFOUND then
                  L_is_physical_wh := 'N';
               end if;
               close C_IS_PHYSICAL_WH;
            end if;

            if L_is_physical_wh = 'N' then
               open C_CHECK_ITEM_TO_LOC;
               fetch C_CHECK_ITEM_TO_LOC into L_dummy;

               if C_CHECK_ITEM_TO_LOC%NOTFOUND then
                  O_tsf_rec.new_itemloc.items.EXTEND;
                  O_tsf_rec.new_itemloc.pack_inds.EXTEND;
                  O_tsf_rec.new_itemloc.pack_types.EXTEND;
                  O_tsf_rec.new_itemloc.locs.EXTEND;
                  O_tsf_rec.new_itemloc.loc_types.EXTEND;

                  L_index := O_tsf_rec.new_itemloc.items.COUNT;

                  O_tsf_rec.new_itemloc.items(L_index)      := L_item;
                  O_tsf_rec.new_itemloc.pack_inds(L_index)  := O_tsf_rec.tsf_detail.pack_inds(i);
                  O_tsf_rec.new_itemloc.pack_types(L_index) := O_tsf_rec.tsf_detail.pack_types(i);
                  O_tsf_rec.new_itemloc.locs(L_index)       := I_message.to_loc;
                  O_tsf_rec.new_itemloc.loc_types(L_index)  := I_message.to_loc_type;
               end if;
               close C_CHECK_ITEM_TO_LOC;
            end if;

            --Range item/from-loc.
            --In case of unranged physical wh, do NOT range item-physical_wh.
            L_is_physical_wh := 'N';
            if I_message.from_loc_type = 'W' then
               open C_IS_PHYSICAL_WH(I_message.from_loc);
               fetch C_IS_PHYSICAL_WH into L_is_physical_wh;
               if C_IS_PHYSICAL_WH%NOTFOUND then
                  L_is_physical_wh := 'N';
               end if;
               close C_IS_PHYSICAL_WH;
            end if;

            if L_is_physical_wh = 'N' then
               open C_CHECK_ITEM_FROM_LOC;
               fetch C_CHECK_ITEM_FROM_LOC into L_dummy;

               if C_CHECK_ITEM_FROM_LOC%NOTFOUND then
                  O_tsf_rec.new_itemloc.items.EXTEND;
                  O_tsf_rec.new_itemloc.pack_inds.EXTEND;
                  O_tsf_rec.new_itemloc.pack_types.EXTEND;
                  O_tsf_rec.new_itemloc.locs.EXTEND;
                  O_tsf_rec.new_itemloc.loc_types.EXTEND;

                  L_index := O_tsf_rec.new_itemloc.items.COUNT;

                  O_tsf_rec.new_itemloc.items(L_index)      := L_item;
                  O_tsf_rec.new_itemloc.pack_inds(L_index)  := O_tsf_rec.tsf_detail.pack_inds(i);
                  O_tsf_rec.new_itemloc.pack_types(L_index) := O_tsf_rec.tsf_detail.pack_types(i);
                  O_tsf_rec.new_itemloc.locs(L_index)       := I_message.from_loc;
                  O_tsf_rec.new_itemloc.loc_types(L_index)  := I_message.from_loc_type;
               end if;
               close C_CHECK_ITEM_FROM_LOC;
            end if;

            -- increment to keep uniqueness
            L_tsf_seq_no := L_tsf_seq_no + 1;

         end LOOP;
      end if; -- dtl_TBL is not NULL

   elsif I_message_type = RMSSUB_XTSF.LP_dtl_mod_type then
if O_tsf_rec.header.tsf_type = 'IC' and (L_existing_status = 'A' or L_existing_status = 'S') then
         open C_CHECK_SHIP_EXISTS;
         fetch C_CHECK_SHIP_EXISTS into L_ship_exists;
         close C_CHECK_SHIP_EXISTS;
      end if;
      FOR i in I_message.XTsfDtl_TBL.first..I_message.XTsfDtl_TBL.last LOOP

         L_item := I_message.XTsfDtl_TBL(i).item;

         O_tsf_rec.tsf_detail.qtys.EXTEND;
         O_tsf_rec.tsf_detail.existing_qtys.EXTEND;
         O_tsf_rec.tsf_detail.items.EXTEND;
         O_tsf_rec.tsf_detail.inv_statuses.EXTEND;
         O_tsf_rec.tsf_detail.pack_inds.EXTEND;
         O_tsf_rec.tsf_detail.cancelled_qtys.EXTEND;
         O_tsf_rec.tsf_detail.supp_pack_sizes.EXTEND;
         O_tsf_rec.tsf_detail.seq_nos.EXTEND;
         O_tsf_rec.tsf_detail.selected_qtys.EXTEND;

         open C_GET_DTL_QTY;
         fetch C_GET_DTL_QTY into L_existing_qty,
                                  L_cancelled_qty,
                                  L_tsf_seq_num;
         close C_GET_DTL_QTY;

         O_tsf_rec.tsf_detail.qtys(i)            := I_message.XTsfDtl_TBL(i).tsf_qty;
         O_tsf_rec.tsf_detail.existing_qtys(i)   := L_existing_qty;
         O_tsf_rec.tsf_detail.items(i)           := I_message.XTsfDtl_TBL(i).item;
         O_tsf_rec.tsf_detail.inv_statuses(i)    := I_message.XTsfDtl_TBL(i).inv_status;
         O_tsf_rec.tsf_detail.supp_pack_sizes(i) := I_message.XTsfDtl_TBL(i).supp_pack_size;
         O_tsf_rec.tsf_detail.seq_nos(i)         := L_tsf_seq_num;

         open C_GET_PACK_IND;
         fetch C_GET_PACK_IND into O_tsf_rec.tsf_detail.pack_inds(i),
                                   L_pack_type,
                                   L_orderable_ind;
         close C_GET_PACK_IND;

         if I_message.XTsfDtl_TBL(i).tsf_qty > (L_existing_qty + NVL(L_cancelled_qty,0)) then
            O_tsf_rec.tsf_detail.cancelled_qtys(i) := NULL;
         else
            L_change := I_message.XTsfDtl_TBL(i).tsf_qty - L_existing_qty;

            if L_existing_status in ('A','S') then
               O_tsf_rec.tsf_detail.cancelled_qtys(i) := NVL(L_cancelled_qty, 0) - L_change;
            else
               O_tsf_rec.tsf_detail.cancelled_qtys(i) := L_cancelled_qty;
            end if;
         end if;

if O_tsf_rec.header.tsf_type != 'IC' and (I_message.XTsfDtl_TBL(i).adjustment_type is NOT NULL or I_message.XTsfDtl_TBL(i).adjustment_value is NOT NULL) then
            O_error_message := SQL_LIB.CREATE_MSG('ADJ_REQUIRED_IC', null, null, null);
            return FALSE;
         end if;
    if O_tsf_rec.header.tsf_type = 'IC' and (L_existing_status = 'I' or L_existing_status = 'A' or L_existing_status = 'S') then
            if L_existing_status = 'A' or L_existing_status = 'S' then
               if L_ship_exists is not NULL and I_message.XTsfDtl_TBL(i).adjustment_type IS NOT NULL and I_message.XTsfDtl_TBL(i).adjustment_value IS NOT NULL then
                  L_ship_flag := 'Y';
                  O_error_message := SQL_LIB.CREATE_MSG('CANT_CHG_TSF_PRICE', null, null, null);
                  return FALSE;
               elsif L_ship_exists is not NULL and I_message.XTsfDtl_TBL(i).adjustment_type IS NULL and I_message.XTsfDtl_TBL(i).adjustment_value IS NULL then
                  L_ship_flag := 'Y';
               else
                  L_ship_flag := 'N';
               end if;
            end if;
            if L_ship_flag = 'N' then
               O_tsf_rec.tsf_detail.tsf_price.EXTEND;
               if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                           L_item,
                                                           I_message.from_loc,
                                                           I_message.from_loc_type,
                                                           'Y',
                                                           L_avg_cost,
                                                           L_dummy_num,
                                                           L_dummy_num,
                                                           L_dummy_num,
                                                           L_dummy_var) = FALSE then
                  return FALSE;
               end if;
               L_cost := ROUND(L_avg_cost,4);
              if I_message.XTsfDtl_TBL(i).adjustment_type IS NOT NULL then
                  if I_message.XTsfDtl_TBL(i).adjustment_type = 'DA' then
                     if L_cost >= I_message.XTsfDtl_TBL(i).adjustment_value then
                        L_value := L_cost - I_message.XTsfDtl_TBL(i).adjustment_value;
                     else
                        O_error_message := SQL_LIB.CREATE_MSG('TSF_PRICE_POS', null, null, null);
                        return FALSE;
                     end if;
                  elsif I_message.XTsfDtl_TBL(i).adjustment_type = 'DP' then
                     if I_message.XTsfDtl_TBL(i).adjustment_value <= 100 then
                        L_value := L_cost - (L_cost * (I_message.XTsfDtl_TBL(i).adjustment_value/100));
                     else
                        O_error_message := SQL_LIB.CREATE_MSG('VALID_PERCENT_RANGE', null, null, null);
                        return FALSE;
                     end if;

                  elsif I_message.XTsfDtl_TBL(i).adjustment_type = 'IA' then
                     L_value := L_cost + I_message.XTsfDtl_TBL(i).adjustment_value;

                  elsif I_message.XTsfDtl_TBL(i).adjustment_type = 'IP' then
                     L_value := L_cost + (L_cost * (I_message.XTsfDtl_TBL(i).adjustment_value/100));

                  elsif I_message.XTsfDtl_TBL(i).adjustment_type = 'S' then
                     if SYSTEM_OPTIONS_SQL.GP_system_options_row.tsf_price_exceed_wac_ind = 'N' and L_cost < I_message.XTsfDtl_TBL(i).adjustment_value then
                        O_error_message := SQL_LIB.CREATE_MSG('TSF_PRICE_NO_EXCEED_WAC', null, null, null);
                        return FALSE;
                     else
                        L_value := I_message.XTsfDtl_TBL(i).adjustment_value;
                     end if;
                  end if;
               else
                  L_value := L_cost;
               end if;

               O_tsf_rec.tsf_detail.tsf_price(i)     := L_value;
            end if;/* End ship_flag*/
         end if;/* End tsf_type IC*/
      END LOOP;
   elsif I_message_type = RMSSUB_XTSF.LP_mod_type and L_existing_status = 'I' and I_message.status = 'A' then

      OPEN C_GET_TSFDETAIL_INFO;
      FETCH C_GET_TSFDETAIL_INFO BULK COLLECT INTO L_tsf_detail_data;
      CLOSE C_GET_TSFDETAIL_INFO;

      FOR i in 1..L_TSF_DETAIL_DATA.COUNT LOOP

         L_item := L_tsf_detail_data(i).item;

         O_tsf_rec.tsf_detail.qtys.EXTEND;
         O_tsf_rec.tsf_detail.existing_qtys.EXTEND;
         O_tsf_rec.tsf_detail.items.EXTEND;
         O_tsf_rec.tsf_detail.inv_statuses.EXTEND;
         O_tsf_rec.tsf_detail.pack_inds.EXTEND;
         O_tsf_rec.tsf_detail.cancelled_qtys.EXTEND;
         O_tsf_rec.tsf_detail.supp_pack_sizes.EXTEND;
         O_tsf_rec.tsf_detail.seq_nos.EXTEND;
         O_tsf_rec.tsf_detail.tsf_price.EXTEND;
         O_tsf_rec.tsf_detail.qtys(i)            := L_tsf_detail_data(i).tsf_qty;
         O_tsf_rec.tsf_detail.existing_qtys(i)   := 0;
         O_tsf_rec.tsf_detail.items(i)           := L_tsf_detail_data(i).item;
         O_tsf_rec.tsf_detail.inv_statuses(i)    := L_tsf_detail_data(i).inv_status;
         O_tsf_rec.tsf_detail.supp_pack_sizes(i) := L_tsf_detail_data(i).supp_pack_size;
         O_tsf_rec.tsf_detail.seq_nos(i)         := L_tsf_detail_data(i).tsf_seq_no;
         O_tsf_rec.tsf_detail.tsf_price(i)       := L_tsf_detail_data(i).tsf_price;
         open C_GET_PACK_IND;
         fetch C_GET_PACK_IND into O_tsf_rec.tsf_detail.pack_inds(i),
                                   L_pack_type,
                                   L_orderable_ind;
         close C_GET_PACK_IND;

      END LOOP;
   end if;

   return TRUE;

EXCEPTION
when RECORD_LOCKED then

      if C_CHECK_SHIP_EXISTS%ISOPEN then
         close C_CHECK_SHIP_EXISTS;
      end if;

      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'SHIPSKU',
                                                              I_message.tsf_no);
      RETURN FALSE;
   when OTHERS then
      if C_CHECK_SHIP_EXISTS%ISOPEN then
         close C_CHECK_SHIP_EXISTS;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
---------------------------------------------------------------------------------------------------------
FUNCTION POPULATE_RECORD(O_error_message   IN OUT          RTK_ERRORS.RTK_TEXT%TYPE,
                         O_tsf_rec         OUT    NOCOPY   RMSSUB_XTSF.TSF_REC,
                         I_message         IN              "RIB_XTsfRef_REC",
                         I_message_type    IN              VARCHAR2)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XTSF_VALIDATE.POPULATE_RECORD';
   L_item         TSFDETAIL.ITEM%TYPE;

   cursor C_GET_DTL_QTY is
      select SUM(tsf_qty)
        from tsfdetail
       where tsf_no = I_message.tsf_no
         and item   = L_item
       group by tsf_no, item;

   cursor C_GET_TSF_DETAILS is
      select item,
             tsf_qty,
             0,
             tsf_seq_no,
             inv_status
        from tsfdetail
       where tsf_no = I_message.tsf_no;

   cursor C_GET_TSF_SEQ_INV is
      select tsf_seq_no,
             inv_status
        from tsfdetail
       where tsf_no = I_message.tsf_no
         and item = L_item;

   cursor C_GET_PACK_IND is
      select pack_ind
        from item_master
       where item = L_item;

   cursor C_GET_HEADER_LOCS is
      select to_loc,
             to_loc_type,
             from_loc,
             from_loc_type
        from tsfhead
       where tsf_no = I_message.tsf_no;

BEGIN
   O_tsf_rec.header.tsf_no    := I_message.tsf_no;

   open C_GET_HEADER_LOCS;
   fetch C_GET_HEADER_LOCS into O_tsf_rec.header.to_loc,
                                O_tsf_rec.header.to_loc_type,
                                O_tsf_rec.header.from_loc,
                                O_tsf_rec.header.from_loc_type;
   close C_GET_HEADER_LOCS;

   -- initialize collection
   O_tsf_rec.tsf_detail.items := ITEM_TBL();
   O_tsf_rec.tsf_detail.qtys := QTY_TBL();
   O_tsf_rec.tsf_detail.existing_qtys := QTY_TBL();
   O_tsf_rec.tsf_detail.pack_inds := INDICATOR_TBL();
   O_tsf_rec.tsf_detail.seq_nos := RMSSUB_XTSF.SEQ_TBL();
   O_tsf_rec.tsf_detail.inv_statuses := RMSSUB_XTSF.INV_TBL();

   if I_message_type = RMSSUB_XTSF.LP_dtl_del_type then

      for i in I_message.XTsfDtlRef_TBL.FIRST..I_message.XTsfDtlRef_TBL.LAST LOOP
         O_tsf_rec.tsf_detail.items.EXTEND;
         O_tsf_rec.tsf_detail.existing_qtys.EXTEND;
         O_tsf_rec.tsf_detail.qtys.EXTEND;
         O_tsf_rec.tsf_detail.pack_inds.EXTEND;
         O_tsf_rec.tsf_detail.seq_nos.EXTEND;
         O_tsf_rec.tsf_detail.inv_statuses.EXTEND;
         ---
         L_item := I_message.XTsfDtlRef_TBL(i).item;

         O_tsf_rec.tsf_detail.items(i) := L_item;
         O_tsf_rec.tsf_detail.qtys(i)  := 0;

         open C_GET_DTL_QTY;
         fetch C_GET_DTL_QTY into O_tsf_rec.tsf_detail.existing_qtys(i);
         close C_GET_DTL_QTY;

         open C_GET_PACK_IND;
         fetch C_GET_PACK_IND into O_tsf_rec.tsf_detail.pack_inds(i);
         close C_GET_PACK_IND;

         open C_GET_TSF_SEQ_INV;
         fetch C_GET_TSF_SEQ_INV into O_tsf_rec.tsf_detail.seq_nos(i),
                                      O_tsf_rec.tsf_detail.inv_statuses(i);
         close C_GET_TSF_SEQ_INV;
      end LOOP;

   elsif I_message_type = RMSSUB_XTSF.LP_del_type then

      open C_GET_TSF_DETAILS;
      fetch C_GET_TSF_DETAILS bulk collect into O_tsf_rec.tsf_detail.items,
                                                O_tsf_rec.tsf_detail.existing_qtys,
                                                O_tsf_rec.tsf_detail.qtys,
                                                O_tsf_rec.tsf_detail.seq_nos,
                                                O_tsf_rec.tsf_detail.inv_statuses;
      close C_GET_TSF_DETAILS;

      for i in O_tsf_rec.tsf_detail.items.FIRST..O_tsf_rec.tsf_detail.items.LAST LOOP
         L_item := O_tsf_rec.tsf_detail.items(i);

         O_tsf_rec.tsf_detail.pack_inds.EXTEND;

         open C_GET_PACK_IND;
         fetch C_GET_PACK_IND into O_tsf_rec.tsf_detail.pack_inds(i);
         close C_GET_PACK_IND;

      end LOOP;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END POPULATE_RECORD;
---------------------------------------------------------------------------------------------------------
END RMSSUB_XTSF_VALIDATE;
/
