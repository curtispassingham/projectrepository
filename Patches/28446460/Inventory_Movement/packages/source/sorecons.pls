
CREATE OR REPLACE PACKAGE STOCK_ORDER_RECONCILE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
-- FORCED_RECEIVE
-- Stock Order Exception Reconciliation: After the adjustment, the discrepancy
-- is overridden and the Transferred quantity is matched to the shipped quantity.
--------------------------------------------------------------------------------
FUNCTION FORCED_RECEIVE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                        I_carton           IN     SHIPSKU.CARTON%TYPE,
                        I_adjust_level     IN     VARCHAR2,
                        I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                        I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                        I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                        I_to_loc           IN     SHIPMENT.TO_LOC%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- FROM_LOC_ADJUST
-- Stock Order Exception Reconciliation: Shipping Location is assessed the cost
-- of the discrepancy.  No adjustment needed for overage.
--------------------------------------------------------------------------------
FUNCTION FROM_LOC_ADJUST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                         I_carton           IN       SHIPSKU.CARTON%TYPE,
                         I_adjust_level     IN       VARCHAR2,
                         I_from_loc_type    IN       SHIPMENT.FROM_LOC_TYPE%TYPE,
                         I_from_loc         IN       SHIPMENT.FROM_LOC%TYPE,
                         I_to_loc_type      IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                         I_to_loc           IN       SHIPMENT.TO_LOC%TYPE,
                         I_distro_no        IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL,
                         I_document_type    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- FREIGHT_CLAIM
-- Stock Order Exception Reconciliation: Cost is flagged for the freight claim
-- process that will occur in the GL.  It is assumed to be applied only for
-- shortage.
--------------------------------------------------------------------------------
FUNCTION FREIGHT_CLAIM(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_shipment         IN     SHIPMENT.SHIPMENT%TYPE,
                       I_carton           IN     SHIPSKU.CARTON%TYPE,
                       I_adjust_level     IN     VARCHAR2,
                       I_from_loc_type    IN     SHIPMENT.FROM_LOC_TYPE%TYPE,
                       I_from_loc         IN     SHIPMENT.FROM_LOC%TYPE,
                       I_to_loc_type      IN     SHIPMENT.TO_LOC_TYPE%TYPE,
                       I_to_loc           IN     SHIPMENT.TO_LOC%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- TO_LOC_ADJUST
-- Stock Order Exception Reconciliation: Receiving Location is assessed the cost
-- of the discrepancy.  It is assumed to be applied only for shortage.
--------------------------------------------------------------------------------
FUNCTION TO_LOC_ADJUST(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                       I_carton           IN       SHIPSKU.CARTON%TYPE,
                       I_adjust_level     IN       VARCHAR2,
                       I_from_loc_type    IN       SHIPMENT.FROM_LOC_TYPE%TYPE,
                       I_from_loc         IN       SHIPMENT.FROM_LOC%TYPE,
                       I_to_loc_type      IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                       I_to_loc           IN       SHIPMENT.TO_LOC%TYPE,
                       I_distro_no        IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL,
                       I_document_type    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- POPULATE_TEMP_TABLES
-- Populates so_shipment_temp and so_shipsku_temp tables for the Stock Order
-- Exception Reconciliation form for the specific condition.
--------------------------------------------------------------------------------
FUNCTION POPULATE_TEMP_TABLES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_where_clause   IN      VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- CLEAN_TEMP_TABLES
-- Deletes all records from so_shipment_temp and so_shipsku_temp tables.
--------------------------------------------------------------------------------
FUNCTION CLEAN_TEMP_TABLES(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- GET_CARTON_INDS
-- Returns O_carton_disp_ind:
--       'Y' if the shipment has adjustments made at the carton level
--       'N' otherwise.
--------------------------------------------------------------------------------
FUNCTION GET_CARTON_INDS(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_carton_disp_ind        IN OUT  SO_SHIPMENT_TEMP.CARTON_DISP_IND%TYPE,
                         I_shipment               IN      SHIPMENT.SHIPMENT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- APPLY_ADJUSTMENTS
-- Apply stock order reconciliation adjustments to records updated on the Stock
-- Order Exception Reconciliation form.  Reads from so_shipsku_temp and
-- so_shipment_temp tables.
--------------------------------------------------------------------------------
FUNCTION APPLY_ADJUSTMENTS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- VALIDATE_BOL_NO
-- Checks the so_shipment_temp tables to see if the passed in BOL_NO exists.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_BOL_NO(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid          IN OUT  BOOLEAN,
                         I_bol_no         IN      SO_SHIPMENT_TEMP.BOL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- VALIDATE_DISTRO_NO
-- Checks the shipsku tables to see if the passed in DISTRO_NO exists for reconciliation.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_DISTRO_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid         IN OUT BOOLEAN,
                            I_distro_no     IN SHIPSKU.DISTRO_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- VALIDATE_CARTON
-- Checks the so_shipsku_temp tables to see if the passed in CARTON exists.
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CARTON(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_valid          IN OUT  BOOLEAN,
                         I_carton         IN      SO_SHIPSKU_TEMP.CARTON%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- LOCK_SHIPSKU
-- Lock shipsku table for shipment passed or shipment and carton passed.
--------------------------------------------------------------------------------
FUNCTION LOCK_SHIPSKU(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_shipment       IN      SO_SHIPMENT_TEMP.SHIPMENT%TYPE,
                      I_carton         IN      SO_SHIPSKU_TEMP.CARTON%TYPE,
                      I_adjust_type    IN      SO_SHIPSKU_TEMP.ADJUST_TYPE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_ADJUST_TYPE 
--       Purpose: The function will check whether or not the given adjust type is allowed.
--                If the associated tsf is single-leg tsf or is the 1st-leg tsf, simply return
--                TRUE to O_ajust_type_allowed. For multi-leg tsf, if the associated tsf is
--                the 2nd-leg and its 1st-leg isn't closed, simply return FALSE to 
--                O_ajust_type_allowed; if the associated tsf is 2nd-leg and its 1st-leg has
--                been closed, verify individually.
--------------------------------------------------------------------------------------------
FUNCTION VALIDATE_ADJUST_TYPE(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              O_ajust_type_allowed  IN OUT  BOOLEAN,
                              I_shipment            IN      SO_SHIPMENT_TEMP.SHIPMENT%TYPE,
                              I_carton              IN      SO_SHIPSKU_TEMP.CARTON%TYPE,
                              I_adjust_type         IN      SO_SHIPSKU_TEMP.ADJUST_TYPE%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- TO_LOC_ADJUST_NL_BL
-- Auto Reconciliation: No Location is assessed the cost
-- of the discrepancy. It is assumed to be applied only for shortage.
---------------------------------------------------------------------------------------------
FUNCTION TO_LOC_ADJUST_NL_BL(O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_shipment         IN       SHIPMENT.SHIPMENT%TYPE,
                             I_carton           IN       SHIPSKU.CARTON%TYPE,
                             I_adjust_level     IN       VARCHAR2,
                             I_from_loc_type    IN       SHIPMENT.FROM_LOC_TYPE%TYPE,
                             I_from_loc         IN       SHIPMENT.FROM_LOC%TYPE,
                             I_to_loc_type      IN       SHIPMENT.TO_LOC_TYPE%TYPE,
                             I_to_loc           IN       SHIPMENT.TO_LOC%TYPE,
                             I_adjust_type      IN       SHIPSKU.ADJUST_TYPE%TYPE,
                             I_distro_no        IN       TSFHEAD.TSF_NO%TYPE DEFAULT NULL,
                             I_document_type    IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- APPLY_ADJUSTMENTS
-- Apply stock order reconciliation adjustments to short received shipments according to
-- tsf_force_close_ind from System_options
--------------------------------------------------------------------------------
FUNCTION APPLY_ADJUSTMENTS(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_document_type  IN       VARCHAR2,
                           I_asn_nbr        IN       SHIPMENT.BOL_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- GET_TO_LOC_VWH and GET_FROM_LOC_VWH
-- Queries SHIPITEM_INV_FLOW for the virtual warehouse used during shipment
-- if the transfer involves a physical warehouse. 
--------------------------------------------------------------------------------
FUNCTION GET_TO_LOC_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_loc_vwh         IN OUT   SHIPMENT.TO_LOC%TYPE,
                        I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                        I_loc_type        IN       SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------

FUNCTION GET_FROM_LOC_VWH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_loc_vwh         IN OUT   SHIPMENT.TO_LOC%TYPE,
                          I_distro_no       IN       SHIPSKU.DISTRO_NO%TYPE,
                          I_loc_type        IN       SHIPMENT.TO_LOC_TYPE%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- APPLY_ADJUSTMENTS_DOC_CLOSE
-- This new function is called during the doc close batch to reconcile the short shipment.
-- It is called if any reconciliation is not done for the transfer/allocation and criteria
-- for Auto close are met.
--------------------------------------------------------------------------------------------
FUNCTION APPLY_ADJUSTMENTS_DOC_CLOSE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_distro_no       IN       TSFHEAD.TSF_NO%TYPE,
                                      I_document_type   IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END STOCK_ORDER_RECONCILE_SQL;
/
