CREATE OR REPLACE PACKAGE BODY RMSSUB_RECEIPT AS


-------------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTION --
-- Called by RMSSUB_RECEIVING.CONSUME when the message type corresponds to a Receipt
-------------------------------------------------------------------------------------------------------

FUNCTION CONSUME(O_status_code          IN OUT  VARCHAR2,
                 O_error_message        IN OUT  VARCHAR2,
                 I_rib_receiptdesc_rec  IN      "RIB_ReceiptDesc_REC",
                 I_message_type         IN      VARCHAR2,
                 O_rib_otbdesc_rec         OUT  "RIB_OTBDesc_REC",
                 O_rib_error_tbl           OUT  RIB_ERROR_TBL)
return BOOLEAN IS

   L_program              VARCHAR2(61)    := 'RMSSUB_RECEIPT.CONSUME';
   L_head_index           BINARY_INTEGER  := 0;
   L_detail_index         BINARY_INTEGER  := 0;
   L_rib_otb_tbl          "RIB_OTB_TBL"     := NULL;
   L_tsf_no               TSFHEAD.TSF_NO%TYPE := NULL;
   L_tsfhead_row          TSFHEAD%ROWTYPE;
   L_second_leg_tsf_no    TSFHEAD.TSF_NO%TYPE := NULL;
   L_second_leg_from_loc  TSFHEAD.FROM_LOC%TYPE := NULL;
   L_l10n_exists_rec      L10N_EXISTS_REC := L10N_EXISTS_REC();
   L_reconcile            BOOLEAN         := false;
   L_tmp_index            BINARY_INTEGER  := 0;
   LP_system_options_row  SYSTEM_OPTIONS%ROWTYPE;
   L_transformed_wo_ind  VARCHAR2(1) := 'N';

   -- Define a table type to hold transfer number for repair context transfers
   TYPE tsf_no_table IS TABLE OF TSFHEAD.TSF_NO%TYPE INDEX BY BINARY_INTEGER;
   L_repair_tsf_no_tbl        tsf_no_table;
   L_tbl_size                 BINARY_INTEGER := 0;

   cursor C_GET_TSFHEAD is
      select *
        from tsfhead
       where tsf_no = L_tsf_no;

   cursor C_GET_SECOND_LEG_TSF is
      select tsf_no
        from tsfhead
       where tsf_parent_no = L_tsf_no;

   cursor C_FROM_LOC_SECOND_LEG_TSF is
      select from_loc
        from tsfhead
       where tsf_parent_no = L_tsf_no;

   cursor C_CHECK_FIRST_LEG_TRANSFORMED is
      select 'Y'
        from dual
       where EXISTS (select 'Y'
                       from tsf_wo_head
                      where tsf_no = (select tsf_parent_no from tsfhead where tsf_no = L_tsf_no))
          or EXISTS (select 'Y'
                       from tsf_xform
                     where tsf_no = (select tsf_parent_no from tsfhead where tsf_no = L_tsf_no));

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      return FALSE;
   end if;
   --- Initialize globals, clear out any leftover OTB info/cache DML
   if ORDER_RCV_SQL.INIT_PO_ASN_LOC_GROUP(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if STOCK_ORDER_RCV_SQL.INIT_TSF_ALLOC_GROUP(O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   L_head_index := I_rib_receiptdesc_rec.receipt_tbl.FIRST;
   while L_head_index is NOT NULL loop

      --- If this is a PO receipt then loop through each detail record and
      --- call PO receiving for each line item
      if I_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type = 'P' then
         If I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl is NOT NULL and I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.count > 0 then
           L_detail_index := I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.FIRST;
         else
           L_detail_index := NULL;
         end if;

         while L_detail_index is NOT NULL loop
            if I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).unit_qty != 0 then
               if ORDER_RCV_SQL.PO_LINE_ITEM(O_error_message,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).dc_dest_id,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).item_id,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).unit_qty,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_xactn_type,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_date,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).receipt_nbr,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr,
                                             I_rib_receiptdesc_rec.appt_nbr,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).container_id,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).distro_doc_type,
                                             to_number(I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).distro_nbr),
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).dest_id,
                                             NVL(I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).to_disposition,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).from_disposition),
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).unit_cost,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).shipped_qty,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).weight_uom,
                                             'N',
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl(L_detail_index).gross_cost,
                                             I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ref_doc_no
                                             ) = FALSE then
                  return FALSE;
               end if;
            end if;
            L_detail_index := I_rib_receiptdesc_rec.receipt_tbl(L_head_index).ReceiptDtl_tbl.NEXT(L_detail_index);
         end loop;

      --- If this is a transfer or allocation receipt then call
      --- the stock order receiving package.
      elsif I_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type in ('T','A','D','V') or I_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type is NULL then
         if RMSSUB_STKORD_RECEIPT.CONSUME(O_status_code,
                                          O_error_message,
                                          I_rib_receiptdesc_rec.appt_nbr,
                                          I_rib_receiptdesc_rec.receipt_tbl(L_head_index)) = FALSE then
            return FALSE;
         end if;

         --if the receipt records is for a transfer then retrieve any REPAIR context transfers
         if I_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type = 'T' and
            I_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr is not NULL then
            ---
            L_tsf_no := I_rib_receiptdesc_rec.receipt_tbl(L_head_index).po_nbr;
            ---
            open  C_GET_TSFHEAD;
            fetch C_GET_TSFHEAD into L_tsfhead_row;
            close C_GET_TSFHEAD;
            ---
            --check if the trasfer is a REPAIR context transfer
            if L_tsfhead_row.to_loc_type = 'E' then
               --retrieve the 2nd leg transfer number
               open C_GET_SECOND_LEG_TSF;
               fetch C_GET_SECOND_LEG_TSF into L_second_leg_tsf_no;
               close C_GET_SECOND_LEG_TSF;

               --copy the 2nd leg transfer number to the array
               L_tbl_size := L_tbl_size + 1;
               L_repair_tsf_no_tbl(L_tbl_size) := L_second_leg_tsf_no;
            end if;
         end if;

          --- check to see if the appt number occurs again:
         L_reconcile := TRUE;
         L_tmp_index := I_rib_receiptdesc_rec.receipt_tbl.NEXT(L_head_index);
         while L_tmp_index IS NOT NULL loop
            if I_rib_receiptdesc_rec.receipt_tbl(L_tmp_index).asn_nbr = I_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr AND
             I_rib_receiptdesc_rec.receipt_tbl(L_tmp_index).document_type in ('T','A','D','V') then
             L_reconcile  := FALSE;
             exit;
            end if;
            L_tmp_index := I_rib_receiptdesc_rec.receipt_tbl.NEXT(L_tmp_index);
         end loop;

         if L_reconcile THEN
            if STOCK_ORDER_RECONCILE_SQL.APPLY_ADJUSTMENTS(O_error_message,
                                                           I_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type,
                                                           I_rib_receiptdesc_rec.receipt_tbl(L_head_index).asn_nbr) = FALSE then
               return FALSE;
            end if;
         end if;
      --- If not PO/TSF/ALLOC then reject with error.
      else
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                               'document_type',
                                               NVL(I_rib_receiptdesc_rec.receipt_tbl(L_head_index).document_type,'NULL'),
                                               'P, A, T, D, or V');
         return FALSE;
      end if;

      L_head_index := I_rib_receiptdesc_rec.receipt_tbl.NEXT(L_head_index);
      ---
   end loop;

   --- Wrap up global/bulk processing
   if ORDER_RCV_SQL.FINISH_PO_ASN_LOC_GROUP(O_error_message,
                                            L_rib_otb_tbl) = FALSE then
      return FALSE;
   end if;
   ---
   if STOCK_ORDER_RCV_SQL.FINISH_TSF_ALLOC_GROUP(O_error_message) = FALSE then
      return FALSE;
   end if;

   --- Return OTB info
   if L_rib_otb_tbl.COUNT > 0 then
      O_rib_otbdesc_rec         := "RIB_OTBDesc_REC"(null,null);
      O_rib_otbdesc_rec.otb_tbl := L_rib_otb_tbl;
   else
      O_rib_otbdesc_rec := NULL;
   end if;

   --create shipment for any REPAIR context transfer
   if L_repair_tsf_no_tbl is NOT NULL and L_repair_tsf_no_tbl.COUNT > 0 then
      FOR i in L_repair_tsf_no_tbl.first..L_repair_tsf_no_tbl.last LOOP
        --
        SQL_LIB.SET_MARK('OPEN',
                         'C_FROM_LOC_SECOND_LEG_TSF',
                         'tsfhead',
                         NULL);
        open C_FROM_LOC_SECOND_LEG_TSF;
        SQL_LIB.SET_MARK('FETCH',
                         'C_FROM_LOC_SECOND_LEG_TSF',
                         'tsfhead',
                         NULL);
        fetch C_FROM_LOC_SECOND_LEG_TSF into L_second_leg_from_loc;
        SQL_LIB.SET_MARK('CLOSE',
                         'C_FROM_LOC_SECOND_LEG_TSF',
                         'tsfhead',
                         NULL);
        close C_FROM_LOC_SECOND_LEG_TSF;
        ---


        L_l10n_exists_rec.procedure_key  := 'CHECK_IF_BRAZIL_LOCALIZED';
        L_l10n_exists_rec.doc_type       := 'TSF';
        L_l10n_exists_rec.source_id      :=  L_second_leg_from_loc;
        L_l10n_exists_rec.source_entity  := 'LOC';
        L_tsf_no                         := L_repair_tsf_no_tbl(i);

        if L10N_SQL.EXEC_FUNCTION (O_error_message,
                                   L_l10n_exists_rec) = FALSE then
           return FALSE;
        end if;
        ---
        SQL_LIB.SET_MARK('OPEN',
                         'C_CHECK_FIRST_LEG_TRANSFORMED',
                         'tsfhead',
                         NULL);
        open C_CHECK_FIRST_LEG_TRANSFORMED;
        SQL_LIB.SET_MARK('FETCH',
                         'C_CHECK_FIRST_LEG_TRANSFORMED',
                         'tsfhead',
                         NULL);
        fetch C_CHECK_FIRST_LEG_TRANSFORMED into L_transformed_wo_ind;
        SQL_LIB.SET_MARK('CLOSE',
                         'C_CHECK_FIRST_LEG_TRANSFORMED',
                         'tsfhead',
                         NULL);
        close C_CHECK_FIRST_LEG_TRANSFORMED;
        ---
        if (nvl(L_l10n_exists_rec.exists_ind,'N') = 'N'  and L_transformed_wo_ind = 'N') then
           if SHIPMENT_SQL.SHIP_TSF(O_error_message,
                                    L_repair_tsf_no_tbl(i)) = FALSE then
              return FALSE;
           end if;
        end if;
         --
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
 when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
    return FALSE;

END CONSUME;
------------------------------------------------------------------------------------------------------
END RMSSUB_RECEIPT;
/
