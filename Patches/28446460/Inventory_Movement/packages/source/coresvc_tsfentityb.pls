CREATE OR REPLACE PACKAGE BODY CORESVC_TSF_ENTITY as
----------------------------------------------------------------------------------------------
-- GLOBAL VARIABLES
----------------------------------------------------------------------------------------------
L_error   BOOLEAN := FALSE;
----------------------------------------------------------------------------------------------
   cursor C_SVC_TSF_ENTITY(I_process_id   NUMBER,
                           I_chunk_id     NUMBER)is
      select  t.pk_tsf_entity_org_unit_sob_rid,
              t.pk_tsf_entity_rid,
              t.teo_fog_fk_rid,
              t.teo_ogu_fk_rid,
              t.teo_tsn_fk_rid,
              t.sttsf_rowid,
              t.stteo_rowid,
              t.sttsf_secondary_desc,
              t.pk_tsf_entity_sec_desc,
              t.sttsf_tsf_entity_id,
              lead(t.sttsf_tsf_entity_id, 1, 0) over (order by t.sttsf_tsf_entity_id)               as next_entity_id,
              row_number() over (partition by t.sttsf_tsf_entity_id order by t.sttsf_tsf_entity_id) as tsf_rank,
              t.sttsf_tsf_entity_desc,
              t.pk_tsf_entity_desc,
              t.sttsf_process_id,
              t.sttsf_chunk_id,
              t.stteo_chunk_id,
              t.sttsf_row_seq,
              t.sttsf_action,
              t.sttsf_process$status,
              t.stteo_set_of_books_id,
              t.stteo_org_unit_id,
              t.stteo_tsf_entity_id,
              t.stteo_process_id,
              t.stteo_row_seq,
              t.stteo_action,
              t.stteo_process$status
         from (select pk_tsf_entity_org_unit_sob.rowid  as pk_tsf_entity_org_unit_sob_rid,
                      pk_tsf_entity.rowid               as pk_tsf_entity_rid,
                      teo_fog_fk.rowid                  as teo_fog_fk_rid,
                      teo_ogu_fk.rowid                  as teo_ogu_fk_rid,
                      teo_tsn_fk.rowid                  as teo_tsn_fk_rid,
                      sttsf.rowid                       as sttsf_rowid,
                      stteo.rowid                       as stteo_rowid,
                      sttsf.secondary_desc              as sttsf_secondary_desc,
                      pk_tsf_entity.secondary_desc      as pk_tsf_entity_sec_desc,
                      sttsf.tsf_entity_id               as sttsf_tsf_entity_id,
                      sttsf.tsf_entity_desc             as sttsf_tsf_entity_desc,
                      pk_tsf_entity.tsf_entity_desc     as pk_tsf_entity_desc,
                      sttsf.process_id                  as sttsf_process_id,
                      sttsf.chunk_id                    as sttsf_chunk_id,
                      stteo.chunk_id                    as stteo_chunk_id,
                      sttsf.row_seq                     as sttsf_row_seq,
                      UPPER(sttsf.action)               as sttsf_action,
                      sttsf.process$status              as sttsf_process$status,
                      stteo.set_of_books_id             as stteo_set_of_books_id,
                      stteo.org_unit_id                 as stteo_org_unit_id,
                      stteo.tsf_entity_id               as stteo_tsf_entity_id,
                      stteo.process_id                  as stteo_process_id,
                      stteo.row_seq                     as stteo_row_seq,
                      UPPER(stteo.action)               as stteo_action,
                      stteo.process$status              as stteo_process$status
                 from svc_tsf_entity sttsf,
                      tsf_entity pk_tsf_entity,
                      svc_tsf_entity_org_unit_sob stteo,
                      tsf_entity_org_unit_sob pk_tsf_entity_org_unit_sob,
                      fif_gL_setup teo_fog_fk,
                      org_unit teo_ogu_fk,
                      tsf_entity teo_tsn_fk
                where sttsf.tsf_entity_id   = pk_tsf_entity.tsf_entity_id (+)
                  and sttsf.process_id      = I_process_id
                  and sttsf.chunk_id        = I_chunk_id
                  and sttsf.process_id      = stteo.process_id(+)
                  and sttsf.chunk_id        = stteo.chunk_id(+)
                  and sttsf.tsf_entity_id   = stteo.tsf_entity_id(+)
                  and stteo.org_unit_id     = pk_tsf_entity_org_unit_sob.org_unit_id (+)
                  and stteo.tsf_entity_id   = pk_tsf_entity_org_unit_sob.tsf_entity_id (+)
                  and stteo.tsf_entity_id   = teo_tsn_fk.tsf_entity_id (+)
                  and stteo.org_unit_id     = teo_ogu_fk.org_unit_id (+)
                  and stteo.set_of_books_id = teo_fog_fk.set_of_books_id (+)
            union all
               select pk_tsf_entity_org_unit_sob.rowid                     as pk_tsf_entity_org_unit_sob_rid,
                      pk_tsf_entity.rowid                                  as pk_tsf_entity_rid,
                      teo_fog_fk.rowid                                     as teo_fog_fk_rid,
                      teo_ogu_fk.rowid                                     as teo_ogu_fk_rid,
                      teo_tsn_fk.rowid                                     as teo_tsn_fk_rid,
                      sttsf.rowid                                          as sttsf_rowid,
                      stteo.rowid                                          as stteo_rowid,
                      sttsf.secondary_desc                                 as sttsf_secondary_desc,
                      pk_tsf_entity.secondary_desc                         as pk_tsf_entity_sec_desc,
                      NVL(sttsf.tsf_entity_id,pk_tsf_entity.tsf_entity_id) as sttsf_tsf_entity_id,
                      sttsf.tsf_entity_desc                                as sttsf_tsf_entity_desc,
                      pk_tsf_entity.tsf_entity_desc                        as pk_tsf_entity_desc,
                      sttsf.process_id                                     as sttsf_process_id,
                      sttsf.chunk_id                                       as sttsf_chunk_id,
                      stteo.chunk_id                                       as stteo_chunk_id,
                      sttsf.row_seq                                        as sttsf_row_seq,
                      UPPER(sttsf.action)                                  as sttsf_action,
                      sttsf.process$status                                 as sttsf_process$status,
                      stteo.set_of_books_id                                as stteo_set_of_books_id,
                      stteo.org_unit_id                                    as stteo_org_unit_id,
                      stteo.tsf_entity_id                                  as stteo_tsf_entity_id,
                      stteo.process_id                                     as stteo_process_id,
                      stteo.row_seq                                        as stteo_row_seq,
                      UPPER(stteo.action)                                  as stteo_action,
                      stteo.process$status                                 as stteo_process$status
                 from svc_tsf_entity_org_unit_sob stteo,
                      svc_tsf_entity sttsf,
                      tsf_entity pk_tsf_entity,
                      tsf_entity_org_unit_sob pk_tsf_entity_org_unit_sob,
                      fif_gL_setup teo_fog_fk,
                      org_unit teo_ogu_fk,
                      tsf_entity teo_tsn_fk
                where stteo.process_id      = I_process_id
                  and stteo.chunk_id        = I_chunk_id
                  and stteo.process_id      = sttsf.process_id(+)
                  and stteo.chunk_id        = sttsf.chunk_id(+)
                  and stteo.tsf_entity_id   = sttsf.tsf_entity_id(+)
                  and stteo.tsf_entity_id   = pk_tsf_entity.tsf_entity_id (+)
                  and stteo.org_unit_id     = pk_tsf_entity_org_unit_sob.org_unit_id (+)
                  and stteo.tsf_entity_id   = pk_tsf_entity_org_unit_sob.tsf_entity_id (+)
                  and stteo.tsf_entity_id   = teo_tsn_fk.tsf_entity_id (+)
                  and stteo.org_unit_id     = teo_ogu_fk.org_unit_id (+)
                  and stteo.set_of_books_id = teo_fog_fk.set_of_books_id (+)
                  and sttsf.tsf_entity_id is null ) t ;

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   Lp_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   Lp_s9t_errors_tab s9t_errors_tab_typ;

   Type TSF_ENTITY_TAB IS TABLE OF tsf_entity_tl%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
--------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR(I_file_id   IN   S9T_ERRORS.FILE_ID%TYPE,
                          I_sheet     IN   VARCHAR2,
                          I_row_seq   IN   NUMBER,
                          I_col       IN   VARCHAR2,
                          I_sqlcode   IN   NUMBER,
                          I_sqlerrm   IN   VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES (I_file_id   NUMBER) IS
   L_sheets             s9t_pkg.names_map_typ;
   TSF_ENTITY_cols      s9t_pkg.names_map_typ;
   tsf_entity_tl_cols   s9t_pkg.names_map_typ;
   TEO_cols             s9t_pkg.names_map_typ;
BEGIN
   L_sheets                      :=s9t_pkg.get_sheet_names(I_file_id);
   TSF_ENTITY_cols               :=s9t_pkg.get_coL_names(I_file_id,
                                                         TSF_ENTITY_sheet);
   TSF_ENTITY$Action             := TSF_ENTITY_cols('ACTION');
   TSF_ENTITY$SECONDARY_DESC     := TSF_ENTITY_cols('SECONDARY_DESC');
   TSF_ENTITY$tsf_entity_id      := TSF_ENTITY_cols('TSF_ENTITY_ID');
   TSF_ENTITY$TSF_ENTITY_DESC    := TSF_ENTITY_cols('TSF_ENTITY_DESC');
   TEO_cols                      :=s9t_pkg.get_coL_names(I_file_id,
                                                         TEO_sheet);
   TEO$Action                    := TEO_cols('ACTION');
   TEO$tsf_entity_id             := TEO_cols('TSF_ENTITY_ID');
   TEO$org_unit_id               := TEO_cols('ORG_UNIT_ID');
   TEO$set_of_books_id           := TEO_cols('SET_OF_BOOKS_ID');

   TSF_ENTITY_TL_COLS            :=s9t_pkg.get_coL_names(I_file_id,
                                                         TSF_ENTITY_TL_SHEET);
   TSF_ENTITY_TL$Action          := TSF_ENTITY_TL_COLS('ACTION');
   TSF_ENTITY_TL$lang            := TSF_ENTITY_TL_COLS('LANG');
   TSF_ENTITY_TL$tsf_entity_id   := TSF_ENTITY_TL_COLS('TSF_ENTITY_ID');
   TSF_ENTITY_TL$tsf_entity_desc := TSF_ENTITY_TL_COLS('TSF_ENTITY_DESC');
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_TSF_ENTITY(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = TSF_ENTITY_sheet)
               select s9t_row(s9t_cells(CORESVC_TSF_ENTITY.action_mod,
                                        TSF_ENTITY_ID,
                                        TSF_ENTITY_DESC,
                                        SECONDARY_DESC))
                 from TSF_ENTITY ;
END POPULATE_TSF_ENTITY;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_TSF_ENTITY_TL(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = TSF_ENTITY_TL_SHEET)
               select s9t_row(s9t_cells(CORESVC_TSF_ENTITY.action_mod,
                                        LANG,
                                        TSF_ENTITY_ID,
                                        TSF_ENTITY_DESC))
                 from tsf_entity_tl ;
END POPULATE_TSF_ENTITY_TL;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_TEO(I_file_id   IN   NUMBER)IS
BEGIN
   insert into TABLE (select ss.s9t_rows
                        from s9t_folder sf,
                             TABLE(sf.s9t_file_obj.sheets) ss
                       where sf.file_id = I_file_id
                         and ss.sheet_name = TEO_sheet)
               select s9t_row(s9t_cells(NULL,
                                        tsf_entity_id,
                                        org_unit_id,
                                        SET_OF_BOOKS_ID))
                 from TSF_ENTITY_ORG_UNIT_SOB;
END POPULATE_TEO;
--------------------------------------------------------------------------------
PROCEDURE INIT_S9T(O_file_id   IN OUT   NUMBER) IS

   L_file        s9t_file;
   L_file_name   S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(TSF_ENTITY_sheet);
   L_file.sheets(L_file.get_sheet_index(TSF_ENTITY_sheet)).column_headers := s9t_cells('ACTION',
                                                                                       'TSF_ENTITY_ID',
                                                                                       'TSF_ENTITY_DESC',
                                                                                       'SECONDARY_DESC');
   L_file.add_sheet(TEO_sheet);
   L_file.sheets(L_file.get_sheet_index(TEO_sheet)).column_headers := s9t_cells('ACTION',
                                                                                'TSF_ENTITY_ID',
                                                                                'ORG_UNIT_ID',
                                                                                'SET_OF_BOOKS_ID');

   L_file.add_sheet(TSF_ENTITY_TL_SHEET);
   L_file.sheets(L_file.get_sheet_index(TSF_ENTITY_TL_SHEET)).column_headers := s9t_cells('ACTION',
                                                                                          'LANG',
                                                                                          'TSF_ENTITY_ID',
                                                                                          'TSF_ENTITY_DESC');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
--------------------------------------------------------------------------------
FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                    I_template_only_ind   IN       CHAR DEFAULT 'N')
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TSF_ENTITY.CREATE_S9T';
   L_file      s9t_file;

BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_TSF_ENTITY(O_file_id);
      POPULATE_TEO(O_file_id);
      POPULATE_TSF_ENTITY_TL(O_file_id);
      Commit;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file := S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TSF_ENTITY(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                 I_process_id   IN   SVC_TSF_ENTITY.process_id%TYPE) IS

   TYPE svc_TSF_ENTITY_coL_typ IS TABLE OF SVC_TSF_ENTITY%ROWTYPE;
   L_temp_rec           SVC_TSF_ENTITY%ROWTYPE;
   svc_TSF_ENTITY_col   svc_TSF_ENTITY_coL_typ := NEW svc_TSF_ENTITY_coL_typ();
   L_process_id         SVC_TSF_ENTITY.process_id%TYPE;
   L_error              BOOLEAN := FALSE;
   L_default_rec       SVC_TSF_ENTITY%ROWTYPE;

   cursor c_mandatory_ind is
      select SECONDARY_DESC_mi,
             TSF_ENTITY_ID_mi,
             TSF_ENTITY_DESC_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_TSF_ENTITY.template_key
                 and wksht_key = 'TSF')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('SECONDARY_DESC' as SECONDARY_DESC,
                                       'TSF_ENTITY_ID' as TSF_ENTITY_ID,
                                       'TSF_ENTITY_DESC' as TSF_ENTITY_DESC,
                                       NULL as dummy));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_TSF_ENTITY';
   L_pk_columns    VARCHAR2(255)  := 'Transfer Entity ID';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
   -- Get default values.
   FOR rec IN (select SECONDARY_DESC_dv,
                      TSF_ENTITY_ID_dv,
                      TSF_ENTITY_DESC_dv,
                      NULL as dummy
                from (select column_key,
                             default_value
                        from s9t_tmpL_cols_def
                       where template_key  = CORESVC_TSF_ENTITY.template_key
                         and wksht_key = 'TSF')
                       PIVOT (MAX(default_value) as dv
                          FOR (column_key) IN ('SECONDARY_DESC'  as SECONDARY_DESC,
                                               'TSF_ENTITY_ID'   as tsf_entity_id,
                                               'TSF_ENTITY_DESC' as TSF_ENTITY_DESC,
                                               NULL              as dummy)))

   LOOP
      BEGIN
         L_default_rec.SECONDARY_DESC := rec.SECONDARY_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TSF_ENTITY',
                            NULL,
                            'SECONDARY_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tsf_entity_id := rec.TSF_ENTITY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TSF_ENTITY',
                            NULL,
                            'TSF_ENTITY_ID',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.TSF_ENTITY_DESC := rec.TSF_ENTITY_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'TSF_ENTITY',
                            NULL,
                            'TSF_ENTITY_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;

   --Get mandatory indicators
   open C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select r.get_cell(TSF_ENTITY$Action)           as Action,
                      r.get_cell(TSF_ENTITY$SECONDARY_DESC)   as SECONDARY_DESC,
                      r.get_cell(TSF_ENTITY$tsf_entity_id)    as tsf_entity_id,
                      r.get_cell(TSF_ENTITY$TSF_ENTITY_DESC)  as TSF_ENTITY_DESC,
                      r.get_row_seq()                         as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(TSF_ENTITY_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         TSF_ENTITY_sheet,
                         rec.row_seq,
                         action_column,
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SECONDARY_DESC := rec.SECONDARY_DESC;
      EXCEPTION
         when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         TSF_ENTITY_sheet,
                         rec.row_seq,
                         'SECONDARY_DESC',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tsf_entity_id := rec.tsf_entity_id;
      EXCEPTION
         when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         TSF_ENTITY_sheet,
                         rec.row_seq,
                         'TSF_ENTITY_ID',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TSF_ENTITY_DESC := rec.TSF_ENTITY_DESC;
      EXCEPTION
         when OTHERS then
         WRITE_S9T_ERROR(I_file_id,
                         TSF_ENTITY_sheet,
                         rec.row_seq,
                         'TSF_ENTITY_DESC',
                         SQLCODE,
                         SQLERRM);
         L_error := TRUE;
      END;
      if rec.action = CORESVC_TSF_ENTITY.action_new then
         L_temp_rec.SECONDARY_DESC  := NVL( L_temp_rec.SECONDARY_DESC,L_default_rec.SECONDARY_DESC);
         L_temp_rec.tsf_entity_id   := NVL( L_temp_rec.tsf_entity_id,L_default_rec.tsf_entity_id);
         L_temp_rec.TSF_ENTITY_DESC := NVL( L_temp_rec.TSF_ENTITY_DESC,L_default_rec.TSF_ENTITY_DESC);
      end if;
      if NOT (L_temp_rec.tsf_entity_id is NOT NULL and
              1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         TSF_ENTITY_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_TSF_ENTITY_col.extend();
         svc_TSF_ENTITY_col(svc_TSF_ENTITY_col.count()):=L_temp_rec;
      end if;

   END LOOP;

   BEGIN
      forall i IN 1..svc_TSF_ENTITY_col.count SAVE EXCEPTIONS
      merge into SVC_TSF_ENTITY st
      using (select (case
                     when L_mi_rec.SECONDARY_DESC_mi = 'N'
                      and svc_TSF_ENTITY_col(i).action = CORESVC_TSF_ENTITY.action_mod
                      and s1.SECONDARY_DESC is NULL then
                          mt.SECONDARY_DESC
                     else s1.SECONDARY_DESC
                     end) as SECONDARY_DESC,
                     (case
                     when L_mi_rec.TSF_ENTITY_ID_mi = 'N'
                      and svc_TSF_ENTITY_col(i).action = CORESVC_TSF_ENTITY.action_mod
                      and s1.tsf_entity_id is NULL then
                          mt.tsf_entity_id
                     else s1.tsf_entity_id
                     end) as tsf_entity_id,
                     (case
                     when L_mi_rec.TSF_ENTITY_DESC_mi = 'N'
                      and svc_TSF_ENTITY_col(i).action = CORESVC_TSF_ENTITY.action_mod
                      and s1.TSF_ENTITY_DESC is NULL then
                          mt.TSF_ENTITY_DESC
                     else s1.TSF_ENTITY_DESC
                     end) as TSF_ENTITY_DESC,
                     NULL as dummy
                from (select svc_TSF_ENTITY_col(i).SECONDARY_DESC as SECONDARY_DESC,
                             svc_TSF_ENTITY_col(i).tsf_entity_id as tsf_entity_id,
                             svc_TSF_ENTITY_col(i).TSF_ENTITY_DESC as TSF_ENTITY_DESC,
                             NULL as dummy
                        from dual) s1,
                             TSF_ENTITY mt
                       where mt.tsf_entity_id (+) = s1.tsf_entity_id
                         and 1 = 1) sq
                          ON (st.tsf_entity_id      = sq.tsf_entity_id and
                              svc_TSF_ENTITY_col(i).action in (CORESVC_TSF_ENTITY.action_mod,CORESVC_TSF_ENTITY.action_del))
      when matched then
      update
         set PROCESS_ID        = svc_TSF_ENTITY_col(i).PROCESS_ID ,
             CHUNK_ID          = svc_TSF_ENTITY_col(i).CHUNK_ID ,
             ROW_SEQ           = svc_TSF_ENTITY_col(i).ROW_SEQ ,
             action            = svc_TSF_ENTITY_col(i).ACTION,
             PROCESS$STATUS    = svc_TSF_ENTITY_col(i).PROCESS$STATUS ,
             SECONDARY_DESC    = sq.SECONDARY_DESC ,
             TSF_ENTITY_DESC   = sq.TSF_ENTITY_DESC ,
             CREATE_ID         = svc_TSF_ENTITY_col(i).CREATE_ID ,
             CREATE_DATETIME   = svc_TSF_ENTITY_col(i).CREATE_DATETIME ,
             LAST_UPD_ID       = svc_TSF_ENTITY_col(i).LAST_UPD_ID ,
             LAST_UPD_DATETIME = svc_TSF_ENTITY_col(i).LAST_UPD_DATETIME
      when NOT matched then
      insert (PROCESS_ID ,
              CHUNK_ID ,
              ROW_SEQ ,
              ACTION ,
              PROCESS$STATUS ,
              SECONDARY_DESC ,
              tsf_entity_id ,
              TSF_ENTITY_DESC ,
              CREATE_ID ,
              CREATE_DATETIME ,
              LAST_UPD_ID ,
              LAST_UPD_DATETIME)
      values (svc_TSF_ENTITY_col(i).PROCESS_ID ,
              svc_TSF_ENTITY_col(i).CHUNK_ID ,
              svc_TSF_ENTITY_col(i).ROW_SEQ ,
              svc_TSF_ENTITY_col(i).ACTION ,
              svc_TSF_ENTITY_col(i).PROCESS$STATUS ,
              sq.SECONDARY_DESC ,
              sq.tsf_entity_id ,
              sq.TSF_ENTITY_DESC ,
              svc_TSF_ENTITY_col(i).CREATE_ID ,
              svc_TSF_ENTITY_col(i).CREATE_DATETIME ,
              svc_TSF_ENTITY_col(i).LAST_UPD_ID ,
              svc_TSF_ENTITY_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_errorS then
      FOR i IN 1..sql%bulk_EXCEPTIONs.count
         LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          TSF_ENTITY_sheet,
                          svc_TSF_ENTITY_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TSF_ENTITY;
----------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_tsf_entity_tl(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id   IN   SVC_tsf_entity_tl.PROCESS_ID%TYPE) IS

   TYPE svc_tsf_entity_tl_col_TYP IS TABLE OF SVC_tsf_entity_tl%ROWTYPE;
   L_temp_rec              SVC_tsf_entity_tl%ROWTYPE;
   svc_tsf_entity_tl_col   svc_tsf_entity_tl_col_TYP := NEW svc_tsf_entity_tl_col_TYP();
   L_process_id            SVC_tsf_entity_tl.PROCESS_ID%TYPE;
   L_error                 BOOLEAN := FALSE;
   L_default_rec           SVC_tsf_entity_tl%ROWTYPE;

   cursor C_MANDATORY_IND is
      select tsf_entity_desc_mi,
             tsf_entity_id_mi,
             lang_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key = template_key
                 and wksht_key    = 'TSF_ENTITY_TL')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('TSF_ENTITY_DESC' as tsf_entity_desc,
                                       'TSF_ENTITY_ID'   as tsf_entity_id,
                                       'LANG'            as lang));

   L_mi_rec C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS EXCEPTION;
   PRAGMA EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_tsf_entity_tl';
   L_pk_columns    VARCHAR2(255)  := 'Transfer Entity ID, Lang';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
  -- Get default values.
   FOR rec IN (select tsf_entity_desc_dv,
                      tsf_entity_id_dv,
                      lang_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key = template_key
                          and wksht_key    = 'TSF_ENTITY_TL')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('TSF_ENTITY_DESC' as tsf_entity_desc,
                                                'TSF_ENTITY_ID'   as tsf_entity_id,
                                                'LANG'            as lang)))
   LOOP
      BEGIN
         L_default_rec.tsf_entity_desc := rec.tsf_entity_desc_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSF_ENTITY_TL_SHEET ,
                            NULL,
                           'TSF_ENTITY_DESC' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.tsf_entity_id := rec.tsf_entity_id_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           TSF_ENTITY_TL_SHEET ,
                            NULL,
                           'TSF_ENTITY_ID' ,
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.lang := rec.lang_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSF_ENTITY_TL_SHEET ,
                            NULL,
                           'LANG' ,
                            NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.get_cell(tsf_entity_tl$action))     as action,
                      r.get_cell(tsf_entity_tl$tsf_entity_desc)   as tsf_entity_desc,
                      r.get_cell(tsf_entity_tl$tsf_entity_id)     as tsf_entity_id,
                      r.get_cell(tsf_entity_tl$lang)              as lang,
                      r.get_row_seq()                             as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = sheet_name_trans(TSF_ENTITY_TL_SHEET))
   LOOP
      L_temp_rec := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.action := rec.action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            tsf_entity_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tsf_entity_desc := rec.tsf_entity_desc;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSF_ENTITY_TL_SHEET,
                            rec.row_seq,
                            'TSF_ENTITY_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tsf_entity_id := rec.tsf_entity_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSF_ENTITY_TL_SHEET,
                            rec.row_seq,
                            'TSF_ENTITY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TSF_ENTITY_TL_SHEET,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TSF_ENTITY.action_new then
         L_temp_rec.tsf_entity_desc := NVL( L_temp_rec.tsf_entity_desc,L_default_rec.tsf_entity_desc);
         L_temp_rec.tsf_entity_id   := NVL( L_temp_rec.tsf_entity_id,L_default_rec.tsf_entity_id);
         L_temp_rec.lang            := NVL( L_temp_rec.lang,L_default_rec.lang);
      end if;
      if NOT (L_temp_rec.tsf_entity_id is NOT NULL and L_temp_rec.lang is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         TSF_ENTITY_TL_SHEET,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_tsf_entity_tl_col.extend();
         svc_tsf_entity_tl_col(svc_tsf_entity_tl_col.COUNT()) := L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_tsf_entity_tl_col.COUNT SAVE EXCEPTIONS
      merge into SVC_tsf_entity_tl st
      using(select
                  (case
                   when l_mi_rec.tsf_entity_desc_mi = 'N'
                    and svc_tsf_entity_tl_col(i).action = CORESVC_TSF_ENTITY.action_mod
                    and s1.tsf_entity_desc IS NULL then
                        mt.tsf_entity_desc
                   else s1.tsf_entity_desc
                   end) as tsf_entity_desc,
                  (case
                   when l_mi_rec.lang_mi = 'N'
                    and svc_tsf_entity_tl_col(i).action = CORESVC_TSF_ENTITY.action_mod
                    and s1.lang IS NULL then
                        mt.lang
                   else s1.lang
                   end) as lang,
                  (case
                   when l_mi_rec.tsf_entity_id_mi = 'N'
                    and svc_tsf_entity_tl_col(i).action = CORESVC_TSF_ENTITY.action_mod
                    and s1.tsf_entity_id IS NULL then
                        mt.tsf_entity_id
                   else s1.tsf_entity_id
                   end) as tsf_entity_id
              from (select svc_tsf_entity_tl_col(i).tsf_entity_desc as tsf_entity_desc,
                           svc_tsf_entity_tl_col(i).tsf_entity_id        as tsf_entity_id,
                           svc_tsf_entity_tl_col(i).lang              as lang
                      from dual) s1,
                   tsf_entity_tl mt
             where mt.tsf_entity_id (+) = s1.tsf_entity_id
               and mt.lang (+)       = s1.lang) sq
                on (st.tsf_entity_id = sq.tsf_entity_id and
                    st.lang = sq.lang and
                    svc_tsf_entity_tl_col(i).action IN (CORESVC_TSF_ENTITY.action_mod,CORESVC_TSF_ENTITY.action_del))
      when matched then
      update
         set process_id        = svc_tsf_entity_tl_col(i).process_id ,
             chunk_id          = svc_tsf_entity_tl_col(i).chunk_id ,
             row_seq           = svc_tsf_entity_tl_col(i).row_seq ,
             action            = svc_tsf_entity_tl_col(i).action ,
             process$status    = svc_tsf_entity_tl_col(i).process$status ,
             tsf_entity_desc = sq.tsf_entity_desc
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             tsf_entity_desc ,
             tsf_entity_id ,
             lang)
      values(svc_tsf_entity_tl_col(i).process_id ,
             svc_tsf_entity_tl_col(i).chunk_id ,
             svc_tsf_entity_tl_col(i).row_seq ,
             svc_tsf_entity_tl_col(i).action ,
             svc_tsf_entity_tl_col(i).process$status ,
             sq.tsf_entity_desc ,
             sq.tsf_entity_id ,
             sq.lang);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;

            WRITE_S9T_ERROR( I_file_id,
                            TSF_ENTITY_TL_SHEET,
                            svc_tsf_entity_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_tsf_entity_tl;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TEO(I_file_id      IN   S9T_FOLDER.FILE_ID%TYPE,
                          I_process_id   IN   SVC_TSF_ENTITY_ORG_UNIT_SOB.process_id%type) IS

   Type svc_TEO_coL_typ IS TABLE OF SVC_TSF_ENTITY_ORG_UNIT_SOB%rowtype;
   L_temp_rec      SVC_TSF_ENTITY_ORG_UNIT_SOB%rowtype;
   svc_TEO_col     svc_TEO_coL_typ := NEW svc_TEO_coL_typ();
   L_process_id    SVC_TSF_ENTITY_ORG_UNIT_SOB.process_id%type;
   L_error         BOOLEAN := FALSE;
   L_default_rec   SVC_TSF_ENTITY_ORG_UNIT_SOB%rowtype;

   cursor c_mandatory_ind IS
      select SET_OF_BOOKS_ID_mi,
             ORG_UNIT_ID_mi,
             TSF_ENTITY_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpL_cols_def
               where template_key  = CORESVC_TSF_ENTITY.template_key
                 and wksht_key = 'TEO')
               PIVOT (MAX(mandatory) as mi
                  FOR (column_key) IN ('SET_OF_BOOKS_ID' as SET_OF_BOOKS_ID,
                                       'ORG_UNIT_ID' as org_unit_id,
                                       'TSF_ENTITY_ID' as tsf_entity_id,
                                       null as dummy));

   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   DML_ERRORS      EXCEPTION;
   PRAGMA          EXCEPTION_INIT(DML_ERRORS, -24381);
   L_table         VARCHAR2(30)   := 'SVC_TSF_ENTITY_ORG_UNIT_SOB';
   L_pk_columns    VARCHAR2(255)  := 'Transfer Entity ID,Org Unit Id';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;

BEGIN
  -- Get default values.
   FOR rec IN (select SET_OF_BOOKS_ID_dv,
                      ORG_UNIT_ID_dv,
                      TSF_ENTITY_ID_dv,
                      null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpL_cols_def
                        where template_key  = CORESVC_TSF_ENTITY.template_key
                          and wksht_key     = 'TEO')
                        PIVOT (MAX(default_value) as dv
                           FOR (column_key) IN ('SET_OF_BOOKS_ID' as SET_OF_BOOKS_ID,
                                                'ORG_UNIT_ID' as org_unit_id,
                                                'TSF_ENTITY_ID' as tsf_entity_id,
                                                NULL as dummy)))

   LOOP
      BEGIN
         L_default_rec.SET_OF_BOOKS_ID := rec.SET_OF_BOOKS_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TSF_ENTITY_ORG_UNIT_SOB',
                           NULL,
                           'SET_OF_BOOKS_ID',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.org_unit_id := rec.ORG_UNIT_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TSF_ENTITY_ORG_UNIT_SOB',
                           NULL,
                           'ORG_UNIT_ID',
                           NULL,
                           'INV_DEFAULT');
      END;

      BEGIN
         L_default_rec.tsf_entity_id := rec.TSF_ENTITY_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TSF_ENTITY_ORG_UNIT_SOB',
                           NULL,
                           'TSF_ENTITY_ID',
                           NULL,
                           'INV_DEFAULT');
      END;
   END LOOP;

 --Get mandatory indicators
   open C_mandatory_ind;
   fetch C_mandatory_ind into L_mi_rec;
   close C_mandatory_ind;
   FOR rec IN(select r.get_cell(TEO$Action)            as Action,
                     r.get_cell(TEO$SET_OF_BOOKS_ID)   as SET_OF_BOOKS_ID,
                     r.get_cell(TEO$org_unit_id)       as org_unit_id,
                     r.get_cell(TEO$tsf_entity_id)     as tsf_entity_id,
                     r.get_row_seq()                   as row_seq
                from s9t_folder sf,
                     TABLE(sf.s9t_file_obj.sheets) ss,
                     TABLE(ss.s9t_rows) r
               where sf.file_id  = I_file_id
                 and ss.sheet_name = sheet_name_trans(TEO_sheet))
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := sysdate;
      L_temp_rec.last_upd_datetime := sysdate;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TEO_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SET_OF_BOOKS_ID := rec.SET_OF_BOOKS_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TEO_sheet,
                            rec.row_seq,
                            'SET_OF_BOOKS_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.org_unit_id := rec.org_unit_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TEO_sheet,
                            rec.row_seq,
                            'ORG_UNIT_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.tsf_entity_id := rec.tsf_entity_id;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TEO_sheet,
                            rec.row_seq,
                            'TSF_ENTITY_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_TSF_ENTITY.action_new then
         L_temp_rec.SET_OF_BOOKS_ID := NVL( L_temp_rec.SET_OF_BOOKS_ID,L_default_rec.SET_OF_BOOKS_ID);
         L_temp_rec.org_unit_id     := NVL( L_temp_rec.org_unit_id,L_default_rec.org_unit_id);
         L_temp_rec.tsf_entity_id   := NVL( L_temp_rec.tsf_entity_id,L_default_rec.tsf_entity_id);
      end if;
      if NOT (L_temp_rec.org_unit_id is not null
         and  L_temp_rec.tsf_entity_id is not null
         and  1 = 1) then
         WRITE_S9T_ERROR(I_file_id,
                         TEO_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_TEO_col.extend();
         svc_TEO_col(svc_TEO_col.count()):=L_temp_rec;
      end if;

   END LOOP;

   BEGIN
      forall i IN 1..svc_TEO_col.count SAVE EXCEPTIONS
      merge into SVC_TSF_ENTITY_ORG_UNIT_SOB st
      using (select (case
                     when L_mi_rec.SET_OF_BOOKS_ID_mi = 'N'
                      and svc_TEO_col(i).action = CORESVC_TSF_ENTITY.action_mod
                      and s1.SET_OF_BOOKS_ID IS NULL then
                          mt.SET_OF_BOOKS_ID
                     else s1.SET_OF_BOOKS_ID
                     end) as SET_OF_BOOKS_ID,
                    (case
                     when L_mi_rec.ORG_UNIT_ID_mi = 'N'
                      and svc_TEO_col(i).action = CORESVC_TSF_ENTITY.action_mod
                      and s1.org_unit_id IS NULL  then
                          mt.org_unit_id
                     else s1.org_unit_id
                     end) as org_unit_id,
                    (case
                     when L_mi_rec.TSF_ENTITY_ID_mi = 'N'
                      and svc_TEO_col(i).action = CORESVC_TSF_ENTITY.action_mod
                      and s1.tsf_entity_id IS NULL then
                          mt.tsf_entity_id
                     else s1.tsf_entity_id
                     end) as tsf_entity_id,
                     null as dummy
               from (select svc_TEO_col(i).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                            svc_TEO_col(i).org_unit_id     as org_unit_id,
                            svc_TEO_col(i).tsf_entity_id   as tsf_entity_id,
                            null as dummy
                       from dual) s1,
                            TSF_ENTITY_ORG_UNIT_SOB mt
                      where mt.org_unit_id (+)   = s1.org_unit_id
                        and mt.tsf_entity_id (+) = s1.tsf_entity_id
                        and 1 = 1) sq
                         ON (st.org_unit_id        = sq.org_unit_id and
                             st.tsf_entity_id      = sq.tsf_entity_id and
                             svc_TEO_col(i).ACTION <> CORESVC_TSF_ENTITY.action_new)
      when matched then
      update
         set PROCESS_ID      = svc_TEO_col(i).PROCESS_ID ,
             CHUNK_ID          = svc_TEO_col(i).CHUNK_ID ,
             ROW_SEQ           = svc_TEO_col(i).ROW_SEQ ,
             PROCESS$STATUS    = svc_TEO_col(i).PROCESS$STATUS ,
             SET_OF_BOOKS_ID   = sq.SET_OF_BOOKS_ID ,
             CREATE_ID         = svc_TEO_col(i).CREATE_ID ,
             CREATE_DATETIME   = svc_TEO_col(i).CREATE_DATETIME ,
             LAST_UPD_ID       = svc_TEO_col(i).LAST_UPD_ID ,
             LAST_UPD_DATETIME = svc_TEO_col(i).LAST_UPD_DATETIME
      when NOT matched then
      insert (PROCESS_ID ,
             CHUNK_ID ,
             ROW_SEQ ,
             ACTION ,
             PROCESS$STATUS ,
             SET_OF_BOOKS_ID ,
             org_unit_id ,
             tsf_entity_id ,
             CREATE_ID ,
             CREATE_DATETIME ,
             LAST_UPD_ID ,
             LAST_UPD_DATETIME)
      values (svc_TEO_col(i).PROCESS_ID ,
             svc_TEO_col(i).CHUNK_ID ,
             svc_TEO_col(i).ROW_SEQ ,
             svc_TEO_col(i).ACTION ,
             svc_TEO_col(i).PROCESS$STATUS ,
             sq.SET_OF_BOOKS_ID ,
             sq.org_unit_id ,
             sq.tsf_entity_id ,
             svc_TEO_col(i).CREATE_ID ,
             svc_TEO_col(i).CREATE_DATETIME ,
             svc_TEO_col(i).LAST_UPD_ID ,
             svc_TEO_col(i).LAST_UPD_DATETIME);
      EXCEPTION
         when DML_ERRORS then
            FOR i IN 1..sql%bulk_exceptions.count
            LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             TEO_sheet,
                             svc_TEO_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
            END LOOP;
      END;
END PROCESS_S9T_TEO;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_error_count     IN OUT   NUMBER,
                     I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_TSF_ENTITY.PROCESS_S9T';
   L_file             S9T_FILE;
   L_sheets           S9T_PKG.names_map_typ;
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT     EXCEPTION;
   PRAGMA             EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	      EXCEPTION;
   PRAGMA	      EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   S9T_PKG.ODS2OBJ(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);

   if S9T_PKG.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := S9T_PKG.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_TSF_ENTITY(I_file_id,
                              I_process_id);
      PROCESS_S9T_TEO(I_file_id,
                      I_process_id);
      PROCESS_S9T_TSF_ENTITY_TL(I_file_id,
                                I_process_id);
   end if;

   O_error_count := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert into s9t_errors
           values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   --Update process$status in svc_process_tracker
   if O_error_count = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;

   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
              values LP_s9t_errors_tab(i);

      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      COMMIT;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------
FUNCTION EXEC_TSF_ENTITY_INS(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_entity_temp_rec   IN       TSF_ENTITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TSF_ENTITY.EXEC_TSF_ENTITY_INS';

BEGIN
   insert into TSF_ENTITY
        values I_tsf_entity_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TSF_ENTITY_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_TSF_ENTITY_UPD(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_entity_temp_rec   IN       TSF_ENTITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TSF_ENTITY.EXEC_TSF_ENTITY_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_TSF_ENTITY_UPD is
      select 'x'
        from TSF_ENTITY
       where tsf_entity_id = I_tsf_entity_temp_rec.tsf_entity_id
         for update nowait;

BEGIN
   open  C_LOCK_TSF_ENTITY_UPD;
   close C_LOCK_TSF_ENTITY_UPD;

   update TSF_ENTITY
      set row = I_tsf_entity_temp_rec
    where 1 = 1
      and tsf_entity_id = I_tsf_entity_temp_rec.tsf_entity_id;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_tsf_entity_temp_rec.tsf_entity_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_TSF_ENTITY_UPD%ISOPEN then
         close C_LOCK_TSF_ENTITY_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TSF_ENTITY_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_TSF_ENTITY_DEL(O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_tsf_entity_temp_rec   IN       TSF_ENTITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TSF_ENTITY.EXEC_TSF_ENTITY_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_TSF_ENTITY_TL_DEL is
      select 'x'
        from tsf_entity_tl
       where tsf_entity_id = I_tsf_entity_temp_rec.tsf_entity_id
         for update nowait;

    --Cursor to lock the record
   cursor C_LOCK_TSF_ENTITY_DEL is
      select 'x'
        from TSF_ENTITY
       where tsf_entity_id = I_tsf_entity_temp_rec.tsf_entity_id
         for update nowait;

BEGIN
   open  C_LOCK_TSF_ENTITY_TL_DEL;
   close C_LOCK_TSF_ENTITY_TL_DEL;

   delete from tsf_entity_tl
         where tsf_entity_id = I_tsf_entity_temp_rec.tsf_entity_id;

   open  C_LOCK_TSF_ENTITY_DEL;
   close C_LOCK_TSF_ENTITY_DEL;

   delete from TSF_ENTITY
         where tsf_entity_id = I_tsf_entity_temp_rec.tsf_entity_id;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_tsf_entity_temp_rec.tsf_entity_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_TSF_ENTITY_TL_DEL%ISOPEN then
         close C_LOCK_TSF_ENTITY_TL_DEL;
      end if;
      if C_LOCK_TSF_ENTITY_DEL%ISOPEN then
         close C_LOCK_TSF_ENTITY_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TSF_ENTITY_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSF_ENTITY_TL_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tsf_entity_tl_ins_tab    IN       TSF_ENTITY_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TSF_ENTITY.EXEC_TSF_ENTITY_TL_INS';

BEGIN
   if I_tsf_entity_tl_ins_tab is NOT NULL and I_tsf_entity_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_tsf_entity_tl_ins_tab.COUNT()
         insert into tsf_entity_tl
              values I_tsf_entity_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_TSF_ENTITY_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSF_ENTITY_TL_UPD(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tsf_entity_tl_upd_tab   IN       TSF_ENTITY_TAB,
                                I_tsf_entity_tl_upd_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_TSF_ENTITY_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_TSF_ENTITY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TSF_ENTITY.EXEC_TSF_ENTITY_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TSF_ENTITY_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_TSF_ENTITY_TL_UPD(I_tsf_entity  TSF_ENTITY_TL.TSF_ENTITY_ID%TYPE,
                                   I_lang        TSF_ENTITY_TL.LANG%TYPE) is
      select 'x'
        from tsf_entity_tl
       where tsf_entity_id = I_tsf_entity
         and lang = I_lang
         for update nowait;

BEGIN
   if I_tsf_entity_tl_upd_tab is NOT NULL and I_tsf_entity_tl_upd_tab.count > 0 then
      for i in I_tsf_entity_tl_upd_tab.FIRST..I_tsf_entity_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_tsf_entity_tl_upd_tab(i).lang);
            L_key_val2 := 'Transfer Entity ID: '||to_char(I_tsf_entity_tl_upd_tab(i).tsf_entity_id);
            open C_LOCK_TSF_ENTITY_TL_UPD(I_tsf_entity_tl_upd_tab(i).tsf_entity_id,
                                          I_tsf_entity_tl_upd_tab(i).lang);
            close C_LOCK_TSF_ENTITY_TL_UPD;
            
            update tsf_entity_tl
               set tsf_entity_desc = I_tsf_entity_tl_upd_tab(i).tsf_entity_desc,
                   last_update_id = I_tsf_entity_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_tsf_entity_tl_upd_tab(i).last_update_datetime
             where lang = I_tsf_entity_tl_upd_tab(i).lang
               and tsf_entity_id = I_tsf_entity_tl_upd_tab(i).tsf_entity_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_TSF_ENTITY_TL',
                           I_tsf_entity_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_TSF_ENTITY_TL_UPD%ISOPEN then
         close C_LOCK_TSF_ENTITY_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TSF_ENTITY_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_TSF_ENTITY_TL_DEL(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_tsf_entity_tl_del_tab   IN       TSF_ENTITY_TAB,
                                I_tsf_entity_tl_del_rst   IN       ROW_SEQ_TAB,
                                I_process_id              IN       SVC_TSF_ENTITY_TL.PROCESS_ID%TYPE,
                                I_chunk_id                IN       SVC_TSF_ENTITY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TSF_ENTITY.EXEC_TSF_ENTITY_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TSF_ENTITY_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(30) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_TSF_ENTITY_TL_DEL(I_tsf_entity_id  TSF_ENTITY_TL.tsf_entity_id%TYPE,
                                   I_lang           TSF_ENTITY_TL.LANG%TYPE) is
      select 'x'
        from tsf_entity_tl
       where tsf_entity_id = I_tsf_entity_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_tsf_entity_tl_del_tab is NOT NULL and I_tsf_entity_tl_del_tab.count > 0 then
      for i in I_tsf_entity_tl_del_tab.FIRST..I_tsf_entity_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_tsf_entity_tl_del_tab(i).lang);
            L_key_val2 := 'Transfer Entity ID: '||to_char(I_tsf_entity_tl_del_tab(i).tsf_entity_id);
            open C_LOCK_TSF_ENTITY_TL_DEL(I_tsf_entity_tl_del_tab(i).tsf_entity_id,
                                          I_tsf_entity_tl_del_tab(i).lang);
            close C_LOCK_TSF_ENTITY_TL_DEL;
            
            delete tsf_entity_tl
             where lang = I_tsf_entity_tl_del_tab(i).lang
               and tsf_entity_id = I_tsf_entity_tl_del_tab(i).tsf_entity_id;

         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_TSF_ENTITY_TL',
                           I_tsf_entity_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_TSF_ENTITY_TL_DEL%ISOPEN then
         close C_LOCK_TSF_ENTITY_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TSF_ENTITY_TL_DEL;
--------------------------------------------------------------------------------
FUNCTION EXEC_TEO_INS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_teo_temp_rec    IN       TSF_ENTITY_ORG_UNIT_SOB%ROWTYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_TSF_ENTITY.EXEC_TEO_INS';

BEGIN
   insert into TSF_ENTITY_ORG_UNIT_SOB
        values I_teo_temp_rec;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TEO_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_TEO_DEL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_teo_temp_rec    IN       TSF_ENTITY_ORG_UNIT_SOB%ROWTYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_TSF_ENTITY.EXEC_TEO_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY_ORG_UNIT_SOB';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

    --Cursor to lock the record
   cursor C_LOCK_TEO_DEL is
      select 'x'
        from TSF_ENTITY_ORG_UNIT_SOB
       where org_unit_id = I_teo_temp_rec.org_unit_id
         and tsf_entity_id = I_teo_temp_rec.tsf_entity_id
         for update nowait;

BEGIN
   open  C_LOCK_TEO_DEL;
   close C_LOCK_TEO_DEL;

   delete from TSF_ENTITY_ORG_UNIT_SOB
    where 1 = 1
      and org_unit_id = I_teo_temp_rec.org_unit_id
      and tsf_entity_id = I_teo_temp_rec.tsf_entity_id;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_teo_temp_rec.tsf_entity_id,
                                             I_teo_temp_rec.org_unit_id);
      return FALSE;
   when OTHERS then
      if C_LOCK_TEO_DEL%ISOPEN then
         close C_LOCK_TEO_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_TEO_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_VAL_TSF_ENTITY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error           IN OUT   BOOLEAN,
                                L_deL_ok          IN OUT   BOOLEAN,
                                I_rec             IN       C_SVC_TSF_ENTITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64) := 'CORESVC_TSF_ENTITY.PROCESS_VAL_TSF_ENTITY';
   L_table_tsf   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY';
   --L_deL_ok      BOOLEAN := FALSE;

BEGIN
   if I_rec.sttsf_action = action_del
      and (I_rec.stteo_action is NULL or I_rec.stteo_action = action_new)
      and I_rec.PK_TSF_ENTITY_rid is NOT NULL then
      WRITE_ERROR(I_rec.sttsf_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.nextval,
                  I_rec.sttsf_chunk_id,
                  L_table_tsf,
                  I_rec.sttsf_row_seq,
                  'TSF_ENTITY_ID',
                  'CANNOT_DEL_MASTER_REC');
      O_error :=TRUE;
   end if;

   if I_rec.sttsf_action = action_new
      and I_rec.sttsf_tsf_entity_id < 1 then
      WRITE_ERROR(I_rec.sttsf_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.nextval,
                  I_rec.sttsf_chunk_id,
                  L_table_tsf,
                  I_rec.sttsf_row_seq,
                  'TSF_ENTITY_ID',
                  'GREATER_0');
      O_error :=TRUE;
   end if;

   if I_rec.sttsf_action = action_new
      and (I_rec.stteo_action = action_del or I_rec.stteo_action IS NULL) then
  	   WRITE_ERROR(I_rec.sttsf_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.nextval,
                  I_rec.sttsf_chunk_id,
                  L_table_tsf,
                  I_rec.sttsf_row_seq,
                  'ACTION',
                  'INV_ACT_COMB');
      O_error :=TRUE;
   end if;
   if I_rec.sttsf_action = action_del then
      if TSF_ENTITY_SQL.CHECK_DELETE(O_error_message,
                                     L_del_ok,
                                     I_rec.sttsf_tsf_entity_id)= FALSE then
         WRITE_ERROR(I_rec.sttsf_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.sttsf_chunk_id,
                     L_table_tsf,
                     I_rec.sttsf_row_seq,
                     'TSF_ENTITY_ID',
                     O_error_message);
         O_error :=TRUE;
      end if;
      if NOT L_del_ok then
         WRITE_ERROR(I_rec.sttsf_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.sttsf_chunk_id,
                     L_table_tsf,
                     I_rec.sttsf_row_seq,
                     'TSF_ENTITY_ID',
                     'ENTITY_ASSOC_LOC');
         O_error :=TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_VAL_TSF_ENTITY;
--------------------------------------------------------------------------------
FUNCTION PROCESS_TSF_ENTITY_TEO_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error           IN OUT   BOOLEAN,
                                    I_rec             IN       C_SVC_TSF_ENTITY%ROWTYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(64) := 'CORESVC_TSF_ENTITY.PROCESS_TSF_ENTITY_TEO_VAL';
   L_table_tsf       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY';
   L_table_teo       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY_ORG_UNIT_SOB';
   L_org_unit_row    ORG_UNIT%ROWTYPE;
   L_count_org_teo   NUMBER;

   cursor C_ORG_COUNT(I_org TSF_ENTITY_ORG_UNIT_SOB.org_unit_id%TYPE) is
      select count(TSF_ENTITY_ORG_UNIT_SOB.org_unit_id) into L_count_org_teo
        from TSF_ENTITY_ORG_UNIT_SOB
       where org_unit_id = I_org;

BEGIN
      if I_rec.stteo_action = action_new
         and I_rec.TEO_OGU_FK_rid is NOT NULL
         and I_rec.TEO_TSN_FK_rid is NOT NULL
         and I_rec.TEO_FOG_FK_rid is NOT NULL
         and I_rec.stteo_org_unit_id is NOT NULL then
         if ORG_UNIT_SQL.GET_ORG_UNIT(O_error_message,
                                      L_org_unit_row,
                                      I_rec.stteo_org_unit_id) = FALSE then
            WRITE_ERROR(I_rec.stteo_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_rec.stteo_chunk_id,
                        L_table_teo,
                        I_rec.stteo_row_seq,
                        'ORG_UNIT_ID',
                        O_error_message);
            O_error :=TRUE;
         end if;
         if I_rec.stteo_set_of_books_id != L_org_unit_row.set_of_books_id then
            WRITE_ERROR(I_rec.stteo_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_rec.stteo_chunk_id,
                        L_table_teo,
                        I_rec.stteo_row_seq,
                        'Set Of Books Id,Org Unit Id',
                        'ORG_UNIT_SOB_NOT_ASSOC');
            O_error :=TRUE;
         end if;
      end if;

   if I_rec.sttsf_action = action_new
      and (I_rec.stteo_action = action_del
      or (I_rec.stteo_action IS NULL and I_rec.pk_tsf_entity_org_unit_sob_rid IS NOT NULL)) then
         WRITE_ERROR(I_rec.stteo_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.stteo_chunk_id,
                     L_table_teo,
                     I_rec.stteo_row_seq,
                     'ACTION',
                     'INV_ACT_COMB');

      O_error :=TRUE;
   end if;

   if I_rec.stteo_action = action_new
      and I_rec.stteo_tsf_entity_id < 1 then
      WRITE_ERROR(I_rec.stteo_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.nextval,
                  I_rec.stteo_chunk_id,
                  L_table_teo,
                  I_rec.stteo_row_seq,
                  'TSF_ENTITY_ID',
                  'GREATER_0');
      O_error :=TRUE;
   end if;

   if I_rec.stteo_action = action_new then
      open C_ORG_COUNT(I_rec.stteo_org_unit_id);
      fetch C_ORG_COUNT into L_count_org_teo;
      close C_ORG_COUNT;
      if L_count_org_teo > 0 then
         WRITE_ERROR(I_rec.stteo_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_rec.stteo_chunk_id,
                     L_table_teo,
                     I_rec.stteo_row_seq,
                     'ORG_UNIT_ID',
                     'DUP_ORG_UNIT_ENTITY');
         O_error :=TRUE;
      end if;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_ORG_COUNT%ISOPEN then
         close C_ORG_COUNT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS_TSF_ENTITY_TEO_VAL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_TSF_ENTITY(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_process_id      IN       SVC_TSF_ENTITY.PROCESS_ID%TYPE,
                            I_chunk_id        IN       SVC_TSF_ENTITY.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program               VARCHAR2(64) := 'CORESVC_TSF_ENTITY.PROCESS_TSF_ENTITY';
   L_table_tsf             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY';
   L_table_teo             SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY_ORG_UNIT_SOB';
   L_deL_ok                BOOLEAN;
   L_tsf_process_error     BOOLEAN := FALSE;
   L_teo_process_error     BOOLEAN := FALSE;
   L_count_teo             NUMBER;
   L_tsf_entity_temp_rec   TSF_ENTITY%ROWTYPE;
   L_teo_temp_rec          TSF_ENTITY_ORG_UNIT_SOB%ROWTYPE;
   L_teo_error             BOOLEAN ;
   L_error                 BOOLEAN;
   L_rec_exists            BOOLEAN;
   L_rowseq_count               NUMBER;
   c                            NUMBER      := 1;

   TYPE L_row_seq_tab_type IS TABLE OF SVC_UDA_VALUES.ROW_SEQ%TYPE;
   L_row_seq_tab                       L_row_seq_tab_type;

   cursor C_TEO_COUNT(I_entity TSF_ENTITY.tsf_entity_id%TYPE) is
      select count(TSF_ENTITY_ORG_UNIT_SOB.org_unit_id) into L_count_teo
        from TSF_ENTITY_ORG_UNIT_SOB
       where tsf_entity_id = I_entity;

BEGIN
   L_row_seq_tab := L_row_seq_tab_type();
   FOR rec IN C_SVC_TSF_ENTITY(I_process_id,
                               I_chunk_id)
   LOOP
      L_teo_error  := FALSE;
      L_rec_exists := FALSE;

      if rec.tsf_rank = 1 then
         L_error := FALSE;
         L_deL_ok := FALSE;
         L_row_seq_tab.DELETE;
         c:=1;
-- Validating TSF_ENTITY RECORDS

         if rec.sttsf_action NOT IN (action_new,action_mod,action_del)
            or (rec.sttsf_action is NULL and rec.sttsf_rowid is NOT NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_tsf,
                        rec.sttsf_row_seq,
                        'ACTION',
                        'INV_ACT');
            L_error :=TRUE;
         end if;
         if rec.sttsf_action = action_new
            and rec.PK_TSF_ENTITY_rid is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_tsf,
                        rec.sttsf_row_seq,
                        'TSF_ENTITY_ID',
                        'DUP_TSF_ENTITY');
            L_error :=TRUE;
         end if;
         if rec.sttsf_action IN (action_mod,action_del)
            and rec.PK_TSF_ENTITY_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_tsf,
                        rec.sttsf_row_seq,
                        'TSF_ENTITY_ID',
                        'NO_TSF_ENTITY_FOUND');
            L_error :=TRUE;
         end if;
         if rec.sttsf_action IN (action_new,action_mod)
            and rec.sttsf_tsf_entity_desc is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_tsf,
                        rec.sttsf_row_seq,
                        'TSF_ENTITY_DESC',
                        'TSF_ENTITY_DESC_REQ');
            L_error :=TRUE;
         end if;

-- Validating TSF_ENTITY RECORDS - BUSINESS LOGIC
         if PROCESS_VAL_TSF_ENTITY(O_error_message,
                                   L_error,
                                   L_del_ok,
                                   rec) = FALSE then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_tsf,
                        rec.sttsf_row_seq,
                        NULL,
                        O_error_message);
            L_error := TRUE;
         end if;
      end if; -- if rank=1

-- Validating TSF_ENTITY_ORG_UNIT_SOB <VALIDATE TEO EVEN if L_ERROR>
      if rec.sttsf_action = action_del
         and rec.stteo_action = action_new then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table_teo,
                     rec.stteo_row_seq,
                     'ACTION',
                     'INV_ACT_COMB');
         L_teo_error :=TRUE;
      end if;
      if rec.stteo_action NOT IN (action_new,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table_teo,
                     rec.stteo_row_seq,
                     'ACTION',
                     'INV_ACT');
         L_teo_error :=TRUE;
      end if;
      if rec.stteo_action = action_new
         and rec.PK_TSF_ENTITY_ORG_UNIT_SOB_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table_teo,
                     rec.stteo_row_seq,
                     'Org Unit Id,Transfer Entity',
                     'REC_EXIST');
         L_teo_error :=TRUE;
      end if;

      if rec.stteo_action = action_new then
         if rec.stteo_set_of_books_id is NOT NULL and rec.TEO_FOG_FK_rid is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_teo,
                        rec.stteo_row_seq,
                        'SET_OF_BOOKS_ID',
                        'INV_SET_OF_BOOKS_ID');
            L_teo_error :=TRUE;
         end if;
         if rec.TEO_OGU_FK_rid is NULL
            and rec.stteo_org_unit_id is NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_teo,
                        rec.stteo_row_seq,
                        'ORG_UNIT_ID',
                        'INV_ORGUNIT');
            L_teo_error :=TRUE;
         end if;
         if rec.stteo_set_of_books_id is NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_teo,
                        rec.stteo_row_seq,
                        'SET_OF_BOOKS_ID',
                        'MUST_ENTER_SOB_ID');
            L_teo_error :=TRUE;
         end if;
         if rec.TEO_TSN_FK_rid is NULL
            and (rec.sttsf_action IN (action_mod, action_del)
            or rec.sttsf_action is NULL) then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_teo,
                        rec.stteo_row_seq,
                        'TSF_ENTITY_ID',
                        'INV_ENTITY');
            L_teo_error :=TRUE;
         end if;
      end if; --ACTION=NEW

      if rec.stteo_action = action_del
         and rec.PK_TSF_ENTITY_ORG_UNIT_SOB_rid is NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table_teo,
                     rec.stteo_row_seq,
                     'Org Unit Id,Transfer Entity',
                     'NO_RECORD');
         L_teo_error :=TRUE;
      end if;

-- Validating TSF_ENTITY_ORG_UNIT_SOB BUSINESS LOGIC
      if PROCESS_TSF_ENTITY_TEO_VAL(O_error_message,
                                    L_teo_error,
                                    rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.nextval,
                     I_chunk_id,
                     L_table_teo,
                     rec.stteo_row_seq,
                     NULL,
                     O_error_message);
         L_teo_error := TRUE;
      end if;

      if L_error then
         if rec.stteo_row_seq IS  NOT NULL then
            WRITE_ERROR(I_process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.nextval,
                        I_chunk_id,
                        L_table_teo,
                        rec.stteo_row_seq,
                        'TSF_ENTITY_ID',
                        'MASTER_REC_ER');
         end if;
      end if;
-- Begin Processing
      if NOT L_error then
         if rec.tsf_rank = 1 then
            SAVEPOINT do_process;
         end if;
--PROCESS TSF_ENTITY REC
         if rec.sttsf_action is NOT NULL and rec.tsf_rank = 1 then
            L_tsf_process_error := FALSE;
            L_tsf_entity_temp_rec.SECONDARY_DESC  := rec.sttsf_secondary_desc;
            L_tsf_entity_temp_rec.tsf_entity_id   := rec.sttsf_tsf_entity_id;
            L_tsf_entity_temp_rec.TSF_ENTITY_DESC := rec.sttsf_tsf_entity_desc;
            L_tsf_entity_temp_rec.CREATE_ID       := GET_USER;
            L_tsf_entity_temp_rec.CREATE_DATETIME := SYSDATE;
            if rec.sttsf_action = action_new then
               if EXEC_TSF_ENTITY_INS(O_error_message,
                                      L_tsf_entity_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table_tsf,
                              rec.sttsf_row_seq,
                              NULL,
                              O_error_message);
                  L_tsf_process_error := TRUE;
               end if;
            end if;
            if rec.sttsf_action = action_mod
               and (rec.sttsf_tsf_entity_desc <> rec.PK_TSF_ENTITY_desc
               or rec.sttsf_secondary_desc <> rec.PK_TSF_ENTITY_sec_desc) then
               if EXEC_TSF_ENTITY_UPD(O_error_message,
                                      L_tsf_entity_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table_tsf,
                              rec.sttsf_row_seq,
                              NULL,
                              O_error_message);
                  L_tsf_process_error := TRUE;
               end if;
            end if;
            if L_tsf_process_error then
               if rec.stteo_action IS  NOT NULL then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table_teo,
                              rec.stteo_row_seq,
                              'TSF_ENTITY_ID',
                              'MASTER_REC_ER');
               end if;
            end if;
         end if; -- rec.sttsf_action is NOT NULL and rec.tsf_rank = 1

--PROCESS TEO RECS
         if rec.stteo_action is NOT NULL
            and NOT L_teo_error
            and NOT L_tsf_process_error
            and rec.stteo_action IN (action_new,action_del)   then

            L_teo_temp_rec.tsf_entity_id   := rec.stteo_tsf_entity_id;
            L_teo_temp_rec.org_unit_id     := rec.stteo_org_unit_id;
            L_teo_temp_rec.set_of_books_id := rec.stteo_set_of_books_id;
            if rec.stteo_action = action_new then
               if EXEC_TEO_INS(O_error_message,
                               L_teo_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table_teo,
                              rec.stteo_row_seq,
                              NULL,
                              O_error_message);
                  L_teo_process_error := TRUE;
               end if;
            end if;
            if rec.stteo_action = action_del then
               if EXEC_TEO_DEL(O_error_message,
                               L_teo_temp_rec) = FALSE then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table_teo,
                              rec.stteo_row_seq,
                              NULL,
                              O_error_message);
                  L_teo_process_error := TRUE;
               end if;
            end if;
            if NOT L_teo_process_error then
               L_row_seq_tab.extend();
               L_row_seq_tab(c)  := rec.stteo_row_seq;
               c:=c+1;
            end if;
         end if; -- rec.stteo_action is NOT NULL and NOT L_teo_error and NOT L_tsf_process_error

-- CHECK FOR DATA INTEGRITY
         if rec.sttsf_tsf_entity_id <> rec.next_entity_id then
            if rec.sttsf_action in (action_new,action_mod) or rec.sttsf_action is NULL then
               open C_TEO_COUNT(rec.sttsf_tsf_entity_id);
               fetch C_TEO_COUNT into L_count_teo;
               close C_TEO_COUNT;
               if L_count_teo < 1 then
                  L_rowseq_count := L_row_seq_tab.count();
                  if L_rowseq_count > 0 then
                     for i in 1..L_rowseq_count
                     LOOP
                        if rec.stteo_action = action_del then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                       I_chunk_id,
                                       L_table_teo,
                                       L_row_seq_tab(i),
                                       'TSF_ENTITY_ID',
                                       'LAST_TSF_ORG');
                        end if;
                     END LOOP;
                     ROLLBACK TO do_process;
                  end if;

                  if rec.sttsf_action is NOT NULL then
                     ROLLBACK TO do_process;
                        WRITE_ERROR(I_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                    I_chunk_id,
                                    L_table_tsf,
                                    rec.sttsf_row_seq,
                                    'TSF_ENTITY_ID',
                                    'ATLEAST_ONE_TSF_ORG');
                  end if;
               end if;
            end if;

            -- The logic to check delete on stteo sheet and check association
            if rec.stteo_action = action_del   then
               if TSF_ENTITY_SQL.CHECK_ORG_UNIT_LOC_EXISTS(O_error_message,
                                                           L_rec_exists,
                                                           rec.stteo_tsf_entity_id,
                                                           rec.stteo_org_unit_id) = FALSE   then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table_tsf,
                              rec.stteo_row_seq,
                              'Transfer Entity,Org Unit Id',
                              O_error_message);
                  ROLLBACK TO do_process;
               elsif L_rec_exists = TRUE   then
                     WRITE_ERROR(I_process_id,
                                 SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                 I_chunk_id,
                                 L_table_tsf,
                                 rec.stteo_row_seq,
                                 'Transfer Entity,Org Unit Id',
                                 O_error_message);
                     ROLLBACK TO do_process;
               end if;
            end if;

            if rec.sttsf_action = action_del then
               open C_TEO_COUNT(rec.sttsf_tsf_entity_id);
               fetch C_TEO_COUNT into L_count_teo;
               close C_TEO_COUNT;
               if L_count_teo > 0 then
                  WRITE_ERROR(I_process_id,
                              SVC_ADMIN_UPLD_ER_SEQ.nextval,
                              I_chunk_id,
                              L_table_tsf,
                              rec.sttsf_row_seq,
                              'TSF_ENTITY_ID',
                              'CANNOT_DEL_MASTER_REC');
                  ROLLBACK TO do_process;
               else
                  if L_deL_ok then
                     if NOT L_error then
                        if EXEC_TSF_ENTITY_DEL(O_error_message,
                                               L_tsf_entity_temp_rec) = FALSE then
                           WRITE_ERROR(I_process_id,
                                       SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                       I_chunk_id,
                                       L_table_tsf,
                                       rec.sttsf_row_seq,
                                       NULL,
                                       O_error_message);
                           L_tsf_process_error := TRUE;
                        end if;
                     end if;
                  end if;

                  if L_tsf_process_error then
                     if rec.stteo_action IS  NOT NULL then
                        WRITE_ERROR(I_process_id,
                                    SVC_ADMIN_UPLD_ER_SEQ.nextval,
                                    I_chunk_id,
                                    L_table_teo,
                                    rec.stteo_row_seq,
                                    'TSF_ENTITY_ID',
                                    'MASTER_REC_ER');
                     end if;
                  end if;
               end if;
            end if;
         end if; --if rec.sttsf_tsf_entity_id <> rec.next_entity_id
      end if; --if NOT L_error
   END LOOP;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_TEO_COUNT%ISOPEN then
         close C_TEO_COUNT;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_TSF_ENTITY;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TSF_ENTITY_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_TSF_ENTITY_TL.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_TSF_ENTITY_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program              VARCHAR2(64) := 'CORESVC_TSF_ENTITY.PROCESS_TSF_ENTITY_TL';
   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table     SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TSF_ENTITY_TL';
   L_base_table           SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'TSF_ENTITY';
   L_table                SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_TSF_ENTITY_TL';
   L_error                BOOLEAN := FALSE;
   L_process_error        BOOLEAN := FALSE;
   L_tsf_entity_TL_temp_rec    TSF_ENTITY_TL%ROWTYPE;
   L_tsf_entity_tl_upd_rst     ROW_SEQ_TAB;
   L_tsf_entity_tl_del_rst     ROW_SEQ_TAB;

   cursor C_SVC_TSF_ENTITY_TL(I_process_id NUMBER,
                              I_chunk_id   NUMBER) is
      select pk_tsf_entity_tl.rowid  as pk_tsf_entity_tl_rid,
             fk_tsf_entity.rowid     as fk_tsf_entity_rid,
             fk_lang.rowid           as fk_lang_rid,
             st.lang,
             st.tsf_entity_id,
             st.tsf_entity_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_tsf_entity_tl  st,
             tsf_entity         fk_tsf_entity,
             tsf_entity_tl      pk_tsf_entity_tl,
             lang               fk_lang
       where st.process_id  =  I_process_id
         and st.chunk_id    =  I_chunk_id
         and st.tsf_entity_id  =  fk_tsf_entity.tsf_entity_id (+)
         and st.lang        =  pk_tsf_entity_TL.lang (+)
         and st.tsf_entity_id  =  pk_tsf_entity_TL.tsf_entity_id (+)
         and st.lang        =  fk_lang.lang (+);

   TYPE SVC_TSF_ENTITY_TL is TABLE OF C_SVC_TSF_ENTITY_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_tsf_entity_tab        SVC_TSF_ENTITY_TL;

   L_tsf_entity_tl_ins_tab         TSF_ENTITY_TAB         := NEW TSF_ENTITY_TAB();
   L_tsf_entity_tl_upd_tab         TSF_ENTITY_TAB         := NEW TSF_ENTITY_TAB();
   L_tsf_entity_tl_del_tab         TSF_ENTITY_TAB         := NEW TSF_ENTITY_TAB();

BEGIN
   if C_SVC_TSF_ENTITY_TL%ISOPEN then
      close C_SVC_TSF_ENTITY_TL;
   end if;

   open C_SVC_TSF_ENTITY_TL(I_process_id,
                            I_chunk_id);
   LOOP
      fetch C_SVC_TSF_ENTITY_TL bulk collect into L_svc_tsf_entity_tab limit LP_bulk_fetch_limit;
      if L_svc_tsf_entity_tab.COUNT > 0 then
         FOR i in L_svc_tsf_entity_tab.FIRST..L_svc_tsf_entity_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_tsf_entity_tab(i).action is NULL
               or L_svc_tsf_entity_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsf_entity_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_tsf_entity_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsf_entity_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_tsf_entity_tab(i).action = action_new
               and L_svc_tsf_entity_tab(i).pk_tsf_entity_TL_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsf_entity_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_tsf_entity_tab(i).action IN (action_mod, action_del)
               and L_svc_tsf_entity_tab(i).lang is NOT NULL
               and L_svc_tsf_entity_tab(i).tsf_entity_id is NOT NULL
               and L_svc_tsf_entity_tab(i).pk_tsf_entity_TL_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_tsf_entity_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_tsf_entity_tab(i).action = action_new
               and L_svc_tsf_entity_tab(i).tsf_entity_id is NOT NULL
               and L_svc_tsf_entity_tab(i).fk_tsf_entity_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_tsf_entity_tab(i).row_seq,
                            'TSF_ENTITY_ID',
                            'INV_TSF_ENTITY_ID');
               L_error :=TRUE;
            end if;

            if L_svc_tsf_entity_tab(i).action = action_new
               and L_svc_tsf_entity_tab(i).lang is NOT NULL
               and L_svc_tsf_entity_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsf_entity_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_tsf_entity_tab(i).action in (action_new, action_mod) then
               if L_svc_tsf_entity_tab(i).tsf_entity_desc is NULL then
                  WRITE_ERROR(I_process_id,
                              svc_admin_upld_er_seq.NEXTVAL,
                              I_chunk_id,
                              L_table,
                              L_svc_tsf_entity_tab(i).row_seq,
                              'TSF_ENTITY_DESC',
                              'TSF_ENTITY_DESC_REQ');
                  L_error :=TRUE;
               end if;
            end if;

            if L_svc_tsf_entity_tab(i).tsf_entity_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsf_entity_tab(i).row_seq,
                           'TSF_ENTITY_ID',
                           'TSF_ENTITY_ID_REQ');
               L_error :=TRUE;
            end if;

            if L_svc_tsf_entity_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_tsf_entity_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANGUAGE');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_tsf_entity_TL_temp_rec.lang := L_svc_tsf_entity_tab(i).lang;
               L_tsf_entity_TL_temp_rec.tsf_entity_id := L_svc_tsf_entity_tab(i).tsf_entity_id;
               L_tsf_entity_TL_temp_rec.tsf_entity_desc := L_svc_tsf_entity_tab(i).tsf_entity_desc;
               L_tsf_entity_TL_temp_rec.create_datetime := SYSDATE;
               L_tsf_entity_TL_temp_rec.create_id := GET_USER;
               L_tsf_entity_TL_temp_rec.last_update_datetime := SYSDATE;
               L_tsf_entity_TL_temp_rec.last_update_id := GET_USER;

               if L_svc_tsf_entity_tab(i).action = action_new then
                  L_tsf_entity_TL_ins_tab.extend;
                  L_tsf_entity_TL_ins_tab(L_tsf_entity_TL_ins_tab.count()) := L_tsf_entity_TL_temp_rec;
               end if;

               if L_svc_tsf_entity_tab(i).action = action_mod then
                  L_tsf_entity_TL_upd_tab.extend;
                  L_tsf_entity_TL_upd_tab(L_tsf_entity_TL_upd_tab.count()) := L_tsf_entity_TL_temp_rec;
                  L_tsf_entity_tl_upd_rst(L_tsf_entity_TL_upd_tab.count()) := L_svc_tsf_entity_tab(i).row_seq;
               end if;

               if L_svc_tsf_entity_tab(i).action = action_del then
                  L_tsf_entity_TL_del_tab.extend;
                  L_tsf_entity_TL_del_tab(L_tsf_entity_TL_del_tab.count()) := L_tsf_entity_TL_temp_rec;
                  L_tsf_entity_tl_del_rst(L_tsf_entity_TL_del_tab.count()) := L_svc_tsf_entity_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_TSF_ENTITY_TL%NOTFOUND;
   END LOOP;
   close C_SVC_TSF_ENTITY_TL;

   if EXEC_TSF_ENTITY_TL_INS(O_error_message,
                             L_tsf_entity_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_TSF_ENTITY_TL_UPD(O_error_message,
                             L_tsf_entity_tl_upd_tab,
                             L_tsf_entity_tl_upd_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_TSF_ENTITY_TL_DEL(O_error_message,
                             L_tsf_entity_tl_del_tab,
                             L_tsf_entity_tl_del_rst,
                             I_process_id,
                             I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TSF_ENTITY_TL;
----------------------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id   IN   NUMBER) IS

BEGIN
   delete
     from svc_tsf_entity_tl
    where process_id = I_process_id;

   delete
     from svc_tsf_entity
    where process_id = I_process_id;

   delete
     from svc_tsf_entity_org_unit_sob
    where process_id = I_process_id;
END;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT   NUMBER,
                 I_process_id      IN       NUMBER,
                 I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program          VARCHAR2(64) := 'CORESVC_TSF_ENTITY.PROCESS';
   L_process_status   SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   Lp_errors_tab := NEW errors_tab_typ();
   if PROCESS_TSF_ENTITY(O_error_message,
                         I_process_id,
                         I_chunk_id) = FALSE then
      return FALSE;
   end if;
   if PROCESS_TSF_ENTITY_TL(O_error_message,
                            I_process_id,
                            I_chunk_id) = FALSE then
      return FALSE;
   end if;

   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
      set status = (CASE
                    when status = 'PE'
                          then 'PE'
                    else L_process_status
                    END),
              action_date = SYSDATE
        where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;

EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_TSF_ENTITY;
/