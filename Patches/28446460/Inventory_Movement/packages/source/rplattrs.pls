CREATE OR REPLACE PACKAGE REPL_ATTRIBUTE_MAINTENANCE_SQL AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------------------------
-- Function name:  EXPLODE_ITEM_LIST
-- Purpose      :  This function will take each ITEM in the item list and write
--                 a record on the repl_attr_update_item table to indicate how the
--                 replenishment attributes should be updated.  It will also break
--                 down the locations in each location type  write each location
--                 to the repl_attr_update_locn table.  A batch program will
--                 then call repl_attribute_maintenance_sql to do the update.
-- Calls        :  none
-- Created      :  April 25, 1997
--------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEM_LIST( O_error_message         IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exist                 IN OUT   VARCHAR2,
                            O_sched_updates_exist   IN OUT   BOOLEAN
                          )
    RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function name:  ITEM_LOCNS_DEACTIVATE
-- Purpose      :  This function deactivates a item from replenishment by removing
--                 its record from the REPL_ITEM_LOC table.
-- Calls        :  none
-- Created      :  October 10, 1998
---------------------------------------------------------------------------------------------------
FUNCTION ITEM_LOCNS_DEACTIVATE(O_error_message   IN OUT   VARCHAR2,
                               I_item            IN       repl_item_loc.item%type)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- Function name:  ITEM_LOCNS_UPDATE
-- Purpose      :  This function calls UPDATE_ITEM with each location for the
--              :  passed in item.
-- Calls        :  UPDATE_ITEM
-- Created      :  October 10, 1998
---------------------------------------------------------------------------------------------------
Function ITEM_LOCNS_UPDATE(O_error_message   IN   OUT VARCHAR2,
                           I_item            IN    repl_item_loc.item%type)
    RETURN BOOLEAN;
----------------------------------------------------------------------------------------------------
-- Function name:  UPDATE_ITEM
-- Purpose      :  This internal function is private to ITEM_LOCNS_UPDATE.  It updates the
--              :  item/location record in the REPL_ITEM_LOC table with the new
--              :  data, after first checking that the new data is valid.
-- Calls        :  none
-- Created      :  October 10, 1998
------------------------------------------------------------------------------------------------------
-- Function Name: UPDATE_PACK
-- Purpose      : This internal function verifies that a pack exists at the specified location before it is
--              : written to repl_item_loc. This function is internal to the UPDATE_ITEM_LOCNS function.
-------------------------------------------------------------------------------------------------------
-- Function name:  ITEM_LOCNS_ACTIVATE
-- Purpose      :  This function will call ACTIVATE_ITEM.
-- Created      :  October 10, 1998
-------------------------------------------------------------------------------------------------------
FUNCTION ITEM_LOCNS_ACTIVATE( O_error_message   IN OUT   VARCHAR2,
                              I_item            IN       REPL_ITEM_LOC.ITEM%TYPE )
    RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ACTIVATE_ITEM
-- Purpose      :  This internal function activates a item for replenishment at a particular
--              :  store or warehouse by adding a record to the REPL_ITEM_LOC
--              :  table.
-- Calls        :  none
-- Created      :  October 10, 1998
--------------------------------------------------------------------------------------------------------
-- Function Name: ACTIVATE_PACK
-- Purpose      : This internal function verifies that a pack exists at the specified location before it is
--              : written to repl_item_loc. This function is internal to the ACTIVATE_ITEM function.
--------------------------------------------------------------------------------------------------------
-- Function Name:  ITEM_LOCNS
-- Purpose      :  This function determins the replenishment action, and passes
--              :  the item to the approprite function.
-- Calls        :  ITEM_LOCNS_ACTIVATE, ITEM_LOCNS_DEACTIVATE, ITEM_LOCNS_UPDATE
-- Created      :  October 10, 1998
--------------------------------------------------------------------------------------------------------
Function ITEM_LOCNS(O_error_message IN OUT VARCHAR2,
                    I_item          IN  repl_item_loc.item%type)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name:  EXPLODE_PARENT_ITEM
-- Purpose      :  This function call ITEM_LOCNS fall every child under the parent item passed in.
-- Created      :  October 10, 1998
--------------------------------------------------------------------------------------------------------
Function EXPLODE_PARENT_ITEM(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-- Function Name:  EXPLODE_ITEM_HIER
-- Purpose      :  This function call ITEM_LOCNS for every item within a specific hierarchy dept/class/subclass
-- Created      :  December 18, 2007
--------------------------------------------------------------------------------------------------------
Function EXPLODE_ITEM_HIER(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
-- Function Name:  GET_ITEM_TYPE
-- Purpose      :  The only public function in the package, it will be
--                 called by the Replenishment Attributes Maintenance form
--                 and batch program to update item location replenishment
--                 attributes.  By looking at the
--                 item type that is passed into the function, it will call the
--                 appropriate internal function to update the attributes.
-- Calls        :  STAPLE_ITEM_LOCNS, FASHION_STYLE, FASHION_ITEM_LOCNS,
--                 ITEM_LIST_EXPLODE
-- Created      :  April 14, 1997
---------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_TYPE( O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_report_required           IN OUT   BOOLEAN,
                        O_exist                     IN OUT   VARCHAR2,
                        O_found                     IN OUT   BOOLEAN,
                        O_repl_attr_id              IN OUT   REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE,
                        I_action                    IN       VARCHAR2,
                        I_repl_method_update        IN       VARCHAR2,
                        I_item_level                IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
                        I_tran_level                IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
                        I_item                      IN       REPL_ITEM_LOC.ITEM%TYPE,
                        I_sch_active_date           IN       REPL_ATTR_UPDATE_HEAD.SCHEDULED_ACTIVE_DATE%TYPE,
                        I_mra_update                IN       REPL_ATTR_UPDATE_HEAD.MRA_UPDATE%TYPE,
                        I_mra_restore               IN       REPL_ATTR_UPDATE_HEAD.MRA_RESTORE%TYPE,
                        I_group_type                IN       VARCHAR2,
                        I_value                     IN       VARCHAR2,
                        I_repl_order_ctrl           IN       REPL_ITEM_LOC.REPL_ORDER_CTRL%TYPE,
                        I_stock_cat                 IN       REPL_ITEM_LOC.STOCK_CAT%TYPE,
                        I_activate_date             IN       REPL_ITEM_LOC.ACTIVATE_DATE%TYPE,
                        I_deactivate_date           IN       REPL_ITEM_LOC.DEACTIVATE_DATE%TYPE,
                        I_source_wh                 IN       REPL_ITEM_LOC.SOURCE_WH%TYPE,
                        I_pres_stock                IN       REPL_ITEM_LOC.PRES_STOCK%TYPE,
                        I_demo_stock                IN       REPL_ITEM_LOC.DEMO_STOCK%TYPE,
                        I_repl_method               IN       REPL_ITEM_LOC.REPL_METHOD%TYPE,
                        I_min_stock                 IN       REPL_ITEM_LOC.MIN_STOCK%TYPE,
                        I_max_stock                 IN       REPL_ITEM_LOC.MAX_STOCK%TYPE,
                        I_incr_pct                  IN       REPL_ITEM_LOC.INCR_PCT%TYPE,
                        I_min_supply_days           IN       REPL_ITEM_LOC.MIN_SUPPLY_DAYS%TYPE,
                        I_max_supply_days           IN       REPL_ITEM_LOC.MAX_SUPPLY_DAYS%TYPE,
                        I_time_supply_horizon       IN       REPL_ITEM_LOC.TIME_SUPPLY_HORIZON%TYPE,
                        I_inv_selling_days          IN       REPL_ITEM_LOC.INV_SELLING_DAYS%TYPE,
                        I_service_level             IN       REPL_ITEM_LOC.SERVICE_LEVEL%TYPE,
                        I_lost_sales_factor         IN       REPL_ITEM_LOC.LOST_SALES_FACTOR%TYPE,
                        I_non_scale_ind             IN       REPL_ITEM_LOC.NON_SCALING_IND%TYPE,
                        I_max_scale_value           IN       REPL_ITEM_LOC.MAX_SCALE_VALUE%TYPE,
                        I_pickup_lead_time          IN       REPL_ITEM_LOC.PICKUP_LEAD_TIME%TYPE,
                        I_wh_lead_time              IN       REPL_ITEM_LOC.WH_LEAD_TIME%TYPE,
                        I_terminal_stock_qty        IN       REPL_ITEM_LOC.TERMINAL_STOCK_QTY%TYPE,
                        I_season_id                 IN       REPL_ITEM_LOC.SEASON_ID%TYPE,
                        I_phase_id                  IN       REPL_ITEM_LOC.PHASE_ID%TYPE,
                        I_supplier                  IN       REPL_ITEM_LOC.PRIMARY_REPL_SUPPLIER%TYPE,
                        I_origin_country_id         IN       REPL_ITEM_LOC.ORIGIN_COUNTRY_ID%TYPE,
                        I_review_cycle              IN       REPL_ITEM_LOC.REVIEW_CYCLE%TYPE,
                        I_service_level_type        IN       REPL_ATTR_UPDATE_HEAD.SERVICE_LEVEL_TYPE%TYPE,
                        I_update_days               IN       VARCHAR2,
                        I_monday                    IN       VARCHAR2,
                        I_tuesday                   IN       VARCHAR2,
                        I_wednesday                 IN       VARCHAR2,
                        I_thursday                  IN       VARCHAR2,
                        I_friday                    IN       VARCHAR2,
                        I_saturday                  IN       VARCHAR2,
                        I_sunday                    IN       VARCHAR2,
                        I_unit_tolerance            IN       REPL_ITEM_LOC.UNIT_TOLERANCE%TYPE,
                        I_pct_tolerance             IN       REPL_ITEM_LOC.PCT_TOLERANCE%TYPE,
                        I_use_tolerance_ind         IN       REPL_ITEM_LOC.USE_TOLERANCE_IND%TYPE,
                        I_user_id                   IN       USER_USERS.USERNAME%TYPE,
                        I_primary_pack_no           IN       REPL_ITEM_LOC.PRIMARY_PACK_NO%TYPE,
                        I_rej_st_ord_ind            IN       REPL_ITEM_LOC.REJECT_STORE_ORD_IND%TYPE,
                        I_default_pack_ind          IN       VARCHAR2,
                        I_remove_pack_ind           IN       VARCHAR2,
                        I_create_date               IN       REPL_ATTR_UPDATE_HEAD.CREATE_DATE%TYPE,
                        I_create_id                 IN       REPL_ATTR_UPDATE_HEAD.CREATE_ID%TYPE,
                        I_sch_repl_desc             IN       REPL_ATTR_UPDATE_HEAD.SCH_RPL_DESC%TYPE,
                        I_size_profile_ind          IN       VARCHAR2,
                        I_add_lead_time_ind         IN       REPL_ITEM_LOC.ADD_LEAD_TIME_IND%TYPE,
                        I_dept                      IN       ITEM_MASTER.DEPT%TYPE,
                        I_class                     IN       ITEM_MASTER.CLASS%TYPE,
                        I_subclass                  IN       ITEM_MASTER.SUBCLASS%TYPE,
                        I_diff_1                    IN       REPL_ATTR_UPDATE_ITEM.DIFF_1%TYPE,
                        I_diff_2                    IN       REPL_ATTR_UPDATE_ITEM.DIFF_2%TYPE,
                        I_diff_3                    IN       REPL_ATTR_UPDATE_ITEM.DIFF_3%TYPE,
                        I_diff_4                    IN       REPL_ATTR_UPDATE_ITEM.DIFF_4%TYPE,
                        I_supp_lead_time            IN       REPL_ITEM_LOC.SUPP_LEAD_TIME%TYPE,
                        I_multiple_runs_per_day_ind IN       REPL_ITEM_LOC.MULT_RUNS_PER_DAY_IND%TYPE,
                        I_transfer_zero_soh_ind     IN       REPL_ITEM_LOC.TSF_ZERO_SOH_IND%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: REPL_ITEM_LOC_UPDATES
-- Purpose      : This function inserts to the repl_item_loc_updates table when a new item/location
--              : is placed on replenishment or when the supplier, origin country, pack, review cycle
--              : or review days have been updated.
------------------------------------------------------------------------------------------------------
FUNCTION REPL_ITEM_LOC_UPDATES(O_error_message     IN OUT     VARCHAR2,
                               I_item              IN         ITEM_MASTER.ITEM%TYPE,
                               I_loc_type          IN         ITEM_LOC.LOC_TYPE%TYPE,
                               I_location          IN         ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------
-- Function Name: PARENT_WH_EXISTS
-- Purpose      : This function verifies that the source warehouse for the parent item passed to this
--              : package also exists for its' children.
------------------------------------------------------------------------------------------------------
/*FUNCTION PARENT_WH_EXISTS (O_error_message     IN OUT     VARCHAR2,
                             I_item              IN         ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;*/
------------------------------------------------------------------------------------------------------
-- Function Name: DELETE_SCH_UPDATE
-- Purpose      : This function inserts record into table
--                REPL_ATTR_UPDATE_EXCLUDE when an item/location is to be deleted.
------------------------------------------------------------------------------------------------------
FUNCTION DELETE_SCH_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_repl_attr_id    IN       REPL_ATTR_UPDATE_ITEM.REPL_ATTR_ID%TYPE,
                           I_item            IN       REPL_ATTR_UPDATE_ITEM.ITEM%TYPE,
                           I_loc             IN       REPL_ATTR_UPDATE_LOC.LOC%TYPE,
                           I_loc_type        IN       REPL_ATTR_UPDATE_LOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
-- Function Name: GET_NEXT_REPL_ID
-- Purpose      : This function returns the next available replenishment attribute id.
------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_REPL_ID( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_repl_attr_id    IN OUT   REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE )
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
-- Function Name: CREATE_SCHEDULED_UPDATE
-- Purpose      : This function creates a schedule update for a given item/location combination.
------------------------------------------------------------------------------------------------------
FUNCTION CREATE_SCHEDULED_UPDATE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_repl_attr_id    IN OUT   REPL_ATTR_UPDATE_HEAD.REPL_ATTR_ID%TYPE,
                                 O_found           IN OUT   BOOLEAN)
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
-- Function Name: GET_REPL_ATTR_UPDATE_EXCLUDE
-- Purpose      : This function returns a row from the excludes table
------------------------------------------------------------------------------------------------------
FUNCTION GET_REPL_ATTR_UPDATE_EXCLUDE(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_exists          IN OUT   BOOLEAN,
                                      O_row             IN OUT   REPL_ATTR_UPDATE_EXCLUDE%ROWTYPE,
                                      I_repl_id         IN       REPL_ATTR_UPDATE_EXCLUDE.REPL_ATTR_ID%TYPE,
                                      I_item            IN       REPL_ATTR_UPDATE_EXCLUDE.ITEM%TYPE,
                                      I_location        IN       REPL_ATTR_UPDATE_EXCLUDE.LOCATION%TYPE
                                      )
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
-- Function Name: INSERT_REPL_HIST
-- Purpose      : This function tracks the changes in the replenishment attributes
------------------------------------------------------------------------------------------------------
FUNCTION INSERT_REPL_HIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_repl_hist_row   IN       REPL_ATTR_UPD_HIST%ROWTYPE
                         )
RETURN BOOLEAN;
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
-- Function Name: GET_MASTER_ITEM
-- Purpose      : This function gets the master replenishment item for a complex pack.
------------------------------------------------------------------------------------------------------
FUNCTION GET_MASTER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_master_item     IN OUT   ITEM_MASTER.ITEM%TYPE,
                         I_item            IN       ITEM_MASTER.ITEM%TYPE,
                         I_location_type   IN       REPL_RESULTS.LOC_TYPE%TYPE,
                         I_location        IN       REPL_RESULTS.LOCATION%TYPE,
                         I_alloc_no        IN       REPL_RESULTS.ALLOC_NO%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                         I_repl_date       IN       REPL_RESULTS.REPL_DATE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END REPL_ATTRIBUTE_MAINTENANCE_SQL;
/
