CREATE OR REPLACE PACKAGE BODY CORESVC_DLVY_SLOT AS
--------------------------------------------------------------------------------
   cursor C_SVC_DELIVERY_SLOT(I_process_id   NUMBER,
                              I_chunk_id     NUMBER) IS
      select pk_delivery_slot.rowid     as pk_delivery_slot_rid,
             st.rowid                   as st_rid,
             st.delivery_slot_desc,
             st.delivery_slot_sequence,
             st.delivery_slot_id,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)           as action,
             st.process$status
        from svc_delivery_slot st,
             delivery_slot pk_delivery_slot,
             dual
       where st.process_id              = I_process_id
         and st.chunk_id                = I_chunk_id
         and st.delivery_slot_id        = pk_delivery_slot.delivery_slot_id (+);

   TYPE errors_tab_typ is TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab errors_tab_typ;
   TYPE s9t_errors_tab_typ is TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab s9t_errors_tab_typ;

   Type DELIVERY_SLOT_TL_TAB IS TABLE OF DELIVERY_SLOT_TL%ROWTYPE;
   TYPE ROW_SEQ_TAB is TABLE OF SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE INDEX BY BINARY_INTEGER;
   LP_bulk_fetch_limit   CONSTANT NUMBER(12) := 1000;

   LP_primary_lang    LANG.LANG%TYPE;
--------------------------------------------------------------------------------

FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;

--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN S9T_ERRORS.FILE_ID%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN

   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := S9T_ERRORS_SEQ.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
--------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR(I_process_id    IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                      I_error_seq     IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                      I_chunk_id      IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                      I_table_name    IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                      I_row_seq       IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                      I_column_name   IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                      I_error_msg     IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE,
                      I_error_type    IN   SVC_ADMIN_UPLD_ER.ERROR_TYPE%TYPE DEFAULT 'E') IS
BEGIN
   Lp_errors_tab.extend();
   Lp_errors_tab(Lp_errors_tab.count()).process_id  := I_process_id;
   Lp_errors_tab(Lp_errors_tab.count()).error_seq   := I_error_seq;
   Lp_errors_tab(Lp_errors_tab.count()).chunk_id    := I_chunk_id;
   Lp_errors_tab(Lp_errors_tab.count()).table_name  := I_table_name;
   Lp_errors_tab(Lp_errors_tab.count()).row_seq     := I_row_seq;
   Lp_errors_tab(Lp_errors_tab.count()).column_name := I_column_name;
   Lp_errors_tab(Lp_errors_tab.count()).error_msg   := I_error_msg;
   Lp_errors_tab(Lp_errors_tab.count()).error_type  := I_error_type;

END WRITE_ERROR;
--------------------------------------------------------------------------------

PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   DELIVERY_SLOT_cols s9t_pkg.names_map_typ;
   DELIVERY_SLOT_TL_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                         := s9t_pkg.get_sheet_names(I_file_id);
   DELIVERY_SLOT_cols               := s9t_pkg.get_col_names(I_file_id,DELIVERY_SLOT_sheet);
   DELIVERY_SLOT$Action             := DELIVERY_SLOT_cols('ACTION');
   DLVYSLT$DELIVERY_SLOT_DESC       := DELIVERY_SLOT_cols('DELIVERY_SLOT_DESC');
   DLVYSLT$DELIVERY_SLOT_SEQUENCE   := DELIVERY_SLOT_cols('DELIVERY_SLOT_SEQUENCE');
   DELIVERY_SLOT$DELIVERY_SLOT_ID   := DELIVERY_SLOT_cols('DELIVERY_SLOT_ID');

   DELIVERY_SLOT_TL_cols               := s9t_pkg.get_col_names(I_file_id,DELIVERY_SLOT_TL_sheet);
   DELIVERY_SLOT_TL$Action             := DELIVERY_SLOT_TL_cols('ACTION');
   DELIVERY_SLOT_TL$Lang               := DELIVERY_SLOT_TL_cols('LANG');
   DLVYSLT_TL$DELIVERY_SLOT_ID         := DELIVERY_SLOT_TL_cols('DELIVERY_SLOT_ID');
   DLVYSLT_TL$DELIVERY_SLOT_DESC       := DELIVERY_SLOT_TL_cols('DELIVERY_SLOT_DESC');
END POPULATE_NAMES;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_DELIVERY_SLOT( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = DELIVERY_SLOT_sheet )
   select s9t_row(s9t_cells( CORESVC_DLVY_SLOT.action_mod,
                             delivery_slot_id,
                             delivery_slot_desc,
                             delivery_slot_sequence
                           ))
     from delivery_slot ;
END POPULATE_DELIVERY_SLOT;
--------------------------------------------------------------------------------
PROCEDURE POPULATE_DELIVERY_SLOT_TL( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = DELIVERY_SLOT_TL_sheet )
   select s9t_row(s9t_cells( CORESVC_DLVY_SLOT.action_mod,
                             lang,
                             delivery_slot_id,
                             delivery_slot_desc
                           ))
     from delivery_slot_tl ;
END POPULATE_DELIVERY_SLOT_TL;
--------------------------------------------------------------------------------

PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file s9t_file;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;

BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := S9T_FOLDER_SEQ.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   if L_file.user_lang is NULL then
       L_file.user_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
    end if;

   L_file.add_sheet(DELIVERY_SLOT_sheet);
   L_file.sheets(L_file.get_sheet_index(DELIVERY_SLOT_sheet)).column_headers := s9t_cells('ACTION',
                                                                                          'DELIVERY_SLOT_ID',
                                                                                          'DELIVERY_SLOT_DESC',
                                                                                          'DELIVERY_SLOT_SEQUENCE'
                                                                                          );

   L_file.add_sheet(DELIVERY_SLOT_TL_sheet);
   L_file.sheets(L_file.get_sheet_index(DELIVERY_SLOT_TL_sheet)).column_headers := s9t_cells('ACTION',
                                                                                             'LANG',
                                                                                             'DELIVERY_SLOT_ID',
                                                                                             'DELIVERY_SLOT_DESC'
                                                                                              );
   s9t_pkg.SAVE_OBJ(l_file);

END INIT_S9T;
--------------------------------------------------------------------------------

FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_program VARCHAR2(255):='CORESVC_DLVY_SLOT.CREATE_S9T';
   L_file s9t_file;

BEGIN
   INIT_S9T(O_file_id);

   --populate column lists
    if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key)= FALSE then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_DELIVERY_SLOT(O_file_id);
      POPULATE_DELIVERY_SLOT_TL(O_file_id);
      commit;
   end if;

   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);

   S9T_PKG.APPLY_TEMPLATE(O_file_id,template_key);

   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file)= FALSE then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);

   S9T_PKG.UPDATE_ODS(L_file);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
--------------------------------------------------------------------------------

PROCEDURE PROCESS_S9T_DELIVERY_SLOT(I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                    I_process_id IN   SVC_DELIVERY_SLOT.process_id%TYPE) IS

   TYPE SVC_DELIVERY_SLOT_COL_TYP is TABLE OF SVC_DELIVERY_SLOT%ROWTYPE;
   L_temp_rec SVC_DELIVERY_SLOT%ROWTYPE;
   svc_DELIVERY_SLOT_col svc_DELIVERY_SLOT_col_typ :=NEW svc_DELIVERY_SLOT_col_typ();
   L_process_id SVC_DELIVERY_SLOT.PROCESS_ID%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_DELIVERY_SLOT%ROWTYPE;
   cursor C_MANDATORY_IND is
      select DELIVERY_SLOT_DESC_mi,
             DELIVERY_SLOT_SEQUENCE_mi,
             DELIVERY_SLOT_ID_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = CORESVC_DLVY_SLOT.template_key
                 and wksht_key         = 'DELIVERY_SLOT'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ( 'DELIVERY_SLOT_DESC' as DELIVERY_SLOT_DESC,
                                             'DELIVERY_SLOT_SEQUENCE' as DELIVERY_SLOT_SEQUENCE,
                                             'DELIVERY_SLOT_ID' as DELIVERY_SLOT_ID));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DELIVERY_SLOT';
   L_pk_columns    VARCHAR2(255)  := 'Slot Code';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN ( select DELIVERY_SLOT_DESC_dv,
                       DELIVERY_SLOT_SEQUENCE_dv,
                       DELIVERY_SLOT_ID_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_DLVY_SLOT.template_key
                          and wksht_key     = 'DELIVERY_SLOT'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'DELIVERY_SLOT_DESC' as DELIVERY_SLOT_DESC,
                                                      'DELIVERY_SLOT_SEQUENCE' as DELIVERY_SLOT_SEQUENCE,
                                                      'DELIVERY_SLOT_ID' as DELIVERY_SLOT_ID)))
   LOOP
      BEGIN
         L_default_rec.DELIVERY_SLOT_DESC := rec.DELIVERY_SLOT_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DELIVERY_SLOT',
                            NULL,
                            'DELIVERY_SLOT_DESC',
                            NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DELIVERY_SLOT_SEQUENCE := rec.DELIVERY_SLOT_SEQUENCE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DELIVERY_SLOT',
                             NULL,
                            'DELIVERY_SLOT_SEQUENCE',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DELIVERY_SLOT_ID := rec.DELIVERY_SLOT_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            'DELIVERY_SLOT',
                             NULL,
                            'DELIVERY_SLOT_ID',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.GET_CELL(DELIVERY_SLOT$Action))            as Action,
                      r.GET_CELL(DLVYSLT$DELIVERY_SLOT_DESC)             as DELIVERY_SLOT_DESC,
                      r.GET_CELL(DLVYSLT$DELIVERY_SLOT_SEQUENCE)         as DELIVERY_SLOT_SEQUENCE,
                      r.GET_CELL(DELIVERY_SLOT$DELIVERY_SLOT_ID)         as DELIVERY_SLOT_ID,
                      r.get_row_seq()                                    as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(DELIVERY_SLOT_sheet)
              )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DELIVERY_SLOT_DESC := rec.DELIVERY_SLOT_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_sheet,
                            rec.row_seq,
                            'DELIVERY_SLOT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DELIVERY_SLOT_SEQUENCE := rec.DELIVERY_SLOT_SEQUENCE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_sheet,
                            rec.row_seq,
                            'DELIVERY_SLOT_SEQUENCE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DELIVERY_SLOT_ID := rec.DELIVERY_SLOT_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_sheet,
                            rec.row_seq,
                            'DELIVERY_SLOT_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_DLVY_SLOT.action_new then
         L_temp_rec.DELIVERY_SLOT_DESC     := NVL( L_temp_rec.DELIVERY_SLOT_DESC,L_default_rec.DELIVERY_SLOT_DESC);
         L_temp_rec.DELIVERY_SLOT_SEQUENCE := NVL( L_temp_rec.DELIVERY_SLOT_SEQUENCE,L_default_rec.DELIVERY_SLOT_SEQUENCE);
         L_temp_rec.DELIVERY_SLOT_ID       := NVL( L_temp_rec.DELIVERY_SLOT_ID,L_default_rec.DELIVERY_SLOT_ID);
      end if;


      if NOT (L_temp_rec.DELIVERY_SLOT_ID is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         DELIVERY_SLOT_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));

         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_DELIVERY_SLOT_col.EXTEND();
         svc_DELIVERY_SLOT_col(svc_DELIVERY_SLOT_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..svc_DELIVERY_SLOT_col.COUNT SAVE EXCEPTIONS

      Merge into SVC_DELIVERY_SLOT st USING
         (select (case
                     when L_mi_rec.DELIVERY_SLOT_DESC_mi    = 'N'
                      and svc_DELIVERY_SLOT_col(i).action   = CORESVC_DLVY_SLOT.action_mod
                      and s1.DELIVERY_SLOT_DESC             is NULL
                     then mt.DELIVERY_SLOT_DESC
                     else s1.DELIVERY_SLOT_DESC
                   end) as DELIVERY_SLOT_DESC,
                 (case
                     when L_mi_rec.DELIVERY_SLOT_SEQUENCE_mi    = 'N'
                      and svc_DELIVERY_SLOT_col(i).action       = CORESVC_DLVY_SLOT.action_mod
                      and s1.DELIVERY_SLOT_SEQUENCE             is NULL
                     then mt.DELIVERY_SLOT_SEQUENCE
                     else s1.DELIVERY_SLOT_SEQUENCE
                   end) as DELIVERY_SLOT_SEQUENCE,
                 (case
                     when L_mi_rec.DELIVERY_SLOT_ID_mi    = 'N'
                     and svc_DELIVERY_SLOT_col(i).action  = CORESVC_DLVY_SLOT.action_mod
                     and s1.DELIVERY_SLOT_ID             is NULL
                     then mt.DELIVERY_SLOT_ID
                     else s1.DELIVERY_SLOT_ID
                   END) as DELIVERY_SLOT_ID
         from (select svc_DELIVERY_SLOT_col(i).DELIVERY_SLOT_DESC as DELIVERY_SLOT_DESC,
                      svc_DELIVERY_SLOT_col(i).DELIVERY_SLOT_SEQUENCE as DELIVERY_SLOT_SEQUENCE,
                      svc_DELIVERY_SLOT_col(i).DELIVERY_SLOT_ID as DELIVERY_SLOT_ID
                 from dual) s1,
               delivery_slot mt
         where mt.DELIVERY_SLOT_ID (+)     = s1.DELIVERY_SLOT_ID ) sq
           ON (st.DELIVERY_SLOT_ID              = sq.DELIVERY_SLOT_ID
               --and svc_DELIVERY_SLOT_col(i).ACTION <> coresvc_item.action_new)
               and svc_DELIVERY_SLOT_col(i).action in (CORESVC_DLVY_SLOT.action_mod,CORESVC_DLVY_SLOT.action_del))
         when matched then
         update set PROCESS_ID              = svc_DELIVERY_SLOT_col(i).PROCESS_ID ,
                    CHUNK_ID                = svc_DELIVERY_SLOT_col(i).CHUNK_ID ,
                    ROW_SEQ                 = svc_DELIVERY_SLOT_col(i).ROW_SEQ ,
                    action                  = svc_DELIVERY_SLOT_col(i).ACTION,
                    PROCESS$STATUS          = svc_DELIVERY_SLOT_col(i).PROCESS$STATUS ,
                    DELIVERY_SLOT_SEQUENCE  = sq.DELIVERY_SLOT_SEQUENCE ,
                    DELIVERY_SLOT_DESC      = sq.DELIVERY_SLOT_DESC ,
                    CREATE_ID               = svc_DELIVERY_SLOT_col(i).CREATE_ID ,
                    CREATE_DATETIME         = svc_DELIVERY_SLOT_col(i).CREATE_DATETIME ,
                    LAST_UPD_ID             = svc_DELIVERY_SLOT_col(i).LAST_UPD_ID ,
                    LAST_UPD_DATETIME       = svc_DELIVERY_SLOT_col(i).LAST_UPD_DATETIME
         when NOT matched then
         insert(PROCESS_ID ,
                CHUNK_ID ,
                ROW_SEQ ,
                ACTION ,
                PROCESS$STATUS ,
                DELIVERY_SLOT_DESC ,
                DELIVERY_SLOT_SEQUENCE ,
                DELIVERY_SLOT_ID ,
                CREATE_ID ,
                CREATE_DATETIME ,
                LAST_UPD_ID ,
                LAST_UPD_DATETIME)
         values(svc_DELIVERY_SLOT_col(i).PROCESS_ID ,
                svc_DELIVERY_SLOT_col(i).CHUNK_ID ,
                svc_DELIVERY_SLOT_col(i).ROW_SEQ ,
                svc_DELIVERY_SLOT_col(i).ACTION ,
                svc_DELIVERY_SLOT_col(i).PROCESS$STATUS ,
                sq.DELIVERY_SLOT_DESC ,
                sq.DELIVERY_SLOT_SEQUENCE ,
                sq.DELIVERY_SLOT_ID ,
                svc_DELIVERY_SLOT_col(i).CREATE_ID ,
                svc_DELIVERY_SLOT_col(i).CREATE_DATETIME ,
                svc_DELIVERY_SLOT_col(i).LAST_UPD_ID ,
                svc_DELIVERY_SLOT_col(i).LAST_UPD_DATETIME);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             DELIVERY_SLOT_sheet,
                             svc_DELIVERY_SLOT_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
      END LOOP;
   END;
END PROCESS_S9T_DELIVERY_SLOT;
--------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DELIVERY_SLOT_TL(I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                       I_process_id IN   SVC_DELIVERY_SLOT_TL.process_id%TYPE) IS

   TYPE svc_delivery_slot_tl_col_TYP is TABLE OF SVC_DELIVERY_SLOT_TL%ROWTYPE;
   L_temp_rec SVC_DELIVERY_SLOT_TL%ROWTYPE;
   svc_delivery_slot_tl_col svc_delivery_slot_tl_col_typ :=NEW svc_delivery_slot_tl_col_typ();
   L_process_id SVC_DELIVERY_SLOT_TL.PROCESS_ID%TYPE;
   L_error BOOLEAN:=FALSE;
   L_default_rec SVC_DELIVERY_SLOT_TL%ROWTYPE;
   cursor C_MANDATORY_IND is
      select DELIVERY_SLOT_DESC_mi,
             DELIVERY_SLOT_ID_mi,
             LANG_mi
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = CORESVC_DLVY_SLOT.template_key
                 and wksht_key         = 'DELIVERY_SLOT_TL'
              ) PIVOT (MAX(mandatory) as mi
                       FOR (column_key) IN ( 'DELIVERY_SLOT_DESC' as DELIVERY_SLOT_DESC,
                                             'DELIVERY_SLOT_ID'   as DELIVERY_SLOT_ID,
                                             'LANG'               as LANG));
   L_mi_rec        C_MANDATORY_IND%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DELIVERY_SLOT_TL';
   L_pk_columns    VARCHAR2(255)  := 'Slot Code, Language';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN ( select DELIVERY_SLOT_DESC_dv,
                       DELIVERY_SLOT_ID_dv,
                       LANG_dv
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key  = CORESVC_DLVY_SLOT.template_key
                          and wksht_key     = 'DELIVERY_SLOT_TL'
                       ) PIVOT (MAX(default_value) as dv
                                FOR (column_key) IN ( 'DELIVERY_SLOT_DESC' as DELIVERY_SLOT_DESC,
                                                      'DELIVERY_SLOT_ID'   as DELIVERY_SLOT_ID,
                                                      'LANG'               as LANG)))
   LOOP
      BEGIN
         L_default_rec.DELIVERY_SLOT_DESC := rec.DELIVERY_SLOT_DESC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_TL_sheet,
                            NULL,
                            'DELIVERY_SLOT_DESC',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.DELIVERY_SLOT_ID := rec.DELIVERY_SLOT_ID_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_TL_sheet,
                             NULL,
                            'DELIVERY_SLOT_ID',
                             NULL,
                            'INV_DEFAULT');
      END;
      BEGIN
         L_default_rec.LANG := rec.LANG_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_TL_sheet,
                             NULL,
                            'LANG',
                             NULL,
                            'INV_DEFAULT');
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN (select UPPER(r.GET_CELL(DELIVERY_SLOT_TL$Action))         as Action,
                      r.GET_CELL(DLVYSLT_TL$DELIVERY_SLOT_DESC)          as DELIVERY_SLOT_DESC,
                      r.GET_CELL(DLVYSLT_TL$DELIVERY_SLOT_ID)            as DELIVERY_SLOT_ID,
                      r.GET_CELL(DELIVERY_SLOT_TL$LANG)                  as LANG,
                      r.get_row_seq()                                    as row_seq
                 from s9t_folder sf,
                      TABLE(sf.s9t_file_obj.sheets) ss,
                      TABLE(ss.s9t_rows) r
                where sf.file_id  = I_file_id
                  and ss.sheet_name = GET_SHEET_NAME_TRANS(DELIVERY_SLOT_TL_sheet)
              )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_error := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_TL_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DELIVERY_SLOT_DESC := rec.DELIVERY_SLOT_DESC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_TL_sheet,
                            rec.row_seq,
                            'DELIVERY_SLOT_DESC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DELIVERY_SLOT_ID := rec.DELIVERY_SLOT_ID;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_TL_sheet,
                            rec.row_seq,
                            'DELIVERY_SLOT_ID',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.lang := rec.lang;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DELIVERY_SLOT_TL_sheet,
                            rec.row_seq,
                            'LANG',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      if rec.action = CORESVC_DLVY_SLOT.action_new then
         L_temp_rec.delivery_slot_desc     := nvl( L_temp_rec.delivery_slot_desc,L_default_rec.delivery_slot_desc);
         L_temp_rec.delivery_slot_id       := nvl( L_temp_rec.delivery_slot_id,L_default_rec.delivery_slot_id);
         L_temp_rec.lang                   := nvl( L_temp_rec.lang,L_default_rec.lang);
      end if;

      if NOT (L_temp_rec.DELIVERY_SLOT_ID is NOT NULL and L_temp_rec.LANG is NOT NULL) then
         WRITE_S9T_ERROR(I_file_id,
                         DELIVERY_SLOT_TL_sheet,
                         rec.row_seq,
                         NULL,
                         NULL,
                         SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));

         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_delivery_slot_tl_col.EXTEND();
         svc_delivery_slot_tl_col(svc_delivery_slot_tl_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;

   BEGIN
      FORALL i IN 1..svc_delivery_slot_tl_col.COUNT SAVE EXCEPTIONS

      merge into svc_delivery_slot_tl st using
         (select (case
                     when L_mi_rec.delivery_slot_desc_mi    = 'N'
                      and svc_delivery_slot_tl_col(i).action   = coresvc_dlvy_slot.action_mod
                      and s1.delivery_slot_desc             is null
                     then mt.delivery_slot_desc
                     else s1.delivery_slot_desc
                   end) as delivery_slot_desc,
                 (case
                     when L_mi_rec.lang_mi    = 'N'
                      and svc_delivery_slot_tl_col(i).action       = coresvc_dlvy_slot.action_mod
                      and s1.lang             is null
                     then mt.lang
                     else s1.lang
                   end) as lang,
                 (case
                     when L_mi_rec.delivery_slot_id_mi    = 'N'
                     and svc_delivery_slot_tl_col(i).action  = coresvc_dlvy_slot.action_mod
                     and s1.delivery_slot_id             is null
                     then mt.delivery_slot_id
                     else s1.delivery_slot_id
                   end) as delivery_slot_id
         from (select svc_delivery_slot_tl_col(i).delivery_slot_desc as delivery_slot_desc,
                      svc_delivery_slot_tl_col(i).lang as lang,
                      svc_delivery_slot_tl_col(i).delivery_slot_id as delivery_slot_id
                 from dual) s1,
               delivery_slot_tl mt
         where mt.lang (+)     = s1.lang
           and mt.delivery_slot_id (+)     = s1.delivery_slot_id) sq
           on (st.lang              = sq.lang
               and st.delivery_slot_id  = sq.delivery_slot_id
               --and svc_delivery_slot_tl_col(i).action <> coresvc_item.action_new)
               and svc_delivery_slot_tl_col(i).action in (coresvc_dlvy_slot.action_mod,coresvc_dlvy_slot.action_del))
         when matched then
         update set process_id              = svc_delivery_slot_tl_col(i).process_id ,
                    chunk_id                = svc_delivery_slot_tl_col(i).chunk_id ,
                    row_seq                 = svc_delivery_slot_tl_col(i).row_seq ,
                    action                  = svc_delivery_slot_tl_col(i).action,
                    process$status          = svc_delivery_slot_tl_col(i).process$status ,
                    delivery_slot_desc      = sq.delivery_slot_desc
         when not matched then
         insert(process_id ,
                chunk_id ,
                row_seq ,
                action ,
                process$status ,
                delivery_slot_desc ,
                lang ,
                delivery_slot_id)
         values(svc_delivery_slot_tl_col(i).process_id ,
                svc_delivery_slot_tl_col(i).chunk_id ,
                svc_delivery_slot_tl_col(i).row_seq ,
                svc_delivery_slot_tl_col(i).action ,
                svc_delivery_slot_tl_col(i).process$status ,
                sq.delivery_slot_desc ,
                sq.lang ,
                sq.delivery_slot_id);
   EXCEPTION
      when DML_ERRORS then
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                                 L_pk_columns);
            end if;
            WRITE_S9T_ERROR( I_file_id,
                             DELIVERY_SLOT_TL_sheet,
                             svc_delivery_slot_tl_col(sql%bulk_exceptions(i).error_index).row_seq,
                             NULL,
                             L_error_code,
                             L_error_msg);
      END LOOP;
   END;
END PROCESS_S9T_DELIVERY_SLOT_TL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                     O_error_count   IN OUT NUMBER,
                     I_file_id       IN     S9T_FOLDER.FILE_ID%TYPE,
                     I_process_id    IN     NUMBER)

RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_DLVY_SLOT.process_s9t';
   L_file s9t_file;
   L_sheets s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR     EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   commit;--to ensure that the record in s9t_folder is commited
   s9t_pkg.ODS2OBJ(I_file_id);
   commit;
   L_file := s9t_pkg.get_obj(I_file_id);
   Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE)= FALSE then
      return FALSE;
   end if;
   s9t_pkg.SAVE_OBJ(l_file);
   if s9t_pkg.VALIDATE_TEMPLATE(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(l_file.template_key,
                                              l_file.user_lang);
      PROCESS_S9T_DELIVERY_SLOT(I_file_id,
                                I_process_id);
      PROCESS_S9T_DELIVERY_SLOT_TL(I_file_id,
                                   I_process_id);
   end if;

   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
   insert into s9t_errors
        values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count = 0 then
       L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status     = L_process_status,
           file_id    = I_file_id
     where process_id = I_process_id;

    commit;

   return TRUE;

EXCEPTION
   when INVALID_FORMAT then
       rollback;
        O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                            NULL,
                                            NULL,
                                            NULL);
        Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
        WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
        O_error_count := Lp_s9t_errors_tab.count();
        FORALL i IN 1..O_error_count
        insert into s9t_errors
           values lp_s9t_errors_tab(i);
        update svc_process_tracker
            set status      = 'PE',
                 file_id     = i_file_id
         where process_id  = i_process_id;
        commit;
        return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
     insert into s9t_errors
          values Lp_s9t_errors_tab(i);

      update svc_process_tracker
     set status = 'PE',
         file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------

FUNCTION POST_EXEC_DELIVERY_SLOT( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_error             IN OUT   BOOLEAN,
                                  I_rec               IN       C_SVC_DELIVERY_SLOT%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64)    := 'CORESVC_DLVY_SLOT.POST_EXEC_DELIVERY_SLOT';
   L_exists         BOOLEAN  := FALSE;
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DELIVERY_SLOT';
BEGIN
   --To check if the DELIVERY_SLOT_SEQUENCE is unique
   if I_rec.delivery_slot_sequence is NOT NULL and
      DLVYSLT_SQL.CHECK_SEQUENCE_UNIQUE(O_error_message,
                                        L_exists) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
      O_error := TRUE;
   end if;
   ---
   if L_exists = TRUE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  'DELIVERY_SLOT_SEQUENCE',
                  'DELV_SEQ_DUPLICATE');
      O_error := TRUE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END POST_EXEC_DELIVERY_SLOT;
--------------------------------------------------------------------------------

FUNCTION EXEC_DELIVERY_SLOT_INS(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error                    IN OUT BOOLEAN,
                                I_delivery_slot_temp_rec   IN     DELIVERY_SLOT%ROWTYPE,
                                I_rec                      IN     C_SVC_DELIVERY_SLOT%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_DLVY_SLOT.EXEC_DELIVERY_SLOT_INS';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'DELIVERY_SLOT';
   L_error   BOOLEAN := FALSE;
BEGIN
   insert
     into delivery_slot
   values I_delivery_slot_temp_rec;


   if POST_EXEC_DELIVERY_SLOT(O_error_message,
                              L_error,
                              I_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
   end if;

   if L_error then
      O_error := TRUE;
      ROLLBACK TO process_dlvyslt_sp;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DELIVERY_SLOT_INS;
--------------------------------------------------------------------------------

FUNCTION EXEC_DELIVERY_SLOT_UPD(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_error                    IN OUT BOOLEAN,
                                I_delivery_slot_temp_rec   IN     DELIVERY_SLOT%ROWTYPE,
                                I_rec                      IN     C_SVC_DELIVERY_SLOT%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) := 'CORESVC_DLVY_SLOT.EXEC_DELIVERY_SLOT_UPD';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE:= 'DELIVERY_SLOT';
   L_error   BOOLEAN := FALSE;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DELIVERY_SLOT_UPD is
      select 'x'
        from delivery_slot
       where delivery_slot_id   = I_delivery_slot_temp_rec.delivery_slot_id
         for update nowait;

BEGIN
   open  C_LOCK_DELIVERY_SLOT_UPD;
   close C_LOCK_DELIVERY_SLOT_UPD;

   update delivery_slot
      set row = I_delivery_slot_temp_rec
    where delivery_slot_id = I_delivery_slot_temp_rec.delivery_slot_id;

   if POST_EXEC_DELIVERY_SLOT(O_error_message,
                              L_error,
                              I_rec) = FALSE then
      WRITE_ERROR(I_rec.process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_rec.chunk_id,
                  L_table,
                  I_rec.row_seq,
                  NULL,
                  O_error_message);
   end if;

   if L_error then
      O_error := TRUE;
      ROLLBACK TO process_dlvyslt_sp;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_delivery_slot_temp_rec.delivery_slot_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_DELIVERY_SLOT_UPD%ISOPEN then
         close C_LOCK_DELIVERY_SLOT_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DELIVERY_SLOT_UPD;
--------------------------------------------------------------------------------

FUNCTION EXEC_DELIVERY_SLOT_DEL(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_delivery_slot_temp_rec   IN     DELIVERY_SLOT%ROWTYPE )
RETURN BOOLEAN IS
   L_program VARCHAR2(64):= 'CORESVC_DLVY_SLOT.EXEC_DELIVERY_SLOT_DEL';
   L_table   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DELIVERY_SLOT';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_DELIVERY_SLOT_DEL is
      select 'x'
        from delivery_slot
       where delivery_slot_id   = I_delivery_slot_temp_rec.delivery_slot_id
         for update nowait;

   cursor C_LOCK_DELIVERY_SLOT_TL_DEL is
      select 'x'
        from delivery_slot_tl
       where delivery_slot_id   = I_delivery_slot_temp_rec.delivery_slot_id
         for update nowait;

BEGIN
   open  C_LOCK_DELIVERY_SLOT_TL_DEL;
   close C_LOCK_DELIVERY_SLOT_TL_DEL;

   open  C_LOCK_DELIVERY_SLOT_DEL;
   close C_LOCK_DELIVERY_SLOT_DEL;

   delete
     from delivery_slot_tl
    where delivery_slot_id = I_delivery_slot_temp_rec.delivery_slot_id;

   delete
     from delivery_slot
    where delivery_slot_id = I_delivery_slot_temp_rec.delivery_slot_id;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                             L_table,
                                             I_delivery_slot_temp_rec.delivery_slot_id,
                                             NULL);
      return FALSE;
   when OTHERS then
      if C_LOCK_DELIVERY_SLOT_DEL%ISOPEN then
         close C_LOCK_DELIVERY_SLOT_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DELIVERY_SLOT_DEL;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DELIVERY_SLOT_TL_INS(O_error_message               IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_delivery_slot_tl_ins_tab    IN       DELIVERY_SLOT_TL_TAB)
RETURN BOOLEAN IS
   L_program   VARCHAR2(64) := 'CORESVC_DLVY_SLOT.EXEC_DELIVERY_SLOT_TL_INS';

BEGIN
   if I_delivery_slot_tl_ins_tab is NOT NULL and I_delivery_slot_tl_ins_tab.count > 0 then
      FORALL i IN 1..I_delivery_slot_tl_ins_tab.COUNT()
         insert into delivery_slot_tl
              values I_delivery_slot_tl_ins_tab(i);
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXEC_DELIVERY_SLOT_TL_INS;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DELIVERY_SLOT_TL_UPD(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_delivery_slot_tl_upd_tab   IN       DELIVERY_SLOT_TL_TAB,
                                   I_delivery_slot_tl_upd_rst   IN       ROW_SEQ_TAB,
                                   I_process_id                 IN       SVC_DELIVERY_SLOT_TL.PROCESS_ID%TYPE,
                                   I_chunk_id                   IN       SVC_DELIVERY_SLOT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_DLVY_SLOT.EXEC_DELIVERY_SLOT_TL_UPD';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DELIVERY_SLOT_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_DELIVERY_SLOT_TL_UPD(I_delivery_slot_id  DELIVERY_SLOT_TL.DELIVERY_SLOT_ID%TYPE,
                                      I_lang              DELIVERY_SLOT_TL.LANG%TYPE) is
      select 'x'
        from delivery_slot_tl
       where delivery_slot_id = I_delivery_slot_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_delivery_slot_tl_upd_tab is NOT NULL and I_delivery_slot_tl_upd_tab.count > 0 then
      for i in I_delivery_slot_tl_upd_tab.FIRST..I_delivery_slot_tl_upd_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_delivery_slot_tl_upd_tab(i).lang);
            L_key_val2 := 'Delivery Slot ID: '||to_char(I_delivery_slot_tl_upd_tab(i).delivery_slot_id);
            open C_LOCK_DELIVERY_SLOT_TL_UPD(I_delivery_slot_tl_upd_tab(i).delivery_slot_id,
                                             I_delivery_slot_tl_upd_tab(i).lang);
            close C_LOCK_DELIVERY_SLOT_TL_UPD;
            
            update delivery_slot_tl
               set delivery_slot_desc = I_delivery_slot_tl_upd_tab(i).delivery_slot_desc,
                   last_update_id = I_delivery_slot_tl_upd_tab(i).last_update_id,
                   last_update_datetime = I_delivery_slot_tl_upd_tab(i).last_update_datetime
             where lang = I_delivery_slot_tl_upd_tab(i).lang
               and delivery_slot_id = I_delivery_slot_tl_upd_tab(i).delivery_slot_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_DELIVERY_SLOT_TL',
                           I_delivery_slot_tl_upd_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_DELIVERY_SLOT_TL_UPD%ISOPEN then
         close C_LOCK_DELIVERY_SLOT_TL_UPD;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_DELIVERY_SLOT_TL_UPD;
----------------------------------------------------------------------------------------------
FUNCTION EXEC_DELIVERY_SLOT_TL_DEL(O_error_message              IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_delivery_slot_tl_del_tab   IN       DELIVERY_SLOT_TL_TAB,
                                   I_delivery_slot_tl_del_rst   IN       ROW_SEQ_TAB,
                                   I_process_id                 IN       SVC_DELIVERY_SLOT_TL.PROCESS_ID%TYPE,
                                   I_chunk_id                   IN       SVC_DELIVERY_SLOT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(64) := 'CORESVC_DLVY_SLOT.EXEC_DELIVERY_SLOT_TL_DEL';
   L_table         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DELIVERY_SLOT_TL';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(RECORD_LOCKED, -54);
   L_key_val1      VARCHAR2(30) := NULL;
   L_key_val2      VARCHAR2(50) := NULL;

    --Cursor to lock the record
   cursor C_LOCK_DELIVERY_SLOT_TL_DEL(I_delivery_slot_id  DELIVERY_SLOT_TL.DELIVERY_SLOT_ID%TYPE,
                                      I_lang           DELIVERY_SLOT_TL.LANG%TYPE) is
      select 'x'
        from delivery_slot_tl
       where delivery_slot_id = I_delivery_slot_id
         and lang = I_lang
         for update nowait;

BEGIN
   if I_delivery_slot_tl_del_tab is NOT NULL and I_delivery_slot_tl_del_tab.count > 0 then
      for i in I_delivery_slot_tl_del_tab.FIRST..I_delivery_slot_tl_del_tab.LAST loop
         BEGIN
            L_key_val1 := 'Lang: '||to_char(I_delivery_slot_tl_del_tab(i).lang);
            L_key_val2 := 'Delivery Slot ID: '||to_char(I_delivery_slot_tl_del_tab(i).delivery_slot_id);
            open C_LOCK_DELIVERY_SLOT_TL_DEL(I_delivery_slot_tl_del_tab(i).delivery_slot_id,
                                          I_delivery_slot_tl_del_tab(i).lang);
            close C_LOCK_DELIVERY_SLOT_TL_DEL;
           
            delete delivery_slot_tl
             where lang = I_delivery_slot_tl_del_tab(i).lang
               and delivery_slot_id = I_delivery_slot_tl_del_tab(i).delivery_slot_id;
         EXCEPTION
            when RECORD_LOCKED then
               O_error_message := SQL_LIB.CREATE_MSG('RECORD_LOCKED',
                                                      L_table,
                                                      L_key_val1,
                                                      L_key_val2);
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.nextval,
                           I_chunk_id,
                           'SVC_DELIVERY_SLOT_TL',
                           I_delivery_slot_tl_del_rst(i),
                           NULL,
                           O_error_message);
         END;
      end loop;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      if C_LOCK_DELIVERY_SLOT_TL_DEL%ISOPEN then
         close C_LOCK_DELIVERY_SLOT_TL_DEL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END EXEC_DELIVERY_SLOT_TL_DEL;
--------------------------------------------------------------------------------

FUNCTION PROCESS_VAL_DLVYSLT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_error         IN OUT BOOLEAN,
                             I_rec           IN     C_SVC_DELIVERY_SLOT%ROWTYPE)
RETURN BOOLEAN IS
   L_exists         BOOLEAN := FALSE;
   L_delete_ok      BOOLEAN := TRUE;
   L_table          SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DELIVERY_SLOT';
   L_program VARCHAR2(64):='CORESVC_DLVY_SLOT.PROCESS_VAL_DLVYSLT';
BEGIN

   --To check if the DELIVERY_SLOT to be deleted is referred in any other forms
   if I_rec.action = action_del then
      if I_rec.delivery_slot_id is NOT NULL and
         DLVYSLT_SQL.CHECK_DELETE(O_error_message,
                                  L_delete_ok,
                                  I_rec.delivery_slot_id) = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     NULL,
                     O_error_message);
         O_error := TRUE;
      end if;

      if L_delete_ok = FALSE then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'DELIVERY_SLOT_ID',
                     O_error_message);
         O_error := TRUE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
  return FALSE;

END PROCESS_VAL_DLVYSLT;
----------------------------------------------------------------------------------------------
FUNCTION PROCESS_DELIVERY_SLOT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id      IN       SVC_DELIVERY_SLOT.PROCESS_ID%TYPE,
                               I_chunk_id        IN       SVC_DELIVERY_SLOT.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program   VARCHAR2(64):='CORESVC_DLVY_SLOT.PROCESS_DELIVERY_SLOT';
   L_error BOOLEAN;
   L_process_error BOOLEAN := FALSE;
   L_delivery_slot_temp_rec DELIVERY_SLOT%ROWTYPE;
   L_table SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE    :='SVC_DELIVERY_SLOT';
BEGIN
   FOR rec IN C_SVC_DELIVERY_SLOT(I_process_id,
                                  I_chunk_id)
   LOOP
      L_error       := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.pk_delivery_slot_rid is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DELIVERY_SLOT_ID',
                     'DELIVERY_SLOT_EXISTS');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.pk_delivery_slot_rid is NULL
         and rec.delivery_slot_id is NOT NULL then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DELIVERY_SLOT_ID',
                     'DELIVERY_SLOT_MISSING');
         L_error :=TRUE;
      end if;
   if rec.action IN (action_new,action_mod)   then
      if NOT(  rec.delivery_slot_desc is NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DELIVERY_SLOT_DESC',
                     'DLVY_SLOT_DESC_IS_NULL');
         L_error :=TRUE;
      end if;

      if NOT(  rec.delivery_slot_sequence  is NOT NULL ) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DELIVERY_SLOT_SEQUENCE',
                     'DLVY_SLOT_SEQ_IS_NULL');
         L_error :=TRUE;
      end if;

      if rec.delivery_slot_sequence is NOT NULL
         and rec.delivery_slot_sequence < 1 then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'DELIVERY_SLOT_SEQUENCE',
                     'GREATER_0');
         L_error :=TRUE;
      end if;
   end if;
      if PROCESS_VAL_DLVYSLT(O_error_message,
                             L_error,
                             rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     O_error_message);
         L_error := TRUE;
      end if;

      if NOT L_error then
         L_delivery_slot_temp_rec.delivery_slot_desc      := rec.delivery_slot_desc;
         L_delivery_slot_temp_rec.delivery_slot_sequence  := rec.delivery_slot_sequence;
         L_delivery_slot_temp_rec.delivery_slot_id        := rec.delivery_slot_id;

         SAVEPOINT process_dlvyslt_sp;

         if rec.action = action_new then
            if EXEC_DELIVERY_SLOT_INS(O_error_message,
                                      L_process_error,
                                      L_delivery_slot_temp_rec,
                                      rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_DELIVERY_SLOT_UPD(O_error_message,
                                      L_process_error,
                                      L_delivery_slot_temp_rec,
                                      rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_DELIVERY_SLOT_DEL( O_error_message,
                                       L_delivery_slot_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           O_error_message);
               L_process_error := TRUE;
            end if;
         end if;
        end if;
   END LOOP;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DELIVERY_SLOT;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_DELIVERY_SLOT_TL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_process_id      IN       SVC_DELIVERY_SLOT_TL.PROCESS_ID%TYPE,
                                  I_chunk_id        IN       SVC_DELIVERY_SLOT_TL.CHUNK_ID%TYPE)
RETURN BOOLEAN IS
   L_program                       VARCHAR2(64) := 'CORESVC_DLVY_SLOT.PROCESS_DELIVERY_SLOT_TL';
   L_table                         SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'SVC_DELIVERY_SLOT_TL';
   L_error_message                 RTK_ERRORS.RTK_TEXT%TYPE;
   L_base_trans_table              SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE := 'DELIVERY_SLOT_TL';
   L_error                         BOOLEAN := FALSE;
   L_process_error                 BOOLEAN := FALSE;
   L_delivery_slot_tl_temp_rec     DELIVERY_SLOT_TL%ROWTYPE;
   L_delivery_slot_tl_upd_rst      ROW_SEQ_TAB;
   L_delivery_slot_tl_del_rst      ROW_SEQ_TAB;

   cursor C_SVC_DELIVERY_SLOT_TL(I_process_id NUMBER,
                                 I_chunk_id   NUMBER) is
      select pk_delivery_slot_tl.rowid    as pk_delivery_slot_tl_rid,
             fk_delivery_slot.rowid       as fk_delivery_slot_rid,
             fk_lang.rowid                as fk_lang_rid,
             st.lang,
             st.delivery_slot_id,
             st.delivery_slot_desc,
             st.process_id,
             st.chunk_id,
             st.row_seq,
             UPPER(st.action)        as action,
             st.process$status
        from svc_delivery_slot_tl  st,
             delivery_slot         fk_delivery_slot,
             delivery_slot_tl      pk_delivery_slot_tl,
             lang                  fk_lang
       where st.process_id       =  I_process_id
         and st.chunk_id         =  I_chunk_id
         and st.delivery_slot_id =  fk_delivery_slot.delivery_slot_id (+)
         and st.lang             =  pk_delivery_slot_tl.lang (+)
         and st.delivery_slot_id =  pk_delivery_slot_tl.delivery_slot_id (+)
         and st.lang             =  fk_lang.lang (+);

   TYPE svc_delivery_slot_tl is TABLE OF C_SVC_DELIVERY_SLOT_TL%ROWTYPE INDEX BY BINARY_INTEGER;
   L_svc_delivery_slot_tl_tab         svc_delivery_slot_tl;

   L_delivery_slot_tl_ins_tab         delivery_slot_tl_tab         := NEW delivery_slot_tl_tab();
   L_delivery_slot_tl_upd_tab         delivery_slot_tl_tab         := NEW delivery_slot_tl_tab();
   L_delivery_slot_tl_del_tab         delivery_slot_tl_tab         := NEW delivery_slot_tl_tab();

BEGIN
   if C_SVC_DELIVERY_SLOT_TL%ISOPEN then
      close C_SVC_DELIVERY_SLOT_TL;
   end if;

   open C_SVC_DELIVERY_SLOT_TL(I_process_id,
                               I_chunk_id);
   LOOP
      fetch C_SVC_DELIVERY_SLOT_TL bulk collect into L_svc_delivery_slot_tl_tab limit LP_bulk_fetch_limit;
      if L_svc_delivery_slot_tl_tab.COUNT > 0 then
         FOR i in L_svc_delivery_slot_tl_tab.FIRST..L_svc_delivery_slot_tl_tab.LAST LOOP
         L_error := FALSE;

            -- check if action is valid
            if L_svc_delivery_slot_tl_tab(i).action is NULL
               or L_svc_delivery_slot_tl_tab(i).action NOT IN (action_new, action_mod, action_del)   then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_delivery_slot_tl_tab(i).row_seq,
                           'ACTION',
                           'INV_ACT');
               L_error :=TRUE;
            end if;

            --check for primary_lang
            if L_svc_delivery_slot_tl_tab(i).lang = LP_primary_lang then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_delivery_slot_tl_tab(i).row_seq,
                           'LANG',
                           'ERR_PRIM_LANG',
                           'W');
               continue;
            end if;

            -- check if primary key values already exist
            if L_svc_delivery_slot_tl_tab(i).action = action_new
               and L_svc_delivery_slot_tl_tab(i).pk_delivery_slot_tl_rid is NOT NULL then
               L_error_message := SQL_LIB.CREATE_MSG('REC_EXISTS_RMS',
                                                     L_base_trans_table,
                                                     NULL,
                                                     NULL);
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_delivery_slot_tl_tab(i).row_seq,
                           NULL,
                           L_error_message);
               L_error :=TRUE;
            end if;

            if L_svc_delivery_slot_tl_tab(i).action IN (action_mod, action_del)
               and L_svc_delivery_slot_tl_tab(i).lang is NOT NULL
               and L_svc_delivery_slot_tl_tab(i).delivery_slot_id is NOT NULL
               and L_svc_delivery_slot_tl_tab(i).pk_delivery_slot_tl_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_delivery_slot_tl_tab(i).row_seq,
                            NULL,
                           'NO_RECORD_UPD_DEL');
               L_error :=TRUE;
            end if;

            -- check for FK
            if L_svc_delivery_slot_tl_tab(i).action = action_new
               and L_svc_delivery_slot_tl_tab(i).delivery_slot_id is NOT NULL
               and L_svc_delivery_slot_tl_tab(i).fk_delivery_slot_rid is NULL then
               WRITE_ERROR( I_process_id,
                            svc_admin_upld_er_seq.NEXTVAL,
                            I_chunk_id,
                            L_table,
                            L_svc_delivery_slot_tl_tab(i).row_seq,
                            'DELIVERY_SLOT_ID',
                            'INV_DELV_SLOT_ID');
               L_error :=TRUE;
            end if;

            if L_svc_delivery_slot_tl_tab(i).action = action_new
               and L_svc_delivery_slot_tl_tab(i).lang is NOT NULL
               and L_svc_delivery_slot_tl_tab(i).fk_lang_rid is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_delivery_slot_tl_tab(i).row_seq,
                           'LANG',
                           'LANG_EXIST');
               L_error :=TRUE;
            end if;

            --check for required fields
            if L_svc_delivery_slot_tl_tab(i).delivery_slot_id is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_delivery_slot_tl_tab(i).row_seq,
                           'DELIVERY_SLOT_ID',
                           'MUST_ENTER_FIELD');
               L_error :=TRUE;
            end if;

            if L_svc_delivery_slot_tl_tab(i).action IN (action_new, action_mod) and L_svc_delivery_slot_tl_tab(i).delivery_slot_desc is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_delivery_slot_tl_tab(i).row_seq,
                           'DELIVERY_SLOT_DESC',
                           'DLVY_SLOT_DESC_IS_NULL');
               L_error :=TRUE;
            end if;

            if L_svc_delivery_slot_tl_tab(i).lang is NULL then
               WRITE_ERROR(I_process_id,
                           svc_admin_upld_er_seq.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           L_svc_delivery_slot_tl_tab(i).row_seq,
                           'LANG',
                           'MUST_ENTER_LANG');
               L_error :=TRUE;
            end if;

            if NOT L_error then
               L_delivery_slot_tl_temp_rec.lang := L_svc_delivery_slot_tl_tab(i).lang;
               L_delivery_slot_tl_temp_rec.delivery_slot_id := L_svc_delivery_slot_tl_tab(i).delivery_slot_id;
               L_delivery_slot_tl_temp_rec.delivery_slot_desc := L_svc_delivery_slot_tl_tab(i).delivery_slot_desc;
               L_delivery_slot_tl_temp_rec.create_datetime := SYSDATE;
               L_delivery_slot_tl_temp_rec.create_id := GET_USER;
               L_delivery_slot_tl_temp_rec.last_update_datetime := SYSDATE;
               L_delivery_slot_tl_temp_rec.last_update_id := GET_USER;

               if L_svc_delivery_slot_tl_tab(i).action = action_new then
                  L_delivery_slot_tl_ins_tab.extend;
                  L_delivery_slot_tl_ins_tab(L_delivery_slot_tl_ins_tab.count()) := L_delivery_slot_tl_temp_rec;
               end if;

               if L_svc_delivery_slot_tl_tab(i).action = action_mod then
                  L_delivery_slot_tl_upd_tab.extend;
                  L_delivery_slot_tl_upd_tab(L_delivery_slot_tl_upd_tab.count()) := L_delivery_slot_tl_temp_rec;
                  L_delivery_slot_tl_upd_rst(L_delivery_slot_tl_upd_tab.count()) := L_svc_delivery_slot_tl_tab(i).row_seq;
               end if;

               if L_svc_delivery_slot_tl_tab(i).action = action_del then
                  L_delivery_slot_tl_del_tab.extend;
                  L_delivery_slot_tl_del_tab(L_delivery_slot_tl_del_tab.count()) := L_delivery_slot_tl_temp_rec;
                  L_delivery_slot_tl_del_rst(L_delivery_slot_tl_del_tab.count()) := L_svc_delivery_slot_tl_tab(i).row_seq;
               end if;
            end if;
         END LOOP;
      end if;
      EXIT WHEN C_SVC_DELIVERY_SLOT_TL%NOTFOUND;
   END LOOP;
   close C_SVC_DELIVERY_SLOT_TL;

   if EXEC_DELIVERY_SLOT_TL_INS(O_error_message,
                                L_delivery_slot_tl_ins_tab) = FALSE then
      return FALSE;
   end if;

   if EXEC_DELIVERY_SLOT_TL_UPD(O_error_message,
                                L_delivery_slot_tl_upd_tab,
                                L_delivery_slot_tl_upd_rst,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   if EXEC_DELIVERY_SLOT_TL_DEL(O_error_message,
                                L_delivery_slot_tl_del_tab,
                                L_delivery_slot_tl_del_rst,
                                I_process_id,
                                I_chunk_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      if C_SVC_DELIVERY_SLOT_TL%ISOPEN then
         close C_SVC_DELIVERY_SLOT_TL;
      end if;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DELIVERY_SLOT_TL;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
       from svc_delivery_slot_tl
      where process_id = I_process_id;

   delete
       from svc_delivery_slot
      where process_id = I_process_id;
END;
--------------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_DLVY_SLOT.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
   L_err_count        VARCHAR2(1);
   L_warn_count       VARCHAR2(1);

   cursor C_GET_ERR_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'E';

   cursor C_GET_WARN_COUNT is
      select 'x'
        from svc_admin_upld_er
       where process_id = I_process_id
         and error_type = 'W';

BEGIN
   LP_primary_lang := LANGUAGE_SQL.GET_PRIMARY_LANGUAGE;
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_DELIVERY_SLOT(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE then
      return FALSE;
   end if;
   if PROCESS_DELIVERY_SLOT_TL(O_error_message,
                               I_process_id,
                               I_chunk_id)=FALSE then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   FORALL i IN 1..O_error_count
      insert into SVC_ADMIN_UPLD_ER
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   open  c_get_err_count;
   fetch c_get_err_count into L_err_count;
   close c_get_err_count;

   open  c_get_warn_count;
   fetch c_get_warn_count into L_warn_count;
   close c_get_warn_count;

   if L_err_count is NOT NULL then
      L_process_status := 'PE';
   elsif L_warn_count is NOT NULL then
      L_process_status := 'PW';
   else
      L_process_status := 'PS';
   end if;

   update svc_process_tracker
        set status = (CASE
                        when status = 'PE'
                        then 'PE'
                    else L_process_status
                    END),
          action_date = sysdate
    where process_id = I_process_id;
   --- Clear staging tables for this process_id
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;
   return TRUE;
exception
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
--------------------------------------------------------------------------------
END CORESVC_DLVY_SLOT;
/