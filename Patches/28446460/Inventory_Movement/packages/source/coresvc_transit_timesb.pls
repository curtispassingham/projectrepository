create or replace PACKAGE BODY CORESVC_TRANSIT_TIMES AS
   cursor C_SVC_TRANSIT_TIMES(I_process_id NUMBER,
                              I_chunk_id NUMBER) is
     select uk_transit_times.rowid             as uk_transit_times_rid,
            uk_transit_times.transit_times_id  as transit_times_id,
            st.rowid                           as st_rid,
            st.transit_time,
            st.destination_type,
            st.origin_type,
            st.destination,
            st.origin,
            st.subclass,
            st.class,
            uk_transit_times.class             as old_class,
            uk_transit_times.subclass          as old_subclass,
            st.dept,
            st.process_id,
            st.row_seq,
            st.chunk_id,
            upper(st.action)                   as action,
            st.process$status
       from svc_transit_times st,
            transit_times uk_transit_times
      where st.process_id         = I_process_id
        and st.chunk_id           = I_chunk_id
        and NVL(st.class,-1)      = NVL(uk_transit_times.class (+),-1)
        and NVL(st.subclass,-1)   = NVL(uk_transit_times.subclass (+),-1)
        and st.dept               = uk_transit_times.dept (+)
        and st.origin             = uk_transit_times.origin (+)
        and st.destination        = uk_transit_times.destination (+)
        and st.origin_type        = uk_transit_times.origin_type (+)
        and st.destination_type   = uk_transit_times.destination_type (+);
        
   TYPE errors_tab_typ     IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab                       errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF S9T_ERRORS%ROWTYPE;
   LP_s9t_errors_tab                   s9t_errors_tab_typ;
   LP_system_options_row               SYSTEM_OPTIONS%ROWTYPE;

----------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.exists(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
   end if;
END GET_SHEET_NAME_TRANS;
--------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   Lp_s9t_errors_tab(Lp_s9t_errors_tab.count()).ERROR_KEY            :=(
    CASE
       WHEN I_sqlcode IS NULL THEN
          I_sqlerrm
       ELSE
          'IIND-ORA-'||lpad(I_sqlcode,5,'0')
       END
   );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.PROCESS_ID%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.ERROR_SEQ%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.CHUNK_ID%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.ROW_SEQ%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.COLUMN_NAME%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.ERROR_MSG%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
-------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets s9t_pkg.names_map_typ;
   TRANSIT_TIMES_cols s9t_pkg.names_map_typ;
BEGIN
   
   L_sheets                             :=s9t_pkg.get_sheet_names(I_file_id);
   TRANSIT_TIMES_cols                   :=s9t_pkg.get_col_names(I_file_id,TRANSIT_TIMES_sheet);
   TRANSIT_TIMES$Action                 := TRANSIT_TIMES_cols('ACTION');
   TRANSIT_TIMES$DEPT                   := TRANSIT_TIMES_cols('DEPT');
   TRANSIT_TIMES$CLASS                  := TRANSIT_TIMES_cols('CLASS');
   TRANSIT_TIMES$SUBCLASS               := TRANSIT_TIMES_cols('SUBCLASS');
   TRANSIT_TIMES$ORIGIN                 := TRANSIT_TIMES_cols('ORIGIN');
   TRANSIT_TIMES$ORIGIN_TYPE            := TRANSIT_TIMES_cols('ORIGIN_TYPE');
   TRANSIT_TIMES$DESTINATION            := TRANSIT_TIMES_cols('DESTINATION');
   TRANSIT_TIMES$DESTINATION_TYPE       := TRANSIT_TIMES_cols('DESTINATION_TYPE');
   TRANSIT_TIMES$TRANSIT_TIME           := TRANSIT_TIMES_cols('TRANSIT_TIME');
   
END POPULATE_NAMES;
--------------------------------------------------------------------------------------------
PROCEDURE POPULATE_TRANSIT_TIMES( I_file_id   IN   NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = TRANSIT_TIMES_sheet )
   select s9t_row(s9t_cells(coresvc_transit_times.action_mod ,
                           transit_times_id,
                            dept,
                            class,
                            subclass,
                            origin,
                            origin_type,
                            destination,
                            destination_type,
                            transit_time))
     from transit_times ;
END POPULATE_TRANSIT_TIMES;
----------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id   IN OUT   NUMBER) IS
   L_file S9T_FILE;
   L_file_name S9T_FOLDER.FILE_NAME%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := L_file_name;
   L_file.template_key := template_key;
   L_file.user_lang    := GET_USER_LANG;
   L_file.add_sheet(TRANSIT_TIMES_sheet);
  L_file.sheets(L_file.get_sheet_index(TRANSIT_TIMES_sheet)).column_headers := s9t_cells( 'ACTION',
                                                                                           'TRANSIT_TIMES_ID',
                                                                                           'DEPT',
                                                                                           'CLASS',
                                                                                           'SUBCLASS',
                                                                                           'ORIGIN',
                                                                                           'ORIGIN_TYPE',
                                                                                           'DESTINATION',
                                                                                           'DESTINATION_TYPE',
                                                                                           'TRANSIT_TIME');
   S9T_PKG.SAVE_OBJ(L_file);
END INIT_S9T;
-------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_TRANSIT_TIMES.CREATE_S9T';
   L_file s9t_file;
BEGIN
   INIT_S9T(O_file_id);
   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE then
      return FALSE;
   end if;
   if I_template_only_ind = 'N' then
      POPULATE_TRANSIT_TIMES(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE then
      return FALSE;
   end if;
   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_TRANSIT_TIMES( I_file_id    IN   S9T_FOLDER.FILE_ID%TYPE,
                                     I_process_id IN   SVC_TRANSIT_TIMES.PROCESS_ID%TYPE) IS

   TYPE svc_transit_times_col_typ IS TABLE OF SVC_TRANSIT_TIMES%ROWTYPE;
   L_temp_rec                                 SVC_TRANSIT_TIMES%ROWTYPE;
   svc_transit_times_col                      svc_transit_times_col_typ              :=NEW svc_transit_times_col_typ();
   L_process_id                               SVC_TRANSIT_TIMES.PROCESS_ID%TYPE;
   L_error                                    BOOLEAN                                :=FALSE;
   L_default_rec                              SVC_TRANSIT_TIMES%ROWTYPE;

   cursor C_MANDATORY_IND is
      select TRANSIT_TIME_mi,
             DESTINATION_TYPE_mi,
             ORIGIN_TYPE_mi,
             DESTINATION_mi,
             ORIGIN_mi,
             SUBCLASS_mi,
             CLASS_mi,
             DEPT_mi,
             TRANSIT_TIMES_ID_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key                              = template_key
                 and wksht_key                                 = 'TRANSIT_TIMES'
              ) PIVOT (MAX(mandatory) AS mi
                       FOR (column_key) IN ('TRANSIT_TIME'     AS TRANSIT_TIME,
                                            'DESTINATION_TYPE' AS DESTINATION_TYPE,
                                            'ORIGIN_TYPE'      AS ORIGIN_TYPE,
                                            'DESTINATION'      AS DESTINATION,
                                            'ORIGIN'           AS ORIGIN,
                                            'SUBCLASS'         AS SUBCLASS,
                                            'CLASS'            AS CLASS,
                                            'DEPT'             AS DEPT,
                                            'TRANSIT_TIMES_ID' AS TRANSIT_TIMES_ID,
                                             null              as dummy));
      L_mi_rec c_mandatory_ind%ROWTYPE;
      dml_errors EXCEPTION;
      PRAGMA exception_init(dml_errors, -24381);
      L_table         VARCHAR2(30)   := 'SVC_TRANSIT_TIMES';
      L_pk_columns    VARCHAR2(255)  := 'Department,Point of Origin,Point of Origin Type,Point of Destination,Point of Destination Type';
      L_error_code    NUMBER;
      L_error_msg     RTK_ERRORS.RTK_TEXT%TYPE;
BEGIN
  
  -- Get default values.
   FOR rec IN (select
                       TRANSIT_TIME_dv,
                       DESTINATION_TYPE_dv,
                       ORIGIN_TYPE_dv,
                       DESTINATION_dv,
                       ORIGIN_dv,
                       SUBCLASS_dv,
                       CLASS_dv,
                       DEPT_dv,
                       TRANSIT_TIMES_ID_dv,
                          null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key                                  = template_key
                          and wksht_key                                     = 'TRANSIT_TIMES'
                       ) PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN (
                                                      'TRANSIT_TIME'     AS TRANSIT_TIME,
                                                      'DESTINATION_TYPE' AS DESTINATION_TYPE,
                                                      'ORIGIN_TYPE'      AS ORIGIN_TYPE,
                                                      'DESTINATION'      AS DESTINATION,
                                                      'ORIGIN'           AS ORIGIN,
                                                      'SUBCLASS'         AS SUBCLASS,
                                                      'CLASS'            AS CLASS,
                                                      'DEPT'             AS DEPT,
                                                      'TRANSIT_TIMES_ID' AS TRANSIT_TIMES_ID,
                                                       NULL              AS dummy)))
   LOOP
      BEGIN
         L_default_rec.TRANSIT_TIME := rec.TRANSIT_TIME_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TRANSIT_TIMES ' ,
                            NULL,
                           'TRANSIT_TIME ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DESTINATION_TYPE := rec.DESTINATION_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TRANSIT_TIMES ' ,
                            NULL,
                           'DESTINATION_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.ORIGIN_TYPE := rec.ORIGIN_TYPE_dv;
      EXCEPTION
        when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TRANSIT_TIMES ' ,
                            NULL,
                           'ORIGIN_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DESTINATION := rec.DESTINATION_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TRANSIT_TIMES ' ,
                            NULL,
                          'DESTINATION ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.ORIGIN := rec.ORIGIN_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TRANSIT_TIMES ' ,
                            NULL,
                           'ORIGIN ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.SUBCLASS := rec.SUBCLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TRANSIT_TIMES ' ,
                            NULL,
                           'SUBCLASS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.CLASS := rec.CLASS_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TRANSIT_TIMES ' ,
                            NULL,
                           'CLASS ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.DEPT := rec.DEPT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'TRANSIT_TIMES ' ,
                            NULL,
                           'DEPT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;

   END LOOP;

   
--Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into L_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select r.get_cell(TRANSIT_TIMES$Action)                 as Action,
          r.get_cell(TRANSIT_TIMES$TRANSIT_TIME)           as TRANSIT_TIME,
          r.get_cell(TRANSIT_TIMES$DESTINATION_TYPE)       as DESTINATION_TYPE,
          r.get_cell(TRANSIT_TIMES$ORIGIN_TYPE)            as ORIGIN_TYPE,
          r.get_cell(TRANSIT_TIMES$DESTINATION)            as DESTINATION,
          r.get_cell(TRANSIT_TIMES$ORIGIN)                 as ORIGIN,
          r.get_cell(TRANSIT_TIMES$SUBCLASS)               as SUBCLASS,
          r.get_cell(TRANSIT_TIMES$CLASS)                  as CLASS,
          r.get_cell(TRANSIT_TIMES$DEPT)                   as DEPT,
          r.get_cell(TRANSIT_TIMES$TRANSIT_TIMES_ID)       as TRANSIT_TIMES_ID,
          r.get_row_seq()                                  as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(TRANSIT_TIMES_sheet)
  )
   LOOP
      L_temp_rec                   := NULL;
      L_temp_rec.process_id        := I_process_id;
      L_temp_rec.chunk_id          := 1;
      L_temp_rec.row_seq           := rec.row_seq;
      L_temp_rec.process$status    := 'N';
      L_temp_rec.create_id         := GET_USER;
      L_temp_rec.last_upd_id       := GET_USER;
      L_temp_rec.create_datetime   := SYSDATE;
      L_temp_rec.last_upd_datetime := SYSDATE;
      L_error                      := FALSE;
      BEGIN
         L_temp_rec.Action := rec.Action;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.TRANSIT_TIME := rec.TRANSIT_TIME;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            'TRANSIT_TIME',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DESTINATION_TYPE := rec.DESTINATION_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            'DESTINATION_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ORIGIN_TYPE := rec.ORIGIN_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            'ORIGIN_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DESTINATION := rec.DESTINATION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            'DESTINATION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.ORIGIN := rec.ORIGIN;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            'ORIGIN',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SUBCLASS := rec.SUBCLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            'SUBCLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.CLASS := rec.CLASS;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            'CLASS',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEPT := rec.DEPT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            TRANSIT_TIMES_sheet,
                            rec.row_seq,
                            'DEPT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_TRANSIT_TIMES.action_new then
         L_temp_rec.TRANSIT_TIME     := NVL( L_temp_rec.TRANSIT_TIME,L_default_rec.TRANSIT_TIME);
         L_temp_rec.DESTINATION_TYPE := NVL( L_temp_rec.DESTINATION_TYPE,L_default_rec.DESTINATION_TYPE);
         L_temp_rec.ORIGIN_TYPE      := NVL( L_temp_rec.ORIGIN_TYPE,L_default_rec.ORIGIN_TYPE);
         L_temp_rec.DESTINATION      := NVL( L_temp_rec.DESTINATION,L_default_rec.DESTINATION);
         L_temp_rec.ORIGIN           := NVL( L_temp_rec.ORIGIN,L_default_rec.ORIGIN);
         L_temp_rec.SUBCLASS         := NVL( L_temp_rec.SUBCLASS,L_default_rec.SUBCLASS);
         L_temp_rec.CLASS            := NVL( L_temp_rec.CLASS,L_default_rec.CLASS);
         L_temp_rec.DEPT             := NVL( L_temp_rec.DEPT,L_default_rec.DEPT);
      end if;
      
      if NOT (L_temp_rec.dept is NOT NULL
              and L_temp_rec.origin is NOT NULL
              and L_temp_rec.destination is NOT NULL
              and L_temp_rec.origin_type is NOT NULL
              and L_temp_rec.destination_type is NOT NULL )then

         WRITE_S9T_ERROR( I_file_id,
                       TRANSIT_TIMES_sheet,
                       rec.row_seq,
                       NULL,
                       NULL,
                       SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;

      if NOT L_error then
         svc_transit_times_col.extend();
         svc_transit_times_col(svc_transit_times_col.COUNT()):=l_temp_rec;
      end if;
      
   END LOOP;
   
   BEGIN
      
      forall i IN 1..svc_transit_times_col.COUNT SAVE EXCEPTIONS

      merge into SVC_TRANSIT_TIMES st
      using(select
                  (case
                   when L_mi_rec.TRANSIT_TIME_mi    = 'N'
                    and svc_transit_times_col(i).action = CORESVC_TRANSIT_TIMES.action_mod
                    and s1.TRANSIT_TIME IS NULL
                   then mt.TRANSIT_TIME
                   else s1.TRANSIT_TIME
                   end) AS TRANSIT_TIME,

                  (case
                   when L_mi_rec.DESTINATION_TYPE_mi    = 'N'
                    and svc_transit_times_col(i).action = CORESVC_TRANSIT_TIMES.action_mod
                    and s1.DESTINATION_TYPE IS NULL
                   then mt.DESTINATION_TYPE
                   else s1.DESTINATION_TYPE
                   end) AS DESTINATION_TYPE,

                  (case
                   when L_mi_rec.ORIGIN_TYPE_mi    = 'N'
                    and svc_transit_times_col(i).action = CORESVC_TRANSIT_TIMES.action_mod
                    and s1.ORIGIN_TYPE IS NULL
                   then mt.ORIGIN_TYPE
                   else s1.ORIGIN_TYPE
                   end) AS ORIGIN_TYPE,

                  (case
                   when L_mi_rec.DESTINATION_mi    = 'N'
                    and svc_transit_times_col(i).action = CORESVC_TRANSIT_TIMES.action_mod
                    and s1.DESTINATION IS NULL
                   then mt.DESTINATION
                   else s1.DESTINATION
                   end) AS DESTINATION,

                  (case
                   when L_mi_rec.ORIGIN_mi    = 'N'
                    and svc_transit_times_col(i).action = CORESVC_TRANSIT_TIMES.action_mod
                    and s1.ORIGIN IS NULL
                   then mt.ORIGIN
                   else s1.ORIGIN
                   end) AS ORIGIN,

                  (case
                   when L_mi_rec.SUBCLASS_mi    = 'N'
                    and svc_transit_times_col(i).action = CORESVC_TRANSIT_TIMES.action_mod
                    and s1.SUBCLASS IS NULL
                   then mt.SUBCLASS
                   else s1.SUBCLASS
                   end) AS SUBCLASS,

                  (case
                   when L_mi_rec.CLASS_mi    = 'N'
                    and svc_transit_times_col(i).action = CORESVC_TRANSIT_TIMES.action_mod
                    and s1.CLASS IS NULL
                   then mt.CLASS
                   else s1.CLASS
                   end) AS CLASS,

                  (case
                   when L_mi_rec.DEPT_mi    = 'N'
                    and svc_transit_times_col(i).action = CORESVC_TRANSIT_TIMES.action_mod
                   and s1.DEPT IS NULL
                   then mt.DEPT
                   else s1.DEPT
                   end) AS DEPT,

                  null as dummy
              from (select svc_transit_times_col(i).TRANSIT_TIME AS TRANSIT_TIME,
                           svc_transit_times_col(i).DESTINATION_TYPE AS DESTINATION_TYPE,
                           svc_transit_times_col(i).ORIGIN_TYPE AS ORIGIN_TYPE,
                           svc_transit_times_col(i).DESTINATION AS DESTINATION,
                           svc_transit_times_col(i).ORIGIN AS ORIGIN,
                           svc_transit_times_col(i).SUBCLASS AS SUBCLASS,
                           svc_transit_times_col(i).CLASS AS CLASS,
                           svc_transit_times_col(i).DEPT AS DEPT,
                           null as dummy
                      from dual ) s1,
            TRANSIT_TIMES mt
             where
                  mt.origin_type (+)             = s1.origin_type   and
                  mt.origin (+)                  = s1.origin   and
                  mt.destination_type (+)        = s1.destination_type   and
                  mt.destination (+)             = s1.destination   and
                  mt.DEPT (+)                    = s1.DEPT and
                  nvl(mt.class (+),-1)           = nvl(s1.class,-1) and
                  nvl(mt.subclass (+),-1)        = nvl(s1.subclass,-1))sq
                on (st.DEPT                   = sq.DEPT and
                    nvl(st.class (+),-1)      = nvl(sq.class,-1) and
                    nvl(st.subclass (+),-1)   = nvl(sq.subclass,-1) and
                    st.origin_type            = sq.origin_type and
                    st.origin                 = sq.origin and
                    st.destination_type       = sq.destination_type and
                    st.destination            = sq.destination and
                    svc_transit_times_col(i).ACTION IN (CORESVC_TRANSIT_TIMES.action_mod,CORESVC_TRANSIT_TIMES.action_del))
      when matched then
      update
         set process_id        = svc_transit_times_col(i).process_id ,
             chunk_id          = svc_transit_times_col(i).chunk_id ,
             row_seq           = svc_transit_times_col(i).row_seq ,
             action            = svc_transit_times_col(i).action ,
             process$status    = svc_transit_times_col(i).process$status ,
             transit_time      = sq.transit_time,
             create_id         = svc_transit_times_col(i).create_id ,
             create_datetime   = svc_transit_times_col(i).create_datetime ,
             last_upd_id       = svc_transit_times_col(i).last_upd_id ,
             last_upd_datetime = svc_transit_times_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             transit_time ,
             destination_type ,
             origin_type ,
             destination ,
             origin ,
             subclass ,
             class ,
             dept ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_transit_times_col(i).process_id ,
             svc_transit_times_col(i).chunk_id ,
             svc_transit_times_col(i).row_seq ,
             svc_transit_times_col(i).action ,
             svc_transit_times_col(i).process$status ,
             sq.transit_time ,
             sq.destination_type ,
             sq.origin_type ,
             sq.destination ,
             sq.origin ,
             sq.subclass ,
             sq.class ,
             sq.dept ,
             svc_transit_times_col(i).create_id ,
             svc_transit_times_col(i).create_datetime ,
             svc_transit_times_col(i).last_upd_id ,
             svc_transit_times_col(i).last_upd_datetime );
   
   EXCEPTION
      when DML_ERRORS then
         
         FOR i IN 1..sql%bulk_exceptions.COUNT
         LOOP
            L_error_code:=sql%bulk_exceptions(i).error_code;
            if L_error_code=1 then
               L_error_code:=NULL;
               L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',L_pk_columns);
            end if;
            
            WRITE_S9T_ERROR( I_file_id,
                            TRANSIT_TIMES_sheet,
                            svc_transit_times_col(sql%bulk_exceptions(i).error_index).row_seq,
                            NULL,
                            L_error_code,
                            L_error_msg);
         END LOOP;
   END;
END PROCESS_S9T_TRANSIT_TIMES;
------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count     IN OUT   NUMBER,
                      I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                      I_process_id      IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_TRANSIT_TIMES.PROCESS_S9T';
   L_file           S9T_FILE;
   L_sheets         S9T_PKG.NAMES_MAP_TYP;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT, -31011);
   MAX_CHAR	    EXCEPTION;
   PRAGMA	    EXCEPTION_INIT(MAX_CHAR, -01706);   
BEGIN
   
   COMMIT;

   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   
   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE then
      return FALSE;
   end if;
   
   s9t_pkg.save_obj(L_file);
   
   if s9t_pkg.validate_template(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      
      PROCESS_S9T_TRANSIT_TIMES(I_file_id,I_process_id);

   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into s9t_errors
      values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
     
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;

   COMMIT;
   
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert
           into s9t_errors
          values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = i_file_id
       where process_id   = i_process_id;
      commit;
      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
	 insert into s9t_errors
	      values Lp_s9t_errors_tab(i);

      update svc_process_tracker
	 set status = 'PE',
	     file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;
      
   when OTHERS then
      
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------------
FUNCTION PROCESS_TRANSIT_TIMES_VAL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_error           IN OUT   BOOLEAN,
                                   I_rec             IN       C_SVC_TRANSIT_TIMES%ROWTYPE)
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                       := 'CORESVC_TRANSIT_TIMES.PROCESS_TRANSIT_TIMES_VAL';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_TRANSIT_TIMES';
   L_sups_rec               V_SUPS%ROWTYPE;
   L_sup_access_exists      BOOLEAN;
   L_desc                   VARCHAR2(240)                      := NULL;
   L_exists                 BOOLEAN;

BEGIN
   if I_rec.origin_type=I_rec.destination_type then
      if I_rec.origin=I_rec.destination then
         WRITE_ERROR(I_rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_rec.chunk_id,
                     L_table,
                     I_rec.row_seq,
                     'Point of Origin,Point of Destination',
                     'TRANTIME_SAME_LOC');
         O_error:=TRUE;
      end if;
   end if;

   if I_rec.origin is NOT NULL then
      if I_rec.origin_type = 'SU' then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(O_error_message,
                                                      L_sup_access_exists,
                                                      L_sups_rec,
                                                      I_rec.origin) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        NULL,
                        O_error_message);
            O_error:=TRUE;
         end if;
          
         if L_sup_access_exists = FALSE then
            if L_sups_rec.sup_name is NULL and
               LP_system_options_row.supplier_sites_ind = 'Y' then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'ORIGIN',
                           'INV_SUPPLIER_SITE');
               O_error:=TRUE;
            else
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'ORIGIN',
                           O_error_message);
               O_error:=TRUE;
            end if;
         end if;
        if LP_system_options_row.supplier_sites_ind = 'Y' then
            if L_sups_rec.supplier_parent is NULL then
               WRITE_ERROR(I_rec.process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_rec.chunk_id,
                           L_table,
                           I_rec.row_seq,
                           'ORIGIN',
                           'INV_SUPPLIER_SITE');
               O_error:=TRUE;
           end if;
         end if;
      end if;
      
      if I_rec.origin_type = 'ST' then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(O_error_message,
                                                   L_exists,
                                                   L_desc,
                                                   I_rec.origin) = FALSE
            or L_exists = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'ORIGIN_TYPE',
                        O_error_message);
            O_error:=TRUE;
         end if;
      end if;
      
      if I_rec.origin_type = 'WH' then
         if WH_ATTRIB_SQL.GET_PWH_NAME(O_error_message,
                                       I_rec.origin,
                                       L_desc) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'ORIGIN_TYPE',
                        O_error_message);
            O_error:=TRUE;
         end if;
      end if;
      if I_rec.destination_type = 'ST' then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(O_error_message,
                                                   L_exists,
                                                   L_desc,
                                                   I_rec.destination) = FALSE
            or L_exists = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'DESTINATION_TYPE',
                        O_error_message);
            O_error:=TRUE;
         end if;
      end if;
      
      if I_rec.destination_type = 'WH' then
         if WH_ATTRIB_SQL.GET_PWH_NAME(O_error_message,
                                       I_rec.destination,
                                       L_desc) = FALSE then
            WRITE_ERROR(I_rec.process_id,
                        SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                        I_rec.chunk_id,
                        L_table,
                        I_rec.row_seq,
                        'DESTINATION_TYPE',
                        O_error_message);
            O_error:=TRUE;
         end if;
      end if;
      
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TRANSIT_TIMES_VAL;
------------------------------------------------------------------------------
FUNCTION EXEC_TRANSIT_TIMES_INS(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_transit_times_temp_rec   IN       TRANSIT_TIMES%ROWTYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                       := 'CORESVC_TRANSIT_TIMES.EXEC_TRANSIT_TIMES_INS';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_TRANSIT_TIMES';

BEGIN

   insert
     into transit_times
   values I_transit_times_temp_rec;
   return TRUE;
EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TRANSIT_TIMES_INS;
-----------------------------------------------------------------------
FUNCTION EXEC_TRANSIT_TIMES_UPD(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_transit_times_temp_rec   IN       TRANSIT_TIMES%ROWTYPE)
RETURN BOOLEAN IS

   L_program     VARCHAR2(64)                       := 'CORESVC_TRANSIT_TIMES.EXEC_TRANSIT_TIMES_UPD';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_TRANSIT_TIMES';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_TRANSIT_TIMES is
      select 'X'
        from transit_times
       where nvl(class,-1)            = nvl(I_transit_times_temp_rec.class,-1)
         and nvl(subclass,-1)         = nvl(I_transit_times_temp_rec.subclass,-1)
         and dept             = I_transit_times_temp_rec.dept
         and origin           = I_transit_times_temp_rec.origin
         and destination      = I_transit_times_temp_rec.destination
         and origin_type      = I_transit_times_temp_rec.origin_type
         and destination_type = I_transit_times_temp_rec.destination_type
         for update nowait;

BEGIN

   open C_TRANSIT_TIMES;
   close C_TRANSIT_TIMES;

   update transit_times
      set transit_time = I_transit_times_temp_rec.transit_time
    where nvl(class,-1)            = nvl(I_transit_times_temp_rec.class,-1)
      and nvl(subclass,-1)         = nvl(I_transit_times_temp_rec.subclass,-1)
      and dept             = I_transit_times_temp_rec.dept
      and origin           = I_transit_times_temp_rec.origin
      and destination      = I_transit_times_temp_rec.destination
      and origin_type      = I_transit_times_temp_rec.origin_type
      and destination_type = I_transit_times_temp_rec.destination_type;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'TRANSIT_TIMES',
                                                                I_transit_times_temp_rec.class,
                                                               I_transit_times_temp_rec.subclass);
      return FALSE;

   when OTHERS then
      if C_TRANSIT_TIMES%ISOPEN then
         close C_TRANSIT_TIMES;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TRANSIT_TIMES_UPD;
------------------------------------------------------------------------------
FUNCTION EXEC_TRANSIT_TIMES_DEL(O_error_message            IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_transit_times_temp_rec   IN       TRANSIT_TIMES%ROWTYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(64)                       := 'CORESVC_TRANSIT_TIMES.EXEC_TRANSIT_TIMES_DEL';
   L_table       SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_TRANSIT_TIMES';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);

   cursor C_TRANSIT_TIMES is
      select 'X'
        from transit_times
       where nvl(class,-1)    = nvl(I_transit_times_temp_rec.class,-1)
         and nvl(subclass,-1) = nvl(I_transit_times_temp_rec.subclass,-1)
         and dept             = I_transit_times_temp_rec.dept
         and origin           = I_transit_times_temp_rec.origin
         and destination      = I_transit_times_temp_rec.destination
         and origin_type      = I_transit_times_temp_rec.origin_type
         and destination_type = I_transit_times_temp_rec.destination_type
         for update nowait;

BEGIN
   open C_TRANSIT_TIMES;
   close C_TRANSIT_TIMES;

   delete
     from transit_times
    where nvl(class,-1)    = nvl(I_transit_times_temp_rec.class,-1)
      and nvl(subclass,-1) = nvl(I_transit_times_temp_rec.subclass,-1)
      and dept             = I_transit_times_temp_rec.dept
      and origin           = I_transit_times_temp_rec.origin
      and destination      = I_transit_times_temp_rec.destination
      and origin_type      = I_transit_times_temp_rec.origin_type
      and destination_type = I_transit_times_temp_rec.destination_type;
   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'TRANSIT_TIMES',
                                                                I_transit_times_temp_rec.class,
                                                                I_transit_times_temp_rec.subclass);
      return FALSE;

   when OTHERS then
      if C_TRANSIT_TIMES%ISOPEN then
         close C_TRANSIT_TIMES;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_TRANSIT_TIMES_DEL;
------------------------------------------------------------------------------------------------
FUNCTION PROCESS_TRANSIT_TIMES(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_process_id    IN      SVC_TRANSIT_TIMES.PROCESS_ID%TYPE,
                               I_chunk_id      IN      SVC_TRANSIT_TIMES.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                       :='CORESVC_TRANSIT_TIMES.PROCESS_TRANSIT_TIMES';
   L_error                  BOOLEAN;
   L_process_error          BOOLEAN                            := FALSE;
   L_error_message          VARCHAR2(600);
   L_TRANSIT_TIMES_temp_rec TRANSIT_TIMES%ROWTYPE;
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE  := 'SVC_TRANSIT_TIMES';
   L_valid                  BOOLEAN;
   L_dept_name              V_DEPS.DEPT_NAME%TYPE;
   L_class_name             V_CLASS.CLASS_NAME%TYPE;
   L_subclass_name          V_SUBCLASS.SUB_NAME%TYPE;

BEGIN
   FOR rec IN C_SVC_TRANSIT_TIMES(I_process_id,
                                  I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            LP_system_options_row) = FALSE then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  NULL,
                  O_error_message);
      L_error :=TRUE;
   end if;

   if rec.action is NULL
      or rec.action NOT IN (action_new,action_mod,action_del) then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'ACTION',
                  'INV_ACT');
      L_error :=TRUE;
   end if;

   if rec.action = action_new
      and rec.uk_transit_times_rid is NOT NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'Department,Point of Origin,Point of Origin Type,Point of Destination,Point of Destination Type',
                  'DUP_TRANSIT_TIMES');
      L_error :=TRUE;
   end if;

   if rec.action IN (action_mod,action_del)
      and rec.uk_transit_times_rid is NULL then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'Department,Point of Origin,Point of Origin Type,Point of Destination,Point of Destination Type',
                  'INV_TRANSIT_TIMES');
      L_error :=TRUE;
   end if;

   if rec.dept is NOT NULL then
      
      if FILTER_LOV_VALIDATE_SQL.VALIDATE_DEPS(O_error_message,
                                               L_valid,
                                               L_dept_name,
                                               NULL,
                                               NULL,
                                               rec.dept) = FALSE
         or L_valid = FALSE then
         WRITE_ERROR(rec.process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     rec.chunk_id,
                     L_table,
                     rec.row_seq,
                     'DEPT',
                     O_error_message);
         L_error:=TRUE;
      end if;
   end if;

    if rec.action IN (action_new,action_mod) then
	   
		if rec.class is NULL and
		  rec.subclass is NOT NULL then
		 
		  WRITE_ERROR(rec.process_id,
					  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
					  rec.chunk_id,
					  L_table,
					  rec.row_seq,
					  'CLASS',
					  'ENTER_THE_CLASS');
		  L_error:=TRUE;

		end if;
   
	   if L_valid then
		  
		  if rec.dept is NOT NULL and
			 rec.class is NOT NULL then
			 
			 if FILTER_LOV_VALIDATE_SQL.VALIDATE_CLASS(O_error_message,
													   L_valid,
													   L_class_name,
													   rec.dept,
													   rec.class) = FALSE
				or L_valid = FALSE then
				WRITE_ERROR(rec.process_id,
							SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
							rec.chunk_id,
							L_table,
							rec.row_seq,
							'CLASS',
							O_error_message);
				L_error:=TRUE;
			 end if;
		  end if;
	   end if;

	   if L_valid then
		  if rec.dept is NOT NULL and
			 rec.class is NOT NULL and
			 rec.subclass is NOT NULL then
			 
			 if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUBCLASS(O_error_message,
														  L_valid,
														  L_subclass_name,
														  rec.dept,
														  rec.class,
														  rec.subclass) = FALSE
				or L_valid = FALSE then
				
				WRITE_ERROR(rec.process_id,
							SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
							rec.chunk_id,
							L_table,
							rec.row_seq,
							'SUBCLASS',
							O_error_message);
				L_error:=TRUE;
			 end if;
		  end if;
	   end if;

	   if NOT(  rec.transit_time is NOT NULL ) then
			 WRITE_ERROR(I_process_id,
						 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
						 I_chunk_id,
						 L_table,
						 rec.row_seq,
						 'TRANSIT_TIME',
						 'MUST_ENTER_FIELD');
			 L_error :=TRUE;
	   end if;
	   
	   if rec.transit_time < 0 
		   or rec.transit_time > 9999 then
		   WRITE_ERROR(I_process_id,
						 SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
						 I_chunk_id,
						 L_table,
						 rec.row_seq,
						 'TRANSIT_TIME',
						 'ISC_BETWEEN_ZERO_9999');
			 L_error :=TRUE;
	   end if;
   end if; --rec.action IN (action_new,action_mod)
      

   if NOT( rec.origin_type IN ( 'SU','ST','WH' )) then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     'ORIGIN_TYPE',
                     'INV_ORIGIN_TYPE');
         L_error :=TRUE;
   end if;
      

   
   if NOT( rec.destination_type IN  ( 'ST','WH' )) then
      WRITE_ERROR(I_process_id,
                  SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                  I_chunk_id,
                  L_table,
                  rec.row_seq,
                  'DESTINATION_TYPE',
                  'INV_DESTINATION_TYPE');
      L_error :=TRUE;
   end if;
      
      if PROCESS_TRANSIT_TIMES_VAL(O_error_message,
                                   L_error,
                                   rec) = FALSE then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     rec.row_seq,
                     NULL,
                     L_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error then

         if rec.action=action_new then
            if TRANSIT_TIMES_SQL.NEXT_TRANSIT_TIMES_ID(O_error_message,
                                                       rec.transit_times_id) = FALSE then

               return FALSE;
            else
               L_transit_times_temp_rec.transit_times_id           := rec.transit_times_id;
            end if;
         end if;

         if rec.action IN (action_mod,action_del) then
            L_transit_times_temp_rec.transit_times_id              := rec.transit_times_id;
         end if;

         L_transit_times_temp_rec.dept                          := rec.dept;
         L_transit_times_temp_rec.class                         := rec.class;
         L_transit_times_temp_rec.subclass                      := rec.subclass;
         L_transit_times_temp_rec.origin                        := rec.origin;
         L_transit_times_temp_rec.destination                   := rec.destination;
         L_transit_times_temp_rec.origin_type                   := rec.origin_type;
         L_transit_times_temp_rec.destination_type              := rec.destination_type;
         L_transit_times_temp_rec.transit_time                  := rec.transit_time;

         if rec.action = action_new then
            if EXEC_TRANSIT_TIMES_INS(O_error_message,
                                      L_transit_times_temp_rec)=FALSE then

               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_mod then
            if EXEC_TRANSIT_TIMES_UPD(O_error_message,
                                      L_transit_times_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;

         if rec.action = action_del then
            if EXEC_TRANSIT_TIMES_DEL(O_error_message,
                                      L_transit_times_temp_rec)=FALSE then
               WRITE_ERROR(I_process_id,
                           SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                           I_chunk_id,
                           L_table,
                           rec.row_seq,
                           NULL,
                           L_error_message);
               L_process_error :=TRUE;
            end if;
         end if;
     end if;
   END LOOP;
   
   return TRUE;
EXCEPTION
   when OTHERS then
      
      if C_SVC_TRANSIT_TIMES%ISOPEN then
         close C_SVC_TRANSIT_TIMES;
      end if;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_TRANSIT_TIMES;
------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete
                   from svc_transit_times
                  where process_id = I_process_id;
END;
-----------------------------------------------------------------------------
FUNCTION PROCESS( O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  O_error_count     IN OUT   NUMBER,
                  I_process_id      IN       NUMBER,
                  I_chunk_id        IN       NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                      :='CORESVC_TRANSIT_TIMES.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE   := 'PS';
BEGIN
   
   LP_errors_tab := NEW errors_tab_typ();
   
   if PROCESS_TRANSIT_TIMES(O_error_message,
                            I_process_id,
                           I_chunk_id)=FALSE then
      return FALSE;
   end if;
   
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_COUNT
      insert
        into svc_admin_upld_er
      values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();
   if O_error_count    = 0 THEN
      L_process_status := 'PS';
   else
                    L_process_status := 'PE';
   end if;

   update svc_process_tracker
                    set status = (case
                                                when status = 'PE'
                                                then 'PE'
                    else L_process_status
                    end),
                                      action_date = sysdate
    where process_id = I_process_id;
   
    CLEAR_STAGING_DATA(I_process_id);
    COMMIT;
   
   return TRUE;
EXCEPTION               
   when OTHERS then
      
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
---------------------------------------------------------
END CORESVC_TRANSIT_TIMES;
/
