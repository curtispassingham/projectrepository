CREATE OR REPLACE PACKAGE BODY CORESVC_DEAL_PASSTHRU AS
   cursor C_SVC_DEAL_PASSTHRU(I_process_id   NUMBER,
                              I_chunk_id     NUMBER) is
      select pk_deal_passthru.rowid  as pk_deal_passthru_rid,
             st.rowid                as st_rid,
             st.dept,
             st.supplier,
             st.costing_loc,
             st.location,
             UPPER(st.loc_type)      as loc_type,
             st.passthru_pct,
             st.process_id,
             st.row_seq,
             st.chunk_id,
             UPPER(st.action)        as action,
             st.process$status
        from svc_deal_passthru  st,
             deal_passthru      pk_deal_passthru,
             dual
       where st.process_id  = I_process_id
         and st.chunk_id    = I_chunk_id
         and st.costing_loc = pk_deal_passthru.costing_loc (+)
         and st.supplier    = pk_deal_passthru.supplier (+)
         and st.dept        = pk_deal_passthru.dept (+)
         and st.location    = pk_deal_passthru.location (+);

   TYPE errors_tab_typ IS TABLE OF SVC_ADMIN_UPLD_ER%ROWTYPE;
   LP_errors_tab              errors_tab_typ;
   TYPE s9t_errors_tab_typ IS TABLE OF s9t_errors%ROWTYPE;
   LP_s9t_errors_tab          s9t_errors_tab_typ;

   --Internal variables for holding records that are inserted updated and deleted.
   LP_inserted_rec_tbl        DEAL_PASSTHRU_SQL.DEAL_PASSTHRU_TBL;
   LP_updated_rec_tbl         DEAL_PASSTHRU_SQL.DEAL_PASSTHRU_TBL;
   LP_deleted_rec_tbl         DEAL_PASSTHRU_SQL.DEAL_PASSTHRU_TBL;

   --Internal variables
   LP_new_deal_passthru_tbl   OBJ_DEAL_PASSTHRU_EVENT_TBL;
   LP_mod_deal_passthru_tbl   OBJ_DEAL_PASSTHRU_EVENT_TBL;
   LP_rem_deal_passthru_tbl   OBJ_DEAL_PASSTHRU_EVENT_TBL;
   
   --Internal Variables
   LP_inserted_count          INTEGER;
   LP_updated_count           INTEGER;
   LP_deleted_count           INTEGER;
------------------------------------------------------------------------------------------------------
FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name IN VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
   if sheet_name_trans.EXISTS(I_sheet_name) then
      return sheet_name_trans(I_sheet_name);
   else
      return NULL;
end if;
end GET_SHEET_NAME_TRANS;
------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_S9T_ERROR( I_file_id IN s9t_errors.file_id%TYPE,
                           I_sheet   IN VARCHAR2,
                           I_row_seq IN NUMBER,
                           I_col     IN VARCHAR2,
                           I_sqlcode IN NUMBER,
                           I_sqlerrm IN VARCHAR2) IS
BEGIN
   LP_s9t_errors_tab.EXTEND();
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).FILE_ID              := I_file_id;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_SEQ_NO         := s9t_errors_seq.NEXTVAL;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).TEMPLATE_KEY         := template_key;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).WKSHT_KEY            := I_sheet;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).COLUMN_KEY           := I_col;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ROW_SEQ              := I_row_seq;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).ERROR_KEY            := (
                                                                        CASE
                                                                           WHEN I_sqlcode IS NULL THEN
                                                                              I_sqlerrm
                                                                           ELSE
                                                                              'IIND-ORA-'||lpad(I_sqlcode,5,'0')
                                                                           END
                                                                        );
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_ID            := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).CREATE_DATETIME      := SYSDATE;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_ID       := GET_USER;
   LP_s9t_errors_tab(LP_s9t_errors_tab.COUNT()).LAST_UPDATE_DATETIME := SYSDATE;
END WRITE_S9T_ERROR;
-------------------------------------------------------------------------------------------------------
PROCEDURE WRITE_ERROR( I_process_id   IN   SVC_ADMIN_UPLD_ER.process_id%TYPE,
                       I_error_seq    IN   SVC_ADMIN_UPLD_ER.error_seq%TYPE,
                       I_chunk_id     IN   SVC_ADMIN_UPLD_ER.chunk_id%TYPE,
                       I_table_name   IN   SVC_ADMIN_UPLD_ER.table_name%TYPE,
                       I_row_seq      IN   SVC_ADMIN_UPLD_ER.row_seq%TYPE,
                       I_column_name  IN   SVC_ADMIN_UPLD_ER.column_name%TYPE,
                       I_error_msg    IN   SVC_ADMIN_UPLD_ER.error_msg%TYPE)IS
BEGIN
   LP_errors_tab.EXTEND();
   LP_errors_tab(LP_errors_tab.COUNT()).process_id  := I_process_id;
   LP_errors_tab(LP_errors_tab.COUNT()).error_seq   := I_error_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).chunk_id    := I_chunk_id;
   LP_errors_tab(LP_errors_tab.COUNT()).table_name  := I_table_name;
   LP_errors_tab(LP_errors_tab.COUNT()).row_seq     := I_row_seq;
   LP_errors_tab(LP_errors_tab.COUNT()).column_name := I_column_name;
   LP_errors_tab(LP_errors_tab.COUNT()).error_msg   := I_error_msg;
END WRITE_ERROR;
------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_NAMES(I_file_id NUMBER) IS
   L_sheets           s9t_pkg.names_map_typ;
   DEAL_PASSTHRU_cols s9t_pkg.names_map_typ;
BEGIN
   L_sheets                   := s9t_pkg.get_sheet_names(I_file_id);
   DEAL_PASSTHRU_cols         := s9t_pkg.get_col_names(I_file_id,DEAL_PASSTHRU_sheet);
   DEAL_PASSTHRU$Action       := DEAL_PASSTHRU_cols('ACTION');
   DEAL_PASSTHRU$DEPT         := DEAL_PASSTHRU_cols('DEPT');
   DEAL_PASSTHRU$SUPPLIER     := DEAL_PASSTHRU_cols('SUPPLIER');
   DEAL_PASSTHRU$COSTING_LOC  := DEAL_PASSTHRU_cols('COSTING_LOC');
   DEAL_PASSTHRU$LOCATION     := DEAL_PASSTHRU_cols('LOCATION');
   DEAL_PASSTHRU$LOC_TYPE     := DEAL_PASSTHRU_cols('LOC_TYPE');
   DEAL_PASSTHRU$PASSTHRU_PCT := DEAL_PASSTHRU_cols('PASSTHRU_PCT');
END POPULATE_NAMES;
-------------------------------------------------------------------------------------------------------
PROCEDURE POPULATE_DEAL_PASSTHRU( I_file_id IN NUMBER ) IS
BEGIN
   insert
     into TABLE( select ss.s9t_rows
                   from s9t_folder sf,
                        TABLE(sf.s9t_file_obj.sheets) ss
                  where sf.file_id  = I_file_id
                    and ss.sheet_name = DEAL_PASSTHRU_sheet )
   select s9t_row(s9t_cells(CORESVC_DEAL_PASSTHRU.action_mod ,
                           dept,
                           supplier,
                           costing_loc,
                           location,
                           loc_type,
                           passthru_pct ))
     from deal_passthru ;
END POPULATE_DEAL_PASSTHRU;
-------------------------------------------------------------------------------------------------------
PROCEDURE INIT_S9T( O_file_id  IN OUT  NUMBER) IS
   L_file      s9t_file;
   L_file_name s9t_folder.file_name%TYPE;
BEGIN
   L_file              := NEW s9t_file();
   O_file_id           := s9t_folder_seq.NEXTVAL;
   L_file.file_id      := O_file_id;
   L_file_name         := template_key||'_'||GET_USER||'_'||SYSDATE||'.ods';
   L_file.file_name    := l_file_name;
   L_file.template_key := template_key;

   L_file.user_lang    := GET_USER_LANG;

   L_file.add_sheet(DEAL_PASSTHRU_sheet);
   L_file.sheets(l_file.get_sheet_index(DEAL_PASSTHRU_sheet)).column_headers := s9t_cells( 'ACTION'
                                                                                          ,'DEPT'
                                                                                          ,'SUPPLIER'
                                                                                          ,'COSTING_LOC'
                                                                                          ,'LOCATION'
                                                                                          ,'LOC_TYPE'
                                                                                          ,'PASSTHRU_PCT' ) ;
   s9t_pkg.SAVE_OBJ(L_file);
END INIT_S9T;
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_S9T( O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     O_file_id           IN OUT   s9t_folder.file_id%TYPE,
                     I_template_only_ind IN       CHAR DEFAULT 'N')
  RETURN BOOLEAN IS
   L_program VARCHAR2(64):='CORESVC_DEAL_PASSTHRU.CREATE_S9T';   
   L_file    s9t_file;
BEGIN
   INIT_S9T(O_file_id);

   if S9T_PKG.POPULATE_LISTS(O_error_message,
                             O_file_id,
                             template_category,
                             template_key) = FALSE   then
      return FALSE;
   end if;

   if I_template_only_ind = 'N' then
      POPULATE_DEAL_PASSTHRU(O_file_id);
      COMMIT;
   end if;
   S9T_PKG.TRANSLATE_TO_USER_LANG(O_file_id);
   S9T_PKG.APPLY_TEMPLATE(O_file_id,
                          template_key);
   L_file:=S9T_FILE(O_file_id);

   if S9T_PKG.CODE2DESC(O_error_message,
                        template_category,
                        L_file) = FALSE   then
      return FALSE;
   end if;

   S9T_PKG.SAVE_OBJ(L_file);
   S9T_PKG.UPDATE_ODS(L_file);
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END CREATE_S9T;
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_S9T_DEAL_PASSTHRU( I_file_id    IN   s9t_folder.file_id%TYPE,
                                     I_process_id IN   SVC_DEAL_PASSTHRU.process_id%TYPE) IS
   L_error               BOOLEAN                   := FALSE;
   TYPE svc_DEAL_PASSTHRU_col_typ IS TABLE OF SVC_DEAL_PASSTHRU%ROWTYPE;
   L_temp_rec            SVC_DEAL_PASSTHRU%ROWTYPE;
   svc_DEAL_PASSTHRU_col svc_DEAL_PASSTHRU_col_typ  := NEW svc_DEAL_PASSTHRU_col_typ();
   L_default_rec         SVC_DEAL_PASSTHRU%ROWTYPE;
   L_process_id          SVC_DEAL_PASSTHRU.process_id%TYPE;
   cursor C_MANDATORY_IND is
      select DEPT_mi,
             SUPPLIER_mi,
             COSTING_LOC_mi,
             LOCATION_mi,
             LOC_TYPE_mi,
             PASSTHRU_PCT_mi,
             1 as dummy
        from (select column_key,
                     mandatory
                from s9t_tmpl_cols_def
               where template_key      = template_key
                 and wksht_key         = 'DEAL_PASSTHRU'  )
               PIVOT (MAX(mandatory) AS mi FOR (column_key) IN ('DEPT'         as DEPT,
                                                                'SUPPLIER'     as SUPPLIER,
                                                                'COSTING_LOC'  as COSTING_LOC,
                                                                'LOCATION'     as LOCATION,
                                                                'LOC_TYPE'     as LOC_TYPE,
                                                                'PASSTHRU_PCT' as PASSTHRU_PCT,
                                                                          null as dummy));
   L_mi_rec        c_mandatory_ind%ROWTYPE;
   dml_errors      EXCEPTION;
   PRAGMA          exception_init(dml_errors, -24381);
   L_table         VARCHAR2(30)   := 'SVC_DEAL_PASSTHRU';
   L_pk_columns    VARCHAR2(255)  := 'Dept,Supplier,Costing Loc,Location';
   L_error_code    NUMBER;
   L_error_msg     RTK_ERRORS.RTK_TEXT%type;
BEGIN
  -- Get default values.
   FOR rec IN (select  DEPT_dv,
                       SUPPLIER_dv,
                       COSTING_LOC_dv,
                       LOCATION_dv,
                       LOC_TYPE_dv,
                       PASSTHRU_PCT_dv,
                       null as dummy
                 from (select column_key,
                              default_value
                         from s9t_tmpl_cols_def
                        where template_key      = template_key
                          and wksht_key         = 'DEAL_PASSTHRU'  )
                        PIVOT (MAX(default_value) AS dv
                                FOR (column_key) IN ('DEPT'         as DEPT,
                                                     'SUPPLIER'     as SUPPLIER,
                                                     'COSTING_LOC'  as COSTING_LOC,
                                                     'LOCATION'     as LOCATION,
                                                     'LOC_TYPE'     as LOC_TYPE,
                                                     'PASSTHRU_PCT' as PASSTHRU_PCT,
                                                               null as dummy)))
   LOOP
      BEGIN
         L_default_rec.DEPT := rec.DEPT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_PASSTHRU ' ,
                            NULL,
                           'DEPT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.SUPPLIER := rec.SUPPLIER_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_PASSTHRU ' ,
                            NULL,
                           'SUPPLIER ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.COSTING_LOC := rec.COSTING_LOC_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_PASSTHRU ' ,
                            NULL,
                           'COSTING_LOC ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LOCATION := rec.LOCATION_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_PASSTHRU ' ,
                            NULL,
                           'LOCATION ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.LOC_TYPE := rec.LOC_TYPE_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_PASSTHRU ' ,
                            NULL,
                           'LOC_TYPE ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
      BEGIN
         L_default_rec.PASSTHRU_PCT := rec.PASSTHRU_PCT_dv;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                           'DEAL_PASSTHRU ' ,
                            NULL,
                           'PASSTHRU_PCT ' ,
                           'INV_DEFAULT',
                            SQLERRM);
      END;
   END LOOP;
 --Get mandatory indicators
   open  C_MANDATORY_IND;
   fetch C_MANDATORY_IND into l_mi_rec;
   close C_MANDATORY_IND;
   FOR rec IN
  (select UPPER(r.get_cell(DEAL_PASSTHRU$Action))   as Action,
          r.get_cell(DEAL_PASSTHRU$DEPT)            as DEPT,
          r.get_cell(DEAL_PASSTHRU$SUPPLIER)        as SUPPLIER,
          r.get_cell(DEAL_PASSTHRU$COSTING_LOC)     as COSTING_LOC,
          r.get_cell(DEAL_PASSTHRU$LOCATION)        as LOCATION,
          UPPER(r.get_cell(DEAL_PASSTHRU$LOC_TYPE)) as LOC_TYPE,
          r.get_cell(DEAL_PASSTHRU$PASSTHRU_PCT)    as PASSTHRU_PCT,
          r.get_row_seq()                           as row_seq
     from s9t_folder sf,
          TABLE(sf.s9t_file_obj.sheets) ss,
          TABLE(ss.s9t_rows) r
     where sf.file_id  = I_file_id
       and ss.sheet_name = sheet_name_trans(DEAL_PASSTHRU_sheet) )   LOOP
     L_temp_rec                   := NULL;
     L_temp_rec.process_id        := I_process_id;
     L_temp_rec.chunk_id          := 1;
     L_temp_rec.row_seq           := rec.row_seq;
     L_temp_rec.process$status    := 'N';
     L_temp_rec.create_id         := GET_USER;
     L_temp_rec.last_upd_id       := GET_USER;
     L_temp_rec.create_datetime   := SYSDATE;
     L_temp_rec.last_upd_datetime := SYSDATE;
     L_error := FALSE;
     BEGIN
        L_temp_rec.Action := rec.Action;
     EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_PASSTHRU_sheet,
                            rec.row_seq,
                            action_column,
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.DEPT := rec.DEPT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_PASSTHRU_sheet,
                            rec.row_seq,
                            'DEPT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.SUPPLIER := rec.SUPPLIER;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_PASSTHRU_sheet,
                            rec.row_seq,
                            'SUPPLIER',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.COSTING_LOC := rec.COSTING_LOC;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_PASSTHRU_sheet,
                            rec.row_seq,
                            'COSTING_LOC',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LOCATION := rec.LOCATION;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_PASSTHRU_sheet,
                            rec.row_seq,
                            'LOCATION',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.LOC_TYPE := rec.LOC_TYPE;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_PASSTHRU_sheet,
                            rec.row_seq,
                            'LOC_TYPE',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;
      BEGIN
         L_temp_rec.PASSTHRU_PCT := rec.PASSTHRU_PCT;
      EXCEPTION
         when OTHERS then
            WRITE_S9T_ERROR(I_file_id,
                            DEAL_PASSTHRU_sheet,
                            rec.row_seq,
                            'PASSTHRU_PCT',
                            SQLCODE,
                            SQLERRM);
            L_error := TRUE;
      END;

      if rec.action = CORESVC_DEAL_PASSTHRU.action_new then
         L_temp_rec.DEPT         := NVL( L_temp_rec.DEPT,
                                         L_default_rec.DEPT);
         L_temp_rec.SUPPLIER     := NVL( L_temp_rec.SUPPLIER,
                                         L_default_rec.SUPPLIER);
         L_temp_rec.COSTING_LOC  := NVL( L_temp_rec.COSTING_LOC,
                                         L_default_rec.COSTING_LOC);
         L_temp_rec.LOCATION     := NVL( L_temp_rec.LOCATION,
                                         L_default_rec.LOCATION);
         L_temp_rec.LOC_TYPE     := NVL( L_temp_rec.LOC_TYPE,
                                         L_default_rec.LOC_TYPE);
         L_temp_rec.PASSTHRU_PCT := NVL( L_temp_rec.PASSTHRU_PCT,
                                         L_default_rec.PASSTHRU_PCT);
      end if;

      if NOT (L_temp_rec.COSTING_LOC is NOT NULL and
              L_temp_rec.SUPPLIER    is NOT NULL and
              L_temp_rec.DEPT        is NOT NULL and
              L_temp_rec.LOCATION    is NOT NULL )   then
         WRITE_S9T_ERROR( I_file_id,
                          DEAL_PASSTHRU_sheet,
                          rec.row_seq,
                          NULL,
                          NULL,
                          SQL_LIB.CREATE_MSG('PK_COLS_REQUIRED',L_pk_columns));
         L_error := TRUE;
      end if;
      if NOT L_error then
         svc_DEAL_PASSTHRU_col.EXTEND();
         svc_DEAL_PASSTHRU_col(svc_DEAL_PASSTHRU_col.COUNT()):=L_temp_rec;
      end if;
   END LOOP;
   BEGIN
      forall i IN 1..svc_DEAL_PASSTHRU_col.COUNT SAVE EXCEPTIONS
      merge into SVC_DEAL_PASSTHRU st
      using(select
                  (case
                   when l_mi_rec.DEPT_mi         = 'N'
                    and svc_DEAL_PASSTHRU_col(i).action = CORESVC_DEAL_PASSTHRU.action_mod
                    and s1.DEPT IS NULL
                   then mt.DEPT
                   else s1.DEPT
                   end) AS DEPT,
                  (case
                   when l_mi_rec.SUPPLIER_mi     = 'N'
                    and svc_DEAL_PASSTHRU_col(i).action = CORESVC_DEAL_PASSTHRU.action_mod
                    and s1.SUPPLIER IS NULL
                   then mt.SUPPLIER
                   else s1.SUPPLIER
                   end) AS SUPPLIER,
                  (case
                   when l_mi_rec.COSTING_LOC_mi  = 'N'
                    and svc_DEAL_PASSTHRU_col(i).action = CORESVC_DEAL_PASSTHRU.action_mod
                    and s1.COSTING_LOC IS NULL
                   then mt.COSTING_LOC
                   else s1.COSTING_LOC
                   end) AS COSTING_LOC,
                  (case
                   when l_mi_rec.LOCATION_mi     = 'N'
                    and svc_DEAL_PASSTHRU_col(i).action = CORESVC_DEAL_PASSTHRU.action_mod
                    and s1.LOCATION IS NULL
                   then mt.LOCATION
                   else s1.LOCATION
                   end) AS LOCATION,
                  (case
                   when l_mi_rec.LOC_TYPE_mi     = 'N'
                    and svc_DEAL_PASSTHRU_col(i).action = CORESVC_DEAL_PASSTHRU.action_mod
                    and s1.LOC_TYPE IS NULL
                   then mt.LOC_TYPE
                   else s1.LOC_TYPE
                   end) AS LOC_TYPE,
                  (case
                   when l_mi_rec.PASSTHRU_PCT_mi = 'N'
                    and svc_DEAL_PASSTHRU_col(i).action = CORESVC_DEAL_PASSTHRU.action_mod
                    and s1.PASSTHRU_PCT IS NULL
                   then mt.PASSTHRU_PCT
                   else s1.PASSTHRU_PCT
                   end) AS PASSTHRU_PCT,
                  null as dummy
              from (select svc_DEAL_PASSTHRU_col(i).DEPT         as DEPT,
                           svc_DEAL_PASSTHRU_col(i).SUPPLIER     as SUPPLIER,
                           svc_DEAL_PASSTHRU_col(i).COSTING_LOC  as COSTING_LOC,
                           svc_DEAL_PASSTHRU_col(i).LOCATION     as LOCATION,
                           svc_DEAL_PASSTHRU_col(i).LOC_TYPE     as LOC_TYPE,
                           svc_DEAL_PASSTHRU_col(i).PASSTHRU_PCT as PASSTHRU_PCT,
                                                            null as dummy
                      from dual ) s1,
            DEAL_PASSTHRU mt
             where mt.COSTING_LOC (+) = s1.COSTING_LOC
               and mt.SUPPLIER (+)    = s1.SUPPLIER
               and mt.DEPT (+)        = s1.DEPT
               and mt.LOCATION (+)    = s1.LOCATION
               and               1    = 1 )   sq
               on ( st.COSTING_LOC    = sq.COSTING_LOC 
               and st.SUPPLIER        = sq.SUPPLIER 
               and st.DEPT            = sq.DEPT 
               and st.LOCATION        = sq.LOCATION 
               and svc_DEAL_PASSTHRU_col(i).ACTION IN (CORESVC_DEAL_PASSTHRU.action_mod,CORESVC_DEAL_PASSTHRU.action_del))
      when matched then
      update
         set process_id        = svc_DEAL_PASSTHRU_col(i).process_id ,
             chunk_id          = svc_DEAL_PASSTHRU_col(i).chunk_id ,
             row_seq           = svc_DEAL_PASSTHRU_col(i).row_seq ,
             action            = svc_DEAL_PASSTHRU_col(i).action ,
             process$status    = svc_DEAL_PASSTHRU_col(i).process$status ,
             passthru_pct      = sq.passthru_pct ,
             loc_type          = sq.loc_type ,
             create_id         = svc_DEAL_PASSTHRU_col(i).create_id ,
             create_datetime   = svc_DEAL_PASSTHRU_col(i).create_datetime ,
             last_upd_id       = svc_DEAL_PASSTHRU_col(i).last_upd_id ,
             last_upd_datetime = svc_DEAL_PASSTHRU_col(i).last_upd_datetime
      when NOT matched then
      insert(process_id,
             chunk_id ,
             row_seq ,
             action ,
             process$status ,
             dept ,
             supplier ,
             costing_loc ,
             location ,
             loc_type ,
             passthru_pct ,
             create_id ,
             create_datetime ,
             last_upd_id ,
             last_upd_datetime)
      values(svc_DEAL_PASSTHRU_col(i).process_id ,
             svc_DEAL_PASSTHRU_col(i).chunk_id ,
             svc_DEAL_PASSTHRU_col(i).row_seq ,
             svc_DEAL_PASSTHRU_col(i).action ,
             svc_DEAL_PASSTHRU_col(i).process$status ,
             sq.dept ,
             sq.supplier ,
             sq.costing_loc ,
             sq.location ,
             sq.loc_type ,
             sq.passthru_pct ,
             svc_DEAL_PASSTHRU_col(i).create_id ,
             svc_DEAL_PASSTHRU_col(i).create_datetime ,
             svc_DEAL_PASSTHRU_col(i).last_upd_id ,
             svc_DEAL_PASSTHRU_col(i).last_upd_datetime );
EXCEPTION
   when DML_ERRORS then
      FOR i IN 1..sql%bulk_exceptions.COUNT   
      LOOP
         L_error_code:=sql%bulk_exceptions(i).error_code;
         if L_error_code=1 then
            L_error_code:=NULL;
            L_error_msg := SQL_LIB.CREATE_MSG('DUP_REC_EXISTS_S9T',
                                              L_pk_columns);
         end if;
         WRITE_S9T_ERROR( I_file_id,
                          DEAL_PASSTHRU_sheet,
                          svc_DEAL_PASSTHRU_col(sql%bulk_exceptions(i).error_index).row_seq,
                          NULL,
                          L_error_code,
                          L_error_msg);                         
      END LOOP;
   END;
END PROCESS_S9T_DEAL_PASSTHRU;
--------------------------------------------------------------------------------
FUNCTION PROCESS_S9T( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE ,
                      O_error_count   IN OUT NUMBER,
                      I_file_id       IN     s9t_folder.file_id%TYPE,
                      I_process_id    IN     NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64):='CORESVC_DEAL_PASSTHRU.process_s9t';   
   L_file           s9t_file;
   L_sheets         s9t_pkg.names_map_typ;
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE;
   INVALID_FORMAT   EXCEPTION;
   PRAGMA           EXCEPTION_INIT(INVALID_FORMAT,-31011);
   MAX_CHAR     EXCEPTION;
   PRAGMA       EXCEPTION_INIT(MAX_CHAR, -01706);
BEGIN
   COMMIT;
   s9t_pkg.ods2obj(I_file_id);
   COMMIT;
   L_file := s9t_pkg.get_obj(I_file_id);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();

   if s9t_pkg.code2desc(O_error_message,
                        template_category,
                        L_file,
                        TRUE) = FALSE   then
      return FALSE;
   end if;

   s9t_pkg.save_obj(L_file);
   if s9t_pkg.validate_template(I_file_id) = FALSE then
      WRITE_S9T_ERROR(I_file_id,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                     'S9T_INVALID_TEMPLATE');
   else
      POPULATE_NAMES(I_file_id);
      sheet_name_trans := s9t_pkg.SHEET_TRANS(L_file.template_key,
                                              L_file.user_lang);
      PROCESS_S9T_DEAL_PASSTHRU(I_file_id,I_process_id);
   end if;
   O_error_COUNT := LP_s9t_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert into s9t_errors
         values LP_s9t_errors_tab(i);
   LP_s9t_errors_tab := NEW s9t_errors_tab_typ();
   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;
   update svc_process_tracker
      set status     = L_process_status,
          file_id    = I_file_id
    where process_id = I_process_id;
   COMMIT;
   return TRUE;
EXCEPTION
   when INVALID_FORMAT then
      rollback;
      O_error_message := SQL_LIB.CREATE_MSG('INV_FILE_FORMAT',
                                             NULL,
                                             NULL,
                                             NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      WRITE_S9T_ERROR( I_file_id,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                      'INV_FILE_FORMAT');
      O_error_count := LP_s9t_errors_tab.count();
      forall i IN 1..O_error_count
         insert into s9t_errors
            values LP_s9t_errors_tab(i);
      update svc_process_tracker
         set status       = 'PE',
             file_id      = I_file_id
       where process_id   = I_process_id;
      commit;

      return FALSE;

   when MAX_CHAR then
      ROLLBACK;
      O_error_message := SQL_LIB.CREATE_MSG('EXCEEDS_4000_CHAR', NULL, NULL, NULL);
      Lp_s9t_errors_tab := NEW s9t_errors_tab_typ();
      write_s9t_error(I_file_id,NULL,NULL,NULL,NULL,'EXCEEDS_4000_CHAR');
      O_error_count := Lp_s9t_errors_tab.count();
      forall i IN 1..O_error_count
    insert into s9t_errors
         values Lp_s9t_errors_tab(i);

      update svc_process_tracker
    set status = 'PE',
        file_id  = I_file_id
       where process_id = I_process_id;
      COMMIT;
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
   return FALSE;
END PROCESS_S9T;
--------------------------------------------------------------------------------
FUNCTION PROCESS_DEAL_PASSTHRU_VAL( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_error          IN OUT  BOOLEAN,
                                    I_rec            IN      C_SVC_DEAL_PASSTHRU%ROWTYPE )
RETURN BOOLEAN IS
   L_program                 VARCHAR2(64)                      :='CORESVC_DEAL_PASSTHRU.PROCESS_DEAL_PASSTHRU_VAL';
   L_table                   SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_DEAL_PASSTHRU';   
   L_ws_fr_store             BOOLEAN;
   L_closed                  BOOLEAN;
   L_valid                   BOOLEAN;
   L_supplier_access_exists  BOOLEAN;
   L_dept_name               VARCHAR2(255);
   L_org_description         VARCHAR2(255);
   L_supplier_name           VARCHAR2(255);
   L_supplier_rec            V_SUPS%ROWTYPE;
   L_costing_loc_name        V_STORE.STORE_NAME%TYPE;
   L_loc_type                V_STORE.STORE_TYPE%TYPE;
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_deleted_records_tbl     DEAL_PASSTHRU_SQL.DEAL_PASSTHRU_TBL;

BEGIN
   --Validating DEPT
   if I_rec.action IN(action_new,action_mod)   then

      if I_rec.dept is NOT NULL   then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_DEPS(L_error_message,
                                                  L_valid,
                                                  L_dept_name,
                                                  I_rec.dept) = FALSE   then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                         'DEPT',
                         L_error_message);
            O_error := TRUE;
         elsif L_valid = FALSE   then
            L_dept_name := NULL;
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'DEPT',
                        'POS_INV_DEPT');
            O_error := TRUE;
         end if;
      else
         L_dept_name := NULL;
      end if;

      --Validate STORES - Franchise stores: Loc_type must not be NULL or NOT S.
      if I_rec.loc_type IS NULL then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'LOC_TYPE',
                      'ENT_LOC_TYPE');
         O_error := TRUE; 
      elsif I_rec.loc_type <> 'S'   then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                      'LOC_TYPE',
                      'INVALID_LOC_TYPE_S');         
         O_error := TRUE;
      elsif FILTER_LOV_VALIDATE_SQL.WS_FR_STORE(L_error_message,
                                                L_ws_fr_store,
                                                L_org_description,
                                                I_rec.location) = FALSE then
            L_org_description := NULL;
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'LOCATION',
                         L_error_message);
            O_error := TRUE;
      elsif L_ws_fr_store = FALSE   then
            L_org_description := NULL;
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'LOCATION',
                        'WF_STORE_ONLY');
            O_error := TRUE;
      elsif FILTER_LOV_VALIDATE_SQL.VALIDATE_CLOSED_STORE(L_error_message,
                                                          L_closed,
                                                          I_rec.location) = FALSE   then
            L_org_description := NULL;
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                         'LOCATION',
                         L_error_message);
            O_error := TRUE;
      elsif L_closed = TRUE   then
            L_org_description := NULL;
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'LOCATION',
                        'WF_CLOSED');
            O_error := TRUE;
      else
         L_org_description := NULL;
      end if;

      --Validate Supplier
      if I_rec.supplier IS NOT NULL   then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_SUPPLIER(L_error_message,
                                                      L_supplier_access_exists,
                                                      L_supplier_rec,
                                                      I_rec.supplier) = FALSE then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                         'SUPPLIER',
                         L_error_message);
            O_error := TRUE;
         elsif L_supplier_rec.sup_status != 'A' then
            L_supplier_name := NULL;
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'SUPPLIER',
                        'INACTIVE_SUPPLIER');
            O_error := TRUE;
         -- Security check
         elsif L_supplier_access_exists = FALSE then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'SUPPLIER',
                         L_error_message);
            O_error := TRUE;
         elsif L_supplier_rec.supplier_parent IS NOT NULL then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'SUPPLIER',
                        'SUPP_SITE_NOT_ALLOW');
            O_error := TRUE;
         elsif SUPP_ATTRIB_SQL.GET_SUPP_DESC(L_error_message,
                                             I_rec.supplier,
                                             L_supplier_name) = FALSE   then
            L_supplier_name := NULL;
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                         'SUPPLIER',
                         L_error_message);
            O_error := TRUE;
         else
            L_supplier_name := NULL;
         end if;
      else
         L_supplier_name := NULL;
      end if;

      --VALIDATING COSTING_LOC
      if I_rec.costing_loc is NOT NULL   then
         if FILTER_LOV_VALIDATE_SQL.VALIDATE_COSTING_LOCATION(L_error_message,
                                                              L_valid,
                                                              L_costing_loc_name,
                                                              L_loc_type,
                                                              I_rec.costing_loc) = FALSE then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'COSTING_LOC',
                         L_error_message);
            O_error := TRUE;
         elsif L_valid = FALSE   then
            L_costing_loc_name := NULL;
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'COSTING_LOC',
                        'INV_LOC');
            O_error := TRUE;
         end if;
      else
          L_costing_loc_name := NULL;
      end if;

      --Validating Passthru Pct %
      if I_rec.passthru_pct IS NULL   then
         WRITE_ERROR( I_rec.process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_rec.chunk_id,
                      L_table,
                      I_rec.row_seq,
                     'PASSTHRU_PCT',
                     'ENTER_PASSTHRU_PCT');
         O_error :=TRUE;
      elsif I_rec.passthru_pct <= 0
            or I_rec.passthru_pct > 100   then
            WRITE_ERROR( I_rec.process_id,
                         SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                         I_rec.chunk_id,
                         L_table,
                         I_rec.row_seq,
                        'PASSTHRU_PCT',
                        'PERCENT_100');
            O_error := TRUE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DEAL_PASSTHRU_VAL;
--------------------------------------------------------------------------------
FUNCTION EXEC_DEAL_PASSTHRU_INS(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_deal_passthru_temp_rec  IN      DEAL_PASSTHRU%ROWTYPE)
RETURN BOOLEAN IS
   L_program VARCHAR2(64) :='CORESVC_DEAL_PASSTHRU.EXEC_DEAL_PASSTHRU_INS';
BEGIN

   insert into deal_passthru
        values I_deal_passthru_temp_rec;

   LP_inserted_count := LP_inserted_rec_tbl.COUNT()+1;
   LP_inserted_rec_tbl(LP_inserted_count).DEPT         := I_deal_passthru_temp_rec.DEPT;
   LP_inserted_rec_tbl(LP_inserted_count).SUPPLIER     := I_deal_passthru_temp_rec.SUPPLIER;
   LP_inserted_rec_tbl(LP_inserted_count).COSTING_LOC  := I_deal_passthru_temp_rec.COSTING_LOC;
   LP_inserted_rec_tbl(LP_inserted_count).LOCATION     := I_deal_passthru_temp_rec.LOCATION;
   LP_inserted_rec_tbl(LP_inserted_count).LOC_TYPE     := I_deal_passthru_temp_rec.LOC_TYPE;
   LP_inserted_rec_tbl(LP_inserted_count).PASSTHRU_PCT := I_deal_passthru_temp_rec.PASSTHRU_PCT;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DEAL_PASSTHRU_INS;
--------------------------------------------------------------------------------
FUNCTION EXEC_DEAL_PASSTHRU_UPD(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_deal_passthru_temp_rec  IN      DEAL_PASSTHRU%ROWTYPE )
RETURN BOOLEAN IS
   L_program     VARCHAR2(64) := 'CORESVC_DEAL_PASSTHRU.EXEC_DEAL_PASSTHRU_UPD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        EXCEPTION_INIT(Record_Locked,-54);
   cursor C_DEAL_PASSTHRU_LOCK is
      select 'X'
        from DEAL_PASSTHRU
       where costing_loc = I_deal_passthru_temp_rec.costing_loc
         and supplier    = I_deal_passthru_temp_rec.supplier
         and dept        = I_deal_passthru_temp_rec.dept
         and location    = I_deal_passthru_temp_rec.location
         for update nowait;
BEGIN
   open C_DEAL_PASSTHRU_LOCK;
   close C_DEAL_PASSTHRU_LOCK;

   update deal_passthru
      set row = I_deal_passthru_temp_rec
    where costing_loc = I_deal_passthru_temp_rec.costing_loc
      and supplier    = I_deal_passthru_temp_rec.supplier
      and dept        = I_deal_passthru_temp_rec.dept
      and location    = I_deal_passthru_temp_rec.location;

   LP_updated_count := LP_updated_rec_tbl.COUNT()+1;
   LP_updated_rec_tbl(LP_updated_count).DEPT         := I_deal_passthru_temp_rec.DEPT;
   LP_updated_rec_tbl(LP_updated_count).SUPPLIER     := I_deal_passthru_temp_rec.SUPPLIER;
   LP_updated_rec_tbl(LP_updated_count).COSTING_LOC  := I_deal_passthru_temp_rec.COSTING_LOC;
   LP_updated_rec_tbl(LP_updated_count).LOCATION     := I_deal_passthru_temp_rec.LOCATION;
   LP_updated_rec_tbl(LP_updated_count).LOC_TYPE     := I_deal_passthru_temp_rec.LOC_TYPE;
   LP_updated_rec_tbl(LP_updated_count).PASSTHRU_PCT := I_deal_passthru_temp_rec.PASSTHRU_PCT;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               'DEAL_PASSTHRU',
                                                                I_deal_passthru_temp_rec.DEPT,
                                                                I_deal_passthru_temp_rec.SUPPLIER);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      if C_DEAL_PASSTHRU_LOCK%ISOPEN   then
         close C_DEAL_PASSTHRU_LOCK;
      end if;
      return FALSE;
END EXEC_DEAL_PASSTHRU_UPD;
--------------------------------------------------------------------------------
FUNCTION EXEC_DEAL_PASSTHRU_DEL(O_error_message           IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_deal_passthru_temp_rec  IN      DEAL_PASSTHRU%ROWTYPE )
RETURN BOOLEAN IS
   L_program  VARCHAR2(64) :='CORESVC_DEAL_PASSTHRU.EXEC_DEAL_PASSTHRU_DEL';
BEGIN
      if DEAL_PASSTHRU_SQL.DELETE_PASSTHRU_RECS(O_error_message,
                                                I_deal_passthru_temp_rec.dept,
                                                I_deal_passthru_temp_rec.supplier,
                                                I_deal_passthru_temp_rec.costing_loc,
                                                I_deal_passthru_temp_rec.location) = FALSE   then
         return FALSE;
      else
         LP_deleted_count := LP_deleted_rec_tbl.COUNT()+1;
         LP_deleted_rec_tbl(LP_deleted_count).DEPT         := I_deal_passthru_temp_rec.DEPT;
         LP_deleted_rec_tbl(LP_deleted_count).SUPPLIER     := I_deal_passthru_temp_rec.SUPPLIER;
         LP_deleted_rec_tbl(LP_deleted_count).COSTING_LOC  := I_deal_passthru_temp_rec.COSTING_LOC;
         LP_deleted_rec_tbl(LP_deleted_count).LOCATION     := I_deal_passthru_temp_rec.LOCATION;
         LP_deleted_rec_tbl(LP_deleted_count).LOC_TYPE     := I_deal_passthru_temp_rec.LOC_TYPE;
         LP_deleted_rec_tbl(LP_deleted_count).PASSTHRU_PCT := I_deal_passthru_temp_rec.PASSTHRU_PCT;
      end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END EXEC_DEAL_PASSTHRU_DEL;
--------------------------------------------------------------------------------
FUNCTION PROCESS_DEAL_PASSTHRU( O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_process_id    IN     SVC_DEAL_PASSTHRU.PROCESS_ID%TYPE,
                                I_chunk_id      IN     SVC_DEAL_PASSTHRU.CHUNK_ID%TYPE )
RETURN BOOLEAN IS
   L_program                VARCHAR2(64)                      :='CORESVC_DEAL_PASSTHRU.PROCESS_DEAL_PASSTHRU';
   L_table                  SVC_ADMIN_UPLD_ER.TABLE_NAME%TYPE :='SVC_DEAL_PASSTHRU';   
   L_process_error          BOOLEAN                           := FALSE;
   L_error                  BOOLEAN;
   L_deal_passthru_temp_rec DEAL_PASSTHRU%ROWTYPE;
BEGIN
   LP_inserted_rec_tbl.DELETE;
    LP_updated_rec_tbl.DELETE;      
   LP_deleted_rec_tbl.DELETE;    
   SAVEPOINT BEGIN_COMMIT;

   FOR rec IN c_svc_DEAL_PASSTHRU(I_process_id,
                                  I_chunk_id)
   LOOP
      L_error               := FALSE;
      L_process_error       := FALSE;

      if rec.action is NULL
         or rec.action NOT IN (action_new,action_mod,action_del)   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'ACTION',
                     'INV_ACT');
         L_error :=TRUE;
      end if;

      if rec.action = action_new
         and rec.PK_DEAL_PASSTHRU_rid is NOT NULL   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'Dept,Supplier,Costing Loc,Location',
                     'DUP_RECORD');
         L_error :=TRUE;
      end if;

      if rec.action IN (action_mod,action_del)
         and rec.PK_DEAL_PASSTHRU_rid is NULL   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                     'Dept,Supplier,Costing Loc,Location',
                     'NO_RECORD');
         L_error :=TRUE;
      end if;

      if PROCESS_DEAL_PASSTHRU_VAL(O_error_message,
                                   L_error,
                                   rec) = FALSE   then
         WRITE_ERROR( I_process_id,
                      SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                      I_chunk_id,
                      L_table,
                      rec.row_seq,
                      NULL,
                      O_error_message);
         L_error :=TRUE;
      end if;

      if NOT L_error   then
             L_deal_passthru_temp_rec.passthru_pct := rec.passthru_pct;
             L_deal_passthru_temp_rec.loc_type     := rec.loc_type;
             L_deal_passthru_temp_rec.location     := rec.location;
             L_deal_passthru_temp_rec.costing_loc  := rec.costing_loc;
             L_deal_passthru_temp_rec.supplier     := rec.supplier;
             L_deal_passthru_temp_rec.dept         := rec.dept;

             if rec.action = action_new   then
                if EXEC_DEAL_PASSTHRU_INS(O_error_message,
                                          L_deal_passthru_temp_rec ) = FALSE   then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table,
                               rec.row_seq,
                               NULL,
                               O_error_message);
                   L_process_error := TRUE;
                end if;
             end if;

             if rec.action = action_mod   then
                if EXEC_DEAL_PASSTHRU_UPD(O_error_message,
                                          L_deal_passthru_temp_rec) = FALSE   then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table,
                               rec.row_seq,
                               NULL,
                               O_error_message);
                   L_process_error := TRUE;
                end if;
             end if;

             if rec.action = action_del   then
                if EXEC_DEAL_PASSTHRU_DEL(O_error_message,
                                          L_deal_passthru_temp_rec) = FALSE   then
                   WRITE_ERROR(I_process_id,
                               SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                               I_chunk_id,
                               L_table,
                               rec.row_seq,
                               NULL,
                               O_error_message);
                   L_process_error := TRUE;
                end if;
             end if;
      end if;
   END LOOP;

   if LP_inserted_rec_tbl.count > 0   then
      --Populating the table GTT_DEAL_PASSTHRU with the records along with the type of modifications.
      if DEAL_PASSTHRU_SQL.POPULATE_GTT( O_error_message,
                                         LP_new_deal_passthru_tbl,
                                         LP_mod_deal_passthru_tbl,
                                         LP_rem_deal_passthru_tbl,
                                         LP_inserted_rec_tbl,
                                         L_deal_passthru_temp_rec.passthru_pct,
                                        'ADD') = FALSE   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     NULL,
                     NULL,
                     O_error_message);
         L_process_error := TRUE;         
      end if;
   end if;

   if LP_updated_rec_tbl.count > 0   then
      --Populating the table GTT_DEAL_PASSTHRU with the records along with the type of modifications.
      if DEAL_PASSTHRU_SQL.POPULATE_GTT( O_error_message,
                                         LP_new_deal_passthru_tbl,
                                         LP_mod_deal_passthru_tbl,
                                         LP_rem_deal_passthru_tbl,
                                         LP_updated_rec_tbl,
                                         L_deal_passthru_temp_rec.passthru_pct,
                                        'MOD') = FALSE   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     NULL,
                     NULL,
                     O_error_message);
         L_process_error := TRUE;      
      end if;
   end if;

   if LP_deleted_rec_tbl.count > 0   then
      --Populating the table GTT_DEAL_PASSTHRU with the records along with the type of modifications.
      if DEAL_PASSTHRU_SQL.POPULATE_GTT( O_error_message,
                                         LP_new_deal_passthru_tbl,
                                         LP_mod_deal_passthru_tbl,
                                         LP_rem_deal_passthru_tbl,
                                         LP_deleted_rec_tbl,
                                         L_deal_passthru_temp_rec.passthru_pct,
                                        'REM') = FALSE   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     NULL,
                     NULL,
                     O_error_message);
         L_process_error := TRUE;      
      end if;
   end if;

   --Updating the future cost
   if LP_deleted_rec_tbl.count > 0
      or LP_updated_rec_tbl.count > 0
      or LP_inserted_rec_tbl.count > 0   then
      if DEAL_PASSTHRU_SQL.FUTURE_COST_UPD(O_error_message,
                                           LP_new_deal_passthru_tbl,
                                           LP_mod_deal_passthru_tbl,
                                           LP_rem_deal_passthru_tbl) = FALSE   then
         WRITE_ERROR(I_process_id,
                     SVC_ADMIN_UPLD_ER_SEQ.NEXTVAL,
                     I_chunk_id,
                     L_table,
                     NULL,
                     NULL,
                     O_error_message);
         L_process_error := TRUE;
         ROLLBACK TO BEGIN_COMMIT;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS_DEAL_PASSTHRU;
--------------------------------------------------------------------------------
PROCEDURE CLEAR_STAGING_DATA(I_process_id IN NUMBER) IS
BEGIN
   delete 
     from SVC_DEAL_PASSTHRU
    where process_id = I_process_id;
END;
--------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 O_error_count     IN OUT  NUMBER,
                 I_process_id      IN      NUMBER,
                 I_chunk_id        IN      NUMBER)
RETURN BOOLEAN IS
   L_program        VARCHAR2(64)                    :='CORESVC_DEAL_PASSTHRU.PROCESS';
   L_process_status SVC_PROCESS_TRACKER.STATUS%TYPE :='PS';
BEGIN
   LP_errors_tab := NEW errors_tab_typ();
   if PROCESS_DEAL_PASSTHRU(O_error_message,
                            I_process_id,
                            I_chunk_id)=FALSE   then
      return FALSE;
   end if;
   O_error_count := LP_errors_tab.COUNT();
   forall i IN 1..O_error_count
      insert into svc_admin_upld_er
           values LP_errors_tab(i);
   LP_errors_tab := NEW errors_tab_typ();

   if O_error_count    = 0 then
      L_process_status := 'PS';
   else
      L_process_status := 'PE';
   end if;

   update svc_process_tracker
      set status = (CASE
                       when status = 'PE'
                       then 'PE'
                    else L_process_status
                    END),
          action_date = SYSDATE
    where process_id = I_process_id;
   CLEAR_STAGING_DATA(I_process_id);
   COMMIT;

   return TRUE;
EXCEPTION
   when OTHERS then
      CLEAR_STAGING_DATA(I_process_id);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PROCESS;
END CORESVC_DEAL_PASSTHRU;
-------------------------------------------------------------------------------
/