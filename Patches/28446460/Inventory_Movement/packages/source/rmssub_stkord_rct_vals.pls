CREATE OR REPLACE PACKAGE RMSSUB_STKORD_RECEIPT_VALIDATE AUTHID CURRENT_USER AS

--------------------------------------------------------------------------------
-- CHECK_RECEIPT
-- Perform RECEIPT level validation for stock order receiving
--------------------------------------------------------------------------------
FUNCTION CHECK_RECEIPT(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid               OUT  BOOLEAN,
                       O_validation_code     OUT  VARCHAR2,
                       I_rib_receipt_rec  IN      "RIB_Receipt_REC")
return BOOLEAN;

--------------------------------------------------------------------------------
-- CHECK_BOL
-- Perform BOL level validation for stock order receiving
--------------------------------------------------------------------------------
FUNCTION CHECK_BOL(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_valid               IN OUT  BOOLEAN,
                   O_validation_code     IN OUT  VARCHAR2,
                   O_shipment            IN OUT  SHIPMENT.SHIPMENT%TYPE,
                   O_item_table          IN OUT  STOCK_ORDER_RCV_SQL.ITEM_TAB,
                   O_qty_expected_table  IN OUT  STOCK_ORDER_RCV_SQL.QTY_TAB,
                   O_inv_status_table    IN OUT  STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                   O_carton_table        IN OUT  STOCK_ORDER_RCV_SQL.CARTON_TAB,
                   O_distro_no_table     IN OUT  STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                   O_tampered_ind_table  IN OUT  STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                   I_bol_no              IN      SHIPMENT.BOL_NO%TYPE,
                   I_to_loc              IN      SHIPMENT.TO_LOC%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------------
-- CHECK_BOL_CARTON
-- Check if any of the cartons in the BOL level receipt message have been received.
-- BOL, carton, quantity received, adjustment type and actual receiving store will
-- be the criteria for determining a duplicate carton. If any quantity of the carton
-- has been received, or the adjustment type or actual receiving store is populated,
-- then the carton will be considered fully received and the duplicate carton will
-- not be received again.
--------------------------------------------------------------------------------
FUNCTION CHECK_BOL_CARTON (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_received_ind    IN OUT   VARCHAR2,
                           I_bol_no          IN       SHIPMENT.BOL_NO%TYPE,
                           I_carton          IN       SHIPSKU.CARTON%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------------
-- CHECK_CARTON
-- Perform CARTON level validation for stock order receiving
--------------------------------------------------------------------------------
FUNCTION CHECK_CARTON(O_error_message             IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_valid                     IN OUT  BOOLEAN,
                      O_validation_code           IN OUT  VARCHAR2,
                      O_ctn_shipment              IN OUT  SHIPMENT.SHIPMENT%TYPE,
                      O_ctn_to_loc                IN OUT  SHIPMENT.TO_LOC%TYPE,
                      O_ctn_bol_no                IN OUT  SHIPMENT.BOL_NO%TYPE,
                      O_item_table                IN OUT  STOCK_ORDER_RCV_SQL.ITEM_TAB,
                      O_qty_expected_table        IN OUT  STOCK_ORDER_RCV_SQL.QTY_TAB,
                      O_inv_status_table          IN OUT  STOCK_ORDER_RCV_SQL.INV_STATUS_TAB,
                      O_carton_table              IN OUT  STOCK_ORDER_RCV_SQL.CARTON_TAB,
                      O_distro_no_table           IN OUT  STOCK_ORDER_RCV_SQL.DISTRO_NO_TAB,
                      O_tampered_ind_table        IN OUT  STOCK_ORDER_RCV_SQL.TAMPERED_IND_TAB,
                      O_wrong_store_ind           IN OUT  VARCHAR2,
                      O_wrong_store               IN OUT  SHIPMENT.TO_LOC%TYPE,
                      I_bol_no                    IN      SHIPMENT.BOL_NO%TYPE,
                      I_to_loc                    IN      SHIPMENT.TO_LOC%TYPE,
                      I_from_loc                  IN      SHIPMENT.FROM_LOC%TYPE,
                      I_from_loc_type             IN      SHIPMENT.FROM_LOC_TYPE%TYPE,
                      I_distro_type               IN      SHIPSKU.DISTRO_TYPE%TYPE,
                      I_rib_receiptcartondtl_rec  IN      "RIB_ReceiptCartonDtl_REC",
                      I_flag                      IN      VARCHAR2 DEFAULT NULL)
return BOOLEAN;

--------------------------------------------------------------------------------
-- GET_ITEMS
-- 1) Separate items in the message into 2 groups: BTS sellable items
--    and the rest (L_receiptdtl_TBL).
-- 2) Find the orderables of the BTS sellables and add them to
--    O_receiptdtl_TBL. This is the table of receipt records to process.
--------------------------------------------------------------------------------
FUNCTION GET_ITEMS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                   O_valid            IN OUT  BOOLEAN,
                   O_validation_code  IN OUT  VARCHAR2,
                   O_receiptdtl_tbl   IN OUT  "RIB_ReceiptDtl_TBL",
                   I_receiptdtl_tbl   IN      "RIB_ReceiptDtl_TBL",
                   I_to_loc           IN      SHIPMENT.TO_LOC%TYPE,
                   I_distro_type      IN      SHIPSKU.DISTRO_TYPE%TYPE,
                   I_distro_no        IN      SHIPSKU.DISTRO_NO%TYPE)
return BOOLEAN;

--------------------------------------------------------------------------------
-- CHECK_ITEM
-- Perform ITEM level validation for stock order receiving
--------------------------------------------------------------------------------
FUNCTION CHECK_ITEM(O_error_message       IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                    O_valid               IN OUT  BOOLEAN,
                    O_validation_code     IN OUT  VARCHAR2,
                    I_rib_receipt_rec     IN      "RIB_Receipt_REC",
                    I_rib_receiptdtl_rec  IN      "RIB_ReceiptDtl_REC")
return BOOLEAN;

--------------------------------------------------------------------------------
END RMSSUB_STKORD_RECEIPT_VALIDATE;
/
