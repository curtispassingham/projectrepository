edit the file daemon_rpo.ctl and replace the following:
    - #DD_PORT#: Domain Daemon port that is set on the fusion client installation. Check the Foundation.xml file on the fusion client installation if requested
	- #RPO_SERVER_WALLET_DIR#: Location for the wallet installed with the fusion client (e.g. /u01/product/rpassys/rpo/rpas/rpaswallets/server) 
	- #GLOBAL_DOMAIN_DIR#: Location for the global domain. It must match the variable $GLOBAL_DOMAIN

confirm that the RPO environment variable O_APPLICATION is set to "rpo".