#!/usr/bin/ksh
#
# Copyright © 2016, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2016/02/01
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Oracle Retail Consulting
#
###############################################################################
# rpo_env_rpas.sh
#
# This is the common environment "include" script. This file should normally
# be sourced only by rpo_env_rpas.sh.
#
# Args: None
##############################################################################

#
# Guard against re-sourcing
#
if [[ "${_rpo_env_rpas_sh-0}" = 1 ]]; then
    return 0
else
    typeset -i -r _rpo_env_rpas_sh=1
fi

# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
# * * * IMPORTANT: Variables below corresponding to directory paths     * * *
# * * *            should NOT contain spaces!                           * * *
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

#
# Domain path.
# For flexibility and test purposes, these are set to externally defined values.
#
O_LOG_HOME=${BATCH_HOME}/logs
SCRIPTS_BATCH=$BATCH_HOME/scripts

#offreDOMAIN="${GLOBAL_DOMAIN}"
DEFAULT_DOMAIN="${GLOBAL_DOMAIN}/ldom099"
#INTEGRATION_HOME=${O_APP_INSTANCE}/integration
EXTERNAL_INBOUND_DIR=${RETL_IN}
EXTERNAL_OUTBOUND_DIR=${RETL_OUT}

#BSA_SEETINGS
BSA_ARCHIVE_DIR=${BATCH_HOME}/archiv
BSA_CONFIG_DIR=${BATCH_HOME}/config
BSA_LOG_TYPE=1
BSA_LOG_ARCHIVE_TYPE=ERROR
BSA_LOG_ARCHIVE_DEST=${BATCH_HOME}/error
BSA_TEMP_DIR=${BATCH_HOME}/temp

#
# Script and screen log levels.
# Must be one of: { PROFILE | DEBUG | INFORMATION | WARNING | ERROR | NONE }
#
BSA_LOG_LEVEL=INFORMATION
BSA_SCREEN_LEVEL=INFORMATION
#
# RPAS_INTEGRATION_HOME may commonly be set equal to offreDOMAIN. For
# flexibility and test purposes, it is set to a distinct, externally defined
# value.
#
#RPAS_INTEGRATION_HOME="${O_APPLICATION_HOME}/rpas/scripts/integration"

#
# Temp directory. This should be a writeable path.  Use of /tmp
# is discouraged due to typically small size of this partition on UNIX
# machines.
#
#BSA_TEMP_DIR="${O_APPLICATION_TMPDIR}"
#
# Since offre, through BSA, sorts with -T option to specify temporary space,
# specify the sort temp space separately.  This is due to some operating
# systems' relatively low limits on the length of the -T argument.
#
#BSA_SORT_TEMP_DIR="${O_APPLICATION_TMPDIR}"

#
# Script parallel process fan-out maximum. The number of processes that any given
# process may spawn.
#
if [[ $(uname) = CYGWIN* ]] ; then
  BSA_MAX_PARALLEL=10
else
  #BSA_MAX_PARALLEL=${O_BSA_MAX_PARALLEL}
  BSA_MAX_PARALLEL=10
fi

#
# Script log home directory
#
BSA_LOG_HOME="${O_LOG_HOME}"

#
# Script log type. Must be one of { 0 | 1 | 2 | 3 }
#    0 - No logging
#    1 - Text ".log" file
#    2 - XML structured ".xml" file
#    3 - Text and XML
#
BSA_LOG_TYPE=1 #0=No logging; 1=Text Only; 2=XML Only; 3=Text & XML;

#
# BSA script configuration directory.
# Location for bsa_prep_files.config.
#
#BSA_CONFIG_DIR="${O_BATCH_HOME}/config"

#
# BSA archive directory
#
#BSA_ARCHIVE_DIR="${O_ARCHIVE_HOME}"

#
# Logging level for RPAS binaries that use Logger.
# Must be one of: { PROFILE | DEBUG | INFORMATION | WARNING | ERROR }
#
if [ "${RPAS_LOG_LEVEL}" = "" ]; then
  export RPAS_LOG_LEVEL="INFORMATION"
fi

#
# The Unix environment variable, RPAS_PAGE_SPLIT_PERCENTAGE, is set so as to
# optimize offre performance.
#
export RPAS_PAGE_SPLIT_PERCENTAGE=90

# Enforce byte-counting vs character counting inside awk programs by
# exporting LC_ALL, which overrides all locale variable settings.  Set
# the locale to C which is the POSIX compliant standard.  New (3.1.5+)
# versions of gawk will use non-POSIX compliant character counting
# instead of POSIX-standard byte-counting for some functions like substr
# which will cause awk code to process incorrectly any multi-byte characters.
typeset -x LC_ALL=C


#
# Setting up RPAS_TODAY
#
#export RPAS_TODAY=$(cat ${GLOBAL_DOMAIN}/vdate.int)
#export RPAS_TODAY=20180514
