# 1 : identifier
# 2 : command to execute cp or mv
# 3 : source directory
# 4 : source subdirectory
# 5 : filename pattern
# 6 : destination directory
# 7 : destination subdirectory
# 8 : destination filename pattern (if different).
# 9 : destination timeformat if necessary, for example: +%Y%m%d_%H%M%S or +%Y%m%d

RPO_RMS_PRICECHANGE|cp|RETL_OUT|MOM|XXRPO202_PriceChange_*.dat|INTEGRATION_HOME|int/incoming/MOM||