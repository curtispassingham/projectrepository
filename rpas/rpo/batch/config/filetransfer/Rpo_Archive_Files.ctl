# 1 : identifier
# 2 : command to execute cp or mv
# 3 : source directory
# 4 : source subdirectory
# 5 : filename pattern
# 6 : destination directory
# 7 : destination subdirectory
# 8 : destination filename pattern (if different).
# 9 : destination timeformat if necessary, for example: +%Y%m%d_%H%M%S or +%Y%m%d

#incoming files
RMS_RPO_ORGHIER|mv|RETL_IN|RPO|loc.csv.dat|PROCESSED_IN||loc.csv.dat|+_%Y%m%d_%H%M%S
RMS_RPO_DST_ORGHIER|mv|RETL_IN|RPO|nbitembumap.csv.*|PROCESSED_IN|||
RMS_RPO_COMPHIER|mv|RETL_IN|RPO|comp.csv.dat|PROCESSED_IN||comp.csv.dat|+_%Y%m%d_%H%M%S
RMS_RPO_COMPETITOR_PRICES|mv|RETL_IN|RPO|ol1cmp*.csv*.dat|PROCESSED_IN|||
RMS_RPO_RGITPC|mv|RETL_IN|RPO|ol1orgitpc.csv*.dat|PROCESSED_IN|||
RMS_RPO_NATITPC|mv|RETL_IN|RPO|nbnatitpc.csv*.dat|PROCESSED_IN|||
RMS_RPO_ITEMVAT|mv|RETL_IN|RPO|ol1itvat.csv*|PROCESSED_IN|||
RMS_RPO_FTMEDNLD|mv|RETL_IN|RPO|rmse_rpas_clndmstr.dat|PROCESSED_IN||rmse_rpas_clndmstr.dat|+_%Y%m%d_%H%M%S
RMS_RPO_DST_FTMEDNLD|mv|RETL_IN|RPO|clnd.csv.dat|PROCESSED_IN||clnd.csv.dat|+_%Y%m%d_%H%M%S
RMS_RPO_MERCHHIER|mv|RETL_IN|RPO|rmse_rpas_merchhier.dat|PROCESSED_IN||rmse_rpas_merchhier.dat|+_%Y%m%d_%H%M%S
RMS_RPO_DST_MERCHHIER_1|mv|RETL_IN|RPO|prod.csv.dat|PROCESSED_IN||prod.csv.dat|+_%Y%m%d_%H%M%S
RMS_RPO_DST_MERCHHIER_2|mv|RETL_IN|RPO|pror.csv.dat|PROCESSED_IN||pror.csv.dat|+_%Y%m%d_%H%M%S
RMS_RPO_ITEM_MASTER|mv|RETL_IN|RPO|rmse_rpas_item_master.dat|PROCESSED_IN||rmse_rpas_item_master.dat|+_%Y%m%d_%H%M%S
RMS_RPO_ITEM_FLAGS|mv|RETL_IN|RPO|XXRMS214_ITEMFLAGS*.dat|PROCESSED_IN|||
RMS_RPO_ITEM_ATTR|mv|RETL_IN|RPO|XXRMS209_*.dat|PROCESSED_IN|||
RMS_RPO_DST_ITEM_ATTR|mv|RETL_IN|RPO|nbituocap.csv.*|PROCESSED_IN|||
RMS_RPO_DST_ITEM_ATTR_2|mv|RETL_IN|RPO|nbitbumppc.csv.*|PROCESSED_IN|||
RMS_RPO_MERCHHIERTL|mv|RETL_IN|RPO|XXRMS216_*.dat|RETL_IN|processed||
RMS_RPO_DST_MERCHHIERTL_1|mv|RETL_IN|RPO|r_itemlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_2|mv|RETL_IN|RPO|r_iterlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_3|mv|RETL_IN|RPO|r_clsslabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_4|mv|RETL_IN|RPO|r_clsrlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_5|mv|RETL_IN|RPO|r_scatlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_6|mv|RETL_IN|RPO|r_scarlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_7|mv|RETL_IN|RPO|r_catlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_8|mv|RETL_IN|RPO|r_catrlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_9|mv|RETL_IN|RPO|r_deptlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_10|mv|RETL_IN|RPO|r_deprlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_11|mv|RETL_IN|RPO|r_chnlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_12|mv|RETL_IN|RPO|r_chnrlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_13|mv|RETL_IN|RPO|r_brndlabel.csv*|PROCESSED_IN|||
RMS_RPO_DST_MERCHHIERTL_14|mv|RETL_IN|RPO|r_brnrlabel.csv*|PROCESSED_IN|||
RMS_RPO_ITEMMASTERTL|mv|RETL_IN|RPO|XXRMS219_*.dat|PROCESSED_IN|||
RMS_RPO_ITEMATTRTL|mv|RETL_IN|RPO|XXRMS220_*.dat|PROCESSED_IN|||
RMS_RPO_ITEMPACK|mv|RETL_IN|RPO|XXRMS221_ITEMPACK_*.dat|PROCESSED_IN|||
RMS_RPO_DST_ITEMPACK|mv|RETL_IN|RPO|ol1ilgititrmp.csv.*|PROCESSED_IN|||
RMS_RPO_DST_ITEMPACK_2|mv|RETL_IN|RPO|ol1ituom.csv.*|PROCESSED_IN|||
RMS_RPO_SUBSTITUTEITEM|mv|RETL_IN|RPO|XXRMS217_SUBSTITUTEITEMS_*.dat|PROCESSED_IN|||
RMS_RPO_RELATEDITEM|mv|RETL_IN|RPO|XXRMS218_RELATEDITEMS_*.dat|PROCESSED_IN|||
RMS_RPO_TEQUIU|mv|RETL_IN|RPO|ol1itequiu.csv*|PROCESSED_IN|||
RMS_RPO_UOM_DOUBLON|mv|RETL_IN|RPO|ol1ituom_doublon.csv*|PROCESSED_IN|||
#BI_RDF_RPO_DL1ITBDSP|mv|RETL_IN|RPO|Dl1ItBdsp.csv.*|PROCESSED_IN|||
#RDF_RPO_BDSPITEMSTOR|mv|RETL_IN|RPO|bdspitemstor.csv*|PROCESSED_IN|||
#RPO_SCNCATMP|mv|RETL_IN|RPO|scncatmp.csv*|PROCESSED_IN|||
RMS_RPO_DL1ORGITCST|mv|RETL_IN|RPO|dl1orgitcst.csv*|PROCESSED_IN|||
RPM_RPO_OL1NATITPC|mv|RETL_IN|RPO|ol1natitpc.csv*|RETL_IN|PROCESSED_IN|||
RMS_RPO_OL1CMPPCCHKDT|mv|RETL_IN|RPO|ol1cmppcchkdt.csv*|PROCESSED_IN|||
#RPO_IG|mv|RETL_IN|RPO|ig.csv.dat|PROCESSED_IN||ig.csv.dat|+_%Y%m%d_%H%M%S
#RPO_CIG|mv|RETL_IN|RPO|cig.csv.dat|PROCESSED_IN||cig.csv.dat|+_%Y%m%d_%H%M%S
#RPO_IC|mv|RETL_IN|RPO|ic.csv.dat|PROCESSED_IN||ic.csv.dat|+_%Y%m%d_%H%M%S
#RPO_IIG|mv|RETL_IN|RPO|iig.csv.dat|PROCESSED_IN||iig.csv.dat|+_%Y%m%d_%H%M%S
#RPO_PL|mv|RETL_IN|RPO|pl.csv.dat|PROCESSED_IN||pl.csv.dat|+_%Y%m%d_%H%M%S
#RPO_PP|mv|RETL_IN|RPO|pp.csv.dat|PROCESSED_IN||pp.csv.dat|+_%Y%m%d_%H%M%S
#RPO_SCN|mv|RETL_IN|RPO|scn.csv.dat|PROCESSED_IN||scn.csv.dat|+_%Y%m%d_%H%M%S

#outgoing files
RPO_RMS_PRICECHANGE|mv|RETL_OUT||XXRPO202_PriceChange_*.dat|PROCESSED_OUT|||
