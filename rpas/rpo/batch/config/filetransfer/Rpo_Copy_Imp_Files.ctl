# 1 : identifier
# 2 : command to execute cp or mv
# 3 : source directory
# 4 : source subdirectory
# 5 : filename pattern
# 6 : destination directory
# 7 : destination subdirectory
# 8 : destination filename pattern (if different).
# 9 : destination timeformat if necessary, for example: +%Y%m%d_%H%M%S or +%Y%m%d

RPO_VDATE|cp|GLOBAL_DOMAIN||vdate.int|RETLforXXADEO|rfx/etc|vdate.txt|
RMS_RPO_ORGHIER|cp|RETL_IN|RPO|loc.csv.dat|GLOBAL_DOMAIN|input||
RMS_RPO_COMPHIER|cp|RETL_IN|RPO|comp.csv.dat|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIER|cp|RETL_IN|RPO|prod.csv.dat|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIER|cp|RETL_IN|RPO|pror.csv.dat|GLOBAL_DOMAIN|input||
RMS_RPO_FTMEDNLD|cp|RETL_IN|RPO|clnd.csv.dat|GLOBAL_DOMAIN|input||
RMS_RPO_COMPETITOR_PRICES|cp|RETL_IN|RPO|ol1cmpcurpc.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_RGITPC|cp|RETL_IN|RPO|ol1orgitpc.csv*.dat|GLOBAL_DOMAIN|input||
RMS_RPO_NATITPC|cp|RETL_IN|RPO|nbnatitpc.csv*.dat|GLOBAL_DOMAIN|input||
RMS_RPO_ITEMVAT|cp|RETL_IN|RPO|ol1itvat.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_NBITEMBUMAP|cp|RETL_IN|RPO|nbitembumap.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_OL1ILGITITRMP|cp|RETL_IN|RPO|ol1ilgititrmp.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_NBITBUMPPC|cp|RETL_IN|RPO|nbitbumppc.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_1|cp|RETL_IN|RPO|r_brndlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_2|cp|RETL_IN|RPO|r_brnrlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_3|cp|RETL_IN|RPO|r_catlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_4|cp|RETL_IN|RPO|r_catrlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_5|cp|RETL_IN|RPO|r_chnlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_6|cp|RETL_IN|RPO|r_chnrlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_7|cp|RETL_IN|RPO|r_clsrlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_8|cp|RETL_IN|RPO|r_clsslabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_9|cp|RETL_IN|RPO|r_deprlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_10|cp|RETL_IN|RPO|r_deptlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_11|cp|RETL_IN|RPO|r_itemlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_12|cp|RETL_IN|RPO|r_iterlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_13|cp|RETL_IN|RPO|r_scarlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_MERCHHIERTL_14|cp|RETL_IN|RPO|r_scatlabel.csv.ovr_*|GLOBAL_DOMAIN|input||
RMS_RPO_UOCAP|cp|RETL_IN|RPO|nbituocap.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_UOM|cp|RETL_IN|RPO|ol1ituom.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_TEQUIU|cp|RETL_IN|RPO|ol1itequiu.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_UOM_DOUBLON|cp|RETL_IN|RPO|ol1ituom_doublon.csv*|GLOBAL_DOMAIN|input||
BI_RDF_RPO_DL1ITBDSP|cp|RETL_IN|RPO|Dl1ItBdsp.csv.*|GLOBAL_DOMAIN|input||
RDF_RPO_BDSPITEMSTOR|cp|RETL_IN|RPO|bdspitemstor.csv*|GLOBAL_DOMAIN|input||
RPO_SCNCATMP|cp|RETL_IN|RPO|scncatmp.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_DL1ORGITCST|cp|RETL_IN|RPO|dl1orgitcst.csv*|GLOBAL_DOMAIN|input||
RPM_RPO_OL1NATITPC|cp|RETL_IN|RPO|ol1natitpc.csv*|GLOBAL_DOMAIN|input||
RMS_RPO_OL1CMPPCCHKDT|cp|RETL_IN|RPO|ol1cmppcchkdt.csv*|GLOBAL_DOMAIN|input||
RPO_IG|cp|RETL_IN|RPO|ig.csv.dat|GLOBAL_DOMAIN|input||
RPO_CIG|cp|RETL_IN|RPO|cig.csv.dat|GLOBAL_DOMAIN|input||
RPO_IC|cp|RETL_IN|RPO|ic.csv.dat|GLOBAL_DOMAIN|input||
RPO_IIG|cp|RETL_IN|RPO|iig.csv.dat|GLOBAL_DOMAIN|input||
RPO_PL|cp|RETL_IN|RPO|pl.csv.dat|GLOBAL_DOMAIN|input||
RPO_PP|cp|RETL_IN|RPO|pp.csv.dat|GLOBAL_DOMAIN|input||
RPO_SCN|cp|RETL_IN|RPO|scn.csv.dat|GLOBAL_DOMAIN|input||