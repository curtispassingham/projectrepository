#Export measure control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) M - Measure Name (at least 1 entry needed)
# 3) F - Filter Mask Measure (if specified, export will happen at mask measure base intersection)
# 4) X - Base Intersection (F or X is needed)
# 5) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 6) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 7) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 8) S - SkipNA (Optional) - Control whether a line of data is exported based on having NA's in a cell. Valid values are [never|allna|anyna]. Default is allna.
# 9) C - useDate (Optional) - Control whether an acual clnd position name is exported or the start or end date of that position. Valid values are [s|e]. Default is position id.

# export data access rights

# export product hierarchy position level access right at levels all, user group and user
prodsecurityall|X|cat_
prodsecurityall|O|prorsecurityall.csv.ovr
prodsecurityall|M|prodsecurityall
prodsecurityall|S|never

prodsecuritygrp|X|cat_grp_
prodsecuritygrp|O|prorsecuritygrp.csv.ovr
prodsecuritygrp|M|prodsecuritygrp
prodsecuritygrp|S|never

prodsecurityusr|X|cat_user
prodsecurityusr|O|prorsecurityusr.csv.ovr
prodsecurityusr|M|prodsecurityusr
prodsecurityusr|S|never

# export location hierarchy position level access right at levels all, user group and user
locsecurityall|X|przn
locsecurityall|O|locrsecurityall.csv.ovr
locsecurityall|M|locsecurityall
locsecurityall|S|never

locsecuritygrp|X|przngrp_
locsecuritygrp|O|locrsecuritygrp.csv.ovr
locsecuritygrp|M|locsecuritygrp
locsecuritygrp|S|never

locsecurityusr|X|prznuser
locsecurityusr|O|locrsecurityusr.csv.ovr
locsecurityusr|M|locsecurityusr
locsecurityusr|S|never
