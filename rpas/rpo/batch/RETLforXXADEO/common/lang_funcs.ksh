
# See if PREFERRED_LANG is supported; default to English if not
SetPreferredLang () 
{
	if [[ ! -s "$RESOURCE_DIR/SupportedLanguages.txt" ]]; then
		echo "RESOURCE_DIR/SupportedLanguages.txt doesn't exist or is empty.  Setting PREFERRED_LANG to English."
		export PREFERRED_LANG=_en
	fi

	isSupportedLang=$(awk -F, -v pLang=$PREFERRED_LANG '$5 == pLang {print $4}' $RESOURCE_DIR/SupportedLanguages.txt)
	if [[ "$isSupportedLang" != "Yes" || -z "$isSupportedLang" ]]; then
		echo "PREFERRED_LANG ($PREFERRED_LANG) is either not recognized or not supported; defaulting to English."
		export PREFERRED_LANG=_en
	fi
}