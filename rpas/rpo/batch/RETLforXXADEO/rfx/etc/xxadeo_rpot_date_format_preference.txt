#  This file contains the text (after the last comment line) that is needed by
#  rdft_calhier.ksh to determine what format is to be used for the Date
#  Description field (calendar date) in the Calendar Hierarchy load file that
#  it produces. The text must be (literally) either mm/dd/yyyy or dd/mm/yyyy.
#  A user prompt should be provided in the install script to replace the default
#  text that is here now with the customer preference.
dd/mm/yyyy
