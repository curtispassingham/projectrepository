# config file defines the ATTR KEY which contains the
# CAPACITY_QUANTITY information in RMS per Item.
#
# E.g: |QUANTITY_CAPACITY_KEY|KEY|
#
# QUANTITY_CAPACITY_KEY is the parameter to be defined
# KEY is the CAPACITY QUANTITY CFA KEY defined in RMS table mom_dvm
#
|QUANTITY_CAPACITY_KEY|QUANTITY_CAPACITY|