#!/bin/ksh
###############################################################################
#  $Workfile:   xxadeo_rmse_rpas_merchhier.ksh
#  $Modtime:    Mon 28 2018
#               24/JUL/2018 - 001
#               Script changed to generate two identical files.
###############################################################################

###############################################################################
#  This script builds 2 data file
#
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################
export PROGRAM_NAME='xxadeo_rpot_merchhier'

# ETC directory
ETC_DIR=${RETLforXXADEO}/rfx/etc

#Custom xxadeo input/output directory
O_DATA_DIR_XXADEO=$RETL_IN/RPO
I_DATA_DIR_XXADEO=$RETL_IN/RPO

#Standard input directory
I_DATA_DIR=$RETL_IN/RPO

# Schema directories
SCHEMA_DIR=${RETLforXXADEO}/rfx/schema
SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema


###############################################################################
#  INCLUDES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh

###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################
export I_ITEMFLAG_PATTERN="XXRMS214_ITEMFLAGS_*.dat"
export I_ITEMATTR_PATTERN="XXRMS209_ITEMATTR_*.dat"
export I_ATTR_PATTERN="XXRMS209_ATTR_*.dat"

I_ITEMFLAGS_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ITEMFLAG_PATTERN} | sort | tail -n 1`
I_ITEMUDAS_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ITEMATTR_PATTERN} | sort | tail -n 1`
I_UDAS_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ATTR_PATTERN} | sort | tail -n 1`

I_MERCHHIER_FILE=$I_DATA_DIR/rmse_rpas_merchhier.dat
I_ITEMMASTER_FILE=$I_DATA_DIR/rmse_rpas_item_master.dat


RETL_OUTPUT_FILE1=$O_DATA_DIR_XXADEO/prod.csv.dat
RETL_OUTPUT_FILE2=$O_DATA_DIR_XXADEO/pror.csv.dat


###############################################################################
#  FUNCTIONS
###############################################################################
function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${I_UDAS_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${I_ITEMUDAS_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${I_ITEMFLAGS_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${RETL_OUTPUT_FILE1}.${PROGRAM_NAME}.retl"
                rm -f "${RETL_OUTPUT_FILE1}"
                rm -f "${RETL_OUTPUT_FILE2}.${PROGRAM_NAME}.retl"
                rm -f "${RETL_OUTPUT_FILE2}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${I_UDAS_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${I_ITEMUDAS_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${I_ITEMFLAGS_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${RETL_OUTPUT_FILE1}.${PROGRAM_NAME}.retl"
                rm -f "${RETL_OUTPUT_FILE2}.${PROGRAM_NAME}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}

###############################################################################
#  Check input files
###############################################################################


    if [[ ! -f $I_MERCHHIER_FILE ]];  then
        message "Input file merchandise hierarchy $I_MERCHHIER_FILE missing!!"
        exit 1
    fi

    if [[ ! -f $I_ITEMMASTER_FILE ]];  then
        message "Input file item master $I_ITEMMASTER_FILE missing!!"
        exit 1
    fi

    if [[ ! -f $I_UDAS_FILE ]];  then
        message "Input file attr $I_UDAS_FILE missing!!"
        exit 1
    fi

    if [[ ! -f $I_ITEMUDAS_FILE ]];  then
        message "Input file item attr $I_ITEMUDAS_FILE missing!!"
        exit 1
    fi

    if [[ ! -f $I_ITEMFLAGS_FILE ]];  then
        message "Input file item flags $I_ITEMFLAGS_FILE missing!!"
        exit 1
    fi
###############################################################################
#  RETL input/output schemas
###############################################################################

I_MERCHHIER_SCHEMA=${SCHEMA_DIR}/rmse_rpas_merchhier.schema
I_ITEMMASTER_SCHEMA=${SCHEMA_DIR}/rmse_rpas_item_master.schema
I_ITEMFLAGS_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_item_flags.schema
I_UDAS_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_attr.schema
I_ITEMUDAS_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_itemattr.schema

O_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rpot_merchhier.schema


###############################################################################
# Interface Configuration file
###############################################################################

CONFIG_FILE=${ETC_DIR}/${PROGRAM_NAME}.txt
# Evaluate if the interface config file do not exist
if [ ! -r  ${CONFIG_FILE} ]; then
       message "Config file ${CONFIG_FILE} missing!!"
       exit 1
fi

# Get BRAND UDA ID
BRAND_ATTR_KEY_CONFIG_LINE=$(cat ${CONFIG_FILE} | grep -v "^[[:blank:]]*#" | grep "BRAND_ATTR_KEY")
BRAND_ATTR_KEY=$(echo ${BRAND_ATTR_KEY_CONFIG_LINE} | cut -d"|" -f3 | tr -d '[[:blank:]]')
BRAND_UDA_ID=`grep -i $BRAND_ATTR_KEY ${I_UDAS_FILE} | cut -d ',' -f2 | sort -u`

# Evaluate if the UDA Key was retrieved successfully
if [ -z ${BRAND_ATTR_KEY} ]; then
       message "ERROR: Interface config file not correctly set. Please check config file ${CONFIG_FILE}"
       exit 1
fi

# Get ITEM TYPE UDA ID
ITEM_TYPE_ATTR_KEY_CONFIG_LINE=$(cat ${CONFIG_FILE} | grep -v "^[[:blank:]]*#" | grep "ITEM_TYPE_ATTR_KEY")
ITEM_TYPE_ATTR_KEY=$(echo ${ITEM_TYPE_ATTR_KEY_CONFIG_LINE} | cut -d"|" -f3 | tr -d '[[:blank:]]')
ITEM_TYPE_UDA_ID=`grep -i $ITEM_TYPE_ATTR_KEY ${I_UDAS_FILE} | cut -d ',' -f2 | sort -u`

# Evaluate if the UDA Key was retrieved successfully
if [ -z ${ITEM_TYPE_ATTR_KEY} ]; then
       message "ERROR: Interface config file not correctly set. Please check config file ${CONFIG_FILE}"
       exit 1
fi

# Get ITEM TYPES TO EXCLUDE
ITE_ATTR_KEY_CONFIG_LINE=$(cat ${CONFIG_FILE} | grep -v "^[[:blank:]]*#" | grep "ITEM_TYPES_TO_EXCLUDE")
ITE_ATTR_VALUE=$(echo ${ITE_ATTR_KEY_CONFIG_LINE} | cut -d"|" -f3 | tr -d '[[:blank:]]')
BKP_IFS=$IFS
IFS=','
set -A ITEM_TYPES_TO_EXCLUDE_ARRAY $ITE_ATTR_VALUE
ITEM_TYPES_TO_EXCLUDE_COND="ITEM_TYPE NE 'DUMMY'"
for ITEM_TYPE_TO_EXCLUDE in ${ITEM_TYPES_TO_EXCLUDE_ARRAY[@]}
do
	ITEM_TYPES_TO_EXCLUDE_COND+=" AND ITEM_TYPE NE '${ITEM_TYPE_TO_EXCLUDE}'"
done
IFS=$BKP_IFS

# Evaluate if the Config Key was retrieved successfully
if [ -z ${ITE_ATTR_KEY_CONFIG_LINE} ]; then
       message "ERROR: Interface config file not correctly set. Please check config file ${CONFIG_FILE}"
       exit 1
fi


###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Convert input data files in RETLSV format
###############################################################################

#csv_converter "csv-retlsv" "${I_ITEMFLAGS_FILE}" "${I_ITEMFLAGS_FILE}.${PROGRAM_NAME}.retl"
#check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_UDAS_FILE}" "${I_UDAS_FILE}.${PROGRAM_NAME}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_ITEMUDAS_FILE}" "${I_ITEMUDAS_FILE}.${PROGRAM_NAME}.retl"
check_error_and_clean_before_exit $?


###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
<!-- STEP_1 : filter file -->
<!-- ITEM FLAGS FILE -->
        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMFLAGS_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_ITEMFLAGS_SCHEMA}"/>
                <OUTPUT name="i_item_flags.v"/>
        </OPERATOR>
        <OPERATOR type="filter">
                <INPUT name="i_item_flags.v"/>
                <PROPERTY name="filter" value="SELLABLE_IND EQ 'Y'"/>
                <OUTPUT name="filter_item_sellable.v"/>
        </OPERATOR>
        <OPERATOR type="fieldmod">
                <INPUT name="filter_item_sellable.v" />
                <PROPERTY name="keep" value="ITEM"/>
                <OUTPUT name="item_flags_mod.v"/>
        </OPERATOR>

<!-- ITEM MASTER FILE -->
        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMMASTER_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_ITEMMASTER_SCHEMA}"/>
                <OUTPUT name="i_item_master.v"/>
        </OPERATOR>
        <OPERATOR type="filter">
                <INPUT name="i_item_master.v"/>
                <PROPERTY name="filter" value="ITEM_LEVEL EQ TRAN_LEVEL"/>
                <OUTPUT name="filter_item_level_tran_level.v"/>
        </OPERATOR>
        <OPERATOR type="fieldmod">
                <INPUT name="filter_item_level_tran_level.v" />
                <PROPERTY name="keep" value="ITEM ITEM_DESC DEPT CLASS SUBCLASS" />
                <OUTPUT name="item_master_mod.v"/>
        </OPERATOR>

<!-- STEP_2: JOIN ITEM FLAGS AND ITEM MASTER files (sort file is needed before the join) -->
        <OPERATOR type="sort">
                <INPUT name="item_flags_mod.v"/>
                <PROPERTY name="key" value="ITEM"/>
                <OUTPUT name="sorted_i_item_sellable.v"/>
        </OPERATOR>
        <OPERATOR type="sort">
                <INPUT name="item_master_mod.v"/>
                <PROPERTY name="key" value="ITEM"/>
                <OUTPUT name="sorted_i_item_master.v"/>
        </OPERATOR>
        <OPERATOR type="innerjoin">
                <INPUT name="sorted_i_item_master.v"/>
                <INPUT name="sorted_i_item_sellable.v"/>
                <PROPERTY name="key" value="ITEM"/>
                <OUTPUT name="item_master_tmp.v"/>
        </OPERATOR>
        <OPERATOR type="fieldmod">
                <INPUT name="item_master_tmp.v" />
                <PROPERTY name="keep" value="ITEM ITEM_DESC DEPT CLASS SUBCLASS"/>
                <PROPERTY name="rename" value="ITEM_ID=ITEM"/>
                <PROPERTY name="rename" value="ITEM_LABEL=ITEM_DESC"/>
                <OUTPUT name="item_master.v"/>
        </OPERATOR>

<!-- MERCHANDISE HIERARCHY file -->
        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_MERCHHIER_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_MERCHHIER_SCHEMA}"/>
                <OUTPUT name="i_merchhier.v"/>
        </OPERATOR>

<!-- STEP_3: JOIN STEP_2 file  AND MERCHANDISE HIERARCHY file (sort file is needed before the join) -->
        <OPERATOR type="sort">
                <INPUT name="i_merchhier.v"/>
                <PROPERTY name="key" value="DEPT CLASS SUBCLASS"/>
                <OUTPUT name="sorted_i_merchhier.v"/>
        </OPERATOR>
        <OPERATOR type="sort">
                <INPUT name="item_master.v"/>
                <PROPERTY name="key" value="DEPT CLASS SUBCLASS"/>
                <OUTPUT name="sorted_item_master.v"/>
        </OPERATOR>
        <OPERATOR type="innerjoin">
                <INPUT name="sorted_i_merchhier.v"/>
                <INPUT name="sorted_item_master.v"/>
                <PROPERTY name="key" value="DEPT CLASS SUBCLASS" />
                <OUTPUT name="merchhier_item_master_tmp.v"/>
        </OPERATOR>
        <OPERATOR type="sort">
                <INPUT name="merchhier_item_master_tmp.v"/>
                <PROPERTY name="key" value="ITEM_ID"/>
                <OUTPUT name="sorted_merchhier_item_master.v"/>
        </OPERATOR>

<!-- FILETERING ITEM ATTR FILE WITH THE ITEM_TYPE UDA_ID -->
        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMUDAS_FILE}.${PROGRAM_NAME}.retl"/>
                <PROPERTY  name="schemafile" value="${I_ITEMUDAS_SCHEMA}"/>
                <OUTPUT name="i_item_udas1.v"/>
        </OPERATOR>
        
        <OPERATOR  type="filter">
                <INPUT name="i_item_udas1.v"/>
                <PROPERTY name="filter" value="ATTR_ID EQ '${ITEM_TYPE_UDA_ID}'"/>
                <OUTPUT name="filter_item_attr_key1.v"/>
        </OPERATOR>
        
        <OPERATOR type="fieldmod">
            <INPUT name="filter_item_attr_key1.v" />
            <PROPERTY name="keep" value="ITEM_ID ATTR_VALUE"/>
            <PROPERTY name="rename" value="ITEM_TYPE=ATTR_VALUE"/>
            <OUTPUT name="filter_item_attr_key1_renamed.v"/>
        </OPERATOR>

<!-- STEP_4.1: JOIN STEP_3 FILE AND ITEM_UDAS files (sort file is needed before the join) -->
        <OPERATOR type="sort">
                <INPUT name="filter_item_attr_key1_renamed.v"/>
                <PROPERTY name="key" value="ITEM_ID"/>
                <OUTPUT name="sorted_i_item_udas1.v"/>
        </OPERATOR>
         <OPERATOR type="leftouterjoin">
                <INPUT name="sorted_merchhier_item_master.v"/>
                <INPUT name="sorted_i_item_udas1.v"/>
                <PROPERTY name="key" value="ITEM_ID" />
                <PROPERTY name="nullvalue" value="ITEM_TYPE=no_type" />
                <OUTPUT name="item_udas_tmp1.v"/>
        </OPERATOR>
        <OPERATOR  type="filter">
                <INPUT name="item_udas_tmp1.v"/>
                <PROPERTY name="filter" value="${ITEM_TYPES_TO_EXCLUDE_COND}"/>
                <OUTPUT name="filter_item_udas_tmp1.v"/>
        </OPERATOR>
        
<!-- FILETERING ITEM ATTR FILE WITH THE PRODUCT BRAND UDA_ID -->
        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMUDAS_FILE}.${PROGRAM_NAME}.retl"/>
                <PROPERTY  name="schemafile" value="${I_ITEMUDAS_SCHEMA}"/>
                <OUTPUT name="i_item_udas2.v"/>
        </OPERATOR>
        
        <OPERATOR  type="filter">
                <INPUT name="i_item_udas2.v"/>
                <PROPERTY name="filter" value="ATTR_ID EQ '${BRAND_UDA_ID}'"/>
                <OUTPUT name="filter_item_attr_key2.v"/>
        </OPERATOR>
        
        <OPERATOR type="fieldmod">
            <INPUT name="filter_item_attr_key2.v" />
            <PROPERTY name="keep" value="ITEM_ID ATTR_VALUE"/>
            <OUTPUT name="filter_item_attr_key_renamed2.v"/>
        </OPERATOR>

<!-- STEP_4.2: JOIN STEP_3 FILE AND ITEM_UDAS files (sort file is needed before the join) -->
        <OPERATOR type="sort">
                <INPUT name="filter_item_attr_key_renamed2.v"/>
                <PROPERTY name="key" value="ITEM_ID"/>
                <OUTPUT name="sorted_i_item_udas2.v"/>
        </OPERATOR>
        
        <OPERATOR type="leftouterjoin">
                <INPUT name="filter_item_udas_tmp1.v"/>
                <INPUT name="sorted_i_item_udas2.v"/>
                <PROPERTY name="key" value="ITEM_ID" />
                <PROPERTY name="nullvalue" value="ATTR_VALUE=no_brand" />
                <OUTPUT name="item_udas_tmp2.v"/>
        </OPERATOR>
        
<!-- FILTERING ATTR FILE WITH THE PRODUCT BRAND KEY-->
        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_UDAS_FILE}.${PROGRAM_NAME}.retl"/>
                <PROPERTY  name="schemafile" value="${I_UDAS_SCHEMA}"/>
                <OUTPUT name="i_udas.v"/>
        </OPERATOR>

        <OPERATOR  type="filter">
                <INPUT name="i_udas.v"/>
                <PROPERTY name="filter" value="KEY EQ '${BRAND_ATTR_KEY}'"/>
                <OUTPUT name="filter_attr_key.v"/>
        </OPERATOR>

<!-- STEP_5: JOIN STEP_4 FILE AND UDAS files (sort file is needed before the join) -->
        <OPERATOR type="sort">
                <INPUT name="filter_attr_key.v"/>
                <PROPERTY name="key" value="ATTR_VALUE"/>
                <OUTPUT name="sorted_i_udas.v"/>
        </OPERATOR>
        <OPERATOR type="sort">
                <INPUT name="item_udas_tmp2.v"/>
                <PROPERTY name="key" value="ATTR_VALUE"/>
                <OUTPUT name="sorted_item_udas.v"/>
        </OPERATOR>
        <OPERATOR type="leftouterjoin">
                <INPUT name="sorted_item_udas.v"/>
                <INPUT name="sorted_i_udas.v"/>
                <PROPERTY name="key" value="ATTR_VALUE" />
                <PROPERTY name="nullvalue" value="ATTR_VALUE_NAME=No Brand" />
                <OUTPUT name="item_udas_final_tmp.v"/>
        </OPERATOR>
        <OPERATOR type="fieldmod">
                <INPUT name="item_udas_final_tmp.v" />
                <PROPERTY name="keep" value="ITEM_ID ITEM_LABEL SUBCLASS SUB_NAME CLASS CLASS_NAME DEPT DEPT_NAME GROUP_NO GROUP_NAME DIVISION DIV_NAME COMPANY CO_NAME ATTR_VALUE ATTR_VALUE_NAME"/>
                <PROPERTY name="rename" value="CLASS_ID=SUBCLASS"/>
                <PROPERTY name="rename" value="CLASS_LABEL=SUB_NAME"/>
                <PROPERTY name="rename" value="SUB_CATEGORY_ID=CLASS"/>
                <PROPERTY name="rename" value="SUB_CATEGORY_LABEL=CLASS_NAME"/>
                <PROPERTY name="rename" value="CATEGORY_ID=DEPT"/>
                <PROPERTY name="rename" value="CATEGORY_LABEL=DEPT_NAME"/>
                <PROPERTY name="rename" value="DEPARTMENT_ID=GROUP_NO"/>
                <PROPERTY name="rename" value="DEPARTMENT_LABEL=GROUP_NAME"/>
                <PROPERTY name="rename" value="CHANNEL_ID=DIVISION"/>
                <PROPERTY name="rename" value="CHANNEL_LABEL=DIV_NAME"/>
                <PROPERTY name="rename" value="CHANNEL_ID1=COMPANY"/>
                <PROPERTY name="rename" value="CHANNEL_LABEL1=CO_NAME"/>
                <PROPERTY name="rename" value="BRAND_ID=ATTR_VALUE"/>
                <PROPERTY name="rename" value="BRAND_LABEL=ATTR_VALUE_NAME"/>
                <OUTPUT name="prod_tmp.v"/>
        </OPERATOR>

    <OPERATOR type="generator">
      <INPUT name="prod_tmp.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="ITEM_LAB_CONCAT" type="string" maxlength="100" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CLASS_ID_CONCAT" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CLASS_ID_TMP" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CLASS_LAB_CONCAT" type="string" maxlength="100" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="SUB_CAT_ID_CONCAT" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="SUB_CAT_ID_TMP" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="SUB_CAT_LAB_CONCAT" type="string" maxlength="100" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CAT_ID_CONCAT" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CAT_LAB_CONCAT" type="string" maxlength="100" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CAT_ID_TMP" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="DEPT_ID_CONCAT" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="DEPT_LAB_CONCAT" type="string" maxlength="100" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="DEPT_ID_TMP" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CHANNEL_ID_CONCAT" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CHANNEL_LAB_CONCAT" type="string" maxlength="100" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CHANNEL_ID_TMP" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CHANNEL_ID1_CONCAT" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CHANNEL_LAB1_CONCAT" type="string" maxlength="100" nullable="false">
               <CONST value=""/>
            </FIELD>
            <FIELD name="CHANNEL_ID1_TMP" type="string" maxlength="20" nullable="false">
               <CONST value=""/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="prod_tmp0.v"/>
   </OPERATOR>


    <OPERATOR type="parser">
        <INPUT name="prod_tmp0.v"/>
          <PROPERTY name="expression">
          <![CDATA[
             {
                if (RECORD.CLASS_ID < 10)
                {
                        RECORD.CLASS_ID_TMP = "0000";
                }
                else if (RECORD.CLASS_ID < 100)
                {
                        RECORD.CLASS_ID_TMP = "000";
                }
                else if (RECORD.CLASS_ID < 1000)
                {
                        RECORD.CLASS_ID_TMP = "00";
                }
                else if (RECORD.CLASS_ID < 10000)
                {
                        RECORD.CLASS_ID_TMP = "0";
                }

                if (RECORD.SUB_CATEGORY_ID < 10)
                {
                        RECORD.SUB_CAT_ID_TMP = "0000";
                }
                else if (RECORD.SUB_CATEGORY_ID < 100)
                {
                        RECORD.SUB_CAT_ID_TMP = "000";
                }
                else if (RECORD.SUB_CATEGORY_ID < 1000)
                {
                        RECORD.SUB_CAT_ID_TMP = "00";
                }
                else if (RECORD.SUB_CATEGORY_ID < 10000)
                {
                        RECORD.SUB_CAT_ID_TMP = "0";
                }

                if (RECORD.CATEGORY_ID < 10)
                {
                        RECORD.CAT_ID_TMP = "0000";
                }
                else if (RECORD.CATEGORY_ID < 100)
                {
                        RECORD.CAT_ID_TMP = "000";
                }
                else if (RECORD.CATEGORY_ID < 1000)
                {
                        RECORD.CAT_ID_TMP = "00";
                }
                else if (RECORD.CATEGORY_ID < 10000)
                {
                        RECORD.CAT_ID_TMP = "0";
                }

                if (RECORD.DEPARTMENT_ID < 10)
                {
                        RECORD.DEPT_ID_TMP = "0000";
                }
                else if (RECORD.DEPARTMENT_ID < 100)
                {
                        RECORD.DEPT_ID_TMP = "000";
                }
                else if (RECORD.DEPARTMENT_ID < 1000)
                {
                        RECORD.DEPT_ID_TMP = "00";
                }
                else if (RECORD.DEPARTMENT_ID < 10000)
                {
                        RECORD.DEPT_ID_TMP = "0";
                }

                if (RECORD.CHANNEL_ID < 10)
                {
                        RECORD.CHANNEL_ID_TMP = "0000";
                }
                else if (RECORD.CHANNEL_ID < 100)
                {
                        RECORD.CHANNEL_ID_TMP = "000";
                }
                else if (RECORD.CHANNEL_ID < 1000)
                {
                        RECORD.CHANNEL_ID_TMP = "00";
                }
                else if (RECORD.CHANNEL_ID < 10000)
                {
                        RECORD.CHANNEL_ID_TMP = "0";
                }

                if (RECORD.CHANNEL_ID1 < 10)
                {
                        RECORD.CHANNEL_ID1_TMP = "0000";
                }
                else if (RECORD.CHANNEL_ID1 < 100)
                {
                        RECORD.CHANNEL_ID1_TMP = "000";
                }
                else if (RECORD.CHANNEL_ID1 < 1000)
                {
                        RECORD.CHANNEL_ID1_TMP = "00";
                }
                else if (RECORD.CHANNEL_ID1 < 10000)
                {
                        RECORD.CHANNEL_ID1_TMP = "0";
                }

             }
          ]]>
          </PROPERTY>
        <OUTPUT name="prod_tmp02.v"/>
   </OPERATOR>


   <OPERATOR type="convert">
        <INPUT name="prod_tmp02.v"/>
        <PROPERTY name="convertspec">
                <![CDATA[
                        <CONVERTSPECS>
                                <CONVERT destfield="CLASS_ID" sourcefield="CLASS_ID" newtype="string">
                                        <CONVERTFUNCTION name="string_from_int16"/>
                                </CONVERT>
                                <CONVERT destfield="SUB_CATEGORY_ID" sourcefield="SUB_CATEGORY_ID" newtype="string">
                                        <CONVERTFUNCTION name="string_from_int16"/>
                                </CONVERT>
                                <CONVERT destfield="CATEGORY_ID" sourcefield="CATEGORY_ID" newtype="string">
                                        <CONVERTFUNCTION name="string_from_int16"/>
                                </CONVERT>
                                <CONVERT destfield="DEPARTMENT_ID" sourcefield="DEPARTMENT_ID" newtype="string">
                                        <CONVERTFUNCTION name="string_from_int16"/>
                                </CONVERT>
                                <CONVERT destfield="CHANNEL_ID" sourcefield="CHANNEL_ID" newtype="string">
                                        <CONVERTFUNCTION name="string_from_int16"/>
                                </CONVERT>
                                <CONVERT destfield="CHANNEL_ID1" sourcefield="CHANNEL_ID1" newtype="string">
                                        <CONVERTFUNCTION name="string_from_int16"/>
                                </CONVERT>
                        </CONVERTSPECS>
                ]]>
        </PROPERTY>
        <OUTPUT name="prod_tmp1.v"/>
    </OPERATOR>

<OPERATOR type="parser">
        <INPUT name="prod_tmp1.v"/>
          <PROPERTY name="expression">
          <![CDATA[
             {
                //CHANNEL (COMPANY)
				RECORD.CHANNEL_LAB1_CONCAT += RECORD.CHANNEL_ID1_TMP;
				RECORD.CHANNEL_LAB1_CONCAT += RECORD.CHANNEL_ID1;
                RECORD.CHANNEL_LAB1_CONCAT += " ";
                RECORD.CHANNEL_LAB1_CONCAT += RECORD.CHANNEL_LABEL1;
                RECORD.CHANNEL_LABEL1 = RECORD.CHANNEL_LAB1_CONCAT;
				
				//CHANNEL (DIVISION *Not Used*)
                RECORD.CHANNEL_LAB_CONCAT += RECORD.CHANNEL_ID_TMP;
				RECORD.CHANNEL_LAB_CONCAT += RECORD.CHANNEL_ID;
                RECORD.CHANNEL_LAB_CONCAT += " ";
                RECORD.CHANNEL_LAB_CONCAT += RECORD.CHANNEL_LABEL;
                RECORD.CHANNEL_LABEL = RECORD.CHANNEL_LAB_CONCAT;
				
				//DEPARTEMENT (GROUP)
                RECORD.DEPT_LAB_CONCAT += RECORD.DEPT_ID_TMP;
				RECORD.DEPT_LAB_CONCAT += RECORD.DEPARTMENT_ID;
                RECORD.DEPT_LAB_CONCAT += " ";
                RECORD.DEPT_LAB_CONCAT += RECORD.DEPARTMENT_LABEL;
                RECORD.DEPARTMENT_LABEL = RECORD.DEPT_LAB_CONCAT;
			 
				//CATEGORY (DEPT)
                RECORD.CAT_LAB_CONCAT += RECORD.CAT_ID_TMP;
				RECORD.CAT_LAB_CONCAT += RECORD.CATEGORY_ID;
                RECORD.CAT_LAB_CONCAT += " ";
                RECORD.CAT_LAB_CONCAT += RECORD.CATEGORY_LABEL;
                RECORD.CATEGORY_LABEL = RECORD.CAT_LAB_CONCAT;

				//SUBCATEGORY (CLASS)
                RECORD.SUB_CAT_LAB_CONCAT += RECORD.SUB_CAT_ID_TMP;
				RECORD.SUB_CAT_LAB_CONCAT += RECORD.SUB_CATEGORY_ID;
                RECORD.SUB_CAT_LAB_CONCAT += " ";
                RECORD.SUB_CAT_LAB_CONCAT += RECORD.SUB_CATEGORY_LABEL;
                RECORD.SUB_CATEGORY_LABEL = RECORD.SUB_CAT_LAB_CONCAT;

                RECORD.SUB_CAT_ID_CONCAT += RECORD.CATEGORY_ID;
				RECORD.SUB_CAT_ID_CONCAT += "_";
                RECORD.SUB_CAT_ID_CONCAT += RECORD.SUB_CATEGORY_ID;
                RECORD.SUB_CATEGORY_ID = RECORD.SUB_CAT_ID_CONCAT;

				//CLASS LABEL (SUBCLASS)
                RECORD.CLASS_LAB_CONCAT += RECORD.CLASS_ID_TMP;
				RECORD.CLASS_LAB_CONCAT += RECORD.CLASS_ID;
                RECORD.CLASS_LAB_CONCAT += " ";
                RECORD.CLASS_LAB_CONCAT += RECORD.CLASS_LABEL;
                RECORD.CLASS_LABEL = RECORD.CLASS_LAB_CONCAT;
				
                RECORD.CLASS_ID_CONCAT += RECORD.SUB_CATEGORY_ID;
				RECORD.CLASS_ID_CONCAT += "_";
                RECORD.CLASS_ID_CONCAT += RECORD.CLASS_ID;
                RECORD.CLASS_ID = RECORD.CLASS_ID_CONCAT;
				
				//ITEM 
                RECORD.ITEM_LAB_CONCAT += RECORD.ITEM_ID;   
                RECORD.ITEM_LAB_CONCAT += " ";			
                RECORD.ITEM_LAB_CONCAT += RECORD.ITEM_LABEL;
                RECORD.ITEM_LABEL = RECORD.ITEM_LAB_CONCAT;
             }
          ]]>
          </PROPERTY>
        <OUTPUT name="prod_tmp2.v"/>
   </OPERATOR>


<!-- STEP_6: EXPORT OUTPUT FILE STEP_5 -->
        <!-- <OPERATOR type="copy"> -->
                <!-- <INPUT name="prod_tmp2.v"/> -->
                <!-- <OUTPUT name="fic1.v"/> -->
                <!-- <OUTPUT name="fic2.v"/> -->
        <!-- </OPERATOR>-->-->
		
        <!-- <OPERATOR type="fieldmod"> -->
                <!-- <INPUT name="fic1.v" /> -->
                <!-- <PROPERTY name="keep" value="ITEM_ID ITEM_LABEL CLASS_ID CLASS_LABEL SUB_CATEGORY_ID SUB_CATEGORY_LABEL CATEGORY_ID CATEGORY_LABEL DEPARTMENT_ID DEPARTMENT_LABEL CHANNEL_ID CHANNEL_LABEL BRAND_ID BRAND_LABEL "/> -->
                <!-- <OUTPUT name="prod_tmp3.v"/> -->
        <!-- </OPERATOR> -->
        <!-- <OPERATOR type="export"> -->
                <!-- <INPUT name="prod_tmp3.v"/> -->
                <!-- <PROPERTY name="schemafile" value="${O_SCHEMA}"/> -->
                <!-- <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE1}.${PROGRAM_NAME}.retl"/> -->
        <!-- </OPERATOR> -->

<!-- STEP_8: EXPORT OUTPUT FILE PROR.CSV.DAT -->
<!-- Change 001: changed fic2.v to prod_tmp2 in operator "fieldmod" -->
        <OPERATOR type="fieldmod">
                <INPUT name="prod_tmp2.v" />
                <PROPERTY name="keep" value="ITEM_ID ITEM_LABEL CLASS_ID CLASS_LABEL SUB_CATEGORY_ID SUB_CATEGORY_LABEL CATEGORY_ID CATEGORY_LABEL DEPARTMENT_ID DEPARTMENT_LABEL CHANNEL_ID1 CHANNEL_LABEL1 BRAND_ID BRAND_LABEL "/>
                <PROPERTY name="rename" value="CHANNEL_ID=CHANNEL_ID1" />
                <PROPERTY name="rename" value="CHANNEL_LABEL=CHANNEL_LABEL1" />
                <OUTPUT name="pror_tmp1.v"/>
        </OPERATOR>
        <OPERATOR type="export">
                <INPUT name="pror_tmp1.v"/>
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE2}.${PROGRAM_NAME}.retl"/>
        </OPERATOR>


</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################
# Change 001: Replaced output of prod.csv.dat by the copy of prod.csv.dat
#csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE1}.${PROGRAM_NAME}.retl" "${RETL_OUTPUT_FILE1}"
#check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE2}.${PROGRAM_NAME}.retl" "${RETL_OUTPUT_FILE2}"
check_error_and_clean_before_exit $?

cp ${RETL_OUTPUT_FILE2} ${RETL_OUTPUT_FILE1}


###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${RETL_OUTPUT_FILE1}

log_num_recs ${RETL_OUTPUT_FILE2}


###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi

message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"