#!/bin/ksh
###############################################################################
#  $Workfile:   xxadeo_Inbound_Interface_lien_article.ksh
#  $Modtime:    Mar 18 2018
###############################################################################

###############################################################################
#  This script builds many data file
#
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################

PROGRAM_NAME='xxadeo_rpot_item_link'

###############################################################################
#  INCLUDES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh

###############################################################################
#  VARIABLES
###############################################################################

date=`date +"%G%m%d-%H%M%S"`

#RFX_EXE=${RFX_EXE:=rfx}
DATA_DIR_RPAS=$RETL_IN/RPO
DATA_DIR=$RETL_IN

###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################

I_ITEMPACK_PATTERN=XXRMS221_ITEMPACK_*.dat
I_SUBSTITUTEITEM_PATTERN=XXRMS217_SUBSTITUTEITEM*.dat
I_RELATEDITEM_PATTERN=XXRMS218_RELATEDITEM*.dat

RETL_OUTPUT_FILE=$DATA_DIR/RPO/ol1ilgititrmp.csv.rpl_${date}
RETL_OUTPUT_FILE_TMP=$DATA_DIR/RPO/ol1ilgititrmp.csv.rpl_${date}_tmp
###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${I_ITEMPACK_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${I_SUBSTITUTEITEM_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${I_RELATEDITEM_FILE}.${PROGRAM_NAME}.retl"

                rm -f "${RETL_OUTPUT_FILE_TMP}.${PROGRAM_NAME}.retl"
                rm -f "${RETL_OUTPUT_FILE_TMP}"
                rm -f "${RETL_OUTPUT_FILE}"

                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${I_ITEMPACK_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${I_SUBSTITUTEITEM_FILE}.${PROGRAM_NAME}.retl"
                rm -f "${I_RELATEDITEM_FILE}.${PROGRAM_NAME}.retl"

                rm -f "${RETL_OUTPUT_FILE_TMP}.${PROGRAM_NAME}.retl"

                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}

###############################################################################
#  Get latest available input files, because since this interface works in a
#  FULL mode there is no need to process all available input files.
###############################################################################
:
I_ITEMPACK_FILE=`find $DATA_DIR_RPAS -name ${I_ITEMPACK_PATTERN} | sort | tail -n 1`
I_SUBSTITUTEITEM_FILE=`find $DATA_DIR_RPAS -name ${I_SUBSTITUTEITEM_PATTERN} | sort | tail -n 1`
I_RELATEDITEM_FILE=`find $DATA_DIR_RPAS -name ${I_RELATEDITEM_PATTERN} | sort | tail -n 1`


    if [[ ! -f $I_ITEMPACK_FILE ]];  then
        message "item_pack file does not exist"
        exit 1
    fi

        if [[ ! -f $I_SUBSTITUTEITEM_FILE ]];  then
        message "substitute item file does not exist"
        exit 1
    fi

        if [[ ! -f $I_RELATEDITEM_FILE ]];  then
        message "related item does not exist"
        exit 1
    fi
###############################################################################
#  RETL input/output schemas
###############################################################################
I_ITEMPACK_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_itempack.schema
I_SUBSTITUTEITEM_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_substitute_items.schema
I_RELATEDITEM_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_related_items.schema

O_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema


###############################################################################
#  Convert input data files in RETLSV format
###############################################################################

#csv_converter "csv-retlsv" "${I_ITEMPACK_FILE}" "${I_ITEMPACK_FILE}.${PROGRAM_NAME}.retl"
#check_error_and_clean_before_exit $?

#csv_converter "csv-retlsv" "${I_SUBSTITUTEITEM_FILE}" "${I_SUBSTITUTEITEM_FILE}.${PROGRAM_NAME}.retl"
#check_error_and_clean_before_exit $?

#csv_converter "csv-retlsv" "${I_RELATEDITEM_FILE}" "${I_RELATEDITEM_FILE}.${PROGRAM_NAME}.retl"
#check_error_and_clean_before_exit $?


###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">


        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMPACK_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_ITEMPACK_SCHEMA}"/>
                <OUTPUT name="i_itempack.v"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_SUBSTITUTEITEM_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_SUBSTITUTEITEM_SCHEMA}"/>
                <OUTPUT name="i_substituteitem.v"/>
        </OPERATOR>
        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_RELATEDITEM_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_RELATEDITEM_SCHEMA}"/>
                <OUTPUT name="i_relateditem.v"/>
        </OPERATOR>


        <OPERATOR type="fieldmod">
                <INPUT name="i_itempack.v" />
                <PROPERTY name="keep" value="PACK_NO ITEM"/>
                <PROPERTY name="rename" value="ITEM_ID=PACK_NO"/>
                <PROPERTY name="rename" value="LINKED_ITEM_ID=ITEM"/>
                <OUTPUT name="i_itempack_1.v"/>
        </OPERATOR>
        <OPERATOR type="fieldmod">
                <INPUT name="i_substituteitem.v" />
                <PROPERTY name="keep" value="ITEM SUB_ITEM"/>
                <PROPERTY name="rename" value="ITEM_ID=ITEM"/>
                <PROPERTY name="rename" value="LINKED_ITEM_ID=SUB_ITEM"/>
                <OUTPUT name="i_substituteitem_1.v"/>
        </OPERATOR>
        <OPERATOR type="fieldmod">
                <INPUT name="i_relateditem.v" />
                <PROPERTY name="keep" value="ITEM RELATED_ITEM"/>
                <PROPERTY name="rename" value="ITEM_ID=ITEM"/>
                <PROPERTY name="rename" value="LINKED_ITEM_ID=RELATED_ITEM"/>
                <OUTPUT name="i_relateditem_1.v"/>
        </OPERATOR>

   <OPERATOR type="generator">
      <INPUT name="i_itempack_1.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="LINKED" type="string" maxlength="1" nullable="false">
               <CONST value="T"/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="i_itempack_2.v"/>
   </OPERATOR>

       <OPERATOR type="generator">
      <INPUT name="i_substituteitem_1.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="LINKED" type="string" maxlength="1" nullable="false">
               <CONST value="T"/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="i_substituteitem_2.v"/>
   </OPERATOR>

    <OPERATOR type="generator">
      <INPUT name="i_relateditem_1.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="LINKED" type="string" maxlength="1" nullable="false">
               <CONST value="T"/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="i_relateditem_2.v"/>
   </OPERATOR>


 <!-- Use a convert operator to make not nullable some fields that use substring into the query  -->
   <OPERATOR type="convert">
      <INPUT    name="i_itempack_2.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="ITEM_ID" sourcefield="ITEM_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                 <CONVERT destfield="LINKED_ITEM_ID" sourcefield="LINKED_ITEM_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
          </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="i_itempack_3.v"/>
   </OPERATOR>


   <OPERATOR type="convert">
      <INPUT    name="i_relateditem_2.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="ITEM_ID" sourcefield="ITEM_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                 <CONVERT destfield="LINKED_ITEM_ID" sourcefield="LINKED_ITEM_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
          </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="i_relateditem_3.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT    name="i_substituteitem_2.v"/>
      <PROPERTY name="convertspec">
        <![CDATA[
          <CONVERTSPECS>
                <CONVERT destfield="ITEM_ID" sourcefield="ITEM_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                 <CONVERT destfield="LINKED_ITEM_ID" sourcefield="LINKED_ITEM_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
          </CONVERTSPECS>
        ]]>
      </PROPERTY>
      <OUTPUT name="i_substituteitem_3.v"/>
   </OPERATOR>


        <OPERATOR type="export">
                <INPUT name="i_itempack_3.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE_TMP}"/>
                <PROPERTY name="outputmode" value="append"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="i_substituteitem_3.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE_TMP}"/>
                <PROPERTY name="outputmode" value="append"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="i_relateditem_3.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE_TMP}"/>
                <PROPERTY name="outputmode" value="append"/>
        </OPERATOR>

</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?


###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${RETL_OUTPUT_FILE_TMP}


cat ${RETL_OUTPUT_FILE_TMP} |
while IFS=, read item_id linked_item_id linked
do
        echo "$item_id,$linked_item_id,$linked" >> ${RETL_OUTPUT_FILE}
        echo "$linked_item_id,$item_id,$linked" >> ${RETL_OUTPUT_FILE}
done

# DELETE TMP file
if [ -e ${RETL_OUTPUT_FILE_TMP} ];then
        rm ${RETL_OUTPUT_FILE_TMP}
fi
###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"
