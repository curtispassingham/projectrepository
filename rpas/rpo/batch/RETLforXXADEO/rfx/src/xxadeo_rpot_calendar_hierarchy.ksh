#!/bin/ksh
########################################################
# Copyright (c) 2018, Oracle.  All rights reserved.
# $Workfile:  xxadeo_rpot_calendar_hierarchy.ksh  $
# $Revision:  1.0 $
# $Modtime:   Feb 22  2018 $
########################################################

########################################################
#  This script has 3 steps:
#		1 - Filtering the RMS Source Calendar data
#			according to the horizon setup defined in
#			XXADEO_rpot_calendarYearsHorizon.txt config file
#		2 - Execute the Calendar
#			Transformation program in 2 steps
#		3 - Transform the Output file of step 2 from
#			Fixed Width format to CSV format.
#
#  The purpose of this is to transform the source
#  Calendar data file from RMS to the correct loadable
#  format by RPO.
#
########################################################

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="xxadeo_rpot_calendar_hierarchy"

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh
. ${RETLforXXADEO}/common/lang_funcs.ksh

RESOURCE_DIR="${RETLforXXADEO}/resources"

###############################################################################
#  FUNCTIONS
###############################################################################

##############################################################################
#
#  The log_message function allows logging a message with special characters
#
#     Usage: log_message $1 $2
#
#            $1
#                Type of error, like INFORMATION, ERROR, ...", which will be
#                displayed before the message in argument 2.
#            $2
#                Message with special characters to be logged.
#
##############################################################################
function log_message {
	if [[ $# -ne 2 ]]; then
		e_msg="ERROR: log_message: This function must have exactly 2 arguments,"
		e_msg+=" but it has $#."
		message "$e_msg"
		rmse_terminate 1
	else
		e_msg=$(echo -e "${1}"":""${2}")
		message "$e_msg"
	fi
}

##############################################################################
#
#  The check_error_and_clean_before_exit does a cleanup of temporary files created
#  during the execution of this script, before exiting the script.
#  It can cleanup the files in case of return code error, or simply clean them 
#  and exist, no matter the return code.
#
#     Usage: check_error_and_clean_before_exit  $1 [$2]
#
#            $1
#                Return Code
#            $2 (OPTIONAL)
#                CHECK (DEFAULT) - Check the return code and only cleanup and
#                        exist if return code is not Zero.
#                NO_CHECK - Does the cleanup and exist no matter the return code.
#
##############################################################################
function check_error_and_clean_before_exit {
	error_check=1
	if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]] ; then
		message "ERROR: Parameter 2 is not valid: ${2}"
		message "    Parameter 2 must be true, false or empty (true)"
		rmse_terminate 1
	elif [[ "${2}" = "NO_CHECK" ]] ; then
		error_check=0
	fi
	
	if [[ $1 -ne 0 && error_check -eq 1 ]]
	then
		rm -f "$FLT_INPUT_CALENDAR_FILE" \
			"$TR1_INPUT_CALENDAR_FILE" \
			"$TR2_INPUT_CALENDAR_FILE" \
			"$OUTPUT_CALENDAR_FILE"
			
		checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
		
	elif [[ error_check -eq 0 ]]
	then
		rm -f "$FLT_INPUT_CALENDAR_FILE" \
			"$TR1_INPUT_CALENDAR_FILE" \
			"$TR2_INPUT_CALENDAR_FILE"
		
		checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
		rmse_terminate $1
	fi
}

##################  INPUT/OUTPUT DEFINES ######################
########### (this section must come after INCLUDES) ###########

# Store locally the path of the inbound data directory
I_DATA_DIR=$RETL_IN/RPO
# Evaluate if the inbound data directory do not exist
if [ ! -r  $I_DATA_DIR ]; then
	e_msg="Inbound Data Dir does not exist or it is not readable. "
	e_msg+="\tDir: $I_DATA_DIR"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

INPUT_CALENDAR_FILE=$I_DATA_DIR/rmse_rpas_clndmstr.dat
# Evaluate if the Calendar input file do not exist
if [ ! -r  $INPUT_CALENDAR_FILE ]; then
	e_msg="Calendar input file does not exist or it is not readable. "
	e_msg+="\tFile: $INPUT_CALENDAR_FILE"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

FLT_INPUT_CALENDAR_FILE=$I_DATA_DIR/${PROGRAM_NAME}.flt.dat
TR1_INPUT_CALENDAR_FILE=$I_DATA_DIR/${PROGRAM_NAME}.tr1.dat
TR2_INPUT_CALENDAR_FILE=$I_DATA_DIR/${PROGRAM_NAME}.tr2.dat
OUTPUT_CALENDAR_FILE=$I_DATA_DIR/clnd.csv.dat

touch $FLT_INPUT_CALENDAR_FILE ; check_error_and_clean_before_exit $?
touch $TR1_INPUT_CALENDAR_FILE ; check_error_and_clean_before_exit $?
touch $TR2_INPUT_CALENDAR_FILE ; check_error_and_clean_before_exit $?
touch $OUTPUT_CALENDAR_FILE ; check_error_and_clean_before_exit $?



#  VDATE_FILE is the name of a file that contains the current VDATE
#  information in the format YYYYMMDD.
VDATE_FILE=$ETC_DIR/vdate.txt
# Evaluate if the VDATE config file exists.
if [ ! -r  $VDATE_FILE ]; then
	e_msg="VDATE config file does not exist or it is not readable.\n"
	e_msg+="\tFile: $VDATE_FILE\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

#  Read the vdate value, ignoring comment lines:
VDATE=`awk '{if(substr($0,1,1) != "#"){print $1;exit}}' $VDATE_FILE`

#  RPO_CLND_HORIZON_FILE is the name of a file that contains the setup
#  of the Calendar Horizon to be used in RPO.
# Evaluate if the Calendar Horizon config file exists.
RPO_CLND_HORIZON_FILE=$ETC_DIR/xxadeo_rpot_calendar_years_horizon.txt
# Evaluate if the Calendar Horizon config file exists.
if [ ! -r  $RPO_CLND_HORIZON_FILE ]; then
	e_msg="Calendar Horizon config file does not exist or it is not readable.\n"
	e_msg+="\tFile: $RPO_CLND_HORIZON_FILE\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

RPO_CLND_PAST_HORIZON=`awk -F"=" '{if(substr($0,1,1) != "#" && $1 == "PAST_HORIZON") {print $2; exit}}' $RPO_CLND_HORIZON_FILE`
RPO_CLND_FUTURE_HORIZON=`awk -F"=" '{if(substr($0,1,1) != "#" && $1 == "FUTURE_HORIZON") {print $2; exit}}' $RPO_CLND_HORIZON_FILE`

#  DATE_PREF is the name of a file that contains text indicating whether
#  the format of the Date Description field will be mm/dd/yyyy or dd/mm/yyyy
#  at this installation:
DATE_PREF=$ETC_DIR/xxadeo_rpot_date_format_preference.txt
# Evaluate if the Date Format Preference config file exists.
if [ ! -r  $DATE_PREF ]; then
	e_msg="Date Format Preference config file does not exist or it is not readable.\n"
	e_msg+="\tFile: $DATE_PREF\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

#  Read the date preference file, ignoring comment lines:
DATE_PREFERENCE=`awk '{if(substr($0,1,1) != "#"){print $1;exit}}' $DATE_PREF`

#  LAST_DOW is the name of a file that contains a day of week name or
#  abbreviation indicating which day of the week is considered to be the end
#  of the week for the fiscal calendar being used at this installation:
LAST_DOW=$ETC_DIR/xxadeo_rpot_last_day_week.txt
# Evaluate if the Last Day Week config file exists.
if [ ! -r  $LAST_DOW ]; then
	e_msg="Last Day Week config file does not exist or it is not readable.\n"
	e_msg+="\tFile: $LAST_DOW\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

#  Read the last day of week file, ignoring comment lines:
LAST_DAY_OF_WEEK=`awk '{if(substr($0,1,1) != "#"){print $1;exit}}' $LAST_DOW`

#  START_DOS is the name of a file that contains start week of 
#  the season for the fiscal calendar being used at this installation:
START_DOS=$ETC_DIR/first_day_of_season.txt

if [ -f $START_DOS ]; then
	#  Read the first day of the season file, ignoring comment lines:
	START_DAY_OF_SEASON=`awk '{if(substr($0,1,1) != "#"){print $1;exit}}' $START_DOS`
else
	START_DAY_OF_SEASON=1
fi

#  START_WOS is the name of a file that contains start week of season 
#  for the fiscal calendar being used at this installation:
START_WOS=$ETC_DIR/first_week_of_season.txt

if [ -f $START_WOS ]; then
	#  Read the first week of the season file, ignoring comment lines:
	START_WEEK_OF_SEASON=`awk '{if(substr($0,1,1) != "#"){print $1;exit}}' $START_WOS`
else
	START_WEEK_OF_SEASON=1
fi


#  WEEK_LABEL is the name of a file that contains the week label
#  format being used at this installation:
WEEK_LABEL=$ETC_DIR/xxadeo_rpot_week_date_preference.txt

if [ -f $WEEK_LABEL ]; then
	#  Read the week label format file, ignoring comment lines:
	WEEK_LABEL_FORMAT=`awk '{if(substr($0,1,1) != "#"){print $1;exit}}' $WEEK_LABEL`
else
	WEEK_LABEL_FORMAT="week_end_date"
fi


# This variable is to properly calculate the day and week of the season
#initialize CURRENT_SEASON
CURRENT_SEASON=-1

msg="Calendar Setup Information to be used in the transformation:\n"
msg+="\tVDATE: $VDATE\n"
msg+="\tRPO_CLND_PAST_HORIZONE: $RPO_CLND_PAST_HORIZON\n"
msg+="\tRPO_CLND_FUTURE_HORIZON: $RPO_CLND_FUTURE_HORIZON\n"
msg+="\tDATE_PREFERENCE: $DATE_PREFERENCE\n"
msg+="\tLAST_DAY_OF_WEEK: $LAST_DAY_OF_WEEK\n"
msg+="\tSTART_DAY_OF_SEASON: $START_DAY_OF_SEASON\n"
msg+="\tSTART_WEEK_OF_SEASON: $START_WEEK_OF_SEASON\n"
msg+="\tWEEK_LABEL_FORMAT: $WEEK_LABEL_FORMAT\n"
log_message DEBUG  "$msg"

##################  LANGUAGE DETERMINATION ######################

if [[ -z "$RESOURCE_DIR" ]]; then
	echo "RESOURCE_DIR not set, quiting..."
	exit 2
fi

log_message DEBUG "Preferred Language Setup: $PREFERRED_LANG"
SetPreferredLang

. $RESOURCE_DIR/retl_msgs.$PREFERRED_LANG 

########################## MAIN ########################

log_message INFORMATION "Source Calendar Filtering AWK program running ..."

#####################################################################################
#
#  The following awk script is filtering the original calendar extract from
#  RMS to include only the necessary RPO dates, which are the dates belonging
#  to X number of years in the past, the dates of the current year and the
#  dates of Y number of years in the future.
#
#######################  Beginning of awk script No. 1  #############################

LC_ALL=C awk -v vdate="$VDATE"  \
	-v clnd_past_horizon="$RPO_CLND_PAST_HORIZON" \
	-v clnd_future_horizon="$RPO_CLND_FUTURE_HORIZON" \ '
BEGIN{

out_file = ARGV[2]; ARGV[2] = ""
bsaloglevel = ARGV[3]; ARGV[3] = ""

last_clnd_record = ""

if ( bsaloglevel == "DEBUG" || bsaloglevel == "ALL" ) {
	print"DEBUG - out_file: ", out_file
	print"DEBUG - vdate: ", vdate
}

vdate_year = substr(vdate, 1, 4)
vdate_year_num = 1 * vdate_year  #  Convert the vdate year to a numeric value.

if ( vdate_year_num !~ /^[0-9]+$/ && (vdate_year_num < 1 || vdate_year_num > 9999) ) {
	print" ***** Error - Bad vdate format."
	print" vdate=", vdate
	print" Examine the config file: vdate.txt"
	exit
}

if (clnd_past_horizon !~ /^[0-9]+$/ && clnd_future_horizon !~ /^[0-9]+$/) {
	print" ***** Error - Bad horizon setup format."
	print" PAST_HORIZON=", clnd_past_horizon
	print" FUTURE_HORIZON=", clnd_future_horizon
	print" Examine the config file: XXADEO_rpot_calendarYearsHorizon.txt"
	exit
}

start_year = vdate_year_num - clnd_past_horizon
end_year = vdate_year_num + clnd_future_horizon
print" Calendar Horizon Filtered - Start Year: ", start_year, ", End Year (Inclusive): ", end_year

if ( bsaloglevel == "DEBUG" || bsaloglevel == "ALL" ) {
	print"DEBUG - start_year: ", start_year
	print"DEBUG - end_year: ", end_year
}

}
# ==================  Beginning of main program   =========================

#  Begin reading calendar data records:
{

record_year = substr($0, 1, 4)
if (record_year > end_year) {
	print" Last date in the filtered Calendar (YYYYMMDD): ", substr(last_clnd_record, 12, 8)
	
	if (bsaloglevel == "ALL" ) {
		print"record_year: ", record_year, ", end_year: ", end_year
	}
	
	exit
}
else if(record_year >= start_year)
{
	if (last_clnd_record == "") {
		print" First date in the filtered Calendar (YYYYMMDD): ", substr($0, 12, 8)
	}
	
	if (bsaloglevel == "ALL") {
		print"record_year: ", record_year, ", end_year: ", end_year, ", record: ", $0
	}
	
	printf( "%s\n" , $0 ) > out_file
	
	last_clnd_record = $0
}

}

' $INPUT_CALENDAR_FILE $FLT_INPUT_CALENDAR_FILE $BSA_LOG_LEVEL

#######################  End of awk script No. 1  ###################################

exit_stat=$?
log_message INFORMATION "Source Calendar Filtering AWK program completed. Exit status: $exit_stat"

if [[ $exit_stat -ne 0 ]]; then
	e_msg="There was an error with the Source Calendar Filtering AWK program.\n"
	e_msg+="\tCheck the error log file: $ERR_FILE\n"
	e_msg+="\tCheck the log file: $LOG_FILE\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit $exit_stat
fi

nbr_x_recs=`wc -l  $INPUT_CALENDAR_FILE  |                 \
		   awk '{if(NF >= 1) print $1; else print 0; exit}'`
nbr_c_recs=`wc -l   $FLT_INPUT_CALENDAR_FILE |                    \
		   awk '{if(NF >= 1) print $1; else print 0; exit}'`

msg="Number of records after filtering:\n"
msg+="\tOriginal input file number of records: $nbr_x_recs\n"
msg+="\tOriginal input file path: $INPUT_CALENDAR_FILE\n"
msg+="\tFiltered input file number of records: $nbr_c_recs\n"
msg+="\tFiltered input file path: $FLT_INPUT_CALENDAR_FILE\n"
log_message INFORMATION "$msg"

log_message INFORMATION "Preliminary Calendar Transformation AWK program running ..."

#####################################################################################
#
#  Input file is readable and is not empty -
#  Call the clndhier.awk script to do the preliminary
#  processing of the extracted calendar data, storing 
#  the new version of the calendar data in a temporary
#  file:
#
#######################  Beginning of awk script No. 2  #############################

LC_ALL=C awk -f $LIB_DIR/clndhier.awk \
	-v day_1="$day_1" \
	-v day_2="$day_2" \
	-v day_3="$day_3" \
	-v day_4="$day_4" \
	-v day_5="$day_5" \
	-v day_6="$day_6" \
	-v day_7="$day_7" \
	-v invalid_dow="$invalid_dow" \
	-v month_01="$month_01" \
	-v month_02="$month_02" \
	-v month_03="$month_03" \
	-v month_04="$month_04" \
	-v month_05="$month_05" \
	-v month_06="$month_06" \
	-v month_07="$month_07" \
	-v month_08="$month_08" \
	-v month_09="$month_09" \
	-v month_10="$month_10" \
	-v month_11="$month_11" \
	-v month_12="$month_12" \
	-v invalid_month="$invalid_month" \
	-v year_id_affix="$year_id_affix" \
	-v year_desc_affix="$year_desc_affix" \
	-v half_id_affix="$half_id_affix" \
	-v half_desc_affix="$half_desc_affix" \
	-v qrtr_id_affix="$qrtr_id_affix" \
	-v qrtr_desc_affix="$qrtr_desc_affix" \
	-v mnth_id_affix="$mnth_id_affix" \
	-v week_id_affix="$week_id_affix" \
	-v week_desc_affix="$week_desc_affix" \
	-v date_sep="$date_sep" \
	$FLT_INPUT_CALENDAR_FILE > $TR1_INPUT_CALENDAR_FILE

#######################  End of awk script No. 2  ###################################

exit_stat=$?
log_message INFORMATION "Preliminary Calendar Transformation AWK program completed. Exit status: $exit_stat"

if [[ $exit_stat -ne 0 ]]; then
	e_msg="There was an error with the Preliminary Calendar Transformation AWK program.\n"
	e_msg+="\tCheck the error log file: $ERR_FILE\n"
	e_msg+="\tCheck the log file: $LOG_FILE\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit $exit_stat
fi

nbr_x_recs=`wc -l  $FLT_INPUT_CALENDAR_FILE  |                 \
		   awk '{if(NF >= 1) print $1; else print 0; exit}'`
nbr_c_recs=`wc -l   $TR1_INPUT_CALENDAR_FILE |                    \
		   awk '{if(NF >= 1) print $1; else print 0; exit}'`

#  Verify that the temp file contains the same number of records
#  as the original input file:
if [[ $nbr_c_recs -ne $nbr_x_recs || nbr_c_recs -le 0 ]]; then
	e_msg="clndhier.awk error - output file has wrong number of records .\n"
	e_msg+="\tOutput file: $TR1_INPUT_CALENDAR_FILE\n"
	e_msg+="\tOutput file has $nbr_c_recs records - input file has $nbr_x_recs\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

log_message INFORMATION "Main Calendar Transformation AWK program running ..."

#####################################################################################
#
#  The following awk script completes the processing necessary to convert
#  the extracted calendar data to a file that can be uploaded into RDF: 
#
#######################  Beginning of awk script No. 2  #############################

LC_ALL=C awk  -v date_pref=$DATE_PREFERENCE   -v end_of_week_nm=$LAST_DAY_OF_WEEK  \
	-v day_abbrev_1="$day_abbrev_1" \
	-v day_abbrev_2="$day_abbrev_2" \
	-v day_abbrev_3="$day_abbrev_3" \
	-v day_abbrev_4="$day_abbrev_4" \
	-v day_abbrev_5="$day_abbrev_5" \
	-v day_abbrev_6="$day_abbrev_6" \
	-v day_abbrev_7="$day_abbrev_7" \
	-v day_1="$day_1" \
	-v day_2="$day_2" \
	-v day_3="$day_3" \
	-v day_4="$day_4" \
	-v day_5="$day_5" \
	-v day_6="$day_6" \
	-v day_7="$day_7" \
	-v month_01="$month_01" \
	-v month_02="$month_02" \
	-v month_03="$month_03" \
	-v month_04="$month_04" \
	-v month_05="$month_05" \
	-v month_06="$month_06" \
	-v month_07="$month_07" \
	-v month_08="$month_08" \
	-v month_09="$month_09" \
	-v month_10="$month_10" \
	-v month_11="$month_11" \
	-v month_12="$month_12" \
	-v date_sep="$date_sep" \
	-v half_id_affix="$half_id_affix" \
	-v year_desc_affix="$year_desc_affix" \
	-v bsaloglevel="$BSA_LOG_LEVEL" \ '
BEGIN{

day_abbrev[1] = day_abbrev_7
day_abbrev[2] = day_abbrev_1
day_abbrev[3] = day_abbrev_2
day_abbrev[4] = day_abbrev_3
day_abbrev[5] = day_abbrev_4
day_abbrev[6] = day_abbrev_5
day_abbrev[7] = day_abbrev_6

day_name[1] = day_7
day_name[2] = day_1
day_name[3] = day_2
day_name[4] = day_3
day_name[5] = day_4
day_name[6] = day_5
day_name[7] = day_6

mon_abbrev[1]  = substr(month_01, 1, 3)
mon_abbrev[2]  = substr(month_02, 1, 3)
mon_abbrev[3]  = substr(month_03, 1, 3)
mon_abbrev[4]  = substr(month_04, 1, 3)
mon_abbrev[5]  = substr(month_05, 1, 3)
mon_abbrev[6]  = substr(month_06, 1, 3)
mon_abbrev[7]  = substr(month_07, 1, 3)
mon_abbrev[8]  = substr(month_08, 1, 3)
mon_abbrev[9]  = substr(month_09, 1, 3)
mon_abbrev[10] = substr(month_10, 1, 3)
mon_abbrev[11] = substr(month_11, 1, 3)
mon_abbrev[12] = substr(month_12, 1, 3)

mon_name[1]  = month_01
mon_name[2]  = month_02
mon_name[3]  = month_03
mon_name[4]  = month_04
mon_name[5]  = month_05
mon_name[6]  = month_06
mon_name[7]  = month_07
mon_name[8]  = month_08
mon_name[9]  = month_09
mon_name[10] = month_10
mon_name[11] = month_11
mon_name[12] = month_12


out_file = ARGV[2]; ARGV[2] = ""
last_day_of_week = ARGV[3]; ARGV[3] = ""
day_of_season = ARGV[4]; ARGV[4] = ""
week_of_season = ARGV[5]; ARGV[5] = ""
current_season = ARGV[6]; ARGV[6] = ""
week_label_format = ARGV[7]; ARGV[7] = ""

if( vflg == 1)  {
	print" date_pref  = " , date_pref
	print" end_of_week  = " ,end_of_week
	print" out_file =", out_file
	print" week_label_format=", week_label_format
}
#  Convert the end-of-fiscal-week day abbrev. (obtained  
# from the LAST_DOW file) to a number (1-7):

end_of_week = 0
eow_uc = toupper(end_of_week_nm)
for(i=1; i<=7; i++)  {
	
	if ( bsaloglevel == "DEBUG" || bsaloglevel == "ALL" ) {
		print"DEBUG - Check if: ", eow_uc, ", is equal to: ", toupper(day_name[i])
	}
	
	if(eow_uc == toupper(day_name[i]))  {
		end_of_week = i - 1
		if( end_of_week <= 0 ) end_of_week = 7
		break
	}
}

if ( bsaloglevel == "DEBUG" || bsaloglevel == "ALL" ) {
	print"DEBUG - end_of_week_nm: ", end_of_week_nm, ", end_of_week: ", end_of_week
}

if(end_of_week == 0)  {
	print" ***** Error - Bad end-of-week day supplied during installation."
	print" end_of_week_nm =", end_of_week_nm
	print" Examine the config file: XXADEO_rpot_lastDayOfWeek.txt"
	exit
}

if(week_label_format != "week_start_date" && week_label_format != "week_end_date")
{
	print" ***** Error - Bad week-label-format supplied during installation."
	print" week_label_format=", week_label_format
	print" Examine the config file: XXADEO_rpot_weekDatePreference.txt"
}

}  #  End of BEGIN block

# ==================  Beginning of main program   =========================

#  Begin reading calendar data records:
{
date_id = substr($1, 1, 8)  #  This is the calendar date in the yyyymmdd format.
mon = substr($1, 5, 2)      #  Numeric month (1-12).
day = substr($1, 7, 2)      #  Numeric day of month (01-31).
year = substr($1, 1, 4)

#  Format the calendar date as either mm/dd/yyy or dd/mm/yyyy according  
#  to the date format preference obtained from the DATE_PREF file:

date_format = tolower(date_pref)
if(date_format == "mm/dd/yyyy")  date_desc = mon  date_sep day  date_sep year
else if(date_format == "dd/mm/yyyy")  date_desc = day  date_sep mon  date_sep year
else  {
	print" ***** Error - Date preference must be either mm/dd/yyyy or dd/mm/yyyy"
	print" date_format =", date_format 
	print" Possible bad date preference supplied during installation."
	print" Examine the config file: XXADEO_rpot_dateFormatPreference.txt"
	exit
}
week_id = substr($2, 1, 8)  #  For example: W05_2003
day_of_week_abbrev = $11    #  For example: Wed

#  dow_num gets the number of the day of the calendar week (1-7):
dow = dow_num(day_of_week_abbrev)
idow = dow_idx(day_of_week_abbrev)

if (bsaloglevel == "ALL") {
	print"dow: ", dow, ", day_of_week_abbrev: ", day_of_week_abbrev
}

#  The end_of_week_date function calculates the calendar date of the end of 
#  the fiscal week for the current calendar date. The variable week_desc is 
#  returned by the end_of_week_date function in mm/dd/yyyy format:

if(week_label_format == "week_start_date") {
	if ( bsaloglevel == "ALL" ) {
		print"start_of_week_date - DOW: ", dow, " DAY: ", day, " MON: ", mon, " YEAR: ", year, " EOW: ", end_of_week
	}
	week_desc = start_of_week_date(dow, mon, day, year, end_of_week)
	
	if ( bsaloglevel == "ALL" ) {
		print"start_of_week_date - week_desc: ", week_desc
	}
}
else {
	week_desc = end_of_week_date(dow, mon, day, year, end_of_week)
}

mnth_id = substr($3, 1, 8)  #  For example: M06_1996
m = substr($3, 14)          #  Month number (1-12)
mon_num = 1 * mon           #  Convert string value to numeric
mnth_desc = mon_abbrev[mon_num] " " substr($3, 9, 4)
qrtr_id = $4
qrtr_desc = substr($5, 6, 2) " " substr($5, 1, 4)
half_id = half_id_affix substr($6, 2, 6)
half_desc = half_id_affix substr($8, 1, 1) ", " year_desc_affix substr($8, 3, 4)
# intialize the CURRENT_SEASON only once, with the data coming from RMS  
# to handle the problem of starting the day of season from the correct
# no. 
if(current_season == -1) {
	current_season = substr($6, 2, 1)
}
year_id = "Y" substr($9, 2, 4)
year_desc = substr($10, 3, 4)
dow_id = day_abbrev[idow]
dow_desc = day_name[idow]
dos_id = "DOS" next_day_of_season(substr($6, 2, 1))
dos_desc = "DOS " substr(dos_id, 4, 3)
woy_id = "WY" substr($2, 2, 2)
woy_desc = "Week " substr($2, 2, 2)
wos_id = "WS" next_week_of_season(substr($6, 2, 1), dow_desc)
wos_desc = "WOS " substr(wos_id, 3, 2)

printf( "%-8s" , date_id )  >  out_file
printf( "%-20s", date_desc )  >  out_file
printf( "%-8s" , week_id )  >  out_file
printf( "W %-18s", week_desc )  >  out_file
printf( "%-8s" , mnth_id )  >  out_file
printf( "%-20s", mnth_desc )  >  out_file
printf( "%-7s" , qrtr_id )  >  out_file
printf( "%-20s", qrtr_desc )  >  out_file
printf( "%-7s" , half_id )  >  out_file
printf( "%-20s", half_desc )  >  out_file
printf( "%-5s" , year_id )  >  out_file
printf( "%-20s", year_desc )  >  out_file
printf( "%-3s" , dow_id )  >  out_file
printf( "%-20s", dow_desc )  >  out_file
printf( "%-6s",  dos_id )  >  out_file
printf( "%-20s",  dos_desc )  >  out_file
printf( "%-4s", woy_id )  >  out_file
printf( "%-20s", woy_desc )  >  out_file
printf( "%-4s", wos_id )  >  out_file
printf( "%-20s", wos_desc )  >  out_file
printf( "\n"   )  >  out_file
}
# =========================  End of main program  ===========================

##################################################################
#  The start_of_week_date function calculates the calendar date  #
#  for the start of the fiscal week for any given calendar date. #
##################################################################

function start_of_week_date (day_of_wk, cmon, cday, cyr, last_day_of_week)  {

	split("31 0 31 30 31 30 31 31 30 31 30 31",ndays)

	cmon_num = 1 * cmon  #  Convert the current month to a numeric value.

	#  Get the number of days in February:
	ndays[2] = feb_days(cyr)

	#  Compute the day of the month that is the start of the fiscal week:
	sow_day = cday - day_of_wk + 1

	sow_mon = cmon
	sow_yr = cyr

	if ( bsaloglevel == "ALL" ) {
		print"start_of_week_date - sow_day: ", sow_day
	}

	#  If the calculated day of the month is smaller than 0, adjust it
	#  to decrease the month:
	if(sow_day < 1)  {

		sow_mon = cmon - 1

		#  If the calculated month below 0, set it to one and decrease the year:
		if(sow_mon < 1)  {
			sow_mon = 12
			sow_yr = cyr - 1
		}

		sow_day = ndays[sow_mon] + sow_day
	}
  
	#  Single digit days or months must be given a leading zero:
	if(length(sow_day) < 2 )  sow_day_str = "0" sow_day
	else  sow_day_str = sow_day
	if(length(sow_mon) < 2 )  sow_mon_str = "0" sow_mon
	else  sow_mon_str = sow_mon

	#  Build the date string based on client preference for date format:
	if(date_format == "mm/dd/yyyy")  {
		sow_date = sow_mon_str date_sep sow_day_str date_sep sow_yr
	}
	else  sow_date = sow_day_str date_sep sow_mon_str date_sep sow_yr
	return sow_date
}  #  End of Function start_of_week_date

################################################################
#  The end_of_week_date function calculates the calendar date  #
#  for the end of the fiscal week for any given calendar date. #
################################################################

function end_of_week_date (day_of_wk, cmon, cday, cyr, last_day_of_week)  {

	split("31 0 31 30 31 30 31 31 30 31 30 31",ndays)

	cmon_num = 1 * cmon  #  Convert the current month to a numeric value.

	#  Get the number of days in February:
	ndays[2] = feb_days(cyr)

	#  Compute the day of the month that is the end of the fiscal week:
	eow_day = cday + 7 - day_of_wk

	eow_mon = cmon
	eow_yr = cyr
	
	#  If the calculated day of the month is greater than the number of 
	#  days in the current month, adjust it and increment the month:
	if(eow_day > ndays[cmon_num])  {
		eow_day = eow_day - ndays[cmon_num]
		eow_mon = cmon + 1
	}
	#  If the calculated month is 13, set it to one and increment the year:
	if(eow_mon > 12)  {
		eow_mon = 1
		eow_yr = cyr + 1
	}
	#  Single digit days or months must be given a leading zero:
	if(length(eow_day) < 2 )  eow_day_str = "0" eow_day
	else  eow_day_str = eow_day
	if(length(eow_mon) < 2 )  eow_mon_str = "0" eow_mon
	else  eow_mon_str = eow_mon
  
	#  Build the date string based on client preference for date format:
	if(date_format == "mm/dd/yyyy")  {
		eow_date = eow_mon_str date_sep eow_day_str date_sep eow_yr
	}
	else  eow_date = eow_day_str date_sep eow_mon_str date_sep eow_yr
	return eow_date
}  #  End of Function end_of_week_date

################################################################
#  Function feb_days computes the number of days in February,  #
#  taking leap years into account.                             #
################################################################

function feb_days (year)  {
	
	#  If the year is not evenly divisible by 4 it is not a leap year:
	if(year % 4 != 0)  return 28  
	
	#  If evenly divisible by 4 but not by 100, it is a leap year:
	if(year % 100 != 0)  return 29   
	else  {
		#  If evenly divisible by 100 and also by 400, it is also a leap year:
		if(year % 400 == 0) return 29

		#  If evenly divisible by 100 but not by 400, it is not a leap year:
		else  return 28
	}
}  #  End of Function feb_days


####################################################################
#  Function next_day_of_season computes the next day in the season #
####################################################################

function next_day_of_season (season)  {
	
	if(current_season != season)
	{
		# update the current_season value if season is change
		# and reset the dos and wos
		current_season = season
		day_of_season = 1
		week_of_season = 1 
	}
	
	if(length(day_of_season) == 1)
	{
		return "0" day_of_season++
	}
	
	return day_of_season++
}  #  End of Function day_of_season 

######################################################################
#  Function next_week_of_season computes the next week in the season #
######################################################################

function next_week_of_season (season, day)  {
	
	wos = week_of_season
	
	if(day == last_day_of_week)
	{
		week_of_season++
	}
	if(length(wos) == 1)
	{
		return "0" wos 
	}
	return wos
}

################################################################################
#  Function dow_num converts the day of week abbreviation (dow_abbr) from the  #
#  clndhier.awk output file back to a calendar day of week number.             #
#  The clndhier.awk script generates a day of week abbrev. by using a fiscal   #
#  day of week number (1-7) from its input file (clndmstr.dat - the extracted  #
#  file) and assuming that the last day of the fiscal week is always Sunday.   #
#  This is not always the case for every client. Therefore a number (1-7)      #
#  indicating the last day of the fiscal week for the current installation     #
#  (which is derived from the day name in the LAST_DOW file) must be used      # 
#  here (variable name is end_of_week) to recalculate the true calendar day    #
#  of week number.                                                             #
################################################################################

function dow_num (dow_abbr)  { 

	#  Convert the day of week abbrev. to a number (1-7) to get the number that
	#  was in the original extracted date file, taking into account that Monday
	#  was assumed to be the first day of the fiscal week:
	 
	extract_file_dow = ""
	for( i = 1; i <= 7; i++)  {
		if( toupper(day_abbrev[i]) == toupper(dow_abbr))  {
			extract_file_dow = i - 1
			if( extract_file_dow <= 0 ) extract_file_dow = 7
			break
		}
	}
	
	if ( bsaloglevel == "ALL" ) {
		print"dow_abbr: ", dow_abbr, ", extract_file_dow: ", extract_file_dow
	}
	
	if( extract_file_dow == "")  {
		print" ********  Error - Bad day of week in extract file  ********"
		print" record no." NR
		print $0 
		print " dow_abbr =", dow_abbr  
		print" extract_file_dow =",  extract_file_dow
		exit
	}
	
	#  Compute the true day of the calendar week number using the number derived 
	#  above (extract_file_dow) that was in the extracted date input file and the
	#  end of fiscal week number (end_of_week) that was obtained from the
	#  LAST_DOW file (in the main program). Since it is always a number from 
	#  one through seven, modular arithmetic (%) is used:
	
	
	day_of_week = ( extract_file_dow + 7 - end_of_week - 1 ) % 7 + 1
	
	if ( bsaloglevel == "ALL" ) {
		print"day_of_week: ", day_of_week, ", dow_abbr: ", dow_abbr, ", end_of_week: ", end_of_week
	}
	
	return  day_of_week
	
}  #  End of Function dow_num

################################################################################
#                                                                              #
################################################################################

function dow_idx (dow_abbr)  {

	#  Comment

	extract_file_dow_idx = ""
	for( i = 1; i <= 7; i++)  {
		if( toupper(day_abbrev[i]) == toupper(dow_abbr))  {
			extract_file_dow_idx = i
			break
		}
	}

	return extract_file_dow_idx

}  #  End of Function dow_idx

' $TR1_INPUT_CALENDAR_FILE \
	$TR2_INPUT_CALENDAR_FILE \
	$LAST_DAY_OF_WEEK \
	$START_DAY_OF_SEASON \
	$START_WEEK_OF_SEASON \
	$CURRENT_SEASON \
	$WEEK_LABEL_FORMAT

#######################  End of awk script No. 3  ###################################

exit_stat=$?
log_message INFORMATION "Main Calendar Transformation AWK program completed. Exit status: $exit_stat"

if [[ $exit_stat -ne 0 ]]; then
	e_msg="There was an error with the Main Calendar Transformation AWK program.\n"
	e_msg+="\tCheck the error log file: $ERR_FILE\n"
	e_msg+="\tCheck the log file: $LOG_FILE\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit $exit_stat
fi

log_message INFORMATION "Output file transformation to CSV format AWK program running ..."

#####################################################################################
#
#  The following awk script is transforming the FIXED_WIDTH format of the 
#  output file of the RDF Standard Calendar Transformation program into
#  CSV format.

#######################  Beginning of awk script No. 4  #############################

LC_ALL=C awk '
BEGIN{

out_file = ARGV[2]; ARGV[2] = ""

#  Use COMMA (,) as the FIELD output separator, as per CSV format
OFS=","

#  Input File Fixed Width Format to separate each RECORD in the correspondent 
#  20 FIELDS, according to the export of the RDF Standard Calendar Transformation:
#		 1 - DATE_ID
#		 2 - DATE_LABEL
#		 3 - WEEK_ID
#		 4 - WEEK_LABEL
#		 5 - MONTH_ID
#		 6 - MONTH_LABEL
#		 7 - QUARTER_ID
#		 8 - QUARTER_LABEL
#		 9 - SEASON_ID
#		10 - SEASON_LABEL
#		11 - YEAR_ID
#		12 - YEAR_LABEL
#		13 - DAY_OF_WEEK_ID
#		14 - DAY_OF_WEEK_LABEL
#		15 - DAY_OF_SEASON_ID
#		16 - DAY_OF_SEASON_LABEL
#		17 - WEEK_OF_YEAR_ID
#		18 - WEEK_OF_YEAR_LABEL
#		19 - WEEK_OF_SEASON_ID
#		20 - WEEK_OF_SEASON_LABEL
FIELDWIDTHS="8 20 8 20 8 20 7 20 7 20 5 20 3 20 6 20 4 20 4 20"

}
# ==================  Beginning of main program   =========================

#  Begin reading calendar data records:
{
	#  Loop to manipulate each FIELD in each of the RECORDS of the INPUT file
	for(i=1; i<=NF; i++) {
		
		#  Remove all trailing spaces from the FIELD $i
		gsub(/[[:space:]]+$/,"",$i)
		
		#  Escape all DOUBLE_QUOTES found in FIELD $i, as per CSV by
		#  replacing them by two DOUBLE_QUOTES.
		gsub(/\"/,"\"\"",$i)
		
		#  Encapsulate FIELD $i in DOUBLE_QUOTES if it contains a COMMA (,)
		if( $i ~ /,/ ) { $i = "\""$i"\"" }
	}	

	# Output only the required RPO Calendar FIELDS, which are:
	#		 1 - DATE_ID
	#		 2 - DATE_LABEL
	#		 3 - WEEK_ID
	#		 4 - WEEK_LABEL
	#		 5 - MONTH_ID
	#		 6 - MONTH_LABEL
	#		 7 - QUARTER_ID
	#		 8 - QUARTER_LABEL
	#		 9 - YEAR_ID
	#		10 - YEAR_LABEL
	print $1,$2,$3,$4,$5,$6,$7,$8,$11,$12 > out_file

}
' $TR2_INPUT_CALENDAR_FILE $OUTPUT_CALENDAR_FILE

#######################  End of awk script No. 4  ###################################

exit_stat=$?
log_message INFORMATION "Output file transformation to CSV format AWK program completed. Exit status: $exit_stat"

if [[ $exit_stat -ne 0 ]]; then
	e_msg="There was an error with the Output file transformation to CSV format AWK program.\n"
	e_msg+="\tCheck the error log file: $ERR_FILE\n"
	e_msg+="\tCheck the log file: $LOG_FILE\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit $exit_stat
fi


#  Verify that the output file was created successfully and has the same
#  number of records as the input file:
if [ -s $OUTPUT_CALENDAR_FILE ]; then

	#  Non-empty output file was created - verify matching number of records:
	nrecs_src=`wc -l $INPUT_CALENDAR_FILE | awk '{if(NF >= 1) print $1; else print 0; exit}'`
	nrecs_filt=`wc -l $TR2_INPUT_CALENDAR_FILE | awk '{if(NF >= 1) print $1; else print 0; exit}'`
	nrecs_out=`wc -l $OUTPUT_CALENDAR_FILE | awk '{if(NF >= 1) print $1; else print 0; exit}'`
	if [[ $nrecs_src -lt $nrecs_filt && $nrecs_filt -ne $nrecs_out ]]; then
		e_msg="Output file has wrong number of records.\n"
		e_msg+="\tOriginal # Recs: $nrecs_src, Filtered # Recs: $nrecs_filt, Output # Recs: $nrecs_out\n"
		e_msg+="\tSource file: $INPUT_CALENDAR_FILE\n"
		e_msg+="\tOutput file: $OUTPUT_CALENDAR_FILE\n"
		log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
	fi
else

	#  An error has occurred - determine whether the output file was not
	#  produced, or was produced but is 0 bytes:
	if [ -f $OUTPUT_CALENDAR_FILE ]; then
		e_msg+="Output file $OUTPUT_CALENDAR_FILE was created but contains no data."
		log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
	else
		e_msg+="No output file was produced ($OUTPUT_CALENDAR_FILE)."
		log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
		
	fi
fi

#  Log the number of records written to the output file:
log_num_recs $OUTPUT_CALENDAR_FILE

#  Delete the status file:
if [[ -f  $STATUS_FILE ]]; then rm  $STATUS_FILE; fi

log_message INFORMATION "Program completed successfully."
check_error_and_clean_before_exit 0 "NO_CHECK"

