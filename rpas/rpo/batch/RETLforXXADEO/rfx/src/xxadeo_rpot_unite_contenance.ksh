#!/bin/ksh
###############################################################################
#  $Workfile:   xxadeo_rpot_unite_contenance.ksh
#  $Modtime:    Jul 23 2018
###############################################################################

###############################################################################
#  
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################

PROGRAM_NAME='xxadeo_rpot_unite_contenance'

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")

###############################################################################
#  INCLUDES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh

###############################################################################
#  FUNCTIONS
###############################################################################

##############################################################################
#
#  The log_message function allows logging a message with special characters
#
#     Usage: log_message $1 $2
#
#            $1
#                Type of error, like INFORMATION, ERROR, ...", which will be
#                displayed before the message in argument 2.
#            $2
#                Message with special characters to be logged.
#
##############################################################################
function log_message {
	if [[ $# -ne 2 ]]; then
		e_msg="ERROR: log_message: This function must have exactly 2 arguments,"
		e_msg+=" but it has $#."
		message "$e_msg"
		rmse_terminate 1
	else
		e_msg=$(echo -e "${1}"":""${2}")
		message "$e_msg"
	fi
}

##############################################################################
#
#  The check_error_and_clean_before_exit does a cleanup of temporary files created
#  during the execution of this script, before exiting the script.
#  It can cleanup the files in case of return code error, or simply clean them 
#  and exist, no matter the return code.
#
#     Usage: check_error_and_clean_before_exit  $1 [$2]
#
#            $1
#                Return Code
#            $2 (OPTIONAL)
#                CHECK (DEFAULT) - Check the return code and only cleanup and
#                        exist if return code is not Zero.
#                NO_CHECK - Does the cleanup and exist no matter the return code.
#
##############################################################################
function check_error_and_clean_before_exit {
	error_check=1
	if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
	then
		message "ERROR: Parameter 2 is not valid: ${2}"
		message "    Parameter 2 must be true, false or empty (true)"
		rmse_terminate 1
	elif [[ "${2}" = "NO_CHECK" ]]
	then
		error_check=0
	fi
	#message "check_error_and_clean_before_exit: error_check: ${error_check}"
	
	if [[ $1 -ne 0 && error_check -eq 1 ]]
	then
		rm -f "${I_ATTR_FILE}.${PROGRAM_NAME}.retl" \
			"${I_ITEMATTR_FILE}.${PROGRAM_NAME}.retl" \
			"${RETL_OUTPUT_FILE}.${PROGRAM_NAME}.retl" \
			"${RETL_OUTPUT_FILE}"
		
		checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
		
	elif [[ error_check -eq 0 ]]
	then
		rm -f "${I_ATTR_FILE}.${PROGRAM_NAME}.retl" \
			"${I_ITEMATTR_FILE}.${PROGRAM_NAME}.retl" \
			"${RETL_OUTPUT_FILE}.${PROGRAM_NAME}.retl"
		
		checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
		rmse_terminate $1
	fi
}

###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################

# Store locally the path of the inbound data directory
I_DATA_DIR=$RETL_IN/RPO
# Evaluate if the inbound data directory do not exist
if [ ! -r  $I_DATA_DIR ]; then
	e_msg="Inbound Data Dir does not exist or it is not readable.\n"
	e_msg+="\tINBOUND_DATA_DIR: $I_DATA_DIR\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

# Patterns to look for the source files to be used in the transformation
I_ATTR_F_PATTERN=XXRMS209_ATTR_*.dat
I_ITEMATTR_F_PATTERN=XXRMS209_ITEMATTR_*.dat

# Get latest available input files, because since this interface works in a
# FULL mode there is no need to process all available input files.
I_ATTR_FILE=`find $I_DATA_DIR -name ${I_ATTR_F_PATTERN} | sort | tail -n 1`
if [ ! -r  $I_ATTR_FILE ]; then
	e_msg="Attributes input file does not exist or it is not readable.\n"
	e_msg+="\tI_ATTR_FILE: $I_ATTR_FILE\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

I_ITEMATTR_FILE=`find $I_DATA_DIR -name ${I_ITEMATTR_F_PATTERN} | sort | tail -n 1`
# Evaluate if the Item UDAs input file do not exist
if [ ! -r  $I_ITEMATTR_FILE ]; then
	e_msg="Item Attributes input file does not exist or it is not readable.\n"
	e_msg+="\tI_ITEMATTR_FILE: $I_ITEMATTR_FILE\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

# Path for the transformed output file.
RETL_OUTPUT_FILE=$I_DATA_DIR/nbituocap.csv.rpl_$TIMESTAMP
# Evaluate if the ouput data file is writable
touch $RETL_OUTPUT_FILE ; check_error_and_clean_before_exit $?

# Interface Configuration file
CONFIG_FILE=$ETC_DIR/$PROGRAM_NAME.txt
# Evaluate if the interface config file do not exist
if [ ! -r  $CONFIG_FILE ]; then
	e_msg="ERROR: Interface config file does not exist or it is not readable. "
	e_msg+= "Config File: $CONFIG_FILE"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

# Get Abonnement BU UDA ID
ATTR_KEY_CONFIG_LINE=$(cat ${CONFIG_FILE} | grep -v "^[[:blank:]]*#" | grep "UNITE_CONTENANCE_ATTR_KEY")
log_message DEBUG "ATTR_KEY_CONFIG_LINE: $ATTR_KEY_CONFIG_LINE"
ATTR_KEY=$(echo ${ATTR_KEY_CONFIG_LINE} | cut -d"|" -f3 | tr -d '[[:blank:]]')
log_message DEBUG "ATTR_KEY: $ATTR_KEY"
# Evaluate if the UDA Key was retrieved successfully
if [ -z $ATTR_KEY ]; then
	e_msg="ERROR: Interface config file not correctly set."
	e_msg+="Please check Config File: $CONFIG_FILE"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

###############################################################################
#  RETL input/output schemas
###############################################################################


I_ATTR_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_attr.schema
# Evaluate if the UDAs Schema exists.
if [ ! -r  $I_ATTR_SCHEMA ]; then
	e_msg="ERROR: Interface schema file does not exist or it is not readable. "
	e_msg+="Schema File: $I_ATTR_SCHEMA"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

I_ITEMATTR_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_itemattr.schema
# Evaluate if the Items-UDAs Schema exists.
if [ ! -r  $I_ITEMATTR_SCHEMA ]; then
	e_msg="ERROR: Interface schema file does not exist or it is not readable. "
	e_msg+="Schema File: $I_ITEMATTR_SCHEMA"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

O_SCHEMA=${SCHEMA_DIR}/xxadeo_rpot_unite_contenance.schema
# Evaluate if the Ouput data Schema exists.
if [ ! -r  $O_SCHEMA ]; then
	e_msg="ERROR: Interface schema file does not exist or it is not readable. "
	e_msg+="Schema File: $O_SCHEMA"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

###############################################################################
#  Convert input data files in RETLSV format
###############################################################################

csv_converter "csv-retlsv" "${I_ATTR_FILE}" "${I_ATTR_FILE}.${PROGRAM_NAME}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_ITEMATTR_FILE}" "${I_ITEMATTR_FILE}.${PROGRAM_NAME}.retl"
check_error_and_clean_before_exit $?

###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
	
	<!--
		Import the Attributes source file with the link between Attributes and Items.
	-->
	<OPERATOR  type="import">
		<PROPERTY  name="inputfile" value="${I_ATTR_FILE}.${PROGRAM_NAME}.retl"/>
		<PROPERTY  name="schemafile" value="${I_ATTR_SCHEMA}"/>
		<OUTPUT name="i_attr.v"/>
	</OPERATOR>
	
	<!--
		Filter the Attributes source file to have only the Unité de Contenance CFA.
	-->
	<OPERATOR type="filter">
		<INPUT name="i_attr.v"/>
		<PROPERTY name="filter" value="KEY EQ '${ATTR_KEY}'"/>
		<PROPERTY name="rejects" value="false"/>
		<OUTPUT name="attr_unite_contenance.v"/>
	</OPERATOR>
	
	<!--
		Keep only the ATTR_ID field that represents the KEY used in the filter
		above.
	-->
	<OPERATOR type="fieldmod">
		<INPUT name="attr_unite_contenance.v" />
		<PROPERTY name="keep" value="ATTR_ID"/>
		<OUTPUT name="attr_unite_contenance_id.v"/>
	</OPERATOR>
	
	<!--
		Sort the Attributes, using 2 threads.
	-->
	<OPERATOR type="sort">
		<INPUT name="attr_unite_contenance_id.v"/>
		<PROPERTY name="key" value="ATTR_ID"/>
		<PROPERTY name="removedup" value="true"/>
		<PROPERTY name="numsort" value="${NO_OF_CPUS}"/>
		<OUTPUT name="attr_unite_contenance_id_sorted.v"/>
	</OPERATOR>
	
	<!--
		Import the ItemAttributes source file with the link between Attributes and Items.
	-->
	<OPERATOR  type="import">
		<PROPERTY  name="inputfile" value="${I_ITEMATTR_FILE}.${PROGRAM_NAME}.retl"/>
		<PROPERTY  name="schemafile" value="${I_ITEMATTR_SCHEMA}"/>
		<OUTPUT name="i_itemattr.v"/>
	</OPERATOR>
	
	<!--
		Sort the ItemAttributes source file, using 2 threads.
	-->
	<OPERATOR type="sort">
		<INPUT name="i_itemattr.v"/>
		<PROPERTY name="key" value="ATTR_ID ATTR_VALUE"/>
		<PROPERTY name="numsort" value="${NO_OF_CPUS}"/>
		<OUTPUT name="i_itemattr_sorted.v"/>
	</OPERATOR>
	
	<!--
		Join the Attributes with the ItemAttributes file, to remove from the last
		all the records that are not related with Unite de Contenance CFA.
	-->
	<OPERATOR type="innerjoin">
		<INPUT name="attr_unite_contenance_id_sorted.v"/>
		<INPUT name="i_itemattr_sorted.v"/>
		<PROPERTY name="key" value="ATTR_ID"/>
		<OUTPUT name="item_unite_contenance.v"/>
	</OPERATOR>
	
	<!--
		Keep only the required CFA information (ITEM_ID and CFA_VALUE).
	-->
	<OPERATOR type="fieldmod">
		<INPUT name="item_unite_contenance.v" />
		<PROPERTY name="keep" value="ITEM_ID ATTR_VALUE"/>
		<PROPERTY name="rename" value="UNITE_CONTENANCE=ATTR_VALUE"/>
		<OUTPUT name="item_unite_contenance_2.v"/>
	</OPERATOR>
	
	<!--
		Export this information to a file in a RETL format.
	-->
	<OPERATOR type="export">
		<INPUT name="item_unite_contenance_2.v"/>
		<PROPERTY name="schemafile" value="${O_SCHEMA}"/>
		<PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE}.${PROGRAM_NAME}.retl"/>
	</OPERATOR>
	
</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE}.${PROGRAM_NAME}.retl" "${RETL_OUTPUT_FILE}"
check_error_and_clean_before_exit $?

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${RETL_OUTPUT_FILE}

###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"
