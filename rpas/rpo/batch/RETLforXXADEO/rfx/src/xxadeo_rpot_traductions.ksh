#!/bin/ksh
###############################################################################
#  $Workfile:   xxadeo_rpot_traductions.ksh
#  $Modtime:    Mar 18 2018
#               Jun 18 2018: Rename the schema files
###############################################################################

###############################################################################
#  This script extract translation data for RPAS modules
#
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################
PROGRAM_NAME='xxadeo_rpot_traductions'

###############################################################################
#  INCLUDES
###############################################################################


. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh

DATE_FILENAME=`date +%Y%m%d-%H%M%S`


###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################

export ETC_DIR=${RETLforXXADEO}/rfx/etc

export DATA_DIR="${RETL_IN}/RPO"

export I_COMPHEAD_PATTERN="XXRMS216_COMPHEADTL_*.dat"
export I_DIVISION_PATTERN="XXRMS216_DIVISIONTL_*.dat"
export I_GROUPS_PATTERN="XXRMS216_GROUPSTL_*.dat"
export I_DEPS_PATTERN="XXRMS216_DEPSTL_*.dat"
export I_CLASS_PATTERN="XXRMS216_CLASSTL_*.dat"
export I_SUBCLASS_PATTERN="XXRMS216_SUBCLASSTL_*.dat"
export I_ITEMMASTER_PATTERN="XXRMS219_ITEMMASTERTL_*.dat"
export I_ATTR_TL_PATTERN="XXRMS220_ATTR_TL_*.dat"

export I_COMPHEAD_FILE=`find $DATA_DIR -name ${I_COMPHEAD_PATTERN} | sort | tail -n 1`
export I_DIVISION_FILE=`find $DATA_DIR -name ${I_DIVISION_PATTERN} | sort | tail -n 1`
export I_GROUPS_FILE=`find $DATA_DIR -name ${I_GROUPS_PATTERN} | sort | tail -n 1`
export I_DEPS_FILE=`find $DATA_DIR -name ${I_DEPS_PATTERN} | sort | tail -n 1`
export I_CLASS_FILE=`find $DATA_DIR -name ${I_CLASS_PATTERN} | sort | tail -n 1`
export I_SUBCLASS_FILE=`find $DATA_DIR -name ${I_SUBCLASS_PATTERN} | sort | tail -n 1`
export I_ITEMMASTER_FILE=`find $DATA_DIR -name ${I_ITEMMASTER_PATTERN} | sort | tail -n 1`
export I_ATTR_TL_FILE=`find $DATA_DIR -name ${I_ATTR_TL_PATTERN} | sort | tail -n 1`


export RETL_OUTPUT_FILE1=$DATA_DIR/r_itemlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE2=$DATA_DIR/r_iterlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE3=$DATA_DIR/r_clsslabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE4=$DATA_DIR/r_clsrlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE5=$DATA_DIR/r_scatlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE6=$DATA_DIR/r_scarlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE7=$DATA_DIR/r_catlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE8=$DATA_DIR/r_catrlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE9=$DATA_DIR/r_deptlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE10=$DATA_DIR/r_deprlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE11=$DATA_DIR/r_chnlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE12=$DATA_DIR/r_chnrlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE13=$DATA_DIR/r_brndlabel.csv.ovr_${DATE_FILENAME}.dat
export RETL_OUTPUT_FILE14=$DATA_DIR/r_brnrlabel.csv.ovr_${DATE_FILENAME}.dat

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${I_ITEMMASTER_FILE}.retl"
                rm -f "${I_ATTR_TL_FILE}.retl"
                rm -f "${I_COMPHEAD_FILE}.retl"
                rm -f "${I_DIVISION_FILE}.retl"
                rm -f "${I_GROUPS_FILE}.retl"
                rm -f "${I_DEPS_FILE}.retl"
                rm -f "${I_CLASS_FILE}.retl"
                rm -f "${I_SUBCLASS_FILE}.retl"

                rm -f "${RETL_OUTPUT_FILE1}.retl"
                rm -f "${RETL_OUTPUT_FILE1}"
                rm -f "${RETL_OUTPUT_FILE2}.retl"
                rm -f "${RETL_OUTPUT_FILE2}"
                rm -f "${RETL_OUTPUT_FILE3}.retl"
                rm -f "${RETL_OUTPUT_FILE3}"
                rm -f "${RETL_OUTPUT_FILE4}.retl"
                rm -f "${RETL_OUTPUT_FILE4}"
                rm -f "${RETL_OUTPUT_FILE5}.retl"
                rm -f "${RETL_OUTPUT_FILE5}"
                rm -f "${RETL_OUTPUT_FILE6}.retl"
                rm -f "${RETL_OUTPUT_FILE6}"
                rm -f "${RETL_OUTPUT_FILE7}.retl"
                rm -f "${RETL_OUTPUT_FILE7}"
                rm -f "${RETL_OUTPUT_FILE8}.retl"
                rm -f "${RETL_OUTPUT_FILE8}"
                rm -f "${RETL_OUTPUT_FILE9}.retl"
                rm -f "${RETL_OUTPUT_FILE9}"
                rm -f "${RETL_OUTPUT_FILE10}.retl"
                rm -f "${RETL_OUTPUT_FILE10}"
                rm -f "${RETL_OUTPUT_FILE11}.retl"
                rm -f "${RETL_OUTPUT_FILE11}"
                rm -f "${RETL_OUTPUT_FILE12}.retl"
                rm -f "${RETL_OUTPUT_FILE12}"
                rm -f "${RETL_OUTPUT_FILE13}.retl"
                rm -f "${RETL_OUTPUT_FILE13}"
                rm -f "${RETL_OUTPUT_FILE14}.retl"
                rm -f "${RETL_OUTPUT_FILE14}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${I_ITEMMASTER_FILE}.retl"
                rm -f "${I_ATTR_TL_FILE}.retl"
                rm -f "${I_ATTR_TL_FILE}.retl"
                rm -f "${I_COMPHEAD_FILE}.retl"
                rm -f "${I_DIVISION_FILE}.retl"
                rm -f "${I_GROUPS_FILE}.retl"
                rm -f "${I_DEPS_FILE}.retl"
                rm -f "${I_CLASS_FILE}.retl"
                rm -f "${I_SUBCLASS_FILE}.retl"

                rm -f "${RETL_OUTPUT_FILE1}.retl"
                rm -f "${RETL_OUTPUT_FILE2}.retl"
                rm -f "${RETL_OUTPUT_FILE3}.retl"
                rm -f "${RETL_OUTPUT_FILE4}.retl"
                rm -f "${RETL_OUTPUT_FILE5}.retl"
                rm -f "${RETL_OUTPUT_FILE6}.retl"
                rm -f "${RETL_OUTPUT_FILE7}.retl"
                rm -f "${RETL_OUTPUT_FILE8}.retl"
                rm -f "${RETL_OUTPUT_FILE9}.retl"
                rm -f "${RETL_OUTPUT_FILE10}.retl"
                rm -f "${RETL_OUTPUT_FILE11}.retl"
                rm -f "${RETL_OUTPUT_FILE12}.retl"
                rm -f "${RETL_OUTPUT_FILE13}.retl"
                rm -f "${RETL_OUTPUT_FILE14}.retl"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}

###############################################################################
#  Check if input files are available. If one file is missing the script
#  ends in error
###############################################################################


if [[ ! -f $I_COMPHEAD_FILE ]];  then
    message "$I_COMPHEAD_PATTERN does not exist"
    exit 1
fi

if [[  ! -f $I_DIVISION_FILE ]];  then
    message "$I_DIVISION_PATTERN does not exist"
    exit 1
fi

if [[  ! -f $I_GROUPS_FILE ]];  then
    message "$I_GROUPS_PATTERN does not exist"
    exit 1
fi
if [[ ! -f $I_DEPS_FILE ]];  then
    message "$I_DEPS_PATTERN does not exist"
    exit 1
fi

if [[  ! -f $I_CLASS_FILE ]];  then
    message "$I_CLASS_PATTERN does not exist"
    exit 1
fi

if [[  ! -f $I_SUBCLASS_FILE ]];  then
    message "$I_SUBCLASS_PATTERN does not exist"
    exit 1
fi

if [[ ! -f $I_ITEMMASTER_FILE ]];  then
    message "$I_ITEMMASTER_PATTERN does not exist"
    exit 1
fi

if [[  ! -f $I_ATTR_TL_FILE ]];  then
    message "$I_ATTR_TL_PATTERN does not exist"
    exit 1
fi


###############################################################################
#  RETL input/output schemas
###############################################################################

I_COMPHEAD_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_comphead.schema
I_DIVISION_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_division.schema
I_GROUPS_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_groups.schema
I_DEPS_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_deps.schema
I_CLASS_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_class.schema
I_SUBCLASS_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhiertl_subclass.schema
I_ITEMMASTER_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_itemtranslation.schema
I_ATTR_TL_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_attr_tl.schema

O_SCHEMA=${SCHEMA_DIR}/xxadeo_rpot_traductions.schema

###############################################################################
#  Convert input data files in RETLSV format
###############################################################################

csv_converter "csv-retlsv" "${I_ITEMMASTER_FILE}" "${I_ITEMMASTER_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_ATTR_TL_FILE}" "${I_ATTR_TL_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_COMPHEAD_FILE}" "${I_COMPHEAD_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_DIVISION_FILE}" "${I_DIVISION_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_GROUPS_FILE}" "${I_GROUPS_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_DEPS_FILE}" "${I_DEPS_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_CLASS_FILE}" "${I_CLASS_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_SUBCLASS_FILE}" "${I_SUBCLASS_FILE}.retl"
check_error_and_clean_before_exit $?


###############################################################################
# Interface Configuration file
###############################################################################

CONFIG_FILE=${ETC_DIR}/${PROGRAM_NAME}.txt
# Evaluate if the interface config file do not exist
if [ ! -r  ${CONFIG_FILE} ]; then
       message "Config file ${CONFIG_FILE} missing!!"
       exit 1
fi

# Get BRAND UDA ID
ATTR_KEY_CONFIG_LINE=$(cat ${CONFIG_FILE} | grep -v "^[[:blank:]]*#" | grep "BRAND_ATTR_KEY")
ATTR_KEY=$(echo ${ATTR_KEY_CONFIG_LINE} | cut -d"|" -f3 | tr -d '[[:blank:]]')

# Evaluate if the UDA Key was retrieved successfully
if [ -z ${ATTR_KEY} ]; then
       message "ERROR: Interface config file not correctly set. Please check config file ${CONFIG_FILE}"
       exit 1
fi

###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">


<OPERATOR  type="import">
        <PROPERTY  name="inputfile" value="${I_COMPHEAD_FILE}.retl"/>
        <PROPERTY  name="schemafile" value="${I_COMPHEAD_SCHEMA}"/>
        <OUTPUT name="i_comphead.v"/>
</OPERATOR>

<OPERATOR  type="import">
        <PROPERTY  name="inputfile" value="${I_GROUPS_FILE}.retl"/>
        <PROPERTY  name="schemafile" value="${I_GROUPS_SCHEMA}"/>
        <OUTPUT name="i_groups.v"/>
</OPERATOR>

<OPERATOR  type="import">
        <PROPERTY  name="inputfile" value="${I_DEPS_FILE}.retl"/>
        <PROPERTY  name="schemafile" value="${I_DEPS_SCHEMA}"/>
        <OUTPUT name="i_deps.v"/>
</OPERATOR>

<OPERATOR  type="import">
        <PROPERTY  name="inputfile" value="${I_CLASS_FILE}.retl"/>
        <PROPERTY  name="schemafile" value="${I_CLASS_SCHEMA}"/>
        <OUTPUT name="i_class.v"/>
</OPERATOR>

<OPERATOR  type="import">
        <PROPERTY  name="inputfile" value="${I_SUBCLASS_FILE}.retl"/>
        <PROPERTY  name="schemafile" value="${I_SUBCLASS_SCHEMA}"/>
        <OUTPUT name="i_subclass.v"/>
</OPERATOR>

<OPERATOR  type="import">
        <PROPERTY  name="inputfile" value="${I_ITEMMASTER_FILE}.retl"/>
        <PROPERTY  name="schemafile" value="${I_ITEMMASTER_SCHEMA}"/>
        <OUTPUT name="i_itemmaster.v"/>
</OPERATOR>

<OPERATOR  type="import">
        <PROPERTY  name="inputfile" value="${I_ATTR_TL_FILE}.retl"/>
        <PROPERTY  name="schemafile" value="${I_ATTR_TL_SCHEMA}"/>
        <OUTPUT name="i_attr_tl.v"/>
</OPERATOR>

<OPERATOR type="fieldmod">
        <INPUT name="i_itemmaster.v" />
        <PROPERTY name="keep" value="ITEM LANG_NAME ITEM_DESC"/>
        <PROPERTY name="rename" value="ID=ITEM"/>
        <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
        <PROPERTY name="rename" value="LABEL_TRANSLATED=ITEM_DESC"/>
        <OUTPUT name="i_itemmaster_2.v"/>
</OPERATOR>

<OPERATOR type="copy">
        <INPUT name="i_itemmaster_2.v"/>
        <OUTPUT name="r_itemlabel.v"/>
        <OUTPUT name="r_iterlabel.v"/>
</OPERATOR>

<OPERATOR type="fieldmod">
        <INPUT name="i_subclass.v" />
        <PROPERTY name="keep" value="RPAS_SUBCLASS_ID LANG_NAME RPAS_SUBCLASS_NAME"/>
        <PROPERTY name="rename" value="ID=RPAS_SUBCLASS_ID"/>
        <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
        <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_SUBCLASS_NAME"/>
        <OUTPUT name="i_subclass_2.v"/>
</OPERATOR>

<OPERATOR type="copy">
        <INPUT name="i_subclass_2.v"/>
        <OUTPUT name="r_clsslabel.v"/>
        <OUTPUT name="r_clsrlabel.v"/>
</OPERATOR>


<OPERATOR type="fieldmod">
        <INPUT name="i_class.v" />
        <PROPERTY name="keep" value="RPAS_CLASS_ID LANG_NAME RPAS_CLASS_NAME"/>
        <PROPERTY name="rename" value="ID=RPAS_CLASS_ID"/>
        <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
        <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_CLASS_NAME"/>
        <OUTPUT name="i_class_2.v"/>
</OPERATOR>

<OPERATOR type="copy">
        <INPUT name="i_class_2.v"/>
        <OUTPUT name="r_scatlabel.v"/>
        <OUTPUT name="r_scarlabel.v"/>
</OPERATOR>



<OPERATOR type="fieldmod">
        <INPUT name="i_deps.v" />
        <PROPERTY name="keep" value="RPAS_DEPT_ID LANG_NAME RPAS_DEPT_NAME"/>
        <PROPERTY name="rename" value="ID=RPAS_DEPT_ID"/>
        <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
        <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_DEPT_NAME"/>
        <OUTPUT name="i_deps_2.v"/>
</OPERATOR>

<OPERATOR type="copy">
        <INPUT name="i_deps_2.v"/>
        <OUTPUT name="r_catlabel.v"/>
        <OUTPUT name="r_catrlabel.v"/>
</OPERATOR>


<OPERATOR type="fieldmod">
        <INPUT name="i_groups.v" />
        <PROPERTY name="keep" value="RPAS_GROUP_NO LANG_NAME RPAS_GROUP_NAME"/>
        <PROPERTY name="rename" value="ID=RPAS_GROUP_NO"/>
        <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
        <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_GROUP_NAME"/>
        <OUTPUT name="i_groups_2.v"/>
</OPERATOR>

<OPERATOR type="copy">
        <INPUT name="i_groups_2.v"/>
        <OUTPUT name="r_deptlabel.v"/>
        <OUTPUT name="r_deprlabel.v"/>
</OPERATOR>


<OPERATOR type="fieldmod">
        <INPUT name="i_comphead.v" />
        <PROPERTY name="keep" value="RPAS_COMPANY_ID LANG_NAME RPAS_COMPANY_NAME"/>
        <PROPERTY name="rename" value="ID=RPAS_COMPANY_ID"/>
        <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
        <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_COMPANY_NAME"/>
        <OUTPUT name="i_comphead_2.v"/>
</OPERATOR>

<OPERATOR type="copy">
        <INPUT name="i_comphead_2.v"/>
        <OUTPUT name="r_chnlabel.v"/>
        <OUTPUT name="r_chnrlabel.v"/>
</OPERATOR>


<OPERATOR type="filter">
        <INPUT name="i_attr_tl.v"/>
        <PROPERTY name="filter" value="KEY EQ '${ATTR_KEY}'"/>
        <OUTPUT name="i_attr_tl_2.v"/>
</OPERATOR>


<OPERATOR type="fieldmod">
        <INPUT name="i_attr_tl_2.v" />
        <PROPERTY name="keep" value="ATTR_VALUE LANG_NAME ATTR_VALUE_DESC"/>
        <PROPERTY name="rename" value="ID=ATTR_VALUE"/>
        <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
        <PROPERTY name="rename" value="LABEL_TRANSLATED=ATTR_VALUE_DESC"/>
        <OUTPUT name="i_attr_tl_3.v"/>
</OPERATOR>

<OPERATOR type="copy">
        <INPUT name="i_attr_tl_3.v"/>
        <OUTPUT name="r_brndlabel.v"/>
        <OUTPUT name="r_brnrlabel.v"/>
</OPERATOR>

<!-- r_itemlabel -->
                 <OPERATOR type="convert">
                        <INPUT    name="r_itemlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_itemlabel2.v"/>
                </OPERATOR>
               <OPERATOR type="export">
                       <INPUT name="r_itemlabel2.v" />
                       <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                       <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE1}.retl"/>
               </OPERATOR>

<!-- r_iterlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_iterlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_iterlabel2.v"/>
                </OPERATOR>
               <OPERATOR type="export">
                       <INPUT name="r_iterlabel2.v"/>
                       <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                       <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE2}.retl"/>
               </OPERATOR>

<!-- r_clsslabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_clsslabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_clsslabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_clsslabel2.v" />
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE3}.retl"/>
                </OPERATOR>


<!-- r_clsrlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_clsrlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_clsrlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_clsrlabel2.v"/>
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE4}.retl"/>
                </OPERATOR>

<!-- r_scatlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_scatlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_scatlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_scatlabel2.v" />
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE5}.retl"/>
                </OPERATOR>

<!-- r_scarlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_scarlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_scarlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_scarlabel2.v"/>
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE6}.retl"/>
                </OPERATOR>

<!-- r_catlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_catlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_catlabel2.v"/>
                </OPERATOR>
                 <OPERATOR type="export">
                         <INPUT name="r_catlabel2.v" />
                         <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                         <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE7}.retl"/>
                 </OPERATOR>

<!-- r_catrabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_catrlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_catrlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_catrlabel2.v"/>
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE8}.retl"/>
                </OPERATOR>

<!-- r_deptlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_deptlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_deptlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_deptlabel2.v" />
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE9}.retl"/>
                </OPERATOR>

<!-- r_deprlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_deprlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_deprlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_deprlabel2.v"/>
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE10}.retl"/>
                </OPERATOR>

<!-- r_chnlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_chnlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_chnlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_chnlabel2.v" />
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE11}.retl"/>
                </OPERATOR>

<!-- r_chnrlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_chnrlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_chnrlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_chnrlabel2.v"/>
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE12}.retl"/>
                </OPERATOR>

<!-- r_brndlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_brndlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_brndlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_brndlabel2.v" />
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE13}.retl"/>
                </OPERATOR>

<!-- r_brnrlabel -->
                <OPERATOR type="convert">
                        <INPUT    name="r_brnrlabel.v"/>
                        <PROPERTY name="convertspec">
                        <![CDATA[
                        <CONVERTSPECS>
                <CONVERT destfield="ID" sourcefield="ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                                <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                </CONVERTFUNCTION>
                </CONVERT>
                        </CONVERTSPECS>
                        ]]>
                        </PROPERTY>
                        <OUTPUT name="r_brnrlabel2.v"/>
                </OPERATOR>
                <OPERATOR type="export">
                        <INPUT name="r_brnrlabel2.v"/>
                        <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                        <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE14}.retl"/>
                </OPERATOR>
</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE1}.retl" "${RETL_OUTPUT_FILE1}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE2}.retl" "${RETL_OUTPUT_FILE2}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE3}.retl" "${RETL_OUTPUT_FILE3}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE4}.retl" "${RETL_OUTPUT_FILE4}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE5}.retl" "${RETL_OUTPUT_FILE5}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE6}.retl" "${RETL_OUTPUT_FILE6}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE7}.retl" "${RETL_OUTPUT_FILE7}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE8}.retl" "${RETL_OUTPUT_FILE8}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE9}.retl" "${RETL_OUTPUT_FILE9}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE10}.retl" "${RETL_OUTPUT_FILE10}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE11}.retl" "${RETL_OUTPUT_FILE11}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE12}.retl" "${RETL_OUTPUT_FILE12}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE13}.retl" "${RETL_OUTPUT_FILE13}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE14}.retl" "${RETL_OUTPUT_FILE14}"
check_error_and_clean_before_exit $?

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${RETL_OUTPUT_FILE1}

log_num_recs ${RETL_OUTPUT_FILE2}

log_num_recs ${RETL_OUTPUT_FILE3}

log_num_recs ${RETL_OUTPUT_FILE4}

log_num_recs ${RETL_OUTPUT_FILE5}

log_num_recs ${RETL_OUTPUT_FILE6}

log_num_recs ${RETL_OUTPUT_FILE7}

log_num_recs ${RETL_OUTPUT_FILE8}

log_num_recs ${RETL_OUTPUT_FILE9}

log_num_recs ${RETL_OUTPUT_FILE10}

log_num_recs ${RETL_OUTPUT_FILE11}

log_num_recs ${RETL_OUTPUT_FILE12}

log_num_recs ${RETL_OUTPUT_FILE13}

log_num_recs ${RETL_OUTPUT_FILE14}


###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"
