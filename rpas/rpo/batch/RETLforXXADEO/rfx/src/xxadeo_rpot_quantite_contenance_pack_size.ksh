#!/bin/ksh
###############################################################################
#  $Workfile:   xxadeo_rpot_quantite_contenance_pack_size.ksh
#  $Modtime:    Mar 18 2018
###############################################################################

###############################################################################
#  This script builds 2 data file
#
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################


export PROGRAM_NAME="xxadeo_rpot_quantite_contenance_pack_size"
date=`date +"%G%m%d-%H%M%S"`


###############################################################################
#  INCLUDES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh

###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################

# ETC directory
export ETC_DIR=${RETLforXXADEO}/rfx/etc

export I_PACKITEM_PATTERN="XXRMS221_ITEMPACK_*.dat"
export I_ITEMATTR_PATTERN="XXRMS209_ITEMATTR_*.dat"
export I_ATTR_PATTERN="XXRMS209_ATTR_*.dat"

export I_DATA_DIR=${RETL_IN}/RPO
export I_PACKITEM_FILE=`find $I_DATA_DIR -name ${I_PACKITEM_PATTERN} | sort | tail -n 1`
export I_ITEMATTR_FILE=`find $I_DATA_DIR -name ${I_ITEMATTR_PATTERN} | sort | tail -n 1`
export I_ATTR_FILE=`find $I_DATA_DIR -name ${I_ATTR_PATTERN} | sort | tail -n 1`

export O_DATA_DIR=${RETL_IN}/RPO
export RETL_OUTPUT_FILE1=$O_DATA_DIR/ol1ituom.csv.rpl_${date}
export RETL_OUTPUT_FILE2=$O_DATA_DIR/ol1itequiu.csv.rpl_${date}
export RETL_OUTPUT_FILE3=$O_DATA_DIR/ol1ituom_doublon.csv.rpl

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
               rm -f "${I_ATTR_FILE}.${PROGRAM_NAME}.retl"
               rm -f "${I_ITEMATTR_FILE}.${PROGRAM_NAME}.retl"
               checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
               rm -f "${I_ATTR_FILE}.${PROGRAM_NAME}.retl"
               rm -f "${I_ITEMATTR_FILE}.${PROGRAM_NAME}.retl"
               checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
               rmse_terminate $1
        fi
}

###############################################################################
#  Get latest available input files, because since this interface works in a
#  FULL mode there is no need to process all available input files.
###############################################################################


if [[ ! -f $I_ITEMATTR_FILE ]];  then
    message "XXRMS209_ITEMATTR_YYYYMMDD-HHMMSS.dat does not exist"
    exit 1
fi

if [[  ! -f $I_ATTR_FILE ]];  then
    message "XXRMS209_ATTR_YYYYMMDD-HHMMSS.dat does not exist"
    exit 1
fi

if [[  ! -f $I_PACKITEM_FILE ]];  then
    message "XXRMS221_ITEMPACK_YYYYMMDD-HHMMSS.dat does not exist"
    exit 1
fi

###############################################################################
#  RETL input/output schemas
###############################################################################


export I_ATTR_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_attr.schema
export I_ITEMATTR_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_itemattr.schema
export I_PACKITEM_SCHEMA=${SCHEMA_DIR}/xxadeo_rmse_rpas_itempack.schema
export O_SCHEMA1=${SCHEMA_DIR}/xxadeo_rpot_ol1ituom.schema
export O_SCHEMA2=${SCHEMA_DIR}/xxadeo_rpot_ol1itequiu.schema


###############################################################################
# Interface Configuration file
###############################################################################

CONFIG_FILE=${ETC_DIR}/${PROGRAM_NAME}.txt
# Evaluate if the interface config file do not exist
if [ ! -r  ${CONFIG_FILE} ]; then
       message "Config file ${CONFIG_FILE} missing!!"
       exit 1
fi

# Get CAPACITY QUANTITY CFA ID
ATTR_KEY_CONFIG_LINE=$(cat ${CONFIG_FILE} | grep -v "^[[:blank:]]*#" | grep "QUANTITY_CAPACITY_KEY")
ATTR_KEY=$(echo ${ATTR_KEY_CONFIG_LINE} | cut -d"|" -f3 | tr -d '[[:blank:]]')
CONTENANCE_ID=`grep -i ${ATTR_KEY} ${I_ATTR_FILE} | cut -d ',' -f2 | sort -u`

# Evaluate if the UDA Key was retrieved successfully
if [ -z ${ATTR_KEY} ]; then
       message "ERROR: Interface config file not correctly set. Please check config file ${CONFIG_FILE}"
       exit 1
fi


###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Convert input data files in RETLSV format
###############################################################################

csv_converter "csv-retlsv" "${I_ATTR_FILE}" "${I_ATTR_FILE}.${PROGRAM_NAME}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_ITEMATTR_FILE}" "${I_ITEMATTR_FILE}.${PROGRAM_NAME}.retl"
check_error_and_clean_before_exit $?

###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
<OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_PACKITEM_FILE}"/>
                <PROPERTY  name="schemafile" value="${I_PACKITEM_SCHEMA}"/>
                <OUTPUT name="i_packitem.v"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMATTR_FILE}.${PROGRAM_NAME}.retl"/>
                <PROPERTY  name="schemafile" value="${I_ITEMATTR_SCHEMA}"/>
                <OUTPUT name="i_itemudas.v"/>
        </OPERATOR>

<!-- Filter ATTR and ITEM_ATTER files -->

                <OPERATOR  type="filter">
                <INPUT name="i_itemudas.v"/>
                <PROPERTY name="filter" value="ATTR_ID EQ '${CONTENANCE_ID}'"/>
                <OUTPUT name="i_itemudas_filter.v"/>
        </OPERATOR>

<!-- FILE: ol1itequiu.csv.ovr -->

        <OPERATOR type="fieldmod">
                <INPUT name="i_itemudas_filter.v" />
                <PROPERTY name="keep" value="ITEM_ID ATTR_VALUE"/>
                <PROPERTY name="rename" value="ITEM_EUOM=ATTR_VALUE"/>
                <OUTPUT name="ol1itequiu.v"/>
        </OPERATOR>


        <OPERATOR type="sort">
                <INPUT name="ol1itequiu.v"/>
                <PROPERTY name="key" value="ITEM_ID"/>
                <PROPERTY name="numsort" value="2"/>
                <OUTPUT name="sorted_ol1itequiu.v"/>
        </OPERATOR>

<!-- FILE: ol1ituom.csv.ovr -->
        <OPERATOR type="fieldmod">
                <INPUT name="i_packitem.v" />
                <PROPERTY name="keep" value="PACK_NO PACK_ITEM_QTY"/>
                <PROPERTY name="rename" value="ITEM_ID=PACK_NO"/>
                <PROPERTY name="rename" value="ITEM_UOM=PACK_ITEM_QTY"/>
                <OUTPUT name="ol1ituom_doublon.v"/>
        </OPERATOR>


        <OPERATOR type="sort">
                <INPUT name="ol1ituom_doublon.v"/>
                <PROPERTY name="key" value="ITEM_ID"/>
                <PROPERTY name="numsort" value="2"/>
                <OUTPUT name="sorted_ol1ituom_doublon.v"/>
        </OPERATOR>


<!-- EXPORT to output file -->
<OPERATOR type="convert">
        <INPUT name="sorted_ol1itequiu.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="ITEM_EUOM" sourcefield="ITEM_EUOM">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="sorted_ol1itequiu_2.v"/>
</OPERATOR>


        <OPERATOR type="export">
                <INPUT name="sorted_ol1itequiu_2.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA2}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE2}"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="sorted_ol1ituom_doublon.v"/>
                <PROPERTY name="schemafile" value="${O_SCHEMA1}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE3}"/>
        </OPERATOR>



</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?


###############################################################################
#  simple pack and complex pack
###############################################################################

#${RETL_OUTPUT_FILE1}  File without duplicates

#${RETL_OUTPUT_FILE3} Temp file with duplicates

item_id=""
item_id_new=""
export item_id_old=""
item_uom=""
 item_uom_new=""
export item_uom_old=""



cat "${RETL_OUTPUT_FILE3}" |
while IFS=, read item_id item_uom
do
        item_id_new=$item_id
        item_uom_new=$item_uom
        if [[ $item_id_new == $item_id_old ]]; then
                if [ $item_uom_old != "" ]; then
                        item_uom_old=999
                fi
        else
                if [[ $item_id_old != "" ]]; then
                        echo "$item_id_old,$item_uom_old" >> "${RETL_OUTPUT_FILE1}"
                fi
                item_id_old=$item_id_new
                item_uom_old=$item_uom_new

        fi
done
echo "$item_id_old,$item_uom_old" >> "${RETL_OUTPUT_FILE1}"



###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${RETL_OUTPUT_FILE1}

log_num_recs ${RETL_OUTPUT_FILE2}


###############################################################################
#  cleanup and exit:
###############################################################################

rm ${RETL_OUTPUT_FILE3}

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"
