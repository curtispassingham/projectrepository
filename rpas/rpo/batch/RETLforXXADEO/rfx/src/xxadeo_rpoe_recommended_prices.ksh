#!/bin/ksh
###############################################################################
#  $Workfile:   xxadeo_rpoe_recommended_prices.ksh
#  $Modtime:    Mar 14 2018
###############################################################################

###############################################################################
#  
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################

PROGRAM_NAME='xxadeo_rpoe_recommended_prices'

TIMESTAMP=$(date "+%Y%m%d-%H%M%S")

###############################################################################
#  INCLUDES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh

###############################################################################
#  FUNCTIONS
###############################################################################


##############################################################################
#
#  The log_message function allows logging a message with special characters
#
#     Usage: log_message $1 $2
#
#            $1
#                Type of error, like INFORMATION, ERROR, ...", which will be
#                displayed before the message in argument 2.
#            $2
#                Message with special characters to be logged.
#
##############################################################################
function log_message {
	if [[ $# -ne 2 ]]; then
		e_msg="ERROR: log_message: This function must have exactly 2 arguments,"
		e_msg+=" but it has $#."
		message "$e_msg"
		rmse_terminate 1
	else
		e_msg=$(echo -e "${1}"":""${2}")
		message "$e_msg"
	fi
}

##############################################################################
#
#  The check_error_and_clean_before_exit does a cleanup of temporary files created
#  during the execution of this script, before exiting the script.
#  It can cleanup the files in case of return code error, or simply clean them 
#  and exist, no matter the return code.
#
#     Usage: check_error_and_clean_before_exit  $1 [$2]
#
#            $1
#                Return Code
#            $2 (OPTIONAL)
#                CHECK (DEFAULT) - Check the return code and only cleanup and
#                        exist if return code is not Zero.
#                NO_CHECK - Does the cleanup and exist no matter the return code.
#
##############################################################################
function check_error_and_clean_before_exit {
	error_check=1
	if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]] ; then
		message "ERROR: Parameter 2 is not valid: ${2}"
		message "    Parameter 2 must be true, false or empty (true)"
		rmse_terminate 1
	elif [[ "${2}" = "NO_CHECK" ]] ; then
		error_check=0
	fi
	
	if [[ $1 -ne 0 && error_check -eq 1 ]] ; then
		rm -f "${O_PC_FILE}.source.new" \
			"${O_PC_FILE}.source.mod" \
			"${O_PC_FILE}.source.del" \
			"${O_PC_FILE}.source.new.retl" \
			"${O_PC_FILE}.source.mod.retl" \
			"${O_PC_FILE}.source.del.retl" \
			"${O_PC_FILE}.retl" \
			"${O_PC_FILE}" \
			"${TMP_FILE_PATH}"
		
		checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
		
	elif [[ error_check -eq 0 ]] ; then
		rm -f "${O_PC_FILE}.source.new" \
			"${O_PC_FILE}.source.mod" \
			"${O_PC_FILE}.source.del" \
			"${O_PC_FILE}.source.new.retl" \
			"${O_PC_FILE}.source.mod.retl" \
			"${O_PC_FILE}.source.del.retl" \
			"${O_PC_FILE}.retl" \
			"${TMP_FILE_PATH}"
		
		checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
		rmse_terminate $1
	fi
}

##############################################################################
#
#  The f_registerMeasure registers measure in the domain if it does not exist.
#  The Measure property Base Intx should have been collected from the source measure, using the function f_getMeasureProperties.
#
#     Usage: f_registerMeasure  $1 $2 $3 $4
#
#            $1
#                New Measure Name to register
#            $2
#                Measure Type (real or boolean only)
#            $3
#                Base Intersection
#            $4
#                Measure Label
#
##############################################################################
function f_registerMeasure
{
	# Check good call to the function.
	if [[ $# -ne 4 ]] ; then
		e_msg="f_registerMeasure: Invalid number of arguments.\n"
		e_msg+="\tExpected 4 and found $#.\n"
		log_message ERROR "$e_msg"
		check_error_and_clean_before_exit 1
	elif [[ "$2" != "real" && "$2" != "boolean" && "$2" != "date" ]] ; then
		e_msg="f_registerMeasure: Invalid parameter #2.\n"
		e_msg+="\tExpected either \"real\" or \"boolean\" and found $2.\n"
		log_message ERROR "$e_msg"
		check_error_and_clean_before_exit 1
	fi
	
	# Check if the measure being created already exists and if it does do nothing.
	printMeasure -d $DOMAIN_PATH -m $1 -specs > /dev/null 2>&1
	result=$?
	# Execute when measure being created do not exist in the domain yet.
	if [[ $result -ne 0 ]] ; then
		# Check the type of the measure being created to decide the Default Aggregation
		# of the measure being created. If real is total, if boolean is or.
		if [[ "$2" = "real" ]] ; then
			DEF_AGG="total"
		elif [[ "$2" = "boolean" ]] ; then
			DEF_AGG="or"
		else
			DEF_AGG="max"
		fi
		
		# Register the measure accordingly.
		regmeasure -d "$DOMAIN_PATH" -add "$1" -type "$2" -baseint "$3" \
			-label "$4" -db "data/nonbase" -defagg "$DEF_AGG" -defspread repl \
			-insertable false -basestate read -aggstate read \
			-description "\""
		check_error_and_clean_before_exit $?
	fi
}

##############################################################################
#
#  The f_executeDomainExpression executes a expression in the Domain
#
#     Usage: f_executeDomainExpression  $1
#
#            $1
#                Expression to execute
#
##############################################################################
function f_executeDomainExpression
{
	# Check good call to the function.
	if [[ $# -ne 1 ]] ; then
		e_msg="f_executeDomainExpression: Invalid number of arguments.\n"
		e_msg+="\tExpected 1 and found $#.\n"
		log_message ERROR "$e_msg"
		check_error_and_clean_before_exit 1
	fi
	
	mace -d $DOMAIN_PATH -processes $NO_OF_CPUS -run -expression "$1"
	check_error_and_clean_before_exit $?
}

###############################################################################
#  DEFINES
###############################################################################

# Setting up RPAS_TODAY 
export RPAS_TODAY=$VDATE
log_message INFORMATION "Virtual Date is: $RPAS_TODAY"

# Define DOMAIN PATH and check that it contains a valid domain, otherwise exit
# and log error.
DOMAIN_PATH=$RPO_HOME/domain/RegPrice
domaininfo -d $DOMAIN_PATH -domainversion > /dev/null 2>&1 ; result=$?
if [[ $result -ne 0 ]] ; then
	e_msg="Invalid domain provided in the path: $DOMAIN_PATH"
	log_message ERROR "$e_msg"
	check_error_and_clean_before_exit $result
else
	log_message INFORMATION "RPO Domain Path is: $DOMAIN_PATH"
fi

###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################

# Store locally the path of the inbound data directory
O_DATA_DIR=$RETL_OUT/MOM
# Evaluate if the inbound data directory do not exist
if [ ! -r  $O_DATA_DIR ]; then
	e_msg="Outbound Data Dir does not exist or it is not readable.\n"
	e_msg+="\tOUTBOUND_DATA_DIR: $O_DATA_DIR\n"
	log_message ERROR "$e_msg"
	check_error_and_clean_before_exit 1
else
	log_message INFORMATION "Output Data Directory is: $O_DATA_DIR"
fi

# Path for the output file.
O_PC_FILE=$O_DATA_DIR/XXRPM203_RPOPriceChanges_$TIMESTAMP.csv
# Evaluate if the ouput data files are writable
touch $O_PC_FILE ; check_error_and_clean_before_exit $?

###############################################################################
#  RETL input/output schemas
###############################################################################

I_NEWMOD_PC_SCHEMA=${SCHEMA_DIR}/xxadeo_rpo_rpm_recommeded_price_changes_new_mod_input.schema
# Evaluate if the New/Mod Recommended Price Schema exists.
if [ ! -r  $I_NEWMOD_PC_SCHEMA ]; then
	e_msg="Interface schema file does not exist or it is not readable.\n"
	e_msg+="\tSchema File: $I_NEWMOD_PC_SCHEMA\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

I_DEL_PC_SCHEMA=${SCHEMA_DIR}/xxadeo_rpo_rpm_recommeded_price_changes_del_input.schema
# Evaluate if the Organizational Hierarchy Schema exists.
if [ ! -r  $I_DEL_PC_SCHEMA ]; then
	e_msg="Interface schema file does not exist or it is not readable.\n"
	e_msg+="\tSchema File: $I_DEL_PC_SCHEMA\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

O_PC_SCHEMA=${SCHEMA_DIR}/xxadeo_rpo_rpm_recommeded_price_changes_output.schema
# Evaluate if the New/Mod Recommended Price Schema exists.
if [ ! -r  $O_PC_SCHEMA ]; then
	e_msg="Interface schema file does not exist or it is not readable.\n"
	e_msg+="\tSchema File: $O_PC_SCHEMA\n"
	log_message ERROR "$e_msg" ; check_error_and_clean_before_exit 1
fi

###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Register in the domain the required measures to execute the export logic
###############################################################################

f_registerMeasure "NbCurFutWksMsk"     "boolean" "week"         "Current & Future Weeks Mask"
f_registerMeasure "NbPstWksMsk"        "boolean" "week"         "Past Weeks Mask"
f_registerMeasure "NbCurWkMsk"         "boolean" "week"         "Current Week Mask"
f_registerMeasure "NbLstExpAppPcChg"   "real"    "prznitemweek" "Last Exported Approved Price Changes"
f_registerMeasure "NbLstExpAppPcChgED" "date"    "prznitemweek" "Last Exported Approved Price Changes Effective Date"
f_registerMeasure "NbFAppItPcLag"      "real"    "prznitemweek" "Approved Item Price LAG"
f_registerMeasure "NbPcToEvaluate"     "boolean" "prznitemweek" "Price Changes to Evaluate"

f_registerMeasure "NbRPMChkPst"        "real"    "prznitemweek" "Check RPM Emergency change for Past Price Changes"
f_registerMeasure "NbRPMChkFut"        "real"    "prznitemweek" "Check RPM Emergency change for Future Price Changes"
f_registerMeasure "NbRPMChkPstInd"     "real"    "prznitem"     "Check RPM Emergency change for Last Past Price Changes"
f_registerMeasure "NbRPMChkInd"        "real"    "prznitem"     "Check RPM Emergency change for Effective Price Change"
f_registerMeasure "NbRPMChkAddPc"      "boolean" "prznitemweek" "Check RPM Emergency Additional Price Changes to Evaluate"

f_registerMeasure "NbLstExpPcChgInd"   "boolean" "prznitemweek" "Last Exported Price Change Indicator"
f_registerMeasure "NbWIPstPcChgIndCnd" "boolean" "prznitemweek" "Week Index Past Price Change Indicator Condition"
f_registerMeasure "NbWIPstPcChgInd"    "real"    "prznitemweek" "Week Index Past Price Change Indicator"
f_registerMeasure "NbLstWIPstPcChgInd" "real"    "prznitem"     "Latest Week Index Past Price Change Indicator"
f_registerMeasure "NbPcChgInd"         "boolean" "prznitemweek" "Price Change Indicator"
f_registerMeasure "NbDelPcChg"         "real"    "prznitemweek" "Deleted Price Changes"
f_registerMeasure "NbDelPcChgInd"      "boolean" "prznitemweek" "Deleted Price Changes  Indicator"
f_registerMeasure "NbModPcChg"         "real"    "prznitemweek" "Modified  Price Changes"
f_registerMeasure "NbModPcChgInd"      "boolean" "prznitemweek" "Modified  Price Changes Indicator"
f_registerMeasure "NbNewPcChg"         "real"    "prznitemweek" "New Price Changes"
f_registerMeasure "NbNewPcChgInd"      "boolean" "prznitemweek" "New Price Changes Indicator"
f_registerMeasure "NbAdjFStatDate"     "date"    "prznitemweek" "Adjusted Final Effective Date"

###############################################################################
#  Execute the necessary export calculations using the above newly created
#  measures.
###############################################################################

# CALC_A
#
# Calculate Todays Date
#
f_executeDomainExpression "Tday=now"

# CALC_B
#
# Calculate the RPO Domain Week Index correspondent to Todays Date
#
f_executeDomainExpression "TdayReal=index([CLND].[week],Tday)"
#
# CALC_C
#
# Calculate a boolean mask to be TRUE for All Future and Current Weeks
#
f_executeDomainExpression "NbCurFutWksMsk=if( index([CLND].current) >= TdayReal, TRUE, FALSE )"
#
# CALC_D
#
# Calculate a boolean mask to be TRUE for All Past Weeks
#
f_executeDomainExpression "NbPstWksMsk=if( index([CLND].current) < TdayReal, TRUE, FALSE )"
#
# CALC_E
#
# Calculate a boolean mask to be TRUE for the Current Week
#
f_executeDomainExpression "NbCurWkMsk=if( index([CLND].current) == TdayReal, TRUE, FALSE )"

# CALC_F
#
# Calculate the LAG measure of the latest approved prices.This is going to
# provide the necessary logic to understand in which Weeks are the Price Changes.
# A Price Change exist only in a Week where the price is different from the previous
# Week. Therefore the LAG is useful to compare the current Week with the last.
#
f_executeDomainExpression "NbFAppItPcLag = lag(FAppItPc)"

# CALC_G
#
# Calculate Price Changes to Evaluate
#
EXP="NbPcToEvaluate = if(FAppItPc != navalue(FAppItPc) && FAppItPc != NbFAppItPcLag, true, false)"
f_executeDomainExpression "$EXP"

#############################################################################################
#############################################################################################
# Calculations to check if the Approval executed since the last extract produced a Price Change
# that needs to be consider, in case of a RPM Emergency Price Change.
# This code expected that the RPM Item-PriceZone current Price is loaded in RPO before the
# extract is executed.

# CALC_RPM_EMERGENCY_PC_EVALUATION_A
#
# Check for all newly Approved Item-PriceZones, that are either in the Past up
# to Tomorrow date, which is the correspondent calendar index
#
# COND1 - Represents all Price Changes in the current Week with Past Effective Date
# CLNDI - Retrieve the Calendar Index
#
COND1="NbCurWkMsk && NbFStatDate <= addPeriods(Tday, 1, \"DAY\")"
CLNDI="index([CLND].current)"
EXP="NbRPMChkPst = if( NbFPcInd, if( NbPstWksMsk || ( $COND1 ), $CLNDI, 0 ), 0 )"
f_executeDomainExpression "$EXP"

# CALC_RPM_EMERGENCY_PC_EVALUATION_B
#
# Check for all newly Approved Item-PriceZones, that are either in the Past up
# to Tomorrow date, which is the correspondent calendar index
#
# COND1 - Represents all Price Changes in the current & future Weeks with Future Effective Date
# CLNDI - Retrieve the Calendar Index
#
COND1="NbCurFutWksMsk  && NbFStatDate > addPeriods(Tday, 1, \"DAY\")"
CLNDI="index([CLND].current)"
EXP="NbRPMChkFut = if( NbFPcInd, if( $COND1, $CLNDI, 0 ), 0 )"
f_executeDomainExpression "$EXP"

# CALC_RPM_EMERGENCY_PC_EVALUATION_C
#
# Latest Past newly Approved Item-PriceZones Price Change
#
EXP="NbRPMChkPstInd = NbRPMChkPst.max"
f_executeDomainExpression "$EXP"

# CALC_RPM_EMERGENCY_PC_EVALUATION_D
#
# The Effective Price Change to be consider when checking if there was a RPM Emergency Price
# Change
#
EXP="NbRPMChkInd = if( NbRPMChkPstInd == 0, NbRPMChkFut.min_pop, NbRPMChkPstInd)"
f_executeDomainExpression "$EXP"

# CALC_RPM_EMERGENCY_PC_EVALUATION_E
#
# Check for the Effective Price Changes the ones with different Price then RPM
#
COND1="NbRPMChkInd == current && Ol1OrgItPc != FAppItPc"
EXP="NbRPMChkAddPc = if(NbRPMChkInd != 0, if( $COND1, true, false), false)"
f_executeDomainExpression "$EXP"

# CALC_RPM_EMERGENCY_PC_EVALUATION_F
#
# Add to Price Changes to Evaluate the ones that need to be evaluated because have a different price
# then RPM due to Emergency Price change.
#
EXP="NbPcToEvaluate = if(NbRPMChkAddPc, true, ignore)"
f_executeDomainExpression "$EXP"

#############################################################################################
#############################################################################################

# CALC_H
#
# Store the Week Index of the Past Weeks where there are new Price Changes
#
# CLNDI - Retrieve the Calendar Index
#
CLNDI="index([CLND].current)"
EXP="NbWIPstPcChgInd= if( NbPstWksMsk, if( NbPcToEvaluate, $CLNDI, 0 ), 0 )"
f_executeDomainExpression "$EXP"

# CALC_I
#
# Calculation to understand for the current Week, of all Item-PriceZones,
# which ones have a Price Change in RPO.
#
EXP="NbWIPstPcChgIndCnd = if( NbCurWkMsk, if( NbPcToEvaluate, TRUE, FALSE) , FALSE )"
f_executeDomainExpression "$EXP"

# CALC_J
#
# Add to the measure data already calculated in CALC_F, the information of the Current Week
# Index if there is a new Price Change in the current week, that has an effective date of
# tomorrow or before. For this calculation we use the condition calculated in CALC_H.
#
# COND1 - Represents all the Weeks, per Item-PriceZone, that have a Price Change Effective Date
#           before or equal to Tomorrow.
# CLNDI - Retrieve the Calendar Index
#
COND1="nbfappat <= addPeriods(Tday, 1, \"DAY\")"
CLNDI="index([CLND].current)"
EXP="NbWIPstPcChgInd= if( NbWIPstPcChgIndCnd, if( $COND1, $CLNDI, 0 ), ignore )"
f_executeDomainExpression "$EXP"

# CALC_K
#
# Calculate the MAX Week Index per Item-PriceZone, of the previous calculated
# measure, to understand the lastest Past new Price Change. That is the only
# one we need to process and make it effective with the date of tomorrow.
#
EXP="NbLstWIPstPcChgInd = NbWIPstPcChgInd.max"
f_executeDomainExpression "$EXP"

# CALC_L
# 
# Calculate all the positions where there is a new Price Change in the
# Current or Future Weeks
#
EXP="NbPcChgInd = if( NbCurFutWksMsk, if( NbPcToEvaluate, TRUE, FALSE ), FALSE )"
f_executeDomainExpression "$EXP"

# CALC_M
#
# Add the measure data already calculated in CALC_K, the information of the most
# recent New Price Changes, per each of the Items-PriceZones, that have been
# created with a Past Effective Date, which therefore should be extracted with
# the Effective Date of tomorrow.
#
# COND1 - Represents, per Item-PriceZone, all the Past Weeks where there is a New
#           Price Change and all the Current Weeks where there is a New Price Change
#           with the Effective Date prior to Tomorrow.
# COND2 - Represents, per each of the Items-PriceZones, the most recent Week with
#           a New Price Change.
#
COND1="NbWIPstPcChgInd != navalue(NbWIPstPcChgInd)"
COND2="NbWIPstPcChgInd == NbLstWIPstPcChgInd"
EXP="NbPcChgInd = if( $COND1, if( $COND2, TRUE, FALSE), ignore)"
f_executeDomainExpression "$EXP"

# CALC_N
#
# For the New Price Changes being integrated, change any Past Effective Date to
# Tomorrows' Date.
#
# COND1 - Represents all the Weeks, per Item-PriceZone, that had a Price Change in
#         previous extracts and that will now change on the current extract.
# COND2 - Represents all the Price Changes that have an Effective Date prior or
#         equal to Today.
# ADD_DAY - Expression to SUM up 1 Day to Todays date.
COND1="FAppItPc != NbLstExpAppPcChg"
COND2="NbFStatDate <= Tday"
ADD_DAY="addPeriods(Tday, 1, \"DAY\")"
EXP="NbAdjFStatDate = if( $COND1, if( $COND2, $ADD_DAY, NbFStatDate), ignore)"
f_executeDomainExpression "$EXP"

# CALC_O
#
# Calculate the Deleted Price Changes and the correspondent indicator.
# Deleted Price Changes are all the Week-Item-PriceZone that had a Price
# Change before and no longer have it. And also all the Week-Item-PriceZone
# that had a Price Change before and now have a new Price Change with a
# different effective date.
#
# COND1 - Represents all Price Changes existing in the last extract (not
#         only the ones in the extracted files, but all price changes in
#         RPO domain when the last extract was executed), that not longer
#         exist in the RPO domain at this extract. Those are all the deleted
#         Price Changes in the system
# COND2 - Represents all the new/modified Price Changes that need to be included
#         in the current extract compared to the last extract.
# COND3 - Represents all Weeks per Item-PriceZone where already existed a
#         Price Change in previous extracts.
# COND4 - Represents all the Weeks per Item-PriceZone where the effective date
#         of the last Price Change has been different then the current Price
#         Change effective date for that Week-Item-PriceZone.
# COND5 - Represents all the Weeks per Item-PriceZone, where there is a modified
#         Price Change that has a different effective date than before.
# COND6 - Represents all the Weeks, per Item-PriceZone, where there has been either
#         a deleted Price Change or a Modified Price Change (overriding price change
#         for the correspondent week) where the Effective Date is now different.
#
COND1="NbLstExpPcChgInd && !NbPcChgInd"
COND2="FAppItPc != NbLstExpAppPcChg"
COND3="NbLstExpAppPcChg != navalue(NbLstExpAppPcChg)"
COND4="NbAdjFStatDate != NbLstExpAppPcChgED"
COND5="( $COND2 && $COND3 && $COND4 )"
COND6="$COND1 || $COND5"
f_executeDomainExpression "NbDelPcChg = if( NbCurFutWksMsk, if( $COND6, NbLstExpAppPcChg, 0 ), 0 )"
f_executeDomainExpression "NbDelPcChgInd = if( NbDelPcChg != navalue(NbDelPcChg), TRUE, FALSE )"

#CALC_P
#
# Calculate the Modified Price Changes and the correspondent indicator.
# Modified Price Changes are all the Week-Item-PriceZone that had a Price
# Change before and now still have the Price Change with the same Effective
# Date but with a different Price associated.
# 
# COND1 - Represents all the new/modified Price Changes that need to be included
#         in the current extract compared to the last extract.
# COND2 - Represents all Weeks, per Item-PriceZone, where already existed a
#         Price Change in previous extracts.
# COND3 - Represents all the Weeks, per Item-PriceZone, where the effective date
#         of the last Price Change is the same then the current Price
#         Change effective date for that Week-Item-PriceZone.
COND1="FAppItPc != NbLstExpAppPcChg"
COND2="NbLstExpAppPcChg != navalue(NbLstExpAppPcChg)"
COND3="NbAdjFStatDate == NbLstExpAppPcChgED"
EXP="NbModPcChg = if( NbPcChgInd, if( $COND1 && $COND2 && $COND3, FAppItPc, 0 ), 0 )"
f_executeDomainExpression "$EXP"
f_executeDomainExpression "NbModPcChgInd = if( NbModPcChg != navalue(NbModPcChg), TRUE, FALSE )"

#CALC_Q
#
# Calculate the New Price Changes and the correspondent indicator
#
# COND1 - Represents all the new/modified Price Changes that need to be included
#         in the current extract compared to the last extract.
# COND2 - Represents all Weeks, per Item-PriceZone, where there is a New Price Change,
#         which did not existed before.
# COND3 - Represents all the Weeks per Item-PriceZone, where there is a modified
#         Price Change that has a different effective date than before.
# COND4 - Represents all the Weeks, per Item-PriceZone, where there has been either
#         a New Price Change or a Modified Price Change (overriding price change
#         for the correspondent week) where the Effective Date is now different.
COND1="FAppItPc != NbLstExpAppPcChg"
COND2="NbLstExpAppPcChg == navalue(NbLstExpAppPcChg)"
COND3="( NbLstExpAppPcChg != navalue(NbLstExpAppPcChg) && NbAdjFStatDate != NbLstExpAppPcChgED )"
COND4="$COND1 && ( $COND2 || $COND3 )"
EXP="NbNewPcChg = if( NbPcChgInd, if( $COND4, FAppItPc, 0 ), 0 )"
f_executeDomainExpression "$EXP"
f_executeDomainExpression "NbNewPcChgInd = if( NbNewPcChg != navalue(NbNewPcChg), TRUE, FALSE )"

###############################################################################
#  Export data from the RPO domain to FLATFILEs
###############################################################################

# Export the Deleted Price Changes
exportMeasure -d $DOMAIN_PATH -processes $NO_OF_CPUS -mask nbdelpcchgind \
	-meas "nblstexpapppcchged.format(%Y%m%d),nbdelpcchg.format(%.2f)" \
	-out "${O_PC_FILE}.source.del"
check_error_and_clean_before_exit $?

# Export the Modified Price Changes
MEAS1="nbadjfstatdate.format(%Y%m%d)"
MEAS2="nbmodpcchg.format(%.2f)"
MEAS3="nbfappat.format(%Y%m%d)"
MEAS4="nbfappby"
exportMeasure -d $DOMAIN_PATH -processes $NO_OF_CPUS -mask nbmodpcchgind \
	-meas "${MEAS1},${MEAS2},${MEAS3},${MEAS4}" \
	-out "${O_PC_FILE}.source.mod"
check_error_and_clean_before_exit $?

# Export the New Price Changes
MEAS2="nbnewpcchg.format(%.2f)"
exportMeasure -d $DOMAIN_PATH -processes $NO_OF_CPUS -mask nbnewpcchgind \
	-meas "${MEAS1},${MEAS2},${MEAS3},${MEAS4}" \
	-out "${O_PC_FILE}.source.new"
check_error_and_clean_before_exit $?

###############################################################################
#  Convert input data files in RETLSV format
###############################################################################

csv_converter "csv-retlsv" "${O_PC_FILE}.source.del" "${O_PC_FILE}.source.del.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${O_PC_FILE}.source.mod" "${O_PC_FILE}.source.mod.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${O_PC_FILE}.source.new" "${O_PC_FILE}.source.new.retl"
check_error_and_clean_before_exit $?

###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
	
	<!--
		Import the Deleted Price Changes files for transformation.
	-->
	<OPERATOR  type="import">
		<PROPERTY  name="inputfile" value="${O_PC_FILE}.source.del.retl"/>
		<PROPERTY  name="schemafile" value="${I_DEL_PC_SCHEMA}"/>
		<OUTPUT name="del_pc_i.v"/>
	</OPERATOR>
	
	<!--
		Add additional fields to the Deleted Price Changes file to allow it
		to match with the Output file structure.
	-->
	<OPERATOR type="generator">
		<INPUT name="del_pc_i.v"/>
		<PROPERTY name="schema">
			<![CDATA[
				<GENERATE>
					<FIELD name="APPROVAL_DATE" type="date"
						nullable="true">
						<CONST value=""/>
					</FIELD>
					<FIELD name="APPROVAL_USER_ID" type="string"
						nullable="true">
						<CONST value=""/>
					</FIELD>
					<FIELD name="REASON_CODE" type="int8"
						nullable="false">
						<CONST value="1"/>
					</FIELD>
					<FIELD name="UPDATE_TYPE" type="string"
						nullable="false">
						<CONST value="D"/>
					</FIELD>
				</GENERATE>
			]]>
		</PROPERTY>
		<OUTPUT name="del_pc.v"/>
	</OPERATOR>
	
	<!--
		Import the Modified Price Changes files for transformation.
	-->
	<OPERATOR  type="import">
		<PROPERTY  name="inputfile" value="${O_PC_FILE}.source.mod.retl"/>
		<PROPERTY  name="schemafile" value="${I_NEWMOD_PC_SCHEMA}"/>
		<OUTPUT name="mod_pc_i.v"/>
	</OPERATOR>
	
	<!--
		Add additional fields to the Modified Price Changes file to allow it
		to match with the Output file structure.
	-->
	<OPERATOR type="generator">
		<INPUT name="mod_pc_i.v"/>
		<PROPERTY name="schema">
			<![CDATA[
				<GENERATE>
					<FIELD name="REASON_CODE" type="int8"
						nullable="false">
						<CONST value="1"/>
					</FIELD>
					<FIELD name="UPDATE_TYPE" type="string"
						nullable="false">
						<CONST value="U"/>
					</FIELD>
				</GENERATE>
			]]>
		</PROPERTY>
		<OUTPUT name="mod_pc.v"/>
	</OPERATOR>
	
	<!--
		Import the New Price Changes files for transformation.
	-->
	<OPERATOR  type="import">
		<PROPERTY  name="inputfile" value="${O_PC_FILE}.source.new.retl"/>
		<PROPERTY  name="schemafile" value="${I_NEWMOD_PC_SCHEMA}"/>
		<OUTPUT name="new_pc_i.v"/>
	</OPERATOR>
	
	<!--
		Add additional fields to the New Price Changes file to allow it
		to match with the Output file structure.
	-->
	<OPERATOR type="generator">
		<INPUT name="new_pc_i.v"/>
		<PROPERTY name="schema">
			<![CDATA[
				<GENERATE>
					<FIELD name="REASON_CODE" type="int8"
						nullable="false">
						<CONST value="1"/>
					</FIELD>
					<FIELD name="UPDATE_TYPE" type="string"
						nullable="false">
						<CONST value="C"/>
					</FIELD>
				</GENERATE>
			]]>
		</PROPERTY>
		<OUTPUT name="new_pc.v"/>
	</OPERATOR>
	
	<!--
		Aggregate all the three files into one.
	-->
	<OPERATOR type="collect">
		<INPUT name="new_pc.v"/>
		<INPUT name="mod_pc.v"/>
		<INPUT name="del_pc.v"/>
		<OUTPUT name="all_pc.v"/>
	</OPERATOR>
	
	<!--
		Export Price Changes to a file in a RETL format.
	-->
	<OPERATOR type="export">
		<INPUT name="all_pc.v"/>
		<PROPERTY name="schemafile" value="${O_PC_SCHEMA}"/>
		<PROPERTY name="outputfile" value="${O_PC_FILE}.retl"/>
	</OPERATOR>
	
</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${O_PC_FILE}.retl" "${O_PC_FILE}"
check_error_and_clean_before_exit $?

###############################################################################
#  Execute the final export calculations before existing the interface script
###############################################################################

# CALC_F_A
#
# Delete from the stored Price Change all the ones that have been deleted.
#
f_executeDomainExpression "NbLstExpAppPcChg = if( NbDelPcChgInd, 0, ignore )"

# CALC_F_B
#
# Store all the existing Price Changes in the RPO Domain.
#
f_executeDomainExpression "NbLstExpAppPcChg = if( NbPcChgInd, FAppItPc, 0 )"

#CALC_F_C
#
# Store the Effective Date that was associated with the Price Change, taking into
# account the ones that have been moved for Tomorrow's Date, due to being planned
# to Past Effective Dates.
#
EXP="NbLstExpAppPcChgED = if( NbPcChgInd, NbAdjFStatDate, ignore )"
f_executeDomainExpression "$EXP"

#CALC_F_D
#
# Store all indicator of all existing Price Changes in the RPO domain.
#
f_executeDomainExpression "NbLstExpPcChgInd = NbPcChgInd"

#CALC_F_E
#
# Cleared the marked Price Change positions
#
f_executeDomainExpression "NbFPcInd = if(NbRPMChkAddPc, true, false)"

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

NR_PC=`wc -l ${O_PC_FILE} | awk '{print $1}'`
log_message INFORMATION "Number of records in ${O_PC_FILE} = ${NR_PC}"

###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
log_message INFORMATION "Program completed successfully."
check_error_and_clean_before_exit 0 "NO_CHECK"
