#!/bin/ksh


#
# Copyright ? 2007, 2008, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 1193735 $
# $Date: 2016-07-22 12:50:44 -0500 (Fri, 22 Jul 2016) $
# $Author: mlei $
#
##############################################################################
# This script is used to load the required measures for the RegPrice solution.  This script
#	may be modified to, 
#		1) account for the use for MNL demand model
#		2) Adding removing measure loads for different optimization and demand levels
#		3) Loading any custom attributes
#		4) Loading any other custom measures
#
# For proper functioning, RegPrice needs a minimal set of measures.  These measures include:
#	1) Those that define Location hierarchy dynamic hierachy (str -> przn)
#	2) Demand parameters (APE outputs in the GA solution)
#	3) UOM and EquiUOM attributes for the items
#	4) At least one Demand-Group definition
#	5) At least one price ladder definition
#	6) At least one item link group definition
#	7) Competition prices and competition item linkages to retailer items
#
# RegPrice = Regular Price Optimization
# MNL = MultiNomial Logit
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"

domainPath="$PWD/../domain/RegPrice"
CmdOL=3
CmdDL=3
MaxProcess=1

scriptName=$(basename $0)

function f_Usage
{
  _log_message ERROR "Usage: $scriptName options (-dob)"
  _log_message ERROR "   d: domain path [default = $PWD/../domain]"
  _log_message ERROR "   o: Optimization Level [default = 3; Valid Values = 1-3]"
  _log_message ERROR "   b: Demand Level [default = 3; Valid Values = 1-3]"
  _log_message ERROR "   p: max processes [DEFUALT =1]"
}

##### NB CODE -RB124-1- START #####

##############################################################################
#
#  The f_checkIfMeasExistsRemove if measure exists in domain, then remove it.
#
#     Usage: f_CountUniqLinesOnFiles  $1
#
#            $1
#                Measure Name
#
##############################################################################
function f_checkIfMeasExistsRemove
{
	printMeasure -d $domainPath -m $1 -specs > /dev/null 2>&1
	result=$?
	
	_log_message DEBUG "Check if measure exists result: $result"
	
	if [[ $result -eq "0" ]] ; then
		_call regmeasure -d "$domainPath" -remove $1 ; _verify_binary
	fi
}

##### NB CODE -RB124-1- END #####

# get optional inputs
while getopts ":d:o:b:p:" Option
do
  case $Option in
    d    ) domainPath="${OPTARG}"       ;;
    o    ) CmdOL="${OPTARG}"            ;;
    b    ) CmdDL="${OPTARG}"            ;;
    p    ) MaxProcess="${OPTARG}"       ;;

    *    )
      f_Usage
      _clean_exit NONZERO_EXIT ;;
  esac
done

shift $(($OPTIND - 1))


# perform sanity checks on Optimization and Demand levels
if [ $CmdOL -lt 1 -o $CmdOL -gt 3 ]
then
	_log_message ERROR "Invalid Optimization Level: $CmdOL.  Aborting Load"
	_clean_exit 1 "Aborting."
fi

if [ $CmdDL -lt 1 -o $CmdDL -gt 3 ]
then
	_log_message ERROR "Invalid Demand Level: $CmdDL.  Aborting Load"
	_clean_exit 1 "Aborting."
fi

# Start loading measures
# Dynamic Hierarchy
_log_message INFORMATION "Loading str->przn Dynamic Hierarchy Definitions"
_call loadmeasure -d $domainPath -measure PzDynHrMp -processes $MaxProcess -loglevel debug
            _verify_binary
#_call loadmeasure -d $domainPath -measure PzDynHrLbl -processes $MaxProcess -loglevel debug
#            _verify_binary

_log_message INFORMATION "Finished loading str->przn Dynamic Hierarchy Definitions"

# Demand Parameters
optLvl=1
dmdLvl=1

_log_message INFORMATION "Loading ScanPro demand model parameters"

# Optimization level parameters
#while [ $optLvl -le $CmdOL ]
#do
	_log_message INFORMATION "Loading Optimization Level $optLvl parameters"
	for measName in OrgItPc GammaSP HisHiPrc AnchPrc HisLoPrc
	do
	  _call loadmeasure -d "$domainPath" -measure Ol$optLvl$measName -processes $MaxProcess -loglevel debug
      _verify_binary
	done

	optLvl=`expr $optLvl + 1`
#done

_log_message INFORMATION "Finished loading optimization level parameters"

# Demand level parameters
#while [ $dmdLvl -le $CmdDL ]
#do
	_log_message INFORMATION "Loading Demand Level $dmdLvl parameters"

	for measName in ItBDSP OrgItCst
	do
	  _call loadmeasure -d "$domainPath" -measure Dl$dmdLvl$measName -processes $MaxProcess -loglevel debug
      _verify_binary
	done
	dmdLvl=`expr $dmdLvl + 1`
#done

_log_message INFORMATION "Finished loading demand level parameters"
_log_message INFORMATION "Finished loading ScanPro demand model parameters"

#  Attributes
_log_message INFORMATION "Loading Price Family, UOM and Equivalent UOM Attributes"
optLvl=1

	_log_message INFORMATION "Loading Optimization Level $optLvl Attributes"

	for measName in ItUOM ItEquiU PcFml
	do
	  _call loadmeasure -d "$domainPath" -measure Ol$optLvl$measName -processes $MaxProcess -loglevel debug
      _verify_binary
	done
	optLvl=`expr $optLvl + 1`

_log_message INFORMATION "Finished UOM and Equivalent UOM Attributes"

# Demand Group Management
_log_message INFORMATION "Loading Demand Group management meeasures"

#_call loadmeasure -d $domainPath -measure DmndLvl -processes $MaxProcess -loglevel debug
#_call loadmeasure -d $domainPath -measure OptLvl -processes $MaxProcess -loglevel debug

_call loadmeasure -d $domainPath -measure ScnCatMp -processes $MaxProcess -loglevel debug
           _verify_binary
_call loadmeasure -d $domainPath -measure pldrppt -processes $MaxProcess -loglevel debug
           _verify_binary
_call loadmeasure -d $domainPath -measure pos -processes $MaxProcess -loglevel debug
           _verify_binary
_call loadmeasure -d $domainPath -measure scnrigmap -processes $MaxProcess -loglevel debug
           _verify_binary
_call loadmeasure -d $domainPath -measure ol1futprom -processes $MaxProcess -loglevel debug
           _verify_binary
_call loadmeasure -d $domainPath -measure ol1ItPcLdr -processes $MaxProcess -loglevel debug
           _verify_binary

optLvl=1


# Competitive Data
_log_message INFORMATION "Loading Competitive Data"
optLvl=1
#while [ $optLvl -le $CmdOL ]
#do
	_log_message INFORMATION "Load Optimization Level $optLvl Competitive Data"
	for measName in CmpCurPc CmpPcChkDt CmpPcTp
	do
	  _call loadmeasure -d "$domainPath" -measure Ol$optLvl$measName -processes $MaxProcess -loglevel debug
      _verify_binary
	done
	#optLvl=`expr $optLvl + 1`
#done

_call cd $domainPath
_call exportHier -d $domainPath -hier prod -datFile $domainPath/input/tempprod.txt
_verify_binary
cat $domainPath/input/tempprod.txt | awk -F ',' '{print $1 "," $1 ",true" }' > $domainPath/input/itemitermap.csv.ovr
#_call mace -d $domainPath -run -expression "ItemIterMap=if(position([PROD].[ITEM])==position([PROR].[ITER]),true,false)"

_call loadmeasure -d $domainPath -measure itemitermap -processes $MaxProcess -loglevel debug
_verify_binary
_call rm $domainPath/input/tempprod.txt
_verify_binary


_verify_log
_log_message INFORMATION "Finished loading Competitive Data"
_log_message INFORMATION "set the RHS/LHS item map"

##### NB CODE -1- START #####

_log_message INFORMATION "Load ADEO Additional Integration Data"
for measName in ItVat IlgItItrMp
do
	_call loadmeasure -d "$domainPath" -measure Ol$optLvl$measName -processes $MaxProcess -loglevel debug
	_verify_binary
done

_log_message INFORMATION "Finished loading ADEO Additional Integration Data"

##### NB CODE -1- END #####

##### NB CODE -RB124-2- START #####

_log_message INFORMATION "Copy RPO security setup from the Merchandise Hierarchy (PROD) to RHS Merchandise (PROR)."

# Register a temporary measure to support the copy of the security setup from the Merchandise Hierarchy (PROD)
# to the RHS Merchandise (PROR)
measure="prodprorsecuritygrp"
f_checkIfMeasExistsRemove $measure ; _verify_script

_call regmeasure -d "$domainPath" -add $measure -type boolean -baseint cat_catrgrp_ \
			-defagg or -defspread repl -filename prodprorsecuritygrp -db data/security
_verify_binary

# Transform the PROD security to a measure sharing PROD and PROR
_call mace -d "$domainPath" \
			-run -expression "${measure} = if(position([PROD].[CAT])==position([PROR].[CATR]), prodsecuritygrp, false)" \
			-processes $MaxProcess -loglevel debug
_verify_binary

# Copy security from the shared measure to the PROR hierarchy
_call mace -d "$domainPath" -run -expression " prorsecuritygrp = ${measure}.or " -processes $MaxProcess -loglevel debug
_verify_binary

# Unregister the temporary measure
f_checkIfMeasExistsRemove $measure ; _verify_script

_log_message INFORMATION "Finished copying RPO security setup from the Merchandise Hierarchy (PROD) to RHS Merchandise (PROR)."

##### NB CODE -RB124-2- END #####

##### NB CODE -RB125-1- START #####

_log_message INFORMATION "Load ADEO External Source Translations"
for dimName in item iter clss clsr scat scar cat catr dept depr chn chnr brnd brnr
do
	_call loadmeasure -d "$domainPath" -measure "r_${dimName}label" -processes $MaxProcess -loglevel debug
	_verify_binary
done

_log_message INFORMATION "Finished loading ADEO External Source Translations"

##### NB CODE -RB125-1- END #####

##### NB CODE -RB47-1- START #####

# Load the National Item Price data into RPO
_call loadmeasure -d "$domainPath" -measure NbNatItPc -processes $MaxProcess -loglevel debug
_verify_binary

##### NB CODE -RB47-1- END #####

##### NB CODE -RB131-1- START #####

#  Set the link between a Category (CAT) and a Category RHS (CATR)
_log_message INFORMATION "Set the RHS/LHS Category Map"
_call mace -d $domainPath -run -expression "NbCatCatrMap=if(position([PROD].[CAT])==position([PROR].[CATR]),true,false)" \
			-processes $MaxProcess -loglevel debug
_verify_binary
_log_message INFORMATION "Finish setting the RHS/LHS Category Map"

# Set the link of Scenarios with the Categories RHS based on the Mapping of Scenarios
# with Categories
_log_message INFORMATION "Set the Scenario-Categories RHS Map"
_call mace -d $domainPath -run -expression "NbBScnCatCatrMp=if(NbCatCatrMap,ScnCatMp,false)" \
			-processes $MaxProcess -loglevel debug
_verify_binary
_call mace -d $domainPath -run -expression "NbBScnCatrMp=NbBScnCatCatrMp" \
			-processes $MaxProcess -loglevel debug
_verify_binary
_log_message INFORMATION "Finish setting the Scenario-Categories RHS Map"


#  Load the Item BU Map data sourced from RMS and Transformed within the custom code XXRPO207
_log_message INFORMATION "Loading Item Abonnement BU meeasure"
_call loadmeasure -d $domainPath -measure NbItemBUMap -processes $MaxProcess -loglevel debug
_verify_binary
_log_message INFORMATION "Finish loading Item Abonnement BU meeasure"

#  Set the Map of Item (ITEM) and Item RHS (ITER), according to the availability of Abonné Items
#  for a specific BU
_log_message INFORMATION "Set the RHS/LHS Item Map, according to the Abonnement BU"
_call mace -d $domainPath -run -expression "NbItemIterBUMap=if(ItemIterMap,NbItemBUMap,false)" \
			-processes $MaxProcess  -loglevel debug
_verify_binary
_log_message INFORMATION "Finish setting the RHS/LHS Category Map, according to the Abonnement BU"

#  Set the Map of Category (CAT) and Category RHS (CATR), according to the availability of Abonné Items
#  in that Category for a specific BU
_log_message INFORMATION "Set the RHS/LHS Category Map, according to the Abonnement BU"
_call mace -d $domainPath -run -expression "NbCatCatrBUMap=if(NbCatCatrMap,NbItemBUMap,false)" \
			-processes $MaxProcess -loglevel debug
_verify_binary
_log_message INFORMATION "Finish setting the RHS/LHS Category Map, according to the Abonnement BU"

#  Remove from all RPO existing Scenario Groups, the items that are no longer Abonné with the Price Zones
#  associated with that Scenario Group.
_log_message INFORMATION "Remove Items no longer Abonné to Scenario Groups."
_call mace -d $domainPath -run -expression "NbBScnrItLocMap = if(ScnrLocMap, if(ScnrItMbr, NbItemBUMap, false), false)" \
			-processes $MaxProcess  -loglevel debug
_verify_binary
_call mace -d $domainPath -run -expression "ScnrItMbr = if(NbBScnrItLocMap, ignore, false)" \
			-processes $MaxProcess  -loglevel debug
_verify_binary
_log_message INFORMATION "Finish removing Items no longer Abonné to Scenario Groups."

##### NB CODE -RB131-1- END #####

##### NB CODE -RB37a-1- START #####

#  Load the Item Unit of Capacity data sourced from RMS and Transformed within
#  the custom code XXRPO209
_log_message INFORMATION "Loading Item Unit of Capacity measure"
_call loadmeasure -d $domainPath -measure NbItUoCap -processes $MaxProcess -loglevel debug
_verify_binary
_log_message INFORMATION "Finish loading Item Unit of Capacity measure"

#  Load the Item-BU Main Publishing Price data sourced from RMS and Transformed
#  within the custom code XXRPO210
_log_message INFORMATION "Loading Item-BU Main Publishing Price measure"
_call loadmeasure -d $domainPath -measure NbItBUMPPc -processes $MaxProcess -loglevel debug
_verify_binary
_log_message INFORMATION "Finish loading Item-BU Main Publishing Price measure"

##### NB CODE -RB37a-1- END #####

_clean_exit SUCCESS

