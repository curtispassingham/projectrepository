#!/bin/ksh
#
# Copyright (c) 2007, 2016, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 1198206 $
# $Date: 2016-08-12 10:34:01 -0500 (Fri, 12 Aug 2016) $
# $Author: bassaf $
#
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"

scriptName=$(basename "$0")
domainPath="."

while getopts :d: Option
do
  case $Option in
    d    ) domainPath="${OPTARG}"     ;;

    *    )
        _log_message ERROR "Usage: $scriptName -d {domainpath}"
        _log_message ERROR "d: domain path [default = $PWD/../domain]"
        _clean_exit SUCCESS
        ;;
  esac
done

shift $(($OPTIND - 1))

_call cd "$domainPath"
_verify_binary

function f_newitem
{
   _call mace -d . -run -group common_data
   _verify_binary

   _call mace -d . -run -group post_common_data
   _verify_binary
}


ldomList=$( domaininfo -d . -shellsubdomains -loglevel none )
_verify_binary "domaininfo -d . -shellsubdomains"

for _ldom in ${ldomList}; do
   _call cd "${_ldom}"
   _verify_binary

   _para_spawn f_newitem  # Run in background

   _call cd ..
   _verify_binary
done

_para_wait    # Wait for background processes to complete.
_verify_script

_log_message INFORMATION "New Item batch complete."
_verify_log
_clean_exit SUCCESS

