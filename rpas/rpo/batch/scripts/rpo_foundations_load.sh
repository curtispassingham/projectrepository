#!/bin/ksh
#
# $Date: 2018-03-20 $
#
##############################################################################
#
# This script is used to load the required Hierarchies for the RPO solution.
# This script may be modified to load any other custom Hierarchies
#
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"

domainPath="$PWD/../domain/RegPrice"
defaultDomainPath="$domainPath/ldom99999"
maxProcess="${BSA_MAX_PARALLEL:-1}"
logLevel="${BSA_LOG_LEVEL:-INFORMATION}"
ctrlFile="$BATCH_HOME/scripts/control_files/rpo_foundations_load.ctrl"
usePurgeAgeZero=false

scriptName=$(basename $0)
scriptTimestamp=$(date +%y%m%d_%H%M%S)

###############################################################################
#  FUNCTIONS
###############################################################################

function f_Usage
{
	_log_message ERROR "Usage: $scriptName options (-dplz)"
	_log_message ERROR "    d: domain path [DEFAULT = ${domainPath}"
	_log_message ERROR "    g: default local partition domain \
		[DEFAULT = ${domainPath}/ldom99999"
	_log_message ERROR "    p: max processes [DEFAULT = ${maxProcess}]"
	_log_message ERROR "    l: log level [DEFAULT = ${logLevel}]"
	_log_message ERROR "    z: use PurgeAge 0 for all Hierarchies [DEFAULT - purgeAge defined in the Control file]"
	_log_message ERROR "    "
	_log_message ERROR "Example: $scriptName -d [DOMAIN_PATH] -g [DEFAUL_LOCAL_DOMAIN_PATH] -p [#_MAX_PROCESSES] -l [LOG_LEVEL] -z"
}

##############################################################################
#
#  The f_CountUniqLinesOnFiles sums up the number of lines existing in all files that
#  fall under by the filename pattern provided in all arguments
#
#     Usage: f_CountUniqLinesOnFiles  $1 $2 $3
#
#            $1
#                Path where to look for the filename patterns
#            $2
#                Filename patterns that should be used to search for the files to be
#                included in the sum.
#            $3
#                Return Sum of number of lines in files
#
##############################################################################
function f_CountUniqLinesOnFiles
{
	_log_message DEBUG "f_CountUniqLinesOnFiles: Start"
	
	if [[ $# -ne 3 ]]
	then
		_log_message ERROR "f_CountUniqLinesOnFiles: Bad usage."
		_log_message DEBUG "f_CountUniqLinesOnFiles: End"
		eval "$3='-1'"
		return -1
	else
		_log_message DEBUG "f_CountUniqLinesOnFiles: Calculating the number of records in the files: $1/$2"
		
		# Count all uniq non-blank lines out of all files matched in the pattern $2
		t_num_lines=`cat $1/$2 | sort | uniq | sed '/^\s*$/d' | wc -l`
		
		_log_message DEBUG "        Number of Records: ${t_num_lines}"
		
		_log_message DEBUG "f_CountUniqLinesOnFiles: End"
		eval "$3='$t_num_lines'"
		return 0
	fi
}

##############################################################################
#
#  The f_getLatestProcessedFileTimestamp gets the timestamp added by the loadHier
#  RPAS command to the input data files of the latest set of files loaded.
#
#     Usage: f_getLatestProcessedFileTimestamp  $1 $2
#
#            $1
#                Path where to look for the filename patterns
#            $2
#                The pattern of filenames to look for.
#            $3
#                Return TimeStamp string
#
##############################################################################
function f_getLatestProcessedFileTimestamp
{
	_log_message DEBUG "f_getLatestProcessedFileTimestamp: Start"
	
	if [[ $# -ne 3 ]]
	then
		_log_message ERROR "f_getLatestProcessedFileTimestamp: Bad usage."
		_log_message DEBUG "f_getLatestProcessedFileTimestamp: End"
		return -1
	else
		_log_message DEBUG "f_getLatestProcessedFileTimestamp: Search command: ls -lt ${1}/${2} | head -n 1 | awk '{print \$9}' | awk -F"." '{ print \$NF }'"
		filesTimestamp=`ls -lt ${1}/${2} | head -n 1 | awk '{print $9}' | awk -F"." '{ print $NF }'`
		
		_log_message DEBUG "f_getLatestProcessedFileTimestamp: filesTimestamp: ${filesTimestamp}"
		_log_message DEBUG "f_getLatestProcessedFileTimestamp: End"
		eval "$3='$filesTimestamp'"
		return 0
	fi
}

##############################################################################
#
#  The f_restoreProcessedFilesTimestamp move files that have filename timestamp $3 from the
#  source path to the destination path and remove the Timestamp from the filename
#
#     Usage: f_restoreProcessedFilesTimestamp  $1 $2 $3
#
#            $1
#                Source Path
#            $2
#                Destination Path
#            $3
#                Timestamp string
#
##############################################################################
function f_restoreProcessedFilesTimestamp
{
	_log_message DEBUG "f_restoreProcessedFilesTimestamp: Start"
	
	if [[ $# -ne 3 ]]
	then
		_log_message ERROR "f_restoreProcessedFilesTimestamp: Bad usage."
		_log_message DEBUG "f_restoreProcessedFilesTimestamp: End"
		return -1
	else
		
		ls -l $1/*$3* | awk '{print $9}' | while read line; do
			_log_message DEBUG "f_restoreProcessedFilesTimestamp: File processing: $line"
			
			# Get the destination filename by removing the timestamp from the source filename
			_log_message DEBUG "f_restoreProcessedFilesTimestamp: DestFilename Call: basename "$line" | sed 's/.${3}//g'"
			destFilename=`basename "$line" | sed "s/.${3}//g"`
			_log_message DEBUG "f_restoreProcessedFilesTimestamp: DestFilename: $destFilename"
			
			# Move the file from the source to dest with the new filename
			_call mv $line $2/$destFilename
			_verify_binary
		done
		
		_log_message DEBUG "f_restoreProcessedFilesTimestamp: End"
		return 0
	fi
}

##############################################################################
#
#  The f_loadHierarchy loads optional and mandatory hierarchies in RPO.
#
#     Usage: f_loadHierarchy  $1 $2 $3
#
#            $1
#                Hierarchy Name (e.g. prod, loc, clnd, ...)
#            $2
#                0 - Mandatory hierarchy load
#                1 - Optional hierarchy load
#            $3
#                PurgeAge value (non-negative integer: 0+)
#
##############################################################################
function f_loadHierarchy
{
	_log_message DEBUG "f_loadHierarchy: Start"
	
	purgeAge=$3
	
	mandatoryOptional=$([ "${2}" == "0" ] && echo "Mandatory" || echo "Optional")
	_log_message INFORMATION "Loading RPO $mandatoryOptional Hierarchy: ${1}"
	
	hierFileFW="${1}.dat"
	hierFileCSV="${1}.csv.dat"
	
	backupHierFilePath="${TMP}/${scriptName}_${scriptTimestamp}_${1}.csv.dat"
	
	_log_message DEBUG "f_loadHierarchy: Check if one of the following files exist:"
	_log_message DEBUG "    1 -> ${domainPath}/input/${hierFileFW}"
	_log_message DEBUG "    2 -> ${domainPath}/input/${hierFileCSV}"
	
	# Evaluate if the Hierarchy input data files do not exist
	if [ ! -r  "${domainPath}/input/${hierFileFW}" -a ! -r "${domainPath}/input/${hierFileCSV}" ]; then
		
		_log_message DEBUG "f_loadHierarchy: None of the files exist."
		
		# Evaluate if the Hierarchy input is optional
		if [[ $2 -eq 1 ]]; then
			
			message="Load Optional RPO Hierarchy ${1}: Input data file not available. Skipping the load."
			_log_message WARNING "$message"
			
		# Evaluate if the Hierarchy input is mandatory
		else
			message="Load Mandatory RPO Hierarchy ${1}: Input data file not available.\n\n"
			message+="\tShould exist in the RPO Domain Input folder one of the following files:\n"
			message+="\t\tIf data in FIX_WIDTH format: ${domainPath}/input/${hierFileFW}\n"
			message+="\t\tIf data in CSV format: ${domainPath}/input/${hierFileCSV}\n"
			_log_message ERROR "$message"
			_clean_exit 1 "Aborting."
		fi
	
	# Process Hierarchy input data files loading
	else
		_log_message DEBUG "f_loadHierarchy: One of the files exist."
		
		# Check the type of input data file (CSV or FIXED_WIDTH), because it can't be both
		if [ -f $domainPath/input/$hierFileFW ]; then
			_log_message INFORMATION "Input data files of Hierarchy ${1} are in FIXED_WIDTH format."
			hierFilePath=$hierFileFW
		elif [ -f $domainPath/input/$hierFileCSV ]; then
			_log_message INFORMATION "Input data files of Hierarchy ${1} are in CSV format."
			hierFilePath=$hierFileCSV
		else
			_log_message ERROR "Input data files of Hierarchy ${1} are in an UNKNOWN format."
			_clean_exit 1 "Aborting."
		fi
		
		# Calculate the number of lines in the input files for after Hierarchy Load validation
		_call f_CountUniqLinesOnFiles $domainPath/input "${hierFilePath}*" linesToLoad
		_verify_script
		message="Number of unique records in the input data files is: ${linesToLoad}"
		_log_message INFORMATION "$message"
		
		# Create Hierachy Backup file to restore domain positions in case of error
		message="Extract to current RPO domain hierarchy, in case there are format issues with input data files "
		message+="records, to allow a restore to the current domain hierarchy."
		_log_message INFORMATION "$message"
		_call exportHier -d "$domainPath" -hier $1 -datFile $backupHierFilePath \
			-loglevel $logLevel
		_verify_binary
		
		# Execute the loadHier test of the available Input data files to check they are in the correct format
		message="Execute the test loadHier command to check that the format of all input data files records is ok. "
		message+="loadHier executed with MAX purgeAge to avoid position deletion in case of input data files records "
		message+="with format issues."
		_log_message INFORMATION "$message"
		_call loadHier -d "$domainPath" -load $1 -processes $maxProcess -loglevel $logLevel \
			-logLoadedPositions -purgeAge 99999 -defaultDomain $defaultDomainPath
		_verify_binary
		
		# Read the number of lines loaded in the test loadHier process
		_call f_CountUniqLinesOnFiles $domainPath/input/processed "loaded${1}.dat" linesLoaded
		_verify_script
		message="Number of records successfully loaded in the test loadHier execution is: ${linesLoaded}"
		_log_message INFORMATION "$message"
		
		# Remove loaded records files after collecting it's quantity
		_log_message DEBUG "Remove file with Hierarchy loaded records after collecting it's quantity."
		rm "${domainPath}/input/processed/loaded${1}.dat"
		_verify_binary
		
		# Collect loaded input data files timestamp after being loaded by the loadHier command.
		message="Get timestamp of latest loaded files with f_getLatestProcessedFileTimestamp "
		message+="$domainPath/input/processed \"${hierFilePath}*\""
		_log_message DEBUG "$message"
		f_getLatestProcessedFileTimestamp $domainPath/input/processed "${hierFilePath}*" orgFilesTimestamp
		_verify_script
		message="Timestamp of latest loaded files with f_getLatestProcessedFileTimestamp: ${orgFilesTimestamp}"
		_log_message DEBUG "$message"
		
		# If the loaded number of records are not the same of the input data files records, then restore
		# the hierarchy positions by loading the backed up positions.
		if [[ linesLoaded -ne linesToLoad ]]; then
			message="Loading $mandatoryOptional RPO Hierarchy ${1}: Some input data file records have format issues.\n\n"
			message+="\tPlease check the loadHier logfile for more information.\n"
			message+="\tIf no sufficient information is available, please run this script again with a different"
			message+=" LOGLEVEL.\n"
			message+="\tCurrent LOGLEVEL is set to ${logLevel}."
			message+="\tEither fix the input data file or produce a new one: ${hierFilePath}.\n\n"
			message+="\tAdditional clean up tasks are going to be executed.\n"
			_log_message ERROR "$message"
			
			# Retore RPO Domain Hierarchy positions from data file backup
			message="Restore the backed up hierarchy data file to be loaded and to restore the hierachy to it's initial "
			message+="data."
			_log_message INFORMATION "$message"
			_call mv $backupHierFilePath ${domainPath}/input/${1}.csv.dat
			_verify_binary
			
			# Load into the Domain the exact positions it had just before this Hierarchy load process
			message="Execute the loadHier command to restore the Hierarchy data."
			_log_message INFORMATION "$message"
			_call loadHier -d "$domainPath" -load $1 -processes $maxProcess -purgeAge 0 \
				-defaultDomain $defaultDomainPath
			_verify_binary
			
			
			# Get the loadHier timestamp of the files just loaded and remove them, since they were a backup process
			# and not a normal load process. Therefore, no need to keep these files for the record.
			message="Remove loaded backup data files from processed folder, as those was not a normal Hierarchy data load. "
			message+="So, they shouldn't be stored in the processed folder."
			_log_message INFORMATION "$message"
			message="Get timestamp of latest loaded files with f_getLatestProcessedFileTimestamp "
			message+="$domainPath/input/processed \"${hierFilePath}*\""
			_log_message DEBUG "$message"
			
			f_getLatestProcessedFileTimestamp $domainPath/input/processed "${hierFilePath}*" bkpFilesTimestamp
			_verify_script
			
			message="Timestamp of latest loaded files with f_getLatestProcessedFileTimestamp: ${bkpFilesTimestamp}"
			_log_message DEBUG "$message"
			
			_call rm "${domainPath}/input/processed/*${bkpFilesTimestamp}*"
			_verify_binary
			
			
			# Restore the files that have been originally loaded, but that has records out of format.
			message="Restore loaded input data files, with records formatting issues, back to the input folder for "
			message+="easier user debug."
			_log_message INFORMATION $message
			_call f_restoreProcessedFilesTimestamp $domainPath/input/processed $domainPath/input $orgFilesTimestamp
			_verify_script
			
			_clean_exit 1 "Aborting."
		
		# If all input data files records were loaded successfully, then re-load the hierarchy with the 
		# -purgeAge option at 0, in order to delete from RPO all positions that are no longer in the file.
		else
			_log_message INFORMATION "The execution of the test loadHier was a success."
			
			# Restore the files that have been originally loaded to reload them with purgeAge 0, since load was ok
			message="Restore previously loaded files in the test execution of loadHier, to reload them again"
			_log_message INFORMATION "$message"
			_call f_restoreProcessedFilesTimestamp $domainPath/input/processed $domainPath/input $orgFilesTimestamp
			_verify_script
			
			# Remove previously stored backup hierarchy data file
			rm $backupHierFilePath
			_verify_binary
			
			# Final loadHier execution with purgeAge as defined in the Control File.
			message="Re-execute the loadHier command, with purgeAge $purgeAge, "
			message+="according to the control file setup."
			_log_message INFORMATION "$message"
			_call loadHier -d "$domainPath" -load $1 -processes $maxProcess -loglevel $logLevel \
				-purgeAge $purgeAge -defaultDomain $defaultDomainPath
			_verify_binary
		fi
	fi
	
	_log_message INFORMATION "Finish loading RPO $mandatoryOptional Hierarchy: ${1}"
	
	_log_message DEBUG "f_loadHierarchy: End"
}

###############################################################################
#  MAIN CODE
###############################################################################

# get optional inputs
while getopts ":d:g:p:l:z" Option
do
	case $Option in
	d ) domainPath="${OPTARG}" ;;
	g ) defaultDomainPath="${OPTARG}" ;;
	p ) maxProcess="${OPTARG}" ;;
	l ) logLevel="${OPTARG}" ;;
	z ) usePurgeAgeZero=true ;;

	* )
		f_Usage
		_clean_exit NONZERO_EXIT ;;
	esac
done

while getopts ":z:"
do
	case $Option in
	d ) domainPath="${OPTARG}" ;;
	g ) defaultDomainPath="${OPTARG}" ;;
	p ) maxProcess="${OPTARG}" ;;
	l ) logLevel="${OPTARG}" ;;
	z ) usePurgeAgeZero=true ;;

	* )
		f_Usage
		_clean_exit NONZERO_EXIT ;;
	esac
done

shift $(($OPTIND - 1))


# Check the Foundations control file for the number of hierarchies to be loaded.
list_cnt=$(cat "${ctrlFile}" | grep -v "^[[:blank:]]*#" | tr -s ' ' | wc -l |  tr -s ' ' | cut -d" " -f2)
if [[ "${list_cnt}" -eq "0" ]]; then
	message="The Foundations load control file does not specify any hierarchy to be loaded.\n"
	message+="\t Please check the control file: ${ctrlFile}\n"
	_log_message ERROR $message
	_clean_exit 1 "Aborting."
fi
_log_message INFORMATION "The number of hierarchies to be loaded in RPO are: ${list_cnt}"

# Get the list of hierarchies to be loaded from the Foundations Control File
rows_to_load=$(cat "${ctrlFile}" | tr -s ' ' | grep -v "^#")
# Converting the control file strings into array
set -A arr_rows_to_load `echo ${rows_to_load}|tr '@' '\n'`

_log_message INFORMATION "Validating Control File Hierarchy setup."

# Validate each Control File hierarchy setup before loading.
for hier_row in ${arr_rows_to_load[*]}
do
	_log_message DEBUG "Validating Control File line: ${hier_row}"
	
	# Read the 3 Hierarchy parameters
	hier=$(echo "${hier_row}" | cut -d "|" -f1)
	opt=$(echo "${hier_row}" | cut -d "|" -f2)
	purgeAge=$(echo "${hier_row}" | cut -d "|" -f3)
	
	# Validate hierarchy ID
	hierVld='^[a-zA-Z0-9][a-zA-Z0-9]?[a-zA-Z0-9]?[a-zA-Z0-9]?$'
	if ! [[ $hier =~ $hierVld  ]] then
		message="Hierarchy ID not valid: ${hier}\n"
		message+="\tHierarchy ID should have between 1 and 4 chars.\n"
		_log_message ERROR "$message"
		_clean_exit 1 "Aborting."
	fi
	
	# Validate Optional Flag
	optVld='^[0-1]$'
	if ! [[ $opt =~ $optVld ]] ; then
		message="Optional Flag not valid: ${opt}\n"
		message+="\tOptional Flag should be 0 or 1.\n"
		_log_message ERROR "$message"
		_clean_exit 1 "Aborting."
	fi
	
	# Validate Purge Age
	purgeAgeVld='^(0|[1-9][0-9]*)$'
	if ! [[ $purgeAge =~ $purgeAgeVld ]] ; then
		message="Purge Age not valid: ${purgeAge}\n"
		message+="\tPurge Age should be a non-negative integer (0+).\n"
		_log_message ERROR "$message"
		_clean_exit 1 "Aborting."
	fi
	
	_log_message DEBUG "Valid Control File line: ${hier_row}"
done

_log_message INFORMATION "Control File Hierarchy setup is correct."

_log_message INFORMATION "Loading RPO Hierarchies."

# Process each hierarchy in the Control File
for hier_row in ${arr_rows_to_load[*]}
do

	# Read the 3 Hierarchy parameters
	hier=$(echo "${hier_row}" | cut -d "|" -f1)
	opt=$(echo "${hier_row}" | cut -d "|" -f2)
	
	if $usePurgeAgeZero ; then
		purgeAge=0
	else
		purgeAge=$(echo "${hier_row}" | cut -d "|" -f3)
	fi
	
	_log_message DEBUG "Call f_loadHierarchy: $hier $opt $purgeAge"
	f_loadHierarchy $hier $opt $purgeAge
	_verify_script
	
done

_log_message INFORMATION "Finished loading RPO Hierarchies."

_clean_exit SUCCESS

