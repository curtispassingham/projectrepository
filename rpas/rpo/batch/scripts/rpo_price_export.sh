#!/bin/ksh


#
# Copyright ? 2007, 2008, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 718995 $
# $Date: 2013-09-16 09:03:47 -0500 (Mon, 16 Sep 2013) $
# $Author: ming.lei@oracle.com $
#
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"
scriptName=$(basename "$0")

########

function f_validate
{
   f_printscalar needDynAgg|f_search_for_true > .validate_rollup.tmp
   [[ -s .validate_rollup.tmp ]]
}

function f_Usage
{
  _log_message ERROR "Usage: $scriptName -d {domainpath}"
  _log_message ERROR "   -d  [Default {domainpath} is '$PWD/../domain']"
}

domainPath="."
processes=1

while getopts :d:p: Option
do
  case $Option in

      d  ) domainPath="${OPTARG}"     ;;
	  p  ) processes="${OPTARG}"     ;;

      *  ) f_Usage
          _clean_exit NONZERO_EXIT
          ;;
  esac
done

#shift $(($OPTIND - 1))

_call cd "$domainPath"
_verify_binary

_call mace -d . -run -expression "itstrprznmap=pzdynhrmp" -processes "$processes"
_verify_binary

_call mace -d . -run -expression "needDynAgg=if(itstrprznmap.popcount>0,true,false)" -processes "$processes"
_verify_binary
	
_call regmeasure -d . -add itstrpr -type real -baseint str_itemweek -defagg total -filename  itstrpr -db data/output
_verify_binary

if f_validate; then
	_call mace -d . -run -expression "itstrpr <- DynamicAssignment(FAppItPc,itstrprznmap)" -processes "$processes"
    _verify_binary
else
    _call mace -d . -run -expression "itstrpr =FAppItPc" -processes "$processes"
    _verify_binary
fi

_call rm -f .validate_rollup.tmp

# _call mace -d . -run -expression "itstrpr <- DynamicAssignment(FAppItPc,itstrprznmap)"

_call exportMeasure -d . -meas itstrpr -intx str_itemweek -out ./output/itstrpr.csv -processes "$processes"
_verify_binary

_call regmeasure -d . -remove itstrpr
_verify_binary

_verify_log
_log_message INFORMATION "Export Price Batch Complete!"
_clean_exit SUCCESS


