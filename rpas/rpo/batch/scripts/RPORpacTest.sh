#!/bin/ksh
#
# Copyright ? 2007-2011 Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 674977 $
# $Date: 2013-07-08 14:29:31 -0400 (Mon, 08 Jul 2013) $
# $Author: ming.lei@oracle.com $
#
##############################################################################
# This script is used to create a simple domain for RegPrice.
# This runs rpasInstall, dimensionMgr and loadmeasure to first install the domain,
# 	reshape arrays to have a minimum buffer size and then loads all measures
#	necessary for minimal functionality
# This script may be modified to suit particular needs of a customer implementation.
##############################################################################
. "$RPO_HOME/scripts/rpo_environment.ksh"
scriptName=$(basename "$0")



_call cd ..
_verify_binary

rpacXmlDir="$RPO_HOME/test"
logDir="$RPO_HOME/test"
domainPath="$RPO_HOME/domain/RegPrice/ldom0"


function f_Usage
{
  _log_message ERROR "Usage: $scriptName options (-dlx)"
  _log_message ERROR "  d: domain path [default = $RPO_HOME/domain]"
  _log_message ERROR "  l: log dir [default = $RPO_HOME/test/ ]"
  _log_message ERROR "  x: RPAC test script dir [default = $RPO_HOME/test/ ] "

}

while getopts ":d:l:x" Option
do
  case $Option in

    l    ) logDir="${OPTARG}"         ;;
    d    ) domainPath="${OPTARG}"     ;;
    x    ) rpacXmlDir="${OPTARG}"          ;;


    * )
       f_Usage
       _clean_exit NONZERO_EXIT
       ;;
  esac
done

shift $(($OPTIND - 1))



_call cd  "$domainPath/.."
_verify_binary


_call usermgr -d . -addGroup RPACGROUP -label RPACGROUP 
_verify_binary

_call usermgr -d . -add rpactest -label rpactest -group RPACGROUP -admin -noPassword
_verify_binary

_call cd  "$domainPath"
_verify_binary

_call mace -d . -run -group common_data
_verify_binary

_call mace -d . -run -group post_common_data
_verify_binary

_call cd  "$rpacXmlDir"
_verify_binary

_log_message INFORMATION "Regular Price Optimization RPAC test"

_log_message INFORMATION "  Log Dir: $logDir"
_log_message INFORMATION "  Domain Path: $domainPath"
_log_message INFORMATION "  RPAC xml script: $rpacXmlDir"



 rpac -d "${domainPath}" -title "RPOAdmin"  		-input RPOAdmin.xml     >   "${logDir}/rpactest.log"
 rpac -d "${domainPath}" -title "ScenarioManagement"  	-input ScenarioManagement.xml   >>  "${logDir}/rpactest.log"
 rpac -d "${domainPath}" -title "ItemManagement"  		-input ItemManagement.xml      >> "${logDir}/rpactest.log"
 rpac -d "${domainPath}" -title "PostPriceAnalysis"  	-input PostPriceAnalysis.xml    >>  "${logDir}/rpactest.log"
 rpac -d "${domainPath}" -title "PriceAnalysisSimple"  	-input PriceAnalysisSimple.xml   >>  "${logDir}/rpactest.log"
 rpac -d "${domainPath}" -title "PriceAnalysisMedium"  	-input PriceAnalysisMedium.xml  >>  "${logDir}/rpactest.log"
 rpac -d "${domainPath}" -title "PriceAnalysisHigh"  	 -input PriceAnalysisHigh.xml   >>  "${logDir}/rpactest.log"
 rpac -d "${domainPath}" -title "PriceAnalysisApproveandCopy"   -input PriceAnalysisApproveandCopy.xml  >>  "${logDir}/rpactest.log"

 grep seconds "${logDir}/rpactest.log" > "${logDir}/rpactestresult.log"

awk '
    BEGIN {
        tests = 0;
        errors = 0;
        failures = 0;
    }
                {
                                tests += ($2 + 0);
                                errors += ($4 + 0);
                                failures += ($6 + 0);
                }
    END {
        print "total tests run = " tests ", total errors = " errors ", total failures = " failures;
    }
' "${logDir}/rpactestresult.log" # > parseRpasTestResults.txt



_log_message INFORMATION "Test Complete Complete."
_clean_exit SUCCESS

