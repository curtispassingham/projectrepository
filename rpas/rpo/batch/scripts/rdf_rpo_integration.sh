#!/bin/ksh


#
# Copyright ? 2007, 2008, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 669194 $
# $Date: 2013-07-01 08:57:49 -0500 (Mon, 01 Jul 2013) $
# $Author: ming.lei@oracle.com $
#
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"
scriptName=$(basename "$0")
processes=1
########

function f_validate
{
   f_printscalar needDynAgg|f_search_for_true > .validate_rollup.tmp
   [[ -s .validate_rollup.tmp ]]
}

function f_Usage
{
  _log_message ERROR " Usage: $scriptName -d {domainpath}"
	 _log_message ERROR "[Default {domainpath} is '$PWD/../domain']"
}

domainPath="."

while getopts :d:p: Option
do
  case $Option in
    d    ) domainPath="${OPTARG}"     ;;
	p    ) processes="${OPTARG}"        ;;

      *  ) f_Usage
           _clean_exit NONZERO_EXIT
           ;;
  esac
done

shift $(($OPTIND - 1))

_call cd "$domainPath"
          _verify_binary

_call regmeasure -d . -add bdspitemstor -type real -baseint str_itemweek -defagg total -filename  bdspitemstor -db data/input
          _verify_binary
_call loadmeasure -d . -measure bdspitemstor -processes "$processes"
          _verify_binary
_call mace -d . -run -expression "itstrprznmap=pzdynhrmp" -processes "$processes"
          _verify_binary
_call mace -d . -run -expression "needDynAgg=if(itstrprznmap.popcount>0,true,false)" -processes "$processes"
          _verify_binary

if f_validate; then
	_call rm -f .validate_rollup.tmp
	_call mace -d . -run -expression "Dl1ItBdsp <- DynamicAssignment(bdspitemstor,itstrprznmap)" -processes "$processes"
    _verify_binary
else
	_call rm -f .validate_rollup.tmp
    _call mace -d . -run -expression "Dl1ItBdsp =bdspitemstor" -processes "$processes"
    _verify_binary
fi


#_call mace -d . -run -expression "Dl1ItBdsp <- DynamicAssignment(bdspitemstor,itstrprznmap)"

_call regmeasure -d . -remove bdspitemstor
_verify_binary

_verify_log
_clean_exit SUCCESS

