#!/bin/ksh


#
# Copyright ? 2007, 2008, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 1005616 $
# $Date: 2014-10-27 10:06:17 -0500 (Mon, 27 Oct 2014) $
# $Author: ming.lei@oracle.com $
#
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"

domainPath="."

while getopts :d: Option
do
  case $Option in
    d    ) domainPath="${OPTARG}"     ;;

    *    )
       _log_message ERROR "Usage: `basename $0` options (-bcdilo) [flags (-tb)]"
       _log_message ERROR "  d: domain path [default = $PWD/../domain]"
       _clean_exit NONZERO_EXIT ;;
  esac
done

shift $(($OPTIND - 1))

_call cd "$domainPath"
_verify_binary

_call loadmeasure -d . -measure ol1anchprc -loglevel debug
        _verify_binary
_call loadmeasure -d . -measure ol1gammasp -loglevel debug
        _verify_binary
_call loadmeasure -d . -measure ol1hishiprc -loglevel debug
        _verify_binary
_call loadmeasure -d . -measure ol1hisloprc -loglevel debug
        _verify_binary
_call loadmeasure -d . -measure StdErr -loglevel debug
        _verify_binary
_call loadmeasure -d . -measure TStat -loglevel debug
        _verify_binary
_call loadmeasure -d . -measure Ol1MxDmPrct -loglevel debug
        _verify_binary
_call loadmeasure -d . -measure Ol1MxUpPrct -loglevel debug
        _verify_binary

_verify_log
_clean_exit SUCCESS

