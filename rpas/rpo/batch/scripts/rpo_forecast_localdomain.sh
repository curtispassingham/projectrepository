#!/bin/ksh

#
# Copyright (c) 1999, 2011, Oracle and/or its affiliates. All rights reserved. 
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 1005616 $
#

# ------------------------------------------------------------------------

. "$RPO_HOME/scripts/rpo_environment.ksh"

########

function f_validate
{
   f_printscalar needDynAgg|f_search_for_true > .validate_rollup.tmp
   [[ -s .validate_rollup.tmp ]]
}


_call mace -d . -run -group "Preprocess_ls"
            _verify_binary
_call mace -d . -run -group "PreProcess_promo"
            _verify_binary
_call mace -d . -run -group "Forecast_flv"
            _verify_binary
_call mace -d . -run -group "Forecast_src"
            _verify_binary
_call mace -d . -run -expression "itstrprznmap=pzdynhrmp"
            _verify_binary
_call mace -d . -run -expression "needDynAgg=if(itstrprznmap.popcount>0,true,false)"
            _verify_binary


if f_validate; then
	_call rm -f .validate_rollup.tmp
          _verify_binary
	_call mace -d . -run -expression "Dl1ItBdsp <- DynamicAssignment(forecastF,itstrprznmap)"
          _verify_binary
else
	_call rm -f .validate_rollup.tmp
          _verify_binary
    _call mace -d . -run -expression "Dl1ItBdsp =forecastF"
          _verify_binary
fi

_call mace -d . -run -expression "LsAdjSls=0.0"
          _verify_binary
_call mace -d . -run -expression "LsSls=0.0"
          _verify_binary
_call mace -d . -run -expression "profile=0.0"
          _verify_binary
_call mace -d . -run -expression "PromoAdj=0.0"
          _verify_binary
_call mace -d . -run -expression "PromoAdjSls=0.0"
          _verify_binary
_call mace -d . -run -expression "totSlsAdjF=0.0"
          _verify_binary
_call mace -d . -run -expression "tmpLsSls=0.0"
          _verify_binary
_call mace -d . -run -expression "tmpLsPromo=0.0"
          _verify_binary

_verify_log
_clean_exit SUCCESS

