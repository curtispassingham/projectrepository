#!/bin/ksh
#
# Copyright ? 2007-2011 Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 1326505 $
# $Date: 2018-08-24 13:44:34 -0700 (Fri, 24 Aug 2018) $
# $Author: mlei $
#
##############################################################################
# This script is used to create a simple domain for RegPrice.
# This runs rpasInstall, dimensionMgr and loadmeasure to first install the domain,
# 	reshape arrays to have a minimum buffer size and then loads all measures
#	necessary for minimal functionality
# This script may be modified to suit particular needs of a customer implementation.
##############################################################################
. "$RPO_HOME/scripts/rpo_environment.ksh"
scriptName=$(basename "$0")

typeInstall="-fullinstall"

_call cd ..
_verify_binary

configDir="$PWD/config"
inputDir="$PWD/input"
logDir="$PWD"
domainPath="$PWD/domain"
CmdOL=3
CmdDL=3
libPrefix=""

function f_Usage
{
  _log_message ERROR "Usage: $scriptName options (-bcdilo) [flags (-tb)]"
  _log_message ERROR "  d: domain path [default = $PWD/../domain]"
  _log_message ERROR "  c: configDir [default = $PWD/../config]"
  _log_message ERROR "  i: input dir [default = $PWD/../input]"
  _log_message ERROR "  l: log dir [default = $PWD/../ ]"
  _log_message ERROR "  o: Optimization Level [default: 3; Valid Values = 1-3]"
  _log_message ERROR "  b: Demand Level [default: 3; Valid Values = 1-3]"
  _log_message ERROR "  t: make test build (no argument)"
  _log_message ERROR "  p: make patch build (no argument)"
  _log_message ERROR "  g: use debug function libs (no argument)"
}

while getopts ":b:c:d:i:l:o:tgp" Option
do
  case $Option in
    c    ) configDir="${OPTARG}"      ;;
    i    ) inputDir="${OPTARG}"       ;;
    l    ) logDir="${OPTARG}"         ;;
    d    ) domainPath="${OPTARG}"     ;;
    o    ) CmdOL="${OPTARG}"          ;;
    b    ) CmdDL="${OPTARG}"          ;;
    t    ) typeInstall="-testinstall" ;;
	p    ) typeInstall="-patchinstall" ;;
    g    ) libPrefix="d"              ;;

    * )
       f_Usage
       _clean_exit NONZERO_EXIT
       ;;
  esac
done

shift $(($OPTIND - 1))

# perform sanity checks on Optimization and Demand levels
if [ $CmdOL -lt 1 -o $CmdOL -gt 3 ]
then
	_log_message ERROR "ERROR: Invalid Optimization Level: $CmdOL.  Aborting Domain Build"
	_clean_exit NONZERO_EXIT
fi

if [ $CmdDL -lt 1 -o $CmdDL -gt 3 ]
then
	_log_message ERROR "Invalid Demand Level: $CmdDL.  Aborting Domain Build"
	_clean_exit NONZERO_EXIT
fi

_call mkdir -p "$domainPath"
_verify_binary

_log_message INFORMATION "Building domain at $domainPath with -"
_log_message INFORMATION "  Config Dir: $configDir"
_log_message INFORMATION "  Input Dir: $inputDir"
_log_message INFORMATION "  Log Dir: $logDir"
_log_message INFORMATION "  Install Type: $typeInstall"
_log_message INFORMATION "  Optimization Level: $CmdOL"
_log_message INFORMATION "  Demand Level: $CmdDL"
_log_message INFORMATION "  Lib Prefix: $libPrefix"

_call rpasInstall $typeInstall -ch "$configDir" -cn RegPrice -in "$inputDir" -log "${logDir}/build.log" -dh "$domainPath" -updatestyles -rf "${libPrefix}RegPriceExpressions" -rf "${libPrefix}RdfFunctions" -rf "${libPrefix}AppFunctions" -rf "${libPrefix}LostSaleFunctions" -rf "Transform" -p dept -skipvalidation
_verify_script


_log_message INFORMATION "Looking for problems in $logDir/build.log"
grep -i -e error -e fail -e excep "${logDir}/build.log" >> $log_path
_verify_log

# if test install, then exit here
if [[ "$typeInstall" = "-testinstall" ]] ; then
  _log_message INFORMATION "Test install complete. Exiting..."
  _clean_exit SUCCESS
fi

_call cd scripts
_verify_binary

# Load measures
_log_message INFORMATION "Loading measures"

case "$(uname)" in
   wind* | Wind* ) delimiter=';' ;;
         *       ) delimiter=':' ;;
esac

# Add current directory to path.
PATH=".${delimiter}$PATH"

_call loadRegPriceMeasures.sh -d "${domainPath}"/RegPrice -o $CmdOL -b $CmdDL
_verify_binary

_log_message INFORMATION "Domain location: '$domainPath'"
_log_message INFORMATION "Logs locations: '$logDir'"

_verify_log
_log_message INFORMATION "Build Complete."
_clean_exit SUCCESS

