#!/bin/ksh


#
# Copyright ? 2007, 2008, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 719003 $
# $Date: 2013-09-16 09:28:19 -0500 (Mon, 16 Sep 2013) $
# $Author: ming.lei@oracle.com $
#
##############################################################################
# This script is used to create a simple domain for RegPrice.
# This runs rpasInstall, dimensionMgr and loadmeasure to first install the domain,
# 	reshape arrays to have a minimum buffer size and then loads all measures
#	necessary for minimal functionality
# This script may be modified to suit particular needs of a customer implementation.
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"

domainPath="."

while getopts :d: Option
do
  case $Option in
    d    ) domainPath="${OPTARG}"     ;;

    *    )
    _log_message ERROR "Usage: `basename $0` options (-bcdilo) [flags (-tb)]"
    _log_message ERROR "      d: domain path [default = $PWD/../domain]"

    _clean_exit NONZERO_EXIT ;;
  esac
done

shift $(($OPTIND - 1))

#_call cd "$domainPath"
#_verify_binary

#
# Capture local domains to log file before saving to $localDomains variable.
#
_log_message INFORMATION "List of subdomains:"
_call domaininfo -d $domainPath -shellsubdomains -loglevel error
_verify_binary

# Get the list of local domains.
ldomList=$( domaininfo -d $domainPath -shellsubdomains -loglevel none )

_verify_binary "Getting domain paths for domain '$PWD'."


   for _ldom in ${ldomList}; do
	  
	  _call $RPO_HOME/scripts/rpo_batch_localdomain.sh -d ${_ldom}
      _verify_script

   done


_verify_log
_log_message INFORMATION "Price Optimization Batch Complete."
_clean_exit SUCCESS

