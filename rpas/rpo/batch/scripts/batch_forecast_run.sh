#!/bin/ksh


#
# Copyright (c) 2007, 2016, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 1198206 $
# $Date: 2016-08-12 10:34:01 -0500 (Fri, 12 Aug 2016) $
# $Author: bassaf $
#
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"

domainPath="."

while getopts :d: Option
do
  case $Option in
    d    ) domainPath="${OPTARG}"     ;;
    *    )
          _log_message ERROR "Usage: `basename $0` options (-bcdilo) [flags (-tb)]"
          _log_message ERROR "   d: domain path [default = $PWD/../domain]"
          _clean_exit NONZERO_EXIT ;;
  esac
done

shift $(($OPTIND - 1))

_call cd "$domainPath"
_verify_binary

#
# Capture local domains to log file before saving to $localDomains variable.
#
_log_message INFORMATION "List of subdomains:"
_call domaininfo -d . -shellsubdomains -loglevel error
_verify_binary

# Get the list of local domains.
ldomList=$( domaininfo -d . -shellsubdomains -loglevel none 2>> $log_path )
_verify_binary "Getting domain paths for domain '$PWD'."


   for _ldom in ${ldomList}; do
	  _call cd "${_ldom}"
          _verify_binary

	  _para_spawn rpo_forecast_localdomain.sh -d "${_ldom}"

	  _call cd "$OLDPWD"  # Restore current directory.
          _verify_binary
   done

_para_wait      # Wait for background processes.
_verify_script

_verify_log
_log_message INFORMATION "Price Optimization Batch Complete."
_clean_exit SUCCESS

