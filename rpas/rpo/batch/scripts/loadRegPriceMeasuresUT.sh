#!/bin/ksh

#
# Copyright ? 2007, 2008, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 603801 $
# $Date: 2013-02-27 18:00:45 -0600 (Wed, 27 Feb 2013) $
# $Author: daniel.ortmann@oracle.com $
#
##############################################################################
# This script is used to load the required measures for the RegPrice solution.  This script
#	may be modified to, 
#		1) account for the use for MNL demand model
#		2) Adding removing measure loads for different optimization and demand levels
#		3) Loading any custom attributes
#		4) Loading any other custom measures
#
# For proper functioning, RegPrice needs a minimal set of measures.  These measures include:
#	1) Those that define Location hierarchy dynamic hierachy (str -> przn)
#	2) Demand parameters (APE outputs in the GA solution)
#	3) UOM and EquiUOM attributes for the items
#	4) At least one Demand-Group definition
#	5) At least one price ladder definition
#	6) At least one item link group definition
#	7) Competition prices and competition item linkages to retailer items
#
# RegPrice = Regular Price Optimization
# MNL = MultiNomial Logit
##############################################################################

scriptName=$(basename "$0")
. rpo_environment.ksh

domainPath="$PWD/../domain/UT/RegPrice"
CmdOL=3
CmdDL=3

function f_Usage
{
  _log_message ERROR "Usage: $scriptName options (-dob)"
  _log_message ERROR "  d: domain path [default = $PWD/../domain]"
  _log_message ERROR "  o: Optimization Level [default = 3; Valid Values = 1-3]"
  _log_message ERROR "  b: Demand Level [default = 3; Valid Values = 1-3]"
}

# get optional inputs
while getopts :d:o:b: Option
do
  case $Option in
    d    ) domainPath="${OPTARG}"       ;;
    o    ) CmdOL="${OPTARG}"       ;;
    b    ) CmdDL="${OPTARG}"       ;;

       * )
	       f_Usage
           _clean_exit NONZERO_EXIT
           ;;
  esac
done

shift $(($OPTIND - 1))

# perform sanity checks on Optimization and Demand levels
if [ $CmdOL -lt 1 -o $CmdOL -gt 3 ]
then
	_log_message ERROR "Invalid Optimization Level: $CmdOL.  Aborting Load"
	_clean_exit 1 "Aborting."
fi
if [ $CmdDL -lt 1 -o $CmdDL -gt 3 ]
then
	_log_message ERROR "Invalid Demand Level: $CmdDL.  Aborting Load"
	_clean_exit 1 "Aborting."
fi

# Start loading measures
# Dynamic Hierarchy

_log_message INFORMATION "Loading str->przn Dynamic Hierarchy Definitions"

_call loadmeasure -d "$domainPath" -measure PzDynHrMp -loglevel debug
          _verify_binary
_call loadmeasure -d "$domainPath" -measure PzDynHrLbl -loglevel debug
          _verify_binary

_log_message INFORMATION "Finished loading str->przn Dynamic Hierarchy Definitions"

# Demand Parameters
optLvl=1
dmdLvl=1

_log_message INFORMATION "Loading ScanPro demand model parameters"

# Optimization level parameters
while [ $optLvl -le $CmdOL ]
do
	_log_message INFORMATION "Loading Optimization Level $optLvl parameters"

	for measName in OrgItPc ItAncPc ItAncPcB GammaSP
	do
	  _call loadmeasure -d "$domainPath" -measure Ol$optLvl$measName -loglevel debug
      _verify_binary
	done

	optLvl=`expr $optLvl + 1`
done

_log_message INFORMATION "Finished loading optimization level parameters"

# Demand level parameters
while [ $dmdLvl -le $CmdDL ]
do

	_log_message INFORMATION "Loading Demand Level $dmdLvl parameters"

	for measName in ItBDSP OrgItCst
	do
	  _call loadmeasure -d "$domainPath" -measure Dl$dmdLvl$measName -loglevel debug
      _verify_binary
	done

	dmdLvl=`expr $dmdLvl + 1`
done

_log_message INFORMATION "Finished loading demand level parameters"
_log_message INFORMATION "Finished loading ScanPro demand model parameters"

#  Attributes
_log_message INFORMATION "Loading Price Family, UOM and Equivalent UOM Attributes"

optLvl=1
while [ $optLvl -le $CmdOL ]
do
	_log_message INFORMATION "Loading Optimization Level $optLvl Attributes"

	for measName in ItUOM ItEquiU PcFml
	do
	   _call loadmeasure -d "$domainPath" -measure Ol$optLvl$measName  -loglevel debug
       _verify_binary
	done

	optLvl=`expr $optLvl + 1`
done

_log_message INFORMATION "Finished UOM and Equivalent UOM Attributes"

# Demand Group Management

_log_message INFORMATION "Loading Demand Group management meeasures"

_call loadmeasure -d "$domainPath" -measure DgDesc  -loglevel debug
          _verify_binary
_call loadmeasure -d "$domainPath" -measure DgItMbr  -loglevel debug
          _verify_binary
_call loadmeasure -d "$domainPath" -measure DmndLvl  -loglevel debug
          _verify_binary
_call loadmeasure -d "$domainPath" -measure OptLvl  -loglevel debug
          _verify_binary

_log_message INFORMATION "Finished loading Demand Group management meeasures"

# Price Ladders

_log_message INFORMATION "Loading price ladders"

_call loadmeasure -d "$domainPath" -measure PldrLwrBnd -loglevel debug
          _verify_binary
_call loadmeasure -d "$domainPath" -measure PldrUprBnd -loglevel debug
          _verify_binary
_call loadmeasure -d "$domainPath" -measure PldrStp -loglevel debug
          _verify_binary
_call loadmeasure -d "$domainPath" -measure PldrPpt -loglevel debug
          _verify_binary

_log_message INFORMATION "Loading Price-ladder assignments"

_call regmeasure -d "$domainPath" -add pldrAssnmtTmp -type string -label TempPriceLadderLoad -baseint scatprzn -db data/priceladder -defspread repl
          _verify_binary
_call loadmeasure -d "$domainPath" -measure pldrAssnmtTmp -loglevel debug
          _verify_binary

optLvl=1
while [ $optLvl -le $CmdOL ]
do
	for measName in ItPcLdr
	do
	  _call mace -d "$domainPath" -run -expression "Ol$optLvl$measName = pldrAssnmtTmp" -loglevel debug
      _verify_binary
	done

	optLvl=`expr $optLvl + 1`
done

_call regmeasure -d "$domainPath" -remove pldrAssnmtTmp
_verify_binary

_log_message INFORMATION "Finished loading price ladders"

# Item link groups

_log_message INFORMATION "Loading Item-Link-Groups"
optLvl=1
while [ $optLvl -le $CmdOL ]
do
	_log_message INFORMATION "Load Optimization Level $optLvl Item-Link-Groups"

	for measName in IlgItItrMp
	do
	  _call loadmeasure -d "$domainPath" -measure Ol$optLvl$measName  -loglevel debug
      _verify_binary
	done
	optLvl=`expr $optLvl + 1`
done

_log_message INFORMATION "Finished loading Item-Link-Groups"

# Competitive Data

_log_message INFORMATION "Loading Competitive Data"

optLvl=1
while [ $optLvl -le $CmdOL ]
do
	_log_message INFORMATION "Load Optimization Level $optLvl Competitive Data"

	for measName in IlgComp CmpCurPc CmpPcChkDt CmpPcTp
	do
	  _call loadmeasure -d "$domainPath" -measure Ol$optLvl$measName  -loglevel debug
      _verify_binary
	done
	optLvl=`expr $optLvl + 1`
done

_verify_log
_log_message INFORMATION "Finished loading Competitive Data"
_clean_exit SUCCESS

