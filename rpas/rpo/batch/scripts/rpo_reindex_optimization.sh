#!/bin/ksh
#
# $Date: 2018-03-20 $
#
##############################################################################
#
# This script will optimise and reindex the entire RPO Domain 
# when needed and provide analysis of the hierarchies of the domain
#
##############################################################################

. "$RPO_HOME/scripts/rpo_environment.ksh"

domainPath="$PWD/../domain/RegPrice"
maxProcess="${BSA_MAX_PARALLEL:-1}"
logLevel="${BSA_LOG_LEVEL:-INFORMATION}"

scriptName=$(basename "$0")

typeset -i NUM_PARAMS=$#

###############################################################################
#  FUNCTIONS
###############################################################################

function f_Usage
{
	_log_message ERROR "Usage: $scriptName options (-dpl)"
	_log_message ERROR "    d: domain path [DEFAULT = ${domainPath}"
	_log_message ERROR "    p: max processes [DEFAULT = ${maxProcess}]"
	_log_message ERROR "    l: log level [DEFAULT = ${logLevel}]"
}

###############################################################################
#  MAIN CODE
###############################################################################

# get optional inputs
while getopts ":d:p:l:" Option
do
	case $Option in
	d ) domainPath="${OPTARG}" ;;
	p ) maxProcess="${OPTARG}" ;;
	l ) logLevel="${OPTARG}" ;;

	* )
		f_Usage
		_clean_exit NONZERO_EXIT ;;
	esac
done

shift $(($OPTIND - 1))


# 1. Optimize domain
_log_message INFORMATION "Optimizing the domain ${domainPath}"
_call optimizeDomain -d ${domainPath} -processes $maxProcess -loglevel $logLevel
_verify_binary

# 2. Generate the reindex report first
_log_message INFORMATION "Generating the manage_Hierarchy report for prod, clnd and loc hierarchy in ${domainPath}"
_call reindexDomain -d $domainPath -analyze -hier prod
_verify_binary
_call reindexDomain -d $domainPath -analyze -hier loc
_verify_binary
_call reindexDomain -d $domainPath -analyze -hier clnd
_verify_binary

# 3. Reindex Entire Domain as Needed
_log_message INFORMATION "Reindexing ${domainPath}"
_call reindexDomain -d ${domainPath} -processes $maxProcess -loglevel $logLevel
_verify_binary

# Verify and exit
_verify_log
_log_message INFORMATION "Finished running ${scriptName}"
_clean_exit SUCCESS
