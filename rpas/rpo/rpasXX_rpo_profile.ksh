#!/bin/ksh
#------------------------------------------------------
# Copyright (c) 2016, Oracle. All rights reserved.
#
# Script Name : rpasXX_rpo_profile.ksh
#
# Arguments   : None
#
# Description : Initialise the envronment variables
#
#
# Version  Name              Date        Change/Description
# -------- ----------------- ---------   ----------------------
# 0.1      ORC                18/04/2017  Initial version
#
#--------------------------------------------------------------

PROGRAM_NAME="rpasXX_rpo_profile"
LOG_EXT=`date +"%b_%d_%Y_%H%M"`.log
DAY_STAMP=`date +"%Y%m%d%H%M"`

##
# Environment Name
##
export ENV_CONFIGNAME=RPO

##
# Home folder for the RPO installation.
# The main variable to set is O_APPLICATION_HOME
##
export O_APPLICATION="rpo"
O_APPLICATION_HOME=/u01/data/rpasXX
O_INSTANCE=dev
export O_APP_INSTANCE=${O_APPLICATION_HOME}/rpo
export BATCH_HOME=${O_APP_INSTANCE}/batch
export RPO_HOME=${O_APP_INSTANCE}
export RPO_RETAIL_HOME=${RPO_HOME}

##
# RPO DomainDaemon (RpasServer) port.
# Should be a unique port between all the DomainDaemons running on this server
#  and should be an available port.
# This port is also configured in the FusionClient setup for it to be able to
#  access the RpasServer of the solution
##
export RPO_PORT=7015

##
# RETL Setup
##
export RFX_HOME=/u01/product/retlXX
export RFX_TMP=$RFX_HOME/tmp
export LC_ALL=C

##
# Custom RPO RETL Integration Scripts folder
##
export RETLforXXADEO=/u01/data/rpasXX/rpo/batch/RETLforXXADEO

##
# RPO RPAS Domain path
##
export DOMAIN_HOME=$O_APP_INSTANCE/domain
export GLOBAL_DOMAIN=${DOMAIN_HOME}/RegPrice

##
# Backup Dir
##
export BACKUP_DIR=${O_APP_INSTANCE}/backup

##
# Integration Folders
##
export INTEGRATION_HOME=${O_APP_INSTANCE}/integration
export RETL_IN=$INTEGRATION_HOME/inbound
export RETL_OUT=$INTEGRATION_HOME/outbound
export EXTERNAL_INBOUND_DIR=$RETL_IN/RPO
export EXTERNAL_OUTBOUND_DIR=$RETL_OUT/RPO

##
# RPAS Server Installation path
##
APP_PATH=/u01/product/rpasXX/rpo

##
# BSA path for RPAS scripts
##
BSA_PATH=$APP_PATH/rpas/bin

##
# Set RPAS Log Level
##
if [ "${RPAS_LOG_LEVEL}" = "" ]; then
  export RPAS_LOG_LEVEL="INFORMATION"
fi


OLD_PWD=`pwd`

# cd and execute retaillogin.sh to set RPAS_HOME.
if [[ -d $APP_PATH ]];then
        . $APP_PATH/retaillogin.ksh
else
        echo "$PROGRAM_NAME Terminated with Error: $APP_PATH does not exist"
        return 1
fi

# cd and then set the RPO_HOME variable. The CD PATH has to be changed for different environments.
printf "CheckPoint#1 - Checking for variable $RPO_HOME \n"
if [[ -d $RPO_HOME ]];then

        echo "RPO_HOME exists, Checkpoint#1 Passed...."
else
        echo "$PROGRAM_NAME Terminated with Error: $RPO_HOME does not exist"
        return 1
fi
printf "\n"

# Initialise the BSA Path
if [[ -d ${BSA_PATH} ]];then
        export PATH=$BSA_PATH:${PATH}
else
        echo "$PROGRAM_NAME Terminated with Error: $BSA_PATH does not exist"
        return 1
fi

# Initialise the RETL RFX Path
if [[ -d ${RFX_HOME} ]];then
        export PATH=$RFX_HOME/bin:$RFX_TMP:${PATH}
else
        echo "$PROGRAM_NAME Terminated with Error: $RFX_HOME does not exist"
        return 1
fi

OLD_RPO_MASTERDOMAIN="$RPO_MASTERDOMAIN:-"

printf "\n"
export RPO_MASTERDOMAIN="$RPO_RETAIL_HOME/domain/RegPrice"
printf "**** RPO_MASTERDOMAIN = $RPO_MASTERDOMAIN \n"
printf "\n"

f2=$(echo ${PATH:-} | grep "$RPO_MASTERDOMAIN" > /dev/null )
r2=$?

if [[ -z "$OLD_RPO_MASTERDOMAIN" || $r1 != 0 || $r2 != 0 ]]
then
        export PATH=${PATH}:$RPO_MASTERDOMAIN
else
        export PATH=$(echo $PATH | sed "s%$OLD_RPO_MASTERDOMAIN%$RPO_MASTERDOMAIN%g")
fi

export LD_LIBRARY_PATH="$JAVA_HOME/lib:$JAVA_HOME/lib/amd64/server:$LD_LIBRARY_PATH"
export PATH=$PATH:$LD_LIBRARY_PATH

echo $PATH
printf "\n"

echo "Overriding startrpas/stoprpas aliases"

alias startrporpas="ksh $BATCH_HOME/scripts/orc_start_daemon.ksh daemon_rpo.ctl"
alias stoprporpas="ksh $BATCH_HOME/scripts/orc_stop_daemon.ksh daemon_rpo.ctl"
alias rporpasusers="DomainDaemon -port $RPO_PORT -ssl 2 -showActiveServers"

alias startrporpas
alias stoprporpas
alias rporpasusers

printf "\n"

return 0

