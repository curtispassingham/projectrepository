#!/bin/ksh

scriptTimestamp=$(date +%y%m%d_%H%M%S)

if [ -z $RPAS_HOME ]; then
	echo ":ERROR: RPAS_HOME is not set. Please execute the retaillogin.ksh script."
	exit 1
fi

if [ -z $RPO_HOME ]; then
	echo ":ERROR: RPO_HOME is not set. Please set the RPO home path where all solution files are."
	echo "        If the RPO Domain has been previously built, then the folder structure should contain:"
	echo "          - config              - RPO RPAS Configurtion files"
	echo "          - domain              - RPO RPAS Domain files"
	echo "          - fusion              - FusionClient Taskflow files in multi languages"
	echo "          - input               - Input data necessary to build the domain"
	echo "          - logs                - RPO Solution main log files."
	echo "          - rpas                - Folder containing special RPO libraries"
	echo "          - scripts             - All scripts associated with RPO solution"
	echo "          - translations        - Translations in several languages for RPO measures."
	echo "          - translations_custom - Override of Vanilla Translations in several languages for RPO measures."
	echo "        "
	echo "        Otherwise it should be an empty folder where to store the RPO solution."
	exit 1
fi

if [ -z $RPO_HOME/translations_custom ]; then
	echo ":ERROR: RPO has not yet been installed in the RPO_HOME path."
	echo "        Folder translations_custom does not exist inside RPO_HOME."
	echo "        RPO_HOME is set to: $RPO_HOME"
	exit 1
fi

find $RPO_HOME/translations_custom -name "*.*" -not -name "taskflowBundle.properties" -type f -exec cp {} $RPO_HOME/domain/RegPrice/input \;

for measName in r_dimlabel r_hierlabel r_langattr r_measdescripti r_measlabel r_measpicklist r_msglabel r_wbtglabel r_wbtlabel
do
	loadmeasure -d $RPO_HOME/domain/RegPrice -measure $measName -loglevel none
done

echo "Success"
exit 0

