#!/bin/ksh

echo "###################### START: Domain Full Install"
scriptName=$(basename "$0")
scriptTimestamp=$(date +%y%m%d_%H%M%S)

domainPath="$RPO_HOME/domain/RegPrice"
defaultDomainPath="$domainPath/ldom99999"
maxProcess="${BSA_MAX_PARALLEL:-1}"
logLevel="${BSA_LOG_LEVEL:-INFORMATION}"

# Check Parameters
numParams=$#
dateSet=$1

if [[ ${numParams} -ne 1 ]]
then
    echo ":ERROR: Wrong Paramenters entered. Date parameter missing (format YYYYMMDD)"
    exit 100
elif [[ ${#dateSet} -ne 8 ]]
then
   echo ":ERROR: Invalid date parameter. Date parameter must be with the format YYYYMMDD"
   exit 110
else
  checkDate=$(date +%Y%m%d -d ${dateSet})
  if [[ "$?" -ne "0" ]]; then
   echo ":ERROR: Invalid date. Please make sure you enter a valid date with the format YYYYMMDD"
   exit 120
  fi
fi



if [ -z $RPAS_HOME ]; then
	echo ":ERROR: RPAS_HOME is not set. Please execute the retaillogin.ksh script."
	exit 1
fi

if [ -z $RPO_HOME ]; then
	echo ":ERROR: RPO_HOME is not set. Please set the RPO home path where all solution files are."
	echo "        If the RPO Domain has been previously built, then the folder structure should contain:"
	echo "          - config              - RPO RPAS Configurtion files"
	echo "          - domain              - RPO RPAS Domain files"
	echo "          - fusion              - FusionClient Taskflow files in multi languages"
	echo "          - input               - Input data necessary to build the domain"
	echo "          - RETLforXXADEO       - ADEO Custom Integration scripts"
	echo "          - logs                - RPO Solution main log files."
	echo "          - rpas                - Folder containing special RPO libraries"
	echo "          - scripts             - All scripts associated with RPO solution"
	echo "          - translations        - Translations in several languages for RPO measures."
	echo "          - translations_custom - Override of Vanilla Translations in several languages for RPO measures."
	echo "        "
	echo "        Otherwise it should be an empty folder where to store the RPO solution."
	exit 1
fi

if [ -z $RETL_IN -o -z $RETL_IN/RPO ]; then
	echo ":ERROR: RETL_IN is not set. Please set the RETL_IN pointing to the SFTP folder that contains the inbound data of the solution."
	echo "        RETL_IN directory should contain a folder RPO with the RPO specific inbound data."
	exit 1
fi

if [ -z $RETL_OUT -o -z $RETL_OUT/RPO ]; then
	echo ":ERROR: RETL_OUT is not set. Please set the RETL_OUT pointing to the SFTP folder that contains the outbound data of the solution."
	echo "        RETL_OUT directory should contain a folder RPO with the RPO specific outbound data."
	exit 1
fi

echo "###################### START: Pre-Build Activities"

# find $RPO_HOME -name "*sh" -exec dos2unix {} +
# find $RPO_HOME -name "*.schema" -exec dos2unix {} +
# find $RPO_HOME -name "*.env" -exec dos2unix {} +
# find $RPO_HOME -name "*.awk" -exec dos2unix {} +
# find $RPO_HOME -name "*.txt" -exec dos2unix {} +
# find $RPO_HOME -name "*.conf" -exec dos2unix {} +
# find $RPO_HOME -name "*.ctl" -exec dos2unix {} +
# find $RPO_HOME -name "retl_msgs.*" -exec dos2unix {} +

echo "    => Check Batch scripts existance"
cd $RPO_HOME/batch/scripts
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Folder scripts do not exist $RPO_HOME/scripts"
    exit 1
fi

echo "    => Set Batch scripts permissions"
chmod -R +x $RPO_HOME/scripts
chmod -R +x $RPO_HOME/batch

if [ -d $domainPath ]; then
	echo "    => Removing existing RPO Domain"
	rm -r $domainPath
fi 

echo "    => Extract required RPAS libraries for RPO"
tar -xf $RPO_HOME/rpas/Linux7/rpas.tar
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Can uncompress file $RPO_HOME/rpas/rpo/Linux7/rpas.tar"
    exit 1
fi

echo "    => Set the required RPAS libraries for RPO"
mv -n applib/* $RPAS_HOME/applib
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Problem copying RPO libraries from $RPO_HOME/rpas/rpo/Linux7/applib to $RPAS_HOME"
    exit 1
fi

echo "    => Libraries Temporary Folder Clean Up"
rm -r applib/

mkdir -p $RPO_HOME/logs/installationlogs
echo "###################### END: Pre-Build Activities"

echo "###################### START: Build Activity"
echo "    => Building Domain..."
./buildRegPrice.sh -c $RPO_HOME/config -i $RPO_HOME/input -l $RPO_HOME/logs/installationlogs -d $RPO_HOME/domain
if [ "$?" -ne "0" ]; then
   	echo ":ERROR: RPO Domain Build Error. Please check $RPO_HOME/logs/installationlogs"
	exit 1
fi
echo "###################### END: Build Activity"

echo "###################### START: Post-Build Activities"

echo "    => Set system VDATE to \"${dateSet}\""
ksh $BATCH_HOME/scripts/orc_update_vdate.ksh set ${dateSet}
cp $domainPath/vdate.int $RETLforXXADEO/rfx/etc/vdate.txt

echo "    => Prepare RPO Translations Load"
find $RPO_HOME/translations -name "*.*" -not -name "taskflowBundle.properties" -type f -exec cp {} $RPO_HOME/domain/RegPrice/input \;

echo "    => Executing RPO Vanilla Translations Load"
for measName in r_dimlabel r_hierlabel r_langattr r_measdescripti r_measlabel r_measpicklist r_msglabel r_wbtglabel r_wbtlabel
do
	loadmeasure -d $domainPath -measure $measName -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log
done

echo "    => Executing RPO Custom Translations Load"
ksh $RPO_HOME/rpo_load_custom_translations.ksh >> $RPO_HOME/logs/installationlogs/build.log


echo "    => Post RPO Translations Load Setup"
errorCheck=0
regmeasure -d $domainPath -add tmp_r_measpicklist -label tmp_r_measpicklist -type string -baseint meas -db data/language -loglevel $logLevel ; errorCheck=$(( $errorCheck + $? ))
mace -d $domainPath -run -expression "tmp_r_measpicklist = r_measpicklist.first_pop" -loglevel $logLevel ; errorCheck=$(( $errorCheck + $? ))
mace -d $domainPath -run -expression "r_measpicklist = tmp_r_measpicklist" -loglevel $logLevel ; errorCheck=$(( $errorCheck + $? ))
regmeasure -d $domainPath -remove tmp_r_measpicklist -loglevel $logLevel ; errorCheck=$(( $errorCheck + $? ))
if [ "$errorCheck" -ne "0" ]; then
    echo ":ERROR: Opening RPO Picklists translations."
    exit 1
fi

echo "    => Executing RPO RETL Transformation for Abonnement BU (IR0063 - XXRPO207)"
$RETLforXXADEO/rfx/src/xxadeo_rpot_abonnement_bu.ksh
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Running RETL Transformation $RETLforXXADEO/rfx/src/xxadeo_rpot_abonnement_bu.ksh"
    exit 1
fi

echo "    => Executing RPO RETL Transformation for Calendar Hierarchy (IR0056 - XXRPO200)"
$RETLforXXADEO/rfx/src/xxadeo_rpot_calendar_hierarchy.ksh
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Running RETL Transformation $RETLforXXADEO/rfx/src/xxadeo_rpot_calendar_hierarchy.ksh"
    exit 1
fi

echo "    => Executing RPO RETL Transformation for Item Links (IR0058 - XXRPO205)"
$RETLforXXADEO/rfx/src/xxadeo_rpot_item_link.ksh
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Running RETL Transformation $RETLforXXADEO/rfx/src/xxadeo_rpot_item_link.ksh"
    exit 1
fi

echo "    => Executing RPO RETL Transformation for Merchandise Hierarchy (IR0057 - XXRPO201)"
$RETLforXXADEO/rfx/src/xxadeo_rpot_merchhier.ksh
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Running RETL Transformation $RETLforXXADEO/rfx/src/xxadeo_rpot_merchhier.ksh"
    exit 1
fi

echo "    => Executing RPO RETL Transformation for Main Publishing Selling Price Flag (IR0044 - XXRPO210)"
$RETLforXXADEO/rfx/src/xxadeo_rpot_pv_principal_publication.ksh
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Running RETL Transformation $RETLforXXADEO/rfx/src/xxadeo_rpot_pv_principal_publication.ksh"
    exit 1
fi

echo "    => Executing RPO RETL Transformation for Item Capacity Quantity (IR0062 - XXRPO203)"
$RETLforXXADEO/rfx/src/xxadeo_rpot_quantite_contenance_pack_size.ksh
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Running RETL Transformation $RETLforXXADEO/rfx/src/xxadeo_rpot_quantite_contenance_pack_size.ksh"
    exit 1
fi

echo "    => Executing RPO RETL Transformation for Merchandise Hierarchy Translations (IR0064 - XXRPO204)"
$RETLforXXADEO/rfx/src/xxadeo_rpot_traductions.ksh
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Running RETL Transformation $RETLforXXADEO/rfx/src/xxadeo_rpot_traductions.ksh"
    exit 1
fi

echo "    => Executing RPO RETL Transformation for Item Capacity Unit (IR0040 - XXRPO209)"
$RETLforXXADEO/rfx/src/xxadeo_rpot_unite_contenance.ksh
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Running RETL Transformation $RETLforXXADEO/rfx/src/xxadeo_rpot_unite_contenance.ksh"
    exit 1
fi

echo "    => Copy Input files and Transformed Input Files to the RPO Domain."
cp $RETL_IN/RPO/* $RPO_HOME/domain/RegPrice/input
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Not possible to copy customer data from $RPO_HOME/integration/internal_inbound/* to newly created domain input folder $RPO_HOME/domain/RegPrice/input"
    exit 1
fi


echo "    => Define the necessary RPO Hierarchy Dimensions BitSizes according to the Input Data Set"
echo "        :NOTE: If this step is not successfull then review the BitSizes in this script against the Input Data Set Size"
errorCheck=0
dimensionMgr  -d $domainPath -dim item -bitSize 20 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim iter -bitSize 20 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim clss -bitSize 16 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim clsr -bitSize 16 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim scat -bitSize 14 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim scar -bitSize 14 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim cat  -bitSize 12 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim catr -bitSize 12 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim dept -bitSize 10 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim depr -bitSize 10 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim brnd -bitSize 14 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim brnr -bitSize 14 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim str  -bitSize 12 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim zone -bitSize 10 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim rgn  -bitSize 10 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim ppnt -bitSize 14 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
dimensionMgr  -d $domainPath -dim scnr -bitSize 10 -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
reindexDomain -d $domainPath -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log ; errorCheck=$(( $errorCheck + $? ))
if [ "$errorCheck" -ne "0" ]; then
    echo ":ERROR: Defining RPO Domain Hierarchy Dimensions BitSize."
    exit 1
fi

echo "    => Execute the RPO Hierarchies Load"
cd $RPO_HOME/batch/scripts
./rpo_foundations_load.sh -z -d $domainPath -g $defaultDomainPath -p $maxProcess -l $logLevel
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Loading customer foundation data (hierarchies)."
    exit 1
fi

echo "    => Execute the RPO Data Load"
cd $RPO_HOME/batch/scripts
./loadRegPriceMeasures.sh -d $domainPath -p $maxProcess
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Loading customer measure data."
    exit 1
fi

echo "    => Set RPO property: Linkage Mode"
mace -d $domainPath -run -expression "slinkmod = 1" -processes $maxProcess -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Setting Linkage Mode to OneWay."
    exit 1
fi

echo "    => Set RPO property: Optimization Mode"
mace -d $domainPath -run -expression "scap = 1" -processes $maxProcess -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Optimization Mode to Rule Management."
    exit 1
fi

echo "    => Set RPO property: Item No Touch Period"
mace -d $domainPath -run -expression "ol1itnotchprd = -1" -processes $maxProcess -loglevel $logLevel >> $RPO_HOME/logs/installationlogs/build.log
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Setting Item No Touch Period to -1."
    exit 1
fi

echo "    => Clean Up the RPO Domain Input Folder"
rm $domainPath/input/*.*
echo "###################### END: Post-Build Activities"

echo "###################### END: Domain Full Install - Success"
exit 0

