#!/bin/ksh

echo "###################### START: Domain Patch Install"
scriptName=$(basename "$0")
scriptTimestamp=$(date +%y%m%d_%H%M%S)

domainPath="$RPO_HOME/domain/RegPrice"
defaultDomainPath="$domainPath/ldom99999"
maxProcess="${BSA_MAX_PARALLEL:-1}"
logLevel="${BSA_LOG_LEVEL:-INFORMATION}"

if [ -z $RPAS_HOME ]; then
	echo ":ERROR: RPAS_HOME is not set. Please execute the retaillogin.ksh script."
	exit 1
fi

if [ -z $RPO_HOME ]; then
	echo ":ERROR: RPO_HOME is not set. Please set the RPO home path where all solution files are."
	echo "        If the RPO Domain has been previously built, then the folder structure should contain:"
	echo "          - config              - RPO RPAS Configurtion files"
	echo "          - domain              - RPO RPAS Domain files"
	echo "          - fusion              - FusionClient Taskflow files in multi languages"
	echo "          - input               - Input data necessary to build the domain"
	echo "          - logs                - RPO Solution main log files."
	echo "          - rpas                - Folder containing special RPO libraries"
	echo "          - scripts             - All scripts associated with RPO solution"
	echo "          - translations        - Translations in several languages for RPO measures."
	echo "          - translations_custom - Override of Vanilla Translations in several languages for RPO measures."
	echo "        "
	echo "        Otherwise it should be an empty folder where to store the RPO solution."
	exit 1
fi

echo "###################### START: Pre-Patch Activities"

# find $RPO_HOME -name "*sh" -exec dos2unix {} +
# find $RPO_HOME -name "*.schema" -exec dos2unix {} +
# find $RPO_HOME -name "*.env" -exec dos2unix {} +
# find $RPO_HOME -name "*.awk" -exec dos2unix {} +
# find $RPO_HOME -name "*.txt" -exec dos2unix {} +
# find $RPO_HOME -name "*.conf" -exec dos2unix {} +
# find $RPO_HOME -name "*.ctl" -exec dos2unix {} +
# find $RPO_HOME -name "retl_msgs.*" -exec dos2unix {} +

echo "    => Check Batch scripts existance"
cd $RPO_HOME/batch/scripts
if [ "$?" -ne "0" ]; then
    echo ":ERROR: Folder scripts do not exist $RPO_HOME/scripts"
    exit 1
fi

echo "    => Set Batch scripts permissions"
chmod -R +x $RPO_HOME/scripts
chmod -R +x $RPO_HOME/batch

echo "    => Check for the Existence of a RPO Domain"
if [ ! -d $domainPath ]; then
	echo ":ERROR: Do not exist a current RPO domain to be patched."
	exit 1
fi 

mkdir -p $RPO_HOME/logs/installationlogs
echo "###################### END: Pre-Patch Activities"

echo "###################### START: Patch Activity"
./buildRegPrice.sh -c $RPO_HOME/config -i $RPO_HOME/input -l $RPO_HOME/logs/installationlogs -d $RPO_HOME/domain -p
if [ "$?" -ne "0" ]; then
   	echo ":ERROR: RPO Domain Build Error. Please check $RPO_HOME/logs/installationlogs"
	exit 1
fi
echo "###################### END: Patch Activity"

echo "###################### END: Domain Patch Install - Success"
exit 0

