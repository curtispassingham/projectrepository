RPO_DT_CONFIG_ERROR     English             Regular Price Optimization configuration error.
RPO_DGRP_NOLOAD         English             Demand group not loaded.
RPO_PSCP_NOCREATE       English             Planning scope not created.
RPO_SCNR_NOCREATE       English             Scenario not created.
RPO_ILG_NOLOAD          English             Item link group not loaded.
RPO_DGRP_MUST_SEL       English             One demand group must be selected.
RPO_DGRP_ONE_SEL        English             Only one demand group may be selected.
RPO_DGRP_ITEM_ASSIGN    English             No items are assigned to the demand group.
RPO_NONE_IGRP           English             No available item group.
RPO_NONE_PSCP           English             No available planning scope
RPO_NONE_SCNR           English             No available scenario.
RPO_NONE_PRZN           English             No available locations.
RPO_INVALID_PSCP        English             Selected planning scope is invalid.
RPO_PSCP_MUST_SEL       English             One planning scope must be selected.
RPO_PSCP_ONE_SEL        English             Only one planning scope may be selected.
RPO_SCNR_MUST_SEL       English             One scenario must be selected.
RPO_SCNR_ONE_SEL        English             Only one scenario may be selected.
RPO_INVALID_UOMEUOM     English             Invalid UOM or EUOM value.
RPO_INVALID_PRICELADDER English             Invalid price ladder.
RPO_BAD_RNGCONSTRNT     English             Bad item range or competition constraint.
RPO_BAD_IGPRNGCONSTRNT  English             Bad item group range or competition constraint.
RPO_BAD_HOLDCONSTRNT    English             Bad price-hold constraint.
RPO_BAD_INTITMCONSTRNT  English             Bad inter-item constraint.
RPO_INVALID_ITEMLINK    English             Invalid item link groups for item group constraint.
RPO_OPT_ZEROITEMS       English             Number of items is 0
RPO_OPT_LT2_ITEMS_MNL   English             Number of items is less than 2 for MNL model
RPO_OPT_NUM_BS_PR_OOR   English             Number of base price entries out of range
RPO_OPT_NUM_BS_SLS_OOR  English             Number of base sales entries out of range
RPO_OPT_NUM_BS_CST_OOR  English             Number of base cost entries out of range
RPO_OPT_INT_ITM_DAT_OOR English             Inter-item constraint data out of range
RPO_OPT_GAM_BS_PR_DM_DF English             Gamma and base price dimensions are different
RPO_OPT_ALP_BS_PR_DM_DF English             Alpha and base price dimensions are different
RPO_OPT_RANGE_INFEAS    English             Range infeasibility
RPO_OPT_INTER_RANGE_INF English             Inter/range infeasibility
RPO_OPT_FEASIBLE        English             Feasible
RPO_OPT_PRICE_CH_LM_INF English             Price change limit infeasibility
RPO_OPT_PRICE_LD_INFES  English             Price ladder infeasibility
RPO_OPT_HARD_SOFT_INFES English             Hard and soft infeasible
RPO_OPT_EXCEPTION       English             Exception
RPO_IGRP_NOCREATE       English             Item group not created
RPO_OPT_LESSTHAN_DMD    English             Optimization level less than demand level
RPO_INT_CONS_IDX_OOR    English             Inter item constraint item index out of range
RPO_INT_CONS_INV_CMPTR  English             Inter item constraint invalid comparator
RPO_INT_CONS_INv_COFF   English             Inter item constraint invalid coefficient
RPO_INT_CONS_DATA_OOR   English             Inter item constraint data Out of Range
RPO_ITM_NUM_IS_ZERO     English             Number of items is zero
RPO_ITM_LT_TWO          English             Number of items is less than 2 for MNL model
RPO_GAMMA_OOR           English             Number of gamma entries Out of Range
RPO_INVALID_MODE        English             Model number out of range
RPO_NUM_ITM_NO_MATCH    English             Number of items does not match
RPO_NUM_STORE_IS_ZERO   English             Number of stores is zero
RPO_PLDR_STEP_OOR       English             Number of price ladder steps out of range
RPO_NUM_COST_OOR        English             Number of base cost entries out of range
RPO_NUM_SLS_OOR         English             Number of base sales entries out of range
RPO_NUM_CONS_OOR        English             Number of constraints out of range
RPO_INTER_RANG_INF      English             Inter/Range infeasibility
RPO_RANG_INF            English             Range infeasibility
RPO_PRC_CHG_LMT_INF     English             Price change limit infeasibility
RPO_PLDR_INF            English             Price ladder infeasibility
RPO_HD_SFT_FEA          English             Hard and soft feasible
RPO_FEASIBLE            English             Feasible
RPO_EXCEPTION           English             Exception
RPO_ANA_NO_PERM         English             No permission to change some items in this demand group
RPO_NO_RELAX            English             No price conflict with constraint(s)
RPO_VLDT_RELAX          English             Some price conflict with constraint(s)  
RPO_CNS_RELAX           English             is relaxed.
RPO_CNS_VIOLATE         English             is violated.
RPO_RANGE_CNS           English             Price Range Constraint
RPO_INTER_ITEM_CNS      English             Inter Item Constraint
RPO_MARGIN_CNS          English             Margin Constraint
RPO_COMP_CNS            English             Competition Constraint
RPO_PRC_HLD_CNS         English             Price Hold Constraint
RPO_PRC_FMLY_CNS        English             Price Family Constraint
RPO_PER_CHG_CNS         English             Max Percentage Price Change Constraint
RPO_PRC_LAD_CNS         English             Price is not on the specified price ladder.
RPO_MIN_PRC_CNS         English             Minimum Price Change Constraint
RPO_INVLID_LOHI_PRC     English             Low Price should be less than High Price.
RPO_NO_END_DIG          English             Must select at lease one ending digit.
RPO_NON_POS_PRC         English             Price step or high/low price should be positive.
RPO_CANNOT_OPEN         English             Should not build Price Analysis workbook in master domain.