#!/bin/ksh

#
# Copyright (c) 1999, 2011, Oracle and/or its affiliates. All rights reserved. 
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 719002 $
#

# ------------------------------------------------------------------------

. "$RPO_HOME/scripts/rpo_environment.ksh"
scriptName=$(basename "$0")

function Usage
{
  _log_message ERROR "Usage: $scriptName options (-d)"
  _log_message ERROR "  d: domain path [default: '$(pwd)']"
}

domainPath="."

while getopts :d: Option
do
  case $Option in
    d    ) domainPath="${OPTARG}"     ;;

    *    )
       Usage
       _clean_exit NONZERO_EXIT ;;
  esac
done

shift $(($OPTIND - 1))

echo "test"
echo $domainPath
cd  $domainPath
########

function f_validate
{
   _call mace -d . -run -group "Rpo_Batch_Vldt"
   _verify_binary

   f_printscalar BValidate|f_search_for_true > .validate_scenario.tmp

   # Set this function's return code to 0 (true) if temp file is not empty.
   [[ -s .validate_scenario.tmp ]]
}

# --------------------------------------------------------
# Find all scenarios in next job run).
# --------------------------------------------------------

f_print1d scnrtorun > /dev/null 2>&1
_verify_binary

f_print1d scnrtorun | f_search_for_true > .enabled_scenario.tmp

if [[ -s .enabled_scenario.tmp ]]; then
   # for _scnr in .enabled_scenario.tmp do
    while read _scnr 
    do
         _call mace -d . -run -expression "sltscnr=\"${_scnr}\""
         _verify_binary

         if f_validate; then

            _call rm -f .validate_scenario.tmp
                     _verify_binary

            _call mace -d . -run -group "Rpo_Batch_Pre" 
                     _verify_binary

            _call mace -d . -run -group "Rpo_Batch_Run" 
                     _verify_binary

            _call mace -d . -run -group "Rpo_Batch_Post" 
                     _verify_binary

         else
            rm -f .validate_scenario.tmp
            sleep 1
         fi

   done < .enabled_scenario.tmp

   _call rm -f .enabled_scenario.tmp
else
   _call rm -f .enabled_scenario.tmp
   _log_message INFORMATION "No scenarios in this batch job run."
   _clean_exit 0 "Aborting."
fi

_verify_log
_clean_exit SUCCESS

