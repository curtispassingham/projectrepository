#!/bin/ksh
#
# Copyright (c) 2012 Oracle.  All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 642944 $
# $Date: 2013-05-14 14:14:51 -0500 (Tue, 14 May 2013) $
# $Author: ming.lei@oracle.com $
#
################################################################################
# File: rpo_functions.ksh
#

function f_print1d
{
   typeset _measure="$1"

   printMeasure -d . -m $_measure -slice info:info -loglevel none
   _verify_binary "print one-dimensional measure $measure"
}

function f_search_for_true
{
   grep -i -e 'true$' | awk '{print $1}'
}

function f_printscalar
{
   typeset measure="$1"

   printMeasure -d . -m $measure -cell info:info -loglevel none  
   _verify_binary "print scalar measure $measure"
}

return 0

