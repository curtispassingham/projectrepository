#!/bin/ksh
#
# Copyright (c) 2012 Oracle.  All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 603801 $
# $Date: 2013-02-27 18:00:45 -0600 (Wed, 27 Feb 2013) $
# $Author: daniel.ortmann@oracle.com $
#
################################################################################
# File: rpo_environment.sh
#
# This is the common environment "include" script. This file should be sourced
# by all RPO scripts.
#
# Args: None
################################################################################

_script_name_="rpo_environment.ksh"

#
# Guard against re-sourcing
#
if [[ "${_rpo_environment_vanilla_sh-0}" = 1 ]]; then
    return 0
else
    typeset -i -r _rpo_environment_vanilla_sh=1
fi

#
# Source the specialized environment
#
. ${O_APP_INSTANCE}/batch/environment/rpo_env_rpas.sh

if [[ "${BSA_MAX_PARALLEL-}" = "" ]]; then
   typeset -i BSA_MAX_PARALLEL=3
   export BSA_MAX_PARALLEL
fi

if [[ "${BSA_SCREEN_LEVEL-}" = "" ]]; then
   export BSA_SCREEN_LEVEL="INFORMATION"
fi

: ${TMP:=/tmp}; export TMP
: ${BSA_LOG_LEVEL:=INFORMATION}; export BSA_LOG_LEVEL
: ${BSA_LOG_TYPE:=1}; export BSA_LOG_TYPE
: ${BSA_ARCHIVE_DIR:=$HOME}; export BSA_ARCHIVE_DIR
: ${BSA_CONFIG_DIR:=$HOME}; export BSA_CONFIG_DIR
: ${BSA_LOG_HOME:=$HOME}; export BSA_LOG_HOME
: ${BSA_TEMP_DIR:=$HOME}; export BSA_TEMP_DIR

. bsa_common.sh

PATH="$RPO_HOME/scripts:$PATH"
. rpo_functions.ksh


###
#
# Guard against re-sourcing
#
if [[ "${_rpo_environment_sh-0}" = 1 ]] ; then
   echo "Warning: you have already already run '$_script_name_'---no action taken."
   return 0
else
   typeset _rpo_environment_sh=1
fi

log_path=$(_get_log_path)

unset CDPATH  # Ensure that "cd" doesn't use CDPATH variable.

O_UNAME=$(uname)

return 0

