#!/bin/ksh
#
# $Revision: 1.0
# $Date: 30/05/2018
# $Author: ADEO Builders - Offre - Vanilla
#
# Version              Change                         Author
# 1.0                  Initial Create                 ADEO Builders - Offre - Vanilla
#
##############################################################################
#
# Description:
#   Script to load conversion measures in catman.
#
#
##############################################################################
#------------------------------------------------------------------------------------------------------#
# STARTING GLOBAL VARIABLES
#------------------------------------------------------------------------------------------------------#
# Execute config and batch profiles to load Environment Variables and specific batch variables.

#. $ADEO_OFFRE_BATCH_VARIABLES

app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")

importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
else
    echo "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
echo "LOG_PATH: "${logPath}

#------------------------------------------------------------------------------------------------------#
# PARAMETERS
#------------------------------------------------------------------------------------------------------#
if [ $# -eq 0 ]
then
  echo "Insufficient or invalid parameters received"
  echo "Valid options:"
  echo " Exchange Rate [exchange_rate]"
  echo " Packing Dim  [Packing_Dim]"
  exit 1
else
  if [ ! -z $1 ]
  then
     #upperStep=`echo $1 | tr "[:lower:]" "[:upper:]"`
     #step=`echo $1 | tr "[:upper:]" "[:lower:]"`
     step=$1
     echo "STEP: ${step}"
  fi
fi

#------------------------------------------------------------------------------------------------------#
# STARTING STEPS
#------------------------------------------------------------------------------------------------------#
case "$step" in

#------------------------------------------------------------------------------------------------------
  itemCreFlag|lettreGamme|artPCB|attributsMag|desgn_sec|art_UDA|artBU_UDA|art_CFA|itemlie|rmse_rpas_article_photo|mes_std)    #Catman conversion measures
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ${step}
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  store_cluster)    #CR271_Lien Cluster-Magasin
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmwpstrtoclsttx.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmwpstrtoclsttx
  _verify_binary

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmcpstrtoclsttx.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmcpstrtoclsttx
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  assortment_preco_mag)    #CR276_Assortiment régulier préconisé par magasin
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmwpusrstrassrtvaltx.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmwpusrstrassrtvaltx
  _verify_binary

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmcpusrstrassrtvaltx.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmcpusrstrassrtvaltx
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  init_assort_clstr)    #CR270_Assortiment preco at Cluster
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmwpusrclsassrtvaltx.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmwpusrclsassrtvaltx
  _verify_binary

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmcpusrclsassrtvaltx.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmcpusrclsassrtvaltx
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  dates_debut_fin_vente)    #CR275_Dates Debut/Fin Ventes Omnicanal
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  controlFileSplit="Ctm_Conversion_Split.ctl"
  _log_message INFORMATION "Control File to Split input file into measure files is: ${controlFileSplit}"

  ksh $SCRIPTS_BATCH/script_conversion_offre_split.ksh ${controlFileSplit} ${step}
  _verify_binary

  _log_message INFORMATION "Measure file splitted into single measure files, start loading each measure file into RPAS"

  convImportList=`cat ${EXTERNAL_INBOUND_DIR}/TMP_${step}`
  _log_message INFORMATION "measure list: ${convImportList}"

  for measFile in $convImportList; do
     _log_message INFORMATION "Start loading $measFile"
     measName=`echo ${measFile} | cut -f1 -d "."`
     ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ${measName}
     _verify_binary
  done

  _log_message INFORMATION "Removing temporary file ${EXTERNAL_INBOUND_DIR}/TMP_${step}"
  _call rm -f ${EXTERNAL_INBOUND_DIR}/TMP_${step}
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION "------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  mode_assort)    #CR274_Assortiment_Mode
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmcpassrtmodetx.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmcpassrtmodetx
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  Art_Four_BU)    #CR283_Article/Fournisseur/BU Status
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/comtyartcbusuplb.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} comtyartcbusuplb
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  remplace)    #CR_364: Article Remplacé
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmcprplcitmcodtx.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmcprplcitmcodtx
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  top_saisonier)    #CR_365: Subclass / BU - Top Saisonnier
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/ctmadseasonalb.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ctmadseasonalb
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  XXADEO_ASSORTMENT)    #CR_XXX: Assortiment Choix Magasin
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  #_call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/stpcpusrstrassrtchotx.csv.ovr
  #_verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ${step}
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;
#------------------------------------------------------------------------------------------------------
  PVC)    #CR_279: PV Conseil
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Ctm_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  _call cp ${EXTERNAL_INBOUND_DIR}/${step}* ${EXTERNAL_INBOUND_DIR}/statynsprsfv.csv.ovr
  _verify_binary

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} statynsprsfv
  _verify_binary

  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;

  *)
     _log_message INFORMATION "------ step does not exist --------"
     exit ${__CODE_UNSUPPORTED_FUNCTIONALITY}
  ;;
esac
#------------------------------------------------------------------------------------------------------#
# FINISHING STEPS
#------------------------------------------------------------------------------------------------------#


