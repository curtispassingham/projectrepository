#!/bin/ksh
#
# Version              Change                        Author
# 1.0                  Initial Create                Hassan Kishk
#
#########################################################################
#
# File:  script_conversion_offre_split.sh
# Application Version: none
#
# Syntax: script_conversion_offre_split.sh
#
# Description:
#   This scripts pre processes conversion files containning more than one measure in it. It receives the control file
#   and the specific conversion file to process, and it generates several files, one with each measure inside the input file
#
#
########################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    ${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    #. orc_util_library.sh
else
    ${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
echo "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to prepare conversion measure files specified by given control file "
    echo "  Usage:                                                                     "
    echo "************************************************************************* ***"
    echo "  ${scriptName} [controlFileName] [fileName]                                 "
    echo "  ${scriptName} [-h |-help]                                                  "
    echo "  -help  - print usage information and exit                                  "
    echo "***********************************************************i*****************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 2 2

controlFile=$1
inputFile=$2


#
# Begin main processing
#
_log_message INFORMATION "RPAS_TODAY=${RPAS_TODAY}"

#read list of measures in the control file that match the given input file name
importList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${controlFile}" | grep $inputFile | cut -f3 -d "|"  `

#verify if the file name parameter exists in the control file.
if [[ -z ${importList} ]]
then
   _log_message ERROR "The file name $inputFile record(s) was not found in the control file ${BATCH_HOME}/config/imports/data/${controlFile}"
   _clean_exit ${__CODE_FILE_NOT_FOUND}
else
   echo ${importList} > ${EXTERNAL_INBOUND_DIR}/TMP_${inputFile}
   _log_message INFORMATION "The list of measures to be processed is: \n$importList"
fi

#start processing the list of measures
#for each measure, get all measure's parameters from the control file to generate the output file .
for measLine in $importList; do
   _log_message INFORMATION "Processing input file to generate  measure file $measLine"

   #get input file name
   inputFilename=`grep ${measLine} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 1 `

   #get last dimension position
   lastDimPosition=`grep ${measLine} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 2 `

   #get output measure file name
   outputMeasFilename=`grep ${measLine} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 3 `

   #get measure column
   outputMeasPosition=`grep ${measLine} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 4 `

   if [[ -f ${EXTERNAL_INBOUND_DIR}/${inputFilename} ]]
   then
      _log_message INFORMATION "Generating measure file ${EXTERNAL_INBOUND_DIR}/${outputMeasFilename} from ${inputFilename}"

      awk -F',' -v cstart=1, -v cend=${lastDimPosition}, -v cMeas=${outputMeasPosition} '{ s=""; for(c=1; c<=cend; c++) length(s)==0 ? s=$c :  s=s","$c; print s,$cMeas } ' OFS=","   ${EXTERNAL_INBOUND_DIR}/${inputFilename} > ${EXTERNAL_INBOUND_DIR}/${outputMeasFilename}
      cmdResult=`echo $?`
      if [ ${cmdResult} -ne 0 ]
      then
         _log_message ERROR "Error generating measure file ${EXTERNAL_INBOUND_DIR}/${outputMeasFilename} from ${EXTERNAL_INBOUND_DIR}/${inputFilename}: ${cmdResult}"
         _clean_exit ${__CODE_UNSPECIFIED_ERROR}
      else
         _log_message INFORMATION "measure file ${EXTERNAL_INBOUND_DIR}/${outputMeasFilename} generated succesfully"
      fi
   else
      _log_message WARNING "input file not found ${EXTERNAL_INBOUND_DIR}/${inputFilename}, measure file ${EXTERNAL_INBOUND_DIR}/${outputMeasFilename} not generated"
   fi
done

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS
