#CP EDIT 9

#!/bin/ksh
#
# $Revision: 1.0
# $Date: 30/05/2018
# $Author: ADEO Builders - Offre - Vanilla
#
# Version              Change                         Author
# 1.0                  Initial Create                 ADEO Builders - Offre - Vanilla
#
##############################################################################
#
# Description:
#   Script to load conversion measures in costing.
#
#
##############################################################################
#------------------------------------------------------------------------------------------------------#
# STARTING GLOBAL VARIABLES
#------------------------------------------------------------------------------------------------------#
# Execute config and batch profiles to load Environment Variables and specific batch variables.

#. $ADEO_OFFRE_BATCH_VARIABLES

app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")

importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
else
    echo "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
echo "LOG_PATH: "${logPath}

#------------------------------------------------------------------------------------------------------#
# PARAMETERS
#------------------------------------------------------------------------------------------------------#
if [ $# -eq 0 ]
then
  echo "Insufficient or invalid parameters received"
  echo "Valid options:"
  echo " Exchange Rate [exchange_rate]"
  echo " Packing Dim  [Packing_Dim]"
  exit 1
else
  if [ ! -z $1 ]
  then
     #upperStep=`echo $1 | tr "[:lower:]" "[:upper:]"`
     #step=`echo $1 | tr "[:upper:]" "[:lower:]"`
     step=$1
     echo "STEP: ${step}"
  fi
fi

#------------------------------------------------------------------------------------------------------#
# STARTING STEPS
#------------------------------------------------------------------------------------------------------#
case "$step" in


#------------------------------------------------------------------------------------------------------
MAINFRE|MAINFRE_ART|INLDFRE|INLDFRE_ART|circuitPrArt|circuitPrFo|CUSTOM|Inter_Insur|Bank_Fees|Agent_Commission|After_SalesProvision|Pro_SAV|delay_pay|purchasecostparam|qualityart|Quality_Cost|adeomarkup|supplychaincostparam|ecotax|stock_cov_article|stock_cov|stock_cov_article|bu_art_supply|Bu_SR|MAINFRE_POL_POD|Bu_Art_Suppl_TypEntr|Packing_Dim|incoterm|ComTyArtcToBuB|ComTyArtcBuSuplB|CstT1CPrInvIndCbl|CstT1CPrMrcIndCbl|plan_pal|wh_entry_exit_cadency|wh_picking_cadency)    #Costing conversion measures
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Cst_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ${step} 
  _verify_binary
  
  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;

#------------------------------------------------------------------------------------------------------
  ventes|produitMatche|flagprixbloque|prix_achat|prix_achat_futur|Boni|itemsupmagdwh|rfa|tva|circuitPrFo|circuitPrArt|donnes_dwh|pr_vente_mag|qty_rec)    #Simtar conversion measures
#------------------------------------------------------------------------------------------------------
  _log_message INFORMATION "Starting $step at `date`"

  controlFile="Sta_Conversion_Measures.ctl"
  _log_message INFORMATION "Control File is: ${controlFile}"

  ksh $SCRIPTS_BATCH/orc_load_measures.ksh ${controlFile} ${step} 
  _verify_binary
  
  _log_message INFORMATION "Finishing $step at `date`"
  _log_message INFORMATION " ------------------------------------------------------------------------------------------------"
  ;;

  *)
     _log_message INFORMATION " ------ step does not exist --------"
     _log_message INFORMATION " ---------------------------------------------------------------------------------------------"
     exit ${__CODE_UNSUPPORTED_FUNCTIONALITY}
  ;;
esac
#------------------------------------------------------------------------------------------------------#
# FINISHING STEPS
#------------------------------------------------------------------------------------------------------#

