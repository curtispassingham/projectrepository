#CP EDIT 9

#!/bin/ksh
#
# Copyright © 2018, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1 
# $Date: 2018/06/01
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Oracle Retail Consulting
#
##############################################################################
# orc_util_library.sh
#
# Description: 
#   This script contains functions for the orc scripts.
#
##############################################################################

#
# ORC function to manage RPAS_TODAY
#
function orc_set_RPAS_TODAY
{
    if [[ -n ${RPAS_TODAY} ]]; then
        echo "Found RPAS_TODAY=${RPAS_TODAY}. Unsetting.."
        unset RPAS_TODAY
    fi

    actualDate=`cat ${GLOBAL_DOMAIN}/vdate.int`
    #verify date from ${GLOBAL_DOMAIN}/vdate.int
    checkDate=$(date +%Y%m%d -d ${actualDate})
    _verify_binary "Date ${checkDate} defined in ${GLOBAL_DOMAIN}/vdate.int is not a valid date"
    export RPAS_TODAY=${actualDate}
    _log_message INFORMATION "RPAS_TODAY was set to ${checkDate}"
}

