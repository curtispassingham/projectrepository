#!/bin/ksh
#
# Copyright © 2018 Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.2
# $Date: 2018/10/09
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
# 0.2                  New direcory structures       Orace Retail Consulting
#
#########################################################################
#
# File:  orc_load_measures.sh
# Application Version: none
#
# Syntax: orc_load_measures.sh control_file
#
# Description:
#   Loads all measures to the domain specified by environment variables. This script takes two
#   parameters; the given control file specifies which measures will be loaded and which boolean values will be transformed.
#   The secont parameter is the specific measure filename inside the given control file.
#   
# Calls: loadMeas and printArray
#
########################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    #. orc_util_library.sh
else
    #${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    exit 9
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to load measures specified by given control file    "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName} [controlFileName] [measureFilename]          "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 1 2

controlFile=$1
measureFilenameParam=$2

#
# Begin main processing
#

#read list of measure filenames in the control file
importList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -f1 -d "|"  `

#If a parameter was passed with the measure filename, the script will just process this measure file.
if [[ -z ${measureFilenameParam} ]]
then
   _log_message INFORMATION "The list of measure files to be processed is: \n$importList"
else
   #verify if the measure file parameter exists in the control file.
   importList=`echo $importList | grep -w $measureFilenameParam`
   if [[ -z ${importList} ]]
   then
      _log_message ERROR "The measure file $measureFilenameParam record was not found in the control file ${BATCH_HOME}/config/imports/data/${controlFile}"
      _clean_exit ${__CODE_FILE_NOT_FOUND}
   else
      importList=$measureFilenameParam
      _log_message INFORMATION "The measure file to be processed is: $importList"
   fi
fi

#start processing the list of measure files
if [[ -z ${importList} ]]
then
   _log_message ERROR "No measures found in the control file ${BATCH_HOME}/config/imports/data/${controlFile} to be imported"
   _clean_exit ${__CODE_NONZERO_EXIT}
else
   #for each measure file, get all measure(s) parameters from the control file to prepare for the loadMeasure command.
   for measFile in $importList; do
       _log_message INFORMATION "Start processing measure file $measFile"
       #get flag that indicates if input measure file needs to be transformed (example: Y/N to true/false)
       transformMeasFile=`grep -w ${measFile} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 2 `

       #get information if the measure file is required or not.
       requiredMeasFile=`grep -w ${measFile} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 3`
       
       if ls ${EXTERNAL_INBOUND_DIR}/${measFile}.* 1> /dev/null 2>&1
       then
           # Verify if boolean need to be transformed and copy file to input directory in global domain
           if [[ ${transformMeasFile} = "Y" ]]; then
             _log_message INFORMATION "Transforming boolean values for input measure file ${measFile}"
             for origFile in $(ls -1 ${EXTERNAL_INBOUND_DIR}/${measFile}.*); do
               _log_message INFORMATION "Processing transformation for ${measFile}"
               baseFileName=`basename ${origFile}`
               #echo $baseFileName
               _log_message DEBUG "baseFileName is $baseFileName"
               cat ${origFile} | sed -e 's/,Y$/,T/gI' | sed -e 's/,Y,/,T,/gI' | sed -e 's/,N$/,F/gI' | sed -e 's/,N,/,F,/gI' > ${GLOBAL_DOMAIN}/input/${baseFileName}
               _verify_binary "Could not transform ${origFile} to ${GLOBAL_DOMAIN}/input/${baseFileName}" 
               _log_message INFORMATION "Finished processing transformation for ${baseFileName}, file copied to $GLOBAL_DOMAIN/input directory"
             done
           else
             _call cp ${EXTERNAL_INBOUND_DIR}/${measFile}* ${GLOBAL_DOMAIN}/input
             _verify_binary "Could not copy  ${EXTERNAL_INBOUND_DIR}/${measFile}* to $GLOBAL_DOMAIN/input" 
            _log_message INFORMATION "No transformation required for ${measFile}, file copied to $GLOBAL_DOMAIN/input directory"
           fi

           #get measure(s) list from the global domain
           _log_message INFORMATION "Getting list of measure names for input file ${measFile}"
           matchMeasures=`printArray -array ${GLOBAL_DOMAIN}/data/measdata.measinfo -slice info:filename -cellsperrow 1 -loglevel none | grep -wi ${measFile} | tr -d \" | tr -d " " | cut -d ":" -f1 `
           _verify_binary "Cound not determine matchMeasures: ${matchMeasures}"
           _log_message DEBUG "matchMeasures are $matchMeasures"
           
           # due to bug in loadmeasure binary we must determine the correct sequence of measures manually 
           grepString=""
           for meas in ${matchMeasures}; do
               grepString="${grepString} -e ${meas} "
           done
           _log_message DEBUG "Pattern string is ${grepString}"

           matchMeasures=$(printArray -array ${GLOBAL_DOMAIN}/data/measdata.measinfo -slice info:start -cellsperrow 1 -loglevel none | grep -w ${grepString} | tr ":" " " | tr -s " " | sort -k 2 | cut -d " " -f 1 )
           _verify_binary "Cound not determine correct sequence of matchMeasures: ${matchMeasures}"
           # end of temporary fix

           if [[ -z ${matchMeasures} ]]
           then
               _log_message INFORMATION "Warning: no measures found for input file ${measFile}"
           else
               
               typeset -i first
               let first=0
               loadmeas=""
               for meas in $matchMeasures; do
                 if [[ $first -eq 0 ]] ; then
                   let first=1
                   loadmeas=${meas}
                 else
                   loadmeas="${loadmeas},${meas}"
                 fi
               done
               _log_message INFORMATION "Loading measures ${loadmeas} in ${GLOBAL_DOMAIN}."

                RPAS_LOG_LEVEL=$($PRINT ${RPAS_LOG_LEVEL} | tr "[:lower:]" "[:upper:]")

               _log_message INFORMATION "Executing command loadmeasure -d ${GLOBAL_DOMAIN} -measure $loadmeas -loglevel ${RPAS_LOG_LEVEL}"
               _call loadmeasure -d "${GLOBAL_DOMAIN}" -measure ${loadmeas} -loglevel "${RPAS_LOG_LEVEL}"
               _verify_binary "loadmeasure reported error during load of measures $loadmeas" 

               _log_message INFORMATION "Done loading measure(s) ${loadmeas} in ${GLOBAL_DOMAIN}"
           fi
       else
          if [[ $requiredMeasFile = "OPTIONAL" ]]
          then
             _log_message WARNING "Optional measure file  not found ${EXTERNAL_INBOUND_DIR}/${measFile}, measure(s) not loaded."
          else
             _log_message ERROR "Required measure file  not found ${EXTERNAL_INBOUND_DIR}/${measFile}, measure(s) not loaded."
             _clean_exit ${__CODE_FILE_NOT_FOUND}
          fi
       fi
   done # end of loop over import files
fi

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS
