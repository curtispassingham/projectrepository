#!/bin/ksh
#
# Copyright © 2016, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2016/07/01
# $Author: Oracle Retail Consulting
#
# Version              Change                         Author
# 0.1                  Initial Create                 Oracle Retail Consulting
#
##############################################################################
# Syntax: orc_batch_alerts.ksh
#
# Description:
#   Script to find and count the alert hits in the domains. This script takes two parameters.
#   The first parameter is the conrol file that contains alerts/categories information to be calculated.
#   The second parameter is the specific alert/category inside the given control file.
#
# Calls: alertmgr
#
##############################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

# 20180409_HKK Comment: Included full path for common rpas script.
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"
utilLibFile="${O_APP_INSTANCE}/batch/lib/orc_util_library.ksh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    . ${utilLibFile}
else
    # Cannot rely on BSA functions here - lib file not found
    echo "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 9
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

##############################################################################
## Function: usage                                                          ##
## Desc: Display the Usage information                                      ##
##--------------------------------------------------------------------------##

usage ()
{
    ${PRINT} "Usage: $scriptName{-h}"
    ${PRINT} "eg: $scriptName [controlFile] [alert/category]"
    ${PRINT} "eg: $scriptName -h"
}

##############################################################################
## Retrieve parameters                                                      ##
##--------------------------------------------------------------------------##

# 20180409_HKK Comment: Removed check if argument begins with -
#echo $1
if [[ ${numParams} -gt 0 ]]
then
   case "$1" in
   # the -help option displays usage
   -[Hh]|-[Hh][Ee][Ll][Pp]) usage "Display help"
             _clean_exit SUCCESS
                         ;;
                      *)
                         ;;
   esac
fi

# Validate the Number of Paramters
_check_arg_count ${numParams} 1 2


# 20180409_HKK Comment: Read alerts/categories from control files.
controlFile=$1
alertCategoryParam=$2

#
# Begin main processing
#
# Read vdate from global domain and set RPAS_TODAY
orc_set_RPAS_TODAY

if [[ ! -f "${BATCH_HOME}/config/alerts/${controlFile}" ]]; then
   _log_message INFORMATION "$Control file ${BATCH_HOME}/config/alerts/${controlFile} does not exist!"
  _clean_exit ${__CODE_FILE_NOT_FOUND}
fi

#read list of alerts or categories in the control file
alertCategoryList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/alerts/${controlFile}" `

if [[ -z ${alertCategoryList} ]]
then
   _log_message WARNING "Ignoring empty file ${controlFile}"
   _verify_log
   _clean_exit ${__CODE_SUCCESS}
fi

#If a parameter was passed with the alert or category name, the script will just process this alert or category.
if [[ -z ${alertCategoryParam} ]]
then
   _log_message INFORMATION "The list of alerts or categories to be processed is: \n$alertCategoryList"
else
   #verify if the alert or category parameter exists in the control file.
   alertCategoryList=`echo $alertCategoryList | grep $alertCategoryParam`

   if [[ -z ${alertCategoryList} ]]
   then
      _log_message ERROR "The alert or category $alertCategoryParam record was not found in the control file ${BATCH_HOME}/config/alerts/${controlFile}"
      _clean_exit ${__CODE_GENERAL_SCRIPT_ERROR}
   else
      alertCategoryList=$alertCategoryParam
      _log_message INFORMATION "The list of alerts or categories to be processed is: $alertCategoryList"
   fi
fi

#while IFS="|" read -r alertType alertTypeList

#start processing the list of alerts or categories
if [[ -z ${alertCategoryList} ]]
then
   _log_message ERROR "No alerts or categories found in the control file ${BATCH_HOME}/config/alerts/${controlFile} to be executed"
   _clean_exit ${__CODE_TOO_FEW_ARGS}
else
   #for each alert or category, get all parameters from the control file to prepare for the alertMgr command.
   #while IFS="|" read -r alertType alertTypeList
   for alertCategory in ${alertCategoryList}; do
     _log_message INFORMATION "Start processing $alertCategory"
      #get alert or category type
      alertCategoryType=`grep ${alertCategory} "${BATCH_HOME}/config/alerts/${controlFile}" | cut -d "|" -f 1 `
      _verify_binary "Could not determine alertCategoryType..."

      #get alert or category list
      alertCategoryList=`grep ${alertCategory} "${BATCH_HOME}/config/alerts/${controlFile}" | cut -d "|" -f 2 `
      _verify_binary "Could not determine alertCategoryList..."

      ldoms=$(domaininfo -d ${GLOBAL_DOMAIN} -shellsubdomains -loglevel none)
      _verify_binary "Could not determine subdomains..."
      for ldom in ${ldoms}
      do
         _log_message INFORMATION "Executing in ${ldom}"
         _para_spawn _call alertmgr -d ${ldom} -findAlerts -${alertCategoryType} "${alertCategoryList}" -navigationThreshold 100000000 -loglevel ${RPAS_LOG_LEVEL}
      done
      _para_wait
      _verify_binary
   done
   #done < "${BATCH_HOME}/config/alerts/${controlFile}"

   #
   #Executes alertmgr binary in the global domain to count the alert hits
   #
   _call alertmgr -d ${GLOBAL_DOMAIN} -sumAlerts -navigationThreshold 100000000 -loglevel ${RPAS_LOG_LEVEL}
   _verify_binary "alertmgr reported error while executing -d ${GLOBAL_DOMAIN} -sumAlerts -navigationThreshold 100000000 -loglevel ${RPAS_LOG_LEVEL}"
fi
#
# Verify and exit
#
_log_message INFORMATION "Finished running ${scriptName}"
_verify_log
_clean_exit SUCCESS

##############################################################################
## SPECIFIC SCRIPT SECTION END                                              ##
##############################################################################

