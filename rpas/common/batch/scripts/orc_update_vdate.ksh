#!/bin/ksh
#
# Copyright © 2016, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2018/05/24
# $Author: Oracle Retail Consulting
#
# Version              Change                         Author
# 0.1                  Initial Create                 Oracle Retail Consulting
#
##############################################################################
# Syntax: orc_update_vdate.ksh
#
# Description:
#   Script to update vdate.int file in the global domain, the date set in vdate.int will then
#   be used to set RPAS_TODAY environment variable in the batch processes.
#
##############################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"


which ${importLibFile} > /dev/null 2>&1
if [ "$?" -eq 0 ]
then
    . ${importLibFile}
    #. orc_util_library.sh
else
    # Cannot rely on BSA functions here - lib file not found
    echo "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 9
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

##############################################################################
## Function: usage                                                          ##
## Desc: Display the Usage information                                      ##
##--------------------------------------------------------------------------##

usage ()
{
    ${PRINT} "Usage: $scriptName{-h}"
    ${PRINT} "eg: $scriptName [set|increment|reset] [20180525]"
    ${PRINT} "eg: $scriptName -h"
}



##############################################################################
## Retrieve parameters                                                      ##
##--------------------------------------------------------------------------##

if [[ ${numParams} -gt 0 ]]
then
   case "$1" in
   # the -help option displays usage
   -[Hh]|-[Hh][Ee][Ll][Pp]) usage "Display help"
             _clean_exit SUCCESS
                         ;;
                      *)
                         ;;
   esac
fi

# Validate the Number of Paramters
_check_arg_count ${numParams} 1 2


optionVdate=$1
dateSet=$2

#
# Begin main processing
#
if [[ -n ${RPAS_TODAY} ]]; then
    _log_message INFORMATION "Found RPAS_TODAY=${RPAS_TODAY}. Unsetting.."
    unset RPAS_TODAY
    _verify_binary "Error unsetting RPAS_TODAY"
fi

#Check if the vdate.int file exists in the global domain - if not, create file and populate with current system date.

actualDate=""
if [[ ! -f ${GLOBAL_DOMAIN}/vdate.int ]]
then
    _log_message WARNING "The vdate.int file doesn't exist in the Global Domain ${GLOBAL_DOMAIN}, it will be created using current system date"
    currentDate=$(date +%Y%m%d)
    echo "${currentDate}" > ${GLOBAL_DOMAIN}/vdate.int
   _verify_binary "Error creating new vdate file with date ${dateSet} at ${GLOBAL_DOMAIN}/vdate.int"
else
    actualDate=`cat ${GLOBAL_DOMAIN}/vdate.int`
    #verify date from ${GLOBAL_DOMAIN}/vdate.int
    checkDate=$(date +%Y%m%d -d ${actualDate})
    _verify_binary "Date from ${GLOBAL_DOMAIN}/vdate.int is not valid: ${checkDate}"

    _log_message INFORMATION "The vdate.int file in the Global Domain ${GLOBAL_DOMAIN} has the value ${actualDate}"
fi

#Check the option parameter sent to the script in order to define which date will be set in vdate.int
case ${optionVdate} in
    increment)
       #increment existing vdate.int value by one day.

       #dateSet=$(date +%Y%m%d --date "${actualDate} + 1 day")

       #check the current hour to decide whether it will increment the actual date by 0 or 1.
       incrementValue=0
       currentHourMinute=`date +%H%M`
       #if it is required to change the range of hour/minute to increment or not, it should be done here.
       if [[ $currentHourMinute>1300 ]] && [[ $currentHourMinute<2359 ]]
       then
          incrementValue=1
       fi

       #Call the perl script to increment X days to the current date.
       dateSet=$(perl -w $BATCH_HOME/scripts/xxadeo_increment_date.pl $incrementValue)
       _verify_binary "Could not increment date. dateSet: ${dateSet}"

       _log_message INFORMATION "${GLOBAL_DOMAIN}/vdate.int will be set with value ${dateSet}"
       echo ${dateSet} > ${GLOBAL_DOMAIN}/vdate.int
       _verify_binary "Error writing new vdate ${dateSet} to file ${GLOBAL_DOMAIN}/vdate.int"

       _log_message INFORMATION "${GLOBAL_DOMAIN}/vdate.int successfully updated with value ${dateSet}"
    ;;
    set)
       #set the date sent as a parameter in vdate.int
       #check if second parameter was sent with the date.
       if [[ -z ${dateSet} ]]
       then
           _log_message ERROR "The date parameter to be set is not valid"
          _clean_exit ${__CODE_GENERAL_SCRIPT_ERROR}
       else
           checkDate=$(date +%Y%m%d -d ${dateSet})
           _verify_binary "Parameter ${dateSet} is not valid: ${checkDate}"
           _log_message INFORMATION "${GLOBAL_DOMAIN}/vdate.int will be set with value ${dateSet}"
           echo ${dateSet}>${GLOBAL_DOMAIN}/vdate.int
           _verify_binary "Error writing new vdate ${dateSet} to file ${GLOBAL_DOMAIN}/vdate.int"
          _log_message INFORMATION "${GLOBAL_DOMAIN}/vdate.int successfully updated with value ${dateSet}"
       fi
    ;;
    reset)
        #set actual date in vdate.int
        dateSet=`date +%Y%m%d`
        _log_message INFORMATION "${GLOBAL_DOMAIN}/vdate.int will be set with value ${dateSet}"
        echo ${dateSet}>${GLOBAL_DOMAIN}/vdate.int
        _verify_binary "Error writing new vdate ${dateSet} to file ${GLOBAL_DOMAIN}/vdate.int"
        _log_message INFORMATION "${GLOBAL_DOMAIN}/vdate.int successfully updated with value ${dateSet}"
    ;;
   *)
       _log_message ERROR "Unknown parameter ${optionVdate}"
       _clean_exit ${__CODE_UNSUPPORTED_FUNCTIONALITY}
   ;;
esac

#export RPAS_TODAY=`cat ${GLOBAL_DOMAIN}/vdate.int`
#_log_message INFORMATION "RPAS_TODAY=${RPAS_TODAY}"

#
# Verify and exit
#
_log_message INFORMATION "Finished running ${scriptName}"
_verify_log
_clean_exit SUCCESS

##############################################################################
## SPECIFIC SCRIPT SECTION END                                              ##
##############################################################################
