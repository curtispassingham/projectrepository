#!/bin/ksh
#
# Copyright © 2018, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1 
# $Date: 2018/09/05
# $Author: Oracle Retail Consulting
#
# Version              Change                         Author
# 0.1                  Initial Create                 Oracle Retail Consulting
#
##############################################################################
# orc_run_mace.sh
#
# Description: 
#   This wrapper script executes the java command to inherit workbook 
#   access rights from group level defaults.
#
##############################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"
utilLibFile="${O_APP_INSTANCE}/batch/lib/orc_util_library.ksh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    . ${utilLibFile}
else
    # Cannot rely on BSA functions here - script might be called from a blank environment
    #${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    echo  "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}


##############################################################################
## Function: usage                                                          ##
## Desc: Display the Usage information                                      ##
##--------------------------------------------------------------------------##

usage ()
{
    ${PRINT} "Usage: $scriptName [RULE_CONTROL_FILE] {-h}"
    ${PRINT} "eg: $scriptName run_post_attribute_calcs.rgrp"
    ${PRINT} "eg: $scriptName -h"
}

##############################################################################
## Retrieve parameters                                                      ##
##--------------------------------------------------------------------------##

if [[ ${numParams} -gt 0 && "$1" = -* ]]
then
   case "$1" in
   # the -help option displays usage
   -[Hh]|-[Hh][Ee][Ll][Pp]) usage "Display help"
             _clean_exit SUCCESS
                         ;;
                      *) usage "Display help"
             _clean_exit GENERAL_SCRIPT_ERROR
                         ;;
   esac
fi

# Validate the Number of Paramters
_check_arg_count ${numParams} 0 1

#logPath=$(_get_log_path)
#_log_message INFORMATION "Log file: "${logPath}

##############################################################################
## GENERAL SECTION COMPLETE                                                 ##
##############################################################################


##############################################################################
## SPECIFIC SCRIPT SECTION START                                            ##
##############################################################################

jarFile=InheritWbAccessFromGroups.jar
commandFile="${BATCH_HOME}/scripts/${jarFile}"

if [ ! -r ${commandFile} ]
then
    _log_message ERROR "Cannot read jar file  $commandFile"
    _clean_exit ${__CODE_FILE_NOT_FOUND}
fi 

##############################################################################
## Main Procedure                                                           ##
##--------------------------------------------------------------------------##

_log_message INFORMATION "Starting of ${scriptName}"
_log_message INFORMATION "BSA_MAX_PARALLEL=${BSA_MAX_PARALLEL}"

# Read vdate from global domain and set RPAS_TODAY
#orc_set_RPAS_TODAY

_log_message INFORMATION "Script was called with num params $#"
_log_message INFORMATION "Script was called with params $*."

# Validate the Number of Paramters
_check_arg_count ${numParams} 0 1

_log_message INFORMATION "Calling $commandFile with params $*."
_call java -jar $commandFile ${GLOBAL_DOMAIN} $*
_verify_binary "${jarFile} reported error"

_log_message INFORMATION "Finished inheriting user access rights."

#
# Verify and exit
#
_log_message INFORMATION "Finished running ${scriptName}"
_verify_log
_clean_exit SUCCESS

##############################################################################
## SPECIFIC SCRIPT SECTION END                                              ##
##############################################################################
