#!/bin/ksh
#
# Copyright © 2004, 2013, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2018/09/12
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
#
#########################################################################
#
# File:  orc_translation_to_measurerange.sh
# Application Version: none
#
# Syntax: orc_translation_to_measurerange.sh
#
# Description:
# The purpose of the script is to update the range of a picklist (r_measpicklist) based on the label translations file.
#
# The script reads a control file that contains a list of triplets:
#   1. Data file names
#   2. Destination measure-range names
#   3. Optional hierarchy indicator
#   4. File Name to produce (it should be named r_measpicklist as it is the name that RPAS recognizes for the picklist range measures translation)
#   5. Timestamp to append as a suffix into the file name to produce (to ensure unicity in case of multiple data files to the same r_measpicklist)
#
# For each member of the list the script:
# 1. Reads the contents of the translation input file
# 2. Converts the content to valid RPAS measure-range string format: measure,language,"id1,(label1),id2,(label2)"
# 3. Create a file with all the translations from step 2
# 3. Loads the produced file into the laguange database using the File name to produce parameter
#
# The script receives two parameters, the first one is the control file that contains the translation files and picklist measures.
#
# Calls: loadmeasure
#
########################################################################
scriptName=${0##/*/}
numParams=$#

# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    ${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi
#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    #. orc_util_library.sh
else
    ${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*********************************************************************"
    echo "  Script to load dimensions picklist specified by given control file "
    echo "  Usage:                                                     "
    echo "*********************************************************************"
    echo "  ${scriptName} [controlFileName]                                    "
    echo "  ${scriptName} [-h |-help]                                          "
    echo "  -help  - print usage information and exit                          "
    echo "*********************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
#if [[ ${numParams} -gt 0 ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 1 2

controlFile=$1
measureNameParam=$2

#
# Begin main processing
#

#read list of measure lines in the control file
importList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -f2 -d "|"  `

#If a parameter was passed with the measure name, the script will just process this measure.
if [[ -z ${measureNameParam} ]]
then
   _log_message INFORMATION "The list of measure to be processed is: \n$importList"
else
   #verify if the measure parameter exists in the control file.
   importList=`echo $importList | grep $measureNameParam`
   if [[ -z ${importList} ]]
   then
      _log_message ERROR "The $measureNameParam measure's record was not found in the control file ${BATCH_HOME}/config/imports/data/${controlFile}"
      _clean_exit ${__CODE_FILE_NOT_FOUND}
   else
      importList=$measureNameParam
      _log_message INFORMATION "The measure to be processed is: $importList"
   fi
fi

#start processing the list of measures
if [[ -z ${importList} ]]
then
   _log_message ERROR "No measures found in the control file ${BATCH_HOME}/config/imports/data/${controlFile} to be imported"
   _clean_exit 1
else
   #for each measure, get all measure(s) parameters from the control file to prepare for the loadmeasure command.
    for meas in $importList; do
   
       _log_message INFORMATION "Start processing $meas"
         #get measure name to load pick list
         measureName=`grep -w ${meas} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 2 `
         if [[ -n ${measureName} ]]; then
            _log_message INFORMATION "Measure name for picklist is ${measureName} in ${controlFile}"
         else
            _log_message INFORMATION "Measure name not found in control file "${BATCH_HOME}/config/imports/data/${controlFile}""
            _clean_exit ${__CODE_GENERAL_SCRIPT_ERROR}
         fi

         dimTranslFile=`grep -w ${meas} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 1 `
	     _log_message INFORMATION "Searching for ${EXTERNAL_INBOUND_DIR}/${dimTranslFile}"
         if [[ -f ${EXTERNAL_INBOUND_DIR}/${dimTranslFile} ]]
         then
 
            ignorevalues=`grep -w ${meas} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 4 | sed 's/;/|/g'`
            fileNameDateFormat=`grep -w ${meas} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 5`
            currentDateExp=`date ${fileNameDateFormat}`
            outputFileName=r_measpicklist.csv.ovr.${currentDateExp}

            _log_message INFORMATION "Data file ${dimTranslFile} located in ${EXTERNAL_INBOUND_DIR}/"
            _log_message INFORMATION "Transforming dimension values for data file ${EXTERNAL_INBOUND_DIR}/${dimTranslFile} into picklist for measure ${measureName}"
			
            for language in $(awk -F , '{ print $2}' "${EXTERNAL_INBOUND_DIR}/${dimTranslFile}" | sort -u); do
 
                if [[ -n ${ignorevalues} ]]; then
                    _log_message INFORMATION "Creating picklist for the ${language} language."
                    _log_message INFORMATION "The values to ignore in the picklist for the measure ${measureName} in ${controlFile} are: ${ignorevalues}"				
                    cat "${EXTERNAL_INBOUND_DIR}/${dimTranslFile}" | grep -wi "${language}" | awk 'BEGIN{IGNORECASE=1} '"${ignorevalues}"'' | awk -F , '{ printf "%s%s(%s)", c, tolower($1), $3; c = ","} END { print "\""}' | sed -e "s/^/${meas},${language},\"/" >> "${GLOBAL_DOMAIN}/input/${outputFileName}"
                   _verify_binary
               else
                   _log_message INFORMATION "Creating picklist for the ${language} language."
                   _log_message INFORMATION "No values to ignore in the picklist"				
                   cat "${EXTERNAL_INBOUND_DIR}/${dimTranslFile}" | grep -wi "${language}" | awk -F , '{ printf "%s%s(%s)", c, tolower($1), $3; c = ","} END { print "\""}' | sed -e "s/^/${meas},${language},\"/" >> "${GLOBAL_DOMAIN}/input/${outputFileName}"
                   _verify_binary
               fi
           
            done

            _log_message INFORMATION "Picklist creation for measure ${measureName} finished."
               
         else
            
             _log_message ERROR "No data file ${dimTranslFile} located in ${EXTERNAL_INBOUND_DIR}/"
             _clean_exit ${__CODE_FILE_NOT_FOUND}
         fi
   done
   
   _log_message INFORMATION "Loading translation picklist in ${GLOBAL_DOMAIN}."

   RPAS_LOG_LEVEL=$($PRINT ${RPAS_LOG_LEVEL} | tr "[:lower:]" "[:upper:]")

   _call  loadmeasure -d "${GLOBAL_DOMAIN}" -measure r_measpicklist -loglevel $RPAS_LOG_LEVEL
   _verify_binary "Load Measure reported error during load: loadmeasure -d ${GLOBAL_DOMAIN} -measure r_measpicklist -loglevel $RPAS_LOG_LEVEL"

   _log_message INFORMATION "Done loading translation picklist in ${GLOBAL_DOMAIN}"
   
fi

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS
