#!/bin/ksh
#
# Copyright © 2018, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1 
# $Date: 2018/04/13
# $Author: Oracle Retail Consulting
#
# Version              Change                         Author
# 0.1                  Initial Create                 Oracle Retail Consulting
#
##############################################################################
# orc_run_mace.sh
#
# Description: 
#   This wrapper script executes the mace expressions, rules, rulegroups
#   defined in the rule control file.
#
##############################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"
utilLibFile="${O_APP_INSTANCE}/batch/lib/orc_util_library.ksh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    . ${utilLibFile}
else
    # Cannot rely on BSA functions here - script might be called from a blank environment
    #${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    echo  "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}



##############################################################################
## Function: usage                                                          ##
## Desc: Display the Usage information                                      ##
##--------------------------------------------------------------------------##

usage ()
{
    ${PRINT} "Usage: $scriptName [RULE_CONTROL_FILE] {-h}"
    ${PRINT} "eg: $scriptName run_post_attribute_calcs.rgrp"
    ${PRINT} "eg: $scriptName -h"
}

##############################################################################
## Retrieve parameters                                                      ##
##--------------------------------------------------------------------------##

if [[ ${numParams} -gt 0 && "$1" = -* ]]
then
   case "$1" in
   # the -help option displays usage
   -[Hh]|-[Hh][Ee][Ll][Pp]) usage "Display help"
             _clean_exit SUCCESS
                         ;;
                      *) usage "Display help"
             _clean_exit GENERAL_SCRIPT_ERROR
                         ;;
   esac
fi

# Validate the Number of Paramters
_check_arg_count ${numParams} 1 1

##############################################################################
## GENERAL SECTION COMPLETE                                                 ##
##############################################################################

##############################################################################
## SPECIFIC SCRIPT SECTION START                                            ##
##############################################################################

ruleExecutionFile="${BATCH_HOME}/config/rulegroups/${1}"

if [ ! -r ${ruleExecutionFile} ]
then
    _log_message ERROR "Cannot read control $ruleExecutionFile"
    _clean_exit ${__CODE_FILE_NOT_FOUND}
fi 

##############################################################################
## Function: determineMaceJobAndExecute                                     ##
## Desc: Looks at the configuration file and determines what needs to be    ##
##       executed.                                                          ##
##--------------------------------------------------------------------------##

determineMaceJobAndExecute ()
{
    #Assign members of the split array to variables
    O_AWKSEP="|"
    jobTybe=$($PRINT ${1}|$AWK -F"${O_AWKSEP}" '{print $1}')
    _verify_binary
    jobLevel=$($PRINT ${1}|$AWK -F"${O_AWKSEP}" '{print $2}')
    _verify_binary
    jobValue=`echo ${1} | cut -d '|' -f3-`
    _verify_binary
    origJobValue=""
    

    _log_message INFORMATION "Executing mace ${jobValue}"

    if [ "${jobTybe}" == "group" ]
    then
        _log_message INFORMATION "jobTybe=group"
    elif [ "${jobTybe}" == "expression" ]
    then
        _log_message INFORMATION "jobTybe=expression"
    elif [ "${jobTybe}" == "rule" ]
    then
        _log_message INFORMATION "jobTybe=rule"
        origJobValue=${jobValue}
        lowerparam=`echo ${origJobValue} | tr [:upper:] [:lower:]`
        jobValue=`printArray -array ${GLOBAL_DOMAIN}/data/measdata.rules__ -cellplain rule:${lowerparam},expr:e0 -cellsperrow 1 -loglevel error`
        _verify_binary "Cannot determine jobValue for $origJobValue : jobValue: ${jobValue}"
        _log_message INFORMATION "Completed finding rule for ${origJobValue}"
    else
        _log_message ERROR "Unknown Jobtype found '${jobTybe}'"
        _clean_exit ${__CODE_UNSUPPORTED_FUNCTIONALITY}
    fi

    if [ "${jobLevel}" == "G" ]
    then
        if [  "${jobTybe}" == "group" ]
        then
            _call mace -d $GLOBAL_DOMAIN -run -group $jobValue
            _verify_binary "mace reported error during exection of group $jobValue in global domain $GLOBAL_DOMAIN"
        else
            _call mace -d $GLOBAL_DOMAIN -run -expression "$jobValue"
            _verify_binary "mace reported error during exection of expression \"${jobValue}\" in global domain $GLOBAL_DOMAIN"
        fi
    elif [ "${jobLevel}" == "L" ]
    then
        ldoms=$(domaininfo -d ${GLOBAL_DOMAIN} -shellsubdomains -loglevel none)
        for ldom in ${ldoms}
        do
            _log_message INFORMATION "Executing in ${ldom}"
	    if [  "${jobTybe}" == "group" ]
            then 
                _para_spawn mace -d ${ldom} -run -group $jobValue
            else
                _para_spawn mace -d ${ldom} -run -expression \"${jobValue}\"
            fi
        done
        _para_wait
        _verify_binary
    else
        _log_message ERROR "Incorrect parameter passed. Needs to be 'G' or 'L'"
        _clean_exit ${__CODE_UNSUPPORTED_FUNCTIONALITY}    
    fi
    if [[ -n ${origJobValue}  && -n $origJobValue ]]; then
        _log_message INFORMATION "Restoring original job value for logging purposes"
        jobValue=${origJobValue} 
    fi
    _log_message INFORMATION "Finished executing mace ${jobValue}"
    
}

##############################################################################
## Main Procedure                                                           ##
##--------------------------------------------------------------------------##

_log_message INFORMATION "Starting of ${scriptName}"
_log_message INFORMATION "BSA_MAX_PARALLEL=${BSA_MAX_PARALLEL}"

# Read vdate from global domain and set RPAS_TODAY
orc_set_RPAS_TODAY

#_orc_checkFilePath ${ruleExecutionFile}
#_orc_checkGlobalDomainPath ${GLOBAL_DOMAIN}

_log_message INFORMATION "Running mace(s) defined in the ${ruleExecutionFile} file."

while read -r job || [[ "$job" ]] 
do
    # check if line contains comments
    if [[ ${job:0:1} == "#" ]]; then
	_log_message INFORMATION "Ignoring commented line $job"
        continue
    fi
    
    # ignore blank line
    if [[ -z ${job} ]]; then
	_log_message INFORMATION "Ignoring blank line $job"
        continue
    fi

    determineMaceJobAndExecute "${job}"
done < ${ruleExecutionFile}

_log_message INFORMATION "Finished executing mace(s) defined in ${ruleExecutionFile} file."

#
# Verify and exit
#
_log_message INFORMATION "Finished running ${scriptName}"
_verify_log
_clean_exit SUCCESS

##############################################################################
## SPECIFIC SCRIPT SECTION END                                              ##
##############################################################################
