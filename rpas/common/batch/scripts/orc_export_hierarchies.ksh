#!/bin/ksh
#
# Copyright © 2018, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2018/05/03
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
#
#########################################################################
#
# File: orc_export_hierarchies.ksh 
# Application Version: none
#
# Syntax: orc_export_hierarchies.ksh controlFile
#
# Description:
#   Exports hierarchies from the domain specified by environment variables. This script takes two
#   parameters; the first one is the given control file specifies which hierarchies will be exported and required information to export them
#   (output file,output directory ...etc).
#   The second optional parameter is the export set name inside the control file.
#
# Calls: exportHier
#
########################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
else
    #${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    # Cannot rely on BSA functions here - lib file not found
    echo "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 9
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "****************************************************************"
    echo "  Script to export hierarchies specified by given control file  "
    echo "  Usage:                                                        "
    echo "****************************************************************"
    echo "  ${scriptName} [controlFileName]  [exportSetName]              "
    echo "  ${scriptName} [-h |-help]                                     "
    echo "  -help  - print usage information and exit                     "
    echo "****************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 1 2

controlFile=$1
exportSetParam=$2

#
# Begin main processing
#
if [[ ! -f "${BATCH_HOME}/config/exports/${controlFile}" ]]; then
   _log_message INFORMATION "$Control file {BATCH_HOME}/config/exports/${controlFile} does not exist!"
  _clean_exit ${__CODE_FILE_NOT_FOUND}
fi

#read list of export sets in the control file
exportList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/exports/${controlFile}" | cut -f1 -d "|" | sort -u `
_verify_binary "Could not determine exportList..."

#If a parameter was passed with the export Set Name, the script will just process this export Set.
if [[ -z ${exportSetParam} ]]
then
   _log_message INFORMATION "The list of export sets to be processed is: \n$exportList"
else
   #verify if the export set parameter exists in the control file.
   exportList=`echo $exportList | grep $exportSetParam`
   if [[ -z ${exportList} ]]
   then
      _log_message ERROR "The export set $exportSetParam record was not found in the control file ${BATCH_HOME}/config/exports/${controlFile}"
      _clean_exit ${__CODE_FILE_NOT_FOUND}
   else
      exportList=$exportSetParam
      _log_message INFORMATION "The export set to be processed is: $exportList"
   fi
fi

#start processing the list of exportSets
if [[ -z ${exportList} ]]
then
   _log_message ERROR "No measures found in the control file ${BATCH_HOME}/config/exports/${controlFile} to be exported"
   _clean_exit ${__CODE_TOO_FEW_ARGS}
else
   #for each export set, get all export parameters from the control file to prepare for the exportMeasure command.
   for exportSet in ${exportList}; do
      _log_message INFORMATION "Exporting ${exportSet} measure(s) from ${GLOBAL_DOMAIN}."

      #get export parameters for each export set found in the control file.
      #exportSetParameters=`grep "$exportSet|" "${BATCH_HOME}/config/exports/${controlFile}" | grep -vE '^(#|$)'`
      hierName=""
      outputDirectory=""
      outputFileName=""
      fileNameDateFormat=""
      optionalFlags=""

      grep "$exportSet|" "${BATCH_HOME}/config/exports/${controlFile}" | grep -vE '^(#|$)' | while read -r exportSetParameters ; do
          if [[ -z ${exportSetParameters} ]]
          then
             _log_message INFORMATION "Warning: no parameters defined for $exportSet. Ignoring export set"
             continue
          fi
          
          
          exportSetParameterType=`echo $exportSetParameters | cut -f2 -d "|"`
          exportSetParameterValue=`echo $exportSetParameters | cut -f3 -d "|"`

          #Verify the parameter type in order to create the correct argument to the export command.
          case ${exportSetParameterType} in
             H)
                hierName="${exportSetParameterValue}"
                ;;
             F)
                optionalFlags="${exportSetParameterValue}"
                ;;
             D)
                outputDirectory="${exportSetParameterValue}"
                ;;
             O)
                outputFileName="${exportSetParameterValue}"
                ;;
             T)
                fileNameDateFormat=${exportSetParameterValue}
                ;;
             *)
                _log_message ERROR "Unknown parameter |${exportSetParameterValue}|"
                _clean_exit ${__CODE_UNSUPPORTED_FUNCTIONALITY}
                ;;
          esac
      done
      #Verify whether the required parameters have values or not.
      if [[ -z ${hierName} || -z ${outputFileName} ]]
      then
         _log_message INFORMATION "Warning: parameters for export set $exportSet are not valid. hierName is ${hierName}. Ignoring export set"
      else
         #set default output directory if it doesn't exist in the control file.
         if [[ -z ${outputDirectory} ]] then
            outputDirectory=${EXTERNAL_OUTBOUND_DIR}
         fi
         #append date in the specified format, if found in the control file to the file name.
         if [[ ! -z ${fileNameDateFormat} ]] then
            currentDateExp=`date ${fileNameDateFormat}`
            outputFileName=${outputFileName}${currentDateExp}
         fi
         RPAS_LOG_LEVEL=$($PRINT ${RPAS_LOG_LEVEL} | tr "[:lower:]" "[:upper:]")
         _call exportHier -d "${GLOBAL_DOMAIN}" -hier "${hierName}" -loglevel "${RPAS_LOG_LEVEL}" -datFile ${outputDirectory}/${outputFileName} ${optionalFlags}
         _verify_binary "exportHier error during export of ${hierName}"
         _log_message INFORMATION "Done exporting ${exportSet} hier from ${GLOBAL_DOMAIN}"
      fi
   done # end of for loop (over export sets defined in control file and potentially limited by second parameter exportSetParam
   
fi

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS
