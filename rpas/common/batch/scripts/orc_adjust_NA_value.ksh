#CP EDIT 9

#!/bin/ksh
#
# Copyright © 2018 Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2018/08/23
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
#
#########################################################################
#
# File:  orc_adjust_NA_value.ksh
# Application Version: none
#
# Syntax: orc_adjust_NA_value.ksh control_file
#
# Description:
#   Script to adjust measure NA values on the fly.
#   
#   Should be used only for measures that are NOT part of the configuration, i.e. platform or plug-in generated
#   Default (NA) value for position level security meaures is true. That means new positions
#   are accessible by default until an administrator logs in and adjusts access rights. 
#  
#   
# Calls: populateArray and syncNAValue
#
########################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    #. orc_util_library.sh
else
    #${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    exit 9
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to load measures specified by given control file    "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName} [controlFileName] [measureFilename]          "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 1 1

controlFileName=$1

#
# Begin main processing
#

# verify that control file exists
if [[ ! -f ${BATCH_HOME}/config/measures/${controlFileName} ]]
then
   _log_message ERROR "Cannot access control file ${BATCH_HOME}/config/measures/${controlFileName}"
   _clean_exit ${__CODE_FILE_NOT_FOUND}
else
   controlFile=${BATCH_HOME}/config/measures/${controlFileName}
fi

# get list of measures from control file
measureList="$(grep -vE '^(#|$)' "${controlFile}" | cut -f1 -d "|" | tr "\n" " " ) "
_verify_binary "Cannot determine measureList"

_log_message INFORMATION "Measure list: $measureList"

#start processing 

for meas in $measureList ; do 
    _log_message INFORMATION "Processing measure ${meas}"
    # read params for measure 
    dataType=$(grep -vE '^(#|$)' "${controlFile}" | grep -w ${meas} | cut -d "|" -f 2)
    _verify_binary "Cannot determine data type for measure ${meas}"
    naValue=$(grep -vE '^(#|$)' "${controlFile}" | grep -w ${meas} | cut -d "|" -f3-)
    _verify_binary "Cannot determine NA value for measure ${meas}"
    
    _log_message INFORMATION "Properties for measure ${meas}"
    _log_message INFORMATION "Data type: ${dataType}"
    _log_message INFORMATION "NA value : ${naValue}"

    _call $RPAS_HOME/bin/.private/populateArray -array $GLOBAL_DOMAIN/data/measdata.measinfo -set "info:navalue,meas:$meas" -type $dataType -value $naValue
    _verify_binary "$RPAS_HOME/.private/populateArray"

    _call syncNAValue -d $GLOBAL_DOMAIN -m $meas
    _verify_binary "syncNAValue failed for measure $meas"
done

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log

_clean_exit SUCCESS
