#!/bin/ksh
#
# Copyright © 2016, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1 
# $Date: 2018/05/24
# $Author: Oracle Retail Consulting
#
# Version              Change                         Author
# 0.1                  Initial Create                 Oracle Retail Consulting
#
##############################################################################
# Syntax: orc_update_vdate.ksh
#
# Description: 
#   This script will stop Domain Daemon, if there are active users, it will
#   wait for some minutes, if users are still active, their processes are killed

##############################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"


which ${importLibFile} > /dev/null 2>&1
if [ "$?" -eq 0 ]
then
    . ${importLibFile}
    #. orc_util_library.sh
else
    # Cannot rely on BSA functions here - lib file not found
    echo "Could not find ${O_APP_INSTANCE}/batch/${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 9
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}
logDir=$(dirname ${logPath})
daemonLogPath=${O_LOG_HOME}/daemon
mkdir -p ${daemonLogPath}
_log_message INFORMATION "Daemon log path: ${daemonLogPath}"
RPAS_LOG_PATH=${daemonLogPath}

##############################################################################
## Function: usage                                                          ##
## Desc: Display the Usage information                                      ##
##--------------------------------------------------------------------------##

usage ()
{
    ${PRINT} "Usage: $scriptName{-h}"
    ${PRINT} "eg: $scriptName "  
    ${PRINT} "eg: $scriptName -h"
}

##############################################################################
## Retrieve parameters                                                      ##
##--------------------------------------------------------------------------##

if [[ ${numParams} -gt 0 ]]
then
   case "$1" in
   # the -help option displays usage
   -[Hh]|-[Hh][Ee][Ll][Pp]) usage "Display help"
             _clean_exit SUCCESS
                         ;;
                      *) 
                         ;;
   esac
fi

# Validate the Number of Paramters
_check_arg_count ${numParams} 1 1

controlFile=$1
#
# Begin main processing
#
_log_message INFORMATION "RPAS_TODAY=${RPAS_TODAY}"

daemonPort=""
walletFile=""
waitTime=""
numWaits=""
stopMessage=""
domainList=""
sslLevel=""

if [[ -f "${BATCH_HOME}/environment/${controlFile}" ]]
then
   daemonInfo=$(cat ${BATCH_HOME}/environment/${controlFile})
   daemonPort=$(echo ${daemonInfo}|cut -f1 -d "|")
   walletFile=$(echo ${daemonInfo}|cut -f2 -d "|")
   waitTime=$(echo ${daemonInfo}|cut -f3 -d "|")
   numWaits=$(echo ${daemonInfo}|cut -f4 -d "|")
   stopMessage=$(echo ${daemonInfo}|cut -f5 -d "|")
   sslLevel=$(echo ${daemonInfo}|cut -f6 -d "|")
   domainList=$(echo ${daemonInfo}|cut -f7 -d "|" | tr "," " ")

   # check if total wait time from file will be at least 60 seconds
   typeset -i totalWaitTime
   let "totalWaitTime=${waitTime} * ${numWaits}"

   if [[ ${totalWaitTime} -lt 65 ]]; then
	_log_message WARNING "Wait time from file not sufficent. Using default values instead"
        waitTime=6
        numWaits=11
   fi

else
   _log_message ERROR "control file ${BATCH_HOME}/environment/${controlFile} not found"
   _clean_exit ${__CODE_FILE_NOT_FOUND}
fi

# Stop active servers before putting DomainDaemon down
#_log_message INFORMATION "Stopping active users"
#_call DomainDaemon -port ${daemonPort} -stopActiveServers "${stopMessage}" -wallet ${walletFile}

# Deactivate domains in domain list
for domain in ${domainList}; do
    baseDomainName=$(basename ${domain})
    #_call nohup DomainDaemon -port ${daemonPort} -deactivate ${domain} "${stopMessage}" -wallet ${walletFile} > ${daemonLogPath}/DomainDaemon_deactivate_${O_INSTANCE}${baseDomainName}.log &
    _call nohup DomainDaemon -port ${daemonPort} -ssl ${sslLevel} -deactivate ${domain} "${stopMessage}" > ${daemonLogPath}/DomainDaemon_deactivate_${O_INSTANCE}${baseDomainName}.log &
    _verify_binary "Could not deactivate domain ${domain}"
done

typeset -i waitcount
let "waitcount=0"
while [ ${waitcount} -le ${numWaits} ]; do
    numDeactivateProcs=$(ps -ef | grep "DomainDaemon -port ${daemonPort} -deactivate" | grep -v "grep" | wc -l)
    _log_message INFORMATION "Found # ${numDeactivateProcs} processes."
    _log_message INFORMATION "Num waits is ${numWaits} and current wait count is ${waitcount}"
    if [[ ${numDeactivateProcs} -gt 0 ]];then
        _log_message INFORMATION "Still waiting to deactive domains. Will sleep for ${waitTime} seconds"
        let "waitcount=waitcount + 1"
        sleep ${waitTime}
    else
        _log_message INFORMATION "All domains deactivated"
        break
    fi
done

# Verify if there are active processes
_log_message INFORMATION "Verifying if sessions processes are still active in RPAS Server"
#activePrcs=`ps ax | grep RpasDbServer | grep ${walletFile} |awk '{$1=$1};1'| cut -d" " -f1`
activePrcs=`ps ax | grep RpasDbServer | grep ${daemonPort} |awk '{$1=$1};1'| cut -d" " -f1`
if [[ ! -z ${activePrcs} ]]
then
   #wait some seconds before killing the processes
   _log_message WARNING "Waiting ${waitTime} seconds before stopping active users processes: \n${activePrcs}"
   sleep ${waitTime}
else
   _log_message INFORMATION "No more active users found, proceeding to stop DomainDaemon"
fi

# trying to stop RpasDbServers politely
#activePrcs=`ps ax | grep RpasDbServer | grep ${walletFile} |awk '{$1=$1};1'| cut -d" " -f1`
activePrcs=`ps ax | grep RpasDbServer | grep ${daemonPort} |awk '{$1=$1};1'| cut -d" " -f1`
if [[ ! -z ${activePrcs} ]]
then
   _log_message INFORMATION "Trying to stop active users processes: \n${activePrcs}"
   for activeProcess in ${activePrcs}
   do
      StopServerLogFile="${logDir}_DD_StopServer_${activeProcess}.log"
      _log_message INFORMATION "Calling DomainDaemon to stop server ${activeProcess}. See log file  ${StopServerLogFile} for details."
      #_call nohup DomainDaemon -port ${daemonPort} -wallet ${walletFile} -stopServer ${activeProcess} > ${StopServerLogFile} &
      _call nohup DomainDaemon -port ${daemonPort} -stopServer ${activeProcess} > ${StopServerLogFile} &
   done
   # wait for stop server commnds
   let "waitcount=0"
   while [ ${waitcount} -le ${numWaits} ]; do
      let "waitcount=waitcount + 1"
      #numStopServerCommands=$(ps -ef | grep "DomainDaemon -port ${daemonPort} -wallet ${walletFile} -stopServer)" | grep -v "grep" | wc -l)
      numStopServerCommands=$(ps -ef | grep "DomainDaemon -port ${daemonPort} -stopServer)" | grep -v "grep" | wc -l)
      if [[ ${numStopServerCommands} -gt 0 ]];then
          _log_message INFORMATION "Still trying to stop servers. Wait count is ${waitcount}, numwaits is  ${numWaits}"
          _log_message INFORMATION "Waiting for ${waitTime} seconds"
          sleep ${waitTime}
      else
          _log_message INFORMATION "Stop server commands finished"
          break
      fi
   done
fi

_log_message WARNING "Waiting ${waitTime} seconds before checking for still active users processes"
sleep ${waitTime}


# start killing all the processes
#activePrcs=`ps ax | grep RpasDbServer | grep ${walletFile} |awk '{$1=$1};1'| cut -d" " -f1`
activePrcs=`ps ax | grep RpasDbServer | grep ${daemonPort} |awk '{$1=$1};1'| cut -d" " -f1`
if [[ ! -z ${activePrcs} ]]
then
   _log_message WARNING "start killing active users processes: \n${activePrcs}"
   for activeProcess in ${activePrcs}
   do
      _call kill ${activeProcess}
   done
fi

_log_message WARNING "Waiting ${waitTime} seconds before checking for still active users processes. Remaining processes will be killed the hard way."
sleep ${waitTime}

# start killing all the processes
#activePrcs=`ps ax | grep RpasDbServer | grep ${walletFile} |awk '{$1=$1};1'| cut -d" " -f1`
activePrcs=`ps ax | grep RpasDbServer | grep ${daemonPort} |awk '{$1=$1};1'| cut -d" " -f1`
if [[ ! -z ${activePrcs} ]]
then
   _log_message WARNING "start killing active users processes: \n${activePrcs}"
   for activeProcess in ${activePrcs}
   do
      _log_message WARNING "Killing process using kill -9 ${activeProcess}"
      #_call kill -9 ${activeProcess}
   done
fi

# Stop Domain Daemon
_log_message INFORMATION "Stop DomainDaemon"
#_call DomainDaemon -port ${daemonPort} -stop -wallet ${walletFile}
_call DomainDaemon -port ${daemonPort} -stop -ssl ${sslLevel}
_verify_binary

#
# Verify and exit
#
_log_message INFORMATION "Finished running ${scriptName}"
_verify_log
_clean_exit SUCCESS

##############################################################################
## SPECIFIC SCRIPT SECTION END                                              ##
##############################################################################

