#!/bin/ksh
#
# Copyright © 2018, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2018/04/13
# $Author: Oracle Retail Consulting
#
# Version              Change                         Author
# 0.1                  Initial Create                 Oracle Retail Consulting
#
##############################################################################
# orc_load_constants.sh
#
# Description:
#   This wrapper script executes the mace expressions based on a control file
#   to fill in hardcoded values in specific measures.
#   It was decided it will run ad-hoc
#
##############################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"
utilLibFile="${O_APP_INSTANCE}/batch/lib/orc_util_library.ksh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    . ${utilLibFile}
else
    ${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo  "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi


##############################################################################
## Function: usage                                                          ##
## Desc: Display the Usage information                                      ##
##--------------------------------------------------------------------------##

usage ()
{
    ${PRINT} "Usage: $scriptName [RULE_CONTROL_FILE] {-h}"
    ${PRINT} "eg: $scriptName Cst_Load_Costants.ctl"
    ${PRINT} "eg: $scriptName -h"
}

##############################################################################
## Retrieve parameters                                                      ##
##--------------------------------------------------------------------------##

if [[ ${numParams} -gt 0 && "$1" = -* ]]
then
   case "$1" in
   # the -help option displays usage
   -[Hh]|-[Hh][Ee][Ll][Pp]) usage "Display help"
             _clean_exit SUCCESS
                         ;;
                      *) usage "Display help"
             _clean_exit GENERAL_SCRIPT_ERROR
                         ;;
   esac
fi

# Validate the Number of Paramters
_check_arg_count ${numParams} 1 1

##############################################################################
## GENERAL SECTION COMPLETE                                                 ##
##############################################################################

##############################################################################
## SPECIFIC SCRIPT SECTION START                                            ##
##############################################################################

ruleExecutionFile="${BATCH_HOME}/config/imports/data/${1}"

if [ ! -r ${ruleExecutionFile} ]
then
    _log_message ERROR "Cannot read control $ruleExecutionFile"
    _clean_exit ${__CODE_FILE_NOT_FOUND}
fi


##############################################################################
## Main Procedure                                                           ##
##--------------------------------------------------------------------------##

_log_message INFORMATION "Starting of ${scriptName}"
_log_message INFORMATION "BSA_MAX_PARALLEL=${BSA_MAX_PARALLEL}"

# Read vdate from global domain and set RPAS_TODAY
orc_set_RPAS_TODAY

#_orc_checkFilePath ${ruleExecutionFile}
#_orc_checkGlobalDomainPath ${GLOBAL_DOMAIN}

_log_message INFORMATION "Running mace(s) defined in the ${ruleExecutionFile} file."


while read -r line; do
    check_line=$(echo ${line} | grep -cE '^(#|$)' )
    check_structure=$(echo ${line} | grep -c -e "^|" -e "||" -e "|$" )

    if [[ ${check_line} == 0  && ${check_structure} == 0 ]]; then
        meas=$(echo $line | cut -f1 -d "|")
        value=$(echo $line | cut -f2 -d "|")

        printMeasureInfo -d ${GLOBAL_DOMAIN} -measure ${meas} -all > /dev/null 2>&1
        if [[ $? == 0 ]]; then
            _call mace -d ${GLOBAL_DOMAIN} -run -expression "${meas} = \"${value}\""
            _verify_binary "mace reported an error during setting the value of ${meas} in the global domain: $GLOBAL_DOMAIN"
        else
            _log_message ERROR "Measure does not exist in the domain: ${meas}"
        fi
    else
        _log_message INFORMATION "Ignoring commented or empty line: ${line}"
    fi
done < ${ruleExecutionFile}


_log_message INFORMATION "Finished executing mace(s) defined in file: ${ruleExecutionFile}"

#
# Verify and exit
#
_log_message INFORMATION "Finished running: ${scriptName}"
_verify_log
_clean_exit SUCCESS

##############################################################################
## SPECIFIC SCRIPT SECTION END                                              ##
##############################################################################
