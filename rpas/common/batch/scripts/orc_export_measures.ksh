#!/bin/ksh
#
# Copyright © 2018, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2018/05/03
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
#
#########################################################################
#
# File:  orc_export_measures.sh
# Application Version: none
#
# Syntax: orc_export_measures.ksh
#
# Description:
#   Exports measures from the domain specified by environment variables. This script takes two
#   parameters; the first one is the given control file specifies which measures will be exported and required information to export them
#   (output file,output directory, measure(s) name(s), base intersection, Filter mask measure...etc).
#   The secont parameter is the export set name inside the control file.
#
# Calls: exportMeasure
#
########################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
else
    #${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    # Cannot rely on BSA functions here - lib file not found
    echo "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 9
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to export measures specified by given control file    "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName} [controlFileName]  [exportSetName]           "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 1 2

controlFile=$1
exportSetParam=$2

#
# Begin main processing
#
if [[ ! -f "${BATCH_HOME}/config/exports/${controlFile}" ]]; then
   _log_message INFORMATION "$Control file ${BATCH_HOME}/config/exports/${controlFile} does not exist!"
  _clean_exit ${__CODE_FILE_NOT_FOUND}
fi

#read list of export sets in the control file
exportList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/exports/${controlFile}" | cut -f1 -d "|" | sort -u `
_verify_binary "Could not determine exportList..."

#check if control file actually contains something to export
if [[ -z ${exportList} ]]
then
   _log_message WARNING "Ignoring empty control file"
   _verify_log
   _clean_exit ${__CODE_SUCCESS}
fi

#If a parameter was passed with the export Set Name, the script will just process this export Set.
if [[ -z ${exportSetParam} ]]
then
   _log_message INFORMATION "The list of export sets to be processed is: \n$exportList"
else
   #verify if the export set parameter exists in the control file.
   exportList=`echo $exportList | grep -w $exportSetParam`
   if [[ -z ${exportList} ]]
   then
      _log_message ERROR "The export set $exportSetParam record was not found in the control file ${BATCH_HOME}/config/exports/${controlFile}"
      _clean_exit ${__CODE_FILE_NOT_FOUND}
   else
      exportList=$exportSetParam
      _log_message INFORMATION "The export set to be processed is: $exportList"
   fi
fi

#start processing the list of exportSets
if [[ -z ${exportList} ]]
then
   _log_message ERROR "No measures found in the control file ${BATCH_HOME}/config/exports/${controlFile} to be exported"
   _clean_exit ${__CODE_TOO_FEW_ARGS}
else
   #for each export set, get all export parameters from the control file to prepare for the exportMeasure command.
   for exportSet in ${exportList}; do
      _log_message INFORMATION "Exporting ${exportSet} measure(s) from ${GLOBAL_DOMAIN}."

      #get export parameters for each export set found in the control file.
      exportSetParameters=`grep "$exportSet|" "${BATCH_HOME}/config/exports/${controlFile}" | grep -vE '^(#|$)'`
      if [[ -z ${exportSetParameters} ]]
      then
         _log_message INFORMATION "Warning: no parameters defined for $exportSet. Ignoring export set"
      else
         typeset -i firstMeasureName
         let firstMeasureName=0
         measureNames=""
         filterMaskMeasure=""
         baseIntersection=""
         outputDirectory=""
         outputFileName=""
         fileNameDateFormat=""
         skipNA=""
         useDate=""
         for exportSetParameter in ${exportSetParameters}; do
            exportSetParameterType=`echo $exportSetParameter | cut -f2 -d "|"`
            exportSetParameterValue=`echo $exportSetParameter | cut -f3 -d "|"`
            #Verify the parameter type in order to create the correct argument to the export command.
            case ${exportSetParameterType} in
               M)
                  if [[ $firstMeasureName -eq 0 ]] ; then
                     let firstMeasureName=1
                     measureNames=${exportSetParameterValue}
                  else
                     measureNames="${measureNames},${exportSetParameterValue}"
                  fi
                  ;;
               F)
                  filterMaskMeasure="-mask ${exportSetParameterValue}"
                  ;;
               X)
                  baseIntersection="-intx ${exportSetParameterValue}"
                  ;;
               D)
                  outputDirectory="${exportSetParameterValue}"
                  ;;
               O)
                  outputFileName="${exportSetParameterValue}"
                  ;;
               T)
                  fileNameDateFormat=${exportSetParameterValue}
                  ;;
               S)
                  skipNA="-skipNA ${exportSetParameterValue}"
                  ;;
               C)
                  useDate="-useDate ${exportSetParameterValue}"
                  ;;
               *)
                  _log_message ERROR "Unknown parameter ${exportSetParameterValue}"
                  _clean_exit ${__CODE_UNSUPPORTED_FUNCTIONALITY}
                  ;;
            esac
         done
         #Verify whether the required parameters have values or not.
         if [[ -z ${measureNames} ]]
         then
            _log_message INFORMATION "Warning: parameters for export set $exportSet are not valid. Ignoring export set"
         else
            #set default output directory if it doesn't exist in the control file.
            if [[ -z ${outputDirectory} ]] then
               outputDirectory=${EXTERNAL_OUTBOUND_DIR}
            else
                outputDirectory=${EXTERNAL_OUTBOUND_DIR}/${outputDirectory}
            fi
            #append date in the specified format, if found in the control file to the file name.
            if [[ ! -z ${fileNameDateFormat} ]] then
               currentDateExp=`date ${fileNameDateFormat}`
               outputFileName=${outputFileName}${currentDateExp}
            fi
            RPAS_LOG_LEVEL=$($PRINT ${RPAS_LOG_LEVEL} | tr "[:lower:]" "[:upper:]")
            #SAVE_UMASK=`umask -S`
            #umask -S u=rw,g=rw,o=
            _call exportMeasure -d "${GLOBAL_DOMAIN}" -meas "${measureNames}" -processes ${BSA_MAX_PARALLEL} -loglevel "${RPAS_LOG_LEVEL}" ${filterMaskMeasure} ${baseIntersection} ${skipNA} ${useDate} -out ${outputDirectory}/${outputFileName}
             
            _verify_binary "exportMeasure reported error during export of ${measureNames}"
            
            #umask ${SAVE_UMASK}
            #_verify_binary "Cannot restore umask to ${SAVE_UMASK}"
            
            _log_message INFORMATION "Done exporting ${exportSet} measure(s) from ${GLOBAL_DOMAIN}"
         fi
      fi
   done
fi

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS
