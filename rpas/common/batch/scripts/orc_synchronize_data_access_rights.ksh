#!/bin/ksh
#
# Copyright © 2018, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.3 
# $Date: 2018/10/11
# $Author: Oracle Retail Consulting
#
# Version              Change                         Author
# 0.1                  Initial Create                 Oracle Retail Consulting
# 0.2                  Addded parameter check         Oracle Retail Consulting
# 0.3                  New directory structure        Oracle Retail Consulting
#
##############################################################################
# orc_run_mace.sh
#
# Description: 
#   This wrapper script executes the java command to inherit workbook 
#   access rights from group level defaults.
#
##############################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"
utilLibFile="${O_APP_INSTANCE}/batch/lib/orc_util_library.ksh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    . ${utilLibFile}
else
    ${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo  "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi


##############################################################################
## Function: usage                                                          ##
## Desc: Display the Usage information                                      ##
##--------------------------------------------------------------------------##

usage ()
{
    ${PRINT} "Usage: $scriptName [RULE_CONTROL_FILE] {-h}"
    ${PRINT} "eg: $scriptName run_post_attribute_calcs.rgrp"
    ${PRINT} "eg: $scriptName -h"
}

##############################################################################
## Retrieve parameters                                                      ##
##--------------------------------------------------------------------------##

if [[ ${numParams} -gt 0 && "$1" = -* ]]
then
   case "$1" in
   # the -help option displays usage
   -[Hh]|-[Hh][Ee][Ll][Pp]) usage "Display help"
             _clean_exit SUCCESS
                         ;;
                      *) usage "Display help"
             _clean_exit GENERAL_SCRIPT_ERROR
                         ;;
   esac
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

# Validate the Number of Paramters
_check_arg_count ${numParams} 0 0

##############################################################################
## GENERAL SECTION COMPLETE                                                 ##
##############################################################################


##############################################################################
## SPECIFIC SCRIPT SECTION START                                            ##
##############################################################################

exportControlFileName=Com_Export_Position_Level_Security.ctl
exportControlFile="${BATCH_HOME}/config/exports/${exportControlFileName}"
exportScript=orc_export_measures.ksh
exportCommand="${BATCH_HOME}/scripts/${exportScript}"

importControlFileName=Com_Load_RHS_Position_Level_Security.ctl
importScript=orc_load_measures.ksh
importCommand="${BATCH_HOME}/scripts/${importScript}"

#verify export command exists and is executable
if [ ! -x ${exportCommand} ]
then
    _log_message ERROR "Cannot read/execute command file $exportCommand"
    _clean_exit ${__CODE_FILE_NOT_FOUND}
fi 


#verify export control file exists 
if [ ! -r ${exportControlFile} ]
then
    _log_message ERROR "Cannot read export control file $exportControlFile"
    _clean_exit ${__CODE_FILE_NOT_FOUND}
fi 

##############################################################################
## Main Procedure                                                           ##
##--------------------------------------------------------------------------##

_log_message INFORMATION "Starting of ${scriptName}"
_log_message INFORMATION "BSA_MAX_PARALLEL=${BSA_MAX_PARALLEL}"

# Read vdate from global domain and set RPAS_TODAY
#orc_set_RPAS_TODAY


# Validate the Number of Paramters
_check_arg_count ${numParams} 0 1

_log_message INFORMATION "Exporting position level access rights using as defined in control file ${exportControlFileName}."
_call ${exportCommand} ${exportControlFileName}
_verify_binary "${exportScript} reported error"

_log_message INFORMATION "Finished exporting user access rights."

importList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${importControlFileName}" | cut -f1 -d "|"  `

for importFile in ${importList};do
    
    _log_message INFORMATION "Moving file $importFile"
    
    mv ${EXTERNAL_OUTBOUND_DIR}/${importFile}.* ${EXTERNAL_INBOUND_DIR}/
    _verify_binary "Cannot move ${EXTERNAL_OUTBOUND_DIR}/${importFile}.*"
done

_call ${importCommand} ${importControlFileName}
_verify_binary "${importCommand} reported error"

#
# Verify and exit
#
_log_message INFORMATION "Finished running ${scriptName}"
_verify_log
_clean_exit SUCCESS

##############################################################################
## SPECIFIC SCRIPT SECTION END                                              ##
##############################################################################
