#!/bin/ksh
#
# Copyright � 2004, 2013, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2018/05/22
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
#
#########################################################################
#
# File:  orc_remove_user_workbooks.ksh
# Application Version: none
#
# Syntax: orc_remove_user_workbooks.ksh
#
# Description:
#   Remove all user workbooks in local domains and global domain   
#
# Calls: wbmgr
#
########################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#

if [[ -z ${O_APPLICATION} ]]
then
    ${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    #. orc_util_library.sh
else
    ${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
echo "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to remove user workbooks                            "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName}                                              "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

_log_message INFORMATION "RPAS_TODAY=${RPAS_TODAY}"

_log_message INFORMATION "Start removing user workbooks in Global Domain ${GLOBAL_DOMAIN}"

wblist=$(wbmgr -d ${GLOBAL_DOMAIN} -list -all)

if [[ ${wblist} = *"no workbooks"* ]]
then
	_log_message INFORMATION "*****************************"
	_log_message INFORMATION "No workbook in $GLOBAL_DOMAIN"
	_log_message INFORMATION "*****************************"
else
	_log_message INFORMATION "These workbooks of $GLOBAL_DOMAIN will be removed : "
	_log_message INFORMATION "\n ${wblist}"
	_call wbmgr -d "${GLOBAL_DOMAIN}" -remove -all
	_verify_binary
	_log_message INFORMATION "*************************************************"
	_log_message INFORMATION "The workbooks in $GLOBAL_DOMAIN have been removed"
	_log_message INFORMATION "*************************************************"
fi

_log_message INFORMATION "Start removing user workbooks in local Domains"

ldoms=$(domaininfo -d ${GLOBAL_DOMAIN} -shellsubdomains -loglevel none)
for ldom in ${ldoms}
do
  wblist=$(wbmgr -d "$ldom" -list -all)
  if [[ ${wblist} = *"no workbooks"* ]]
  then
        _log_message INFORMATION "*****************************"
        _log_message INFORMATION "No workbook in $ldom"
        _log_message INFORMATION "*****************************"
  else
	_log_message INFORMATION "These workbooks of $ldom will be removed : "
        _log_message INFORMATION "\n ${wblist}"
  	_call wbmgr -d "${ldom}" -remove -all
  	_verify_binary

	_log_message INFORMATION "*************************************************"
	_log_message INFORMATION "The workbooks in $ldom have been removed"
	_log_message INFORMATION "*************************************************"
  fi
done


_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS

