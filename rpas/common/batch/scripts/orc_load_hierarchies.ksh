#!/bin/ksh
#
# Copyright © 2004, 2013, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.2
# $Date: 2018/10/09
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
# 0.2                  New directory structures      Orace Retail Consulting
#
#########################################################################
#
# Syntax: orc_load_hierarchies.sh control_file
#
# Description:
#   Loads all hierarchies to the domain specified by environment variables. This script takes two
#   parameters; the first parameter is the given control file specifies which hierachies will be loaded.
#   The second parameter is the specific hierarchy to be loaded inside the given control file.
#
# Calls: loadHier
#
########################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"
utilLibFile="${O_APP_INSTANCE}/batch/lib/orc_util_library.ksh"

echo "lib: ${importLibFile}"

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    . ${utilLibFile}
else
    #${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to load hierarchies specified by given control file "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName} [controlFileName] [hierarchy]                "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 1 2

controlFile=$1
hierParam=$2


#
# Begin main processing
#
# Read vdate from global domain and set RPAS_TODAY
orc_set_RPAS_TODAY

#read list of hierarchies in the control file
importList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/hierarchies/${controlFile}" | cut -f1 -d "|"  `

#If a parameter was passed with the hierarchy name, the script will just process this hierarchy.
if [[ -z ${hierParam} ]]
then
   _log_message INFORMATION "The list of hierarchies to be processed is: \n$importList"
else
   #verify if the hierarchy name parameter exists in the control file.
   importList=`echo $importList | grep $hierParam`
   if [[ -z ${importList} ]]
   then
      _log_message ERROR "The hierarchy's $hierParam record was not found in the control file ${BATCH_HOME}/config/imports/hierarchies/${controlFile}"
      _clean_exit ${__CODE_FILE_NOT_FOUND}
   else
      importList=$hierParam
      _log_message INFORMATION "The hierarchy to be processed is: $importList"
   fi
fi

#start processing the list of hierarchies
if [[ -z ${importList} ]]
then
   _log_message ERROR "No hierarchies found in the control file ${BATCH_HOME}/config/imports/hierarchies/${controlFile} to be imported"
   _clean_exit 1
else
   #for each hierarchy, get all hierarchy's parameters from the control file to prepare for the loadHier command.
   for hierLine in $importList; do
       _log_message INFORMATION "Start processing hierarchy $hierLine"

      #get hierarchy file name
      hierFilename=`grep ${hierLine} "${BATCH_HOME}/config/imports/hierarchies/${controlFile}" | cut -d "|" -f 1 `

      #get hierarchy name
      hier=`grep ${hierLine} "${BATCH_HOME}/config/imports/hierarchies/${controlFile}" | cut -d "|" -f 1 | cut -d "." -f 1`

      #get information if the hierarchy is required or not.
      requiredHier=`grep ${hierLine} "${BATCH_HOME}/config/imports/hierarchies/${controlFile}" | cut -d "|" -f 3`

      #get purge age if defined
      purgeAge=`grep ${hierLine} "${BATCH_HOME}/config/imports/hierarchies/${controlFile}" | cut -d "|" -f 2 `
      purgeAgeParameter=""
      if [[ -n ${purgeAge} ]]; then
          _log_message INFORMATION "Found purge age ${purgeAge} in ${controlFile}"
          purgeAgeParameter="-purgeAge ${purgeAge}"
      else
         unset purgeAge
      fi

      if [[ -f ${EXTERNAL_INBOUND_DIR}/${hierFilename} ]]
      then
         _log_message INFORMATION "Copy ${EXTERNAL_INBOUND_DIR}/${hierFilename} to ${GLOBAL_DOMAIN}/input"
         _call cp "${EXTERNAL_INBOUND_DIR}/${hierFilename}" "${GLOBAL_DOMAIN}/input/."
         _verify_binary "Cannot copy files from ${EXTERNAL_INBOUND_DIR}/${hierFilename} to ${GLOBAL_DOMAIN}/input/."

         _log_message INFORMATION "Loading hierarchy ${hier} in ${GLOBAL_DOMAIN}."

         RPAS_LOG_LEVEL=$($PRINT ${RPAS_LOG_LEVEL} | tr "[:lower:]" "[:upper:]")
         if [ "${RPAS_LOG_LEVEL}" = "PROFILE" -o "${RPAS_LOG_LEVEL}" = "DEBUG" ]
         then
             _call loadHier -d "${GLOBAL_DOMAIN}" -load ${hier} -defaultDomain "${DEFAULT_DOMAIN}" -checkParents -maxProcesses ${BSA_MAX_PARALLEL} -forceInputRollups ${purgeAgeParameter} -logLoadedPositions -noClean
             _verify_binary "loadHier reported error"
         else
             _call loadHier -d "${GLOBAL_DOMAIN}" -load ${hier} -defaultDomain "${DEFAULT_DOMAIN}" -checkParents -maxProcesses ${BSA_MAX_PARALLEL} -forceInputRollups ${purgeAgeParameter}
             _verify_binary "loadHier reported error"
         fi
         _log_message INFORMATION "Done loading hierarchy ${hier} in ${GLOBAL_DOMAIN}"
      else
         if [[ $requiredHier = "OPTIONAL" ]]
         then
            _log_message WARNING "Optional hierarchy file  not found ${EXTERNAL_INBOUND_DIR}/${hierFilename}, hierarchy ${hier} not loaded."
         else
            _log_message ERROR "Required hierarchy file  not found ${EXTERNAL_INBOUND_DIR}/${hierFilename}, hierarchy ${hier} not loaded."
            _clean_exit ${__CODE_FILE_NOT_FOUND} 
         fi
      fi
  done
fi

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS
