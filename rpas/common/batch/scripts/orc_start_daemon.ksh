#!/bin/ksh
#
# Copyright � 2016, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1 
# $Date: 2018/05/28
# $Author: Oracle Retail Consulting
#
# Version              Change                         Author
# 0.1                  Initial Create                 Oracle Retail Consulting
#
##############################################################################
# Syntax: orc_start_daemon.ksh
#
# Description: 
#   This script will start Domain Daemon
##############################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"
utilLibFile="${O_APP_INSTANCE}/batch/lib/orc_util_library.ksh"


which ${importLibFile} > /dev/null 2>&1
if [ "$?" -eq 0 ]
then
    . ${importLibFile}
    . ${utilLibFile}
else
    # Cannot rely on BSA functions here - lib file not found
    echo "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 9
fi

logPath=$(_get_log_path)
#echo "logPath: "${logPath}
_log_message INFORMATION "logPath: "${logPath}
currentDir=${PWD}
daemonLogPath=${O_LOG_HOME}/daemon/daemon
mkdir -p ${daemonLogPath} 
_log_message INFORMATION "Daemon log path: ${daemonLogPath}"
#RPAS_LOG_PATH=${daemonLogPath}/
#export RPAS_LOG_PATH
#_log_message INFORMATION "RPAS_LOG_PATH=${RPAS_LOG_PATH}"

##############################################################################
## Function: usage                                                          ##
## Desc: Display the Usage information                                      ##
##--------------------------------------------------------------------------##

usage ()
{
    ${PRINT} "Usage: $scriptName{-h}"
    ${PRINT} "eg: $scriptName "  
    ${PRINT} "eg: $scriptName -h"
}

##############################################################################
## Retrieve parameters                                                      ##
##--------------------------------------------------------------------------##

if [[ ${numParams} -gt 0 ]]
then
   case "$1" in
   # the -help option displays usage
   -[Hh]|-[Hh][Ee][Ll][Pp]) usage "Display help"
             _clean_exit SUCCESS
                         ;;
                      *) 
                         ;;
   esac
fi

# Validate the Number of Paramters
_check_arg_count ${numParams} 1 1

controlFile=$1
#
# Begin main processing
#
#_log_message INFORMATION "RPAS_TODAY=${RPAS_TODAY}"
orc_set_RPAS_TODAY

daemonPort=""
walletFile=""
waitTime=""
stopMessage=""
sslLevel=""

if [[ -f "${BATCH_HOME}/environment/${controlFile}" ]]
then
   daemonInfo=`cat ${BATCH_HOME}/environment/${controlFile}`
   _verify_binary "Error reading daemon control file ${BATCH_HOME}/environment/${controlFile}"
   
   daemonPort=$(echo ${daemonInfo}|cut -f1 -d "|")
   walletFile=$(echo ${daemonInfo}|cut -f2 -d "|")
   waitTime=$(echo ${daemonInfo}|cut -f3 -d "|")
   numWaits=$(echo ${daemonInfo}|cut -f4 -d "|") 
   stopMessage=$(echo ${daemonInfo}|cut -f5 -d "|")
   sslLevel=$(echo ${daemonInfo}|cut -f6 -d "|")
   domainList=$(echo ${daemonInfo}|cut -f7 -d "|" | tr "," " ")
else
   _log_message ERROR "control file ${BATCH_HOME}/environment/${controlFile} not found"
   _clean_exit ${__CODE_FILE_NOT_FOUND}
fi


#Start Domain Daemon
_log_message INFORMATION "Changing current directory to ${daemonLogPath}"
cd ${daemonLogPath}
_log_message INFORMATION "Starting DomainDaemon..."
#_call nohup DomainDaemon -port ${daemonPort} -start -ssl ${sslLevel} -wallet ${walletFile} -no_auto_add -loglevel all &
_call nohup DomainDaemon -port ${daemonPort} -start -ssl ${sslLevel} -no_auto_add -loglevel all &
_verify_binary "Cannot start DomainDaemon -port ${daemonPort} -start -ssl ${sslLevel} &"
_log_message INFORMATION "Restoring previous directory to ${currentDir}"
cd ${currentDir}


#final check if DomainDaemon is up and running
# sleep some seconds 
sleep ${waitTime}

#_call DomainDaemon -port ${daemonPort} -ping -wallet ${walletFile}
_call DomainDaemon -port ${daemonPort} -ping -ssl ${sslLevel}
_verify_binary "DomainDaemon is not running"
_log_message INFORMATION "DomainDaemon is running."

# add domains listed in control file
for domain in ${domainList}; do
    #_call DomainDaemon -port ${daemonPort} -wallet ${walletFile} -add ${domain}
    _call DomainDaemon -port ${daemonPort} -add ${domain} -ssl ${sslLevel}
    _verify_binary "Could not add ${domain} to list of managed domains"
    _log_message INFORMATION "Added domain  ${domain}"

    #_call DomainDaemon -port ${daemonPort} -wallet ${walletFile} -activate ${domain}
    _call DomainDaemon -port ${daemonPort} -activate ${domain} -ssl ${sslLevel}
    _verify_binary "Could not activate ${domain}"
    _log_message INFORMATION "Domain ${domain} is now available"
done

#
# Verify and exit
#
_log_message INFORMATION "Finished running ${scriptName}"
_verify_log
_clean_exit SUCCESS

##############################################################################
## SPECIFIC SCRIPT SECTION END                                              ##
##############################################################################


