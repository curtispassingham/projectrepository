#!/bin/ksh
#
# Copyright © 2004, 2013, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2018/04/17
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
# 0.2                  New directory structure       Orace Retail Consulting
#
#########################################################################
#
# File:  dim_to_measurerange.sh
# Application Version: none
#
# Syntax: dims_picklist_load.sh
#
# Description:
# The purpose of the script is to update the range of a picklist based on the labels of a single-dimension hierarchy.
#
# The script reads a control file that contains a list of triplets:
#   1. Hierarchy file names
#   2. Destination measure-range names
#   3. Optional hierarchy indicator
#
# For each member of the list the script:
# 1. Reads the contents of the single-dimension hiearchy input file
# 2. Converts the content to valid RPAS measure-range string format: id1,(label1),id2,(label2)
# 3. Loads the result string to the measure-range measure
#
# The script receives two parameters, the first one is the control file that contains the hierarchy files and picklist measures.
# The second parameter is the specific picklist measure in the given control file.
#
# Calls: mace
#
########################################################################
scriptName=${0##/*/}
numParams=$#

# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    ${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi
#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    #. orc_util_library.sh
else
    ${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    exit 1
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*********************************************************************"
    echo "  Script to load dimensions picklist specified by given control file "
    echo "  Usage:                                                     "
    echo "*********************************************************************"
    echo "  ${scriptName} [controlFileName] [measureName]                      "
    echo "  ${scriptName} [-h |-help]                                          "
    echo "  -help  - print usage information and exit                          "
    echo "*********************************************************************"
}

if [[ $numParams -gt 0 && "$1" = -* ]]
#if [[ ${numParams} -gt 0 ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 1 2

controlFile=$1
measureNameParam=$2

#
# Begin main processing
#

#read list of measure lines in the control file
importList=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -f2 -d "|"  `

#If a parameter was passed with the measure name, the script will just process this measure.
if [[ -z ${measureNameParam} ]]
then
   _log_message INFORMATION "The list of measure to be processed is: \n$importList"
else
   #verify if the measure parameter exists in the control file.
   importList=`echo $importList | grep -w $measureNameParam`
   if [[ -z ${importList} ]]
   then
      _log_message ERROR "The $measureNameParam measure's record was not found in the control file ${BATCH_HOME}/config/imports/data/${controlFile}"
      _clean_exit ${__CODE_FILE_NOT_FOUND}
   else
      importList=$measureNameParam
      _log_message INFORMATION "The measure to be processed is: $importList"
   fi
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}


#start processing the list of measures
if [[ -z ${importList} ]]
then
   _log_message ERROR "No measures found in the control file ${BATCH_HOME}/config/imports/data/${controlFile} to be imported"
   _clean_exit 1
else
   #for each measure, get all measure(s) parameters from the control file to prepare for the mace command.
   for meas in $importList; do
       _log_message INFORMATION "Start processing $meas"
         #get measure name to load pick list
         measureName=`grep ${meas} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 2 `
         if [[ -n ${measureName} ]]; then
            _log_message INFORMATION "Measure name for picklist is ${measureName} in ${controlFile}"
         else
            _log_message INFORMATION "Measure name not found in control file "${BATCH_HOME}/config/imports/data/${controlFile}""
            _clean_exit ${__CODE_GENERAL_SCRIPT_ERROR}
         fi

         hierFile=`grep ${meas} "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -d "|" -f 1 `
         if [[ -f ${EXTERNAL_INBOUND_DIR}/${hierFile} ]]
         then
            _log_message INFORMATION "Hierarchy file ${hierFile} located in ${EXTERNAL_INBOUND_DIR}"
            _log_message INFORMATION "Transforming dimension values for hierarchy file ${EXTERNAL_INBOUND_DIR}/${hierFile} into picklist"

            awk -F , '{ printf "%s%s(%s)", c, $1, $2; c = ","} END { print ""}' ${EXTERNAL_INBOUND_DIR}/${hierFile}  > "${GLOBAL_DOMAIN}/input/TEMP_PICKLIST_FILE"
            _verify_binary

            measureValue=`cat "${GLOBAL_DOMAIN}/input/TEMP_PICKLIST_FILE"`

            _call rm -f "${GLOBAL_DOMAIN}/input/TEMP_PICKLIST_FILE"
            _verify_binary "Could not delete file ${GLOBAL_DOMAIN}/input/TEMP_PICKLIST_FILE"

            _log_message INFORMATION "Loading measure ${measureName} with value ${measureValue} in ${GLOBAL_DOMAIN}."

            RPAS_LOG_LEVEL=$($PRINT ${RPAS_LOG_LEVEL} | tr "[:lower:]" "[:upper:]")

            _call mace -d "${GLOBAL_DOMAIN}" -run -expression "$measureName=\"$measureValue\""
            _verify_binary "Mace reported error during calculation of expression $measureName=\"$measureValue\""

            _log_message INFORMATION "Done loading measure ${measureName} in ${GLOBAL_DOMAIN}"
         else
            _log_message INFORMATION "No hierarchy file ${hierFile} located in ${EXTERNAL_INBOUND_DIR}/"
         fi
   done
fi

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS

