#!/bin/ksh
#
# Copyright © 2018 Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.2
# $Date: 2018/10/09
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Orace Retail Consulting
# 0.2                  New directory structures      Orace Retail Consulting
#
#########################################################################
#
# File:  orc_load_users.ksh
# Application Version: none
#
# Syntax: orc_load_users.ksh control_file
#
# Description:
#   This scripts loads all user xml files placed in $EXTERNAL_INBOUND_DIR that match the file name specified in control file
#   It checks for the presence of a trigger file before it moves the file
#   
# Calls: usermgr 
#
########################################################################

scriptName=${0##/*/}
numParams=$#

#
# Validate the parameters
#
if [[ -z ${O_APPLICATION} ]]
then
    #${PRINT} "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    echo "Couldn't determine variable O_APPLICATION, please ensure the correct argument has passed to script."
    exit 9
else
    app_lower=$(echo ${O_APPLICATION} | tr "[:upper:]" "[:lower:]")
    app_upper=$(echo ${O_APPLICATION} | tr "[:lower:]" "[:upper:]")
fi

#
# Import environment file
#
importLibFile="${O_APP_INSTANCE}/batch/environment/${app_lower}_common_rpas.sh"

echo "lib: ${importLibFile}"
#. ${importLibFile}

if [ -f ${importLibFile} ]
then
    . ${importLibFile}
    # import additional files if needed
    #. orc_util_library.sh
else
    #${PRINT} "Could not find ${app_lower}_common_rpas.sh, please ensure that this file exists."
    # Cannot rely on BSA functions here - script might be called from a blank environment
    exit 9
fi

logPath=$(_get_log_path)
_log_message INFORMATION "logPath: "${logPath}

#Print the Usage Info based on the input parameter
Usage()
{
    echo "*************************************************************"
    echo "  Script to load measures specified by given control file    "
    echo "  Usage:                                                     "
    echo "*************************************************************"
    echo "  ${scriptName} [controlFileName] [measureFilename]          "
    echo "  ${scriptName} [-h |-help]                                  "
    echo "  -help  - print usage information and exit                  "
    echo "*************************************************************"
}

# ***************************************
# ovverides empty _finalize_hook from bsa
# performs some cleanup before exiting  
function _finalize_hook
{
   # in caes we created a processing trigger file we must clean this up before exiting
   if [[ -f ${EXTERNAL_INBOUND_DIR}/${processingTriggerFileName} && cleanup_required -gt 0 ]]
   then
      _log_message INFORMATION "Removing trigger file ${EXTERNAL_INBOUND_DIR}/${processingTriggerFileName}"
      rm ${EXTERNAL_INBOUND_DIR}/${processingTriggerFileName}
   fi
}
# end of _finalize_hook

# ***************************************
# checks for trigger file to be present *
function _wait_for_trigger
{
   _log_message INFORMATION "Check if trigger file ${EXTERNAL_INBOUND_DIR}/${triggerFileName} is present"
   typeset -i maxWaitTime
   let "maxWaitTime=numWaits * waitTime"
   typeset -i currentRun
   let "currentRun = 1"
   while  [ ${currentRun} -le ${numWaits} ] ; do
      if [[ -f  ${EXTERNAL_INBOUND_DIR}/${triggerFileName} ]]
      then
         _log_message INFORMATION "Found trigger file"
         break
      fi
      sleep $waitTime
      let "currentRun=currentRun + 1"
   done
   if [[ ${currentRun} -gt ${numWaits} ]]
   then
      _clean_exit ${__CODE_FILE_NOT_FOUND} "Trigger file ${EXTERNAL_INBOUND_DIR}/${triggerFileName} not found within maximum wait time ($maxWaitTime seconds)"
   fi
}
# end of _wait_for_trigger

# ***************************************
# renames all files to be processed     *
function _rename_files
{
      typeset -i numFiles
      numFiles=$(find ${EXTERNAL_INBOUND_DIR}/ -maxdepth 1 -type f -name "${fileName}.*" | grep -E '\.xml' | grep -c -v -E '\.gz|\.zip')
      if [[ $numFiles -gt 0 ]]
      then
         _log_message INFORMATION "Found $numFiles file(s) to process"
         typeset -i movedFiles
         let "movedFiles=0"
         for userFile in $(find ${EXTERNAL_INBOUND_DIR} -maxdepth 1 -type f -name "${fileName}.*" | grep -E '\.xml' | grep -v -E '\.gz|\.zip')
         do
            newName=${tempMask}$(basename ${userFile})
            _log_message INFORMATION "Moving file ${userFile} to ${EXTERNAL_INBOUND_DIR}/${newName}"
            mv ${userFile}  ${EXTERNAL_INBOUND_DIR}/${newName}
            _verify_binary "Could not move  ${userFile} to ${EXTERNAL_INBOUND_DIR}/${newName}"
            let "movedFiles=movedFiles + 1"
         done
         if [[ ${numFiles} -ne ${movedFiles} ]]
         then
            _clean_exit ${__CODE_GENERAL_SCRIPT_ERROR} "Number of moved files (${numFiles}) differs from number of files to process (${numFiles})"
         fi
         _log_message INFORMATION "Finished renaming files"

      else
         _clean_exit 0 "No files to process"
      fi
}
# end of _rename_files

# ****************************************************************
# *   function _process_files
# *   this will loop over all files (with ${tempMask}${fileName} *
# *   and load them using usermgr                                * 
function _process_files
{
   for fileToLoad in $(find ${EXTERNAL_INBOUND_DIR} -maxdepth 1 -type f -name "${tempMask}${fileName}.*" | grep -E '\.xml' | grep -v -E '\.gz|\.zip')
   do
      _log_message INFORMATION "Processing file ${fileToLoad}"
      mv ${fileToLoad} ${GLOBAL_DOMAIN}/input/users.xml
      _verify_binary "Could not move ${fileToLoad} to  ${GLOBAL_DOMAIN}/input/users.xml"
      _call usermgr -d ${GLOBAL_DOMAIN} -importXml -replace -noPassword
      _verify_binary "usermgr reported error while processing file" 

   done
}
# end of _process_files

if [[ $numParams -gt 0 && "$1" = -* ]]
then
    case "$1" in
    # the -help option displays usage
    -[Hh]|-[Hh][Ee][Ll][Pp]) Usage "Display help"
              _clean_exit SUCCESS
                          ;;
                       *)
                          ;;
    esac
fi

#
# Validate the parameters
#
_check_arg_count ${numParams} 1 1

controlFile=$1
#measureFilenameParam=$2

#
# Begin main processing
#
# set up internal cleanup flag
typeset -i cleanup_required
let cleanup_required=0

# define sleep time and maximum number of sleep steps
typeset -i waitTime
typeset -i numWaits

let "waitTime=10"
let "numWaits=1"

# temp file mask
tempMask="tmp_"


#read filenames in the control file
fileName=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${controlFile}" | cut -f1 -d "|"  `


#start processing 
if [[ -z ${fileName} ]]
then
   _log_message ERROR "No file name found in the control file ${BATCH_HOME}/config/imports/data/${controlFile} to be imported"
   _clean_exit ${__CODE_NONZERO_EXIT}
else
   _log_message INFORMATION "Start processing files with name $fileName"
   #get flag that indicates if input measure file needs to be transformed (example: Y/N to true/false)
   _log_message INFORMATION "Get triger file name"
   triggerFileName=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${controlFile}" |grep -w ${fileName} | cut -d "|" -f 2 `
   _verify_binary "Could not determine trigger file name"
   if [[ -z ${triggerFileName} ]]
   then
      _log_message ERROR "Trigger file name is empty. Cannot continiue"
      _clean_exit ${__CODE_GENERAL_SCRIPT_ERROR}
   else
      #get information if the measure file is required or not.
      _log_message INFORMATION "Get processing trigger file name"
      processingTriggerFileName=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${controlFile}" |grep -w ${fileName} | cut -d "|" -f 3`
      _verify_binary "Could not determine processing trigger file name"
      
      if [[ -z ${processingTriggerFileName} ]]
      then
         _log_message ERROR "Processing trigger file name not found. Cannot continue"
         _clean_exit ${__CODE_GENERAL_SCRIPT_ERROR}
      fi 
      
       
      #get information if loadable file is required or not.
      requiredFlag=`grep -vE '^(#|$)' "${BATCH_HOME}/config/imports/data/${controlFile}" |grep -w ${fileName} | cut -d "|" -f 4`
      _verify_binary "Could not determine whether loadable file is optional or required"
      if [[ -z ${requiredFlag} ]]
      then
         _log_message ERROR "Required flag is empty. Cannot continue"
         _clean_exit ${__CODE_GENERAL_SCRIPT_ERROR}
      fi 
      # start main processing

      # check if trigger file exists
      _wait_for_trigger 
      

      # if processing trigger file exists another instance of the script is running so exit else generate processing trigger file
      if [[ -f ${EXTERNAL_INBOUND_DIR}/${processingTriggerFileName} ]]
      then
          _log_message INFORMATION "Processing trigger file ${EXTERNAL_INBOUND_DIR}/${processingTriggerFileName} exists."
          _log_message INFORMATION "Is there another instance of this script running?"
          _log_message INFORMATION "Will stop this instance without cleanup"
          _clean_exit 0 "Should not do anything" 

      fi
      touch ${EXTERNAL_INBOUND_DIR}/${processingTriggerFileName}
      _verify_binary "Could not create processing trigger file ${EXTERNAL_INBOUND_DIR}/${processingTriggerFileName}"
      let cleanup_required=1
      
      # check if loadable files exits and move them away
      _rename_files

      # process files
      _process_files

   fi
fi

_log_message INFORMATION "Finished running ${scriptName}"

#
# Verify and exit
#
_verify_log
_clean_exit SUCCESS
