
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

public class InheritWbAccessFromGroups {
   
    private static final String accessRightsLoadableFileNameTemplate = "input/r_wbtrights.csv.ovr";
    
    private static final String command_InitGroupWbAccessList = "printArray -array DOMAIN/data/security.r_grpwbtrights%1 -loglevel none";
    private static final String command_InitializeGroupsFromDomain = "printArray -array DOMAIN/data/admin.dim_grp  -slice info:label -cellsperrow 1 -loglevel none";
    private static final String command_GetWorkbookTemplatesFromDomain = "printArray -array DOMAIN/data/wbdata.dim_wbt -loglevel none ";
    private static final String command_GetGroupsForUser = "printArray -array DOMAIN/data/admin.usergroup -slice user:USER -cellsperrow 1 -loglevel none" ;
    private static final String command_GetGroupsForAllUsers = "printArray -array DOMAIN/data/admin.usergroup -loglevel none";
    private static final String command_InitUserList = "printArray -array DOMAIN/data/admin.dim_user -slice info:label -cellsperrow 1 -loglevel none ";
    
    private static final String command_LoadAccessrights = "loadmeasure -d DOMAIN -measure r_wbtrights -loglevel information";
    
    private static int defaultStringBuilderSize = 4096;
    private static int defaultVecSize=1000;
    private static int defaultNumTemplates=50;
    private static int defaultNumGroups=100;
    private static int defaultVecIncrement=20;
    private String domain;
    private String user;
    private String runMode;
    private boolean stop;
    private String status; 
    private Vector<AccessRight> templateAcccesRights;
    private HashMap<String,RpasSecUser> userMap;
    private HashMap<String,RpasWbAccessGroup> wbSecGroupList;
    private Vector<String> usersAndGroups = null;
    private Vector<String> finalAccessRights;
    private SimpleLogger logger;
    
    private class AccessRight{
        private String template;
        private int access; 
        public AccessRight(String template, int access){
            this.template = template;
            this.access = access;
        }
        public String getTemplate(){
            return this.template;
        }
        public int getAccess(){
            return this.access;
        }
        public void setAccess(int access){
            if (access>this.access){
                this.access = access;
            }
        }
        public String printAccess(){
            return this.template+","+String.valueOf(access);
        }
    }
    
    private class RpasSecUser{
        private String userId;
        private Vector<String> groups;
        private HashMap<String,AccessRight> accessMap;
        public RpasSecUser(String user){
            this.userId = user;
            accessMap = new HashMap<String,AccessRight>(defaultNumTemplates,defaultVecIncrement);
            groups = new Vector<String>(defaultNumGroups,defaultVecIncrement);
        }
        public void setAccessRight(String template, int access){
            if (accessMap.containsKey(template)){
                accessMap.get(template).setAccess(access);
            }else{
                accessMap.put(template, new AccessRight(template,access));
            }
        }//end void setAccessRight(String template, int access)
        public Vector<String> getAccessRights(){
            Vector<String> results = new Vector<String>(defaultNumTemplates,defaultVecIncrement);
            Iterator<AccessRight> iter =  accessMap.values().iterator();
            while (iter.hasNext()){
                AccessRight wbAccess = iter.next();
                results.add(this.userId+","+wbAccess.printAccess());
            }
            return results;
        } //end Vector<String> getAccessRights()
        
        public void addGroup(String group){
            if (!groups.contains(group)){
                groups.add(group);
            }
        }// end void addGroup(String group)
        public boolean isMemberOfGroup(String group){
            return groups.contains(group);
        }
        public void inheritWbAccessfromGroups(){
            //System.out.println("groups.size: "+ Integer.toString(groups.size()));
            for (int i = 0; i < groups.size(); i++){
                RpasWbAccessGroup wbAccessGroup = null;
                if (wbSecGroupList.containsKey(groups.get(i))){
                    wbAccessGroup = wbSecGroupList.get(groups.get(i));
                    Iterator<String>  iter = accessMap.keySet().iterator();
                    while (iter.hasNext()){
                        String template = iter.next();
                        //watch out: not every group grants access to every template
                        AccessRight ar = wbAccessGroup.getAccessRight(template);
                        if (ar != null){
                            //System.out.println("User:"+this.userId + " Group:"+ wbAccessGroup.groupId + " Template: " + ar.getTemplate() + " Access Right:" + ar.printAccess());
                            this.setAccessRight(ar.getTemplate(), ar.getAccess());    
                        }
                        
                    } // end while (iter.hasNext())
                }else{
                    //System.out.println("No entry found for group: "+ groups.get(i));
                }// end if (wbSecGroupList.containsKey(groups.get(i)))
            }// end for loop
        }// end inheritWbAccessfromGroups
        
    }//end private class RpasUser
    
    private class RpasWbAccessGroup{
        private String groupId;
        private HashMap<String,AccessRight> accessRights;
        public RpasWbAccessGroup(String group){
            this.groupId = group;
            accessRights = new HashMap<String,AccessRight> (defaultNumTemplates,defaultVecIncrement);
        }
        public void setAccessRight(String template, int access){
            if (accessRights.containsKey(template)){
                accessRights.get(template).setAccess(access);
            }else{
                AccessRight ar = new AccessRight(template,access);
                accessRights.put(template, ar);
            }
        }
        public AccessRight getAccessRight(String template){
            return accessRights.get(template);
        }
    }
    
    private class SimpleFileWriter{
        protected String _path;
        protected String _file;
        protected boolean isReady;
        protected FileSystem myFs;
        protected Path myPath;
        protected BufferedReader myBuffreader;
        protected Vector<String> myLines;
        
        public Path getPath(){
            return myPath;
        }
        
        protected void init(){
            Path newFile = null;
            myFs = null;
            myBuffreader = null;
            isReady = false;
            myLines = new Vector<String>(20,5);
            myFs = FileSystems.getDefault();
            int fileNameExtension = 0;
            int maxTries = 100;
            boolean foundUniqueName = false;
            String myBaseFileName = this._file;
            String myNewFileName = myBaseFileName;


            while (!foundUniqueName && fileNameExtension < maxTries) {

                if (_path == null) {
                    myPath = myFs.getPath(myNewFileName);
                } else {
                    myPath = myFs.getPath(_path, myNewFileName);
                }
              
                if (Files.exists(myPath)) {
                    fileNameExtension++;
                    myNewFileName = myBaseFileName + "." + Integer.toUnsignedString(fileNameExtension);
                } else {
                    foundUniqueName = true;
                    this._file = myNewFileName;
                }
            }// end (!foundUniqueName && fileNameExtension < maxTries)
            
            // check if we have now a unique file name
            if (foundUniqueName == false){
                this.isReady = false;
                throw new RuntimeException ("Unable to generate unique filename. Last try was "+this._file);
            } // end if (foundUniqueName == false)
                
           
            try{
                newFile = Files.createFile(myPath);
            }catch (IOException ioe){
                System.err.format("IOException during init-> cannot create file "+ myPath.toString()+ " : %s%n", ioe);
            }
            myPath = newFile;
            isReady = true;
            
        }/* end init */
        
        public String getFileName(){
            return this._file;
        } // end String getFileName()
        
        public boolean writeLine(String line){
            myLines.clear();
            myLines.add(line);
            
            try{
                myPath = Files.write(myPath,myLines,StandardOpenOption.APPEND);
            }catch (IOException ioe){
                System.err.format("IOException during write: %s%n", ioe);
            }
            return true;
        } /* end writeLine(String line); */
        
        public boolean writeLines(Vector<String> lines){
            myLines.clear(); 
            try{
                myPath = Files.write(myPath,lines,StandardOpenOption.APPEND);
            }catch (IOException ioe){
                System.err.format("IOException during write: %s%n", ioe);
            }    
            return true;
        } /* end writeLines(Vector<String> line); */

        /**
         * @param myPath
         * @param myFile
         */
        public SimpleFileWriter(String pathName, String fileName){
            this._path = pathName;
            this._file = fileName;
            myPath = null;
            init();         
        }/* end SimpleFileWriter(String myPath, String myFile) */

        /**
         * @param myFile
         */
        public SimpleFileWriter(String fileName){
            this._path = null;
            this._file = fileName;
            myPath = null;
            init();         
        }/* end SimpleFileWriter(String myPath, String myFile) */
        
        public SimpleFileWriter(Path aPath){
            this._path = null;
            this._file = null;
            myPath = aPath;
            init();         
        }/* end SimpleFileWriter(String myPath, String myFile) */
    }
    
    public enum LoglevelEnum {
        DEBUG ("DEBUG"),
        INFORMATION ("INFORMATION"),
        WARNING ("WARNING"),
        ERROR ("ERROR"),
        NONE ("NONE");
        
        private String loglevel;
        LoglevelEnum(String loglevel) {
            this.loglevel = loglevel.toUpperCase();
        }
        
        public static LoglevelEnum parse(String loglevel){
            LoglevelEnum result = null;
            for (LoglevelEnum level: LoglevelEnum.values()){
                if (level.loglevel.equalsIgnoreCase(loglevel)){
                    result =  level;
                }
            }
            return result;
        }
    }
    
    private class SimpleLogger{
        private LoglevelEnum loglevel;
        private LoglevelEnum defaultLoglevel = LoglevelEnum.ERROR;
        //LocalDateTime datetime = LocalDateTime.now();
        private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        //String suffix = datetime.format(formatter);
        public SimpleLogger(LoglevelEnum loglevel){
            this.loglevel = loglevel;
        }// end SimpleLogger(LoglevelEnum loglevel)
        public SimpleLogger(){
            String envLogLevel = null;
            envLogLevel = System.getenv("RPAS_LOG_LEVEL");
           
            if (envLogLevel != null){
                
                this.loglevel = LoglevelEnum.parse(envLogLevel);
                if (this.loglevel == null){
                    this.loglevel = defaultLoglevel;        
                }
            }else{
                this.loglevel = defaultLoglevel;
            }
            logDebugMessage("RPAS_LOGLEVEL->" + this.loglevel.toString());
        }// end SimpleLogger(LoglevelEnum loglevel)
        
        public void logMessage(LoglevelEnum loglevel, String message){
            if (this.loglevel.ordinal()<=loglevel.ordinal()){
                LocalDateTime datetime = LocalDateTime.now();
                String ts = datetime.format(formatter);
                System.out.println(ts + " : " + loglevel.toString() + " : " + message);
            }// end if
        }//end void logMessage(LogleveEnum loglevel, String message)
        public void logErrorMessage(String message){
            logMessage(LoglevelEnum.ERROR, message);
        }
        public void logInformationMessage(String message){
            logMessage(LoglevelEnum.INFORMATION, message);
        }
        public void logDebugMessage(String message){
            logMessage(LoglevelEnum.DEBUG, message);
        }
        public void logWarningMessage(String message){
            logMessage(LoglevelEnum.WARNING, message);
        }
        public LoglevelEnum getLogLevel(){
            return this.loglevel;
        }
        
    } // end private class SimpleLogger 
    
    private void loadAccessRights(){
        
        Vector<String> result;
        logger.logDebugMessage("Entering function loadAccessRights");
        result = execCommand("ksh", command_LoadAccessrights.replace("DOMAIN", this.domain), false);
        if (result == null){
            this.stop = true;
            this.status = "Could not load r_wbtrights";
        }
        if (logger.getLogLevel().ordinal()<= LoglevelEnum.INFORMATION.ordinal()){
            for (int i = 0; i < result.size(); i++){
                logger.logInformationMessage(result.get(i));
            }
        }// end if (logger.getLogLevel().ordinal()<= LoglevelEnum.INFORMATION)
        logger.logDebugMessage("Leaving function loadAccessRights");
    }// end void loadAccessRights()
    
    private Vector<String> execCommand(String command, String params, boolean cleanResults, boolean preserveNA){
        int exitValue = 0;
        boolean success = true;
        boolean processStopped = false;
        logger.logDebugMessage("Entering function execCommand");
        logger.logDebugMessage("About to execute "+ command + " with arguments "+params);
        Vector<String> lineBuffer = null;
        Vector<String> tempBuffer = null;
        ProcessBuilder pb = new ProcessBuilder(command,params);
        pb.redirectErrorStream(true);
        Process p = null;
        try{
            p = pb.start();
        }catch (IOException ioe){
            System.out.println(ioe);
            success = false;
        }
        
        try{
            processStopped = p.waitFor(0, TimeUnit.SECONDS);
        }catch (InterruptedException ire){
            System.out.println(ire);
            success = false;
        }
        
        if (processStopped){
            //check exit value
            exitValue = p.exitValue();
            if (exitValue != 0){
                success = false;
                logger.logErrorMessage("Error detected while executing command "+ command + " with arguments "+params);
                logger.logErrorMessage("Exit value is:" +String.valueOf(exitValue));
                BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line = "";
                try {
                    while ((line = b.readLine()) != null) {
                        logger.logErrorMessage(line);
                    }// end while
                }catch (IOException ioe){
                    System.out.println(ioe);
                }
            }else{ // exitValue == 0
                logger.logInformationMessage("Finished successfully command " + command + " with parameters "+ params);
                BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line = null;
                tempBuffer = new Vector<String> (defaultVecSize,defaultVecIncrement);
                try{
                    while ((line = b.readLine()) != null) {
                        tempBuffer.add(line);
                    }//end while
                }catch (IOException ioe){
                    System.out.println(ioe);
                    success = false;
                    return null;
                }
                
            }// end if (exitValue != 0)
            
        }else{
            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            tempBuffer = new Vector<String> (defaultVecSize,defaultVecIncrement);
            try {
                while ((line = b.readLine()) != null) {
                    tempBuffer.add(line);
                }// end while
            }catch (IOException ioe){
                System.out.println(ioe);
            }// end try 
        }// end
        
        try{
            p.waitFor();
        }catch (InterruptedException ire){
            System.out.println(ire);
            success = false;
        }
        exitValue = p.exitValue();
        logger.logDebugMessage("After process finished exit value is:" +String.valueOf(exitValue));
        if (exitValue != 0){
            logger.logDebugMessage("TempBuffer.size():"+String.valueOf(tempBuffer.size()));
            logger.logErrorMessage("Error detected during command " + command + " with parameters "+ params +" with result " + Integer.valueOf(exitValue));
            for (int i = 0; i < tempBuffer.size(); i++){
                logger.logErrorMessage(tempBuffer.get(i));
            }
            success = false;
        }else{ // seems as everything went well - time to process results
            
            logger.logInformationMessage("Finished successfully command " + command + " with parameters "+ params);
            String line = null;
            lineBuffer = new Vector<String> (defaultVecSize,defaultVecIncrement);           
           
            for (int i = 0; i < tempBuffer.size(); i++){
                    line = tempBuffer.get(i);
                    if (cleanResults == true){
                       // System.out.println("Clean results is active");
                        // remove unneccessary line
                        if (line.startsWith("navalue")){
                            if (preserveNA == false){
                                continue;
                            }                      
                        }
                        if (line.isEmpty()){
                            continue;
                        }
                        //delete "():" | squeeze blanks and convert result into comma separated line
                        line = line.replace ("(", "");
                        line = line.replace(")", "");
                        line = line.replace(":", "");
                        line = line.replaceAll("  *", " ");
                        line = line.replaceAll(" ", ",");
                        lineBuffer.addElement(line);
                    }else{
                        lineBuffer.addElement(line);
                    } // end if (cleanResults == true) 
                }// end (int i = 0; i < tempBuffer.size(); i++)
                    
            }// end else (exitValue was 0)
      
        
        if (success){
            logger.logDebugMessage("Leaving function execCommand");
            return lineBuffer;
        }else{
            logger.logDebugMessage("Leaving function execCommand");
            return null;
        }//end if (success)    
    } // end Vector<String> execCommand(String command, String params, boolean cleanResults, boolean preserveNA)
    
    private Vector<String> execCommand(String command, String params, boolean cleanResults){
        return execCommand(command, params, cleanResults, false);
    }// end execCommand(String command, String params, cleanResults)

    private void initTemplateAccessRights(){
       
        Vector<String> workbookTemplates = getWorkbookTemplatesFromDomain();
        if (!this.stop){
            int numTemplates = workbookTemplates.size();
        
            /* Set default access rights at template level - no access granted */
            templateAcccesRights = new Vector<AccessRight>(numTemplates,1);
            for (int i = 0; i < numTemplates; i++){
                //System.out.println(workbookTemplates.get(i));
                AccessRight accessRight = this.new AccessRight(workbookTemplates.get(i),0);
                templateAcccesRights.add(accessRight);
            } // end (int i = 0; i < numTemplates; i++)
        }
        logger.logInformationMessage("Found # "+workbookTemplates.size()+" workbook templates");
    } // end void initTemplateAccessRights()
    
    private void initUser(RpasSecUser newUser){
        //do something
        AccessRight ar=null;
        for (int i = 0; i < templateAcccesRights.size(); i++){
            ar = templateAcccesRights.get(i);
            newUser.setAccessRight(ar.getTemplate(), ar.getAccess());
        }
     }// end void initUser

    
    private void updateGroupMemberShip(){
        logger.logDebugMessage("Entering function updateGroupMemberShip");
        String [] splittedLine;
        String line = null;
        String currentUserId;
        String currentGroupId;
        for (int i = 0; i < usersAndGroups.size(); i++){
            line = usersAndGroups.get(i);
            splittedLine = line.split(",");
            currentUserId = splittedLine[0];
            currentGroupId = splittedLine[1];
            if (userMap.containsKey(currentUserId)){
                userMap.get(currentUserId).addGroup(currentGroupId);
                logger.logDebugMessage("added group " +currentGroupId+" to user " + currentUserId);
               
            }else{
                logger.logWarningMessage("Did not find user:"+currentUserId+": in userMap. Creating a new user");
                RpasSecUser newUser = this.new RpasSecUser(currentUserId);
                newUser.addGroup(currentGroupId);
                userMap.put(currentUserId, newUser);
                logger.logWarningMessage("added group " +currentGroupId+" to new user " + currentUserId);
            } // end else (userMap.containsKey(currentUserId))
        }// end for(...) loop
        logger.logInformationMessage("Processed #"+String.valueOf(usersAndGroups.size())+" records of user and groups vector");
        logger.logDebugMessage("Leaving function updateGroupMemberShip");
    }// end void updateGroupMemberShip
    
    private void inheritAccessRights(){
       
        Iterator<RpasSecUser> iter = userMap.values().iterator();
        RpasSecUser currentUser = null;
        while (iter.hasNext()){
            currentUser = iter.next();
            //System.out.println("in inheritAccessRights -> processing user: "+currentUser.userId );
            currentUser.inheritWbAccessfromGroups();
        }
    } // end void inheritAccessRights()
    
    private void initializeGroupsFromDomain(int accessLevel){
        String line = null;
        String [] splittedLine = null;
        String groupId = null; 
        RpasWbAccessGroup currentGroup = null;
        
        Vector<String> groupsFromDomain = execCommand("ksh", command_InitializeGroupsFromDomain.replace("DOMAIN", this.domain), true);
        
        if (groupsFromDomain == null){
            this.stop = true;
            this.status = "Unable to initialize groups from domain "+this.domain;
        }
        for (int i = 0; i < groupsFromDomain.size(); i++){
            line = groupsFromDomain.get(i);
            splittedLine = line.split(",");
            groupId = splittedLine[0];
            if (!wbSecGroupList.containsKey(groupId)){
                currentGroup = this.new RpasWbAccessGroup(groupId);
                wbSecGroupList.put(groupId, currentGroup);
            }else{
                currentGroup = wbSecGroupList.get(groupId);
            }
            //inner loop on templates
            for (int t = 0; t > templateAcccesRights.size(); t ++){
                currentGroup.setAccessRight(templateAcccesRights.get(t).getTemplate(), accessLevel);
            }
            
        }// end outer loop on groupsFromDomain
        
    } // end initializeGroupsFromDomain(int accessLevel)
    
    private void InitGroupWbAccessList(){
        
        
        Vector<String> groupWbAccessList = execCommand("ksh", command_InitGroupWbAccessList.replace("DOMAIN", this.domain), true,true);
        //check NA value first
       
        String naValueString = groupWbAccessList.get(0).split(",")[1];
        int naValue = Integer.valueOf(naValueString);
        groupWbAccessList.removeElementAt(0);
        
        
        if (naValue != 0){
            initializeGroupsFromDomain(naValue);
        }
        
        String[] splittedLine = null;
        RpasWbAccessGroup currentGroup;
        for (int i = 0; i < groupWbAccessList.size(); i++) {

            splittedLine = groupWbAccessList.get(i).split(",");
            if (splittedLine.length == 4) {
                String groupId = splittedLine[0];
                String templateId = splittedLine[1];
                int accessLevel = Integer.valueOf(splittedLine[3]);
                AccessRight ar = this.new AccessRight(templateId, accessLevel);
                
                if (wbSecGroupList.containsKey(groupId)) {
                    currentGroup = wbSecGroupList.get(groupId);
                    currentGroup.setAccessRight(templateId, accessLevel);

                } else {
                    currentGroup = this.new RpasWbAccessGroup(groupId);
                    currentGroup.setAccessRight(templateId, accessLevel);
                    wbSecGroupList.put(groupId, currentGroup);
                }
                
            } else {
                RuntimeException rex =
                    new RuntimeException("InitGroupWbAccessList: Found unexpected line -> " + groupWbAccessList.get(i));
                throw rex;
            } // end if (splittedLine.length == 4)
        } // end (int i = 0; i < groupWbAccessList.size();i++)
        
    }

    private Vector<String> getAccessRightsVector(){
        Vector<String> results = new Vector<String>(defaultVecSize *4 , defaultVecIncrement * 4);
        Iterator<RpasSecUser> iter = userMap.values().iterator();
        while (iter.hasNext()){
            results.addAll(iter.next().getAccessRights());
        }
        return results;
    } // end private Vector<String> getAccessRightsVector()
    
    public boolean prepare(){
        Vector<String> groupsForUser = null;
        
        initTemplateAccessRights();
        if (this.stop){
            logger.logErrorMessage(this.status);
            return false;
        }
        
        // if user != null then switch to single user mode
        if (this.user != null){
            groupsForUser = getGroupsForUser();
            if (groupsForUser == null){
                this.stop = true;
                this.status = "Failed to initialze groups for user";
                return false;
            }
            for (int i = 0; i < groupsForUser.size();i++){
                usersAndGroups = new Vector<String>(defaultVecSize,defaultVecIncrement);
                usersAndGroups.add(this.user + "," + groupsForUser.get(i));
                //System.out.println("usersAndGroups.size():"+Integer.valueOf(usersAndGroups.size()));
            }//end for (int i = 0; 1 < groupsForUser.size();i++)
        }else{
            // get all groups and users
            usersAndGroups = getGroupsForAllUsers();
            if (usersAndGroups == null){
                this.stop = true;
                this.status = "Failed to initialize groups for all users";
                return false;
            }
        }
        initUserList();
        if (this.stop){
            return false;
        }
        updateGroupMemberShip();
        InitGroupWbAccessList();
        inheritAccessRights();
        
        finalAccessRights = getAccessRightsVector();
        /* Uncomment to debug
        for (int i = 0 ; i < finalAccessRights.size(); i++){
            System.out.println(finalAccessRights.get(i));
        } 
        */
        if (finalAccessRights != null){
            if (finalAccessRights.size() > 0 ){
                return true;
            }else{
                this.stop = true;
                return false;
            }
        }else{
            return false;
        }// end if (finalAccessRights != null
    }// end boolean prepare()
    
    public boolean apply(){
        if (this.stop){
            return false;
        }
        LocalDateTime datetime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd.HHmmss.SSS");
        String suffix = datetime.format(formatter);
        String fileName = accessRightsLoadableFileNameTemplate;
        String almostUniqueFileName = fileName + "." + suffix + "." + this.runMode;
        
        /*
         * SimpleFileWriter will ensure that we get a unique file name 
         * It will try up to 100 times to extend the original file name
         * with pattern ".int" 
         */
        logger.logDebugMessage("Creating file writer");
        SimpleFileWriter simpleFileWriter = this.new SimpleFileWriter(this.domain,almostUniqueFileName);
        if (simpleFileWriter != null){
            logger.logDebugMessage("Writing to file "+simpleFileWriter.getFileName() );
        }
        if (simpleFileWriter.writeLines(finalAccessRights)== false){
            this.stop = true;
            this.status = "Could not write file "+simpleFileWriter.getFileName();
        }else{
            logger.logInformationMessage("Wrote #"+finalAccessRights.size()+" records to file "+simpleFileWriter.getFileName());
        }
        
        if (this.stop){
            return false;
        }
        loadAccessRights();
        if (this.stop){
            return false;
        }
        return true;
    } // end boolean apply()

    /**
     * @param args
     * mandatory args[0]: path to domain
     * optional arg[1]: user id
     */
    public InheritWbAccessFromGroups(String[] args) {
        this.logger = new SimpleLogger();
        logger.logDebugMessage("Constructing new instance of InheritWbAccessFromGroups");
        if (args.length == 2){
            this.domain = args[0];
            this.user = args[1];
            this.runMode = "user_" + this.user;
            logger.logInformationMessage("Run mode is single user. User is "+this.user);
            userMap = new HashMap<String,RpasSecUser> (defaultVecSize,defaultVecIncrement);
        }else{
            this.domain = args[0];
            this.user = null;
            this.runMode = "full";
            logger.logInformationMessage("Run mode is all users.");
            userMap = new HashMap<String,RpasSecUser> (1,1);
        }
        this.stop = false;
        this.status = "constructor";
        wbSecGroupList = new HashMap<String,RpasWbAccessGroup>(defaultVecSize,defaultVecIncrement);
        logger.logDebugMessage("End of constructor");
        
    } // end InheritWbAccessFromGroups(String[] args)

    private Vector<String> getWorkbookTemplatesFromDomain(){
        
        Vector<String> temp = null;
        Vector<String> result = null;
        String line = null;
        String[] splittedLine = null;
        temp = execCommand("ksh",command_GetWorkbookTemplatesFromDomain.replace("DOMAIN", domain), true);
        if (temp != null){
            result = new Vector<String>(temp.size(),defaultVecIncrement);
            
            for (int i = 0; i < temp.size(); i++){
                line = temp.get(i);
                splittedLine = line.split(",");
                if (splittedLine.length >= 2){
                    if (result.contains(splittedLine[1])){
                        continue;
                    }else{
                        result.add(splittedLine[1]);
                    }
                }else{
                    this.stop = true;
                    this.status = "Found unparsable line during getWorkbookTemplatesFromDomain : "+line;
                }
            }
        }else{
            this.stop = true;
            this.status = "Unable to fetch list of workbooks from domain " + this.domain;
        }
        return result;
        
    } // Vector<String> getWorkbookTemplatesFromDomain()
    
    private Vector<String> getGroupsForUser(){
        /* Goal is to get for each user only groups that he/she is actualy a member of.  
         * By default RPAS spits out a matrix containing all groups and users
         * so we would get a lot of entries "user group false" and therefore we ask 
         * grep to get rid of these entries
         */
        String params = null;
        params = command_GetGroupsForUser.replace("DOMAIN",this.domain);
        params = params.replace("USER",this.user);
        Vector<String> temp = execCommand("ksh", params, true);
        /*
         * Remove all lines with value false
         */
        if (temp == null){
            logger.logErrorMessage("Cannot obtain groups for user "+this.user+" in domain "+this.domain);
            return temp;    
        }else{
            Iterator<String> iter = temp.iterator();
            String line = null;
            String [] splittedLine = null;
            while (iter.hasNext()){
                line = iter.next();
                splittedLine = line.split(",");
                if (splittedLine.length == 2){
                    if (Boolean.parseBoolean(splittedLine[1])== false){
                        iter.remove();
                    }
                }else{
                    logger.logErrorMessage("Unparsable line: "+line);
                }
            }
            return temp;
        }
    } // end Vector<String> getGroupsForUser()
    
    private Vector<String> getGroupsForAllUsers(){
        /* Goal is to get for each user only groups that he/she is actualy a member of.  
         * By default RPAS spits out a matrix containing all groups and users
         * so we will get a lot of entries "user group false" that we need to remove
         */
       
        // expected result from printArray
        // navalue                 :    NA
        //
        // (demoadmin admins)      :    true
        // (demoadmin group1)      :    false
        // execCommand (command, args, true) will transform this into
        // demoadmin,admins,true
        
        String line = null;
        String[] splittedLine = null;
        
        Vector<String> temp = execCommand("ksh", command_GetGroupsForAllUsers.replace("DOMAIN",this.domain), true);
        if (temp == null){
            this.stop = true;
            this.status = "Unable to fetch groups for all users in domain "+this.domain;
        }
        Iterator<String> iter = temp.iterator();
        while (iter.hasNext()){
            line = iter.next();
            splittedLine = line.split(",");
            if (Boolean.parseBoolean(splittedLine[2])== false){
                iter.remove();
            }
        }
        return temp;
    } // end Vector<String> getGroupsForAllUsers()
    
    private void initUserList(){
        RpasSecUser newUser;
        if (this.user == null){
            //String args = "printArray -array DOMAIN/data/admin.dim_user -slice info:label -cellsperrow 1 -loglevel none | tr -d \" \" | cut -d \":\" -f 1";
            
            Vector<String> userList =  execCommand("ksh",command_InitUserList.replace("DOMAIN", domain), true);
            if (userList == null){
                this.stop = true;
                this.status = "Unable to initialize user list from domain " + this.domain;
            }else{
                for (int i = 0; i < userList.size(); i++){
                    newUser =  new RpasSecUser(userList.get(i).split(",")[0]);
                    initUser(newUser);
                    userMap.put(newUser.userId,newUser);
                }// end userList loop
            } // end if (userList == null)
        }else{
            newUser = new RpasSecUser(this.user);
            initUser(newUser);
            userMap.put(this.user, newUser);
        }
        logger.logInformationMessage("User list contains "+String.valueOf(userMap.size())+" entries");
        /* Uncomment to debug
        System.out.println("UserList contains:");
        Iterator<RpasSecUser> iter = userMap.values().iterator();
        RpasSecUser currentUser = null;
        while (iter.hasNext()){
            currentUser = iter.next();
            System.out.println(currentUser.userId);
        }
        */
    }// end void initUserList()
    
    public String getStatus(){
        return this.status;
    }
    
    public static void main(String[] args) {
        /*
         * Class InheritWbAccessFromGroups requires full path to RPAS domain as first argument
         * If a sceond paramter is specified this is considered a user name and only for this 
         * user workbook access rights are inherited from group level default values
         */
        if (args.length > 0 && args.length <=2 ){
            InheritWbAccessFromGroups inheritWbAccessFromGroups = new InheritWbAccessFromGroups(args);
            if (inheritWbAccessFromGroups.prepare()){
                //System.out.println("Successfully prepared");
                if (inheritWbAccessFromGroups.apply() == false){
                    System.err.println(inheritWbAccessFromGroups.getStatus());
                    System.exit(2);

                }else{
                    System.out.println("Success");
                    System.exit(0);
                }
                         
            }else{
                System.err.println("Error during preparation");
                System.err.println(inheritWbAccessFromGroups.getStatus());
                System.exit(2);
            }
            
        }else{
            System.out.println("Invalid number of arguments");
            System.exit(3);
        }
    } // end static void main(String[] args)
}
