#!/bin/ksh

echo "###################### START: Domain Full Install"
scriptTimestamp=$(date +%y%m%d_%H%M%S)


# Check Parameters
numParams=$#
dateSet=$1

if [[ ${numParams} -ne 1 ]]
then
    echo ":ERROR: Wrong Paramenters entered. Date parameter missing (format YYYYMMDD)"
    exit 100
elif [[ ${#dateSet} -ne 8 ]]
then
   echo ":ERROR: Invalid date parameter. Date parameter must be with the format YYYYMMDD"
   exit 110
else
  checkDate=$(date +%Y%m%d -d ${dateSet})
  if [[ "$?" -ne "0" ]]; then
   echo ":ERROR: Invalid date. Please make sure you enter a valid date with the format YYYYMMDD"
   exit 120
  fi
fi



# Check RPAS_HOME Directory
if [[ -z $RPAS_HOME ]]; then
	echo ":ERROR: RPAS_HOME is not set. Please execute the retaillogin.ksh script."
	exit 130
fi

if [ -z $O_APP_INSTANCE -o -z $O_APPLICATION ]; then
	echo ":ERROR: O_APP_INSTANCE or O_APPLICATION is not set. Please set the offre O_APP_INSTANCE home path where all solution files are and the O_APPLICATION that specifies the solution name."
	echo "        If the offre Domain has been previously built, then the folder structure should contain:"
	echo "          - config              - OFFRE RPAS Configuration files"
	echo "          - domain              - OFFRE RPAS Domain files"		
	echo "          - logs                - Offre Solution main log files."
	echo "          - batch               - All batch scriots and control files for the Offre Solution."
	echo "        "
	echo "        Otherwise it should be an empty folder where to store the OFFRE solution."
	exit 140
fi

echo "###################### START: check file system structure"
# Check batch Directory
cd $O_APP_INSTANCE/batch
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Folder batch do not exist $O_APP_INSTANCE/batch"
    exit 150
fi

# Check $O_APP_INSTANCE/environment Directory
if [[ -z $O_APP_INSTANCE/environment ]]; then
	echo ":ERROR: $O_APP_INSTANCE/environment does not exists."
	exit 160
fi

# Check inbound data Directory
if [[ -z $RETL_IN ]]; then
	echo ":ERROR: RETL_IN is not set. Please set the RETL_IN pointing to the SFTP folder that contains the inbound data of the solution."
	exit 170
fi

# Check outbound data Directory
if [[ -z $RETL_OUT ]]; then
	echo ":ERROR: RETL_OUT is not set. Please set the RETL_OUT pointing to the SFTP folder that contains the outbound data of the solution."
	exit 180
fi


# check if domain folder already exist
if [[ -d $O_APP_INSTANCE/domain/$O_APPLICATION ]]; then
	#echo ":INFO: Remove existing domain."
	#rm -r $O_APP_INSTANCE/domain/$O_APPLICATION
	echo ":ERROR: The domain already exists. Please remove existing domain before run the build domain script."
    exit 190
fi 

# Create Domain folder if does not exist
if [[ -z $O_APP_INSTANCE/domain ]]; then
	echo ":INFO: Create domain folder"
	mkdir $O_APP_INSTANCE/domain
fi 

# Create log folder if does not exist
if [[ -z $O_APP_INSTANCE/logs/installationlogs ]]; then
	echo ":INFO: Create installation log folder"
	mkdir -p $O_APP_INSTANCE/logs/installationlogs
fi 

# Create RETL error log folder if does not exist
if [[ -z $RETLforXXADEO/error ]]; then
	echo ":INFO: Create RETL Error log folder"
	mkdir -p $RETLforXXADEO/error
fi 

# Create RETL error log folder if does not exist
if [[ -z $RETLforXXADEO/log ]]; then
	echo ":INFO: Create RETL Error log folder"
	mkdir -p $RETLforXXADEO/log
fi 
echo ":INFO: check file system structure - Success"
echo "###################### END: check file system structure"

# set execution permissions on batch scripts
echo "###################### START: privileges to $O_APP_INSTANCE/batch"
chmod -R +x $O_APP_INSTANCE/batch
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Can't set privileges to $O_APP_INSTANCE/batch"
    exit 200
else
	echo ":INFO: privileges to $O_APP_INSTANCE/batch - Success"
fi
echo "###################### END: privileges to $O_APP_INSTANCE/batch"



# ensure correct encoding 
## 20190217 MCO: This step is executed by the deploy script instead
##
#echo "###################### START: dos2unix for sh, schema, ctl and env filess"
#find $O_APP_INSTANCE -name "*sh" -exec dos2unix {} + 
#find $O_APP_INSTANCE -name "*.schema" -exec dos2unix {} +
#find $O_APP_INSTANCE -name "*.env" -exec dos2unix {} +
#find $O_APP_INSTANCE -name "*.ctl" -exec dos2unix {} +
#find $O_APP_INSTANCE -name "*.schema" -exec dos2unix {} +
#find $O_APP_INSTANCE -name "*.pl" -exec dos2unix {} +
#if [ "$?" -ne "0" ]; then
#    echo ":ERROR: Can't perform dos2unix for sh, schema, ctl and env files"
#    exit 205
#fi
#echo "###################### END: dos2unix for sh, schema and env files"
#
## Execute RETL expected files
echo "###################### START: Check for required data files to perform RETL scripts"
##check if files are as expected
listOfFiles=(
"rmse_rpas_clndmstr.dat" "rmse_rpas_orghier.dat" "rmse_rpas_store.dat" "rmse_rpas_merchhier.dat" "rmse_rpas_item_master.dat" "rmse_rpas_suppliers.dat" 
)

for filename in "${listOfFiles[@]}"
do
  if [[ ! -f $RETL_IN/$filename ]]; then
   echo "ERROR: $RETL_IN/$filename does not exists in  RETL_IN"
   exit 210
  fi
done

#	RELATED ITEMS
FILE_1=`ls $RETL_IN | sort -r | grep 'XXRMS218_RELATEDITEMS_.*\.dat' | head -1 | wc -l`
if [[ $FILE_1 -eq "0" ]]; then
	echo "ERROR: Required File not found: $RETL_IN/XXRMS218_RELATEDITEMS_.*\.dat. Please check that the source system has generated this file."
	exit 211
fi
#	ITEM ATTRIBUTES
FILE_2=`ls $RETL_IN | sort -r | grep 'XXRMS209_ITEMATTR_.*\.dat' | head -1 | wc -l`
if [[ $FILE_2 -eq "0" ]]; then
	echo "ERROR: Required File not found: $RETL_IN/XXRMS209_ITEMATTR_.*\.dat. Please check that the source system has generated this file."
	exit 212
fi
#	ATTRIBUTES -Only to get an unic attrib
FILE_3=`ls $RETL_IN | sort -r | grep 'XXRMS209_ATTR_.*\.dat' | head -1 | wc -l`
if [[ $FILE_3 -eq "0" ]]; then
	echo "ERROR: Required File not found: $RETL_IN/XXRMS209_ATTR_.*\.dat. Please check that the source system has generated this file."
	exit 213
fi
echo ":INFO: Check for required data files to perform RETL scripts - Success"
echo "###################### END: Check for required data files to perform RETL scripts"

echo "###################### Set the vdate.txt to allow execution of RETL before domain is created."
echo ${dateSet} > ${RETLforXXADEO}/rfx/etc/vdate.txt

## Execute RETL transformations to gerenate the hierarchy files
echo "###################### START: RETL xxadeo_catmant_calendar_hier.ksh"
ksh $RETLforXXADEO/rfx/src/xxadeo_catmant_calendar_hier.ksh
if [[ "$?" -ne "0" ]]; then
   	echo ":ERROR: Error executing $RETLforXXADEO/rfx/src/xxadeo_catmant_calendar_hier.ksh"
	exit 220
else
echo ":INFO: RETL xxadeo_catmant_calendar_hier.ksh execution - Success"
fi
echo "###################### END: RETL xxadeo_catmant_calendar_hier.ksh"
echo "###################### START: RETL xxadeo_catmant_hier_org.ksh"
ksh $RETLforXXADEO/rfx/src/xxadeo_catmant_hier_org.ksh
if [[ "$?" -ne "0" ]]; then
   	echo ":ERROR: Error executing $RETLforXXADEO/rfx/src/xxadeo_catmant_hier_org.ksh"
	exit 221
else
echo ":INFO: RETL xxadeo_catmant_hier_org.ksh execution - Success"
fi
echo "###################### END: RETL xxadeo_catmant_hier_org.ksh"
echo "###################### START: RETL xxadeo_catmant_nbitemlie_csv_rpl.ksh"
ksh $RETLforXXADEO/rfx/src/xxadeo_catmant_nbitemlie_csv_rpl.ksh
if [[ "$?" -ne "0" ]]; then
   	echo ":ERROR: Error executing $RETLforXXADEO/rfx/src/xxadeo_catmant_nbitemlie_csv_rpl.ksh"
	exit 222
else
echo ":INFO: RETL xxadeo_catmant_nbitemlie_csv_rpl.ksh execution - Success"
fi
echo "###################### END: RETL xxadeo_catmant_nbitemlie_csv_rpl.ksh"
echo "###################### START: RETL xxadeo_catmant_merchhier.ksh"
ksh $RETLforXXADEO/rfx/src/xxadeo_catmant_merchhier.ksh
if [[ "$?" -ne "0" ]]; then
   	echo ":ERROR: Error executing $RETLforXXADEO/rfx/src/xxadeo_catmant_merchhier.ksh"
	exit 223
else
echo ":INFO: RETL xxadeo_catmant_merchhier.ksh execution - Success"
fi
echo "###################### END: RETL xxadeo_catmant_merchhier.ksh"
echo "###################### START: RETL xxadeo_simtart_suppliers.ksh"
ksh $RETLforXXADEO/rfx/src/xxadeo_simtart_suppliers.ksh
if [[ "$?" -ne "0" ]]; then
   	echo ":ERROR: Error executing $RETLforXXADEO/rfx/src/xxadeo_simtart_suppliers.ksh"
	exit 224
else
echo ":INFO: RETL xxadeo_simtart_suppliers.ksh execution - Success"
fi
echo "###################### END: RETL xxadeo_simtart_suppliers.ksh"

echo "###################### START: Check for required hierarchy files to perform domain build"
##check if files are as expected
listOfFiles=(
"clnd.csv.dat" "prod.csv.dat" "pror.csv.dat" "loc.csv.dat" "locr.csv.dat" "splr.csv.dat" 
"clst.csv.dat" "amod.csv.dat" "asv.csv.dat" "cmsh.csv.dat"
"vlrh.csv.dat" "wgrh.csv.dat" "whsh.csv.dat" "warh.csv.dat" "curh.csv.dat" "curx.csv.dat" "loph.csv.dat" 
"ulph.csv.dat" "cish.csv.dat" "cozh.csv.dat" "inch.csv.dat" "pcth.csv.dat" "ptrh.csv.dat" "cirh.csv.dat"
)

for filename in "${listOfFiles[@]}"
do
  if [[ ! -f $RETL_IN/$filename ]]; then
   echo "ERROR: $RETL_IN/$filename does not exists in  RETL_IN"
   exit 230
  fi

done

echo ":INFO: Check for required hierarchy files to perform domain build - Success"
echo "###################### END: Check for required hierarchy files to perform domain build"

#Run Build Domain
echo "###################### START: RPASINSTALL BUILD DOMAIN"
logfilename=build_offre_$(date +%Y%m%d_%H%M).log
echo  "Running command:  rpasInstall -fullinstall -cn $O_APPLICATION -ch $O_APP_INSTANCE/config -dh $O_APP_INSTANCE/domain -log $O_APP_INSTANCE/logs/installationlogs/$logfilename -skipvalidation -rf AppFunctions -in $RETL_IN -p sdep"
rpasInstall -fullinstall -cn $O_APPLICATION -ch $O_APP_INSTANCE/config -dh $O_APP_INSTANCE/domain -log $O_APP_INSTANCE/logs/installationlogs/$logfilename -skipvalidation -rf AppFunctions -in $RETL_IN -p sdep

#Check if file exists
if [[ ! -f $O_APP_INSTANCE/logs/installationlogs/$logfilename ]]; then
    echo ":ERROR: RpasInstall build log file not found"
    exit 240
fi

errorcount=`grep -i -e error -e fail -e excep $O_APP_INSTANCE/logs/installationlogs/$logfilename | wc -l`

if [[ "$?" -ne "0" ]]; then
   	echo ":ERROR: Offre Domain Build Error. Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename"
	exit 241
elif [[ $errorcount -ne "0" ]]; then
	echo "ERROR: Offre Domain Build Error. Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename"
	exit 242
else
	echo ":INFO: PASINSTALL BUILD DOMAIN - Success"
    echo "###################### END: RPASINSTALL BUILD DOMAIN"
fi

echo "###################### START: PRINT Configuration Version"
domaininfo -d $GLOBAL_DOMAIN -history
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Can't check configuration version"
    exit 250
fi
echo "###################### END: PRINT Configuration Version"


logfilename_batch=post_build_offre_$(date +%Y%m%d_%H%M).log

echo "###################### START: Create VDATE Files (domain and RETL)"
echo "Create $GLOBAL_DOMAIN/vdate.int"
if [[ -f $GLOBAL_DOMAIN/vdate.int ]]
then  
  rm $GLOBAL_DOMAIN/vdate.int
fi
ksh $BATCH_HOME/scripts/orc_update_vdate.ksh set ${dateSet} >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command: $BATCH_HOME/scripts/orc_update_vdate.ksh set ${dateSet}"
    exit 260
fi

echo "Create $RETLforXXADEO/rfx/etc/vdate.txt"
cp $GLOBAL_DOMAIN/vdate.int $RETLforXXADEO/rfx/etc/vdate.txt
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command: cp $GLOBAL_DOMAIN/vdate.int $RETLforXXADEO/rfx/etc/vdate.txt"
    exit 261
fi
echo ":INFO:  Create VDATE Files (domain and RETL) - Success"
echo "###################### END: Create VDATE Files (domain and RETL)"

#RUN Minimum batch rule groups to allow ADMIN Workbook to open before the first batch
#Change the default value for position security
#Set Costing constants
echo "###################### START: Run MACE for RuleGroups"
ldoms=$(domaininfo -d ${GLOBAL_DOMAIN} -shellsubdomains -loglevel none)
echo "#### Running rule group Com_Batch_gb in the global domain"
mace -d $O_APP_INSTANCE/domain/$O_APPLICATION -run -group Com_Batch_gb >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command: mace -d $O_APP_INSTANCE/domain/$O_APPLICATION -run -group Com_Batch_gb . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
    exit 270
fi

echo "#### Running rule group Ctm_Bat_Gb in the global domain"
mace -d $O_APP_INSTANCE/domain/$O_APPLICATION -run -group Ctm_Bat_Gb >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command: mace -d $O_APP_INSTANCE/domain/$O_APPLICATION -run -group Ctm_Bat_Gb . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
    exit 271
fi

for ldom in ${ldoms}
do
 echo "#### Running rule group Com_Batch_l in ${ldom}"
 mace -d ${ldom} -run -group Com_Batch_l >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
 if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command:  mace -d ${ldom} -run -group Com_Batch_l . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
    exit 272
 fi
 
 echo "#### Running rule group Com_Bat_L in ${ldom}"
 mace -d ${ldom} -run -group Ctm_Bat_L >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
 if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command:  mace -d ${ldom} -run -group Ctm_Bat_L . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
    exit 273
 fi
 
done

echo "#### Running rule group Ctm_Bat_Prng3_G in the global domain"
mace -d $O_APP_INSTANCE/domain/$O_APPLICATION -run -group Ctm_Bat_Prng1_G >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command:  mace -d $O_APP_INSTANCE/domain/$O_APPLICATION -run -group Ctm_Bat_Prng1_G . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
    exit 274
fi

for ldom in ${ldoms}
do
 echo "#### Running rule group Ctm_Bat_Prng2_L in ${ldom}"
 mace -d ${ldom} -run -group Ctm_Bat_Prng2_L >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
 if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command:  mace -d ${ldom} -run -group Ctm_Bat_Prng2_L . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
    exit 275
 fi
done

echo "#### Running rule group Ctm_Bat_Prng3_G in the global domain"
mace -d $O_APP_INSTANCE/domain/$O_APPLICATION -run -group Ctm_Bat_Prng3_G >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
 if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command:  mace -d $O_APP_INSTANCE/domain/$O_APPLICATION -run -group Ctm_Bat_Prng3_G . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
    exit 276
 fi

for ldom in ${ldoms}
do
echo "#### Running rule group Ctm_Bat_Prng4_L in ${ldom}"
 mace -d ${ldom} -run -group Ctm_Bat_Prng4_L  >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
  if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Error found while executing command: mace -d ${ldom} -run -group Ctm_Bat_Prng4_L . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
    exit 277
  fi 
done
echo ":INFO:  Run MACE for RuleGroups"
echo "###################### END: Run MACE for RuleGroups"

echo "###################### START: Change the position security default value"
ksh $BATCH_HOME/scripts/orc_adjust_NA_value.ksh Com_Position_Level_Security_Measures.ctl >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
if [[ "$?" -ne "0" ]]; then
  echo ":ERROR: Error found while executing command: ksh $BATCH_HOME/scripts/orc_adjust_NA_value.ksh Com_Position_Level_Security_Measures.ctl . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
  exit 280
fi
echo ":INFO:  Change the position security default value - Success"
echo "###################### END: Change the position security default value"

echo "###################### START: Set Costing NA values"
ksh $BATCH_HOME/scripts/orc_load_constants.ksh Cst_Load_Costants.ctl >> $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch
if [[ "$?" -ne "0" ]]; then
  echo ":ERROR: Error found while executing command: ksh $BATCH_HOME/scripts/orc_load_constants.ksh Cst_Load_Costants.ctl . Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename_batch for details."
  exit 290
fi
echo ":INFO: Set Costing NA values - Success"
echo "###################### END: Set Costing NA values"

#SET MLDM Property
echo "###################### START: DomainProp"
domainprop -d $GLOBAL_DOMAIN -property "enable_mldm=true"
domainprop -d $GLOBAL_DOMAIN -property "enable_mldm"
if [[ "$?" -ne "0" ]]; then
  echo ":ERROR: Error found while executing command: domainprop -d $GLOBAL_DOMAIN -property"
  exit 300
fi
echo ":INFO: DomainProp - Success"
echo "###################### END: DomainProp"

echo "###################### END: Domain Full Install - Success"
exit 0