#!/bin/ksh

echo "###################### START: Domain Patch Install"
scriptTimestamp=$(date +%y%m%d_%H%M%S)


# Check Parameters
numParams=$#
deletestyles=$1

if [[ ${numParams} -eq 1 ]]
then
    if [[ ! "$deletestyles" =~ ^(Y|N)$ ]] 
	then
		echo ":ERROR: Invalid Parameter. Parameter should indicate if styles should be removed (Y/N)"
		exit 100	
	else
		echo ":INFO: Delete Styles Parameter set to '${deletestyles}'"
	fi
elif [[ ${numParams} -eq 0 ]]
then
   echo ":INFO: Delete Styles Parameter not entered. Default value 'Y' is assumed for this patch."
   deletestyles="Y"
else
   echo ":ERROR: Invalid Parameter. Parameter should indicate if styles should be removed (Y/N)"
   exit 101 
fi


# Check RPAS_HOME Directory
if [[ -z $RPAS_HOME ]]; then
	echo ":ERROR: RPAS_HOME is not set. Please execute the retaillogin.ksh script."
	exit 110
fi

if [ -z $O_APP_INSTANCE -o -z $O_APPLICATION ]; then
	echo ":ERROR: O_APP_INSTANCE or O_APPLICATION is not set. Please set the offre O_APP_INSTANCE home path where all solution files are and the O_APPLICATION that specifies the solution name."
	echo "        If the offre Domain has been previously built, then the folder structure should contain:"
	echo "          - config              - OFFRE RPAS Configuration files"
	echo "          - domain              - OFFRE RPAS Domain files"		
	echo "          - logs                - Offre Solution main log files."
	echo "          - batch               - All batch scriots and control files for the Offre Solution."
	echo "        "
	echo "        Otherwise it should be an empty folder where to store the OFFRE solution."
	exit 120
fi

echo "###################### START: check file system structure"
# Check batch Directory
cd $O_APP_INSTANCE/batch
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Folder batch do not exist $O_APP_INSTANCE/batch"
    exit 130
fi

# Check $O_APP_INSTANCE/environment Directory
if [[ -z $O_APP_INSTANCE/environment ]]; then
	echo ":ERROR: $O_APP_INSTANCE/environment does not exists."
	exit 140
fi

# Check inbound data Directory
if [[ -z $RETL_IN ]]; then
	echo ":ERROR: RETL_IN is not set. Please set the RETL_IN pointing to the SFTP folder that contains the inbound data of the solution."
	exit 150
fi

# Check outbound data Directory
if [[ -z $RETL_OUT ]]; then
	echo ":ERROR: RETL_OUT is not set. Please set the RETL_OUT pointing to the SFTP folder that contains the outbound data of the solution."
	exit 160
fi

# Check Domain Directory
if [[ ! -d $O_APP_INSTANCE/domain/$O_APPLICATION ]]; then
	echo ":ERROR: Do not exist a current OFFRE domain to be patched."
	exit 170
fi 

# Create log folder if does not exist
if [[ -z $O_APP_INSTANCE/log/installationlogs ]]; then
	echo ":INFO: Create installation log folder"
	mkdir -p $O_APP_INSTANCE/logs/installationlogs
fi 
echo ":INFO: check file system structure - Success"
echo "###################### END: check file system structure"

# set execution permissions on batch scripts
echo "###################### START: privileges to $BATCH_HOME"
chmod -R +x $O_APP_INSTANCE/batch
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Can't set privileges to $BATCH_HOME"
    exit 180
else
	echo ":INFO: privileges to $BATCH_HOME - Success"
fi
echo "###################### END: privileges to $BATCH_HOME"


# ensure correct encoding 
## 20190217 MCO: This step is executed by the deploy script instead
##
#echo "###################### START: dos2unix for sh, schema, ctl and env files"
#find $O_APP_INSTANCE -name "*sh" -exec dos2unix {} + 
#find $O_APP_INSTANCE -name "*.env" -exec dos2unix {} +
#find $O_APP_INSTANCE -name "*.ctl" -exec dos2unix {} +
#find $O_APP_INSTANCE -name "*.schema" -exec dos2unix {} +
#find $O_APP_INSTANCE -name "*.pl" -exec dos2unix {} +
#if [ "$?" -ne "0" ]; then
#    echo ":ERROR: Can't perform dos2unix for sh, schema, ctl and env files"
#    exit 190
#fi
#echo "###################### END: dos2unix for sh, schema and env files"



#Remove saved styles before Patch
echo "###################### START: REMOVE SAVED STYLES"

if [[ ${deletestyles} = "Y" ]]
then
	LOCAL_DOMAINS=($(domaininfo -d $GLOBAL_DOMAIN -shellsubdomains | awk '(index($0, "ldom") != 0){print $1}'))
    STYLES=($( ls $GLOBAL_DOMAIN/styles/ | awk '{print $1}'))
	
   echo ":INFO: Deleting all workbook styles"
   # remove global domain styles
   rm -R $GLOBAL_DOMAIN/styles/*

   # remove local domain styles
   for i in "${LOCAL_DOMAINS[@]}" 
   do
      rm -r $i/styles/*
   done
   echo ":INFO: All workbook styles Deleted Successfully."
else
 echo ":INFO: No saved styles were deleted. The Patch was executed with the parameter to keep the saved styles."
fi

echo "###################### END: REMOVE SAVED STYLES"


#Run Patch Domain
echo "###################### START: RPASINSTALL PATCH DOMAIN"
logfilename=patch_offre_$(date +%Y%m%d_%H%M).log

if [[ ${deletestyles} = "Y" ]]
then
	echo  "Running command updating style: rpasInstall -patchinstall -cn $O_APPLICATION -ch $O_APP_INSTANCE/config -dh $O_APP_INSTANCE/domain -log $O_APP_INSTANCE/logs/installationlogs/$logfilename -skipvalidation -rf AppFunctions -updatestyles -in $RETL_IN -p sdep"
	rpasInstall -patchinstall -cn $O_APPLICATION -ch $O_APP_INSTANCE/config -dh $O_APP_INSTANCE/domain -log $O_APP_INSTANCE/logs/installationlogs/$logfilename -skipvalidation -rf AppFunctions -updatestyles -in $RETL_IN -p sdep
else
	echo  "Running command without updating styles: rpasInstall -patchinstall -cn $O_APPLICATION -ch $O_APP_INSTANCE/config -dh $O_APP_INSTANCE/domain -log $O_APP_INSTANCE/logs/installationlogs/$logfilename -skipvalidation -rf AppFunctions -in $RETL_IN -p sdep"
	rpasInstall -patchinstall -cn $O_APPLICATION -ch $O_APP_INSTANCE/config -dh $O_APP_INSTANCE/domain -log $O_APP_INSTANCE/logs/installationlogs/$logfilename -skipvalidation -rf AppFunctions -in $RETL_IN -p sdep
fi

#Check error in the log
errorcount=`grep -i -e error -e fail -e excep $O_APP_INSTANCE/logs/installationlogs/$logfilename | wc -l`

if [[ "$?" -ne "0" ]]; then
   	echo ":ERROR: Offre Domain Patch Error. Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename"
	exit 200
elif [[ $errorcount -ne "0" ]]; then
	echo "ERROR: Offre Domain Patch Error. Please check $O_APP_INSTANCE/logs/installationlogs/$logfilename"
	exit 201
else
    echo ":INFO: RPASINSTALL PATCH DOMAIN - Success"
    echo "###################### END: RPASINSTALL PATCH DOMAIN"
fi

echo "###################### START: PRINT Configuration Version"
domaininfo -d $GLOBAL_DOMAIN -history
if [[ "$?" -ne "0" ]]; then
    echo ":ERROR: Can't check configuration version"
    exit 300
fi
echo "###################### END: PRINT Configuration Version"

echo "###################### END Domain Patch Install - Success"
exit 0

