# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs


#XXADEO Variables
export RETLforXXADEO=/u01/data/rpassys/offre/batch/RETLforXXADEO
#export RETL_IN=$RETLforXXADEO/data/inbound
#export RETL_OUT=$RETLforXXADEO/data/outbound
export RFX_HOME=/u01/product/retlsys
export RFX_TMP=$RFX_HOME/tmp
export LC_ALL=C


source /u01/product/rpassys/offre/retaillogin.ksh
export LD_LIBRARY_PATH="$JAVA_HOME/lib:$JAVA_HOME/lib/amd64/server:$LD_LIBRARY_PATH"
export RPAS_JAVA_CLASSPATH=$RPAS_HOME/applib/aaijni.jar:$RPAS_HOME/applib/aaiCatMan.jar:$RPAS_HOME/applib/rseCatMan.jar:$RPAS_HOME/lib/rpasjni.jar:$RPAS_HOME/lib/oracleRpasUtils.jar:$RPAS_JAVA_CLASSPATH
export PATH=$PATH:$LD_LIBRARY_PATH

export BSA_PATH=/u01/product/rpassys/offre/rpas/bin
#export O_APPLICATION="offre"
#export O_APP_INSTANCE=/u01/data/rpassys/offre

export PATH=$BSA_PATH:$RPAS_BASE/bin:$RPAS_BASE/lib:$HOME/bin:$PATH

export SCRIPT=/u01/scripts/dev01/dhmbudbrpac14


export O_APPLICATION_HOME=/u01/data/rpassys
export O_APPLICATION=offre
export O_INSTANCE=dev
export O_APP_INSTANCE=${O_APPLICATION_HOME}/offre
export BATCH_HOME=${O_APP_INSTANCE}/batch
export DOMAIN_HOME=$O_APP_INSTANCE/domain
export GLOBAL_DOMAIN=${DOMAIN_HOME}/${O_APPLICATION}
export BACKUP_DIR=${O_APP_INSTANCE}/backup
export INTEGRATION_HOME=${O_APP_INSTANCE}/integration
export RETL_IN=$INTEGRATION_HOME/inbound
export RETL_OUT=$INTEGRATION_HOME/outbound

alias startoffrerpas="ksh $BATCH_HOME/scripts/orc_start_daemon.ksh daemon_offre.ctl"
alias stopoffrerpas="ksh $BATCH_HOME/scripts/orc_stop_daemon.ksh daemon_offre.ctl"
alias offrerpasusers="DomainDaemon -port 7013 -showActiveServers -ssl 2"

alias startoffrerpas
alias stopoffrerpas
alias offrerpasusers

#NEEDED FOR BATCH ENV
#source /u01/product/rpassys/offre/batch/offre_dev_env.sh
#source /u01/product/rpassys/offre/batch/offre_env_rpas.sh
#source /u01/product/rpassys/offre/batch/offre_common_rpas.sh

# Initialise the RETL RFX Path
if [[ -d ${RFX_HOME} ]];then
        export PATH=$RFX_HOME/bin:$RFX_TMP:${PATH}
else
        echo "$PROGRAM_NAME Terminated with Error: $RFX_HOME does not exist"
        return 1
fi
