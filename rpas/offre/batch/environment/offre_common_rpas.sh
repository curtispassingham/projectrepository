#/usr/bin/ksh
#
# Copyright © 2016, Oracle. All rights reserved.
#
# Oracle Highly Confidential Material.
# Do Not Distribute Without Oracle Approval. Use Is Subject To License Terms.
#
# $Revision: 0.1
# $Date: 2016/05/01
# $Author: Oracle Retail Consulting
#
# Version              Change                        Author
# 0.1                  Initial Create                Oracle Retail Consulting
#
##############################################################################
# offre_common_rpas.sh
#
# This is the common "include" script for all RDF scripts
#
# Args: None
#
# This file should be "dot-included" in all scripts as in this example:
# . offre_common_rpas.sh
#
##############################################################################

#
# Guard against re-sourcing
#
if [[ "${_offre_common_rpas_sh-0}" = 1 ]]; then
   return 0
else
   typeset -i -r _offre_common_rpas_sh=1
fi

#
# Source the specialized environment
#
. ${O_APP_INSTANCE}/batch/environment/offre_env_rpas.sh

#
# Complete the BSA environment setup
#
. bsa_common.sh
