#CP EDIT 9

#archiving files
# 1 : identifier
# 2 : source directory
# 3 : source subdirectory
# 4 : filename pattern
# 5 : destination directory
# 6 : destination subdirectory
# 7 : destination filename pattern (if different)

#input
RMS_OFFRE_FTMEDNLD|mv|RETL_IN||rmse_rpas_clndmstr.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_FTMEDNLD|mv|RETL_IN||clnd.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_MERCHHIER|mv|RETL_IN||rmse_rpas_merchhier.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_ITEM_MASTER|mv|RETL_IN||rmse_rpas_item_master.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_ASSORTGROUP|mv|RETL_IN||assortGroup.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEM_1|mv|RETL_IN||prod.csv.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEM_2|mv|RETL_IN||pror.csv.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEM_3|mv|RETL_IN||rpas_pack_item.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEM_4|mv|RETL_IN||itemlie.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_ITEM_FLAGS_1|mv|RETL_IN||XXRMS214_ITEMFLAGS*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_ITEM_FLAGS_2|mv|RETL_IN||itemCreFlag.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_ITEM_ATTR|mv|RETL_IN||XXRMS209_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEM_ATTR_1|mv|RETL_IN||art_UDA.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEM_ATTR_2|mv|RETL_IN||artBU_UDA.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEM_ATTR_3|mv|RETL_IN||art_CFA.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_MERCHHIERTL|mv|RETL_IN||XXRMS216_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_1|mv|RETL_IN||r_artclabel.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_2|mv|RETL_IN||r_artrlabel.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_3|mv|RETL_IN||r_com*label.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_4|mv|RETL_IN||r_dep*label.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_5|mv|RETL_IN||r_der*label.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_6|mv|RETL_IN||r_gpr*label.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_7|mv|RETL_IN||r_prn*label.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_8|mv|RETL_IN||r_sde*label.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_9|mv|RETL_IN||r_sty*label.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_MERCHHIERTL_10|mv|RETL_IN||r_typ*label.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_ITEMMASTERTL|mv|RETL_IN||XXRMS219_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEMMASTERTL|mv|RETL_IN||desgn_sec.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_ITEMATTRTL|mv|RETL_IN||XXRMS220_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ITEMATTRTL|mv|RETL_IN||r_measpicklist.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_ITEMPACK|mv|RETL_IN||XXRMS221_ITEMPACK_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_ORGHIER|mv|RETL_IN||rmse_rpas_orghier.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGHIER_1|mv|RETL_IN||loc.csv.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGHIER_2|mv|RETL_IN||locr.csv.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_RELATEDITEM|mv|RETL_IN||XXRMS218_RELATEDITEMS_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_RELATEDITEM|mv|RETL_IN||nbitemlie.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_ORGTLHIER|mv|RETL_IN||XXADEO_XXRMS227_ORGHIER_TRANS_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_1|mv|RETL_IN||r_str1label.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_2|mv|RETL_IN||r_st1rlabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_3|mv|RETL_IN||r_str2label.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_4|mv|RETL_IN||r_st2rlabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_5|mv|RETL_IN||r_regnlabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_6|mv|RETL_IN||r_regrlabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_7|mv|RETL_IN||r_bullabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_8|mv|RETL_IN||r_burlabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_9|mv|RETL_IN||r_zonelabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_10|mv|RETL_IN||r_zonrlabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_11|mv|RETL_IN||r_grpolabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_ORGTLHIER_12|mv|RETL_IN||r_grprlabel.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_STORESHIER|mv|RETL_IN||rmse_rpas_store.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_STORESATTRIBUTES|mv|RETL_IN||attributsMag.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_STORESATTRIBUTESREJ|mv|RETL_IN||xxadeo_catmant_store_attributes.rej|RETL_IN|../inbound_processed||
RMS_OFFRE_DST_STORESATTRIBUTES|mv|RETL_IN||attributsMag.csv.dat|RETL_IN|../inbound_processed||
#RMS_OFFRE_WARH|mv|RETL_IN||warh.csv.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_ARTICLEPHOTO|mv|RETL_IN||rmse_catman_article_photo.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_MESSTD|mv|RETL_IN||mes_std.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_ARTICLESUPPLIER|mv|RETL_IN||art_Four.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_ARTICLEBUSUPPLIER|mv|RETL_IN||art_Four_BU.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_ITMSUPCOSTBU|mv|RETL_IN||XXOR225_ITMSUPCOSTBU_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_ITMSUPCOSTLOC|mv|RETL_IN||XXOR225_ITMSUPCOSTLOC_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_ITMSUPFUTURCOST|mv|RETL_IN||XXOR225_ITMSUPFUTURCOST_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_INCOTERM|mv|RETL_IN||incoterm.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_MAINSUPPLIER|mv|RETL_IN||XXRMS255_FOURNISSEURPRINCIPAL_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_ITEMPCB|mv|RETL_IN||XXOR330_ITEMPCB_*.dat|RETL_IN|../inbound_processed||
RMS_OFFRE_DELAYPAYMENT|mv|RETL_IN||delay_pay.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_PLANPAL|mv|RETL_IN||plan_pal.csv*|RETL_IN|../inbound_processed||

RMS_OFFRE_XXADEO_ASSORTMENT|mv|RETL_IN||XXADEO_ASSORTMENT.csv.*|RETL_IN|../inbound_processed||
RMS_OFFRE_XXRMS217_SUBSTITUTEITEMS|mv|RETL_IN||XXRMS217_SUBSTITUTEITEMS*|RETL_IN|../inbound_processed||
RMS_OFFRE_LOCRSECURITY|mv|RETL_IN||locrsecurity*|RETL_IN|../inbound_processed||
#RMS_OFFRE_PR_VENTE_MAG|mv|RETL_IN||pr_vente_mag.csv*|RETL_IN|../inbound_processed||
#RMS_OFFRE_PRODUITMATCHE|mv|RETL_IN||produitMatche.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_PRORSECURITY|mv|RETL_IN||prorsecurity*|RETL_IN|../inbound_processed||
#RMS_OFFRE_R_ASVLLABEL|mv|RETL_IN||r_asvllabel.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_SPLR|mv|RETL_IN||splr.csv*|RETL_IN|../inbound_processed||
RMS_OFFRE_RMSE_RPAS_SUPPLIERS|mv|RETL_IN||rmse_rpas_suppliers*|RETL_IN|../inbound_processed||
RMS_OFFRE_RMSE_RPAS_WH|mv|RETL_IN||rmse_rpas_wh*|RETL_IN|../inbound_processed||
RMS_OFFRE_RMSE_RPAS_WEEKLY|mv|RETL_IN||rmse_rpas_weekly*|RETL_IN|../inbound_processed||
RMS_OFFRE_RMSE_RPAS_STOCK|mv|RETL_IN||rmse_rpas_stock*|RETL_IN|../inbound_processed||
RMS_OFFRE_RMSE_RPAS_ATTRIBUTES|mv|RETL_IN||rmse_rpas_attributes*|RETL_IN|../inbound_processed||
RMS_OFFRE_RMSE_RPAS_DAILY_SALES|mv|RETL_IN||rmse_rpas_daily_sales*|RETL_IN|../inbound_processed||


#output
CTM_RMS_ASSRT_PRECO|mv|RETL_OUT||XXRMS208_ASSORTMENT_REG_ITEMLOC*dat|RETL_OUT|../outbound_processed||
CTM_RMS_ASSRT_MODE|mv|RETL_OUT||XXRMS208_ASSORTMENT_ITEMBU_AM*dat|RETL_OUT|../outbound_processed||
CTM_RMS_DATES_OC|mv|RETL_OUT||XXRMS208_ASSORTMENT_ITEMBU_ONOF*dat|RETL_OUT|../outbound_processed||
CTM_RMS_CLASSEMENT|mv|RETL_OUT||XXRMS208_ASSORTMENT_ITEMBU_RS*dat|RETL_OUT|../outbound_processed||
CTM_RMS_XXOR203_ASSORTMENT|mv|RETL_OUT||XXOR203_ASSORTMENT*.dat|RETL_OUT|../outbound_processed||
CST_RMS_COUNTRY_EXPENSES|mv|RETL_OUT||country_expenses.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_ZONE_EXPENSES|mv|RETL_OUT||zone_expenses.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_SRC_EXPENSES|mv|RETL_OUT||MAINFRE.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_ITEMEXP|mv|RETL_OUT||XXRMS332_*.dat|RETL_OUT|../outbound_processed||

CST_RMS_ADEOMU|mv|RETL_OUT||ADEOMU.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_AGENCOM|mv|RETL_OUT||AGENCOM.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_AICUSCR|mv|RETL_OUT||AICUSCR.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_BANKFEE|mv|RETL_OUT||BANKFEE.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_CUSTOMS|mv|RETL_OUT||CUSTOMS.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_ECO|mv|RETL_OUT||ECO*.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_ESTIMAT|mv|RETL_OUT||ESTIMAT.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_INLDFRE|mv|RETL_OUT||INLDFRE.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_INTLASS|mv|RETL_OUT||INTLASS.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_MARGCPM|mv|RETL_OUT||MARGCPM.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_MARKGMU|mv|RETL_OUT||MARKGMU.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_PURCHMU|mv|RETL_OUT||PURCHMU.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_QUALITY|mv|RETL_OUT||QUALITY.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_SUPCRED|mv|RETL_OUT||SUPCRED.csv.*|RETL_OUT|../outbound_processed||
CST_RMS_SUPPLYMU|mv|RETL_OUT||SUPPLYMU.csv.*|RETL_OUT|../outbound_processed||
CTM_STP_StorePortal_AsstMode_Translation|mv|RETL_OUT||XXADEO_ASSORTMENT_MODE_TL*|RETL_OUT|../outbound_processed||
CTM_STP_StorePortal_AsstVal_Translation|mv|RETL_OUT||XXADEO_ASSORTMENT_VALUE_TL*|RETL_OUT|../outbound_processed||
CTM_STP_XXADEO_ASSORTMENT_CLUSTER|mv|RETL_OUT||XXADEO_ASSORTMENT_CLUSTER_*|RETL_OUT|../outbound_processed||
CTM_STP_XXADEO_ASSORTMENT_STORE|mv|RETL_OUT||XXADEO_ASSORTMENT_STORE_*|RETL_OUT|../outbound_processed||
CTM_STP_XXADEO_ASSRT_CLUSTERS|mv|RETL_OUT||XXADEO_ASSRT_CLUSTERS_*|RETL_OUT|../outbound_processed||
CTM_STP_XXADEO_ITEM_BU|mv|RETL_OUT||XXADEO_ITEM_BU_*|RETL_OUT|../outbound_processed||
CTM_STP_XXADEO_STORE_CLUSTER|mv|RETL_OUT||XXADEO_STORE_CLUSTER_*|RETL_OUT|../outbound_processed||

#manual files
CTM_CLSTHIER|mv|RETL_IN||clst.csv.dat|RETL_IN|../inbound_processed||
CTM_AMODHIER|mv|RETL_IN||amod.csv.dat|RETL_IN|../inbound_processed||
CTM_ASVHIER|mv|RETL_IN||asv.csv.dat|RETL_IN|../inbound_processed||
CST_CISHHIER|mv|RETL_IN||cish.csv.dat|RETL_IN|../inbound_processed||
CST_COZHHIER|mv|RETL_IN||cozh.csv.dat|RETL_IN|../inbound_processed||
CST_CURHHIER|mv|RETL_IN||curh.csv.dat|RETL_IN|../inbound_processed||
CST_CURXHIER|mv|RETL_IN||curx.csv.dat|RETL_IN|../inbound_processed||
CST_INCHHIER|mv|RETL_IN||inch.csv.dat|RETL_IN|../inbound_processed||
CST_LOPHHIER|mv|RETL_IN||loph.csv.dat|RETL_IN|../inbound_processed||
CST_PCTHHIER|mv|RETL_IN||pcth.csv.dat|RETL_IN|../inbound_processed||
CST_PTRHHIER|mv|RETL_IN||ptrh.csv.dat|RETL_IN|../inbound_processed||
CST_VLRHHIER|mv|RETL_IN||vlrh.csv.dat|RETL_IN|../inbound_processed||
CST_WGRHHIER|mv|RETL_IN||wgrh.csv.dat|RETL_IN|../inbound_processed||
CST_WHSHHIER|mv|RETL_IN||whsh.csv.dat|RETL_IN|../inbound_processed||
CST_CIRHHIER|mv|RETL_IN||cirh.csv.dat|RETL_IN|../inbound_processed||
CST_ULPHHIER|mv|RETL_IN||ulph.csv.dat|RETL_IN|../inbound_processed||
