# Catman conversion control file to split files into measure files
# Control file must be placed in $BATCH_HOME/config/import/data
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Filename defined in CV.027 with extension.
# 2) Last column for dimensions intersections in the input file.
# 3) Output file name, it consists of the measure name with the file extension.
# 4) Position of the measure value in the input file.

################CR275_Dates_Debut/Fin_Ventes_Omnicanal#####################

# Produit matché (cr284) - nb of competitors
produitMatche.csv.ovr|2|xxxxxxxxxxxx.csv.ovr|3

# A COMPLETER 
#flagprixbloque.csv.ovr
#prix_achat.csv.ovr
#prix_achat_futur.csv.ovr
#Boni.csv.ovr
#itemsupmagdwh.csv.ovr
#itemsupmagdwh.csv.ovr
#rfa.csv.ovr


################CR275_Dates_Debut/Fin_Ventes_Omnicanal#####################

