# Costing conversion measures control file
# Control file must be placed in $BATCH_HOME/config/import/data
# Comments must start with # 
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Filename defined in CV.027 without extension, the same file name defined in DataInterface in ConfigTools.
# 2) Definition if the file may contain boolean values to be transformed from Y/N to T/F that are acceptable in RPAS, possible values for this parameter are Y|N
# 3) Definition of whether the measure is required or not, if it is required, the script will return error if the measure file does not exist, possible valuer are OPTIONAL|REQUIRED

# Main Freight (CR291) *
MAINFRE|Y|OPTIONAL
# Main Freight by item (CR291) *
MAINFRE_ART|Y|OPTIONAL
# Inland Freight (CR293) *
INLDFRE|N|OPTIONAL
# Inland Freight par article (CR294) *
INLDFRE_ART|Y|OPTIONAL
# Circuit Principal Article Mapping (CR268) *
circuitPrArt|Y|OPTIONAL
# Circuit Principal Fournisseur (CR269) *
circuitPrFo|N|OPTIONAL
# Custom Duty per article (CR295)
CUSTOM|Y|OPTIONAL
# International Insurance (CR296) *
Inter_Insur|N|OPTIONAL
# Bank Fees (CR297) *
Bank_Fees|N|OPTIONAL
# Agent Commission (CR298) *
Agent_Commission|N|OPTIONAL
# After Sales Provision (CR299)
After_SalesProvision|N|OPTIONAL
# After Sales Provision by article (CR300) *
Pro_SAV|Y|OPTIONAL
# Supplier Payment term (CR301) *
Supplier_Pay_Term|N|OPTIONAL
# Marketing MarkUp AND Purchasing MarkUp (CR302,CR303) *
purchasecostparam|N|OPTIONAL
# Quality Cost (CR304) *
Quality_Cost|N|OPTIONAL
# Quality Cost by article (CR305) *
qualityart|Y|OPTIONAL
# Supply Chain MarkUp (CR306) *
supplychaincostparam|N|OPTIONAL
#  Bu/Article/Supplier Costing Parameters (CR307) *
adeomarkup|N|OPTIONAL
#  ecotax (CR308 A CR322) *
ecotax|N|OPTIONAL
#  Warehouse Stock Cover (CR328) *
stock_cov|N|OPTIONAL
#  Warehouse Stock Cover by art (CR329) *
stock_cov_article|Y|OPTIONAL
# Bu/Article Supply Parameters (CR330 et CR333) *
bu_art_supply|Y|OPTIONAL
# Bu/SR  Supply Cost Parameter (CR331, CR332, CR334, CR335) *
Bu_SR|N|OPTIONAL
# Main Freight POL/POD (CR336) *
MAINFRE_POL_POD|N|OPTIONAL
# Warehouse Reception Unit (WRU) (CR337), Warehouse Picking Unit (WPU) (CR338), Warehouse Exit Unit (WEU) (CR339)
Bu_Art_Suppl_TypEntr|N|OPTIONAL
# Bu/Supplier Packing Dimensions Costing Parameters (CR340 A 344)
Packing_Dim|N|OPTIONAL
# Incoterm fournisseurs (CR346)
incoterm|N|OPTIONAL
# Wharehouse entry and exit cadencies (CR383)
wh_entry_exit_cadency|N|OPTIONAL
# Wharehouse picking cadencies (CR384)
wh_picking_cadency|N|OPTIONAL
# run solve probleme range item bu
ComTyArtcToBuB|N|OPTIONAL
ComTyArtcBuSuplB|N|OPTIONAL
CstT1CPrInvIndCbl|N|OPTIONAL
CstT1CPrMrcIndCbl|N|OPTIONAL
plan_pal|N|OPTIONAL
delay_pay|N|OPTIONAL
ventes|N|OPTIONAL
comp_mge|N|OPTIONAL







