# Simtar conversion measures control file
# Control file must be placed in $BATCH_HOME/config/import/data
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Filename defined in CV.027 without extension, the same file name defined in DataInterface in ConfigTools.
# 2) Definition if the file may contain boolean values to be transformed from Y/N to T/F that are acceptable in RPAS, possible values for this parameter are Y|N
# 3) Definition of whether the measure is required or not, if it is required, the script will return error if the measure file does not exist, possible valuer are OPTIONAL|REQUIRED

# Realized Sales (cr124)
ventes|N|OPTIONAL
# CR268
circuitPrFo|N|OPTIONAL
# CR 269
circuitPrArt|N|OPTIONAL
# Produit matché (cr284)
produitMatche|N|OPTIONAL
# Prix bloqué (cr285)
flagprixbloque|N|OPTIONAL
# Prix achat fournisseur (cr286)
prix_achat|N|OPTIONAL
prix_achat_futur|N|OPTIONAL
# Boni % (cr287)
Boni|N|OPTIONAL
# remise en caisse et MSV (CR288 et CR289)
donnes_dwh|N|OPTIONAL
itemsupmagdwh|N|OPTIONAL
# rfa (CR290)
rfa|N|OPTIONAL
#tva (CR371)
tva|N|OPTIONAL
# Prix de vente magasin (CR372)
pr_vente_mag|N|OPTIONAL
# CR373
qty_rec|N|OPTIONAL








