# Catman conversion control file to split files into measure files
# Control file must be placed in $BATCH_HOME/config/import/data
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Filename defined in CV.027 with extension.
# 2) Last column for dimensions intersections in the input file.
# 3) Output file name, it consists of the measure name with the file extension.
# 4) Position of the measure value in the input file.

################CR275_Dates_Debut/Fin_Ventes_Omnicanal#####################

#Date debut vente omnicanal - CP Omni Channel Start Date
dates_debut_fin_vente.csv.ovr|2|ctmcpomnistartdt.csv.ovr|3

#Date fin vente omnicanal - CP Omni Channel End Date
dates_debut_fin_vente.csv.ovr|2|ctmcpomnienddt.csv.ovr|4

################CR275_Dates_Debut/Fin_Ventes_Omnicanal#####################

