#CP EDIT 9

# Common measures control file
# Control file must be placed in $BATCH_HOME/config/import/data
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |
# Structure of file - multiple export sets can be defined in the same controle file
# 1) Filename defined in CV.027 without extension, the same file name defined in DataInterface in ConfigTools.
# 2) Definition if the file may contain boolean values to be transformed from Y/N to T/F that are acceptable in RPAS, possible values for this parameter are Y|N
# 3) Definition of whether the measure is required or not, if it is required, the script will return error if the measure file does not exist, possible valuer are OPTIONAL|REQUIRED
################Lettre de gamme_BAZA_CatMan#############################
#TY Lettre de gamme - ComTyBuRngLetterTx
#TY Classement - ComTyBuArtcRankI
#TY Future lettre de gamme - ComTyBuFutRngLetterTx
#TY Date future lettre de gamme - ComTyBuFutRngLetterDt
lettreGamme|N|OPTIONAL

################RMS item creation flags_RMS_CatMan#############################
#TY Sellable Indicator - ComTyArtcSellB
#TY Orderable Indicator - ComTyArtcOrdB
#TY Forecastable Indicator - ComTyArtcFcstB
#TY Inventory Indicator - ComTyArtcInvB
#TY Pack Indicator - ComTyPackIndB
#TY Simple Pack Indicator - ComTySimpPackIndB
#TY Merchandise Indicator - CstImCPrMrcIndCbl
itemCreFlag|Y|OPTIONAL

################Conditionnement_RMS_CatMan#############################
#IM Conditionnement PCB - CtmImPCBU
artPCB|N|OPTIONAL

################Attributs magasins_RMS_CatMan#############################
#TY Store Opening Date - ComTyStrOpenDt
#TY Store Closing Date - ComTyStrCloseDt
#TY Store Class - ComTyStrClassTx
#TY Store Format - ComTyStrFormatTx
attributsMag|N|OPTIONAL

################Article/Fournisseur/BU Status#####################
#TY Article Bu Supplier Link - ComTyArtcBuSuplb
art_Four_BU|N|OPTIONAL

################UDAs et CFA's################
#article's UDAs (comtybrandtx, comtytyposteptx, comtysubtyposteptx, comtyartcseriestx, comtytop1000b)
art_UDA|N|OPTIONAL

#article x BU UDAs (comtyartctobub, comtybulfcyclpt, comtybuprcpolpt, comtybulclprcchgb)
artBU_UDA|Y|OPTIONAL

#article's CFAs (comtyartcuomv, comtyartcuomtx)
art_CFA|N|OPTIONAL

################Designation Secondaire################
#Designation secondaire measures (comtysecdesigfrtx, comtysecdesiggrtx, comtysecdesigittx, comtysecdesigpltx, comtysecdesigpttx, comtysecdesigestx)
desgn_sec|N|OPTIONAL

################Related Item################
#Related item, relation type and relation factor (comtyrelitmtx, comtyrelitmlnktx and comtyrelitmconvv)
itemlie|N|OPTIONAL

################Article Photo################
#Article photo comtyphototx
rmse_catman_article_photo|N|OPTIONAL

################Article - Unité Mesure Standard################
#Unité de mesure standard comtyartcuomstx
mes_std|N|OPTIONAL

################Article - Fournisseur################
art_Four|N|OPTIONAL

################Article - Packs################
rpas_pack_item|N|OPTIONAL

###############Translations###################
r_artclabel|N|OPTIONAL
r_prntlabel|N|OPTIONAL
r_gprnlabel|N|OPTIONAL
r_styplabel|N|OPTIONAL
r_typelabel|N|OPTIONAL
r_sdeplabel|N|OPTIONAL
r_dep1label|N|OPTIONAL
r_dep2label|N|OPTIONAL
r_complabel|N|OPTIONAL
r_artrlabel|N|OPTIONAL
r_prnrlabel|N|OPTIONAL
r_gprrlabel|N|OPTIONAL
r_styrlabel|N|OPTIONAL
r_typrlabel|N|OPTIONAL
r_sderlabel|N|OPTIONAL
r_der1label|N|OPTIONAL
r_der2label|N|OPTIONAL
r_comrlabel|N|OPTIONAL
r_str1label|N|OPTIONAL
r_st1rlabel|N|OPTIONAL
r_str2label|N|OPTIONAL
r_st2rlabel|N|OPTIONAL
r_regnlabel|N|OPTIONAL
r_regrlabel|N|OPTIONAL
r_bullabel|N|OPTIONAL
r_burlabel|N|OPTIONAL
r_zonelabel|N|OPTIONAL
r_zonrlabel|N|OPTIONAL
r_grpolabel|N|OPTIONAL
r_grprlabel|N|OPTIONAL

r_measpicklist|N|OPTIONAL