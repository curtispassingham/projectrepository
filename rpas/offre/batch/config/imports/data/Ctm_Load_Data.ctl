# Catman measures control file
# Control file must be placed in $BATCH_HOME/config/import/data
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |
# Structure of file - multiple export sets can be defined in the same controle file
# 1) Filename defined in CV.027 without extension, the same file name defined in DataInterface in ConfigTools.
# 2) Definition if the file may contain boolean values to be transformed from Y/N to T/F that are acceptable in RPAS, possible values for this parameter are Y|N
# 3) Definition of whether the measure is required or not, if it is required, the script will return error if the measure file does not exist, possible valuer are OPTIONAL|REQUIRED
################Assortment Mode Translation################
#Translation file for the assortment mode hierarchy
r_modelabel|N|OPTIONAL
################Assortment Value Translation################
#Translation file for the assortment value
r_asvllabel|N|OPTIONAL
