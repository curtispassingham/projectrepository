# Catman conversion measures control file
# Control file must be placed in $BATCH_HOME/config/import/data
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Filename defined in CV.027 without extension, the same file name defined in DataInterface in ConfigTools.
# 2) Definition if the file may contain boolean values to be transformed from Y/N to T/F that are acceptable in RPAS, possible values for this parameter are Y|N
# 3) Definition of whether the measure is required or not, if it is required, the script will return error if the measure file does not exist, possible valuer are OPTIONAL|REQUIRED

################CR271_Lien Cluster-Magasin#####################

#WP Store to Cluster Link (User) - CtmWpStrToClstTx
ctmwpstrtoclsttx|N|OPTIONAL

#CP Store to Cluster Link (User) - CtmCpStrToClstTx
ctmcpstrtoclsttx|N|OPTIONAL

################CR271_Lien Cluster-Magasin#####################


################CR270_Assortiment preco at Cluster#####################

#WP Assortment@Cluster (User) - CtmWpUsrClsAssrtValTx
ctmwpusrclsassrtvaltx|N|OPTIONAL

#CP Assortment@Cluster (User) - CtmCpUsrClsAssrtValTx
ctmcpusrclsassrtvaltx|N|OPTIONAL

################CR270_Assortiment preco at Cluster#####################


################CR276_Assortiment régulier préconisé par maga#####################

#WP Assortment@Store (User) - CtmWpUsrStrAssrtValTx
ctmwpusrstrassrtvaltx|N|OPTIONAL

#CP Assortment@Store (User) - CtmCpUsrStrAssrtValTx
ctmcpusrstrassrtvaltx|N|OPTIONAL

################CR276_Assortiment régulier préconisé par magasin#####################

################CR275_Dates Debut/Fin Ventes Omnicanal#####################

#CP Omni Channel Start Date - CtmCpOmniStartDt
ctmcpomnistartdt|N|OPTIONAL

#CP Omni channel End Date - CtmCpOmniEndDt
ctmcpomnienddt|N|OPTIONAL

################CR275_Dates Debut/Fin Ventes Omnicanal#####################


################CR273_Measure_Lettre de gamme_BAZA_CatMan#############################

#TY Lettre de gamme - ComTyBuRngLetterTx
#TY Classement - ComTyBuArtcRankI
#TY Future lettre de gamme - ComTyBuFutRngLetterTx
#TY Date future lettre de gamme - ComTyBuFutRngLetterDt

lettreGamme|N|OPTIONAL


################CR278_Measure_RMS item creation flags_RMS_CatMan#############################

#TY Sellable Indicator - ComTyArtcSellB
#TY Orderable Indicator - ComTyArtcOrdB
#TY Forecastable Indicator - ComTyArtcFcstB
#TY Inventory Indicator - ComTyArtcInvB
#TY Pack Indicator - ComTyPackIndB
#TY Simple Pack Indicator - ComTySimpPackIndB
#TY Merchandise Indicator - CstImCPrMrcIndCbl

itemCreFlag|Y|OPTIONAL


################CR280_Measure_Conditionnement_RMS_CatMan#############################

#IM Conditionnement PCB - CtmImPCBU

artPCB|N|OPTIONAL


################CR281_282_Measure_Attributs magasins_RMS_CatMan#############################

#TY Store Opening Date - ComTyStrOpenDt
#TY Store Closing Date - ComTyStrCloseDt
#TY Store Class - ComTyStrClassTx
#TY Store Format - ComTyStrFormatTx

attributsMag|N|OPTIONAL

################CR274_Assortiment_Mode#####################

#CP Assortment Mode (User) - CtmCpAssrtModeTx
ctmcpassrtmodetx|N|OPTIONAL

################CR274_Assortiment_Mode#####################



################CR283_Article/Fournisseur/BU Status#####################

#TY Article Bu Supplier Link - ComTyArtcBuSuplb
comtyartcbusuplb|N|OPTIONAL

################CR283_Article/Fournisseur/BU Status#####################

################CR277_UDAs et CFA's################

#article's UDAs (comtybrandtx, comtytyposteptx, comtysubtyposteptx, comtyartcseriestx, comtytop1000b)
art_UDA|N|OPTIONAL

#article x BU UDAs (comtyartctobub, comtybulfcyclpt, comtybuprcpolpt, comtybulclprcchgb)
artBU_UDA|N|OPTIONAL

#article's CFAs (comtyartcuomv, comtyartcuomtx)
art_CFA|N|OPTIONAL

################CR277_UDAs et CFA's################

################CR347_Designation Secondaire################

#Designation secondaire measures (comtysecdesigfrtx, comtysecdesiggrtx, comtysecdesigittx, comtysecdesigpltx, comtysecdesigpttx, comtysecdesigestx)
desgn_sec|N|OPTIONAL

################CR347_Designation Secondaire################


################CR_362: Article Lié################

#Article Lié
itemlie|N|OPTIONAL

################CR_362: Article Lié################

################CR_359:  Article - Unité Mesure Standard################

#Unité de mesure standard
mes_std|N|OPTIONAL
comtyartcuomstx|N|OPTIONAL

################CR_359:  Article - Unité Mesure Standard################

################CR_350: Article Photo################

#Article photo comtyphototx
rmse_rpas_article_photo|N|OPTIONAL

################CR_350: Article Photo################

################CR_364: Article Remplacé################

#Article Remplacé
#ctmcpreplaceditmcodtx|N|OPTIONAL
ctmcprplcitmcodtx|N|OPTIONAL

################CR_364: Article Remplacé################

################CR_365: Top Saisonnier################

#Top Saisonnier
ctmadseasonalb|N|OPTIONAL

################CR_365: Top Saisonnier################

################Assortiment Choix Magasin##############
#StpCpUsrStrAssrtChoTx CP Assortment Store Choice (User)
#Filename is XXADEO_ASSORTMENT.csv.ovr measure is stpcpusrstrassrtchotx

XXADEO_ASSORTMENT|N|OPTIONAL
################Assortiment Choix Magasin##############

################CR_279_PV_Conseil##############
#statynsprsfv
#Filename PVC.csv.ovr

statynsprsfv|N|OPTIONAL
