#
#[source file name] | [measure name] | [param] | [ignore expression - use ";" when need to use expression should contain "|"] | [timestamp for the dest file]
#ignore expression: use ";" when need to use expression should contain "|". For example exclude values C and CC the expression will be '!/^c,;^cc,/'
#
r_asvllabel.csv.ovr|CtmWpUsrClsAssrtValTx|OPTIONAL|!/^c,/|+%Y%m%d_%H%M%S.%N
r_asvllabel.csv.ovr|CtmWpUsrStrAssrtValTx|OPTIONAL|!/^c,/|+%Y%m%d_%H%M%S.%N