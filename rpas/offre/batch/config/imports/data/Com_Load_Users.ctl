# Import users control file
# Control file must be placed in $BATCH_HOME/config/import/data
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |
# Structure of file - multiple export sets can be defined in the same controle file
# 1) Filename as agrred with OIM without any extension
# 2) Trigger file name to be checked if files can be processed. OIM will append existing file at any time but will create trigger file once done
#    Import script must check existing of trigger file and move files immediately if trigger file is present 
# 3) Trigger file to be created while moving/processing files
users|OIM.COMPLETE|OIM.PROCESSING|OPTIONAL
