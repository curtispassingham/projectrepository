### cst calc control file
### param_type [group|rule|expr]| domain_type [G|L]|param [group name | rule name | expression]
group|L|Sta_Bc1_Flt_1L
group|G|Sta_Bc1_Flt_2G
group|L|Sta_Bc1_Flt_3L
group|L|Sta_Bc1_Tsr_1L
group|L|Sta_Bc1_Tsr_2L
group|L|Sta_Bc1_Tsr_3L
group|L|Sta_Bc1_Src_1L
group|L|Sta_Bc1_Src_2L
group|L|Sta_Bc2_Spp_1L
group|L|Sta_Bc2_Ssp_1L
group|L|Sta_Bc2_Nsp_1L
group|L|Sta_Bc2_Nsp_2L
group|L|Sta_Bc3_PsT_2L
group|L|Sta_Bc3_PsW_1L
group|L|Sta_Bc3_PsW_2L
group|L|Sta_Bc3_TpT_1L
group|L|Sta_Bc3_TpT_2L
group|L|Sta_Bc3_TpT_3L
group|L|Sta_Bc3_TpT_4L
group|L|Sta_Bc3_TpW_1L
group|L|Sta_Bc3_TpW_2L
group|L|Sta_Bc3_TpW_3L
group|L|Sta_Bc3_TpW_4L
