#Export measure control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) M - Measure Name (at least 1 entry needed)
# 3) F - Filter Mask Measure (if specified, export will happen at mask measure base intersection)
# 4) X - Base Intersection (F or X is needed)
# 5) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 6) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 7) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 8) S - SkipNA (Optional) - Control whether a line of data is exported based on having NA's in a cell. Valid values are [never|allna|anyna]. Default is allna.

#StorePortal Assortment at Cluster
AssCls|O|XXADEO_ASSORTMENT_CLUSTER 
AssCls|T|+_%Y%m%d_%H%M%S.dat
AssCls|F|CtmExSpoIfAsstClsB 
AssCls|M|CtmExSpoAstValClsDt
AssCls|M|CtmExSpoAstValClsTx
AssCls|M|CtmExSpoAppAstClsId
AssCls|M|CtmExSpoAppAstClsDl
AssCls|M|CtmExSpoIfAsstClsTx


# Store Portal Cluster Links
ClstLinks|O|XXADEO_STORE_CLUSTER
ClstLinks|T|+_%Y%m%d_%H%M%S.dat
ClstLinks|F|CtmExSpoIfClstLinkB
ClstLinks|M|CtmCpStrToClstB
ClstLinks|M|CtmCpApprMktgId
ClstLinks|M|CtmCpApprMktgDl
ClstLinks|M|CtmExSpoIfClstLinkTx
ClstLinks|C|s

# Store Portal Item/Bu Attributes
ITEM_BU|O|XXADEO_ITEM_BU
ITEM_BU|T|+_%Y%m%d_%H%M%S.dat
ITEM_BU|F|CtmExSpoIfAssPreB
ITEM_BU|M|ComTyBuRngLetterTx
ITEM_BU|M|ComTyBuArtcRankI
ITEM_BU|M|CtmExSpoOmniStartTx
ITEM_BU|M|CtmExSpoOmniEndTx
ITEM_BU|M|CtmExSpoRplcItmCodTx
ITEM_BU|M|CtmAdSeasonalB
ITEM_BU|M|CtmExSpoAstPrepUsrId
ITEM_BU|M|CtmExSpoAstPrepUsrDtx
ITEM_BU|M|CtmExSpoIfAssPreTx

# Store Portal Assortment at Store
AssStr|O|XXADEO_ASSORTMENT_STORE
AssStr|T|+_%Y%m%d_%H%M%S.dat
AssStr|F|CtmExSpoIfAsstStrB
AssStr|M|CtmExSpoAstValDt
AssStr|M|CtmExSpoReplLmDt
AssStr|M|CtmExSpoAstModTx
AssStr|M|CtmExSpoAstValTx
AssStr|M|CtmExSpoAstVlClsTx
AssStr|M|CtmExSpoAstVlClsId
AssStr|M|CtmExSpoNoModClsB
AssStr|M|CtmExSpoAstStsI
AssStr|M|CtmExSpoAstCmtTx
AssStr|M|CtmExSpoAppAssrtId
AssStr|M|CtmExSpoAppAssrtDl
AssStr|M|CtmExSpoIfAsstStrTx

TradMode|O|XXADEO_ASSORTMENT_MODE_TL
TradMode|X|langmode
TradMode|M|r_modelabel
TradMode|T|+_%Y%m%d_%H%M%S.dat

TradAsvl|O|XXADEO_ASSORTMENT_VALUE_TL
TradAsvl|X|langasvl
TradAsvl|M|r_asvllabel
TradAsvl|T|+_%Y%m%d_%H%M%S.dat
