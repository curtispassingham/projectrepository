#Export measure control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is | 

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) M - Measure Name (at least 1 entry needed)
# 3) F - Filter Mask Measure (if specified, export will happen at mask measure base intersection)
# 4) X - Base Intersection (F or X is needed)
# 5) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 6) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 7) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 8) S - SkipNA (Optional) - Control whether a line of data is exported based on having NA's in a cell. Valid values are [never|allna|anyna]. Default is allna.


ecotax01|O|ECO01.csv.ovr
ecotax01|M|CstExEEcR01Ccm
ecotax01|M|CstExEEcR01Cvr
ecotax01|M|CstExEEcR01Ccu
ecotax01|M|CstExEEcR01Cuo
ecotax01|M|CstExEEcR01Cpe
ecotax01|M|CstExEEcR01Cmm
ecotax01|M|CstExEEcR01Cud
ecotax01|M|CstExEEcR01Cuu
ecotax01|M|CstExEEcR01Cde
ecotax01|F|CstExEEcR01Cbl

ecotax02|O|ECO02.csv.ovr
ecotax02|M|CstExEEcR02Ccm
ecotax02|M|CstExEEcR02Cvr
ecotax02|M|CstExEEcR02Ccu
ecotax02|M|CstExEEcR02Cuo
ecotax02|M|CstExEEcR02Cpe
ecotax02|M|CstExEEcR02Cmm
ecotax02|M|CstExEEcR02Cud
ecotax02|M|CstExEEcR02Cuu
ecotax02|M|CstExEEcR02Cde
ecotax02|F|CstExEEcR02Cbl

ecotax03|O|ECO03.csv.ovr
ecotax03|M|CstExEEcR03Ccm
ecotax03|M|CstExEEcR03Cvr
ecotax03|M|CstExEEcR03Ccu
ecotax03|M|CstExEEcR03Cuo
ecotax03|M|CstExEEcR03Cpe
ecotax03|M|CstExEEcR03Cmm
ecotax03|M|CstExEEcR03Cud
ecotax03|M|CstExEEcR03Cuu
ecotax03|M|CstExEEcR03Cde
ecotax03|F|CstExEEcR03Cbl

ecotax04|O|ECO04.csv.ovr
ecotax04|M|CstExEEcR04Ccm
ecotax04|M|CstExEEcR04Cvr
ecotax04|M|CstExEEcR04Ccu
ecotax04|M|CstExEEcR04Cuo
ecotax04|M|CstExEEcR04Cpe
ecotax04|M|CstExEEcR04Cmm
ecotax04|M|CstExEEcR04Cud
ecotax04|M|CstExEEcR04Cuu
ecotax04|M|CstExEEcR04Cde
ecotax04|F|CstExEEcR04Cbl

ecotax05|O|ECO05.csv.ovr
ecotax05|M|CstExEEcR05Ccm
ecotax05|M|CstExEEcR05Cvr
ecotax05|M|CstExEEcR05Ccu
ecotax05|M|CstExEEcR05Cuo
ecotax05|M|CstExEEcR05Cpe
ecotax05|M|CstExEEcR05Cmm
ecotax05|M|CstExEEcR05Cud
ecotax05|M|CstExEEcR05Cuu
ecotax05|M|CstExEEcR05Cde
ecotax05|F|CstExEEcR05Cbl

ecotax06|O|ECO06.csv.ovr
ecotax06|M|CstExEEcR06Ccm
ecotax06|M|CstExEEcR06Cvr
ecotax06|M|CstExEEcR06Ccu
ecotax06|M|CstExEEcR06Cuo
ecotax06|M|CstExEEcR06Cpe
ecotax06|M|CstExEEcR06Cmm
ecotax06|M|CstExEEcR06Cud
ecotax06|M|CstExEEcR06Cuu
ecotax06|M|CstExEEcR06Cde
ecotax06|F|CstExEEcR06Cbl

ecotax07|O|ECO07.csv.ovr
ecotax07|M|CstExEEcR07Ccm
ecotax07|M|CstExEEcR07Cvr
ecotax07|M|CstExEEcR07Ccu
ecotax07|M|CstExEEcR07Cuo
ecotax07|M|CstExEEcR07Cpe
ecotax07|M|CstExEEcR07Cmm
ecotax07|M|CstExEEcR07Cud
ecotax07|M|CstExEEcR07Cuu
ecotax07|M|CstExEEcR07Cde
ecotax07|F|CstExEEcR07Cbl

ecotax08|O|ECO08.csv.ovr
ecotax08|M|CstExEEcR08Ccm
ecotax08|M|CstExEEcR08Cvr
ecotax08|M|CstExEEcR08Ccu
ecotax08|M|CstExEEcR08Cuo
ecotax08|M|CstExEEcR08Cpe
ecotax08|M|CstExEEcR08Cmm
ecotax08|M|CstExEEcR08Cud
ecotax08|M|CstExEEcR08Cuu
ecotax08|M|CstExEEcR08Cde
ecotax08|F|CstExEEcR08Cbl

ecotax09|O|ECO09.csv.ovr
ecotax09|M|CstExEEcR09Ccm
ecotax09|M|CstExEEcR09Cvr
ecotax09|M|CstExEEcR09Ccu
ecotax09|M|CstExEEcR09Cuo
ecotax09|M|CstExEEcR09Cpe
ecotax09|M|CstExEEcR09Cmm
ecotax09|M|CstExEEcR09Cud
ecotax09|M|CstExEEcR09Cuu
ecotax09|M|CstExEEcR09Cde
ecotax09|F|CstExEEcR09Cbl

ecotax10|O|ECO10.csv.ovr
ecotax10|M|CstExEEcR10Ccm
ecotax10|M|CstExEEcR10Cvr
ecotax10|M|CstExEEcR10Ccu
ecotax10|M|CstExEEcR10Cuo
ecotax10|M|CstExEEcR10Cpe
ecotax10|M|CstExEEcR10Cmm
ecotax10|M|CstExEEcR10Cud
ecotax10|M|CstExEEcR10Cuu
ecotax10|M|CstExEEcR10Cde
ecotax10|F|CstExEEcR10Cbl

ecotax11|O|ECO11.csv.ovr
ecotax11|M|CstExEEcR11Ccm
ecotax11|M|CstExEEcR11Cvr
ecotax11|M|CstExEEcR11Ccu
ecotax11|M|CstExEEcR11Cuo
ecotax11|M|CstExEEcR11Cpe
ecotax11|M|CstExEEcR11Cmm
ecotax11|M|CstExEEcR11Cud
ecotax11|M|CstExEEcR11Cuu
ecotax11|M|CstExEEcR11Cde
ecotax11|F|CstExEEcR11Cbl

ecotax12|O|ECO12.csv.ovr
ecotax12|M|CstExEEcR12Ccm
ecotax12|M|CstExEEcR12Cvr
ecotax12|M|CstExEEcR12Ccu
ecotax12|M|CstExEEcR12Cuo
ecotax12|M|CstExEEcR12Cpe
ecotax12|M|CstExEEcR12Cmm
ecotax12|M|CstExEEcR12Cud
ecotax12|M|CstExEEcR12Cuu
ecotax12|M|CstExEEcR12Cde
ecotax12|F|CstExEEcR12Cbl

ecotax13|O|ECO13.csv.ovr
ecotax13|M|CstExEEcR13Ccm
ecotax13|M|CstExEEcR13Cvr
ecotax13|M|CstExEEcR13Ccu
ecotax13|M|CstExEEcR13Cuo
ecotax13|M|CstExEEcR13Cpe
ecotax13|M|CstExEEcR13Cmm
ecotax13|M|CstExEEcR13Cud
ecotax13|M|CstExEEcR13Cuu
ecotax13|M|CstExEEcR13Cde
ecotax13|F|CstExEEcR13Cbl

ecotax14|O|ECO14.csv.ovr
ecotax14|M|CstExEEcR14Ccm
ecotax14|M|CstExEEcR14Cvr
ecotax14|M|CstExEEcR14Ccu
ecotax14|M|CstExEEcR14Cuo
ecotax14|M|CstExEEcR14Cpe
ecotax14|M|CstExEEcR14Cmm
ecotax14|M|CstExEEcR14Cud
ecotax14|M|CstExEEcR14Cuu
ecotax14|M|CstExEEcR14Cde
ecotax14|F|CstExEEcR14Cbl

ecotax15|O|ECO15.csv.ovr
ecotax15|M|CstExEEcR15Ccm
ecotax15|M|CstExEEcR15Cvr
ecotax15|M|CstExEEcR15Ccu
ecotax15|M|CstExEEcR15Cuo
ecotax15|M|CstExEEcR15Cpe
ecotax15|M|CstExEEcR15Cmm
ecotax15|M|CstExEEcR15Cud
ecotax15|M|CstExEEcR15Cuu
ecotax15|M|CstExEEcR15Cde
ecotax15|F|CstExEEcR15Cbl


# Rest of Zone-Level expenses

INLDFRE|O|INLDFRE.csv.ovr
INLDFRE|M|CstExEInlFrgtCcm
INLDFRE|M|CstExEInlFrgtCvr
INLDFRE|M|CstExEInlFrgtCcu
INLDFRE|M|CstExEInlFrgtCuo
INLDFRE|M|CstExEInlFrgtCpe
INLDFRE|M|CstExEInlFrgtCf4
INLDFRE|M|CstExEInlFrgtCud
INLDFRE|M|CstExEInlFrgtCuu
INLDFRE|M|CstExEInlFrgtCde
INLDFRE|F|CstExEInlFrgtCbl

AICUSCR|O|AICUSCR.csv.ovr
AICUSCR|M|CstExEGsBuCrdCcm
AICUSCR|M|CstExEGsBuCrdCvp
AICUSCR|M|CstExCCnsNaEmtCtx
AICUSCR|M|CstExCCnsNaEmtCtx
AICUSCR|M|CstExCCnsNaEmtCtx
AICUSCR|M|CstExEGsBuCrdCf4
AICUSCR|M|CstExEGsBuCrdCud
AICUSCR|M|CstExEGsBuCrdCuu
AICUSCR|M|CstExEGsBuCrdCde
AICUSCR|F|CstExEGsBuCrdCbl

ADEOMU|O|ADEOMU.csv.ovr
ADEOMU|M|CstExEAdeoMuCcm
ADEOMU|M|CstExEAdeoMuCvr
ADEOMU|M|CstExEAdeoMuCcu
ADEOMU|M|CstExEAdeoMuCuo
ADEOMU|M|CstExEAdeoMuCpe
ADEOMU|M|CstExEAdeoMuCf4
ADEOMU|M|CstExEAdeoMuCud
ADEOMU|M|CstExEAdeoMuCuu
ADEOMU|M|CstExEAdeoMuCde
ADEOMU|F|CstExEAdeoMuCbl

AGENCOM|O|AGENCOM.csv.ovr
AGENCOM|M|CstExEAgtCmsCcm
AGENCOM|M|CstExEAgtCmsCvp
AGENCOM|M|CstExCCnsNaEmtCtx
AGENCOM|M|CstExCCnsNaEmtCtx
AGENCOM|M|CstExCCnsNaEmtCtx
AGENCOM|M|CstExEAgtCmsCf4
AGENCOM|M|CstExEAgtCmsCud
AGENCOM|M|CstExEAgtCmsCuu
AGENCOM|M|CstExEAgtCmsCde
AGENCOM|F|CstExEAgtCmsCbl

BANKFEE|O|BANKFEE.csv.ovr
BANKFEE|M|CstExEBnkFeesCcm
BANKFEE|M|CstExEBnkFeesCvp
BANKFEE|M|CstExCCnsNaEmtCtx
BANKFEE|M|CstExCCnsNaEmtCtx
BANKFEE|M|CstExCCnsNaEmtCtx
BANKFEE|M|CstExEBnkFeesCf4
BANKFEE|M|CstExEBnkFeesCud
BANKFEE|M|CstExEBnkFeesCuu
BANKFEE|M|CstExEBnkFeesCde
BANKFEE|F|CstExEBnkFeesCbl

CUSTOMS|O|CUSTOMS.csv.ovr
CUSTOMS|M|CstExECustDutCcm
CUSTOMS|M|CstExECustDutCvp
CUSTOMS|M|CstExCCnsNaEmtCtx
CUSTOMS|M|CstExCCnsNaEmtCtx
CUSTOMS|M|CstExCCnsNaEmtCtx
CUSTOMS|M|CstExECustDutCf4
CUSTOMS|M|CstExECustDutCud
CUSTOMS|M|CstExECustDutCuu
CUSTOMS|M|CstExECustDutCde
CUSTOMS|F|CstExECustDutCbl

ESTIMAT|O|ESTIMAT.csv.ovr
ESTIMAT|M|CstExEEstSrcCCcm
ESTIMAT|M|CstExEEstSrcCCvr
ESTIMAT|M|CstExEEstSrcCCcu
ESTIMAT|M|CstExEEstSrcCCuo
ESTIMAT|M|CstExEEstSrcCCpe
ESTIMAT|M|CstExEEstSrcCCf4
ESTIMAT|M|CstExEEstSrcCCud
ESTIMAT|M|CstExEEstSrcCCuu
ESTIMAT|M|CstExEEstSrcCCde
ESTIMAT|F|CstExEEstSrcCCbl

INTLASS|O|INTLASS.csv.ovr
INTLASS|M|CstExEIntInsCcm
INTLASS|M|CstExEIntInsCvp
INTLASS|M|CstExCCnsNaEmtCtx
INTLASS|M|CstExCCnsNaEmtCtx
INTLASS|M|CstExCCnsNaEmtCtx
INTLASS|M|CstExEIntInsCf4
INTLASS|M|CstExEIntInsCud
INTLASS|M|CstExEIntInsCuu
INTLASS|M|CstExEIntInsCde
INTLASS|F|CstExEIntInsCbl

MARGCPM|O|MARGCPM.csv.ovr
MARGCPM|M|CstExEMgCplCcm
MARGCPM|M|CstExEMgCplCvr
MARGCPM|M|CstExEMgCplCcu
MARGCPM|M|CstExEMgCplCuo
MARGCPM|M|CstExEMgCplCpe
MARGCPM|M|CstExEMgCplCf4
MARGCPM|M|CstExEMgCplCud
MARGCPM|M|CstExEMgCplCuu
MARGCPM|M|CstExEMgCplCde
MARGCPM|F|CstExEMgCplCbl

MARKGMU|O|MARKGMU.csv.ovr
MARKGMU|M|CstExEMktgMuCcm
MARKGMU|M|CstExEMktgMuCvr
MARKGMU|M|CstExEMktgMuCcu
MARKGMU|M|CstExEMktgMuCuo
MARKGMU|M|CstExEMktgMuCpe
MARKGMU|M|CstExEMktgMuCf4
MARKGMU|M|CstExEMktgMuCud
MARKGMU|M|CstExEMktgMuCuu
MARKGMU|M|CstExEMktgMuCde
MARKGMU|F|CstExEMktgMuCbl

PURCHMU|O|PURCHMU.csv.ovr
PURCHMU|M|CstExEPrchMuCcm
PURCHMU|M|CstExEPrchMuCvr
PURCHMU|M|CstExEPrchMuCcu
PURCHMU|M|CstExEPrchMuCuo
PURCHMU|M|CstExEPrchMuCpe
PURCHMU|M|CstExEPrchMuCf4
PURCHMU|M|CstExEPrchMuCud
PURCHMU|M|CstExEPrchMuCuu
PURCHMU|M|CstExEPrchMuCde
PURCHMU|F|CstExEPrchMuCbl

QUALITY|O|QUALITY.csv.ovr
QUALITY|M|CstExEQltyCCcm
QUALITY|M|CstExEQltyCCvr
QUALITY|M|CstExEQltyCCcu
QUALITY|M|CstExEQltyCCuo
QUALITY|M|CstExEQltyCCpe
QUALITY|M|CstExEQltyCCf4
QUALITY|M|CstExEQltyCCud
QUALITY|M|CstExEQltyCCuu
QUALITY|M|CstExEQltyCCde
QUALITY|F|CstExEQltyCCbl

SUPCRED|O|SUPCRED.csv.ovr
SUPCRED|M|CstExESupCrdCcm
SUPCRED|M|CstExESupCrdCvp
SUPCRED|M|CstExCCnsNaEmtCtx
SUPCRED|M|CstExCCnsNaEmtCtx
SUPCRED|M|CstExCCnsNaEmtCtx
SUPCRED|M|CstExESupCrdCf4
SUPCRED|M|CstExESupCrdCud
SUPCRED|M|CstExESupCrdCuu
SUPCRED|M|CstExESupCrdCde
SUPCRED|F|CstExESupCrdCbl

SUPPLYMU|O|SUPPLYMU.csv.ovr
SUPPLYMU|M|CstExESchMuCcm
SUPPLYMU|M|CstExESchMuCvr
SUPPLYMU|M|CstExESchMuCcu
SUPPLYMU|M|CstExESchMuCuo
SUPPLYMU|M|CstExESchMuCpe
SUPPLYMU|M|CstExESchMuCf4
SUPPLYMU|M|CstExESchMuCud
SUPPLYMU|M|CstExESchMuCuu
SUPPLYMU|M|CstExESchMuCde
SUPPLYMU|F|CstExESchMuCbl


# Country-Level expenses

MAINFRE|O|MAINFRE.csv.ovr
MAINFRE|M|CstExEMnFrgtCcm
MAINFRE|M|CstExEMnFrgtCvr
MAINFRE|M|CstExEMnFrgtCcu
MAINFRE|M|CstExEMnFrgtCuo
MAINFRE|M|CstExEMnFrgtCpe
MAINFRE|M|CstExEMnFrgtCf4
MAINFRE|M|CstExEMnFrgtCud
MAINFRE|M|CstExEMnFrgtCuu
MAINFRE|M|CstExEMnFrgtCde
MAINFRE|F|CstExEMnFrgtCbl
