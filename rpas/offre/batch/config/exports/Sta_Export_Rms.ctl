#Export measure control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) M - Measure Name (at least 1 entry needed)
# 3) F - Filter Mask Measure (if specified, export will happen at mask measure base intersection)
# 4) X - Base Intersection (F or X is needed)
# 5) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 6) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 7) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 8) S - SkipNA (Optional) - Control whether a line of data is exported based on having NA's in a cell. Valid values are [never|allna|anyna]. Default is allna.

PCS_LOC|O|XXRMS1216_PCS_LOC
PCS_LOC|F|StaExSstprStrSbl
PCS_LOC|M|StaExSstprStrSvt
PCS_LOC|M|CstAdLkCuBuCcu
PCS_LOC|M|StaExGMcSci
PCS_LOC|M|StaExSstprStrSad
PCS_LOC|M|StaExUsrExStx
PCS_LOC|M|StaExSstprStrSde
PCS_LOC|M|StaExSysExStx
PCS_LOC|T|+_%Y%m%d_%H%M%S.dat
PCS_LOC|S|allna
 
PCS_BU|O|XXRMS1077_PCS_BU
PCS_BU|F|StaExSstprSbl
PCS_BU|M|StaExSstprSfv
PCS_BU|M|CstAdLkCuBuCcu
PCS_BU|M|StaExGMcSci
PCS_BU|M|StaExSstprSad
PCS_BU|M|StaExUsrExStx
PCS_BU|M|StaExSstprSde
PCS_BU|M|StaExSysExStx
PCS_BU|T|+_%Y%m%d_%H%M%S.dat
PCS_BU|S|allna
