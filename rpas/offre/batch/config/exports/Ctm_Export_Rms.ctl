#Export measure control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) M - Measure Name (at least 1 entry needed)
# 3) F - Filter Mask Measure (if specified, export will happen at mask measure base intersection)
# 4) X - Base Intersection (F or X is needed)
# 5) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 6) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 7) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 8) S - SkipNA (Optional) - Control whether a line of data is exported based on having NA's in a cell. Valid values are [never|allna|anyna]. Default is allna.
# 9) C - useDate (Optional) - Control whether an acual clnd position name is exported or the start or end date of that position. Valid values are [s|e]. Default is position id

Dates_debut_fin_vente_Mappin|O|XXRMS208_ASSORTMENT_ITEMBU_ONOF
Dates_debut_fin_vente_Mappin|F|CtmExRmsIf1128B
Dates_debut_fin_vente_Mappin|M|CtmExRmsOmniStartTx
Dates_debut_fin_vente_Mappin|M|CtmExRmsOmniEndTx
Dates_debut_fin_vente_Mappin|T|+_%Y%m%d_%H%M%S.dat
Dates_debut_fin_vente_Mappin|S|allna


Classement|O|XXRMS208_ASSORTMENT_ITEMBU_RS
Classement|M|ComTyBuArtcRankI
Classement|M|ComT1NavalTx
Classement|M|ComT1NavalTx
Classement|T|+_%Y%m%d_%H%M%S.dat
Classement|X|bul_gprn
Classement|S|allna


RmsAssortPreco|O|XXRMS208_ASSORTMENT_REG_ITEMLOC
RmsAssortPreco|F|CtmExRmsIf340B
RmsAssortPreco|M|CtmExRmsFnlAsstVlTx
RmsAssortPreco|M|CtmExRmsIf340Tx
RmsAssortPreco|T|+_%Y%m%d_%H%M%S.dat
RmsAssortPreco|C|s
RmsAssortPreco|S|allna


ModeAssort|O|XXRMS208_ASSORTMENT_ITEMBU_AM
ModeAssort|F|CtmExRmsIf486B
ModeAssort|M|CtmExRmsAmodTx
ModeAssort|M|CtmExRmsIf486Tx
ModeAssort|T|+_%Y%m%d_%H%M%S.dat
ModeAssort|C|s
ModeAssort|S|allna