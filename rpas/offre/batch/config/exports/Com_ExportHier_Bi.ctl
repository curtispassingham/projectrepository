#Export hierarchies control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) H - Hierarchy Name 
# 3) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 4) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 5) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 6) F - Flags (Optional) - Any additional flag to export the hierarchy. Use exact exportHier syntax, e.g. -onlyInformal -upperCase 

#Hiérarchie valeur d'assortiment
ASV|H|asv
ASV|O|f_bu_CATMAN_ASSTVALH_000
ASV|T|+.%H%M%S.%Y%m%d

#Hiérarchie mode d'assortiment
AMOD|H|amod
AMOD|O|f_bu_CATMAN_ASSTMODH_000
AMOD|T|+.%H%M%S.%Y%m%d

#Type de prestation
WHSH|H|whsh
WHSH|O|f_bu_COSTING_SVCTYPEH_000
WHSH|T|+.%H%M%S.%Y%m%d

#Type de conditionnement
PCTH|H|pcth
PCTH|O|f_bu_COSTING_PACKTYPEH_000
PCTH|T|+.%H%M%S.%Y%m%d

#Hiérarchie Tranche de Poids
WGRH|H|wgrh
WGRH|O|f_bu_COSTING_WGHTRANGEH_000
WGRH|T|+.%H%M%S.%Y%m%d

#Hiérarchie Tranche de Volume
VLRH|H|vlrh
VLRH|O|f_bu_COSTING_VLMRANGEH_000
VLRH|T|+.%H%M%S.%Y%m%d

#Hiérarchie circuit
CIRH|H|cirh
CIRH|O|f_bu_COSTING_CIRCTH_000
CIRH|T|+.%H%M%S.%Y%m%d

#Hiérarchie segment de circuit
CISH|H|cish
CISH|O|f_bu_COSTING_CIRCTSEGH_000
CISH|T|+.%H%M%S.%Y%m%d
