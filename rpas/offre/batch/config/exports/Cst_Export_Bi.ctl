#Export measure control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) M - Measure Name (at least 1 entry needed)
# 3) F - Filter Mask Measure (if specified, export will happen at mask measure base intersection)
# 4) X - Base Intersection (F or X is needed)
# 5) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 6) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 7) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 8) S - SkipNA (Optional) - Control whether a line of data is exported based on having NA's in a cell. Valid values are [never|allna|anyna]. Default is allna.
# 9) C - useDate (Optional) - Control whether an acual clnd position name is exported or the start or end date of that position. Valid values are [s|e]. Default is position id

#Lien type de prestation / segment de circuit
LNK_WRHSVCTYP_CICSEG|O|f_bu_COSTING_LNKWRHSVCTYPCICSEG_000
LNK_WRHSVCTYP_CICSEG|M|cstadlkwhtpcbl
LNK_WRHSVCTYP_CICSEG|T|+.%H%M%S.%Y%m%d
LNK_WRHSVCTYP_CICSEG|S|allna

#Lien circuit / segment de circuit
LNK_CICSGMT_CIC|O|f_bu_COSTING_LNKCICSGMTCIC_000
LNK_CICSGMT_CIC|M|cstadlkcrccscbl
LNK_CICSGMT_CIC|T|+.%H%M%S.%Y%m%d
LNK_CICSGMT_CIC|S|allna

#Lien segment de circuit / composant de coût
LNK_CICSGMT_CSTCOMP|O|f_bu_COSTING_LNKCICSGMTCSTCOMP_000
LNK_CICSGMT_CSTCOMP|M|CstAdLcUWhStrgCCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcUWhPcCCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcUWhOcCoCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcUWhExCCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcUWhEnCCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcUWhCBcPkCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcUWhAdCCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcUSchMuCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcUQltyCCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcUPrchMuCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcUPrvRskCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcUPrvDprcCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcUOcDtrBcCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcUMktgMuCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcUFinSrvCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcUFnCStkCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcUDtrCbl    
LNK_CICSGMT_CSTCOMP|M|CstAdLcUCusrCrdCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcUAfsPrvCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcESchMuCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcESupCrdCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcEQltyCCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEPrchMuCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcEMktgMuCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcEMgCplCbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEMnFrgtCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcEIntInsCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcEInlFrgtCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcEGsBuCrdCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEstSrcCCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR01Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR02Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR03Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR04Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR05Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR06Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR07Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR08Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR09Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR10Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR11Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR12Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR13Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR14Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcEEcR15Cbl  
LNK_CICSGMT_CSTCOMP|M|CstAdLcECustDutCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcEBnkFeesCbl
LNK_CICSGMT_CSTCOMP|M|CstAdLcEAgtCmsCbl 
LNK_CICSGMT_CSTCOMP|M|CstAdLcEAdeoMuCbl 
LNK_CICSGMT_CSTCOMP|T|+.%H%M%S.%Y%m%d
LNK_CICSGMT_CSTCOMP|S|allna

#Lien incoterm / composant de coût
LNK_INCO_CSTCOMP|O|f_bu_COSTING_LNKINCOCSTCOMP_000
LNK_INCO_CSTCOMP|M|CstAdLiEMnFrgtCbl 
LNK_INCO_CSTCOMP|M|CstAdLiEIntInsCbl 
LNK_INCO_CSTCOMP|M|CstAdLiEInlFrgtCbl
LNK_INCO_CSTCOMP|M|CstAdLiECustDutCbl
LNK_INCO_CSTCOMP|M|CstAdLiEBnkFeesCbl
LNK_INCO_CSTCOMP|M|CstAdLiEAgtCmsCbl
LNK_INCO_CSTCOMP|T|+.%H%M%S.%Y%m%d
LNK_INCO_CSTCOMP|S|allna

#Lien circuit / zone de coût
LNK_CIC_CSTZONE|O|f_bu_COSTING_LNKCICCSTZONE_000
LNK_CIC_CSTZONE|M|cstadlkcrcczcbl
LNK_CIC_CSTZONE|T|+.%H%M%S.%Y%m%d
LNK_CIC_CSTZONE|S|allna

#EcoTaxe administration
ECO_TAX_ADMIN|O|f_bu_COSTING_ECOTAXADMIN_000
ECO_TAX_ADMIN|M|CstAdSEcBuR01Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR02Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR03Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR04Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR05Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR06Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR07Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR08Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR09Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR10Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR11Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR12Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR13Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR14Clb
ECO_TAX_ADMIN|M|CstAdSEcBuR15Clb
ECO_TAX_ADMIN|T|+.%H%M%S.%Y%m%d
ECO_TAX_ADMIN|S|allna

