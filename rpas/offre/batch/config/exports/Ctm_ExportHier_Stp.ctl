#Export hierarchies control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) H - Hierarchy Name 
# 3) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 4) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 5) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 6) F - Flags (Optional) - Any additional flag to export the hierarchy. Use exact exportHier syntax, e.g. -onlyInformal -upperCase 

CLST|H|clst
CLST|O|XXADEO_ASSRT_CLUSTERS
#CLST|D|StorePortal
CLST|T|+_%Y%m%d_%H%M%S.dat
#CLST|F|-genHeader

