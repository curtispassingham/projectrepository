#Export measure control file
# Control file must be placed in $BATCH_HOME/config/exports
# Comments must start with #
# Blank lines will be ignored and can used to separate different export sets
# Field separator is |

# Structure of file - multiple export sets can be defined in the same controle file
# 1) Export set name
# 2) M - Measure Name (at least 1 entry needed)
# 3) F - Filter Mask Measure (if specified, export will happen at mask measure base intersection)
# 4) X - Base Intersection (F or X is needed)
# 5) D - Output directory (Optional) - relative to $EXTERNAL_OUTBOUND_DIR, e.g. RMS or StorePortal), if not specified export files will be created in $EXTERNAL_OUTBOUND_DIR
# 6) O - Output File Name (Optional) - if not specified the name of the export set will be used
# 7) T - if present output file name will be extended by timestamp in specified format (using date syntax, e.g. +%Y%m%d_%H%M%S will result in 20180425_091453)
# 8) S - SkipNA (Optional) - Control whether a line of data is exported based on having NA's in a cell. Valid values are [never|allna|anyna]. Default is allna.
# 9) C - useDate (Optional) - Control whether an acual clnd position name is exported or the start or end date of that position. Valid values are [s|e]. Default is position id

#Lien cluster
CLUSTER_LINK|O|f_bu_CATMAN_CLUSTLINK_000
CLUSTER_LINK|M|ctmadclstlinktx
CLUSTER_LINK|T|+.%H%M%S.%Y%m%d
CLUSTER_LINK|S|allna

#Niveau d'assortiment
ASST_LEVEL|O|f_bu_CATMAN_ASSTLVL_000
ASST_LEVEL|M|ctmadasstlvltx
ASST_LEVEL|T|+.%H%M%S.%Y%m%d
ASST_LEVEL|S|allna

#Poids des valeurs d'assortiment
ASST_WEIGHT|O|f_bu_CATMAN_ASSTWGHT_000
ASST_WEIGHT|M|ctmadasstwghti
ASST_WEIGHT|T|+.%H%M%S.%Y%m%d
ASST_WEIGHT|S|allna

#Week mapping
WEEK_MAPPING|O|f_bu_CATMAN_WKMAP_000
WEEK_MAPPING|M|ctmadwkmaptx
WEEK_MAPPING|T|+.%H%M%S.%Y%m%d
WEEK_MAPPING|S|allna
