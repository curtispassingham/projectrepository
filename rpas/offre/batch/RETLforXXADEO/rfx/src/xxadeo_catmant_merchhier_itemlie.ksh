#!/bin/ksh

### INTRO #####################################################################
#  ____________________________________________________________________________
#  Workfile:             | xxadeo_catmant_merchhier_itemlie.ksh
#  Created by:           | PR <paulo.reis@oracle.com>
#  Create Datetime:      | 20180615-1200
#  Last Updated by:      | PVI <pedro.vieira@oracle.com>
#  Last Update Datetime: | 20181009-1720
#  Version:              | 1.2
#
#  Interface IDs:
#  --------------
#  RICEW #: 264 / 265
#  IR ID: IR0079
#
#  This auxiliary script (related to the IR ID above) processes item link
#  information, transformed by IR0318.
#
#  Usage: xxadeo_catmant_merchhier_itemlie.ksh
#
#  Expected file:
#    1. itemlie.csv.rpl
#
#  History:
#       1.2 - Added/Changed Includes and Variables according Adeo Env
#           - Changed the script name according the Adeo Standards
#
###############################################################################

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_catmant_merchhier_itemlie"

### INCLUDES ##################################################################

#RETLforXXADEO=<uncomment and set as needed>
[[ -z $RETLforXXADEO ]] && RETLforXXADEO=$RMS_RPAS_HOME
export RETL_ENV=$RETLforXXADEO

## Environment configuration script
#. ${RETL_ENV}/rfx/etc/rmse_rpas_config.env
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env			# (Adeo ENV - RPAS)

## Script Library Collection
#. ${LIB_DIR}/rmse_rpas_lib.ksh
#. ${LIB_DIR}/rmse_rpas_error_check.ksh
. ${LIB_DIR}/xxadeo_rpas_lib.ksh							# (Adeo ENV - RPAS)

### INPUT / OUTPUT DEFINITIONS ################################################

#systemIN=<uncomment and set as needed>
#[[ -z $systemIN ]] && systemIN="CATMAN"

#if [ -z $RETL_IN ] ; then
#	RETL_IN=${RETL_ENV}/data
#	export RETL_IN=$RETL_IN
#fi
#export INBOUND_DIR=$RETL_IN/${systemIN}
#$(mkdir -p ${INBOUND_DIR})
export INBOUND_DIR=${I_DATA_DIR}						# Adeo ENV (Base RPAS Input Dir)
export INBOUND_DIR_CUST=${I_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Input Dir)


#systemOUT=<uncomment and set as needed>
#[[ -z $systemOUT ]] && systemOUT="CATMAN"

#if [ -z $RETL_OUT ] ; then
#	RETL_OUT=${RETL_ENV}/data
#	export RETL_OUT=$RETL_OUT
#fi
#export OUTBOUND_DIR=$RETL_OUT/${systemOUT}
#$(mkdir -p ${OUTBOUND_DIR})
export OUTBOUND_DIR=${O_DATA_DIR}						# Adeo ENV (Base RPAS Output Dir)
export OUTBOUND_DIR_CUST=${O_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Output Dir)


#export STAGING_DIR=${INBOUND_DIR}
export STAGING_DIR=${INBOUND_DIR_CUST}					# Adeo ENV
#export OUTPUT_DIR=${OUTBOUND_DIR}/transformation
export OUTPUT_DIR=${OUTBOUND_DIR_CUST}	# Adeo ENV
#$(mkdir -p ${OUTPUT_DIR})

#export INPUT_FILE=${RETL_IN}/nbitemlie.csv.rpl
export INPUT_FILE=${INBOUND_DIR_CUST}/nbitemlie.csv.rpl	# Adeo ENV
export INPUT_FILE_SORTED=${STAGING_DIR}/nbitemlie.csv.rpl.tmp
export OUTPUT_FILE=${OUTPUT_DIR}/itemlie.csv.rpl
       $(:> ${OUTPUT_FILE})

## create empty tmp files
$(:> ${STAGING_DIR}/itemlie_f1.tmp)
$(:> ${STAGING_DIR}/itemlie_f2.tmp)
$(:> ${STAGING_DIR}/itemlie_f3.tmp)
$(:> ${STAGING_DIR}/itemlie_f4.tmp)
$(:> ${INPUT_FILE_SORTED})

###  PROCESS SOURCE FILE  #####################################################

$(sort --field-separator="," -k1 ${INPUT_FILE}  $1 > ${INPUT_FILE_SORTED})

currentRelItem=""
linesCount=$(grep "" ${INPUT_FILE_SORTED} | wc -l)
for lineNum in {1..$linesCount}
do
   inLine=`sed -n "${lineNum}p" ${INPUT_FILE_SORTED}`

   tmpFields[1]=`awk -F',' '{print $1}' <<< $inLine`
   tmpFields[2]=`awk -F',' '{print $2}' <<< $inLine`
   tmpFields[3]=`awk -F',' '{print $3}' <<< $inLine`
   tmpFields[4]=`awk -F',' '{print $4}' <<< $inLine`

   if [ "${tmpFields[1]}" = "${currentRelItem}" ] ; then
      echo -n ",${tmpFields[2]}" | cat >> ${STAGING_DIR}/itemlie_f2.tmp
      echo -n ",${tmpFields[3]}" | cat >> ${STAGING_DIR}/itemlie_f3.tmp
      echo -n ",${tmpFields[4]}" | cat >> ${STAGING_DIR}/itemlie_f4.tmp
   else
      echo "" | cat >> ${STAGING_DIR}/itemlie_f1.tmp
      echo "\"" | cat >> ${STAGING_DIR}/itemlie_f2.tmp
      echo "\"" | cat >> ${STAGING_DIR}/itemlie_f3.tmp
      echo "\"" | cat >> ${STAGING_DIR}/itemlie_f4.tmp

      echo -n "${tmpFields[1]}" | cat >> ${STAGING_DIR}/itemlie_f1.tmp
      echo -n "\"${tmpFields[2]}" | cat >> ${STAGING_DIR}/itemlie_f2.tmp
      echo -n "\"${tmpFields[3]}" | cat >> ${STAGING_DIR}/itemlie_f3.tmp
      echo -n "\"${tmpFields[4]}" | cat >> ${STAGING_DIR}/itemlie_f4.tmp
   fi
      currentRelItem=${tmpFields[1]}
done
echo "\"" | cat >> ${STAGING_DIR}/itemlie_f2.tmp
echo "\"" | cat >> ${STAGING_DIR}/itemlie_f3.tmp
echo "\"" | cat >> ${STAGING_DIR}/itemlie_f4.tmp
sed -i '1d' ${STAGING_DIR}/itemlie_f1.tmp
sed -i '1d' ${STAGING_DIR}/itemlie_f2.tmp
sed -i '1d' ${STAGING_DIR}/itemlie_f3.tmp
sed -i '1d' ${STAGING_DIR}/itemlie_f4.tmp
paste -d"," ${STAGING_DIR}/itemlie_f1.tmp ${STAGING_DIR}/itemlie_f2.tmp ${STAGING_DIR}/itemlie_f3.tmp ${STAGING_DIR}/itemlie_f4.tmp > ${OUTPUT_FILE}
$(rm ${STAGING_DIR}/itemlie_f1.tmp)
$(rm ${STAGING_DIR}/itemlie_f2.tmp)
$(rm ${STAGING_DIR}/itemlie_f3.tmp)
$(rm ${STAGING_DIR}/itemlie_f4.tmp)
$(rm -f ${INPUT_FILE_SORTED})

### REMOVE STATUS FILE #######################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi
echo "******  FINISHING $PROGRAM_NAME: $(date '+%Y/%m/%d-%H:%M:%S')  ******"
