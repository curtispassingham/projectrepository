#!/bin/ksh

###################################################################
#
#  This script Pre-Processing Calendar Hierarchy information
#  from RMS to RPAS modules.
#  The output of this script will be available and ready to load
#  inside CATMAN.
#
#  _________________________________________________________
# | Workfile:      | xxadeo_catmant_calendar_hier.ksh	    |
# | Created by:    | PV                                     |
# | Creation Date: | 20180427-1740                          |
# | Modify Date:   | 20181119-1817                          |
# | Version:       | 0.5                                    |
#  ---------------------------------------------------------
# History:
#		0.1 - Initial Release
#		0.2 - Output structure has been changed
#		0.3 - Corrections according Adeo ENV RETL Standards
#			  (Names, Variables,...)
#		0.4 - BUG_615 : The week label didn't have the 
#		    correct day of week. They wanted the first day 
#		    of week (Monday).
#		0.5 - Bug Fixing: BUG_681 - The changes are:
#			- Changed the month that was beeing used on 
#			the week label. The correct month is the month 
#			that match with the first day of week.
#			- The same that applied of the year.
#
###################################################################

################## PROGRAM DEFINITIONS ####################

export PROGRAM_NAME="xxadeo_catmant_calendar_hier"

##########                                       ##########
######################## INCLUDES #########################

# Environment configuration script
#. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env	# (Nearshore ENV)
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env	# (Adeo ENV - RPAS)

# Script Library Collection
#. ${LIB_DIR}/rmse_rpas_lib.ksh						# (Nearshore ENV)
. ${LIB_DIR}/xxadeo_rpas_lib.ksh					# (Adeo ENV - RPAS)

# Only to Testing with CATMAN_DOMAIN
#export CATMAN_DOMAIN=${DATA_DIR}/CATMAN_DOMAIN


##########										##########
################## VALIDATIONS #####################


##########                                      ##########
###################  INPUT/OUTPUT DEFINES ######################

#	**Adeo ENV - RPAS**

#INPUT_DIR=$RETL_IN/RPAS				# As I was thinking
INPUT_DIR=$I_DATA_DIR					# As Renato and your time Defined.
INPUT_DIR_XXADEO=$I_DATA_DIR_XXADEO	# As Renato and your time Defined.

#OUTPUT_DIR=$RETL_OUT/CATMAN			# As I was thinking
OUTPUT_DIR=$O_DATA_DIR					# As Renato and your time Defined.
OUTPUT_DIR_XXADEO=$O_DATA_DIR_XXADEO	# As Renato and your time Defined.

message "[INFO] The INPUT = $INPUT_DIR and OUTPUT = $OUTPUT_DIR "


export FIELD_SEPARATOR=0x2020
export STRING_FIELD_DELIMIT_CHAR=\"        # \ escapes for ksh
export STRING_FIELD_DELIMIT_CHAR_ESCAPED=\"\"
export DATE_FORMAT=YYYYMMDDHHMISS

export INPUT_FILE=$INPUT_DIR/rmse_rpas_clndmstr.dat
export INPUT_SCHEMA_FILE=$SCHEMA_DIR_XXADEO/xxadeo_rmse_rpas_clndmstr.schema		# This schema is custom because the extraction from RMS doesn't have any schema file.
export OUTPUT_FILE=$OUTPUT_DIR_XXADEO/clnd.csv.dat
export OUTPUT_SCHEMA_FILE=$SCHEMA_DIR_XXADEO/${PROGRAM_NAME}.schema

# Internal Variables
NOW_YEAR=$(date '+%Y')
INCREMENT_YEARS=2
LAST_YEARS=$((NOW_YEAR-INCREMENT_YEARS))
NEXT_YEARS=$((NOW_YEAR+INCREMENT_YEARS))


##########                                      ##########
##################  CREATE RETL FLOWS ####################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">
	<OPERATOR type="import">
		<PROPERTY name="inputfile" value="${INPUT_FILE}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE}"/>
		<OUTPUT    name="inRMSCalendarHierarchy.v"/>
	</OPERATOR>

	<OPERATOR type="filter">
		<INPUT name="inRMSCalendarHierarchy.v"/>
		<PROPERTY name="filter" value="YEAR GE ${LAST_YEARS} AND YEAR LE ${NEXT_YEARS}"/>
		<PROPERTY name="rejects" value="true"/>
		<OUTPUT name="rmsCalendarHierarchyFilter.v"/>
		<OUTPUT name="hierarchyReject.v"/>
	</OPERATOR>

	<OPERATOR type="generator">
		<INPUT name="rmsCalendarHierarchyFilter.v"/>
		<PROPERTY name="schema">
			<![CDATA[
			<GENERATE>
				<FIELD name="DATE_LABEL" type="string"
					nullable="false" maxlength="100" >
					<CONST value=""/>
				</FIELD>
				<FIELD name="WEEK_S" type="string"
					nullable="false" maxlength="8" >
					<CONST value="W"/>
				</FIELD>
				<FIELD name="WEEK_LABEL" type="string"
					nullable="false" maxlength="100" >
					<CONST value=""/>
				</FIELD>
				<FIELD name="MONTH_S" type="string"
					nullable="false" maxlength="8" >
					<CONST value="M"/>
				</FIELD>
				<FIELD name="MONTH_LABEL" type="string"
					nullable="false" maxlength="100" >
					<CONST value=""/>
				</FIELD>
				<FIELD name="QUARTER_S" type="string"
					nullable="false" maxlength="8" >
					<CONST value="Q"/>
				</FIELD>
				<FIELD name="QUARTER_LABEL" type="string"
					nullable="false" maxlength="100" >
					<CONST value="Q"/>
				</FIELD>
				<FIELD name="HALF_S" type="string"
					nullable="false" maxlength="8" >
					<CONST value="H"/>
				</FIELD>
				<FIELD name="HALF_LABEL" type="string"
					nullable="false" maxlength="100" >
					<CONST value="H"/>
				</FIELD>
				<FIELD name="YEAR_LABEL" type="string"
					nullable="false" maxlength="100" >
					<CONST value=""/>
				</FIELD>
			</GENERATE>
			]]>
		</PROPERTY>
		<OUTPUT name="inRMSCalendarHierarchyPlusColumn.v"/>
	</OPERATOR>



	<!-- PARSER:  -->
	<OPERATOR type="parser">
		<INPUT name="inRMSCalendarHierarchyPlusColumn.v"/>
		<PROPERTY name="expression">
			<![CDATA[

				RECORD.DATE_LABEL += RECORD.DATE;

				if( RECORD.WEEK < 10 )
				{
					RECORD.WEEK_S += "0";
				}
				RECORD.WEEK_S += RECORD.WEEK;
				RECORD.WEEK_S += "_";
				RECORD.WEEK_S += RECORD.YEAR;
				RECORD.WEEK_LABEL += RECORD.DATE;

				if( RECORD.MONTH < 10 )
				{
					RECORD.MONTH_S += "0";
				}
				RECORD.MONTH_S += RECORD.MONTH;
				RECORD.MONTH_S += "_";
				RECORD.MONTH_S += RECORD.YEAR;

				// Get Month Name
				// Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
				if( RECORD.MONTH == 1 ){
					RECORD.MONTH_LABEL += "Jan";
				}else if( RECORD.MONTH == 2 ){
					RECORD.MONTH_LABEL += "Feb";
				}else if( RECORD.MONTH == 3 ){
					RECORD.MONTH_LABEL += "Mar";
				}else if( RECORD.MONTH == 4 ){
					RECORD.MONTH_LABEL += "Apr";
				}else if( RECORD.MONTH == 5 ){
					RECORD.MONTH_LABEL += "May";
				}else if( RECORD.MONTH == 6 ){
					RECORD.MONTH_LABEL += "Jun";
				}else if( RECORD.MONTH == 7 ){
					RECORD.MONTH_LABEL += "Jul";
				}else if( RECORD.MONTH == 8 ){
					RECORD.MONTH_LABEL += "Aug";
				}else if( RECORD.MONTH == 9 ){
					RECORD.MONTH_LABEL += "Sep";
				}else if( RECORD.MONTH == 10 ){
					RECORD.MONTH_LABEL += "Oct";
				}else if( RECORD.MONTH == 11 ){
					RECORD.MONTH_LABEL += "Nov";
				}else if( RECORD.MONTH == 12 ){
					RECORD.MONTH_LABEL += "Dec";
				}

				RECORD.MONTH_LABEL += " ";
				RECORD.MONTH_LABEL += RECORD.YEAR;

				RECORD.QUARTER_S += RECORD.QUARTER;
				RECORD.QUARTER_S += "_";
				RECORD.QUARTER_S += RECORD.YEAR;

				RECORD.QUARTER_LABEL += RECORD.QUARTER;
				RECORD.QUARTER_LABEL += " ";
				RECORD.QUARTER_LABEL += RECORD.YEAR;

				RECORD.HALF_S += RECORD.HALF;
				RECORD.HALF_S += "_";
				RECORD.HALF_S += RECORD.YEAR;

				RECORD.HALF_LABEL += RECORD.HALF;
				RECORD.HALF_LABEL += " ";
				RECORD.HALF_LABEL += RECORD.YEAR;

				RECORD.YEAR_LABEL += RECORD.YEAR;

			]]>
		</PROPERTY>
		<OUTPUT name="parserCalendarHierarchy.v"/>
	</OPERATOR>

	<OPERATOR type="export">
		<INPUT    name="parserCalendarHierarchy.v"/>
		<PROPERTY name="outputfile" value="${OUTPUT_FILE}.tmp"/>
		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
	</OPERATOR>

	<OPERATOR type="debug">
		<INPUT name="hierarchyReject.v"/>
	</OPERATOR>

</FLOW>
EOF

##########                                      ##########
#################  EXECUTE RETL FLOWS ####################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

# More Transformations:
#	Dates - e.g.: 20140831 to 31/08/2014 . It's apply to fields WEEK_LABEL and DATE_LABEL
#awk -F',' -v OFS=',' '{ year = substr($2,1,4); month = substr($2,5,2); day = substr($2,7,2); newDt = day"/"month"/"year; $2 = newDt; $4 = "W" newDt; print $0; }' ${OUTPUT_FILE}.tmp > ${OUTPUT_FILE}
#
#BUG FIXING - Bug: 615 - The week label didn't have the correct day of week. They wanted the first day of week (Monday).
#BUG FIXING - Bug: 681 - The week label didn't have the correct month of week. They wanted the month correctly according the first day of week. 
awk -F',' -v OFS=',' '{ 
	cmd=$2"-$(date -d "$2" +%u)days + 1 day"; 
	cmd="\""cmd"\""; cmd2="date -d "cmd" +%d-%m-%Y"; 
	cmd2 | getline firstMonth_day_week;  
	year = substr($2,1,4); 
	month = substr($2,5,2); 
	day = substr($2,7,2); 
	newDt = day"/"month"/"year; 
	$2 = newDt;
	split(firstMonth_day_week,day_month_array,"-"); 
	$4 = "W"day_month_array[1]"/"day_month_array[2]"/"day_month_array[3]; 
	print $0; }' ${OUTPUT_FILE}.tmp > ${OUTPUT_FILE}


##########                                      ##########
#################  REMOVE STATUS FILE ####################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

##########                                      ##########
###################  REPORT AND EXIT #####################

case $retl_stat in
  0 )
	 msg="Program completed successfully with" ;;
  * )
	 msg="Program exited with error" ;;
esac

##########                                      ##########
#################  CLEAN UP TMP FILES ####################
rm -f ${OUTPUT_FILE}.tmp


message "$msg code: $retl_stat"
rmse_terminate $retl_stat
