#!/bin/ksh

#####################################################################################
#                                                                                   #
# Object Name:  xxadeo_catmant_item_flags.ksh                                       #
# Description:  The xxadeo_catmant_item_flags.ksh program                           #
#               extracts from XXRMS214_ITEMFLAGS_YYYYMMDD-HHMISS.dat information    #
#               to generate 1 output file (itemCreFlag.csv.rpl) for RPAS            #
#                                                                                   #
# Version:       2.2                                                                #
# Author:        CGI                                                                #
# Creation Date: 18/03/2018                                                         #
# Last Modified: 09/11/2018                                                         #
# History:                                                                          #
#               1.0 - Initial version                                               #
#               1.1 - Update the variables for environment, librairies              #
#                     and directory names. We have to use $RETL_IN                  #
#                     to put output file because this IR is a transformation        #
#                     This new version is created because previoulsy (version 1.0)  #
#                     it was an extraction from RMS Tables                          #
#               1.2 - update programm name                                          #
#               1.3 - Deleting variable of input, output and schema                 #
#               1.4 - Modify: Input path, Output path and script name               #
#               2.0 - add a parser to modify the output file                        #
#               2.1 - add filter ITEM_LEVEL==1                                      #
#               2.2 - add sort on input files to avoid warning during execution of  #
#                     the ksh                                                       #
#                                                                                   #
#####################################################################################


###############################################################################
#  settings of the local DIR
#  according to the local variables (.profile)
###############################################################################

export PROGRAM_NAME="xxadeo_catmant_item_flags"
export extractDate=`date +"%G%m%d-%H%M%S"`


##############################################################################
#  PROGRAM DEFINE
#  must be the first define
##############################################################################


##############################################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
##############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh


##############################################################################
#  INPUT/OUTPUT DEFINES
#  This section must come after INCLUDES
##############################################################################

export I_INPUT_FILE_PATTERN="XXRMS214_ITEMFLAGS_*.dat"
export I_INPUT_FILE_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_item_flags.schema

export I_INPUT_RPAS_FILE="rmse_rpas_item_master.dat"
export I_INPUT_RPAS_FILE_SCHEMA=${SCHEMA_DIR_XXADEO}/rmse_rpas_item_master.schema;

export O_ITEMCREFLAGS_FILE=${O_DATA_DIR_XXADEO}/itemCreFlag.csv.rpl
export O_ITEMCREFLAGS_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema


###############################################################################
#  Get latest available input files, because since this interface works in a
#  FULL mode there is no need to process all available input files.
###############################################################################

I_INPUT_FILE=`find ${I_DATA_DIR_XXADEO} -name ${I_INPUT_FILE_PATTERN} | sort | tail -n 1`
if [[ ! -f ${I_INPUT_FILE} ]];  then
    message "XXRMS214_ITEMFLAGS_*.dat  does not exist"
    exit 1
fi

I_INPUT_FILE2=`find ${I_DATA_DIR_XXADEO} -name ${I_INPUT_RPAS_FILE} | sort | tail -n 1`
if [[ ! -f ${I_INPUT_FILE2} ]];  then
    message "rmse_rpas_item_master.dat  does not exist"
    exit 1
fi

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${O_ITEMCREFLAGS_FILE}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        fi
}


###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

if [ ! -s "${I_INPUT_FILE}" ];then
      message "${I_INPUT_FILE} is empty"
else

FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml

cat > ${FLOW_FILE} << EOF
<FLOW name = "${PROGRAM_NAME}.flw">

      <OPERATOR type="import">
                  <PROPERTY name="inputfile" value="${I_INPUT_FILE}"/>
                  <PROPERTY name="schemafile" value="${I_INPUT_FILE_SCHEMA}"/>
                  <OUTPUT   name="itemCreFlag.v"/>
      </OPERATOR>

      <OPERATOR type="sort">
          <INPUT name="itemCreFlag.v" />
          <PROPERTY name="key" value="ITEM"/>
          <OUTPUT name="itemCreFlag_sort1.v" />
      </OPERATOR>

      <OPERATOR type="fieldmod">
                  <INPUT    name="itemCreFlag_sort1.v" />
                  <PROPERTY name="keep" value="ITEM SELLABLE_IND ORDERABLE_IND FORECAST_IND INVENTORY_IND PACK_IND SIMPLE_PACK_IND MERCHANDISE_IND"/>
                  <PROPERTY name="rename" value="ARTICLE=ITEM"/>
                  <OUTPUT   name="itemCreFlag_fieldmod1.v"/>
      </OPERATOR>

      <OPERATOR type="import">
                  <PROPERTY name="inputfile" value="${I_INPUT_FILE2}"/>
                  <PROPERTY name="schemafile" value="${I_INPUT_RPAS_FILE_SCHEMA}"/>
                  <OUTPUT   name="itemCreFlag2.v"/>
      </OPERATOR>

      <OPERATOR type="sort">
                  <INPUT name="itemCreFlag2.v" />
                  <PROPERTY name="key" value="ITEM"/>
                  <OUTPUT name="itemCreFlag_sort2.v" />
          </OPERATOR>

          <OPERATOR type="fieldmod">
                  <INPUT    name="itemCreFlag_sort2.v" />
                  <PROPERTY name="keep" value="ITEM ITEM_LEVEL"/>
                  <PROPERTY name="rename" value="ARTICLE=ITEM"/>
                  <OUTPUT   name="itemCreFlag_fieldmod2.v"/>
      </OPERATOR>

      <OPERATOR type="innerjoin" name="innerjoin,0">
                  <INPUT name="itemCreFlag_fieldmod1.v"/>
                  <INPUT name="itemCreFlag_fieldmod2.v"/>
                  <PROPERTY name="key" value="ARTICLE"/>
                  <OUTPUT name="itemCreFlag_innerjoin.v"/>
      </OPERATOR>

      <OPERATOR type="filter">
                  <INPUT name="itemCreFlag_innerjoin.v"/>
                  <PROPERTY name="filter" value="ITEM_LEVEL EQ 1 "/>
                  <OUTPUT name="itemCreFlag_filter.v"/>
      </OPERATOR>


      <OPERATOR type="sort">
                  <INPUT    name="itemCreFlag_filter.v"/>
                  <PROPERTY name="key" value="ARTICLE"/>
                  <OUTPUT   name="itemCreFlag_sort.v"/>
      </OPERATOR>

      <OPERATOR type="parser">
                  <INPUT name="itemCreFlag_sort.v"/>
                  <PROPERTY name="expression">
                          <![CDATA[
                          if (RECORD.SELLABLE_IND == "Y")
                          {
                                  RECORD.SELLABLE_IND = "1";
                          }
                          else
                          {
                                  RECORD.SELLABLE_IND = "0";
                          }
                          if (RECORD.ORDERABLE_IND == "Y")
                          {
                                  RECORD.ORDERABLE_IND = "1";
                          }
                          else
                          {
                                  RECORD.ORDERABLE_IND = "0";
                          }
                          if (RECORD.FORECAST_IND == "Y")
                          {
                                  RECORD.FORECAST_IND = "1";
                          }
                          else
                          {
                                  RECORD.FORECAST_IND = "0";
                          }
                          if (RECORD.INVENTORY_IND == "Y")
                          {
                                  RECORD.INVENTORY_IND = "1";
                          }
                          else
                          {
                                  RECORD.INVENTORY_IND = "0";
                          }
                          if (RECORD.PACK_IND == "Y")
                          {
                                  RECORD.PACK_IND = "1";
                          }
                          else
                          {
                                  RECORD.PACK_IND = "0";
                          }
                          if (RECORD.SIMPLE_PACK_IND == "Y")
                          {
                                  RECORD.SIMPLE_PACK_IND = "1";
                          }
                          else
                          {
                                  RECORD.SIMPLE_PACK_IND = "0";
                          }
                          if (RECORD.MERCHANDISE_IND  == "Y")
                          {
                                  RECORD.MERCHANDISE_IND  = "1";
                          }
                          else
                          {
                                  RECORD.MERCHANDISE_IND  = "0";
                          }
                          ]]>
                          </PROPERTY>
                          <OUTPUT name="itemCreFlag_parser.v"/>
      </OPERATOR>

      <OPERATOR type="export">
            <INPUT    name="itemCreFlag_parser.v"/>
            <PROPERTY name="outputmode" value="overwrite"/>
            <PROPERTY name="outputfile" value="${O_ITEMCREFLAGS_FILE}"/>
            <PROPERTY name="schemafile" value="${O_ITEMCREFLAGS_SCHEMA}"/>
      </OPERATOR>

</FLOW>
EOF


###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

log_num_recs ${O_ITEMCREFLAGS_FILE}

checkerror -e $? -m "Program failed - check $ERR_FILE"
fi

###############################################################################
# cleanup and exit
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi
message "Program completed successfully"

check_error_and_clean_before_exit 0 "NO_CHECK"