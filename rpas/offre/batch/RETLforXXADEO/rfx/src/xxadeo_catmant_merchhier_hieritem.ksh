#!/bin/ksh

### INTRO #####################################################################
#  ____________________________________________________________________________
#  Workfile:             | xxadeo_catmant_merchhier_hieritem.ksh
#  Created by:           | PR <paulo.reis@oracle.com>
#  Create Datetime:      | 20180427-1600
#  Last Updated by:      | PVI <pedro.vieira@oracle.com>
#  Last Update Datetime: | 20181106-1100
#  Version:              | 1.5
#
#  Interface IDs:
#  --------------
#  RICEW #: 264 / 265
#  IR ID: IR0079
#
#  This auxiliary script (related to the IR ID above) joins Merchandise
#  Hierarchy information with Item information, extracted from RMS.
#
#  Usage: xxadeo_catmant_merchhier_hieritem.ksh
#
#  Expected file:
#    1. xxadeo_catmant_merchhier_hieritem.dat
#
#  History:
#       1.3 - Added/Changed Includes and Variables according Adeo Env
#	    - Also, the schema of this interface has been updated
#	    - Changed the script name according the Adeo Standards
#	1.4 - BUG: Correction on the join between input files
#       1.5 - Bug Fixing: BUG_633_634_635 - The changes are:
#               - Assortment Group Label and ID fixed
#		- Filter applied on Item Level equal Tran Level
#               - Parent and GrandParent Label and ID fixed
#               - Perfomance fixed
#
###############################################################################

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_catmant_merchhier_hieritem"


### INCLUDES ##################################################################

## Environment configuration script
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env			# (Adeo ENV - RPAS)

## Script Library Collection
. ${LIB_DIR}/xxadeo_rpas_lib.ksh							# (Adeo ENV - RPAS)

### INPUT / OUTPUT DEFINITIONS ################################################

export INBOUND_DIR=${I_DATA_DIR}						# Adeo ENV (Base RPAS Input Dir)
export INBOUND_DIR_CUST=${I_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Input Dir)


export OUTBOUND_DIR=${O_DATA_DIR}						# Adeo ENV (Base RPAS Output Dir)
export OUTBOUND_DIR_CUST=${O_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Output Dir)


export STAGING_DIR=${INBOUND_DIR_CUST}					# Adeo ENV

export INPUT_FILE_1=${INBOUND_DIR}/rmse_rpas_merchhier.dat		# Adeo ENV
export INPUT_SCHEMA_FILE_1=${SCHEMA_DIR}/rmse_rpas_merchhier.schema
export INPUT_REJECTED_FILE_1=${STAGING_DIR}/${PROGRAM_NAME}_file_1.rej

export INPUT_FILE_2=${INBOUND_DIR}/rmse_rpas_item_master.dat
export INPUT_SCHEMA_FILE_2=${SCHEMA_DIR}/rmse_rpas_item_master.schema
export INPUT_REJECTED_FILE_2=${STAGING_DIR}/${PROGRAM_NAME}_file_2.rej

export SORT_FIELD="SUBCLASS CLASS DEPT"
export JOIN_FIELD=$SORT_FIELD

export OUTPUT_FILE=${OUTBOUND_DIR_CUST}/${PROGRAM_NAME}.dat				# Adeo ENV
export OUTPUT_SCHEMA_FILE=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema	# Adeo ENV

### CREATE RETL FLOWS #########################################################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
	<!-- IMPORT:
		Input file 1 - merchandise hierarchy
	-->
	<OPERATOR type="import">
		<PROPERTY name="inputfile" value="${INPUT_FILE_1}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_1}"/>
		<PROPERTY name="rejectfile" value="${INPUT_REJECTED_FILE_1}"/>
		<OUTPUT name="merch_hierarchy.v"/>
	</OPERATOR>

	<!-- SORT:
		Sort merchandise hierarchy by Subclass, Class and Dept
	-->
	<OPERATOR type="sort">
		<PROPERTY name="key" value="${SORT_FIELD}"/>
		<INPUT name="merch_hierarchy.v"/>
		<OUTPUT name="merch_hierarchy_sorted.v"/>
	</OPERATOR>

<!-- ....................................................................... -->

	<!-- IMPORT:
		Input file 2 - item master
	-->
	<OPERATOR type="import">
		<PROPERTY name="inputfile" value="${INPUT_FILE_2}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_2}"/>
		<PROPERTY name="rejectfile" value="${INPUT_REJECTED_FILE_2}"/>
		<OUTPUT name="item_master.v"/>
	</OPERATOR>

	<!-- FIELDMOD:
		Item Master - Keep only the necessary fields
	-->
	<OPERATOR type="fieldmod" name="itemsL1_drop_rename_columns">
		<INPUT name="item_master.v"/>
		<PROPERTY name="keep" value="ITEM ITEM_DESC ITEM_PARENT ITEM_GRANDPARENT ITEM_LEVEL TRAN_LEVEL SUBCLASS CLASS DEPT"/>
		<OUTPUT name="item_master_clean.v"/>
	</OPERATOR>

        <!-- FILTER:
                Item Master - Keep only the items that Item Level are equal Tran Level
                output 1: follows the filter condition (ITEM_LEVEL == TRAN_LEVEL)
                output 2: rejects the filter condition (ITEM_LEVEL != TRAN_LEVEL)
        -->
        <OPERATOR type="filter">
                <INPUT name="item_master_clean.v"/>
                <PROPERTY name="filter" value="ITEM_LEVEL EQ TRAN_LEVEL"/>
                <PROPERTY name="rejects" value="true"/>
                <OUTPUT name="item_level_equal_tran_level.v"/>
                <OUTPUT name="item_level_notequal_tran_level.v"/>
        </OPERATOR>

	<OPERATOR type="debug">
		<INPUT name="item_level_notequal_tran_level.v"/>
	</OPERATOR>

	<!-- GENERATOR:
		Item Master - Generator the fields ITEM_PARENT_DESC and ITEM_GRANDPARENT_DESC
	-->
	<OPERATOR type="generator">
		<INPUT name="item_level_equal_tran_level.v"/>
		<PROPERTY name="schema">
			<![CDATA[
				<GENERATE>
					<FIELD name="ITEM_PARENT_DESC" type="string" maxlength="250" nullable="true">
						<CONST value=""/>
					</FIELD>
					<FIELD name="ITEM_GRANDPARENT_DESC" type="string" maxlength="250" nullable="true">
						<CONST value=""/>
					</FIELD>
				</GENERATE>
			]]>
		</PROPERTY>
		<OUTPUT name="item_master_gen.v"/>
	</OPERATOR>

	<!-- FILTER:
		Item Master - Get items without parent and grandparent (Items Level 1)
		output 1: follows the filter condition (items level 1)
		output 2: rejects the filter condition (items level 2 and 3)
	-->
	<OPERATOR type="filter">
		<INPUT name="item_master_gen.v"/>
		<PROPERTY name="filter" value="ITEM_PARENT IS_NULL AND ITEM_GRANDPARENT IS_NULL"/>
		<PROPERTY name="rejects" value="true"/>
		<OUTPUT name="items_level1.v"/>
		<OUTPUT name="items_level_2_3.v"/>
	</OPERATOR>

	<!-- FILTER:
		Item Master - Get items with parent but without grandparent (Items Level 2)
		output 1: follows the filter condition (items level 2)
		output 2: rejects the filter condition (items level 3)
	-->
	<OPERATOR type="filter">
		<INPUT name="items_level_2_3.v"/>
		<PROPERTY name="filter" value="ITEM_PARENT IS_NOT_NULL AND ITEM_GRANDPARENT IS_NULL"/>
		<PROPERTY name="rejects" value="true"/>
		<OUTPUT name="items_level2.v"/>
		<OUTPUT name="items_level3.v"/>
	</OPERATOR>

	<!-- COPY:
		Duplicate the items level 1 data
	-->
	<OPERATOR type="copy">
		<INPUT name="items_level1.v"/>
		<OUTPUT name="items_level1_to_collect.v"/>
		<OUTPUT name="items_level1_to_join.v"/>
	</OPERATOR>

	<!-- FIELDMOD:
		Items Level 1 - Remove unnecessary columns and rename others
	-->
	<OPERATOR type="fieldmod">
		<INPUT name="items_level1_to_join.v"/>
		<PROPERTY name="keep" value="ITEM ITEM_DESC"/>
		<PROPERTY name="rename" value="ITEM_PARENT_DESC=ITEM_DESC"/>
		<PROPERTY name="rename" value="ITEM_PARENT=ITEM"/>
		<OUTPUT name="items_level1_clean.v"/>
	</OPERATOR>

	<!-- SORT:
			By Item Parent Level
	-->
	<OPERATOR type="sort">
		<INPUT name="items_level1_clean.v"/>
		<PROPERTY name="key" value="ITEM_PARENT"/>
		<OUTPUT name="items_level1_sorted.v"/>
	</OPERATOR>

	<!-- FIELDMOD:
		Items Level 2 - Remove unnecessary columns
	-->
	<OPERATOR type="fieldmod">
		<INPUT name="items_level2.v"/>
		<PROPERTY name="drop" value="ITEM_PARENT_DESC"/>
		<OUTPUT name="items_level2_clean.v"/>
	</OPERATOR>

	<!-- SORT:
			By Item Parent Level
	-->
	<OPERATOR type="sort">
		<INPUT name="items_level2_clean.v"/>
		<PROPERTY name="key" value="ITEM_PARENT"/>
		<OUTPUT name="items_level2_sorted.v"/>
	</OPERATOR>

	<!-- JOIN:
		L1 and L2 By Item Parent Level
	-->
	<OPERATOR type="innerjoin">
		<INPUT name="items_level2_sorted.v"/>
		<INPUT name="items_level1_sorted.v"/>
		<PROPERTY name="key" value="ITEM_PARENT"/>
		<OUTPUT name="items_level1_2_joined.v"/>
	</OPERATOR>

	<!-- COPY:
		Duplicate the joined data between item level 1 and 2
	-->
	<OPERATOR type="copy">
		<INPUT name="items_level1_2_joined.v"/>
		<OUTPUT name="items_level1_2_to_collect.v"/>
		<OUTPUT name="items_level1_2_to_join.v"/>
	</OPERATOR>

	<!-- FIELDMOD:
		Items Level 1 & 2 - Remove unnecessary columns and rename others
	-->
	<OPERATOR type="fieldmod">
		<INPUT name="items_level1_2_to_join.v"/>
		<PROPERTY name="keep" value="ITEM ITEM_DESC ITEM_PARENT ITEM_PARENT_DESC"/>
		<PROPERTY name="rename" value="ITEM_GRANDPARENT_DESC=ITEM_PARENT_DESC"/>
		<PROPERTY name="rename" value="ITEM_GRANDPARENT=ITEM_PARENT"/>
		<PROPERTY name="rename" value="ITEM_PARENT_DESC=ITEM_DESC"/>
		<PROPERTY name="rename" value="ITEM_PARENT=ITEM"/>
		<OUTPUT name="items_level1_2_clean.v"/>
	</OPERATOR>

	<!-- SORT:
			By Item and Item Parent Level
	-->
	<OPERATOR type="sort">
		<INPUT name="items_level1_2_clean.v"/>
		<PROPERTY name="key" value="ITEM_PARENT ITEM_GRANDPARENT"/>
		<OUTPUT name="items_level1_2_sorted.v"/>
	</OPERATOR>

	<!-- FIELDMOD:
		Items Level 3 - Remove unnecessary columns and rename others
	-->
	<OPERATOR type="fieldmod">
		<INPUT name="items_level3.v"/>
		<PROPERTY name="drop" value="ITEM_PARENT_DESC ITEM_GRANDPARENT_DESC"/>
		<OUTPUT name="items_level3_clean.v"/>
	</OPERATOR>

	<!-- SORT:
			By Item and Item Parent Level
	-->
	<OPERATOR type="sort">
		<INPUT name="items_level3_clean.v"/>
		<PROPERTY name="key" value="ITEM_PARENT ITEM_GRANDPARENT"/>
		<OUTPUT name="items_level3_sorted.v"/>
	</OPERATOR>

	<!-- JOIN:
		L1_L2 and L3 By Item Parent and Grand Parent Level
	-->
	<OPERATOR type="innerjoin">
		<INPUT name="items_level3_sorted.v"/>
		<INPUT name="items_level1_2_sorted.v"/>
		<PROPERTY name="key" value="ITEM_PARENT ITEM_GRANDPARENT"/>
		<OUTPUT name="items_level1_2_3_to_collect.v"/>
	</OPERATOR>

	<!-- COLLECT:
		Collcet the data L1, L1_L2 and L3
	-->
	<OPERATOR type="collect">
		<INPUT name="items_level1_to_collect.v"/>
		<INPUT name="items_level1_2_to_collect.v"/>
		<INPUT name="items_level1_2_3_to_collect.v"/>
		<OUTPUT name="itemmaster_parent_grandparent_desc.v"/>
	</OPERATOR>

	<!-- SORT:
			Item Master: By Class, Subclass and Dept
	-->
	<OPERATOR type="sort">
		<INPUT name="itemmaster_parent_grandparent_desc.v"/>
		<PROPERTY name="key" value="${SORT_FIELD}"/>
		<OUTPUT name="itemmaster_parent_grandparent_desc_sorted.v"/>
	</OPERATOR>

	<!-- JOIN:
		Between Item Master and Merchandise Hierarchy
	-->
	<OPERATOR type="innerjoin">
		<INPUT name="itemmaster_parent_grandparent_desc_sorted.v"/>
		<INPUT name="merch_hierarchy_sorted.v"/>
		<PROPERTY name="key" value="${JOIN_FIELD}"/>
		<OUTPUT name="itemmaster_merchhierarchy_joined.v"/>
	</OPERATOR>

	<!-- GENERATOR:
                Item Master - Generator the fields ITEM_PARENT_DESC and ITEM_GRANDPARENT_DESC
        -->
        <OPERATOR type="generator">
                <INPUT name="itemmaster_merchhierarchy_joined.v"/>
                <PROPERTY name="schema">
                        <![CDATA[
			<GENERATE>
				<FIELD name="SUBCLASS_LPAD" type="string" maxlength="5" nullable="false">
					<CONST value="0"/>
				</FIELD>
				<FIELD name="CLASS_LPAD" type="string" maxlength="5" nullable="false">
					<CONST value="0"/>
				</FIELD>
				<FIELD name="DEPT_LPAD" type="string" maxlength="5" nullable="false">
					<CONST value="0"/>
				</FIELD>
				<FIELD name="GROUP_NO_LPAD" type="string" maxlength="5" nullable="false">
					<CONST value="0"/>
				</FIELD>
				<FIELD name="DIVISION_LPAD" type="string" maxlength="5" nullable="false">
					<CONST value="0"/>
				</FIELD>
				<FIELD name="COMPANY_LPAD" type="string" maxlength="5" nullable="false">
					<CONST value="0"/>
				</FIELD>
			</GENERATE>
                        ]]>
                </PROPERTY>
                <OUTPUT name="itemmaster_merchhierarchy_gen.v"/>
        </OPERATOR>

	<OPERATOR type="parser">
		<INPUT name="itemmaster_merchhierarchy_gen.v"/>
		<PROPERTY name="expression">
		<![CDATA[
			RECORD.SUBCLASS_LPAD = RECORD.SUBCLASS;
			RECORD.CLASS_LPAD = RECORD.CLASS;
			RECORD.DEPT_LPAD = RECORD.DEPT;
			RECORD.GROUP_NO_LPAD = RECORD.GROUP_NO;
			RECORD.DIVISION_LPAD = RECORD.DIVISION;
			RECORD.COMPANY_LPAD = RECORD.COMPANY;
		]]>
		</PROPERTY>
		<OUTPUT name="itemmaster_merchhierarchy_final.v"/>
	</OPERATOR>


	<!-- EXPORT:
		Export Result
	-->
	<OPERATOR type="export">
	    <INPUT    name="itemmaster_merchhierarchy_final.v"/>
	    <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
	    <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
	</OPERATOR>

</FLOW>
EOF

### EXECUTE RETL FLOWS ########################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

### REMOVE STATUS FILE #######################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

### REMOVE BLANK REJECTED FILE ################################################

## Remove if blank
if [ `cat ${INPUT_REJECTED_FILE_1} | wc -l` -eq 0 ]; then rm ${INPUT_REJECTED_FILE_1}; fi
if [ `cat ${INPUT_REJECTED_FILE_2} | wc -l` -eq 0 ]; then rm ${INPUT_REJECTED_FILE_2}; fi

### ZERO PAD ID CODES  #######################################################

awk -F'\x1F' -v OFS="\x1F" '{ 
$2 = sprintf("%05d", $2); 
$5 = sprintf("%05d", $5);
$8 = sprintf("%05d", $8);
$11 = sprintf("%05d", $11);
$14 = sprintf("%05d", $14);
$17 = sprintf("%05d", $17);
print $0; }' ${OUTPUT_FILE} > ${OUTPUT_FILE}.tmp

mv $OUTPUT_FILE.tmp $OUTPUT_FILE

###  REPORT AND EXIT ##########################################################

case $retl_stat in
  0 )
     msg="Program completed successfully with" ;;
  * )
     msg="Program exited with error" ;;
esac

message "$msg code: $retl_stat"
rmse_terminate $retl_stat
