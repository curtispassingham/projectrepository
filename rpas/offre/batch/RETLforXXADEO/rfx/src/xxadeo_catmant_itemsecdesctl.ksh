#!/bin/ksh

##########################################################################
#
#  Title: Transformation of the Item Translation according CatMan Requirements
#  Description:
#  The xxadeo_catmant_itemsecdesctl.ksh program load and transform the item translation information from
#  RMS file (XXRMS219_ITEMMASTERTL_yyyymmdd-hhmmss.dat) and places the data into a flat file (desgn_sec.csv.ovr)
#  to be be loaded into RPAS module (CatMan).
#  The main goal of this interface is to grouping item by translation. But only for the following LANG_ISO,
#      --> 'FR' 'EL' 'IT' 'PL' 'PT' 'ES'
#  Version:		0.2
#  Owner: Oracle Retail
#  History: 	0.1 - Initial Release
#		0.2 - Corrections about names and variables (Follow Adeo Standards)
#		0.3 - Bug Fixing: BUG_642 - It's not a BUG, it's only an improvement. 
#			Added another input file (item master) to apply a filter 
#			by item level = 1. They only want those items.
#
###############################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="xxadeo_catmant_itemsecdesctl"
EXE_DATE=`date +"%G%m%d-%H%M%S"`;
FIELD_SEP="\x1F";

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

# Environment configuration script
#. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env	# (Adeo ENV - MOM)
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env	# (Adeo ENV - RPAS)

# Script Library Collection
#. ${LIB_DIR}/xxadeo_rmse_lib.ksh					# (Adeo ENV - MOM)
. ${LIB_DIR}/xxadeo_rpas_lib.ksh					# (Adeo ENV - RPAS)

##################  INPUT/OUTPUT DEFINES ######################
########### (this section must come after INCLUDES) ###########

# --INPUTS

#export DATA_DIR_IN=$RETL_IN/RPAS   # As I was thinking
INPUT_DIR=$I_DATA_DIR					      # As Renato and your time Defined.
INPUT_DIR_XXADEO=$I_DATA_DIR_XXADEO	# As Renato and your time Defined.

# --Get Name of the last file that has been provided by RMS about Item Translations
INPUT_FILE_NAME=`ls -t ${INPUT_DIR_XXADEO} | grep 'XXRMS219_ITEMMASTERTL_.*\.dat$' | head -1`

export INPUT_FILE=${INPUT_DIR_XXADEO}/${INPUT_FILE_NAME}

export INPUT_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_itemmastertl.schema

export INPUT_FILE_2=${INPUT_DIR}/rmse_rpas_item_master.dat

export INPUT_SCHEMA_2=${SCHEMA_DIR}/rmse_rpas_item_master.schema

# --OUTPUTS

#export DATA_DIR_OUT=$RETL_OUT/CATMAN   # As I was thinking
OUTPUT_DIR=$O_DATA_DIR					        # As Renato and your time Defined.
OUTPUT_DIR_XXADEO=$O_DATA_DIR_XXADEO  	# As Renato and your time Defined.

export OUTPUT_FILE=${OUTPUT_DIR_XXADEO}/desgn_sec.csv.ovr

export OUTPUT_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema


###################  FUNCTIONS DEFINES ###################
######## (this section must come after FUNCTIONS) ########

######################################################################
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
######################################################################
function USAGE
{
   echo "USAGE: . $pgmName "
}

######################################################################
# Function Name: PROCESS
# Purpose      : Defines the flow of the program should be invoked
######################################################################
function PROCESS
{
####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

message "The input file is "${INPUT_FILE}

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

###############################################################################
#  Convert input CSV file to data file format
###############################################################################

# -- The default field delimiter: delimiter="0x1F"
# -- With parameter csv-retlsv, convert file from CSV to DATA file.
csv_converter "csv-retlsv" "${INPUT_FILE}" "${INPUT_FILE}.retl"
checkerror -e $? -m "Convert from CSV file to Data file failed - check $ERR_FILE"


########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
    	<OPERATOR type="import">
        	<PROPERTY name="inputfile" value="${INPUT_FILE}.retl"/>
        	<PROPERTY name="schemafile" value="${INPUT_SCHEMA}"/>
        	<OUTPUT   name="item_descriptions_tl.v"/>
	</OPERATOR>

	<OPERATOR type="import">
        	<PROPERTY name="inputfile" value="${INPUT_FILE_2}"/>
        	<PROPERTY name="schemafile" value="${INPUT_SCHEMA_2}"/>
        	<OUTPUT   name="item_master.v"/>
     	</OPERATOR>

	<OPERATOR type="fieldmod">
		<INPUT name="item_master.v"/>
		<PROPERTY name="keep" value="ITEM ITEM_LEVEL"/>
		<OUTPUT name="item_master_clean.v"/>
	</OPERATOR>

	<OPERATOR type="filter">
		<INPUT name="item_master_clean.v"/>
		<PROPERTY name="filter" value="ITEM_LEVEL EQ 1"/>
		<OUTPUT name="item_master_filter.v"/>
	</OPERATOR>

	<OPERATOR type="sort">
                <INPUT    name="item_master_filter.v"/>
                <PROPERTY name="key" value="ITEM"/>
                <OUTPUT   name="item_master_sorted.v"/>
        </OPERATOR>

     	<OPERATOR type="sort">
        	<INPUT    name="item_descriptions_tl.v"/>
        	<PROPERTY name="key" value="ITEM"/>
        	<OUTPUT   name="item_descriptions_tl_sorted.v"/>
    	</OPERATOR>

	<OPERATOR type="innerjoin">
		<INPUT name="item_descriptions_tl_sorted.v"/>
		<INPUT name="item_master_sorted.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="item_descriptions_tl_joined.v"/>
	</OPERATOR>

	<OPERATOR type="sort">
                <INPUT    name="item_descriptions_tl_joined.v"/>
                <PROPERTY name="key" value="ITEM LANG_ISO"/>
                <OUTPUT   name="item_descriptions_tl_final.v"/>
        </OPERATOR>
		
    	<OPERATOR type="export">
        	<INPUT    name="item_descriptions_tl_final.v"/>
        	<PROPERTY name="outputfile" value="${OUTPUT_FILE}.retl.tmp"/>
        	<PROPERTY name="schemafile" value="${INPUT_SCHEMA}"/>
     	</OPERATOR>
</FLOW>

EOF

###############################################
#  Execute the RETL flow
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE
checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

# -- Processing the file
# File in structure: ITEM[0x1F]LANG_ISO[0x1F]LANG_NAME[0x1F]ITEM_DESC[0x1F]ITEM_DESC_SECONDARY
# File out structure:ITEM[0x1F]ITEM_DESC_SECONDARY_LANG1[0x1F]ITEM_DESC_SECONDARY_LANG2(...)
awk -F$FIELD_SEP -v v_fseparator=$FIELD_SEP '
function saveDescByLang(lang,desc,arrayDt){
 if( lang == "FR" ){ arrayDt[1] = desc; }
 else if( lang == "EL" ){ arrayDt[2] = desc; }
 else if( lang == "IT" ){ arrayDt[3] = desc; }
 else if( lang == "PL" ){ arrayDt[4] = desc; }
 else if( lang == "PT" ){ arrayDt[5] = desc; }
 else if( lang == "ES" ){ arrayDt[6] = desc; }
 else{}
};
function writeToOutput(item,sep,arrayDt){
  v_desc = ""; l = length(arrayDt); c = 1;
  for ( n = 1; n <= l; n++ ){
    if( c == l ){
      v_desc = v_desc arrayDt[n];
      arrayDt[n] = "";
    }else{
      v_desc = v_desc arrayDt[n] sep;
      arrayDt[n] = "";
    }
    c++;
  };
  print item sep v_desc;
};
BEGIN {
  v_isoLstDesc[1]=""; v_isoLstDesc[2]=""; v_isoLstDesc[3]="";
  v_isoLstDesc[4]=""; v_isoLstDesc[5]=""; v_isoLstDesc[6]="";
  v_last_item = "NULL";
}
{
  if(v_last_item == "NULL"){
    v_last_item = $1;
  };
  v_current_item = $1;
  v_lang_iso = $2;
  if(v_current_item == v_last_item){
    gsub(/\x22\x20/,"",v_lang_iso);
    saveDescByLang(v_lang_iso,$5,v_isoLstDesc);
  }else{
    writeToOutput(v_last_item,v_fseparator,v_isoLstDesc);
    saveDescByLang(v_lang_iso,$5,v_isoLstDesc);
  };
  v_last_item = v_current_item;
}
END {
  if(v_last_item != "NULL"){
    writeToOutput(v_last_item,v_fseparator,v_isoLstDesc);
  };
}' ${OUTPUT_FILE}.retl.tmp > ${OUTPUT_FILE}.retl

checkerror -e $? -m "AWK transformation work failed - check $ERR_FILE"

###############################################################################
#  Convert output data files to CSV format
###############################################################################

# -- The default field delimiter: delimiter="0x1F"
csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
checkerror -e $? -m "Convert from Data file to CSV file failed - check $ERR_FILE"

###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE}

#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."
}

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
######################################################################
#     MAIN
######################################################################
#////////////////////////////////////////////////////////////////////

#----------------------------------------------
#  Log start message and define the RETL flow file
#----------------------------------------------

if [[ $# -gt 0  ]]; then
   USAGE
   rmse_terminate 1
else
  PROCESS
fi

# --Clean
rm -f "${INPUT_FILE}.retl"
rm -f "${OUTPUT_FILE}.retl"
rm -f "${OUTPUT_FILE}.retl.tmp"

message  "xxadeo_catmant_itemsecdesctl.ksh  - Completed by ${USER}" "" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

rmse_terminate 0
