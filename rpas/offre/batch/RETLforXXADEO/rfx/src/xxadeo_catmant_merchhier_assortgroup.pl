#!/usr/bin/perl

##################################################################
#
#  Description:
#
#  This script Pre-Processing Item Pack file and Related Items
#  to produce an assortment group file for all items on these
#  files.
#
#  Input(s):
#  	- XXRMS221_ITEMPACK_YYYYMMDD_HH24MiSS.dat
#  	- nbitemlie.csv.rpl
#
#  Output(s):
#	- assortGroup.dat
#
#  _____________________________________________________________
# | Workfile:      | xxadeo_catmant_merchhier_assortgroup.pl	|
# | Created by:    | PV                                     	|
# | Creation Date: | 20181107-2015                          	|
# | Modify Date:   | 20181212-1400                          	|
# | Version:       | 0.3					|
#  -------------------------------------------------------------
#  History:
#	0.1 - Bug Fixing: BUG_633_634_635 - New Implementation
#	0.2 - Bug Fixing: Change the input file name. According 
#		the input file produce by the ksh parent script.
#	0.3 - Bug Fixing: BUG_739 - Rebuild of script because it 
#		has a performance issue.
#
##################################################################

########	INCUDES/DEFINITIONS		##################
##################################################################

####	Includes	##########################################

use warnings;
use strict;
use Data::Dumper;
#use Shell qw(date);

####	Definitions	##########################################

my $PROGRAM=$ENV{'PROGRAM_NAME'};

# Logger Mode
my $_DEBUG="D";
my $_INFO="I";
my $_ERROR="E";

my $DEBUG_ENABLE=$ENV{'DEBUG_INTG'};

my $INBOUND_DIR_CUST=$ENV{'I_DATA_DIR_XXADEO'};
my $OUTBOUND_DIR_CUST=$ENV{'O_DATA_DIR_XXADEO'};

#my $CUST_FILE=`ls $INBOUND_DIR_CUST | sort -r | grep 'XXRMS221_ITEMPACK_.*\.dat\$' | head -1`;
my $CUST_FILE=`ls $INBOUND_DIR_CUST | sort -r | grep 'XXRMS221_ITEMPACK_.*\.dat.filter\$' | head -1`;
chomp $CUST_FILE;

my $INPUT_FILE_PACKITEM = "$INBOUND_DIR_CUST/$CUST_FILE";
my $INPUT_FILE_RELATEDITEM = "$INBOUND_DIR_CUST/nbitemlie.csv.rpl";
my $OUTPUT_FILE = "$OUTBOUND_DIR_CUST/assortGroup.dat";

# Process Variables
my $AssortGroupPrefix="AG";
my $currentAgNr = 1;		#Start Assortment Group Number 
my %items;			#Hash with Items and your Assortment Group Number
my %groups;			#Hash with Assortment Groups Generated during the process
my $currentFileProcess="";	#Name of Current process file
my $currentFileNrLines=0;	#Number lines of current process file

########	FUNCTIONS			##################
##################################################################

###    function: _logger	        ##########################
#
#       Input: <message>,<mode[D|I]>	# D - Debug | I - Info
#       Output: N/A
#
sub _logger {
	my ($message,$mode) = @_;
	
	my $cdate=`date +"%d/%m/%Y"`;
	chomp $cdate;		# trim/clear the value
	my $chour=`date +"%T"`;
	chomp $chour;		# trim/clear the value
	
	my $now_time="$cdate $chour";
	if($mode eq $_DEBUG && defined $DEBUG_ENABLE && $DEBUG_ENABLE eq "1"){
		print "\n[DEBUG][$now_time] $PROGRAM - $message\n";
	}elsif($mode eq $_ERROR){
		print "\n[ERROR][$now_time] $PROGRAM - $message\n";
	}
	elsif($mode eq $_INFO) {
		print "\n[INFO][$now_time] $PROGRAM - $message\n";
	}
}

###	function: generateAssortmentGroups		##########################
# Generate Assortment Groups for each file as input.
# 	Input: <file_buffer>
# 	Output: <N/A>
#
sub generateAssortmentGroups{

	my ($file_FH) = @_;
	my @values;

	while(<$file_FH>){
		@values = split(',',$_);
		my $key0;			#Group number for Pack or Related
		my $key1;			#Group number for Item of Pack or Item of Related
		
		#Check if Pack or Related Exists on final HashMap
		if(exists $items{$values[0]}){
			$key0 = $items{$values[0]};
			_logger("The $currentFileProcess Item [$values[0]] already have the following Assortment Group= @$key0[0] !",$_DEBUG);
		}
		#Check if Item of Pack or Item of Related Exists on final HashMap
		if(exists $items{$values[1]}){
			$key1 = $items{$values[1]};
			_logger("The Item of $currentFileProcess [$values[1]] already have the following Assortment Group= @$key1[0] !",$_DEBUG);
		}
		#Check if Group Number for Pack Or Related is defined or have some values
		#and if Group Number for Item of Pack or Item of Related is defined or have some value
		if(!defined $key0 && !defined $key1){
			#If current row of file doesn't have group, give the current value and
			#increment to the next assortment group value.
			my @entry = ();			#new array
			$entry[0] = $currentAgNr;	#give the current assortment group

			#Save as a Reference of all Assortment Groups to validate and correct on
			#the final process before produce the output result
			$groups{$currentAgNr}=\@entry;

			$items{$values[0]} = \@entry;		#give a reference to the new assortment group
			$items{$values[1]} = \@entry;		#give a reference to the new assortment group

			_logger("For $currentFileProcess Item [$values[0]] and Item of $currentFileProcess [$values[1]] we gave the following Assortment Group=$currentAgNr !",$_DEBUG);
			
			#increment the assortment group number
			$currentAgNr++;

		}elsif(defined $key0 && !defined $key1){
			$items{$values[1]} = $key0;
		}elsif(!defined $key0 && defined $key1){
			$items{$values[0]} = $key1;
		}else{

			_logger("For $currentFileProcess Item [$values[0]] and Item of $currentFileProcess [$values[1]] have a different Assortment Group. We gave to the Item of $currentFileProcess the Assortment Group of $currentFileProcess !",$_DEBUG);
			
			# Merge between Assortment Groups when are differents between them.
			if(exists $groups{@$key1[0]}){
				delete $groups{@$key1[0]};
			}	
			@$key1[0] = @$key0[0];
		}
		$currentFileNrLines++;		#count number of lines
	}
}

########	MAIN		##################
###################################################################

_logger("Program Start...",$_INFO);

####	Read From Files	##########################################

_logger("Start read Pack Item file to Generate Assortment Groups:\n	$INPUT_FILE_PACKITEM",$_INFO);

$currentFileProcess="Pack";

# Generate Assortment Groups to Pack Item File
open(file_FH, '<', $INPUT_FILE_PACKITEM) or die _logger("The file $INPUT_FILE_PACKITEM does not exist!\n$!",$_ERROR);
generateAssortmentGroups(\*file_FH);
close(file_FH);

_logger("End read Pack Item file to Generate Assortment Groups! The file have $currentFileNrLines lines!",$_INFO);

_logger("Start read Related Item file to Generate Assortment Groups:\n	$INPUT_FILE_RELATEDITEM",$_INFO);

$currentFileProcess="Related";
$currentFileNrLines=0;

# Generate Assortment Groups to Related Item File
open(file_FH, '<', $INPUT_FILE_RELATEDITEM) or die _logger("The file $INPUT_FILE_RELATEDITEM does not exist!\n$!",$_ERROR);
generateAssortmentGroups(\*file_FH);
close(file_FH);

_logger("End read Related Item file to Generate Assortment Groups! The file have $currentFileNrLines lines!",$_INFO);


####	Clean Data	##################################

_logger("Process to clean, validate and organize the Assortment Group Sequential Numbers!",$_INFO);

my $c=1;
for (sort keys %groups) {
	@{$groups{$_}}[0]=$c;
	$c++;
}

####	Output			##################################

_logger("Process to write HashMap on Output File:\n $OUTPUT_FILE!",$_INFO);

open(out_FH,'>',$OUTPUT_FILE) or die _logger("Something wrong with the output file $OUTPUT_FILE ! \n$!",$_ERROR);

my $i=0;
while(my($k, $v) = each %items) {
	print out_FH "$k,$AssortGroupPrefix@$v[0]\n";
	$i++;
}

close(out_FH);

_logger("End Process to write on Output File. The file have $i lines!",$_INFO);

#	Debug Information
if(defined $DEBUG_ENABLE && $DEBUG_ENABLE eq "1"){
	my $dumper = Dumper(\%items);
	_logger("Items= $dumper",$_DEBUG);
	$dumper = Dumper(\%groups);
	_logger("Groups= $dumper",$_DEBUG);
}

my $nextAgNr=$c;
$currentAgNr=$c-1;
_logger("Current Assortment Group Number: $currentAgNr | Next Assortment Group Number: $nextAgNr",$_INFO);

_logger("Program End...",$_INFO);
