#!/bin/ksh

###############################################################################
#                                                                             #
# Object Name:   xxadeo_rmse_simtar_primary_supplier.ksh                      #
# Description:   The xxadeo_rmse_simtar_primary_supplier.ksh program          #
#                extracts the primary suppliers information from RMS table    #
#                and places this data into a flat file                        #
#               (four_princip.csv.ovr) to be loaded to RPAS.                  #
#                                                                             #
# Version:       1.6                                                          #
# Author:        CGI                                                          #
# Creation Date: 11/09/2018                                                   #
# Last Modified: 29/11/2018                                                   #
# History:                                                                    #
#               1.0 - Initial version                                         #
#               1.2 - Update the variables for environment, librairies        #
#                     and directory names. We have to use $RETL_OUT           #
#                     to put output files because this IR is                  #
#                     an extraction (RMS side)                                #
#               1.3 - Update the output file name                             #
#               1.4 - Modify output path, script name and schema name         #
#               1.5 - Add new filters :                                       #
#                     STORE TABLE :                                           #
#                      Only store classes that are defined in the code_detail #
#                      table for the code_type 'CSTR'                         #
#                     ITEM_MASTER :                                           #
#                      Only Approved Items should be sent (Status=A)          #
#                      Only ITEM_LEVEL 1 items should be sent (ITEM_LEVEL = 1)#
#                     Change the extension of the file name from "ovr"        #
#                     to "rpl"                                                #
#                     Remove the SUBSTR in the query, send the ITEM code with #
#                     no modification                                         #
#               1.6 - Rename the output file four_princip.csv.rpl instead of  #
#                     supp_princ_four.csv.ovr                                 #
#                                                                             #
###############################################################################



###############################################################################
####################     (must be the first define)     #######################

export PROGRAM_NAME="xxadeo_rmse_simtar_primary_supplier"

###############################################################################
################# (this section must come after PROGRAM DEFINE) ###############

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

DATE_FILENAME=`date +%Y%m%d-%H%M%S`

###############################################################################
function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        fi
}

###############################################################################
#  Log start message and define the RETL flow file
###############################################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

###############################################################################
################### (this section must come after INCLUDES) ###################

export O_DATA_DIR_XXADEO=${RETL_OUT}/SIMTAR

export OUTPUT_FILE=${O_DATA_DIR_XXADEO}/four_princip.csv.rpl

export SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema

export OUTPUT_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema

###############################################################################
#  Copy the RETL flow to a file to be executed by rfx:
###############################################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read the vat data from the RMS tables:  -->

${DBREAD}
  <PROPERTY name = "query">
    <![CDATA[
     SELECT
         itl.ITEM         ARTICLE
       , itl.LOC          MAGASIN
       , itl.PRIMARY_SUPP FOURNISSEUR
       , 'true'           STATUT_FOURNISSEUR
     FROM
       ${XXADEO_RMS}.ITEM_LOC itl
       , ${XXADEO_RMS}.ITEM_MASTER itm
       , ${XXADEO_RMS}.STORE st
       , ${XXADEO_RMS}.CODE_DETAIL cd
     WHERE
       itl.LOC_TYPE = 'S'
       AND itl.STATUS = 'A'
       AND itl.PRIMARY_SUPP IS NOT NULL
       AND itl.LOC = st.store
       AND itl.item = itm.item
       AND itm.status = 'A'
       AND itm.item_level = 1
       AND st.store_class = cd.code
       AND cd.code_type = 'CSTR'
     ORDER BY
       itl.ITEM
       , itl.LOC
       , itl.PRIMARY_SUPP
         ]]>
      </PROPERTY>
      <OUTPUT   name = "primary_supplier.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
        <INPUT name="primary_supplier.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="ARTICLE" sourcefield="ARTICLE">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="FOURNISSEUR" sourcefield="FOURNISSEUR" newtype="string">
                                <CONVERTFUNCTION name="string_from_int64"/>
                                <TYPEPROPERTY name="nullable" value="false"/>
                        </CONVERT>
                        <CONVERT destfield="STATUT_FOURNISSEUR" sourcefield="StATUT_FOURNISSEUR">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="MAGASIN" sourcefield="MAGASIN" newtype="string">
                                <CONVERTFUNCTION name="string_from_int64"/>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="primary_supplier_1.v"/>
</OPERATOR>


   <!--  Write out the vat data to a flat file:  -->
   <OPERATOR type="export">
      <INPUT    name="primary_supplier_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################################################
#  Execute the RETL flow that was
#  previously copied into rmse_rpas_wh.xml:
###############################################################################

$RFX_EXE  $RFX_OPTIONS  -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?

###############################################################################
#  Log the number of records written to the final output files
#  and add up the total:
###############################################################################

log_num_recs ${OUTPUT_FILE}


###############################################################################
#  Do error checking:
###############################################################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."

#  Cleanup and exit:

check_error_and_clean_before_exit 0 "NO_CHECK"