#!/bin/ksh

#############################################################################
#                                                                           #
# Object Name:   xxadeo_rmse_simtar_stockholding_flag.ksh                   #
# Description:   The xxadeo_rmse_simtar_stockholding_flag.ksh program       #
#                extracts the stockholding flag information from RMS table  #
#                and places this data into a flat file                      #
#               (stockholdingInd.csv.ovr) to be loaded into RPAS.           #
#                                                                           #
# Version:       1.5                                                        #
# Author:        CGI                                                        #
# Creation Date: 12/09/2018                                                 #
# Last Modified: 29/11/2018                                                 #
# History:                                                                  #
#               1.0 - Initial version                                       #
#               1.1 - Update the variables for environment, librairies      #
#                     and directory names. We have to use $RETL_OUT         #
#                     to put output files because this IR is                #
#                     an extraction (RMS side)                              #
#               1.2 - Update output file name                               #
#               1.3 - Update ksh and schema files name                      #
#               1.4 - Add a new RMS table (CODE_DETAIL) and new filters     #
#                       - Only store types F and C                          #
#                       - Only store classes that are defined in the        #
#                         code_detail table for the code_type = CSTR        #
#               1.5 - Update output file name                               #
#                                                                           #
#############################################################################

############################ PROGRAM DEFINE #################################
###################     (must be the first define)     ######################

export PROGRAM_NAME="xxadeo_rmse_simtar_stockholding_flag"

################################# INCLUDES ##################################
############### (this section must come after PROGRAM DEFINE) ###############

. ${RETLforXXADEO}/rfx/etc/xxadeo_rmse_config.env
. ${LIB_DIR}/xxadeo_rmse_lib.ksh

DATE_FILENAME=`date +%Y%m%d-%H%M%S`

#############################################################################
#  FUNCTIONS
#############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${OUTPUT_FILE}"
                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        fi
}

#############################################################################
#  Log start message and define the RETL flow file
#############################################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

############################  OUTPUT DEFINES ################################
################## (this section must come after INCLUDES) ##################

export O_DATA_DIR_XXADEO="${RETL_OUT}/SIMTAR"

export OUTPUT_FILE=${O_DATA_DIR_XXADEO}/stock_holding.csv.rpl

export SCHEMA_DIR_XXADEO=${RETLforXXADEO}/rfx/schema

export OUTPUT_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema


#############################################################################
#  Copy the RETL flow to a file to be executed by rfx:
#############################################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read the vat data from the RMS tables:  -->

${DBREAD}
  <PROPERTY name = "query">
    <![CDATA[
     SELECT
         st.STORE STORE_ID
       , REPLACE(REPLACE(st.STOCKHOLDING_IND, 'Y', 'true'),'N','false') STOCKHOLDING_FLAG
     FROM
        ${XXADEO_RMS}.STORE st
        , ${XXADEO_RMS}.CODE_DETAIL cd
     WHERE
       st.store_type in ('C','F')
       AND st.store_class = cd.code
       AND cd.code_type = 'CSTR'
     ORDER BY
       STORE_ID
         ]]>
      </PROPERTY>
      <OUTPUT   name = "stockholding_flag.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
        <INPUT name="stockholding_flag.v"/>
                <PROPERTY name="convertspec">
                <![CDATA[
                <CONVERTSPECS>
                        <CONVERT destfield="STOCKHOLDING_FLAG" sourcefield="STOCKHOLDING_FLAG">
                                <CONVERTFUNCTION name="make_not_nullable">
                                        <FUNCTIONARG name="nullvalue" value="-1"/>
                                </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="STORE_ID" sourcefield="STORE_ID" newtype="string">
                                <CONVERTFUNCTION name="string_from_int64"/>
                                <TYPEPROPERTY name="nullable" value="false"/>
                        </CONVERT>
                </CONVERTSPECS>
                ]]>
                </PROPERTY>
        <OUTPUT name="stockholding_flag_1.v"/>
</OPERATOR>


   <!--  Write out the vat data to a flat file:  -->
   <OPERATOR type="export">
      <INPUT    name="stockholding_flag_1.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

#############################################################################
#  Execute the RETL flow that was
#  previously copied into rmse_rpas_wh.xml:
#############################################################################

$RFX_EXE  $RFX_OPTIONS  -f $RETL_FLOW_FILE
check_error_and_clean_before_exit $?

#############################################################################
#  Log the number of records written to the final output files
#  and add up the total:
#############################################################################

log_num_recs ${OUTPUT_FILE}


#############################################################################
#  Do error checking on results of the RETL execution:
#############################################################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#############################################################################
#  Remove the status file, log the completion message
#  to the log and error files and clean up the files:
#############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."

#  Cleanup and exit:

check_error_and_clean_before_exit 0 "NO_CHECK"