#!/bin/ksh

###############################################################################
#                                                                             #
# Object Name:   xxadeo_catmant_traduction_cfa_uda.ksh                        #
# Description:   The xxadeo_catmant_traduction_cfa_uda.ksh program            #
#                extract (XXRMS220_ATTR_TL_YYYYMMDD-HH24MISS.dat)             #
#                and generate the flat file r_measpicklist.csv.ovr            #
#                                                                             #
# Version:       1.7                                                          #
# Author:        CGI                                                          #
# Creation Date: 30/07/2018                                                   #
# Last Modified: 22/01/2019                                                   #
# History:                                                                    #
#               1.0 - Initial version                                         #
#               1.1 - Update the variables for environment, librairies        #
#                     and directory names. We have to use $RETL_IN            #
#                     to put output files because this IR is                  #
#                     a treansformation (RPAS side)                           #
#               1.2 - update program name                                     #
#               1.3 - Deleting variable of input, output and schema           #
#               1.4 - Add UDA and CFA filter on input file as expected on     #
#                     CV027                                                   #
#               1.5 - Missing the mapping for UDAs 250, 255 and 110           #
#                     The translation is losing the language                  #
#               1.6 - Take into account the new logic sent by Renato to       #
#                     generate the output file                                #
#               1.7 - Change measure's name ComTyArtcUomPt to ComTyArtcUomTx  #
#                                                                             #
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################

PROGRAM_NAME='xxadeo_catmant_traduction_cfa_uda'


RFX_EXE=${RFX_EXE:=rfx}

###############################################################################
#  INCLUDES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh


###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################

I_ATTR_TL_PATTERN=XXRMS220_ATTR_TL_*.dat

TEMP_FILE=$O_DATA_DIR_XXADEO/temp_file.csv.ovr
RETL_OUTPUT_FILE1=$O_DATA_DIR_XXADEO/r_measpicklist.csv.ovr

touch $RETL_OUTPUT_FILE1


###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
  error_check=1
  if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
  then
    message "ERROR: Parameter 2 is not valid: ${2}"
    message "    Parameter 2 must be true, false or empty (true)"
    rmse_terminate 1
  elif [[ "${2}" = "NO_CHECK" ]]
  then
    error_check=0
  fi
  #message "check_error_and_clean_before_exit: error_check: ${error_check}"

  if [[ $1 -ne 0 && error_check -eq 1 ]]
  then
    rm -f "${I_ATTR_TL_FILE}.retl"
    rm -f "${RETL_OUTPUT_FILE1}.retl"
    rm -f "${RETL_OUTPUT_FILE1}"
    rm -f "${TEMP_FILE}.retl"
    rm -f "${TEMP_FILE}"
    checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
  elif [[ error_check -eq 0 ]]
  then
    rm -f "${I_ATTR_TL_FILE}.retl"
    rm -f "${RETL_OUTPUT_FILE1}.retl"
    rm -f "${TEMP_FILE}.retl"
    rm -f "${TEMP_FILE}"
    checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
    rmse_terminate $1
  fi
}


###############################################################################
#  Get latest available input files, because since this interface works in a
#  FULL mode there is no need to process all available input files.
###############################################################################

I_ATTR_TL_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ATTR_TL_PATTERN} | sort | tail -n 1`

if [[ ! -f $I_ATTR_TL_FILE ]];  then
    message "XXRMS220_ATTR_TL_YYYYMMDD-HHMMSS.dat does not exist"
    exit 1
fi


###############################################################################
#  RETL input/output schemas
###############################################################################

I_ATTR_TL_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_attr_tl.schema

O_SCHEMA1=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema

###############################################################################
#  Convert input data files in RETLSV format
###############################################################################

csv_converter "csv-retlsv" "${I_ATTR_TL_FILE}" "${I_ATTR_TL_FILE}.retl"
check_error_and_clean_before_exit $?


###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

  <OPERATOR  type="import">
    <PROPERTY  name="inputfile" value="${I_ATTR_TL_FILE}.retl"/>
    <PROPERTY  name="schemafile" value="${I_ATTR_TL_SCHEMA}"/>
    <OUTPUT name="i_attr.v"/>
  </OPERATOR>

  <OPERATOR type="filter">
    <INPUT name="i_attr.v"/>
    <PROPERTY name="filter" value="(ATTR_TYPE EQ 'CFA' AND (ATTR_ID EQ '114' OR ATTR_ID EQ '154' OR ATTR_ID EQ '274' OR ATTR_ID EQ '314' OR ATTR_ID EQ '234' OR ATTR_ID EQ '194' OR ATTR_ID EQ '522')) OR
                                   (ATTR_TYPE EQ 'UDA' AND (ATTR_ID EQ '265' OR ATTR_ID EQ '270' OR ATTR_ID EQ '275' OR ATTR_ID EQ '290' OR ATTR_ID EQ '285' OR ATTR_ID EQ '280' OR ATTR_ID EQ '100' OR ATTR_ID EQ '250' OR ATTR_ID EQ '255' OR ATTR_ID EQ '110'))"/>
    <PROPERTY name="rejects" value="false"/>
    <OUTPUT name="i_attr_filter.v"/>
  </OPERATOR>

  <OPERATOR type="generator">
    <INPUT name="i_attr_filter.v"/>
    <PROPERTY  name="schema"> <![CDATA[
      <GENERATE>
        <FIELD name="MEASURE_NAME" type="string" maxlength="25" nullable="false">
          <CONST value=""/>
        </FIELD>
      </GENERATE> ]]>
    </PROPERTY>
    <OUTPUT name="i_attr_3.v"/>
  </OPERATOR>

  <OPERATOR type="generator">
    <INPUT name="i_attr_3.v"/>
    <PROPERTY  name="schema"> <![CDATA[
      <GENERATE>
        <FIELD name="RANGE" type="string" maxlength="520" nullable="false">
          <CONST value=""/>
        </FIELD>
      </GENERATE> ]]>
    </PROPERTY>
    <OUTPUT name="i_attr_4.v"/>
  </OPERATOR>

  <OPERATOR type="parser">
    <INPUT name="i_attr_4.v"/>
    <PROPERTY name="expression">
      <![CDATA[
        if (RECORD.ATTR_TYPE == "CFA"  )
        {
          if (RECORD.ATTR_ID == "114" || RECORD.ATTR_ID == "154" || RECORD.ATTR_ID == "274" || RECORD.ATTR_ID == "314" || RECORD.ATTR_ID == "234" || RECORD.ATTR_ID == "194")
          {
            RECORD.MEASURE_NAME = "ComTyBuPrcPolPt";
          }
        }

        if (RECORD.ATTR_TYPE == "UDA"  )
        {
          if (RECORD.ATTR_ID == "265" || RECORD.ATTR_ID == "270" || RECORD.ATTR_ID == "275" || RECORD.ATTR_ID == "290" || RECORD.ATTR_ID == "285" || RECORD.ATTR_ID == "280")
          {
            RECORD.MEASURE_NAME = "ComTyBuLfCyclPt";
          }
        }

        if (RECORD.ATTR_TYPE == "CFA" && RECORD.ATTR_ID == "522" )
        {
          RECORD.MEASURE_NAME = "ComTyArtcUomTx";
        }

        if (RECORD.ATTR_TYPE == "UDA" && RECORD.ATTR_ID == "100" )
        {
          RECORD.MEASURE_NAME = "ComTyBrandTx";
        }

        if (RECORD.ATTR_TYPE == "UDA" && RECORD.ATTR_ID == "250" )
        {
          RECORD.MEASURE_NAME = "ComTyTypoStepTx";
        }

        if (RECORD.ATTR_TYPE == "UDA" && RECORD.ATTR_ID == "255" )
        {
          RECORD.MEASURE_NAME = "ComTySubTypoStepTx";
        }

        if (RECORD.ATTR_TYPE == "UDA" && RECORD.ATTR_ID == "110" )
        {
          RECORD.MEASURE_NAME = "ComTyArtcSeriesTx";
        }
      ]]>
    </PROPERTY>
    <OUTPUT name="i_attr_5.v"/>
  </OPERATOR>

  <OPERATOR type="parser">
    <INPUT name="i_attr_5.v"/>
    <PROPERTY name="expression">
      <![CDATA[
        RECORD.RANGE += RECORD.ATTR_VALUE;
        RECORD.RANGE += "(";
        RECORD.RANGE += RECORD.ATTR_VALUE_DESC;
        RECORD.RANGE += ")";
      ]]>
    </PROPERTY>
    <OUTPUT name="i_attr_6.v"/>
  </OPERATOR>

  <OPERATOR type="fieldmod">
    <INPUT name="i_attr_6.v" />
    <PROPERTY name="keep" value="MEASURE_NAME LANG_NAME RANGE"/>
    <PROPERTY name="rename" value="LANGUAGE=LANG_NAME"/>
    <OUTPUT name="i_attr_7.v"/>
  </OPERATOR>

  <OPERATOR type="sort">
    <INPUT name="i_attr_7.v" />
    <PROPERTY name="key" value="MEASURE_NAME LANGUAGE RANGE"/>
    <PROPERTY name="removedup" value="true" />
    <OUTPUT name="i_attr_8.v" />
  </OPERATOR>

  <OPERATOR type="export">
    <INPUT name="i_attr_8.v"/>
    <PROPERTY name="schemafile" value="${O_SCHEMA1}"/>
    <PROPERTY name="outputfile" value="${TEMP_FILE}.retl"/>
  </OPERATOR>

</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${TEMP_FILE}.retl" "${TEMP_FILE}"
check_error_and_clean_before_exit $?


###############################################################################
#  concatenation RANGE
###############################################################################

measure_ref=''
lang_ref=''
range=''
line_nb=1
nb_line_tot=$(awk 'END {print NR}' "${TEMP_FILE}")
> ${RETL_OUTPUT_FILE1}

while read ligne
do
  measure_cur=`echo $ligne | cut -d, -f1`
  lang_cur=`echo $ligne | cut -d, -f2`
  range=`echo $ligne | cut -d, -f3`

  if [[ $measure_ref == $measure_cur && $lang_ref == $lang_cur ]];
  then
  {
    param="${param},${range}"
  }
  else
  {
    if [[ $line_nb -eq 1 ]]; then
       param="${range}"
    fi	
    if [[ $line_nb -gt 1 ]]; then
      echo -e "$measure_ref,$lang_ref,$param" >> ${RETL_OUTPUT_FILE1}
      param="${range}"
    fi
  }
  fi
  
  if [[ $nb_line_tot -eq $line_nb ]]; then
    echo -e "$measure_cur,$lang_cur,$param" >> ${RETL_OUTPUT_FILE1}
  fi

  measure_ref=$measure_cur
  lang_ref=$lang_cur
  (( line_nb++ ))
done < ${TEMP_FILE}


###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${RETL_OUTPUT_FILE1}


###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"
