#!/bin/ksh

###############################################################################
#                                                                             #
#  Object Name:   xxadeo_catmant_traductions_merch.ksh                        #
#  Description:   The xxadeo_catmant_traductions_merch.ksh program            #
#                 extracts merchandise translation information from RMS       #
#                 input files and generates output flat files for to be       #
#                 loaded to CATMAN                                            #
#                                                                             #
#  Version:       2.0                                                         #
#  Author:        CGI                                                         #
#  Creation Date: 06/06/2018                                                  #
#  Last Modified: 05/11/2018                                                  #
#  History:                                                                   #
#                 1.0 - Initial version                                       #
#                 1.1 - Review Global variable                                #
#                 1.2 - Updating of the global variable : environment,        #
#                       for custom/standard files/schemas.                    #
#                       Updating RETL script                                  #
#                 1.3 - Update program name                                   #
#                 1.4 - Deleting variable I_DATA_DIR, I_DATA_DIR_XXADEO and   #
#                       O_DATA_DIR_XXADEO                                     #
#                 1.5 - modify the input/output path and program_name         #
#                 1.6 - modify output r_sdeptlabel.csv.ovr in                 #
#                       r_sdeplabel.csv.ovr                                   # 
#                 2.0 - mdification of mapping rule                           #
#                                                                             #
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################
PROGRAM_NAME='xxadeo_catmant_traductions_merch'

###############################################################################
#  INCLUDES
###############################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh

DATE_FILENAME=`date +%Y%m%d-%H%M%S`


###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################

export I_COMPHEAD_PATTERN="XXRMS216_COMPHEADTL_*.dat"
export I_DIVISION_PATTERN="XXRMS216_DIVISIONTL_*.dat"
export I_GROUPS_PATTERN="XXRMS216_GROUPSTL_*.dat"
export I_DEPS_PATTERN="XXRMS216_DEPSTL_*.dat"
export I_CLASS_PATTERN="XXRMS216_CLASSTL_*.dat"
export I_SUBCLASS_PATTERN="XXRMS216_SUBCLASSTL_*.dat"
export I_ITEMMASTERTL_PATTERN="XXRMS219_ITEMMASTERTL_*.dat"
export I_ITEMMASTER_PATTERN="rmse_rpas_item_master*.dat"

export I_COMPHEAD_FILE=`find $I_DATA_DIR_XXADEO -name ${I_COMPHEAD_PATTERN} | sort | tail -n 1`
export I_DIVISION_FILE=`find $I_DATA_DIR_XXADEO -name ${I_DIVISION_PATTERN} | sort | tail -n 1`
export I_GROUPS_FILE=`find $I_DATA_DIR_XXADEO -name ${I_GROUPS_PATTERN} | sort | tail -n 1`
export I_DEPS_FILE=`find $I_DATA_DIR_XXADEO -name ${I_DEPS_PATTERN} | sort | tail -n 1`
export I_CLASS_FILE=`find $I_DATA_DIR_XXADEO -name ${I_CLASS_PATTERN} | sort | tail -n 1`
export I_SUBCLASS_FILE=`find $I_DATA_DIR_XXADEO -name ${I_SUBCLASS_PATTERN} | sort | tail -n 1`
export I_ITEMMASTERTL_FILE=`find $I_DATA_DIR_XXADEO -name ${I_ITEMMASTERTL_PATTERN} | sort | tail -n 1`
export I_ITEMMASTER_FILE=`find $I_DATA_DIR -name ${I_ITEMMASTER_PATTERN} | sort | tail -n 1`


export RETL_OUTPUT_FILE1=$O_DATA_DIR_XXADEO/r_artclabel.csv.ovr
export RETL_OUTPUT_FILE2=$O_DATA_DIR_XXADEO/r_artrlabel.csv.ovr
export RETL_OUTPUT_FILE3=$O_DATA_DIR_XXADEO/r_prntlabel.csv.ovr
export RETL_OUTPUT_FILE4=$O_DATA_DIR_XXADEO/r_prnrlabel.csv.ovr
export RETL_OUTPUT_FILE5=$O_DATA_DIR_XXADEO/r_gprnlabel.csv.ovr
export RETL_OUTPUT_FILE6=$O_DATA_DIR_XXADEO/r_gprrlabel.csv.ovr
export RETL_OUTPUT_FILE7=$O_DATA_DIR_XXADEO/r_styplabel.csv.ovr
export RETL_OUTPUT_FILE8=$O_DATA_DIR_XXADEO/r_styrlabel.csv.ovr
export RETL_OUTPUT_FILE9=$O_DATA_DIR_XXADEO/r_typelabel.csv.ovr
export RETL_OUTPUT_FILE10=$O_DATA_DIR_XXADEO/r_typrlabel.csv.ovr
export RETL_OUTPUT_FILE11=$O_DATA_DIR_XXADEO/r_sdeplabel.csv.ovr
export RETL_OUTPUT_FILE12=$O_DATA_DIR_XXADEO/r_sderlabel.csv.ovr
export RETL_OUTPUT_FILE13=$O_DATA_DIR_XXADEO/r_dep1label.csv.ovr
export RETL_OUTPUT_FILE14=$O_DATA_DIR_XXADEO/r_der1label.csv.ovr
export RETL_OUTPUT_FILE15=$O_DATA_DIR_XXADEO/r_dep2label.csv.ovr
export RETL_OUTPUT_FILE16=$O_DATA_DIR_XXADEO/r_der2label.csv.ovr
export RETL_OUTPUT_FILE17=$O_DATA_DIR_XXADEO/r_complabel.csv.ovr
export RETL_OUTPUT_FILE18=$O_DATA_DIR_XXADEO/r_comrlabel.csv.ovr




###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${I_ITEMMASTERTL_FILE}.retl"
                rm -f "${I_ITEMMASTER_FILE}.retl"
                rm -f "${I_COMPHEAD_FILE}.retl"
                rm -f "${I_DIVISION_FILE}.retl"
                rm -f "${I_GROUPS_FILE}.retl"
                rm -f "${I_DEPS_FILE}.retl"
                rm -f "${I_CLASS_FILE}.retl"
                rm -f "${I_SUBCLASS_FILE}.retl"

                rm -f "${RETL_OUTPUT_FILE1}.retl"
                rm -f "${RETL_OUTPUT_FILE1}"
                rm -f "${RETL_OUTPUT_FILE2}"
                rm -f "${RETL_OUTPUT_FILE3}.retl"
                rm -f "${RETL_OUTPUT_FILE3}"
                rm -f "${RETL_OUTPUT_FILE4}"
                rm -f "${RETL_OUTPUT_FILE5}.retl"
                rm -f "${RETL_OUTPUT_FILE5}"
                rm -f "${RETL_OUTPUT_FILE6}"
                rm -f "${RETL_OUTPUT_FILE7}.retl"
                rm -f "${RETL_OUTPUT_FILE7}"
                rm -f "${RETL_OUTPUT_FILE8}"
                rm -f "${RETL_OUTPUT_FILE9}.retl"
                rm -f "${RETL_OUTPUT_FILE9}"
                rm -f "${RETL_OUTPUT_FILE10}"
                rm -f "${RETL_OUTPUT_FILE11}.retl"
                rm -f "${RETL_OUTPUT_FILE11}"
                rm -f "${RETL_OUTPUT_FILE12}"
                rm -f "${RETL_OUTPUT_FILE13}.retl"
                rm -f "${RETL_OUTPUT_FILE13}"
                rm -f "${RETL_OUTPUT_FILE14}"
                rm -f "${RETL_OUTPUT_FILE15}.retl"
                rm -f "${RETL_OUTPUT_FILE15}"
                rm -f "${RETL_OUTPUT_FILE16}"
                rm -f "${RETL_OUTPUT_FILE17}.retl"
                rm -f "${RETL_OUTPUT_FILE17}"
                rm -f "${RETL_OUTPUT_FILE18}"

                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${I_ITEMMASTER_FILE}.retl"
                rm -f "${I_ITEMMASTERTL_FILE}.retl"
                rm -f "${I_COMPHEAD_FILE}.retl"
                rm -f "${I_DIVISION_FILE}.retl"
                rm -f "${I_GROUPS_FILE}.retl"
                rm -f "${I_DEPS_FILE}.retl"
                rm -f "${I_CLASS_FILE}.retl"
                rm -f "${I_SUBCLASS_FILE}.retl"


                rm -f "${RETL_OUTPUT_FILE1}.retl"
                rm -f "${RETL_OUTPUT_FILE3}.retl"
                rm -f "${RETL_OUTPUT_FILE5}.retl"
                rm -f "${RETL_OUTPUT_FILE7}.retl"
                rm -f "${RETL_OUTPUT_FILE9}.retl"
                rm -f "${RETL_OUTPUT_FILE11}.retl"
                rm -f "${RETL_OUTPUT_FILE13}.retl"
                rm -f "${RETL_OUTPUT_FILE15}.retl"
                rm -f "${RETL_OUTPUT_FILE17}.retl"


                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}

###############################################################################
#  Check if input files are available. If one file is missing the script
#  ends in error
###############################################################################


if [[ ! -f $I_COMPHEAD_FILE ]];  then
    message " Input file COMPHEAD TL does not exist"
    exit 1
fi

if [[  ! -f $I_DIVISION_FILE ]];  then
    message " Input file DIVISION TL  does not exist"
    exit 1
fi

if [[  ! -f $I_GROUPS_FILE ]];  then
    message " Input file GROUPS_TL  does not exist"
    exit 1
fi

if [[ ! -f $I_DEPS_FILE ]];  then
    message " Input file DEPS TL does not exist"
    exit 1
fi

if [[  ! -f $I_CLASS_FILE ]];  then
    message " Input file CLASS TL does not exist"
    exit 1
fi

if [[  ! -f $I_SUBCLASS_FILE ]];  then
    message " Input file SUBCLASS TL does not exist"
    exit 1
fi
if [[ ! -f $I_ITEMMASTERTL_FILE ]];  then
    message " Input file ITEMMASTER TL does not exist"
    exit 1
fi

if [[  ! -f $I_ITEMMASTER_FILE ]];  then
    message "  Input file ITEMMASTER does not exist"
    exit 1
fi


###############################################################################
#  RETL input/output schemas
###############################################################################

I_COMPHEAD_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_merchhiertl_comphead.schema
I_DIVISION_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_merchhiertl_division.schema
I_GROUPS_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_merchhiertl_groups.schema
I_DEPS_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_merchhiertl_deps.schema
I_CLASS_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_merchhiertl_class.schema
I_SUBCLASS_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_merchhiertl_subclass.schema
I_ITEMMASTERTL_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_itemtranslation.schema
I_ITEMMASTER_SCHEMA=${SCHEMA_DIR}/rmse_rpas_item_master.schema

O_SCHEMA=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema


###############################################################################
#  Convert input data files in RETLSV format
###############################################################################


csv_converter "csv-retlsv" "${I_ITEMMASTERTL_FILE}" "${I_ITEMMASTERTL_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_ITEMMASTER_FILE}" "${I_ITEMMASTER_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_DIVISION_FILE}" "${I_DIVISION_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_GROUPS_FILE}" "${I_GROUPS_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_DEPS_FILE}" "${I_DEPS_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_CLASS_FILE}" "${I_CLASS_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_SUBCLASS_FILE}" "${I_SUBCLASS_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_COMPHEAD_FILE}" "${I_COMPHEAD_FILE}.retl"
check_error_and_clean_before_exit $?


###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_COMPHEAD_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${I_COMPHEAD_SCHEMA}"/>
                <OUTPUT name="i_comphead.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="i_comphead.v" />
                <PROPERTY name="keep" value="RPAS_COMPANY_NAME LANG_NAME RPAS_COMPANY_ID"/>
                <PROPERTY name="rename" value="ID=RPAS_COMPANY_ID"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_COMPANY_NAME"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <OUTPUT name="i_comphead_2.v"/>
        </OPERATOR>

        <OPERATOR type="convert">
                <INPUT name="i_comphead_2.v"/>
                  <PROPERTY name="convertspec">
                  <![CDATA[
                    <CONVERTSPECS>
                      <CONVERT destfield="ID" sourcefield="ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                    </CONVERTSPECS>
                  ]]>
                  </PROPERTY>
                <OUTPUT name="i_comphead_5.v"/>
        </OPERATOR>

        <OPERATOR type="export">
            <INPUT name="i_comphead_5.v"/>
            <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
            <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE17}.retl"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_DIVISION_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${I_DIVISION_SCHEMA}"/>
                <OUTPUT name="i_div.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="i_div.v" />
                <PROPERTY name="keep" value="RPAS_DIVISION_ID RPAS_DIV_NAME LANG_NAME"/>
                <PROPERTY name="rename" value="ID=RPAS_DIVISION_ID"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_DIV_NAME"/>
                <OUTPUT name="i_div_2.v"/>
        </OPERATOR>

        <OPERATOR type="convert">
                <INPUT name="i_div_2.v"/>
                  <PROPERTY name="convertspec">
                  <![CDATA[
                    <CONVERTSPECS>
                      <CONVERT destfield="ID" sourcefield="ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                    </CONVERTSPECS>
                  ]]>
                  </PROPERTY>
                <OUTPUT name="i_div_5.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="i_div_5.v"/>
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE15}.retl"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_GROUPS_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${I_GROUPS_SCHEMA}"/>
                <OUTPUT name="i_groups.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="i_groups.v" />
                <PROPERTY name="keep" value="RPAS_GROUP_NO LANG_NAME RPAS_GROUP_NAME"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <PROPERTY name="rename" value="ID=RPAS_GROUP_NO"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_GROUP_NAME"/>
                <OUTPUT name="i_groups_2.v"/>
        </OPERATOR>

        <OPERATOR type="convert">
                <INPUT name="i_groups_2.v"/>
                  <PROPERTY name="convertspec">
                  <![CDATA[
                    <CONVERTSPECS>
                      <CONVERT destfield="ID" sourcefield="ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                    </CONVERTSPECS>
                  ]]>
                  </PROPERTY>
                <OUTPUT name="i_groups_5.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="i_groups_5.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE13}.retl"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_DEPS_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${I_DEPS_SCHEMA}"/>
                <OUTPUT name="i_deps.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="i_deps.v" />
                <PROPERTY name="keep" value="RPAS_DEPT_ID LANG_NAME RPAS_DEPT_NAME"/>
                <PROPERTY name="rename" value="ID=RPAS_DEPT_ID"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_DEPT_NAME"/>
                <OUTPUT name="i_deps_2.v"/>
        </OPERATOR>

        <OPERATOR type="convert">
                <INPUT name="i_deps_2.v"/>
                  <PROPERTY name="convertspec">
                  <![CDATA[
                    <CONVERTSPECS>
                      <CONVERT destfield="ID" sourcefield="ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                    </CONVERTSPECS>
                  ]]>
                  </PROPERTY>
                <OUTPUT name="i_deps_5.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="i_deps_5.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE11}.retl"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_CLASS_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${I_CLASS_SCHEMA}"/>
                <OUTPUT name="i_class.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="i_class.v" />
                <PROPERTY name="keep" value="RPAS_CLASS_ID LANG_NAME RPAS_CLASS_NAME"/>
                <PROPERTY name="rename" value="ID=RPAS_CLASS_ID"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_CLASS_NAME"/>
                <OUTPUT name="i_class_2.v"/>
        </OPERATOR>

        <OPERATOR type="convert">
                <INPUT name="i_class_2.v"/>
                  <PROPERTY name="convertspec">
                  <![CDATA[
                    <CONVERTSPECS>
                      <CONVERT destfield="ID" sourcefield="ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                    </CONVERTSPECS>
                  ]]>
                  </PROPERTY>
                <OUTPUT name="i_class_5.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="i_class_5.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE9}.retl"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_SUBCLASS_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${I_SUBCLASS_SCHEMA}"/>
                <OUTPUT name="i_subclass.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="i_subclass.v" />
                <PROPERTY name="keep" value="RPAS_SUBCLASS_ID LANG_NAME RPAS_SUBCLASS_NAME"/>
                <PROPERTY name="rename" value="ID=RPAS_SUBCLASS_ID"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=RPAS_SUBCLASS_NAME"/>
                <OUTPUT name="i_subclass_2.v"/>
        </OPERATOR>

        <OPERATOR type="convert">
                <INPUT name="i_subclass_2.v"/>
                  <PROPERTY name="convertspec">
                  <![CDATA[
                    <CONVERTSPECS>
                      <CONVERT destfield="ID" sourcefield="ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LABEL_TRANSLATED" sourcefield="LABEL_TRANSLATED">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                      <CONVERT destfield="LANG_ID" sourcefield="LANG_ID">
                       <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="-1"/>
                       </CONVERTFUNCTION>
                      </CONVERT>
                    </CONVERTSPECS>
                  ]]>
                  </PROPERTY>
                <OUTPUT name="i_subclass_5.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="i_subclass_5.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE7}.retl"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMMASTERTL_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${I_ITEMMASTERTL_SCHEMA}"/>
                <OUTPUT name="i_itemmastertl.v"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_ITEMMASTER_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${I_ITEMMASTER_SCHEMA}"/>
                <OUTPUT name="i_itemmaster.v"/>
        </OPERATOR>

        <OPERATOR type="sort">
                <PROPERTY name="key" value="ITEM"/>
                <INPUT name="i_itemmastertl.v"/>
                <OUTPUT name="i_itemmastertl_3_sorted.v"/>
        </OPERATOR>

        <OPERATOR type="sort">
                <PROPERTY name="key" value="ITEM"/>
                <INPUT name="i_itemmaster.v"/>
                <OUTPUT name="i_itemmaster_2_sorted.v"/>
        </OPERATOR>

        <OPERATOR type="innerjoin" name="innerjoin,0">
                <INPUT name="i_itemmastertl_3_sorted.v"/>
                <INPUT name="i_itemmaster_2_sorted.v"/>
                <PROPERTY name="key" value="ITEM"/>
                <OUTPUT name="innerjoin_itemmastertl_copy1_sorted.v"/>
        </OPERATOR>

        <OPERATOR type="copy">
                <INPUT name="innerjoin_itemmastertl_copy1_sorted.v"/>
                <OUTPUT name="item.v"/>
                <OUTPUT name="parent.v"/>
                <OUTPUT name="grandparent.v"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="parent.v"/>
                        <PROPERTY name="expression">
                                <![CDATA[
                                if (RECORD.ITEM_PARENT != "")
                                {
                                        RECORD.ITEM = RECORD.ITEM_PARENT;
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="parent_2.v"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="grandparent.v"/>
                        <PROPERTY name="expression">
                                <![CDATA[
                                if (RECORD.ITEM_PARENT != "")
                                {
                                        RECORD.ITEM = RECORD.ITEM_PARENT;
                                }

                                if (RECORD.ITEM_GRANDPARENT != "")
                                {
                                        RECORD.ITEM = RECORD.ITEM_GRANDPARENT ;
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="grandparent_2.v"/>
        </OPERATOR>

        <OPERATOR type="filter">
                <INPUT name="parent_2.v"/>
                <PROPERTY name="filter" value="ITEM_LEVEL EQ 1 "/>
                <OUTPUT name="parent_3.v"/>
        </OPERATOR>

        <OPERATOR type="filter">
                <INPUT name="grandparent_2.v"/>
                <PROPERTY name="filter" value="ITEM_LEVEL EQ 1 "/>
                <OUTPUT name="grandparent_3.v"/>
        </OPERATOR>

        <OPERATOR type="filter">
                <INPUT name="item.v"/>
                <PROPERTY name="filter" value="ITEM_LEVEL EQ TRAN_LEVEL "/>
                <OUTPUT name="item_2.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="parent_3.v" />
                <PROPERTY name="keep" value="ITEM LANG_NAME ITEM_DESC"/>
                <PROPERTY name="rename" value="ID=ITEM"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=ITEM_DESC"/>
                <OUTPUT name="parent_4.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="grandparent_3.v" />
                <PROPERTY name="keep" value="ITEM LANG_NAME ITEM_DESC"/>
                <PROPERTY name="rename" value="ID=ITEM"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=ITEM_DESC"/>
                <OUTPUT name="grandparent_4.v"/>
        </OPERATOR>

        <OPERATOR type="fieldmod">
                <INPUT name="item_2.v" />
                <PROPERTY name="keep" value="ITEM LANG_NAME ITEM_DESC"/>
                <PROPERTY name="rename" value="ID=ITEM"/>
                <PROPERTY name="rename" value="LANG_ID=LANG_NAME"/>
                <PROPERTY name="rename" value="LABEL_TRANSLATED=ITEM_DESC"/>
                <OUTPUT name="item_3.v"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="item_3.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE1}.retl"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="parent_4.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE3}.retl"/>
        </OPERATOR>

        <OPERATOR type="export">
                <INPUT name="grandparent_4.v" />
                <PROPERTY name="schemafile" value="${O_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE5}.retl"/>
        </OPERATOR>

</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################
csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE1}.retl" "${RETL_OUTPUT_FILE1}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE3}.retl" "${RETL_OUTPUT_FILE3}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE5}.retl" "${RETL_OUTPUT_FILE5}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE7}.retl" "${RETL_OUTPUT_FILE7}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE9}.retl" "${RETL_OUTPUT_FILE9}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE11}.retl" "${RETL_OUTPUT_FILE11}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE13}.retl" "${RETL_OUTPUT_FILE13}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE15}.retl" "${RETL_OUTPUT_FILE15}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE17}.retl" "${RETL_OUTPUT_FILE17}"
check_error_and_clean_before_exit $?


###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################
log_num_recs ${RETL_OUTPUT_FILE1}
log_num_recs ${RETL_OUTPUT_FILE3}
log_num_recs ${RETL_OUTPUT_FILE5}
log_num_recs ${RETL_OUTPUT_FILE7}
log_num_recs ${RETL_OUTPUT_FILE9}
log_num_recs ${RETL_OUTPUT_FILE11}
log_num_recs ${RETL_OUTPUT_FILE13}
log_num_recs ${RETL_OUTPUT_FILE15}
log_num_recs ${RETL_OUTPUT_FILE17}

if [[ -f $RETL_OUTPUT_FILE1 ]]; then
        cp $RETL_OUTPUT_FILE1 $RETL_OUTPUT_FILE2
fi
if [[ -f $RETL_OUTPUT_FILE3 ]]; then
        cp $RETL_OUTPUT_FILE3 $RETL_OUTPUT_FILE4
fi
if [[ -f $RETL_OUTPUT_FILE5 ]]; then
        cp $RETL_OUTPUT_FILE5 $RETL_OUTPUT_FILE6
fi
if [[ -f $RETL_OUTPUT_FILE7 ]]; then
        cp $RETL_OUTPUT_FILE7 $RETL_OUTPUT_FILE8
fi
if [[ -f $RETL_OUTPUT_FILE9 ]]; then
        cp $RETL_OUTPUT_FILE9 $RETL_OUTPUT_FILE10
fi
if [[ -f $RETL_OUTPUT_FILE11 ]]; then
        cp $RETL_OUTPUT_FILE11 $RETL_OUTPUT_FILE12
fi
if [[ -f $RETL_OUTPUT_FILE13 ]]; then
        cp $RETL_OUTPUT_FILE13 $RETL_OUTPUT_FILE14
fi
if [[ -f $RETL_OUTPUT_FILE15 ]]; then
        cp $RETL_OUTPUT_FILE15 $RETL_OUTPUT_FILE16
fi
if [[ -f $RETL_OUTPUT_FILE17 ]]; then
        cp $RETL_OUTPUT_FILE17 $RETL_OUTPUT_FILE18
fi

###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"
