#!/bin/ksh

###############################################################################
#                                                                             #
# Object Name:   xxadeo_catmant_traductions_orghier.ksh                       #
# Description:   The xxadeo_catmant_traductions_orghier.ksh program           #
#                receives and process the Hierarchy Organizational            #
#                translations Data sent by RMS.                               #
#                                                                             #
# Version:       1.6                                                          #
# Author:        CGI                                                          #
# Creation Date: 18/03/2018                                                   #
# Last Modified: 15/11/2018                                                   #
# History:                                                                    #
#                1.0 - Initial version                                        #
#                1.1 - Update the variables for environment,                  #
#                      librairies and directory names.                        #
#                1.2 - Update program name                                    #
#                1.3 - Deleting variable of input, output and schema          #
#                1.4 - Rename the ksh (replace                                #
#                      xxadeo_rms_catman_traduction.ksh by                    #
#                      xxadeo_rms_catmant_orghiertl.ksh), schema and          #
#                      correct test to retrieve label                         #
#                      (for example replace EN by 1 follow the                #
#                      mapping rule in the new CV027 received on 18/10/2018   #
#                1.5 - Update Input and Output path and replace               #
#                      xxadeo_rms_catmant_orghiertl by                        #
#                      xxadeo_catmant_traductions_orghier                     #
#                1.6 - Update ouput filename                                  #
#                                                                             #
###############################################################################

###############################################################################
#  This script builds many data file
#
###############################################################################

###############################################################################
#  PROGRAM NAME DEFINE
###############################################################################
PROGRAM_NAME='xxadeo_catmant_traductions_orghier'

RFX_EXE=${RFX_EXE:=rfx}

###############################################################################
#  INCLUDES
###############################################################################
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env
. ${LIB_DIR}/xxadeo_rpas_lib.ksh

###############################################################################
#  INPUT/OUTPUT DEFINES
###############################################################################

I_STORE_PATTERN=XXADEO_XXRMS227_ORGHIER_TRANS_STORE_*.dat
I_DISTRI_PATTERN=XXADEO_XXRMS227_ORGHIER_TRANS_DISTRI_*.dat
I_REGION_PATTERN=XXADEO_XXRMS227_ORGHIER_TRANS_REGION_*.dat
I_AREA_PATTERN=XXADEO_XXRMS227_ORGHIER_TRANS_AREA_*.dat
I_CHAIN_PATTERN=XXADEO_XXRMS227_ORGHIER_TRANS_CHAIN_*.dat
I_COMP_PATTERN=XXADEO_XXRMS227_ORGHIER_TRANS_COMP_*.dat

RETL_OUTPUT_FILE1=$O_DATA_DIR_XXADEO/r_str1label.csv.rpl
RETL_OUTPUT_FILE2=$O_DATA_DIR_XXADEO/r_st1rlabel.csv.rpl
RETL_OUTPUT_FILE3=$O_DATA_DIR_XXADEO/r_str2label.csv.rpl
RETL_OUTPUT_FILE4=$O_DATA_DIR_XXADEO/r_st2rlabel.csv.rpl
RETL_OUTPUT_FILE5=$O_DATA_DIR_XXADEO/r_regnlabel.csv.rpl
RETL_OUTPUT_FILE6=$O_DATA_DIR_XXADEO/r_regrlabel.csv.rpl
RETL_OUTPUT_FILE7=$O_DATA_DIR_XXADEO/r_bullabel.csv.rpl
RETL_OUTPUT_FILE8=$O_DATA_DIR_XXADEO/r_burlabel.csv.rpl
RETL_OUTPUT_FILE9=$O_DATA_DIR_XXADEO/r_zonelabel.csv.rpl
RETL_OUTPUT_FILE10=$O_DATA_DIR_XXADEO/r_zonrlabel.csv.rpl
RETL_OUTPUT_FILE11=$O_DATA_DIR_XXADEO/r_grpolabel.csv.rpl
RETL_OUTPUT_FILE12=$O_DATA_DIR_XXADEO/r_grprlabel.csv.rpl

###############################################################################
#  FUNCTIONS
###############################################################################

function check_error_and_clean_before_exit {
        error_check=1
        if [[ "${2}" != "CHECK" && "${2}" = "NO_CHECK" && "${2}" = "" ]]
        then
                message "ERROR: Parameter 2 is not valid: ${2}"
                message "    Parameter 2 must be true, false or empty (true)"
                rmse_terminate 1
        elif [[ "${2}" = "NO_CHECK" ]]
        then
                error_check=0
        fi
        #message "check_error_and_clean_before_exit: error_check: ${error_check}"

        if [[ $1 -ne 0 && error_check -eq 1 ]]
        then
                rm -f "${I_STORE_FILE}.retl"
                rm -f "${I_DISTRI_FILE}.retl"
                rm -f "${I_REGION_FILE}.retl"
                rm -f "${I_AREA_FILE}.retl"
                rm -f "${I_CHAIN_FILE}.retl"
                rm -f "${I_COMP_FILE}.retl"

                rm -f "${RETL_OUTPUT_FILE1}.retl"
                rm -f "${RETL_OUTPUT_FILE1}"
                rm -f "${RETL_OUTPUT_FILE2}.retl"
                rm -f "${RETL_OUTPUT_FILE2}"
                rm -f "${RETL_OUTPUT_FILE3}.retl"
                rm -f "${RETL_OUTPUT_FILE3}"
                rm -f "${RETL_OUTPUT_FILE4}.retl"
                rm -f "${RETL_OUTPUT_FILE4}"
                rm -f "${RETL_OUTPUT_FILE5}.retl"
                rm -f "${RETL_OUTPUT_FILE5}"
                rm -f "${RETL_OUTPUT_FILE6}.retl"
                rm -f "${RETL_OUTPUT_FILE6}"
                rm -f "${RETL_OUTPUT_FILE7}.retl"
                rm -f "${RETL_OUTPUT_FILE7}"
                rm -f "${RETL_OUTPUT_FILE8}.retl"
                rm -f "${RETL_OUTPUT_FILE8}"
                rm -f "${RETL_OUTPUT_FILE9}.retl"
                rm -f "${RETL_OUTPUT_FILE9}"
                rm -f "${RETL_OUTPUT_FILE10}.retl"
                rm -f "${RETL_OUTPUT_FILE10}"
                rm -f "${RETL_OUTPUT_FILE11}.retl"
                rm -f "${RETL_OUTPUT_FILE11}"
                rm -f "${RETL_OUTPUT_FILE12}.retl"
                rm -f "${RETL_OUTPUT_FILE12}"

                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
        elif [[ error_check -eq 0 ]]
        then
                rm -f "${I_STORE_FILE}.retl"
                rm -f "${I_DISTRI_FILE}.retl"
                rm -f "${I_REGION_FILE}.retl"
                rm -f "${I_AREA_FILE}.retl"
                rm -f "${I_CHAIN_FILE}.retl"
                rm -f "${I_COMP_FILE}.retl"

                rm -f "${RETL_OUTPUT_FILE1}.retl"
                rm -f "${RETL_OUTPUT_FILE2}.retl"
                rm -f "${RETL_OUTPUT_FILE3}.retl"
                rm -f "${RETL_OUTPUT_FILE4}.retl"
                rm -f "${RETL_OUTPUT_FILE5}.retl"
                rm -f "${RETL_OUTPUT_FILE6}.retl"
                rm -f "${RETL_OUTPUT_FILE7}.retl"
                rm -f "${RETL_OUTPUT_FILE8}.retl"
                rm -f "${RETL_OUTPUT_FILE9}.retl"
                rm -f "${RETL_OUTPUT_FILE10}.retl"
                rm -f "${RETL_OUTPUT_FILE11}.retl"
                rm -f "${RETL_OUTPUT_FILE12}.retl"


                checkerror -e $1 -m "Program failed - check ${ERR_FILE}"
                rmse_terminate $1
        fi
}

###############################################################################
#  Get latest available input files, because since this interface works in a
#  FULL mode there is no need to process all available input files.
###############################################################################
I_STORE_FILE=`find $I_DATA_DIR_XXADEO -name ${I_STORE_PATTERN} | sort | tail -n 1`

I_DISTRI_FILE=`find $I_DATA_DIR_XXADEO -name ${I_DISTRI_PATTERN} | sort | tail -n 1`

I_REGION_FILE=`find $I_DATA_DIR_XXADEO -name ${I_REGION_PATTERN} | sort | tail -n 1`

I_AREA_FILE=`find $I_DATA_DIR_XXADEO -name ${I_AREA_PATTERN} | sort | tail -n 1`

I_CHAIN_FILE=`find $I_DATA_DIR_XXADEO -name ${I_CHAIN_PATTERN} | sort | tail -n 1`

I_COMP_FILE=`find $I_DATA_DIR_XXADEO -name ${I_COMP_PATTERN} | sort | tail -n 1`



if [[ ! -f $I_STORE_FILE ]];  then
    message "XXADEO_XXRMS227_ORGHIER_TRANS_STORE_YYYYMMDD-HH24MISS.dat does not exist"
    exit 1
fi

if [[  ! -f $I_DISTRI_FILE ]];  then
    message "XXADEO_XXRMS227_ORGHIER_TRANS_DISTRI_YYYYMMDD-HH24MISS.dat does not exist"
    exit 1
fi

if [[  ! -f $I_REGION_FILE ]];  then
    message "XXADEO_XXRMS227_ORGHIER_TRANS_REGION_YYYYMMDD-HH24MISS.dat does not exist"
    exit 1
fi
if [[ ! -f $I_AREA_FILE ]];  then
    message "XXADEO_XXRMS227_ORGHIER_TRANS_AREA_YYYYMMDD-HH24MISS.dat does not exist"
    exit 1
fi

if [[  ! -f $I_CHAIN_FILE ]];  then
    message "XXADEO_XXRMS227_ORGHIER_TRANS_CHAIN_YYYYMMDD-HH24MISS.dat does not exist"
    exit 1
fi

if [[  ! -f $I_COMP_FILE ]];  then
    message "XXADEO_XXRMS227_ORGHIER_TRANS_COMP_YYYYMMDD-HH24MISS.dat does not exist"
    exit 1
fi


###############################################################################
#  RETL input/output schemas
###############################################################################
IO_ORGHIERTL_SCHEMA=${SCHEMA_DIR_XXADEO}/xxadeo_rmse_rpas_orghiertl.schema


###############################################################################
#  Convert input data files in RETLSV format
###############################################################################

csv_converter "csv-retlsv" "${I_STORE_FILE}" "${I_STORE_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_DISTRI_FILE}" "${I_DISTRI_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_REGION_FILE}" "${I_REGION_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_AREA_FILE}" "${I_AREA_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_CHAIN_FILE}" "${I_CHAIN_FILE}.retl"
check_error_and_clean_before_exit $?

csv_converter "csv-retlsv" "${I_COMP_FILE}" "${I_COMP_FILE}.retl"
check_error_and_clean_before_exit $?




###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
#
#  Note
#
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">


        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_STORE_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <OUTPUT name="i_store.v"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_DISTRI_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <OUTPUT name="i_distri.v"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_REGION_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <OUTPUT name="i_region.v"/>
        </OPERATOR>

                <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_AREA_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <OUTPUT name="i_area.v"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_CHAIN_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <OUTPUT name="i_chain.v"/>
        </OPERATOR>

        <OPERATOR  type="import">
                <PROPERTY  name="inputfile" value="${I_COMP_FILE}.retl"/>
                <PROPERTY  name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <OUTPUT name="i_comp.v"/>
        </OPERATOR>


        <OPERATOR type="parser">
                <INPUT name="i_store.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                if (RECORD.LANG_ID == "1")
                                {
                                        RECORD.LANG_ID = "english";
                                }
                                if (RECORD.LANG_ID == "3")
                                {
                                        RECORD.LANG_ID = "french";
                                }
                                if (RECORD.LANG_ID == "20")
                                {
                                        RECORD.LANG_ID = "greek";
                                }
                                if (RECORD.LANG_ID == "22")
                                {
                                        RECORD.LANG_ID = "italian";
                                }
                                if (RECORD.LANG_ID == "26")
                                {
                                        RECORD.LANG_ID = "polish";
                                }
                                if (RECORD.LANG_ID == "27")
                                {
                                        RECORD.LANG_ID = "portuguese";
                                }
                                if (RECORD.LANG_ID == "4")
                                {
                                        RECORD.LANG_ID = "spanish";
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="i_store_1.v"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="i_distri.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                if (RECORD.LANG_ID == "1")
                                {
                                        RECORD.LANG_ID = "english";
                                }
                                if (RECORD.LANG_ID == "3")
                                {
                                        RECORD.LANG_ID = "french";
                                }
                                if (RECORD.LANG_ID == "20")
                                {
                                        RECORD.LANG_ID = "greek";
                                }
                                if (RECORD.LANG_ID == "22")
                                {
                                        RECORD.LANG_ID = "italian";
                                }
                                if (RECORD.LANG_ID == "26")
                                {
                                        RECORD.LANG_ID = "polish";
                                }
                                if (RECORD.LANG_ID == "27")
                                {
                                        RECORD.LANG_ID = "portuguese";
                                }
                                if (RECORD.LANG_ID == "4")
                                {
                                        RECORD.LANG_ID = "spanish";
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="i_distri_1.v"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="i_region.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                if (RECORD.LANG_ID == "1")
                                {
                                        RECORD.LANG_ID = "english";
                                }
                                if (RECORD.LANG_ID == "3")
                                {
                                        RECORD.LANG_ID = "french";
                                }
                                if (RECORD.LANG_ID == "20")
                                {
                                        RECORD.LANG_ID = "greek";
                                }
                                if (RECORD.LANG_ID == "22")
                                {
                                        RECORD.LANG_ID = "italian";
                                }
                                if (RECORD.LANG_ID == "26")
                                {
                                        RECORD.LANG_ID = "polish";
                                }
                                if (RECORD.LANG_ID == "27")
                                {
                                        RECORD.LANG_ID = "portuguese";
                                }
                                if (RECORD.LANG_ID == "4")
                                {
                                        RECORD.LANG_ID = "spanish";
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="i_region_1.v"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="i_area.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                if (RECORD.LANG_ID == "1")
                                {
                                        RECORD.LANG_ID = "english";
                                }
                                if (RECORD.LANG_ID == "3")
                                {
                                        RECORD.LANG_ID = "french";
                                }
                                if (RECORD.LANG_ID == "20")
                                {
                                        RECORD.LANG_ID = "greek";
                                }
                                if (RECORD.LANG_ID == "22")
                                {
                                        RECORD.LANG_ID = "italian";
                                }
                                if (RECORD.LANG_ID == "26")
                                {
                                        RECORD.LANG_ID = "polish";
                                }
                                if (RECORD.LANG_ID == "27")
                                {
                                        RECORD.LANG_ID = "portuguese";
                                }
                                if (RECORD.LANG_ID == "4")
                                {
                                        RECORD.LANG_ID = "spanish";
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="i_area_1.v"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="i_chain.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                if (RECORD.LANG_ID == "1")
                                {
                                        RECORD.LANG_ID = "english";
                                }
                                if (RECORD.LANG_ID == "3")
                                {
                                        RECORD.LANG_ID = "french";
                                }
                                if (RECORD.LANG_ID == "20")
                                {
                                        RECORD.LANG_ID = "greek";
                                }
                                if (RECORD.LANG_ID == "22")
                                {
                                        RECORD.LANG_ID = "italian";
                                }
                                if (RECORD.LANG_ID == "26")
                                {
                                        RECORD.LANG_ID = "polish";
                                }
                                if (RECORD.LANG_ID == "27")
                                {
                                        RECORD.LANG_ID = "portuguese";
                                }
                                if (RECORD.LANG_ID == "4")
                                {
                                        RECORD.LANG_ID = "spanish";
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="i_chain_1.v"/>
        </OPERATOR>

        <OPERATOR type="parser">
                <INPUT name="i_comp.v"/>
                        <PROPERTY name="expression">
                        <![CDATA[
                                if (RECORD.LANG_ID == "1")
                                {
                                        RECORD.LANG_ID = "english";
                                }
                                if (RECORD.LANG_ID == "3")
                                {
                                        RECORD.LANG_ID = "french";
                                }
                                if (RECORD.LANG_ID == "20")
                                {
                                        RECORD.LANG_ID = "greek";
                                }
                                if (RECORD.LANG_ID == "22")
                                {
                                        RECORD.LANG_ID = "italian";
                                }
                                if (RECORD.LANG_ID == "26")
                                {
                                        RECORD.LANG_ID = "polish";
                                }
                                if (RECORD.LANG_ID == "27")
                                {
                                        RECORD.LANG_ID = "portuguese";
                                }
                                if (RECORD.LANG_ID == "4")
                                {
                                        RECORD.LANG_ID = "spanish";
                                }
                                ]]>
                        </PROPERTY>
                <OUTPUT name="i_comp_1.v"/>
        </OPERATOR>


        <OPERATOR type="copy">
                <INPUT name="i_store_1.v"/>
                <OUTPUT name="r_Str1label.v"/>
                <OUTPUT name="r_St1rlabel.v"/>
        </OPERATOR>


        <OPERATOR type="copy">
                <INPUT name="i_distri_1.v"/>
                <OUTPUT name="r_Str2label.v"/>
                <OUTPUT name="r_St2rlabel.v"/>
        </OPERATOR>


        <OPERATOR type="copy">
                <INPUT name="i_region_1.v"/>
                <OUTPUT name="r_regnlabel.v"/>
                <OUTPUT name="r_regrlabel.v"/>
        </OPERATOR>


        <OPERATOR type="copy">
                <INPUT name="i_area_1.v"/>
                <OUTPUT name="r_bullabel.v"/>
                <OUTPUT name="r_burlabel.v"/>
        </OPERATOR>


        <OPERATOR type="copy">
                <INPUT name="i_chain_1.v"/>
                <OUTPUT name="r_zonelabel.v"/>
                <OUTPUT name="r_zonerlabel.v"/>
        </OPERATOR>


        <OPERATOR type="copy">
                <INPUT name="i_comp_1.v"/>
                <OUTPUT name="r_grpolabel.v"/>
                <OUTPUT name="r_grprlabel.v"/>
        </OPERATOR>


        <OPERATOR type="export">
                <INPUT name="r_Str1label.v" />
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE1}.retl"/>
        </OPERATOR>


        <OPERATOR type="export">
                <INPUT name="r_St1rlabel.v"/>
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE2}.retl"/>
        </OPERATOR>

                <OPERATOR type="export">
                <INPUT name="r_Str2label.v" />
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE3}.retl"/>
        </OPERATOR>


        <OPERATOR type="export">
                <INPUT name="r_St2rlabel.v"/>
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE4}.retl"/>
        </OPERATOR>

                <OPERATOR type="export">
                <INPUT name="r_regnlabel.v" />
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE5}.retl"/>
        </OPERATOR>


        <OPERATOR type="export">
                <INPUT name="r_regrlabel.v"/>
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE6}.retl"/>
        </OPERATOR>

                <OPERATOR type="export">
                <INPUT name="r_bullabel.v" />
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE7}.retl"/>
        </OPERATOR>


        <OPERATOR type="export">
                <INPUT name="r_burlabel.v"/>
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE8}.retl"/>
        </OPERATOR>

                <OPERATOR type="export">
                <INPUT name="r_zonelabel.v" />
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE9}.retl"/>
        </OPERATOR>


        <OPERATOR type="export">
                <INPUT name="r_zonerlabel.v"/>
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE10}.retl"/>
        </OPERATOR>

                <OPERATOR type="export">
                <INPUT name="r_grpolabel.v" />
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE11}.retl"/>
        </OPERATOR>


        <OPERATOR type="export">
                <INPUT name="r_grprlabel.v"/>
                <PROPERTY name="schemafile" value="${IO_ORGHIERTL_SCHEMA}"/>
                <PROPERTY name="outputfile" value="${RETL_OUTPUT_FILE12}.retl"/>
        </OPERATOR>


</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}
check_error_and_clean_before_exit $?

###############################################################################
#  Convert output data files to CSV format
###############################################################################

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE1}.retl" "${RETL_OUTPUT_FILE1}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE2}.retl" "${RETL_OUTPUT_FILE2}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE3}.retl" "${RETL_OUTPUT_FILE3}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE4}.retl" "${RETL_OUTPUT_FILE4}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE5}.retl" "${RETL_OUTPUT_FILE5}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE6}.retl" "${RETL_OUTPUT_FILE6}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE7}.retl" "${RETL_OUTPUT_FILE7}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE8}.retl" "${RETL_OUTPUT_FILE8}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE9}.retl" "${RETL_OUTPUT_FILE9}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE10}.retl" "${RETL_OUTPUT_FILE10}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE11}.retl" "${RETL_OUTPUT_FILE11}"
check_error_and_clean_before_exit $?

csv_converter "retlsv-csv" "${RETL_OUTPUT_FILE12}.retl" "${RETL_OUTPUT_FILE12}"
check_error_and_clean_before_exit $?


###############################################################################
#  Log the number of records written to the final output files and add up the total:
###############################################################################

log_num_recs ${RETL_OUTPUT_FILE1}

log_num_recs ${RETL_OUTPUT_FILE2}

log_num_recs ${RETL_OUTPUT_FILE3}

log_num_recs ${RETL_OUTPUT_FILE4}

log_num_recs ${RETL_OUTPUT_FILE5}

log_num_recs ${RETL_OUTPUT_FILE6}

log_num_recs ${RETL_OUTPUT_FILE7}

log_num_recs ${RETL_OUTPUT_FILE8}

log_num_recs ${RETL_OUTPUT_FILE9}

log_num_recs ${RETL_OUTPUT_FILE10}

log_num_recs ${RETL_OUTPUT_FILE11}

log_num_recs ${RETL_OUTPUT_FILE12}

###############################################################################
#  cleanup and exit:
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE} ; fi
message "Program completed successfully."

check_error_and_clean_before_exit 0 "NO_CHECK"