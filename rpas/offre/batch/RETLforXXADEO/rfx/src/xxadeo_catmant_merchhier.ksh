#!/bin/ksh

### INTRO #####################################################################
#  ____________________________________________________________________________
#  Workfile:             | xxadeo_catmant_merchhier.ksh
#  Created by:           | PR <paulo.reis@oracle.com>
#  Create Datetime:      | 20180427-1145
#  Last Updated by:      | PVI <pedro.vieira@oracle.com>
#  Last Update Datetime: | 20181106-1100
#  Version:              | 1.5
#
#  Interface IDs:
#  --------------
#  RICEW #: 264 / 265
#  IR ID: IR0079
#
#  This script transforms Merchandise Hierarchy extracted from RMS #  to be
#  used by CatMan and other RPAS modules.
#
#  Usage: xxadeo_catmant_merchhier.ksh
#
#  Expected files:
#    1. prod.csv.dat
#    2. pror.csv.dat
#    3. rpas_pack_item.csv.rpl
#    4. item.csv.rpl
#
#  History:
#       1.4 - Added Includes and Variables according Adeo Env
#           - Changed the script name according the Adeo Standards
#	1.5 - Bug Fixing: BUG_633_634_635 - The changes are:
#		- Assortment Group Label and ID fixed
#		- Filter applied on Item Level equal Tran Level
#		- Parent and GrandParent Label and ID fixed
#		- Perfomance fixed
#
#
###############################################################################

EXEC_START=`date +%s`

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_catmant_merchhier"

### INCLUDES ##################################################################

#RETLforXXADEO=<uncomment and set as needed>				# Nearshore ENV
[[ -z $RETLforXXADEO ]] && RETLforXXADEO=$RMS_RPAS_HOME
export RETL_ENV=$RETLforXXADEO

## Environment configuration script
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env			# (Adeo ENV - RPAS)

## Script Library Collection
. ${LIB_DIR}/xxadeo_rpas_lib.ksh							# (Adeo ENV - RPAS)

### INPUT / OUTPUT DEFINITIONS ################################################

export INBOUND_DIR=${I_DATA_DIR}						# Adeo ENV (Base RPAS Input Dir)
export INBOUND_DIR_CUST=${I_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Input Dir)


export OUTBOUND_DIR=${O_DATA_DIR}						# Adeo ENV (Base RPAS Output Dir)
export OUTBOUND_DIR_CUST=${O_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Output Dir)


#export STAGING_DIR=${INBOUND_DIR}
export STAGING_DIR=${INBOUND_DIR_CUST}                  # Adeo ENV
#export OUTPUT_DIR=${OUTBOUND_DIR}/transformation
export OUTPUT_DIR=${OUTBOUND_DIR_CUST}
#$(mkdir -p ${OUTPUT_DIR})

#export OUTPUT_DIR_RHS=${INBOUND_DIR}/hierarchies
export OUTPUT_DIR_RHS=${INBOUND_DIR_CUST}
#$(mkdir -p ${OUTPUT_DIR_RHS})

export INPUT_FILE_1=${STAGING_DIR}/xxadeo_catmant_merchhier_hieritem.dat
export INPUT_FILE_2=${STAGING_DIR}/assortGroup.dat
#export INPUT_FILE_SCHEMA_1=${SCHEMA_DIR}/xxadeo_rmse_rpas_merchhier_hieritem.schema
export INPUT_FILE_SCHEMA_1=${SCHEMA_DIR_XXADEO}/xxadeo_catmant_merchhier_hieritem.schema  # Adeo ENV
export INPUT_FILE_SCHEMA_2=${SCHEMA_DIR_XXADEO}/xxadeo_catmant_merchhier_assortgroup.schema  # Adeo ENV
export INPUT_REJECTED_FILE_1=${STAGING_DIR}/${PROGRAM_NAME}_file_1.rej

export ID_SEPARATOR='"_"'
export LABEL_SEPARATOR='" "'
export OUTPUT_FILE=${OUTPUT_DIR}/prod.csv.dat
export OUTPUT_FILE_TMP=${OUTPUT_FILE}.tmp
#export OUTPUT_SCHEMA_FILE=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export OUTPUT_SCHEMA_FILE=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema       # Adeo ENV
export OUTPUT_FILE_RHS=${OUTPUT_DIR_RHS}/pror.csv.dat

export agTAG=AG

## Run auxiliar scripts

auxiliarProgs="xxadeo_catmant_merchhier_assortgroup xxadeo_catmant_merchhier_hieritem xxadeo_catmant_merchhier_itemlie"

for prog in $auxiliarProgs; do

    echo "[$(date '+%Y/%m/%d-%H:%M:%S')]: Started running ${prog}.ksh..."
	$SRC_DIR/${prog}.ksh

	exit_status=$?

	if [ ${exit_status} -ne 0 ]; then
	  echo "[$(date '+%Y/%m/%d-%H:%M:%S')]: ERROR running auxiliary script."
	  echo "   Prog name: ${prog}"
	  echo "   Exit status code: ${exit_status}"
	  echo "                see '${ERR_DIR}/${prog}.${FILE_DATE} file for more details."
	else
	  echo "[$(date '+%Y/%m/%d-%H:%M:%S')]: Finished running ${prog}.ksh..."
	fi
done

### CREATE RETL FLOWS #########################################################

message "Program start..."

#	Remove Tag AG from Assortment Group File
sed 's/AG//g' ${INPUT_FILE_2} > ${INPUT_FILE_2}.tmp 


FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">

<!-- INPUT File 1 -->
	<OPERATOR type="import" name="input_file_1">
		<PROPERTY name="inputfile" value="${INPUT_FILE_1}"/>
		<PROPERTY name="schemafile" value="${INPUT_FILE_SCHEMA_1}"/>
		<PROPERTY name="rejectfile" value="${INPUT_REJECTED_FILE_1}"/>
		<OUTPUT name="hierarchy_items.v"/>
	</OPERATOR>

<!-- INPUT File 2 -->
        <OPERATOR type="import" name="input_file_1">
                <PROPERTY name="inputfile" value="${INPUT_FILE_2}.tmp"/>
                <PROPERTY name="schemafile" value="${INPUT_FILE_SCHEMA_2}"/>
                <OUTPUT name="assortment_group.v"/>
        </OPERATOR>


	<!-- SORT:
		Sort hierarchy items by ITEM
	-->
	<OPERATOR type="sort">
		<PROPERTY name="key" value="ITEM"/>
		<INPUT name="hierarchy_items.v"/>
		<OUTPUT name="hierarchy_items_sorted.v"/>
	</OPERATOR>

	<!-- SORT:
                Sort Assortment Groups by ITEM
        -->
        <OPERATOR type="sort">
                <PROPERTY name="key" value="ITEM"/>
                <INPUT name="assortment_group.v"/>
                <OUTPUT name="assortment_group_sorted.v"/>
        </OPERATOR>

	<!-- JOIN:
		Left Join between hierarchy items and assortment group
	-->
	<OPERATOR type="leftouterjoin">
		<INPUT name="hierarchy_items_sorted.v"/>
		<INPUT name="assortment_group_sorted.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="hieritems_assortgroup_joined.v"/>
	</OPERATOR>



<!-- Generate output structure and Parse source fields to match target requirements -->
	<OPERATOR type="generator">
		<INPUT name="hieritems_assortgroup_joined.v"/>
		<PROPERTY name="schema">
		<![CDATA[
		<GENERATE>
		  <FIELD name="SKU"                     nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="SKU_LABEL"               nullable="false" maxlength="250" type="string"><CONST value=""/></FIELD>
		  <FIELD name="PARENT_ITEM"             nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="PARENT_ITEM_LABEL"       nullable="false" maxlength="250" type="string"><CONST value=""/></FIELD>
		  <FIELD name="GRAND_PARENT_ITEM"       nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="GRAND_PARENT_ITEM_LABEL" nullable="false" maxlength="250" type="string"><CONST value=""/></FIELD>
		  <FIELD name="SUBTYPE"                 nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="SUBTYPE_LABEL"           nullable="false" maxlength="120" type="string"><CONST value=""/></FIELD>
		  <FIELD name="TYPE"                    nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="TYPE_LABEL"              nullable="false" maxlength="120" type="string"><CONST value=""/></FIELD>
		  <FIELD name="SUBAREA"                 nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="SUBAREA_LABEL"           nullable="false" maxlength="120" type="string"><CONST value=""/></FIELD>
		  <FIELD name="AREA"                    nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="AREA_LABEL"              nullable="false" maxlength="120" type="string"><CONST value=""/></FIELD>
		  <FIELD name="AREA2"                   nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="AREA2_LABEL"             nullable="false" maxlength="120" type="string"><CONST value=""/></FIELD>
		  <FIELD name="GROUPE"                  nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="GROUPE_LABEL"            nullable="false" maxlength="120" type="string"><CONST value=""/></FIELD>
		  <FIELD name="AG"                      nullable="false" maxlength="25"  type="string"><CONST value=""/></FIELD>
		  <FIELD name="AG_LABEL"                nullable="false" maxlength="120" type="string"><CONST value=""/></FIELD>
		</GENERATE>
		]]>
		</PROPERTY>
		<OUTPUT name="generator_output.v"/>
	</OPERATOR>

	<OPERATOR type="parser">
		<INPUT name="generator_output.v"/>
		<PROPERTY name="expression">
			<![CDATA[

			// 1,17 Groupe -->
			RECORD.GROUPE += RECORD.COMPANY;

			// 1,18 Groupe Label -->
			RECORD.GROUPE_LABEL += RECORD.COMPANY_LPAD;
			RECORD.GROUPE_LABEL += ${LABEL_SEPARATOR};
			RECORD.GROUPE_LABEL += RECORD.CO_NAME;

			// 1,15 Rayon -->
			RECORD.AREA2 += RECORD.DIVISION;

			// 1,16 Rayon Label -->
			RECORD.AREA2_LABEL += RECORD.DIVISION_LPAD;
			RECORD.AREA2_LABEL += ${LABEL_SEPARATOR};
			RECORD.AREA2_LABEL += RECORD.DIV_NAME;

			// 1,13 Rayon -->
			RECORD.AREA = RECORD.GROUP_NO;

			// 1,14 Rayon Label -->
			RECORD.AREA_LABEL += RECORD.GROUP_NO_LPAD;
			RECORD.AREA_LABEL += ${LABEL_SEPARATOR};
			RECORD.AREA_LABEL += RECORD.GROUP_NAME;

			// 1,11 Sous Rayon -->
			RECORD.SUBAREA = RECORD.DEPT;

			// 1,12 Sous Rayon Label -->
			RECORD.SUBAREA_LABEL += RECORD.DEPT_LPAD;
			RECORD.SUBAREA_LABEL += ${LABEL_SEPARATOR};
			RECORD.SUBAREA_LABEL += RECORD.DEPT_NAME;

			// 1,09 Type -->
			RECORD.TYPE = RECORD.SUBAREA;
			RECORD.TYPE += ${ID_SEPARATOR};
			RECORD.TYPE += RECORD.CLASS;

			// 1,10 Type Label -->
			RECORD.TYPE_LABEL += RECORD.CLASS_LPAD;
			RECORD.TYPE_LABEL += ${LABEL_SEPARATOR};
			RECORD.TYPE_LABEL += RECORD.CLASS_NAME;

			// 1,07 Sous type -->
			RECORD.SUBTYPE = RECORD.SUBAREA;
			RECORD.SUBTYPE += ${ID_SEPARATOR};
			RECORD.SUBTYPE += RECORD.CLASS;
			RECORD.SUBTYPE += ${ID_SEPARATOR};
			RECORD.SUBTYPE += RECORD.SUBCLASS;

			// 1,08 Sous type Label -->
			RECORD.SUBTYPE_LABEL += RECORD.SUBCLASS_LPAD;
			RECORD.SUBTYPE_LABEL += ${LABEL_SEPARATOR};
			RECORD.SUBTYPE_LABEL += RECORD.SUB_NAME;

			// 1,01 SKU -->
			RECORD.SKU += RECORD.ITEM;

			// 1,02 SKU Label -->
			RECORD.SKU_LABEL = RECORD.SKU;
			RECORD.SKU_LABEL += ${LABEL_SEPARATOR};
			RECORD.SKU_LABEL += RECORD.ITEM_DESC;
			
			// Parent
			if (RECORD.ITEM_PARENT == "") {
				RECORD.PARENT_ITEM = RECORD.SKU;
                                RECORD.PARENT_ITEM_LABEL = RECORD.SKU_LABEL;
			} else {
				RECORD.PARENT_ITEM = RECORD.ITEM_PARENT;
				RECORD.PARENT_ITEM_LABEL = RECORD.PARENT_ITEM;
				RECORD.PARENT_ITEM_LABEL += ${LABEL_SEPARATOR};
				RECORD.PARENT_ITEM_LABEL += RECORD.ITEM_PARENT_DESC;
			}

			// GrandParent
			if (RECORD.ITEM_GRANDPARENT == "") {
				RECORD.GRAND_PARENT_ITEM = RECORD.PARENT_ITEM;
                                RECORD.GRAND_PARENT_ITEM_LABEL = RECORD.PARENT_ITEM_LABEL;	
			} else {
				RECORD.GRAND_PARENT_ITEM = RECORD.ITEM_GRANDPARENT;
                                RECORD.GRAND_PARENT_ITEM_LABEL = RECORD.GRAND_PARENT_ITEM;
                                RECORD.GRAND_PARENT_ITEM_LABEL += ${LABEL_SEPARATOR};
                                RECORD.GRAND_PARENT_ITEM_LABEL += RECORD.ITEM_GRANDPARENT_DESC;
			}	
			
			// Assortment Group
			if (RECORD.ASSORTMENT_GROUP == "") {
				RECORD.AG = RECORD.GRAND_PARENT_ITEM;
				RECORD.AG_LABEL = RECORD.GRAND_PARENT_ITEM_LABEL;
			} else {
				RECORD.AG = RECORD.ASSORTMENT_GROUP;
				RECORD.AG_LABEL = RECORD.AG;
				RECORD.AG_LABEL += ${LABEL_SEPARATOR};
				RECORD.AG_LABEL += "Assortment Group";
				RECORD.AG_LABEL += ${LABEL_SEPARATOR};
				RECORD.AG_LABEL += RECORD.AG;
			}

			]]>
		</PROPERTY>
	<OUTPUT name="target.v"/>
	</OPERATOR>

<!-- Export transformation to target file  -->
    <OPERATOR type="export">
		<INPUT    name="target.v"/>
		<PROPERTY name="outputfile" value="${OUTPUT_FILE}.retl"/>
		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
    </OPERATOR>
</FLOW>
EOF

### EXECUTE RETL FLOWS ########################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

###############################################################################
#  Convert output data files to CSV format
###############################################################################

message "csv_converter - Started processing the Output File..."

# -- The default field delimiter: delimiter="0x1F"
csv_converter "retlsv-csv" "${OUTPUT_FILE}.retl" "${OUTPUT_FILE}"
checkerror -e $? -m "Convert from Data file to CSV file failed - check $ERR_FILE"

message "csv_converter - Finished processing the Output File..."

###############################################################################
#  Log the number of records written to the final output file:
###############################################################################

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

### REMOVE STATUS FILE ########################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

### REMOVE BLANK REJECTED FILE ################################################

REJ_FILE=${INPUT_REJECTED_FILE_1}
if [ `cat ${REJ_FILE} | wc -l` -eq 0 ]; then rm ${REJ_FILE}; fi


### COPY/CREATE FILES 1 & 2 to OUTPUT_DIR #####################################

cp ${OUTPUT_FILE} ${OUTPUT_FILE_RHS}

### COPY FILE 3 to OUTPUT_DIR #################################################

cp ${RETL_IN}/rpas_pack_item.csv.rpl ${OUTPUT_DIR}
cp ${INBOUND_DIR_CUST}/rpas_pack_item.csv.rpl ${OUTPUT_DIR}

###  CLEAN UP  ################################################################

rm ${INPUT_FILE_1}
rm ${INPUT_FILE_2}.tmp
rm ${OUTPUT_FILE}.retl

###  REPORT AND EXIT ##########################################################

case $retl_stat in
  0 )
     msg="Program completed successfully with" ;;
  * )
     msg="Program exited with error" ;;
esac

EXEC_FINISH=`date +%s`
RUNTIME=$(( EXEC_FINISH-EXEC_START ))
message "$msg code: $retl_stat, in $RUNTIME seconds."
rmse_terminate $retl_stat
