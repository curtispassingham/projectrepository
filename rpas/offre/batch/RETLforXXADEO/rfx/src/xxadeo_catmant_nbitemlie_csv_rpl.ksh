#!/bin/ksh

### INTRO #####################################################################
#  ____________________________________________________________________________
#  Workfile:             | xxadeo_catmant_nbitemlie_csv_rpl.ksh
#  Created by:           | PR <paulo.reis@oracle.com>
#  Create Datetime:      | 20180510-1145
#  Last Updated by:      | PVI <pedro.vieira@oracle.com>
#  Last Update Datetime: | 20181113-1000
#  Version:              | 1.5
#
#  Interface IDs:
#  --------------
#  RICEW #: 1143
#  IR ID: IR00318
#
#
#  Usage: xxadeo_catmant_nbitemlie_csv_rpl.ksh
#  
#	Input Expected:
#		XXRMS218_RELATEDITEMS_YYYYMMDD-HH24MISS.dat
#   		XXRMS209_ITEMATTR_YYYYMMDD-HH24MISS.dat
#		XXRMS209_ATTR_YYYYMMDD-HH24MISS.dat
#		rmse_rpas_item_master.dat
#
#  History:
#       1.3 - Added Includes and Variables according Adeo Env
#	1.4 - Bug Fixing (BUG_632): 
#		- Join field between files has been changed to RELATED_ITEM
#		- Applied a filter condition for unic CFA Attrib ID
#		- Added new input file to get ATTRIB_ID for value= UNIT_CONV_FACTOR_ITEM
#		- Column ATTR_VALUE_ID of output file has been changed to nullable
#	1.5 - Bug Fixing: BUG_649 - The changes are:
#		- Added as input the item master file to apply a filter to get 
#		only the items with item_level equal 1
#		- Change the input files name and applied a search under on input
#		folder to find a input files custom.
#	1.6 - Bug Fixing: BUG_660 - The changes are:
#		- Applied the filter condition item_level 1 to related item column
#		on the related item file
#	
###############################################################################

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_catmant_nbitemlie_csv_rpl"

### INCLUDES ##################################################################

## Environment configuration script
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env			# (Adeo ENV - RPAS)

## Script Library Collection
. ${LIB_DIR}/xxadeo_rpas_lib.ksh							# (Adeo ENV - RPAS)

### INPUT / OUTPUT DEFINITIONS ################################################

export INBOUND_DIR=${I_DATA_DIR}						# Adeo ENV (Base RPAS Input Dir)
export INBOUND_DIR_CUST=${I_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Input Dir)

export OUTBOUND_DIR=${O_DATA_DIR}						# Adeo ENV (Base RPAS Output Dir)
export OUTBOUND_DIR_CUST=${O_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Output Dir)


export STAGING_DIR=${INBOUND_DIR_CUST}					# Adeo ENV
export OUTPUT_DIR=${OUTBOUND_DIR_CUST}					# Adeo ENV

#	RELATED ITEMS
FILE_1=`ls $INBOUND_DIR_CUST | sort -r | grep 'XXRMS218_RELATEDITEMS_.*\.dat\$' | head -1`
#	ITEM ATTRIBUTES
FILE_2=`ls $INBOUND_DIR_CUST | sort -r | grep 'XXRMS209_ITEMATTR_.*\.dat\$' | head -1`
#	ATTRIBUTES -Only to get an unic attrib
FILE_3=`ls $INBOUND_DIR_CUST | sort -r | grep 'XXRMS209_ATTR_.*\.dat\$' | head -1`
#	ITEM MASTER
FILE_4=rmse_rpas_item_master.dat

export FILE_1=${INBOUND_DIR_CUST}/${FILE_1}				# Adeo ENV
export INPUT_FILE_1=${FILE_1}.tmp
export INPUT_SCHEMA_FILE_1=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}_file_1.schema	# Adeo ENV
export INPUT_REJECTED_FILE_1=${STAGING_DIR}/${PROGRAM_NAME}_file_1.rej

export INPUT_FILE_2=${INBOUND_DIR_CUST}/${FILE_2}		# Adeo ENV
export INPUT_SCHEMA_FILE_2=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}_file_2.schema	# Adeo ENV
export INPUT_REJECTED_FILE_2=${STAGING_DIR}/${PROGRAM_NAME}_file_2.rej

export INPUT_FILE_3=${INBOUND_DIR_CUST}/${FILE_3}

export INPUT_FILE_4=${INBOUND_DIR}/${FILE_4}
export INPUT_SCHEMA_FILE_4=${SCHEMA_DIR}/rmse_rpas_item_master.schema

# Join and Sort Key
JOIN_FIELD="RELATED_ITEM"
SORT_FIELD=${JOIN_FIELD}

export OUTPUT_FILE=${OUTPUT_DIR}/nbitemlie.csv.rpl
export OUTPUT_SCHEMA_FILE=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema 			# Adeo ENV

# Filter Conditions Variables
CFA_ATTRIB_PARAMETER="UNIT_CONV_FACTOR_ITEM"
ATTRIB_TYPE_FILTER="CFA"

message "FILE: ${INPUT_FILE_3}"
GREP_CMD=$(grep -e 'CFA,.*,UNIT_CONV_FACTOR_ITEM,.*' ${INPUT_FILE_3})
message "GREP: ${GREP_CMD}"
ATTRIB_ID_FILTER=$(awk -F',' '{ print $2 ; }' <<< ${GREP_CMD})

message "Attrib Filter ID : ${ATTRIB_ID_FILTER} "

## Parse INPUT_FILE_1 to escape commas and inner quotes

$(awk -F'"' -v OSF='' '{ for (i=2; i<=NF; i+=2) gsub(",", "\x2F", $i) } 1' ${FILE_1} > ${INPUT_FILE_1})

### CREATE RETL FLOWS #########################################################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
	<!-- INPUT:
		File 1: RELATED ITEMS
 	-->
	<OPERATOR type="import" name="input_file_1">
		<PROPERTY name="inputfile" value="${INPUT_FILE_1}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_1}"/>
		<PROPERTY name="rejectfile" value="${INPUT_REJECTED_FILE_1}"/>
		<OUTPUT name="related_items.v"/>
	</OPERATOR>

	<OPERATOR type="sort">
		<PROPERTY name="key" value="ITEM"/>
		<INPUT name="related_items.v"/>
		<OUTPUT name="related_items_sorted_byItem.v"/>
	</OPERATOR>

	<!-- INPUT:
		File 2: ITEM ATTRIB
 	-->
	<OPERATOR type="import" name="input_file_2">
		<PROPERTY name="inputfile" value="${INPUT_FILE_2}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_2}"/>
		<PROPERTY name="rejectfile" value="${INPUT_REJECTED_FILE_2}"/>
		<OUTPUT name="item_attrib.v"/>
	</OPERATOR>
	
	<!-- FILTER:
		Apply filter to item attrib data
	-->
	<OPERATOR type="filter">
		<INPUT name="item_attrib.v"/>
		<PROPERTY name="filter" value="ATTR_TYPE EQ ${ATTRIB_TYPE_FILTER} AND ATTR_ID EQ ${ATTRIB_ID_FILTER}"/>
		<PROPERTY name="rejects" value="false"/>
		<OUTPUT name="item_attrib_clean.v"/>
	</OPERATOR>

	<!-- INPUT:
        	File 3: ITEM MASTER
 	-->
        <OPERATOR type="import">
                <PROPERTY name="inputfile" value="${INPUT_FILE_4}"/>
                <PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_4}"/>
                <OUTPUT name="item_master.v"/>
        </OPERATOR>
	
	<!-- FIELDMOD:
		Remove unnecessary columns
	-->
	<OPERATOR type="fieldmod">
                <INPUT name="item_master.v"/>
                <PROPERTY name="keep" value="ITEM ITEM_LEVEL"/>
                <OUTPUT name="item_master_clean.v"/>
        </OPERATOR>
	
	<!-- FILTER:
                Apply filter to get only items where ITEM_LEVEL equal 1
        -->
	<OPERATOR type="filter">
                <INPUT name="item_master_clean.v"/>
                <PROPERTY name="filter" value="ITEM_LEVEL EQ 1"/>
                <PROPERTY name="rejects" value="false"/>
                <OUTPUT name="item_master_cleanf.v"/>
        </OPERATOR>

	<!-- SORT:
		Sort items By Item
	-->
        <OPERATOR type="sort">
                <PROPERTY name="key" value="ITEM"/>
                <INPUT name="item_master_cleanf.v"/>
                <OUTPUT name="item_master_sorted.v"/>
        </OPERATOR>

	<!-- COPY:
		Duplicate the item_master sorted by Item
	-->
	<OPERATOR type="copy">
		<INPUT name="item_master_sorted.v"/>
		<OUTPUT name="item_master_sorted1.v"/>
		<OUTPUT name="item_master_sorted2.v"/>
	</OPERATOR>

	<!-- JOIN 
		Join between related_items and item_master by item
		to get only items that are item level 1
	-->
        <OPERATOR type="innerjoin">
                <INPUT name="related_items_sorted_byItem.v"/>
                <INPUT name="item_master_sorted1.v"/>
                <PROPERTY name="key" value="ITEM"/>
                <OUTPUT name="joined_related_item_byItemL1.v"/>
        </OPERATOR>
	
	<!-- SORT:
                Sort related items By related
        -->
	<OPERATOR type="sort">
                <PROPERTY name="key" value="${JOIN_FIELD}"/>
                <INPUT name="joined_related_item_byItemL1.v"/>
                <OUTPUT name="related_item_sorted_byRelated.v"/>
        </OPERATOR>

	<OPERATOR type="fieldmod">
		<INPUT name="item_master_sorted2.v"/>
		<PROPERTY name="rename" value="RELATED_ITEM=ITEM"/>
		<OUTPUT name="item_master_asRelated.v"/>
	</OPERATOR>

	<OPERATOR type="sort">
                <PROPERTY name="key" value="${JOIN_FIELD}"/>
                <INPUT name="item_master_asRelated.v"/>
                <OUTPUT name="item_master_sorted_byRelated.v"/>
        </OPERATOR>

	<OPERATOR type="innerjoin">
                <INPUT name="related_item_sorted_byRelated.v"/>
                <INPUT name="item_master_sorted_byRelated.v"/>
                <PROPERTY name="key" value="${JOIN_FIELD}"/>
                <OUTPUT name="joined_related_item.v"/>
        </OPERATOR>

	<OPERATOR type="sort">
                <PROPERTY name="key" value="${JOIN_FIELD}"/>
                <INPUT name="joined_related_item.v"/>
                <OUTPUT name="joined_related_item_sorted.v"/>
        </OPERATOR>

	<OPERATOR type="fieldmod">
		<INPUT name="item_attrib_clean.v"/>
		<PROPERTY name="rename" value="RELATED_ITEM=ITEM"/>
		<OUTPUT name="item_attrib_tmp.v"/>
	</OPERATOR>

	<OPERATOR type="sort">
		<PROPERTY name="key" value="${JOIN_FIELD}"/>
		<INPUT name="item_attrib_tmp.v"/>
		<OUTPUT name="item_attrib_sorted.v"/>
	</OPERATOR>

<!-- Join INPUT file 1, INPUT file 2 -->
	<OPERATOR type="leftouterjoin" name="joined_files_1_2">
		<INPUT name="joined_related_item_sorted.v"/>
		<INPUT name="item_attrib_sorted.v"/>
		<PROPERTY name="key" value="${JOIN_FIELD}"/>
		<OUTPUT name="joined_files_1_2.v"/>
	</OPERATOR>

<!-- Export transformation to a new file  -->
	<OPERATOR type="export">
    		<INPUT    name="joined_files_1_2.v"/>
    		<PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
    		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
    	</OPERATOR>
</FLOW>
EOF
### EXECUTE RETL FLOWS ########################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

### REMOVE STATUS FILE #######################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

### REMOVE BLANK REJECTED FILE ################################################

## Remove if blank
if [ `cat ${INPUT_REJECTED_FILE_1} | wc -l` -eq 0 ]; then rm ${INPUT_REJECTED_FILE_1}; fi
if [ `cat ${INPUT_REJECTED_FILE_2} | wc -l` -eq 0 ]; then rm ${INPUT_REJECTED_FILE_2}; fi

###  CLEAN UP  ################################################################

rm ${INPUT_FILE_1}

###  REPORT AND EXIT ##########################################################

case $retl_stat in
  0 )
     msg="Program completed successfully with" ;;
  * )
     msg="Program exited with error" ;;
esac

message "$msg code: $retl_stat"
rmse_terminate $retl_stat
