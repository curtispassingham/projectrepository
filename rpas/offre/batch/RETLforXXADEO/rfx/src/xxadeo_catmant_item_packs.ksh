#!/bin/ksh

###################################################################
# About:
#  This script Pre-Processing Items and Packs information
#  from RMS to RPAS modules.
#  The output of this script will be available and ready to load
#  inside the other script.
#
#	Input Script:	$1 - Input folder	(Not Required)
#					$2 - Output folder	(Not Required)
#
#	Input(s) Required: 	item_master and item_packs.
#	Output(s) Expected: 	item_packs - grouping by Packs and Items.
#	Ex(s): 	i001,C,"p001, p002","p001 description, p002 description"
#		p001,P,"i001","i001 description"
#		i009,S,"",""
#
#  ________________________________________________________________
# | Workfile:      | xxadeo_catmant_item_packs.ksh 				  		  |
# | Created by:    | PV                                     	    |
# | Creation Date: | 20180529-1400                          	    |
# | Modify Date:   | 20181212-1400                          	    |
# | Version:       | 0.5                                    	    |
#  ----------------------------------------------------------------
# History:
#	0.1 - Initial Release
#	0.2 - Output structure has been changed
#	0.3 - Corrections according Adeo ENV RETL Standards
#		  (Names, Variables,...)
#	0.4 - Bug Fixing: BUG_648_656 - The changes are:
#		- Filter Items only to Level 1;
#		- Change the name of Input file about Packs;
#		- Clean Up of the auxiliary files		
#	0.5 - Bug Fixing: BUG_747 - Remove Comma from items that
#		are a singles items without packs.
#
#
###################################################################

################## PROGRAM DEFINITIONS ####################

export PROGRAM_NAME="xxadeo_catmant_item_packs"

##########                                       ##########
######################## INCLUDES #########################

# Environment configuration script
#. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env	# (Nearshore ENV)
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env	# (Adeo ENV - RPAS)

# Script Library Collection
#. ${LIB_DIR}/rmse_rpas_lib.ksh						# (Nearshore ENV)
. ${LIB_DIR}/xxadeo_rpas_lib.ksh					# (Adeo ENV - RPAS)

# Only to Testing with CATMAN_DOMAIN
#export CATMAN_DOMAIN=${DATA_DIR}/CATMAN_DOMAIN


##########										##########
##################  VALIDATIONS #####################


##########                                      ##########
###################  INPUT/OUTPUT DEFINES ######################

#	**Adeo ENV - RPAS**

#	Input Folders on RPAS System
INPUT_DIR=$I_DATA_DIR		
INPUT_DIR_XXADEO=$I_DATA_DIR_XXADEO	

#	Output Folders on RPAS System
OUTPUT_DIR=$O_DATA_DIR
OUTPUT_DIR_XXADEO=$O_DATA_DIR_XXADEO

export FIELD_SEPARATOR=0x2020
export STRING_FIELD_DELIMIT_CHAR=\"        # \ escapes for ksh
export STRING_FIELD_DELIMIT_CHAR_ESCAPED=\"\"
export DATE_FORMAT=YYYYMMDDHHMISS

#	Find the latest input file about Pack Items
INPUT_FILE_PACKITEMS=`ls $INPUT_DIR_XXADEO | sort -r | grep 'XXRMS221_ITEMPACK_.*\.dat' | head -1`

export INPUT_FILE_ITEMS=$INPUT_DIR/rmse_rpas_item_master.dat
export INPUT_SCHEMA_FILE_ITEMS=$SCHEMA_DIR/rmse_rpas_item_master.schema					# Base schema from (RETLforRPAS)

#export INPUT_FILE_PACKS=$INPUT_DIR_XXADEO/nb_rpas_rmse_packitem.dat
export INPUT_FILE_PACKS=$INPUT_DIR_XXADEO/$INPUT_FILE_PACKITEMS
export INPUT_SCHEMA_FILE_PACKS=$SCHEMA_DIR_XXADEO/xxadeo_nb_rpas_rmse_packitem.schema

export OUTPUT_FILE=$OUTPUT_DIR_XXADEO/rpas_pack_item.csv.rpl
export OUTPUT_SCHEMA_FILE=$SCHEMA_DIR_XXADEO/${PROGRAM_NAME}.schema

ITEM_FIELDS="ITEM_PARENT ITEM_GRANDPARENT ITEM_LEVEL TRAN_LEVEL SUBCLASS CLASS DEPT FORECAST_IND SUPPLIER DIFF_1_TYPE DIFF_1 DIFF_DESC_1 DIFF_FILE_POSITION_1 DIFF_1_AGGREGATE_IND DIFF_2_TYPE DIFF_2 DIFF_DESC_2 DIFF_FILE_POSITION_2 DIFF_2_AGGREGATE_IND DIFF_3_TYPE DIFF_3 DIFF_DESC_3 DIFF_FILE_POSITION_3 DIFF_3_AGGREGATE_IND DIFF_4_TYPE DIFF_4 DIFF_DESC_4 DIFF_FILE_POSITION_4 DIFF_4_AGGREGATE_IND"

message "[INFO] INPUT_FILE_1 = $INPUT_FILE_ITEMS
		INPUT_FILE_2 = $INPUT_FILE_PACKS
		OUTPUT_FILE  = $OUTPUT_FILE
	"

##########                                      ##########
##################  CREATE RETL FLOWS ####################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">

	<!-- ##
		## ITEMS
		##
	-->

	<!-- IMPORT: Items -->
	<OPERATOR type="import">
		<PROPERTY name="inputfile" value="${INPUT_FILE_ITEMS}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_ITEMS}"/>
		<OUTPUT    name="items.v"/>
	</OPERATOR>
 	
	<!-- FILTER:
                Item Master - Keep only the items that Item Level equal 1
                output 1: follows the filter condition (ITEM_LEVEL == 1)
                output 2: rejects the filter condition (ITEM_LEVEL != 1)
        -->
        <OPERATOR type="filter">
                <INPUT name="items.v"/>
                <PROPERTY name="filter" value="ITEM_LEVEL EQ 1"/>
                <PROPERTY name="rejects" value="true"/>
                <OUTPUT name="items_Level1.v"/>
                <OUTPUT name="items_NotLevel1.v"/>
        </OPERATOR>

        <OPERATOR type="debug">
                <INPUT name="items_NotLevel1.v"/>
        </OPERATOR>


	<!-- DROP: Remove fields useless -->
	<OPERATOR type="fieldmod" >
		<INPUT    name="items_Level1.v"/>
		<PROPERTY name="drop" value="${ITEM_FIELDS}"/>
		<OUTPUT name="itemIDs.v" />
	</OPERATOR>
	
	<!-- SORT: Items by ItemID -->
	<OPERATOR type="sort" >
		<INPUT    name="itemIDs.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="all_Items_hasSorted.v"/>
	</OPERATOR>

	<!-- COPY: Duplicate All Items -->
	<OPERATOR type="copy">
		<INPUT name="all_Items_hasSorted.v"/>
		<OUTPUT name="all_Items_1.v"/>
		<OUTPUT name="all_Items_2.v"/>
	</OPERATOR>

	<!-- ##
		## ITEMS ARE PACKS
		##
	-->

	<!-- IMPORT: Items are Packs -->
	<OPERATOR type="import">
		<PROPERTY name="inputfile" value="${INPUT_FILE_PACKS}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_PACKS}"/>
		<OUTPUT    name="itemsArePacks.v"/>
	</OPERATOR>

	<!-- RENAME: Change Column names for after to allow to join files as we want.
				So, We change ITEM_IS_PACK to ITEM for join both files to found which items are packs on the item_master.
					This result allow after grouping these items by componentes.
						Ex.: i001 is component of p004. where p004 is a pack and a item i004.
					We change ITEM to ITEM_ID first for doesn't conflit with other.
	-->
	<OPERATOR type="fieldmod" >
		<INPUT    name="itemsArePacks.v"/>
		<PROPERTY name="rename" value="ITEM_ID=ITEM"/>
		<PROPERTY name="rename" value="ITEM=ITEM_IS_PACK"/>
		<OUTPUT name="itemsArePacks_Rename.v" />
	</OPERATOR>

	<!-- SORT: Packs by ITEM (After it was a ITEM_IS_PACK) -->
	<OPERATOR type="sort" >
		<INPUT    name="itemsArePacks_Rename.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="itemsArePacks_hasSorted.v"/>
	</OPERATOR>

	<!-- COPY: Duplicate All Items Packs-->
	<OPERATOR type="copy">
		<INPUT name="itemsArePacks_hasSorted.v"/>
		<OUTPUT name="itemsArePacks_1.v"/>
		<OUTPUT name="itemsArePacks_2.v"/>
	</OPERATOR>

	<!-- ##
		## JOIN AND FILTER TWO FILES
		##
	-->

	<!-- By: #######################
				ITEM - PACK -->

	<!-- JOIN: Join between Items and Packs by ITEM & PACK -->
	<OPERATOR type="leftouterjoin" name="leftouterjoin,0">
		<INPUT name="all_Items_1.v"/>
		<INPUT name="itemsArePacks_1.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="itemsAndItemsArePacks_hasJoined.v"/>
	</OPERATOR>

	<!-- RENAME:  Organize joined files to ITEM_IS_PACK	ITEM_ID	ITEM_DESC	-->
	<OPERATOR type="fieldmod" >
		<INPUT    name="itemsAndItemsArePacks_hasJoined.v"/>
		<PROPERTY name="rename" value="ITEM_IS_PACK=ITEM"/>
		<OUTPUT name="itemsAndItemsArePacks_hasJoined_Rename.v" />
	</OPERATOR>

	<!-- FILTER: Identify which Items that are inside of packs -->
	<OPERATOR type="filter">
		<INPUT name="itemsAndItemsArePacks_hasJoined_Rename.v"/>
		<PROPERTY name="filter" value="ITEM_ID IS_NOT_NULL AND QTD IS_NOT_NULL "/>
		<PROPERTY name="rejects" value="true"/>
		<OUTPUT name="itemsWithPacks_byItem_Pack.v"/>
		<OUTPUT name="rejects_byItem_Packm.v"/> <!-- rejecteds -->
	</OPERATOR>

	<!-- SORT: Items Packs by ITEM_ID to found which packs that the item is a componente -->
	<OPERATOR type="sort" >
		<INPUT    name="itemsWithPacks_byItem_Pack.v"/>
		<PROPERTY name="key" value="ITEM_ID"/>
		<OUTPUT name="itemsWithPacks_byItem_Pack_hasSorted.v"/>
	</OPERATOR>

	<!-- EXPORT: Items and Packs. This exported file allow to identify which are the packs of the items. -->
	<OPERATOR type="export">
		<INPUT    name="itemsWithPacks_byItem_Pack_hasSorted.v"/>
		<PROPERTY name="outputfile" value="${OUTPUT_FILE}.ByItems"/>
		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
	</OPERATOR>

	<!-- #######################
			By:  ITEM - ITEM -->

	<!-- RENAME: Change Column names for after to allow to join files as we want.
				So, We change ITEM to ITEM_ID of the item_master input file to allow a join between item_master and item_packs to find the packs and your componentes.
	-->
	<OPERATOR type="fieldmod" >
		<INPUT    name="all_Items_2.v"/>
		<PROPERTY name="rename" value="ITEM_ID=ITEM"/>
		<OUTPUT name="all_Items_2_Rename.v" />
	</OPERATOR>

	<!-- SORT: Packs by ITEM_ID (After it was a ITEM) -->
	<OPERATOR type="sort" >
		<INPUT    name="itemsArePacks_2.v"/>
		<PROPERTY name="key" value="ITEM_ID"/>
		<OUTPUT name="itemsArePacks_2_hasSorted.v"/>
	</OPERATOR>

	<!-- JOIN: Join between Items and Packs by ITEM_ID  -->
	<OPERATOR type="leftouterjoin" name="leftouterjoin,0">
		<INPUT name="all_Items_2_Rename.v"/>
		<INPUT name="itemsArePacks_2_hasSorted.v"/>
		<PROPERTY name="key" value="ITEM_ID"/>
		<OUTPUT name="itemsAndItemsArePacks_hasJoinedByItem.v"/>
	</OPERATOR>

	<!-- RENAME:  Organize joined files to ITEM_IS_PACK	ITEM_ID	ITEM_DESC -->
	<OPERATOR type="fieldmod" >
		<INPUT    name="itemsAndItemsArePacks_hasJoinedByItem.v"/>
		<PROPERTY name="rename" value="ITEM_IS_PACK=ITEM"/>
		<OUTPUT name="itemsAndItemsArePacks_hasJoinedByItem_Rename.v" />
	</OPERATOR>

	<!-- FILTER: Identify which Items that are inside of packs -->
	<OPERATOR type="filter">
		<INPUT name="itemsAndItemsArePacks_hasJoinedByItem_Rename.v"/>
		<PROPERTY name="filter" value="ITEM_IS_PACK IS_NOT_NULL AND QTD IS_NOT_NULL "/>
		<PROPERTY name="rejects" value="true"/>
		<OUTPUT name="itemsWithPacks_byItem_Item.v"/>
		<OUTPUT name="rejects_byItem_Item.v"/> <!-- rejecteds -->
	</OPERATOR>

	<!-- SORT: Items Packs by ITEM_IS_PACK to found which items are an element of pack -->
	<OPERATOR type="sort" >
		<INPUT    name="itemsWithPacks_byItem_Item.v"/>
		<PROPERTY name="key" value="ITEM_IS_PACK"/>
		<OUTPUT name="itemsWithPacks_byItem_Item_hasSorted.v"/>
	</OPERATOR>

	<!-- EXPORT: Items and Packs. This exported file allow to identify which are items of belong of a pack. -->
	<OPERATOR type="export">
		<INPUT    name="itemsWithPacks_byItem_Item_hasSorted.v"/>
		<PROPERTY name="outputfile" value="${OUTPUT_FILE}.ByPacks"/>
		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
	</OPERATOR>

	<!-- #######################
			By:
				ITEM - NOT PACK AND NOT INSIDE PACK
	-->

	<!-- JOIN between Rejects of the ITEM-ITEM and ITEM-PACK -->

	<!-- RENAME:  Rename the columns to make a join between rejected files on the filters has been made above. -->
	<OPERATOR type="fieldmod" >
		<INPUT    name="rejects_byItem_Packm.v"/>
		<PROPERTY name="rename" value="ITEM_ID_OLD=ITEM_ID"/>
		<PROPERTY name="rename" value="ITEM_ID=ITEM_IS_PACK"/>
		<OUTPUT name="rejects_byItem_Item_Rename.v" />
	</OPERATOR>

	<OPERATOR type="sort" >
		<INPUT    name="rejects_byItem_Item_Rename.v"/>
		<PROPERTY name="key" value="ITEM_ID"/>
		<OUTPUT name="rejects_byItem_Item_Rename_hasSorted.v"/>
	</OPERATOR>

	<OPERATOR type="sort" >
		<INPUT    name="rejects_byItem_Item.v"/>
		<PROPERTY name="key" value="ITEM_ID"/>
		<OUTPUT name="rejects_byItem_Item_hasSorted.v"/>
	</OPERATOR>

	<!-- JOIN: This join allow to identify which items does not are a pack and doesn't belong some pack. -->
	<OPERATOR type="innerjoin" name="innerjoin,0">
		<INPUT name="rejects_byItem_Item_Rename_hasSorted.v"/>
		<INPUT name="rejects_byItem_Item_hasSorted.v"/>
		<PROPERTY name="key" value="ITEM_ID"/>
		<OUTPUT name="items_NotPack.v"/>
	</OPERATOR>

	<!-- EXPORT: Items. This exported file allow to identify which items doesn't are a pack and doesn't belong some pack. -->
	<OPERATOR type="export">
		<INPUT    name="items_NotPack.v"/>
		<PROPERTY name="outputfile" value="${OUTPUT_FILE}.notPacks"/>
		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
	</OPERATOR>

</FLOW>
EOF

##########                                      ##########
#################  EXECUTE RETL FLOWS ####################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

retl_stat=$?

##########                                      ##########
#################  PROCESS THE FILES  ####################

#	Get/Process Packs of Items - Organized by Items
awk -F'\x1F' -v item_1="" -v item_2="" -v packs="" -v packsDesc="" ' { item_2=$2; if( length(item_1) == 0 ){ item_1=$2; }; if( item_1 == item_2 && item_2 != "" ){ if(packs != ""){ packs = packs " "; } packs = packs $1","; if(packsDesc != ""){ packsDesc = packsDesc " "; } packsDesc = packsDesc $3","; }else{ sub(",$","",packs); sub(",$","",packsDesc); print item_1",C,\""packs"\",\""packsDesc"\""; item_1=$2; packs=$1","; packsDesc=$3","; }; }; END{ if( item_1 != ""){ sub(",$","",packs); sub(",$","",packsDesc); print item_1",C,\""packs"\",\""packsDesc"\""; }; }' ${OUTPUT_FILE}.ByItems > ${OUTPUT_FILE}

#	Get/Process Items of Packs - Organized by Packs
awk -F'\x1F' -v pack_1="" -v pack_2="" -v items="" -v itemsDesc="" ' { pack_2=$1; if( length(pack_1) == 0 ){ pack_1=$1; }; if( pack_1 == pack_2 && pack_2 != "" ){ if(items != ""){ items = items " "; } items = items $2","; if(itemsDesc != ""){ itemsDesc = itemsDesc " "; } itemsDesc = itemsDesc $3","; }else{ sub(",$","",items); sub(",$","",itemsDesc); print pack_1",P,\""items"\",\""itemsDesc"\""; pack_1=$1; items=$2","; itemsDesc=$3","; }; }; END{ if( pack_1 != ""){ sub(",$","",items); sub(",$","",itemsDesc); print pack_1",P,\""items"\",\""itemsDesc"\""; }; }' ${OUTPUT_FILE}.ByPacks >> ${OUTPUT_FILE}

#	Get/Process Items without Packs
awk -F'\x1F' ' { if( $2 != "" ){ print $2 ",S,,"; }; }' ${OUTPUT_FILE}.notPacks >> ${OUTPUT_FILE}

##########                                      ##########
####################### ClEAN UP #########################

rm -f ${OUTPUT_FILE}.ByItems
rm -f ${OUTPUT_FILE}.ByPacks
rm -f ${OUTPUT_FILE}.notPacks

##########                                      ##########
#################  REMOVE STATUS FILE ####################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

##########                                      ##########
###################  REPORT AND EXIT #####################

log_num_recs ${OUTPUT_FILE}

case $retl_stat in
  0 )
	 msg="Program completed successfully with" ;;
  * )
	 msg="Program exited with error" ;;
esac

message "$msg code: $retl_stat"
rmse_terminate $retl_stat
