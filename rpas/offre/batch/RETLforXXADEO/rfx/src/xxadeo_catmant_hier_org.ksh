#CP EDIT 9

#!/bin/ksh

### INTRO #####################################################################
#  ____________________________________________________________________________
#  Workfile:             | xxadeo_catmant_hier_org.ksh
#  Created by:           | PR <paulo.reis@oracle.com>
#  Create Datetime:      | 20180522-1400
#  Last Updated by:      | PVI <pedro.vieira@oracle.com>
#  Last Update Datetime: | 20181026-1640
#  Version:              | 1.4
#
#  Interface IDs:
#  --------------
#  RICEW #: 267 / 1054
#  IR ID: IR00317 / IR0182
#
#  This script exports Organisational Hierarchy info from RMS to CatMan and
#  other RPAS modules.
#
#  Usage: xxadeo_catmant_hier_org.ksh
#
#  Expected files:
#    1. transformation/loc.csv.dat
#    2. hierarchies/locr.csv.dat
#
#  History:
#       1.3 - Added Includes and Variables according Adeo Env
#	1.4 - BUG_616: Wrong output folder to files. We Need to put the files
#		under the static folder $O_DATA_DIR_XXADEO wihtout subfolders. 
#
###############################################################################

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_catmant_hier_org"

### INCLUDES ##################################################################

#RETLforXXADEO=<uncomment and set as needed>
[[ -z $RETLforXXADEO ]] && RETLforXXADEO=$RMS_RPAS_HOME
export RETL_ENV=$RETLforXXADEO

## Environment configuration script
#. ${RETL_ENV}/rfx/etc/rmse_rpas_config.env
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env			# (Adeo ENV - RPAS)

## Script Library Collection
#. ${LIB_DIR}/rmse_rpas_lib.ksh
#. ${LIB_DIR}/rmse_rpas_error_check.ksh
. ${LIB_DIR}/xxadeo_rpas_lib.ksh							# (Adeo ENV - RPAS)

### INPUT / OUTPUT DEFINITIONS ################################################

#systemIN=<uncomment and set as needed>
#[[ -z $systemIN ]] && systemIN="CATMAN"

#if [ -z $RETL_IN ] ; then
#	RETL_IN=${RETL_ENV}/data
#	export RETL_IN=$RETL_IN
#fi
#export INBOUND_DIR=$RETL_IN/${systemIN}
#$(mkdir -p ${INBOUND_DIR})
export INBOUND_DIR=${I_DATA_DIR}						# Adeo ENV (Base RPAS Input Dir)
export INBOUND_DIR_CUST=${I_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Input Dir)


#systemOUT=<uncomment and set as needed>
#[[ -z $systemOUT ]] && systemOUT="CATMAN"

#if [ -z $RETL_OUT ] ; then
#	RETL_OUT=${RETL_ENV}/data
#	export RETL_OUT=$RETL_OUT
#fi
#export OUTBOUND_DIR=$RETL_OUT/${systemOUT}
#$(mkdir -p ${OUTBOUND_DIR})
export OUTBOUND_DIR=${O_DATA_DIR}						# Adeo ENV (Base RPAS Output Dir)
export OUTBOUND_DIR_CUST=${O_DATA_DIR_XXADEO}			# Adeo ENV (Custom RPAS Output Dir)


#export STAGING_DIR=${INBOUND_DIR}
export STAGING_DIR=${INBOUND_DIR_CUST}					# Adeo ENV
#export OUTPUT_DIR=${OUTBOUND_DIR}/transformation
#export OUTPUT_DIR=${OUTBOUND_DIR_CUST}/transformation	# Adeo ENV
export OUTPUT_DIR=${OUTBOUND_DIR_CUST}			# Adeo ENV
$(mkdir -p ${OUTPUT_DIR})

#export OUTPUT_DIR_RHS=${INBOUND_DIR}/hierarchies
#export OUTPUT_DIR_RHS=${INBOUND_DIR_CUST}/hierarchies	# Adeo ENV
export OUTPUT_DIR_RHS=${INBOUND_DIR_CUST}		# Adeo ENV
$(mkdir -p ${OUTPUT_DIR_RHS})

#export INPUT_FILE_1=${RETL_IN}/rmse_rpas_orghier.dat
export INPUT_FILE_1=${INBOUND_DIR}/rmse_rpas_orghier.dat	# Adeo ENV
#export INPUT_FILE_2=${RETL_IN}/rmse_rpas_store.dat
export INPUT_FILE_2=${INBOUND_DIR}/rmse_rpas_store.dat		# Adeo ENV
export INPUT_FILE_SCHEMA_1=${SCHEMA_DIR}/rmse_rpas_orghier.schema
export INPUT_FILE_SCHEMA_2=${SCHEMA_DIR}/rmse_rpas_store.schema
export INPUT_REJECTED_FILE_1=${STAGING_DIR}/rmse_rpas_orghier.rej
export INPUT_REJECTED_FILE_2=${STAGING_DIR}/rmse_rpas_store.rej

#export OUTPUT_FILE=${OUTPUT_DIR}/loc.csv.dat
export OUTPUT_FILE=${OUTPUT_DIR_RHS}/loc.csv.dat
#export OUTPUT_SCHEMA_FILE=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export OUTPUT_SCHEMA_FILE=${SCHEMA_DIR_XXADEO}/${PROGRAM_NAME}.schema	# Adeo ENV

export OUTPUT_FILE_RHS=${OUTPUT_DIR_RHS}/locr.csv.dat

###  CREATE RETL FLOWS ########################################################

SEPARATOR=" "

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!-- INPUT File 1 -->
	<OPERATOR type="import" name="input_file_1">
	<PROPERTY name="inputfile" value="${INPUT_FILE_1}"/>
	<PROPERTY name="schemafile" value="${INPUT_FILE_SCHEMA_1}"/>
	<PROPERTY name="rejectfile" value="${INPUT_REJECTED_FILE_1}"/>
	<OUTPUT name="output_file_1.v"/>
	</OPERATOR>

	<OPERATOR type="sort">
	<PROPERTY name="key" value="DISTRICT"/>
	<INPUT name="output_file_1.v"/>
	<OUTPUT name="output_file_1_sorted.v"/>
	</OPERATOR>

<!-- INPUT File 2 -->
	<OPERATOR type="import" name="input_file_2">
	<PROPERTY name="inputfile" value="${INPUT_FILE_2}"/>
	<PROPERTY name="schemafile" value="${INPUT_FILE_SCHEMA_2}"/>
	<PROPERTY name="rejectfile" value="${INPUT_REJECTED_FILE_2}"/>
	<OUTPUT name="output_file_2.v"/>
	</OPERATOR>

	<OPERATOR type="sort">
	<PROPERTY name="key" value="DISTRICT"/>
	<INPUT name="output_file_2.v"/>
	<OUTPUT name="output_file_2_sorted.v"/>
	</OPERATOR>

<!-- Join INPUT file 1, INPUT file 2 -->
	<OPERATOR type="innerjoin" name="joined_files_1_2">
	<INPUT name="output_file_1_sorted.v"/>
	<INPUT name="output_file_2_sorted.v"/>
	<PROPERTY name="key" value="DISTRICT"/>
	<OUTPUT name="joined_files_1_2.v"/>
	</OPERATOR>

<!-- Filter allowed stores as per store_format requirements -->
	<OPERATOR type="filter">
		<INPUT name="joined_files_1_2.v"/>
		<PROPERTY name="filter" value="
		STORE_FORMAT EQ 10 OR
		STORE_FORMAT EQ 20
		"/>
		<OUTPUT name="allowed_stores.v"/>
	</OPERATOR>

<!-- Generate output structure and Parse source fields to match target requirements -->
	<OPERATOR type="generator">
		<INPUT name="allowed_stores.v"/>
		<PROPERTY name="schema">
		<![CDATA[
		<GENERATE>
		  <FIELD name="STORE_LABEL"    nullable="false" maxlength="150" type="string"><CONST value=" "/></FIELD>
		  <FIELD name="DISTRICT_LABEL" nullable="true" maxlength="150"  type="string"><CONST value=" "/></FIELD>
		  <FIELD name="REGION_LABEL"   nullable="true" maxlength="120"  type="string"><CONST value=" "/></FIELD>
		  <FIELD name="BU"             nullable="true" maxlength="11"   type="string"><CONST value=" "/></FIELD>
		  <FIELD name="BU_LABEL"       nullable="true" maxlength="120"  type="string"><CONST value=" "/></FIELD>
		  <FIELD name="ZONE"           nullable="false" maxlength="11"  type="string"><CONST value=" "/></FIELD>
		  <FIELD name="ZONE_LABEL"     nullable="false" maxlength="120" type="string"><CONST value=" "/></FIELD>
		  <FIELD name="GROUPE"         nullable="false" maxlength="11"  type="string"><CONST value=" "/></FIELD>
		  <FIELD name="GROUPE_LABEL"   nullable="false" maxlength="120" type="string"><CONST value=" "/></FIELD>
		</GENERATE>
		]]>
		</PROPERTY>
		<OUTPUT name="generator_output.v"/>
	</OPERATOR>

	<OPERATOR type="parser">
		<INPUT name="generator_output.v"/>
		<PROPERTY name="expression">
			<![CDATA[
			RECORD.STORE_LABEL = RECORD.STORE;
			RECORD.STORE_LABEL += "${SEPARATOR}";
			RECORD.STORE_LABEL += RECORD.STORE_NAME;

			RECORD.DISTRICT_LABEL = RECORD.DISTRICT;
			RECORD.DISTRICT_LABEL += "${SEPARATOR}";
			RECORD.DISTRICT_LABEL += RECORD.DISTRICT_NAME;

			RECORD.REGION_LABEL = RECORD.REGION;
			RECORD.REGION_LABEL += "${SEPARATOR}";
			RECORD.REGION_LABEL += RECORD.REGION_NAME;

			RECORD.BU = RECORD.AREA;

			RECORD.BU_LABEL = RECORD.AREA;
			RECORD.BU_LABEL += "${SEPARATOR}";
			RECORD.BU_LABEL += RECORD.AREA_NAME;

			RECORD.ZONE = RECORD.CHAIN;

			RECORD.ZONE_LABEL = RECORD.CHAIN;
			RECORD.ZONE_LABEL += "${SEPARATOR}";
			RECORD.ZONE_LABEL += RECORD.CHAIN_NAME;

			RECORD.GROUPE = RECORD.COMPANY;

			RECORD.GROUPE_LABEL = RECORD.COMPANY;
			RECORD.GROUPE_LABEL += "${SEPARATOR}";
			RECORD.GROUPE_LABEL += RECORD.CO_NAME;
			]]>
		</PROPERTY>
	<OUTPUT name="target.v"/>
	</OPERATOR>

<!-- Export transformation to target file  -->
    <OPERATOR type="export">
		<INPUT    name="target.v"/>
		<PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
    </OPERATOR>
</FLOW>
EOF

### EXECUTE RETL FLOWS ########################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

### REMOVE STATUS FILE ########################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

### REMOVE BLANK REJECTED FILE ################################################

## Remove if blank
if [ `cat ${INPUT_REJECTED_FILE_1} | wc -l` -eq 0 ]; then rm ${INPUT_REJECTED_FILE_1}; fi
if [ `cat ${INPUT_REJECTED_FILE_2} | wc -l` -eq 0 ]; then rm ${INPUT_REJECTED_FILE_2}; fi

### CREATE/DUPLICATE FILE FOR RHS #############################################

cp ${OUTPUT_FILE} ${OUTPUT_FILE_RHS}

###  REPORT AND EXIT ##########################################################

case $retl_stat in
  0 )
     msg="Program completed successfully with" ;;
  * )
     msg="Program exited with error" ;;
esac

message "$msg code: $retl_stat"
rmse_terminate $retl_stat
