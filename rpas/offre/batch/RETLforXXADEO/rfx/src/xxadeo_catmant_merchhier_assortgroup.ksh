#!/bin/ksh

### INTRO #####################################################################
#  ____________________________________________________________________________
#  Workfile:             | xxadeo_catmant_merchhier_assortgroup.ksh
#  Created by:           | PR <paulo.reis@oracle.com>
#  Create Datetime:      | 20180608-1600
#  Last Updated by:      | PVI <pedro.vieira@oracle.com>
#  Last Update Datetime: | 20181204-1800
#  Version:              | 1.4
#
#  Interface IDs:
#  --------------
#  RICEW #: 264 / 265
#  IR ID: IR0079
#
#  This auxiliary script (related to the IR ID above) creates assortment groups
#  relating PACKs, Items and Related Item information, extracted from RMS.
#  
#  This ksh call a perl script that process and generate the assortment groups.
#  (xxadeo_catmant_merchhier_assortgroup.pl)
#
#  Usage: xxadeo_catmant_merchhier.ksh
#
#  Expected file:
#    1. assortGroup.dat
#
#  History:
#       1.2 - Added/Changed Includes and Variables according Adeo Env
#           - Changed the script name according the Adeo Standards
#	1.3 - Bug Fixing: BUG_633_634_635_658 - The changes are:
#		- Re-Work that interface. Changed from shell to perl.
#		- Perfomance fixed.
#		- Assortment Group ID fixed.
#	1.4 - Bug Fixing: The changes are:
#		- Item Pack files have items on the status 'W'. Remove that items.
#
###############################################################################

### PROGRAM DEFINITIONS #######################################################

export PROGRAM_NAME="xxadeo_catmant_merchhier_assortgroup"

### INCLUDES ##################################################################

. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env			# (Adeo ENV - RPAS)

. ${LIB_DIR}/xxadeo_rpas_lib.ksh							# (Adeo ENV - RPAS)

### INPUT/OUTPUT DEFINES ######################################################

#	Input Folders on RPAS System
INPUT_DIR=$I_DATA_DIR
INPUT_DIR_XXADEO=$I_DATA_DIR_XXADEO

#	Output Folders on RPAS System
OUTPUT_DIR=$O_DATA_DIR
OUTPUT_DIR_XXADEO=$O_DATA_DIR_XXADEO

#	Find the latest input file about Pack Items
INPUT_FILE_PACKITEMS=`ls $INPUT_DIR_XXADEO | sort -r | grep 'XXRMS221_ITEMPACK_.*\.dat\$' | head -1`

export INPUT_FILE_ITEMS=$INPUT_DIR/rmse_rpas_item_master.dat
export INPUT_SCHEMA_FILE_ITEMS=$SCHEMA_DIR/rmse_rpas_item_master.schema

export INPUT_FILE_PACKS=$INPUT_DIR_XXADEO/$INPUT_FILE_PACKITEMS
export INPUT_SCHEMA_FILE_PACKS=$SCHEMA_DIR_XXADEO/xxadeo_nb_rpas_rmse_packitem.schema

export OUTPUT_FILE=$INPUT_DIR_XXADEO/${INPUT_FILE_PACKITEMS}.filter
export OUTPUT_SCHEMA_FILE=$INPUT_SCHEMA_FILE_PACKS

### RETL FLOW DEFINITION ######################################################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">

  <!-- IMPORT:
        Items
  -->
  <OPERATOR type="import">
    <PROPERTY name="inputfile" value="${INPUT_FILE_ITEMS}"/>
    <PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_ITEMS}"/>
    <OUTPUT    name="items.v"/>
  </OPERATOR>

  <!-- FILTER:
          Item Master - Keep only the items that Item Level equal 1
          output : follows the filter condition (ITEM_LEVEL == 1)
    -->
	<OPERATOR type="filter">
			<INPUT name="items.v"/>
			<PROPERTY name="filter" value="ITEM_LEVEL EQ 1"/>
			<OUTPUT name="items_Level1.v"/>
	</OPERATOR>

  <!-- FIELDMOD:
        Keep only the Item field
  -->
	<OPERATOR type="fieldmod" >
		<INPUT    name="items_Level1.v"/>
		<PROPERTY name="keep" value="ITEM"/>
		<OUTPUT name="itemIDs.v" />
	</OPERATOR>

  <!-- SORT:
        Items by ItemID -->
	<OPERATOR type="sort" >
		<INPUT    name="itemIDs.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="all_Items_hasSorted.v"/>
	</OPERATOR>

  <!-- COPY:
        Duplicate All Items
  -->
	<OPERATOR type="copy">
		<INPUT name="all_Items_hasSorted.v"/>
		<OUTPUT name="all_Items_1.v"/>
		<OUTPUT name="all_Items_2.v"/>
	</OPERATOR>

  <!-- IMPORT:
      Items and Packs
  -->
  <OPERATOR type="import">
    <PROPERTY name="inputfile" value="${INPUT_FILE_PACKS}"/>
    <PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE_PACKS}"/>
    <OUTPUT    name="itemsAndPacks.v"/>
  </OPERATOR>

  <!-- SORT:
        Items and Packs by Item -->
	<OPERATOR type="sort" >
		<INPUT    name="itemsAndPacks.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="itemsAndPacks_hasSorted_ByItem.v"/>
	</OPERATOR>

  <!-- JOIN:
      Join between Items and Packs by ITEM
  -->
	<OPERATOR type="innerjoin">
    <INPUT name="itemsAndPacks_hasSorted_ByItem.v"/>
		<INPUT name="all_Items_1.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="itemsAndPacks_hasJoined_ByItem.v"/>
	</OPERATOR>

  <!-- FIELDMOD:
        Rename columns:
          - ITEM to ITEM_ID
          - ITEM_IS_PACK to ITEM
  -->
	<OPERATOR type="fieldmod" >
		<INPUT    name="itemsAndPacks_hasJoined_ByItem.v"/>
		<PROPERTY name="rename" value="ITEM_ID=ITEM"/>
    <PROPERTY name="rename" value="ITEM=ITEM_IS_PACK"/>
		<OUTPUT name="itemsAndPacks_rename.v" />
	</OPERATOR>

  <!-- SORT:
        Items and Packs by ItemID -->
	<OPERATOR type="sort" >
		<INPUT    name="itemsAndPacks_rename.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="itemsAndPacks_hasSorted_ByPackItem.v"/>
	</OPERATOR>

  <!-- JOIN:
      Join between Items and Packs by ITEM
  -->
	<OPERATOR type="innerjoin">
  	<INPUT name="itemsAndPacks_hasSorted_ByPackItem.v"/>
		<INPUT name="all_Items_2.v"/>
		<PROPERTY name="key" value="ITEM"/>
		<OUTPUT name="itemsAndPacks_hasJoined_ByPackItem.v"/>
	</OPERATOR>

  <!-- FIELDMOD:
        Rename columns:
          - ITEM to ITEM_ID
          - ITEM_IS_PACK to ITEM
  -->
	<OPERATOR type="fieldmod" >
		<INPUT    name="itemsAndPacks_hasJoined_ByPackItem.v"/>
    <PROPERTY name="rename" value="ITEM_IS_PACK=ITEM"/>
		<PROPERTY name="rename" value="ITEM=ITEM_ID"/>
		<OUTPUT name="itemPacksApprovedLevel1.v" />
	</OPERATOR>

  <!-- EXPORT:
      Items and Packs already approved and level 1.
  -->
  <OPERATOR type="export">
    <INPUT    name="itemPacksApprovedLevel1.v"/>
    <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
    <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
  </OPERATOR>

</FLOW>
EOF

### MAIN SCRIPT   #############################################################

#   RETL FLOW

message "Exec RETL flow..."

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

retl_stat=$?

#   PERL FLOW

message "Call Perl Program..."

$SRC_DIR/${PROGRAM_NAME}.pl

perl_stat=$?

checkerror -e $perl_stat -m "Program failed - check ${ERR_FILE}"

### REMOVE STATUS FILE #######################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

### CLEAN UP	###### #######################################################

rm ${OUTPUT_FILE}

###  REPORT AND EXIT ##########################################################

case $perl_stat in
  0 )
     msg="Program completed successfully with" ;;
  * )
     msg="Program exited with error" ;;
esac

message "$msg code: $perl_stat"
rmse_terminate $perl_stat
