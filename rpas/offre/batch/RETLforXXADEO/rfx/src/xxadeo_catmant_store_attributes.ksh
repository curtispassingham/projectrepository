#!/bin/ksh

###################################################################
#
#  This script Pre-Processing Store Dates and Attributes information
#  from RMS to RPAS modules.
#  The output of this script will be available and ready to load
#  inside CATMAN.
#
#  ________________________________________________________________
# | Workfile:      | xxadeo_catmant_store_attributes.ksh 		  |
# | Created by:    | PV                                     	  |
# | Creation Date: | 20180525-1200                          	  |
# | Modify Date:   | 20181212-1500                          	  |
# | Version:       | 0.3                                    	  |
#  ----------------------------------------------------------------
# History:
#		0.1 - Initial Release
#		0.2 - Corrections according Adeo ENV RETL Standards
#			  (Names, Variables,...)
#		0.3 - Bug Fixing: BUG_740 - Change the output file 
#			name
#
###################################################################

################## PROGRAM DEFINITIONS ####################

export PROGRAM_NAME="xxadeo_catmant_store_attributes"

##########                                       ##########
######################## INCLUDES #########################

# Environment configuration script
#. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env	# (Nearshore ENV)
. ${RETLforXXADEO}/rfx/etc/xxadeo_rpas_config.env	# (Adeo ENV - RPAS)

# Script Library Collection
#. ${LIB_DIR}/rmse_rpas_lib.ksh						# (Nearshore ENV)
. ${LIB_DIR}/xxadeo_rpas_lib.ksh					# (Adeo ENV - RPAS)

# Only to Testing with CATMAN_DOMAIN
#export CATMAN_DOMAIN=${DATA_DIR}/CATMAN_DOMAIN


##########										##########
################## VALIDATIONS #####################


##########                                      ##########
###################  INPUT/OUTPUT DEFINES ######################

#	**Adeo ENV - RPAS**

#INPUT_DIR=$RETL_IN/RPAS							# As I was thinking
INPUT_DIR=$I_DATA_DIR									# As Renato and your time Defined.
INPUT_DIR_XXADEO=$I_DATA_DIR_XXADEO		# As Renato and your time Defined.

#OUTPUT_DIR=$RETL_OUT/CATMAN					# As I was thinking
OUTPUT_DIR=$O_DATA_DIR								# As Renato and your time Defined.
OUTPUT_DIR_XXADEO=$O_DATA_DIR_XXADEO	# As Renato and your time Defined.

message "[INFO] The INPUT = $INPUT_DIR and OUTPUT = $OUTPUT_DIR "

export FIELD_SEPARATOR=0x2020
export STRING_FIELD_DELIMIT_CHAR=\"        # \ escapes for ksh
export STRING_FIELD_DELIMIT_CHAR_ESCAPED=\"\"
export DATE_FORMAT=YYYYMMDDHHMISS

export INPUT_FILE=$INPUT_DIR/rmse_rpas_store.dat
#export INPUT_SCHEMA_FILE=$SCHEMA_DIR/xxadeo_rmse_rpas_store.schema
export INPUT_SCHEMA_FILE=$SCHEMA_DIR/rmse_rpas_store.schema
export OUTPUT_FILE=$OUTPUT_DIR_XXADEO/attributsMag.csv.rpl
export OUTPUT_SCHEMA_FILE=$SCHEMA_DIR_XXADEO/${PROGRAM_NAME}.schema

ARRAY_STORE_FILTER=(10 15 20 25 30 40 50)
SIZE_A_ST_FILTER=${#ARRAY_STORE_FILTER[@]}	# get length of array
FIELD_STORE_FILTER="STORE_FORMAT"
EXPRESS_STORE_FILTER=""

#	Create Filter Expression Dynamic
for (( i=0; i<${SIZE_A_ST_FILTER}; i++ ))
do
	if [ "$i" -lt "$((${SIZE_A_ST_FILTER}-1))" ]; then
		EXPRESS_STORE_FILTER=${EXPRESS_STORE_FILTER}""${FIELD_STORE_FILTER}" EQ "${ARRAY_STORE_FILTER[$i]}" OR "
	else
		EXPRESS_STORE_FILTER=${EXPRESS_STORE_FILTER}""${FIELD_STORE_FILTER}" EQ "${ARRAY_STORE_FILTER[$i]}
	fi;
done

message "[INFO] The Filter Express= ${EXPRESS_STORE_FILTER}"

##########                                      ##########
##################  CREATE RETL FLOWS ####################

message "Program start..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

<FLOW name="${PROGRAM_NAME}.flw">
	<OPERATOR type="import">
		<PROPERTY name="inputfile" value="${INPUT_FILE}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_FILE}"/>
		<OUTPUT    name="inRMSStoreData.v"/>
	</OPERATOR>

	<OPERATOR type="filter">
		<INPUT name="inRMSStoreData.v"/>
		<PROPERTY name="filter" value="${EXPRESS_STORE_FILTER}"/>
		<PROPERTY name="rejects" value="true"/>
		<OUTPUT name="rmsStoreDataFilter.v"/>
		<OUTPUT name="storeDataReject.v"/>
	</OPERATOR>

	<OPERATOR type="export">
		<INPUT    name="rmsStoreDataFilter.v"/>
		<PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
	</OPERATOR>

	<OPERATOR type="export">
		<INPUT    name="storeDataReject.v"/>
		<PROPERTY name="outputfile" value="${OUTPUT_DIR}/${PROGRAM_NAME}.rej"/>
		<PROPERTY name="schemafile" value="${OUTPUT_SCHEMA_FILE}"/>
	</OPERATOR>

</FLOW>
EOF

##########                                      ##########
#################  EXECUTE RETL FLOWS ####################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

retl_stat=$?

# Deprecated
# More Transformations:
#	Dates - e.g.: 20140831 to 31/08/2014 . It's apply to fields WEEK_LABEL and DATE_LABEL
#	The dates necessary to transformation are STORE_OPEN_DATE, it's column number 2 and STORE_CLOSE_DATE, it's a column number 3
#awk -F',' -v OFS=',' '{ year = substr($2,1,4); month = substr($2,5,2); day = substr($2,7,2); newDt = day"/"month"/"year; $2 = newDt;
#						if( $3 != ""){ year = substr($3,1,4); month = substr($3,5,2); day = substr($3,7,2); newDt = day"/"month"/"year; $3 = newDt; } print $0; }' ${OUTPUT_FILE}.tmp > ${OUTPUT_FILE}

##########                                      ##########
#################  REMOVE STATUS FILE ####################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

rm ${OUTPUT_DIR}/${PROGRAM_NAME}.rej

##########                                      ##########
###################  REPORT AND EXIT #####################

case $retl_stat in
  0 )
	 msg="Program completed successfully with" ;;
  * )
	 msg="Program exited with error" ;;
esac

message "$msg code: $retl_stat"
rmse_terminate $retl_stat
