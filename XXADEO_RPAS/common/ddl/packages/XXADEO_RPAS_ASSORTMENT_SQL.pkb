create or replace package body xxadeo_rpas_assortment_sql is

  --
  --------------------------------------------------------------------------------
  --   GLOBAL VARIABLES
  --------------------------------------------------------------------------------
  lp_user_id  varchar2(30) := nvl(sys_context('RETAIL_CTX', 'APP_USER_ID'),
                                  user);
  g_stage_pre varchar2(10) := 'PRE';
  g_stage_pos varchar2(10) := 'POS';
  ----------------------------------------------------------------------------------
  procedure xxadeo_assort_cluster_insert(i_item              in xxadeo_assortment_cluster.item%type,
                                         i_bu                in xxadeo_assortment_cluster.bu%type,
                                         i_start_date        in xxadeo_assortment_cluster.start_date%type,
                                         i_cluster_id        in xxadeo_assortment_cluster.cluster_id%type,
                                         i_hq_recommendation in xxadeo_assortment_cluster.hq_recommendation%type,
                                         i_week              in xxadeo_assortment_cluster.week%type,
                                         i_create_id         in xxadeo_assortment_cluster.create_id%type) is
    --
    l_count number;
    --
    l_update_flag  boolean := false;
    l_record_exist boolean := false;
    --
    l_end_date_to_insert date := null;
    --
    l_program       varchar2(64) := 'XXADEO_RPAS_ASSORTMENT_SQL.XXADEO_ASSORT_CLUSTER_INSERT';
    l_error_message varchar2(2000);
    l_user_id       varchar2(30) := nvl(sys_context('USERENV',
                                                    'CLIENT_INFO'),
                                        sys_context('USERENV',
                                                    'SESSION_USER'));
    --
    cursor c_assort_cluster_count(v_item       in xxadeo_assortment_cluster.item%type,
                                  v_bu         in xxadeo_assortment_cluster.bu%type,
                                  v_cluster_id in xxadeo_assortment_cluster.cluster_id%type) is
      select count(1)
      from   xxadeo_assortment_cluster
      where  item = v_item
      and    bu = v_bu
      and    cluster_id = v_cluster_id
      --and    trunc(get_vdate) <=
      and    nvl(trunc(get_vdate),to_date('19000101', 'yyyymmdd')) <=
             nvl(end_date, to_date('99990101', 'yyyymmdd'));
    cursor c_assort_cluster_data(v_item       in xxadeo_assortment_cluster.item%type,
                                 v_bu         in xxadeo_assortment_cluster.bu%type,
                                 v_cluster_id in xxadeo_assortment_cluster.cluster_id%type) is
      select *
      from   xxadeo_assortment_cluster
      where  item = v_item
      and    bu = v_bu
      and    cluster_id = v_cluster_id
      --and    trunc(get_vdate) <=
      and    nvl(trunc(get_vdate),to_date('19000101', 'yyyymmdd')) <=
             nvl(end_date, to_date('99990101', 'yyyymmdd'))
      order  by start_date desc;
    --
    l_assort_cluster_rec c_assort_cluster_data%rowtype;
    --
  begin
    --
    open c_assort_cluster_count(i_item, i_bu, i_cluster_id);
    fetch c_assort_cluster_count
      into l_count;
    close c_assort_cluster_count;
    --
    -- Validate if exist rows to keys{item,bu,cluster_id}
    if l_count > 0
    then
      --
      -- Insert and Update Needed
      for assort_cluster_rec in c_assort_cluster_data(i_item,
                                                      i_bu,
                                                      i_cluster_id)
      loop
        --
        if trunc(assort_cluster_rec.start_date) = trunc(i_start_date)
        then
          --
          -- Record already exist with the same keys{item,bu,cluster_id,start_date}
          l_record_exist := true;
          --
          l_assort_cluster_rec := assort_cluster_rec;
          --
          exit;
          --
        elsif trunc(assort_cluster_rec.start_date) < trunc(i_start_date)
              and assort_cluster_rec.end_date is null
        then
          --
          update xxadeo_assortment_cluster
          set    end_date             = i_start_date - 1,
                 last_update_id       = l_user_id,
                 last_update_datetime = sysdate
          where  item = assort_cluster_rec.item
          and    bu = assort_cluster_rec.bu
          and    trunc(start_date) = trunc(assort_cluster_rec.start_date)
          and    cluster_id = assort_cluster_rec.cluster_id;
          --
          l_update_flag := true;
          --
          -- Exit because already update the previsous record.
          exit;
          --
        elsif trunc(assort_cluster_rec.start_date) < trunc(i_start_date)
              and assort_cluster_rec.end_date is not null
        then
          --
          -- This first condition it's depedent if the order by of the select of the cursor it's ASC or DESC.
          -- This case, we use DESC. So, the right condition is 'assort_cluster_rec.start_date <= I_start_date'
          --L_end_date_to_insert := assort_cluster_rec.end_date;
          l_end_date_to_insert := l_assort_cluster_rec.start_date - 1;
          --
          update xxadeo_assortment_cluster
          set    end_date             = i_start_date - 1,
                 last_update_id       = l_user_id,
                 last_update_datetime = sysdate
          where  item = assort_cluster_rec.item
          and    bu = assort_cluster_rec.bu
          and    trunc(start_date) = trunc(assort_cluster_rec.start_date)
          and    cluster_id = assort_cluster_rec.cluster_id;
          --
          l_update_flag := true;
          --
          -- Exit of the loop when find the middle of the values about start date
          exit;
          --
        end if;
        --
        l_assort_cluster_rec := assort_cluster_rec;
        --
      end loop;
      --
      if not l_update_flag
         and not l_record_exist
      then
        --
        l_end_date_to_insert := l_assort_cluster_rec.start_date - 1;
        --
      end if;
      --
    end if;
    --
    if not l_record_exist
    then
      --
      insert into xxadeo_assortment_cluster
        (item,
         bu,
         start_date,
         cluster_id,
         end_date,
         hq_recommendation,
         week,
         create_id,
         last_update_id)
      values
        (i_item,
         i_bu,
         i_start_date,
         i_cluster_id,
         l_end_date_to_insert,
         i_hq_recommendation,
         i_week,
         i_create_id,
         i_create_id);
      --
    end if;
    --
  exception
    when others then
      --
      if c_assort_cluster_count%isopen
      then
        close c_assort_cluster_count;
      end if;
      --
      if c_assort_cluster_data%isopen
      then
        close c_assort_cluster_data;
      end if;
      --
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
  end xxadeo_assort_cluster_insert;

  --
  ----------------------------------------------------------------------------------
  procedure xxadeo_assort_cluster_delete(i_item       in xxadeo_assortment_cluster.item%type,
                                         i_bu         in xxadeo_assortment_cluster.bu%type,
                                         i_start_date in xxadeo_assortment_cluster.start_date%type,
                                         i_cluster_id in xxadeo_assortment_cluster.cluster_id%type) is
    --
    l_end_date_of_deleted date;
    --
    l_program       varchar2(64) := 'XXADEO_RPAS_ASSORTMENT_SQL.XXADEO_ASSORT_CLUSTER_DELETE';
    l_error_message varchar2(2000);
    l_user_id       varchar2(30) := nvl(sys_context('USERENV',
                                                    'CLIENT_INFO'),
                                        sys_context('USERENV',
                                                    'SESSION_USER'));
    --
    cursor c_assort_cluster_del(v_item       in xxadeo_assortment_cluster.item%type,
                                v_bu         in xxadeo_assortment_cluster.bu%type,
                                v_cluster_id in xxadeo_assortment_cluster.cluster_id%type,
                                v_start_date in xxadeo_assortment_cluster.start_date%type) is
      select end_date
      from   xxadeo_assortment_cluster
      where  item = v_item
      and    bu = v_bu
      and    cluster_id = v_cluster_id
      and    trunc(start_date) = trunc(v_start_date)
      order  by start_date desc;
    --
  begin
    --
    open c_assort_cluster_del(i_item, i_bu, i_cluster_id, i_start_date);
    fetch c_assort_cluster_del
      into l_end_date_of_deleted;
    close c_assort_cluster_del;
    --
    delete from xxadeo_assortment_cluster
    where  item = i_item
    and    bu = i_bu
    and    cluster_id = i_cluster_id
    and    start_date = i_start_date;
    --
    if l_end_date_of_deleted is null
    then
      --
      update xxadeo_assortment_cluster
      set    end_date             = null,
             last_update_id       = l_user_id,
             last_update_datetime = sysdate
      where  item = i_item
      and    bu = i_bu
      and    cluster_id = i_cluster_id
      and    start_date = (select max(start_date)
                           from   xxadeo_assortment_cluster
                           where  item = i_item
                           and    bu = i_bu
                           and    cluster_id = i_cluster_id);
      --
    else
      --
      update xxadeo_assortment_cluster
      set    end_date             = l_end_date_of_deleted,
             last_update_id       = l_user_id,
             last_update_datetime = sysdate
      where  item = i_item
      and    bu = i_bu
      and    cluster_id = i_cluster_id
      and    end_date = (i_start_date - 1);
      --
    end if;
    --
  exception
    when others then
      --
      if c_assort_cluster_del%isopen
      then
        close c_assort_cluster_del;
      end if;
      --
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
  end xxadeo_assort_cluster_delete;

  --
  ----------------------------------------------------------------------------------
  procedure xxadeo_assort_store_insert(i_item              in xxadeo_assortment_store.item%type,
                                       i_store             in xxadeo_assortment_store.store%type,
                                       i_start_date        in xxadeo_assortment_store.start_date%type,
                                       i_response_date     in xxadeo_assortment_store.response_date%type,
                                       i_assortment_mode   in xxadeo_assortment_store.assortment_mode%type,
                                       i_hq_recommendation in xxadeo_assortment_store.hq_recommendation%type,
                                       i_not_mod_ind       in xxadeo_assortment_store.not_mod_ind%type,
                                       i_status            in xxadeo_assortment_store.status%type,
                                       i_comments          in xxadeo_assortment_store.comments%type,
                                       i_week              in xxadeo_assortment_store.week%type,
                                       i_create_id         in xxadeo_assortment_store.create_id%type,
                                       i_cluster_size      in xxadeo_assortment_store.cluster_size%type,
                                       i_cs_recommendation in xxadeo_assortment_store.cs_recommendation%type) is
    --
    l_count number;
    --
    l_update_flag  boolean := false;
    l_record_exist boolean := false;
    --
    l_end_date_to_insert date := null;
    --
    l_program       varchar2(64) := 'XXADEO_RPAS_ASSORTMENT_SQL.XXADEO_ASSORT_STORE_INSERT';
    l_error_message varchar2(2000);
    l_user_id       varchar2(30) := nvl(sys_context('USERENV',
                                                    'CLIENT_INFO'),
                                        sys_context('USERENV',
                                                    'SESSION_USER'));
    --
    cursor c_assort_store_count(v_item  in xxadeo_assortment_store.item%type,
                                v_store in xxadeo_assortment_store.store%type) is
      select count(1)
      from   xxadeo_assortment_store
      where  item = v_item
      and    store = v_store
      --and    trunc(get_vdate) <=
      and    nvl(trunc(get_vdate),to_date('19000101', 'yyyymmdd')) <=
             nvl(end_date, to_date('99990101', 'yyyymmdd'));
    cursor c_assort_store_data(v_item  in xxadeo_assortment_store.item%type,
                               v_store in xxadeo_assortment_store.store%type) is
      select *
      from   xxadeo_assortment_store
      where  item = v_item
      and    store = v_store
      --and    trunc(get_vdate) <=
      and    nvl(trunc(get_vdate),to_date('19000101', 'yyyymmdd')) <=
             nvl(end_date, to_date('99990101', 'yyyymmdd'))
      order  by start_date desc;
    --
    l_assort_store_rec c_assort_store_data%rowtype;
    --
  begin
    --
    open c_assort_store_count(i_item, i_store);
    fetch c_assort_store_count
      into l_count;
    close c_assort_store_count;
    --
    -- Validate if exist rows to keys{item,bu,cluster_id}
    if l_count > 0
    then
      --
      for assort_store_rec in c_assort_store_data(i_item, i_store)
      loop
        --
        -- when only have a record that have a end_date as null

        if trunc(assort_store_rec.start_date) = trunc(i_start_date)
        then
          --
          -- Record already exist with the same keys{item,store,start_date}
          l_record_exist := true;
          --
          l_assort_store_rec := assort_store_rec;
          --
          exit;
          --
        elsif trunc(assort_store_rec.start_date) < trunc(i_start_date)
              and assort_store_rec.end_date is null
        then
          --
          update xxadeo_assortment_store
          set    end_date             = i_start_date - 1,
                 last_update_id       = l_user_id,
                 last_update_datetime = sysdate
          where  item = assort_store_rec.item
          and    store = assort_store_rec.store
          and    trunc(start_date) = trunc(assort_store_rec.start_date);
          --
          l_update_flag := true;
          --
          -- Exit because already update the previous record.
          exit;
          --
        elsif trunc(assort_store_rec.start_date) < trunc(i_start_date)
              and assort_store_rec.end_date is not null /*and L_end_date_to_insert is null*/
        then
          --
          -- This first condition it's depedent if the order by of the select of the cursor it's ASC or DESC.
          -- This case, we use DESC. So, the right condition is 'assort_store_rec.start_date <= I_start_date'
          --L_end_date_to_insert := assort_store_rec.end_date;
          --
          update xxadeo_assortment_store
          set    end_date             = i_start_date - 1,
                 last_update_id       = l_user_id,
                 last_update_datetime = sysdate
          where  item = assort_store_rec.item
          and    store = assort_store_rec.store
          and    trunc(start_date) = trunc(assort_store_rec.start_date);
          --
          l_update_flag := true;
          --
          -- Exit of the loop when find the middle of the values about start date
          exit;
          --
        end if;
        --
        l_assort_store_rec := assort_store_rec;
        --
      end loop;
      --
      if l_update_flag
         and not l_record_exist
      then
        --
        --L_end_date_to_insert := L_assort_store_rec.start_date-1;
        --
        delete from xxadeo_assortment_store
        where  item = i_item
        and    store = i_store
        and    trunc(start_date) > trunc(i_start_date)
        and    trunc(i_start_date) > trunc(get_vdate)
        and    l_assort_store_rec.cluster_size != i_cluster_size;
        --
        if sql%rowcount = 0
        then
          --
          l_end_date_to_insert := l_assort_store_rec.start_date - 1;
          --
        end if;
        --
      elsif not l_update_flag
            and not l_record_exist
      then
        --
        --L_end_date_to_insert := L_assort_store_rec.start_date-1;
        --
        delete from xxadeo_assortment_store
        where  item = i_item
        and    store = i_store
        and    trunc(start_date) > trunc(i_start_date)
        and    trunc(i_start_date) > trunc(get_vdate)
        and    l_assort_store_rec.cluster_size != i_cluster_size;
        --
        if sql%rowcount = 0
        then
          --
          l_end_date_to_insert := l_assort_store_rec.start_date - 1;
          --
        end if;
        --
      end if;
      --
    end if;
    --
    if not l_record_exist
    then
      --
      insert into xxadeo_assortment_store
        (item,
         store,
         start_date,
         end_date,
         response_date,
         assortment_mode,
         hq_recommendation,
         not_mod_ind,
         status,
         week,
         comments,
         create_id,
         last_update_id,
         cluster_size,
         cs_recommendation)
      values
        (i_item,
         i_store,
         i_start_date,
         l_end_date_to_insert,
         i_response_date,
         i_assortment_mode,
         i_hq_recommendation,
         i_not_mod_ind,
         i_status,
         i_week,
         i_comments,
         i_create_id,
         i_create_id,
         i_cluster_size,
         i_cs_recommendation);
      --
    end if;
    --
  exception
    when others then
      --
      if c_assort_store_count%isopen
      then
        close c_assort_store_count;
      end if;
      --
      if c_assort_store_data%isopen
      then
        close c_assort_store_data;
      end if;
      --
      --
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
  end xxadeo_assort_store_insert;

  --
  ----------------------------------------------------------------------------------
  procedure xxadeo_assort_store_delete(i_item       in xxadeo_assortment_store.item%type,
                                       i_store      in xxadeo_assortment_store.store%type,
                                       i_start_date in xxadeo_assortment_store.start_date%type) is
    --
    l_end_date_of_deleted date;
    --
    l_program       varchar2(64) := 'XXADEO_RPAS_ASSORTMENT_SQL.XXADEO_ASSORT_STORE_DELETE';
    l_error_message varchar2(2000);
    l_user_id       varchar2(30) := nvl(sys_context('USERENV',
                                                    'CLIENT_INFO'),
                                        sys_context('USERENV',
                                                    'SESSION_USER'));
    --
    cursor c_assort_store_del(v_item       in xxadeo_assortment_store.item%type,
                              v_store      in xxadeo_assortment_store.store%type,
                              v_start_date in xxadeo_assortment_store.start_date%type) is
      select end_date
      from   xxadeo_assortment_store
      where  item = v_item
      and    store = v_store
      and    trunc(start_date) = trunc(v_start_date)
      order  by start_date desc;
    --
  begin
    --
    open c_assort_store_del(i_item, i_store, i_start_date);
    fetch c_assort_store_del
      into l_end_date_of_deleted;
    close c_assort_store_del;
    --
    delete from xxadeo_assortment_store
    where  item = i_item
    and    store = i_store
    and    start_date = i_start_date;
    --
    if l_end_date_of_deleted is null
    then
      --
      update xxadeo_assortment_store
      set    end_date             = null,
             last_update_id       = l_user_id,
             last_update_datetime = sysdate
      where  item = i_item
      and    store = i_store
      and    start_date = (select max(start_date)
                           from   xxadeo_assortment_store
                           where  item = i_item
                           and    store = i_store);
      --
    else
      --
      update xxadeo_assortment_store
      set    end_date             = l_end_date_of_deleted,
             last_update_id       = l_user_id,
             last_update_datetime = sysdate
      where  item = i_item
      and    store = i_store
      and    end_date = (i_start_date - 1);
      --
    end if;
    --
  exception
    when others then
      --
      if c_assort_store_del%isopen
      then
        close c_assort_store_del;
      end if;
      --
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
  end xxadeo_assort_store_delete;

  function xxadeo_assortment_tbl_process return boolean is

    l_program       varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_TBL_PROCESS';
    l_error_message varchar2(2000);

    l_vdate              date := get_vdate;
    l_history_flag       xxadeo_assortment.history_flag%type;
    l_cluster_popup_flag xxadeo_assortment.cluster_popup_flag%type;

    cursor cur_assortment is
      select a.item,
             a.store,
             a.start_date,
             a.end_date
      from   xxadeo_assortment a;

  begin

    /*************************************************************************************
    * DELETE RECORDS THAT WERE DELETED ON THE ORIGIN                                     *
    *************************************************************************************/
    delete from xxadeo_assortment
    where  (nvl(end_date, to_date(99990101, 'yyyymmdd')) < l_vdate)
    or     (item, store, start_date) in
           (select a.item       assort_item,
                    a.store      assort_store,
                    a.start_date assort_start_date /*,
                                                                               a.end_date   assort_end_date,
                                                                               s.item       store_item,
                                                                               s.store      store_store,
                                                                               s.start_date store_start_date,
                                                                               s.end_date   store_end_date*/
             from   xxadeo_assortment a
             left   join xxadeo_assortment_store s on a.item = s.item
                                               and    a.store = s.store
                                               and    a.start_date =
                                                      s.start_date
             where  s.item is null);

    /*************************************************************************************
    *                                                                                    *
    *************************************************************************************/

    merge into xxadeo_assortment a
    using (select s.item,
                  s.store,
                  s.start_date,
                  s.end_date,
                  null validated_date,
                  s.response_date,
                  s.assortment_mode,
                  s.hq_recommendation,
                  null curr_val_assrt,
                  case
                    when s.hq_recommendation = 'C' then
                     s.cs_recommendation
                    else
                     s.hq_recommendation
                  end store_choice,
                  s.not_mod_ind,
                  s.status,
                  xxadeo_assortment_history(i_item  => s.item,
                                            i_store => s.store) history_flag,
                  xxadeo_assortment_cluster_rec(i_item       => s.item,
                                                i_store      => s.store,
                                                i_start_date => s.start_date,
                                                i_end_date   => s.end_date) cluster_popup_flag,
                  s.week,
                  0 user_action,
                  s.comments,
                  lp_user_id create_id,
                  sysdate create_datetime,
                  lp_user_id last_update_id,
                  sysdate last_update_datetime,
                  s.cluster_size,
                  s.cs_recommendation
           from   xxadeo_assortment_store s
           where  nvl(s.end_date, to_date(99990101, 'yyyymmdd')) > nvl(l_vdate, trunc(sysdate))) xas
    on (a.item = xas.item and a.store = xas.store and trunc(a.start_date) = trunc(xas.start_date))
    when matched then
      update
      set    a.end_date             = xas.end_date,
             a.validated_date       = xas.validated_date,
             a.response_date        = xas.response_date,
             a.assortment_mode      = xas.assortment_mode,
             a.hq_recommendation    = xas.hq_recommendation,
             a.curr_val_assrt       = xas.curr_val_assrt,
             a.store_choice         = xas.store_choice,
             a.not_mod_ind          = xas.not_mod_ind,
             a.status               = xas.status,
             a.history_flag         = xas.history_flag,
             a.cluster_popup_flag   = xas.cluster_popup_flag,
             a.week                 = xas.week,
             a.user_action          = xas.user_action,
             a.comments             = xas.comments,
             a.cluster_size         = xas.cluster_size,
             a.cs_recommendation    = xas.cs_recommendation,
             a.last_update_id       = xas.last_update_id,
             a.last_update_datetime = xas.last_update_datetime
      where  a.store_choice != xas.store_choice
    when not matched then
      insert
        (a.item,
         a.store,
         a.start_date,
         a.end_date,
         a.validated_date,
         a.response_date,
         a.assortment_mode,
         a.hq_recommendation,
         a.curr_val_assrt,
         a.store_choice,
         a.not_mod_ind,
         a.status,
         a.history_flag,
         a.cluster_popup_flag,
         a.week,
         a.user_action,
         a.comments,
         a.create_id,
         a.create_datetime,
         a.last_update_id,
         a.last_update_datetime,
         a.cluster_size,
         a.cs_recommendation)
      values
        (xas.item,
         xas.store,
         xas.start_date,
         xas.end_date,
         xas.validated_date,
         xas.response_date,
         xas.assortment_mode,
         xas.hq_recommendation,
         xas.curr_val_assrt,
         xas.store_choice,
         xas.not_mod_ind,
         xas.status,
         xas.history_flag,
         xas.cluster_popup_flag,
         xas.week,
         xas.user_action,
         xas.comments,
         xas.create_id,
         xas.create_datetime,
         xas.last_update_id,
         xas.last_update_datetime,
         xas.cluster_size,
         xas.cs_recommendation);

    --FOR ALL UPDATE HISTORY_FLAG AND CLUSTER_POPUP_FLAG
    /*   update xxadeo_assortment a
    set    a.history_flag       = xxadeo_assortment_history(i_item  => a.item,
                                                            i_store => a.store),
           a.cluster_popup_flag = xxadeo_assortment_cluster_rec(i_item       => a.item,
                                                                i_store      => a.store,
                                                                i_start_date => a.start_date,
                                                                i_end_date   => a.end_date);
      where  a.item = l_cur_assortment.item
        and    a.store = l_cur_assortment.store
        and    a.start_date = l_cur_assortment.start_date;
    */
    commit;
    --dbms_mview.refresh(list => 'XXADEO_SP_V_ITEMS_ASSORT_MV');
    return true;
  exception
    when others then
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(l_error_message);
      rollback;
      return false;

  end xxadeo_assortment_tbl_process;

  ----------------------------------------------------------------------------------
  function xxadeo_assortment_batch(i_stage in varchar2) return boolean is

    l_vdate         date := trunc(nvl(get_vdate, sysdate));
    l_program       varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_BATCH';
    l_error_message varchar2(2000);

    cursor cur_assortment is
      select a.item,
             a.store,
             a.start_date,
             a.end_date,
             a.store_choice
      from   xxadeo_assortment a
      where  l_vdate between a.start_date and
             nvl(a.end_date, to_date(99990101, 'yyyymmdd'));

  begin

    if i_stage = g_stage_pre
    then
      /*************************************************************************************
      *  BEFORE NEW CATMAN RECORDS: VALIDATE CURRENT DATA AND UPDATE NECESSARY FIELDS      *
      *************************************************************************************/
      --The Response End Date will be deleted by the Store Portal the next day it is passed (As soon as �Today Date�> �Response End Date�).
      ----Assortment which still has not been validated by user will be automatically validated by the system and sent to CatMan.
      update xxadeo_assortment a
      set    a.response_date        = null,
             a.user_action          = 0,
             a.validated_date       = trunc(sysdate),
             a.last_update_id       = lp_user_id, /*SYSTEM*/
             a.last_update_datetime = sysdate
      where  trunc(a.response_date) < trunc(l_vdate)
      and    a.validated_date is null;

    elsif i_stage = g_stage_pos
    then

      --  Cell Current Regular Assortment is filled by Store Portal with the value Store Choice as soon as � Assortment Start Date �
      ---- (for this value of Store Choice) becomes equal to � Date of Today �.
      ------  Filled at the same time for all lines existing for the Item

      for l_cur_assortment in cur_assortment
      loop
        update xxadeo_assortment a
        set    a.curr_val_assrt       = l_cur_assortment.store_choice,
               a.last_update_id       = lp_user_id, /*SYSTEM*/
               a.last_update_datetime = sysdate
        where  a.item = l_cur_assortment.item
        and    a.store = l_cur_assortment.store;
      end loop;

      --Update Store Choice to HQ Recomendation or value of recommendation for the Cluster Size if "HQ recommendation" contains the value "C".
      -- for all lines that Response End Date is in 45 days or less
      update xxadeo_assortment a
      set    a.store_choice = (case
                                when a.hq_recommendation = 'C' then
                                 a.cs_recommendation
                                else
                                 a.hq_recommendation
                              end),
             a.last_update_id       = lp_user_id, /*SYSTEM*/
             a.last_update_datetime = sysdate
      where  trunc(a.response_date) - trunc(l_vdate) <= 45
      and    a.validated_date is null;

    else
      return false;
    end if;

    commit;
    --dbms_mview.refresh(list => 'XXADEO_SP_V_ITEMS_ASSORT_MV');
    return true;
  exception
    when others then
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(l_error_message);
      rollback;
      return false;

  end xxadeo_assortment_batch;
  ----------------------------------------------------------------------------------
  function xxadeo_assortment_history(i_item  in xxadeo_assortment.item%type,
                                     i_store in xxadeo_assortment.store%type)
    return number is

    l_count         number;
    l_program       varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_HISTORY';
    l_error_message varchar2(2000);
  begin

    /*    select count(1)
    into   l_count
    from   xxadeo_sp_v_assortment ia
    where  ia.item = i_item
    and    ia.store = i_store
    and    nvl(ia.end_date, to_date(99990101, 'yyyymmdd')) >=
           add_months(get_vdate, -18)
    and rownum = 2;*/

    select count(1)
    into   l_count
    /*,versions_starttime
    ,versions_endtime
    ,versions_operation*/
    from   xxadeo_assortment versions between timestamp minvalue and maxvalue a
    where  a.item = i_item
    and    a.store = i_store
    and    a.end_date < get_vdate
    and    rownum = 1;

    if l_count > 0
    then
      return 1;
    else
      return 0;
    end if;

  exception
    when others then
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(l_error_message);

      return 0;
  end xxadeo_assortment_history;

  ----------------------------------------------------------------------------------
  function xxadeo_assortment_cluster_rec(i_item       in xxadeo_assortment.item%type default null,
                                         i_store      in xxadeo_assortment.store%type,
                                         i_start_date in xxadeo_assortment.start_date%type,
                                         i_end_date   in xxadeo_assortment.end_date%type)
    return number is

    l_count         number;
    l_program       varchar2(64) := 'XXADEO_SP_ASSORTMENT_SQL.XXADEO_ASSORTMENT_CLUSTER_REC';
    l_error_message varchar2(2000);
  begin

    select count(1)
    into   l_count
    from   (select a.item,
                   a.store,
                   a.start_date,
                   a.end_date,
                   s.area bu,
                   sc.cluster_id,
                   max(sc.start_date) max_start_date
            from   xxadeo_assortment    a,
                   item_master          im,
                   xxadeo_store_cluster sc,
                   v_store              s
            where  a.item = im.item
            and    (im.dept = sc.dept and im.class = sc.class and
                  im.subclass = sc.subclass)
            and    a.store = sc.store
            and    a.store = s.store
            and    i_start_date = a.start_date
            and    a.item = i_item
            and    a.store = i_store
            group  by a.item,
                      a.store,
                      a.start_date,
                      a.end_date,
                      s.area,
                      sc.cluster_id) src,
           xxadeo_assortment_cluster ac
    where  src.item = ac.item
    and    src.cluster_id = ac.cluster_id
    and    src.bu = ac.bu
    and    (src.start_date between ac.start_date and
          nvl(ac.end_date, to_date(99990101, 'yyyymmdd')) or
          nvl(src.end_date, to_date(99990101, 'yyyymmdd')) between
          ac.start_date and
          nvl(ac.end_date, to_date(99990101, 'yyyymmdd')) or
          (src.start_date <= ac.start_date and
          nvl(src.end_date, to_date(99990101, 'yyyymmdd')) >=
          nvl(ac.end_date, to_date(99990101, 'yyyymmdd'))))
    and    rownum = 1;

    if l_count > 0
    then
      return 1;
    else
      return 0;
    end if;

  exception
    when others then
      l_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            sqlerrm,
                                            l_program,
                                            to_char(sqlcode));
      --
      dbms_output.put_line(l_error_message);

      return 0;
  end xxadeo_assortment_cluster_rec;

--
end xxadeo_rpas_assortment_sql;
/
