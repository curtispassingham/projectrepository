create or replace package XXADEO_RPAS_ASSORTMENT_SQL is

/********************************************************************************/
/* CREATE DATE - August 2018                                                    */
/* CREATE USER - Pedro Vieira                                                   */
/* PROJECT     - ADEO                                                           */
/* DESCRIPTION - Package "XXADEO_RPAS_ASSORTMENT_SQL" for Assortment            */
/********************************************************************************/

-- Procedure to validate, create and update new rows inside of the Assortment Cluster Table
----------------------------------------------------------------------------------
procedure xxadeo_assort_cluster_insert(I_item                 IN  xxadeo_assortment_cluster.item%type
                                      ,I_bu                   IN  xxadeo_assortment_cluster.bu%type
                                      ,I_start_date           IN  xxadeo_assortment_cluster.start_date%type
                                      ,I_cluster_id           IN  xxadeo_assortment_cluster.cluster_id%type
                                      ,I_hq_recommendation    IN  xxadeo_assortment_cluster.hq_recommendation%type
                                      ,I_week                 IN  xxadeo_assortment_cluster.week%type
                                      ,I_create_id            IN  xxadeo_assortment_cluster.create_id%type)
;

-- Procedure to delete and update the rows inside of the Assortment Cluster Table
----------------------------------------------------------------------------------
procedure xxadeo_assort_cluster_delete(I_item                 IN  xxadeo_assortment_cluster.item%type
                                      ,I_bu                   IN  xxadeo_assortment_cluster.bu%type
                                      ,I_start_date           IN  xxadeo_assortment_cluster.start_date%type
                                      ,I_cluster_id           IN  xxadeo_assortment_cluster.cluster_id%type)
;

-- Procedure to validate, create and update new rows inside of the Assortment Store Table
----------------------------------------------------------------------------------
procedure xxadeo_assort_store_insert(I_item                 IN  xxadeo_assortment_store.item%type
                                    ,I_store                IN  xxadeo_assortment_store.store%type
                                    ,I_start_date           IN  xxadeo_assortment_store.start_date%type
                                    ,I_response_date        IN  xxadeo_assortment_store.response_date%type
                                    ,I_assortment_mode      IN  xxadeo_assortment_store.assortment_mode%type
                                    ,I_hq_recommendation    IN  xxadeo_assortment_store.hq_recommendation%type
                                    ,I_not_mod_ind          IN  xxadeo_assortment_store.not_mod_ind%type
                                    ,I_status               IN  xxadeo_assortment_store.status%type
                                    ,I_comments             IN  xxadeo_assortment_store.comments%type
                                    ,I_week                 IN  xxadeo_assortment_store.week%type
                                    ,I_create_id            IN  xxadeo_assortment_store.create_id%type
                                    ,I_cluster_size         IN  xxadeo_assortment_store.cluster_size%type
                                    ,I_cs_recommendation    IN  xxadeo_assortment_store.cs_recommendation%type)
;

-- Procedure to delete and update the rows inside of the Assortment Store Table
----------------------------------------------------------------------------------
procedure xxadeo_assort_store_delete(I_item                 IN  xxadeo_assortment_store.item%type
                                    ,I_store                IN  xxadeo_assortment_store.store%type
                                    ,I_start_date           IN  xxadeo_assortment_store.start_date%type)
;
----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_TBL_PROCESS RETURN BOOLEAN;
----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_BATCH(I_stage IN VARCHAR2) RETURN BOOLEAN;
----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_CLUSTER_REC(I_item          IN XXADEO_ASSORTMENT.ITEM%TYPE DEFAULT NULL,
                                         I_store         IN XXADEO_ASSORTMENT.STORE%TYPE,
                                         I_start_date    IN XXADEO_ASSORTMENT.START_DATE%TYPE,
                                         I_end_date      IN XXADEO_ASSORTMENT.END_DATE%TYPE)
    RETURN number;
----------------------------------------------------------------------------------
  FUNCTION XXADEO_ASSORTMENT_HISTORY(I_item          IN XXADEO_ASSORTMENT.ITEM%TYPE,
                                     I_store         IN XXADEO_ASSORTMENT.STORE%TYPE)
    RETURN number;
--
end XXADEO_RPAS_ASSORTMENT_SQL;
/
