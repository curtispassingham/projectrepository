/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_ASSORTMENT_CLUSTER" table for Integration 	  */
/*					between Store Portal and CATMAN				        	  */
/******************************************************************************/

-- Create Table
CREATE TABLE XXADEO_ASSORTMENT_CLUSTER
(ITEM                  VARCHAR2(25 BYTE)  	NOT NULL ENABLE,
 BU                    NUMBER(10,0)       	NOT NULL ENABLE,
 START_DATE            DATE               	NOT NULL ENABLE,
 CLUSTER_ID            VARCHAR2(30 BYTE)  	NOT NULL ENABLE,
 END_DATE              DATE               	,
 HQ_RECOMMENDATION     VARCHAR2(7 BYTE)   	NOT NULL ENABLE,
 WEEK				           VARCHAR2(8 BYTE)		NOT NULL ENABLE,
 CREATE_ID             VARCHAR2(30 BYTE)  	DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE,
 CREATE_DATETIME       DATE 				DEFAULT SYSDATE NOT NULL ENABLE,
 LAST_UPDATE_ID        VARCHAR2(30 BYTE)  	DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE,
 LAST_UPDATE_DATETIME  DATE 				DEFAULT SYSDATE NOT NULL ENABLE
 );
-- Add comments to the columns
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.ITEM is 'Item Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.BU is 'BU Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.START_DATE is 'Date on which the recommended assortment becomes effective. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.CLUSTER_ID is 'Cluster identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.END_DATE is 'Date on which the recommended assortment is no longer valid. Calculated as the next start date minus one day or null if no other start date exists..'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.HQ_RECOMMENDATION is 'Recommended assortment for the item.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.WEEK is 'Week Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.CREATE_ID is 'User that created the record.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.CREATE_DATETIME is 'Date in which the record was created.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.LAST_UPDATE_ID is 'User that last updated the record.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT_CLUSTER.LAST_UPDATE_DATETIME is 'Date in which the record was last updated.'
;
-- Create/Recreate primary, unique and foreign key constraints
ALTER TABLE XXADEO_ASSORTMENT_CLUSTER
 ADD CONSTRAINT XXADEO_PK_ASSORTMENT_CLUSTER PRIMARY KEY
  (ITEM,
   BU,
   START_DATE,
   CLUSTER_ID
 );
