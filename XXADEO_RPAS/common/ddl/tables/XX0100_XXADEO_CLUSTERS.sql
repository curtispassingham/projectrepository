/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_CLUSTERS" table for Integration between		  */ 
/*					Store Portal and CATMAN				        	 		  */
/******************************************************************************/

-- Create Table
CREATE TABLE XXADEO_CLUSTERS 
(
 CLUSTER_ID      		VARCHAR2(30 BYTE)  NOT NULL ENABLE, 
 CLUSTER_LABEL   		VARCHAR2(30 BYTE)  NOT NULL ENABLE,
 CLUSTER_TYPE_ID 		VARCHAR2(30 BYTE)  NOT NULL ENABLE, 
 CLUSTER_TYPE_LABEL    	VARCHAR2(30 BYTE)  NOT NULL ENABLE,  
 BU_ID		            NUMBER(10,0)       NOT NULL ENABLE,
 BU_LABEL		    	VARCHAR2(30 BYTE)  NOT NULL ENABLE, 
 CREATE_ID       		VARCHAR2(30 BYTE)  DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE, 
 CREATE_DATETIME 		DATE DEFAULT SYSDATE NOT NULL ENABLE
 );
-- Add comments to the columns    
COMMENT ON COLUMN XXADEO_CLUSTERS.BU_ID is 'BU Identifier. Sent from CATMAN.' 
;
COMMENT ON COLUMN XXADEO_CLUSTERS.BU_LABEL is 'BU Name. Sent from CATMAN.' 
;  
COMMENT ON COLUMN XXADEO_CLUSTERS.CLUSTER_TYPE_ID is 'Cluster Type Identifier. Sent from CATMAN.' 
;
COMMENT ON COLUMN XXADEO_CLUSTERS.CLUSTER_TYPE_LABEL is 'Cluster Type Name. Sent from CATMAN.' 
;  
COMMENT ON COLUMN XXADEO_CLUSTERS.CLUSTER_ID is 'Cluster Identifier. Sent from CATMAN.' 
;  
COMMENT ON COLUMN XXADEO_CLUSTERS.CLUSTER_LABEL is 'Cluster Name. Sent from CATMAN.' 
;                
COMMENT ON COLUMN XXADEO_CLUSTERS.CREATE_ID is 'User that created the record.'  
;                 
COMMENT ON COLUMN XXADEO_CLUSTERS.CREATE_DATETIME is 'Date in which the record was created.' 
;
-- Create/Recreate primary, unique and foreign key constraints 
ALTER TABLE XXADEO_CLUSTERS
 ADD CONSTRAINT XXADEO_PK_CLUSTERS PRIMARY KEY
  (
   CLUSTER_ID
 ); 
