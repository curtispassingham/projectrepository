/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_STORE_CLUSTER" table for Integration between  */
/*					Store Portal and CATMAN				        	 		  */
/******************************************************************************/

-- Create Table
CREATE TABLE XXADEO_STORE_CLUSTER
(DEPT                  NUMBER(4,0)  NOT NULL ENABLE,
 CLASS                 NUMBER(4,0)  NOT NULL ENABLE,
 SUBCLASS              NUMBER(4,0)  NOT NULL ENABLE,
 STORE                 NUMBER(10,0) NOT NULL ENABLE,
 CLUSTER_ID            VARCHAR2(30 BYTE) NOT NULL ENABLE,
 START_DATE            DATE              NOT NULL ENABLE,
 LINK_STORE_CLUSTER    VARCHAR2(1 BYTE)  NOT NULL ENABLE,
 WEEK				   VARCHAR2(8 BYTE)		NOT NULL ENABLE,
 CREATE_ID             VARCHAR2(30 BYTE) DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE,
 CREATE_DATETIME       DATE DEFAULT SYSDATE NOT NULL ENABLE,
 LAST_UPDATE_ID        VARCHAR2(30 BYTE)  	DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE,
 LAST_UPDATE_DATETIME  DATE 				DEFAULT SYSDATE NOT NULL ENABLE
 );
-- Add comments to the columns
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.DEPT is 'Department Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.CLASS is 'Class Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.SUBCLASS is 'Subclass Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.STORE is 'Store Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.CLUSTER_ID is 'Cluster identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.START_DATE is 'Date which the association becomes valid. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.LINK_STORE_CLUSTER is 'Indicator of the relationship between store and cluster if it is activate (Y) or deactivate (N). Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.WEEK is 'Week Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.CREATE_ID is 'User that created the record.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.CREATE_DATETIME is 'Date in which the record was created.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.LAST_UPDATE_ID is 'User that last updated the record.'
;
COMMENT ON COLUMN XXADEO_STORE_CLUSTER.LAST_UPDATE_DATETIME is 'Date in which the record was last updated.'
;
-- Create/Recreate primary, unique and foreign key constraints
ALTER TABLE XXADEO_STORE_CLUSTER
 ADD CONSTRAINT XXADEO_PK_STORE_CLUSTER PRIMARY KEY
  (DEPT,
   CLASS,
   SUBCLASS,
   STORE,
   CLUSTER_ID,
   START_DATE
  );
