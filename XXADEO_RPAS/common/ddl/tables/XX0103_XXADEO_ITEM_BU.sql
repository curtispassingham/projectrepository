/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_ITEM_BU" table for Integration between		  */ 
/*					Store Portal and CATMAN				        	 		  */
/******************************************************************************/

-- Create Table
CREATE TABLE XXADEO_ITEM_BU 
(ITEM                   VARCHAR2(25 BYTE)  NOT NULL ENABLE,
 BU                     NUMBER(10,0)       NOT NULL ENABLE,
 RANGE_LETTER           VARCHAR2(1 BYTE)   				  ,
 GRADING                NUMBER(4,0)        				  ,
 OMNICHANNEL_START_DATE DATE               NOT NULL ENABLE,
 OMNICHANNEL_END_DATE   DATE							  ,
 SUBSTITUTE_ITEM		VARCHAR2(25 BYTE)				  ,
 SEASON_ITEM_IND		VARCHAR2(1 BYTE)   NOT NULL ENABLE,
 CREATE_ID              VARCHAR2(30 BYTE) DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE, 
 CREATE_DATETIME        DATE DEFAULT SYSDATE NOT NULL ENABLE,
 LAST_UDPATE_ID         VARCHAR2(30 BYTE) DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE, 
 LAST_UDPATE_DATETIME   DATE DEFAULT SYSDATE NOT NULL ENABLE
 );
-- Add comments to the columns             
COMMENT ON COLUMN XXADEO_ITEM_BU.ITEM is 'Item Unique Identifier. Sent from CATMAN.' 
;               
COMMENT ON COLUMN XXADEO_ITEM_BU.BU is 'BU Unique Identifier. Sent from CATMAN.' 
;              
COMMENT ON COLUMN XXADEO_ITEM_BU.RANGE_LETTER is 'Indicates the Range Letter. Sent from CATMAN.' 
;         
COMMENT ON COLUMN XXADEO_ITEM_BU.GRADING is 'Indicates the item grading. Sent from CATMAN.' 
;               
COMMENT ON COLUMN XXADEO_ITEM_BU.OMNICHANNEL_START_DATE is 'Item omnichannel selling start date. Sent from CATMAN.' 
;              
COMMENT ON COLUMN XXADEO_ITEM_BU.OMNICHANNEL_END_DATE is 'Item omnichannel selling end date. Sent from CATMAN.' 
;               
COMMENT ON COLUMN XXADEO_ITEM_BU.SUBSTITUTE_ITEM is 'Substitute item unique identifier. Sent from CATMAN.' 
;              
COMMENT ON COLUMN XXADEO_ITEM_BU.SEASON_ITEM_IND is 'Indicates if it''s a seasonal item. Sent from CATMAN.' 
;                
COMMENT ON COLUMN XXADEO_ITEM_BU.CREATE_ID is 'User that created the record.'  
;                 
COMMENT ON COLUMN XXADEO_ITEM_BU.CREATE_DATETIME is 'Date in which the record was created.' 
;
COMMENT ON COLUMN XXADEO_ITEM_BU.LAST_UDPATE_ID is 'User that last updated the record.'  
;                 
COMMENT ON COLUMN XXADEO_ITEM_BU.LAST_UDPATE_DATETIME is 'Date in which the record was last updated.' 
;
-- Create/Recreate primary, unique and foreign key constraints 
ALTER TABLE XXADEO_ITEM_BU
 ADD CONSTRAINT XXADEO_PK_ITEM_BU PRIMARY KEY
  (ITEM,
   BU
 );
