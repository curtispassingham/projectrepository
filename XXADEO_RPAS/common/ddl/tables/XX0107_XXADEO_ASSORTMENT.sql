/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_ASSORTMENT" table for Integration 	  		    */
/*					               between Store Portal and CATMAN				        	  */
/******************************************************************************/

-- Create Table
CREATE TABLE XXADEO_ASSORTMENT
(ITEM                  	VARCHAR2(25 BYTE)  	NOT NULL ENABLE,
 STORE                 	NUMBER(10,0)       	NOT NULL ENABLE,
 START_DATE            	DATE               	NOT NULL ENABLE,
 END_DATE              	DATE				,
 VALIDATED_DATE        	DATE				,
 RESPONSE_DATE         	DATE				,
 ASSORTMENT_MODE       	VARCHAR2(2 BYTE)   	NOT NULL ENABLE,
 CLUSTER_SIZE           VARCHAR2(30 BYTE)    NOT NULL ENABLE,
 CS_RECOMMENDATION      VARCHAR2(7 BYTE)    NOT NULL ENABLE,
 HQ_RECOMMENDATION     	VARCHAR2(7 BYTE)   	NOT NULL ENABLE,
 CURR_VAL_ASSRT        	VARCHAR2(7 BYTE)	,
 STORE_CHOICE          	VARCHAR2(7 BYTE)   	NOT NULL ENABLE,
 NOT_MOD_IND           	VARCHAR2(1 BYTE)   	NOT NULL ENABLE,
 STATUS                	NUMBER(1,0)			    NOT NULL ENABLE,
 HISTORY_FLAG 			NUMBER(1,0),
 CLUSTER_POPUP_FLAG 	NUMBER(1,0),
 WEEK					          VARCHAR2(8 BYTE)	  NOT NULL ENABLE,
 USER_ACTION			      NUMBER(1,0)         NOT NULL ENABLE,
 COMMENTS              	VARCHAR2(4000 BYTE)	,
 CREATE_ID             	VARCHAR2(30 BYTE)  	DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE,
 CREATE_DATETIME       	DATE 				DEFAULT SYSDATE NOT NULL ENABLE,
 LAST_UPDATE_ID        	VARCHAR2(30 BYTE)   DEFAULT NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) NOT NULL ENABLE,
 LAST_UPDATE_DATETIME  	DATE 				DEFAULT SYSDATE NOT NULL ENABLE
 );
-- Add comments to the columns
COMMENT ON COLUMN XXADEO_ASSORTMENT.ITEM is 'Item Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.STORE is 'Store Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.START_DATE is 'Date on which the recommended assortment becomes effective. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.END_DATE is 'Date on which the recommended assortment is no longer valid. Calculated as the next start date minus one day or null if no other start date exists.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.VALIDATED_DATE is 'Date which the user has made the assortment validation'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.RESPONSE_DATE is 'Date limit which a user will have to act on the assortment from the Store perspective. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.ASSORTMENT_MODE is 'Indicates if the assortment if national, local or test. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.CLUSTER_SIZE is 'Contains different values of size cluster.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.CS_RECOMMENDATION is 'Recommended assortment for the size cluster.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.HQ_RECOMMENDATION is 'Recommended assortment for the item. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.CURR_VAl_ASSRT is 'This column is calculated by the Store Portal with the value Store Choice when the start date is reached.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.STORE_CHOICE is 'The assortment chosen by the Store User.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.NOT_MOD_IND is 'Indicator which prevents store to change the assortment.Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.STATUS is 'Assortment change status in CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.HISTORY_FLAG is 'Flag that indicates if the record as history records.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.CLUSTER_POPUP_FLAG is 'Flag that indicates in the record has valid options for the Cluster Pop Up.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.WEEK is 'Week Unique Identifier. Sent from CATMAN.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.USER_ACTION is 'Determine if the change has been made by user or system.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.COMMENTS is 'Comments of the CATMAN user.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.CREATE_ID is 'User that created the record.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.CREATE_DATETIME is 'Date in which the record was created.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.LAST_UPDATE_ID is 'User that last updated the record.'
;
COMMENT ON COLUMN XXADEO_ASSORTMENT.LAST_UPDATE_DATETIME is 'Date in which the record was last updated.'
;
-- Create/Recreate primary, unique and foreign key constraints
ALTER TABLE XXADEO_ASSORTMENT
 ADD CONSTRAINT XXADEO_PK_ASSORTMENT PRIMARY KEY
  (ITEM,
   STORE,
   START_DATE
 );
