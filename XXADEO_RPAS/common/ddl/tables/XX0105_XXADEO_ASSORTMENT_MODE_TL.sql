/******************************************************************************/
/* CREATE DATE - May 2018                                                     */
/* CREATE USER - Pedro Vieira                                                 */
/* PROJECT     - ADEO                                                         */
/* DESCRIPTION - DML of "XXADEO_ASSORTMENT_MODE_TL" table for Integration 	  */
/*					between Store Portal and CATMAN				        	  */
/******************************************************************************/

-- Create table
create table XXADEO_ASSORTMENT_MODE_TL
(
  lang                   NUMBER(6) not null,
  assortment_mode_id     VARCHAR2(2) not null,
  assortment_mode_label  VARCHAR2(30) not null,
  create_id              VARCHAR2(30) default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) not null,
  create_datetime        DATE default SYSDATE not null,
  last_update_id         VARCHAR2(30) default NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER')) not null,
  last_update_datetime   DATE default SYSDATE not null
);
-- Add comments to the columns
comment on column XXADEO_ASSORTMENT_MODE_TL.lang
  is 'This field contains the language in which the translated text is maintained. ';
comment on column XXADEO_ASSORTMENT_MODE_TL.assortment_mode_id
  is 'Assortmentb Mode Unique Identifier. Sent from CATMAN.';
comment on column XXADEO_ASSORTMENT_MODE_TL.assortment_mode_label
  is 'Assortment mode lablel translated into the different languages. Sent from CATMAN.';
comment on column XXADEO_ASSORTMENT_MODE_TL.create_id
  is 'User that created the record.';
comment on column XXADEO_ASSORTMENT_MODE_TL.create_datetime
  is 'Date in which the record was created.';
comment on column XXADEO_ASSORTMENT_MODE_TL.last_update_id
  is 'User that last updated the record.';
comment on column XXADEO_ASSORTMENT_MODE_TL.last_update_datetime
  is 'Date in which the record was last updated.';
-- Create/Recreate primary, unique and foreign key constraints
alter table XXADEO_ASSORTMENT_MODE_TL
  add constraint XXADEO_PK_ASSORTMENT_MODE_TL primary key (LANG, ASSORTMENT_MODE_ID);
