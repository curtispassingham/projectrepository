set serveroutput on size unlimited
set escape on

declare
    -- Destination
    param_1 varchar2(30):=sys.dbms_assert.schema_name(upper('&1'));
    -- Owner
    param_2 varchar2(30):=sys.dbms_assert.schema_name(upper('&2'));

BEGIN

	execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_ITEM_BU TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_CLUSTERS TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_ASSORTMENT_CLUSTER TO '||param_1;
	execute immediate 'GRANT SELECT, INSERT, UPDATE, DELETE ON '||param_2||'.XXADEO_ASSORTMENT TO '||param_1;
	execute immediate 'GRANT FLASHBACK ON '||param_2||'.XXADEO_ASSORTMENT TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_ASSORTMENT_STORE TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_STORE_CLUSTER TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_ASSORTMENT_MODE_TL TO '||param_1;
	execute immediate 'GRANT SELECT ON '||param_2||'.XXADEO_ASSORTMENT_VALUE_TL TO '||param_1;

END;
/
