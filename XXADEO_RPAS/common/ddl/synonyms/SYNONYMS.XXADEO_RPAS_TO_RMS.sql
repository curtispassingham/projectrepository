set serveroutput on size unlimited
set escape on

declare
    -- Destination
    -- param_1 varchar2(30):=sys.dbms_assert.schema_name(upper(_var1'));
    param_1 varchar2(30):=upper('&DBLINK');
    -- Owner
    param_2 varchar2(30):=sys.dbms_assert.schema_name(upper('&DESTINATION'));

BEGIN

  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.ITEM_MASTER FOR ITEM_MASTER@'||param_1;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.GET_VDATE FOR GET_VDATE@'||param_1;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.SQL_LIB FOR SQL_LIB@'||param_1;
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.LANG FOR LANG@'||param_1;
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.STORE FOR STORE@'||param_1;
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.V_STORE FOR V_STORE@'||param_1;
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.V_STORE_TL FOR V_STORE_TL@'||param_1;
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.STORE_HIERARCHY FOR STORE_HIERARCHY@'||param_1;
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.STORE_TL FOR STORE_TL@'||param_1;
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.LANGUAGE_SQL FOR LANGUAGE_SQL@'||param_1;
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.GET_PRIMARY_LANG FOR GET_PRIMARY_LANG@'||param_1;
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_2||'.SYSTEM_OPTIONS FOR SYSTEM_OPTIONS@'||param_1;

END;
/
