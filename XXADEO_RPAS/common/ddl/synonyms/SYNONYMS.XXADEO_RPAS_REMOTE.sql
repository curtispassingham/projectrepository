set serveroutput on size unlimited
set escape on

declare
    -- Destination
    param_1 varchar2(30):=sys.dbms_assert.schema_name(upper('&1'));
    -- Owner
    param_2 varchar2(30):=sys.dbms_assert.schema_name(upper('&2'));

BEGIN

	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ITEM_BU FOR '||param_2||'.XXADEO_ITEM_BU';
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_CLUSTERS FOR '||param_2||'.XXADEO_CLUSTERS';
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT_CLUSTER FOR '||param_2||'.XXADEO_ASSORTMENT_CLUSTER';
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT FOR '||param_2||'.XXADEO_ASSORTMENT';
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT_STORE FOR '||param_2||'.XXADEO_ASSORTMENT_STORE';
	execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_STORE_CLUSTER FOR '||param_2||'.XXADEO_STORE_CLUSTER';
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT_MODE_TL FOR '||param_2||'.XXADEO_ASSORTMENT_MODE_TL';
  execute immediate 'CREATE OR REPLACE SYNONYM '||param_1||'.XXADEO_ASSORTMENT_VALUE_TL FOR '||param_2||'.XXADEO_ASSORTMENT_VALUE_TL';

END;
/
