#!/bin/ksh

########################################################
#
# Script to load Clusters inside of the Store Portal(SP)
# This script is depend of the another interface to do
# extract information from CATMAN.
# This data it will be load to Store Portal in the
# schema 'XXADEO_RPAS' inside the table
# 'XXADEO_CLUSTERS'
#  _________________________________________________________
# | Workfile:      | xxadeo_spl_clusters.ksh     	    |
# | Created by:    | PV                                     |
# | Creation Date: | 20180402-1010                          |
# | Modify Date:   | 20180925-1000                          |
# | Version:       | 0.3                                    |
#  --------------------------------------------------------- 
#  History:
#	0.1 - Initial Release
#	0.2 - Structure of the input file and operations 
#		were changed 
#	0.3 - Installation in Adeo Environment. Changes 
#		according to the environment
#
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME='XXADEO_SPL_CLUSTERS'

if [[ $# -gt 0  ]]; then
	#       ADD to variable the input parameter (File)
	V_INPUT_FILE=$1
	#	DROP input parameter
	shift
fi


########################################################
#  INCLUDES ########################
#  This section must come after PROGRAM DEFINES
########################################################

#. ${RMS_STOREPORTAL_HOME}/rfx/etc/rmse_rpas_config.env		# Neashore ENV (Store Portal)
. ${RETLforXXADEO}/rfx/etc/xxadeo_sp_config.env			# Adeo ENV (Store Portal)

#. ${LIB_DIR}/rmse_rpas_lib.ksh
. ${LIB_DIR}/xxadeo_rmse_lib.ksh				# Adeo ENV (RMS & Store Portal)

#	Only Testing variable according specification
#export MMIN=$RMS_STOREPORTAL_HOME

message "Program started ..."

########################################################
#  VARIABLES DEFINES 
#  This section must defines all variables you need
########################################################

#	Pattern to DateTime OutPut Data
V_PATTERN_DATE_OUTPUT="YYYYMMDDHH24MISS"
#	Flow 
RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml
#	Table Name
V_TABLE_NAME="XXADEO_CLUSTERS"
#	All Fields Required to Insert or Delete.
V_ALL_INPUT_FIELDS="BU CLUSTER_TYPE CLUSTER_ID"


###############################################################################
#  OUTPUT DEFINES & VALIDATIONS
#  This section must come after INCLUDES and Some Validations
###############################################################################

V_IN_FOLDER=$RETL_IN/STOREPORTAL

#	Check name of File after export.
if [ -z $V_INPUT_FILE ]; then 
#	message "[ERROR] Missing Input Parameter! ('File Name')"
	message "[INFO] Didn't receive input filename as parameter. Searching inside the folder!"
	# Looking for XXADEO_ASSRT_CLUSTERS_YYYYMMDD-HH24MISS.dat
	V_INPUT_FILE=`ls -t ${RETL_IN}/STOREPORTAL | grep 'XXADEO_ASSRT_CLUSTERS_.*\.dat$' | head -1`

#	<< DEPRECATED >>
#
#	rmse_terminate -1
#elif [[ "$V_INPUT_FILE" = *".dat" || "$V_INPUT_FILE" = *".DAT" ]]; then
#	message "[INFO] File Contains extension '.dat'!"
#else
#	message "[INFO] File Not Contains extension '.dat'!"
#	V_INPUT_FILE=$V_INPUT_FILE.dat
fi;

export INPUT_FILE_CLUSTERS=$V_IN_FOLDER/$V_INPUT_FILE

#	<< DEPRECATED >>
#
#	Name of file is OK
#	Check if specification variable to input folder is available
#if [ -z "$MMIN" ]; then
	#	$MMIN it's not defined or it's empty.
#	export INPUT_FILE_CLUSTERS=$DATA_DIR/$V_INPUT_FILE
#	V_IN_FOLDER=$DATA_DIR
#else
#	export INPUT_FILE_CLUSTERS=$MMIN/data/Store_Portal/$V_INPUT_FILE
#	V_IN_FOLDER=$MMIN/data/Store_Portal
#fi;

#	Check If file Exist
if [ ! -f $INPUT_FILE_CLUSTERS ]; then
	message "[ERROR] File Not Exist! ('${INPUT_FILE_CLUSTERS}')"
	rmse_terminate -2
fi;

#	Check If file is empty or not
if [ ! -s $INPUT_FILE_CLUSTERS ]; then
	message "[ERROR] File is Empty! ('${INPUT_FILE_CLUSTERS}')"
	rmse_terminate -3
fi;

export INPUT_SCHEMA_CLUSTERS=$SCHEMA_DIR/XXADEO_CLUSTERS.schema

###############################################################################
#	Validate File Content Data
###############################################################################

#	N/A - The input file of this interface it's a full loaded data.

###############################################################################
#  Create a disk-based flow file
###############################################################################

message "[INFO] Start Write Flow! "

#	STEP 1 - Start of the Flow
cat > $RETL_FLOW_FILE << EOF
<FLOW name = "$PROGRAM_NAME.flw">
	<!-- IMPORT: File -->
	<OPERATOR type="import">
		<PROPERTY name="inputfile" value="${INPUT_FILE_CLUSTERS}"/>
		<PROPERTY name="schemafile" value="${INPUT_SCHEMA_CLUSTERS}"/>
		<OUTPUT    name="allClusters.v"/>
	</OPERATOR>

	${DBPREPSTMT}	
		<PROPERTY name="tablename" value="${V_TABLE_NAME}" />
        	<INPUT name="allClusters.v" />
		<PROPERTY name="statement">
                	<![CDATA[
			declare
			--
			P_cluster_id		${V_TABLE_NAME}.cluster_id%type := ? ;
			P_cluster_label 	${V_TABLE_NAME}.cluster_label%type := ? ;
			P_cluster_type_id	${V_TABLE_NAME}.cluster_type_id%type := ? ;
			P_cluster_type_label	${V_TABLE_NAME}.cluster_type_label%type := ? ;
			P_bu_id			${V_TABLE_NAME}.bu_id%type := ? ;
			P_bu_label		${V_TABLE_NAME}.bu_label%type := ? ;
			--
			begin
			--
			insert into ${V_TABLE_NAME} (cluster_id
						    ,cluster_label
						    ,cluster_type_id
						    ,cluster_type_label
						    ,bu_id
						    ,bu_label)
			values	(P_cluster_id
				,P_cluster_label
				,P_cluster_type_id
				,P_cluster_type_label
				,P_bu_id
				,P_bu_label);
			--
			end;
			]]>
                </PROPERTY>
                <PROPERTY name="fields" value="CLUSTER_ID CLUSTER_LABEL CLUSTER_TYPE_ID CLUSTER_TYPE_LABEL BU_ID BU_LABEL"/>
	</OPERATOR>
	
</FLOW>
	
EOF

message "[INFO] End Write Flow!"

###############################################################################
#  Execute the SQL Plus - Truncate Table
###############################################################################
message "[INFO] SQL@Plus Operator - Doing truncate table to ${V_TABLE_NAME} !"

sqlplus /nolog << EOF
	conn $SQLPLUS_LOGON;
	WHENEVER SQLERROR EXIT 4;
	TRUNCATE TABLE ${V_TABLE_NAME};
	EXIT;
EOF

SQLPLUS_STAT=$?

message "[INFO] SQL@Plus Operator - Status Result ${SQLPLUS_STAT} !"

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

rmse_terminate 0
